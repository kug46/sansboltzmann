INCLUDE( CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

#Get the name of the binary directory
STRING( TOUPPER ${CMAKE_BINARY_DIR} BIN_DIR_NAME )
GET_FILENAME_COMPONENT( BIN_DIR_NAME ${BIN_DIR_NAME} NAME_WE )

#A pretty little welcome message
MESSAGE("\n\n         _____ ___    _   _______\n        / ___//   |  / | / / ___/\n        \\__ \\/ /| | /  |/ /\\__ \\ \n       ___/ / ___ |/ /|  /___/ / \n      /____/_/  |_/_/ |_//____/  \n" )
MESSAGE(" Solution Adaptive Numerical Simulator\n\n")

#This must come before PROJECT. It changes the compiler based on the build directory name
INCLUDE( CMakeInclude/Compiler.cmake )

SET(CMAKE_LEGACY_CYGWIN_WIN32 0)

PROJECT( SANS CXX C )
CMAKE_MINIMUM_REQUIRED( VERSION 2.8.8 FATAL_ERROR )

LIST(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/CMakeInclude )
LIST(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/CMakeInclude/Finds )
LIST(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/CMakeInclude/Checks )
LIST(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/CMakeInclude/Builds )
LIST(APPEND CMAKE_PREFIX_PATH $ENV{VERA_ROOT} )

INCLUDE(CheckCXXSourceCompiles)
INCLUDE(CheckCXXSourceRuns)

#Don't enable testing for IDE's
IF( NOT CMAKE_GENERATOR MATCHES "Xcode" )
  ENABLE_TESTING()
  INCLUDE(CTest)
  MARK_AS_ADVANCED( BUILD_TESTING )
ENDIF()


# Make sure we include from our source first in case dependencies have similar names
INCLUDE_DIRECTORIES( src test )

INCLUDE( CMakeInclude/CompilerFlags.cmake )
INCLUDE( CMakeInclude/XCodeFileGlob.cmake )
INCLUDE( CMakeInclude/Vera.cmake )
INCLUDE( CMakeInclude/CopyPCH.cmake )
INCLUDE( CMakeInclude/HeaderCompileCheck.cmake )
INCLUDE( CMakeInclude/StaticAnalysis.cmake )
INCLUDE( CMakeInclude/Dependencies.cmake )
INCLUDE( CMakeInclude/SANScotire.cmake ) #Must be after Dependencies.cmake
INCLUDE( CMakeInclude/Valgrind.cmake )
INCLUDE( CMakeInclude/ccache.cmake )

OPTION( USE_TIMEDCOMPILE "Time each compiler command" OFF )
IF( USE_TIMEDCOMPILE )
  GET_PROPERTY(OLD_RULE_LAUNCH_COMPILE GLOBAL PROPERTY RULE_LAUNCH_COMPILE )
  SET_PROPERTY(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "time ${OLD_RULE_LAUNCH_COMPILE}")
ENDIF()


#==================================================
# A list of unit tests that are intentionally skipped
# because they don't compile and/or don't work properly
#==================================================
SET( UNIT_SKIP
#    GasModel_Surreal1_btest
   )

IF( NOT USE_TETGEN )
  SET( UNIT_SKIP LinearizedIncompressiblePotential3D_Trefftz_btest ${UNIT_SKIP} )
ENDIF()

#==================================================

#==================================================
# Setup Surreal types
#==================================================
IF( NOT SURREAL )
  SET( SURREAL "SURREAL_LAZY" CACHE STRING "Surreal operator type. Options are SURREAL_TRAD, SURREAL_LAZY or SURREAL_REVERSE" FORCE )
ENDIF()

IF( SURREAL )
  STRING( TOUPPER ${SURREAL} SURREAL )
  ADD_DEFINITIONS( -D${SURREAL} )
ENDIF()

#==================================================
IF( UNIX AND NOT APPLE AND NOT CYGWIN )
  EXECUTE_PROCESS( COMMAND getconf LEVEL1_DCACHE_LINESIZE OUTPUT_VARIABLE CACHE_LINE_SIZE OUTPUT_STRIP_TRAILING_WHITESPACE )
  IF( CACHE_LINE_SIZE GREATER 0 )
    MESSAGE( STATUS "Cache Line Size CACHE_LINE_SIZE=${CACHE_LINE_SIZE}")
    ADD_DEFINITIONS( -DCACHE_LINE_SIZE=${CACHE_LINE_SIZE} )
  ELSE()
     MESSAGE( STATUS "Using default Cache Line Size" )
  ENDIF()
ENDIF()

#==================================================
# Use backtrace with line info if compiled with debug flags
#==================================================
IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  IF( BUILD_TYPE MATCHES "DEBUG" )
    ADD_DEFINITIONS( -DDEBUG_BACKTRACE )
  ENDIF()
ENDIF()

#==================================================
# External libraries
#==================================================

ADD_SUBDIRECTORY( external )

#==================================================
# Global list of all SANS libraries and dependency libraries
#==================================================

IF( USE_AFLR )
  SET( SANS_AFLRLIB AFLRLib )
ENDIF()

IF( USE_TETGEN )
  SET( SANS_TETGENLIB TetGenLib )
ENDIF()

IF( USE_AVRO )
  SET( SANS_AVROLIB avroLib )
ENDIF()

SET( SANS_LIBS
     BlockLib
     DGBR2ADLib
     DGBR2BurgersLib
     DGBR2PorusMediaLib
     DGBR2EulerAVLib
     DGBR2NSLib
     DGBR2RANSSALib
     DGBR2Lib
     DGAdvectiveLib
     HDGLib
     GalerkinLib
     ErrorEstimateLib
     AdvectionDiffusionLib
     BurgersLib
     CauchyRiemannLib
     HSMLib
     IBL_Lib
     NSLib
     StokesLib
     LorenzLib
     pdeSensorLib
     PorousMediaLib
     ShallowWaterLib
     PanelMethodLib
     ContinuationLib
     DistanceLib
     LIPSolverLib
     FullPotentialLib
     AnalyticFunctionLib
     pdeLib
     NonLinearSolverLib
#     OutputFunctionalLib
#     NDConvertLib
     AnalyticMetricLib
     AdaptationLib
     FeFloaLib
     libMeshbLib
     ugridLib
     EPICLib
     gmshLib
     ${SANS_AFLRLIB}
     refineLib
     ${SANS_AVROLIB}
     EGTessLib
     ${SANS_TETGENLIB}
     EGADSLib
     EmbeddingLib
     XField1DLib
     FieldLib
     BasisFunctionLib
     QuadratureLib
     BlockLinAlgLib
     SparseLinAlgLib
     DenseLinAlgLib
     PythonLib
     SurrealLib
     TopologyLib
     MPILib
     toolsLib
     ${LIBMESHB_LIBRARIES}
   )

IF( USE_GLPK )
  SET( SANS_LIBS LinOptLib ${SANS_LIBS} )
ENDIF()

#==================================================
# add the automatically determined parts of the RPATH
# which point to directories outside the build tree to the install RPATH
#==================================================
IF(APPLE)
  SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
  SET(CMAKE_INSTALL_NAME_DIR @rpath)
  #SET(CMAKE_BUILD_WITH_INSTALL_RPATH ON)
  # cmake --help-policy CMP0068
  IF (CMAKE_VERSION VERSION_GREATER 3.9 OR CMAKE_VERSION VERSION_EQUAL 3.9)
    SET(CMAKE_BUILD_WITH_INSTALL_NAME_DIR ON)
  ENDIF()
  GET_PROPERTY( CMAKE_BUILD_RPATH DIRECTORY ${CMAKE_SOURCE_DIR} PROPERTY LINK_DIRECTORIES  )
  LIST(APPEND CMAKE_BUILD_RPATH ${CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES} )
ENDIF(APPLE)

#==================================================
# Add sub directories
#==================================================
ADD_SUBDIRECTORY( src )
ADD_SUBDIRECTORY( test )


#==================================================
# A global Vera++ check for enforcing source code formatting
#==================================================
IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  IF (BUILD_TYPE MATCHES "COVERAGE")
    ADD_CUSTOM_TARGET( global_style )
    ADD_VERA_CHECKS_RECURSE( global_style src/*.h src/*.cpp test/*.h test/*.cpp )
  ENDIF()
ENDIF()

#Custom targets for debugging cmake
ADD_CUSTOM_TARGET( cmake_debug COMMAND ${CMAKE_COMMAND} -Wdev --warn-uninitialized WORKSPACE ${CMAKE_BINARY_DIR} )
ADD_CUSTOM_TARGET( cmake_debug_trace COMMAND ${CMAKE_COMMAND} -Wdev --warn-uninitialized --trace WORKSPACE ${CMAKE_BINARY_DIR} )


#==================================================
# Write summary to terminal
#==================================================

# write a summary of the build configuration
INCLUDE(CMakeInclude/MakeSummaryFile.cmake)

#Display the summary file
FILE(READ ${CMAKE_BINARY_DIR}/summary.log SANS_LOG_SUMMARY)
MESSAGE("${SANS_LOG_SUMMARY}")
