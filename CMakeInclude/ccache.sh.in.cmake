#!/bin/bash

# Executes ccache with a build specific cache directory

${GCC_COLORS}
export CCACHE_DIR=${CCACHE_CONFIGPATH}
${CCACHE} $@