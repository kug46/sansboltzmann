
IF( BIN_DIR_NAME MATCHES "DEPLOY" )
  SET( HEADERCHECK_INIT OFF )
ELSE()
  SET( HEADERCHECK_INIT ON )
ENDIF()

OPTION( USE_HEADERCHECK "Enable checks that all headers can be compiled indepdnently" ${HEADERCHECK_INIT} )

# A global target that can be used to only check if header files compile
ADD_CUSTOM_TARGET( HeaderCompileCheck )

IF( USE_HEADERCHECK )

FUNCTION( ADD_HEADER_COMPILE_CHECK CUSTOM_NAME )
  FILE_GLOB( HEADER_FILES ${ARGN} )
  ADD_HEADER_COMPILE_CHECK_IMPL( ${CUSTOM_NAME} ${HEADER_FILES} )
ENDFUNCTION()

FUNCTION( ADD_HEADER_COMPILE_CHECK_RECURSE CUSTOM_NAME )
  FILE_GLOB_RECURSE( HEADER_FILES ${ARGN} )  
  ADD_HEADER_COMPILE_CHECK_IMPL( ${CUSTOM_NAME} ${HEADER_FILES} )
ENDFUNCTION()

#This function should be executed in the root directory of the source
FUNCTION( ADD_HEADER_COMPILE_CHECK_IMPL CUSTOM_NAME )

  SET( HEADER_FILES )
  FOREACH( HEADER_FILE ${ARGN} )

    #Don't include any files that are part of documentation
    #Don't include implementation header files
    IF( NOT HEADER_FILE MATCHES "Documentation" AND
        NOT HEADER_FILE MATCHES "_impl.h" )
      LIST(APPEND HEADER_FILES ${HEADER_FILE} )
    ENDIF()
  ENDFOREACH()
  

  STRING( LENGTH "${CMAKE_SOURCE_DIR}" SOURCE_DIR_LEN )
  MATH( EXPR SOURCE_DIR_LEN "${SOURCE_DIR_LEN} + 1" )

  FOREACH( HEADER_FILE ${HEADER_FILES} )
    GET_FILENAME_COMPONENT( HEADER_NAME ${HEADER_FILE} NAME_WE )
    GET_FILENAME_COMPONENT( HEADER_EXT  ${HEADER_FILE} EXT     )
    GET_FILENAME_COMPONENT( HEADER_DIR  ${HEADER_FILE} PATH    )

    #Get the path in the source tree
    STRING( SUBSTRING "${HEADER_DIR}" ${SOURCE_DIR_LEN} -1 SRC_TREE_DIR )
    
    #Create the path to the dummy output file
    SET( HEADER_DUMMY_DIR ${CMAKE_BINARY_DIR}/${SRC_TREE_DIR} )
    SET( HEADER_DUMMY_FILE ${HEADER_DUMMY_DIR}/${HEADER_NAME}${HEADER_EXT}.cxx )

    #If a directory only contains headers, it may not exist in the binary
    #build tree. So it must be created.
    IF( NOT EXISTS ${HEADER_DUMMY_DIR} )
       FILE( MAKE_DIRECTORY ${HEADER_DUMMY_DIR} )
    ENDIF()

    IF( NOT EXISTS ${HEADER_DUMMY_FILE} )
      FILE(WRITE ${HEADER_DUMMY_FILE} "#define SANS_HEADERCOMPILE_CHECK\n#include <${HEADER_FILE}>\ntemplate<class T> struct foo { T get() { return 0; } };\ntemplate struct foo<int>;") 
    ENDIF()

    # Alway use debug flags when compiling the header check to reduce compile time
    IF (NOT USE_COTIRE)
      SET_SOURCE_FILES_PROPERTIES(${HEADER_DUMMY_FILE} PROPERTIES COMPILE_FLAGS "-O0 -g0" )
    ENDIF()

    LIST( APPEND HEADER_COMPILE_CHECK_FILES ${HEADER_DUMMY_FILE} )
  ENDFOREACH()
  
  #CMake 2.8.8 has OBJECT which results in less compiling, otherwise do all the compiling
  IF( ${CMAKE_MAJOR_VERSION} GREATER 1 AND ${CMAKE_MINOR_VERSION} GREATER 7 AND ${CMAKE_PATCH_VERSION} GREATER 7  )
    SET( LIBTYPE OBJECT )
  ENDIF()
  
  #This library simply checks that all header files can be compiled indepdendently
  ADD_LIBRARY( ${CUSTOM_NAME}_headercheck STATIC ${HEADER_COMPILE_CHECK_FILES} )
  ADD_DEPENDENCIES( ${CUSTOM_NAME} ${CUSTOM_NAME}_headercheck )
  ADD_DEPENDENCIES( HeaderCompileCheck ${CUSTOM_NAME}_headercheck )

  # Don't use cppcheck on header checks, just ccache
  SET_PROPERTY( TARGET ${CUSTOM_NAME}_headercheck PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_CMD}" )

  IF( DEFINED SANS_EXTERNAL_DEPENDS )
    ADD_DEPENDENCIES( ${CUSTOM_NAME}             ${SANS_EXTERNAL_DEPENDS} )
    ADD_DEPENDENCIES( ${CUSTOM_NAME}_headercheck ${SANS_EXTERNAL_DEPENDS} )
  ENDIF()

  # Reduce compile time of the header check, and use the same precompiled header for the actual target
  cotire( ${CUSTOM_NAME}_headercheck )
  IF( TARGET ${CUSTOM_NAME}_headercheck_pch AND DEFINED SANS_EXTERNAL_DEPENDS )
    ADD_DEPENDENCIES( ${CUSTOM_NAME}_headercheck_pch ${SANS_EXTERNAL_DEPENDS} )
  ENDIF()
  
  # Copy the PCH so it's used with the regular target as well
  COPYPCH( ${CUSTOM_NAME}_headercheck ${CUSTOM_NAME} )

ENDFUNCTION()

ELSE( USE_HEADERCHECK )

FUNCTION( ADD_HEADER_COMPILE_CHECK CUSTOM_NAME )
  
  IF( DEFINED SANS_EXTERNAL_DEPENDS )
    ADD_DEPENDENCIES( ${CUSTOM_NAME} ${SANS_EXTERNAL_DEPENDS} )
  ENDIF()

  # Reduce compile time of the target
  cotire( ${CUSTOM_NAME} )

  IF( TARGET ${CUSTOM_NAME}_pch AND DEFINED SANS_EXTERNAL_DEPENDS )
    ADD_DEPENDENCIES( ${CUSTOM_NAME}_pch ${SANS_EXTERNAL_DEPENDS} )
  ENDIF()
ENDFUNCTION()

FUNCTION( ADD_HEADER_COMPILE_CHECK_RECURSE CUSTOM_NAME )
  
  IF( DEFINED SANS_EXTERNAL_DEPENDS )
    ADD_DEPENDENCIES( ${CUSTOM_NAME} ${SANS_EXTERNAL_DEPENDS} )
  ENDIF()

  # Reduce compile time of the target
  cotire( ${CUSTOM_NAME} )

  IF( TARGET ${CUSTOM_NAME}_pch AND DEFINED SANS_EXTERNAL_DEPENDS )
    ADD_DEPENDENCIES( ${CUSTOM_NAME}_pch ${SANS_EXTERNAL_DEPENDS} )
  ENDIF()
ENDFUNCTION()

ENDIF( USE_HEADERCHECK )
