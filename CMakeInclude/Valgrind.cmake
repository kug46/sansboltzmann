INCLUDE( CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

#==================================================
# Setup valgrind commands
#==================================================
SET( MEMCHECK_FLAGS --tool=memcheck --leak-check=yes --show-leak-kinds=all --errors-for-leak-kinds=all --num-callers=50 --error-exitcode=1 --gen-suppressions=all )
LIST(APPEND MEMCHECK_FLAGS --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/OSX.supp )
LIST(APPEND MEMCHECK_FLAGS --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/Ubuntu.supp )
LIST(APPEND MEMCHECK_FLAGS --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/valgrind-python.supp )
LIST(APPEND MEMCHECK_FLAGS --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/SANS-python.supp )
LIST(APPEND MEMCHECK_FLAGS --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/OpenCASCADE.supp )
LIST(APPEND MEMCHECK_FLAGS --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/EGADS.supp )
LIST(APPEND MEMCHECK_FLAGS --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/LAPACKE.supp )
LIST(APPEND MEMCHECK_FLAGS --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/intel.supp )
LIST(APPEND MEMCHECK_FLAGS --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/OpenMPI.supp )

SET( VALGRIND_MEMCHECK_FLAGS CACHE STRING "Additonal valgrind memcheck flags: Useful ones are --track-origins=yes or --log-file=tmp/valgrind_memcheck.%p.log" )
SET( MEMCHECK_COMMAND ${CTEST_MEMORYCHECK_COMMAND} ${MEMCHECK_FLAGS} ${VALGRIND_MEMCHECK_FLAGS} )

IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE ) 
ENDIF()

IF( BUILD_TYPE AND BUILD_TYPE MATCHES "DEBUG" )
  SET( STACKCHECK_COMMAND ${CTEST_MEMORYCHECK_COMMAND} --tool=exp-sgcheck --gen-suppressions=all )
  SET( STACKCHECK_COMMAND ${STACKCHECK_COMMAND} --suppressions=${CMAKE_SOURCE_DIR}/scripts/valgrind/sgcheck.supp )
ELSE()
  SET( STACKCHECK_COMMAND ${CMAKE_COMMAND} -E echo "Please set CMAKE_BUILD_TYPE to \\'debug\\' for stack checking of" )
ENDIF()

SET( CALLGRIND_COMMAND ${CTEST_MEMORYCHECK_COMMAND} --tool=callgrind )
SET( VALGRIND_MASSIF_FLAGS CACHE STRING "Additonal valgrind massif flags" )
SET( MASSIF_COMMAND ${CTEST_MEMORYCHECK_COMMAND} --tool=massif ${VALGRIND_MASSIF_FLAGS} )


#===============================================================================
# GUI for CallGrind
#===============================================================================
IF( APPLE )
  SET( KCACHEGRINDEXEC qcachegrind )
ELSE()
  SET( KCACHEGRINDEXEC kcachegrind )
ENDIF()

FIND_PROGRAM( KCACHEGRIND ${KCACHEGRINDEXEC} )
IF( NOT KCACHEGRIND )
  MESSAGE( STATUS "Please install ${KCACHEGRINDEXEC} for graphical profiling." )
  SET( USE_KCACHEGRIND OFF )
ELSE()
  OPTION( USE_KCACHEGRIND "Use ${KCACHEGRINDEXEC} with valgrinds callgrind." ON )
ENDIF()

#===============================================================================
# GUI for massif
#===============================================================================
SET( MASSIFGUIEXEC massif-visualizer )

FIND_PROGRAM( VALGRIND_MASSIFGUI ${MASSIFGUIEXEC} )
IF( NOT VALGRIND_MASSIFGUI )
  MESSAGE( STATUS "Please install ${MASSIFGUIEXEC} for graphical heap memory usage." )
  SET( USE_MASSIFGUI OFF )
ELSE()
  OPTION( USE_MASSIFGUI "Use ${MASSIFGUIEXEC} with valgrinds massif." ON )
ENDIF()

#IF ( BIN_DIR_NAME MATCHES "DEPLOY" )
SET( VALGRIND_INIT OFF )
#ELSE()
#  SET( VALGRIND_INIT ON )
#ENDIF()

OPTION( VALGRIND_CALLGRIND "Enable make targets for valgrinds callgrind tool." OFF )
OPTION( VALGRIND_MEMCHECK "Enable make targets for valgrinds memcheck tool." ${VALGRIND_INIT} )
OPTION( VALGRIND_STACKCHECK "Enable make targets for valgrinds stackcheck tool." OFF )
OPTION( VALGRIND_MASSIF "Enable make targets for valgrinds massif heap memory useage tool." OFF )

#===============================================================================
# Function for adding valgrind to a make target
#===============================================================================
FUNCTION( ADD_VALGRIND_TARGETS UNIT_TEST WORKDIR )
  #Assumes that the target ${UNIT_TEST}_build exists

  IF( VALGRIND_MEMCHECK )
    #Add the memory checking target
    ADD_CUSTOM_TARGET( ${UNIT_TEST}_memcheck COMMAND ${MPI_COMMAND} ${MEMCHECK_COMMAND} $<TARGET_FILE:${UNIT_TEST}_build> $(UNITARGS)
                       WORKING_DIRECTORY ${WORKDIR} )
  ENDIF()
  
  IF( VALGRIND_STACKCHECK )
    #Add the stack checking target
    ADD_CUSTOM_TARGET( ${UNIT_TEST}_stackcheck COMMAND ${MPI_COMMAND} ${STACKCHECK_COMMAND} $<TARGET_FILE:${UNIT_TEST}_build> $(UNITARGS)
                       WORKING_DIRECTORY ${WORKDIR} )
  ENDIF()

  SET( CALLGRIND_FILE $<TARGET_FILE_DIR:${UNIT_TEST}_build>/callgrind.out )
  SET( CALLGRIND_MESSAGE "Callgrind file: ${CALLGRIND_FILE}" )

  IF( VALGRIND_CALLGRIND )
    IF( KCACHEGRIND AND USE_KCACHEGRIND )
      #Add the callgrind target with kcachegrind gui
      ADD_CUSTOM_TARGET( ${UNIT_TEST}_callgrind 
                         COMMAND ${CALLGRIND_COMMAND} --callgrind-out-file=${CALLGRIND_FILE} $<TARGET_FILE:${UNIT_TEST}_build> 
                         COMMAND ${KCACHEGRIND} ${CALLGRIND_FILE}
                         COMMAND ${CMAKE_COMMAND} -E echo
                         COMMAND ${CMAKE_COMMAND} -E echo ${CALLGRIND_MESSAGE}
                         COMMAND ${CMAKE_COMMAND} -E echo
                         WORKING_DIRECTORY ${WORKDIR} )
    ELSE()
      #Add the callgrind target
      ADD_CUSTOM_TARGET( ${UNIT_TEST}_callgrind 
                         COMMAND ${CALLGRIND_COMMAND} --callgrind-out-file=${CALLGRIND_FILE} $<TARGET_FILE:${UNIT_TEST}_build>
                         COMMAND ${CMAKE_COMMAND} -E echo
                         COMMAND ${CMAKE_COMMAND} -E echo ${CALLGRIND_MESSAGE} 
                         COMMAND ${CMAKE_COMMAND} -E echo
                         WORKING_DIRECTORY ${WORKDIR} )
    ENDIF()
  ENDIF()

  SET( MASSIF_FILE $<TARGET_FILE_DIR:${UNIT_TEST}_build>/massif.out )
  SET( MASSIF_MESSAGE "Massif file: ${MASSIF_FILE}" )

  IF( VALGRIND_MASSIF )
    IF( VALGRIND_MASSIFGUI AND USE_MASSIFGUI )
      #Add the massif target with MASSIF_GUI
      ADD_CUSTOM_TARGET( ${UNIT_TEST}_massif
                         COMMAND ${MASSIF_COMMAND} --massif-out-file=${MASSIF_FILE} $<TARGET_FILE:${UNIT_TEST}_build> 
                         COMMAND ${VALGRIND_MASSIFGUI} ${MASSIF_FILE}
                         COMMAND ${CMAKE_COMMAND} -E echo
                         COMMAND ${CMAKE_COMMAND} -E echo ${MASSIF_MESSAGE}
                         COMMAND ${CMAKE_COMMAND} -E echo
                         WORKING_DIRECTORY ${WORKDIR} )
    ELSE()
      #Add the massif target
      ADD_CUSTOM_TARGET( ${UNIT_TEST}_massif
                         COMMAND ${MASSIF_COMMAND} --massif-out-file=${MASSIF_FILE} $<TARGET_FILE:${UNIT_TEST}_build>
                         COMMAND ${CMAKE_COMMAND} -E echo
                         COMMAND ${CMAKE_COMMAND} -E echo ${MASSIF_MESSAGE} 
                         COMMAND ${CMAKE_COMMAND} -E echo
                         WORKING_DIRECTORY ${WORKDIR} )
    ENDIF()
  ENDIF()
ENDFUNCTION()

#===============================================================================
# Function for adding valgrind to a make target
#===============================================================================
MACRO( ADD_VALGRIND_CTEST UNIT_TEST WORKDIR LABEL_SUFFIX )
      
  IF( VALGRIND_MEMCHECK )
    #Add intividiaul memcheck tests to ctest
    ADD_TEST( NAME ${UNIT_TEST}_memcheck COMMAND ${MPI_COMMAND} ${MEMCHECK_COMMAND} $<TARGET_FILE:${UNIT_TEST}_build>
             WORKING_DIRECTORY ${WORKDIR} )
    SET_TESTS_PROPERTIES( ${UNIT_TEST}_memcheck PROPERTIES LABELS SANSMemCheck_${LABEL_SUFFIX} )
  ENDIF()
      
  IF( VALGRIND_STACKCHECK )
    #Add intividiaul stackcheck tests to ctest
    ADD_TEST( NAME ${UNIT_TEST}_stackcheck COMMAND ${MPI_COMMAND} ${STACKCHECK_COMMAND} $<TARGET_FILE:${UNIT_TEST}_build>
              WORKING_DIRECTORY ${WORKDIR} )
    SET_TESTS_PROPERTIES( ${UNIT_TEST}_stackcheck PROPERTIES LABELS SANSStackCheck_${LABEL_SUFFIX} )
  ENDIF()

ENDMACRO()
