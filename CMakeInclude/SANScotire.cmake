
#==================================================
# COmpile TIme REducer (cotire)
#==================================================
IF ( (CMAKE_VERSION VERSION_GREATER 2.8.12 OR CMAKE_VERSION VERSION_EQUAL 2.8.12) AND 
     ${CMAKE_CXX_COMPILER_ID} MATCHES "GNU|Clang" AND NOT CMAKE_GENERATOR MATCHES "Xcode" AND NOT APPLE AND NOT MINGW )

  OPTION( USE_COTIRE "Use COmpile TIme REducer" ON )
  IF ( USE_COTIRE )

    INCLUDE( CMakeInclude/cotire/cotire.cmake )

    # Don't generate unity targets. We don't use them and they simply bolat the makefile
    SET_PROPERTY(DIRECTORY PROPERTY COTIRE_ADD_UNITY_BUILD FALSE )

    # Prioritise python files. They like to be first in the include list
    SET_PROPERTY(DIRECTORY PROPERTY COTIRE_PREFIX_HEADER_INCLUDE_PRIORITY_PATH ${PYTHON_INCLUDE_DIRS} )
  
    # EGADS changes relatively often, and pre-compiling does not really reduce compile time
    SET( COTIER_IGNORE "${EGADS_INCLUDE_DIRS}" "${CMAKE_SOURCE_DIR}" "${CMAKE_BINARY_DIR}" )

    # Boost.Python really blows up the pre-compiled header without helping reduce compile time 
    LIST( APPEND COTIER_IGNORE  "${Boost_INCLUDE_DIR}/boost/python" )

    # The boost preprocessor should be processed just-in-time
    LIST( APPEND COTIER_IGNORE  "${Boost_INCLUDE_DIR}/boost/preprocessor" )

    # Boost does not want you to include these directly
    LIST( APPEND COTIER_IGNORE  "${Boost_INCLUDE_DIR}/boost/utility/detail" )

    # CBLAS is often lacking extern "C"
    LIST(APPEND COTIER_IGNORE "${BLAS_INCLUDE_DIRS}/cblas.h" )

    # This file is missing an include blocker
    LIST(APPEND COTIER_IGNORE "${Boost_INCLUDE_DIRS}/boost/mpl/aux_/preprocessed/gcc/vector.hpp" )

    IF (USE_MPI)
      # Don't propagate MPI to other files through pre-compiled headers
      LIST( APPEND COTIER_IGNORE  "${MPI_INCLUDE_DIRS}" "${Boost_INCLUDE_DIR}/boost/mpi" "${Boost_INCLUDE_DIR}/boost/serialization" "${Boost_INCLUDE_DIR}/boost/archive *metis.h" )
    ENDIF()
    
    IF (USE_LAPACK)
      # Lapacke needs some #defines before it included, so don't use it in the pre-compiled headers
      LIST(APPEND COTIER_IGNORE "${LAPACKE_INCLUDE_DIRS}/lapacke.h" "${BLAS_INCLUDE_DIRS}/lapacke.h" )
    ENDIF()

    IF (DEFINED AVRO_INCLUDE_TRIANGLE)
      # Triangle needs some #defines before it included, so don't use it in the pre-compiled headers
      LIST(APPEND COTIER_IGNORE "${AVRO_INCLUDE_TRIANGLE}/triangle.h")
    ENDIF()
  
    SET_PROPERTY(DIRECTORY PROPERTY COTIRE_PREFIX_HEADER_IGNORE_PATH ${COTIER_IGNORE} )
  
    MARK_AS_ADVANCED( COTIRE_ADDITIONAL_PREFIX_HEADER_IGNORE_EXTENSIONS )
    MARK_AS_ADVANCED( COTIRE_ADDITIONAL_PREFIX_HEADER_IGNORE_PATH )
    MARK_AS_ADVANCED( COTIRE_DEBUG )
    MARK_AS_ADVANCED( COTIRE_MAXIMUM_NUMBER_OF_UNITY_INCLUDES )
    MARK_AS_ADVANCED( COTIRE_MINIMUM_NUMBER_OF_TARGET_SOURCES )
    MARK_AS_ADVANCED( COTIRE_UNITY_SOURCE_EXCLUDE_EXTENSIONS )
    MARK_AS_ADVANCED( COTIRE_VERBOSE )
  ELSE()
    MACRO( cotire )
    ENDMACRO()
  ENDIF()
ELSE()
  #Precompiled headers do not seem to work at all for the intel compiler, so don't bother
  MACRO( cotire )
  ENDMACRO()
  SET( USE_COTIRE OFF )
ENDIF()
