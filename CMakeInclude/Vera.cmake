
FIND_PROGRAM( VERA vera++ )
MARK_AS_ADVANCED( VERA )

IF( VERA AND NOT BIN_DIR_NAME MATCHES "DEPLOY")
  SET( VERA_INIT ON )
ELSE()
  SET( VERA_INIT OFF )
ENDIF()

SET( USE_VERA ${VERA_INIT} CACHE BOOL "Check source files with Vera++")

IF( USE_VERA )

IF( NOT VERA )
  MESSAGE( FATAL_ERROR "Please install vera++ and add it to your path." )
ENDIF()

IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
ENDIF()

# Decide if vera should warn or error
# default to error for "debug" and "coverage", otherwise warn
SET( VERA_ERROR_INIT OFF )
IF( BUILD_TYPE AND (BUILD_TYPE MATCHES "DEBUG" OR BUILD_TYPE MATCHES "COVERAGE") )
  SET( VERA_ERROR_INIT ON )
ENDIF()
OPTION( VERA_ERROR "Vera++ formatting messages as errors" ${VERA_ERROR_INIT} )
IF( VERA_ERROR )
  SET( VERA_WARN_ERROR --error )
ELSE()
  SET( VERA_WARN_ERROR --warning )
ENDIF()

IF( NOT "${VERA_WARN_ERROR}" STREQUAL "${VERA_WARN_ERROR_STORE}" )
  # Remove all the old scripts if settings have changed
  FILE( REMOVE_RECURSE ${CMAKE_BINARY_DIR}/*VeraScript*.cmake )
ENDIF()

#This is a global target for checking the style of all files
ADD_CUSTOM_TARGET( vera )
#ADD_CUSTOM_TARGET( vera_force )

FUNCTION( ADD_VERA_CHECKS CUSTOM_NAME )
  FILE_GLOB( VERA_SRC_FILES ${ARGN} )
  ADD_VERA_CHECKS_IMPL( ${CUSTOM_NAME} ${VERA_SRC_FILES} )
ENDFUNCTION()

FUNCTION( ADD_VERA_CHECKS_RECURSE CUSTOM_NAME )
  FILE_GLOB_RECURSE( VERA_SRC_FILES ${ARGN} )
  ADD_VERA_CHECKS_IMPL( ${CUSTOM_NAME} ${VERA_SRC_FILES} )
ENDFUNCTION()


FUNCTION( ADD_VERA_CHECKS_IMPL CUSTOM_NAME )

  SET( VERA_SRC_FILES )
  FOREACH( VERA_SRC_FILE ${ARGN} )

    #Dont include any files that are part of documentation
    IF( NOT VERA_SRC_FILE MATCHES "Documentation" )
      LIST(APPEND VERA_SRC_FILES ${VERA_SRC_FILE} )
    ENDIF()
  ENDFOREACH()
  
  STRING( LENGTH "${CMAKE_SOURCE_DIR}" SOURCE_DIR_LEN )
  MATH( EXPR SOURCE_DIR_LEN "${SOURCE_DIR_LEN} + 1" )

  IF( MSVC )
    SET( style vc )
  ELSE()
    SET( style std )
  ENDIF()


  FOREACH( VERA_SRC_FILE ${VERA_SRC_FILES} )
    GET_FILENAME_COMPONENT( SRC_NAME ${VERA_SRC_FILE} NAME_WE )
    GET_FILENAME_COMPONENT( SRC_EXT  ${VERA_SRC_FILE} EXT     )
    GET_FILENAME_COMPONENT( SRC_DIR  ${VERA_SRC_FILE} PATH    )

    #Get the path in the source tree
    STRING( SUBSTRING "${SRC_DIR}" ${SOURCE_DIR_LEN} -1 SRC_TREE_DIR )

    #Create the path to the dummy output file
    SET( VERA_DUMMY_DIR ${CMAKE_BINARY_DIR}/${SRC_TREE_DIR} )
    SET( VERA_DUMMY_FILE ${SRC_NAME}${SRC_EXT}.vera )
    SET( VERA_DUMMY_OUTPUT ${VERA_DUMMY_DIR}/${VERA_DUMMY_FILE} )
    SET( VERA_DUMMY_SCRIPT ${VERA_DUMMY_DIR}/VeraScript.${VERA_DUMMY_FILE}.cmake )

    #If a directory only contains headers, it may not exist in the binary
    #build tree. So it must be created.
    IF( NOT EXISTS ${VERA_DUMMY_DIR} )
       FILE( MAKE_DIRECTORY ${VERA_DUMMY_DIR} )
    ENDIF()

    SET( VERA_COMMENT "Checking style of ${SRC_TREE_DIR}/${SRC_NAME}${SRC_EXT}" )

    #This makes vera++ act similar to a compiler, checking any file that gets modified
    ADD_CUSTOM_COMMAND( OUTPUT ${VERA_DUMMY_OUTPUT}
                        COMMAND ${CMAKE_COMMAND} ARGS -P ${VERA_DUMMY_SCRIPT}
                        DEPENDS ${VERA_SRC_FILE}
                        COMMENT ${VERA_COMMENT}
                      )
    
    # Only create new vera scripts if they don't already exists
    # This improves the speed of re-configuring cmake
    IF( NOT EXISTS ${VERA_DUMMY_SCRIPT} )
      CONFIGURE_FILE( ${CMAKE_SOURCE_DIR}/CMakeInclude/VeraScript.in.cmake ${VERA_DUMMY_SCRIPT} @ONLY )
    ENDIF()
    
    #This is for make style, that will always run the style checker on all files
    #SET( VERA_NOFILE_OUTPUT ${VERA_DUMMY_OUTPUT}.never.will.exist )
    #ADD_CUSTOM_COMMAND( OUTPUT ${VERA_NOFILE_OUTPUT}
    #                    COMMAND ${VERA}
    #                    ARGS ${VERA_SRC_FILE}
    #                         --root=${CMAKE_SOURCE_DIR}/vera/
    #                         --show-rule
    #                         --${style}-report=- #This prins to stdout
    #                         --profile default
    #                         ${VERA_WARN_ERROR}
    #                         --exclusions ${CMAKE_SOURCE_DIR}/scripts/vera/exclusions.txt
    #                    DEPENDS ${VERA_SRC_FILE}
    #                    COMMENT ${VERA_COMMENT}
    #                  )
    LIST( APPEND VERA_DUMMY_FILES ${VERA_DUMMY_OUTPUT} )
    #LIST( APPEND VERA_NOFILE_FILES ${VERA_NOFILE_OUTPUT} )
  ENDFOREACH()

  #This will only run vera if the files are modified
  ADD_CUSTOM_TARGET( ${CUSTOM_NAME}_vera DEPENDS ${VERA_DUMMY_FILES} )

  #This will run vera regardless if the files are modified
  #ADD_CUSTOM_TARGET( ${CUSTOM_NAME}_vera_force DEPENDS ${VERA_NOFILE_FILES} )

  #Make the custom name depdnent on the style checks
  ADD_DEPENDENCIES( ${CUSTOM_NAME} ${CUSTOM_NAME}_vera )

  #Make these vera check dependent on the global check
  ADD_DEPENDENCIES( vera ${CUSTOM_NAME}_vera )
  #ADD_DEPENDENCIES( vera_force ${CUSTOM_NAME}_vera_force )

ENDFUNCTION()

SET( VERA_WARN_ERROR_STORE ${VERA_WARN_ERROR} CACHE INTERNAL "Store the warn/error to indicate if vera script files need to be replaced" FORCE )

ELSE()

#Dummy functions so that CMake does not complain
FUNCTION( ADD_VERA_CHECKS CUSTOM_NAME )
ENDFUNCTION()

FUNCTION( ADD_VERA_CHECKS_RECURSE CUSTOM_NAME )
ENDFUNCTION()

ENDIF()
