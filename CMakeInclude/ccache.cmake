
# Don't use ccache with Xcode
IF( NOT CMAKE_GENERATOR MATCHES "Xcode" )

FIND_PROGRAM(CCACHE ccache)
MARK_AS_ADVANCED( CCACHE )

IF( CCACHE )
  SET( CCACHE_ON ON )
ELSE()
  SET( CCACHE_ON OFF )
ENDIF()

OPTION( USE_CCACHE "Use ccache to increase compile speed" ${CCACHE_ON} )

IF( USE_CCACHE )
  
  IF( NOT (CMAKE_COMPILER_IS_GNUCXX AND ${CMAKE_CXX_COMPILER_VERSION} VERSION_GREATER 4.8.9) )
    # ccache adds -fdiagnostics-color if the GCC_COLORS is set, but that option is only available with gcc 4.9 or newer
    # This simply removes the environment variable in the ccache script
    SET(GCC_COLORS "unset GCC_COLORS" )
  ENDIF()
 
  SET(CCACHE_MAX_SIZE "5G" CACHE STRING "Maximum size of files stored cache. Number with optional suffix: k, M, G, T (decimal), Ki, Mi, Gi or Ti (binary). The default suffix is G. Use 0 for no limit.")

  SET(CCACHE_CONFIGPATH ${CMAKE_BINARY_DIR}/ccache CACHE STRING "Directory for ccache database and configurations")
  MARK_AS_ADVANCED( CCACHE_CONFIGPATH )
  
  #Create the config dir if it does not exist
  IF( NOT EXISTS ${CCACHE_CONFIGPATH} )
   FILE( MAKE_DIRECTORY ${CCACHE_CONFIGPATH} )
  ENDIF()
  
  GET_PROPERTY(OLD_RULE_LAUNCH_COMPILE GLOBAL PROPERTY RULE_LAUNCH_COMPILE )
  
  # Use the custon bash script to run ccache
  SET( CCACHE_CMD "${CCACHE_CONFIGPATH}/ccache.sh")

  CONFIGURE_FILE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ccache.sh.in.cmake ${CCACHE_CONFIGPATH}/ccache.sh )
  CONFIGURE_FILE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ccache.conf.in.cmake ${CCACHE_CONFIGPATH}/ccache.conf )
  
  SET_PROPERTY(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${OLD_RULE_LAUNCH_COMPILE} ${CCACHE_CMD}")
  #SET_PROPERTY(GLOBAL PROPERTY RULE_LAUNCH_LINK "${CCACHE_CMD}")
  
ENDIF()

ENDIF( NOT CMAKE_GENERATOR MATCHES "Xcode" )
