
# include for CMAKE_INSTALL_LIBDIR
INCLUDE(GNUInstallDirs)

IF( APPLE )

    #OSX provides the lapack library in the Accelerate framewwork
    FIND_LIBRARY(LAPACK_LIBRARY NAMES Accelerate )
    ADD_DEFINITIONS( -DLAPACK_ACCELERATE )

ELSEIF(NOT DEFINED SANS_LAPACK_DIR)
    #First find lapack 
    PKG_CHECK_MODULES(LAPACK QUIET liblapack)
    
    IF( LAPACK_FOUND )
        SET(LAPACK_DEFINITIONS ${LAPACK_CFLAGS_OTHER})
    ELSE()
        FIND_LIBRARY(LAPACK_LIBRARY NAMES lapack
                     HINTS $ENV{LAPACK_DIR}/lib 
                           $ENV{LAPACK_DIR}/${CMAKE_INSTALL_LIBDIR})
   ENDIF()

ELSE()
    #First find lapack
    FIND_LIBRARY(LAPACK_LIBRARY NAMES lapack
                 HINTS ${SANS_LAPACK_DIR}
                 PATH_SUFFIXES lib NO_DEFAULT_PATH)
ENDIF()

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(LAPACK  DEFAULT_MSG
                                  LAPACK_LIBRARY)

SET( LAPACK_LIBRARIES ${LAPACK_LIBRARY} )

IF( LAPACK_FOUND )

  GET_FILENAME_COMPONENT( LAPACK_LIBRARY_PATH ${LAPACK_LIBRARY} PATH )

  SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
                LINK_DIRECTORIES ${LAPACK_LIBRARY_PATH} )
                
ELSEIF( USE_LAPACK )

  MESSAGE(STATUS "" )

  IF( DEFINED ENV{LAPACK_DIR} )
    MESSAGE(STATUS "  LAPACK_DIR environment variable is set to: $ENV{LAPACK_DIR}" )
    IF( NOT LAPACK_LIBRARY )
      MESSAGE(STATUS "    could not find 'lapack' library in $LAPACK_DIR/lib or $LAPACK_DIR/${CMAKE_INSTALL_LIBDIR}")
    ENDIF()
  ELSE()
    MESSAGE(STATUS "  LAPACK_DIR environment variable is not set" )
  ENDIF()
  
  IF( DEFINED SANS_LAPACK_DIR )
    MESSAGE(STATUS "  SANS_LAPACK_DIR is set to: ${SANS_LAPACK_DIR}" )
    IF( NOT LAPACK_LIBRARY )
      MESSAGE(STATUS "    could not find 'lapack' library in $SANS_LAPACK_DIR/lib or $SANS_LAPACK_DIR/${CMAKE_INSTALL_LIBDIR}")
    ENDIF()
  ENDIF()
  MESSAGE(STATUS "" )
  MESSAGE(STATUS "  SANS will attempt to download and compile lapack for you." )
  MESSAGE(STATUS "" )

ENDIF()
