 #
 # Find the Portable, Extensible Toolkit for Scientific Computation includes and libraries
 #
 # PETSc can be found at
 #     https://www.mcs.anl.gov/petsc/
 #

IF(NOT DEFINED SANS_PETSC_DIR)
    #Find PETSC 
    PKG_CHECK_MODULES(PETSC QUIET PETSc)
    
    IF( PETSC_FOUND )
        SET(PETSC_DEFINITIONS ${PETSC_CFLAGS_OTHER})
    ELSE()
        FIND_LIBRARY(PETSC_LIBRARIES NAMES petsc
                 HINTS $ENV{PETSC_DIR}/lib )
        FIND_PATH(PETSC_INCLUDE_DIR
                  NAMES "petsc.h"
                  PATHS $ENV{PETSC_DIR}/include
                  PATH_SUFFIXES "include" "petsc" )

        SET(PETSC_INCLUDE_DIRS ${PETSC_INCLUDE_DIR} )

        IF( NOT USE_MPI )
          FIND_PATH(PETSC_MPIUNI_INCLUDE_DIR
                    NAMES "mpi.h"
                    PATHS $ENV{PETSC_DIR}/include/petsc/mpiuni
                    PATH_SUFFIXES "include" "petsc" NO_SYSTEM_ENVIRONMENT_PATH NO_DEFAULT_PATH )
          LIST( APPEND PETSC_INCLUDE_DIRS ${PETSC_MPIUNI_INCLUDE_DIR} )
          MARK_AS_ADVANCED( PETSC_MPIUNI_INCLUDE_DIR )
        ENDIF()

   ENDIF()

ELSE()
    #Find PETSC
    FIND_LIBRARY(PETSC_LIBRARIES NAMES petsc
                 HINTS ${SANS_PETSC_DIR}
                 PATH_SUFFIXES lib NO_DEFAULT_PATH)
ENDIF()

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(PETSC  DEFAULT_MSG
                                  PETSC_LIBRARIES PETSC_INCLUDE_DIRS)

MARK_AS_ADVANCED(PETSC_INCLUDE_DIRS PETSC_LIBRARIES )
