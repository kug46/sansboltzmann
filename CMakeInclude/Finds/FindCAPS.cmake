
IF( USE_INTERNAL_EGADS )

  SET( CAPS_AIMUTIL_LIBRARY ${ESP_ROOT}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}aimUtil${CMAKE_STATIC_LIBRARY_SUFFIX} )
  SET( CAPS_OPENCSM_LIBRARY ${ESP_ROOT}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}ocsm${CMAKE_SHARED_LIBRARY_SUFFIX} )

  IF( APPLE )
    SET( CAPS_UDUNITS2_LIBRARY ${ESP_ROOT}/src/CAPS/udunits/${CMAKE_SHARED_LIBRARY_PREFIX}udunits2.0${CMAKE_SHARED_LIBRARY_SUFFIX} )
  ELSE()
    SET( CAPS_UDUNITS2_LIBRARY ${ESP_ROOT}/src/CAPS/udunits/${CMAKE_SHARED_LIBRARY_PREFIX}udunits2${CMAKE_STATIC_LIBRARY_SUFFIX} )
  ENDIF()
  
  SET( CAPS_LIBRARIES ${CAPS_AIMUTIL_LIBRARY} ${CAPS_OPENCSM_LIBRARY} ${CAPS_UDUNITS2_LIBRARY} )
  SET( CAPS_INCLUDE_DIRS ${ESP_ROOT}/include )
  SET( CAPS_LIBRARY_PATH ${ESP_ROOT}/lib )

ELSE()

  SET( CAPS_LIB_HINTS $ENV{ESP_ROOT}/lib $ENV{ESP_DIR}/lib ${SANS_ESP_DIR}/lib 
                      ${CMAKE_SOURCE_DIR}/external/ESP/lib )

  #Find CAPS aimUtil which is part of Engineering Sketch Pad
  FIND_LIBRARY( CAPS_AIMUTIL_LIBRARY aimUtil HINTS ${CAPS_LIB_HINTS} REQUIRED ) 

  IF( NOT CAPS_AIMUTIL_LIBRARY )
    MESSAGE("")
    MESSAGE( "====================================================================" )
    MESSAGE("ERROR: Cannot find CAPS aimUtil library.")
    MESSAGE("       Plese install the Engineering Sketch Pad from acdl.mit.edu/ESP" )
    MESSAGE("       and set the envoronment vaiable ESP_DIR or the cmake variable"  )
    MESSAGE("       SANS_ESP_DIR to the EngSketchPad directory." )
    MESSAGE( "====================================================================" )
    MESSAGE("")
    MESSAGE(FATAL_ERROR "")
  ENDIF()

  FIND_LIBRARY( CAPS_OPENCSM_LIBRARY ocsm HINTS ${CAPS_LIB_HINTS} REQUIRED ) 

  IF( NOT CAPS_OPENCSM_LIBRARY )
    MESSAGE("")
    MESSAGE( "====================================================================" )
    MESSAGE("ERROR: Cannot find OpenCSM library.")
    MESSAGE("       Plese install the Engineering Sketch Pad from acdl.mit.edu/ESP" )
    MESSAGE("       and set the envoronment vaiable ESP_DIR or the cmake variable"  )
    MESSAGE("       SANS_ESP_DIR to the EngSketchPad directory." )
    MESSAGE( "====================================================================" )
    MESSAGE("")
    MESSAGE(FATAL_ERROR "")
  ENDIF()

  FIND_LIBRARY( CAPS_UDUNITS2_LIBRARY udunits2 HINTS ${CAPS_LIB_HINTS} REQUIRED ) 

  IF( NOT CAPS_UDUNITS2_LIBRARY )
    MESSAGE("")
    MESSAGE( "====================================================================" )
    MESSAGE("ERROR: Cannot find Udunits 2 library: udunits2.")
    MESSAGE( "====================================================================" )
    MESSAGE("")
    MESSAGE(FATAL_ERROR "")
  ENDIF()

  FIND_PATH( CAPS_INCLUDE_DIR aimUtil.h HINTS $ENV{ESP_ROOT}/include $ENV{ESP_DIR}/include ${SANS_ESP_DIR}/include
                                              ${CMAKE_SOURCE_DIR}/external/ESP/include REQUIRED ) 

  GET_FILENAME_COMPONENT( CAPS_LIBRARY_PATH ${CAPS_AIMUTIL_LIBRARY} PATH )

  INCLUDE(FindPackageHandleStandardArgs)

  FIND_PACKAGE_HANDLE_STANDARD_ARGS(CAPS  DEFAULT_MSG
                                    CAPS_AIMUTIL_LIBRARY 
                                    CAPS_OPENCSM_LIBRARY 
                                    CAPS_UDUNITS2_LIBRARY 
                                    CAPS_INCLUDE_DIR )


  SET( CAPS_LIBRARIES ${CAPS_AIMUTIL_LIBRARY} ${CAPS_OPENCSM_LIBRARY} ${CAPS_UDUNITS2_LIBRARY} )
  SET( CAPS_INCLUDE_DIRS ${CAPS_INCLUDE_DIR} )

  SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
                LINK_DIRECTORIES ${CAPS_LIBRARY_PATH} )

  SET( CAPS_FOUND ON )
ENDIF()
