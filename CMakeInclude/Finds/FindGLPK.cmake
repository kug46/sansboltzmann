 #
 # Find the GNU Linear Programming Kit includes and libraries
 #
 # GLPK can be found at
 #     https://www.gnu.org/software/glpk/
 #

IF(NOT DEFINED SANS_GLPK_DIR)
    PKG_CHECK_MODULES(GLPK QUIET libglpk)
    
    IF ( GLPK_FOUND )
        SET(GLPK_DEFINITIONS ${GLPK_CFLAGS_OTHER})
    ELSE()
        FIND_PATH(GLPK_INCLUDE_DIRS glpk.h
              HINTS $ENV{GLPK_DIR}/include
              PATH_SUFFIXES glpk )
    
        FIND_LIBRARY(GLPK_LIBRARIES NAMES glpk
                 HINTS $ENV{GLPK_DIR}/lib )
    ENDIF()
ELSE()
    FIND_PATH(GLPK_INCLUDE_DIRS glpk.h
              HINTS ${SANS_GLPK_DIR}/include
              PATH_SUFFIXES glpk NO_DEFAULT_PATH)

    FIND_LIBRARY(GLPK_LIBRARIES NAMES glpk glpk_cxx
                 HINTS ${SANS_GLPK_DIR}/lib NO_DEFAULT_PATH)
ENDIF()

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(GLPK  DEFAULT_MSG
                                  GLPK_LIBRARIES GLPK_INCLUDE_DIRS)

MARK_AS_ADVANCED(GLPK_INCLUDE_DIRS GLPK_LIBRARIES)

