#
# http://www.cs.sandia.gov/Zoltan/Zoltan_download.html
#

# Find the zoltan library
FIND_PATH( ZOLTAN_INCLUDE_DIRS zoltan.h HINTS $ENV{ZOLTAN_DIR}/include ${SANS_ZOLTAN_DIR}/include ) 

FIND_LIBRARY( ZOLTAN_LIBRARIES zoltan HINTS $ENV{ZOLTAN_DIR}/lib ${SANS_ZOLTAN_DIR}/lib )

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(ZOLTAN  DEFAULT_MSG
                                  ZOLTAN_LIBRARIES ZOLTAN_INCLUDE_DIRS)

IF( ZOLTAN_FOUND )
  GET_FILENAME_COMPONENT( ZOLTAN_LIBRARY_PATH ${ZOLTAN_LIBRARIES} PATH )

  SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
                LINK_DIRECTORIES ${ZOLTAN_LIBRARY_PATH} )
                
ELSEIF( USE_ZOLTAN )

  MESSAGE(STATUS "" )

  IF( DEFINED ENV{ZOLTAN_DIR} )
    MESSAGE(STATUS "  ZOLTAN_DIR environment variable is set to: $ENV{ZOLTAN_DIR}" )
    IF( NOT ZOLTAN_LIBRARIES )
      MESSAGE(STATUS "    could not find 'zoltan' library in $ZOLTAN_DIR/lib")
    ENDIF()
    IF( NOT ZOLTAN_LIBRARIES )
      MESSAGE(STATUS "    could not find 'zoltan.h' in $ZOLTAN_DIR/include")
    ENDIF()
  ELSE()
    MESSAGE(STATUS "  ZOLTAN_DIR environment variable is not set" )
  ENDIF()
  
  IF( DEFINED SANS_ZOLTAN_DIR )
    MESSAGE(STATUS "  SANS_ZOLTAN_DIR is set to: ${SANS_ZOLTAN_DIR}" )
    IF( NOT ZOLTAN_LIBRARIES )
      MESSAGE(STATUS "    could not find 'zoltan' library in $SANS_ZOLTAN_DIR/lib")
    ENDIF()
    IF( NOT ZOLTAN_LIBRARIES )
      MESSAGE(STATUS "    could not find 'zoltan.h' in $SANS_ZOLTAN_DIR/include")
    ENDIF()
  ENDIF()
  MESSAGE(STATUS "" )
  MESSAGE(STATUS "  SANS will attempt to download and compile ZOLTAN for you." )
  MESSAGE(STATUS "" )

ENDIF()
