
#Find Engineering Sketch Pad
FIND_LIBRARY( EGADS_LIBRARY  egads   HINTS $ENV{ESP_ROOT}/lib     $ENV{ESP_DIR}/lib     ${SANS_ESP_DIR}/lib     ${CMAKE_SOURCE_DIR}/external/ESP/lib     REQUIRED )
FIND_PATH( EGADS_INCLUDE_DIR egads.h HINTS $ENV{ESP_ROOT}/include $ENV{ESP_DIR}/include ${SANS_ESP_DIR}/include ${CMAKE_SOURCE_DIR}/external/ESP/include REQUIRED ) 

INCLUDE(FindPackageHandleStandardArgs)
 
IF( NOT EGADS_LIBRARY OR NOT EGADS_INCLUDE_DIR )
  MESSAGE(STATUS "")
  MESSAGE(STATUS "======================================================================" )
  MESSAGE(STATUS "WARNING: Cannot find EGADS library or include directory.")
  MESSAGE(STATUS "         SANS attempt to download and compile Engineering Sketch Pad (ESP) for you." )
  MESSAGE(STATUS "         You may install ESP from acdl.mit.edu/ESP" )
  MESSAGE(STATUS "         and set the envoronment vaiable \$ESP_DIR or the cmake variable" )
  MESSAGE(STATUS "         SANS_ESP_DIR to the EngSketchPad directory." )
  MESSAGE(STATUS "======================================================================" )
  MESSAGE(STATUS "")
#  MESSAGE(FATAL_ERROR "")

ELSE()

  GET_FILENAME_COMPONENT( ESP_LIBRARY_PATH ${EGADS_LIBRARY} PATH )

  SET( EGADS_LIBRARIES ${EGADS_LIBRARY} )
  SET( EGADS_INCLUDE_DIRS ${EGADS_INCLUDE_DIR} )

  SET( ESP_LIBRARY_PATH ${ESP_LIBRARY_PATH} CACHE PATH "ESP library directory." )

  SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
                LINK_DIRECTORIES ${ESP_LIBRARY_PATH} )

  SET(EGADS_FOUND ON)

ENDIF()

#Find Engineering Sketch Pad version of OpenCASCADE
IF(MINGW)
  FIND_LIBRARY( CAS_LIBRARY TKernel HINTS $ENV{CASROOT}/win64/vc14/lib $ENV{CAS_DIR}/win64/vc14/lib ${SANS_CAS_DIR}/win64/vc14/lib
                ${CMAKE_SOURCE_DIR}/external/OpenCASCADE-7.3.1/win64/vc11/lib REQUIRED ) 
ELSE()
  FIND_LIBRARY( CAS_LIBRARY TKernel HINTS $ENV{CASROOT}/lib $ENV{CAS_DIR}/lib ${SANS_CAS_DIR}/lib 
                ${CMAKE_SOURCE_DIR}/external/OpenCASCADE-7.3.1/lib REQUIRED ) 
ENDIF()

IF( NOT CAS_LIBRARY )
  MESSAGE(STATUS "")
  MESSAGE(STATUS "======================================================================" )
  MESSAGE(STATUS "WARNING: Cannot find OpenCASCADE library TKernel.")
  MESSAGE(STATUS "         SANS will download OpenCASCADE for you." )
  MESSAGE(STATUS "         You may download OpenCASCDE from acdl.mit.edu/ESP" )
  MESSAGE(STATUS "         and set envoronment vaiable CAS_DIR or the cmake variable" )
  MESSAGE(STATUS "         SANS_CAS_DIR to the OpenCASCADE directory containing lib." )
  MESSAGE(STATUS "======================================================================" )
  MESSAGE(STATUS "")
#  MESSAGE(FATAL_ERROR "")
  SET( FOUND_CAS FALSE )

ELSE()

  SET( CAS_LIBRARIES ${CAS_LIBRARY} )
  SET( FOUND_CAS TRUE )

  GET_FILENAME_COMPONENT( OCAS_LIBRARY_PATH ${CAS_LIBRARY} PATH )

  SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
                LINK_DIRECTORIES ${OCAS_LIBRARY_PATH} )

ENDIF()

FIND_PACKAGE_HANDLE_STANDARD_ARGS(EGADS  DEFAULT_MSG
                                  EGADS_LIBRARY EGADS_INCLUDE_DIR CAS_LIBRARY) 
