
# include for CMAKE_INSTALL_LIBDIR
INCLUDE(GNUInstallDirs)

IF( APPLE )

    #OSX provides the lapack library in the Accelerate framewwork
    MESSAGE(FATAL_ERROR "Developer Error! LAPACKE is not supported on OSX")

ELSEIF(NOT DEFINED SANS_LAPACK_DIR)

   #find the lacpacke C interface
   PKG_CHECK_MODULES(LAPACKE QUIET liblapacke)
   
    IF( LAPACKE_FOUND )
        SET(LAPACKE_DEFINITIONS ${LAPACKE_CFLAGS_OTHER})
    ELSE()
        FIND_PATH(LAPACKE_INCLUDE_DIRS NAMES lapacke.h
                  HINTS $ENV{LAPACK_DIR}/include
                  PATH_SUFFIXES lapack )
    
        FIND_LIBRARY(LAPACKE_LIBRARY NAMES lapacke
                     HINTS $ENV{LAPACK_DIR}/lib 
                           $ENV{LAPACK_DIR}/${CMAKE_INSTALL_LIBDIR} )
    ENDIF()

ELSE()

    #Find the lacpacke C interface
    FIND_PATH(LAPACKE_INCLUDE_DIRS NAMES lapacke.h
              HINTS ${SANS_LAPACK_DIR}
              PATH_SUFFIXES include NO_DEFAULT_PATH)
              
    FIND_LIBRARY(LAPACKE_LIBRARY NAMES lapacke
                 HINTS ${SANS_LAPACK_DIR}
                 PATH_SUFFIXES lib NO_DEFAULT_PATH)
ENDIF()


INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(LAPACKE  DEFAULT_MSG
                                  LAPACKE_LIBRARY LAPACKE_INCLUDE_DIRS)

IF( LAPACKE_FOUND )

  SET( LAPACKE_LIBRARIES ${LAPACKE_LIBRARY} )
  
  GET_FILENAME_COMPONENT( LAPACKE_LIBRARY_PATH ${LAPACKE_LIBRARY} PATH )

  SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
                LINK_DIRECTORIES ${LAPACKE_LIBRARY_PATH} )
                
ELSEIF( USE_LAPACKE )

  MESSAGE(STATUS "" )

  IF( DEFINED ENV{LAPACK_DIR} )
    MESSAGE(STATUS "  LAPACK_DIR environment variable is set to: $ENV{LAPACK_DIR}" )
    IF( NOT LAPACKE_LIBRARY )
      MESSAGE(STATUS "    could not find 'lapacke' library in $LAPACK_DIR/lib or $LAPACK_DIR/${CMAKE_INSTALL_LIBDIR}")
    ENDIF()
    IF( NOT LAPACKE_INCLUDE_DIRS )
      MESSAGE(STATUS "    could not find 'lapacke.h' in $LAPACK_DIR/include")
    ENDIF()
  ELSE()
    MESSAGE(STATUS "  LAPACK_DIR environment variable is not set" )
  ENDIF()
  
  IF( DEFINED SANS_LAPACK_DIR )
    MESSAGE(STATUS "  SANS_LAPACK_DIR is set to: ${SANS_LAPACK_DIR}" )
    IF( NOT LAPACKE_LIBRARY )
      MESSAGE(STATUS "    could not find 'lapacke' library in $SANS_LAPACK_DIR/lib or $SANS_LAPACK_DIR/${CMAKE_INSTALL_LIBDIR}")
    ENDIF()
    IF( NOT LAPACKE_INCLUDE_DIRS )
      MESSAGE(STATUS "    could not find 'lapacke.h' in $SANS_LAPACK_DIR/include")
    ENDIF()
  ENDIF()
  MESSAGE(STATUS "" )
  MESSAGE(STATUS "  SANS will attempt to download and compile lapacke for you." )
  MESSAGE(STATUS "" )

ENDIF()
