
OPTION( MKL_MULTI_THREADED "Use multi-threaded version of MKL" ON )
IF(MKL_MULTI_THREADED)
    SET(MKL_THREADING_LIBNAME mkl_intel_thread)
ELSE()
    SET(MKL_THREADING_LIBNAME mkl_sequential)
ENDIF()

# Search paths for MKL libraries
SET( MKL_PATHS $ENV{MKLROOT}/lib/intel64 $ENV{MKLROOT}/lib $ENV{MKLROOT}/../compiler/lib/intel64/ $ENV{MKLROOT}/../compiler/lib )

# Dynamic linker
FIND_LIBRARY(MKL_DL_LIBRARY dl)

# Find include dir
FIND_PATH(MKL_INCLUDE_DIRS mkl.h PATH $ENV{MKLROOT}/include)

# MKL is composed by four layers: Interface, Threading, Computational and RTL

#Interface layer
FIND_LIBRARY(MKL_INTERFACE_LIBRARY mkl_intel_lp64 PATH ${MKL_PATHS} )

#Threading layer
FIND_LIBRARY(MKL_THREADING_LIBRARY ${MKL_THREADING_LIBNAME} PATH ${MKL_PATHS})

# Computational layer
FIND_LIBRARY(MKL_CORE_LIBRARY mkl_core PATH ${MKL_PATHS})

# RTL layer
FIND_LIBRARY(MKL_RTL_LIBRARY iomp5 PATH ${MKL_PATHS})

# Parallel mkl_pardiso isn't quite working yet...
#FIND_LIBRARY(MKL_BLACS_LIBRARY libmkl_blacs_openmpi_lp64.a PATH ${MKL_PATHS})
IF( MKL_BLACS_LIBRARY )
  SET( MKL_BLACS_LIBRARY "-Wl,--whole-archive ${MKL_BLACS_LIBRARY} -Wl,--no-whole-archive" )
ELSE()
  UNSET( MKL_BLACS_LIBRARY )
ENDIF()

SET(MKL_LIBRARIES ${MKL_BLACS_LIBRARY} 
                  ${MKL_INTERFACE_LIBRARY} 
                  ${MKL_THREADING_LIBRARY} 
                  ${MKL_CORE_LIBRARY} 
                  ${MKL_RTL_LIBRARY}
                  ${MKL_DL_LIBRARY} )

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(MKL  DEFAULT_MSG
                                  MKL_INTERFACE_LIBRARY
                                  MKL_THREADING_LIBRARY
                                  MKL_CORE_LIBRARY
                                  MKL_RTL_LIBRARY
                                  MKL_INCLUDE_DIRS)

MARK_AS_ADVANCED(MKL_INCLUDE_DIRS MKL_LIBRARIES)


# Find static versions of the libaries

IF (UNIX)
  SET( CMAKE_FIND_LIBRARY_SUFFIXES_OLD ${CMAKE_FIND_LIBRARY_SUFFIXES} )
  SET( CMAKE_FIND_LIBRARY_SUFFIXES ".a")
ENDIF()

# MKL is composed by four layers: Interface, Threading, Computational and RTL

#Interface layer
FIND_LIBRARY(MKL_INTERFACE_LIBRARY_STATIC mkl_intel_lp64 PATH ${MKL_PATHS} )

#Threading layer
FIND_LIBRARY(MKL_THREADING_LIBRARY_STATIC ${MKL_THREADING_LIBNAME} PATH ${MKL_PATHS})

# Computational layer
FIND_LIBRARY(MKL_CORE_LIBRARY_STATIC mkl_core PATH ${MKL_PATHS})

# RTL layer
FIND_LIBRARY(MKL_RTL_LIBRARY_STATIC iomp5 PATH ${MKL_PATHS})

SET(MKL_LIBRARIES_STATIC ${MKL_INTERFACE_LIBRARY_STATIC} 
                         ${MKL_THREADING_LIBRARY_STATIC} ${MKL_CORE_LIBRARY_STATIC}  
                         ${MKL_THREADING_LIBRARY_STATIC} ${MKL_CORE_LIBRARY_STATIC} 
                         ${MKL_RTL_LIBRARY_STATIC} ${MKL_INTERFACE_LIBRARY_STATIC}
                         ${MKL_DL_LIBRARY} )

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(MKL_STATIC  DEFAULT_MSG
                                  MKL_INTERFACE_LIBRARY_STATIC
                                  MKL_THREADING_LIBRARY_STATIC
                                  MKL_CORE_LIBRARY_STATIC
                                  MKL_RTL_LIBRARY_STATIC
                                  MKL_INCLUDE_DIRS)

MARK_AS_ADVANCED(MKL_LIBRARIES_STATIC)

IF (UNIX)
  SET( CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES_OLD} )
ENDIF()
