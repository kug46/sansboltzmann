STRING( TOLOWER ${CMAKE_BUILD_TYPE} AVRO_BUILD_TYPE )
GET_FILENAME_COMPONENT( AVRO_BIN_NAME ${CMAKE_BINARY_DIR} NAME_WE )
FIND_LIBRARY( AVRO_LIBRARY avro HINTS $ENV{AVRO_BUILD_DIR}/lib $ENV{AVRO_DIR}/build/${AVRO_BUILD_TYPE}/lib $ENV{AVRO_DIR}/build/${AVRO_BIN_NAME}/lib REQUIRED )

FIND_PATH( AVRO_SRC_DIR include/avro.h HINTS $ENV{AVRO_DIR}/src REQUIRED )

SET( AVRO_INCLUDE_DIRS ${AVRO_SRC_DIR}/include
                       ${AVRO_SRC_DIR}/lib
                       ${AVRO_SRC_DIR}/third_party
                       ${AVRO_SRC_DIR}/third_party/triangle )

SET( AVRO_INCLUDE_TRIANGLE ${AVRO_SRC_DIR}/third_party/triangle )

# libz hopefully is on the system
FIND_LIBRARY( Z_LIBRARY z REQUIRED )
MARK_AS_ADVANCED( Z_LIBRARY )

# dl is required by avro (unless on Windoze where it does not exist)
IF (NOT MSYS)
  FIND_LIBRARY( DL_LIBRARY dl REQUIRED )
  MARK_AS_ADVANCED( DL_LIBRARY )
ENDIF()

# Find 'wsserver' and 'emp' which are part of ESP
FIND_LIBRARY( WSSERVER_LIBRARY  wsserver HINTS $ENV{ESP_ROOT}/lib $ENV{ESP_DIR}/lib ${SANS_ESP_DIR}/lib ${CMAKE_SOURCE_DIR}/external/ESP/lib REQUIRED )
FIND_LIBRARY( EMP_LIBRARY       emp      HINTS $ENV{ESP_ROOT}/lib $ENV{ESP_DIR}/lib ${SANS_ESP_DIR}/lib ${CMAKE_SOURCE_DIR}/external/ESP/lib REQUIRED )

SET( AVRO_LIBRARIES ${AVRO_LIBRARY} ${WSSERVER_LIBRARY} ${EMP_LIBRARY} ${Z_LIBRARY} ${DL_LIBRARY} )

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(AVRO DEFAULT_MSG
                                       AVRO_INCLUDE_DIRS
                                       AVRO_LIBRARY
                                       WSSERVER_LIBRARY
                                       EMP_LIBRARY
                                       Z_LIBRARY
                                       DL_LIBRARY )

IF( AVRO_FOUND )

  GET_FILENAME_COMPONENT( AVRO_LIBRARY_PATH ${AVRO_LIBRARY} PATH )

  SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
                LINK_DIRECTORIES ${AVRO_LIBRARY_PATH} )
                
ELSEIF( USE_AVRO )

  MESSAGE(STATUS "" )

  IF( DEFINED ENV{AVRO_DIR} )
    MESSAGE(STATUS "  AVRO_DIR environment variable is set to: $ENV{AVRO_DIR}" )
    IF( NOT AVRO_LIBRARIES )
      MESSAGE(STATUS "    could not find 'avro' library in $AVRO_DIR/build/${AVRO_BUILD_TYPE}/lib, $AVRO_DIR/build/${AVRO_BIN_NAME}/lib or $AVRO_BUILD_DIR/lib")
    ENDIF()
    IF( NOT AVRO_INCLUDE_DIRS )
      MESSAGE(STATUS "    could not find 'api/avro.h' in $AVRO_DIR/src")
    ENDIF()
  ELSE()
    MESSAGE(STATUS "  AVRO_DIR environment variable is not set" )
  ENDIF()
  
  IF( DEFINED SANS_AVRO_DIR )
    MESSAGE(STATUS "  SANS_AVRO_DIR is set to: ${SANS_AVRO_DIR}" )
    IF( NOT AVRO_LIBRARIES )
      MESSAGE(STATUS "    could not find 'avro' library in $SANS_AVRO_DIR/lib")
    ENDIF()
    IF( NOT AVRO_INCLUDE_DIRS )
      MESSAGE(STATUS "    could not find 'api/avro.h' in $SANS_AVRO_DIR/include")
    ENDIF()
  ENDIF()
  MESSAGE(STATUS "" )
  MESSAGE(STATUS "  SANS will attempt to download and compile avro for you." )
  MESSAGE(STATUS "" )

ENDIF()
