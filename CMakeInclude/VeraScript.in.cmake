#This script will execute vera and remove the report file if vera produces a non-zero return value
#This causes vera to behave like a compiler and not leave the VERA_DUMMY_OUTPUT when there is an error

EXECUTE_PROCESS( 
                COMMAND @VERA@ 
                     @VERA_SRC_FILE@ 
                     --root=@CMAKE_SOURCE_DIR@/scripts/vera/ 
                     --show-rule
                     --@style@-report=- #This prins to stdout
                     --profile default
                     @VERA_WARN_ERROR@
                     --@style@-report=@VERA_DUMMY_OUTPUT@
                     --exclusions @CMAKE_SOURCE_DIR@/scripts/vera/exclusions.txt
                     --parameter copyright=@CMAKE_SOURCE_DIR@/COPYRIGHT.txt
                RESULT_VARIABLE res_var
                )
                
IF(NOT "${res_var}" STREQUAL "0")
  EXECUTE_PROCESS( COMMAND ${CMAKE_COMMAND} -E remove @VERA_DUMMY_OUTPUT@ )
  MESSAGE(FATAL_ERROR "Please fix the formatting")
ENDIF()