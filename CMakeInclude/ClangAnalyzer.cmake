
IF ( ${CMAKE_VERSION} VERSION_GREATER 3.4.2 )
  SET( CLANG_ANALYZER "${CMAKE_CXX_COMPILER} -D__clang_analyzer__ --analyze -Xanalyzer -analyzer-output=text <SOURCE> <DEFINES> <FLAGS> <INCLUDES>" )
ELSE()
  SET( CLANG_ANALYZER "${CMAKE_CXX_COMPILER} -D__clang_analyzer__ --analyze -Xanalyzer -analyzer-output=text <SOURCE> <DEFINES> <FLAGS>" )
ENDIF()

SET(CMAKE_CXX_COMPILE_OBJECT "${CLANG_ANALYZER}; ${CMAKE_CXX_COMPILE_OBJECT}")

