

FUNCTION( COPYPCH TARGET_FROM TARGET_TO )

  IF( TARGET ${TARGET_FROM}_pch )
    GET_TARGET_PROPERTY(_prefixHeader ${TARGET_FROM} COTIRE_CXX_PREFIX_HEADER)
    
    IF (${CMAKE_CXX_COMPILER_ID} MATCHES "GNU")
      # GCC options used
      # -include process include file as the first line of the primary source file
      # -Winvalid-pch warns if precompiled header is found but cannot be used
      # note: ccache requires the -include flag to be used in order to process precompiled header correctly
      SET (_flags "-Winvalid-pch -include \"${_prefixHeader}\"")
    ELSEIF (${CMAKE_CXX_COMPILER_ID} MATCHES "Clang")
      # Clang options used
      # -include process include file as the first line of the primary source file
      # -include-pch include precompiled header file
      # -Qunused-arguments don't emit warning for unused driver arguments
      # note: ccache requires the -include flag to be used in order to process precompiled header correctly
      SET (_flags "-Qunused-arguments -include \"${_prefixHeader}\"")
    ENDIF()
      
    GET_TARGET_PROPERTY(_pchFile ${TARGET_FROM} COTIRE_CXX_PRECOMPILED_HEADER)

    GET_TARGET_PROPERTY(SOURCE_FILES ${TARGET_TO} SOURCES)
   
    FOREACH( SRCFILE ${SOURCE_FILES} )
      GET_FILENAME_COMPONENT(_prefixFileExt "${SRCFILE}" EXT)
      IF ( _prefixFileExt MATCHES "^\\.cpp")
        SET_PROPERTY(SOURCE ${SRCFILE} APPEND_STRING PROPERTY COMPILE_FLAGS " ${_flags} ")
    
        # make object files generated from source files depend on precompiled header
        SET_PROPERTY(SOURCE ${SRCFILE} APPEND PROPERTY OBJECT_DEPENDS "${_pchFile}")
      ENDIF()
    ENDFOREACH()
  ENDIF()

ENDFUNCTION()