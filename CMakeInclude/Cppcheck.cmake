

FIND_PROGRAM( CPPCHECK cppcheck )
MARK_AS_ADVANCED( CPPCHECK )

#IF( CPPCHECK AND NOT BIN_DIR_NAME MATCHES "DEPLOY" AND NOT BUILD_WITH_IDE )
#  SET( CPPCHECK_INIT ON )
#ELSE()
SET( CPPCHECK_INIT OFF )
#ENDIF()

OPTION( USE_CPPCHECK "Check source files with Cppcheck" ${CPPCHECK_INIT} )
OPTION( USE_CPPCHECK_COMP "Use Cppcheck with the compiler" OFF )

IF( USE_CPPCHECK OR USE_CPPCHECK_COMP )

IF( NOT CPPCHECK )
  MESSAGE( FATAL_ERROR "Please install cppcheck and add it to your path." )
ENDIF()

OPTION( CPPCHECK_ERROR "Treat cppcheck warnings as errors" OFF )

IF( CPPCHECK_ERROR )
  SET( CPPCHECK_EXITCODE 1 )
ELSE()
  SET( CPPCHECK_EXITCODE 0 )
ENDIF()

#This is the gcc tempalte with the ID of the error included
SET( CPPCHECK_TEMPLATE "{file}:{line}:" "cppcheck" "{severity}:" "({id})" "{message}")
SET( CPPCHECK_SEVERITY "warning,style,performance,portability" )
SET( CPPCHECK_INCLUDE -I ${CMAKE_SOURCE_DIR}/src -I ${CMAKE_SOURCE_DIR}/test )
SET( CPPCHECK_SUPPRESS ${CMAKE_SOURCE_DIR}/scripts/cppcheck/suppressions.txt )

#Deal with CMake quote oddities...
SET( CPPCHECK_TEMPLATE2 "" )
FOREACH( TMP ${CPPCHECK_TEMPLATE})
  SET( CPPCHECK_TEMPLATE2 "${CPPCHECK_TEMPLATE2} ${TMP}" )
ENDFOREACH()

SET( CPPCHECK_INCLUDE2 "" )
FOREACH( TMP ${CPPCHECK_INCLUDE})
  SET( CPPCHECK_INCLUDE2 "${CPPCHECK_INCLUDE2} ${TMP}" )
ENDFOREACH()

SET(CPPCHECK_COMMAND "${CPPCHECK} <DEFINES> --quiet --enable=${CPPCHECK_SEVERITY} --error-exitcode=${CPPCHECK_EXITCODE} --inline-suppr --suppressions-list=${CPPCHECK_SUPPRESS} --template=\"${CPPCHECK_TEMPLATE2}\" ${CPPCHECK_INCLUDE2} <SOURCE>")


IF( USE_CPPCHECK_COMP )
#Modify the compile command to include running cppcheck before compiling the code
#SET(CMAKE_C_COMPILE_OBJECT "${CPPCHECK_COMMAND}; ${CMAKE_C_COMPILE_OBJECT}")
#SET(CMAKE_CXX_COMPILE_OBJECT "${CPPCHECK_COMMAND}; ${CMAKE_CXX_COMPILE_OBJECT}")
  SET_PROPERTY( GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CPPCHECK_COMMAND} && " )
ENDIF()

IF( USE_CPPCHECK )
  # A single target that can be used to trigger cppcheck on all files
  ADD_CUSTOM_TARGET( cppcheck )
  
  FUNCTION( ADD_CPPCHECK CUSTOM_NAME )
  
    # Get the list of definitions for the current directory
    GET_PROPERTY( DEFLIST DIRECTORY PROPERTY COMPILE_DEFINITIONS )
    
    # Add the -D flag to each definition
    FOREACH( DEF ${DEFLIST} )
      LIST(APPEND DEFINES -D${DEF} )
    ENDFOREACH()
    
    # Generate the custom target to run cppcheck on the provided files
    ADD_CUSTOM_TARGET( ${CUSTOM_NAME}_cppcheck
                       COMMAND ${CPPCHECK} 
                            --enable=${CPPCHECK_SEVERITY}
                            --inline-suppr 
                            --suppressions-list=${CPPCHECK_SUPPRESS} 
                            --template="${CPPCHECK_TEMPLATE}"
                            --error-exitcode=${CPPCHECK_EXITCODE}
                            ${CPPCHECK_INCLUDE}
                            ${DEFINES}
                            ${ARGN}
                       COMMENT "Running Cppcheck on ${CUSTOM_NAME}"
                       WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                     )
                       
    ADD_DEPENDENCIES( cppcheck ${CUSTOM_NAME}_cppcheck )
  ENDFUNCTION()
ENDIF( USE_CPPCHECK )

ELSE( USE_CPPCHECK OR USE_CPPCHECK_COMP )

#Dummy command so that CMake does not complain
FUNCTION( ADD_CPPCHECK CUSTOM_NAME )
ENDFUNCTION()

ENDIF( USE_CPPCHECK OR USE_CPPCHECK_COMP )
