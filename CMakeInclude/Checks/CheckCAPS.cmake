# make sure that the EGADS library is available
SET(CMAKE_REQUIRED_LIBRARIES ${CAPS_LIBRARIES} ${EGADS_LIBRARIES} ${CAS_LIBRARIES})
SET(CMAKE_REQUIRED_INCLUDES ${CAPS_INCLUDE_DIRS})
IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${BUILD_TYPE}} ${CMAKE_EXE_LINKER_FLAGS_${BUILD_TYPE}}")
ELSE()
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS}")
ENDIF()

IF( DEFINED CAPS_TEST AND NOT CAPS_TEST )
  UNSET( CAPS_TEST CACHE )
ENDIF()

CHECK_CXX_SOURCE_COMPILES(
  "
#ifdef _WIN32
#define WIN32 // aimUtil uses WIN32 to identify windoze
#endif

#include <caps.h>
#include <aimUtil.h>

extern \"C\"
{

// ********************** AIM Function Break *****************************
int
aimInitialize(int ngIn, capsValue *gIn, int *qeFlag, const char *unitSys,
              int *nIn, int *nOut, int *nFields, char ***fnames, int **ranks)
{
  return CAPS_SUCCESS;
}

// ********************** AIM Function Break *****************************
int aimInputs(/*@unused@*/ int inst, /*@unused@*/ void *aimInfo, int index,
          char **ainame, capsValue *defval)
{
  return CAPS_SUCCESS;
}

// ********************** AIM Function Break *****************************
int aimPreAnalysis(/*@unused@*/ int inst, void *aimInfo,
                    const char *apath, /*@null@*/ capsValue *inputs, capsErrs **errs)
{
 return CAPS_SUCCESS;
}

// ********************** AIM Function Break *****************************
int aimOutputs( int inst, void *aimInfo, int index, char **aoname, capsValue *form)
{
  return CAPS_SUCCESS;
}

// ********************** AIM Function Break *****************************
int aimCalcOutput(int inst, /*@unused@*/ void *aimInfo, const char *apath, int index,
                  capsValue *val, capsErrs **errors)
{
  return CAPS_SUCCESS;
}


// ********************** AIM Function Break *****************************
int
aimFreeDiscr(/*@null@*/ capsDiscr *discr)
{
  return CAPS_SUCCESS;
}

} // extern C

/* dummy main because cmake does not have a test for shared libraries */
int main(int argc, char *argv[]) { return 0;}

  "
  CAPS_TEST)
  
IF(NOT CAPS_TEST)
  MESSAGE( "====================================================================" )
  MESSAGE( "")
  MESSAGE( "Compile tests of CAPS failed.\n\nSee CMakeFiles/CMakeError.log for more details." )
  MESSAGE( "")
  MESSAGE( " CAPS_LIBRARIES:" )
  FOREACH(LIBNAME ${CAPS_LIBRARIES})
  MESSAGE( "                ${LIBNAME}" )
  ENDFOREACH()
  MESSAGE( "" )
  MESSAGE( " CAPS_INCLUDE_DIRS:" )
  FOREACH(INCNAME ${CAPS_INCLUDE_DIRS})
  MESSAGE( "                   ${INCNAME}" )
  ENDFOREACH()
  MESSAGE( "")
  MESSAGE( "====================================================================" )
  MESSAGE( "" )
  MESSAGE( FATAL_ERROR "" )
ENDIF()

IF(CAPS_TEST)
  SET(CAPS_TEST_FAIL 0)
ELSE()
  SET(CAPS_TEST_FAIL 1)
ENDIF()

