# make sure that the TETGEN library is available
SET(CMAKE_REQUIRED_LIBRARIES ${TETGEN_LIBRARIES})
SET(CMAKE_REQUIRED_INCLUDES ${TETGEN_INCLUDE_DIRS})
IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${BUILD_TYPE}} ${CMAKE_EXE_LINKER_FLAGS_${BUILD_TYPE}}")
ELSE()
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS}")
ENDIF()

IF( DEFINED TETGEN_TEST AND (NOT TETGEN_TEST OR NOT (TETGEN_TEST_LIBRARIES STREQUAL TETGEN_LIBRARIES AND TETGEN_INCLUDE_DIRS STREQUAL TETGEN_TEST_INCLUDE_DIRS)) )
  UNSET( TETGEN_TEST CACHE )
  UNSET( TETGEN_TEST_LIBRARIES CACHE )
  UNSET( TETGEN_TEST_INCLUDE_DIRS CACHE )
ENDIF()

CHECK_CXX_SOURCE_COMPILES(
"

#define TETLIBRARY
#include <tetgen.h> // Defined tetgenio, tetrahedralize().

int main(int argc, char *argv[])
{
  int npts = 10;
  
  tetgenio in;

  //Initialize tetgen
  in.firstnumber = 0;
 
  in.numberofpointmtrs = 0; // One metric per point

  in.numberofpoints = npts;
  in.pointlist = new REAL[3*in.numberofpoints];
  in.pointmtrlist = new REAL[in.numberofpoints];
  in.pointmarkerlist = new int[in.numberofpoints];
  
  npts = 0;
  REAL metric = 0;
  in.pointmtrlist[npts] = metric;
  in.pointmarkerlist[npts] = 0;
  
  int ntri = 10;
  in.numberoffacets = ntri;
  in.facetlist = new tetgenio::facet[in.numberoffacets];
  in.facetmarkerlist = new int[in.numberoffacets];
  
  tetgenio emptymesh;
  tetrahedralize((char*)\"pYQ\", &in, &emptymesh);
  
  return 0;
}
"
TETGEN_TEST)
  
IF(TETGEN_TEST)
  SET(TETGEN_TEST_FAIL FALSE)
  
  # Save off the current state in case the library or inlcude is changed
  SET( TETGEN_TEST_LIBRARIES ${TETGEN_LIBRARIES} CACHE INTERNAL "TETGEN libraries used for testing" FORCE)
  SET( TETGEN_TEST_INCLUDE_DIRS ${TETGEN_INCLUDE_DIRS} CACHE INTERNAL "TETGEN inlude used for testing" FORCE)
ELSE()

  MESSAGE(STATUS "" )  
  MESSAGE(STATUS "====================================================================" )
  MESSAGE(STATUS " TetGen library test failed. See CMakeFiles/CMakeError.log for more details." )
  MESSAGE(STATUS "" )
  MESSAGE(STATUS " TETGEN_LIBRARIES:" )
  FOREACH(LIBNAME ${TETGEN_LIBRARIES})
  MESSAGE(STATUS "                  ${LIBNAME}" )
  ENDFOREACH()
  MESSAGE(STATUS "" )
  MESSAGE(STATUS " TETGEN_INCLUDE_DIRS:" )
  FOREACH(INCNAME ${TETGEN_INCLUDE_DIRS})
  MESSAGE(STATUS "                     ${INCNAME}" )
  ENDFOREACH()
  MESSAGE(STATUS "" )
  MESSAGE(STATUS " SANS will attempt to compile TetGen for you to resolve this." )
  MESSAGE(STATUS "====================================================================" )
  MESSAGE(STATUS "" )

  SET(TETGEN_TEST_FAIL TRUE)
ENDIF()

UNSET( CMAKE_REQUIRED_FLAGS )
UNSET( CMAKE_REQUIRED_DEFINITIONS )
UNSET( CMAKE_REQUIRED_INCLUDES )
UNSET( CMAKE_REQUIRED_LIBRARIES )
