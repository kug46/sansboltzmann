# make sure that the EGADS library is available
SET(CMAKE_REQUIRED_LIBRARIES ${Boost_LIBRARIES} ${MPI_LIBRARIES} ${PYTHON_LIBRARIES})
SET(CMAKE_REQUIRED_INCLUDES ${Boost_INCLUDE_DIR} ${MPI_INCLUDE_DIRS})
IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${BUILD_TYPE}} ${CMAKE_EXE_LINKER_FLAGS_${BUILD_TYPE}}")
ELSE()
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS}")
ENDIF()


IF( DEFINED BOOSTMPI_TEST AND NOT BOOSTMPI_TEST )
  UNSET( BOOSTMPI_TEST CACHE )
ENDIF()

CHECK_CXX_SOURCE_COMPILES(
"

#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>

int main(int argc, char *argv[])
{
  boost::mpi::environment env(argc, argv);
  boost::mpi::communicator world;
  
  std::cout << world.rank() << std::endl;
  
  return 0;
}
"
BOOSTMPI_TEST)
  
IF(NOT BOOSTMPI_TEST)
  MESSAGE( "====================================================================" )
  MESSAGE( "")
  MESSAGE( "Compile tests of Boost.MPI failed.\n\nSee CMakeFiles/CMakeError.log for more details." )
  MESSAGE( "")
  MESSAGE( " Boost_LIBRARIES:" )
  FOREACH(LIBNAME ${Boost_LIBRARIES})
  MESSAGE( "                ${LIBNAME}" )
  ENDFOREACH()
  MESSAGE( "" )
  MESSAGE( " Boost_INCLUDE_DIR:" )
  FOREACH(INCNAME ${Boost_INCLUDE_DIR})
  MESSAGE( "                   ${INCNAME}" )
  ENDFOREACH()
  MESSAGE( "")
  MESSAGE( "====================================================================" )
  MESSAGE( "" )
  MESSAGE( FATAL_ERROR "" )
ENDIF()

UNSET( CMAKE_REQUIRED_LIBRARIES )
UNSET( CMAKE_REQUIRED_INCLUDES )
UNSET( CMAKE_REQUIRED_FLAGS )
