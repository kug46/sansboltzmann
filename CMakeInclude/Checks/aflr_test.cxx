
// the minimum version of AFLR4_LIB
const int AFLR4_MIN_VERSION[3] = { 10,2,7 };

// the minimum version of AFLR3_LIB
const int AFLR3_MIN_VERSION[3] = { 16,30,10 };

#include <ug/UG_LIB.h> // Must be included first

#include <aflr2c/AFLR2_LIB.h>

#include <aflr3/AFLR3_LIB.h>
#include <ug3/UG3_LIB.h>
#include <anbl3/ANBL3_LIB.h>

#include <aflr4/AFLR4_LIB.h>
#include <egen/EGEN_LIB.h>
#include <surf_auto/SURF_AUTO_LIB.h>

#include <string.h> //strtok_r

int aflr2
 (int prog_argc,
  char *prog_argv[])

{
  // Declare AFLR2 grid generation variables.
  // This is required in all implementations!

  INT_1D *Bnd_Edge_Grid_BC_Flag = NULL;
  INT_1D *Bnd_Edge_ID_Flag = NULL;
  INT_1D *Bnd_Edge_Error_Flag  =  NULL;
  INT_2D *Bnd_Edge_Connectivity = NULL;
  INT_3D *Tria_Connectivity = NULL;
  INT_4D *Quad_Connectivity = NULL;

  DOUBLE_2D *Coordinates = NULL;
  DOUBLE_1D *Initial_Normal_Spacing = NULL;

  INT_1D *BG_Bnd_Edge_Grid_BC_Flag = NULL;
  INT_1D *BG_Bnd_Edge_ID_Flag = NULL;
  INT_2D *BG_Bnd_Edge_Connectivity = NULL;
  INT_3D *BG_Tria_Neighbors = NULL;
  INT_3D *BG_Tria_Connectivity = NULL;

  DOUBLE_1D *BG_Spacing = NULL;
  DOUBLE_2D *BG_Coordinates = NULL;
  DOUBLE_3D *BG_Metric = NULL;

  DOUBLE_1D *Source_Spacing = NULL;
  DOUBLE_2D *Source_Coordinates = NULL;
  DOUBLE_3D *Source_Metric = NULL;

  INT_ Error_Flag = 0;
  INT_ Message_Flag = 1;
  INT_ Number_of_Bnd_Edges = 0;
  INT_ Number_of_Nodes = 0;
  INT_ Number_of_Quads = 0;
  INT_ Number_of_Trias = 0;

  INT_ Number_of_BG_Bnd_Edges = 0;
  INT_ Number_of_BG_Nodes = 0;
  INT_ Number_of_BG_Trias = 0;

  INT_ Number_of_Source_Nodes = 0;

  Coordinates = (DOUBLE_2D *) ug_malloc (&Error_Flag, (Number_of_Nodes+1) * sizeof (DOUBLE_2D));

  // Register and set program parameter functions.

  ug_set_prog_param_n_dim (2);

  ug_set_prog_param_function1 (ug_initialize_aflr_param);
  ug_set_prog_param_function2 (aflr2_initialize_param);
  ug_set_prog_param_function2 (ice2_initialize_param);

  Error_Flag = aflr2_grid_generator (prog_argc, prog_argv,
                                     Message_Flag,
                                     &Number_of_Bnd_Edges, &Number_of_Nodes,
                                     &Number_of_Quads, &Number_of_Trias,
                                     &Number_of_BG_Bnd_Edges,
                                     &Number_of_BG_Nodes, &Number_of_BG_Trias,
                                     &Number_of_Source_Nodes,
                                     &Bnd_Edge_Connectivity,
                                     &Bnd_Edge_Error_Flag,
                                     &Bnd_Edge_Grid_BC_Flag,
                                     &Bnd_Edge_ID_Flag,
                                     &Quad_Connectivity, &Tria_Connectivity,
                                     &BG_Bnd_Edge_Connectivity,
                                     &BG_Bnd_Edge_Grid_BC_Flag,
                                     &BG_Bnd_Edge_ID_Flag,
                                     &BG_Tria_Neighbors,
                                     &BG_Tria_Connectivity,
                                     &Coordinates,
                                     Initial_Normal_Spacing,
                                     &BG_Coordinates,
                                     &BG_Spacing,
                                     &BG_Metric,
                                     &Source_Coordinates,
                                     &Source_Spacing,
                                     &Source_Metric);

  ug_free( &Coordinates );

  return Error_Flag;
}


int aflr3
 (int prog_argc,
  char *prog_argv[])

{
  // Declare AFLR3 grid generation variables.
  // This is required in all implementations!

  INT_1D *Surf_Error_Flag = NULL;
  INT_1D *Surf_Grid_BC_Flag = NULL;
  INT_1D *Surf_ID_Flag = NULL;
  INT_1D *Surf_Reconnection_Flag = NULL;
  INT_3D *Surf_Tria_Connectivity = NULL;
  INT_4D *Surf_Quad_Connectivity = NULL;
  INT_4D *Vol_Tet_Connectivity = NULL;
  INT_5D *Vol_Pent_5_Connectivity = NULL;
  INT_6D *Vol_Pent_6_Connectivity = NULL;
  INT_8D *Vol_Hex_Connectivity = NULL;

  DOUBLE_3D *Coordinates = NULL;
  DOUBLE_1D *Initial_Normal_Spacing = NULL;
  DOUBLE_1D *BL_Thickness = NULL;

  INT_4D *Vol_Tet_Neighbor = NULL;

  INT_4D *BG_Vol_Tet_Neighbors = NULL;
  INT_4D *BG_Vol_Tet_Connectivity = NULL;

  DOUBLE_3D *BG_Coordinates = NULL;
  DOUBLE_1D *BG_Spacing = NULL;
  DOUBLE_6D *BG_Metric = NULL;

  DOUBLE_3D *Source_Coordinates = NULL;
  DOUBLE_1D *Source_Spacing = NULL;
  DOUBLE_6D *Source_Metric = NULL;

  INT_ Error_Flag = 0;
  INT_ Message_Flag = 1;
  INT_ Number_of_BL_Vol_Tets = 0;
  INT_ Number_of_Nodes = 0;
  INT_ Number_of_Surf_Quads = 0;
  INT_ Number_of_Surf_Trias = 0;
  INT_ Number_of_Vol_Hexs = 0;
  INT_ Number_of_Vol_Pents_5 = 0;
  INT_ Number_of_Vol_Pents_6 = 0;
  INT_ Number_of_Vol_Tets = 0;

  INT_ Number_of_BG_Nodes = 0;
  INT_ Number_of_BG_Vol_Tets = 0;

  INT_ Number_of_Source_Nodes = 0;

  aflr3_register_anbl3 (anbl3_grid_generator, anbl3_initialize_param, anbl3_reset_ibcibf);

  Error_Flag = aflr3_grid_generator (prog_argc, prog_argv,
                                     Message_Flag,
                                     &Number_of_BL_Vol_Tets,
                                     &Number_of_BG_Nodes,
                                     &Number_of_BG_Vol_Tets,
                                     &Number_of_Nodes,
                                     &Number_of_Source_Nodes,
                                     &Number_of_Surf_Quads,
                                     &Number_of_Surf_Trias,
                                     &Number_of_Vol_Hexs,
                                     &Number_of_Vol_Pents_5,
                                     &Number_of_Vol_Pents_6,
                                     &Number_of_Vol_Tets,
                                     &Surf_Error_Flag, &Surf_Grid_BC_Flag,
                                     &Surf_ID_Flag, &Surf_Reconnection_Flag,
                                     &Surf_Quad_Connectivity,
                                     &Surf_Tria_Connectivity,
                                     &Vol_Hex_Connectivity,
                                     &Vol_Pent_5_Connectivity,
                                     &Vol_Pent_6_Connectivity,
                                     &Vol_Tet_Connectivity,
                                     &BG_Vol_Tet_Neighbors,
                                     &BG_Vol_Tet_Connectivity,
                                     &Coordinates,
                                     &Initial_Normal_Spacing, &BL_Thickness,
                                     &BG_Coordinates,
                                     &BG_Spacing,
                                     &BG_Metric,
                                     &Source_Coordinates,
                                     &Source_Spacing,
                                     &Source_Metric);

  Coordinates = (DOUBLE_3D *) ug_malloc (&Error_Flag, (Number_of_Nodes+1) * sizeof (DOUBLE_3D));
  ug_free(Coordinates);

  // Get neighbor information
  Error_Flag = ug3_ieliel2(Number_of_Vol_Tets, Number_of_Nodes, Vol_Tet_Connectivity, &Vol_Tet_Neighbor);
  ug_free(Vol_Tet_Neighbor);

  // Update neighbor information with BC markers
  Error_Flag = ug3_ieliel2b(Number_of_Surf_Trias, Number_of_Vol_Tets, Number_of_Nodes,
                            Surf_ID_Flag, Surf_Tria_Connectivity, Vol_Tet_Connectivity, Vol_Tet_Neighbor);

  return Error_Flag;
}

int check_aflr3_version()
{
  CHAR_133 Compile_Date;
  CHAR_133 Compile_OS;
  CHAR_133 Version_Date;
  CHAR_133 Version_Number;

  char *rest = NULL, *token = NULL;

  int i = 0, status = 0;

  // get the version
  aflr3_version(Compile_Date, Compile_OS, Version_Date, Version_Number);

  rest = Version_Number;
  while ((token = strtok_r(rest, ".", &rest))) {
    if (i > 2) {
      aflr3_version(Compile_Date, Compile_OS, Version_Date, Version_Number);
      printf("error: AFLR3 version number %s has more than 3 integers. Please fix aflr_test.cxx\\n", Version_Number );
      return 1;
    }
    if ( atoi(token) < AFLR3_MIN_VERSION[i] ) {
      status = 1;
      break;
    } else if ( atoi(token) > AFLR3_MIN_VERSION[i] ) {
      break;
    }
    i++;
  }

  if (status != 0) {
    // get the version again as strtok corrupts it
    aflr3_version(Compile_Date, Compile_OS, Version_Date, Version_Number);
    printf("\\n");
    printf("error: AFLR3 version number %s is less than %d.%d.%d\\n",
           Version_Number,
           AFLR3_MIN_VERSION[0],
           AFLR3_MIN_VERSION[1],
           AFLR3_MIN_VERSION[2]);
    printf("\\n");
  }

  return status;
}


int check_aflr4_version()
{
  CHAR_133 Compile_Date;
  CHAR_133 Compile_OS;
  CHAR_133 Version_Date;
  CHAR_133 Version_Number;

  char *rest = NULL, *token = NULL;

  int i = 0, status = 0;

  // get the version
  aflr4_version(Compile_Date, Compile_OS, Version_Date, Version_Number);

  rest = Version_Number;
  while ((token = strtok_r(rest, ".", &rest))) {
    if (i > 2) {
      aflr4_version(Compile_Date, Compile_OS, Version_Date, Version_Number);
      printf("error: AFLR4 version number %s has more than 3 integers. Please fix aflr_test.cxx\\n", Version_Number );
      return 1;
    }
    if ( atoi(token) < AFLR4_MIN_VERSION[i] ) {
      status = 1;
      break;
    } else if ( atoi(token) > AFLR4_MIN_VERSION[i] ) {
      break;
    }
    i++;
  }

  if (status != 0) {
    // get the version again as strtok corrupts it
    aflr4_version(Compile_Date, Compile_OS, Version_Date, Version_Number);
    printf("\\n");
    printf("error: AFLR4 version number %s is less than %d.%d.%d\\n",
           Version_Number,
           AFLR4_MIN_VERSION[0],
           AFLR4_MIN_VERSION[1],
           AFLR4_MIN_VERSION[2]);
    printf("\\n");
    return 1;
  }

  return status;
}


int main(int prog_argc, char *prog_argv[])
{
  int status = 0;
  if (prog_argc > 20)
  {
    aflr2(prog_argc, prog_argv);
    aflr3(prog_argc, prog_argv);
  }

  status += check_aflr3_version();
  status += check_aflr4_version();

  return status;
}
