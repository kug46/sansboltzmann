# make sure that the EGADS library is available
SET(CMAKE_REQUIRED_LIBRARIES ${Boost_LIBRARIES} ${PYTHON_LIBRARIES})
SET(CMAKE_REQUIRED_INCLUDES ${Boost_INCLUDE_DIR} ${PYTHON_INCLUDE_PATH})
IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${BUILD_TYPE}} ${CMAKE_EXE_LINKER_FLAGS_${BUILD_TYPE}}")
ELSE()
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS}")
ENDIF()


IF( DEFINED BOOSTPYTHON_TEST AND NOT BOOSTPYTHON_TEST )
  UNSET( BOOSTPYTHON_TEST CACHE )
ENDIF()

CHECK_CXX_SOURCE_COMPILES(
"

// Hello world test from Boost Pyton tutorial
char const* greet()
{
 return \"hello, world\";
}

#include <boost/python.hpp>

BOOST_PYTHON_MODULE(hello_ext)
{
    using namespace boost::python;
    def(\"greet\", greet);
}

// Dummy main needed for the test
int main(int argc, char *argv[])
{
  return 0;
}
"
BOOSTPYTHON_TEST)
  
IF(NOT BOOSTPYTHON_TEST)
  MESSAGE( "====================================================================" )
  MESSAGE( "")
  MESSAGE( "Compile tests of Boost.Python failed. Make sure Boost.Python" )
  MESSAGE( "")
  FOREACH(LIBNAME ${Boost_LIBRARIES}) 
    IF( LIBNAME MATCHES "python" )
      MESSAGE( "${LIBNAME}")
    ENDIF()
  ENDFOREACH()
  MESSAGE( "")
  MESSAGE( "was compiled with found Python library" )
  MESSAGE( "")
  MESSAGE( "${PYTHON_LIBRARIES}")
  MESSAGE( "")
  MESSAGE( "See CMakeFiles/CMakeError.log for more details." )
  MESSAGE( "")
  MESSAGE( "====================================================================" )
  MESSAGE( "" )
  MESSAGE( FATAL_ERROR "" )
ENDIF()

UNSET( CMAKE_REQUIRED_LIBRARIES )
UNSET( CMAKE_REQUIRED_INCLUDES )
UNSET( CMAKE_REQUIRED_FLAGS )
