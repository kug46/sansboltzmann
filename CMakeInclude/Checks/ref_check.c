
/* Copyright 2014 United States Government as represented by the
 * Administrator of the National Aeronautics and Space
 * Administration. No copyright is claimed in the United States under
 * Title 17, U.S. Code.  All Other Rights Reserved.
 *
 * The refine platform is licensed under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#include <stdlib.h>
#include <stdio.h>

// refine includes
#include <ref_mpi.h>
#include <ref_grid.h>
#include <ref_malloc.h>
#include <ref_metric.h>
#include <ref_adapt.h>
#include <ref_validation.h>
#include <ref_histogram.h>
#include <ref_export.h>
#include <ref_gather.h>
#include <ref_face.h>
#include <ref_migrate.h>
#include <ref_part.h>


int main( void )
{
  REF_INT i;

  // Include a veriety of functions that SANS uses
  REF_MPI ref_mpi = NULL;
  RSS( ref_mpi_create_from_comm( &ref_mpi, NULL ), "create ref mpi" );

  REF_GRID ref_grid = NULL;
  RSS( ref_grid_create( &ref_grid, ref_mpi ), "create grid" );
  REF_NODE ref_node = ref_grid_node(ref_grid);

  REF_INT pos = 0, global = 0;
  RSS( ref_node_add( ref_node, global, &pos ), "add node");
  ref_node_xyz(ref_node,0,pos) = 0;

  RSS( ref_node_local( ref_node, global, &pos ), "local node");

  // assigning metrics
  REF_DBL ref_m[6] = {0,1,2,3,4,5};
  ref_node_metric_set(ref_node,pos,ref_m);

  // retrieving metrics
  ref_node_metric_get(ref_node,pos,ref_m);

  // assigning partitioning
  ref_node_part(ref_node,pos) = 0;

  // initialize the nodes
  REF_INT nnodeGlobal = 10;
  RSS( ref_node_initialize_n_global( ref_node, nnodeGlobal ), "init global");

  // get the group with tets
  REF_CELL ref_cell_tet = ref_grid_tet(ref_grid);

  int nodeMapPos[4];
  REF_INT new_cell;
  RSS( ref_cell_add( ref_cell_tet, nodeMapPos, &new_cell ), "add cell");

  // get the group of triangles
  REF_CELL ref_cell_tri = ref_grid_tri(ref_grid);
  REF_INT new_face;
  RSS( ref_cell_add( ref_cell_tri, nodeMapPos, &new_face ), "add face");


  RSS( ref_migrate_shufflin(ref_grid), "shufflin" );

  // perform load balancing for refines preferences
  RSS(ref_migrate_to_balance(ref_grid), "balance");

  REF_GRID background_grid = NULL;
  RSS( ref_grid_deep_copy(&background_grid, ref_grid), "copy grid" );

  RSS( ref_validation_all( ref_grid ), "all validation" );
  RSS( ref_histogram_ratio( ref_grid ), "gram");

  // TODO: Make this optional
  RSS( ref_metric_constrain_curvature( ref_grid ), "curvature constraint");

  REF_BOOL all_done = REF_FALSE;
  REF_INT passes = 20;
  for (i = 0; i < passes ; i++ )
  {
    //RSS( ref_metric_sanitize(ref_grid), "sanitize metric" );

    RSS( ref_adapt_pass( ref_grid, &all_done ), "pass");
    RSS( ref_metric_synchronize(ref_grid), "sync with background");

    // TODO: Make this optional
    RSS( ref_metric_constrain_curvature( ref_grid ), "curvature constraint");

    // Check the volume of all cells
    RSS( ref_validation_cell_volume( ref_grid ),"vol");
  }
  // Optional: Prints to the screen
  RSS( ref_histogram_ratio( ref_grid ), "gram");
  RSS( ref_histogram_quality( ref_grid ), "mean ratio");

  // Cleanup memory
  RSS( ref_grid_free( background_grid ), "free background grid");

  RSS( ref_node_synchronize_globals(ref_grid_node(ref_grid)), "synchronize globals" );

  return 0;
}
