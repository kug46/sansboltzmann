# make sure that the LAPACK library is available
SET(CMAKE_REQUIRED_LIBRARIES ${LAPACK_LIBRARIES} ${BLAS_LIBRARIES})

IF( NOT APPLE )

  INCLUDE(CheckFunctionExists)

  # Extract all the functions used by SANS
  FILE(READ "${CMAKE_SOURCE_DIR}/src/LinearAlgebra/DenseLinAlg/tools/DenseLinAlg_LAPACK.h" SANS_LAPACK_HEADER )
    
  STRING(REGEX MATCHALL "void LAPACK_[a-z]*[(]" SANS_LAPACK_FUNCS ${SANS_LAPACK_HEADER})
  STRING(REGEX MATCHALL "LAPACK_[a-z]*" SANS_LAPACK_FUNCS "${SANS_LAPACK_FUNCS}")
  STRING(REGEX REPLACE "LAPACK_" "" SANS_LAPACK_FUNCS "${SANS_LAPACK_FUNCS}")
  
  FOREACH(FUNC ${SANS_LAPACK_FUNCS})
    # Other possible variants of the function
    SET( FUNC_ADD_ "${FUNC}_" )
    STRING(TOUPPER FUNC FUNC_UPPER)
    
    # check what name mangling is used
    CHECK_FUNCTION_EXISTS("${FUNC_ADD_}"  LAPACK_${FUNC}_ADD_    )
    IF (NOT LAPACK_${FUNC}_ADD_)
    CHECK_FUNCTION_EXISTS("${FUNC_UPPER}" LAPACK_${FUNC}_UPPER   )
    IF (NOT LAPACK_${FUNC}_UPPER)
    CHECK_FUNCTION_EXISTS("${FUNC}"       LAPACK_${FUNC}_NOCHANGE)
    ENDIF()
    ENDIF()

    IF( NOT (LAPACK_${FUNC}_ADD_ OR LAPACK_${FUNC}_UPPER OR LAPACK_${FUNC}_NOCHANGE))
      MESSAGE(STATUS "====================================================================" )
      MESSAGE(STATUS "Failed to find LAPACK function ${FUNC}. See CMakeFiles/CMakeError.log for more details.")
      MESSAGE(STATUS "" )
      MESSAGE(STATUS " LAPACK_LIBRARIES:" )
      FOREACH(LIBNAME ${LAPACK_LIBRARIES})
      MESSAGE(STATUS "                  ${LIBNAME}" )
      ENDFOREACH()
      MESSAGE(STATUS "")
      MESSAGE(STATUS " SANS will attempt to compile LAPACK for you to resolve this." )
      MESSAGE(STATUS "====================================================================" )
      SET( LAPACK_FOUND FALSE )
    ENDIF()
    
    IF (LAPACK_${FUNC}_ADD_)
      SET( LAPACK_ADD_ TRUE )
    ELSEIF( LAPACK_${FUNC}_UPPER)
      SET( LAPACK_UPPER TRUE )
    ELSEIF( LAPACK_${FUNC}_NOCHANGE )
      SET( LAPACK_NOCHANGE TRUE )
    ENDIF()
    
  ENDFOREACH()
  
  IF (LAPACK_ADD_)
    ADD_DEFINITIONS( -DLAPACK_GLOBAL_PATTERN_LC )
    SET(CMAKE_REQUIRED_DEFINITIONS "${CMAKE_REQUIRED_DEFINITIONS} -DLAPACK_GLOBAL_PATTERN_LC")
  ELSEIF (LAPACK_UPPER)
    ADD_DEFINITIONS( -DLAPACK_GLOBAL_PATTERN_UC )
    SET(CMAKE_REQUIRED_DEFINITIONS "${CMAKE_REQUIRED_DEFINITIONS} -DLAPACK_GLOBAL_PATTERN_UC")
  ELSEIF (LAPACK_NOCHANGE)
    ADD_DEFINITIONS( -DLAPACK_GLOBAL_PATTERN_MC )
    SET(CMAKE_REQUIRED_DEFINITIONS "${CMAKE_REQUIRED_DEFINITIONS} -DLAPACK_GLOBAL_PATTERN_MC")
  ENDIF()

ENDIF()

# Set remaining varaibles after the function check
SET(CMAKE_REQUIRED_INCLUDES ${CMAKE_SOURCE_DIR})
SET(CMAKE_REQUIRED_DEFINITIONS "-DDLA_LAPACK")
IF( APPLE )
  SET(CMAKE_REQUIRED_DEFINITIONS "${CMAKE_REQUIRED_DEFINITIONS} -DLAPACK_ACCELERATE")
ENDIF()
IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${BUILD_TYPE}} ${CMAKE_EXE_LINKER_FLAGS_${BUILD_TYPE}}")
ELSE()
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS}")
ENDIF()

IF( DEFINED LAPACK_TEST AND (NOT LAPACK_TEST OR NOT (LAPACK_TEST_LIBRARIES STREQUAL LAPACK_LIBRARIES)) )
  UNSET( LAPACK_TEST CACHE )
  UNSET( LAPACK_TEST_LIBRARIES CACHE )
ENDIF()

IF (LAPACK_FOUND)

CHECK_CXX_SOURCE_RUNS(
  "
  #include <src/LinearAlgebra/DenseLinAlg/tools/DenseLinAlg_LAPACK.h>

  int
  main (void)
  {
    double a[] = {3, 2, 1,
                  2, 3, 2,
                  1, 2, 3};
    double wr[3] = {0};
    double wi[3] = {0};

    char jobvl = 'N';   // left eigenvector (which is r-eig of row-matrix)
    char jobvr = 'N';   // right eigenvector
    double *vl = nullptr;  // left eigenvector place holder
    lapack_int ldvl = 1;
    double *vr = nullptr;  // right eigenvector place holder
    lapack_int ldvr = 1;
    lapack_int n = 3;
    lapack_int lda = 3;
    lapack_int lwork = 16*3;
    double work[ 16*3 ];
    lapack_int INFO;

    LAPACK_dgeev(&jobvl,&jobvr,&n,a,&lda,wr,wi,vl,&ldvl,vr,&ldvr,work,&lwork,&INFO);
 
    return INFO;
  }
  "
  LAPACK_TEST)

ELSE()
  SET (LAPACK_TEST FALSE)
ENDIF()

UNSET( CMAKE_REQUIRED_FLAGS )
UNSET( CMAKE_REQUIRED_DEFINITIONS )
UNSET( CMAKE_REQUIRED_INCLUDES )
UNSET( CMAKE_REQUIRED_LIBRARIES )

IF(LAPACK_TEST AND LAPACK_FOUND)
  SET(LAPACK_TEST_FAIL FALSE)
  
    # Save off the current state in case the library or inlcude is changed
  SET( LAPACK_TEST_LIBRARIES ${LAPACK_LIBRARIES} CACHE INTERNAL "LAPACK libraries used for testing" FORCE)
ELSE()

  MESSAGE(STATUS "" )  
  MESSAGE(STATUS "====================================================================" )
  MESSAGE(STATUS " LAPACK library test failed. See CMakeFiles/CMakeError.log for more details." )
  MESSAGE(STATUS "" )
  MESSAGE(STATUS " LAPACK_LIBRARIES:" )
  FOREACH(LIBNAME ${LAPACK_LIBRARIES})
  MESSAGE(STATUS "                  ${LIBNAME}" )
  ENDFOREACH()
  MESSAGE(STATUS "" )
  MESSAGE(STATUS " SANS will attempt to compile LAPACK for you to resolve this." )
  MESSAGE(STATUS "====================================================================" )
  MESSAGE(STATUS "" )

  SET(LAPACK_TEST_FAIL TRUE)
ENDIF()

