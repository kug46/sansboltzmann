
#include "mkl_pardiso.h"
#include "mkl_types.h"


int main (void)
{
  // Matrix data.
  MKL_INT64 n = 8;
  MKL_INT64 ia[9] = { 0, 4, 7, 9, 11, 14, 16, 17, 18};
  MKL_INT64 ja[18] =
  { 0,   2,       5, 6,
      1, 2,    4,
         2,             7,
            3,       6,
               4, 5, 6,
                  5,    7,
                     6,
                        7
  };
  double a[18] =
  { 7.0,      1.0,           2.0, 7.0,
        -4.0, 8.0,      2.0,
              1.0,                     5.0,
                   7.0,           9.0,
                        5.0, 1.0, 5.0,
                            -1.0,      5.0,
                                 11.0,
                                       5.0
  };
  MKL_INT64 mtype = -2; // Real symmetric matrix
  double b[8], x[8];    // RHS and solution vectors.
  MKL_INT64 nrhs = 1;   // Number of right hand sides.
  void *pt[64];         // Internal solver memory pointer pt
  MKL_INT64 iparm[64];  // Pardiso control parameters.
  MKL_INT64 maxfct, mnum, phase, error, msglvl;
  double ddum;          // dummy
  MKL_INT64 idum;       // dummy

/* -------------------------------------*/
/*  Pardiso control parameters          */
/* -------------------------------------*/
  for (int i = 0; i < 64; i++ )
      iparm[i] = 0;

  iparm[0] = 1;         // No solver default
  iparm[1] = 2;         // Fill-in reordering from METIS
  iparm[3] = 0;         // No iterative-direct algorithm
  iparm[4] = 0;         // No user fill-in reducing permutation
  iparm[5] = 0;         // Write solution into x
  iparm[7] = 2;         // Max numbers of iterative refinement steps
  iparm[9] = 13;        // Perturb the pivot elements with 1E-13
  iparm[10] = 1;        // Use nonsymmetric permutation and scaling MPS
  iparm[12] = 0;        // Maximum weighted matching algorithm is switched-off (default for symmetric). Try iparm[12] = 1 in case of inappropriate accuracy
  iparm[13] = 0;        // Output: Number of perturbed pivots
  iparm[17] = 0;        // Output: Number of nonzeros in the factor LU
  iparm[18] = 0;        // Output: Mflops for LU factorization
  iparm[19] = 0;        // Output: Numbers of CG Iterations
  iparm[34] = 1;        // PARDISO use C-style indexing for ia and ja arrays
  maxfct = 1;           // Maximum number of numerical factorizations.
  mnum = 1;             // Which factorization to use.
  msglvl = 0;           // No output
  error = 0;            // Initialize error flag

  for (int i = 0; i < 64; i++ )
      pt[i] = 0;

/* --------------------------------------------------------------------*/
/*   Reordering and Symbolic Factorization. This step also allocates   */
/*    all memory that is necessary for the factorization.              */
/* --------------------------------------------------------------------*/
  phase = 11;
  PARDISO_64(pt, &maxfct, &mnum, &mtype, &phase,
             &n, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &error);
  if ( error != 0 )
      return 1;

/* ----------------------------*/
/*  Numerical factorization    */
/* ----------------------------*/
  phase = 22;
  PARDISO_64(pt, &maxfct, &mnum, &mtype, &phase,
             &n, a, ia, ja, &idum, &nrhs, iparm, &msglvl, &ddum, &ddum, &error);
  if ( error != 0 )
    return 1;

/* -----------------------------------------------*/
/*  Back substitution and iterative refinement   */
/* -----------------------------------------------*/
  phase = 33;

  // Set right hand side to one.
  for (int i = 0; i < n; i++ )
      b[i] = 1;

  PARDISO_64(pt, &maxfct, &mnum, &mtype, &phase,
             &n, a, ia, ja, &idum, &nrhs, iparm, &msglvl, b, x, &error);
  if ( error != 0 )
    return 1;

/* --------------------------------------*/
/*  Termination and release of memory.   */
/* --------------------------------------*/
  phase = -1;           /* Release internal memory. */
  PARDISO_64(pt, &maxfct, &mnum, &mtype, &phase,
             &n, &ddum, ia, ja, &idum, &nrhs,
             iparm, &msglvl, &ddum, &ddum, &error);
  return 0;
}
