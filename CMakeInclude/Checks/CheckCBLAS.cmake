# make sure that the CBLAS library is available

# MKL needs the math library for this test
IF( USE_MKL )
  FIND_LIBRARY(MATH_LIBRARY m)
ELSE()
  SET( MATH_LIBRARY )
ENDIF()

SET(CMAKE_REQUIRED_LIBRARIES ${MATH_LIBRARY} ${BLAS_LIBRARIES} ${MATH_LIBRARY})
SET(CMAKE_REQUIRED_INCLUDES ${BLAS_INCLUDE_DIRS} ${CMAKE_SOURCE_DIR}/src)
SET(CMAKE_REQUIRED_DEFINITIONS ${BLAS_DEFINITIONS} )
UNSET( CMAKE_REQUIRED_FLAGS )
UNSET( MATH_LIBRARY CACHE )

IF( DEFINED CBLAS_TEST AND NOT CBLAS_TEST )
  UNSET( CBLAS_TEST CACHE )
ENDIF()

CHECK_CXX_SOURCE_COMPILES(
"
/* */

#include <LinearAlgebra/DenseLinAlg/tools/DenseLinAlg_BLAS.h>

void floatBLAS()
{
  float *x=0, *y=0, a=0, *A=0, *B=0, *C=0, sgn=0;
  int size=0, stride=0, start=0, m=0, n=0, k=0, Astride=0, Bstride=0, Cstride=0;

  cblas_sswap(size, x, 1, y, 1);
  cblas_sscal(size, a, x, 1);
  cblas_saxpy(size, a, x, 1, y, 1);
  cblas_isamax(size-start, x + start*stride, stride);
  cblas_sgemv( CblasRowMajor, CblasNoTrans, m, k, sgn, A, Astride, B, Bstride, 0., C, Cstride );
  cblas_sgemm( CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, sgn, A, Astride, B, Bstride, 0., C, Cstride );
}

void doubleBLAS()
{
  double *x=0, *y=0, a=0, *A=0, *B=0, *C=0, sgn=0;
  int size=0, stride=0, start=0, m=0, n=0, k=0, Astride=0, Bstride=0, Cstride=0;

  cblas_dswap(size, x, 1, y, 1);
  cblas_dscal(size, a, x, 1);
  cblas_daxpy(size, a, x, 1, y, 1);
  cblas_idamax(size-start, x + start*stride, stride);
  cblas_dgemv( CblasRowMajor, CblasNoTrans, m, k, sgn, A, Astride, B, Bstride, 0., C, Cstride );
  cblas_dgemm( CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, k, sgn, A, Astride, B, Bstride, 0., C, Cstride );
  
}

int main(int argc, char** argv)
{
  floatBLAS();
  doubleBLAS();
  return 0;
}
"
CBLAS_TEST )

IF ( NOT CBLAS_TEST )
  MESSAGE(STATUS "" )  
  MESSAGE(STATUS "=================================" )
  MESSAGE(STATUS " Missing CBLAS library. See CMakeFiles/CMakeError.log for more details." )
  MESSAGE(STATUS " You may set the envonment variable $BLAS_LIBRARIES with any missing blas libraries." )
  MESSAGE(STATUS " Found libraries: ${BLAS_LIBRARIES}" )
  MESSAGE(STATUS " $BLAS_LIBRARIES is currently set to: $ENV{BLAS_LIBRARIES}" )
  MESSAGE(STATUS "" )
  MESSAGE(STATUS " SANS will attempt to compile cblas for you to resolve this." )
  MESSAGE(STATUS "=================================" )
  MESSAGE(STATUS "" )
  SET( BUILD_CBLAS TRUE )
ENDIF()

UNSET( CMAKE_REQUIRED_FLAGS )
UNSET( CMAKE_REQUIRED_DEFINITIONS )
UNSET( CMAKE_REQUIRED_INCLUDES )
UNSET( CMAKE_REQUIRED_LIBRARIES )

