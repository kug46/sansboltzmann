# make sure that the EGADS library is available
SET(CMAKE_REQUIRED_LIBRARIES ${EGADS_LIBRARIES} ${CAS_LIBRARIES})
SET(CMAKE_REQUIRED_INCLUDES ${EGADS_INCLUDE_DIRS})
IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${BUILD_TYPE}} ${CMAKE_EXE_LINKER_FLAGS_${BUILD_TYPE}}")
ELSE()
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_CXX_FLAGS}")
ENDIF()

IF( DEFINED EGADS_TEST AND NOT EGADS_TEST )
  UNSET( EGADS_TEST CACHE )
ENDIF()

CHECK_CXX_SOURCE_COMPILES(
  "
/*
 *      EGADS: Electronic Geometry Aircraft Design System
 *
 *             Sweep Test
 *
 *      Copyright 2011-2014, Massachusetts Institute of Technology
 *      Licensed under The GNU Lesser General Public License, version 2.1
 *      See http://www.opensource.org/licenses/lgpl-2.1.php
 *
 */


#include <egads.h>
#include <cassert>

#if EGADSMAJOR == 1 && EGADSMINOR < 9
#error \"SANS requires EGADS version 1.9 or greater\"
#endif

int main(int argc, char *argv[])
{
  int mtype, oclass, nbody, nface, nedge, *senses, i, j;
  const char *OCCrev;
  ego context, model, newModel, geom, body, *bodies, *faces, *edges, tess=NULL;

  EG_revision(&i, &j, &OCCrev);
  printf(\"EGADS    : Using EGADS %2d.%02d with %s\", i, j, OCCrev);

  /* initialize */
  assert( EG_open(&context) == EGADS_SUCCESS );
  assert( EG_loadModel(context, 0, \"Piston.BRep\", &model) == EGADS_SUCCESS );
  /* get all bodies */
  assert( EG_getTopology(model, &geom, &oclass, &mtype, NULL, &nbody, &bodies, &senses) == EGADS_SUCCESS );
  /* get all faces in body */
  assert( EG_getBodyTopos(bodies[0], NULL, FACE, &nface, &faces) == EGADS_SUCCESS );

  /* get all edges in body */
  assert( EG_getBodyTopos(bodies[0], NULL, EDGE, &nedge, &edges) == EGADS_SUCCESS );

  assert( EG_sweep(faces[5], edges[1], 0, &body) == EGADS_SUCCESS );

  /* make a model */
  assert( EG_makeTopology(context, NULL, MODEL, 0, NULL, 1, &body, NULL, &newModel) == EGADS_SUCCESS );

  assert( EG_openTessBody(tess) );
  int nloop = 0;
  const int *lIndices = NULL;
  assert( EG_getTessLoops( tess, i, &nloop, &lIndices ) );
  int state, np;
  assert( EG_statusTessBody(tess, &body, &state, &np) );

  assert( EG_deleteObject(newModel) );
  EG_free(faces);
  assert( EG_deleteObject(model) );
  assert( EG_close(context) );
  return 0;
}
  "
  EGADS_TEST)
  
IF(NOT EGADS_TEST)
  MESSAGE( "====================================================================" )
  MESSAGE( "")
  MESSAGE( "Compile tests of EGADS failed.\n\nSee CMakeFiles/CMakeError.log for more details." )
  MESSAGE( "")
  MESSAGE( " EGADS_LIBRARIES:" )
  FOREACH(LIBNAME ${EGADS_LIBRARIES})
  MESSAGE( "                 ${LIBNAME}" )
  ENDFOREACH()
  MESSAGE( "" )
  MESSAGE( " EGADS_INCLUDE_DIRS:" )
  FOREACH(INCNAME ${EGADS_INCLUDE_DIRS})
  MESSAGE( "                    ${INCNAME}" )
  ENDFOREACH()
  MESSAGE( "")
  MESSAGE( "====================================================================" )
  MESSAGE( "" )
  MESSAGE( FATAL_ERROR "" )
ENDIF()

IF(EGADS_TEST)
  SET(EGADS_TEST_FAIL 0)
ELSE()
  SET(EGADS_TEST_FAIL 1)
ENDIF()

