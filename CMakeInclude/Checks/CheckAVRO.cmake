# make sure that the avro library is available
INCLUDE( CheckCSourceCompiles )

# C math library
FIND_LIBRARY(CMATH_LIBRARY m )
MARK_AS_ADVANCED(CMATH_LIBRARY)

SET(CMAKE_REQUIRED_LIBRARIES ${AVRO_LIBRARIES} ${CMATH_LIBRARY} ${EGADS_LIBRARIES} ${CAS_LIBRARIES} ${BLAS_LIBRARIES} )
SET(CMAKE_REQUIRED_INCLUDES ${AVRO_INCLUDE_DIRS})
IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_C_FLAGS} ${CMAKE_C_FLAGS_${BUILD_TYPE}} ${CMAKE_EXE_LINKER_FLAGS_${BUILD_TYPE}}")
ELSE()
  SET(CMAKE_REQUIRED_FLAGS "${CMAKE_C_FLAGS}")
ENDIF()

IF( DEFINED AVRO_TEST AND (NOT AVRO_TEST OR NOT (AVRO_TEST_LIBRARIES STREQUAL AVRO_LIBRARIES AND AVRO_INCLUDE_DIRS STREQUAL AVRO_TEST_INCLUDE_DIRS)) )
  UNSET( AVRO_TEST CACHE )
  UNSET( AVRO_TEST_LIBRARIES CACHE )
  UNSET( AVRO_TEST_INCLUDE_DIRS CACHE )
ENDIF()

CHECK_CXX_SOURCE_COMPILES( 
"
#include <avro.h>

int main(int argc, char *argv[])
{
  using avro::coord_t;
  using avro::index_t;

  coord_t number = 2;

  std::vector<double> lens(number,1.);
  std::vector<index_t> dims(number,5);

  avro::library::CubeMesh mesh( lens , dims );

  avro::Context context;

  double xc[3] = {.5,.5,0.};
  avro::library::EGADSSquare body( &context , xc , lens[0] , lens[1] );
  mesh.vertices().findGeometry(body);
}
"
AVRO_TEST)

IF(AVRO_TEST)
  SET(AVRO_TEST_FAIL FALSE)
  
  # Save off the current state in case the library or inlcude is changed
  SET( AVRO_TEST_LIBRARIES ${AVRO_LIBRARIES} CACHE INTERNAL "avro libraries used for testing" FORCE)
  SET( AVRO_TEST_INCLUDE_DIRS ${AVRO_INCLUDE_DIRS} CACHE INTERNAL "avro inlude used for testing" FORCE)
ELSE()

  MESSAGE(STATUS "" )  
  MESSAGE(STATUS "====================================================================" )
  MESSAGE(STATUS " avro library test failed. See CMakeFiles/CMakeError.log for more details." )
  MESSAGE(STATUS "" )
  MESSAGE(STATUS " AVRO_LIBRARIES:" )
  FOREACH(LIBNAME ${AVRO_LIBRARIES})
  MESSAGE(STATUS "                ${LIBNAME}" )
  ENDFOREACH()
  MESSAGE(STATUS "" )
  MESSAGE(STATUS " AVRO_INCLUDE_DIRS:" )
  FOREACH(INCNAME ${AVRO_INCLUDE_DIRS})
  MESSAGE(STATUS "                   ${INCNAME}" )
  ENDFOREACH()
  MESSAGE(STATUS "" )
  MESSAGE(STATUS " SANS will attempt to compile avro for you to resolve this." )
  MESSAGE(STATUS "====================================================================" )
  MESSAGE(STATUS "" )

  SET(AVRO_TEST_FAIL TRUE)
ENDIF()

UNSET( CMAKE_REQUIRED_FLAGS )
UNSET( CMAKE_REQUIRED_DEFINITIONS )
UNSET( CMAKE_REQUIRED_INCLUDES )
UNSET( CMAKE_REQUIRED_LIBRARIES )
