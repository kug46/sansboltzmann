INCLUDE(ExternalProject)

SET( ESP_ROOT ${CMAKE_SOURCE_DIR}/external/ESP/ )

IF( NOT FOUND_CAS )

  SET( CASREV 7.3 )

  IF(APPLE)
    SET(ESP_ARCH DARWIN64)
    SET(CAS_TAR OCC731osx64.tgz)
  ELSEIF(MSVC)
    SET(ESP_ARCH WIN64)
  ELSE()
    SET(ESP_ARCH LINUX64)
    SET(CAS_TAR OCC731lin64.tgz)
  ENDIF()

  SET( CAS_URL ${CMAKE_SOURCE_DIR}/external/${CAS_TAR} )  
  MESSAGE(STATUS "Checking for ${CAS_URL}")
  IF( NOT EXISTS ${CAS_URL} )
    SET( CAS_URL https://acdl.mit.edu/ESP/${CAS_TAR} )
    MESSAGE(STATUS "Downloading OpenCASCADE from ${CAS_URL}")
  ELSE()
    MESSAGE(STATUS "Using ${CAS_URL}")
  ENDIF()

  SET( CASROOT ${CMAKE_SOURCE_DIR}/external/OpenCASCADE-${CASREV}.1 )

  ExternalProject_Add(
    libOCC
    URL ${CAS_URL}
    DOWNLOAD_DIR ${CMAKE_SOURCE_DIR}/external/
    SOURCE_DIR ${CASROOT}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    BUILD_IN_SOURCE 1
    INSTALL_COMMAND ""
  )

  SET( OCC_DEPENDS libOCC )
  SET(OCAS_LIBRARY_PATH ${CASROOT}/lib CACHE PATH "OpenCASCADE library directory." )

ELSE()
  # Use the library previously found bith without /lib
  STRING( FIND ${OCAS_LIBRARY_PATH} "/lib" LAST_DIR_IDX REVERSE )
  STRING( SUBSTRING ${OCAS_LIBRARY_PATH} 0 ${LAST_DIR_IDX} CASROOT )
ENDIF()

IF(APPLE)
  SET(ESP_ARCH DARWIN64)
ELSEIF(MSVC)
  SET(ESP_ARCH WIN64)
ELSE()
  SET(ESP_ARCH LINUX64)
ENDIF()

SET( ESP_URL ${CMAKE_SOURCE_DIR}/external/ESP.tgz )  
MESSAGE(STATUS "Checking for ${ESP_URL}")
IF( NOT EXISTS ${ESP_URL} )
  SET( ESP_URL https://acdl.mit.edu/ESP/ESP.tgz )
  MESSAGE(STATUS "Downloading and compiling ESP from ${ESP_URL}")
ELSE()
  MESSAGE(STATUS "Using ${ESP_URL}")
ENDIF()

# Need bash to build ESP...
FIND_PROGRAM(BASH bash)

ExternalProject_Add(
  libESP
  DEPENDS ${OCC_DEPENDS}
  URL ${ESP_URL}
  DOWNLOAD_DIR ${CMAKE_SOURCE_DIR}/external/
  SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/ESP/
  CONFIGURE_COMMAND ${BASH} -c "cd ${ESP_ROOT}/config && ./makeEnv ${CASROOT}"
  BINARY_DIR ${ESP_ROOT}/src
  BUILD_COMMAND ${BASH} -c "source ${ESP_ROOT}/ESPenv.sh && make"
  INSTALL_DIR ${ESP_ROOT}
  INSTALL_COMMAND ""
)

IF( USE_CAPS )
  ExternalProject_Add_Step(libESP CAPS
    DEPENDEES build
    COMMENT "Building CAPS"
    COMMAND ${BASH} -c "source ${ESP_ROOT}/ESPenv.sh && make CAPS"
    WORKING_DIRECTORY ${ESP_ROOT}/src
  )
  SET(CAPS_USE_INTERNAL TRUE)
ENDIF()

SET_PROPERTY( TARGET libESP PROPERTY FOLDER "Externals")

SET(ESP_INCLUDE_DIRS ${ESP_ROOT}/include)
SET(ESP_LIBRARIES ${ESP_ROOT}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}egads${CMAKE_SHARED_LIBRARY_SUFFIX} )
SET(ESP_LIBRARY_PATH ${ESP_ROOT}/lib)

SET(EGADS_DEPENDS libESP)
SET(EGADS_USE_INTERNAL TRUE)

SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
              LINK_DIRECTORIES ${EGADS_LIBRARY_PATH} ${OCAS_LIBRARY_PATH} )

LIST(APPEND SANS_EXTERNAL_DEPENDS ${EGADS_DEPENDS})
