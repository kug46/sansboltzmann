INCLUDE(ExternalProject)

SET( LAPACK_VERSION 3.8.0 )

SET( LAPACK_INSTALL_DIR ${CMAKE_BINARY_DIR}/liblapack-prefix/lapack-${LAPACK_VERSION} )

SET( LAPACK_URL ${CMAKE_SOURCE_DIR}/external/lapack-${LAPACK_VERSION}.tar.gz )  
IF( NOT EXISTS ${LAPACK_URL} )
  SET( LAPACK_URL http://www.netlib.org/lapack/lapack-${LAPACK_VERSION}.tar.gz )
  MESSAGE(STATUS "Downloading and compiling LAPACK from ${LAPACK_URL}")
ELSE()
  MESSAGE(STATUS "Using ${LAPACK_URL}")
ENDIF()

ExternalProject_Add(
  liblapack
  URL ${LAPACK_URL}
  INSTALL_DIR ${LAPACK_INSTALL_DIR}
  SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/lapack-${LAPACK_VERSION}
  CMAKE_ARGS -DCMAKE_BUILD_TYPE:STRING=Release
             -DCMAKE_INSTALL_PREFIX:PATH=${LAPACK_INSTALL_DIR}
             -DUSE_OPTIMIZED_LAPACK:BOOL=OFF # Compile lapack
             -DUSE_OPTIMIZED_BLAS:BOOL=ON
             -DLAPACKE:BOOL=OFF
             -DBUILD_SHARED_LIBS:BOOL=ON
             -DBUILD_DOUBLE:BOOL=ON
             -DBUILD_SINGLE:BOOL=ON
             -DBUILD_TESTING:BOOL=OFF
             -DBUILD_COMPLEX:BOOL=ON   # 3.8.0 has a bug in lapacke that requires this on...
             -DBUILD_COMPLEX16:BOOL=ON # 3.8.0 has a bug in lapacke that requires this on...
)

SET_PROPERTY( TARGET liblapack PROPERTY FOLDER "Externals")

SET(LAPACK_INCLUDE_DIRS ${LAPACK_INSTALL_DIR}/include)

# include for CMAKE_INSTALL_LIBDIR
INCLUDE(GNUInstallDirs)

SET(LAPACK_LIBRARY ${LAPACK_INSTALL_DIR}/${CMAKE_INSTALL_LIBDIR}/${CMAKE_SHARED_LIBRARY_PREFIX}lapack${CMAKE_SHARED_LIBRARY_SUFFIX} )

SET(LAPACK_LIBRARIES ${LAPACK_LIBRARY} )
SET(LAPACK_DEPENDS liblapack)
SET(LAPACK_USE_INTERNAL TRUE)

SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
              LINK_DIRECTORIES ${LAPACK_INSTALL_DIR}/${CMAKE_INSTALL_LIBDIR} )

LIST(APPEND SANS_EXTERNAL_DEPENDS ${LAPACK_DEPENDS})
