INCLUDE(ExternalProject)

SET( TETGEN_VERSION 1.5.0 )

SET( TETGEN_URL ${CMAKE_SOURCE_DIR}/external/tetgen${TETGEN_VERSION}.tar.gz )
MESSAGE(STATUS "Checking for ${TETGEN_URL}")
IF( NOT EXISTS ${TETGEN_URL} )
  SET( TETGEN_URL http://www.tetgen.org/1.5/src/tetgen${TETGEN_VERSION}.tar.gz )
  MESSAGE(STATUS "Downloading and compiling TetGen from ${TETGEN_URL}")
ELSE()
  MESSAGE(STATUS "Using ${TETGEN_URL}")
ENDIF()

IF( "${TETGEN_VERSION}" STREQUAL "1.5.0" )
  SET( TETGEN_MD5 3b9fd9cdec121e52527b0308f7aad5c1 )
ELSEIF( "${TETGEN_VERSION}" STREQUAL "1.5.1" )
  SET( TETGEN_MD5 eb56a8523f5f6a68f9fdecb98c5499df )
ELSE()
  MESSAGE(ERROR "Please add the TetGen ${TETGEN_VERSION} hash to BuildTETGEN.cmake" )
ENDIF()

SET( GENERATOR "Unix Makefiles" )
IF ( MSYS )
  SET( GENERATOR "MSYS Makefiles" )
ENDIF()

ExternalProject_Add(
  libtetgen
  DOWNLOAD_DIR ${CMAKE_SOURCE_DIR}/external/
  URL ${TETGEN_URL}
  CMAKE_GENERATOR ${GENERATOR}
  CMAKE_ARGS -Wno-dev -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} -DCMAKE_CXX_FLAGS:STRING=-fPIC\ -w
  BUILD_COMMAND $(MAKE) tet
  URL_MD5 ${TETGEN_MD5}
  INSTALL_COMMAND ""
)

# Specifying the local compiler does not work with intel... sigh...
#             -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER}
#             -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER}

SET_PROPERTY( TARGET libtetgen PROPERTY FOLDER "Externals")

SET(TETGEN_INCLUDE_DIRS ${CMAKE_BINARY_DIR}/libtetgen-prefix/src/libtetgen)
SET(TETGEN_LIBRARIES ${CMAKE_BINARY_DIR}/libtetgen-prefix/src/libtetgen-build/libtet.a )
SET(TETGEN_DEPENDS libtetgen)
SET(TETGEN_USE_INTERNAL TRUE)

LIST(APPEND SANS_EXTERNAL_DEPENDS ${TETGEN_DEPENDS})
