INCLUDE(ExternalProject)

SET( TRIANGLE_URL ${CMAKE_SOURCE_DIR}/external/triangle.zip )
MESSAGE(STATUS "Checking for ${TRIANGLE_URL}")
IF( NOT EXISTS ${TRIANGLE_URL} )
  SET( TRIANGLE_URL http://www.netlib.org/voronoi/triangle.zip )
  MESSAGE(STATUS "Downloading and compiling Triangle from ${TRIANGLE_URL}")
ELSE()
  MESSAGE(STATUS "Using ${TRIANGLE_URL}")
ENDIF()
  
IF ( CYGWIN OR MINGW OR APPLE )
  SET( CSWITCHES -fPIC\ -O\ -w\ -I/usr/X11R6/include\ -L/usr/X11R6/lib )
ELSEIF( UNIX )
  SET( CSWITCHES -fPIC\ -O\ -DLINUX\ -w\ -I/usr/X11R6/include\ -L/usr/X11R6/lib )
ELSE()
  MESSAGE( FATAL_ERROR "Unkown operating system" )
ENDIF()

SET(TRIANGLE_INCLUDE_DIRS ${CMAKE_BINARY_DIR}/libtriangle-prefix/src/libtriangle )
SET(TRIANGLE_LIBRARIES ${CMAKE_BINARY_DIR}/libtriangle-prefix/src/libtriangle/libtriangle.a )
  
ExternalProject_Add(
  libtriangle
  DOWNLOAD_DIR ${CMAKE_SOURCE_DIR}/external/
  URL ${TRIANGLE_URL}
  URL_MD5 10aff8d7950f5e0e2fb6dd2e340be2c9
  CONFIGURE_COMMAND ""
  BUILD_IN_SOURCE 1
  BUILD_COMMAND $(MAKE) CC=${CMAKE_C_COMPILER}\ -DVOID=void\ -DNO_TIMER CSWITCHES=${CSWITCHES} triangle.o
  INSTALL_COMMAND ${CMAKE_AR} rcs ${TRIANGLE_LIBRARIES} ${CMAKE_BINARY_DIR}/libtriangle-prefix/src/libtriangle/triangle.o
)

SET_PROPERTY( TARGET libtriangle PROPERTY FOLDER "Externals")

SET(TRIANGLE_DEPENDS libtriangle)
SET(TRIANGLE_USE_INTERNAL TRUE)

LIST(APPEND SANS_EXTERNAL_DEPENDS ${TRIANGLE_DEPENDS})
