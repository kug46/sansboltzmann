INCLUDE(ExternalProject)

#
# http://www.cs.sandia.gov/Zoltan/Zoltan_download.html
#
# http://www.cs.sandia.gov/~kddevin/Zoltan_Distributions/zoltan_distrib_v3.83.tar.gz

SET( ZOLTAN_VERSION 3.83 )

SET( MD5_3.83 1ff1bc93f91e12f2c533ddb01f2c095f )

SET( ZOLTAN_URL ${CMAKE_SOURCE_DIR}/external/zoltan_distrib_v${ZOLTAN_VERSION}.tar.gz )
MESSAGE(STATUS "Checking for ${ZOLTAN_URL}")
IF( NOT EXISTS ${ZOLTAN_URL} )
  SET( ZOLTAN_URL http://www.cs.sandia.gov/~kddevin/Zoltan_Distributions/zoltan_distrib_v${ZOLTAN_VERSION}.tar.gz )
  MESSAGE(STATUS "Downloading and compiling Zoltan from ${ZOLTAN_URL}")
ELSE()
  MESSAGE(STATUS "Using ${ZOLTAN_URL}")
ENDIF()

SET( ZOLTAN_SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/Zoltan_v${ZOLTAN_VERSION} )
SET( ZOLTAN_INSTALL_DIR ${CMAKE_BINARY_DIR}/libzoltan-prefix/install)

IF( USE_MPI )
  STRING (REPLACE ";" " " ZOLTAN_MPI_LIBRARIES "${MPI_LIBRARIES}")
  #zoltan can only take 1 include directory, so find the one with mpi.h
  FOREACH( MPI_INCLUDE ${MPI_C_INCLUDE_PATH})
    FIND_PATH( TMP_PATH mpi.h PATHS ${MPI_INCLUDE} NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH NO_CMAKE_PATH
                                                   NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH NO_CMAKE_FIND_ROOT_PATH )
    IF( TMP_PATH ) 
      SET( ZOLTAN_MPI_INCLUDE_DIR ${TMP_PATH} )
    ENDIF()
  ENDFOREACH()
  UNSET( TMP_PATH CACHE )
  SET( WITH_MPI --enable-mpi --with-mpi-compilers=no --with-mpi-incdir=${ZOLTAN_MPI_INCLUDE_DIR} --with-mpi-libs=${ZOLTAN_MPI_LIBRARIES} )
ElSE()
  SET( WITH_MPI )
ENDIF()

ExternalProject_Add(
  libzoltan
  DOWNLOAD_DIR ${CMAKE_SOURCE_DIR}/external/
  URL ${ZOLTAN_URL}
  URL_MD5 ${MD5_${ZOLTAN_VERSION}}
  SOURCE_DIR ${ZOLTAN_SOURCE_DIR}
  INSTALL_DIR ${ZOLTAN_INSTALL_DIR}
  CONFIGURE_COMMAND ${ZOLTAN_SOURCE_DIR}/configure ${WITH_MPI} --prefix=${ZOLTAN_INSTALL_DIR} CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER} CFLAGS=-fPIC CXXFLAGS=-fPIC
  BUILD_IN_SOURCE 0
  BUILD_COMMAND $(MAKE)
  INSTALL_COMMAND $(MAKE) install
)

SET_PROPERTY( TARGET libzoltan PROPERTY FOLDER "Externals")

SET(ZOLTAN_INCLUDE_DIRS ${ZOLTAN_INSTALL_DIR}/include )
SET(ZOLTAN_LIBRARIES ${ZOLTAN_INSTALL_DIR}/lib/libzoltan.a )
SET(ZOLTAN_DEPENDS libzoltan)
SET(ZOLTAN_USE_INTERNAL TRUE)

LIST(APPEND SANS_EXTERNAL_DEPENDS ${ZOLTAN_DEPENDS})