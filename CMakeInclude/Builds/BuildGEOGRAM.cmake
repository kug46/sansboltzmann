INCLUDE(ExternalProject)

# set the desired version of geogram (this is the latest )
SET( GEOGRAM_DIR ${CMAKE_SOURCE_DIR}/external/geogram_1.3.5 )

INCLUDE_DIRECTORIES( ${GEOGRAM_DIR}/src/lib/ )

# set the platform similar to the configure script for geogram (we might not catch all of them for now)
IF (CMAKE_SYSTEM_NAME MATCHES "Linux")
  # check if this is intel
  IF (CMAKE_CXX_COMPILER_ID MATCHES "Intel")
    SET(VORPA_PLAT "Linux64-icc")
  ELSEIF (CMAKE_SYSTEM_PROCESSOR MATCHES "x86_64")
    SET(VORPA_PLAT "Linux64-gcc")
  ELSEIF (CMAKE_SYSTEM_PROCESSOR MATCHES "amd64")
    SET(VORPA_PLAT "Linux64-gcc")
  ELSEIF (CMAKE_SYSTEM_PROCESSOR MATCHES "i586")
    SET(VORPA_PLAT "Linux32-gcc")
  ELSEIF (CMAKE_SYSTEM_PROCESSOR MATCHES "i686")
    SET(VORPA_PLAT "Linux32-gcc")
  ENDIF()
ELSEIF (CMAKE_SYSTEM_NAME MATCHES "Darwin")
  SET(VORPA_PLAT "Darwin-clang")
ELSEIF (CMAKE_SYSTEM_NAME MATCHES "CYGWIN")
  SET(VORPA_PLAT "Linux64-gcc")
ELSE()
  MESSAGE(FATAL_ERROR "unknown platform for geogram")
ENDIF()

IF( CMAKE_BUILD_TYPE MATCHES "Debug" )
  SET( GOEGRAM_ARGS -DCMAKE_BUILD_TYPE=Debug )
ENDIF()

IF( CMAKE_COMPILER_IS_GNUCXX )
  SET( GOEGRAM_ARGS ${GOEGRAM_ARGS} -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER} -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER})
ENDIF()

SET( BUILD_SUFFIX )
IF( CMAKE_GENERATOR MATCHES "Xcode" )
  SET( BUILD_SUFFIX ${CMAKE_BUILD_TYPE}/ )
ENDIF()

IF( CMAKE_VERSION VERSION_GREATER 3.1 )
  # this basically does the same thing as the configure script in geogram
  ExternalProject_Add(
    libgeogram
    SOURCE_DIR ${GEOGRAM_DIR}
    CMAKE_ARGS -DVORPALINE_PLATFORM=${VORPA_PLAT} ${GOEGRAM_ARGS}
    BUILD_ALWAYS 1
    INSTALL_COMMAND ""
  )
ELSE()
  # this basically does the same thing as the configure script in geogram
  ExternalProject_Add(
    libgeogram
    SOURCE_DIR ${GEOGRAM_DIR}
    CMAKE_ARGS -DVORPALINE_PLATFORM=${VORPA_PLAT} ${GOEGRAM_ARGS}
    INSTALL_COMMAND ""
  )

  # BUILD_ALWAYS is not available, so make a separate step to force rebuild
  ExternalProject_Add_Step(libgeogram rebuild
    COMMAND "$(MAKE)"
    COMMENT "Building 'libgeogram'"
    DEPENDEES build
    WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/libgeogram-prefix/src/libgeogram-build/
    ALWAYS 1 
    )
ENDIF()

SET_PROPERTY( TARGET libgeogram PROPERTY FOLDER "Externals")

SET(GEOGRAM_INCLUDE_DIRS ${GEOGRAM_DIR}/src/lib )
#SET(GEOGRAM_LIBRARIES ${CMAKE_BINARY_DIR}/libgeogram-prefix/src/libgeogram-build/lib/${CMAKE_SHARED_LIBRARY_PREFIX}geogram${CMAKE_SHARED_LIBRARY_SUFFIX} )
SET(GEOGRAM_LIBRARIES ${CMAKE_BINARY_DIR}/libgeogram-prefix/src/libgeogram-build/lib/${BUILD_SUFFIX}libgeogram.a )
SET(GEOGRAM_DEPENDS libgeogram)

SET(GEOGRAM_USE_INTERNAL ON)

# This resolves __stack_chk_fail link errors on cygwin
IF (CYGWIN)
  SET( GEOGRAM_LIBRARIES ${GEOGRAM_LIBRARIES} ssp )
ENDIF()

LIST(APPEND SANS_EXTERNAL_DEPENDS ${GEOGRAM_DEPENDS})