INCLUDE(ExternalProject)

#------------------------------------------------------------------------------

# Approved versions of PETSc
SET( PETSC_MD5_3.7.6 152c1f15f535af22fc015de1d3099db3 )
SET( PETSC_MD5_3.8.2 be81a1416390207c5506856a61065000 )
SET( PETSC_MD5_3.8.3 b21d15cdc033d50cc47b9997ef490fef )
SET( PETSC_MD5_3.8.4 dbc724ec61e12258c4c7f8b52f026bb6 )
# KSPSolveTranspose does not work in 3.9.[0-3]
# SET( PETSC_MD5_3.9.0 ---- )
# SET( PETSC_MD5_3.9.1 ---- )
# SET( PETSC_MD5_3.9.2 ---- )
# SET( PETSC_MD5_3.9.3 ---- )
SET( PETSC_MD5_3.10.0 7859b77d6069d066f59531ec00eef67e )
SET( PETSC_MD5_3.10.1 c93e8420f000bb0aa6a96e2c76a9f238 )
SET( PETSC_MD5_3.10.2 c13c67999957ff617123668a1de5c361 )
SET( PETSC_MD5_3.10.3 661455bd5dd848f060800d3d575286cc )
SET( PETSC_MD5_3.10.4 06205518845df58771d579eab4e4e01b )
SET( PETSC_MD5_3.10.5 928b079d2dfbe33e7b7f4e62824c0b12 )
SET( PETSC_MD5_3.11.0 574993f9c1e88d70e9a6d1e6f7eac328 )
SET( PETSC_MD5_3.11.1 4258da3c7968673d433e2bf8fec69d73 )
SET( PETSC_MD5_3.11.2 ccd4f1e07edc453a74b52fd9a0c52a1c )
SET( PETSC_MD5_3.11.3 e04cbb1aaccde4a33c8843e75a1fba15 )
SET( PETSC_MD5_3.11.4 33da4b6a430d5d9e13b19871d707af0f )
SET( PETSC_MD5_3.12.0 997dc9475c634891ba63fefa56ac3621 )
SET( PETSC_MD5_3.12.1 87616bbdc8549239b102db371df3b038 )
SET( PETSC_MD5_3.12.5 f717968f13edab4c895720c189bda343 )
SET( PETSC_MD5_3.13.0 8ea59cc9e329611d73eb1be56cc32fda )

SET( PETSC_LATEST 3.13.0 )

SET( PETSC_VERNUM "" CACHE STRING "Build specific version of PETSc, e.g. ${PETSC_LATEST}")

IF( PETSC_VERNUM STREQUAL "" )
  # No version specified, look for the latest in external by default
  SET( PETSC_VER ${PETSC_LATEST} )
ELSE()
  IF( NOT DEFINED PETSC_MD5_${PETSC_VERNUM} )
    MESSAGE( "=======================" )
    MESSAGE( "" )
    MESSAGE( "Build for PETSc Version 'PETSC_VERNUM=${PETSC_VERNUM}' is not approved." )
    MESSAGE( "See CMakeInclude/Build/BuildPETSC.cmake for approved versions." )
    MESSAGE( "" )
    MESSAGE( "=======================" )
    MESSAGE( FATAL_ERROR "" )
  ENDIF()

  # Use the specified version of PETSc
  SET( PETSC_VER ${PETSC_VERNUM} )
ENDIF()

#------------------------------------------------------------------------------

# Check to see if a tar file is available
SET( PETSC_URL ${CMAKE_SOURCE_DIR}/external/petsc-lite-${PETSC_VER}.tar.gz )  
MESSAGE(STATUS "Checking for ${PETSC_URL}")

IF( EXISTS ${PETSC_URL} OR NOT PETSC_VERNUM STREQUAL "" )
  # Use a specific version of PETSc
  IF( NOT EXISTS ${PETSC_URL} )
    SET( PETSC_URL http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-${PETSC_VER}.tar.gz )
    MESSAGE(STATUS "Downloading and compiling PETSc from ${PETSC_URL}")
  ELSE()
    MESSAGE(STATUS "Using ${PETSC_URL}")
  ENDIF()
    
  SET( PETSC_SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/petsc-${PETSC_VER} )
  SET( PETSC_INSTALL_DIR ${CMAKE_BINARY_DIR}/libPETSc-prefix/petsc-${PETSC_VER} )
  
  SET( PETSC_REPO URL ${PETSC_URL} URL_MD5 ${PETSC_MD5_${PETSC_VER}} DOWNLOAD_DIR ${CMAKE_SOURCE_DIR}/external )
ELSE()
  # Use the latest version of PETSc via the repo
  SET( PETSC_URL https://gitlab.com/petsc/petsc )  
  MESSAGE(STATUS "Downloading and compiling PETSc from ${PETSC_URL}")
    
  SET( PETSC_SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/petsc )
  SET( PETSC_INSTALL_DIR ${CMAKE_BINARY_DIR}/libPETSc-prefix/petsc )
  
  SET( PETSC_REPO GIT_REPOSITORY ${PETSC_URL} GIT_TAG maint DOWNLOAD_DIR ${PETSC_SOURCE_DIR} )
ENDIF()

#------------------------------------------------------------------------------

IF ( MKL_BLACS_LIBRARY )
  SET( WITH_MKL ) #--with-blaslapack-dir=$ENV{MKLROOT} --with-mkl_cpardiso=1 --with-mkl_cpardiso-dir=$ENV{MKLROOT})
ELSE()
  SET( WITN_MKL )
ENDIF()

#------------------------------------------------------------------------------

GET_FILENAME_COMPONENT( BIN_DIR ${CMAKE_BINARY_DIR} NAME_WE )

SET( PETSC_DIR ${PETSC_SOURCE_DIR} )
SET( PETSC_ARCH ${BIN_DIR}-${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR} )

IF( USE_MPI )
  STRING (REPLACE ";" "," PETSC_MPI_LIBRARIES "[${MPI_LIBRARIES}]")
  #PETSc can only take 1 include directory, so find the one with mpi.h
  FOREACH( MPI_INCLUDE ${MPI_C_INCLUDE_PATH})
    FIND_PATH( TMP_PATH mpi.h PATHS ${MPI_INCLUDE} NO_DEFAULT_PATH NO_CMAKE_ENVIRONMENT_PATH NO_CMAKE_PATH
                                                   NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH NO_CMAKE_FIND_ROOT_PATH )
    IF( TMP_PATH ) 
      SET( PETSC_MPI_INCLUDE_DIR ${TMP_PATH} )
    ENDIF()
  ENDFOREACH()
  UNSET( TMP_PATH CACHE )
  SET( WITH_MPI --with-mpi=1 --with-mpi-include=${PETSC_MPI_INCLUDE_DIR} --with-mpi-lib=${PETSC_MPI_LIBRARIES} )
ElSE()
  SET( WITH_MPI --with-mpi=0 )
ENDIF()

STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
IF( BUILD_TYPE MATCHES "DEBUG" )
  SET( PETSC_DEBUG --enable-debug=1 "--COPTFLAGS=-g -w" "--CXXOPTFLAGS=-g -w" )
ELSEIF( BUILD_TYPE MATCHES "MEMCHECK" )
  SET( PETSC_DEBUG --enable-debug=1 
                   "--COPTFLAGS=${CMAKE_C_FLAGS_MEMCHECK} ${CMAKE_EXE_LINKER_FLAGS_MEMCHECK} -w" 
                   "--CXXOPTFLAGS=${CMAKE_CXX_FLAGS_MEMCHECK} ${CMAKE_EXE_LINKER_FLAGS_MEMCHECK} -w" 
                   "--CC_LINKER_FLAGS=${CMAKE_EXE_LINKER_FLAGS_MEMCHECK}"
                   "--CXX_LINKER_FLAGS=${CMAKE_EXE_LINKER_FLAGS_MEMCHECK}")
ELSE()
  SET( PETSC_DEBUG --enable-debug=0 --with-debugging=0  "--COPTFLAGS=-O3 -w" "--CXXOPTFLAGS=-O3 -w" )
ENDIF()

ExternalProject_Add(
  libPETSc
  ${PETSC_REPO}
  UPDATE_COMMAND ""
  SOURCE_DIR ${PETSC_SOURCE_DIR}
  CONFIGURE_COMMAND ${PETSC_SOURCE_DIR}/configure --download-f2cblaslapack=1 --with-cc=${CMAKE_C_COMPILER} --with-fc=0 --with-cxx=${CMAKE_CXX_COMPILER} ${WITH_MPI} ${WITH_MKL} ${PETSC_DEBUG} --prefix=${PETSC_INSTALL_DIR} --PETSC_DIR=${PETSC_DIR} --PETSC_ARCH=${PETSC_ARCH}
  BUILD_IN_SOURCE 1
  BUILD_COMMAND $(MAKE) PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${PETSC_ARCH} all
  INSTALL_COMMAND $(MAKE) PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${PETSC_ARCH} install
  )

SET_PROPERTY( TARGET libPETSc PROPERTY FOLDER "Externals")

SET(PETSC_USE_INTERNAL TRUE)
SET(PETSC_INCLUDE_DIRS ${PETSC_INSTALL_DIR}/include )
IF( NOT USE_MPI )
  LIST( APPEND PETSC_INCLUDE_DIRS ${PETSC_INSTALL_DIR}/include/petsc/mpiuni/ )
ENDIF()
SET(PETSC_LIBRARIES ${PETSC_INSTALL_DIR}/lib/libpetsc${CMAKE_SHARED_LIBRARY_SUFFIX} )
SET(PETSC_DEPENDS libPETSc)

SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
              LINK_DIRECTORIES ${PETSC_INSTALL_DIR}/lib )

LIST(APPEND SANS_EXTERNAL_DEPENDS ${PETSC_DEPENDS})
