
# CMake comes with a builtin add_definitions command to add definitions to
# everything in the current directory, but we need to add definitions to
# targets.
FUNCTION(ADD_TARGET_DEFINITIONS target)
  GET_TARGET_PROPERTY(defs ${target} COMPILE_DEFINITIONS)
  IF (defs MATCHES "NOTFOUND")
    SET(defs "")
  ENDIF ()
  FOREACH (def ${defs} ${ARGN})
    LIST(APPEND deflist ${def})
  ENDFOREACH ()
  SET_TARGET_PROPERTIES(${target} PROPERTIES COMPILE_DEFINITIONS "${deflist}")
ENDFUNCTION()