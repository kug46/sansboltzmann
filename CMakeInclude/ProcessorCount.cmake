
##======================================================================
##
## Automatically define -j for builds with make
##
##======================================================================
# Use PROCESSOR_COUNT in SET(CTEST_BUILD_FLAGS "-j ${PROCESSOR_COUNT}")
IF(NOT DEFINED PROCESSOR_COUNT)
  # Unknown:
  set(PROCESSOR_COUNT 0)
  # Linux:
  set(cpuinfo_file "/proc/cpuinfo")
  if(EXISTS "${cpuinfo_file}")
    file(STRINGS "${cpuinfo_file}" procs REGEX "^processor.: [0-9]+$")
    list(LENGTH procs PROCESSOR_COUNT)
  endif()
  # Mac:
  if(APPLE)
    # sysctl hw.physicalcpu gives the number of physical cores if the machine is hyperthreaded
    execute_process(COMMAND "sysctl" "hw.ncpu" OUTPUT_VARIABLE info)
    string(REGEX REPLACE "^hw.ncpu: ([0-9]+).*$" "\\1"  PROCESSOR_COUNT "${info}")
  endif()
  # Windows:
  if(WIN32)
    set(PROCESSOR_COUNT "$ENV{NUMBER_OF_PROCESSORS}")
  endif()
ENDIF()
