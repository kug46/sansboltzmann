 #==================================================
# Remove old log file. Otherwise errors are just appeneded
#==================================================
IF( EXISTS ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeError.log )
  FILE( REMOVE ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMakeError.log )
ENDIF()

# PkgConfig is used by many Finds/*.cmake
FIND_PACKAGE(PkgConfig)

#==================================================
# Define a macro to look for a package and install a local copy if we can't find it
#==================================================
MACRO (GetDependency name)
    FIND_PACKAGE(${name})
    IF(${name}_FOUND)
      # check to make sure the library can be linked to
      INCLUDE(Check${name})

      IF(${name}_TEST_FAIL)
        SET(USE_INTERNAL_${name} 1)
      ELSE()
        SET(USE_INTERNAL_${name} 0) 
      ENDIF()

    ELSE()
      SET(USE_INTERNAL_${name} 1) 
    ENDIF()
    
    IF(USE_INTERNAL_${name})
      INCLUDE(Build${name})
    ENDIF()
    
ENDMACRO(GetDependency)

#==================================================
# MPI
#==================================================

# Look for MPI first as others depend on it, but MPI libraries 
# must be added to SANS_EXTERNAL_LIBS after boost-mpi, parmetis, and PETSc
IF( NOT DEFINED USE_MPI OR ( DEFINED USE_MPI AND USE_MPI ) )
  FIND_PACKAGE(MPI)
ENDIF()
OPTION( USE_MPI "Compile parallel with MPI" ${MPI_FOUND} )
IF(USE_MPI AND MPI_FOUND )
  FIND_PACKAGE_HANDLE_STANDARD_ARGS(MPIEXEC DEFAULT_MSG MPIEXEC)
  SET( MPI_INCLUDE_DIRS ${MPI_C_INCLUDE_PATH} )
  ADD_DEFINITIONS( -DSANS_MPI )
  INCLUDE_DIRECTORIES(SYSTEM ${MPI_INCLUDE_DIRS} )
  
  FOREACH (MPI_LIB ${MPI_LIBRARIES})
    GET_FILENAME_COMPONENT( MPI_LIBRARY_DIR ${MPI_LIB} PATH )
    SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
                  LINK_DIRECTORIES ${MPI_LIBRARY_DIR} )
  ENDFOREACH ()
ENDIF()

#==================================================
# ParMETIS
#==================================================
IF( USE_MPI )
  GetDependency( PARMETIS )
  INCLUDE_DIRECTORIES(SYSTEM ${PARMETIS_INCLUDE_DIRS})
  LIST(APPEND SANS_EXTERNAL_LIBS ${PARMETIS_LIBRARIES})
ENDIF()

#==================================================
# Intel(R) MKL libraries
#==================================================
IF( ${CMAKE_CXX_COMPILER_ID} STREQUAL "Intel" )
  SET( MKL ON )
ELSE()
  SET( MKL OFF )
ENDIF()
OPTION( USE_MKL "Enable use of Intel(R) MKL libraries" ${MKL} )
IF( USE_MKL )
  INCLUDE( FindMKL )
  INCLUDE( CheckMKL )
  
  SET( BLAS_INCLUDE_DIRS ${MKL_INCLUDE_DIRS} )
  SET( BLAS_LIBRARIES ${MKL_LIBRARIES})
  SET( BLAS_DEFINITIONS -DINTEL_MKL -DDLA_BLAS_MKL )
  INCLUDE( CheckCBLAS )
  
  ADD_DEFINITIONS( ${BLAS_DEFINITIONS} )
  INCLUDE_DIRECTORIES(SYSTEM ${MKL_INCLUDE_DIRS})
ENDIF()

#==================================================
# PETSc (uses MKL)
#==================================================
FIND_PACKAGE(PETSC)

IF(PETSC_FOUND)
  INCLUDE(CheckPETSC)
ENDIF()

IF( PETSC_FOUND AND NOT PETSC_TEST_FAIL OR USE_MPI )
  SET( PETSC_INIT ON )
ELSE()
  SET( PETSC_INIT OFF )
ENDIF()

OPTION( USE_PETSC "Enable use of PETSC" ${PETSC_INIT} )
IF( USE_PETSC )
  IF( NOT PETSC_FOUND OR PETSC_TEST_FAIL )
    INCLUDE(BuildPETSC)
  ENDIF()
  
  ADD_DEFINITIONS( -DSANS_PETSC )
  INCLUDE_DIRECTORIES(SYSTEM ${PETSC_INCLUDE_DIRS})
  LIST(APPEND SANS_EXTERNAL_LIBS ${PETSC_LIBRARIES})
ENDIF()

#==================================================
# Python Libraries
#==================================================
#SET( Python_ADDITIONAL_VERSIONS 2.7 )
FIND_PACKAGE (PythonInterp )
FIND_PACKAGE( PythonLibs REQUIRED )

INCLUDE_DIRECTORIES( SYSTEM ${PYTHON_INCLUDE_DIRS} )
GET_FILENAME_COMPONENT( PYTHON_LIBRARY_DIR ${PYTHON_LIBRARY} PATH )
SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
              LINK_DIRECTORIES ${PYTHON_LIBRARY_DIR} )

OPTION( UI_PYTHON "Compile Python user interaface" ON )
IF( UI_PYTHON )
  FIND_PACKAGE( NumPy )
ENDIF()

#==================================================
# Boost Libraries
#==================================================
SET( Boost_ADDITIONAL_VERSIONS "1.50" "1.50.0" "1.51" "1.51.0" "1.52" "1.52.0" "1.54" "1.54.0" "1.55" "1.55.0" )
#SET( Boost_USE_MULTITHREADED  ON )
#IF( APPLE )
#  SET( Boost_USE_STATIC_LIBS ON )
#ENDIF()
SET( BOOST_COMPONENTS unit_test_framework iostreams filesystem system regex )
IF( USE_MPI )
  LIST(APPEND BOOST_COMPONENTS mpi serialization )
ENDIF()

SET( Boost_NO_BOOST_CMAKE ON )

FIND_PACKAGE( Boost 1.50 COMPONENTS ${BOOST_COMPONENTS} )

IF (Boost_FOUND)

  IF( DEFINED Boost_LIBRARY_DIR )
    SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY LINK_DIRECTORIES ${Boost_LIBRARY_DIR} )
  ELSEIF( DEFINED Boost_LIBRARY_DIR_RELEASE )
    SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY LINK_DIRECTORIES ${Boost_LIBRARY_DIR_RELEASE} )
  ELSEIF( DEFINED Boost_LIBRARY_DIR_DEBUG )
    SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY LINK_DIRECTORIES ${Boost_LIBRARY_DIR_DEBUG} )
  ENDIF()

  # Boost 1.67 started adding a suffix to the python library, but older versions don't have it
  IF ( ${Boost_VERSION} GREATER 106700 OR ${Boost_VERSION} EQUAL 106700 )
    SET( BOOST_PYTHON_SUFFIX ${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR} )
  ENDIF()

  LIST(APPEND BOOST_COMPONENTS python${BOOST_PYTHON_SUFFIX} )

  # Boost numpy was introduced in 1.65
  IF (UI_PYTHON AND (${Boost_VERSION} GREATER 106500 OR ${Boost_VERSION} EQUAL 106500) )
    LIST(APPEND BOOST_COMPONENTS numpy${BOOST_PYTHON_SUFFIX} )
  ENDIF()

  FIND_PACKAGE( Boost COMPONENTS ${BOOST_COMPONENTS} )

  IF (NOT Boost_FOUND)
    LIST(REMOVE_ITEM BOOST_COMPONENTS python${BOOST_PYTHON_SUFFIX} )
    IF (UI_PYTHON AND (${Boost_VERSION} GREATER 106500 OR ${Boost_VERSION} EQUAL 106500) )
      LIST(REMOVE_ITEM BOOST_COMPONENTS numpy${BOOST_PYTHON_SUFFIX} )
    ENDIF()
    SET( BOOST_PYTHON_SUFFIX ${PYTHON_VERSION_MAJOR} )

    #Try just the major python version as the suffix
    LIST(APPEND BOOST_COMPONENTS python${BOOST_PYTHON_SUFFIX} )
    IF (UI_PYTHON AND (${Boost_VERSION} GREATER 106500 OR ${Boost_VERSION} EQUAL 106500) )
      LIST(APPEND BOOST_COMPONENTS numpy${BOOST_PYTHON_SUFFIX} )
    ENDIF()
    FIND_PACKAGE( Boost COMPONENTS ${BOOST_COMPONENTS} )
  ENDIF()

  IF (Boost_FOUND)
    # Test the boost pyton works with the python that was found
    INCLUDE(CheckBoostPYTHON)
  ELSE()
    INCLUDE(BuildBoost)
  ENDIF()
ELSE()
  INCLUDE(BuildBoost)
ENDIF()

INCLUDE_DIRECTORIES(SYSTEM ${Boost_INCLUDE_DIR} )
LIST(APPEND SANS_EXTERNAL_LIBS ${Boost_LIBRARIES})

# Add python library after boost python library
LIST(APPEND SANS_EXTERNAL_LIBS ${PYTHON_LIBRARY})

#==================================================
# NLOPT Libraries
#==================================================
GetDependency( NLOPT )

INCLUDE_DIRECTORIES(SYSTEM ${NLOPT_INCLUDE_DIRS} )
LIST(APPEND SANS_EXTERNAL_LIBS ${NLOPT_LIBRARIES})

#==================================================
# GLPK Libraries
#==================================================
FIND_PACKAGE( GLPK )
OPTION( USE_GLPK "Enable use of GNU Linear Programming Kit: https://www.gnu.org/software/glpk/" ${GLPK_FOUND} )
IF( USE_GLPK AND GLPK_FOUND )
  ADD_DEFINITIONS( -DSANS_GLPK )
  INCLUDE_DIRECTORIES(SYSTEM ${GLPK_INCLUDE_DIRS} )
  LIST(APPEND SANS_EXTERNAL_LIBS ${GLPK_LIBRARIES})
ENDIF()

#==================================================
# EGADS
#==================================================
OPTION( UI_CAPSAIM "Compile CAPS AIM interaface" OFF )
SET( USE_CAPS ${UI_CAPSAIM})
OPTION( UI_OPENCSM_UDF "Compile OpenCSM udf functions" OFF )

GetDependency( EGADS )

INCLUDE_DIRECTORIES( ${EGADS_INCLUDE_DIRS} )
#EGADS libraries added to SANS_EXTERNAL_LIBS after refine

IF( UI_CAPSAIM )
  GetDependency( CAPS )
ENDIF()

#==================================================
# NASA Refine mesh adaptation
#==================================================
IF ( NOT DEFINED USE_REFINE )
  INCLUDE( FindREFINE )
  SET( REFINE_INIT OFF )
  IF(REFINE_FOUND)
    INCLUDE( CheckREFINE )
    IF( NOT REFINE_TEST_FAIL )
      SET( REFINE_INIT ON )
    ENDIF()
  ENDIF()
  OPTION( USE_REFINE "Enable use of NASA refine mesher" ${REFINE_INIT} )
ELSEIF ( USE_REFINE )
  INCLUDE( FindREFINE )
  IF(REFINE_FOUND)
    INCLUDE( CheckREFINE )
  ENDIF()
ENDIF()

IF( USE_REFINE )
  IF( NOT REFINE_FOUND OR REFINE_TEST_FAIL )
    INCLUDE( BuildREFINE )
  ENDIF()
  ADD_DEFINITIONS( -DSANS_REFINE )
  # Only include refine in src/Meshing/refine
  #INCLUDE_DIRECTORIES(${REFINE_INCLUDE_DIRS} )
  LIST(APPEND SANS_EXTERNAL_LIBS ${REFINE_LIBRARIES} )
ENDIF()

# refine depends on EGADS, so EGADS must be added after refine
LIST(APPEND SANS_EXTERNAL_LIBS ${EGADS_LIBRARIES})

#==================================================
# BLAS Libraries
#==================================================

SET(CMAKE_REQUIRED_LIBRARIES "")
SET(CMAKE_REQUIRED_INCLUDES "")
SET(CMAKE_REQUIRED_FLAGS "")

# Suitesparse depends on BLAS, so it's not optional until SuiteSparse is optional.

#OPTION( USE_BLAS "Enable use of BLAS routines" ON )
#IF( USE_LAPACK ) 
#  SET( USE_BLAS ON ) #LAPACK requires BLAS
#ENDIF()

# Additional BLAS library search locations can be added via LD_LIBRARY_PATH
IF( NOT USE_MKL )
  FIND_PACKAGE( BLAS REQUIRED )
  IF( BLAS_FOUND )
    IF( BLAS_atlas_LIBRARY )
      MESSAGE( STATUS "Found ATLAS BLAS" )
      SET( CMAKE_EXE_LINKER_FLAGS
           "${CMAKE_EXE_LINKER_FLAGS} ${BLAS_LINKER_FLAGS}"
           CACHE STRING "Linker flags for executables" FORCE )
      GET_FILENAME_COMPONENT( ATLAS_ROOT ${BLAS_atlas_LIBRARY} PATH ) #This gives /path/to/ATLAS/lib
      GET_FILENAME_COMPONENT( ATLAS_ROOT ${ATLAS_ROOT} PATH ) #This gives /path/to/ATLAS/
      FIND_PATH(BLAS_INCLUDE_DIRS cblas.h HINTS ${ATLAS_ROOT}/include $ENV{BLAS_INCLUDE} /usr/include REQUIRED )
      FIND_LIBRARY( BLAS_atlas_cblas_LIBRARY cblas PATHS ${ATLAS_ROOT}/lib )
      MARK_AS_ADVANCED( BLAS_atlas_cblas_LIBRARY )
      SET( BLAS_LIBRARIES ${BLAS_atlas_cblas_LIBRARY} ${BLAS_atlas_LIBRARY} )
      SET( BLAS_DEFINITIONS -DDLA_BLAS_ATLAS )
    ELSEIF( BLAS_Accelerate_LIBRARY )
      MESSAGE( STATUS "Found Accelerate BLAS" )
      SET( BLAS_DEFINITIONS -DDLA_BLAS_ACCELERATE )
      SET( BLAS_INCLUDE_DIR )
    ELSEIF( BLAS_mkl_core_LIBRARY )
      MESSAGE( STATUS "Found Intel MKL BLAS" )
      SET( BLAS_DEFINITIONS -DINTEL_MKL -DDLA_BLAS_MKL )
      SET( BLAS_INCLUDE_DIR )
      FOREACH( BLAS_MKL_LIB ${BLAS_LIBRARIES} )
        GET_FILENAME_COMPONENT( BLAS_MKL_LIB_DIR ${BLAS_MKL_LIB} PATH)
        SET_PROPERTY( DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY 
                      LINK_DIRECTORIES ${BLAS_MKL_LIB_DIR} )
      ENDFOREACH()
    ELSE()
      MESSAGE( STATUS "Found Generic BLAS" )
      FIND_PATH(BLAS_INCLUDE_DIRS cblas.h HINTS $ENV{BLAS_INCLUDE} /usr/include /usr/include/openblas /usr/include/cblas REQUIRED )
      IF ( NOT BLAS_INCLUDE_DIRS )
        MESSAGE( "" )  
        MESSAGE( "=================================" )
        MESSAGE( " Could not find cblas.h. Please set \$BLAS_INCLUDE to the directory containing it." )
        MESSAGE( " SANS will download and compile it for you otherwise." )
        MESSAGE( "=================================" )
        MESSAGE( "" )
        SET( BUILD_CBLAS TRUE )
      ELSE()
        MESSAGE( STATUS "Found cblas.h in ${BLAS_INCLUDE_DIRS}" )
        FIND_LIBRARY( BLAS_cblas_LIBRARY cblas )
	IF(BLAS_cblas_LIBRARY)
          SET( BLAS_LIBRARIES ${BLAS_cblas_LIBRARY} ${BLAS_LIBRARIES} )
          MARK_AS_ADVANCED( BLAS_cblas_LIBRARY )
	  MESSAGE( STATUS "Found CBLAS: ${BLAS_cblas_LIBRARY}" )
	ENDIF()
      ENDIF()
      SET( BLAS_DEFINITIONS -DDLA_BLAS_GENERIC )
    ENDIF()
    ADD_DEFINITIONS( ${BLAS_DEFINITIONS} )
    SET(BLAS_LIBRARIES $ENV{BLAS_LIBRARIES} ${BLAS_LIBRARIES})
    INCLUDE(CheckCBLAS)

    IF( BUILD_CBLAS )
      INCLUDE( BuildCBLAS )
      INCLUDE_DIRECTORIES(SYSTEM ${CBLAS_INCLUDE_DIRS} )
    ELSE()
      SET( BLAS_INCLUDE_DIRS ${BLAS_INCLUDE_DIRS} CACHE PATH "BLAS Include Directory" )
      MARK_AS_ADVANCED( BLAS_INCLUDE_DIRS )
      INCLUDE_DIRECTORIES(SYSTEM ${BLAS_INCLUDE_DIRS} )
    ENDIF()
    
    UNSET( BLAS_INCLUDE_DIR CACHE )
  ENDIF( BLAS_FOUND )
ENDIF()

#==================================================
# SuiteSparse with UMFPACK (requires BLAS from above, but not CBLAS)
#==================================================
GetDependency( SUITESPARSE )

INCLUDE_DIRECTORIES(SYSTEM ${SUITESPARSE_INCLUDE_DIRS} )
LIST(APPEND SANS_EXTERNAL_LIBS ${SUITESPARSE_LIBRARIES} )

# Now add CBLAS in case we are building it
IF( NOT USE_MKL AND BUILD_CBLAS )
  SET(BLAS_LIBRARIES ${CBLAS_LIBRARIES} ${BLAS_LIBRARIES} )
ENDIF()

#Finally add (CBLAS and) BLAS libraries to the list of dependencies 
IF( DEFINED BLAS_LIBRARIES )
  LIST(APPEND SANS_EXTERNAL_LIBS ${BLAS_LIBRARIES})
ENDIF()

#==================================================
# LAPACK
#==================================================
OPTION( USE_LAPACK "Enable use of LAPACK routines" OFF )
IF( NOT APPLE )
  OPTION( USE_LAPACKE "Enable use of LAPACKE routines" OFF )
ELSE()
  SET ( USE_LAPACKE FALSE )
ENDIF()

IF( USE_LAPACK OR USE_LAPACKE)
  GetDependency( LAPACK )
  ADD_DEFINITIONS( -DDLA_LAPACK )
  IF( NOT USE_LAPACKE )
    LIST(APPEND SANS_EXTERNAL_LIBS ${LAPACK_LIBRARIES})
  ENDIF()
ENDIF()

#==================================================
# LAPACKE C-interface for LAPACK (only used for testing)
#==================================================
IF( USE_LAPACKE )
  GetDependency( LAPACKE )
  ADD_DEFINITIONS( -DDLA_LAPACKE )
  INCLUDE_DIRECTORIES(SYSTEM ${LAPACKE_INCLUDE_DIRS} )
  LIST(APPEND SANS_EXTERNAL_LIBS ${LAPACKE_LIBRARIES} ${LAPACK_LIBRARIES})
ENDIF()

#==================================================
# TetGen: http://wias-berlin.de/software/tetgen/
#         TetGen is also provided by AVRO so it must be added here first
#==================================================
OPTION( USE_TETGEN "Enable use of TetGen tetrahedral mesher: http://wias-berlin.de/software/tetgen/" ON )
IF (USE_TETGEN)
  GetDependency( TETGEN )
  INCLUDE_DIRECTORIES(SYSTEM ${TETGEN_INCLUDE_DIRS})
  LIST(APPEND SANS_EXTERNAL_LIBS ${TETGEN_LIBRARIES})
ENDIF()

#==================================================
# avro mesh adaptation
#==================================================
INCLUDE( FindAVRO )
SET( AVRO_INIT OFF )
IF(AVRO_FOUND)
  INCLUDE( CheckAVRO )
  IF( NOT AVRO_TEST_FAIL )
    SET( AVRO_INIT ON )
  ENDIF()
ENDIF()
OPTION( USE_AVRO "Enable use avro grid generator" ${AVRO_INIT} )
IF( USE_AVRO )
  ADD_DEFINITIONS( -DSANS_AVRO )
    IF( NOT AVRO_FOUND OR AVRO_TEST_FAIL )
    INCLUDE( BuildAVRO )
  ENDIF()
  INCLUDE_DIRECTORIES( ${AVRO_INCLUDE_DIRS} )
  LIST( APPEND SANS_EXTERNAL_LIBS ${AVRO_LIBRARIES} )
ELSEIF( USE_AVRO AND (NOT AVRO_FOUND OR AVRO_TEST_FAIL) )
  MESSAGE("")
  MESSAGE( "====================================================================" )
  MESSAGE("ERROR: Cannot use avro because it was not found")
  MESSAGE("       Plese set envoronment vaiable AVRO_BUILD_DIR and AVRO_DIR or the cmake variable" )
  MESSAGE("       SANS_AVRO_BUILD_DIR and SANS_AVRO_DIR to the root avro directory." )
  MESSAGE( "====================================================================" )
  MESSAGE("")
  MESSAGE(FATAL_ERROR "")
ENDIF()

IF( NOT USE_AVRO )

  #==================================================
  # Triangle: https://www.cs.cmu.edu/~quake/triangle.html
  #         Triangle is also provided by AVRO with customizations
  #==================================================
  INCLUDE(BuildTRIANGLE)
  INCLUDE_DIRECTORIES(SYSTEM ${TRIANGLE_INCLUDE_DIRS})
  LIST(APPEND SANS_EXTERNAL_LIBS ${TRIANGLE_LIBRARIES})
ELSE()
  SET( USE_TRIANGLE ON )
  SET( TRIANGLE_INCLUDE_DIRS ${AVRO_INCLUDE_DIRS} )
  SET( TRIANGLE_LIBRARIES ${AVRO_LIBRARIES} )
ENDIF()

#==================================================
# MPI Libraries
#==================================================

IF(USE_MPI AND MPI_FOUND )
  LIST(APPEND SANS_EXTERNAL_LIBS ${MPI_LIBRARIES})
  
  # Test the boost MPI works
  IF( Boost_FOUND )
    INCLUDE(CheckBoostMPI)
  ENDIF()
ENDIF()

#==================================================
# AFLR
#==================================================
GetDependency( AFLR )

IF( AFLR_FOUND AND NOT AFLR_TEST_FAIL )
  SET( AFLR_INIT ON )
ELSE()
  SET( AFLR_INIT OFF )
ENDIF()

#AFLR has a cyclic dependency, at cmake does something wrong if AFLR is not the last dependency
OPTION( USE_AFLR "Enable use AFLR grid generator" ${AFLR_INIT} )
IF( USE_AFLR AND AFLR_FOUND )
  ADD_DEFINITIONS( -DSANS_AFLR )
  # AFLR include directories are only required locally
  # so they are added in src/Meshing/AFLR/CMakeLists.txt
  LIST(APPEND SANS_EXTERNAL_LIBS ${AFLR_LIBRARIES})
ELSEIF( USE_AFLR AND (NOT AFLR_FOUND) )
  MESSAGE("")
  MESSAGE( "====================================================================" )
  MESSAGE("ERROR: Cannot use AFLR because it was not found")
  MESSAGE("       Plese set envoronment vaiable AFLR_DIR or the cmake variable" )
  MESSAGE("       SANS_AFLR_DIR to the root AFLR directory." )
  MESSAGE( "====================================================================" )
  MESSAGE("")
  MESSAGE(FATAL_ERROR "")
ENDIF()
