SET(_log_summary  "${CMAKE_BINARY_DIR}/summary.log")
FILE(REMOVE ${_log_summary})

FUNCTION( PRINTFIXEDWIDTH LINE_LEN_INIT CMAKE_VARIABLE_LIST )

SET( LINE_LEN ${LINE_LEN_INIT} )
FOREACH( ITEM ${${CMAKE_VARIABLE_LIST}} )
  STRING( LENGTH ${ITEM} ITEM_LEN )
  MATH( EXPR LINE_LEN "${LINE_LEN} + ${ITEM_LEN}" )
  IF( ${LINE_LEN} GREATER 80 )
    FILE(APPEND ${_log_summary} 
"\n#                                  " )
    SET( LINE_LEN ${LINE_LEN_INIT} )
  ENDIF()
  FILE(APPEND ${_log_summary} "${ITEM} " )
ENDFOREACH()

ENDFUNCTION()

GET_DIRECTORY_PROPERTY( SANS_LINK_DIRECTORIES LINK_DIRECTORIES ${CMAKE_SOURCE_DIR} )

FILE(APPEND ${_log_summary}
"#############################################
#
#  SANS configuration:
#
#        CMAKE_BUILD_TYPE:         ${CMAKE_BUILD_TYPE}
#        CMAKE_INSTALL_PREFIX:     ${CMAKE_INSTALL_PREFIX}
#        CMAKE_CXX_COMPILER name:  ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION} on platform ${CMAKE_SYSTEM_NAME} ${CMAKE_SYSTEM_PROCESSOR}
#        CMAKE_CXX_COMPILER path:  ${CMAKE_CXX_COMPILER}
#
"
  )

FILE(APPEND ${_log_summary}
"#        CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES:
#                                  ")
PRINTFIXEDWIDTH( 35 CMAKE_CXX_IMPLICIT_LINK_DIRECTORIES )
FILE(APPEND ${_log_summary} "\n#\n")

FILE(APPEND ${_log_summary}
"#  Compiler flags used for this build:
#
#        CMAKE_CXX_FLAGS:          ")
STRING(REPLACE " " ";" CMAKE_CXX_FLAGS_LIST ${CMAKE_CXX_FLAGS}) #Change the flags to a list
PRINTFIXEDWIDTH( 35 CMAKE_CXX_FLAGS_LIST )
FILE(APPEND ${_log_summary} "\n#\n")

IF( CMAKE_BUILD_TYPE )
  STRING( TOUPPER ${CMAKE_BUILD_TYPE} BUILD_TYPE )
  IF (DEFINED CMAKE_CXX_FLAGS_${BUILD_TYPE})
    FILE(APPEND ${_log_summary}
"#        CMAKE_CXX_FLAGS_${BUILD_TYPE}:  ")
    STRING(REPLACE " " ";" CMAKE_CXX_FLAGS_LIST ${CMAKE_CXX_FLAGS_${BUILD_TYPE}}) #Change the flags to a list
    PRINTFIXEDWIDTH( 35 CMAKE_CXX_FLAGS_LIST )
  ENDIF()
  FILE(APPEND ${_log_summary} "\n#\n")
ENDIF()


#FILE(APPEND ${_log_summary}
#"#
#  Compiler definitions used for this build:
#        COMPILE_DEFINITIONS:   ${COMPILE_DEFINITIONS}
#
#"
#)

MACRO (PrintRequired name pad)
  FILE(APPEND ${_log_summary} "#\n")

  IF(${name}_USE_INTERNAL)
    IF(${name}_FOUND)
      FILE(APPEND ${_log_summary}
"#        ${name}${pad}-----> Met with internal build -- Failed compilation test. See CMakeFiles/CMakeError.log for details.\n")
    ELSE()
      FILE(APPEND ${_log_summary}
"#        ${name}${pad}-----> Met with internal build -- Could not find library.\n")
    ENDIF()

  ELSE()

    FILE(APPEND ${_log_summary}
"#        ${name}${pad}-----> Met with existing library:\n"
"#                                Include Directories:\n")

    FOREACH(incDir ${${name}_INCLUDE_DIRS}) 
      FILE(APPEND ${_log_summary} 
"#                                  ${incDir}\n")
    ENDFOREACH()

    IF(DEFINED ${name}_LIBRARIES)
      FILE(APPEND ${_log_summary} "#                                Libraries:\n")

      FOREACH(libName ${${name}_LIBRARIES}) 
        FILE(APPEND ${_log_summary} 
"#                                  ${libName}\n") 
      ENDFOREACH(libName) 
    ENDIF()
  ENDIF()
ENDMACRO(PrintRequired)

FILE(APPEND ${_log_summary}
"#  Required dependencies: \n"
)
PrintRequired(EGADS " ------")
PrintRequired(Boost " ------")
PrintRequired(PYTHON " -----")
PrintRequired(NLOPT " ------")
#PrintRequired(HDF5 " ----")
#PrintRequired(HDF5HL " --")
PrintRequired(SUITESPARSE " ")
PrintRequired(BLAS " -------") #Suitesparse requires BLAS, so BLAS will be optional when suitesparse is
PrintRequired(TRIANGLE " ---")
IF( USE_MPI )
  PrintRequired(PARMETIS " ---")
ENDIF()
FILE(APPEND ${_log_summary} "#\n")



MACRO(PrintOptional name pad)
  FILE(APPEND ${_log_summary} "#\n")
  IF(DEFINED ${name}_FOUND)
    IF(NOT ${name}_FOUND AND ${name}_USE_INTERNAL)
      FILE(APPEND ${_log_summary} 
"#        ${name}${pad}-----> ON. -- Met with internal build. -- Could not find library.\n")
    ELSEIF(${name}_FOUND AND ${name}_TEST_FAIL AND ${name}_USE_INTERNAL )
      FILE(APPEND ${_log_summary} 
"#        ${name}${pad}-----> ON -- Met with internal build. -- Failed compilation test. See CMakeFiles/CMakeError.log for details.\n")
    ELSEIF(${name}_FOUND AND ${name}_TEST_FAIL AND (NOT ${USE_${name}} ) )
      FILE(APPEND ${_log_summary} 
"#        ${name}${pad}-----> OFF -- Failed compilation test. See CMakeFiles/CMakeError.log for details.\n")
    ELSEIF(NOT ${name}_FOUND AND (NOT ${USE_${name}}))
      FILE(APPEND ${_log_summary} 
"#        ${name}${pad}-----> OFF -- Could not find library.\n")
    ELSEIF(${name}_FOUND AND (NOT ${USE_${name}}))
      FILE(APPEND ${_log_summary} 
"#        ${name}${pad}-----> OFF -- Disabled with USE_${name}.\n")
    ELSE()
      FILE(APPEND ${_log_summary} 
"#        ${name}${pad}-----> ON.\n")
    ENDIF()
    
    IF( ${USE_${name}} )
      IF(DEFINED ${name}_INCLUDE_DIRS)
        FILE(APPEND ${_log_summary} 
"#                                Include Directories:\n")
        FOREACH(incDir ${${name}_INCLUDE_DIRS})
          FILE(APPEND ${_log_summary} 
"#                                  ${incDir}\n")
        ENDFOREACH(incDir) 
      ENDIF()

      IF(DEFINED ${name}_LIBRARIES)
        FILE(APPEND ${_log_summary} 
"#                                Libraries:\n")
        FOREACH(libName ${${name}_LIBRARIES}) 
          FILE(APPEND ${_log_summary} 
"#                                  ${libName}\n") 
        ENDFOREACH(libName) 
      ENDIF()
    ENDIF()
  ELSE(${name}_FOUND AND ${name}_TEST_FAIL)
  
    FILE(APPEND ${_log_summary} "#        ${name}${pad}-----> OFF.\n")
    
  ENDIF()
ENDMACRO(PrintOptional)

# print optional libraries status
FILE(APPEND ${_log_summary} "#  Optional dependencies:\n")
#PrintOptional(BLAS " -------")
PrintOptional(LAPACK " -----")
IF (NOT APPLE)
PrintOptional(LAPACKE " ----")
ENDIF()
PrintOptional(GLPK " -------")
PrintOptional(MKL " --------")
PrintOptional(AFLR " -------")
PrintOptional(TETGEN " -----")
PrintOptional(AVRO " -------")
PrintOptional(REFINE " -----")
PrintOptional(PETSC " ------")
PrintOptional(CAPS " -------")
#==================================================
PrintOptional(MPI " --------")
IF(DEFINED MPIEXEC AND USE_MPI)
  FILE(APPEND ${_log_summary} 
"#                                mpiexec:\n")
  FILE(APPEND ${_log_summary} 
"#                                  ${MPIEXEC}\n")
ENDIF()
#==================================================

FILE(APPEND ${_log_summary} "#\n")
  
#FILE(APPEND ${_log_summary}
#"#  Optional tools:   
#       LAPACK -----------------> ${USE_LAPACK}
#
#       MKL --------------------> ${USE_MKL}
#
#       PETSC-------------------> ${USE_PETSC}
#
#"
#)
#        OpenMP: --------------> ${MUQ_USE_OPENMP}
#        CUDA: ----------------> ${MUQ_USE_CUDA}
#        MKL: -----------------> ${MUQ_USE_MKL}
#        Python: --------------> ${MUQ_USE_PYTHON}

#FILE(APPEND ${_log_summary}
#"#  MUQ Modules:   
#        Utilities:              (BUILD=${UtilitiesAndModelling_build},TESTS=${UtilitiesAndModelling_tests})
#        Modelling:              (BUILD=${UtilitiesAndModelling_build},TESTS=${UtilitiesAndModelling_tests})
#        Inference:              (BUILD=${Inference_build},TESTS=${Inference_tests})
#        Optimization:           (BUILD=${Optimization_build},TESTS=${Optimization_tests})
#        PDE:                    (BUILD=${PDE_build},TESTS=${PDE_tests})
#        Geostats:               (BUILD=${Geostats_build},TESTS=${Geostats_tests})
#        Approximation:          (BUILD=${Approximation_build},TESTS=${Approximation_tests})
#
#"
#)

FILE(APPEND ${_log_summary} "#############################################\n\n")
