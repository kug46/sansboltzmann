#include <iostream>
#include <stdlib.h>
#include "Surreal/SurrealD.h"


using namespace std;

#ifndef nderiv
#define nderiv 4
#endif

template<class T>
__attribute__((noinline))
void RoeFlux(const T UL[], const T UR[], double *n, double gam, T F[]);

int main(int argc, char **argv)
{
//  if( argc == 1 )
//  {
//    std::cout << "Please execute as " << argv[0] << " 1" << std::endl;
//    return 1;
//  }

  int i,j,k;
  SurrealD UL[4], UR[4], F[4];
  double n[2], gam;

  SurrealD size8(0,0,nderiv);
  for(int j = 0; j < 4; j ++)
  {
    UL[j] = size8;
    UR[j] = size8;
  }

  UL[0].value() = 1.0;
  UL[1].value() = 1.25;
  UL[2].value() = 0.0;
  UL[3].value() = 1.75;

  UR[0].value() = 1.1;
  UR[1].value() = 0.25;
  UR[2].value() = 0.2;
  UR[3].value() = 2.15;

  for(int j = 0; j < 4; j ++)
  {
    for(int k = 0; k < nderiv; k ++)
    {
      UL[j].deriv(k) = 0.0;
      UR[j].deriv(k) = 0.0;
    }
    UL[j].deriv(j) = 1.0;
    UR[j].deriv(j+4) = 1.0;
  }

  n[0] = 0.25;
  n[1] = 0.75;

  gam = 1.4;

  int iter = 1;
  if(argc > 1) {
    iter = atoi(argv[1]);
  }

  for( i = 0; i < iter; i++ )
  {
    if( i % 10000000 == 0 ) printf("i=%d\n", i);

    RoeFlux(UL,UR,n,gam,F);
  }
  for(j=0; j <= 3; j++) {
    printf("F[%d] = %f\n",j, F[j].value());
    for( k=0; k <= 3; k++)
      printf( "F_UL[%d][%d] = %f\n",j,k, F[j].deriv(k) );
    for( k=0; k <= 3; k++)
      printf( "F_UR[%d][%d] = %f\n",j,k, F[j].deriv(k+4) );
  }

  return 0;

}
