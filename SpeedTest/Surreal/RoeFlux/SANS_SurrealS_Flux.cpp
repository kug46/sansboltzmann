#include "SANS_Surreal_Flux.h"

#ifndef nderiv
#define nderiv 8
#endif

template void RoeFlux(const SurrealS<nderiv> UL[], const SurrealS<nderiv> UR[], double *n, double gam, SurrealS<nderiv> F[]);
