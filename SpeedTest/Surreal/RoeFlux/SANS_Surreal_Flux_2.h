#include <iostream>
#include "Surreal/SurrealD.h"
#include "Surreal/SurrealS.h"

template<class T>
__attribute__((noinline))
void RoeFlux(const T UL[], const T UR[], double *n, double gam, T F[])
{
  T rhol1, ul, vl, u2l, rul, pl, hl;
  T FL[4];

  T rhor1, ur, vr, u2r, rur, pr, hr;
  T FR[4];

  T di, d1, ui, vi, hi, af, ucp, c2, ci, ci1;

  T du0, du1, du2, du3;
  T lam[3];
  T eps;

  T s1, s2, al1x, al2x, c21, ct1, ct2, cc1 , cc2;

  double gmi  = gam - 1.0;
  double NN = sqrt(n[0]*n[0] + n[1]*n[1]);
  double NN1 = 1.0/NN;

  double n1[2];
  n1[0] = n[0]*NN1;
  n1[1] = n[1]*NN1;

  // Left State
  rhol1 = 1/UL[0];

  ul    = UL[1]*rhol1;
  vl    = UL[2]*rhol1;
  u2l   = (ul*ul + vl*vl)*UL[0];
  rul   = (ul  *n1[0] + vl  *n1[1]);

  pl    = (UL[3] - 0.5*u2l  )*gmi;
  hl    = (UL[3] + pl  )*rhol1;

  FL   [0]    = UL[0]*rul;
  FL   [1]    = n1[0]*pl   + UL[1]*rul;
  FL   [2]    = n1[1]*pl   + UL[2]*rul;
  FL   [3]    = (pl   + UL[3])*rul;


  // Right State
  rhor1 = 1/UR[0];

  ur    = UR[1]*rhor1;
  vr    = UR[2]*rhor1;

  u2r   = (ur*ur + vr*vr)*UR[0];
  rur   = (ur  *n1[0] + vr  *n1[1]);

  pr    = (UR[3] - 0.5*u2r  )*gmi;
  hr    = (UR[3] + pr  )*rhor1;

  FR   [0]    = UR[0]*rur;
  FR   [1]    = n1[0]*pr   + UR[1]*rur;
  FR   [2]    = n1[1]*pr   + UR[2]*rur;
  FR   [3]    = (pr   + UR[3])*rur;


  // Average state
  di     = sqrt(UR[0]*rhol1);
  d1     = 1.0/(1.0+di);

  ui     = (di   *ur+   ul  )*d1;
  vi     = (di   *vr+   vl  )*d1;
  hi     = (di*hr+hl)*d1;

  af     = 0.5*(ui*ui   +vi*vi   );

  ucp    = ui   *n1[0]+vi   *n1[1];

  c2     = gmi*(hi   -af   );

  ci    = sqrt(c2);
  ci1   = 1/ci;

  // du = UR-UL
  du0 = UR[0] - UL[0];
  du1 = UR[1] - UL[1];
  du2 = UR[2] - UL[2];
  du3 = UR[3] - UL[3];

  // eigenvalues
  lam   [0] = ucp    + ci;
  lam   [1] = ucp    - ci;
  lam   [2] = ucp ;

  // Entropy fix
  const double ep     = 1e-2;
  eps    = ep*ci;

  for (int i=0; i<3; i++)
    if ((lam[i]<eps)&&(lam[i]>-eps)){
      lam[i] = 0.5*(eps+lam[i]*lam[i]/eps);
    }

  double el[3];
  for (int i=0; i<3; i++){
    if (lam[i]<0)
      el[i] = -1;
    else
      el[i] =  1;
  }

  s1    = 0.5*(el[0]*lam   [0]+el[1]*lam   [1]);
  s2    = 0.5*(el[0]*lam   [0]-el[1]*lam   [1]);

  al1x    = gmi*(af   *du0-ui   *du1-vi   *du2+du3);
  al2x    = -ucp   *du0+du1*n1[0]+du2*n1[1];

  c21    = ci1*ci1;

  ct1    = (s2   *al1x            *ci1);
  ct2    = (s2   *al2x            *ci1);

  cc1    = ct2   +(s1   -el[2]*lam   [2])*al1x*c21;
  cc2    = ct1   +(s1   -el[2]*lam   [2])*al2x;

  F[0] = NN*(0.5*(FL[0]+FR[0])-0.5*(el[2]*lam   [2]*du0+cc1   ));
  F[1] = NN*(0.5*(FL[1]+FR[1])-0.5*(el[2]*lam   [2]*du1+cc1   *ui+cc2   *n1[0]));
  F[2] = NN*(0.5*(FL[2]+FR[2])-0.5*(el[2]*lam   [2]*du2+cc1   *vi+cc2   *n1[1]));
  F[3] = NN*(0.5*(FL[3]+FR[3])-0.5*(el[2]*lam   [2]*du3+cc1   *hi+cc2   *ucp  ));

}

