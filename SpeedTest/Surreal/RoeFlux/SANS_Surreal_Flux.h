#include <iostream>
#include "Surreal/SurrealD.h"
#include "Surreal/SurrealS.h"

template<class T>
void RoeFlux(const T UL[], const T UR[], double *n, double gam, T F[])
{
  double gmi  = gam - 1.0;
  double NN = sqrt(n[0]*n[0] + n[1]*n[1]);
  double NN1 = 1.0/NN;

  double n1[2];
  n1[0] = n[0]*NN1;
  n1[1] = n[1]*NN1;

  // Left State
  T rhol1 = 1/UL[0];

  T ul    = UL[1]*rhol1;
  T vl    = UL[2]*rhol1;
  T u2l   = (ul*ul + vl*vl)*UL[0];
  T rul   = (ul  *n1[0] + vl  *n1[1]);

  T pl    = (UL[3] - 0.5*u2l  )*gmi;
  T hl    = (UL[3] + pl  )*rhol1;

  T FL[4];
  FL   [0]    = UL[0]*rul;
  FL   [1]    = n1[0]*pl   + UL[1]*rul;
  FL   [2]    = n1[1]*pl   + UL[2]*rul;
  FL   [3]    = (pl   + UL[3])*rul;


  // Right State
  T rhor1 = 1/UR[0];

  T ur    = UR[1]*rhor1;
  T vr    = UR[2]*rhor1;

  T u2r   = (ur*ur + vr*vr)*UR[0];
  T rur   = (ur  *n1[0] + vr  *n1[1]);

  T pr    = (UR[3] - 0.5*u2r  )*gmi;
  T hr    = (UR[3] + pr  )*rhor1;

  T FR[4];
  FR   [0]    = UR[0]*rur;
  FR   [1]    = n1[0]*pr   + UR[1]*rur;
  FR   [2]    = n1[1]*pr   + UR[2]*rur;
  FR   [3]    = (pr   + UR[3])*rur;


  // Average state
  T di     = sqrt(UR[0]*rhol1);
  T d1     = 1.0/(1.0+di);

  T ui     = (di   *ur+   ul  )*d1;
  T vi     = (di   *vr+   vl  )*d1;
  T hi     = (di*hr+hl)*d1;

  T af     = 0.5*(ui*ui   +vi*vi   );

  T ucp    = ui   *n1[0]+vi   *n1[1];

  T c2     = gmi*(hi   -af   );

  T ci    = sqrt(c2);
  T ci1   = 1/ci;

  // du = UR-UL
  T du0 = UR[0] - UL[0];
  T du1 = UR[1] - UL[1];
  T du2 = UR[2] - UL[2];
  T du3 = UR[3] - UL[3];

  // eigenvalues
  T lam[3];
  lam   [0] = ucp    + ci;
  lam   [1] = ucp    - ci;
  lam   [2] = ucp ;

  // Entropy fix
  const double ep     = 1e-2;
  T eps    = ep*ci;

  for (int i=0; i<3; i++)
    if ((lam[i]<eps)&&(lam[i]>-eps)){
      lam[i] = 0.5*(eps+lam[i]*lam[i]/eps);
    }

  double el[3];
  for (int i=0; i<3; i++){
    if (lam[i]<0)
      el[i] = -1;
    else
      el[i] =  1;
  }

  T s1    = 0.5*(el[0]*lam   [0]+el[1]*lam   [1]);
  T s2    = 0.5*(el[0]*lam   [0]-el[1]*lam   [1]);

  T al1x    = gmi*(af   *du0-ui   *du1-vi   *du2+du3);
  T al2x    = -ucp   *du0+du1*n1[0]+du2*n1[1];

  T c21    = ci1*ci1;

  T ct1    = (s2   *al1x            *ci1);
  T ct2    = (s2   *al2x            *ci1);

  T cc1    = ct2   +(s1   -el[2]*lam   [2])*al1x*c21;
  T cc2    = ct1   +(s1   -el[2]*lam   [2])*al2x;

  F[0] = NN*(0.5*(FL[0]+FR[0])-0.5*(el[2]*lam   [2]*du0+cc1   ));
  F[1] = NN*(0.5*(FL[1]+FR[1])-0.5*(el[2]*lam   [2]*du1+cc1   *ui+cc2   *n1[0]));
  F[2] = NN*(0.5*(FL[2]+FR[2])-0.5*(el[2]*lam   [2]*du2+cc1   *vi+cc2   *n1[1]));
  F[3] = NN*(0.5*(FL[3]+FR[3])-0.5*(el[2]*lam   [2]*du3+cc1   *hi+cc2   *ucp  ));

}

