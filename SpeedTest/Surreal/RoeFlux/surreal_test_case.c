
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


__attribute__((noinline))
void RoeFluxJacobian(double *UL, double *UR, double *n, double gamma, double *F, double *F_UL, double *F_UR)
{
  int i, j, ind, StateRank;
  double rhol1;
  double ul, ul_0, ul_1;
  double vl, vl_0, vl_2;
  double rhor1;
  double ur, ur_0, ur_1;
  double vr, vr_0, vr_2;
  double u2l, u2l_0, u2l_1, u2l_2;
  double u2r, u2r_0, u2r_1, u2r_2;
  double rul, rul_0, rul_1, rul_2;
  double rur, rur_0, rur_1, rur_2;
  double pl, pl_0, pl_1, pl_2, pl_3;
  double hl, hl_0, hl_1, hl_2, hl_3;
  double pr, pr_0, pr_1, pr_2, pr_3;
  double hr, hr_0, hr_1, hr_2, hr_3;

  double FL[4], FL_UL[4][4];
  double FR[4], FR_UR[4][4];
  double rum    = 0.0;
  double rum_l0 = 0.0, rum_l1 = 0.0, rum_l2 = 0.0;
  double rum_r0 = 0.0, rum_r1 = 0.0, rum_r2 = 0.0;

  double di  = 0.0, di_l0  = 0.0, di_r0  = 0.0;
  double d1  = 0.0, d1_l0  = 0.0, d1_r0  = 0.0;
  double ui  = 0.0, ui_l0  = 0.0, ui_l1  = 0.0, ui_r0  = 0.0, ui_r1  = 0.0;
  double vi  = 0.0, vi_l0  = 0.0, vi_l2  = 0.0, vi_r0  = 0.0, vi_r2  = 0.0;
  double hi  = 0.0, hi_l0  = 0.0, hi_l1  = 0.0, hi_l2  = 0.0, hi_l3  = 0.0, hi_r0  = 0.0, hi_r1  = 0.0, hi_r2 = 0.0, hi_r3 = 0.0;
  double af  = 0.0, af_l0  = 0.0, af_l1  = 0.0, af_l2  = 0.0, af_r0  = 0.0, af_r1  = 0.0, af_r2  = 0.0;
  double ucp = 0.0, ucp_l0 = 0.0, ucp_l1 = 0.0, ucp_l2 = 0.0, ucp_r0 = 0.0, ucp_r1 = 0.0, ucp_r2 = 0.0;
  double c2  = 0.0, c2_l0  = 0.0, c2_l1  = 0.0, c2_l2  = 0.0, c2_l3  = 0.0, c2_r0  = 0.0, c2_r1  = 0.0, c2_r2 = 0.0, c2_r3 = 0.0;
  double ci  = 0.0, ci_l0  = 0.0, ci_l1  = 0.0, ci_l2  = 0.0, ci_l3  = 0.0, ci_r0  = 0.0, ci_r1  = 0.0, ci_r2 = 0.0, ci_r3 = 0.0;

  double du0 = 0.0, du1 = 0.0, du2 = 0.0, du3 = 0.0, ci1 = 0.0, c21 = 0.0, el[3], ep = 0.0;

  double lam[3], lam_l0[3], lam_l1[3], lam_l2[3], lam_l3[3];
  double lam_r0[3], lam_r1[3], lam_r2[3], lam_r3[3];
  double lt = 0.0, lt2;

  double eps  = 0.0, eps_l0 = 0.0, eps_l1 = 0.0, eps_l2 = 0.0, eps_l3 = 0.0;
  double eps1 = 0.0, eps_r0 = 0.0, eps_r1 = 0.0, eps_r2 = 0.0, eps_r3 = 0.0;

  double s1   = 0.0, s1_l0   = 0.0, s1_l1   = 0.0, s1_l2   = 0.0, s1_l3   = 0.0, s1_r0   = 0.0, s1_r1   = 0.0, s1_r2   = 0.0, s1_r3   = 0.0;
  double s2   = 0.0, s2_l0   = 0.0, s2_l1   = 0.0, s2_l2   = 0.0, s2_l3   = 0.0, s2_r0   = 0.0, s2_r1   = 0.0, s2_r2   = 0.0, s2_r3   = 0.0;
  double al1x = 0.0, al1x_l0 = 0.0, al1x_l1 = 0.0, al1x_l2 = 0.0, al1x_l3 = 0.0, al1x_r0 = 0.0, al1x_r1 = 0.0, al1x_r2 = 0.0, al1x_r3 = 0.0;
  double al2x = 0.0, al2x_l0 = 0.0, al2x_l1 = 0.0, al2x_l2 = 0.0, al2x_r0 = 0.0, al2x_r1 = 0.0, al2x_r2 = 0.0;
  double ct1  = 0.0, ct1_l0  = 0.0, ct1_l1  = 0.0, ct1_l2  = 0.0, ct1_l3  = 0.0, ct1_r0  = 0.0, ct1_r1  = 0.0, ct1_r2  = 0.0, ct1_r3  = 0.0;
  double ct2  = 0.0, ct2_l0  = 0.0, ct2_l1  = 0.0, ct2_l2  = 0.0, ct2_l3  = 0.0, ct2_r0  = 0.0, ct2_r1  = 0.0, ct2_r2  = 0.0, ct2_r3  = 0.0;
  double cc1  = 0.0, cc1_l0  = 0.0, cc1_l1  = 0.0, cc1_l2  = 0.0, cc1_l3  = 0.0, cc1_r0  = 0.0, cc1_r1  = 0.0, cc1_r2  = 0.0, cc1_r3  = 0.0;
  double cc2  = 0.0, cc2_l0  = 0.0, cc2_l1  = 0.0, cc2_l2  = 0.0, cc2_l3  = 0.0, cc2_r0  = 0.0, cc2_r1  = 0.0, cc2_r2  = 0.0, cc2_r3  = 0.0;

  double gmi; //, gmi1;
  double NN, NN1, n1[2];

  double tmp, tmp_UL[4], tmp_UR[4];
  double c21_l0, c21_l1, c21_l2, c21_l3;
  double c21_r0, c21_r1, c21_r2, c21_r3;
  double ci1_l0, ci1_l1, ci1_l2, ci1_l3;
  double ci1_r0, ci1_r1, ci1_r2, ci1_r3;
  double tmp1, tmp1_UL[4], tmp1_UR[4];
  double tmp2, tmp2_UL[4], tmp2_UR[4];

  /* initialize flux */
  for (i=0; i<4; i++){
    FL[i] = 0.0;
    FR[i] = 0.0;
    for (j=0; j<4; j++){
      FL_UL[i][j] = 0.0;
      FR_UR[i][j] = 0.0;
    }
  }

  StateRank = 4;

  gmi  = gamma - 1;
  //gmi1 = 1.0/gmi;;
  NN = sqrt(n[0]*n[0] + n[1]*n[1]);

  NN1 = 1.0/NN;

  n1[0] = n[0]*NN1;
  n1[1] = n[1]*NN1;


  // Left State
  rhol1 = 1/UL[0];

  ul    = UL[1]*rhol1;
  ul_0  =   -ul*rhol1;
  ul_1  =       rhol1;

  vl    = UL[2]*rhol1;
  vl_0  =   -vl*rhol1;
  vl_2  =       rhol1;

  u2l   = (ul*ul + vl*vl)*UL[0];
  u2l_0 =  -u2l*rhol1;
  u2l_1 = 2*ul;
  u2l_2 = 2*vl;

  rul   = (ul  *n1[0] + vl  *n1[1]);
  rul_0 = (ul_0*n1[0] + vl_0*n1[1]);
  rul_1 =  ul_1*n1[0]              ;
  rul_2 =               vl_2*n1[1] ;

  pl    = (UL[3] - 0.5*u2l  )*gmi;
  pl_0  = (      - 0.5*u2l_0)*gmi;
  pl_1  = (      - 0.5*u2l_1)*gmi;
  pl_2  = (      - 0.5*u2l_2)*gmi;
  pl_3  = (1.0              )*gmi;

  hl    = (UL[3] + pl  )*rhol1;
  hl_0  = (      + pl_0)*rhol1-hl*rhol1;
  hl_1  = (      + pl_1)*rhol1;
  hl_2  = (      + pl_2)*rhol1;
  hl_3  = (1.0   + pl_3)*rhol1;

  FL   [0]    = UL[0]*rul;
  FL_UL[0][0] =       rul + UL[0]*rul_0;
  FL_UL[0][1] =             UL[0]*rul_1;
  FL_UL[0][2] =             UL[0]*rul_2;
  FL_UL[0][3] = 0.0;

  FL   [1]    = n1[0]*pl   + UL[1]*rul;
  FL_UL[1][0] = n1[0]*pl_0 + UL[1]*rul_0;
  FL_UL[1][1] = n1[0]*pl_1 + UL[1]*rul_1 + rul;
  FL_UL[1][2] = n1[0]*pl_2 + UL[1]*rul_2;
  FL_UL[1][3] = n1[0]*pl_3              ;

  FL   [2]    = n1[1]*pl   + UL[2]*rul;
  FL_UL[2][0] = n1[1]*pl_0 + UL[2]*rul_0;
  FL_UL[2][1] = n1[1]*pl_1 + UL[2]*rul_1;
  FL_UL[2][2] = n1[1]*pl_2 + UL[2]*rul_2 + rul;
  FL_UL[2][3] = n1[1]*pl_3              ;

  FL   [3]    = (pl   + UL[3])*rul;
  FL_UL[3][0] = (pl_0        )*rul + (pl + UL[3])*rul_0;
  FL_UL[3][1] = (pl_1        )*rul + (pl + UL[3])*rul_1;
  FL_UL[3][2] = (pl_2        )*rul + (pl + UL[3])*rul_2;
  FL_UL[3][3] = (pl_3 + 1.0  )*rul                     ;

  // Right State
  rhor1 = 1/UR[0];

  ur    = UR[1]*rhor1;
  ur_0  =   -ur*rhor1;
  ur_1  =       rhor1;

  vr    = UR[2]*rhor1;
  vr_0  =   -vr*rhor1;
  vr_2  =       rhor1;

  u2r   = (ur*ur + vr*vr)*UR[0];
  u2r_0 =  -u2r*rhor1;
  u2r_1 = 2*ur;
  u2r_2 = 2*vr;

  rur   = (ur  *n1[0] + vr  *n1[1]);
  rur_0 = (ur_0*n1[0] + vr_0*n1[1]);
  rur_1 =  ur_1*n1[0]              ;
  rur_2 =               vr_2*n1[1] ;

  pr    = (UR[3] - 0.5*u2r  )*gmi;
  pr_0  = (      - 0.5*u2r_0)*gmi;
  pr_1  = (      - 0.5*u2r_1)*gmi;
  pr_2  = (      - 0.5*u2r_2)*gmi;
  pr_3  = (1.0              )*gmi;

  hr    = (UR[3] + pr  )*rhor1;
  hr_0  = (      + pr_0)*rhor1-hr*rhor1;
  hr_1  = (      + pr_1)*rhor1;
  hr_2  = (      + pr_2)*rhor1;
  hr_3  = (1.0   + pr_3)*rhor1;

  FR   [0]    = UR[0]*rur;
  FR_UR[0][0] =       rur + UR[0]*rur_0;
  FR_UR[0][1] =             UR[0]*rur_1;
  FR_UR[0][2] =             UR[0]*rur_2;
  FR_UR[0][3] = 0.0;

  FR   [1]    = n1[0]*pr   + UR[1]*rur;
  FR_UR[1][0] = n1[0]*pr_0 + UR[1]*rur_0;
  FR_UR[1][1] = n1[0]*pr_1 + UR[1]*rur_1 + rur;
  FR_UR[1][2] = n1[0]*pr_2 + UR[1]*rur_2;
  FR_UR[1][3] = n1[0]*pr_3              ;

  FR   [2]    = n1[1]*pr   + UR[2]*rur;
  FR_UR[2][0] = n1[1]*pr_0 + UR[2]*rur_0;
  FR_UR[2][1] = n1[1]*pr_1 + UR[2]*rur_1;
  FR_UR[2][2] = n1[1]*pr_2 + UR[2]*rur_2 + rur;
  FR_UR[2][3] = n1[1]*pr_3              ;

  FR   [3]    = (pr   + UR[3])*rur;
  FR_UR[3][0] = (pr_0        )*rur + (pr + UR[3])*rur_0;
  FR_UR[3][1] = (pr_1        )*rur + (pr + UR[3])*rur_1;
  FR_UR[3][2] = (pr_2        )*rur + (pr + UR[3])*rur_2;
  FR_UR[3][3] = (pr_3 + 1.0  )*rur                     ;


  // Average state
  di     = sqrt(UR[0]*rhol1);
  di_l0  = -0.5*di*rhol1;
  di_r0  =  0.5*di*rhor1;

  d1     = 1.0/(1.0+di);
  d1_l0  = -di_l0*d1*d1;
  d1_r0  = -di_r0*d1*d1;

  ui     = (di   *ur+   ul  )*d1;
  ui_l0  = (di_l0*ur+   ul_0)*d1+(di*ur+ul)*d1_l0;
  ui_l1  = (            ul_1)*d1                 ;
  ui_r0  = (di_r0*ur+di*ur_0)*d1+(di*ur+ul)*d1_r0;
  ui_r1  = (         di*ur_1)*d1                 ;

  vi     = (di   *vr+   vl  )*d1;
  vi_l0  = (di_l0*vr+   vl_0)*d1+(di*vr+vl)*d1_l0;
  vi_l2  = (            vl_2)*d1                 ;
  vi_r0  = (di_r0*vr+di*vr_0)*d1+(di*vr+vl)*d1_r0;
  vi_r2  = (         di*vr_2)*d1                 ;

  hi     = (di*hr+hl)*d1;
  hi_l0  = (di_l0*hr+   hl_0)*d1+(di*hr+hl)*d1_l0;
  hi_l1  = (            hl_1)*d1;
  hi_l2  = (            hl_2)*d1;
  hi_l3  = (            hl_3)*d1;
  hi_r0  = (di_r0*hr+di*hr_0)*d1+(di*hr+hl)*d1_r0;
  hi_r1  = (         di*hr_1)*d1;
  hi_r2  = (         di*hr_2)*d1;
  hi_r3  = (         di*hr_3)*d1;

  af     = 0.5*(ui*ui   +vi*vi   );
  af_l0  =      ui*ui_l0+vi*vi_l0;
  af_l1  =      ui*ui_l1         ;
  af_l2  =               vi*vi_l2;
  af_r0  =      ui*ui_r0+vi*vi_r0;
  af_r1  =      ui*ui_r1         ;
  af_r2  =               vi*vi_r2;

  ucp    = ui   *n1[0]+vi   *n1[1];
  ucp_l0 = ui_l0*n1[0]+vi_l0*n1[1];
  ucp_l1 = ui_l1*n1[0]            ;
  ucp_l2 =             vi_l2*n1[1];
  ucp_r0 = ui_r0*n1[0]+vi_r0*n1[1];
  ucp_r1 = ui_r1*n1[0]            ;
  ucp_r2 =             vi_r2*n1[1];

  c2     = gmi*(hi   -af   );
  c2_l0  = gmi*(hi_l0-af_l0);
  c2_l1  = gmi*(hi_l1-af_l1);
  c2_l2  = gmi*(hi_l2-af_l2);
  c2_l3  = gmi*(hi_l3      );
  c2_r0  = gmi*(hi_r0-af_r0);
  c2_r1  = gmi*(hi_r1-af_r1);
  c2_r2  = gmi*(hi_r2-af_r2);
  c2_r3  = gmi*(hi_r3      );


  ci    = sqrt(c2);
  ci1   = 1/ci;
  ci_l0 = 0.5*ci1*c2_l0;
  ci_l1 = 0.5*ci1*c2_l1;
  ci_l2 = 0.5*ci1*c2_l2;
  ci_l3 = 0.5*ci1*c2_l3;
  ci_r0 = 0.5*ci1*c2_r0;
  ci_r1 = 0.5*ci1*c2_r1;
  ci_r2 = 0.5*ci1*c2_r2;
  ci_r3 = 0.5*ci1*c2_r3;

  ci1_l0 = -ci_l0*(ci1*ci1);
  ci1_l1 = -ci_l1*(ci1*ci1);
  ci1_l2 = -ci_l2*(ci1*ci1);
  ci1_l3 = -ci_l3*(ci1*ci1);
  ci1_r0 = -ci_r0*(ci1*ci1);
  ci1_r1 = -ci_r1*(ci1*ci1);
  ci1_r2 = -ci_r2*(ci1*ci1);
  ci1_r3 = -ci_r3*(ci1*ci1);

  // du = UR-UL
  du0 = UR[0] - UL[0];
  du1 = UR[1] - UL[1];
  du2 = UR[2] - UL[2];
  du3 = UR[3] - UL[3];

  // eigenvalues
  lam   [0] = ucp    + ci;
  lam_l0[0] = ucp_l0 + ci_l0;
  lam_l1[0] = ucp_l1 + ci_l1;
  lam_l2[0] = ucp_l2 + ci_l2;
  lam_l3[0] =          ci_l3;
  lam_r0[0] = ucp_r0 + ci_r0;
  lam_r1[0] = ucp_r1 + ci_r1;
  lam_r2[0] = ucp_r2 + ci_r2;
  lam_r3[0] =          ci_r3;

  lam   [1] = ucp    - ci;
  lam_l0[1] = ucp_l0 - ci_l0;
  lam_l1[1] = ucp_l1 - ci_l1;
  lam_l2[1] = ucp_l2 - ci_l2;
  lam_l3[1] =        - ci_l3;
  lam_r0[1] = ucp_r0 - ci_r0;
  lam_r1[1] = ucp_r1 - ci_r1;
  lam_r2[1] = ucp_r2 - ci_r2;
  lam_r3[1] =        - ci_r3;

  lam   [2] = ucp ;
  lam_l0[2] = ucp_l0;
  lam_l1[2] = ucp_l1;
  lam_l2[2] = ucp_l2;
  lam_l3[2] = 0.0   ;
  lam_r0[2] = ucp_r0;
  lam_r1[2] = ucp_r1;
  lam_r2[2] = ucp_r2;
  lam_r3[2] = 0.0   ;

  // Entropy fix
  ep     = 1e-2;
  eps    = ep*ci;
  eps_l0 = ep*ci_l0;
  eps_l1 = ep*ci_l1;
  eps_l2 = ep*ci_l2;
  eps_l3 = ep*ci_l3;
  eps_r0 = ep*ci_r0;
  eps_r1 = ep*ci_r1;
  eps_r2 = ep*ci_r2;
  eps_r3 = ep*ci_r3;

  for (i=0; i<3; i++){
    if ((lam[i]<eps)&&(lam[i]>-eps)){
      eps1 = 1/eps;
      lt = lam[i];
      lt2 = 0.5*lt*lt;
      lam_l0[i] = 0.5*eps_l0 + (lt*lam_l0[i]-lt2*eps1*eps_l0)*eps1;
      lam_l1[i] = 0.5*eps_l1 + (lt*lam_l1[i]-lt2*eps1*eps_l1)*eps1;
      lam_l2[i] = 0.5*eps_l2 + (lt*lam_l2[i]-lt2*eps1*eps_l2)*eps1;
      lam_l3[i] = 0.5*eps_l3 + (lt*lam_l3[i]-lt2*eps1*eps_l3)*eps1;
      lam_r0[i] = 0.5*eps_r0 + (lt*lam_r0[i]-lt2*eps1*eps_r0)*eps1;
      lam_r1[i] = 0.5*eps_r1 + (lt*lam_r1[i]-lt2*eps1*eps_r1)*eps1;
      lam_r2[i] = 0.5*eps_r2 + (lt*lam_r2[i]-lt2*eps1*eps_r2)*eps1;
      lam_r3[i] = 0.5*eps_r3 + (lt*lam_r3[i]-lt2*eps1*eps_r3)*eps1;

      lam[i] = 0.5*(eps+lt*lt*eps1);
    }
  }

  for (i=0; i<3; i++){
    if (lam[i]<0)
      el[i] = -1;
    else
      el[i] =  1;
  }

  s1    = 0.5*(el[0]*lam   [0]+el[1]*lam   [1]);
  s1_l0 = 0.5*(el[0]*lam_l0[0]+el[1]*lam_l0[1]);
  s1_l1 = 0.5*(el[0]*lam_l1[0]+el[1]*lam_l1[1]);
  s1_l2 = 0.5*(el[0]*lam_l2[0]+el[1]*lam_l2[1]);
  s1_l3 = 0.5*(el[0]*lam_l3[0]+el[1]*lam_l3[1]);
  s1_r0 = 0.5*(el[0]*lam_r0[0]+el[1]*lam_r0[1]);
  s1_r1 = 0.5*(el[0]*lam_r1[0]+el[1]*lam_r1[1]);
  s1_r2 = 0.5*(el[0]*lam_r2[0]+el[1]*lam_r2[1]);
  s1_r3 = 0.5*(el[0]*lam_r3[0]+el[1]*lam_r3[1]);

  s2    = 0.5*(el[0]*lam   [0]-el[1]*lam   [1]);
  s2_l0 = 0.5*(el[0]*lam_l0[0]-el[1]*lam_l0[1]);
  s2_l1 = 0.5*(el[0]*lam_l1[0]-el[1]*lam_l1[1]);
  s2_l2 = 0.5*(el[0]*lam_l2[0]-el[1]*lam_l2[1]);
  s2_l3 = 0.5*(el[0]*lam_l3[0]-el[1]*lam_l3[1]);
  s2_r0 = 0.5*(el[0]*lam_r0[0]-el[1]*lam_r0[1]);
  s2_r1 = 0.5*(el[0]*lam_r1[0]-el[1]*lam_r1[1]);
  s2_r2 = 0.5*(el[0]*lam_r2[0]-el[1]*lam_r2[1]);
  s2_r3 = 0.5*(el[0]*lam_r3[0]-el[1]*lam_r3[1]);

  al1x    = gmi*(af   *du0-ui   *du1-vi   *du2+du3);
  al1x_l0 = gmi*(af_l0*du0-ui_l0*du1-vi_l0*du2- af  );
  al1x_l1 = gmi*(af_l1*du0-ui_l1*du1            + ui  );
  al1x_l2 = gmi*(af_l2*du0            -vi_l2*du2+ vi  );
  al1x_l3 = gmi*(                                   - 1.0 );
  al1x_r0 = gmi*(af_r0*du0-ui_r0*du1-vi_r0*du2+ af  );
  al1x_r1 = gmi*(af_r1*du0-ui_r1*du1            - ui  );
  al1x_r2 = gmi*(af_r2*du0            -vi_r2*du2- vi  );
  al1x_r3 = gmi*(                                   + 1.0 );

  al2x    = -ucp   *du0+du1*n1[0]+du2*n1[1];
  al2x_l0 = -ucp_l0*du0+ucp                    ;
  al2x_l1 = -ucp_l1*du0-      n1[0]            ;
  al2x_l2 = -ucp_l2*du0            -      n1[1];
  al2x_r0 = -ucp_r0*du0-ucp                    ;
  al2x_r1 = -ucp_r1*du0+      n1[0]            ;
  al2x_r2 = -ucp_r2*du0            +      n1[1];

  c21    = ci1*ci1;
  c21_l0 = 2.0*ci1*ci1_l0;
  c21_l1 = 2.0*ci1*ci1_l1;
  c21_l2 = 2.0*ci1*ci1_l2;
  c21_l3 = 2.0*ci1*ci1_l3;

  c21_r0 = 2.0*ci1*ci1_r0;
  c21_r1 = 2.0*ci1*ci1_r1;
  c21_r2 = 2.0*ci1*ci1_r2;
  c21_r3 = 2.0*ci1*ci1_r3;


  ct1    = (s2   *al1x            *ci1);
  ct1_l0 = (s2_l0*al1x+s2*al1x_l0)*ci1+s2*al1x*ci1_l0;
  ct1_l1 = (s2_l1*al1x+s2*al1x_l1)*ci1+s2*al1x*ci1_l1;
  ct1_l2 = (s2_l2*al1x+s2*al1x_l2)*ci1+s2*al1x*ci1_l2;
  ct1_l3 = (s2_l3*al1x+s2*al1x_l3)*ci1+s2*al1x*ci1_l3;
  ct1_r0 = (s2_r0*al1x+s2*al1x_r0)*ci1+s2*al1x*ci1_r0;
  ct1_r1 = (s2_r1*al1x+s2*al1x_r1)*ci1+s2*al1x*ci1_r1;
  ct1_r2 = (s2_r2*al1x+s2*al1x_r2)*ci1+s2*al1x*ci1_r2;
  ct1_r3 = (s2_r3*al1x+s2*al1x_r3)*ci1+s2*al1x*ci1_r3;


  ct2    = (s2   *al2x            *ci1);
  ct2_l0 = (s2_l0*al2x+s2*al2x_l0)*ci1+s2*al2x*ci1_l0;
  ct2_l1 = (s2_l1*al2x+s2*al2x_l1)*ci1+s2*al2x*ci1_l1;
  ct2_l2 = (s2_l2*al2x+s2*al2x_l2)*ci1+s2*al2x*ci1_l2;
  ct2_l3 = (s2_l3*al2x           )*ci1+s2*al2x*ci1_l3;
  ct2_r0 = (s2_r0*al2x+s2*al2x_r0)*ci1+s2*al2x*ci1_r0;
  ct2_r1 = (s2_r1*al2x+s2*al2x_r1)*ci1+s2*al2x*ci1_r1;
  ct2_r2 = (s2_r2*al2x+s2*al2x_r2)*ci1+s2*al2x*ci1_r2;
  ct2_r3 = (s2_r3*al2x           )*ci1+s2*al2x*ci1_r3;


  cc1    = ct2   +(s1   -el[2]*lam   [2])*al1x*c21;
  cc1_l0 = ct2_l0+(s1_l0-el[2]*lam_l0[2])*al1x*c21+(s1-el[2]*lam[2])*(al1x_l0*c21+al1x*c21_l0);
  cc1_l1 = ct2_l1+(s1_l1-el[2]*lam_l1[2])*al1x*c21+(s1-el[2]*lam[2])*(al1x_l1*c21+al1x*c21_l1);
  cc1_l2 = ct2_l2+(s1_l2-el[2]*lam_l2[2])*al1x*c21+(s1-el[2]*lam[2])*(al1x_l2*c21+al1x*c21_l2);
  cc1_l3 = ct2_l3+(s1_l3-el[2]*lam_l3[2])*al1x*c21+(s1-el[2]*lam[2])*(al1x_l3*c21+al1x*c21_l3);
  cc1_r0 = ct2_r0+(s1_r0-el[2]*lam_r0[2])*al1x*c21+(s1-el[2]*lam[2])*(al1x_r0*c21+al1x*c21_r0);
  cc1_r1 = ct2_r1+(s1_r1-el[2]*lam_r1[2])*al1x*c21+(s1-el[2]*lam[2])*(al1x_r1*c21+al1x*c21_r1);
  cc1_r2 = ct2_r2+(s1_r2-el[2]*lam_r2[2])*al1x*c21+(s1-el[2]*lam[2])*(al1x_r2*c21+al1x*c21_r2);
  cc1_r3 = ct2_r3+(s1_r3-el[2]*lam_r3[2])*al1x*c21+(s1-el[2]*lam[2])*(al1x_r3*c21+al1x*c21_r3);

  cc2    = ct1   +(s1   -el[2]*lam   [2])*al2x;
  cc2_l0 = ct1_l0+(s1_l0-el[2]*lam_l0[2])*al2x+(s1-el[2]*lam[2])*(al2x_l0);
  cc2_l1 = ct1_l1+(s1_l1-el[2]*lam_l1[2])*al2x+(s1-el[2]*lam[2])*(al2x_l1);
  cc2_l2 = ct1_l2+(s1_l2-el[2]*lam_l2[2])*al2x+(s1-el[2]*lam[2])*(al2x_l2);
  cc2_l3 = ct1_l3+(s1_l3-el[2]*lam_l3[2])*al2x;
  cc2_r0 = ct1_r0+(s1_r0-el[2]*lam_r0[2])*al2x+(s1-el[2]*lam[2])*(al2x_r0);
  cc2_r1 = ct1_r1+(s1_r1-el[2]*lam_r1[2])*al2x+(s1-el[2]*lam[2])*(al2x_r1);
  cc2_r2 = ct1_r2+(s1_r2-el[2]*lam_r2[2])*al2x+(s1-el[2]*lam[2])*(al2x_r2);
  cc2_r3 = ct1_r3+(s1_r3-el[2]*lam_r3[2])*al2x;

  F   [0]    = NN*(0.5*(FL[0]+FR[0])-0.5*(el[2]*lam   [2]*du0+cc1   ));
  F   [1]    = NN*(0.5*(FL[1]+FR[1])-0.5*(el[2]*lam   [2]*du1+cc1   *ui+cc2   *n1[0]));
  F   [2]    = NN*(0.5*(FL[2]+FR[2])-0.5*(el[2]*lam   [2]*du2+cc1   *vi+cc2   *n1[1]));
  F   [3]    = NN*(0.5*(FL[3]+FR[3])-0.5*(el[2]*lam   [2]*du3+cc1   *hi+cc2   *ucp  ));

  if ( F_UL != NULL ){
    F_UL[0] = NN*(0.5* FL_UL[0][0] -0.5*(el[2]*lam_l0[2]*du0+cc1_l0-el[2]*lam[2]));
    F_UL[1] = NN*(0.5* FL_UL[0][1] -0.5*(el[2]*lam_l1[2]*du0+cc1_l1));
    F_UL[2] = NN*(0.5* FL_UL[0][2] -0.5*(el[2]*lam_l2[2]*du0+cc1_l2));
    F_UL[3] = NN*(0.5* FL_UL[0][3] -0.5*(el[2]*lam_l3[2]*du0+cc1_l3));

    ind = StateRank;
    F_UL[ind + 0] = NN*(0.5* FL_UL[1][0] -0.5*(el[2]*lam_l0[2]*du1+cc1_l0*ui+cc2_l0*n1[0]+cc1*ui_l0));
    F_UL[ind + 1] = NN*(0.5* FL_UL[1][1] -0.5*(el[2]*lam_l1[2]*du1+cc1_l1*ui+cc2_l1*n1[0]+cc1*ui_l1-el[2]*lam[2]));
    F_UL[ind + 2] = NN*(0.5* FL_UL[1][2] -0.5*(el[2]*lam_l2[2]*du1+cc1_l2*ui+cc2_l2*n1[0]));
    F_UL[ind + 3] = NN*(0.5* FL_UL[1][3] -0.5*(el[2]*lam_l3[2]*du1+cc1_l3*ui+cc2_l3*n1[0]));

    ind = StateRank*2;
    F_UL[ind + 0] = NN*(0.5* FL_UL[2][0] -0.5*(el[2]*lam_l0[2]*du2+cc1_l0*vi+cc2_l0*n1[1]+cc1*vi_l0));
    F_UL[ind + 1] = NN*(0.5* FL_UL[2][1] -0.5*(el[2]*lam_l1[2]*du2+cc1_l1*vi+cc2_l1*n1[1]));
    F_UL[ind + 2] = NN*(0.5* FL_UL[2][2] -0.5*(el[2]*lam_l2[2]*du2+cc1_l2*vi+cc2_l2*n1[1]+cc1*vi_l2-el[2]*lam[2]));
    F_UL[ind + 3] = NN*(0.5* FL_UL[2][3] -0.5*(el[2]*lam_l3[2]*du2+cc1_l3*vi+cc2_l3*n1[1]));

    ind = StateRank*3;
    F_UL[ind + 0] = NN*(0.5* FL_UL[3][0] -0.5*(el[2]*lam_l0[2]*du3+cc1_l0*hi+cc2_l0*ucp+cc1*hi_l0+cc2*ucp_l0));
    F_UL[ind + 1] = NN*(0.5* FL_UL[3][1] -0.5*(el[2]*lam_l1[2]*du3+cc1_l1*hi+cc2_l1*ucp+cc1*hi_l1+cc2*ucp_l1));
    F_UL[ind + 2] = NN*(0.5* FL_UL[3][2] -0.5*(el[2]*lam_l2[2]*du3+cc1_l2*hi+cc2_l2*ucp+cc1*hi_l2+cc2*ucp_l2));
    F_UL[ind + 3] = NN*(0.5* FL_UL[3][3] -0.5*(el[2]*lam_l3[2]*du3+cc1_l3*hi+cc2_l3*ucp+cc1*hi_l3           -el[2]*lam[2]));
  }

  if ( F_UR != NULL ){
    F_UR[0] = NN*(0.5* FR_UR[0][0] -0.5*(el[2]*lam_r0[2]*du0+cc1_r0+el[2]*lam[2]));
    F_UR[1] = NN*(0.5* FR_UR[0][1] -0.5*(el[2]*lam_r1[2]*du0+cc1_r1));
    F_UR[2] = NN*(0.5* FR_UR[0][2] -0.5*(el[2]*lam_r2[2]*du0+cc1_r2));
    F_UR[3] = NN*(0.5* FR_UR[0][3] -0.5*(el[2]*lam_r3[2]*du0+cc1_r3));

    ind = StateRank;
    F_UR[ind + 0] = NN*(0.5* FR_UR[1][0] -0.5*(el[2]*lam_r0[2]*du1+cc1_r0*ui+cc2_r0*n1[0]+cc1*ui_r0));
    F_UR[ind + 1] = NN*(0.5* FR_UR[1][1] -0.5*(el[2]*lam_r1[2]*du1+cc1_r1*ui+cc2_r1*n1[0]+cc1*ui_r1+el[2]*lam[2]));
    F_UR[ind + 2] = NN*(0.5* FR_UR[1][2] -0.5*(el[2]*lam_r2[2]*du1+cc1_r2*ui+cc2_r2*n1[0]));
    F_UR[ind + 3] = NN*(0.5* FR_UR[1][3] -0.5*(el[2]*lam_r3[2]*du1+cc1_r3*ui+cc2_r3*n1[0]));

    ind = StateRank*2;
    F_UR[ind + 0] = NN*(0.5* FR_UR[2][0] -0.5*(el[2]*lam_r0[2]*du2+cc1_r0*vi+cc2_r0*n1[1]+cc1*vi_r0));
    F_UR[ind + 1] = NN*(0.5* FR_UR[2][1] -0.5*(el[2]*lam_r1[2]*du2+cc1_r1*vi+cc2_r1*n1[1]));
    F_UR[ind + 2] = NN*(0.5* FR_UR[2][2] -0.5*(el[2]*lam_r2[2]*du2+cc1_r2*vi+cc2_r2*n1[1]+cc1*vi_r2+el[2]*lam[2]));
    F_UR[ind + 3] = NN*(0.5* FR_UR[2][3] -0.5*(el[2]*lam_r3[2]*du2+cc1_r3*vi+cc2_r3*n1[1]));

    ind = StateRank*3;
    F_UR[ind + 0] = NN*(0.5* FR_UR[3][0] -0.5*(el[2]*lam_r0[2]*du3+cc1_r0*hi+cc2_r0*ucp+cc1*hi_r0+cc2*ucp_r0));
    F_UR[ind + 1] = NN*(0.5* FR_UR[3][1] -0.5*(el[2]*lam_r1[2]*du3+cc1_r1*hi+cc2_r1*ucp+cc1*hi_r1+cc2*ucp_r1));
    F_UR[ind + 2] = NN*(0.5* FR_UR[3][2] -0.5*(el[2]*lam_r2[2]*du3+cc1_r2*hi+cc2_r2*ucp+cc1*hi_r2+cc2*ucp_r2));
    F_UR[ind + 3] = NN*(0.5* FR_UR[3][3] -0.5*(el[2]*lam_r3[2]*du3+cc1_r3*hi+cc2_r3*ucp+cc1*hi_r3           +el[2]*lam[2]));
  }

}



int main(int argc, char **argv)
{

  int i,j,k;
  double UL[4], UR[4], n[2], gam, F[4], F_UL[16], F_UR[16];

  UL[0] = 1.0;
  UL[1] = 1.25;
  UL[2] = 0.0;
  UL[3] = 1.75;

  UR[0] = 1.1;
  UR[1] = 0.25;
  UR[2] = 0.2;
  UR[3] = 2.15;

  n[0] = 0.25;
  n[1] = 0.75;

  gam = 1.4;

  int iter = 1;
  if(argc > 1) {
    iter = atoi(argv[1]);
  }

  for( i = 0; i < iter; i++ )
  {
    if( i % 10000000 == 0 ) printf("i=%d\n", i);

    RoeFluxJacobian(UL,UR,n,gam,F,F_UL,F_UR);

  }
  for(j=0; j <= 3; j++) {
    printf("F[%d] = %f\n",j, F[j]);
    for( k=0; k <= 3; k++)
      printf( "F_UL[%d][%d] = %f\n",j,k, F_UL[j*4+k] );
    for( k=0; k <= 3; k++)
      printf( "F_UR[%d][%d] = %f\n",j,k, F_UR[j*4+k] );
  }

  return 0;

}

