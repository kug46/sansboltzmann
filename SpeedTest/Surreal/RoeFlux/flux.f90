subroutine RoeFluxJacobian(QL, QR, n, gamma, F, F_UL, F_UR)

real, intent(in) :: QL(4), QR(4), n(2), gamma
real, intent(out) :: F(4), F_UL(16), F_UR(16)

  integer i, j, ind, StateRank;
  real rhol1;
  real ul, ul_0, ul_1;
  real vl, vl_0, vl_2;
  real rhor1;
  real ur, ur_0, ur_1;
  real vr, vr_0, vr_2;
  real u2l, u2l_0, u2l_1, u2l_2;
  real u2r, u2r_0, u2r_1, u2r_2;
  real rul, rul_0, rul_1, rul_2;
  real rur, rur_0, rur_1, rur_2;
  real pl, pl_0, pl_1, pl_2, pl_3;
  real hl, hl_0, hl_1, hl_2, hl_3;
  real pr, pr_0, pr_1, pr_2, pr_3;
  real hr, hr_0, hr_1, hr_2, hr_3;

  real FL(4), FL_UL(4,4);
  real FR(4), FR_UR(4,4);
  real rum
  real rum_l0, rum_l1, rum_l2;
  real rum_r0, rum_r1, rum_r2;

  real di , di_l0 , di_r0 ;
  real d1 , d1_l0 , d1_r0 ;
  real ui , ui_l0 , ui_l1 , ui_r0 , ui_r1 ;
  real vi , vi_l0 , vi_l2 , vi_r0 , vi_r2 ;
  real hi , hi_l0 , hi_l1 , hi_l2 , hi_l3 , hi_r0 , hi_r1 , hi_r2, hi_r3;
  real af , af_l0 , af_l1 , af_l2 , af_r0 , af_r1 , af_r2 ;
  real ucp, ucp_l0, ucp_l1, ucp_l2, ucp_r0, ucp_r1, ucp_r2;
  real c2 , c2_l0 , c2_l1 , c2_l2 , c2_l3 , c2_r0 , c2_r1 , c2_r2, c2_r3;
  real ci , ci_l0 , ci_l1 , ci_l2 , ci_l3 , ci_r0 , ci_r1 , ci_r2, ci_r3;

  real du0, du1, du2, du3, ci1, c21, el(4), ep;

  real lam(4), lam_l0(4), lam_l1(4), lam_l2(4), lam_l3(4);
  real lam_r0(4), lam_r1(4), lam_r2(4), lam_r3(4);
  real lt, lt2;

  real eps , eps_l0, eps_l1, eps_l2, eps_l3;
  real eps1, eps_r0, eps_r1, eps_r2, eps_r3;

  real s1  , s1_l0  , s1_l1  , s1_l2  , s1_l3  , s1_r0  , s1_r1  , s1_r2  , s1_r3  ;
  real s2  , s2_l0  , s2_l1  , s2_l2  , s2_l3  , s2_r0  , s2_r1  , s2_r2  , s2_r3  ;
  real al1x, al1x_l0, al1x_l1, al1x_l2, al1x_l3, al1x_r0, al1x_r1, al1x_r2, al1x_r3;
  real al2x, al2x_l0, al2x_l1, al2x_l2, al2x_r0, al2x_r1, al2x_r2;
  real ct1 , ct1_l0 , ct1_l1 , ct1_l2 , ct1_l3 , ct1_r0 , ct1_r1 , ct1_r2 , ct1_r3 ;
  real ct2 , ct2_l0 , ct2_l1 , ct2_l2 , ct2_l3 , ct2_r0 , ct2_r1 , ct2_r2 , ct2_r3 ;
  real cc1 , cc1_l0 , cc1_l1 , cc1_l2 , cc1_l3 , cc1_r0 , cc1_r1 , cc1_r2 , cc1_r3 ;
  real cc2 , cc2_l0 , cc2_l1 , cc2_l2 , cc2_l3 , cc2_r0 , cc2_r1 , cc2_r2 , cc2_r3 ;

  real gmi;
  real NN, NN1, n1(3);

  real tmp, tmp_UL(4), tmp_UR(4);
  real c21_l0, c21_l1, c21_l2, c21_l3;
  real c21_r0, c21_r1, c21_r2, c21_r3;
  real ci1_l0, ci1_l1, ci1_l2, ci1_l3;
  real ci1_r0, ci1_r1, ci1_r2, ci1_r3;
  real tmp1, tmp1_UL(4), tmp1_UR(4);
  real tmp2, tmp2_UL(4), tmp2_UR(4);

  ! initialize flux
  do i=1,4
    FL(i) = 0.0;
    FR(i) = 0.0;
    do j=1, 4
      FL_UL(i,j) = 0.0;
      FR_UR(i,j) = 0.0;
    enddo
  enddo

  StateRank = 4;

  gmi  = gamma - 1;
  NN = sqrt(n(1)*n(1) + n(2)*n(2));

  NN1 = 1.0/NN;

  n1(1) = n(1)*NN1;
  n1(2) = n(2)*NN1;


  ! Left State
  rhol1 = 1/QL(1);

  ul    = QL(2)*rhol1;
  ul_0  =   -ul*rhol1;
  ul_1  =       rhol1;

  vl    = QL(3)*rhol1;
  vl_0  =   -vl*rhol1;
  vl_2  =       rhol1;

  u2l   = (ul*ul + vl*vl)*QL(1);
  u2l_0 =  -u2l*rhol1;
  u2l_1 = 2*ul;
  u2l_2 = 2*vl;

  rul   = (ul  *n1(1) + vl  *n1(2));
  rul_0 = (ul_0*n1(1) + vl_0*n1(2));
  rul_1 =  ul_1*n1(1)              ;
  rul_2 =               vl_2*n1(2) ;

  pl    = (QL(4) - 0.5*u2l  )*gmi;
  pl_0  = (      - 0.5*u2l_0)*gmi;
  pl_1  = (      - 0.5*u2l_1)*gmi;
  pl_2  = (      - 0.5*u2l_2)*gmi;
  pl_3  = (1.0              )*gmi;

  hl    = (QL(4) + pl  )*rhol1;
  hl_0  = (      + pl_0)*rhol1-hl*rhol1;
  hl_1  = (      + pl_1)*rhol1;
  hl_2  = (      + pl_2)*rhol1;
  hl_3  = (1.0   + pl_3)*rhol1;

  FL   (1)    = QL(1)*rul;
  FL_UL(1,1) =       rul + QL(1)*rul_0;
  FL_UL(1,2) =             QL(1)*rul_1;
  FL_UL(1,3) =             QL(1)*rul_2;
  FL_UL(1,4) = 0.0;

  FL   (2)   = n1(1)*pl   + QL(2)*rul;
  FL_UL(2,1) = n1(1)*pl_0 + QL(2)*rul_0;
  FL_UL(2,2) = n1(1)*pl_1 + QL(2)*rul_1 + rul;
  FL_UL(2,3) = n1(1)*pl_2 + QL(2)*rul_2;
  FL_UL(2,4) = n1(1)*pl_3              ;

  FL   (3)   = n1(2)*pl   + QL(3)*rul;
  FL_UL(3,1) = n1(2)*pl_0 + QL(3)*rul_0;
  FL_UL(3,2) = n1(2)*pl_1 + QL(3)*rul_1;
  FL_UL(3,3) = n1(2)*pl_2 + QL(3)*rul_2 + rul;
  FL_UL(3,4) = n1(2)*pl_3              ;

  FL   (4)   = (pl   + QL(4))*rul;
  FL_UL(4,1) = (pl_0        )*rul + (pl + QL(4))*rul_0;
  FL_UL(4,2) = (pl_1        )*rul + (pl + QL(4))*rul_1;
  FL_UL(4,3) = (pl_2        )*rul + (pl + QL(4))*rul_2;
  FL_UL(4,4) = (pl_3 + 1.0  )*rul                     ;

  ! Right State
  rhor1 = 1/QR(1);

  ur    = QR(2)*rhor1;
  ur_0  =   -ur*rhor1;
  ur_1  =       rhor1;

  vr    = QR(3)*rhor1;
  vr_0  =   -vr*rhor1;
  vr_2  =       rhor1;

  u2r   = (ur*ur + vr*vr)*QR(1);
  u2r_0 =  -u2r*rhor1;
  u2r_1 = 2*ur;
  u2r_2 = 2*vr;

  rur   = (ur  *n1(1) + vr  *n1(2));
  rur_0 = (ur_0*n1(1) + vr_0*n1(2));
  rur_1 =  ur_1*n1(1)              ;
  rur_2 =               vr_2*n1(2) ;

  pr    = (QR(4) - 0.5*u2r  )*gmi;
  pr_0  = (      - 0.5*u2r_0)*gmi;
  pr_1  = (      - 0.5*u2r_1)*gmi;
  pr_2  = (      - 0.5*u2r_2)*gmi;
  pr_3  = (1.0              )*gmi;

  hr    = (QR(4) + pr  )*rhor1;
  hr_0  = (      + pr_0)*rhor1-hr*rhor1;
  hr_1  = (      + pr_1)*rhor1;
  hr_2  = (      + pr_2)*rhor1;
  hr_3  = (1.0   + pr_3)*rhor1;

  FR   (1)   = QR(1)*rur;
  FR_UR(1,1) =       rur + QR(1)*rur_0;
  FR_UR(1,2) =             QR(1)*rur_1;
  FR_UR(1,3) =             QR(1)*rur_2;
  FR_UR(1,4) = 0.0;

  FR   (2)   = n1(1)*pr   + QR(2)*rur;
  FR_UR(2,1) = n1(1)*pr_0 + QR(2)*rur_0;
  FR_UR(2,2) = n1(1)*pr_1 + QR(2)*rur_1 + rur;
  FR_UR(2,3) = n1(1)*pr_2 + QR(2)*rur_2;
  FR_UR(2,4) = n1(1)*pr_3              ;

  FR   (3)   = n1(2)*pr   + QR(3)*rur;
  FR_UR(3,1) = n1(2)*pr_0 + QR(3)*rur_0;
  FR_UR(3,2) = n1(2)*pr_1 + QR(3)*rur_1;
  FR_UR(3,3) = n1(2)*pr_2 + QR(3)*rur_2 + rur;
  FR_UR(3,4) = n1(2)*pr_3              ;

  FR   (4)   = (pr   + QR(4))*rur;
  FR_UR(4,1) = (pr_0        )*rur + (pr + QR(4))*rur_0;
  FR_UR(4,2) = (pr_1        )*rur + (pr + QR(4))*rur_1;
  FR_UR(4,3) = (pr_2        )*rur + (pr + QR(4))*rur_2;
  FR_UR(4,4) = (pr_3 + 1.0  )*rur                     ;


  ! Average state
  di     = sqrt(QR(1)*rhol1);
  di_l0  = -0.5*di*rhol1;
  di_r0  =  0.5*di*rhor1;

  d1     = 1.0/(1.0+di);
  d1_l0  = -di_l0*d1*d1;
  d1_r0  = -di_r0*d1*d1;

  ui     = (di   *ur+   ul  )*d1;
  ui_l0  = (di_l0*ur+   ul_0)*d1+(di*ur+ul)*d1_l0;
  ui_l1  = (            ul_1)*d1                 ;
  ui_r0  = (di_r0*ur+di*ur_0)*d1+(di*ur+ul)*d1_r0;
  ui_r1  = (         di*ur_1)*d1                 ;

  vi     = (di   *vr+   vl  )*d1;
  vi_l0  = (di_l0*vr+   vl_0)*d1+(di*vr+vl)*d1_l0;
  vi_l2  = (            vl_2)*d1                 ;
  vi_r0  = (di_r0*vr+di*vr_0)*d1+(di*vr+vl)*d1_r0;
  vi_r2  = (         di*vr_2)*d1                 ;

  hi     = (di*hr+hl)*d1;
  hi_l0  = (di_l0*hr+   hl_0)*d1+(di*hr+hl)*d1_l0;
  hi_l1  = (            hl_1)*d1;
  hi_l2  = (            hl_2)*d1;
  hi_l3  = (            hl_3)*d1;
  hi_r0  = (di_r0*hr+di*hr_0)*d1+(di*hr+hl)*d1_r0;
  hi_r1  = (         di*hr_1)*d1;
  hi_r2  = (         di*hr_2)*d1;
  hi_r3  = (         di*hr_3)*d1;

  af     = 0.5*(ui*ui   +vi*vi   );
  af_l0  =      ui*ui_l0+vi*vi_l0;
  af_l1  =      ui*ui_l1         ;
  af_l2  =               vi*vi_l2;
  af_r0  =      ui*ui_r0+vi*vi_r0;
  af_r1  =      ui*ui_r1         ;
  af_r2  =               vi*vi_r2;

  ucp    = ui   *n1(1)+vi   *n1(2);
  ucp_l0 = ui_l0*n1(1)+vi_l0*n1(2);
  ucp_l1 = ui_l1*n1(1)            ;
  ucp_l2 =             vi_l2*n1(2);
  ucp_r0 = ui_r0*n1(1)+vi_r0*n1(2);
  ucp_r1 = ui_r1*n1(1)            ;
  ucp_r2 =             vi_r2*n1(2);

  c2     = gmi*(hi   -af   );
  c2_l0  = gmi*(hi_l0-af_l0);
  c2_l1  = gmi*(hi_l1-af_l1);
  c2_l2  = gmi*(hi_l2-af_l2);
  c2_l3  = gmi*(hi_l3      );
  c2_r0  = gmi*(hi_r0-af_r0);
  c2_r1  = gmi*(hi_r1-af_r1);
  c2_r2  = gmi*(hi_r2-af_r2);
  c2_r3  = gmi*(hi_r3      );


  ci    = sqrt(c2);
  ci1   = 1/ci;
  ci_l0 = 0.5*ci1*c2_l0;
  ci_l1 = 0.5*ci1*c2_l1;
  ci_l2 = 0.5*ci1*c2_l2;
  ci_l3 = 0.5*ci1*c2_l3;
  ci_r0 = 0.5*ci1*c2_r0;
  ci_r1 = 0.5*ci1*c2_r1;
  ci_r2 = 0.5*ci1*c2_r2;
  ci_r3 = 0.5*ci1*c2_r3;

  ci1_l0 = -ci_l0*(ci1*ci1);
  ci1_l1 = -ci_l1*(ci1*ci1);
  ci1_l2 = -ci_l2*(ci1*ci1);
  ci1_l3 = -ci_l3*(ci1*ci1);
  ci1_r0 = -ci_r0*(ci1*ci1);
  ci1_r1 = -ci_r1*(ci1*ci1);
  ci1_r2 = -ci_r2*(ci1*ci1);
  ci1_r3 = -ci_r3*(ci1*ci1);

  ! du = QR-QL
  du0 = QR(1) - QL(1);
  du1 = QR(2) - QL(2);
  du2 = QR(3) - QL(3);
  du3 = QR(4) - QL(4);

  ! eigenvalues
  lam   (1) = ucp    + ci;
  lam_l0(1) = ucp_l0 + ci_l0;
  lam_l1(1) = ucp_l1 + ci_l1;
  lam_l2(1) = ucp_l2 + ci_l2;
  lam_l3(1) =          ci_l3;
  lam_r0(1) = ucp_r0 + ci_r0;
  lam_r1(1) = ucp_r1 + ci_r1;
  lam_r2(1) = ucp_r2 + ci_r2;
  lam_r3(1) =          ci_r3;

  lam   (2) = ucp    - ci;
  lam_l0(2) = ucp_l0 - ci_l0;
  lam_l1(2) = ucp_l1 - ci_l1;
  lam_l2(2) = ucp_l2 - ci_l2;
  lam_l3(2) =        - ci_l3;
  lam_r0(2) = ucp_r0 - ci_r0;
  lam_r1(2) = ucp_r1 - ci_r1;
  lam_r2(2) = ucp_r2 - ci_r2;
  lam_r3(2) =        - ci_r3;

  lam   (3) = ucp ;
  lam_l0(3) = ucp_l0;
  lam_l1(3) = ucp_l1;
  lam_l2(3) = ucp_l2;
  lam_l3(3) = 0.0   ;
  lam_r0(3) = ucp_r0;
  lam_r1(3) = ucp_r1;
  lam_r2(3) = ucp_r2;
  lam_r3(3) = 0.0   ;

  ! Entropy fix
  ep     = 1e-2;
  eps    = ep*ci;
  eps_l0 = ep*ci_l0;
  eps_l1 = ep*ci_l1;
  eps_l2 = ep*ci_l2;
  eps_l3 = ep*ci_l3;
  eps_r0 = ep*ci_r0;
  eps_r1 = ep*ci_r1;
  eps_r2 = ep*ci_r2;
  eps_r3 = ep*ci_r3;

  do i=1, 3
    if ( (lam(i) .lt. eps) .and. (lam(i).gt.-eps)) then
      eps1 = 1/eps;
      lt = lam(i);
      lt2 = 0.5*lt*lt;
      lam_l0(i) = 0.5*eps_l0 + (lt*lam_l0(i)-lt2*eps1*eps_l0)*eps1;
      lam_l1(i) = 0.5*eps_l1 + (lt*lam_l1(i)-lt2*eps1*eps_l1)*eps1;
      lam_l2(i) = 0.5*eps_l2 + (lt*lam_l2(i)-lt2*eps1*eps_l2)*eps1;
      lam_l3(i) = 0.5*eps_l3 + (lt*lam_l3(i)-lt2*eps1*eps_l3)*eps1;
      lam_r0(i) = 0.5*eps_r0 + (lt*lam_r0(i)-lt2*eps1*eps_r0)*eps1;
      lam_r1(i) = 0.5*eps_r1 + (lt*lam_r1(i)-lt2*eps1*eps_r1)*eps1;
      lam_r2(i) = 0.5*eps_r2 + (lt*lam_r2(i)-lt2*eps1*eps_r2)*eps1;
      lam_r3(i) = 0.5*eps_r3 + (lt*lam_r3(i)-lt2*eps1*eps_r3)*eps1;

      lam(i) = 0.5*(eps+lt*lt*eps1);
    endif
  enddo

  do i=1,3
    if (lam(i)<0) then
      el(i) = -1;
    else
      el(i) =  1;
    endif
  enddo

  s1    = 0.5*(el(1)*lam   (1)+el(2)*lam   (2));
  s1_l0 = 0.5*(el(1)*lam_l0(1)+el(2)*lam_l0(2));
  s1_l1 = 0.5*(el(1)*lam_l1(1)+el(2)*lam_l1(2));
  s1_l2 = 0.5*(el(1)*lam_l2(1)+el(2)*lam_l2(2));
  s1_l3 = 0.5*(el(1)*lam_l3(1)+el(2)*lam_l3(2));
  s1_r0 = 0.5*(el(1)*lam_r0(1)+el(2)*lam_r0(2));
  s1_r1 = 0.5*(el(1)*lam_r1(1)+el(2)*lam_r1(2));
  s1_r2 = 0.5*(el(1)*lam_r2(1)+el(2)*lam_r2(2));
  s1_r3 = 0.5*(el(1)*lam_r3(1)+el(2)*lam_r3(2));

  s2    = 0.5*(el(1)*lam   (1)-el(2)*lam   (2));
  s2_l0 = 0.5*(el(1)*lam_l0(1)-el(2)*lam_l0(2));
  s2_l1 = 0.5*(el(1)*lam_l1(1)-el(2)*lam_l1(2));
  s2_l2 = 0.5*(el(1)*lam_l2(1)-el(2)*lam_l2(2));
  s2_l3 = 0.5*(el(1)*lam_l3(1)-el(2)*lam_l3(2));
  s2_r0 = 0.5*(el(1)*lam_r0(1)-el(2)*lam_r0(2));
  s2_r1 = 0.5*(el(1)*lam_r1(1)-el(2)*lam_r1(2));
  s2_r2 = 0.5*(el(1)*lam_r2(1)-el(2)*lam_r2(2));
  s2_r3 = 0.5*(el(1)*lam_r3(1)-el(2)*lam_r3(2));

  al1x    = gmi*(af   *du0-ui   *du1-vi   *du2+du3);
  al1x_l0 = gmi*(af_l0*du0-ui_l0*du1-vi_l0*du2- af  );
  al1x_l1 = gmi*(af_l1*du0-ui_l1*du1            + ui  );
  al1x_l2 = gmi*(af_l2*du0            -vi_l2*du2+ vi  );
  al1x_l3 = gmi*(                                   - 1.0 );
  al1x_r0 = gmi*(af_r0*du0-ui_r0*du1-vi_r0*du2+ af  );
  al1x_r1 = gmi*(af_r1*du0-ui_r1*du1            - ui  );
  al1x_r2 = gmi*(af_r2*du0            -vi_r2*du2- vi  );
  al1x_r3 = gmi*(                                   + 1.0 );

  al2x    = -ucp   *du0+du1*n1(1)+du2*n1(2);
  al2x_l0 = -ucp_l0*du0+ucp                    ;
  al2x_l1 = -ucp_l1*du0-      n1(1)            ;
  al2x_l2 = -ucp_l2*du0            -      n1(2);
  al2x_r0 = -ucp_r0*du0-ucp                    ;
  al2x_r1 = -ucp_r1*du0+      n1(1)            ;
  al2x_r2 = -ucp_r2*du0            +      n1(2);

  c21    = ci1*ci1;
  c21_l0 = 2.0*ci1*ci1_l0;
  c21_l1 = 2.0*ci1*ci1_l1;
  c21_l2 = 2.0*ci1*ci1_l2;
  c21_l3 = 2.0*ci1*ci1_l3;

  c21_r0 = 2.0*ci1*ci1_r0;
  c21_r1 = 2.0*ci1*ci1_r1;
  c21_r2 = 2.0*ci1*ci1_r2;
  c21_r3 = 2.0*ci1*ci1_r3;


  ct1    = (s2   *al1x            *ci1);
  ct1_l0 = (s2_l0*al1x+s2*al1x_l0)*ci1+s2*al1x*ci1_l0;
  ct1_l1 = (s2_l1*al1x+s2*al1x_l1)*ci1+s2*al1x*ci1_l1;
  ct1_l2 = (s2_l2*al1x+s2*al1x_l2)*ci1+s2*al1x*ci1_l2;
  ct1_l3 = (s2_l3*al1x+s2*al1x_l3)*ci1+s2*al1x*ci1_l3;
  ct1_r0 = (s2_r0*al1x+s2*al1x_r0)*ci1+s2*al1x*ci1_r0;
  ct1_r1 = (s2_r1*al1x+s2*al1x_r1)*ci1+s2*al1x*ci1_r1;
  ct1_r2 = (s2_r2*al1x+s2*al1x_r2)*ci1+s2*al1x*ci1_r2;
  ct1_r3 = (s2_r3*al1x+s2*al1x_r3)*ci1+s2*al1x*ci1_r3;


  ct2    = (s2   *al2x            *ci1);
  ct2_l0 = (s2_l0*al2x+s2*al2x_l0)*ci1+s2*al2x*ci1_l0;
  ct2_l1 = (s2_l1*al2x+s2*al2x_l1)*ci1+s2*al2x*ci1_l1;
  ct2_l2 = (s2_l2*al2x+s2*al2x_l2)*ci1+s2*al2x*ci1_l2;
  ct2_l3 = (s2_l3*al2x           )*ci1+s2*al2x*ci1_l3;
  ct2_r0 = (s2_r0*al2x+s2*al2x_r0)*ci1+s2*al2x*ci1_r0;
  ct2_r1 = (s2_r1*al2x+s2*al2x_r1)*ci1+s2*al2x*ci1_r1;
  ct2_r2 = (s2_r2*al2x+s2*al2x_r2)*ci1+s2*al2x*ci1_r2;
  ct2_r3 = (s2_r3*al2x           )*ci1+s2*al2x*ci1_r3;


  cc1    = ct2   +(s1   -el(3)*lam   (3))*al1x*c21;
  cc1_l0 = ct2_l0+(s1_l0-el(3)*lam_l0(3))*al1x*c21+(s1-el(3)*lam(3))*(al1x_l0*c21+al1x*c21_l0);
  cc1_l1 = ct2_l1+(s1_l1-el(3)*lam_l1(3))*al1x*c21+(s1-el(3)*lam(3))*(al1x_l1*c21+al1x*c21_l1);
  cc1_l2 = ct2_l2+(s1_l2-el(3)*lam_l2(3))*al1x*c21+(s1-el(3)*lam(3))*(al1x_l2*c21+al1x*c21_l2);
  cc1_l3 = ct2_l3+(s1_l3-el(3)*lam_l3(3))*al1x*c21+(s1-el(3)*lam(3))*(al1x_l3*c21+al1x*c21_l3);
  cc1_r0 = ct2_r0+(s1_r0-el(3)*lam_r0(3))*al1x*c21+(s1-el(3)*lam(3))*(al1x_r0*c21+al1x*c21_r0);
  cc1_r1 = ct2_r1+(s1_r1-el(3)*lam_r1(3))*al1x*c21+(s1-el(3)*lam(3))*(al1x_r1*c21+al1x*c21_r1);
  cc1_r2 = ct2_r2+(s1_r2-el(3)*lam_r2(3))*al1x*c21+(s1-el(3)*lam(3))*(al1x_r2*c21+al1x*c21_r2);
  cc1_r3 = ct2_r3+(s1_r3-el(3)*lam_r3(3))*al1x*c21+(s1-el(3)*lam(3))*(al1x_r3*c21+al1x*c21_r3);

  cc2    = ct1   +(s1   -el(3)*lam   (3))*al2x;
  cc2_l0 = ct1_l0+(s1_l0-el(3)*lam_l0(3))*al2x+(s1-el(3)*lam(3))*(al2x_l0);
  cc2_l1 = ct1_l1+(s1_l1-el(3)*lam_l1(3))*al2x+(s1-el(3)*lam(3))*(al2x_l1);
  cc2_l2 = ct1_l2+(s1_l2-el(3)*lam_l2(3))*al2x+(s1-el(3)*lam(3))*(al2x_l2);
  cc2_l3 = ct1_l3+(s1_l3-el(3)*lam_l3(3))*al2x;
  cc2_r0 = ct1_r0+(s1_r0-el(3)*lam_r0(3))*al2x+(s1-el(3)*lam(3))*(al2x_r0);
  cc2_r1 = ct1_r1+(s1_r1-el(3)*lam_r1(3))*al2x+(s1-el(3)*lam(3))*(al2x_r1);
  cc2_r2 = ct1_r2+(s1_r2-el(3)*lam_r2(3))*al2x+(s1-el(3)*lam(3))*(al2x_r2);
  cc2_r3 = ct1_r3+(s1_r3-el(3)*lam_r3(3))*al2x;

  F   (1)    = NN*(0.5*(FL(1)+FR(1))-0.5*(el(3)*lam   (3)*du0+cc1   ));
  F   (2)    = NN*(0.5*(FL(2)+FR(2))-0.5*(el(3)*lam   (3)*du1+cc1   *ui+cc2   *n1(1)));
  F   (3)    = NN*(0.5*(FL(3)+FR(3))-0.5*(el(3)*lam   (3)*du2+cc1   *vi+cc2   *n1(2)));
  F   (4)    = NN*(0.5*(FL(4)+FR(4))-0.5*(el(3)*lam   (3)*du3+cc1   *hi+cc2   *ucp  ));

  F_UL(1) = NN*(0.5* FL_UL(1,1) -0.5*(el(3)*lam_l0(3)*du0+cc1_l0-el(3)*lam(3)));
  F_UL(2) = NN*(0.5* FL_UL(1,2) -0.5*(el(3)*lam_l1(3)*du0+cc1_l1));
  F_UL(3) = NN*(0.5* FL_UL(1,3) -0.5*(el(3)*lam_l2(3)*du0+cc1_l2));
  F_UL(4) = NN*(0.5* FL_UL(1,4) -0.5*(el(3)*lam_l3(3)*du0+cc1_l3));

  ind = StateRank;
  F_UL(ind + 1) = NN*(0.5* FL_UL(2,1) -0.5*(el(3)*lam_l0(3)*du1+cc1_l0*ui+cc2_l0*n1(1)+cc1*ui_l0));
  F_UL(ind + 2) = NN*(0.5* FL_UL(2,2) -0.5*(el(3)*lam_l1(3)*du1+cc1_l1*ui+cc2_l1*n1(1)+cc1*ui_l1-el(3)*lam(3)));
  F_UL(ind + 3) = NN*(0.5* FL_UL(2,3) -0.5*(el(3)*lam_l2(3)*du1+cc1_l2*ui+cc2_l2*n1(1)));
  F_UL(ind + 4) = NN*(0.5* FL_UL(2,4) -0.5*(el(3)*lam_l3(3)*du1+cc1_l3*ui+cc2_l3*n1(1)));

  ind = StateRank*2;
  F_UL(ind + 1) = NN*(0.5* FL_UL(3,1) -0.5*(el(3)*lam_l0(3)*du2+cc1_l0*vi+cc2_l0*n1(2)+cc1*vi_l0));
  F_UL(ind + 2) = NN*(0.5* FL_UL(3,2) -0.5*(el(3)*lam_l1(3)*du2+cc1_l1*vi+cc2_l1*n1(2)));
  F_UL(ind + 3) = NN*(0.5* FL_UL(3,3) -0.5*(el(3)*lam_l2(3)*du2+cc1_l2*vi+cc2_l2*n1(2)+cc1*vi_l2-el(3)*lam(3)));
  F_UL(ind + 4) = NN*(0.5* FL_UL(3,4) -0.5*(el(3)*lam_l3(3)*du2+cc1_l3*vi+cc2_l3*n1(2)));

  ind = StateRank*3;
  F_UL(ind + 1) = NN*(0.5* FL_UL(4,1) -0.5*(el(3)*lam_l0(3)*du3+cc1_l0*hi+cc2_l0*ucp+cc1*hi_l0+cc2*ucp_l0));
  F_UL(ind + 2) = NN*(0.5* FL_UL(4,2) -0.5*(el(3)*lam_l1(3)*du3+cc1_l1*hi+cc2_l1*ucp+cc1*hi_l1+cc2*ucp_l1));
  F_UL(ind + 3) = NN*(0.5* FL_UL(4,3) -0.5*(el(3)*lam_l2(3)*du3+cc1_l2*hi+cc2_l2*ucp+cc1*hi_l2+cc2*ucp_l2));
  F_UL(ind + 4) = NN*(0.5* FL_UL(4,4) -0.5*(el(3)*lam_l3(3)*du3+cc1_l3*hi+cc2_l3*ucp+cc1*hi_l3           -el(3)*lam(3)));


  F_UR(1) = NN*(0.5* FR_UR(1,1) -0.5*(el(3)*lam_r0(3)*du0+cc1_r0+el(3)*lam(3)));
  F_UR(2) = NN*(0.5* FR_UR(1,2) -0.5*(el(3)*lam_r1(3)*du0+cc1_r1));
  F_UR(3) = NN*(0.5* FR_UR(1,3) -0.5*(el(3)*lam_r2(3)*du0+cc1_r2));
  F_UR(4) = NN*(0.5* FR_UR(1,4) -0.5*(el(3)*lam_r3(3)*du0+cc1_r3));

  ind = StateRank;
  F_UR(ind + 1) = NN*(0.5* FR_UR(2,1) -0.5*(el(3)*lam_r0(3)*du1+cc1_r0*ui+cc2_r0*n1(1)+cc1*ui_r0));
  F_UR(ind + 2) = NN*(0.5* FR_UR(2,2) -0.5*(el(3)*lam_r1(3)*du1+cc1_r1*ui+cc2_r1*n1(1)+cc1*ui_r1+el(3)*lam(3)));
  F_UR(ind + 3) = NN*(0.5* FR_UR(2,3) -0.5*(el(3)*lam_r2(3)*du1+cc1_r2*ui+cc2_r2*n1(1)));
  F_UR(ind + 4) = NN*(0.5* FR_UR(2,4) -0.5*(el(3)*lam_r3(3)*du1+cc1_r3*ui+cc2_r3*n1(1)));

  ind = StateRank*2;
  F_UR(ind + 1) = NN*(0.5* FR_UR(3,1) -0.5*(el(3)*lam_r0(3)*du2+cc1_r0*vi+cc2_r0*n1(2)+cc1*vi_r0));
  F_UR(ind + 2) = NN*(0.5* FR_UR(3,2) -0.5*(el(3)*lam_r1(3)*du2+cc1_r1*vi+cc2_r1*n1(2)));
  F_UR(ind + 3) = NN*(0.5* FR_UR(3,3) -0.5*(el(3)*lam_r2(3)*du2+cc1_r2*vi+cc2_r2*n1(2)+cc1*vi_r2+el(3)*lam(3)));
  F_UR(ind + 4) = NN*(0.5* FR_UR(3,4) -0.5*(el(3)*lam_r3(3)*du2+cc1_r3*vi+cc2_r3*n1(2)));

  ind = StateRank*3;
  F_UR(ind + 1) = NN*(0.5* FR_UR(4,1) -0.5*(el(3)*lam_r0(3)*du3+cc1_r0*hi+cc2_r0*ucp+cc1*hi_r0+cc2*ucp_r0));
  F_UR(ind + 2) = NN*(0.5* FR_UR(4,2) -0.5*(el(3)*lam_r1(3)*du3+cc1_r1*hi+cc2_r1*ucp+cc1*hi_r1+cc2*ucp_r1));
  F_UR(ind + 3) = NN*(0.5* FR_UR(4,3) -0.5*(el(3)*lam_r2(3)*du3+cc1_r2*hi+cc2_r2*ucp+cc1*hi_r2+cc2*ucp_r2));
  F_UR(ind + 4) = NN*(0.5* FR_UR(4,4) -0.5*(el(3)*lam_r3(3)*du3+cc1_r3*hi+cc2_r3*ucp+cc1*hi_r3           +el(3)*lam(3)));


end subroutine RoeFluxJacobian

