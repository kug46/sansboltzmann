program surreally

character(1024) :: argStr

integer :: i,j,k,iter,argLen;
real :: UL(4), UR(4), n(2), gam, F(4), F_UL(16), F_UR(16);

UL(1) = 1.0;
UL(2) = 1.25;
UL(3) = 0.0;
UL(4) = 1.75;

UR(1) = 1.1;
UR(2) = 0.25;
UR(3) = 0.2;
UR(4) = 2.15;

n(1) = 0.25;
n(2) = 0.75;

gam = 1.4;

iter = 1
if(command_argument_count() > 0) then
    call get_command_argument(1, argStr, argLen)
    read (argStr,*) iter
end if

do i = 1, iter

  !if( i % 10000000 == 0 ) printf("i=%d\n", i);

  call RoeFluxJacobian(UL,UR,n,gam,F,F_UL,F_UR);

enddo
do k=1,4
  print *, "F = ", F(k)
  do j=1,4
    print *, "F_UL = ", F_UL(j*4+k)
  enddo
  do j=1,4
    print *, "F_UR = ", F_UR(j*4+k)
  enddo
enddo


end program surreally

