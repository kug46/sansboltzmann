#!/usr/bin/env python
import os
import subprocess
import time

def run(path, nIter):
    cmd = "%s %d" % (path, nIter)
    t0 = time.time()
    p = subprocess.Popen(cmd, shell=True)
    pid, sts = os.waitpid(p.pid, 0)
    t1 = time.time()
    return t1 - t0

tools = "../../../src/tools/SANSException.cpp ../../../src/tools/BackTraceException.cpp"

compiler = []
cppcompiler = []
fcompiler = []
ccompiler = []

compiler.append( "GNU 4.8" )
cppcompiler.append( "g++-4.8 -std=c++11 -O3 -fstrict-aliasing -Wstrict-aliasing -Wall -Dnderiv=8" )
fcompiler.append( "gfortran-4.8 -O3 -fdefault-real-8" )
ccompiler.append( "gcc-4.8 -O3 -fstrict-aliasing -Wstrict-aliasing" )

compiler.append( "Intel 14" )
#cppcompiler = "icpc -finline-functions -inline-level=2 -inline-forceinline -O3 -fstrict-aliasing -ansi-alias-check -std=c++11 -Wall -Dnderiv=8"
cppcompiler.append( "icpc -O3 -fstrict-aliasing -ansi-alias-check -std=c++11 -Wall -Dnderiv=8" )
fcompiler.append( "ifort -O3 -r8" )
ccompiler.append( "icc -O3 -fstrict-aliasing -ansi-alias-check" )

compiler.append( "Clang 3.4" )
cppcompiler.append( "clang++-3.4 -O3 -std=c++11 -fstrict-aliasing -Wstrict-aliasing -Wall -Dnderiv=8" )
fcompiler.append( "gfortran -O3 -fdefault-real-8" )
ccompiler.append( "clang-3.4 -O3 -fstrict-aliasing -Wstrict-aliasing" )

Surreals = ["TRAD", "RVO", "LAZY"]
Surreals = ["TRAD", "LAZY"]
Surreals = ["REVERSE"]


for c in [0,1,2]:
    for Surreal in Surreals:
        print( "Compiling: " + compiler[c] + " Surreal: " + Surreal)
        
        # Build executables.
        #print( "Compiling RN_test" )
        #p = subprocess.Popen(cppcompiler + " surreal_test_case2.cpp NR_Surreal_Flux.cpp -o RN_test", shell=True)
        #pid, sts = os.waitpid(p.pid, 0)
        print( "Compiling SANS_dynb" )
        p = subprocess.Popen(cppcompiler[c] + " SANS_SurrealD_test_case2.cpp SANS_SurrealD_Flux.cpp " + tools + " -I ../../../src/ -DSURREAL_" + Surreal + " -DSURREALB -I$BOOST_ROOT/include/boost-1_54/ -o SANS_dynb", shell=True)
        pid, sts = os.waitpid(p.pid, 0)
        print( "Compiling SANS_fixed" )
        p = subprocess.Popen(cppcompiler[c] + " SANS_SurrealS_test_case2.cpp SANS_SurrealS_Flux.cpp " + tools + " -I ../../../src/ -DSURREAL_" + Surreal + " -I$BOOST_ROOT/include/boost-1_54/ -o SANS_fixed", shell=True)
        pid, sts = os.waitpid(p.pid, 0)
        print( "Compiling ftest" )
        p = subprocess.Popen(fcompiler[c] + " surreal_test_case.f90 flux.f90 -o ftest", shell=True)
        pid, sts = os.waitpid(p.pid, 0)
        print( "Compiling ctest" )
        p = subprocess.Popen(ccompiler[c] + " surreal_test_case.c -o ctest -lm", shell=True)
        pid, sts = os.waitpid(p.pid, 0)
        
        print( "Running tests" )
        
        xtimes = []
        def Increasing():
            # Run each executable at a growing iteration count.
            baseIter = 1000000
            for iter in (baseIter, baseIter*10, baseIter*100):
                #SANS_dyn = run('./SANS_dyn > /dev/null', iter)
                SANS_dynb = run('./SANS_dynb > /dev/null', iter)
                SANS_fixed = run('./SANS_fixed > /dev/null', iter)
                #RN = run('./RN_test > /dev/null', iter)
                ct = run('./ctest > /dev/null', iter)
                ft = run('./ftest > /dev/null', iter)
                xtimes.append({'iter':iter, 'c':ct, 'f':ft, 'dynb':SANS_dynb, 'fix':SANS_fixed})
        
        def Repeated():
            # Run each executable a number of times to get average timing
            runs = 1
            iter = 25000000
            SANS_dynb = 0
            SANS_fixed = 0
            ct = 0
            ft = 0
            for runIter in xrange(0,runs):
                #SANS_dyn = run('./SANS_dyn > /dev/null', iter)
                SANS_dynb += run('./SANS_dynb > /dev/null', iter)
                SANS_fixed += run('./SANS_fixed > /dev/null', iter)
                #RN = run('./RN_test > /dev/null', iter)
                ct += run('./ctest > /dev/null', iter)
                ft += run('./ftest > /dev/null', iter)
            
            SANS_dynb /= runs
            SANS_fixed /= runs
            ct /= runs
            ft /= runs
        
            xtimes.append({'iter':iter, 'c':ct, 'f':ft, 'dynb':SANS_dynb, 'fix':SANS_fixed})
        
        #Increasing()
        Repeated()
        
        # Print results.
        print "       Iter    C(s)    F(s)    fix(s)   dynb(s)   fix/C ratio  dynb/C ratio  fix/F ratio  dynb/F ratio"
        for i in range(len(xtimes)):
            print "%11d %7.2f %7.2f %9.2f %9.2f %13.2f %13.2f %12.2f %13.2f" % ( \
                                         xtimes[i]['iter'], \
                                         xtimes[i]['c'], \
                                         xtimes[i]['f'], \
                                         xtimes[i]['fix'], \
                                         xtimes[i]['dynb'], \
                                         xtimes[i]['fix']/xtimes[i]['c'], \
                                         xtimes[i]['dynb']/xtimes[i]['c'], \
                                         xtimes[i]['fix']/xtimes[i]['f'], \
                                         xtimes[i]['dynb']/xtimes[i]['f'])
