#include <iostream>
#include <stdlib.h>

#include "Surreal/SurrealS.h"

#ifndef nderiv
#define nderiv 4
#endif

using namespace std;

template<class T>
__attribute__((noinline)) void flux( const T *q, T *f, double nx, double ny, double gam, double l);

int main(int argc, char **argv)
{
//  if( argc == 1 )
//  {
//    std::cout << "Please execute as " << argv[0] << " 1" << std::endl;
//    return 1;
//  }

  SurrealS<nderiv> q[4], f[4];
  double nx = 0.25, ny = 0.75, gam = 1.4, l = 1.0;

  q[0] = 1.0;
  q[1] = 1.25;
  q[2] = 0.0;
  q[3] = 1.75;

  int iter = 1;
  if(argc > 1) {
      iter = atoi(argv[1]);
  }

  for(int i = 0; i < iter; i++) { //10000000
    for(int j = 0; j < 4; j ++)
      q[j].deriv(j) = 1.0;

    flux( q, f, nx, ny, gam, l);

  }
  for(int j=0; j <= 3; j++) {
    cout << "j   " << f[j].value() << endl;
    for(int k=0; k <= 3; k++)
      cout << "j,k   " << f[j].deriv(k) << endl;
  }

  return 0;
}
