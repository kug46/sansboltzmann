
subroutine fluxp(q,nx,ny,gam,l,n,fp)
  integer, intent(in) :: n
  real, intent(in) :: q(4)
  real, intent(out) :: fp(4)
  real cs,ubar,u,v,p

  ubar = nx*q(2)/q(1)+ny*q(3)/q(1)
  u = q(2)/q(1)
  v = q(3)/q(1)
  p = (gam-1.0)*q(4)-0.5*(gam-1.0)*(q(2)**2+q(3)**2)/q(1)
  cs = sqrt(gam*(gam-1.0)*(q(4)-0.5*(q(2)**2+q(3)**2)/q(1))/q(1))

  fp(1) = l*0.25*q(1)*cs*(ubar/cs+1.0)**2
  fp(2) = fp(1)*((nx/gam)*(-ubar+2.0*cs)+u)
  fp(3) = fp(1)*((ny/gam)*(-ubar+2.0*cs)+v)
  fp(4) = fp(1)*((-(gam-1.0)*ubar**2+2.0*(gam-1.0)*ubar*cs+  &
             2.0*cs**2)/(gam**2-1.0)+0.5*(u**2+v**2))
end subroutine fluxp



subroutine fluxjacp(q,nx,ny,gam,l,n,dfp_dq)
  implicit none
  integer, intent(in) :: n
  real, intent(in) :: q(4),nx,ny,gam,l
  real, intent(out) :: dfp_dq(4,4)
  integer i,j
  real ubar,c,fp1,rho,u,v,e,p
  real rho_q(4),u_q(4),v_q(4),p_q(4),ubar_q(4),c_q(4),e_q(4)

  rho = q(1)
  u = q(2)/rho
  v = q(3)/rho
  e = q(4)
  p = (gam-1.0)*(e-0.5*rho*(u**2+v**2))

  c = sqrt(gam*(gam-1.0)*(q(4)-0.5*(q(2)**2+q(3)**2)/q(1))/q(1))
  ubar = nx*q(2)/q(1)+ny*q(3)/q(1)


  fp1 = l*0.25*rho*c*(ubar/c+1.0)**2


  rho_q(1) = 1.0
  do i=2,n
    rho_q(i) = 0.0
  enddo

  e_q(4) = 1.0
  do i=1,n-1
    e_q(i) = 0.0
  enddo

  u_q(1) = -q(2)/(q(1)*q(1))
  u_q(2) = 1.0/q(1)
  u_q(3) = 0.0
  u_q(4) = 0.0

  v_q(1) = -q(3)/(q(1)*q(1))
  v_q(2) = 0.0
  v_q(3) = 1.0/q(1)
  v_q(4) = 0.0

  p_q(1) = 0.5*(gam-1.0)*(q(2)**2+q(3)**2)/(q(1)**2)
  p_q(2) = -(gam-1.0)*q(2)/q(1)
  p_q(3) = -(gam-1.0)*q(3)/q(1)
  p_q(4) = gam-1.0

  do i=1,n
    ubar_q(i) = nx*u_q(i)+ny*v_q(i)
    c_q(i) = 0.5*(gam/c)*(p_q(i)/rho-p*rho_q(i)/(rho*rho))
  enddo


  do i = 1,n
    dfp_dq(1,i)= l*0.25*((rho_q(i)*c+rho*c_q(i))*(ubar/c+1.0)**2+      &
               rho*c*2.0*(ubar/c+1.0)*(ubar_q(i)/c-ubar*c_q(i)/c**2))
    dfp_dq(2,i) = dfp_dq(1,i)*((nx/gam)*(-ubar+2.0*c)+u)+          &
                fp1*((nx/gam)*(-ubar_q(i)+2.0*c_q(i))+u_q(i))
    dfp_dq(3,i) = dfp_dq(1,i)*((ny/gam)*(-ubar+2.0*c)+v)+         &
                fp1*((ny/gam)*(-ubar_q(i)+2.0*c_q(i))+v_q(i))
    dfp_dq(4,i) = dfp_dq(1,i)*((1.0/(gam**2-1.0))*((gam-1.0)*        &
                (-ubar**2+2.0*ubar*c)+2.0*c**2)+0.5*(u**2+v**2))+   &
                fp1*((1.0/(gam**2-1.0))*((gam-1.0)*(-2.0*ubar*      &
                ubar_q(i)+2.0*(ubar*c_q(i)+c*ubar_q(i)))+           &
                4.0*c*c_q(i))+u*u_q(i)+v*v_q(i))
  enddo

end subroutine fluxjacp
