
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

__attribute__((noinline)) void fluxp(double q[], double nx, double ny, double gam, double l, int n, double *fp)
{
  double cs,ubar,u,v,p;

  ubar = nx*q[1]/q[0]+ny*q[2]/q[0];
  u = q[1]/q[0];
  v = q[2]/q[0];
  p = (gam-1.0)*q[3]-0.5*(gam-1.0)*(q[1]*q[1]+q[2]*q[2])/q[0];
  cs = sqrt(gam*(gam-1.0)*(q[3]-0.5*(q[1]*q[1]+q[2]*q[2])/q[0])/q[0]);

  fp[0] = l*0.25*q[0]*cs*pow(ubar/cs+1.0, 2);
  fp[1] = fp[0]*((nx/gam)*(-ubar+2.0*cs)+u);
  fp[2] = fp[0]*((ny/gam)*(-ubar+2.0*cs)+v);
  fp[3] = fp[0]*((-(gam-1.0)*ubar*ubar+2.0*(gam-1.0)*ubar*cs+
             2.0*cs*cs)/(gam*gam-1.0)+0.5*(u*u+v*v));
}


__attribute__((noinline)) void fluxjacp( double q[], double nx, double ny, double gam, double l, int n, double dfp_dq[][4] )
{
  int i,j;
  double ubar,c,fp1,rho,u,v,e,p;
  double rho_q[4],u_q[4],v_q[4],p_q[4],ubar_q[4],c_q[4],e_q[4];

  rho = q[0];
  u = q[1]/rho;
  v = q[2]/rho;
  e = q[3];
  p = (gam-1.0)*(e-0.5*rho*(u*u+v*v));

  c = sqrt(gam*(gam-1.0)*(q[3]-0.5*(q[1]*q[1]+q[2]*q[2])/q[0])/q[0]);
  ubar = nx*q[1]/q[0]+ny*q[2]/q[0];


  fp1 = l*0.25*rho*c*pow((ubar/c+1.0),2);


  rho_q[0] = 1.0;
  for( i=1; i < n; i++ )
    rho_q[i] = 0.0;

  e_q[3] = 1.0;
  for( i=0; i<n-1; i++)
    e_q[i] = 0.0;

  u_q[0] = -q[1]/(q[0]*q[0]);
  u_q[1] = 1.0/q[0];
  u_q[2] = 0.0;
  u_q[3] = 0.0;

  v_q[0] = -q[2]/(q[0]*q[0]);
  v_q[1] = 0.0;
  v_q[2] = 1.0/q[0];
  v_q[3] = 0.0;

  p_q[0] = 0.5*(gam-1.0)*(q[1]*q[1]+q[2]*q[2])/(q[0]*q[0]);
  p_q[1] = -(gam-1.0)*q[1]/q[0];
  p_q[2] = -(gam-1.0)*q[2]/q[0];
  p_q[3] = gam-1.0;

  for( i=0; i < n; i++ )
  {
    ubar_q[i] = nx*u_q[i]+ny*v_q[i];
    c_q[i] = 0.5*(gam/c)*(p_q[i]/rho-p*rho_q[i]/(rho*rho));
  }

  for( i = 0; i < n; i++ )
  {
    dfp_dq[0][i]= l*0.25*((rho_q[i]*c+rho*c_q[i])*pow(ubar/c+1.0,2)+
               rho*c*2.0*(ubar/c+1.0)*(ubar_q[i]/c-ubar*c_q[i]/(c*c)));
    dfp_dq[1][i] = dfp_dq[0][i]*((nx/gam)*(-ubar+2.0*c)+u)+
                fp1*((nx/gam)*(-ubar_q[i]+2.0*c_q[i])+u_q[i]);
    dfp_dq[2][i] = dfp_dq[0][i]*((ny/gam)*(-ubar+2.0*c)+v)+
                fp1*((ny/gam)*(-ubar_q[i]+2.0*c_q[i])+v_q[i]);
    dfp_dq[3][i] = dfp_dq[0][i]*((1.0/(gam*gam-1.0))*((gam-1.0)*
                (-ubar*ubar+2.0*ubar*c)+2.0*c*c)+0.5*(u*u+v*v))+
                fp1*((1.0/(gam*gam-1.0))*((gam-1.0)*(-2.0*ubar*
                ubar_q[i]+2.0*(ubar*c_q[i]+c*ubar_q[i]))+
                4.0*c*c_q[i])+u*u_q[i]+v*v_q[i]);
  }
}





int main(int argc, char **argv)
{

int n,i,j,k;
double q[4], nx,ny,gam,l,dfp_dq[4][4],fp[4];


q[0] = 1.0;
q[1] = 1.25;
q[2] = 0.0;
q[3] = 1.75;

nx = 0.25;
ny = 0.75;

gam = 1.4;

l = 1.0;
n = 4;

int iter = 1000000;
if(argc > 1) {
    iter = atoi(argv[1]);
}

for( i = 0; i < iter; i++ )
{
   if( i % 10000000 == 0 ) printf("i=%d\n", i);
   nx = nx + 0.0;
   ny = ny + 0.0;
   fluxp(q,nx,ny,gam,l,n,fp);

   fluxjacp(q,nx,ny,gam,l,n,dfp_dq);

}
for(j=0; j <= 3; j++) {
	printf("j  %f\n", fp[j]);
	for( k=0; k <= 3; k++)
		printf( "j,k   %f\n", dfp_dq[j][k] );
}

  return 0;

}

