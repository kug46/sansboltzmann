program surreally

integer :: n,i,j,k,dynamic,argLen,iter
real :: q(4), nx,ny,gam,l,dfp_dq(4,4),fp(4)
character(1024) :: argStr


!read(*,*) dynamic
dynamic = 1

q(1) = 1.0*dynamic
q(2) = 1.25*dynamic
q(3) = 0.0*dynamic
q(4) = 1.75*dynamic

nx = 0.25*dynamic
ny = 0.75*dynamic

gam = 1.4*dynamic

l = 1.0*dynamic
n = 4

iter = 1
if(command_argument_count() > 0) then
    call get_command_argument(1, argStr, argLen)
    read (argStr,*) iter
end if

do i = 1, iter
!   if( mod(i,10000000) == 0 ) write(*,*) i
   call fluxp(q,nx,ny,gam,l,n,fp)

   call fluxjacp(q,nx,ny,gam,l,n,dfp_dq)

enddo
   print *, fp
   do j = 1,4
     do k = 1,4
       print *, "j,k", dfp_dq(j,k)
     enddo
   enddo


end program surreally
