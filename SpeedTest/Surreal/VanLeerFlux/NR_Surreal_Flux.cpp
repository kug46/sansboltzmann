#include <iostream>
#include <stdlib.h>
#include "Surreal_lazy.h"

using namespace std;

#ifndef nderiv
#define nderiv 4
#endif

__attribute__((noinline)) void flux( Surreal<double, nderiv> *q, Surreal<double, nderiv> *f, double nx, double ny, double gam, double l)
{
  Surreal<double, nderiv> cs, ubar, u, v, p;

  ubar = nx*q[1]/q[0]+ny*q[2]/q[0];
  u = q[1]/q[0];
  v = q[2]/q[0];
  p = (gam - 1.0)*(q[3] - .5*(q[1]*q[1] + q[2]*q[2])/q[0]);
  cs = pow((gam*p)/q[0],.5);

  f[0] = l*0.25*q[0]*cs*(ubar/cs+1.0)*(ubar/cs+1.0);
  f[1] = f[0]*((nx/gam)*(-1.0*ubar)+(nx/gam)*(2.0*cs)+u);
  f[2] = f[0]*((ny/gam)*(-1.0*ubar)+(ny/gam)*(2.0*cs)+v);
  f[3] = f[0]*((-(gam-1.0)*ubar*ubar+2.0*(gam-1.0)*ubar*cs+2.0*cs*cs)
        /(gam*gam-1.0)+0.5*(u*u)+0.5*(v*v));
}
