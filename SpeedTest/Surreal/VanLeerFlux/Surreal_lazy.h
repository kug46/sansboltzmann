// -*-c++-*-
//****************************************************************************80
//! \class Surreal
//! \brief Is a class defining surreal numbers...that are assume to be functions
//!        of large 1-D vectors.
//! \details  This is the class defining a surreal number i.e. a number
//!           with value and derivative w.r.t. an arbitrary number of
//!           parameters.  NOTE: This class does no checking on derivative size
//!           it's expensive and you should know better than to do something
//!           that dumb.
//!           NOTE: No copy constructor implemented instances of class NOT
//!           PASSABLE by value.
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam dataT The datatype of real numbers, i.e. float or double
//! \tparam N The number of derivatives you want to take
//****************************************************************************80
#include <cmath>
#include <iostream>
#include <utility>

#ifndef SURREAL_H
#define SURREAL_H


/*---> Forward declare the general class SurrealOP.  The general case is empty
  because we only fill specialization. */
template<int N, class dataT, class LHST, class RHST, class OP> class SurrealOP;
/*---> Forward declare operator classes, that we use to specialize SurrealOP
 */
class Plus;
class Minus;
class Times;
class Divide;

template <typename dataT, int N = 1 >
class Surreal {

private:
//++++++++++++++++++++++++++++++++ PRIVATE STUFF +++++++++++++++++++++++++++++++
  dataT value;  /*!< The value of the number. */
  //dataT* deriv; /*!< Derivative values... we can have nfld of them */
  dataT deriv[N];
public:
//++++++++++++++++++++++++++++++++ PUBLIC STUFF ++++++++++++++++++++++++++++++++

//****************************************************************************80
//! \fn Surreal::Surreal()
//! \brief The default constructor, no arguments.
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//!
//****************************************************************************80
  Surreal() {
    //---> Assume that we have a scalar

    //---> Set value to zero
    this->value = 0.0;
    //---> Set value of derivative to zero
    //this->deriv = new dataT[this->n];
    for( unsigned i = 0; i < N; i++){
      this->deriv[i] = 0.0;
    }

  }// End Surreal

//****************************************************************************80
//! \fn Surreal::Surreal()
//! \brief The default constructor, no arguments.
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//!
//****************************************************************************80
  Surreal(const dataT& a) {
    //---> Assume that we have a scalar

    //---> Set value to zero
    this->value = a;
    //---> Set value of derivative to zero
    //this->deriv = new dataT[this->n];
    for( unsigned i = 0; i < N; i++){
      this->deriv[i] = 0.0;
    }

  }// End Surreal

//****************************************************************************80
//! \fn Surreal::Surreal(const Surreal<dataT, N>& a)
//! \brief The copy constructor
//! \details Note Template's should ensure n is the same for both
//! \nick
//! \version $Rev$
//! \date $Date$
//! \param[in] a The instance to copy from
//****************************************************************************80
  Surreal(const Surreal<dataT, N>& a)
  {
    //---> Set value
    this->value = a.value;
    //---> Copy the derivative
    for(int i = 0; i < N; i++){
      this->deriv[i] = a.deriv[i];
    }

  }// End Surreal Copy constructor


//****************************************************************************80
//!
//! \brief set_val : Set the value.
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//****************************************************************************80
  inline void set_val(const dataT& v) {

    //---> Set value;
    this->value = v;

  }// End set_val

//****************************************************************************80
//!
//! \brief Value : Get the value.
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//****************************************************************************80
  inline const dataT Value() const {

    //---> Return value to user
    return(this->value);

  }// End Value

//****************************************************************************80
//!
//! \brief set_deriv : Set  the ith partial derivative, i starts from 0
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \param[in] i Which partial derivative you want to set
//! \param[in] dv Derivative value
//****************************************************************************80
  inline void set_deriv(const int& i, const dataT& dv) {

    //---> Set value of ith derivative
    this->deriv[i] = dv;


  }// End set_deriv

//****************************************************************************80
//!
//! \brief Deriv : Get the ith partial derivative, i starts from 0
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \param[in] i
//****************************************************************************80
 inline const dataT& Deriv(const unsigned& i) const {

    //---> Return value to user
    return(this->deriv[i]);

  }// End Deriv

 //++++++++++++++++++++++++++++++++ PUBLIC OPERATORS ++++++++++++++++++++++++++

//****************************************************************************80
//! \fn std::ostream& Surreal<dataT, N>::operator <<
//!  (std::ostream& os, const Surreal<dataT, N>& z);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \param[in] os The ostream object to stream to
//! \param[in] a The surreal number to stream
//! \return os The ostream object we streamed to
//****************************************************************************80
  friend std::ostream& operator << (std::ostream& os,
				    const Surreal<double,N>& a)
  {
    //---> Write the value
    os << "(" << a.value << ", { ";
    for(int i = 0; i < a.N; i++) os << a.deriv[i] << " ";
    os << "} )";

    return(os);

  }// End operator <<

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator = (const & v);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] v Assign a real value using equal sign
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator = (const dataT& v)
  {

    //---> Set derivative to zero because we assigned a real value
    for(unsigned i = 0; i < N; i++) { // Derivative initialization
      this->deriv[i] = 0.0;
    }

    //---> Set the value to v
    this->value = v;

    //---> Return the pointer to this instance
    return(*this);

  } // End operator =

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator = (const ExprType& tba);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator = (const Surreal<dataT, N>& a)
  {

    for(unsigned i = 0; i < N; i++) {//Derivative evaluation
      this->deriv[i] = a.Deriv(i);
    } // End derivative evaluation

    //---> Assign the value
    this->value = a.Value();
    //---> Return the pointer to this instance
    return(*this);

  } // End operator =

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator = (const SurrealOP& tba);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  template<class LHST, class RHST, class OP>
  inline Surreal<dataT, N>& operator =
                            (const SurrealOP<N, dataT, LHST, RHST, OP>& tba)
  {

    for(unsigned i = 0; i < N; i++) {//Derivative evaluation
      this->deriv[i] = tba.Deriv(i);
    } // End derivative evaluation

    //---> Assign the value
    this->value = tba.Value();

    //---> Return the pointer to this instance
    return(*this);
  } // End operator =

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator += (const & v);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] v Assign a real value using equal sign
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator += (const dataT& v)
  {

    //---> Set derivative to zero because we assigned a real value
    for(unsigned i = 0; i < N; i++) { // Derivative initialization
      this->deriv[i] = 0.0;
    }
    //---> Set the value to v
    this->value += v;

    //---> Return the pointer to this instance
    return(*this);

  } // End operator +=

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator += (const Surreal& a);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator += (const Surreal<dataT, N>& a)
  {
    for(unsigned i = 0; i < N; i++) { // Derivative initialization
      this->deriv[i] = Deriv(i) + a.Deriv(i);
    }  // End Derivative eval

       this->value += a.Value();

    return(*this);

  } // End operator +=
//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator += (const SurrealOP& tba);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  template<class LHST, class RHST, class OP>
  inline Surreal<dataT, N>& operator += (const SurrealOP<N, dataT, LHST, RHST, OP>& tba)
  {

    for(unsigned i = 0; i < N; i++) {//Derivative evaluation
      this->deriv[i] = Deriv(i) + tba.Deriv(i) ;
    } // End derivative evaluation

    //---> Assign the value
    this->value += tba.Value();

    //---> Return the pointer to this instance
    return(*this);
  } // End operator +=

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator -= (const & v);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] v Assign a real value using equal sign
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator -= (const dataT& v)
  {

    //---> Set derivative to zero because we assigned a real value
    for(unsigned i = 0; i < N; i++) { // Derivative initialization
      this->deriv[i] = 0.0;
    }
    //---> Set the value to v
    this->value -= v;

    //---> Return the pointer to this instance
    return(*this);

  } // End operator -=

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator -= (const Surreal& a);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator -= (const Surreal<dataT, N>& a)
  {
    for(unsigned i = 0; i < N; i++) { // Derivative initialization
      this->deriv[i] = Deriv(i) - a.Deriv(i);
    }  // End Derivative eval

       this->value -= a.Value();

    return(*this);

  } // End operator -=
//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator -= (const SurrealOP& tba);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  template<class LHST, class RHST, class OP>
  inline Surreal<dataT, N>& operator -= (const SurrealOP<N, dataT, LHST, RHST, OP>& tba)
  {

    for(unsigned i = 0; i < N; i++) {//Derivative evaluation
      this->deriv[i] = Deriv(i) - tba.Deriv(i) ;
    } // End derivative evaluation

    //---> Assign the value
    this->value -= tba.Value();

    //---> Return the pointer to this instance
    return(*this);
  } // End operator -=

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator *= (const & v);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] v Assign a real value using equal sign
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator *= (const dataT& v)
  {

    //---> Set derivative to zero because we assigned a real value
    for(unsigned i = 0; i < N; i++) { // Derivative initialization
      this->deriv[i] = 0.0;
    }
    //---> Set the value to v
    this->value *= v;

    //---> Return the pointer to this instance
    return(*this);

  } // End operator *=

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator *= (const Surreal& a);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator *= (const Surreal<dataT, N>& a)
  {
    for(unsigned i = 0; i < N; i++) { // Derivative initialization
      this->deriv[i] =
         Deriv(i)*a.Value() + Value()*a.Deriv(i) ;
    }  // End Derivative eval

       this->value *= a.Value();

    return(*this);

  } // End operator *=
//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator *= (const SurrealOP& tba);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  template<class LHST, class RHST, class OP>
  inline Surreal<dataT, N>& operator *= (const SurrealOP<N, dataT, LHST, RHST, OP>& tba)
  {

    for(unsigned i = 0; i < N; i++) {//Derivative evaluation
      this->deriv[i] =
         Deriv(i)*tba.Value() + Value()*tba.Deriv(i) ;
    } // End derivative evaluation

    //---> Assign the value
    this->value *= tba.Value();

    //---> Return the pointer to this instance
    return(*this);
  } // End operator *=

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator /= (const & v);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] v Assign a real value using equal sign
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator /= (const dataT& v)
  {

    //---> Set derivative to zero because we assigned a real value
    for(unsigned i = 0; i < N; i++) { // Derivative initialization
      this->deriv[i] = 0.0;
    }
    //---> Set the value to v
    this->value /= v;

    //---> Return the pointer to this instance
    return(*this);

  } // End operator /=

//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator /= (const Surreal& a);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  inline Surreal<dataT, N>& operator /= (const Surreal<dataT, N>& a)
  {
    for(unsigned i = 0; i < N; i++) { // Derivative initialization
      this->deriv[i] =
         (Deriv(i)*a.Value() - Value()*a.Deriv(i))/
         (a.Value()*a.Value() ) ;
    }  // End Derivative eval

       this->value /= a.Value();

    return(*this);

  } // End operator /=
//****************************************************************************80
//! \fn  Sureal<dataT,N> & operator /= (const SurrealOP& tba);
//! \brief  Stream operator  ostream << Surreal
//! \details
//! \nick
//! \version $Rev$
//! \date $Date$
//! \tparam ExprType The type of expression we are going to apply
//! \param[in] tba The expession to be applied (see tba = to be applied)
//! \return *this The pointer to current instance of a Surreal
//****************************************************************************80
  template<class LHST, class RHST, class OP>
  inline Surreal<dataT, N>& operator /= (const SurrealOP<N, dataT, LHST, RHST, OP>& tba)
  {

    for(unsigned i = 0; i < N; i++) {//Derivative evaluation
      this->deriv[i] =
         (Deriv(i)*tba.Value() - Value()*tba.Deriv(i))/
         (tba.Value()*tba.Value() ) ;
    } // End derivative evaluation

    //---> Assign the value
    this->value /= tba.Value();

    //---> Return the pointer to this instance
    return(*this);
  } // End operator /=


};


//+++++++++++++++++++++++++++++++++ Addition +++++++++++++++++++++++++++++++++++
//---> Define specialization of SurrealOP for plus with arbitrary LHS and RHS
//---> LHST + RHST
template<int N, class dataT, class LHST, class RHST>
class SurrealOP<N, dataT, LHST, RHST, Plus>
{
private:
  const LHST& lhs;
  const RHST& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const LHST& larg, const RHST& rarg) : lhs(larg), rhs(rarg) {}

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs.Value() + rhs.Value()); }

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return( lhs.Deriv(i) +
						 rhs.Deriv(i) ); }

};// End class SurrealOP<N, LHST, RHST, Plus>

//---> LHST + dataT
template<int N, class dataT, class LHST>
class SurrealOP<N, dataT, LHST, dataT, Plus>
{
private:
  const LHST& lhs;
  const dataT& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const LHST& larg, const dataT& rarg) : lhs(larg), rhs(rarg) {}

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs.Value() + rhs); }

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return( lhs.Deriv(i) ); }

};// End class SurrealOP<N, LHST, dataT, Plus>

//---> dataT + RHST
template<int N, class dataT, class RHST>
class SurrealOP<N, dataT, dataT, RHST, Plus>
{
private:
  const dataT& lhs;
  const RHST& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const dataT& larg, const RHST& rarg) :
    lhs(larg), rhs(rarg) {}

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs + rhs.Value()); }

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return( rhs.Deriv(i) ); }

};// End class SurrealOP<N, LHST, dataT, Plus>


/*---> dataT + dataT
  This is intrisic...define nothing */

//---> dataT + Surreal<dataT, N>
template<int N, class dataT>
inline SurrealOP<N, dataT, dataT, Surreal<dataT, N>, Plus>
operator + ( const dataT& left, const Surreal<dataT, N>& right)
{
  //---> Construct a SurrealOP<N, dataT, Surreal<dataT, N>, Plus>
  return( SurrealOP<N, dataT, dataT, Surreal<dataT, N>, Plus> (left, right));

}// End Operator +

//---> dataT + SurrealOP<N, dataT, LHST, RHST, OP>
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N, dataT, dataT, SurrealOP<N, dataT, LHST, RHST, OP>, Plus>
operator + (const dataT& left, const SurrealOP<N, dataT, LHST, RHST, OP>& right)
{
  //---> Construct a SurrealOP<N, dataT, dataT, SurrealOP<N, LHST, RHST, Plus>, Plus>
  return( SurrealOP<N, dataT, dataT, SurrealOP<N, dataT, LHST, RHST, OP>, Plus>
	  (left, right));
}// End Operator +

//---> Surreal<dataT, N> + dataT
template<int N, class dataT>
inline SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Plus>
operator + (const Surreal<dataT, N>& left, const dataT& right)
{
  //---> Construct a SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Plus>
  return( SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Plus> (left, right) );
}// End Operator +

//---> Surreal<dataT, N> + Surreal<dataT, N>
template<int N, class dataT>
inline SurrealOP<N, dataT, Surreal<dataT,N>, Surreal<dataT,N>, Plus>
operator + (const Surreal<dataT, N>& left,
	    const Surreal<dataT, N>& right)
{
  //---> Construct a SurrealOP<N,  Surreal<dataT, N>, Surreal<dataT, N>, Plus>
  return( SurrealOP<N, dataT, Surreal<dataT, N>,
		    Surreal<dataT, N>, Plus> ( left, right) );
} // End Operator +

//---> Surreal<dataT, N> + SurrealOP<N, LHST, RHST, OP>
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N, dataT, Surreal<dataT, N>, SurrealOP<N, dataT, LHST, RHST, OP>,
		 Plus>
operator + (const Surreal<dataT, N>& left,
	    const SurrealOP<N, dataT, LHST, RHST, OP>& right)
{
  /*---> Construct a SurrealOP<N,  Surreal<dataT, N>, SurrealOP<N, LHST, RHST, OP>,
    Plus> */
  return( SurrealOP<N, dataT, Surreal<dataT, N>,
	  SurrealOP<N, dataT, LHST, RHST, OP>, Plus>  (left, right) );
}// End Operator +

//---> SurrealOP + dataT
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>, dataT, Plus >
operator + (const SurrealOP<N,  dataT, LHST, RHST, OP >& left,
	    const dataT& right)
{
  /*---> Construct a SurrealOP<N,  dataT, SurrealOP, dataT, Plus> */
  return(SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>, dataT, Plus >
	 (left,right) );
}// End Operator +

//---> SurrealOP + Surreal
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>,
		  Surreal<dataT, N>, Plus >
operator + (const SurrealOP<N, dataT, LHST, RHST, OP>& left,
	    const Surreal<dataT, N>& right)
{
  /*---> Construct A SurrealOP<N, dataT, SurrealOP, Surreal, Plus> */
  return(SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>,
	 Surreal<dataT, N>, Plus > (left, right)) ;

}// End Operator +

//---> SurrealOP + SurrealOP
template<int N, class dataT, class LHSTL, class RHSTL, class LHSTR, class RHSTR,
	  class OPL, class OPR>
inline SurrealOP<N, dataT, SurrealOP<N, dataT, LHSTL, RHSTL, OPL>,
		        SurrealOP<N, dataT, LHSTR, RHSTR, OPR>, Plus>
operator + (const SurrealOP<N, dataT, LHSTL, RHSTL, OPL>& left,
	    const SurrealOP<N, dataT, LHSTR, RHSTR, OPR>& right)
{
  /*---> Construct a SurrealOP<N,  dataT, SurrealOP, SurrealOP, Plus> */
  return( SurrealOP<N, dataT, SurrealOP<N, dataT, LHSTL, RHSTL, OPL>,
		    SurrealOP<N, dataT, LHSTR, RHSTR, OPR>, Plus> (left, right) );

}// End Operator +

//+++++++++++++++++++++++++++++++++ Subtraction +++++++++++++++++++++++++++++++++++
//---> Define specialization of SurrealOP for minus with arbitrary LHS and RHS
//---> LHST - RHST
template<int N, class dataT, class LHST, class RHST>
class SurrealOP<N, dataT, LHST, RHST, Minus>
{
private:
  const LHST& lhs;
  const RHST& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const LHST& larg, const RHST& rarg) : lhs(larg), rhs(rarg) {}

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs.Value() - rhs.Value()); }

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return( lhs.Deriv(i) -
						 rhs.Deriv(i) ); }

};// End class SurrealOP<N, LHST, RHST, Minus>

//---> LHST - dataT
template<int N, class dataT, class LHST>
class SurrealOP<N, dataT, LHST, dataT, Minus>
{
private:
  const LHST& lhs;
  const dataT& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const LHST& larg, const dataT& rarg) : lhs(larg), rhs(rarg) {}

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs.Value() - rhs); }

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return( lhs.Deriv(i) ); }

};// End class SurrealOP<N, LHST, dataT, Minus>

//---> dataT - RHST
template<int N, class dataT, class RHST>
class SurrealOP<N, dataT, dataT, RHST, Minus>
{
private:
  const dataT& lhs;
  const RHST& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const dataT& larg, const RHST& rarg) :
    lhs(larg), rhs(rarg) {}

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs - rhs.Value()); }

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return( (-1.0)*rhs.Deriv(i) ); }

};// End class SurrealOP<N, LHST, dataT, Minus>


/*---> dataT - dataT
  This is intrisic...define nothing */

//---> dataT - Surreal<dataT, N>
template<int N, class dataT>
inline SurrealOP<N, dataT, dataT, Surreal<dataT, N>, Minus>
operator - ( const dataT& left, const Surreal<dataT, N>& right)
{
  //---> Construct a SurrealOP<N, dataT, Surreal<dataT, N>, Minus>
  return( SurrealOP<N, dataT, dataT, Surreal<dataT, N>, Minus> (left, right));

}// End Operator -

//---> dataT - SurrealOP<N, dataT, LHST, RHST, OP>
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N, dataT, dataT, SurrealOP<N, dataT, LHST, RHST, OP>, Minus>
operator - (const dataT& left, const SurrealOP<N, dataT, LHST, RHST, OP>& right)
{
  //---> Construct a SurrealOP<N, dataT, dataT, SurrealOP<N, LHST, RHST, Minus>, Minus>
  return( SurrealOP<N, dataT, dataT, SurrealOP<N, dataT, LHST, RHST, OP>, Minus>
	  (left, right));
}// End Operator -

//---> Surreal<dataT, N> - dataT
template<int N, class dataT>
inline SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Minus>
operator - (const Surreal<dataT, N>& left, const dataT& right)
{
  //---> Construct a SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Minus>
  return( SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Minus> (left, right) );
}// End Operator -

//---> Surreal<dataT, N> - Surreal<dataT, N>
template<int N, class dataT>
inline SurrealOP<N, dataT, Surreal<dataT,N>, Surreal<dataT,N>, Minus>
operator - (const Surreal<dataT, N>& left,
	    const Surreal<dataT, N>& right)
{
  //---> Construct a SurrealOP<N,  Surreal<dataT, N>, Surreal<dataT, N>, Minus>
  return( SurrealOP<N, dataT, Surreal<dataT, N>,
		    Surreal<dataT, N>, Minus> ( left, right) );
} // End Operator -

//---> Surreal<dataT, N> - SurrealOP<N, LHST, RHST, OP>
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N, dataT, Surreal<dataT, N>, SurrealOP<N, dataT, LHST, RHST, OP>,
		 Minus>
operator - (const Surreal<dataT, N>& left,
	    const SurrealOP<N, dataT, LHST, RHST, OP>& right)
{
  /*---> Construct a SurrealOP<N,  Surreal<dataT, N>, SurrealOP<N, LHST, RHST, OP>,
    Minus> */
  return( SurrealOP<N, dataT, Surreal<dataT, N>,
	  SurrealOP<N, dataT, LHST, RHST, OP>, Minus>  (left, right) );
}// End Operator -

//---> SurrealOP - dataT
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>, dataT, Minus >
operator - (const SurrealOP<N,  dataT, LHST, RHST, OP >& left,
	    const dataT& right)
{
  /*---> Construct a SurrealOP<N,  dataT, SurrealOP, dataT, Minus> */
  return(SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>, dataT, Minus >
	 (left,right) );
}// End Operator -

//---> SurrealOP - Surreal
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>,
		  Surreal<dataT, N>, Minus >
operator - (const SurrealOP<N, dataT, LHST, RHST, OP>& left,
	    const Surreal<dataT, N>& right)
{
  /*---> Construct A SurrealOP<N, dataT, SurrealOP, Surreal, Minus> */
  return(SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>,
	 Surreal<dataT, N>, Minus > (left, right)) ;

}// End Operator -

//---> SurrealOP - SurrealOP
template<int N, class dataT, class LHSTL, class RHSTL, class LHSTR, class RHSTR,
	  class OPL, class OPR>
inline SurrealOP<N, dataT, SurrealOP<N, dataT, LHSTL, RHSTL, OPL>,
		        SurrealOP<N, dataT, LHSTR, RHSTR, OPR>, Minus>
operator - (const SurrealOP<N, dataT, LHSTL, RHSTL, OPL>& left,
	    const SurrealOP<N, dataT, LHSTR, RHSTR, OPR>& right)
{
  /*---> Construct a SurrealOP<N,  dataT, SurrealOP, SurrealOP, Minus> */
  return( SurrealOP<N, dataT, SurrealOP<N, dataT, LHSTL, RHSTL, OPL>,
		    SurrealOP<N, dataT, LHSTR, RHSTR, OPR>, Minus> (left, right) );

}// End Operator -

//+++++++++++++++++++++++++++++++++ Multiplication +++++++++++++++++++++++++++++
//---> Define specialization of SurrealOP for times with arbitrary LHS and RHS
//---> LHST * RHST
template<int N, class dataT, class LHST, class RHST>
class SurrealOP<N, dataT, LHST, RHST, Times>
{
private:
  const LHST& lhs;
  const RHST& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const LHST& larg, const RHST& rarg) : lhs(larg), rhs(rarg) {}

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return(
         lhs.Deriv(i)*rhs.Value() + lhs.Value()*rhs.Deriv(i) ); }

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs.Value() * rhs.Value()); }


};// End class SurrealOP<N, LHST, RHST, Times>

//---> LHST * dataT
template<int N, class dataT, class LHST>
class SurrealOP<N, dataT, LHST, dataT, Times>
{
private:
  const LHST& lhs;
  const dataT& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const LHST& larg, const dataT& rarg) : lhs(larg), rhs(rarg) {}

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return( lhs.Deriv(i)*rhs ); }

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs.Value() * rhs); }


};// End class SurrealOP<N, LHST, dataT, Times>

//---> dataT * RHST
template<int N, class dataT, class RHST>
class SurrealOP<N, dataT, dataT, RHST, Times>
{
private:
  const dataT& lhs;
  const RHST& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const dataT& larg, const RHST& rarg) :
    lhs(larg), rhs(rarg) {}

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return( lhs*rhs.Deriv(i) ); }

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs * rhs.Value()); }


};// End class SurrealOP<N, LHST, dataT, Times>


/*---> dataT * dataT
  This is intrisic...define nothing */

//---> dataT * Surreal<dataT, N>
template<int N, class dataT>
inline SurrealOP<N, dataT, dataT, Surreal<dataT, N>, Times>
operator * ( const dataT& left, const Surreal<dataT, N>& right)
{
  //---> Construct a SurrealOP<N, dataT, Surreal<dataT, N>, Times>
  return( SurrealOP<N, dataT, dataT, Surreal<dataT, N>, Times> (left, right));

}// End Operator *

//---> dataT * SurrealOP<N, dataT, LHST, RHST, OP>
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N, dataT, dataT, SurrealOP<N, dataT, LHST, RHST, OP>, Times>
operator * (const dataT& left, const SurrealOP<N, dataT, LHST, RHST, OP>& right)
{
  //---> Construct a SurrealOP<N, dataT, dataT, SurrealOP<N, LHST, RHST, Times>, Times>
  return( SurrealOP<N, dataT, dataT, SurrealOP<N, dataT, LHST, RHST, OP>, Times>
	  (left, right));
}// End Operator *

//---> Surreal<dataT, N> * dataT
template<int N, class dataT>
inline SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Times>
operator * (const Surreal<dataT, N>& left, const dataT& right)
{
  //---> Construct a SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Times>
  return( SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Times> (left, right) );
}// End Operator *

//---> Surreal<dataT, N> * Surreal<dataT, N>
template<int N, class dataT>
inline SurrealOP<N, dataT, Surreal<dataT,N>, Surreal<dataT,N>, Times>
operator * (const Surreal<dataT, N>& left,
	    const Surreal<dataT, N>& right)
{
  //---> Construct a SurrealOP<N,  Surreal<dataT, N>, Surreal<dataT, N>, Times>
  return( SurrealOP<N, dataT, Surreal<dataT, N>,
		    Surreal<dataT, N>, Times> ( left, right) );
} // End Operator *

//---> Surreal<dataT, N> * SurrealOP<N, LHST, RHST, OP>
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N, dataT, Surreal<dataT, N>, SurrealOP<N, dataT, LHST, RHST, OP>,
		 Times>
operator * (const Surreal<dataT, N>& left,
	    const SurrealOP<N, dataT, LHST, RHST, OP>& right)
{
  /*---> Construct a SurrealOP<N,  Surreal<dataT, N>, SurrealOP<N, LHST, RHST, OP>,
    Times> */
  return( SurrealOP<N, dataT, Surreal<dataT, N>,
	  SurrealOP<N, dataT, LHST, RHST, OP>, Times>  (left, right) );
}// End Operator *

//---> SurrealOP * dataT
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>, dataT, Times >
operator * (const SurrealOP<N,  dataT, LHST, RHST, OP >& left,
	    const dataT& right)
{
  /*---> Construct a SurrealOP<N,  dataT, SurrealOP, dataT, Times> */
  return(SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>, dataT, Times >
	 (left,right) );
}// End Operator *

//---> SurrealOP * Surreal
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>,
		  Surreal<dataT, N>, Times >
operator * (const SurrealOP<N, dataT, LHST, RHST, OP>& left,
	    const Surreal<dataT, N>& right)
{
  /*---> Construct A SurrealOP<N, dataT, SurrealOP, Surreal, Times> */
  return(SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>,
	 Surreal<dataT, N>, Times > (left, right)) ;

}// End Operator *

//---> SurrealOP * SurrealOP
template< int N, class dataT, class LHSTL, class RHSTL, class LHSTR, class RHSTR,
	  class OPL, class OPR>
inline SurrealOP<N, dataT, SurrealOP<N, dataT, LHSTL, RHSTL, OPL>,
		        SurrealOP<N, dataT, LHSTR, RHSTR, OPR>, Times>
operator * (const SurrealOP<N, dataT, LHSTL, RHSTL, OPL>& left,
	    const SurrealOP<N, dataT, LHSTR, RHSTR, OPR>& right)
{
  /*---> Construct a SurrealOP<N,  dataT, SurrealOP, SurrealOP, Times> */
  return( SurrealOP<N, dataT, SurrealOP<N, dataT, LHSTL, RHSTL, OPL>,
		    SurrealOP<N, dataT, LHSTR, RHSTR, OPR>, Times> (left, right) );

}// End Operator *

//+++++++++++++++++++++++++++++++++ Division +++++++++++++++++++++++++++++++++++
//---> Define specialization of SurrealOP for divide with arbitrary LHS and RHS
//---> LHST / RHST
template<int N, class dataT, class LHST, class RHST>
class SurrealOP<N, dataT, LHST, RHST, Divide>
{
private:
  const LHST& lhs;
  const RHST& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const LHST& larg, const RHST& rarg) : lhs(larg), rhs(rarg) {}

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return(
         (lhs.Deriv(i)*rhs.Value() - lhs.Value()*rhs.Deriv(i))/
         (rhs.Value()*rhs.Value() ) ); }

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs.Value() / rhs.Value()); }


};// End class SurrealOP<N, LHST, RHST, Divide>

//---> LHST / dataT
template<int N, class dataT, class LHST>
class SurrealOP<N, dataT, LHST, dataT, Divide>
{
private:
  const LHST& lhs;
  const dataT& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const LHST& larg, const dataT& rarg) : lhs(larg), rhs(rarg) {}

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return( lhs.Deriv(i)/rhs ); }

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs.Value() / rhs); }


};// End class SurrealOP<N, LHST, dataT, Divide>

//---> dataT / RHST
template<int N, class dataT, class RHST>
class SurrealOP<N, dataT, dataT, RHST, Divide>
{
private:
  const dataT& lhs;
  const RHST& rhs;

public:

  //---> Constructor: initializes the const & lhs and rhs
  SurrealOP(const dataT& larg, const RHST& rarg) :
    lhs(larg), rhs(rarg) {}

  //---> Derivative function controls the derivatives
  inline dataT Deriv(const unsigned& i) const { return(
           (-1.0)*lhs*rhs.Deriv(i)/(rhs.Value()*rhs.Value() ) ); }

  //---> Value function that constrols the values;
  inline dataT Value() const { return(lhs / rhs.Value()); }


};// End class SurrealOP<N, LHST, dataT, Divide>


/*---> dataT / dataT
  This is intrisic...define nothing */

//---> dataT / Surreal<dataT, N>
template<int N, class dataT>
inline SurrealOP<N, dataT, dataT, Surreal<dataT, N>, Divide>
operator / ( const dataT& left, const Surreal<dataT, N>& right)
{
  //---> Construct a SurrealOP<N, dataT, Surreal<dataT, N>, Divide>
  return( SurrealOP<N, dataT, dataT, Surreal<dataT, N>, Divide> (left, right));

}// End Operator /

//---> dataT / SurrealOP<N, dataT, LHST, RHST, OP>
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N, dataT, dataT, SurrealOP<N, dataT, LHST, RHST, OP>, Divide>
operator / (const dataT& left, const SurrealOP<N, dataT, LHST, RHST, OP>& right)
{
  //---> Construct a SurrealOP<N, dataT, dataT, SurrealOP<N, LHST, RHST, OP>, Divide>
  return( SurrealOP<N, dataT, dataT, SurrealOP<N, dataT, LHST, RHST, OP>, Divide>
	  (left, right));
}// End Operator /

//---> Surreal<dataT, N> / dataT
template<int N, class dataT>
inline SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Divide>
operator / (const Surreal<dataT, N>& left, const dataT& right)
{
  //---> Construct a SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Divide>
  return( SurrealOP<N, dataT, Surreal<dataT, N>, dataT, Divide> (left, right) );
}// End Operator /

//---> Surreal<dataT, N> / Surreal<dataT, N>
template<int N, class dataT>
inline SurrealOP<N, dataT, Surreal<dataT,N>, Surreal<dataT,N>, Divide>
operator / (const Surreal<dataT, N>& left,
	    const Surreal<dataT, N>& right)
{
  //---> Construct a SurrealOP<N,  Surreal<dataT, N>, Surreal<dataT, N>, Divide>
  return( SurrealOP<N, dataT, Surreal<dataT, N>,
		    Surreal<dataT, N>, Divide> ( left, right) );
} // End Operator /

//---> Surreal<dataT, N> / SurrealOP<N, LHST, RHST, OP>
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N, dataT, Surreal<dataT, N>, SurrealOP<N, dataT, LHST, RHST, OP>,
		 Divide>
operator / (const Surreal<dataT, N>& left,
	    const SurrealOP<N, dataT, LHST, RHST, OP>& right)
{
  /*---> Construct a SurrealOP<N,  Surreal<dataT, N>, SurrealOP<N, LHST, RHST, OP>,
    Divide> */
  return( SurrealOP<N, dataT, Surreal<dataT, N>,
	  SurrealOP<N, dataT, LHST, RHST, OP>, Divide>  (left, right) );
}// End Operator /

//---> SurrealOP / dataT
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>, dataT, Divide >
operator / (const SurrealOP<N,  dataT, LHST, RHST, OP >& left,
	    const dataT& right)
{
  /*---> Construct a SurrealOP<N,  dataT, SurrealOP, dataT, Divide> */
  return(SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>, dataT, Divide >
	 (left,right) );
}// End Operator /

//---> SurrealOP / Surreal
template<int N, class dataT, class LHST, class RHST, class OP>
inline SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>,
		  Surreal<dataT, N>, Divide >
operator / (const SurrealOP<N, dataT, LHST, RHST, OP>& left,
	    const Surreal<dataT, N>& right)
{
  /*---> Construct A SurrealOP<N, dataT, SurrealOP, Surreal, Divide> */
  return(SurrealOP<N,  dataT, SurrealOP<N, dataT, LHST, RHST, OP>,
	 Surreal<dataT, N>, Divide > (left, right)) ;

}// End Operator /

//---> SurrealOP / SurrealOP
template<int N, class dataT, class LHSTL, class RHSTL, class LHSTR, class RHSTR,
	  class OPL, class OPR>
inline SurrealOP<N, dataT, SurrealOP<N, dataT, LHSTL, RHSTL, OPL>,
		        SurrealOP<N, dataT, LHSTR, RHSTR, OPR>, Divide>
operator / (const SurrealOP<N, dataT, LHSTL, RHSTL, OPL>& left,
	    const SurrealOP<N, dataT, LHSTR, RHSTR, OPR>& right)
{
  /*---> Construct a SurrealOP<N,  dataT, SurrealOP, SurrealOP, Divide> */
  return( SurrealOP<N, dataT, SurrealOP<N, dataT, LHSTL, RHSTL, OPL>,
		    SurrealOP<N, dataT, LHSTR, RHSTR, OPR>, Divide> (left, right) );

}// End Operator /

//****************************************************************************80
//! \fn Surreal<dataT, N> pow(const Surreal<dataT, N>& a, const dataT& b)
//! \brief pow of surreal a raised to b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \param[in] b The second argument is dataT
//! \return c The return variable
//****************************************************************************80
template<int N, typename dataT>
inline Surreal<dataT, N> pow(const Surreal<dataT, N>& a, const dataT& b)
{
  Surreal<dataT, N> c;
  c.set_val( pow(a.Value(),b ));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)*b*pow(a.Value(),b-1.0));
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> pow(const Surreal<dataT, N>& a, const dataT& b)
//! \brief pow of surreal a raised to b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \param[in] b The second argument is dataT
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> pow(const SurrealOP<N, dataT, LHS, RHS, OP>& a,
			     const dataT& b)
{
  Surreal<dataT, N> c;
  c.set_val( pow(a.Value(),b ));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)*b*pow(a.Value(),b-1.0));
  }

  //---> Return to the user
  return(c);
}


//****************************************************************************80
//! \fn Surreal<dataT, N> log(const Surreal<dataT, N>& a)
//! \brief log of surreal a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \return c The return variable
//****************************************************************************80
template<typename dataT, int N>
inline Surreal<dataT, N> log(const Surreal<dataT, N>& a)
{
  Surreal<dataT, N> c;
  c.set_val( log(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)/a.Value());
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> log(const Surreal<dataT, N>& a)
//! \brief log of surrealop a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surrealop
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> log(const SurrealOP<N, dataT, LHS, RHS, OP>& a)
{
  Surreal<dataT, N> c;
  c.set_val( log(a.Value() ));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)/a.Value());
  }

  //---> Return to the user
  return(c);
}
//****************************************************************************80
//! \fn Surreal<dataT, N> abs(const Surreal<dataT, N>& a)
//! \brief abs of surreal a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \return c The return variable
//****************************************************************************80
template<typename dataT, int N>
inline Surreal<dataT, N> abs(const Surreal<dataT, N>& a)
{
  Surreal<dataT, N> c;
  c.set_val( abs(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, abs(a.Deriv(i)));
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> abs(const Surreal<dataT, N>& a)
//! \brief abs of surrealop a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surrealop
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> abs(const SurrealOP<N, dataT, LHS, RHS, OP>& a)
{
  Surreal<dataT, N> c;
  c.set_val( abs(a.Value() ));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, abs(a.Deriv(i)));
  }

  //---> Return to the user
  return(c);
}
//****************************************************************************80
//! \fn Surreal<dataT, N> exp(const Surreal<dataT, N>& a)
//! \brief exp of surreal a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \return c The return variable
//****************************************************************************80
template<typename dataT, int N>
inline Surreal<dataT, N> exp(const Surreal<dataT, N>& a)
{
  Surreal<dataT, N> c;
  c.set_val( exp(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)*exp(a.Value()));
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> exp(const Surreal<dataT, N>& a)
//! \brief exp of surrealop a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surrealop
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> exp(const SurrealOP<N, dataT, LHS, RHS, OP>& a)
{
  Surreal<dataT, N> c;
  c.set_val( exp(a.Value() ));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)*exp(a.Value()));
  }

  //---> Return to the user
  return(c);
}
//****************************************************************************80
//! \fn Surreal<dataT, N> sin(const Surreal<dataT, N>& a)
//! \brief sin of surreal a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \return c The return variable
//****************************************************************************80
template<typename dataT, int N>
inline Surreal<dataT, N> sin(const Surreal<dataT, N>& a)
{
  Surreal<dataT, N> c;
  c.set_val( sin(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)*cos(a.Value()));
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> sin(const Surreal<dataT, N>& a)
//! \brief sin of surrealop a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surrealop
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> sin(const SurrealOP<N, dataT, LHS, RHS, OP>& a)
{
  Surreal<dataT, N> c;
  c.set_val( sin(a.Value() ));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)*cos(a.Value()));
  }

  //---> Return to the user
  return(c);
}
//****************************************************************************80
//! \fn Surreal<dataT, N> cos(const Surreal<dataT, N>& a)
//! \brief cos of surreal a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \return c The return variable
//****************************************************************************80
template<typename dataT, int N>
inline Surreal<dataT, N> cos(const Surreal<dataT, N>& a)
{
  Surreal<dataT, N> c;
  c.set_val( cos(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, (-1.0)*a.Deriv(i)*sin(a.Value()));
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> cos(const Surreal<dataT, N>& a)
//! \brief cos of surrealop a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surrealop
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> cos(const SurrealOP<N, dataT, LHS, RHS, OP>& a)
{
  Surreal<dataT, N> c;
  c.set_val( cos(a.Value() ));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, (-1.0)*a.Deriv(i)*sin(a.Value()));
  }

  //---> Return to the user
  return(c);
}
//****************************************************************************80
//! \fn Surreal<dataT, N> tan(const Surreal<dataT, N>& a)
//! \brief tan of surreal a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \return c The return variable
//****************************************************************************80
template<typename dataT, int N>
inline Surreal<dataT, N> tan(const Surreal<dataT, N>& a)
{
  Surreal<dataT, N> c;
  c.set_val( tan(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)/pow(cos(a.Value()),2.0));
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> tan(const Surreal<dataT, N>& a)
//! \brief tan of surrealop a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surrealop
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> tan(const SurrealOP<N, dataT, LHS, RHS, OP>& a)
{
  Surreal<dataT, N> c;
  c.set_val( tan(a.Value() ));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)/pow(cos(a.Value()),2.0));
  }

  //---> Return to the user
  return(c);
}
//****************************************************************************80
//! \fn Surreal<dataT, N> csc(const Surreal<dataT, N>& a)
//! \brief csc of surreal a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \return c The return variable
//****************************************************************************80
template<typename dataT, int N>
inline Surreal<dataT, N> csc(const Surreal<dataT, N>& a)
{
  Surreal<dataT, N> c;
  c.set_val( 1.0/sin(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, (-1.0)*a.Deriv(i)/(sin(a.Value())*tan(a.Value())));
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> csc(const Surreal<dataT, N>& a)
//! \brief csc of surrealop a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surrealop
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> csc(const SurrealOP<N, dataT, LHS, RHS, OP>& a)
{
  Surreal<dataT, N> c;
  c.set_val( 1.0/sin(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, (-1.0)*a.Deriv(i)/(sin(a.Value())*tan(a.Value())));
  }

  //---> Return to the user
  return(c);
}
//****************************************************************************80
//! \fn Surreal<dataT, N> sec(const Surreal<dataT, N>& a)
//! \brief sec of surreal a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \return c The return variable
//****************************************************************************80
template<typename dataT, int N>
inline Surreal<dataT, N> sec(const Surreal<dataT, N>& a)
{
  Surreal<dataT, N> c;
  c.set_val( 1.0/cos(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)*tan(a.Value())/(cos(a.Value())));
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> sec(const Surreal<dataT, N>& a)
//! \brief sec of surrealop a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surrealop
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> sec(const SurrealOP<N, dataT, LHS, RHS, OP>& a)
{
  Surreal<dataT, N> c;
  c.set_val( 1.0/cos(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, a.Deriv(i)*tan(a.Value())/(cos(a.Value())));
  }

  //---> Return to the user
  return(c);
}
//****************************************************************************80
//! \fn Surreal<dataT, N> cot(const Surreal<dataT, N>& a)
//! \brief cot of surreal a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surreal
//! \return c The return variable
//****************************************************************************80
template<typename dataT, int N>
inline Surreal<dataT, N> cot(const Surreal<dataT, N>& a)
{
  Surreal<dataT, N> c;
  c.set_val( 1.0/tan(a.Value()));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, (-1.0)*a.Deriv(i)/pow(sin(a.Value()),2.0));
  }

  //---> Return to the user
  return(c);
}

//****************************************************************************80
//! \fn Surreal<dataT, N> cot(const Surreal<dataT, N>& a)
//! \brief cot of surrealop a
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The first argument is surrealop
//! \return c The return variable
//****************************************************************************80
template<int N, class dataT, class LHS, class RHS, class OP>
inline Surreal<dataT, N> cot(const SurrealOP<N, dataT, LHS, RHS, OP>& a)
{
  Surreal<dataT, N> c;
  c.set_val( 1.0/tan(a.Value() ));

  for(int i = 0; i < N; i++){
    c.set_deriv(i, (-1.0)*a.Deriv(i)/pow(sin(a.Value()),2.0));
  }

  //---> Return to the user
  return(c);
}
//****************************************************************************80
//! \fn Surreal<dataT, N> operator <(Surreal<dataT, N> a,
//!     const Surreal<dataT, N>& b
//! \brief < of Surreals a and  b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of < operator
//! \param[in] b The right side of < operator
//! \return boolean result of a value < b value;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator <(const Surreal<dataT, N>& a,
                       const Surreal<dataT, N>& b)
{
  return(a.Value()<b.Value());
} // End operator <
//****************************************************************************80
//! \fn Surreal<dataT, N> operator <(Surreal<dataT, N> a,
//!     const dataT& b
//! \brief < of Surreal a and  dataT b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of < operator
//! \param[in] b The right side of < operator
//! \return boolean result of a value < b ;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator <(const Surreal<dataT, N>& a,
                       const dataT& b)
{
  return(a.Value()<b);
} // End operator <

//****************************************************************************80
//! \fn Surreal<dataT, N> operator <(dataT a,
//!     const Surreal<dataT,N>& b)
//! \brief < of dataT a and  Surreal b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of < operator
//! \param[in] b The right side of < operator
//! \return boolean result of a < b value ;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator <(const dataT& a,
                       const Surreal<dataT, N>& b)
{
  return(a<b.Value());
} // End operator <
//****************************************************************************80
//! \fn Surreal<dataT, N> operator >(Surreal<dataT, N> a,
//!     const Surreal<dataT, N>& b
//! \brief > of Surreals a and  b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of > operator
//! \param[in] b The right side of > operator
//! \return boolean result of a value > b value;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator >(const Surreal<dataT, N>& a,
                       const Surreal<dataT, N>& b)
{
  return(a.Value()>b.Value());
} // End operator >
//****************************************************************************80
//! \fn Surreal<dataT, N> operator >(Surreal<dataT, N> a,
//!     const dataT& b
//! \brief > of Surreal a and  dataT b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of > operator
//! \param[in] b The right side of > operator
//! \return boolean result of a value > b ;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator >(const Surreal<dataT, N>& a,
                       const dataT& b)
{
  return(a.Value()>b);
} // End operator >

//****************************************************************************80
//! \fn Surreal<dataT, N> operator >(dataT a,
//!     const Surreal<dataT,N>& b)
//! \brief > of dataT a and  Surreal b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of > operator
//! \param[in] b The right side of > operator
//! \return boolean result of a > b value ;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator >(const dataT& a,
                       const Surreal<dataT, N>& b)
{
  return(a>b.Value());
} // End operator >
//****************************************************************************80
//! \fn Surreal<dataT, N> operator <=(Surreal<dataT, N> a,
//!     const Surreal<dataT, N>& b
//! \brief <= of Surreals a and  b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of <= operator
//! \param[in] b The right side of <= operator
//! \return boolean result of a value <= b value;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator <=(const Surreal<dataT, N>& a,
                       const Surreal<dataT, N>& b)
{
  return(a.Value()<=b.Value());
} // End operator <=
//****************************************************************************80
//! \fn Surreal<dataT, N> operator <=(Surreal<dataT, N> a,
//!     const dataT& b
//! \brief <= of Surreal a and  dataT b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of <= operator
//! \param[in] b The right side of <= operator
//! \return boolean result of a value <= b ;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator <=(const Surreal<dataT, N>& a,
                       const dataT& b)
{
  return(a.Value()<=b);
} // End operator <=

//****************************************************************************80
//! \fn Surreal<dataT, N> operator <=(dataT a,
//!     const Surreal<dataT,N>& b)
//! \brief <= of dataT a and  Surreal b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of <= operator
//! \param[in] b The right side of <= operator
//! \return boolean result of a <= b value ;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator <=(const dataT& a,
                       const Surreal<dataT, N>& b)
{
  return(a<=b.Value());
} // End operator <=
//****************************************************************************80
//! \fn Surreal<dataT, N> operator >=(Surreal<dataT, N> a,
//!     const Surreal<dataT, N>& b
//! \brief >= of Surreals a and  b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of >= operator
//! \param[in] b The right side of >= operator
//! \return boolean result of a value >= b value;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator >=(const Surreal<dataT, N>& a,
                       const Surreal<dataT, N>& b)
{
  return(a.Value()>=b.Value());
} // End operator >=
//****************************************************************************80
//! \fn Surreal<dataT, N> operator >=(Surreal<dataT, N> a,
//!     const dataT& b
//! \brief >= of Surreal a and  dataT b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of >= operator
//! \param[in] b The right side of >= operator
//! \return boolean result of a value >= b ;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator >=(const Surreal<dataT, N>& a,
                       const dataT& b)
{
  return(a.Value()>=b);
} // End operator >=

//****************************************************************************80
//! \fn Surreal<dataT, N> operator >=(dataT a,
//!     const Surreal<dataT,N>& b)
//! \brief >= of dataT a and  Surreal b
//! \details
//! \ryan
//! \version $Rev$
//! \date $Date$
//! \param[in] a The left side of >= operator
//! \param[in] b The right side of >= operator
//! \return boolean result of a >= b value ;
//****************************************************************************80
template <typename dataT, int N>
inline bool operator >=(const dataT& a,
                       const Surreal<dataT, N>& b)
{
  return(a>=b.Value());
} // End operator >=


#endif
