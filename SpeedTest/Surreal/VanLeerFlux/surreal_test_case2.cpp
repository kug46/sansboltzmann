#include <iostream>
#include <stdlib.h>
#include "Surreal_lazy.h"

using namespace std;

#ifndef nderiv
#define nderiv 4
#endif

__attribute__((noinline)) void flux( Surreal<double, nderiv> *q, Surreal<double, nderiv> *f, double nx, double ny, double gam, double l);

int main(int argc, char **argv)
{
  Surreal<double, nderiv> q[4], f[4];
  double nx = 0.25,ny = 0.75,gam = 1.4,l = 1.0;
  int iter;
  q[0] = 1.0;
  q[1] = 1.25;
  q[2] = 0.0;
  q[3] = 1.75;

  iter = 1e6;
  if(argc > 1) {
    iter = atoi(argv[1]);
  }


  for(int i = 0; i < iter; i++) { //1e8
    for(int j = 0; j < 4; j ++)
      q[j].set_deriv(j, 1.0);

    flux(q, f, nx, ny, gam, l);
  }
  for(int j=0; j < 4; j++) {
    cout << "j   " << f[j].Value() << endl;
    for(int k=0; k < nderiv; k++)
      cout << "j,k   " << f[j].Deriv(k) << endl;
  }

  return 0;
}
