#include <iostream>
#include <stdlib.h>
#include "Surreal/SurrealD.h"

using namespace std;

#ifndef nderiv
#define nderiv 4
#endif

template<class T>
__attribute__((noinline)) void flux( const T *q, T *f, double nx, double ny, double gam, double l);

int main(int argc, char **argv)
{
  SurrealD q[4], f[4];
  double nx = 0.25,ny = 0.75,gam = 1.4,l = 1.0;

  SurrealD size4(0,0,nderiv);
  for(int j = 0; j < 4; j ++)
  {
    q[j] = size4;
  }


  q[0].value() = 1.0;
  q[1].value() = 1.25;
  q[2].value() = 0.0;
  q[3].value() = 1.75;

  int iter = 1;
  if(argc > 1) {
      iter = atoi(argv[1]);
  }

  for(int i = 0; i < iter; i++) { //1e8
    for(int j = 0; j < 4; j ++)
      q[j].deriv(j) = 1.0;

    flux(q, f, nx, ny, gam, l);
  }
  for(int j=0; j < 4; j++) {
    cout << "j   " << f[j].value() << endl;
    for(int k=0; k < nderiv; k++)
      cout << "j,k   " << f[j].deriv(k) << endl;
  }

  return 0;
}
