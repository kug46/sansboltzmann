#!/usr/bin/tclsh
# Don't use DBL_* or FLT_* and use std::numeric_limits instead

foreach fileName [getSourceFileNames] {
    foreach t [getTokens $fileName 1 0 -1 -1 {}] {
        set value [lindex $t 0]
        set line [lindex $t 1]
        set column [lindex $t 2]
        set name [lindex $t 3]

        if {${name} == "identifier" && ($value == "DBL_EPSILON" || $value == "FLT_EPSILON")} {
            report $fileName $line "Please use 'std::numeric_limits<Real>::epsilon()' instead of '${value}'"
            
        } elseif {${name} == "identifier" && ($value == "DBL_MAX" || $value == "FLT_MAX")} {
            report $fileName $line "Please use 'std::numeric_limits<Real>::max()' instead of '${value}'"
            
        } elseif {${name} == "identifier" && ($value == "DBL_MIN" || $value == "FLT_MIN")} {
            report $fileName $line "Please use 'std::numeric_limits<Real>::min()' instead of '${value}'"
            
        } elseif {${name} == "identifier" && ([regexp {DBL_.*} $value] || [regexp {FLT_.*} $value])} {
            report $fileName $line "Please use 'std::numeric_limits<Real>' instead of '${value}'"
            
        }
    }
}