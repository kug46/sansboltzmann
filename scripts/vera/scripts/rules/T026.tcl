#!/usr/bin/tclsh
# Don't use <math.h> and use <cmath> instead

foreach fileName [getSourceFileNames] {
    foreach t [getTokens $fileName 1 0 -1 -1 {}] {
        set value [lindex $t 0]
        set line [lindex $t 1]
        set column [lindex $t 2]
        set name [lindex $t 3]
 
        if {$name == "pp_hheader" && [string match "*math.h*" $value]} {
            report $fileName $line "Please use '#include <cmath>' instead of '${value}'"
        }
    }
}
