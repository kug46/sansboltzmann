#!/usr/bin/tclsh
# Don't use SANS_ASSERT in unit tests

foreach fileName [getSourceFileNames] {
    # Only look in *_btest.cpp files
    if [regexp {.*_btest.cpp} $fileName] {
        foreach t [getTokens $fileName 1 0 -1 -1 {}] {
            set value [lindex $t 0]
            set line [lindex $t 1]
            set column [lindex $t 2]
            set name [lindex $t 3]

            if {${name} == "identifier" && [regexp {SANS_ASSERT.*} $value]} {
                report $fileName $line "Please use 'BOOST_REQUIRE' instead of 'SANS_ASSERT' in unit tests"
            }
        }
    }
}
