#!/usr/bin/tclsh
# SANS_ASSERT should always be used instead of assert

foreach f [getSourceFileNames] {
    foreach t [getTokens $f 1 0 -1 -1 {}] {
        set value [lindex $t 0]
        set line [lindex $t 1]
        set column [lindex $t 2]
        set name [lindex $t 3]

        if {${name} == "identifier" && ${value} == "assert"} {
            report $f $line "Please use 'SANS_ASSERT' instead of 'assert'"
        }
    }
}
