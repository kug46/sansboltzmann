#!/usr/bin/tclsh
# else should not be preseeded by any tokens

foreach f [getSourceFileNames] {
    foreach t [getTokens $f 1 0 -1 -1 {else enum}] {
        set value [lindex $t 0]
        set line [lindex $t 1]
        set column [lindex $t 2]
        set preceding [getTokens $f $line 0 $line $column {}]
        foreach char $preceding {
           set name [lindex $char end]
           if {$name != "space"} {
             report $f $line "'$value' should only be preceded by whitespaces"
             break
           }
        }
    }
}
