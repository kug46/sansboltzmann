#!/usr/bin/tclsh
# BOOST_THROW_EXCEPTION should always be used instead of throw

foreach f [getSourceFileNames] {
    foreach t [getTokens $f 1 0 -1 -1 {throw}] {
        set value [lindex $t 0]
        set line [lindex $t 1]
        set column [lindex $t 2]
        set name [lindex $t 3]

        set preceding [getTokens $f $line 0 $line $column {}]
        set onlyspaces 1
        foreach char $preceding {
            set name [lindex $char end]
            if {$name != "space"} {
                set onlyspaces 0
                break
            }
        }
        set post [getTokens $f $line $column $line [expr $column + 1] {semicolon}]
	    
        if { $onlyspaces == 1 && [llength post] == 0 } {
            report $f $line "Please use 'BOOST_THROW_EXCEPTION' instead of 'throw'"
        }
    }
}
