#!/usr/bin/tclsh
# Source files should contain the SANS copyright notice

set copyrightfile [getParameter "copyright" "Copyright file not given as a parameter"]
set ncopyright [getLineCount $copyrightfile]

foreach file [getSourceFileNames] {
    for {set i 1} {$i <= $ncopyright} {incr i} {

        set line [getLine $file $i]
        set copyline [getLine $copyrightfile $i]
        if {$line != $copyline} {
            report $file $i "copyright notice is incorrect, expected: $copyline"
        }
    }
}
