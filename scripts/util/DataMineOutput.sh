#!/bin/bash
READFILE=$1
FILENOEXT=${READFILE%.*}
WRITEFILE=$FILENOEXT".log"


STRINGS=(
          "Primal solve time"
          "Adjoint order=1 solve time"
          "Adjoint order=2 solve time"
          "Adjoint order=3 solve time"
          "Error estimation time"
          "Wall time"
          "CPU time"
          "MOESS optimization time"
          "EPIC execution time"
        );

echo "Writing to file: "$WRITEFILE
echo -e "Log File output for "$READFILE > $WRITEFILE
# look for lines starting with the string
# remove the search string from the line
# extract the numbers from the line (including scientific notation)
# then remove the s if it didn't get killed already
for STR in "${STRINGS[@]}"
do
  echo "$STR"
  echo -e "\n""$STR" >> $WRITEFILE
  grep "$STR" $READFILE | sed "s|$STR||g" | egrep -o "[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?." | sed 's/s//g' >> $WRITEFILE
done
