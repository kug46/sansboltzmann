#!/usr/bin/env bash

# InsertCopyright.sh
#---------------------------------------------------------
# Adds license blurb to top of specified files.  Note that
# this script does check if the license blurb already 
# exists.
#
# Usage:
#   InsertCopyright.sh [filename]
#
#     filename -- list of files
#     
#     If no filename is specified the blurb is applied to 
#     all .c* and .h files found.
#---------------------------------------------------------
 

# Find the top level directory
WORKSPACE=$(git rev-parse --show-toplevel)

files=$@

# If file list not provided, get all possible source files
if [ -z "$files" ]; then
    files=`find $WORKSPACE/src -name "*.[c,h]*" | grep -v -e "Documentation"`
    files+=`find $WORKSPACE/test -name "*.[c,h]*" | grep -v -e "Documentation"`
fi

temp='insertTempFile'
blurb="$WORKSPACE/COPYRIGHT.txt"

# Add blurb to top of all files in file list
for file in $files; do
    echo $file
    if (grep "// Copyright" $file); then
      echo "Copyright already in file, skipping"
      continue
    fi
    cp $file $temp
    cat  $blurb > $file
    cat $temp >> $file
    rm $temp
done
