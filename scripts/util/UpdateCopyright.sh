#!/usr/bin/env bash

# UpdateCopyright.sh
#---------------------------------------------------------
# Updates data of license blurb to top of specified files.
#
# Usage:
#   InsertCopyright.sh [filename]
#
#     filename -- list of files
#     
#     If no filename is specified the blurb is applied to 
#     all .c* and .h files found.
#---------------------------------------------------------
 

# Find the top level directory
WORKSPACE=$(git rev-parse --show-toplevel)

files=$@

# If file list not provided, get all possible source files
if [ -z "$files" ]; then
    files="$WORKSPACE/COPYRIGHT.txt "
    files+=`find $WORKSPACE/src -name "*.[c,h]*" | grep -v -e "Documentation"`
    files+=`find $WORKSPACE/test -name "*.[c,h]*" | grep -v -e "Documentation"`
fi

# Update dates top of all files in file list
for file in $files; do
    echo $file
    sed -i 's/\/\/ Copyright 2013-2016/\/\/ Copyright 2013-2019/g' $file
done
