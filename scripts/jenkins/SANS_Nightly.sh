#!/bin/bash

source $WORKSPACE/scripts/jenkins/copy_external.sh

cmakedir=$WORKSPACE/build/$builddir

rm -rf $WORKSPACE/build
mkdir -p $cmakedir
cd $cmakedir

source $WORKSPACE/scripts/jenkins/jenkins_env.sh

if [[ $builddir == *"cppcheck"* ]]; then
  CMAKE_ARGS="-DUSE_CPPCHECK=ON"
elif [[ $builddir != *"memcheck"* ]]; then
  CMAKE_ARGS="-DSANS_FULLTEST=ON"
elif [[ $builddir != *"intel"* ]]; then
  CMAKE_ARGS="-DUSE_LAPACK=ON"
fi

time source $WORKSPACE/scripts/jenkins/cmake_jenkins.sh \
           -DUSE_HEADERCHECK=OFF \
           -DVALGRIND_MEMCHECK=ON \
           -DUSE_MPI=OFF \
           -DVALGRIND_MEMCHECK_FLAGS="--xml=yes;--xml-file=$cmakedir/test/valgrind.%p.memcheck.xml" \
           -DCCACHE_MAX_SIZE=10G \
           -DCCACHE_CONFIGPATH=${WORKSPACE/jenkins\/workspace\/SANS_Nightly/jenkins/SANS_ccache} \
           $CMAKE_ARGS

if [[ $builddir == *"cppcheck"* ]]; then

  # Run cppcheck on all sources
  make cppcheck

else

  # Compile all external libraries
  time source $WORKSPACE/scripts/jenkins/make_external.sh

  if [[ $builddir != *"memcheck"* ]]; then
    # Build each library to see how long they take
    SANSLIBS=`make -qp | awk -F':' '/^[a-zA-Z0-9][^$#\/\t=]*:([^=]|$)/ {split($1,A,/ /);for(i in A)print A[i]}' | grep "Lib$" | xargs`
    for i in $SANSLIBS
    do
      time make $i | tee -a build.out
    done

    time make unit_build | tee -a build.out
    time make system_build | tee -a build.out
  fi
fi


if [[ $builddir == *"analyze"* ]]; then

  # Don't need to execute when compiled for static analysis
  # Only build the sandbox with the analyzer
  time make sandbox_build | tee -a build.out
 
elif [[ $builddir == *"cppcheck"* ]]; then

  # Need to have a command here...
  echo "cppcheck complete"

elif [[ $builddir == *"memcheck"* ]]; then

  # Run compiler memcheck on all tests
  time make check_unit CTESTARGS="-T Test" && status=0 || status=1
  mv `find $cmakedir -name Test.xml` $WORKSPACE/build/Test_unit_memcheck.xml
  
  if [ $status -eq 1 ]; then exit $status; fi
  
elif [[ $builddir == *"NA"* && `uname -a` != "Darwin"* ]]; then #This is dissabled for now because stackcheck is not working on Ubuntu 14.04

  #This will check both dynamic and static memory
  time make MemoryCheck CTESTARGS="-T Test" && status=0 || status=1
  mv `find $cmakedir -name Test.xml` $WORKSPACE/build/Test_unit_memcheck.xml

  if [ $status -eq 1 ]; then exit $status; fi
  
else

  # Run memcheck on the unit tests
  time make memcheck_unit CTESTARGS="-T Test" && status=0 || status=1
  mv `find $cmakedir -name Test.xml` $WORKSPACE/build/Test_unit_memcheck.xml

  if [ $status -eq 1 ]; then exit $status; fi
fi

if [[ $builddir == *"release"* || $builddir == *"memcheck"* ]]; then

  # Run full tests on system tests
  time make check_system CTESTARGS="-T Test" && status=0 || status=1
  mv `find $cmakedir -name Test.xml` $WORKSPACE/build/Test_system_fulltest.xml
  
  if [ $status -eq 1 ]; then exit $status; fi
fi

#Just to clean up some disk space
time make clean

#Fail the build if any files were generated.
#Count the number of files
cd $WORKSPACE
tmpfiles=`git ls-files --others --exclude-standard | wc -l`

if [ $tmpfiles -ne 0 ]; then
  echo "error: files should not be generated in code pushed to the repository."
  echo "Removing files found"
  for f in `git ls-files --others --exclude-standard`; do
    rm -f $f
  done
  exit 1
fi
