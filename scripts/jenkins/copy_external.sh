#!/bin/bash

#Copy over local versions of external dependencies
if [[ `hostname` == "reynolds"* ]]; then
  cp $HOME/util/SANSexternal/* $WORKSPACE/external/
elif [[ `hostname` == "macys"* ]]; then
  cp /Users/jenkins/util/SANSexternal/* $WORKSPACE/external/
fi
