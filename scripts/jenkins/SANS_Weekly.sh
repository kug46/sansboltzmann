#!/bin/bash

source $WORKSPACE/scripts/jenkins/copy_external.sh

cmakedir=$WORKSPACE/build/$builddir

rm -rf $WORKSPACE/build
mkdir -p $cmakedir
cd $cmakedir

source $WORKSPACE/scripts/jenkins/jenkins_env.sh

if [[ $builddir == *"debug"* ]]; then
  CMAKE_ARGS="-DUSE_MPI=OFF"
fi

# MKL is dissabled on weekly tests as it introduces use of uninitialized variables with valgrind...

time source $WORKSPACE/scripts/jenkins/cmake_jenkins.sh \
           -DUSE_HEADERCHECK=OFF \
           -DCCACHE_MAX_SIZE=10G \
           -DCCACHE_CONFIGPATH=${WORKSPACE/jenkins\/workspace\/SANS_Weekly/jenkins/SANS_ccache} \
           -DVALGRIND_MEMCHECK=ON \
           -DVALGRIND_MEMCHECK_FLAGS="--xml=yes;--xml-file=$cmakedir/test/valgrind.%p.memcheck.xml" \
           $CMAKE_ARGS

# Compile all external libraries
time source $WORKSPACE/scripts/jenkins/make_external.sh

if [[ $builddir != *"debug"* ]]; then

  # Run memcheck on the unit tests
  time make memcheck_unit CTESTARGS="-T Test" && status=0 || status=1
  mv `find $cmakedir -name Test.xml` $WORKSPACE/build/Test_unit_mpi_memcheck.xml

  if [ $status -eq 1 ]; then exit $status; fi
fi

# Run full tests on system tests
time make memcheck_system CTESTARGS="-T Test" && status=0 || status=1
mv `find $cmakedir -name Test.xml` $WORKSPACE/build/Test_system_memcheck.xml

if [ $status -eq 1 ]; then exit $status; fi

#Just to clean up some disk space
time make clean
