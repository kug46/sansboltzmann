#!/bin/bash

cd $WORKSPACE

#Files might linger if a build was aborted
echo "Removing any lingering untracked files"
for f in `git ls-files --others --exclude-standard`; do
  rm -rf $f
done

if [[ $builddir == *"coverage"* ]]; then
  #Update the email template for generating emails
  scp $WORKSPACE/scripts/jenkins/SANS_email.template jenkins@acdl.mit.edu:email-templates/
  scp $WORKSPACE/scripts/jenkins/SANS_parse jenkins@acdl.mit.edu:logparse/
  scp $WORKSPACE/scripts/SANS_developer.sh jenkins@acdl.mit.edu:userContent/
fi

source $WORKSPACE/scripts/jenkins/copy_external.sh

cmakedir=$WORKSPACE/build/$builddir

#Create the build directory if it does not exist
mkdir -p $cmakedir
cd $cmakedir

#Create the boosttest directory if it does not exist
mkdir -p $WORKSPACE/build/boosttest

#Just in case any files are lingering where they should not
rm -rf $WORKSPACE/test/unit/tmp/*
rm -rf $WORKSPACE/test/system/tmp/*
rm -f $WORKSPACE/build/boosttest/*.xml
rm -f $WORKSPACE/build/coverage.xml

if [[ $builddir == *"coverage"* ]]; then
  CMAKE_ARGS="-DUSE_HEADERCHECK=OFF -DUSE_MKL=ON"
else
  CMAKE_ARGS="-DUSE_COTIRE=OFF"
fi
if [[ `hostname` == "reynolds-ubuntu12" ]]; then
  CMAKE_ARGS="$CMAKE_ARGS -DUSE_MPI=OFF -DUSE_PETSC=OFF"
fi
if [[ `hostname` == "reynolds" ]]; then
  CMAKE_ARGS="$CMAKE_ARGS -DUSE_LAPACKE=ON"
fi
CMAKE_ARGS="$CMAKE_ARGS -DVERA_ERROR=ON -DUSE_CCACHE=ON -DCCACHE_MAX_SIZE=10G -DCCACHE_CONFIGPATH=${WORKSPACE/jenkins\/workspace\/SANS_Commit/jenkins/SANS_ccache}"

time source $WORKSPACE/scripts/jenkins/cmake_jenkins.sh -DXML_BOOST_CHECK=ON \
                                                        -DUSE_LAPACK=ON \
                                                        $CMAKE_ARGS

# Copy over the makefile that pipes parallel execution to files
cp $WORKSPACE/scripts/jenkins/Makefile.parallel .

#Number of processors used in compile
nproc=2

if [[ $builddir == *"coverage"* ]]; then
  # Just because we have to clean, use more processors for coverage
  nproc=6
  
  #Coverage information always needs to be completely recompiled
  time make coverage_cleaner | tee -a build.out
  time make clean_cotire | tee -a build.out
  time make global_style_vera | tee -a build.out

  # Build all of SANS
  time make -j $nproc | tee -a build.out

  # Build the single unit and system executables to look for link errors
  time make -j $nproc unit_build | tee -a build.out
  time make -j $nproc system_build | tee -a build.out

  # Build all individial testing files
  time make -j $nproc check_unit_build | tee -a build.out
  time make -j $nproc check_system_build | tee -a build.out

else

  #Build all external libraries
  time make -j $nproc -f Makefile.parallel depends

  #Build all headerchek libraries
  time make -j $nproc -f Makefile.parallel headercheck

  #Build all SANS libraries
  time make -j $nproc -f Makefile.parallel sanslibs

  # Build all (i.e. any non-libraries)
  time make | tee -a build.out

  # Build the sandbox and all the individual testing files
  time make -j $nproc -f Makefile.parallel check_build
  
  # Build the single unit and system executables to look for link errors
  time make -j $nproc -f Makefile.parallel check_link_build
fi

#Run all unit tests individually so their execution time can be limited
time make check_unit

#Run all system tests, covearge will not run tests but add non-covered lines
time make check_system

if [[ $builddir == *"coverage"* ]]; then
  
  time make coverage_info
  if [[ `hostname` == "reynolds" ]]; then
    time python $HOME/util/lcov/lcov_cobertura.py coverage.info -o $WORKSPACE/build/coverage.xml
  #elif [[ `hostname` == "macys.mit.edu" ]]; then
    #python /Users/jenkins/util/lcov/lcov_cobertura.py coverage.info -o $WORKSPACE/build/coverage.xml
  fi
  # Generate html documents
  time make genhtml

  # udpdate coverage information on acdl
  time rsync -rtuc --delete CoverageHTML jenkins@acdl.mit.edu:/var/lib/jenkins/userContent/

  # coverage uses a lot of disk space, be nice and tidy up
  time make coverage_cleaner
  time make clean_cotire
fi

# Remove the pre-compiled header files every 50th build so they are updated next build
#if [[ $builddir != *"intel"* && $(($BUILD_NUMBER % 50)) == 0 ]]; then
#  make clean_cotire
#fi


#Fail the build if any files were generated.
#Count the number of files
cd $WORKSPACE
tmpfiles=`git ls-files --others --exclude=build --exclude=external | wc -l`

if [ $tmpfiles -ne 0 ]; then
  set -x
  echo "error: Files should not be generated in code pushed to the repository."
  echo "error: Files found :"
  for f in `git ls-files --others --exclude=build --exclude=external`; do
    echo "error: $f"
  done
  exit 1
fi


