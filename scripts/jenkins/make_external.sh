#!/bin/bash

#Find list of external libraries to explicitly compile
libexternal=`make -qp | awk -F':' '/^[a-zA-Z0-9][^$#\/\t=]*:([^=]|$)/ {split($1,A,/ /);for(i in A)print A[i]}' | grep "^lib"  | grep -v libMeshb | xargs`

#Compile external libraries and pipe output to a file to prevent failures from warning parser
for lib in `echo $libexternal`
do
  time make $lib > ${lib}.out 2>&1 || cat ${lib}.out
done
