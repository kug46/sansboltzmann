#!/bin/bash

WORKSPACE=$(git rev-parse --show-toplevel)

source $WORKSPACE/scripts/jenkins/jenkins_env.sh

cmake -DBoost_NO_SYSTEM_PATHS=$Boost_NO_SYSTEM_PATHS \
      -DXDIR_DEFAULT=ON \
      -DPython_ADDITIONAL_VERSIONS=2.7 \
      -DUI_CAPSAIM=ON \
      -DUI_OPENCSM_UDF=ON \
      $CMAKEARGS \
      $@ \
      $WORKSPACE
