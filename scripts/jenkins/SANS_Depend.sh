#!/bin/bash

cd $WORKSPACE

#Files might linger if a build was aborted
echo "Removing any lingering untracked files"
for f in `git ls-files --others --exclude-standard`; do
  rm -rf $f
done

source $WORKSPACE/scripts/jenkins/copy_external.sh

cmakedir=$WORKSPACE/build/$builddir

#Create the build directory if it does not exist
mkdir -p $cmakedir
cd $cmakedir

#Create the boosttest directory if it does not exist
mkdir -p $WORKSPACE/build/boosttest

CMAKE_ARGS="$CMAKE_ARGS -DUSE_${DEPEND}=ON -DUSE_COTIRE=OFF"
CMAKE_ARGS="$CMAKE_ARGS -DUSE_CCACHE=ON -DCCACHE_MAX_SIZE=10G -DCCACHE_CONFIGPATH=${WORKSPACE/jenkins\/workspace\/${JOB_NAME%/*}/jenkins/SANS_ccache}"
CMAKE_ARGS="$CMAKE_ARGS -DUSE_VERA=OFF -DUSE_HEADERCHECK=OFF"

source $WORKSPACE/scripts/jenkins/jenkins_env.sh

# Make sure SANS compiles the dependency
unset ${DEPEND}_DIR

cmake -DBoost_NO_SYSTEM_PATHS=$Boost_NO_SYSTEM_PATHS \
      -DXML_BOOST_CHECK=ON \
      -DUSE_LAPACK=ON \
      -DPython_ADDITIONAL_VERSIONS=2.7 \
      -DUI_CAPSAIM=ON \
      -DUI_OPENCSM_UDF=ON \
      $CMAKE_ARGS \
      $WORKSPACE

#Build external dependencies
time source $WORKSPACE/scripts/jenkins/make_external.sh

#Build and execute tests for the dependency
time make ${DEPEND_TARGETS}


#Fail the build if any files were generated.
#Count the number of files
cd $WORKSPACE
tmpfiles=`git ls-files --others --exclude=build --exclude=external | wc -l`

if [ $tmpfiles -ne 0 ]; then
  set -x
  echo "error: Files should not be generated in code pushed to the repository."
  echo "error: Files found :"
  for f in `git ls-files --others --exclude=build --exclude=external`; do
    echo "error: $f"
  done
  exit 1
fi
