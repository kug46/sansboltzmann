#!/bin/bash

shopt -s nocasematch

dir=`pwd`
builddir=`basename $dir`

# cmake also uses this in FindAFLR.cmake to update the AFLR version
export AFLR_VERSION=16.30.11

if [[ `hostname` == *"reynolds"* ]]; then

  if [[ $builddir == *"intel"* || $builddir == *"coverage"* ]]; then
    source /opt/intel/compilers_and_libraries/linux/bin/compilervars.sh intel64 > iccvars.out 2>&1
  fi
  # This makes sure cmake finds open-mpi rather than intel mpi
  export PATH=/usr/bin:$PATH
  export LD_LIBRARY_PATH=/usr/lib:$LD_LIBRARY_PATH

  if [[ $builddir == *"gnu8"* ]]; then
    export PATH=/home/jenkins/util/gcc/gcc-8-20180921/install_dir/bin:$PATH
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/jenkins/util/gcc/gcc-8-20180921/install_dir/lib64
  fi

  export LAPACK_DIR=/home/jenkins/util/lapack/lapack-3.6.1/install
  export ESP_DIR=/home/jenkins/util/ESP/EngSketchPad
  export CAS_DIR=/home/jenkins/util/ESP/OpenCASCADE-7.4.1
  export AFLR_DIR=/home/jenkins/util/AFLR/aflr3_lib_$AFLR_VERSION
  export PETSC_DIR=`ls -d /home/jenkins/util/PETSc/petsc*/install_dir`
  export PARMETIS_DIR=/home/jenkins/util/parmetis/parmetis-4.0.3/install
  export METIS_DIR=/home/jenkins/util/parmetis/metis-5.1.0/install
  export REFINE_DIR=/home/jenkins/util/refine/build_parallel/install
  export AVRO_DIR=/home/jenkins/util/avro
  if [ -d /home/jenkins/util/NLOPT ]; then
    export NLOPT_DIR=`ls -d /home/jenkins/util/NLOPT/nlopt-*/install`
  fi

  export PATH=/home/jenkins/util/vera/vera/install/bin:$PATH
  export PATH=/home/jenkins/util/lcov/lcov-1.13/bin:$PATH
  export PATH=/home/jenkins/util/cppcheck/cppcheck-1.79/:$PATH
  export PATH=/home/jenkins/util/ccache/ccache-3.7.7/install/bin:$PATH
  export PATH=/home/jenkins/util/valgrind/valgrind-3.15.0/install/bin:$PATH

  export PATH=/home/jenkins/util/fefloa/bin:$PATH

  if [[ `hostname` == *"centOS68"* ]]; then
    export PATH=/home/jenkins/util/gcc/gcc-4.8.5/install/bin:$PATH
    export PATH=/home/jenkins/util/vera/vera/install/bin:$PATH
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/jenkins/util/gcc/gcc-4.8.5/install/lib64
    
    export SUITESPARSE_DIR=/home/jenkins/util/suitesparse/SuiteSparse-4.5.3
    export BOOST_ROOT=/home/jenkins/util/boost/boost_1_53_0/install
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BOOST_ROOT/lib
    export BLAS_LIBRARIES=/usr/lib64/libopenblas.so.0
  fi

  if [[ `hostname` == *"ubuntu12"* ]]; then
    export BOOST_ROOT=/home/jenkins/util/boost/boost_1_50_0/install
  fi
  
  if [[ `hostname` == *"rhel"* ]]; then
    export BLAS_LIBRARIES=/usr/lib64/atlas/libsatlas.so
  fi

elif [[ `hostname` == *"macys"* ]]; then

  export PATH=/usr/local/bin:/usr/local/sbin:$PATH
  export LAPACK_DIR=/usr/local/opt/lapack/
  export ESP_DIR=/Users/jenkins/util/ESP/EngSketchPad
  export CAS_DIR=/Users/jenkins/util/ESP/OpenCASCADE-7.4.1/
  export AFLR_DIR=/Users/jenkins/util/AFLR/aflr3_lib_$AFLR_VERSION
  export PETSC_DIR=`ls -d /Users/jenkins/util/PETSc/petsc*/install_dir`
  export PARMETIS_DIR=/Users/jenkins/util/parmetis/parmetis-4.0.3/install

  export PATH=/Users/jenkins/util/fefloa/bin:$PATH

else
  if [ -z "$LAPACK_DIR" ]; then
    echo "Please set LAPACK_DIR in your environment."
    exit 0
  fi
fi

#Use the system version by default
export Boost_NO_SYSTEM_PATHS=OFF

env > env.log 2>&1
