// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONVOLUME_TETRAHEDRON_HIERARCHICAL_H
#define BASISFUNCTIONVOLUME_TETRAHEDRON_HIERARCHICAL_H

// tetrahedron basis functions: Hierarchical

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionVolume.h


#include <array>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionVolume.h"
#include "BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// volume basis functions: Hierarchical
//
// reference tetrahedron element defined: s in [0, 1], t in [0, 1-s], u in [0, 1-s-t]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionVolume_Tet_HierarchicalPMax = 2;


//----------------------------------------------------------------------------//
// Hierarchical: linear

template <>
class BasisFunctionVolume<Tet,Hierarchical,1> :
  public BasisFunctionVolumeBase<Tet>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 4; }
  virtual int nBasisNode() const { return 4; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int4&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int4&,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
public:
  static const BasisFunctionVolume* self();
};

//----------------------------------------------------------------------------//
// Hierarchical: quadratic

template <>
class BasisFunctionVolume<Tet,Hierarchical,2> :
  public BasisFunctionVolumeBase<Tet>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 2; }
  virtual int nBasis() const { return 10; }
  virtual int nBasisNode() const { return 4; }
  virtual int nBasisEdge() const { return 6; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int4&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int4&,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
public:
  static const BasisFunctionVolume* self();
};

}

#endif  // BASISFUNCTIONVOLUME_TETRAHEDRON_HIERARCHICAL_H
