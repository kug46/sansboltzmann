// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionArea_Quad_Legendre.h"

// Construct tensor products with the linear Legendre basis
#include "BasisFunctionLine_Legendre.h"

#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Legendre: constant

void
BasisFunctionArea<Quad,Legendre,0>::evalBasis(
    const Real&, const Real&, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phi[0] = 1;
}


void
BasisFunctionArea<Quad,Legendre,0>::evalBasisDerivative(
    const Real&, const Real&, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phis[0] = 0;
  phit[0] = 0;
}


void
BasisFunctionArea<Quad,Legendre,0>::evalBasisHessianDerivative(
    const Real&, const Real&, const Int4&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phiss[0] = 0;
  phist[0] = 0;
  phitt[0] = 0;

}

//----------------------------------------------------------------------------//
// Legendre: linear
void
BasisFunctionArea<Quad,Legendre,1>::tensorProduct(
    const Real sphi[], const Real tphi[], Real phi[] ) const
{
  // P0
  phi[0] = sphi[0]*tphi[0];

  // P1
  phi[1] = sphi[1]*tphi[0];
  phi[2] = sphi[0]*tphi[1];
  phi[3] = sphi[1]*tphi[1];
}

void
BasisFunctionArea<Quad,Legendre,1>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real sphi[2], tphi[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasis(s, sphi, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasis(t, tphi, 2);

  tensorProduct(sphi, tphi, phi);
}


void
BasisFunctionArea<Quad,Legendre,1>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real sphi[2], tphi[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasis(s, sphi, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasis(t, tphi, 2);

  Real sphis[2], tphit[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(s, sphis, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(t, tphit, 2);

  tensorProduct(sphis, tphi , phis);
  tensorProduct(sphi , tphit, phit);
}


void
BasisFunctionArea<Quad,Legendre,1>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int4&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);


  Real sphi[2], tphi[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasis(s, sphi, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasis(t, tphi, 2);

  Real sphis[2], tphit[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(s, sphis, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(t, tphit, 2);

  Real sphiss[2], tphitt[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(s, sphiss, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(t, tphitt, 2);

  tensorProduct(sphiss, tphi  , phiss);
  tensorProduct(sphis , tphit , phist);
  tensorProduct(sphi  , tphitt, phitt);
}

//----------------------------------------------------------------------------//
// Legendre: quadratic

void
BasisFunctionArea<Quad,Legendre,2>::tensorProduct(
    const Real sphi[], const Real tphi[], Real phi[] ) const
{
  // P0
  phi[0] = sphi[0]*tphi[0];

  // P1
  phi[1] = sphi[1]*tphi[0];
  phi[2] = sphi[0]*tphi[1];
  phi[3] = sphi[1]*tphi[1];

  // P2
  phi[4] = sphi[2]*tphi[0];
  phi[5] = sphi[0]*tphi[2];
  phi[6] = sphi[2]*tphi[1];
  phi[7] = sphi[1]*tphi[2];
  phi[8] = sphi[2]*tphi[2];
}

void
BasisFunctionArea<Quad,Legendre,2>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 9);

  Real sphi[3], tphi[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasis(t, tphi, 3);

  tensorProduct(sphi, tphi, phi);
}

void
BasisFunctionArea<Quad,Legendre,2>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 9);

  Real sphi[3], tphi[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasis(t, tphi, 3);

  Real sphis[3], tphit[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(s, sphis, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(t, tphit, 3);

  tensorProduct(sphis, tphi , phis);
  tensorProduct(sphi , tphit, phit);
}

void
BasisFunctionArea<Quad,Legendre,2>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int4&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 9);

  Real sphi[3], tphi[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasis(t, tphi, 3);

  Real sphis[3], tphit[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(s, sphis, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(t, tphit, 3);

  Real sphiss[3], tphitt[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(s, sphiss, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(t, tphitt, 3);

  tensorProduct(sphiss, tphi  , phiss);
  tensorProduct(sphis , tphit , phist);
  tensorProduct(sphi  , tphitt, phitt);
}


//----------------------------------------------------------------------------//
// Legendre: cubic

void
BasisFunctionArea<Quad,Legendre,3>::tensorProduct(
    const Real sphi[], const Real tphi[], Real phi[] ) const
{
  // P0
  phi[0] = sphi[0]*tphi[0];

  // P1
  phi[1] = sphi[1]*tphi[0];
  phi[2] = sphi[0]*tphi[1];
  phi[3] = sphi[1]*tphi[1];

  // P2
  phi[4] = sphi[2]*tphi[0];
  phi[5] = sphi[0]*tphi[2];
  phi[6] = sphi[2]*tphi[1];
  phi[7] = sphi[1]*tphi[2];
  phi[8] = sphi[2]*tphi[2];

  // P3
  phi[9]  = sphi[3]*tphi[0];
  phi[10] = sphi[0]*tphi[3];
  phi[11] = sphi[3]*tphi[1];
  phi[12] = sphi[1]*tphi[3];
  phi[13] = sphi[3]*tphi[2];
  phi[14] = sphi[2]*tphi[3];
  phi[15] = sphi[3]*tphi[3];
}

void
BasisFunctionArea<Quad,Legendre,3>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 16);

  Real sphi[4], tphi[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasis(t, tphi, 4);

  tensorProduct(sphi, tphi, phi);
}

void
BasisFunctionArea<Quad,Legendre,3>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 16);

  Real sphi[4], tphi[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasis(t, tphi, 4);

  Real sphis[4], tphit[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(s, sphis, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(t, tphit, 4);

  tensorProduct(sphis, tphi , phis);
  tensorProduct(sphi , tphit, phit);
}

void
BasisFunctionArea<Quad,Legendre,3>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int4&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 16);

  Real sphi[4], tphi[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasis(t, tphi, 4);

  Real sphis[4], tphit[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(s, sphis, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(t, tphit, 4);

  Real sphiss[4], tphitt[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(s, sphiss, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(t, tphitt, 4);

  tensorProduct(sphiss, tphi  , phiss);
  tensorProduct(sphis , tphit , phist);
  tensorProduct(sphi  , tphitt, phitt);
}


//----------------------------------------------------------------------------//
// Legendre: quartic

void
BasisFunctionArea<Quad,Legendre,4>::tensorProduct(
    const Real sphi[], const Real tphi[], Real phi[] ) const
{
  // P0
  phi[0] = sphi[0]*tphi[0];

  // P1
  phi[1] = sphi[1]*tphi[0];
  phi[2] = sphi[0]*tphi[1];
  phi[3] = sphi[1]*tphi[1];

  // P2
  phi[4] = sphi[2]*tphi[0];
  phi[5] = sphi[0]*tphi[2];
  phi[6] = sphi[2]*tphi[1];
  phi[7] = sphi[1]*tphi[2];
  phi[8] = sphi[2]*tphi[2];

  // P3
  phi[9]  = sphi[3]*tphi[0];
  phi[10] = sphi[0]*tphi[3];
  phi[11] = sphi[3]*tphi[1];
  phi[12] = sphi[1]*tphi[3];
  phi[13] = sphi[3]*tphi[2];
  phi[14] = sphi[2]*tphi[3];
  phi[15] = sphi[3]*tphi[3];

  // P4
  phi[16] = sphi[4]*tphi[0];
  phi[17] = sphi[0]*tphi[4];
  phi[18] = sphi[4]*tphi[1];
  phi[19] = sphi[1]*tphi[4];
  phi[20] = sphi[4]*tphi[2];
  phi[21] = sphi[2]*tphi[4];
  phi[22] = sphi[4]*tphi[3];
  phi[23] = sphi[3]*tphi[4];
  phi[24] = sphi[4]*tphi[4];
}

void
BasisFunctionArea<Quad,Legendre,4>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 25);

  Real sphi[5], tphi[5];
  BasisFunctionLine<Legendre,4>::self()->evalBasis(s, sphi, 5);
  BasisFunctionLine<Legendre,4>::self()->evalBasis(t, tphi, 5);

  tensorProduct(sphi, tphi, phi);
}

void
BasisFunctionArea<Quad,Legendre,4>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 25);

  Real sphi[5], tphi[5];
  BasisFunctionLine<Legendre,4>::self()->evalBasis(s, sphi, 5);
  BasisFunctionLine<Legendre,4>::self()->evalBasis(t, tphi, 5);

  Real sphis[5], tphit[5];
  BasisFunctionLine<Legendre,4>::self()->evalBasisDerivative(s, sphis, 5);
  BasisFunctionLine<Legendre,4>::self()->evalBasisDerivative(t, tphit, 5);

  tensorProduct(sphis, tphi , phis);
  tensorProduct(sphi , tphit, phit);
}

void
BasisFunctionArea<Quad,Legendre,4>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int4&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 25);

  Real sphi[5], tphi[5];
  BasisFunctionLine<Legendre,4>::self()->evalBasis(s, sphi, 5);
  BasisFunctionLine<Legendre,4>::self()->evalBasis(t, tphi, 5);

  Real sphis[5], tphit[5];
  BasisFunctionLine<Legendre,4>::self()->evalBasisDerivative(s, sphis, 5);
  BasisFunctionLine<Legendre,4>::self()->evalBasisDerivative(t, tphit, 5);

  Real sphiss[5], tphitt[5];
  BasisFunctionLine<Legendre,4>::self()->evalBasisHessianDerivative(s, sphiss, 5);
  BasisFunctionLine<Legendre,4>::self()->evalBasisHessianDerivative(t, tphitt, 5);

  tensorProduct(sphiss, tphi  , phiss);
  tensorProduct(sphis , tphit , phist);
  tensorProduct(sphi  , tphitt, phitt);
}

#if 0
//----------------------------------------------------------------------------//
// Legendre: quintic

void
BasisFunctionArea<Quad,Legendre,5>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 21);
}


void
BasisFunctionArea<Quad,Legendre,5>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 21);
}


//----------------------------------------------------------------------------//
// Legendre: P6

void
BasisFunctionArea<Quad,Legendre,6>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 28);
}


void
BasisFunctionArea<Quad,Legendre,6>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 28);
}


//----------------------------------------------------------------------------//
// Legendre: P7

void
BasisFunctionArea<Quad,Legendre,7>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 36);
}


void
BasisFunctionArea<Quad,Legendre,7>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 36);
}
#endif

}
