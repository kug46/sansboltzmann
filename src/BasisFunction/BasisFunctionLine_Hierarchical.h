// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONLINE_HIERARCHICAL_H
#define BASISFUNCTIONLINE_HIERARCHICAL_H

// line basis functions
// implemented:
//   Hierarchical: P1 - P7

// NOTES:
// - Implemented via abstract base class (ABC)
// - Derived classes only contain static data. Hence, they are
//   constructed to be singletons. Using templates instead of ABC
//   would lead to code bloat


#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "BasisFunctionLineBase.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// line basis functions
//
// reference line element defined: s in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edge interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinate
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionLine_HierarchicalPMax = 7;

//----------------------------------------------------------------------------//
// Hierarchical: linear
template<>
class BasisFunctionLine<Hierarchical,1> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 1; }
  virtual int nBasis() const override { return 2; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 0; }
  virtual BasisFunctionCategory category() const override  { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Hierarchical: quadratic
template<>
class BasisFunctionLine<Hierarchical,2> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 2; }
  virtual int nBasis() const override { return 3; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 1; }
  virtual BasisFunctionCategory category() const override  { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Hierarchical: cubic
template<>
class BasisFunctionLine<Hierarchical,3> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 3; }
  virtual int nBasis() const override { return 4; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 2; }
  virtual BasisFunctionCategory category() const override  { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Hierarchical: quartic
template<>
class BasisFunctionLine<Hierarchical,4> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 4; }
  virtual int nBasis() const override { return 5; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 3; }
  virtual BasisFunctionCategory category() const override  { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Hierarchical: quintic
template<>
class BasisFunctionLine<Hierarchical,5> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 5; }
  virtual int nBasis() const override { return 6; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 4; }
  virtual BasisFunctionCategory category() const override  { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Hierarchical: P6
template<>
class BasisFunctionLine<Hierarchical,6> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 6; }
  virtual int nBasis() const override { return 7; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 5; }
  virtual BasisFunctionCategory category() const override  { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Hierarchical: P7
template<>
class BasisFunctionLine<Hierarchical,7> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 7; }
  virtual int nBasis() const override { return 8; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 6; }
  virtual BasisFunctionCategory category() const override  { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};

}

#endif  // BASISFUNCTIONLINE_HIERARCHICAL_H
