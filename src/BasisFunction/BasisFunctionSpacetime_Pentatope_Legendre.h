// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNTIONSPACETIME_PENTATOPE_LEGENDRE_H
#define BASISFUNTIONSPACETIME_PENTATOPE_LEGENDRE_H

// pentatope basis functions: Legendre

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionSpacetime.h

#include <array>

#include "tools/SANSnumerics.h"   // real

#include "Topology/ElementTopology.h"

#include "BasisFunctionSpacetime.h"
#include "BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// spacetime basis functions: Legendre
//
// reference triangle element defined: s in [0, 1], t in [0, 1-s], u in [0, 1-s-t],
// and v in [0, 1 - s - t - u]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionSpacetime_Pentatope_LegendrePMax= 4;

//----------------------------------------------------------------------------//
// Legendre: const

template <>
class BasisFunctionSpacetime<Pentatope, Legendre, 0> :
  public BasisFunctionSpacetimeBase<Pentatope>
{
public:
  static const int D= 4;     // physical dimensions

  typedef std::array<int, 5> Int5;

  virtual ~BasisFunctionSpacetime() {}

  virtual int order() const { return 0; }
  virtual int nBasis() const { return 1; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisArea() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                          const Int5&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                                    const Int5&, Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef, const Int5& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[],
                                           Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionSpacetime() {}
public:
  static const BasisFunctionSpacetime* self();
};

//----------------------------------------------------------------------------//
// Legendre: linear

template <>
class BasisFunctionSpacetime<Pentatope, Legendre, 1> :
  public BasisFunctionSpacetimeBase<Pentatope>
{
public:
  static const int D= 4;     // physical dimensions

  typedef std::array<int, 5> Int5;

  virtual ~BasisFunctionSpacetime() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 5; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisArea() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 5; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                          const Int5&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                                    const Int5&, Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef, const Int5& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[],
                                           Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionSpacetime() {}
public:
  static const BasisFunctionSpacetime* self();
};

//----------------------------------------------------------------------------//
// Legendre: quadratic

template <>
class BasisFunctionSpacetime<Pentatope, Legendre, 2> :
  public BasisFunctionSpacetimeBase<Pentatope>
{
public:
  static const int D= 4;     // physical dimensions

  typedef std::array<int, 5> Int5;

  virtual ~BasisFunctionSpacetime() {}

  virtual int order() const { return 2; }
  virtual int nBasis() const { return 15; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisArea() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 15; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                          const Int5&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                                    const Int5&, Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef, const Int5& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[],
                                           Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionSpacetime() {}
public:
  static const BasisFunctionSpacetime* self();
};

//----------------------------------------------------------------------------//
// Legendre: cubic

template <>
class BasisFunctionSpacetime<Pentatope, Legendre, 3> :
  public BasisFunctionSpacetimeBase<Pentatope>
{
public:
  static const int D= 4;     // physical dimensions

  typedef std::array<int, 5> Int5;

  virtual ~BasisFunctionSpacetime() {}

  virtual int order() const { return 3; }
  virtual int nBasis() const { return 35; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisArea() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 35; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                          const Int5&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                                    const Int5&, Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef, const Int5& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[],
                                           Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionSpacetime() {}
public:
  static const BasisFunctionSpacetime* self();
};


//----------------------------------------------------------------------------//
// Legendre: P4

template <>
class BasisFunctionSpacetime<Pentatope, Legendre, 4> :
  public BasisFunctionSpacetimeBase<Pentatope>
{
public:
  static const int D= 4;     // physical dimensions

  typedef std::array<int, 5> Int5;

  virtual ~BasisFunctionSpacetime() {}

  virtual int order() const { return 4; }
  virtual int nBasis() const { return 70; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisArea() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 70; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                          const Int5&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                                    const Int5&, Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef, const Int5& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[],
                                           Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionSpacetime() {}
public:
  static const BasisFunctionSpacetime* self();
};

}

#endif
