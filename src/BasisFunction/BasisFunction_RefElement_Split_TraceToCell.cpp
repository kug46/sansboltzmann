// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction_RefElement_Split_TraceToCell.h"
#include "ReferenceElement.h"

#include "tools/SANSException.h"

namespace SANS
{


void
BasisFunction_RefElement_Split_TraceToCell<TopoD1, Line>::transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type,
                                                                    int split_edge_index, int sub_trace_index, RefCoordCell& Refcoord_cell)
{
  if (split_type == ElementSplitType::Isotropic)
  {
    if (split_edge_index == -1)
    {
      if ( sub_trace_index == 0 )
      {
        if (Refcoord_trace[0] == 0.0)
        {
          Refcoord_cell[0] = ReferenceElement<Line>::centerRef[0];
        }
        else
          SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD1,Line> - "
                                   "Reference coordinate for a node should be 0.0.");
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD1,Line> - "
                                 "Sub-trace index (=%d) should be 0.", sub_trace_index);
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD1,Line> - "
                               "Split edge index (=%d) should be -1 for an isotropic-split.", split_edge_index);
  }
  else
    SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD1,Line> - Invalid ElementSplitType.");
}

void
BasisFunction_RefElement_Split_TraceToCell<TopoD2, Triangle>::transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type,
                                                                        int split_edge_index, int sub_trace_index, RefCoordCell& Refcoord_cell)
{
  Real s = Refcoord_trace[0];

  if (split_type == ElementSplitType::Edge)
  {
    if ( sub_trace_index == 0 )
    {
      if (split_edge_index == 0) //Splitting trace0
      {
        Refcoord_cell[0] = 0.5*(1.0 - s);
        Refcoord_cell[1] = 0.5*(1.0 - s);
      }
      else if (split_edge_index == 1) //Splitting trace1
      {
        Refcoord_cell[0] = s;
        Refcoord_cell[1] = 0.5*(1.0 - s);
      }
      else if (split_edge_index == 2) //Splitting trace2
      {
        Refcoord_cell[0] = 0.5*(1.0 - s);
        Refcoord_cell[1] = s;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD2,Triangle> - "
                                 "Split edge index (=%d) should be 0, 1 or 2 for an edge-split.", split_edge_index);
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD2,Triangle> - "
                               "Sub-trace index (=%d) should be 0.", sub_trace_index);
  }
  //-------------------------------------------------------------------------------------------------------------------
  else if (split_type == ElementSplitType::Isotropic)
  {
    if (split_edge_index == -1)
    {
      if (sub_trace_index == 0) // between sub-cell 0 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5;
        Refcoord_cell[1] = 0.5*(1.0 - s);
      }
      else if (sub_trace_index == 1) // between sub-cell 1 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*s;
        Refcoord_cell[1] = 0.5;
      }
      else if (sub_trace_index == 2) // between sub-cell 2 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*(1.0 - s);
        Refcoord_cell[1] = 0.5*s;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD2,Triangle> - Invalid sub_trace_index.");
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD2,Triangle> - "
                               "Split edge index (=%d) should be -1 for an isotropic-split.", split_edge_index);
  }
  else
    SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD2,Triangle> - Invalid ElementSplitType.");
}

void
BasisFunction_RefElement_Split_TraceToCell<TopoD2, Quad>::transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type,
                                                                    int split_edge_index, int sub_cell_index, RefCoordCell& Refcoord_cell)
{
  SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD2,Quad> - Quad splitting not supported yet.");
}

void
BasisFunction_RefElement_Split_TraceToCell<TopoD3, Tet>::transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type,
                                                                   int split_edge_index, int sub_trace_index, RefCoordCell& Refcoord_cell)
{
  Real s = Refcoord_trace[0];
  Real t = Refcoord_trace[1];

  if (split_type==ElementSplitType::Edge)
  {
    if ( sub_trace_index == 0 )
    {
      if (split_edge_index == 0) //Splitting edge0
      {
        Refcoord_cell[0] = s;
        Refcoord_cell[1] = 0.5*t;
        Refcoord_cell[2] = 0.5*t;
      }
      else if (split_edge_index == 1) //Splitting edge1
      {
        Refcoord_cell[0] = 0.5*t;
        Refcoord_cell[1] = s;
        Refcoord_cell[2] = 0.5*t;
      }
      else if (split_edge_index == 2) //Splitting edge2
      {
        Refcoord_cell[0] = 0.5*t;
        Refcoord_cell[1] = 0.5*t;
        Refcoord_cell[2] = s;
      }
      else if (split_edge_index == 3) //Splitting edge3
      {
        Refcoord_cell[0] = s;
        Refcoord_cell[1] = 0.5*(1.0 - s - t);
        Refcoord_cell[2] = t;
      }
      else if (split_edge_index == 4) //Splitting edge4
      {
        Refcoord_cell[0] = 1.0 - s - t;
        Refcoord_cell[1] = s;
        Refcoord_cell[2] = 0.5*t;
      }
      else if (split_edge_index == 5) //Splitting edge5
      {
        Refcoord_cell[0] = 0.5*(1.0 - s - t);
        Refcoord_cell[1] = s;
        Refcoord_cell[2] = t;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD3,Tet> - "
                                 "Split edge index (=%d) should be 0, 1, 2, 3, 4 or 5 for an edge-split.", split_edge_index);
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD3,Tet> - "
                               "Sub-trace index (=%d) should be 0.", sub_trace_index);
  }
  //-------------------------------------------------------------------------------------------------------------------
  else if (split_type == ElementSplitType::Isotropic)
  {
    if (split_edge_index == -1)
    {
      if (sub_trace_index==0) // between sub-cell 0 and sub-cell 4
      {
        Refcoord_cell[0] = 0.5*s;
        Refcoord_cell[1] = 0.5;
        Refcoord_cell[2] = 0.5*t;
      }
      else if (sub_trace_index==1) // between sub-cell 1 and sub-cell 5
      {
        Refcoord_cell[0] = 0.5*t;
        Refcoord_cell[1] = 0.5*s;
        Refcoord_cell[2] = 0.5;
      }
      else if (sub_trace_index==2) // between sub-cell 2 and sub-cell 6
      {
        Refcoord_cell[0] = 0.5;
        Refcoord_cell[1] = 0.5*t;
        Refcoord_cell[2] = 0.5*s;
      }
      else if (sub_trace_index==3) // between sub-cell 3 and sub-cell 7
      {
        Refcoord_cell[0] = 0.5*(1.0 - s - t);
        Refcoord_cell[1] = 0.5*s;
        Refcoord_cell[2] = 0.5*t;
      }
      else if (sub_trace_index==4) // between sub-cell 4 and sub-cell 5
      {
        Refcoord_cell[0] = 0.5*s;
        Refcoord_cell[1] = 0.5*(1.0 - s);
        Refcoord_cell[2] = 0.5*(s + t);
      }
      else if (sub_trace_index==5) // between sub-cell 4 and sub-cell 6
      {
        Refcoord_cell[0] = 0.5*(s + t);
        Refcoord_cell[1] = 0.5*(1.0 - t);
        Refcoord_cell[2] = 0.5*t;
      }
      else if (sub_trace_index==6) // between sub-cell 5 and sub-cell 7
      {
        Refcoord_cell[0] = 0.5*t;
        Refcoord_cell[1] = 0.5*s;
        Refcoord_cell[2] = 0.5*(1.0 - s);
      }
      else if (sub_trace_index==7) // between sub-cell 6 and sub-cell 7
      {
        Refcoord_cell[0] = 0.5*(1.0 - t);
        Refcoord_cell[1] = 0.5*t;
        Refcoord_cell[2] = 0.5*s;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - "
                               "Split edge index (=%d) should be -1 for an isotropic-split.", split_edge_index);
  }
  //-------------------------------------------------------------------------------------------------------------------
  else if (split_type==ElementSplitType::IsotropicFace)
  {
    if (split_edge_index == 0) //Splitting face0
    {
      if (sub_trace_index==0) // between sub-cell 0 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*s;
        Refcoord_cell[1] = 0.5*(s + t);
        Refcoord_cell[2] = 0.5*t;
      }
      else if (sub_trace_index==1) // between sub-cell 1 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*t;
        Refcoord_cell[1] = 0.5*s;
        Refcoord_cell[2] = 0.5*(s + t);
      }
      else if (sub_trace_index==2) // between sub-cell 2 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*(s + t);
        Refcoord_cell[1] = 0.5*t;
        Refcoord_cell[2] = 0.5*s;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 1) //Splitting face1
    {
      if (sub_trace_index==0) // between sub-cell 0 and sub-cell 3
      {
        Refcoord_cell[0] = t;
        Refcoord_cell[1] = 0.5*s;
        Refcoord_cell[2] = 0.5*(1.0 - t);
      }
      else if (sub_trace_index==1) // between sub-cell 1 and sub-cell 3
      {
        Refcoord_cell[0] = s;
        Refcoord_cell[1] = 0.5*(1.0 - s);
        Refcoord_cell[2] = 0.5*t;
      }
      else if (sub_trace_index==2) // between sub-cell 2 and sub-cell 3
      {
        Refcoord_cell[0] = 1.0 - s - t;
        Refcoord_cell[1] = 0.5*s;
        Refcoord_cell[2] = 0.5*t;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 2) //Splitting face2
    {
      if (sub_trace_index==0) // between sub-cell 0 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*(1.0 - t);
        Refcoord_cell[1] = t;
        Refcoord_cell[2] = 0.5*s;
      }
      else if (sub_trace_index==1) // between sub-cell 1 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*t;
        Refcoord_cell[1] = s;
        Refcoord_cell[2] = 0.5*(1.0 - s);
      }
      else if (sub_trace_index==2) // between sub-cell 2 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*(1.0 - s - t);
        Refcoord_cell[1] = s;
        Refcoord_cell[2] = 0.5*t;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 3) //Splitting face3
    {
      if (sub_trace_index==0) // between sub-cell 0 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*s;
        Refcoord_cell[1] = 0.5*(1.0 - t);
        Refcoord_cell[2] = t;
      }
      else if (sub_trace_index==1) // between sub-cell 1 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*(1.0 - s);
        Refcoord_cell[1] = 0.5*t;
        Refcoord_cell[2] = s;
      }
      else if (sub_trace_index==2) // between sub-cell 2 and sub-cell 3
      {
        Refcoord_cell[0] = 0.5*(1.0 - s - t);
        Refcoord_cell[1] = 0.5*s;
        Refcoord_cell[2] = t;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD3,Tet> - "
                               "Split face index (=%d) should be 0, 1, 2, 3 for an isotropic-face-split.", split_edge_index);
  }
  else
    SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD3,Tet> - Invalid ElementSplitType.");
}

void
BasisFunction_RefElement_Split_TraceToCell<TopoD3, Hex>::transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type,
                                                                   int sub_trace_index, int sub_cell_index, RefCoordCell& Refcoord_cell)
{
  SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD3,Hex> - Hex splitting not supported yet.");
}

void
BasisFunction_RefElement_Split_TraceToCell<TopoD4, Pentatope>::transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type,
                                                                         int sub_trace_index, int sub_cell_index, RefCoordCell& Refcoord_cell)
{
  SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD4,Pentatope> - Pentatope splitting not supported yet.");
}

}
