// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONAREA_TRIANGLE_LAGRANGE_H
#define BASISFUNCTIONAREA_TRIANGLE_LAGRANGE_H

// triangle basis functions: Lagrange

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionLine.h
// - duplicate functionality to BasisFunctionTriangle.h


#include <array>
#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionArea.h"
#include "BasisFunctionCategory.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// area basis functions: Lagrange
//
// reference triangle element defined: s in [0, 1], t in [0, 1-s]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionArea_Triangle_LagrangePMax = 5;

template<class Topology>
struct LagrangeNodes;

template<>
struct LagrangeNodes<Triangle>
{
  static const int PMax = BasisFunctionArea_Triangle_LagrangePMax;

  static void get(const int order, std::vector<DLA::VectorS<TopoD2::D,Real>>& sRef);
};

void getLagrangeNodes_Triangle(const int order, std::vector<Real>& s, std::vector<Real>& t);

//----------------------------------------------------------------------------//
// Lagrange: linear

template <>
class BasisFunctionArea<Triangle,Lagrange,1> :
  public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 3; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int3&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Lagrange: quadratic

template <>
class BasisFunctionArea<Triangle,Lagrange,2> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 2; }
  virtual int nBasis() const { return 6; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 3; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int3&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
public:
  static const BasisFunctionArea* self();
};

//----------------------------------------------------------------------------//
// Lagrange: cubic

template <>
class BasisFunctionArea<Triangle,Lagrange,3> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 3; }
  virtual int nBasis() const { return 10; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 6; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int3& sgn,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
public:
  static const BasisFunctionArea* self();
};

//----------------------------------------------------------------------------//
// Lagrange: quartic

template <>
class BasisFunctionArea<Triangle,Lagrange,4> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 4; }
  virtual int nBasis() const { return 15; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 9; }
  virtual int nBasisCell() const { return 3; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int3& sgn,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Lagrange: quintic

template <>
class BasisFunctionArea<Triangle,Lagrange,5> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 5; }
  virtual int nBasis() const { return 21; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 12; }
  virtual int nBasisCell() const { return 6; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int3& sgn,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
public:
  static const BasisFunctionArea* self();
};


////----------------------------------------------------------------------------//
//// Lagrange: hextic
//
//template <>
//class BasisFunctionArea<Triangle,Lagrange,6> :
//    public BasisFunctionAreaBase<Triangle>
//{
//public:
//  static const int D = 2;     // physical dimensions
//
//  typedef std::array<int,3> Int3;
//
//  virtual ~BasisFunctionArea() {}
//
//  virtual int order() const { return 6; }
//  virtual int nBasis() const { return 28; }
//  virtual int nBasisNode() const { return 3; }
//  virtual int nBasisEdge() const { return 15; }
//  virtual int nBasisCell() const { return 10; }
//  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }
//
//  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
//  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;
//  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int3& sgn,
//      Real phiss[], Real phist[], Real phitt[], int nphi ) const;
//
//  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;
//
//protected:
//  //Singleton!
//  BasisFunctionArea() {}
//  static const std::vector<Real> coords_s_;
//  static const std::vector<Real> coords_t_;
//public:
//  static const BasisFunctionArea* self();
//};


////----------------------------------------------------------------------------//
//// Lagrange: heptic
//
//template <>
//class BasisFunctionArea<Triangle,Lagrange,7> :
//    public BasisFunctionAreaBase<Triangle>
//{
//public:
//  static const int D = 2;     // physical dimensions
//
//  typedef std::array<int,3> Int3;
//
//  virtual ~BasisFunctionArea() {}
//
//  virtual int order() const { return 7; }
//  virtual int nBasis() const { return 36; }
//  virtual int nBasisNode() const { return 3; }
//  virtual int nBasisEdge() const { return 18; }
//  virtual int nBasisCell() const { return 15; }
//  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }
//
//  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
//  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;
//  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int3& sgn,
//      Real phiss[], Real phist[], Real phitt[], int nphi ) const;
//
//  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;
//
//protected:
//  //Singleton!
//  BasisFunctionArea() {}
//  static const std::vector<Real> coords_s_;
//  static const std::vector<Real> coords_t_;
//public:
//  static const BasisFunctionArea* self();
//};

}

#endif  // BASISFUNCTIONAREA_TRIANGLE_LAGRANGE_H
