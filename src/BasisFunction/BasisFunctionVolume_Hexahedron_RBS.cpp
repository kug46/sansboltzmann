// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionVolume_Hexahedron_RBS.h"
#include "tools/SANSException.h"

namespace SANS
{

void
BasisFunctionVolume<Hex,RBS,0>::evalBasis( const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phi[0] = 1;
}


void
BasisFunctionVolume<Hex,RBS,0>::evalBasisDerivative( const Real&, const Real&, const Real&, const Int6&,
                                                              Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phis[0] = 0;

  phit[0] = 0;

  phiu[0] = 0;
}

void
BasisFunctionVolume<Hex,RBS,0>::evalBasisHessianDerivative( const Real&, const Real&, const Real&, const Int6&,
    Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi==1);
  phiss[0] = 0;
  phist[0] = 0;
  phitt[0] = 0;
  phisu[0] = 0;
  phitu[0] = 0;
  phiuu[0] = 0;
}

}
