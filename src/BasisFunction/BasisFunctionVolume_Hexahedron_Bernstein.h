// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONVOLUME_HEXAHEDRON_BERNSTEIN_H
#define BASISFUNCTIONVOLUME_HEXAHEDRON_BERNSTEIN_H

// hexahedron basis functions: Bernstein

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionVolume.h


#include <array>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionVolume.h"
#include "BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// volume basis functions: Bernstein
//
// reference triangle element defined: s in [0, 1], t in [0, 1], u in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionVolume_Hex_BernsteinPMax = 3;

//----------------------------------------------------------------------------//
// Bernstein: const

template <>
class BasisFunctionVolume<Hex,Bernstein,1> :
  public BasisFunctionVolumeBase<Hex>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,6> Int6;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 0; }
  virtual int nBasis() const { return 1; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Bernstein; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
public:
  static const BasisFunctionVolume* self();
};

//----------------------------------------------------------------------------//
// Bernstein: const

template <>
class BasisFunctionVolume<Hex,Bernstein,2> :
  public BasisFunctionVolumeBase<Hex>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,6> Int6;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 0; }
  virtual int nBasis() const { return 1; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Bernstein; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
public:
  static const BasisFunctionVolume* self();
};

//----------------------------------------------------------------------------//
// Bernstein: const

template <>
class BasisFunctionVolume<Hex,Bernstein,3> :
  public BasisFunctionVolumeBase<Hex>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,6> Int6;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 0; }
  virtual int nBasis() const { return 1; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Bernstein; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
public:
  static const BasisFunctionVolume* self();
};

//----------------------------------------------------------------------------//
// Bernstein: const

template <>
class BasisFunctionVolume<Hex,Bernstein,4> :
  public BasisFunctionVolumeBase<Hex>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,6> Int6;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 0; }
  virtual int nBasis() const { return 1; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Bernstein; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
public:
  static const BasisFunctionVolume* self();
};

}

#endif  // BASISFUNCTIONVOLUME_HEXAHEDRON_BERNSTEIN_H
