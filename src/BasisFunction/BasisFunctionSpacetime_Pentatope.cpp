// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// sppacetime basis functions (pentatope)

#include <string>
#include <iostream>
#include <limits>

#include "tools/SANSException.h"
#include "BasisFunctionSpacetime.h"
#include "BasisFunctionSpacetime_Pentatope.h"

#include "Quadrature/QuadratureSpacetime.h"
#include "Quadrature/QuadratureVolume.h"
#include "Quadrature/QuadratureArea.h"
#include "Quadrature_Cache.h"

#include "TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//Instantiate the singletons

const BasisFunctionSpacetime<Pentatope, Lagrange, 1>*
      BasisFunctionSpacetime<Pentatope, Lagrange, 1>::self() { static const BasisFunctionSpacetime singleton; return &singleton; }

const BasisFunctionSpacetime<Pentatope, Lagrange, 2>*
      BasisFunctionSpacetime<Pentatope, Lagrange, 2>::self() { static const BasisFunctionSpacetime singleton; return &singleton; }

const BasisFunctionSpacetime<Pentatope, Lagrange, 3>*
      BasisFunctionSpacetime<Pentatope, Lagrange, 3>::self() { static const BasisFunctionSpacetime singleton; return &singleton; }

const BasisFunctionSpacetime<Pentatope, Lagrange, 4>*
     BasisFunctionSpacetime<Pentatope, Lagrange, 4>::self() { static const BasisFunctionSpacetime singleton; return &singleton; }

const BasisFunctionSpacetime<Pentatope, Legendre, 0>*
      BasisFunctionSpacetime<Pentatope, Legendre, 0>::self() { static const BasisFunctionSpacetime singleton; return &singleton; }

const BasisFunctionSpacetime<Pentatope, Legendre, 1>*
      BasisFunctionSpacetime<Pentatope, Legendre, 1>::self() { static const BasisFunctionSpacetime singleton; return &singleton; }

const BasisFunctionSpacetime<Pentatope, Legendre, 2>*
      BasisFunctionSpacetime<Pentatope, Legendre, 2>::self() { static const BasisFunctionSpacetime singleton; return &singleton; }

const BasisFunctionSpacetime<Pentatope, Legendre, 3>*
      BasisFunctionSpacetime<Pentatope, Legendre, 3>::self() { static const BasisFunctionSpacetime singleton; return &singleton; }

const BasisFunctionSpacetime<Pentatope, Legendre, 4>*
      BasisFunctionSpacetime<Pentatope, Legendre, 4>::self() { static const BasisFunctionSpacetime singleton; return &singleton; }

//----------------------------------------------------------------------------//
//Initialize the base class pointers

template<>
const BasisFunctionSpacetime<Pentatope, Lagrange, 1>*
BasisFunctionSpacetimeBase<Pentatope>::LagrangeP1 = BasisFunctionSpacetime<Pentatope, Lagrange, 1>::self();

template<>
const BasisFunctionSpacetime<Pentatope, Lagrange, 2>*
BasisFunctionSpacetimeBase<Pentatope>::LagrangeP2 = BasisFunctionSpacetime<Pentatope, Lagrange, 2>::self();

template<>
const BasisFunctionSpacetime<Pentatope, Lagrange, 3>*
BasisFunctionSpacetimeBase<Pentatope>::LagrangeP3 = BasisFunctionSpacetime<Pentatope, Lagrange, 3>::self();

template<>
const BasisFunctionSpacetime<Pentatope, Lagrange, 4>*
BasisFunctionSpacetimeBase<Pentatope>::LagrangeP4 = BasisFunctionSpacetime<Pentatope, Lagrange, 4>::self();

template<>
const BasisFunctionSpacetime<Pentatope, Legendre, 0>*
BasisFunctionSpacetimeBase<Pentatope>::LegendreP0= BasisFunctionSpacetime<Pentatope, Legendre, 0>::self();

template<>
const BasisFunctionSpacetime<Pentatope, Legendre, 1>*
BasisFunctionSpacetimeBase<Pentatope>::LegendreP1= BasisFunctionSpacetime<Pentatope, Legendre, 1>::self();

template<>
const BasisFunctionSpacetime<Pentatope, Legendre, 2>*
BasisFunctionSpacetimeBase<Pentatope>::LegendreP2= BasisFunctionSpacetime<Pentatope, Legendre, 2>::self();

template<>
const BasisFunctionSpacetime<Pentatope, Legendre, 3>*
BasisFunctionSpacetimeBase<Pentatope>::LegendreP3= BasisFunctionSpacetime<Pentatope, Legendre, 3>::self();

template<>
const BasisFunctionSpacetime<Pentatope, Legendre, 4>*
BasisFunctionSpacetimeBase<Pentatope>::LegendreP4= BasisFunctionSpacetime<Pentatope, Legendre, 4>::self();

//----------------------------------------------------------------------------//
template<>
const BasisFunctionSpacetimeBase<Pentatope>*
BasisFunctionSpacetimeBase<Pentatope>::getBasisFunction( const int order, const BasisFunctionCategory& category )
{
  if (category == BasisFunctionCategory_Lagrange)
  {
    switch (order)
    {
    case 1:
      return BasisFunctionSpacetime<Pentatope, Lagrange, 1>::self();
    case 2:
      return BasisFunctionSpacetime<Pentatope, Lagrange, 2>::self();
    case 3:
      return BasisFunctionSpacetime<Pentatope, Lagrange, 3>::self();
   case 4:
     return BasisFunctionSpacetime<Pentatope, Lagrange, 4>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionSpacetimeBase<Pentatope>::getBasisFunction: unexpected Lagrange order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Legendre)
  {
    switch (order)
    {
      case 0:
        return BasisFunctionSpacetime<Pentatope, Legendre, 0>::self();
      case 1:
        return BasisFunctionSpacetime<Pentatope, Legendre, 1>::self();
      case 2:
        return BasisFunctionSpacetime<Pentatope, Legendre, 2>::self();
      case 3:
        return BasisFunctionSpacetime<Pentatope, Legendre, 3>::self();
      case 4:
        return BasisFunctionSpacetime<Pentatope, Legendre, 4>::self();
      default:
        SANS_DEVELOPER_EXCEPTION("Error in BasisFunctionSpacetimeBase<Pentatope>::getBasisFunction: unexpected LEgendre order= %d\n", order);
        break;
    }
  }

  SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionSpacetimeBase<Pentatope>::getBasisFunction: unexpected basis function category = %d\n",
                            static_cast<int>(category) );

  //Just so the compiler will not complain
  return NULL;
}

//===========================================================================//

template<class Topology>
QuadratureCache<Topology>::QuadratureCache()
{
  if ( BasisFunctionCategory_Hierarchical != 4 )
  {
    // Exceptions don't get printed during static initialization, probably a boost test thing
    std::cout << "You need to add the maximum order for the new basis" << std::endl;
    SANS_ASSERT(false);
  }

  BasisFunctionCategory categores[BasisFunctionCategory_Hierarchical];
  int Pmin[BasisFunctionCategory_Hierarchical];
  int Pmax[BasisFunctionCategory_Hierarchical];

  categores[BasisFunctionCategory_Legendre] = BasisFunctionCategory_Legendre;
  categores[BasisFunctionCategory_Lagrange] = BasisFunctionCategory_Lagrange;
  categores[BasisFunctionCategory_Bernstein] = BasisFunctionCategory_Bernstein;
  categores[BasisFunctionCategory_RBS] = BasisFunctionCategory_RBS;

  Pmin[BasisFunctionCategory_Legendre] = 0;
  Pmin[BasisFunctionCategory_Lagrange] = 1;
  Pmin[BasisFunctionCategory_Bernstein] = 1;
  Pmin[BasisFunctionCategory_RBS] = 0;

  Pmax[BasisFunctionCategory_Legendre] = BasisFunctionSpacetime_Pentatope_LegendrePMax + 1;
  Pmax[BasisFunctionCategory_Lagrange] = BasisFunctionSpacetime_Pentatope_LagrangePMax+1;
  Pmax[BasisFunctionCategory_Bernstein] = 0;
  Pmax[BasisFunctionCategory_RBS] = 0;


  cell.resize(BasisFunctionCategory_Hierarchical);
  trace.resize(BasisFunctionCategory_Hierarchical);
  for (int icat = 0; icat < BasisFunctionCategory_Hierarchical; icat++)
  {
//    IntTrace sgn; // Should not be used by these basis functions

    BasisFunctionCategory cat = categores[icat];

    // nothing to do
    if (Pmax[cat] == 0) continue;

    cell[cat].resize(Pmax[cat]);
    trace[cat].resize(Pmax[cat]);
    for (int poly_order = Pmin[cat]; poly_order < Pmax[cat]; poly_order++)
    {
//      const BasisFunctionSpacetimeBase<Pentatope>* basis =
//          BasisFunctionSpacetimeBase<Pentatope>::getBasisFunction(poly_order, cat);

      QuadratureBasisPointStore<D>& pointStore = cell[cat][poly_order];
      pointStore.category = cat;
      pointStore.poly_order = poly_order;
      pointStore.eval.resize(QuadratureRule::eNone);

      pointStore.eval[QuadratureRule::eGauss].resize(QuadratureSpacetime<Pentatope>::nOrderIdx);
#if 0
      for (int orderidx = 0; orderidx < QuadratureSpacetime<Pentatope>::nOrderIdx; orderidx++)
      {
        QuadratureSpacetime<Pentatope> quadrature( QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderidx) );
        pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

        Real s, t, u, v;
        for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
        {
          QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

          point.phi.resize(basis->nBasis());
          point.dphi[0].resize(basis->nBasis());
          point.dphi[1].resize(basis->nBasis());
          point.dphi[2].resize(basis->nBasis());
          point.dphi[3].resize(basis->nBasis());

          quadrature.coordinates(iquad, s, t, u, v);
          basis->evalBasis( s, t, u, v, sgn, point.phi.data(), point.phi.size() );

          basis->evalBasisDerivative( s, t, u, v, sgn, point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(), point.dphi[3].data(),
                                                    point.dphi[0].size() );
        } // iquad
      } // orderidx
#endif

      trace[cat][poly_order].resize(Pentatope::NTrace);
      for (int itrace = 0; itrace < Pentatope::NTrace; itrace++)
      {
        QuadratureBasisPointStore<D>& pointStore = trace[cat][poly_order][itrace];
        pointStore.category = cat;
        pointStore.poly_order = poly_order;
        pointStore.eval.resize(QuadratureRule::eNone);

        CanonicalTraceToCell canonicalTrace(itrace, 1);

        pointStore.eval[QuadratureRule::eGauss].resize(QuadratureVolume<Tet>::nOrderIdx);
#if 0
        for (int orderidx = 0; orderidx < QuadratureVolume<Tet>::nOrderIdx; orderidx++)
        {
          QuadratureVolume<Tet> quadrature( QuadratureVolume<Tet>::getOrderFromIndex(orderidx) );
          pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

          Real sTrace, tTrace, uTrace;
          Real sCell, tCell, uCell, vCell;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

            point.phi.resize(basis->nBasis());
            point.dphi[0].resize(basis->nBasis());
            point.dphi[1].resize(basis->nBasis());
            point.dphi[2].resize(basis->nBasis());
            point.dphi[3].resize(basis->nBasis());

            quadrature.coordinates(iquad, sTrace, tTrace, uTrace);

            TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTrace, sTrace, tTrace, uTrace, sCell, tCell, uCell, vCell );

            basis->evalBasis( sCell, tCell, uCell, vCell , sgn, point.phi.data(), point.phi.size() );

            basis->evalBasisDerivative( sCell, tCell, uCell, vCell, sgn,
                                        point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(), point.dphi[3].data(),
                                        point.dphi[0].size() );
          } //iquad
        } // orderidx
#endif
      } // itrace

    } // poly_order
  } // icat

  //-------------------------------------------------------------------------//
  // Hierarchical basis functions are special with the edge sign
  //-------------------------------------------------------------------------//
  // NOTE hierarchical basis functions not currently implemented for pentatopes
#if 0
  const int nFaceSign = 1;

  // TODO: Not sure how the face sign work out in 3D, so this is just dummy now.
  IntTrace faceSigns[nFaceSign] = { {{ 1, 1, 1, 1}} };

  // TODO: This assertion is here until the face sign is sorted out. Then it should be removed
  SANS_ASSERT( BasisFunctionSpacetime_Pentatope_HierarchicalPMax == 2 );

  hierarchicalCell.resize(BasisFunctionSpacetime_Pentatope_HierarchicalPMax+1);
  hierarchicalTrace.resize(BasisFunctionSpacetime_Pentatope_HierarchicalPMax+1);
  for (int poly_order = 1; poly_order < BasisFunctionSpacetime_Pentatope_HierarchicalPMax+1; poly_order++)
  {
    const BasisFunctionSpacetimeBase<Pentatope>* basis =
        BasisFunctionSpacetimeBase<Pentatope>::getBasisFunction(poly_order, BasisFunctionCategory_Hierarchical);

    for (int isgn = 0; isgn < nFaceSign; isgn++)
    {
      IntTrace sgn = faceSigns[isgn];
      QuadratureBasisPointStore<D>& pointStore = hierarchicalCell[poly_order][sgn];
      pointStore.category = BasisFunctionCategory_Hierarchical;
      pointStore.poly_order = poly_order;
      pointStore.eval.resize(QuadratureRule::eNone);

      pointStore.eval[QuadratureRule::eGauss].resize(QuadratureSpacetime<Pentatope>::nOrderIdx);
#if 0
      for (int orderidx = 0; orderidx < QuadratureSpacetime<Pentatope>::nOrderIdx; orderidx++)
      {
        QuadratureSpacetime<Pentatope> quadrature( QuadratureSpacetime<Pentatope>::getOrderFromIndex(orderidx) );
        pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

        Real s, t, u, v;
        for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
        {
          QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

          point.phi.resize(basis->nBasis());
          point.dphi[0].resize(basis->nBasis());
          point.dphi[1].resize(basis->nBasis());
          point.dphi[2].resize(basis->nBasis());
          point.dphi[3].resize(basis->nBasis());

          quadrature.coordinates(iquad, s, t, u, v);
          basis->evalBasis( s, t, u, v, sgn, point.phi.data(), point.phi.size() );

          basis->evalBasisDerivative( s, t, u, v, sgn, point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(), point.dphi[3].data(),
                                                       point.dphi[0].size() );
        } //iqaud
      } // orderidx
#endif

      hierarchicalTrace[poly_order][sgn].resize(Pentatope::NTrace);
      for (int itrace = 0; itrace < Pentatope::NTrace; itrace++)
      {
        QuadratureBasisPointStore<D>& pointStore = hierarchicalTrace[poly_order][sgn][itrace];
        pointStore.category = BasisFunctionCategory_Hierarchical;
        pointStore.poly_order = poly_order;
        pointStore.eval.resize(QuadratureRule::eNone);

        CanonicalTraceToCell canonicalTrace(itrace, 1);

        pointStore.eval[QuadratureRule::eGauss].resize(QuadratureArea<Triangle>::nOrderIdx);
#if 0
        for (int orderidx = 0; orderidx < QuadratureArea<Triangle>::nOrderIdx; orderidx++)
        {
          QuadratureArea<Triangle> quadrature( QuadratureArea<Triangle>::getOrderFromIndex(orderidx) );
          pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

          Real sTrace, tTrace, uTrace;
          Real sCell, tCell, uCell, vCell;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

            point.phi.resize(basis->nBasis());
            point.dphi[0].resize(basis->nBasis());
            point.dphi[1].resize(basis->nBasis());
            point.dphi[2].resize(basis->nBasis());
            point.dphi[3].resize(basis->nBasis());

            quadrature.coordinates(iquad, sTrace, tTrace);

            TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTrace, sTrace, tTrace, uTrace, sCell, tCell, uCell, vCell);

            basis->evalBasis( sCell, tCell, uCell, vCell, sgn, point.phi.data(), point.phi.size() );

            basis->evalBasisDerivative( sCell, tCell, uCell, vCell, sgn,
                                        point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(), point.dphi[3].data(),
                                        point.dphi[0].size() );
          } //iquad
        } // orderidx
#endif
      } // itrace

    } // isgn
  } // poly_order

#endif

  //-------------------------------------------------------------------------//
  // Trace orientation map that maps any orientation back to the canonical
  //-------------------------------------------------------------------------//

  Real reftol = std::numeric_limits<Real>::max();

  {
    // find the smallest distance between any two quadrature points
    // this will be the tolerance for the equality check between cell quadrature points
    QuadratureVolume<Tet> quadrature( QuadratureVolume<Tet>::getOrderFromIndex(QuadratureVolume<Tet>::nOrderIdx-1) );
    Real s0, t0, u0, s1, t1, u1;
    for (int iquad0 = 0; iquad0 < quadrature.nQuadrature(); iquad0++)
    {
      quadrature.coordinates(iquad0, s0, t0, u0);

      for (int iquad1 = 0; iquad1 < quadrature.nQuadrature(); iquad1++)
      {
        if (iquad0 == iquad1) continue; // the same point will have zero difference...

        quadrature.coordinates(iquad1, s1, t1, u1);
        reftol = std::min( reftol, sqrt( (s1 - s0)*(s1 - s0) +
                                         (t1 - t0)*(t1 - t0) +
                                         (u1 - u0)*(u1 - u0) ) );
      }
    }

    // give a nice margin
    reftol /= 10;
  }

  traceOrientMap.resize(12); // 23 non-canonical orientations + 1 canonical one

  // By design, any mesh can only have orientation 1, and negative orientations
  for (int orientation : {-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1} )
  {
    TraceOrientMap& orientMap = traceOrientMap[ QuadCacheSpacetimeTraceOrientIdx(orientation) ];
    orientMap.canonical.resize(QuadratureRule::eNone);

    orientMap.canonical[QuadratureRule::eGauss].resize(QuadratureVolume<Tet>::nOrderIdx);
    for (int orderidx = 0; orderidx < QuadratureVolume<Tet>::nOrderIdx; orderidx++)
    {
      QuadratureVolume<Tet> quadrature( QuadratureVolume<Tet>::getOrderFromIndex(orderidx) );
      orientMap.canonical[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature(), -1 );

      // Left canonical trace with canonical orientation of 1 (trace does not matter)
      CanonicalTraceToCell canonicalTraceL(0, 1);
      CanonicalTraceToCell canonicalTraceR(0, orientation); // other orientation

      Real sTrace, tTrace, uTrace;
      Real sCellL, tCellL, uCellL, vCellL;
      for (int iquadL = 0; iquadL < quadrature.nQuadrature(); iquadL++)
      {
        // Get the quadrature point on the trace
        quadrature.coordinates(iquadL, sTrace, tTrace, uTrace);

        // get the cell reference coordiantes
        TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTraceL, sTrace, tTrace, uTrace, sCellL, tCellL, uCellL , vCellL );

        // loop over all quadrature points again, and find the same cell point with the right orientation
        std::vector<Real> distances( quadrature.nQuadrature() );
        Real dmin = std::numeric_limits<Real>::max();
        int iquadR,imin = -1;
        for (iquadR = 0; iquadR < quadrature.nQuadrature(); iquadR++)
        {
          quadrature.coordinates(iquadR, sTrace, tTrace, uTrace);

          Real sCellR, tCellR, uCellR, vCellR;
          TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTraceR, sTrace, tTrace, uTrace, sCellR, tCellR, uCellR, vCellR );
          distances[iquadR] = sqrt( (sCellL-sCellR)*(sCellL-sCellR) + (tCellL-tCellR)*(tCellL-tCellR)
                                   +(uCellL-uCellR)*(uCellL-uCellR)  +(vCellL-vCellR)*(vCellL-vCellR) );
          if (distances[iquadR]<dmin)
          {
            dmin = distances[iquadR];
            imin = iquadR;
          }
        } // iquadR

        // check the minimum distance is less than the minimum distance
        // between trace quadrature points
        if (dmin>reftol)
          SANS_DEVELOPER_EXCEPTION("did not find matching quadrature point for iquadL = %d!\n",iquadL);

        // set the map to the quadrature point with minimum distance
        orientMap.canonical[QuadratureRule::eGauss][orderidx][imin] = iquadL;

      } // iquadL
    } // orderidx
  } // orientation
}

//----------------------------------------------------------------------------//

template<class Topology>
void
QuadratureCache<Topology>::
fillCellQuadrature(const QuadraturePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreCell)
{
  BasisFunctionCategory cat = pointStoreCell.category;
  int poly_order = pointStoreCell.poly_order;

  QuadratureSpacetime<Topology> quadrature( QuadratureSpacetime<Topology>::getOrderFromIndex(ref.orderidx) );

  std::vector<QuadratureBasisPointValues<D>>& points = pointStoreCell.eval[ref.rule][ref.orderidx];
  SANS_ASSERT(points.size() == 0);
  points.resize( quadrature.nQuadrature() );

  const BasisFunctionSpacetimeBase<Topology>* basis =
      BasisFunctionSpacetimeBase<Topology>::getBasisFunction(poly_order, cat);

  const int nBasis = basis->nBasis();

  Real s, t, u, v;
  for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
  {
    QuadratureBasisPointValues<D>& point = points[iquad];

    point.phi.resize(nBasis);
    point.dphi[0].resize(nBasis);
    point.dphi[1].resize(nBasis);
    point.dphi[2].resize(nBasis);
    point.dphi[3].resize(nBasis);
    for (int i = 0; i < 10; i++)
      point.d2phi[i].resize(nBasis);

    quadrature.coordinates(iquad, s, t, u, v);
    basis->evalBasis( s, t, u, v, sgn, point.phi.data(), point.phi.size() );

    Real *phis = point.dphi[0].data();
    Real *phit = point.dphi[1].data();
    Real *phiu = point.dphi[2].data();
    Real *phiv = point.dphi[3].data();
    basis->evalBasisDerivative( s, t, u, v, sgn, phis, phit, phiu, phiv, nBasis );

    Real *phiss = point.d2phi[0].data();
    Real *phist = point.d2phi[1].data();
    Real *phitt = point.d2phi[2].data();
    Real *phisu = point.d2phi[3].data();
    Real *phitu = point.d2phi[4].data();
    Real *phiuu = point.d2phi[5].data();
    Real *phisv = point.d2phi[6].data();
    Real *phitv = point.d2phi[7].data();
    Real *phiuv = point.d2phi[8].data();
    Real *phivv = point.d2phi[9].data();
    try
    {
      basis->evalBasisHessianDerivative( s, t, u, v, sgn,
                                         phiss,
                                         phist, phitt,
                                         phisu, phitu, phiuu,
                                         phisv, phitv, phiuv, phivv, nBasis );
    }
    catch (const DeveloperException& e)
    {
      // hessian not implemented, hopefully it's not needed...
      for (int i = 0; i < 10; i++)
      {
        point.d2phi[i].clear(); point.d2phi[i].shrink_to_fit();
      }
    }
  } // iquad
}

//----------------------------------------------------------------------------//

template<class Topology>
void
QuadratureCache<Topology>::
fillTraceQuadrature(const QuadratureCellTracePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreTrace)
{
  BasisFunctionCategory cat = pointStoreTrace.category;
  int poly_order = pointStoreTrace.poly_order;

  QuadratureVolume<Tet> quadrature( QuadratureVolume<Tet>::getOrderFromIndex(ref.orderidx) );

  std::vector<QuadratureBasisPointValues<D>>& points = pointStoreTrace.eval[QuadratureRule::eGauss][ref.orderidx];
  SANS_ASSERT(points.size() == 0);
  points.resize( quadrature.nQuadrature() );

  const BasisFunctionSpacetimeBase<Pentatope>* basis =
      BasisFunctionSpacetimeBase<Pentatope>::getBasisFunction(poly_order, cat);

  CanonicalTraceToCell canonicalTrace(ref.canonicalTrace.trace, 1);

  Real sTrace, tTrace, uTrace;
  Real sCell, tCell, uCell, vCell;
  for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
  {
    QuadratureBasisPointValues<D>& point = points[iquad];

    point.phi.resize(basis->nBasis());
    point.dphi[0].resize(basis->nBasis());
    point.dphi[1].resize(basis->nBasis());
    point.dphi[2].resize(basis->nBasis());
    point.dphi[3].resize(basis->nBasis());

    quadrature.coordinates(iquad, sTrace, tTrace, uTrace);

    TraceToCellRefCoord<Tet, TopoD4, Pentatope>::eval( canonicalTrace, sTrace, tTrace, uTrace, sCell, tCell, uCell, vCell );

    basis->evalBasis( sCell, tCell, uCell, vCell , sgn, point.phi.data(), point.phi.size() );

    basis->evalBasisDerivative( sCell, tCell, uCell, vCell, sgn,
                                point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(), point.dphi[3].data(),
                                point.dphi[0].size() );
  } //iquad
}

// instantiate the class and singleton
template<class Topology>
const QuadratureCache<Topology> QuadratureCache<Topology>::cache;

// The QuadratureCache singleton must be instantiated in this translation unit (cpp file) to guarantee
// that the basis function singletons are initialized before the QuadratureCache
template struct QuadratureCache<Pentatope>;

}


// instantiate the standard library classes
template class std::vector<SANS::QuadratureBasisPointValues<4>>;
template class std::vector<std::vector<SANS::QuadratureBasisPointValues<4>>>;
template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointValues<4>>>>;

template class std::map<std::array<int,5>,SANS::QuadratureBasisPointStore<4>>;
template class std::vector<std::map<std::array<int,5>,SANS::QuadratureBasisPointStore<4>>>;
template class std::vector<std::map<std::array<int,5>,std::vector<SANS::QuadratureBasisPointStore<4>>>>;

template class std::vector<SANS::QuadratureBasisPointStore<4>>;
template class std::vector<std::vector<SANS::QuadratureBasisPointStore<4>>>;
template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointStore<4>>>>;

template class std::vector<SANS::TraceOrientMap>;
