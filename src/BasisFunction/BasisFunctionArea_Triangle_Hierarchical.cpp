// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionArea_Triangle_Hierarchical.h"

#include <cmath>  // sqrt
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Hierarchical: linear

void
BasisFunctionArea<Triangle,Hierarchical,1>::evalBasis( const Real& s, const Real& t, const Int3&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  Real u = 1 - s - t;

  phi[0] = u; // 1 @ node 1 (s = 0, t = 0)
  phi[1] = s; // 1 @ node 2 (s = 1, t = 0)
  phi[2] = t; // 1 @ node 3 (s = 0, t = 1)
}

void
BasisFunctionArea<Triangle,Hierarchical,1>::evalBasisDerivative( const Real&, const Real&, const Int3&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  0;

  phit[0] = -1;
  phit[1] =  0;
  phit[2] =  1;
}

void
BasisFunctionArea<Triangle,Hierarchical,1>::evalBasisHessianDerivative(
    const Real&, const Real&, const Int3&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phiss[0] =  0;
  phiss[1] =  0;
  phiss[2] =  0;

  phist[0] =  0;
  phist[1] =  0;
  phist[2] =  0;

  phitt[0] =  0;
  phitt[1] =  0;
  phitt[2] =  0;
}

//----------------------------------------------------------------------------//
// Hierarchical: quadratic

void
BasisFunctionArea<Triangle,Hierarchical,2>::evalBasis(
    const Real& s, const Real& t, const Int3&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  Real u = 1 - s - t;

  phi[0] = u;      // 1 @ node 1 (s = 0, t = 0)
  phi[1] = s;      // 1 @ node 2 (s = 1, t = 0)
  phi[2] = t;      // 1 @ node 3 (s = 0, t = 1)

  phi[3] = 4*s*t;  // 1 @ edge 1, s = 1/2, t = 1 - s

  phi[4] = 4*t*u;  // 1 @ edge 2, t = 1/2, s = 0

  phi[5] = 4*u*s;  // 1 @ edge 3, s = 1/2, t = 0
}

void
BasisFunctionArea<Triangle,Hierarchical,2>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  0;
  phis[3] =  4*t;
  phis[4] = -4*t;
  phis[5] =  4*(1 - 2*s - t);

  phit[0] = -1;
  phit[1] =  0;
  phit[2] =  1;
  phit[3] =  4*s;
  phit[4] =  4*(1 - s - 2*t);
  phit[5] = -4*s;
}

void
BasisFunctionArea<Triangle,Hierarchical,2>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  phiss[0] =  0;
  phiss[1] =  0;
  phiss[2] =  0;
  phiss[3] =  0;
  phiss[4] =  0;
  phiss[5] = -8;

  phist[0] =  0;
  phist[1] =  0;
  phist[2] =  0;
  phist[3] =  4;
  phist[4] = -4;
  phist[5] = -4;

  phitt[0] =  0;
  phitt[1] =  0;
  phitt[2] =  0;
  phitt[3] =  0;
  phitt[4] = -8;
  phitt[5] =  0;
}


//----------------------------------------------------------------------------//
// Hierarchical: cubic

void
BasisFunctionArea<Triangle,Hierarchical,3>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  Real u = 1 - s - t;

  phi[0] = u;                                 // 1 @ node 1 (s = 0, t = 0)
  phi[1] = s;                                 // 1 @ node 2 (s = 1, t = 0)
  phi[2] = t;                                 // 1 @ node 3 (s = 0, t = 1)

  phi[3] = 4*s*t;                             // max=1 on edge 1 @ s = 1/2, t = 1/2
  phi[4] = 6*sqrt(3)*s*t*(s - t) * sgn[0];    // max=1 on edge 1 (asymmetric)

  phi[5] = 4*t*u;                             // max=1 on edge 2 @ t = 1/2, s = 0 (u = 1/2)
  phi[6] = 6*sqrt(3)*t*u*(t - u) * sgn[1];    // max=1 on edge 2 (asymmetric)

  phi[7] = 4*u*s;                             // max=1 on edge 3 @ s = 1/2, t = 0 (u = 1/2)
  phi[8] = 6*sqrt(3)*u*s*(u - s) * sgn[2];    // max=1 on edge 3 (asymmetric)

  phi[9] = 27*s*t*u;                          // max=1 @ centroid (s,t) = 1/3
#if 0
  std::cout << "BasisFunctionArea<T,H,3>::evalBasis : s = " << s << " t = " << t;
  std::cout << "  sgn = " << sgn[0] << " " << sgn[1] << " " << sgn[2];
  std::cout << "  phi[] =";
  for (int k = 0; k < 10; k++) { std::cout << " " << phi[k]; }
  std::cout << std::endl;
#endif
}


void
BasisFunctionArea<Triangle,Hierarchical,3>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  Real u = 1 - s - t;

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  0;

  phis[3] =  4*t;
  phis[4] = -6*sqrt(3)*t*(t - 2*s) * sgn[0];

  phis[5] = -4*t;
  phis[6] = -6*sqrt(3)*t*(t - 2*u) * sgn[1];

  phis[7] =  4*(u - s);
  phis[8] =  6*sqrt(3)*(s*(s - 2*u) + u*(u - 2*s)) * sgn[2];

  phis[9] =  27*t*(u - s);


  phit[0] = -1;
  phit[1] =  0;
  phit[2] =  1;

  phit[3] =  4*s;
  phit[4] =  6*sqrt(3)*s*(s - 2*t) * sgn[0];

  phit[5] = -4*(t - u);
  phit[6] = -6*sqrt(3)*(u*u - 4*u*t + t*t) * sgn[1];

  phit[7] = -4*s;
  phit[8] =  6*sqrt(3)*s*(s - 2*u) * sgn[2];

  phit[9] =  27*s*(u - t);
}

void
BasisFunctionArea<Triangle,Hierarchical,3>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  Real u = 1 - s - t;

  // S^2
  phiss[0] =  0;
  phiss[1] =  0;
  phiss[2] =  0;

  phiss[3] =  0;
  phiss[4] =  12*sqrt(3)*t * sgn[0];

  phiss[5] =  0;
  phiss[6] = -12*sqrt(3)*t * sgn[1];

  phiss[7] = -8;
  phiss[8] = -36*sqrt(3)*(u - s) * sgn[2];

  phiss[9] = -54*t;

  // CROSS ST
  phist[0] =  0;
  phist[1] =  0;
  phist[2] =  0;

  phist[3] =  4;
  phist[4] =  12*sqrt(3)*(s - t) * sgn[0];

  phist[5] = -4;
  phist[6] =  12*sqrt(3)*(u - 2*t) * sgn[1];

  phist[7] = -4;
  phist[8] = -12*sqrt(3)*(u - 2*s) * sgn[2];

  phist[9] =  27*(u - s - t);

  // T^2
  phitt[0] =  0;
  phitt[1] =  0;
  phitt[2] =  0;

  phitt[3] =  0;
  phitt[4] = -12*sqrt(3)*s * sgn[0];

  phitt[5] = -8;
  phitt[6] = -36*sqrt(3)*(t - u) * sgn[1];

  phitt[7] =  0;
  phitt[8] =  12*sqrt(3)*s * sgn[2];

  phitt[9] =  -54*s;
}


//----------------------------------------------------------------------------//
// Hierarchical: quartic

void
BasisFunctionArea<Triangle,Hierarchical,4>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 15);

  Real u = 1 - s - t;

  phi[ 0] = u;                                // 1 @ node 1 (s = 0, t = 0)
  phi[ 1] = s;                                // 1 @ node 2 (s = 1, t = 0)
  phi[ 2] = t;                                // 1 @ node 3 (s = 0, t = 1)

  phi[ 3] = 4*s*t;                            // max=1 on edge 1 @ s = 1/2, t = 1/2
  phi[ 4] = 6*sqrt(3)*s*t*(s - t) * sgn[0];   // max=1 on edge 1 (asymmetric)
  phi[ 5] = 16*s*s*t*t;                       // max=1 on edge 1

  phi[ 6] = 4*t*u;                            // max=1 on edge 2 @ t = 1/2, s = 0 (u = 1/2)
  phi[ 7] = 6*sqrt(3)*t*u*(t - u) * sgn[1];   // max=1 on edge 2 (asymmetric)
  phi[ 8] = 16*t*t*u*u;                       // max=1 on edge 2

  phi[ 9] = 4*u*s;                            // max=1 on edge 3 @ s = 1/2, t = 0 (u = 1/2)
  phi[10] = 6*sqrt(3)*u*s*(u - s) * sgn[2];   // max=1 on edge 3 (asymmetric)
  phi[11] = 16*u*u*s*s;                       // max=1 on edge 3

  phi[12] = 27*s*t*u;                          // max=1 @ centroid (s,t) = 1/3
  phi[13] = 512*s*t*u*(u - s)/(3*sqrt(3));     // max=1 @ s = (3 - sqrt(3))/8, t = 1/4
  phi[14] = 512*s*t*u*(u - t)/(3*sqrt(3));     // max=1 @ s = 1/4, t = (3 - sqrt(3))/8
}


void
BasisFunctionArea<Triangle,Hierarchical,4>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 15);

  Real u = 1 - s - t;

  phis[ 0] = -1;
  phis[ 1] =  1;
  phis[ 2] =  0;

  phis[ 3] =  4*t;
  phis[ 4] = -6*sqrt(3)*t*(t - 2*s) * sgn[0];
  phis[ 5] =  32*s*t*t;

  phis[ 6] = -4*t;
  phis[ 7] = -6*sqrt(3)*t*(t - 2*u) * sgn[1];
  phis[ 8] = -32*t*t*u;

  phis[ 9] =  4*(u - s);
  phis[10] =  6*sqrt(3)*(s*(s - 2*u) + u*(u - 2*s)) * sgn[2];
  phis[11] =  32*u*s*(u - s);

  phis[12] =  27*t*(u - s);
  phis[13] =  (512/(3*sqrt(3)))*t*(u*(u - 2*s) + s*(s - 2*u));
  phis[14] =  (512/(3*sqrt(3)))*t*(u*(u - t) + s*(t - 2*u));


  phit[ 0] = -1;
  phit[ 1] =  0;
  phit[ 2] =  1;

  phit[ 3] =  4*s;
  phit[ 4] =  6*sqrt(3)*s*(s - 2*t) * sgn[0];
  phit[ 5] =  32*s*s*t;

  phit[ 6] = -4*(t - u);
  phit[ 7] = -6*sqrt(3)*(u*u - 4*u*t + t*t) * sgn[1];
  phit[ 8] = -32*t*u*(t - u);

  phit[ 9] = -4*s;
  phit[10] =  6*sqrt(3)*s*(s - 2*u) * sgn[2];
  phit[11] = -32*s*s*u;

  phit[12] =  27*s*(u - t);
  phit[13] =  (512/(3*sqrt(3)))*s*(u*(u - 2*t) + s*(t - u));
  phit[14] =  (512/(3*sqrt(3)))*s*(u*(u - 2*t) + t*(t - 2*u));
}

void
BasisFunctionArea<Triangle,Hierarchical,4>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 15);

  Real u = 1 - s - t;

  // S^2
  phiss[ 0] =  0;
  phiss[ 1] =  0;
  phiss[ 2] =  0;

  phiss[ 3] =  0;
  phiss[ 4] =  12*sqrt(3)*t * sgn[0];
  phiss[ 5] =  32*t*t;

  phiss[ 6] =  0;
  phiss[ 7] = -12*sqrt(3)*t * sgn[1];
  phiss[ 8] =  32*t*t;

  phiss[ 9] = -8;
  phiss[10] = -36*sqrt(3)*(u - s) * sgn[2];
  phiss[11] =  32*(u*u - 4*u*s + s*s);

  phiss[12] = -54*t;
  phiss[13] = -(1024/(  sqrt(3)))*t*(u - s);
  phiss[14] =  (1024/(3*sqrt(3)))*t*(1 - 3*u);

  // CROSS ST
  phist[ 0] =  0;
  phist[ 1] =  0;
  phist[ 2] =  0;

  phist[ 3] =  4;
  phist[ 4] =  12*sqrt(3)*(s - t) * sgn[0];
  phist[ 5] =  64*s*t;

  phist[ 6] = -4;
  phist[ 7] =  12*sqrt(3)*(u - 2*t) * sgn[1];
  phist[ 8] =  32*t*(t - 2*u);

  phist[ 9] = -4;
  phist[10] = -12*sqrt(3)*(u - 2*s) * sgn[2];
  phist[11] =  32*s*(s - 2*u);

  phist[12] =  27*(u - s - t);
  phist[13] =  (512/(3*sqrt(3)))*(s*(s + 4*t) + u*(u - 2*(t + 2*s)));
  phist[14] =  (512/(3*sqrt(3)))*(t*(t + 4*s) + u*(u - 2*(s + 2*t)));

  // T^2
  phitt[ 0] =  0;
  phitt[ 1] =  0;
  phitt[ 2] =  0;

  phitt[ 3] =  0;
  phitt[ 4] = -12*sqrt(3)*s * sgn[0];
  phitt[ 5] =  32*s*s;

  phitt[ 6] =  -8;
  phitt[ 7] = -36*sqrt(3)*(t - u) * sgn[1];
  phitt[ 8] =  32*(t*(t - 4*u) + u*u);

  phitt[ 9] =  0;
  phitt[10] =  12*sqrt(3)*s * sgn[2];
  phitt[11] =  32*s*s;

  phitt[12] =  -54*s;
  phitt[13] =  (1024/(3*sqrt(3)))*s*(1 - 3*u);
  phitt[14] =  (1024/(  sqrt(3)))*s*(t - u);
}


//----------------------------------------------------------------------------//
// Hierarchical: quintic

void
BasisFunctionArea<Triangle,Hierarchical,5>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 21);

  Real u = 1 - s - t;

  phi[ 0] = u;                                      // 1 @ node 1 (s = 0, t = 0)
  phi[ 1] = s;                                      // 1 @ node 2 (s = 1, t = 0)
  phi[ 2] = t;                                      // 1 @ node 3 (s = 0, t = 1)

  phi[ 3] = 4*s*t;                                  // max=1 on edge 1 @ s = 1/2, t = 1/2
  phi[ 4] = 6*sqrt(3)*s*t*(s - t) * sgn[0];         // max=1 on edge 1 (asymmetric)
  phi[ 5] = 16*s*s*t*t;                             // max=1 on edge 1
  phi[ 6] = 25*sqrt(5)*s*s*t*t*(s - t) * sgn[0];    // max=1 on edge 1 (asymmetric)

  phi[ 7] = 4*t*u;                                  // max=1 on edge 2 @ t = 1/2, s = 0 (u = 1/2)
  phi[ 8] = 6*sqrt(3)*t*u*(t - u) * sgn[1];         // max=1 on edge 2 (asymmetric)
  phi[ 9] = 16*t*t*u*u;                             // max=1 on edge 2
  phi[10] = 25*sqrt(5)*t*t*u*u*(t - u) * sgn[1];    // max=1 on edge 2 (asymmetric)

  phi[11] = 4*u*s;                                  // max=1 on edge 3 @ s = 1/2, t = 0 (u = 1/2)
  phi[12] = 6*sqrt(3)*u*s*(u - s) * sgn[2];         // max=1 on edge 3 (asymmetric)
  phi[13] = 16*u*u*s*s;                             // max=1 on edge 3
  phi[14] = 25*sqrt(5)*u*u*s*s*(u - s) * sgn[2];    // max=1 on edge 3 (asymmetric)

  phi[15] = 27*s*t*u;                               // max=1 @ centroid (s,t) = 1/3
  phi[16] = 512*s*t*u*(u - s)/(3*sqrt(3));          // max=1 @ s = (3 - sqrt(3))/8, t = 1/4
  phi[17] = 512*s*t*u*(u - t)/(3*sqrt(3));          // max=1 @ s = 1/4, t = (3 - sqrt(3))/8
  phi[18] = 3125*s*s*t*t*u/16.;                     // max=1 @ s = 2/5, t = 2/5 (u = 1/5)
  phi[19] = 3125*t*t*u*u*s/16.;                     // max=1 @ s = 1/5, t = 2/5 (u = 2/5)
  phi[20] = 3125*u*u*s*s*t/16.;                     // max=1 @ s = 2/5, t = 1/5 (u = 2/5)
}

void
BasisFunctionArea<Triangle,Hierarchical,5>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 21);

  Real u = 1 - s - t;

  phis[ 0] = -1;
  phis[ 1] =  1;
  phis[ 2] =  0;

  phis[ 3] =  4*t;
  phis[ 4] = -6*sqrt(3)*t*(t - 2*s) * sgn[0];
  phis[ 5] =  32*s*t*t;
  phis[ 6] =  25*sqrt(5)*s*t*t*(3*s - 2*t) * sgn[0];

  phis[ 7] = -4*t;
  phis[ 8] = -6*sqrt(3)*t*(t - 2*u) * sgn[1];
  phis[ 9] = -32*t*t*u;
  phis[10] =  25*sqrt(5)*t*t*u*(3*u - 2*t) * sgn[1];

  phis[11] =  4*(u - s);
  phis[12] =  6*sqrt(3)*(s*(s - 2*u) + u*(u - 2*s)) * sgn[2];
  phis[13] =  32*u*s*(u - s);
  phis[14] =  50*sqrt(5)*u*s*(u*u - 3*u*s + s*s) * sgn[2];

  phis[15] =  27*t*(u - s);
  phis[16] =  (512/(3*sqrt(3)))*t*(u*(u - 2*s) + s*(s - 2*u));
  phis[17] =  (512/(3*sqrt(3)))*t*(u*(u - t) + s*(t - 2*u));
  phis[18] = -(3125/16.)*s*t*t*(s - 2*u);
  phis[19] =  (3125/16.)*t*t*u*(u - 2*s);
  phis[20] =  (6250/16.)*s*t*u*(u - s);


  phit[ 0] = -1;
  phit[ 1] =  0;
  phit[ 2] =  1;

  phit[ 3] =  4*s;
  phit[ 4] =  6*sqrt(3)*s*(s - 2*t) * sgn[0];
  phit[ 5] =  32*s*s*t;
  phit[ 6] = -25*sqrt(5)*s*s*t*(3*t - 2*s) * sgn[0];

  phit[ 7] = -4*(t - u);
  phit[ 8] = -6*sqrt(3)*(u*u - 4*u*t + t*t) * sgn[1];
  phit[ 9] = -32*t*u*(t - u);
  phit[10] = -50*sqrt(5)*t*u*(t*t - 3*u*t + u*u) * sgn[1];

  phit[11] = -4*s;
  phit[12] =  6*sqrt(3)*s*(s - 2*u) * sgn[2];
  phit[13] = -32*s*s*u;
  phit[14] = -25*sqrt(5)*s*s*u*(3*u - 2*s) * sgn[2];

  phit[15] =  27*s*(u - t);
  phit[16] =  (512/(3*sqrt(3)))*s*(u*(u - 2*t) + s*(t - u));
  phit[17] =  (512/(3*sqrt(3)))*s*(u*(u - 2*t) + t*(t - 2*u));
  phit[18] = -(3125/16.)*s*s*t*(t - 2*u);
  phit[19] =  (6250/16.)*s*t*u*(u - t);
  phit[20] =  (3125/16.)*s*s*u*(u - 2*t);
}

void
BasisFunctionArea<Triangle,Hierarchical,5>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 21);

  Real u = 1 - s - t;

  // S^2
  phiss[ 0] =  0;
  phiss[ 1] =  0;
  phiss[ 2] =  0;

  phiss[ 3] =  0;
  phiss[ 4] =  12*sqrt(3)*t * sgn[0];
  phiss[ 5] =  32*t*t;
  phiss[ 6] = -50*sqrt(5)*t*t*(t - 3*s) * sgn[0];

  phiss[ 7] =  0;
  phiss[ 8] = -12*sqrt(3)*t * sgn[1];
  phiss[ 9] =  32*t*t;
  phiss[10] =  50*sqrt(5)*t*t*(t - 3*u) * sgn[1];

  phiss[11] = -8;
  phiss[12] = -36*sqrt(3)*(u - s) * sgn[2];
  phiss[13] =  32*(u*u - 4*u*s + s*s);
  phiss[14] =  50*sqrt(5)*(u - s)*(s*(s - 8*u) + u*u) * sgn[2];

  phiss[15] = -54*t;
  phiss[16] = -(1024/(  sqrt(3)))*t*(u - s);
  phiss[17] =  (1024/(3*sqrt(3)))*t*(1 - 3*u);
  phiss[18] =  (3125/8.)*t*t*(u - 2*s);
  phiss[19] =  (3125/8.)*t*t*(s - 2*u);
  phiss[20] =  (3125/8.)*t*(s*(s - 4*u) + u*u);

  // CROSS ST
  phist[ 0] =  0;
  phist[ 1] =  0;
  phist[ 2] =  0;

  phist[ 3] =  4;
  phist[ 4] =  12*sqrt(3)*(s - t) * sgn[0];
  phist[ 5] =  64*s*t;
  phist[ 6] =  150*sqrt(5)*t*s*(s - t) * sgn[0];

  phist[ 7] = -4;
  phist[ 8] =  12*sqrt(3)*(u - 2*t) * sgn[1];
  phist[ 9] =  32*t*(t - 2*u);
  phist[10] =  50*sqrt(5)*t*(3*u*u - 6*u*t + t*t) * sgn[1];

  phist[11] = -4;
  phist[12] = -12*sqrt(3)*(u - 2*s) * sgn[2];
  phist[13] =  32*s*(s - 2*u);
  phist[14] = -50*sqrt(5)*s*(3*u*u - 6*u*s + s*s)* sgn[2];

  phist[15] =  27*(u - s - t);
  phist[16] =  (512/(3*sqrt(3)))*(s*(s + 4*t) + u*(u - 2*(t + 2*s)));
  phist[17] =  (512/(3*sqrt(3)))*(t*(t + 4*s) + u*(u - 2*(s + 2*t)));
  phist[18] = -(3125/8.)*s*t*(1 - 3*u);
  phist[19] =  (3125/8.)*t*(s*t + u*(u - 2*s - t));
  phist[20] =  (3125/8.)*s*(s*t + u*(u - s - 2*t));

  // T^2
  phitt[ 0] =  0;
  phitt[ 1] =  0;
  phitt[ 2] =  0;

  phitt[ 3] =  0;
  phitt[ 4] = -12*sqrt(3)*s * sgn[0];
  phitt[ 5] =  32*s*s;
  phitt[ 6] =  50*sqrt(5)*s*s*(s - 3*t) * sgn[0];

  phitt[ 7] =  -8;
  phitt[ 8] = -36*sqrt(3)*(t - u) * sgn[1];
  phitt[ 9] =  32*(t*(t - 4*u) + u*u);
  phitt[10] =  50*sqrt(5)*(t - u)*(u*u - 8*u*t + t*t) * sgn[1];

  phitt[11] =  0;
  phitt[12] =  12*sqrt(3)*s * sgn[2];
  phitt[13] =  32*s*s;
  phitt[14] = -50*sqrt(5)*s*s*(s - 3*u) * sgn[2];

  phitt[15] =  -54*s;
  phitt[16] =  (1024/(3*sqrt(3)))*s*(1 - 3*u);
  phitt[17] =  (1024/(  sqrt(3)))*s*(t - u);
  phitt[18] =  (3125/8.)*s*s*(u - 2*t);
  phitt[19] =  (3125/8.)*s*(t*(t - 4*u) + u*u);
  phitt[20] =  (3125/8.)*s*s*(t - 2*u);
}


//----------------------------------------------------------------------------//
// Hierarchical: P6

void
BasisFunctionArea<Triangle,Hierarchical,6>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 28);

  Real u = 1 - s - t;

  phi[ 0] = u;                                      // 1 @ node 1 (s = 0, t = 0)
  phi[ 1] = s;                                      // 1 @ node 2 (s = 1, t = 0)
  phi[ 2] = t;                                      // 1 @ node 3 (s = 0, t = 1)

  phi[ 3] = 4*s*t;                                  // max=1 on edge 1 @ midpoint s = 1/2, t = 1/2, u = 0
  phi[ 4] = 6*sqrt(3)*s*t*(s - t) * sgn[0];         // max=1 on edge 1 (asymmetric)
  phi[ 5] = 16*s*s*t*t;                             // max=1 on edge 1 @ midpoint
  phi[ 6] = 25*sqrt(5)*s*s*t*t*(s - t) * sgn[0];    // max=1 on edge 1 (asymmetric)
  phi[ 7] = 64*s*s*s*t*t*t;                         // max=1 on edge 1 @ midpoint

  phi[ 8] = 4*t*u;                                  // max=1 on edge 2 @ midpoint s = 0, t = 1/2, u = 1/2
  phi[ 9] = 6*sqrt(3)*t*u*(t - u) * sgn[1];         // max=1 on edge 2 (asymmetric)
  phi[10] = 16*t*t*u*u;                             // max=1 on edge 2 @ midpoint
  phi[11] = 25*sqrt(5)*t*t*u*u*(t - u) * sgn[1];    // max=1 on edge 2 (asymmetric)
  phi[12] = 64*t*t*t*u*u*u;                         // max=1 on edge 2 @ midpoint

  phi[13] = 4*u*s;                                  // max=1 on edge 3 @ midpoint s = 1/2, t = 0, u = 1/2
  phi[14] = 6*sqrt(3)*u*s*(u - s) * sgn[2];         // max=1 on edge 3 (asymmetric)
  phi[15] = 16*u*u*s*s;                             // max=1 on edge 3 @ midpoint
  phi[16] = 25*sqrt(5)*u*u*s*s*(u - s) * sgn[2];    // max=1 on edge 3 (asymmetric)
  phi[17] = 64*u*u*u*s*s*s;                         // max=1 on edge 3 @ midpoint

  phi[18] = 27*s*t*u;                               // max=1 @ centroid (s,t) = 1/3
  phi[19] = 512*s*t*u*(u - s)/(3*sqrt(3));          // max=1 @ s = (3 - sqrt(3))/8, t = 1/4
  phi[20] = 512*s*t*u*(u - t)/(3*sqrt(3));          // max=1 @ s = 1/4, t = (3 - sqrt(3))/8
  phi[21] = 3125*s*s*t*t*u/16.;                     // max=1 @ s = 2/5, t = 2/5 (u = 1/5)
  phi[22] = 3125*t*t*u*u*s/16.;                     // max=1 @ s = 1/5, t = 2/5 (u = 2/5)
  phi[23] = 3125*u*u*s*s*t/16.;                     // max=1 @ s = 2/5, t = 1/5 (u = 2/5)

  phi[24] = 729*s*s*t*t*u*u;                        // max=1 @ centroid
  phi[25] = 46656*s*s*t*t*u*(s - t)/(25*sqrt(5));   // max=1 @ s = (5 - sqrt(5))/12, t = (5 + sqrt(5))/12 (u = 1/6)
  phi[26] = 46656*t*t*u*u*s*(t - u)/(25*sqrt(5));   // max=1 @ s = 1/6, t = (5 - sqrt(5))/12 (u = (5 + sqrt(5))/12)
  phi[27] = 46656*u*u*s*s*t*(u - s)/(25*sqrt(5));   // max=1 @ s = (5 + sqrt(5))/12, t = 1/6 (u = (5 - sqrt(5))/12)
}


void
BasisFunctionArea<Triangle,Hierarchical,6>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 28);

  Real u = 1 - s - t;

  phis[ 0] = -1;
  phis[ 1] =  1;
  phis[ 2] =  0;

  phis[ 3] =  4*t;
  phis[ 4] = -6*sqrt(3)*t*(t - 2*s) * sgn[0];
  phis[ 5] =  32*s*t*t;
  phis[ 6] =  25*sqrt(5)*s*t*t*(3*s - 2*t) * sgn[0];
  phis[ 7] =  192*s*s*t*t*t;

  phis[ 8] = -4*t;
  phis[ 9] = -6*sqrt(3)*t*(t - 2*u) * sgn[1];
  phis[10] = -32*t*t*u;
  phis[11] =  25*sqrt(5)*t*t*u*(3*u - 2*t) * sgn[1];
  phis[12] = -192*t*t*t*u*u;

  phis[13] =  4*(u - s);
  phis[14] =  6*sqrt(3)*(s*(s - 2*u) + u*(u - 2*s)) * sgn[2];
  phis[15] =  32*u*s*(u - s);
  phis[16] =  50*sqrt(5)*u*s*(u*u - 3*u*s + s*s) * sgn[2];
  phis[17] =  192*u*u*s*s*(u - s);

  phis[18] =  27*t*(u - s);
  phis[19] =  (512/(3*sqrt(3)))*t*(u*(u - 2*s) + s*(s - 2*u));
  phis[20] =  (512/(3*sqrt(3)))*t*(u*(u - t) + s*(t - 2*u));
  phis[21] = -(3125/16.)*s*t*t*(s - 2*u);
  phis[22] =  (3125/16.)*t*t*u*(u - 2*s);
  phis[23] =  (6250/16.)*s*t*u*(u - s);

  phis[24] =  1458*s*t*t*u*(u - s);
  phis[25] = -(46656/(25*sqrt(5)))*s*t*t*(s*(s - 3*u) - t*(s - 2*u));
  phis[26] = -(46656/(25*sqrt(5)))*t*t*u*(u*(u - 3*s) - t*(u - 2*s));
  phis[27]  =  (93312/(25*sqrt(5)))*s*t*u*(s*s - 3*s*u + u*u);


  phit[ 0] = -1;
  phit[ 1] =  0;
  phit[ 2] =  1;

  phit[ 3] =  4*s;
  phit[ 4] =  6*sqrt(3)*s*(s - 2*t) * sgn[0];
  phit[ 5] =  32*s*s*t;
  phit[ 6] = -25*sqrt(5)*s*s*t*(3*t - 2*s) * sgn[0];
  phit[ 7] =  192*s*s*s*t*t;

  phit[ 8] = -4*(t - u);
  phit[ 9] = -6*sqrt(3)*(u*u - 4*u*t + t*t) * sgn[1];
  phit[10] = -32*t*u*(t - u);
  phit[11] = -50*sqrt(5)*t*u*(t*t - 3*u*t + u*u) * sgn[1];
  phit[12] = -192*t*t*u*u*(t - u);

  phit[13] = -4*s;
  phit[14] =  6*sqrt(3)*s*(s - 2*u) * sgn[2];
  phit[15] = -32*s*s*u;
  phit[16] = -25*sqrt(5)*s*s*u*(3*u - 2*s) * sgn[2];
  phit[17] = -192*s*s*s*u*u;

  phit[18] =  27*s*(u - t);
  phit[19] =  (512/(3*sqrt(3)))*s*(u*(u - 2*t) + s*(t - u));
  phit[20] =  (512/(3*sqrt(3)))*s*(u*(u - 2*t) + t*(t - 2*u));
  phit[21] = -(3125/16.)*s*s*t*(t - 2*u);
  phit[22] =  (6250/16.)*s*t*u*(u - t);
  phit[23] =  (3125/16.)*s*s*u*(u - 2*t);

  phit[24] = -1458*s*s*t*u*(t - u);
  phit[25] =  (46656/(25*sqrt(5)))*s*s*t*(t*(t - 3*u) - s*(t - 2*u));
  phit[26] = -(93312/(25*sqrt(5)))*s*t*u*(t*t - 3*t*u + u*u);
  phit[27] =  (46656/(25*sqrt(5)))*u*s*s*(u*(u - 3*t) - s*(u - 2*t));
}

void
BasisFunctionArea<Triangle,Hierarchical,6>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 28);

  Real u = 1 - s - t;

  // S^2
  phiss[ 0] =  0;
  phiss[ 1] =  0;
  phiss[ 2] =  0;

  phiss[ 3] =  0;
  phiss[ 4] =  12*sqrt(3)*t * sgn[0];
  phiss[ 5] =  32*t*t;
  phiss[ 6] = -50*sqrt(5)*t*t*(t - 3*s) * sgn[0];
  phiss[ 7] =  384*s*t*t*t;

  phiss[ 8] =  0;
  phiss[ 9] = -12*sqrt(3)*t * sgn[1];
  phiss[10] =  32*t*t;
  phiss[11] =  50*sqrt(5)*t*t*(t - 3*u) * sgn[1];
  phiss[12] =  384*t*t*t*u;

  phiss[13] = -8;
  phiss[14] = -36*sqrt(3)*(u - s) * sgn[2];
  phiss[15] =  32*(u*u - 4*u*s + s*s);
  phiss[16] =  50*sqrt(5)*(u - s)*(s*(s - 8*u) + u*u) * sgn[2];
  phiss[17] =  384*s*u*(s*(s - 3*u) + u*u);

  phiss[18] = -54*t;
  phiss[19] = -(1024/(  sqrt(3)))*t*(u - s);
  phiss[20] =  (1024/(3*sqrt(3)))*t*(1 - 3*u);
  phiss[21] =  (3125/8.)*t*t*(u - 2*s);
  phiss[22] =  (3125/8.)*t*t*(s - 2*u);
  phiss[23] =  (3125/8.)*t*(s*(s - 4*u) + u*u);

  phiss[24] =  1458*t*t*(s*(s - 4*u) + u*u);
  phiss[25] = -(93312/(25*sqrt(5)))*t*t*(t*(u - 2*s) - 3*s*(u - s));
  phiss[26] =  (93312/(25*sqrt(5)))*t*t*(t*(s - 2*u) + 3*u*(u - s));
  phiss[27] =  (93312/(25*sqrt(5)))*t*(u - s)*(s*(s - 8*u) + u*u);

  // CROSS ST
  phist[ 0] =  0;
  phist[ 1] =  0;
  phist[ 2] =  0;

  phist[ 3] =  4;
  phist[ 4] =  12*sqrt(3)*(s - t) * sgn[0];
  phist[ 5] =  64*s*t;
  phist[ 6] =  150*sqrt(5)*t*s*(s - t) * sgn[0];
  phist[ 7] =  576*s*s*t*t;

  phist[ 8] = -4;
  phist[ 9] =  12*sqrt(3)*(u - 2*t) * sgn[1];
  phist[10] =  32*t*(t - 2*u);
  phist[11] =  50*sqrt(5)*t*(3*u*u - 6*u*t + t*t) * sgn[1];
  phist[12] =  192*t*t*u*(2*t - 3*u);

  phist[13] = -4;
  phist[14] = -12*sqrt(3)*(u - 2*s) * sgn[2];
  phist[15] =  32*s*(s - 2*u);
  phist[16] = -50*sqrt(5)*s*(3*u*u - 6*u*s + s*s)* sgn[2];
  phist[17] =  192*s*s*u*(2*s - 3*u);

  phist[18] =  27*(u - s - t);
  phist[19] =  (512/(3*sqrt(3)))*(s*(s + 4*t) + u*(u - 2*(t + 2*s)));
  phist[20] =  (512/(3*sqrt(3)))*(t*(t + 4*s) + u*(u - 2*(s + 2*t)));
  phist[21] = -(3125/8.)*s*t*(1 - 3*u);
  phist[22] =  (3125/8.)*t*(s*t + u*(u - 2*s - t));
  phist[23] =  (3125/8.)*s*(s*t + u*(u - s - 2*t));

  phist[24] =  1458*s*t*(s*t - 2*u*(s + t - u));
  phist[25] = -(93312/(25*sqrt(5)))*s*t*(s - t)*(s + t - 3*u);
  phist[26] =  (93312/(25*sqrt(5)))*t*(s*t*t - t*u*(6*s + t) - u*u*(u - 3*(s + t)));
  phist[27] = -(93312/(25*sqrt(5)))*s*(s*s*t - s*u*(s + 6*t) - u*u*(u - 3*(s + t)));

  // T^2
  phitt[ 0] =  0;
  phitt[ 1] =  0;
  phitt[ 2] =  0;

  phitt[ 3] =  0;
  phitt[ 4] = -12*sqrt(3)*s * sgn[0];
  phitt[ 5] =  32*s*s;
  phitt[ 6] =  50*sqrt(5)*s*s*(s - 3*t) * sgn[0];
  phitt[ 7] =  384*s*s*s*t;

  phitt[ 8] =  -8;
  phitt[ 9] = -36*sqrt(3)*(t - u) * sgn[1];
  phitt[10] =  32*(t*(t - 4*u) + u*u);
  phitt[11] =  50*sqrt(5)*(t - u)*(u*u - 8*u*t + t*t) * sgn[1];
  phitt[12] =  384*t*u*(t*(t - 3*u) + u*u);

  phitt[13] =  0;
  phitt[14] =  12*sqrt(3)*s * sgn[2];
  phitt[15] =  32*s*s;
  phitt[16] = -50*sqrt(5)*s*s*(s - 3*u) * sgn[2];
  phitt[17] =  384*s*s*s*u;

  phitt[18] =  -54*s;
  phitt[19] =  (1024/(3*sqrt(3)))*s*(1 - 3*u);
  phitt[20] =  (1024/(  sqrt(3)))*s*(t - u);
  phitt[21] =  (3125/8.)*s*s*(u - 2*t);
  phitt[22] =  (3125/8.)*s*(t*(t - 4*u) + u*u);
  phitt[23] =  (3125/8.)*s*s*(t - 2*u);

  phitt[24] =  1458*s*s*(t*(t - 4*u) + u*u);
  phitt[25] = -(93312/(25*sqrt(5)))*s*s*(s*(2*t - u) - 3*t*(t - u));
  phitt[26] =  (93312/(25*sqrt(5)))*s*(t - u)*(t*(t - 8*u) + u*u);
  phitt[27] = -(93312/(25*sqrt(5)))*s*s*(s*(t - 2*u) - 3*u*(t - u));
}


//----------------------------------------------------------------------------//
// Hierarchical: P7

void
BasisFunctionArea<Triangle,Hierarchical,7>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 36);

  Real u = 1 - s - t;

  phi[ 0] = u;                                              // 1 @ node 1 (s = 0, t = 0)
  phi[ 1] = s;                                              // 1 @ node 2 (s = 1, t = 0)
  phi[ 2] = t;                                              // 1 @ node 3 (s = 0, t = 1)

  phi[ 3] = 4*s*t;                                          // max=1 on edge 1 @ midpoint s = 1/2, t = 1/2, u = 0
  phi[ 4] = 6*sqrt(3)*s*t*(s - t) * sgn[0];                 // max=1 on edge 1 (asymmetric)
  phi[ 5] = 16*s*s*t*t;                                     // max=1 on edge 1 @ midpoint
  phi[ 6] = 25*sqrt(5)*s*s*t*t*(s - t) * sgn[0];            // max=1 on edge 1 (asymmetric)
  phi[ 7] = 64*s*s*s*t*t*t;                                 // max=1 on edge 1 @ midpoint
  phi[ 8] = 2744*sqrt(7)*s*s*s*t*t*t*(s - t)/27. * sgn[0];  // max=1 on edge 1 (asymmetric)

  phi[ 9] = 4*t*u;                                          // max=1 on edge 2 @ midpoint s = 0, t = 1/2, u = 1/2
  phi[10] = 6*sqrt(3)*t*u*(t - u) * sgn[1];                 // max=1 on edge 2 (asymmetric)
  phi[11] = 16*t*t*u*u;                                     // max=1 on edge 2 @ midpoint
  phi[12] = 25*sqrt(5)*t*t*u*u*(t - u) * sgn[1];            // max=1 on edge 2 (asymmetric)
  phi[13] = 64*t*t*t*u*u*u;                                 // max=1 on edge 2 @ midpoint
  phi[14] = 2744*sqrt(7)*t*t*t*u*u*u*(t - u)/27. * sgn[1];  // max=1 on edge 2 (asymmetric)

  phi[15] = 4*u*s;                                          // max=1 on edge 3 @ midpoint s = 1/2, t = 0, u = 1/2
  phi[16] = 6*sqrt(3)*u*s*(u - s) * sgn[2];                 // max=1 on edge 3 (asymmetric)
  phi[17] = 16*u*u*s*s;                                     // max=1 on edge 3 @ midpoint
  phi[18] = 25*sqrt(5)*u*u*s*s*(u - s) * sgn[2];            // max=1 on edge 3 (asymmetric)
  phi[19] = 64*u*u*u*s*s*s;                                 // max=1 on edge 3 @ midpoint
  phi[20] = 2744*sqrt(7)*u*u*u*s*s*s*(u - s)/27. * sgn[2];  // max=1 on edge 3 (asymmetric)

  phi[21] = 27*s*t*u;                                       // max=1 @ centroid (s,t) = 1/3
  phi[22] = 512*s*t*u*(u - s)/(3*sqrt(3));                  // max=1 @ s = (3 - sqrt(3))/8, t = 1/4
  phi[23] = 512*s*t*u*(u - t)/(3*sqrt(3));                  // max=1 @ s = 1/4, t = (3 - sqrt(3))/8
  phi[24] = 3125*s*s*t*t*u/16.;                             // max=1 @ s = 2/5, t = 2/5 (u = 1/5)
  phi[25] = 3125*t*t*u*u*s/16.;                             // max=1 @ s = 1/5, t = 2/5 (u = 2/5)
  phi[26] = 3125*u*u*s*s*t/16.;                             // max=1 @ s = 2/5, t = 1/5 (u = 2/5)

  phi[27] = 729*s*s*t*t*u*u;                                // max=1 @ centroid
  phi[28] = 46656*s*s*t*t*u*(s - t)/(25*sqrt(5));           // max=1 @ s = (5 - sqrt(5))/12, t = (5 + sqrt(5))/12 (u = 1/6)
  phi[29] = 46656*t*t*u*u*s*(t - u)/(25*sqrt(5));           // max=1 @ s = 1/6, t = (5 - sqrt(5))/12 (u = (5 + sqrt(5))/12)
  phi[30] = 46656*u*u*s*s*t*(u - s)/(25*sqrt(5));           // max=1 @ s = (5 + sqrt(5))/12, t = 1/6 (u = (5 - sqrt(5))/12)

  phi[31] = 823543*s*s*t*t*u*u*(u - s)/(100*sqrt(5));       // max=1 @ s = (5 - sqrt(5))/14, t = 2/7, u = (5 + sqrt(5))/14
  phi[32] = 823543*s*s*t*t*u*u*(u - t)/(100*sqrt(5));       // max=1 @ s = 2/7, t = (5 - sqrt(5))/14, u = (5 + sqrt(5))/14
  phi[33] = 823543*s*s*s*t*t*t*u/729.;                      // max=1 @ s = 3/7, t = 3/7 (u = 1/7)
  phi[34] = 823543*t*t*t*u*u*u*s/729.;                      // max=1 @ s = 1/7, t = 3/7 (u = 3/7)
  phi[35] = 823543*u*u*u*s*s*s*t/729.;                      // max=1 @ s = 3/7, t = 1/7 (u = 3/7)
}


void
BasisFunctionArea<Triangle,Hierarchical,7>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 36);

  Real u = 1 - s - t;

  phis[ 0] = -1;
  phis[ 1] =  1;
  phis[ 2] =  0;

  phis[ 3] =  4*t;
  phis[ 4] = -6*sqrt(3)*t*(t - 2*s) * sgn[0];
  phis[ 5] =  32*s*t*t;
  phis[ 6] =  25*sqrt(5)*s*t*t*(3*s - 2*t) * sgn[0];
  phis[ 7] =  192*s*s*t*t*t;
  phis[ 8] =  2744*sqrt(7)*s*s*t*t*t*(4*s - 3*t)/27. * sgn[0];

  phis[ 9] = -4*t;
  phis[10] = -6*sqrt(3)*t*(t - 2*u) * sgn[1];
  phis[11] = -32*t*t*u;
  phis[12] =  25*sqrt(5)*t*t*u*(3*u - 2*t) * sgn[1];
  phis[13] = -192*t*t*t*u*u;
  phis[14] =  2744*sqrt(7)*t*t*t*u*u*(4*u - 3*t)/27. * sgn[1];

  phis[15] =  4*(u - s);
  phis[16] =  6*sqrt(3)*(u*u - 4*u*s + s*s) * sgn[2];
  phis[17] =  32*u*s*(u - s);
  phis[18] =  50*sqrt(5)*u*s*(u*u - 3*u*s + s*s) * sgn[2];
  phis[19] =  192*u*u*s*s*(u - s);
  phis[20] =  2744*sqrt(7)*u*u*s*s*(3*u*u - 8*u*s + 3*s*s)/27. * sgn[2];

  phis[21] =  27*t*(u - s);
  phis[22] =  (512/(3*sqrt(3)))*t*(u*(u - 2*s) + s*(s - 2*u));
  phis[23] =  (512/(3*sqrt(3)))*t*(u*(u - t) + s*(t - 2*u));
  phis[24] = -(3125/16.)*s*t*t*(s - 2*u);
  phis[25] =  (3125/16.)*t*t*u*(u - 2*s);
  phis[26] =  (6250/16.)*s*t*u*(u - s);

  phis[27] =  1458*s*t*t*u*(u - s);
  phis[28] = -(46656/(25*sqrt(5)))*s*t*t*(s*(s - 3*u) - t*(s - 2*u));
  phis[29] = -(46656/(25*sqrt(5)))*t*t*u*(u*(u - 3*s) - t*(u - 2*s));
  phis[30] =  (93312/(25*sqrt(5)))*s*t*u*(s*s - 3*s*u + u*u);

  phis[31] =  (823543/( 50*sqrt(5)))*s*t*t*u*(u*u - 3*u*s + s*s);
  phis[32] =  (823543/(100*sqrt(5)))*s*t*t*u*(u*(2*u - 3*s) - 2*t*(u - s));
  phis[33] = -(823543/729.)*s*s*t*t*t*(s - 3*u);
  phis[34] =  (823543/729.)*t*t*t*u*u*(u - 3*s);
  phis[35] =  (823543/243.)*u*u*s*s*t*(u - s);


  phit[ 0] = -1;
  phit[ 1] =  0;
  phit[ 2] =  1;

  phit[ 3] =  4*s;
  phit[ 4] =  6*sqrt(3)*s*(s - 2*t) * sgn[0];
  phit[ 5] =  32*s*s*t;
  phit[ 6] = -25*sqrt(5)*s*s*t*(3*t - 2*s) * sgn[0];
  phit[ 7] =  192*s*s*s*t*t;
  phit[ 8] =  2744*sqrt(7)*s*s*s*t*t*(3*s - 4*t)/27. * sgn[0];

  phit[ 9] = -4*(t - u);
  phit[10] = -6*sqrt(3)*(u*u - 4*u*t + t*t) * sgn[1];
  phit[11] = -32*t*u*(t - u);
  phit[12] = -50*sqrt(5)*t*u*(t*t - 3*u*t + u*u) * sgn[1];
  phit[13] = -192*t*t*u*u*(t - u);
  phit[14] = -2744*sqrt(7)*t*t*u*u*(3*t*t - 8*t*u + 3*u*u)/27. * sgn[1];

  phit[15] = -4*s;
  phit[16] =  6*sqrt(3)*s*(s - 2*u) * sgn[2];
  phit[17] = -32*s*s*u;
  phit[18] = -25*sqrt(5)*s*s*u*(3*u - 2*s) * sgn[2];
  phit[19] = -192*s*s*s*u*u;
  phit[20] =  2744*sqrt(7)*s*s*s*u*u*(3*s - 4*u)/27. * sgn[2];

  phit[21] =  27*s*(u - t);
  phit[22] =  (512/(3*sqrt(3)))*s*(u*(u - 2*t) + s*(t - u));
  phit[23] =  (512/(3*sqrt(3)))*s*(u*(u - 2*t) + t*(t - 2*u));
  phit[24] = -(3125/16.)*s*s*t*(t - 2*u);
  phit[25] =  (6250/16.)*s*t*u*(u - t);
  phit[26] =  (3125/16.)*s*s*u*(u - 2*t);

  phit[27] = -1458*s*s*t*u*(t - u);
  phit[28] =  (46656/(25*sqrt(5)))*s*s*t*(t*(t - 3*u) - s*(t - 2*u));
  phit[29] = -(93312/(25*sqrt(5)))*s*t*u*(t*t - 3*t*u + u*u);
  phit[30] =  (46656/(25*sqrt(5)))*u*s*s*(u*(u - 3*t) - s*(u - 2*t));

  phit[31] =  (823543/(100*sqrt(5)))*s*s*t*u*(u*(2*u - 3*t) + 2*s*(t - u));
  phit[32] =  (823543/( 50*sqrt(5)))*s*s*t*u*(u*u - 3*u*t + t*t);
  phit[33] = -(823543/729.)*s*s*s*t*t*(t - 3*u);
  phit[34] = -(823543/243.)*s*t*t*u*u*(t - u);
  phit[35] =  (823543/729.)*u*u*s*s*s*(u - 3*t);
}


void
BasisFunctionArea<Triangle,Hierarchical,7>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 36);

  Real u = 1 - s - t;

  // S^2
  phiss[ 0] =  0;
  phiss[ 1] =  0;
  phiss[ 2] =  0;

  phiss[ 3] =  0;
  phiss[ 4] =  12*sqrt(3)*t * sgn[0];
  phiss[ 5] =  32*t*t;
  phiss[ 6] = -50*sqrt(5)*t*t*(t - 3*s) * sgn[0];
  phiss[ 7] =  384*s*t*t*t;
  phiss[ 8] = -(5488*sqrt(7)/9.)*s*t*t*t*(t - 2*s) * sgn[0];

  phiss[ 9] =  0;
  phiss[10] = -12*sqrt(3)*t * sgn[1];
  phiss[11] =  32*t*t;
  phiss[12] =  50*sqrt(5)*t*t*(t - 3*u) * sgn[1];
  phiss[13] =  384*t*t*t*u;
  phiss[14] =  (5488*sqrt(7)/9.)*t*t*t*u*(t - 2*u) * sgn[1];

  phiss[15] = -8;
  phiss[16] = -36*sqrt(3)*(u - s) * sgn[2];
  phiss[17] =  32*(u*u - 4*u*s + s*s);
  phiss[18] =  50*sqrt(5)*(u - s)*(s*(s - 8*u) + u*u) * sgn[2];
  phiss[19] =  384*s*u*(s*(s - 3*u) + u*u);
  phiss[20] =  (5488*sqrt(7)/9.)*s*u*(u - s)*(s*(s - 5*u) + u*u) * sgn[2];

  phiss[21] = -54*t;
  phiss[22] = -(1024/(  sqrt(3)))*t*(u - s);
  phiss[23] =  (1024/(3*sqrt(3)))*t*(1 - 3*u);
  phiss[24] =  (3125/8.)*t*t*(u - 2*s);
  phiss[25] =  (3125/8.)*t*t*(s - 2*u);
  phiss[26] =  (3125/8.)*t*(s*(s - 4*u) + u*u);

  phiss[27] =  1458*t*t*(s*(s - 4*u) + u*u);
  phiss[28] = -(93312/(25*sqrt(5)))*t*t*(t*(u - 2*s) - 3*s*(u - s));
  phiss[29] =  (93312/(25*sqrt(5)))*t*t*(t*(s - 2*u) + 3*u*(u - s));
  phiss[30] =  (93312/(25*sqrt(5)))*t*(u - s)*(s*(s - 8*u) + u*u);

  phiss[31] =  (823543/(50*sqrt(5)))*t*t*(u - s)*(s*(s - 8*u) + u*u);
  phiss[32] = -(823543/(50*sqrt(5)))*t*t*(s*s*(1 - s) + u*u*(1 + 9*s - 2*u) - 4*s*u);
  phiss[33] =  (1647086/243.)*s*t*t*t*(u - s);
  phiss[34] = -(1647086/243.)*t*t*t*u*(u - s);
  phiss[35] =  (1647086/243.)*s*t*u*(s*(s - 3*u) + u*u);

  // CROSS ST
  phist[ 0] =  0;
  phist[ 1] =  0;
  phist[ 2] =  0;

  phist[ 3] =  4;
  phist[ 4] =  12*sqrt(3)*(s - t) * sgn[0];
  phist[ 5] =  64*s*t;
  phist[ 6] =  150*sqrt(5)*t*s*(s - t) * sgn[0];
  phist[ 7] =  576*s*s*t*t;
  phist[ 8] =  (10976*sqrt(7)/9)*s*s*t*t*(s - t) * sgn[0];

  phist[ 9] = -4;
  phist[10] =  12*sqrt(3)*(u - 2*t) * sgn[1];
  phist[11] =  32*t*(t - 2*u);
  phist[12] =  50*sqrt(5)*t*(3*u*u - 6*u*t + t*t) * sgn[1];
  phist[13] =  192*t*t*u*(2*t - 3*u);
  phist[14] =  (5488*sqrt(7)/9.)*t*t*u*(2*u*u - 4*u*t + t*t) * sgn[1];

  phist[15] = -4;
  phist[16] = -12*sqrt(3)*(u - 2*s) * sgn[2];
  phist[17] =  32*s*(s - 2*u);
  phist[18] = -50*sqrt(5)*s*(3*u*u - 6*u*s + s*s)* sgn[2];
  phist[19] =  192*s*s*u*(2*s - 3*u);
  phist[20] = -(5488*sqrt(7)/9)*s*s*u*(s*(s - 4*u) + 2*u*u) * sgn[2];

  phist[21] =  27*(u - s - t);
  phist[22] =  (512/(3*sqrt(3)))*(s*(s + 4*t) + u*(u - 2*(t + 2*s)));
  phist[23] =  (512/(3*sqrt(3)))*(t*(t + 4*s) + u*(u - 2*(s + 2*t)));
  phist[24] = -(3125/8.)*s*t*(1 - 3*u);
  phist[25] =  (3125/8.)*t*(s*t + u*(u - 2*s - t));
  phist[26] =  (3125/8.)*s*(s*t + u*(u - s - 2*t));

  phist[27] =  1458*s*t*(s*t - 2*u*(s + t - u));
  phist[28] = -(93312/(25*sqrt(5)))*s*t*(s - t)*(s + t - 3*u);
  phist[29] =  (93312/(25*sqrt(5)))*t*(s*t*t - t*u*(6*s + t) - u*u*(u - 3*(s + t)));
  phist[30] = -(93312/(25*sqrt(5)))*s*(s*s*t - s*u*(s + 6*t) - u*u*(u - 3*(s + t)));

  phist[31] = -(823543/(50*sqrt(5)))*s*t*(s*s*(1 - s) + u*u*(3 + 9*s - 5*u) - 3*s*u*(2 - s));
  phist[32] = -(823543/(50*sqrt(5)))*s*t*(t*t*(1 - t) + u*u*(3 + 9*t - 5*u) - 3*t*u*(2 - t));
  phist[33] = -(823543/243.)*s*s*t*t*(1 - 4*u);
  phist[34] =  (823543/243.)*t*t*u*(2*t*(1 - t) - u*(3 - 4*u));
  phist[35] =  (823543/243.)*s*s*u*(2*s*(1 - s) - u*(3 - 4*u));

  // T^2
  phitt[ 0] =  0;
  phitt[ 1] =  0;
  phitt[ 2] =  0;

  phitt[ 3] =  0;
  phitt[ 4] = -12*sqrt(3)*s * sgn[0];
  phitt[ 5] =  32*s*s;
  phitt[ 6] =  50*sqrt(5)*s*s*(s - 3*t) * sgn[0];
  phitt[ 7] =  384*s*s*s*t;
  phitt[ 8] =  (5488*sqrt(7)/9.)*s*s*s*t*(s - 2*t) * sgn[0];

  phitt[ 9] = -8;
  phitt[10] = -36*sqrt(3)*(t - u) * sgn[1];
  phitt[11] =  32*(t*(t - 4*u) + u*u);
  phitt[12] =  50*sqrt(5)*(t - u)*(u*u - 8*u*t + t*t) * sgn[1];
  phitt[13] =  384*t*u*(t*(t - 3*u) + u*u);
  phitt[14] =  (5488*sqrt(7)/9.)*t*u*(t - u)*(t*(t - 5*u) + u*u) * sgn[1];

  phitt[15] =  0;
  phitt[16] =  12*sqrt(3)*s * sgn[2];
  phitt[17] =  32*s*s;
  phitt[18] = -50*sqrt(5)*s*s*(s - 3*u) * sgn[2];
  phitt[19] =  384*s*s*s*u;
  phitt[20] = -(5488*sqrt(7)/9.)*s*s*s*u*(s - 2*u) * sgn[2];

  phitt[21] = -54*s;
  phitt[22] =  (1024/(3*sqrt(3)))*s*(1 - 3*u);
  phitt[23] =  (1024/(  sqrt(3)))*s*(t - u);
  phitt[24] =  (3125/8.)*s*s*(u - 2*t);
  phitt[25] =  (3125/8.)*s*(t*(t - 4*u) + u*u);
  phitt[26] =  (3125/8.)*s*s*(t - 2*u);

  phitt[27] =  1458*s*s*(t*(t - 4*u) + u*u);
  phitt[28] = -(93312/(25*sqrt(5)))*s*s*(s*(2*t - u) - 3*t*(t - u));
  phitt[29] =  (93312/(25*sqrt(5)))*s*(t - u)*(t*(t - 8*u) + u*u);
  phitt[30] = -(93312/(25*sqrt(5)))*s*s*(s*(t - 2*u) - 3*u*(t - u));

  phitt[31] = -(823543/(50*sqrt(5)))*s*s*(t*t*(1 - t) + u*u*(1 - 2*u) - t*u*(4 - 9*u));
  phitt[32] = -(823543/(50*sqrt(5)))*s*s*(t - u)*(t*(t - 8*u) + u*u);
  phitt[33] = -(1647086/243.)*s*s*s*t*(t - u);
  phitt[34] =  (1647086/243.)*s*t*u*(t*(t - 3*u) + u*u);
  phitt[35] =  (1647086/243.)*s*s*s*u*(t - u);
}

}
