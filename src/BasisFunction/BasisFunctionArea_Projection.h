// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONAREA_PROJECTION_H
#define BASISFUNCTIONAREA_PROJECTION_H

// projection of basis area functions

#include "BasisFunctionArea.h"
#include "Topology/ElementTopology.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionArea_projectTo( const BasisFunctionAreaBase<Triangle>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                  const BasisFunctionAreaBase<Triangle>* basisTo  ,       T dofTo[]  , const int nDOFTo );

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionArea_projectTo( const BasisFunctionAreaBase<Quad>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                  const BasisFunctionAreaBase<Quad>* basisTo  ,       T dofTo[]  , const int nDOFTo );

}

#endif // BASISFUNCTIONAREA_PROJECTION_H
