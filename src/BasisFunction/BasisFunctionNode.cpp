// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// node basis functions

#include <string>
#include <iostream>

#include "BasisFunctionNode.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

const int LagrangeNodes<Node>::PMax;


//----------------------------------------------------------------------------//
//Instantiate the singleton
const BasisFunctionNodeBase BasisFunctionNodeBase::self_;
const BasisFunctionNodeBase* BasisFunctionNodeBase::self = &BasisFunctionNodeBase::self_;
const BasisFunctionNodeBase* BasisFunctionNodeBase::P0 = &BasisFunctionNodeBase::self_;

//----------------------------------------------------------------------------//
void
LagrangeNodes<Node>::
get(const int order, std::vector<DLA::VectorS<TopoD1::D,Real>>& sRef)
{
  sRef.resize(1);
  sRef[0] = 0;
}

//----------------------------------------------------------------------------//
void
BasisFunctionNodeBase::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BasisFunctionNode: order = " << order()
                << "  nBasis = " << nBasis() << std::endl;
}

}
