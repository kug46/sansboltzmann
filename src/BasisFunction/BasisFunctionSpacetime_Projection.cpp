// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionSpacetime_Projection.h"

// projection of basis volume functions

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "BasisFunctionSpacetime.h"
#include "BasisFunctionCategory.h"
#include "BasisFunctionSpacetime_Pentatope_Lagrange.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionSpacetime_projectTo( const BasisFunctionSpacetimeBase<Pentatope>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                    const BasisFunctionSpacetimeBase<Pentatope>* basisTo  ,       T dofTo[]  , const int nDOFTo )
{
  SANS_ASSERT( nDOFFrom == basisFrom->nBasis() );
  SANS_ASSERT( nDOFTo == basisTo->nBasis() );

  if ( basisFrom->category() == BasisFunctionCategory_Legendre &&
       basisTo->category()   == BasisFunctionCategory_Legendre )
  {
    //Copy Cell DOFs for Legendre
    int nCellTo = basisTo->nBasisCell();
    int nCell = std::min(basisFrom->nBasisCell(), nCellTo);
    for (int n = 0; n < nCell; n++)
      dofTo[n] = dofFrom[n];

    for (int n = nCell; n < nCellTo; n++)
      dofTo[n] = 0;
  }

  else if ( ( basisFrom->category() == BasisFunctionCategory_Hierarchical ||
             (basisFrom->category() == BasisFunctionCategory_Lagrange && basisFrom->order() == 1) ) &&
            basisTo->category()   == BasisFunctionCategory_Hierarchical )
  {
    //Prolongating by arbitrary increment
    if ( basisTo->order() >= basisFrom->order() )
    {
      //Zero out everything first
      for (int n = 0; n < nDOFTo; n++)
        dofTo[n] = 0;

      //Copy Nodes
      const int nNode = basisFrom->nBasisNode();
      for (int n = 0; n < nNode; n++)
        dofTo[n] = dofFrom[n];

      int offsetFrom = nNode;
      int offsetTo   = nNode;

      //Copy edge DOFs
      int nBasisEdgeFrom = basisFrom->nBasisEdge() / 6;
      int nBasisEdgeTo   = basisTo->nBasisEdge() / 6;
      for (int edge = 0; edge < 6; edge++)
      {
        for (int n = 0; n < nBasisEdgeFrom; n++)
          dofTo[offsetTo + n] = dofFrom[offsetFrom + n];

        offsetFrom += nBasisEdgeFrom;
        offsetTo   += nBasisEdgeTo;
      }

      //Copy face DOFs
      int nBasisFaceFrom = basisFrom->nBasisFace() / 4;
      int nBasisFaceTo   = basisTo->nBasisFace() / 4;
      for (int face = 0; face < 4; face++)
      {
        for (int n = 0; n < nBasisFaceFrom; n++)
          dofTo[offsetTo + n] = dofFrom[offsetFrom + n];

        offsetFrom += nBasisFaceFrom;
        offsetTo   += nBasisFaceTo;
      }

      //Copy cell DOFs
      int nBasisCellFrom = basisFrom->nBasisCell();
      for (int n = 0; n < nBasisCellFrom; n++)
        dofTo[offsetTo + n] = dofFrom[offsetFrom + n];
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Hierarchical: order %d to %d not implemented", basisFrom->order(), basisTo->order() );
  }
  else if ( basisTo->category() == BasisFunctionCategory_Lagrange )
  {
    //Prolongating by arbitrary increment
    std::vector<Real> coords_s, coords_t, coords_u, coords_v;
    getLagrangeNodes_Pentatope(basisTo->order(), coords_s, coords_t, coords_u, coords_v);

    std::vector<Real> phi(nDOFFrom);
    std::array<int,Pentatope::NFace> faceSign; //Leaving faceSign empty since Lagrange basis doesn't need it

    for (int i = 0; i < nDOFTo; i++)
    {
      basisFrom->evalBasis(coords_s[i], coords_t[i], coords_u[i], coords_v[i], faceSign, phi.data(), phi.size() );

      //Evaluate the solution in elemFrom at current node
      dofTo[i] = 0;
      for (int n = 0; n < nDOFFrom; n++)
        dofTo[i] += phi[n]*dofFrom[n];
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION( "unknown basis function category" );
}

#define INSTANTIATE_PENTATOPE(T) \
template void BasisFunctionSpacetime_projectTo( const BasisFunctionSpacetimeBase<Pentatope>* basisFrom, const T dofFrom[], const int nDOFFrom, \
                                             const BasisFunctionSpacetimeBase<Pentatope>* basisTo  ,       T dofTo[]  , const int nDOFTo )

INSTANTIATE_PENTATOPE( Real );

typedef DLA::VectorS<1, Real> VectorS1;
INSTANTIATE_PENTATOPE( VectorS1 );

typedef DLA::VectorS<2, Real> VectorS2;
INSTANTIATE_PENTATOPE( VectorS2 );

typedef DLA::VectorS<3, Real> VectorS3;
INSTANTIATE_PENTATOPE( VectorS3 );

typedef DLA::VectorS<4, Real> VectorS4;
INSTANTIATE_PENTATOPE( VectorS4 );

typedef DLA::VectorS<5, Real> VectorS5;
INSTANTIATE_PENTATOPE( VectorS5 );

typedef DLA::VectorS<6, Real> VectorS6;
INSTANTIATE_PENTATOPE( VectorS6 );

typedef DLA::VectorS<7, Real> VectorS7;
INSTANTIATE_PENTATOPE( VectorS7 );

typedef DLA::VectorS<4, VectorS2> Vector4VectorS2;
INSTANTIATE_PENTATOPE( Vector4VectorS2 );

//typedef DLA::VectorS<4, VectorS3> Vector4VectorS3;
//INSTANTIATE_PENTATOPE( Vector4VectorS3 );

//typedef DLA::VectorS<4, VectorS4> Vector4VectorS4;
//INSTANTIATE_PENTATOPE( Vector4VectorS4 );

//typedef DLA::VectorS<4, VectorS5> Vector4VectorS5;
//INSTANTIATE_PENTATOPE( Vector4VectorS5 );

//typedef DLA::VectorS<4, VectorS6> Vector4VectorS6;
//INSTANTIATE_PENTATOPE( Vector4VectorS6 );

//typedef DLA::VectorS<4, VectorS7> Vector4VectorS7;
//INSTANTIATE_PENTATOPE( Vector4VectorS7 );

//typedef DLA::VectorS<4, VectorS5> Vector4Vector5;
//INSTANTIATE_PENTATOPE( Vector4Vector5 );

typedef DLA::MatrixSymS<4, Real> MatrixSymS4;
INSTANTIATE_PENTATOPE( MatrixSymS4 );

}
