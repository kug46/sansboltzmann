// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionLine_Projection.h"

// projection of basis line functions

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "BasisFunctionLine.h"
#include "BasisFunctionCategory.h"
#include "BasisFunctionLine_Hierarchical.h"
#include "BasisFunctionLine_Legendre.h"
#include "BasisFunctionLine_Lagrange.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Surreal/SurrealS.h"

#include "UserVariables/BoltzmannNVar.h"
namespace SANS
{

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionLine_projectTo( const BasisFunctionLineBase* basisFrom, const T dofFrom[], const int nDOFFrom,
                                  const BasisFunctionLineBase* basisTo  ,       T dofTo[]  , const int nDOFTo )
{
  SANS_ASSERT( nDOFFrom == basisFrom->nBasis() );
  SANS_ASSERT( nDOFTo == basisTo->nBasis() );

  if ( ((basisFrom->category() == BasisFunctionCategory_Legendre) &&
        (basisTo->category()   == BasisFunctionCategory_Legendre)) ||

       ((basisFrom->category() == BasisFunctionCategory_Hierarchical) &&
        (basisTo->category()   == BasisFunctionCategory_Hierarchical)) ||

       ((basisFrom->category() == BasisFunctionCategory_Lagrange) &&
        (basisTo->category()   == BasisFunctionCategory_Hierarchical) &&
        (basisFrom->order() == 1)) )
  {
    //Copy Cell DOFs for Legendre
    int nBasisTo = basisTo->nBasis();
    int nBasis   = std::min( basisFrom->nBasis(), nBasisTo );
    for (int n = 0; n < nBasis; n++)
      dofTo[n] = dofFrom[n];

    for (int n = nBasis; n < nBasisTo; n++)
      dofTo[n] = 0;
  }
  else if ( basisTo->category() == BasisFunctionCategory_Lagrange )
  {
    //Prolongating by arbitrary increment
    std::vector<Real> coords_s;
    getLagrangeNodes_Line(basisTo->order(), coords_s);

    std::vector<Real> phi(nDOFFrom);
    for (int i = 0; i < nDOFTo; i++)
    {
      basisFrom->evalBasis(coords_s[i], phi.data(), phi.size() );

      //Evaluate the solution in elemFrom at current node
      dofTo[i] = 0;
      for (int n = 0; n < nDOFFrom; n++)
        dofTo[i] += phi[n]*dofFrom[n];
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION( "BasisFunctionLine_projectTo<T> - Unknown basis function category." );
}

#define INSTANTIATE(T) \
template void BasisFunctionLine_projectTo( const BasisFunctionLineBase* basisFrom, const T dofFrom[], const int nDOFFrom, \
                                           const BasisFunctionLineBase* basisTo  ,       T dofTo[]  , const int nDOFTo )

INSTANTIATE(Real);

INSTANTIATE(SurrealS<1>);

typedef DLA::VectorS<1, Real> VectorS1;
INSTANTIATE(VectorS1);

typedef DLA::VectorS<2, Real> VectorS2;
INSTANTIATE(VectorS2);

typedef DLA::VectorS<3, Real> VectorS3;
INSTANTIATE(VectorS3);

typedef DLA::VectorS<4, Real> VectorS4;
INSTANTIATE(VectorS4);

typedef DLA::VectorS<5, Real> VectorS5;
INSTANTIATE(VectorS5);

typedef DLA::VectorS<6, Real> VectorS6;
INSTANTIATE(VectorS6);

typedef DLA::VectorS<8, Real> VectorS8;
INSTANTIATE(VectorS8);

// For Boltzmann implementation
typedef DLA::VectorS<9, Real> VectorS9;
INSTANTIATE(VectorS9);

//typedef DLA::VectorS<13, Real> VectorS13;
//NSTANTIATE(VectorS13);

//pedef DLA::VectorS<16, Real> VectorS16;
//STANTIATE(VectorS16);

typedef DLA::VectorS<NVar, Real> VectorSNVar;
INSTANTIATE(VectorSNVar);

typedef DLA::VectorS<22, Real> VectorS22;
INSTANTIATE(VectorS22);

typedef DLA::VectorS<1, VectorS1> Vector1VectorS1;
INSTANTIATE( Vector1VectorS1 );

typedef DLA::VectorS<1, VectorS2> Vector1VectorS2;
INSTANTIATE( Vector1VectorS2 );

typedef DLA::VectorS<1, VectorS3> Vector1VectorS3;
INSTANTIATE( Vector1VectorS3 );

typedef DLA::VectorS<1, VectorS4> Vector1VectorS4;
INSTANTIATE( Vector1VectorS4 );


typedef DLA::VectorS<2, VectorS1> Vector2VectorS1;
INSTANTIATE( Vector2VectorS1 );

typedef DLA::VectorS<2, VectorS2> Vector2VectorS2;
INSTANTIATE( Vector2VectorS2 );

typedef DLA::VectorS<2, VectorS3> Vector2VectorS3;
INSTANTIATE( Vector2VectorS3 );

typedef DLA::VectorS<2, VectorS4> Vector2VectorS4;
INSTANTIATE( Vector2VectorS4 );

typedef DLA::VectorS<2, VectorS5> Vector2VectorS5;
INSTANTIATE( Vector2VectorS5 );

typedef DLA::VectorS<2, VectorS6> Vector2VectorS6;
INSTANTIATE( Vector2VectorS6 );

typedef DLA::VectorS<2, VectorS8> Vector2VectorS8;
INSTANTIATE( Vector2VectorS8 );

// For Boltzmann implementation
typedef DLA::VectorS<2, VectorS9> Vector2VectorS9;
INSTANTIATE( Vector2VectorS9 );

//typedef DLA::VectorS<2, VectorS13> Vector2VectorS13;
//INSTANTIATE( Vector2VectorS13 );

//typedef DLA::VectorS<2, VectorS16> Vector2VectorS16;
//INSTANTIATE( Vector2VectorS16 );

typedef DLA::VectorS<2, VectorSNVar> Vector2VectorSNVar;
INSTANTIATE( Vector2VectorSNVar );


typedef DLA::MatrixSymS<1, Real> MatrixSymS1;
INSTANTIATE( MatrixSymS1 );

typedef DLA::MatrixSymS<2, Real> MatrixSymS2;
INSTANTIATE( MatrixSymS2 );

typedef DLA::MatrixS<3, 3, Real> MatrixS33;
INSTANTIATE( MatrixS33 );

typedef DLA::MatrixS<3, 3, SurrealS<1>> MatrixS33S1;
INSTANTIATE( MatrixS33S1 );
}
