// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// reference element operators

#include <algorithm> // std::find

#include "ElementEdges.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "TraceToCellRefCoord.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Quadrature_Cache.h"

namespace SANS
{

// Each canonical trace is numbered based on the opposing node number

//----------------------------------------------------------------------------//
// cell reference coordinates (s,t) given edge reference coordinate (s)
/*
    2
    |\
    | \
    |  \
    |   \
    |    \
    |     \
    |      \
    0 ----- 1
*/
// parameters:
//  canonicalEdge      edge in triangle (E0=1-2, E1=2-0, E2=0-1); positive orientation is CCW

template<class CellTopology>
const int TraceToCellRefCoord<Line, TopoD2, CellTopology>::TraceNodes[ CellTopology::NEdge ][ Line::NNode ] = { {1,2}, {2,0}, {0,1} };

template<>
const int ElementEdges<Triangle>::EdgeNodes[ Triangle::NEdge ][ Line::NNode ] = { {1,2}, {2,0}, {0,1} };

template<class CellTopology> const int TraceToCellRefCoord<Line, TopoD2, CellTopology>::OrientPos[ Line::NNode ] = {0,1};
template<class CellTopology> const int TraceToCellRefCoord<Line, TopoD2, CellTopology>::OrientNeg[ Line::NNode ] = {1,0};

template<class CellTopology>
void
TraceToCellRefCoord<Line, TopoD2, CellTopology>::
eval( const CanonicalTraceToCell& canonicalEdge, const DLA::VectorS<1,Real>& sRefTrace, DLA::VectorS<2,Real>& sRefCell )
{
  eval(canonicalEdge, sRefTrace[0], sRefCell[0], sRefCell[1]);
}

template<class CellTopology>
void
TraceToCellRefCoord<Line, TopoD2, CellTopology>::
eval( const CanonicalTraceToCell& canonicalEdge, const QuadraturePoint<TopoD1>& sRefTrace, QuadratureCellTracePoint<TopoD2>& sRefCell )
{
  sRefCell.set(canonicalEdge, sRefTrace);
  eval(canonicalEdge, sRefTrace.ref[0], sRefCell.ref[0], sRefCell.ref[1]);
}

template<class CellTopology>
void
TraceToCellRefCoord<Line, TopoD2, CellTopology>::eval( const CanonicalTraceToCell& canonicalEdge, const Real& sRef, Real& sRefArea, Real& tRefArea )
{

  Real s = sRef;

  // Reverse s if the orientation is negative so the normal is outward
  if ( canonicalEdge.orientation < 0 )
    s = 1 - s;

  switch (canonicalEdge.trace)
  {
  case 0:      // 1-2
  {
    sRefArea = 1 - s;
    tRefArea =     s;
    break;
  }
  case 1:      // 2-0
  {
    sRefArea = 0;
    tRefArea = 1 - s;
    break;
  }
  case 2:      // 0-1
  {
    sRefArea = s;
    tRefArea = 0;
    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION( "Unexpected canonicalEdge.trace = %d", canonicalEdge.trace );
  }

}

// Returns the node map that would make the triangle nodes orientation +1
template<class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<Line, TopoD2, CellTopology>::getCanonicalTraceLeft(const int *edgeNodes, const int nEdgeNodes,
                                                                       const int *triNodes, const int nTriNodes,
                                                                       int *canonicalLine, const int ncanonicalLine)
{
  SANS_ASSERT(nEdgeNodes == 2);
  SANS_ASSERT(nTriNodes == 3);
  SANS_ASSERT(ncanonicalLine == 2);

  // Find the node that is not part of the triangle to define the canonical edge
  int canonicalEdge = -1;
  for ( int edge = 0; edge < Triangle::NEdge; edge++ )
  {
    if ( std::find(edgeNodes, edgeNodes+nEdgeNodes, triNodes[edge] ) == edgeNodes+nEdgeNodes )
    {
      canonicalEdge = edge;
      break;
    }
  }

  SANS_ASSERT( canonicalEdge != -1 );

  // Get the canonical edge nodes
  for ( int n = 0; n < 2; n++)
    canonicalLine[n] = triNodes[TraceNodes[canonicalEdge][n]];

  return CanonicalTraceToCell(canonicalEdge, 1);
}

template<class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<Line, TopoD2, CellTopology>::getCanonicalTrace(const int *edgeNodes, const int nEdgeNodes,
                                                                   const int *triNodes, const int nTriNodes)
{
  SANS_ASSERT(nEdgeNodes == 2);
  SANS_ASSERT(nTriNodes == 3);

  int canonicalLine[2];
  int canonicalEdge = getCanonicalTraceLeft(edgeNodes, nEdgeNodes, triNodes, nTriNodes, canonicalLine, 2).trace;

  // Find the positive or negative orientation where the nodes on the triangle match the canonical triangle
  if ( edgeNodes[0] == canonicalLine[OrientPos[0]] &&
       edgeNodes[1] == canonicalLine[OrientPos[1]] ) return CanonicalTraceToCell(canonicalEdge,  1);

  if ( edgeNodes[0] == canonicalLine[OrientNeg[0]] &&
       edgeNodes[1] == canonicalLine[OrientNeg[1]] ) return CanonicalTraceToCell(canonicalEdge, -1);

  printf("edge = (%d,%d), tri = (%d,%d,%d)\n",edgeNodes[0],edgeNodes[1],triNodes[0],triNodes[1],triNodes[2]);
  SANS_DEVELOPER_EXCEPTION( "Cannot find Line Triangle canonical orientation" );
  return CanonicalTraceToCell();
}

// Explicitly instantiate the class
template struct TraceToCellRefCoord<Line, TopoD2, Triangle>;

} //namespace SANS
