// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONAREA_QUAD_H
#define BASISFUNCTIONAREA_QUAD_H

#include "BasisFunctionArea_Quad_Hierarchical.h"
#include "BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunctionArea_Quad_Legendre.h"
#include "BasisFunctionArea_Quad_Bernstein.h"
#include "BasisFunctionArea_Quad_RBS.h"

#endif  // BASISFUNCTIONAREA_QUAD_H
