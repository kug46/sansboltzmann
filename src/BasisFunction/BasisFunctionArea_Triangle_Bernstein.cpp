// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionArea_Triangle_Bernstein.h"

#include <cmath>  // sqrt
#include "tools/SANSException.h"

namespace SANS
{
// Node Coordinates are in Barycentric Coordinates based on the referance triangle
/*

Triangle Q1:            Triangle Q2:         Triangle Q3:          Triangle Q4:

t
^                                                                   2
|                                                                   | \
2                       2                    2                      6   5
|`\                     |`\                  | \                    |     \
|  `\                   |  `\                5   4                  7 (14)  4
|    `\                 4    `3              |     \                |         \
|      `\               |      `\            6  (9)  3              8 (12) (13) 3
|        `\             |        `\          |         \            |             \
0----------1 --> s      0-----5----1         0---7---8---1          0---9--10--11---1

*/


//---------------------------------------------------------------------------//

// Bernstein: Linear

void
BasisFunctionArea<Triangle,Bernstein,1>::evalBasis( const Real& s, const Real& t, const Int3&, Real phi[], int nphi) const
{
  SANS_ASSERT(nphi == 3);

  Real b1;
  Real b2;
  Real b3;

  b1 = -s + (1 - t);
  b2 = s;
  b3 = 1 - b1 - b2;

  phi[0] = b1;
  phi[1] = b2;
  phi[2] = b3;
}

void
BasisFunctionArea<Triangle,Bernstein,1>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phis[0] = -1;
  phis[1] = 1;
  phis[2] = 0;

  phit[0] = -1;
  phit[1] = 0;
  phit[2] = 1;
}

void
BasisFunctionArea<Triangle,Bernstein,1>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 0;

  phist[0] = 0;
  phist[1] = 0;
  phist[2] = 0;

  phitt[0] = 0;
  phitt[1] = 0;
  phitt[2] = 0;
}

//---------------------------------------------------------------------------//

// Bernstein: Quadratic

void
BasisFunctionArea<Triangle,Bernstein,2>::evalBasis( const Real& s, const Real& t, const Int3&, Real phi[], int nphi)  const
{
  SANS_ASSERT(nphi == 6);

  Real b1;
  Real b2;
  Real b3;

  b1 = -s + (1 - t);
  b2 = s;
  b3 = 1 - b1 - b2;

  phi[0] = b1*b1;
  phi[1] = b2*b2;
  phi[2] = b3*b3;
  phi[3] = 2*b2*b3;
  phi[4] = 2*b1*b3;
  phi[5] = 2*b1*b2;
}

void
BasisFunctionArea<Triangle,Bernstein,2>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  Real b1;
  Real b2;
  Real b3;

  b1 = -s + (1 - t);
  b2 = s;
  b3 = 1 - b1 - b2;

  phis[0] = -2*b1;
  phis[1] = 2*b2;
  phis[2] = 0;
  phis[3] = 2*b3;
  phis[4] = -2*b3;
  phis[5] = 2*(-b2+b1);

  phit[0] = -2*b1;
  phit[1] = 0;
  phit[2] = 2*b3;
  phit[3] = 2*b2;
  phit[4] = 2*(-b3+b1);
  phit[5] = -2*b2;
}

void
BasisFunctionArea<Triangle,Bernstein,2>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 0;
  phiss[3] = 0;
  phiss[4] = 0;
  phiss[5] = 0;

  phist[0] = 0;
  phist[1] = 0;
  phist[2] = 0;
  phist[3] = 0;
  phist[4] = 0;
  phist[5] = 0;

  phitt[0] = 0;
  phitt[1] = 0;
  phitt[2] = 0;
  phitt[3] = 0;
  phitt[4] = 0;
  phitt[5] = 0;
}

//---------------------------------------------------------------------------//

// Bernstein: Cubic

void
BasisFunctionArea<Triangle,Bernstein,3>::evalBasis( const Real& s, const Real& t, const Int3&, Real phi[], int nphi) const
{
  SANS_ASSERT(nphi == 10);

  Real b1;
  Real b2;
  Real b3;

  b1 = -s + (1 - t);
  b2 = s;
  b3 = 1 - b1 - b2;

  phi[0] = b1*b1*b1;
  phi[1] = b2*b2*b2;
  phi[2] = b3*b3*b3;
  phi[3] = 3*b2*b2*b3;
  phi[4] = 3*b2*b3*b3;
  phi[5] = 3*b3*b3*b1;
  phi[6] = 3*b1*b1*b3;
  phi[7] = 3*b1*b1*b2;
  phi[8] = 3*b1*b2*b2;
  phi[9] = 6*b1*b2*b3;
}

void
BasisFunctionArea<Triangle,Bernstein,3>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  phis[0] = -3*(1-s-t)*(1-s-t);
  phis[1] = 3*s*s;
  phis[2] = 0;
  phis[3] = 6*s*t;
  phis[4] = 3*t*t;
  phis[5] = -3*t*t;
  phis[6] = -6*(1-s-t)*t;
  phis[7] = -6*s*(1-s-t)+3*(1-s-t)*(1-s-t);
  phis[8] = -3*s*s+6*s*(1-s-t);
  phis[9] = -6*s*t+6*(1-s-t)*t;

  phit[0] = -3*(1-s-t)*(1-s-t);
  phit[1] = 0;
  phit[2] = 3*t*t;
  phit[3] = 3*s*s;
  phit[4] = 6*s*t;
  phit[5] = 6*(1-s-t)*t-3*t*t;
  phit[6] = 3*(1-s-t)*(1-s-t)-6*(1-s-t)*t;
  phit[7] = -6*s*(1-s-t);
  phit[8] = -3*s*s;
  phit[9] = 6*s*(1-s-t)-6*s*t;
}

void
BasisFunctionArea<Triangle,Bernstein,3>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 0;
  phiss[3] = 0;
  phiss[4] = 0;
  phiss[5] = 0;

  phist[0] = 0;
  phist[1] = 0;
  phist[2] = 0;
  phist[3] = 0;
  phist[4] = 0;
  phist[5] = 0;

  phitt[0] = 0;
  phitt[1] = 0;
  phitt[2] = 0;
  phitt[3] = 0;
  phitt[4] = 0;
  phitt[5] = 0;

}

//---------------------------------------------------------------------------//

// Bernstein: Quintic

void
BasisFunctionArea<Triangle,Bernstein,4>::evalBasis( const Real& s, const Real& t, const Int3&, Real phi[], int nphi) const
{
  SANS_ASSERT(nphi == 15);

  Real b1;
  Real b2;
  Real b3;

  b1 = -s + (1 - t);
  b2 = s;
  b3 = 1 - b1 - b2;

  phi[0]  = b1*b1*b1*b1;
  phi[1]  = b2*b2*b2*b2;
  phi[2]  = b3*b3*b3*b3;
  phi[3]  = 4*b2*b2*b2*b3;
  phi[4]  = 6*b2*b2*b3*b3;
  phi[5]  = 4*b3*b3*b3*b2;
  phi[6]  = 4*b3*b3*b3*b1;
  phi[7]  = 6*b3*b3*b1*b1;
  phi[8]  = 4*b1*b1*b1*b3;
  phi[9]  = 4*b1*b1*b1*b2;
  phi[10] = 6*b1*b1*b2*b2;
  phi[11] = 4*b1*b2*b2*b2;
  phi[12] = 12*b1*b1*b2*b3;
  phi[13] = 12*b1*b2*b2*b3;
  phi[14] = 12*b1*b2*b3*b3;

}

void
BasisFunctionArea<Triangle,Bernstein,4>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 15);

  phis[0]  = -4*(1-s-t)*(1-s-t)*(1-s-t);
  phis[1]  = 4*s*s*s;
  phis[2]  = 0;
  phis[3]  = 12*s*s*t;
  phis[4]  = 12*s*t*t;
  phis[5]  = 4*t*t*t;
  phis[6]  = -4*t*t*t;
  phis[7]  = -12*(1-s-t)*t*t;
  phis[8]  = -12*(1-s-t)*(1-s-t)*t;
  phis[9]  = -12*s*(1-s-t)*(1-s-t)+4*(1-s-t)*(1-s-t)*(1-s-t) ;
  phis[10] = -12*s*s*(1-s-t)+12*s*(1-s-t)*(1-s-t);
  phis[11] = -4*s*s*s+12*s*s*(1-s-t);
  phis[12] = -24*s*(1-s-t)*t+12*(1-s-t)*(1-s-t)*t;
  phis[13] = -12*s*s*t+24*s*(1-s-t)*t;
  phis[14] = -12*s*t*t+12*(1-s-t)*t*t;

  phit[0]  = -4*(1-s-t)*(1-s-t)*(1-s-t);
  phit[1]  = 0;
  phit[2]  = 4*t*t*t;
  phit[3]  = 4*s*s*s;
  phit[4]  = 12*s*s*t;
  phit[5]  = 12*s*t*t;
  phit[6]  = 12*(1-s-t)*t*t-4*t*t*t;
  phit[7]  = 12*(1-s-t)*(1-s-t)*t-12*(1-s-t)*t*t;
  phit[8]  = 4*(1-s-t)*(1-s-t)*(1-s-t)-12*(1-s-t)*(1-s-t)*t;
  phit[9]  = -12*s*(1-s-t)*(1-s-t);
  phit[10] = -12*s*s*(1-s-t);
  phit[11] = -4*s*s*s;
  phit[12] = 12*s*(1-s-t)*(1-s-t)-24*s*(1-s-t)*t;
  phit[13] = 12*s*s*(1-s-t)-12*s*s*t;
  phit[14] = 24*s*(1-s-t)*t-12*s*t*t;
}

void
BasisFunctionArea<Triangle,Bernstein,4>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 15);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 0;
  phiss[3] = 0;
  phiss[4] = 0;
  phiss[5] = 0;

  phist[0] = 0;
  phist[1] = 0;
  phist[2] = 0;
  phist[3] = 0;
  phist[4] = 0;
  phist[5] = 0;

  phitt[0] = 0;
  phitt[1] = 0;
  phitt[2] = 0;
  phitt[3] = 0;
  phitt[4] = 0;
  phitt[5] = 0;
}

}
