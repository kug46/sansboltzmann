// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONAREA_TRIANGLE_H
#define BASISFUNCTIONAREA_TRIANGLE_H

#include "BasisFunctionArea_Triangle_Hierarchical.h"
#include "BasisFunctionArea_Triangle_Legendre.h"
#include "BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunctionArea_Triangle_Bernstein.h"
#include "BasisFunctionArea_Triangle_RBS.h"

#endif  // BASISFUNCTIONAREA_TRIANGLE_H
