// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTION_TRACEORIENTATION_H
#define BASISFUNCTION_TRACEORIENTATION_H

// Transformation of reference coordinates from reference element to a possibly rotated/flipped reference element

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

template <class TopoDim, class Topology>
class BasisFunction_TraceOrientation;


template <>
class BasisFunction_TraceOrientation<TopoD0, Node>
{
public:
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType;

  static void
  transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new)
  {
    Refcoord_new = Refcoord_ref; //Cannot to flip/rotate a node, so nothing to transform
  }
};

template <>
class BasisFunction_TraceOrientation<TopoD1, Line>
{
public:
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType;

  static void
  transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new);
};

template <>
class BasisFunction_TraceOrientation<TopoD2, Triangle>
{
public:
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  static void
  transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new);
};

template <>
class BasisFunction_TraceOrientation<TopoD2, Quad>
{
public:
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;

  static void
  transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new);
};

template <>
class BasisFunction_TraceOrientation<TopoD3, Tet>
{
public:
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  static void
  transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new);
};

template <>
class BasisFunction_TraceOrientation<TopoD3, Hex>
{
public:
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;

  static void
  transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new);
};

template<>
class BasisFunction_TraceOrientation<TopoD4, Pentatope>
{
public:
  typedef DLA::VectorS<TopoD4::D,Real> RefCoordType;

  static void
  transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new);
};

}

#endif  // BASISFUNCTION_TRACEORIENTATION_H
