// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QUADRATURE_CACHE_H
#define QUADRATURE_CACHE_H

#include "BasisFunctionCategory.h"

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "CanonicalTraceToCell.h"
#include "tools/minmax.h"

#include "Topology/ElementTopology.h"

#include <array>
#include <map>
#include <vector>

namespace SANS
{

enum QuadratureRule
{
  eGauss,
  eNone,
};

template<class TopoDim>
struct QuadraturePoint
{
  static const int TopoD = MAX(1, TopoDim::D);

  // cppcheck-suppress noExplicitConstructor
  QuadraturePoint(const Real& ref) :
    rule(QuadratureRule::eNone), iquad(-1), orderidx(-1), ref(ref) {}

  // cppcheck-suppress noExplicitConstructor
  QuadraturePoint(const std::initializer_list<Real>& ref) :
    rule(QuadratureRule::eNone), iquad(-1), orderidx(-1), ref(ref) {}

  // cppcheck-suppress noExplicitConstructor
  QuadraturePoint(const DLA::VectorS<TopoD,Real>& ref) :
    rule(QuadratureRule::eNone), iquad(-1), orderidx(-1), ref(ref) {}

  QuadraturePoint(QuadratureRule rule, int iquad, int orderidx, const std::initializer_list<Real>& ref) :
    rule(rule), iquad(iquad), orderidx(orderidx), ref(ref) {}

  QuadraturePoint(QuadratureRule rule, int iquad, int orderidx, const DLA::VectorS<TopoD,Real>& ref) :
    rule(rule), iquad(iquad), orderidx(orderidx), ref(ref) {}

  const QuadratureRule rule;          // Cell quadrature rule
  const int iquad;                    // Cell quadrature node number
  const int orderidx;                 // Cell quadrature order index (not the actual order)
  const DLA::VectorS<TopoD,Real> ref; // Reference element coordinate

  // Temporarily needed while implementing the caching system
  //operator const DLA::VectorS<TopoD,Real>&() const { return ref; }
};

// Represents a quadrature point on a cell, that was obtained by evaluating the quadrature
// point on the trace element of the cell element
template<class TopoDim>
struct QuadratureCellTracePoint
{
  static const int TopoD = TopoDim::D;

  QuadratureCellTracePoint() :
    rule(QuadratureRule::eNone), iquad(-1), orderidx(-1), ref(-1) {}

  // cppcheck-suppress noExplicitConstructor
  QuadratureCellTracePoint(const DLA::VectorS<TopoD,Real>& ref) :
    rule(QuadratureRule::eNone), iquad(-1), orderidx(-1), ref(ref) {}

  void set(const CanonicalTraceToCell& canonicalTrace, const QuadraturePoint<typename TopoDim::TopoDTrace>& point)
  {
    rule = point.rule;
    iquad = point.iquad;
    orderidx = point.orderidx;
    this->canonicalTrace = canonicalTrace;
  }

  QuadratureRule rule;                 // Trace quadrature rule
  int iquad;                           // Trace quadrature node number
  int orderidx;                        // Trace quadrature order index (not the actual order)
  DLA::VectorS<TopoD,Real> ref;        // Reference element coordinate
  CanonicalTraceToCell canonicalTrace; // Trace of the cell

  // Temporarily needed while implementing the caching system
//  operator const DLA::VectorS<TopoD,Real>&() const { return ref; }
};

template<int TopoD>
struct QuadratureBasisPointValues
{
  std::vector<Real> phi;
  std::vector<Real> dphi[TopoD];
  std::vector<Real> d2phi[TopoD*(TopoD+1)/2];
};

template<int TopoD>
struct QuadratureBasisPointStore
{
  BasisFunctionCategory category;
  int poly_order;

  //[rule][orderidx][iquad]
  std::vector<std::vector<std::vector<QuadratureBasisPointValues<TopoD>>>> eval;
};

struct TraceOrientMap
{
  //[rule][orderidx][iquad]
  std::vector<std::vector<std::vector<int>>> canonical;
};

template<class Topology>
struct QuadratureCache
{
  typedef typename Topology::TopoDim TopoDim;
  static const int D = Topology::TopoDim::D;
  typedef std::array<int,Topology::NTrace> IntTrace;

  // special storage for hierarchical to deal with the trace orientations
  std::vector<std::map<IntTrace,QuadratureBasisPointStore<D>>> hierarchicalCell;
  std::vector<std::map<IntTrace,std::vector<QuadratureBasisPointStore<D>>>> hierarchicalTrace;

  // [poly category][poly order]
  std::vector<std::vector<QuadratureBasisPointStore<D>>> cell;
  // [poly category][poly order][trace]
  std::vector<std::vector<std::vector<QuadratureBasisPointStore<D>>>> trace;

  //[orientation]
  std::vector<TraceOrientMap> traceOrientMap;

  static void fillCellQuadrature(const QuadraturePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreCell);

  static void fillTraceQuadrature(const QuadratureCellTracePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreTrace);

  //single instance of the cache for a given topology
  static const QuadratureCache cache;
protected:
  //Singleton!
  QuadratureCache();
};

template<class TopoDim, class Topology>
struct QuadratureCacheValues;

template<>
struct QuadratureCacheValues<TopoD1, Line>
{
  static const int D = TopoD1::D;
  typedef std::array<int,Line::NTrace> IntTrace;

  static const QuadratureBasisPointValues<D>& get(const QuadratureBasisPointStore<D>* pointStoreCell,
                                                  const QuadraturePoint<TopoD1>& ref,
                                                  const IntTrace& sgn)
  {
    const std::vector<QuadratureBasisPointValues<D>>& points = pointStoreCell->eval[ref.rule][ref.orderidx];

    if ( unlikely(points.size() == 0) )
      QuadratureCache<Line>::fillCellQuadrature(ref, sgn, const_cast<QuadratureBasisPointStore<D>&>(*pointStoreCell));

    return points[ref.iquad];
  }

  static const QuadratureBasisPointValues<D>& get(const std::vector<QuadratureBasisPointStore<D>>* pointStoreTrace,
                                                  const QuadratureCellTracePoint<TopoD1>& ref,
                                                  const IntTrace& sgn)
  {
    const std::vector<QuadratureBasisPointValues<D>>& points = (*pointStoreTrace)[ref.canonicalTrace.trace].eval[ref.rule][ref.orderidx];

    if ( unlikely(points.size() == 0) )
      QuadratureCache<Line>::fillTraceQuadrature(ref, sgn, const_cast<QuadratureBasisPointStore<D>&>((*pointStoreTrace)[ref.canonicalTrace.trace]));

    return points[ref.iquad];
  }
};

template<class Topology>
struct QuadratureCacheValues<TopoD2, Topology>
{
  static const int D = TopoD2::D;
  typedef std::array<int,Topology::NTrace> IntTrace;

  static const QuadratureBasisPointValues<D>& get(const QuadratureBasisPointStore<D>* pointStoreCell,
                                                  const QuadraturePoint<TopoD2>& ref,
                                                  const IntTrace& sgn)
  {
    const std::vector<QuadratureBasisPointValues<D>>& points = pointStoreCell->eval[ref.rule][ref.orderidx];

    if ( unlikely(points.size() == 0) )
      QuadratureCache<Topology>::fillCellQuadrature(ref, sgn, const_cast<QuadratureBasisPointStore<D>&>(*pointStoreCell));

    return points[ref.iquad];
  }

  static const QuadratureBasisPointValues<D>& get(const std::vector<QuadratureBasisPointStore<D>>* pointStoreTrace,
                                                  const QuadratureCellTracePoint<TopoD2>& ref,
                                                  const IntTrace& sgn)
  {
    const std::vector<QuadratureBasisPointValues<D>>& points = (*pointStoreTrace)[ref.canonicalTrace.trace].eval[ref.rule][ref.orderidx];

    if ( unlikely(points.size() == 0) )
      QuadratureCache<Topology>::
        fillTraceQuadrature(ref, sgn, const_cast<QuadratureBasisPointStore<D>&>((*pointStoreTrace)[ref.canonicalTrace.trace]));

    // Map the quadrature point back to an orientation of 1
    int iquad = ref.iquad;
    if (ref.canonicalTrace.orientation != 1)
      iquad = QuadratureCache<Topology>::cache.traceOrientMap[0].canonical[ref.rule][ref.orderidx][iquad];

    return points[iquad];
  }
};

inline int QuadCacheVolumeTraceOrientIdx(const int orientation)
{
  // By design, any mesh can only have orientation 1, and negative orientations
  switch (orientation)
  {
  case -1:
    return 0;
  case -2:
    return 1;
  case -3:
    return 2;
  case -4:
    return 3;
  default:
    SANS_DEVELOPER_EXCEPTION("invalid orientation = %d", orientation);
  }

  // suppress compiler warnings
  return -1;
}

template<class Topology>
struct QuadratureCacheValues<TopoD3, Topology>
{
  static const int D = TopoD3::D;
  typedef std::array<int,Topology::NTrace> IntTrace;

  static const QuadratureBasisPointValues<D>& get(const QuadratureBasisPointStore<D>* pointStoreCell,
                                                  const QuadraturePoint<TopoD3>& ref,
                                                  const IntTrace& sgn)
  {
    const std::vector<QuadratureBasisPointValues<D>>& points = pointStoreCell->eval[ref.rule][ref.orderidx];

    if ( unlikely(points.size() == 0) )
      QuadratureCache<Topology>::fillCellQuadrature(ref, sgn, const_cast<QuadratureBasisPointStore<D>&>(*pointStoreCell));

    return points[ref.iquad];
  }

  static const QuadratureBasisPointValues<D>& get(const std::vector<QuadratureBasisPointStore<D>>* pointStoreTrace,
                                                  const QuadratureCellTracePoint<TopoD3>& ref,
                                                  const IntTrace& sgn)
  {
    const std::vector<QuadratureBasisPointValues<D>>& points = (*pointStoreTrace)[ref.canonicalTrace.trace].eval[ref.rule][ref.orderidx];

    if ( unlikely(points.size() == 0) )
      QuadratureCache<Topology>::
        fillTraceQuadrature(ref, sgn, const_cast<QuadratureBasisPointStore<D>&>((*pointStoreTrace)[ref.canonicalTrace.trace]));

    // Map the quadrature point back to an orientation of 1
    int iquad = ref.iquad;
    int orient = ref.canonicalTrace.orientation;
    if (orient != 1)
      iquad = QuadratureCache<Topology>::cache.traceOrientMap[QuadCacheVolumeTraceOrientIdx(orient)].canonical[ref.rule][ref.orderidx][iquad];

    return points[iquad];
  }
};

inline int QuadCacheSpacetimeTraceOrientIdx(const int orientation)
{
  // By design, any mesh can only have orientation 1, and negative orientations
  // there are 24 possible orientations for tetrehedra (bounding pentatopes)
  switch (orientation)
  {
  case -1:
    return 0;
  case -2:
    return 1;
  case -3:
    return 2;
  case -4:
    return 3;
  case -5:
    return 4;
  case -6:
    return 5;
  case -7:
    return 6;
  case -8:
    return 7;
  case -9:
    return 8;
  case -10:
    return 9;
  case -11:
    return 10;
  case -12:
    return 11;
  default:
    SANS_DEVELOPER_EXCEPTION("invalid orientation = %d", orientation);
  }

  // suppress compiler warnings
  return -1;
}


template<class Topology>
struct QuadratureCacheValues<TopoD4, Topology>
{
  static const int D = TopoD4::D;
  typedef std::array<int,Topology::NTrace> IntTrace;

  static const QuadratureBasisPointValues<D>& get(const QuadratureBasisPointStore<D>* pointStoreCell,
                                                  const QuadraturePoint<TopoD4>& ref,
                                                  const IntTrace& sgn)
  {
    const std::vector<QuadratureBasisPointValues<D>>& points = pointStoreCell->eval[ref.rule][ref.orderidx];

    if ( unlikely(points.size() == 0) )
      QuadratureCache<Topology>::fillCellQuadrature(ref, sgn, const_cast<QuadratureBasisPointStore<D>&>(*pointStoreCell));

    return points[ref.iquad];
  }

  static const QuadratureBasisPointValues<D>& get(const std::vector<QuadratureBasisPointStore<D>>* pointStoreTrace,
                                                  const QuadratureCellTracePoint<TopoD4>& ref,
                                                  const IntTrace& sgn)
  {
    const std::vector<QuadratureBasisPointValues<D>>& points = (*pointStoreTrace)[ref.canonicalTrace.trace].eval[ref.rule][ref.orderidx];

    if ( unlikely(points.size() == 0) )
      QuadratureCache<Topology>::
        fillTraceQuadrature(ref, sgn, const_cast<QuadratureBasisPointStore<D>&>((*pointStoreTrace)[ref.canonicalTrace.trace]));

    // Map the quadrature point back to an orientation of 1
    int iquad = ref.iquad;
    int orient = ref.canonicalTrace.orientation;
    if (orient != 1)
      iquad = QuadratureCache<Topology>::cache.traceOrientMap[QuadCacheSpacetimeTraceOrientIdx(orient)].canonical[ref.rule][ref.orderidx][iquad];

    return points[iquad];
  }
};

// Declare that this is explicitly instantiated
extern template struct QuadratureCache<Line>;
extern template struct QuadratureCache<Quad>;
extern template struct QuadratureCache<Triangle>;
extern template struct QuadratureCache<Tet>;
extern template struct QuadratureCache<Hex>;
extern template struct QuadratureCache<Pentatope>;

}

// Avoid instantiating these types throughout the code
extern template class std::vector<SANS::QuadratureBasisPointValues<1>>;
extern template class std::vector<SANS::QuadratureBasisPointValues<2>>;
extern template class std::vector<SANS::QuadratureBasisPointValues<3>>;
extern template class std::vector<SANS::QuadratureBasisPointValues<4>>;

extern template class std::vector<std::vector<SANS::QuadratureBasisPointValues<1>>>;
extern template class std::vector<std::vector<SANS::QuadratureBasisPointValues<2>>>;
extern template class std::vector<std::vector<SANS::QuadratureBasisPointValues<3>>>;
extern template class std::vector<std::vector<SANS::QuadratureBasisPointValues<4>>>;

extern template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointValues<1>>>>;
extern template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointValues<2>>>>;
extern template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointValues<3>>>>;
extern template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointValues<4>>>>;

extern template class std::map<std::array<int,2>,SANS::QuadratureBasisPointStore<1>>;
extern template class std::map<std::array<int,3>,SANS::QuadratureBasisPointStore<2>>;
extern template class std::map<std::array<int,4>,SANS::QuadratureBasisPointStore<2>>;
extern template class std::map<std::array<int,4>,SANS::QuadratureBasisPointStore<3>>;
extern template class std::map<std::array<int,6>,SANS::QuadratureBasisPointStore<3>>;
extern template class std::map<std::array<int,5>,SANS::QuadratureBasisPointStore<4>>;

extern template class std::vector<std::map<std::array<int,2>,SANS::QuadratureBasisPointStore<1>>>;
extern template class std::vector<std::map<std::array<int,3>,SANS::QuadratureBasisPointStore<2>>>;
extern template class std::vector<std::map<std::array<int,4>,SANS::QuadratureBasisPointStore<2>>>;
extern template class std::vector<std::map<std::array<int,4>,SANS::QuadratureBasisPointStore<3>>>;
extern template class std::vector<std::map<std::array<int,6>,SANS::QuadratureBasisPointStore<3>>>;
extern template class std::vector<std::map<std::array<int,5>,SANS::QuadratureBasisPointStore<4>>>;

extern template class std::vector<std::map<std::array<int,2>,std::vector<SANS::QuadratureBasisPointStore<1>>>>;
extern template class std::vector<std::map<std::array<int,3>,std::vector<SANS::QuadratureBasisPointStore<2>>>>;
extern template class std::vector<std::map<std::array<int,4>,std::vector<SANS::QuadratureBasisPointStore<2>>>>;
extern template class std::vector<std::map<std::array<int,4>,std::vector<SANS::QuadratureBasisPointStore<3>>>>;
extern template class std::vector<std::map<std::array<int,6>,std::vector<SANS::QuadratureBasisPointStore<3>>>>;
extern template class std::vector<std::map<std::array<int,5>,std::vector<SANS::QuadratureBasisPointStore<4>>>>;

extern template class std::vector<SANS::QuadratureBasisPointStore<1>>;
extern template class std::vector<SANS::QuadratureBasisPointStore<2>>;
extern template class std::vector<SANS::QuadratureBasisPointStore<3>>;
extern template class std::vector<SANS::QuadratureBasisPointStore<4>>;

extern template class std::vector<std::vector<SANS::QuadratureBasisPointStore<1>>>;
extern template class std::vector<std::vector<SANS::QuadratureBasisPointStore<2>>>;
extern template class std::vector<std::vector<SANS::QuadratureBasisPointStore<3>>>;
extern template class std::vector<std::vector<SANS::QuadratureBasisPointStore<4>>>;

extern template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointStore<1>>>>;
extern template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointStore<2>>>>;
extern template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointStore<3>>>>;
extern template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointStore<4>>>>;

extern template class std::vector<SANS::TraceOrientMap>;


#endif // QUADRATURE_CACHE_H
