// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef REFELEMENT_SPLIT_TYPES_H
#define REFELEMENT_SPLIT_TYPES_H

// Element split-types for local solves

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

namespace SANS
{

enum class ElementSplitType
{
  Edge,
  Isotropic,
  IsotropicFace,
  Last
};

enum class ElementSplitFlag
{
  Unsplit,
  Split,
  New
};

struct ElementSplitInfo
{

  //Default constructor is for a no-split config
  ElementSplitInfo() : split_flag(ElementSplitFlag::Unsplit), split_type(ElementSplitType::Last), edge_index(-1), subcell_index(-1) {}

  explicit ElementSplitInfo(const ElementSplitFlag split_flag)
    : split_flag(split_flag), split_type(ElementSplitType::Last), edge_index(-1), subcell_index(-1)
  {
    if (split_flag != ElementSplitFlag::Unsplit)
      SANS_DEVELOPER_EXCEPTION( "ElementSplitInfo - ElementSplitFlag should be 'Unsplit' for this constructor." );
  }

  ElementSplitInfo(const ElementSplitFlag split_flag, const int subcell_index)
    : split_flag(split_flag), split_type(ElementSplitType::Last), edge_index(-1), subcell_index(subcell_index)
  {
    // subcell index is used in Field_Local for projecting field from cells to New traces (for HDG?)
    if (split_flag != ElementSplitFlag::New)
      SANS_DEVELOPER_EXCEPTION( "ElementSplitInfo - ElementSplitFlag should be 'New' for this constructor." );
  }

  ElementSplitInfo(const ElementSplitFlag split_flag, const ElementSplitType split_type, const int edge_index, const int subcell_index)
    : split_flag(split_flag), split_type(split_type), edge_index(edge_index), subcell_index(subcell_index) {}

  void set(const ElementSplitFlag isplit_flag, const ElementSplitType isplit_type, const int iedge_index, const int isubcell_index)
  {
    split_flag = isplit_flag;
    split_type = isplit_type;
    edge_index = iedge_index;
    subcell_index = isubcell_index;
  }

  ElementSplitFlag split_flag; //Indicates if the element is an unsplit, split, or completely new element
  ElementSplitType split_type;
  int edge_index; //Index of canonical edge that was split
  int subcell_index; // (0 -> number of cells created by the split)
};


}

#endif  // REFELEMENT_SPLIT_TYPES_H
