// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONAREA_QUAD_HIERARCHICAL_H
#define BASISFUNCTIONAREA_QUAD_HIERARCHICAL_H

// triangle basis functions: Hierarchical

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionLine.h
// - duplicate functionality to BasisFunctionTriangle.h


#include <array>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionArea.h"
#include "BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// area basis functions: Hierarchical
//
// reference triangle element defined: s in [0, 1], t in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionArea_Quad_HierarchicalPMax = 1;


//----------------------------------------------------------------------------//
// Tensor product Quad

template <>
class BasisFunctionArea<Quad,Hierarchical,1> :
  public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 4; }
  virtual int nBasisNode() const { return 4; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};

#if 0
//----------------------------------------------------------------------------//
// Hierarchical: quadratic

template <>
class BasisFunctionArea<Triangle,Hierarchical,2> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 2; }
  virtual int nBasis() const { return 6; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 3; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3&, Real phis[], Real phit[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};

//----------------------------------------------------------------------------//
// Hierarchical: cubic

template <>
class BasisFunctionArea<Triangle,Hierarchical,3> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 3; }
  virtual int nBasis() const { return 10; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 6; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};

//----------------------------------------------------------------------------//
// Hierarchical: quartic

template <>
class BasisFunctionArea<Triangle,Hierarchical,4> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 4; }
  virtual int nBasis() const { return 15; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 9; }
  virtual int nBasisCell() const { return 3; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Hierarchical: quintic

template <>
class BasisFunctionArea<Triangle,Hierarchical,5> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 5; }
  virtual int nBasis() const { return 21; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 12; }
  virtual int nBasisCell() const { return 6; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Hierarchical: P6

template <>
class BasisFunctionArea<Triangle,Hierarchical,6> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 6; }
  virtual int nBasis() const { return 28; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 15; }
  virtual int nBasisCell() const { return 10; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Hierarchical: P7

template <>
class BasisFunctionArea<Triangle,Hierarchical,7> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 7; }
  virtual int nBasis() const { return 36; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 18; }
  virtual int nBasisCell() const { return 15; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3& sgn, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3& sgn, Real phis[], Real phit[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};
#endif

}

#endif  // BASISFUNCTIONAREA_QUAD_HIERARCHICAL_H
