// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction/BasisFunction_TraceOrientation.h"

#include "tools/SANSException.h"

namespace SANS
{


void
BasisFunction_TraceOrientation<TopoD1, Line>::transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new)
{
  if (orientation == 1)
  {
    Refcoord_new = Refcoord_ref;
  }
  else if (orientation == -1)
  {
    Refcoord_new[0] = 1.0 - Refcoord_ref[0];
  }
  else
    SANS_DEVELOPER_EXCEPTION("BasisFunction_TraceOrientation<TopoD1,Line> - Invalid orientation (=%d). Has to be +1 or -1.", orientation);
}

void
BasisFunction_TraceOrientation<TopoD2, Triangle>::transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new)
{
  Real s0 = Refcoord_ref[0];
  Real t0 = Refcoord_ref[1];

  switch ( abs(orientation) )
  {
    case 1:
    {
      Refcoord_new[0] = s0;
      Refcoord_new[1] = t0;
      break;
    }
    case 2:
    {
      Refcoord_new[0] = t0;
      Refcoord_new[1] = 1.0 - s0 - t0;
      break;
    }
    case 3:
    {
      Refcoord_new[0] = 1.0 - s0 - t0;
      Refcoord_new[1] = s0;
      break;
    }
    default:
      SANS_DEVELOPER_EXCEPTION("BasisFunction_TraceOrientation<TopoD1,Triangle> - Invalid orientation (=%d). "
                               "Has to be +1,+2,+3,-1,-2 or -3.", orientation);
  }

  // Swap s and t to correct the normal if needed
  if ( orientation < 0 )
    std::swap(Refcoord_new[0],Refcoord_new[1]);
}

void
BasisFunction_TraceOrientation<TopoD2, Quad>::transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new)
{
  Real s0 = Refcoord_ref[0];
  Real t0 = Refcoord_ref[1];

  switch ( abs(orientation) )
  {
    case 1:
    {
      Refcoord_new[0] = s0;
      Refcoord_new[1] = t0;
      break;
    }
    case 2:
    {
      Refcoord_new[0] = t0;
      Refcoord_new[1] = 1 - s0;
      break;
    }
    case 3:
    {
      Refcoord_new[0] = 1.0 - s0;
      Refcoord_new[1] = 1.0 - t0;
      break;
    }
    case 4:
    {
      Refcoord_new[0] = 1.0 - t0;
      Refcoord_new[1] = s0;
      break;
    }
    default:
      SANS_DEVELOPER_EXCEPTION("BasisFunction_TraceOrientation<TopoD1,Triangle> - Invalid orientation (=%d). "
                               "Has to be +1,+2,+3,+4,-1,-2,-3 or -4.", orientation);
  }

  // Swap s and t to correct the normal if needed
  if ( orientation < 0 )
    std::swap(Refcoord_new[0],Refcoord_new[1]);
}

void
BasisFunction_TraceOrientation<TopoD3, Tet>::transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new)
{
  // SANS_DEVELOPER_EXCEPTION("BasisFunction_TraceOrientation<TopoD3,Tet> - Tets are not used as traces yet, need 4D meshes.");

  Real s0= Refcoord_ref[0];
  Real t0= Refcoord_ref[1];
  Real u0= Refcoord_ref[2];

  switch (abs(orientation))
  {
    case 1:
    {
      Refcoord_new[0]= s0;
      Refcoord_new[1]= t0;
      Refcoord_new[2]= u0;
      break;
    }
    case 2:
    {
      Refcoord_new[0]= t0;
      Refcoord_new[1]= u0;
      Refcoord_new[2]= s0;
      break;
    }
    case 3:
    {
      Refcoord_new[0]= u0;
      Refcoord_new[1]= s0;
      Refcoord_new[2]= t0;
      break;
    }
    case 4:
    {
      Refcoord_new[0]= 1 - s0 -t0 - u0;
      Refcoord_new[1]= u0;
      Refcoord_new[2]= t0;
      break;
    }
    case 5:
    {
      Refcoord_new[0]= t0;
      Refcoord_new[1]= 1 - s0 - t0 - u0;
      Refcoord_new[2]= u0;
      break;
    }
    case 6:
    {
      Refcoord_new[0]= u0;
      Refcoord_new[1]= t0;
      Refcoord_new[2]= 1 - s0 - t0 - u0;
      break;
    }
    case 7:
    {
      Refcoord_new[0]= 1 - s0 - t0 - u0;
      Refcoord_new[1]= s0;
      Refcoord_new[2]= u0;
      break;
    }
    case 8:
    {
      Refcoord_new[0]= s0;
      Refcoord_new[1]= u0;
      Refcoord_new[2]= 1 - s0 - t0 - u0;
      break;
    }
    case 9:
    {
      Refcoord_new[0]= u0;
      Refcoord_new[1]= 1 - s0 - t0 - u0;
      Refcoord_new[2]= s0;
      break;
    }
    case 10:
    {
      Refcoord_new[0]= 1 - s0 - t0 - u0;
      Refcoord_new[1]= t0;
      Refcoord_new[2]= s0;
      break;
    }
    case 11:
    {
      Refcoord_new[0]= s0;
      Refcoord_new[1]= 1 - s0 - t0 - u0;
      Refcoord_new[2]= t0;
      break;
    }
    case 12:
    {
      Refcoord_new[0]= t0;
      Refcoord_new[1]= s0;
      Refcoord_new[2]= 1 - s0 - t0 - u0;
      break;
    }
    default:
      SANS_DEVELOPER_EXCEPTION("unexpected orientation %d", orientation);
  }

  // swap normal if orientation is reversed
  if (orientation < 0)
    std::swap(Refcoord_new[1], Refcoord_new[2]);

}

void
BasisFunction_TraceOrientation<TopoD3, Hex>::transform(const RefCoordType& Refcoord_ref, const int orientation, RefCoordType& Refcoord_new)
{
  SANS_DEVELOPER_EXCEPTION("BasisFunction_TraceOrientation<TopoD3,Hex> - Hexes are not used as traces yet, need 4D meshes.");
}

}
