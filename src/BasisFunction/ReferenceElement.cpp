// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ReferenceElement.h"

namespace SANS
{

const ReferenceElement<Node>::RefCoordType ReferenceElement<Node>::centerRef = {0.0};
const ReferenceElement<Line>::RefCoordType ReferenceElement<Line>::centerRef = {0.5};
const ReferenceElement<Triangle>::RefCoordType ReferenceElement<Triangle>::centerRef = {1./3., 1./3.};
const ReferenceElement<Quad>::RefCoordType ReferenceElement<Quad>::centerRef = {0.5, 0.5};
const ReferenceElement<Tet>::RefCoordType ReferenceElement<Tet>::centerRef = {0.25, 0.25, 0.25};
const ReferenceElement<Hex>::RefCoordType ReferenceElement<Hex>::centerRef = {0.5, 0.5, 0.5};
const ReferenceElement<Pentatope>::RefCoordType ReferenceElement<Pentatope>::centerRef= {1./5., 1./5., 1./5., 1./5.};

const Real ReferenceElement<Line>::lengthRef        = 1.0;
const Real ReferenceElement<Triangle>::areaRef      = 0.5;
const Real ReferenceElement<Quad>::areaRef          = 1.0;
const Real ReferenceElement<Tet>::volumeRef         = 1.0/6.0;
const Real ReferenceElement<Hex>::volumeRef         = 1.0;
const Real ReferenceElement<Pentatope>::volumeRef   = 1.0/24.0;

//===========================================================================//
bool
ReferenceElement<Node>::isInside( const RefCoordType& sRef )
{
  return (sRef[0] == 0.0);
}

bool
ReferenceElement<Node>::getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol)
{
  sRef_sol = sRef0 + dsRef;

  if (sRef_sol[0] != 0.0)
  {
    sRef_sol[0] = 0.0;
    return true; //update was modified
  }

  return false; //update was not modified
}

void ReferenceElement<Node>::constraintJacobian(const int itrace, MatrixConst& jac)
{
}

void ReferenceElement<Node>::constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res)
{
}

//===========================================================================//
bool
ReferenceElement<Line>::isInside( const RefCoordType& sRef )
{
  return (sRef[0] >= 0.0 && sRef[0] <= 1.0);
}

bool
ReferenceElement<Line>::getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol)
{
  sRef_sol = sRef0 + dsRef;

  if (sRef_sol[0] < 0.0)
  {
    sRef_sol[0] = 0.0;
    return true; //update was modified
  }
  else if (sRef_sol[0] > 1.0)
  {
    sRef_sol[0] = 1.0;
    return true; //update was modified
  }

  return false; //update was not modified
}

void ReferenceElement<Line>::constraintJacobian(const int itrace, MatrixConst& jac)
{
  switch (itrace)
  {
  case 0: // Node 0 at s = 1
    jac(1,0) = 1;
    break;
  case 1: // Node 1 at s = 0
    jac(1,0) = 1;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}

void ReferenceElement<Line>::constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res)
{
  switch (itrace)
  {
  case 0: // Node 0 at s = 1
    res[1] = sRef[0] - 1;
    break;
  case 1: // Node 1 at s = 0
    res[1] = sRef[0];
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}

//===========================================================================//
bool
ReferenceElement<Triangle>::isInside( const RefCoordType& sRef )
{
  return (sRef[0] >= 0.0 && sRef[1] >= 0.0 && (sRef[0] + sRef[1]) <= 1.0);
}

bool
ReferenceElement<Triangle>::getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol)
{
  bool modified = false;
  Real eta = 1.0;
  static const Real tol = 1e-13;

  // Require s >= 0
  if (dsRef[0] < -tol)
  {
    Real f = -sRef0[0]/dsRef[0];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  // Require t >= 0
  if (dsRef[1] < -tol)
  {
    Real f = -sRef0[1]/dsRef[1];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  // Require s + t <= 1
  Real sum_dsRef = dsRef[0] + dsRef[1];
  if (sum_dsRef > tol)
  {
    Real f = (1.0 - sRef0[0] - sRef0[1])/sum_dsRef;
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  sRef_sol = sRef0 + eta*dsRef;
  return modified;
}

void ReferenceElement<Triangle>::constraintJacobian(const int itrace, MatrixConst& jac)
{
  switch (itrace)
  {
  case 0: // Edge 0 at s + t = 1
    jac(2,0) = 1;
    jac(2,1) = 1;
    break;
  case 1: // Edge 1 at s = 0
    jac(2,0) = 1;
    break;
  case 2: // Edge 2 at t = 0
    jac(2,1) = 1;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}


void ReferenceElement<Triangle>::constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res)
{
  switch (itrace)
  {
  case 0: // Edge 0 at s + t = 1
    res[2] = sRef[0] + sRef[1] - 1;
    break;
  case 1: // Edge 1 at s = 0
    res[2] = sRef[0];
    break;
  case 2: // Edge 2 at t = 0
    res[2] = sRef[1];
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}

//===========================================================================//
bool
ReferenceElement<Quad>::isInside( const RefCoordType& sRef )
{
  return (sRef[0] >= 0.0 && sRef[0] <= 1.0 &&
          sRef[1] >= 0.0 && sRef[1] <= 1.0);
}

bool
ReferenceElement<Quad>::getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol)
{
  bool modified = false;
  Real eta = 1.0;
  static const Real tol = 1e-13;

  if (dsRef[0] < -tol) // Require s >= 0
  {
    Real f = -sRef0[0]/dsRef[0];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }
  else if (dsRef[0] > tol) // Require s <= 1
  {
    Real f = (1.0 - sRef0[0])/dsRef[0];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  if (dsRef[1] < -tol) // Require t >= 0
  {
    Real f = -sRef0[1]/dsRef[1];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }
  else if (dsRef[1] > tol) // Require t <= 1
  {
    Real f = (1.0 - sRef0[1])/dsRef[1];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  sRef_sol = sRef0 + eta*dsRef;
  return modified;
}

void ReferenceElement<Quad>::constraintJacobian(const int itrace, MatrixConst& jac)
{
  switch (itrace)
  {
  case 0: // Edge 0 at t = 0
    jac(2,1) = 1;
    break;
  case 1: // Edge 1 at s = 1
    jac(2,0) = 1;
    break;
  case 2: // Edge 2 at t = 1
    jac(2,1) = 1;
    break;
  case 3: // Edge 3 at s = 0
    jac(2,0) = 1;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}

void ReferenceElement<Quad>::constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res)
{
  switch (itrace)
  {
  case 0: // Edge 0 at t = 0
    res[2] = sRef[1];
    break;
  case 1: // Edge 1 at s = 1
    res[2] = sRef[0] - 1;
    break;
  case 2: // Edge 2 at t = 1
    res[2] = sRef[1] - 1;
    break;
  case 3: // Edge 3 at s = 0
    res[2] = sRef[0];
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}

//===========================================================================//
bool
ReferenceElement<Tet>::isInside( const RefCoordType& sRef )
{
  return (sRef[0] >= 0.0 && sRef[1] >= 0.0 && sRef[2] >= 0.0 && (sRef[0] + sRef[1] + sRef[2]) <= 1.0);
}

bool
ReferenceElement<Tet>::getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol)
{
  bool modified = false;
  Real eta = 1.0;
  static const Real tol = 1e-13;

  // Require s >= 0
  if (dsRef[0] < -tol)
  {
    Real f = -sRef0[0]/dsRef[0];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  // Require t >= 0
  if (dsRef[1] < -tol)
  {
    Real f = -sRef0[1]/dsRef[1];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  // Require u >= 0
  if (dsRef[2] < -tol)
  {
    Real f = -sRef0[2]/dsRef[2];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  // Require s + t + u <= 1
  Real sum_dsRef = dsRef[0] + dsRef[1] + dsRef[2];
  if (sum_dsRef > tol)
  {
    Real f = (1.0 - sRef0[0] - sRef0[1] - sRef0[2])/sum_dsRef;
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  sRef_sol = sRef0 + eta*dsRef;
  return modified;
}

void ReferenceElement<Tet>::constraintJacobian(const int itrace, MatrixConst& jac)
{
  switch (itrace)
  {
  case 0: // Face 0 at s + t + u = 1
    jac(3,0) = 1;
    jac(3,1) = 1;
    jac(3,2) = 1;
    break;
  case 1: // Face 1 at s = 0
    jac(3,0) = 1;
    break;
  case 2: // Face 2 at t = 0
    jac(3,1) = 1;
    break;
  case 3: // Face 3 at u = 0
    jac(3,2) = 1;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}

void ReferenceElement<Tet>::constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res)
{
  switch (itrace)
  {
  case 0: // Face 0 at s + t + u = 1
    res[3] = sRef[0] + sRef[1] + sRef[2] - 1;
    break;
  case 1: // Face 1 at s = 0
    res[3] = sRef[0];
    break;
  case 2: // Face 2 at t = 0
    res[3] = sRef[1];
    break;
  case 3: // Face 3 at u = 0
    res[3] = sRef[2];
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}

//===========================================================================//
bool
ReferenceElement<Hex>::isInside( const RefCoordType& sRef )
{
  return (sRef[0] >= 0.0 && sRef[0] <= 1.0 &&
          sRef[1] >= 0.0 && sRef[1] <= 1.0 &&
          sRef[2] >= 0.0 && sRef[2] <= 1.0 );
}

bool
ReferenceElement<Hex>::getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol)
{
  bool modified = false;
  Real eta = 1.0;
  static const Real tol = 1e-13;

  if (dsRef[0] < -tol) // Require s >= 0
  {
    Real f = -sRef0[0]/dsRef[0];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }
  else if (dsRef[0] > tol) // Require s <= 1
  {
    Real f = (1.0 - sRef0[0])/dsRef[0];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  if (dsRef[1] < -tol) // Require t >= 0
  {
    Real f = -sRef0[1]/dsRef[1];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }
  else if (dsRef[1] > tol) // Require t <= 1
  {
    Real f = (1.0 - sRef0[1])/dsRef[1];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  if (dsRef[2] < -tol) // Require u >= 0
  {
    Real f = -sRef0[2]/dsRef[2];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }
  else if (dsRef[2] > tol) // Require u <= 1
  {
    Real f = (1.0 - sRef0[2])/dsRef[2];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  sRef_sol = sRef0 + eta*dsRef;
  return modified;
}

void ReferenceElement<Hex>::constraintJacobian(const int itrace, MatrixConst& jac)
{
  switch (itrace)
  {
  case 0: // Face 0 at u = 0
    jac(3,2) = 1;
    break;
  case 1: // Face 1 at t = 0
    jac(3,1) = 1;
    break;
  case 2: // Face 2 at s = 1
    jac(3,0) = 1;
    break;
  case 3: // Face 3 at t = 1
    jac(3,1) = 1;
    break;
  case 4: // Face 4 at s = 0
    jac(3,0) = 1;
    break;
  case 5: // Face 5 at u = 1
    jac(3,2) = 1;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}

void ReferenceElement<Hex>::constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res)
{
  switch (itrace)
  {
  case 0: // Face 0 at u = 0
    res[3] = sRef[2];
    break;
  case 1: // Face 1 at t = 0
    res[3] = sRef[1];
    break;
  case 2: // Face 2 at s = 1
    res[3] = sRef[0] - 1;
    break;
  case 3: // Face 3 at t = 1
    res[3] = sRef[1] - 1;
    break;
  case 4: // Face 4 at s = 0
    res[3] = sRef[0];
    break;
  case 5: // Face 5 at u = 1
    res[3] = sRef[2] - 1;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}


//===========================================================================//
bool
ReferenceElement<Pentatope>::isInside( const RefCoordType& sRef )
{
  return (sRef[0] >= 0.0 && sRef[1] >= 0.0 && sRef[2] >= 0.0 && sRef[3] >= 0.0 && (sRef[0] + sRef[1] + sRef[2] + sRef[3]) <= 1.0);
}

bool
ReferenceElement<Pentatope>::getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol)
{
  bool modified = false;
  Real eta = 1.0;
  static const Real tol = 1e-13;

  // Require s >= 0
  if (dsRef[0] < -tol)
  {
    Real f = -sRef0[0]/dsRef[0];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  // Require t >= 0
  if (dsRef[1] < -tol)
  {
    Real f = -sRef0[1]/dsRef[1];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  // Require u >= 0
  if (dsRef[2] < -tol)
  {
    Real f = -sRef0[2]/dsRef[2];
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  // Require v >= 0
  if (dsRef[3] < -tol)
  {
    Real f= -sRef0[3]/dsRef[3];
    if (eta > f)
    {
      eta= f;
      modified= true;
    }
  }

  // Require s + t + u + v <= 1
  Real sum_dsRef = dsRef[0] + dsRef[1] + dsRef[2] + dsRef[3];
  if (sum_dsRef > tol)
  {
    Real f = (1.0 - sRef0[0] - sRef0[1] - sRef0[2] - sRef0[3])/sum_dsRef;
    if (eta > f)
    {
      eta = f;
      modified = true;
    }
  }

  sRef_sol = sRef0 + eta*dsRef;
  return modified;
}

void ReferenceElement<Pentatope>::constraintJacobian(const int itrace, MatrixConst& jac)
{
  switch (itrace)
  {
  case 0: // Face 0 at s + t + u = 1
    jac(4, 0) = 1;
    jac(4, 1) = 1;
    jac(4, 2) = 1;
    jac(4, 3) = 1;
    break;
  case 1: // Face 1 at s = 0
    jac(4, 0) = 1;
    break;
  case 2: // Face 2 at t = 0
    jac(4, 1) = 1;
    break;
  case 3: // Face 3 at u = 0
    jac(4, 2) = 1;
    break;
  case 4:
    jac(4, 3) = 1;
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}

void ReferenceElement<Pentatope>::constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res)
{
  switch (itrace)
  {
  case 0: // Face 0 at s + t + u = 1
    res[4] = sRef[0] + sRef[1] + sRef[2] + sRef[3] - 1;
    break;
  case 1: // Face 1 at s = 0
    res[4] = sRef[0];
    break;
  case 2: // Face 2 at t = 0
    res[4] = sRef[1];
    break;
  case 3: // Face 3 at u = 0
    res[4] = sRef[2];
    break;
  case 4:
    res[4] = sRef[3];
    break;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown trace");
  }
}


} //namespace SANS
