// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONLINE_BERNSTEIN_H
#define BASISFUNCTIONLINE_BERNSTEIN_H

// line basis functions
// implemented:
//   Bernstein: P1 - P7

// NOTES:
// - Implemented via abstract base class (ABC)
// - Derived classes only contain static data. Hence, they are
//   constructed to be singletons. Using templates instead of ABC
//   would lead to code bloat


#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "BasisFunctionLineBase.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// line basis functions
//
// reference line element defined: s in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edge interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinate
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionLine_BernsteinPMax = 4;

//----------------------------------------------------------------------------//
// Bernstein: constant
template<>
class BasisFunctionLine<Bernstein,0> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 0; }
  virtual int nBasis() const override { return 1; }
  virtual int nBasisNode() const override { return 0; }
  virtual int nBasisEdge() const override { return 1; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Bernstein; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};

//----------------------------------------------------------------------------//
// Bernstein: linear
template<>
class BasisFunctionLine<Bernstein,1> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 1; }
  virtual int nBasis() const override { return 2; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 0; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Bernstein; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Bernstein: quadratic
template<>
class BasisFunctionLine<Bernstein,2> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 2; }
  virtual int nBasis() const override { return 3; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 1; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Bernstein; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Bernstein: cubic
template<>
class BasisFunctionLine<Bernstein,3> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 3; }
  virtual int nBasis() const override { return 4; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 2; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Bernstein; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Bernstein: quartic
template<>
class BasisFunctionLine<Bernstein,4> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 4; }
  virtual int nBasis() const override { return 5; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 3; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Bernstein; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

protected:
  //Singleton!
  BasisFunctionLine() {}
public:
  static const BasisFunctionLine* self();
};

}

#endif  // BASISFUNCTIONLINE_BERNSTEIN_H
