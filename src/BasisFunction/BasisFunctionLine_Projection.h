// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONLINE_PROJECTION_H
#define BASISFUNCTIONLINE_PROJECTION_H

// projection of basis area functions

#include "BasisFunctionLine.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionLine_projectTo( const BasisFunctionLineBase* basisFrom, const T dofFrom[], const int nDOFFrom,
                                  const BasisFunctionLineBase* basisTo  ,       T dofTo[]  , const int nDOFTo );

}

#endif // BASISFUNCTIONLINE_PROJECTION_H
