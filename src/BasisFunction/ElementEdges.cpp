// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// reference element operators

#include <sstream> // std::stringstream

#include "ElementEdges.h"
#include "tools/SANSException.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// The edge node map is defined in TraceToCellRefCoord_*.cpp
//
// template<class CellTopology>
// const int ElementEdges<CellTopology>::EdgeNodes
//
//
//  See TraceToCellRefCoord cpp files for Edge numbering data
//
//----------------------------------------------------------------------------//

template<class CellTopology>
int
ElementEdges<CellTopology>::getCanonicalEdge( const int *edgeNodes, const int nEdgeNodes,
                                           const int *cellNodes, const int nCellNodes)
{
  SANS_ASSERT(nEdgeNodes == Line::NNode);
  SANS_ASSERT(nCellNodes == CellTopology::NNode);

  for (int i = 0; i < CellTopology::NEdge; i++)
  {
    // inspecting the cell Node pairs as taken from the list above
    if ( ( cellNodes[EdgeNodes[i][0]] == edgeNodes[0] && cellNodes[EdgeNodes[i][1]] == edgeNodes[1] ) ||
         ( cellNodes[EdgeNodes[i][0]] == edgeNodes[1] && cellNodes[EdgeNodes[i][1]] == edgeNodes[0] ) )
    {
      // this is the edge
      return i;
    }
  }

  std::stringstream msg;
  msg << "Cannot find canonical edge." << std::endl;

  msg << "Cell nodes = { ";
  for (int n = 0; n < nCellNodes -1; n++)
    msg << cellNodes[n] << ", ";
  msg << cellNodes[nCellNodes-1] << " } " << std::endl;

  msg << "Edge nodes = { ";
  for (int n = 0; n < nEdgeNodes-1; n++)
    msg << edgeNodes[n] << ", ";
  msg << edgeNodes[nEdgeNodes-1] << " } " << std::endl;

  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return -1;
}


//===========================================================================//
void
elementEdges(const TopologyTypes topo, const int (*&EdgeNodes)[Line::NNode], int& nEdge)
{
  switch (topo)
  {
  case eLine:
    EdgeNodes = ElementEdges<Line>::EdgeNodes;
    nEdge = Line::NEdge;
    return;
  case eTriangle:
    EdgeNodes = ElementEdges<Triangle>::EdgeNodes;
    nEdge = Triangle::NEdge;
    return;
  case eQuad:
    EdgeNodes = ElementEdges<Quad>::EdgeNodes;
    nEdge = Quad::NEdge;
    return;
  case eTet:
    EdgeNodes = ElementEdges<Tet>::EdgeNodes;
    nEdge = Tet::NEdge;
    return;
  case eHex:
    EdgeNodes = ElementEdges<Hex>::EdgeNodes;
    nEdge = Hex::NEdge;
    return;
  case ePentatope:
    EdgeNodes = ElementEdges<Pentatope>::EdgeNodes;
    nEdge = Pentatope::NEdge;
    return;
  default:
    break;
  }

  SANS_DEVELOPER_EXCEPTION("Unknown topology : %d", topo);
}


// Explicitly instantiate the class
template struct ElementEdges<Line>;
template struct ElementEdges<Triangle>;
template struct ElementEdges<Quad>;
template struct ElementEdges<Tet>;
template struct ElementEdges<Hex>;
template struct ElementEdges<Pentatope>;

} //namespace SANS
