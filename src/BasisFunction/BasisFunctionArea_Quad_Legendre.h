// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONAREA_QUAD_LEGENDRE_H
#define BASISFUNCTIONAREA_QUAD_LEGENDRE_H

// quad basis functions: Legendre

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionLine.h


#include <array>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionArea.h"
#include "BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// area basis functions: Legendre
//
// reference triangle element defined: s in [0, 1], t in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionArea_Quad_LegendrePMax = 4;


//----------------------------------------------------------------------------//
// Legendre: constant

template <>
class BasisFunctionArea<Quad,Legendre,0> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 0; }
  virtual int nBasis() const { return 1; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Legendre: linear
//
// orthonormal (within factor of 2)
// basis functions normalized to give L2 norm equal quad area (1)

template <>
class BasisFunctionArea<Quad,Legendre,1> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 4; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 4; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};



//----------------------------------------------------------------------------//
// Legendre: quadratic
//
// orthonormal (within factor)

template <>
class BasisFunctionArea<Quad,Legendre,2> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 2; }
  virtual int nBasis() const { return 9; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 9; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Legendre: cubic
//
// orthonormal (within factor)

template <>
class BasisFunctionArea<Quad,Legendre,3> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 3; }
  virtual int nBasis() const { return 16; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 16; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Legendre: quartic
//
// orthonormal (within factor)

template <>
class BasisFunctionArea<Quad,Legendre,4> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 4; }
  virtual int nBasis() const { return 25; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 25; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};

#if 0
//----------------------------------------------------------------------------//
// Legendre: quintic
//
// orthonormal (within factor of 2)
// basis functions normalized to give L2 norm equal triangle area (1/2)

template <>
class BasisFunctionArea<Quad,Legendre,5> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 5; }
  virtual int nBasis() const { return 21; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 21; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3&, Real phis[], Real phit[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Legendre: P6
//
// orthonormal (within factor of 2)
// basis functions normalized to give L2 norm equal triangle area (1/2)

template <>
class BasisFunctionArea<Quad,Legendre,6> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 6; }
  virtual int nBasis() const { return 28; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 28; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3&, Real phis[], Real phit[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};


//----------------------------------------------------------------------------//
// Legendre: P7
//
// orthonormal (within factor of 2)
// basis functions normalized to give L2 norm equal triangle area (1/2)

template <>
class BasisFunctionArea<Quad,Legendre,7> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 7; }
  virtual int nBasis() const { return 36; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 36; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Legendre; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int3&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3&, Real phis[], Real phit[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};
#endif

}

#endif  // BASISFUNCTIONAREA_TRIANGLE_LEGENDRE_H
