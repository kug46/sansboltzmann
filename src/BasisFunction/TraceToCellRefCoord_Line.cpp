// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// reference element operators

#include <algorithm> // std::find

#include "ElementEdges.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "TraceToCellRefCoord.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Quadrature_Cache.h"

namespace SANS
{

// Each canonical trace is numbered based on the opposing node number

//----------------------------------------------------------------------------//
// cell reference coordinates (s) given canonical node number
/*
    0 ----- 1
*/
// parameters:
//  canonicalNode      node on line (N0=1, N1=0)


const int TraceToCellRefCoord<Node, TopoD1, Line>::TraceNodes[ Line::NNode ][ 1 ] = { {1}, {0} };

template<>
const int ElementEdges<Line>::EdgeNodes[ Line::NEdge ][ Line::NNode ] = { {0,1} };

void
TraceToCellRefCoord<Node, TopoD1, Line>::
eval( const CanonicalTraceToCell& canonicalNode, const DLA::VectorS<1,Real>& sRefTrace, DLA::VectorS<1,Real>& sRefCell )
{
  eval(canonicalNode, sRefCell[0]);
}

void
TraceToCellRefCoord<Node, TopoD1, Line>::
eval( const CanonicalTraceToCell& canonicalNode, const QuadraturePoint<TopoD0>& sRefTrace, QuadratureCellTracePoint<TopoD1>& sRefCell )
{
  sRefCell.set(canonicalNode, sRefTrace);
  eval(canonicalNode, sRefCell.ref[0]);
}

void
TraceToCellRefCoord<Node, TopoD1, Line>::eval( const CanonicalTraceToCell& canonicalNode, Real& sRefLine )
{
  switch (canonicalNode.trace)
  {
  case 0:
  {
    sRefLine = 1;
    break;
  }
  case 1:
  {
    sRefLine = 0;
    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION( "Unexpected canonicalNode.trace = %d", canonicalNode.trace );
  }
}

CanonicalTraceToCell
TraceToCellRefCoord<Node, TopoD1, Line>::getCanonicalTraceLeft( const int *nodeNodes, const int nNodeNodes,
                                                                const int *lineNodes, const int nLineNodes,
                                                                int *canonicalNode,   const int ncanonicalNode )
{
  SANS_ASSERT( nNodeNodes == 1 );
  SANS_ASSERT( nLineNodes == 2 );
  SANS_ASSERT( ncanonicalNode == 1 );

  // Find the node that is not part of the line to define the canonical node
  int canonicalPoint = -1;
  for (int pt = 0; pt < Line::NNode; pt++)
  {
    if ( std::find(nodeNodes, nodeNodes+nNodeNodes, lineNodes[pt] ) == nodeNodes+nNodeNodes )
    {
      canonicalPoint = pt;
      break;
    }
  }

  SANS_ASSERT( canonicalPoint != -1 );

  // Get the canonical node node
  for ( int n = 0; n < 1; n++)
    canonicalNode[n] = lineNodes[TraceNodes[canonicalPoint][n] ];

//  SANS_DEVELOPER_EXCEPTION("getCanonicalTraceLeft not implemented for Lines");
  return CanonicalTraceToCell(canonicalPoint,1);
}


CanonicalTraceToCell
TraceToCellRefCoord<Node, TopoD1, Line>::getCanonicalTrace(const int *nodeNodes, const int nNodeNodes,
                                                           const int *lineNodes, const int nLineNodes)
{
  SANS_ASSERT( nNodeNodes == 1 );
  SANS_ASSERT( nLineNodes == 2 );

  int canonicalPoint[1];
  int canonicalNode = getCanonicalTraceLeft( nodeNodes, nNodeNodes, lineNodes, nLineNodes, canonicalPoint,1 ).trace;

  // canonical traces are always positively oriented in 1D
  return CanonicalTraceToCell(canonicalNode,0);

//  SANS_DEVELOPER_EXCEPTION("getCanonicalTrace not implemented for Lines");
}



} //namespace SANS
