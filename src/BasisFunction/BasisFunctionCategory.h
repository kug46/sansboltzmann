// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONCATEGORY_H
#define BASISFUNCTIONCATEGORY_H

namespace SANS
{

//Basis function polynomials
class Hierarchical;
class Legendre;
class Lagrange;
class Bernstein;
class RBS;

// basis function category
enum BasisFunctionCategory
{
  BasisFunctionCategory_Legendre,
  BasisFunctionCategory_Lagrange,
  BasisFunctionCategory_Bernstein,
  BasisFunctionCategory_RBS,
  BasisFunctionCategory_Hierarchical, // Hierarchical should be last so the QuadratureCache can automatically cache the rest
  BasisFunctionCategory_None
};

template<class BasisPolynomial>
struct BasisFunctionPolynomial;

template<>
struct BasisFunctionPolynomial<Hierarchical>
{
  static BasisFunctionCategory category() { return BasisFunctionCategory_Hierarchical; }
};

template<>
struct BasisFunctionPolynomial<Legendre>
{
  static BasisFunctionCategory category() { return BasisFunctionCategory_Legendre; }
};

template<>
struct BasisFunctionPolynomial<Lagrange>
{
  static BasisFunctionCategory category() { return BasisFunctionCategory_Lagrange; }
};

template<>
struct BasisFunctionPolynomial<Bernstein>
{
  static BasisFunctionCategory category() { return BasisFunctionCategory_Bernstein; }
};

template<>
struct BasisFunctionPolynomial<RBS>
{
  static BasisFunctionCategory category() { return BasisFunctionCategory_RBS; }
};

}

#endif  // BASISFUNCTIONCATEGORY_H
