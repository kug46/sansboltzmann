// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// reference element operators

#include <algorithm> // std::find
#include <sstream>

#include "ElementEdges.h"
#include "ElementFrame.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "TraceToCellRefCoord.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Quadrature_Cache.h"

namespace SANS
{

// Each canonical trace is numbered based on the opposing node number.
// These are designed such that if the "missing" vertex is added to either the beginning or the end
// of the trace, then this creates a pentatope with a positive volume.
// For example, for the {1,2,3,4} trace, a pentatope with positive volume is created
// with both {0,1,2,3,4} and {1,2,3,4,0}
// see avro test: sandbox_xfield_TraceToCellRefCoord_ut
template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::
    TraceNodes[ CellTopology::NFace ][ TraceTopology::NNode ] =
    { {1,2,4,3}, {0,2,3,4}, {0,1,4,3}, {0,1,2,4}, {0,1,3,2} };

template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::
    TraceEdges[ CellTopology::NFace ][ TraceTopology::NEdge ] = { {4,6,5,8,7,9}, {1,2,3,7,8,9}, {0,3,2,6,5,9}, {0,1,3,4,6,8} , {0,2,1,5,4,7} };

template<>
const int ElementEdges<Pentatope>::EdgeNodes[ Pentatope::NEdge ][ Line::NNode ] =
  { {0,1} , {0,2} , {0,3} , {0,4} , {1,2} , {1,3} , {1,4} , {2,3} , {2,4} , {3,4} };
  //  0       1       2       3       4       5       6       7       8       9

template<>
const int ElementFrame<Pentatope>::FrameNodes[ Pentatope::NFrame ][ Triangle::NNode ] =
  { {0,1,2} , {0,1,3} , {0,1,4} , {0,2,3} , {0,2,4} , {0,3,4} , {1,2,3} , {1,2,4} , {1,3,4} , {2,3,4} };

// there are a total of 24 permutations of 0-1-2-3 (12 positive, 12 negative)
template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::
    OrientPos[TraceTopology::NPermutation][ TraceTopology::NNode ] =
  { {0,1,2,3} , {0,2,3,1} , {0,3,1,2} , {1,0,3,2}, {1,2,0,3} , {1,3,2,0} , {2,0,1,3} , {2,1,3,0} , {2,3,0,1} , {3,0,2,1} , {3,1,0,2} , {3,2,1,0} };

template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::
    OrientNeg[TraceTopology::NPermutation][ TraceTopology::NNode ] =
    { {0,1,3,2} , {0,2,1,3} , {0,3,2,1} , {1,0,2,3}, {1,2,3,0} , {1,3,0,2} , {2,0,3,1} , {2,1,0,3} , {2,3,1,0} , {3,0,1,2} , {3,1,2,0} , {3,2,0,1} };

template<class TraceTopology, class CellTopology>
void
TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::
eval( const CanonicalTraceToCell& canonicalFace, const DLA::VectorS<3,Real>& sRefTrace, DLA::VectorS<4,Real>& sRefCell )
{
  eval(canonicalFace, sRefTrace[0], sRefTrace[1], sRefTrace[2], sRefCell[0], sRefCell[1], sRefCell[2] , sRefCell[3] );
}

template<class TraceTopology, class CellTopology>
void
TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::
eval( const CanonicalTraceToCell& canonicalFace, const QuadraturePoint<TopoD3>& sRefTrace, QuadratureCellTracePoint<TopoD4>& sRefCell )
{
  sRefCell.set(canonicalFace, sRefTrace);
  eval(canonicalFace, sRefTrace.ref[0], sRefTrace.ref[1], sRefTrace.ref[2] , sRefCell.ref[0], sRefCell.ref[1], sRefCell.ref[2] , sRefCell.ref[3] );
}

void
mapTetTraceCoordsToCanonical( const Real& s0 , const Real& t0 , const Real& u0,
                              const int orientation , Real& s, Real& t, Real& u )
{
  // see accompanying matlab script which runs through all the
  // permutations of 1-2-3-4 (there are 24)
  // these are written out in the order they appear in the script
  // TODO use std::next_permutation along with the math
  switch (orientation)
  {
    case 1: // 0-1-2-3
    {
      s = s0;
      t = t0;
      u = u0;
      break;
    }
    case -1: // 0-1-3-2
    {
      s = s0;
      t = u0;
      u = t0;
      break;
    }
    case -2: // 0-2-1-3
    {
      s = t0;
      t = s0;
      u = u0;
      break;
    }
    case 2: // 0-2-3-1
    {
      s = t0;
      t = u0;
      u = s0;
      break;
    }
    case 3: // 0-3-1-2
    {
      s = u0;
      t = s0;
      u = t0;
      break;
    }
    case -3: // 0-3-2-1
    {
      s = u0;
      t = t0;
      u = s0;
      break;
    }
    case -4: // 1-0-2-3
    {
      s = 1 -s0 -t0 -u0;
      t = t0;
      u = u0;
      break;
    }
    case 4: // 1-0-3-2
    {
      s = 1 -s0 -t0 -u0;
      t = u0;
      u = t0;
      break;
    }
    case 5: // 1-2-0-3
    {
      s = t0;
      t = 1 -s0 -t0 -u0;
      u = u0;
      break;
    }
    case -5: // 1-2-3-0
    {
      s = t0;
      t = u0;
      u = 1 -s0 -t0 -u0;
      break;
    }
    case -6: // 1-3-0-2
    {
      s = u0;
      t = 1 -s0 -t0 -u0;
      u = t0;
      break;
    }
    case 6: // 1-3-2-0
    {
      s = u0;
      t = t0;
      u = 1 -s0 -t0 -u0;
      break;
    }
    case 7: // 2-0-1-3
    {
      s = 1 -s0 -t0 -u0;
      t = s0;
      u = u0;
      break;
    }
    case -7: // 2-0-3-1
    {
      s = 1 -s0 -t0 -u0;
      t = u0;
      u = s0;
      break;
    }
    case -8: // 2-1-0-3
    {
      s = s0;
      t = 1 -s0 -t0 -u0;
      u = u0;
      break;
    }
    case 8: // 2-1-3-0
    {
      s = s0;
      t = u0;
      u = 1 -s0 -t0 -u0;
      break;
    }
    case 9: // 2-3-0-1
    {
      s = u0;
      t = 1 -s0 -t0 -u0;
      u = s0;
      break;
    }
    case -9: // 2-3-1-0
    {
      s = u0;
      t = s0;
      u = 1 -s0 -t0 -u0;
      break;
    }
    case -10: // 3-0-1-2
    {
      s = 1 -s0 -t0 -u0;
      t = s0;
      u = t0;
      break;
    }
    case 10: // 3-0-2-1
    {
      s = 1 -s0 -t0 -u0;
      t = t0;
      u = s0;
      break;
    }
    case 11: // 3-1-0-2
    {
      s = s0;
      t = 1 -s0 -t0 -u0;
      u = t0;
      break;
    }
    case -11: // 3-1-2-0
    {
      s = s0;
      t = t0;
      u = 1 -s0 -t0 -u0;
      break;
    }
    case -12: // 3-2-0-1
    {
      s = t0;
      t = 1 -s0 -t0 -u0;
      u = s0;
      break;
    }
    case 12: // 3-2-1-0
    {
      s = t0;
      t = s0;
      u = 1 -s0 -t0 -u0;
      break;
    }
    default:
      SANS_DEVELOPER_EXCEPTION("Unexpected orientation %d", orientation);
  }
}

template<class TraceTopology, class CellTopology>
void
TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::eval( const CanonicalTraceToCell& canonicalFace,
                                                                const Real& sRefVol, const Real& tRefVol, const Real& uRefVol,
                                                                Real& sRefSpacetime, Real& tRefSpacetime, Real& uRefSpacetime , Real& vRefSpacetime )
{
  Real s, t, u;
  Real s0 = sRefVol;
  Real t0 = tRefVol;
  Real u0 = uRefVol;

  // map the volume (tet) coordinates to their canonical representation
  // based on the permutation decided by the orientation (sign and value)
  mapTetTraceCoordsToCanonical( s0, t0, u0, canonicalFace.orientation, s, t, u);

  // the faces are properly oriented (see test in avro TraceToCellRefCoord_ut)
  // the intuition behind this is that the coordinate axes are defined starting
  // from the first vertex, then connecting to the second, third, etc.
  // i.e. facet (tet) 0-2-4-3 has edges (0,2), (0,4) and (0,3) defining
  // the coordinate axes as they are in the canonical trace (tet)
  // therefore, (0,2) means that t gets the first reference coordinate (s)
  // then (0,4) means the v gets the second (t) and (0,3) means u gets the
  // third reference coordinate (u).
  switch (canonicalFace.trace)
  {
  case 0:      // 1-2-4-3, 1+s+t+u = 0 plane
  {
    sRefSpacetime = 1 - s - t - u;
    tRefSpacetime = s;
    uRefSpacetime = u;
    vRefSpacetime = t;
    break;
  }
  case 1:      // 0-2-3-4, s = 0 plane
  {
    sRefSpacetime = 0;
    tRefSpacetime = s;
    uRefSpacetime = t;
    vRefSpacetime = u;
    break;
  }
  case 2:      // 0-1-4-3, t = 0 plane
  {
    sRefSpacetime = s;
    tRefSpacetime = 0;
    uRefSpacetime = u;
    vRefSpacetime = t;
    break;
  }
  case 3:      // 0-1-2-4, u = 0 plane
  {
    sRefSpacetime = s;
    tRefSpacetime = t;
    uRefSpacetime = 0;
    vRefSpacetime = u;
    break;
  }
  case 4:     // 0-1-3-2, v = 0 plane
  {
    sRefSpacetime = s;
    tRefSpacetime = u;
    uRefSpacetime = t;
    vRefSpacetime = 0;
    break;

  }
  default:
    SANS_DEVELOPER_EXCEPTION( "Unexpected canonicalFace.trace = %d", canonicalFace.trace );
  }

}

template<class TraceTopology, class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::getCanonicalTraceLeft(const int *tetNodes, const int nTetNodes,
                                                                                const int *pentatopeNodes, const int nPentatopeNodes,
                                                                                int *canonicalTet, const int ncanonicalTet)
{
  SANS_ASSERT(nTetNodes == 4);
  SANS_ASSERT(nPentatopeNodes == 5);
  SANS_ASSERT(ncanonicalTet == 4);

  // Find the node that is not part of the tetrahedron to define the canonical face
  int canonicalFace = -1;
  for ( int face = 0; face < Pentatope::NFace; face++ )
  {
    if ( std::find(tetNodes, tetNodes+nTetNodes, pentatopeNodes[face] ) == tetNodes+nTetNodes )
    {
      canonicalFace = face;
      break;
    }
  }

  SANS_ASSERT( canonicalFace != -1 );

  // Get the canonical tet nodes
  for ( int n = 0; n < Tet::NNode; n++)
    canonicalTet[n] = pentatopeNodes[TraceNodes[canonicalFace][n]];

  return CanonicalTraceToCell(canonicalFace, 1);
}

template<class TraceTopology, class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::getCanonicalTrace(const int *tetNodes, const int nTetNodes,
                                                                            const int *pentatopeNodes, const int nPentatopeNodes)
{
  SANS_ASSERT(nTetNodes == 4);
  SANS_ASSERT(nPentatopeNodes == 5);

  int canonicalTet[4];
  int canonicalFace = getCanonicalTraceLeft(tetNodes, nTetNodes, pentatopeNodes, nPentatopeNodes, canonicalTet, 4).trace;

  // Find the positive or negative orientation where the nodes on the tet match the canonical tet
  const int nPerm = 12;
  for ( int orient = 0; orient < nPerm; orient++)
  {
    if ( tetNodes[0] == canonicalTet[OrientPos[orient][0]] &&
         tetNodes[1] == canonicalTet[OrientPos[orient][1]] &&
         tetNodes[2] == canonicalTet[OrientPos[orient][2]] &&
         tetNodes[3] == canonicalTet[OrientPos[orient][3]]) return CanonicalTraceToCell(canonicalFace, orient+1);

    if ( tetNodes[0] == canonicalTet[OrientNeg[orient][0]] &&
         tetNodes[1] == canonicalTet[OrientNeg[orient][1]] &&
         tetNodes[2] == canonicalTet[OrientNeg[orient][2]] &&
         tetNodes[3] == canonicalTet[OrientNeg[orient][3]] ) return CanonicalTraceToCell(canonicalFace, -(orient+1));
  }

  std::stringstream msg;
  msg << "Cannot find Pentatope Tet canonical orientation." << std::endl;

  msg << "Tet nodes = { ";
  for (int n = 0; n < Tet::NNode-1; n++)
    msg << tetNodes[n] << ", ";
  msg << tetNodes[Tet::NNode-1] << " } " << std::endl;

  msg << "Canonical Trace = " << canonicalFace << std::endl;
  msg << "Canonical Trace nodes = { ";
  for (int n = 0; n < Tet::NNode-1; n++)
    msg << tetNodes[TraceNodes[canonicalFace][n]] << ", ";
  msg << tetNodes[TraceNodes[canonicalFace][Tet::NNode-1]] << " } " << std::endl;

  msg << "Pentatope nodes = { ";
  for (int n = 0; n < Pentatope::NNode-1; n++)
    msg << tetNodes[n] << ", ";
  msg << tetNodes[Pentatope::NNode-1] << " } " << std::endl;


  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return CanonicalTraceToCell();
}

template<class TraceTopology>
int
CanonicalOrientation<TraceTopology, TopoD4>::get(const int *tetNodes, const int nTetNodes,
                                                 const int *canonicalTetNodes, const int nCanonicalTetNodes)
{
  SANS_ASSERT(nTetNodes == 4);
  SANS_ASSERT(nCanonicalTetNodes == 4);

  const int (*OrientPos)[ TraceTopology::NTrace ] = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::OrientPos;
  const int (*OrientNeg)[ TraceTopology::NTrace ] = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::OrientNeg;

  // Find the positive or negative orientation where the nodes on the tet match the canonical tet
  const int nPerm = 12;
  for ( int orient = 0; orient < nPerm; orient++)
  {
    if ( tetNodes[0] == canonicalTetNodes[OrientPos[orient][0]] &&
         tetNodes[1] == canonicalTetNodes[OrientPos[orient][1]] &&
         tetNodes[2] == canonicalTetNodes[OrientPos[orient][2]] &&
         tetNodes[3] == canonicalTetNodes[OrientPos[orient][3]] ) return orient+1;

    if ( tetNodes[0] == canonicalTetNodes[OrientNeg[orient][0]] &&
         tetNodes[1] == canonicalTetNodes[OrientNeg[orient][1]] &&
         tetNodes[2] == canonicalTetNodes[OrientNeg[orient][2]] &&
         tetNodes[3] == canonicalTetNodes[OrientNeg[orient][3]] ) return -(orient+1);
  }

  std::stringstream msg;
  msg << "Cannot find Tet canonical orientation." << std::endl;

  msg << "Tet nodes = { ";
  for (int n = 0; n < Tet::NNode-1; n++)
    msg << tetNodes[n] << ", ";
  msg << tetNodes[Tet::NNode-1] << " } " << std::endl;

  msg << "Canonical Tet nodes = { ";
  for (int n = 0; n < Tet::NNode-1; n++)
    msg << canonicalTetNodes[n] << ", ";
  msg << canonicalTetNodes[Tet::NNode-1] << " } " << std::endl;

  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return 0;
}

template<class TraceTopology, class CellTopology>
int
TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>::getCellEdge(const CanonicalTraceToCell& canonicalTrace,
                                                                      const int canonicalFrame)
{
  int face   = canonicalTrace.trace;
  int orient = canonicalTrace.orientation;

  SANS_DEVELOPER_EXCEPTION( "implement" );
  // likely %3 need to be %4 and orientation needs to go up to 4?

  if (orient==1)
    return TraceEdges[face][canonicalFrame];
  else if (orient==2)
    return TraceEdges[face][(canonicalFrame+1)%3];
  else if (orient==3)
    return TraceEdges[face][(canonicalFrame+2)%3];
  else if (orient== -1)
    return TraceEdges[face][(3-canonicalFrame)%3];
  else if (orient== -2)
    return TraceEdges[face][(4-canonicalFrame)%3];
  else if (orient== -3)
    return TraceEdges[face][(5-canonicalFrame)%3];

  std::stringstream msg;
  msg << "Cannot find Pentatope edge for given Tet orientation." << std::endl;
  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return -1;
}

// Explicitly instantiate the class
template struct TraceToCellRefCoord<Tet, TopoD4, Pentatope>;
template struct CanonicalOrientation<Tet, TopoD4>;

} //namespace SANS
