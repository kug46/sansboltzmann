// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LAGRANGEDOFMAP_H
#define LAGRANGEDOFMAP_H

#include "Topology/ElementTopology.h"
#include "TraceToCellRefCoord.h"

#include <vector>

namespace SANS
{

//---------------------------------------------------------------------------//
// Provides the means to extract different components from
// a SANS Lagrange DOF map

template<class Topology>
struct LagrangeDOFMap;

/*
 Canonical node - This is really just a dummy
 */
template<>
struct LagrangeDOFMap<Node>
{
  static int nBasis( const int order ) { return 1; }

  static std::vector<int>
  getCanonicalNodes()
  {
    return {0};
  }


};



// Canonical nodes DOFs are indicated with 'n'

/*
    Line Q1:                Line Q2:           Line Q3:

 n0---------n1 --> s    n0-----2----n1     n0----2----3---n1

 */

template<>
struct LagrangeDOFMap<Line>
{
  static int nBasis(const int order) { return order+1; }

  static std::vector<int>
  getCanonicalNodes()
  {
    return {0, 1};
  }

  static std::vector< std::vector<int> >
  getTracesCanonicalNodes()
  {
    // The traces are numbered opposing to the node (i.e. see Triangle)
    return {{1}, {0}};
  }

  static std::vector<TopologyTypes>
  getTracesTopo()
  {
    return {eNode, eNode};
  }

  static std::vector<int>
  getCanonicalTraceMap( const int inode, const int order )
  {
    std::vector<int> trace(1);
         if (inode==0) trace[0] = 1; // canonical trace is always the opposing of the node number (i.e. see Triangle)
    else if (inode==1) trace[0] = 0;
    else SANS_DEVELOPER_EXCEPTION("Unknown node = %d", inode);
    return trace;
  }

  // returns dofs that are in the interior of a cell
  static std::vector<int>
  getCellMap(const int order)
  {
    if (order == 1)
    {
      return {};
    }
    else
    {
      std::vector<int> cell(order-1);

      for (int i = 0; i <= order-2; i++)
        cell[i] = i+2; // ignore 0,1 then start numbering

      return cell;
    }
  }

  // returns the P1 nodal functions with non-zero support at the node
  // i.e. if in an edge, returns the end points, in a a face
  static std::vector<int>
  getNodalSupport( const int dof, const int order)
  {
    if (dof == 0)
      return {0}; // left
    else if (dof == 1)
      return {1}; // right
    else
      return {0,1}; // interior
  }
};


/*

  Triangle Q1:            Triangle Q2:         Triangle Q3:          Triangle Q4:

  t
  ^                                                                  n2
  |                                                                   | \
 n2                      n2                   n2                      6   5
  |`\                     |`\                  | \                    |     \
  |  `\                   |  `\                5   4                  7 (14)  4
  |    `\                 4    `3              |     \                |         \
  |      `\               |      `\            6  (9)  3              8 (12) (13) 3
  |        `\             |        `\          |         \            |             \
 n0---------n1 --> s     n0-----5---n1        n0---7---8--n1         n0---9--10--11--n1

*/

template<>
struct LagrangeDOFMap<Triangle>
{
  static int nBasis(const int order) { return (order+1)*(order+2)/2; }

  static std::vector<int>
  getCanonicalNodes()
  {
    return {0, 1, 2};
  }

  static std::vector< std::vector<int> >
  getTracesCanonicalNodes()
  {
    const int (*TraceNodes)[ Line::NNode ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

    std::vector< std::vector<int> > traces;
    for (int iedge = 0; iedge < Triangle::NEdge; iedge++)
      traces.push_back( {TraceNodes[iedge][0], TraceNodes[iedge][1]} );

    return traces;
  }

  static std::vector<TopologyTypes>
  getTracesTopo()
  {
    return {eLine, eLine, eLine};
  }

  static std::vector<int>
  getCanonicalTraceMap( const int iedge, const int order )
  {
    SANS_ASSERT_MSG( iedge >= 0 && iedge < Triangle::NEdge, "iedge = %d", iedge);
    const int (*TraceNodes)[ Line::NNode ] = TraceToCellRefCoord<Line, TopoD2, Triangle>::TraceNodes;

    std::vector<int> trace(order+1);

    // Node DOFs
    trace[0] = TraceNodes[iedge][0];
    trace[1] = TraceNodes[iedge][1];

    // Edge DOFs
    if (iedge == 0)
    {
      for (int i = 1; i < order; i++) trace[i+1] = 2 + i;
    }
    else if (iedge == 1)
    {
      for (int i = 1; i < order; i++) trace[i+1] = 2 + (order-1) + i;
    }
    else if (iedge == 2)
    {
      for (int i = 1; i < order; i++) trace[i+1] = 2 + 2*(order-1) + i;
    }

    return trace;
  }

  static std::vector<int>
  getCellMap(const int order)
  {
    if (order <= 2)
    {
      return {};
    }
    else
    {
      std::vector<int> cell( (order-2)*(order-1)/2 );

      const int offset =  3*order;

      for (int i = 0; i < (order-2)*(order-1)/2; i++)
        cell[i] = i + offset;

      return cell;
    }
  }


  // returns the P1 nodal functions with non-zero support at the node
  // i.e. if a canonical node, returns the node
  // if an edge, returns the two end nodes
  // if
  static std::vector<int>
  getNodalSupport( const int dof, const int order)
  {
#if 1
    SANS_DEVELOPER_EXCEPTION("not tested...");
    return {};
#else
    // canonical nodes
    switch (dof)
    {
    case 0: return {0}; break;
    case 1: return {1}; break;
    case 2: return {2}; break;
    }

    // edge dofs
    const int dof_per_edge = order -1; // the number of dof interior to an edge
    if ( ( dof <= 2 +   dof_per_edge) && ( dof > 2 ) )
      return {0,1};

    if ( ( dof <= 2 + 2*dof_per_edge) && ( dof > 2 +   dof_per_edge ) )
      return {1,2};

    if ( ( dof <= 2 + 3*dof_per_edge) && ( dof > 2 + 2*dof_per_edge ) )
      return {0,2};

    // cell dofs
    if ( dof >=  3*order) // interior
      return {0,1,2};

    SANS_DEVELOPER_EXCEPTION("Should not be able to get here...");
    return {}; // shouldn't reach here
#endif
  }
};


/*

  Quad Q1:                  Quad Q2:             Quad Q3:               Quad Q4:

  t
  ^
  |
 n3---------n2           n3----5----n2
  |          |            |          |
  |          |            |          |
  |          |            6          4
  |          |            |          |
  |          |            |          |
 n0---------n1 --> s     n0----7----n1

*/

template<>
struct LagrangeDOFMap<Quad>
{
  static int nBasis(const int order) { return (order+1)*(order+1); }

  static std::vector<int>
  getCanonicalNodes()
  {
    return {0, 1, 2, 3};
  }

  static std::vector< std::vector<int> >
  getTracesCanonicalNodes()
  {
    const int (*TraceNodes)[ Line::NNode ] = TraceToCellRefCoord<Line, TopoD2, Quad>::TraceNodes;

    std::vector< std::vector<int> > traces;
    for (int iedge = 0; iedge < Quad::NEdge; iedge++)
      traces.push_back( {TraceNodes[iedge][0], TraceNodes[iedge][1]} );

    return traces;
  }

  static std::vector<TopologyTypes>
  getTracesTopo()
  {
    return {eLine, eLine, eLine, eLine};
  }

  static std::vector<int>
  getCanonicalTraceMap( const int iedge, const int order )
  {
    SANS_ASSERT_MSG( iedge >= 0 && iedge < Quad::NEdge, "iedge = %d", iedge);
    const int (*TraceNodes)[ Line::NNode ] = TraceToCellRefCoord<Line, TopoD2, Quad>::TraceNodes;

    std::vector<int> trace(order+1);

    // Node DOFs
    trace[0] = TraceNodes[iedge][0];
    trace[1] = TraceNodes[iedge][1];

    // Edge DOFs
    if (iedge == 0)
    {
      for (int i = 1; i < order; i++) trace[i+1] = 3 + i;
    }
    else if (iedge == 1)
    {
      for (int i = 1; i < order; i++) trace[i+1] = 3 + (order-1) + i;
    }
    else if (iedge == 2)
    {
      for (int i = 1; i < order; i++) trace[i+1] = 3 + 2*(order-1) + i;
    }
    else if (iedge == 3)
    {
      for (int i = 1; i < order; i++) trace[i+1] = 3 + 3*(order-1) + i;
    }

    return trace;
  }


  static std::vector<int>
  getCellMap(const int order)
  {
    SANS_DEVELOPER_EXCEPTION("cell dof numbering for quads not implemented yet");
//    if (order <= 2)
//    {
//      return {};
//    }
//    else
//    {
//      std::vector<int> cell( (order-2)*(order-1)/2 );
//
//      const int offset =  3*order - 1;
//
//      for (int i = 0; i < (order-2)*(order-1)/2; i++)
//        cell[i] = i + offset;
//
//      return cell;
//    }
    return {};
  }



  // returns the P1 nodal functions with non-zero support at the node
  // i.e. if a canonical node, returns the node
  // if an edge, returns the two end nodes
  // if
  static std::vector<int>
  getNodalSupport( const int dof, const int order)
  {
#if 1
    SANS_DEVELOPER_EXCEPTION("not tested...");
    return {};
#else
    // canonical nodes
    switch (dof)
    {
    case 0: return {0}; break;
    case 1: return {1}; break;
    case 2: return {2}; break;
    case 3: return {3}; break;
    }

    // edge dofs
    const int dof_per_edge = order -1; // the number of dof interior to an edge
    if ( ( dof <= 2 +   dof_per_edge) && ( dof > 2 ) )
      return {1,2};

    if ( ( dof <= 2 + 2*dof_per_edge) && ( dof > 2 +   dof_per_edge ) )
      return {2,3};

    if ( ( dof <= 2 + 3*dof_per_edge) && ( dof > 2 + 2*dof_per_edge ) )
      return {3,0};

    if ( ( dof <= 2 + 4*dof_per_edge) && ( dof > 2 + 3*dof_per_edge ) )
      return {0,1};

    // cell dofs
    if ( dof >  2 + 4*dof_per_edge) // interior
      return {0,1,2,3};

    SANS_DEVELOPER_EXCEPTION("Should not be able to get here...");
    return {}; // shouldn't reach here
#endif
  }
};


/*
Tetrahedron                          Tetrahedron10                   Tetrahedron20                     Tetrahedron20

                   t
                 .
               ,/
              /
           2                                  2                             2                                 2
         ,/|`\                              ,/|`\                         ,/|`\                             ,/|`\
       ,/  |  `\                          ,/  |  `\                     10  |  `9                         ,13 |  `12
     ,/    '.   `\                      ,7    '.   `6                 ,/    '.   `\                     ,14   '4   `11
   ,/       |     `\                  ,/       4     `\             11       4     `8                 ,15      |     `10
 ,/         |       `\              ,/         |       `\         ,/         |       `\             ,/         5       `\
0-----------'.--------1 --> s      0--------9--'.--------1       0-------14--'.--15----1           0---19---20-'.---21---1
 `\.         |      ,/              `\.         |      ,/         `\.         |      ,/             `\.         |      ,9
    `\.      |    ,/                   `\.      |    ,5              `12      5    ,7                  `16      6    ,8
       `\.   '. ,/                        `8.   '. ,/                   `13   '. ,6                       `17   '. ,7
          `\. |/                             `\. |/                        `\. |/                            `18 |/
             `3                                 `3                            `3                                `3
                `\.
                   ` u                                         F0=16, F1=17, F2=18, F3=19     F0=22,23,24 F1=25,26,27 F2=28,29,30, F3=31,32,33, C0=34
*/


template<>
struct LagrangeDOFMap<Tet>
{
  static int nBasis(const int order) { return (order+1)*(order+2)*(order+3)/6; }

  static std::vector<int>
  getCanonicalNodes()
  {
    return {0, 1, 2, 3};
  }

  static std::vector< std::vector<int> >
  getTracesCanonicalNodes()
  {
    std::vector< std::vector<int> > traces(Tet::NFace);
    for (std::size_t iface = 0; iface < Tet::NFace; iface++)
      traces[iface] = getCanonicalTraceMap( iface, 1 );

    return traces;
  }

  static std::vector<TopologyTypes>
  getTracesTopo()
  {
    return {eTriangle, eTriangle, eTriangle, eTriangle};
  }

  static std::vector<int>
  getCanonicalTraceMap( const int iface, const int order )
  {
    SANS_ASSERT_MSG( iface >= 0 && iface < Tet::NFace, "iface = %d", iface);
    const int (*TraceNodes)[ Triangle::NNode ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::TraceNodes;

    std::vector<int> trace(LagrangeDOFMap<Triangle>::nBasis(order));
    for (std::size_t n = 0; n < Triangle::NNode; n++)
      trace[n] = TraceNodes[iface][n];

    if ( order == 1 )
    {
      // done
    }
    else if ( order == 2 )
    {
      // Edge DOFs
      if (iface == 0)
      {
        trace[3] = 4;
        trace[4] = 5;
        trace[5] = 6;
      }
      else if (iface == 1)
      {
        trace[3] = 4;
        trace[4] = 7;
        trace[5] = 8;
      }
      else if (iface == 2)
      {
        trace[3] = 5;
        trace[4] = 8;
        trace[5] = 9;
      }
      else if (iface == 3)
      {
        trace[3] = 6;
        trace[4] = 9;
        trace[5] = 7;
      }
    }
    else if ( order == 3 )
    {
      // Edge DOFs
      if (iface == 0)
      {
        trace[3] = 4;
        trace[4] = 5;
        trace[5] = 6;
        trace[6] = 7;
        trace[7] = 8;
        trace[8] = 9;
        trace[9] = 16;
      }
      else if (iface == 1)
      {
        trace[3] = 5;
        trace[4] = 4;
        trace[5] = 10;
        trace[6] = 11;
        trace[7] = 12;
        trace[8] = 13;
        trace[9] = 17;
      }
      else if (iface == 2)
      {
        trace[3] = 7;
        trace[4] = 6;
        trace[5] = 13;
        trace[6] = 12;
        trace[7] = 14;
        trace[8] = 15;
        trace[9] = 18;
      }
      else if (iface == 3)
      {
        trace[3] = 9;
        trace[4] = 8;
        trace[5] = 15;
        trace[6] = 14;
        trace[7] = 11;
        trace[8] = 10;
        trace[9] = 19;
      }
    }
    else if ( order == 4 )
    {
      // Edge DOFs
      if (iface == 0)
      {
        trace[ 3] = 4;
        trace[ 4] = 5;
        trace[ 5] = 6;
        trace[ 6] = 7;
        trace[ 7] = 8;
        trace[ 8] = 9;
        trace[ 9] = 10;
        trace[10] = 11;
        trace[11] = 12;
        trace[12] = 22;
        trace[13] = 23;
        trace[14] = 24;
      }
      else if (iface == 1)
      {
        trace[ 3] = 6;
        trace[ 4] = 5;
        trace[ 5] = 4;
        trace[ 6] = 13;
        trace[ 7] = 14;
        trace[ 8] = 15;
        trace[ 9] = 16;
        trace[10] = 17;
        trace[11] = 18;
        trace[12] = 25;
        trace[13] = 26;
        trace[14] = 27;
      }
      else if (iface == 2)
      {
        trace[ 3] = 9;
        trace[ 4] = 8;
        trace[ 5] = 7;
        trace[ 6] = 18;
        trace[ 7] = 17;
        trace[ 8] = 16;
        trace[ 9] = 19;
        trace[10] = 20;
        trace[11] = 21;
        trace[12] = 28;
        trace[13] = 29;
        trace[14] = 30;
      }
      else if (iface == 3)
      {
        trace[ 3] = 12;
        trace[ 4] = 11;
        trace[ 5] = 10;
        trace[ 6] = 21;
        trace[ 7] = 20;
        trace[ 8] = 19;
        trace[ 9] = 15;
        trace[10] = 14;
        trace[11] = 13;
        trace[12] = 31;
        trace[13] = 32;
        trace[14] = 33;
      }
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown order = %d", order);

    return trace;
  }


  static std::vector<int>
  getCellMap(const int order)
  {
    if (order <= 3)
    {
      return {};
    }
    else
    {
      const int nDOF = (order-3)*(order-2)*(order-1)/6 ;
      std::vector<int> cell( nDOF );

      const int offset =  2*(order*order + 1); // simplification from extracting inner tet

      for (int i = 0; i < nDOF; i++)
        cell[i] = i + offset;

      return cell;
    }
  }

  // returns the P1 nodal functions with non-zero support at the node
  // i.e. if a canonical node, returns the node
  // if an edge, returns the two end nodes, face the three nodes, interior the 4.
  static std::vector<int>
  getNodalSupport( const int dof, const int order)
  {
#if 1
    SANS_DEVELOPER_EXCEPTION("not tested...");
    return {};
#else
    // canonical nodes
    switch (dof)
    {
    case 0: return {0}; break;
    case 1: return {1}; break;
    case 2: return {2}; break;
    case 3: return {3}; break;
    }

    // edge dofs
    int offset = 3;
    const int dof_per_edge = order -1; // the number of dof interior to an edge

    if ( ( dof <= offset +   dof_per_edge ) && ( dof > offset                  ) )
      return {2,3};

    if ( ( dof <= offset + 2*dof_per_edge ) && ( dof > offset +   dof_per_edge ) )
      return {1,3};

    if ( ( dof <= offset + 3*dof_per_edge ) && ( dof > offset + 2*dof_per_edge ) )
      return {1,2};

    if ( ( dof <= offset + 4*dof_per_edge ) && ( dof > offset + 3*dof_per_edge ) )
      return {0,2};

    if ( ( dof <= offset + 5*dof_per_edge ) && ( dof > offset + 3*dof_per_edge ) )
      return {0,3};

    if ( ( dof <= offset + 6*dof_per_edge ) && ( dof > offset + 5*dof_per_edge ) )
      return {0,1};

    // face DOFS
    offset += 6*dof_per_edge; // go past the edge dofs
    const int dof_per_face = (order-2)*(order-1)/2; // 1 for P3, 3 for P4, 6 for P5 etc. It's a triangle

    if ( ( dof <= offset +   dof_per_face ) && ( dof > offset                  ) ) // 0 face
      return {1,2,3};

    if ( ( dof <= offset + 2*dof_per_face ) && ( dof > offset +   dof_per_face ) ) // 1 face
      return {0,2,3};

    if ( ( dof <= offset + 3*dof_per_face ) && ( dof > offset + 2*dof_per_face ) ) // 2 face
      return {0,1,3};

    if ( ( dof <= offset + 4*dof_per_face ) && ( dof > offset + 3*dof_per_face ) ) // 3 face
      return {0,1,2};

    // cell dofs
    if ( dof >=  2*(order*order + 1)) // interior tet of dofs
      return {0,1,2,3};

    SANS_DEVELOPER_EXCEPTION("Should not be able to get here...");
    return {}; // shouldn't reach here
#endif
  }

};

/*
  TODO: Add high-order DOF's

           t
    3----------2
    |\     ^   |\
    | \    |   | \
    |  \   |   |  \
    |   7------+---6
    |   |  +-- |-- | -> s
    0---+---\--1   |
     \  |    \  \  |
      \ |     \  \ |
       \|      u  \|
        4----------5
*/

template<>
struct LagrangeDOFMap<Hex>
{
  static int nBasis(const int order) { return (order+1)*(order+1)*(order+1); }

  static std::vector<int>
  getCanonicalNodes()
  {
    return {0, 1, 2, 3, 4, 5, 6, 7};
  }

  static std::vector< std::vector<int> >
  getTracesCanonicalNodes()
  {
    std::vector< std::vector<int> > traces(Hex::NFace);
    for (std::size_t iface = 0; iface < Hex::NFace; iface++)
      traces[iface] = getCanonicalTraceMap( iface, 1 );

    return traces;
  }

  static std::vector<TopologyTypes>
  getTracesTopo()
  {
    return {eQuad, eQuad, eQuad, eQuad, eQuad, eQuad};
  }

  static std::vector<int>
  getCanonicalTraceMap( const int iface, const int order )
  {
    SANS_ASSERT_MSG( iface >= 0 && iface < Hex::NFace, "iface = %d", iface);
    const int (*TraceNodes)[ Quad::NNode ] = TraceToCellRefCoord<Quad, TopoD3, Hex>::TraceNodes;

    std::vector<int> trace(LagrangeDOFMap<Quad>::nBasis(order));
    for (std::size_t n = 0; n < Quad::NNode; n++)
      trace[n] = TraceNodes[iface][n];

    if ( order == 1 )
    {
      // done
    }
    else if ( order == 2 )
    {
      if (iface == 0)
      {
        trace[4] = 8;
        trace[5] = 9;
        trace[6] = 10;
        trace[7] = 11;
        trace[8] = 20;
      }
      else if (iface == 1)
      {
        trace[4] = 11;
        trace[5] = 12;
        trace[6] = 13;
        trace[7] = 14;
        trace[8] = 21;
      }
      else if (iface == 2)
      {
        trace[4] = 10;
        trace[5] = 15;
        trace[6] = 16;
        trace[7] = 12;
        trace[8] = 22;
      }
      else if (iface == 3)
      {
        trace[4] = 18;
        trace[5] = 19;
        trace[6] = 15;
        trace[7] = 9;
        trace[8] = 23;
      }
      else if (iface == 4)
      {
        trace[4] = 14;
        trace[5] = 17;
        trace[6] = 18;
        trace[7] = 8;
        trace[8] = 24;
      }
      else if (iface == 5)
      {
        trace[4] = 13;
        trace[5] = 16;
        trace[6] = 19;
        trace[7] = 17;
        trace[8] = 25;
      }
    }
    else if ( order == 3 )
    {
      if (iface == 0)
      {
        trace[4] = 8;
        trace[5] = 9;

        trace[6] = 10;
        trace[7] = 11;

        trace[8] = 12;
        trace[9] = 13;

        trace[10] = 14;
        trace[11] = 15;
        //Face
        trace[12] = 32;
        trace[13] = 33;
        trace[14] = 34;
        trace[15] = 35;
      }
      else if (iface == 1)
      {
        trace[4] = 15;
        trace[5] = 14;

        trace[6] = 16;
        trace[7] = 17;

        trace[8] = 18;
        trace[9] = 19;

        trace[10] = 20;
        trace[11] = 21;
        //Face
        trace[12] = 36;
        trace[13] = 37;
        trace[14] = 38;
        trace[15] = 39;
      }
      else if (iface == 2)
      {
        trace[4] = 13;
        trace[5] = 12;

        trace[6] = 22;
        trace[7] = 23;

        trace[8] = 24;
        trace[9] = 25;

        trace[10] = 17;
        trace[11] = 16;
        //Face
        trace[12] = 40;
        trace[13] = 41;
        trace[14] = 42;
        trace[15] = 43;
      }
      else if (iface == 3)
      {
        trace[4] = 28;
        trace[5] = 29;

        trace[6] = 30;
        trace[7] = 31;

        trace[8] = 23;
        trace[9] = 22;

        trace[10] = 11;
        trace[11] = 10;
        //Face
        trace[12] = 44;
        trace[13] = 45;
        trace[14] = 46;
        trace[15] = 47;
      }
      else if (iface == 4)
      {
        trace[4] = 21;
        trace[5] = 20;

        trace[6] = 26;
        trace[7] = 27;

        trace[8] = 29;
        trace[9] = 28;

        trace[10] = 9;
        trace[11] = 8;
        //Face
        trace[12] = 48;
        trace[13] = 49;
        trace[14] = 50;
        trace[15] = 51;
      }
      else if (iface == 5)
      {
        trace[4] = 19;
        trace[5] = 18;

        trace[6] = 25;
        trace[7] = 24;

        trace[8] = 31;
        trace[9] = 30;

        trace[10] = 27;
        trace[11] = 26;
        //Face
        trace[12] = 52;
        trace[13] = 53;
        trace[14] = 54;
        trace[15] = 55;
      }
    }
    else if ( order == 4 )
    {
      if (iface == 0)
      {
        trace[4] = 8;
        trace[5] = 9;
        trace[6] = 10;

        trace[7] = 11;
        trace[8] = 12;
        trace[9] = 13;

        trace[10] = 14;
        trace[11] = 15;
        trace[12] = 16;

        trace[13] = 17;
        trace[14] = 18;
        trace[15] = 19;

        //Face
        trace[16] = 44;
        trace[17] = 45;
        trace[18] = 46;
        trace[19] = 47;
        trace[20] = 48;
        trace[21] = 49;
        trace[22] = 50;
        trace[23] = 51;
        trace[24] = 52;

      }
      else if (iface == 1)
      {
        trace[4] = 19;
        trace[5] = 18;
        trace[6] = 17;

        trace[7] = 20;
        trace[8] = 21;
        trace[9] = 22;

        trace[10] = 23;
        trace[11] = 24;
        trace[12] = 25;

        trace[13] = 26;
        trace[14] = 27;
        trace[15] = 28;

        //Face
        trace[16] = 53;
        trace[17] = 54;
        trace[18] = 55;
        trace[19] = 56;
        trace[20] = 57;
        trace[21] = 58;
        trace[22] = 59;
        trace[23] = 60;
        trace[24] = 61;
      }
      else if (iface == 2)
      {
        trace[4] = 16;
        trace[5] = 15;
        trace[6] = 14;

        trace[7] = 29;
        trace[8] = 30;
        trace[9] = 31;

        trace[10] = 32;
        trace[11] = 33;
        trace[12] = 34;

        trace[13] = 22;
        trace[14] = 21;
        trace[15] = 20;

        //Face
        trace[16] = 62;
        trace[17] = 63;
        trace[18] = 64;
        trace[19] = 65;
        trace[20] = 66;
        trace[21] = 67;
        trace[22] = 68;
        trace[23] = 69;
        trace[24] = 70;
      }
      else if (iface == 3)
      {
        trace[4] = 38;
        trace[5] = 39;
        trace[6] = 40;

        trace[7] = 41;
        trace[8] = 42;
        trace[9] = 43;

        trace[10] = 31;
        trace[11] = 30;
        trace[12] = 29;

        trace[13] = 13;
        trace[14] = 12;
        trace[15] = 11;

        //Face
        trace[16] = 71;
        trace[17] = 72;
        trace[18] = 73;
        trace[19] = 74;
        trace[20] = 75;
        trace[21] = 76;
        trace[22] = 77;
        trace[23] = 78;
        trace[24] = 79;
      }
      else if (iface == 4)
      {
        trace[4] = 28;
        trace[5] = 27;
        trace[6] = 26;

        trace[7] = 35;
        trace[8] = 36;
        trace[9] = 37;

        trace[10] = 40;
        trace[11] = 39;
        trace[12] = 38;

        trace[13] = 10;
        trace[14] = 9;
        trace[15] = 8;

        //Face
        trace[16] = 80;
        trace[17] = 81;
        trace[18] = 82;
        trace[19] = 83;
        trace[20] = 84;
        trace[21] = 85;
        trace[22] = 86;
        trace[23] = 87;
        trace[24] = 88;
      }
      else if (iface == 5)
      {
        trace[4] = 25;
        trace[5] = 24;
        trace[6] = 23;

        trace[7] = 34;
        trace[8] = 33;
        trace[9] = 32;

        trace[10] = 43;
        trace[11] = 42;
        trace[12] = 41;

        trace[13] = 37;
        trace[14] = 36;
        trace[15] = 35;

        //Face
        trace[16] = 89;
        trace[17] = 90;
        trace[18] = 91;
        trace[19] = 92;
        trace[20] = 93;
        trace[21] = 94;
        trace[22] = 95;
        trace[23] = 96;
        trace[24] = 97;
      }
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown order = %d", order);

    return trace;
  }

  static std::vector<int>
  getCellMap(const int order)
  {
    SANS_DEVELOPER_EXCEPTION("cell dof numbering for hexs not implemented yet");
//    if (order <= 3)
//    {
//      return {};
//    }
//    else
//    {
//      const int nDOF = (order-3)*(order-2)*(order-1)/6 ;
//      std::vector<int> cell( nDOF );
//
//      const int offset =  2*(order*order - 1); // simplification from extracting inner tet
//
//      for (int i = 0; i < nDOF; i++)
//        cell[i] = i + offset;
//
//      return cell;
//    }
    return {};
  }

  // returns the P1 nodal functions with non-zero support at the node
  // i.e. if a canonical node, returns the node
  // if an edge, returns the two end nodes, face the three nodes, interior the 4.
  static std::vector<int>
  getNodalSupport( const int dof, const int order)
  {
#if 1
    SANS_DEVELOPER_EXCEPTION("not tested...");
    return {};
#else
    // canonical nodes
    switch (dof)
    {
    case 0: return {0}; break;
    case 1: return {1}; break;
    case 2: return {2}; break;
    case 3: return {3}; break;
    }

    SANS_DEVELOPER_EXCEPTION("higher order dofs for hexes not supported");

//    // edge dofs
//    int offset = 3;
//    const int dof_per_edge = order -1; // the number of dof interior to an edge
//
//    if ( ( dof <= offset +   dof_per_edge ) && ( dof > offset                  ) )
//      return {2,3};
//
//    if ( ( dof <= offset + 2*dof_per_edge ) && ( dof > offset +   dof_per_edge ) )
//      return {1,3};
//
//    if ( ( dof <= offset + 3*dof_per_edge ) && ( dof > offset + 2*dof_per_edge ) )
//      return {1,2};
//
//    if ( ( dof <= offset + 4*dof_per_edge ) && ( dof > offset + 3*dof_per_edge ) )
//      return {0,2};
//
//    if ( ( dof <= offset + 5*dof_per_edge ) && ( dof > offset + 3*dof_per_edge ) )
//      return {0,3};
//
//    if ( ( dof <= offset + 6*dof_per_edge ) && ( dof > offset + 5*dof_per_edge ) )
//      return {0,1};
//
//    // face DOFS
//    offset += 6*dof_per_edge; // go past the edge dofs
//    const int dof_per_face = (order-2)*(order-1)/2; // 1 for P3, 3 for P4, 6 for P5 etc. It's a triangle
//
//    if ( ( dof <= offset +   dof_per_face ) && ( dof > offset                  ) ) // 0 face
//      return {1,2,3};
//
//    if ( ( dof <= offset + 2*dof_per_face ) && ( dof > offset +   dof_per_face ) ) // 1 face
//      return {0,2,3};
//
//    if ( ( dof <= offset + 3*dof_per_face ) && ( dof > offset + 2*dof_per_face ) ) // 2 face
//      return {0,1,3};
//
//    if ( ( dof <= offset + 4*dof_per_face ) && ( dof > offset + 3*dof_per_face ) ) // 3 face
//      return {0,1,2};
//
//    // cell dofs
//    if ( dof >=  2*(order*order + 1)) // interior tet of dofs
//      return {0,1,2,3};

   return {}; // shouldn't reach here
#endif
  }

};

// HUGH STOP VANDALIZING MY CODE
template<>
struct LagrangeDOFMap<Pentatope>
{
  static int nBasis(const int order) { return (order+1)*(order+2)*(order+3)*(order+4)/24; }

  static std::vector<int>
  getCanonicalNodes()
  {
    return {0, 1, 2, 3, 4};
  }

  static std::vector< std::vector<int> >
  getTracesCanonicalNodes()
  {
    std::vector< std::vector<int> > traces(Pentatope::NFace);
    for (std::size_t iface = 0; iface < Pentatope::NFace; iface++)
      traces[iface] = getCanonicalTraceMap( iface, 1 );
    return traces;
  }

  static std::vector<TopologyTypes>
  getTracesTopo()
  {
    return {eTet,eTet,eTet,eTet,eTet};
  }

  static std::vector<int>
  getCanonicalTraceMap( const int iface, const int order )
  {
    SANS_ASSERT_MSG( iface >= 0 && iface < Pentatope::NFace, "iface = %d", iface);\

    const int (*TraceNodes)[ Tet::NNode ] = TraceToCellRefCoord<Tet, TopoD4, Pentatope>::TraceNodes;
    std::vector<int> trace(LagrangeDOFMap<Tet>::nBasis(order));
    for (std::size_t n = 0; n < Tet::NNode; n++)
      trace[n] = TraceNodes[iface][n];

    if ( order == 1 )
    {
      // nothing to do
    }
    else if ( order == 2 )
    {
      // internal trace DOFs
      if (iface == 0)
      {
        // u+s+t+v = 1 trace
        trace[4] = 14;
        trace[5] = 12;
        trace[6] = 13;
        trace[7] = 10;
        trace[8] = 11;
        trace[9] = 9;
      }
      else if (iface == 1)
      {
        // s = 0 trace
        trace[4] = 14;
        trace[5] = 13;
        trace[6] = 12;
        trace[7] = 8;
        trace[8] = 7;
        trace[9] = 6;
      }
      else if (iface == 2)
      {
        // t = 0 trace
        trace[4] = 14;
        trace[5] = 10;
        trace[6] = 11;
        trace[7] = 7;
        trace[8] = 8;
        trace[9] = 5;
      }
      else if (iface == 3)
      {
        // u = 0 trace
        trace[4] = 13;
        trace[5] = 11;
        trace[6] = 9;
        trace[7] = 8;
        trace[8] = 6;
        trace[9] = 5;
      }
      else if (iface == 4 )
      {
        // v = 0 trace
        trace[4] = 12;
        trace[5] = 9;
        trace[6] = 10;
        trace[7] = 6;
        trace[8] = 7;
        trace[9] = 5;
      }
    }
    else if ( order==3 )
    {
      // the dof below is correct but they are in the wrong order
      printf("error: the dof ordering below needs to be ordered consistently ");
      printf("with the ordering of the Lagrange dof for tetrahedra");
      SANS_DEVELOPER_EXCEPTION("implement");

      // internal trace DOFs
      if (iface == 0)
      {
        // u+s+t+v = 1 trace
        trace[4] = 5;
        trace[5] = 6;
        trace[6] = 7;
        trace[7] = 9;
        trace[8] = 10;
        trace[9] = 11;
        trace[10] = 13;
        trace[11] = 14;
        trace[12] = 16;
        trace[13] = 19;
        trace[14] = 20;
        trace[15] = 22;
        trace[16] = 23;
        trace[17] = 25;
        trace[18] = 28;
        trace[19] = 30;
      }
      else if (iface == 1)
      {
        // s = 0 trace
        trace[4] = 19;
        trace[5] = 20;
        trace[6] = 21;
        trace[7] = 22;
        trace[8] = 23;
        trace[9] = 24;
        trace[10] = 25;
        trace[11] = 26;
        trace[12] = 27;
        trace[13] = 28;
        trace[14] = 29;
        trace[15] = 30;
        trace[16] = 31;
        trace[17] = 32;
        trace[18] = 33;
        trace[19] = 34;
      }
      else if (iface == 2)
      {
        // t = 0 trace
        trace[4] = 6;
        trace[5] = 7;
        trace[6] = 8;
        trace[7] = 13;
        trace[8] = 14;
        trace[9] = 15;
        trace[10] = 16;
        trace[11] = 17;
        trace[12] = 18;
        trace[13] = 28;
        trace[14] = 29;
        trace[15] = 30;
        trace[16] = 31;
        trace[17] = 32;
        trace[18] = 33;
        trace[19] = 34;
      }
      else if (iface == 3)
      {
        // u = 0 trace
        trace[4] = 5;
        trace[5] = 7;
        trace[6] = 8;
        trace[7] = 9;
        trace[8] = 11;
        trace[9] = 12;
        trace[10] = 16;
        trace[11] = 17;
        trace[12] = 18;
        trace[13] = 20;
        trace[14] = 21;
        trace[15] = 25;
        trace[16] = 26;
        trace[17] = 27;
        trace[18] = 33;
        trace[19] = 34;
      }
      else if (iface == 4 )
      {
        // v = 0 trace
        trace[4] = 5;
        trace[5] = 6;
        trace[6] = 8;
        trace[7] = 9;
        trace[8] = 10;
        trace[9] = 12;
        trace[10] = 13;
        trace[11] = 15;
        trace[12] = 18;
        trace[13] = 19;
        trace[14] = 21;
        trace[15] = 22;
        trace[16] = 24;
        trace[17] = 27;
        trace[18] = 29;
        trace[19] = 32;
      }
    }
    else if ( order==4 )
    {
      // the dof below is correct but they are in the wrong order
      printf("error: the dof ordering below needs to be ordered consistently ");
      printf("with the ordering of the Lagrange dof for tetrahedra");
      SANS_DEVELOPER_EXCEPTION("implement");

      // internal trace DOFs
      if (iface == 0)
      {
        // u+s+t+v = 1 trace
        trace[4] = 5;
        trace[5] = 6;
        trace[6] = 7;
        trace[7] = 9;
        trace[8] = 10;
        trace[9] = 11;
        trace[10] = 13;
        trace[11] = 14;
        trace[12] = 16;
        trace[13] = 19;
        trace[14] = 20;
        trace[15] = 21;
        trace[16] = 23;
        trace[17] = 24;
        trace[18] = 26;
        trace[19] = 29;
        trace[20] = 30;
        trace[21] = 32;
        trace[22] = 35;
        trace[23] = 39;
        trace[24] = 40;
        trace[25] = 42;
        trace[26] = 43;
        trace[27] = 45;
        trace[28] = 48;
        trace[29] = 49;
        trace[30] = 51;
        trace[31] = 54;
        trace[32] = 58;
        trace[33] = 60;
        trace[34] = 63;
      }
      else if (iface == 1)
      {
        // s = 0 trace
        trace[4] = 39;
        trace[5] = 40;
        trace[6] = 41;
        trace[7] = 42;
        trace[8] = 43;
        trace[9] = 44;
        trace[10] = 45;
        trace[11] = 46;
        trace[12] = 47;
        trace[13] = 48;
        trace[14] = 49;
        trace[15] = 50;
        trace[16] = 51;
        trace[17] = 52;
        trace[18] = 53;
        trace[19] = 54;
        trace[20] = 55;
        trace[21] = 56;
        trace[22] = 57;
        trace[23] = 58;
        trace[24] = 59;
        trace[25] = 60;
        trace[26] = 61;
        trace[27] = 62;
        trace[28] = 63;
        trace[29] = 64;
        trace[30] = 65;
        trace[31] = 66;
        trace[32] = 67;
        trace[33] = 68;
        trace[34] = 69;
      }
      else if (iface == 2)
      {
        // t = 0 trace
        trace[4] = 6;
        trace[5] = 7;
        trace[6] = 8;
        trace[7] = 13;
        trace[8] = 14;
        trace[9] = 15;
        trace[10] = 16;
        trace[11] = 17;
        trace[12] = 18;
        trace[13] = 29;
        trace[14] = 30;
        trace[15] = 31;
        trace[16] = 32;
        trace[17] = 33;
        trace[18] = 34;
        trace[19] = 35;
        trace[20] = 36;
        trace[21] = 37;
        trace[22] = 38;
        trace[23] = 58;
        trace[24] = 59;
        trace[25] = 60;
        trace[26] = 61;
        trace[27] = 62;
        trace[28] = 63;
        trace[29] = 64;
        trace[30] = 65;
        trace[31] = 66;
        trace[32] = 67;
        trace[33] = 68;
        trace[34] = 69;
      }
      else if (iface == 3)
      {
        // u = 0 trace
        trace[4] = 5;
        trace[5] = 7;
        trace[6] = 8;
        trace[7] = 9;
        trace[8] = 11;
        trace[9] = 12;
        trace[10] = 16;
        trace[11] = 17;
        trace[12] = 18;
        trace[13] = 19;
        trace[14] = 21;
        trace[15] = 22;
        trace[16] = 26;
        trace[17] = 27;
        trace[18] = 28;
        trace[19] = 35;
        trace[20] = 36;
        trace[21] = 37;
        trace[22] = 38;
        trace[23] = 40;
        trace[24] = 41;
        trace[25] = 45;
        trace[26] = 46;
        trace[27] = 47;
        trace[28] = 54;
        trace[29] = 55;
        trace[30] = 56;
        trace[31] = 57;
        trace[32] = 67;
        trace[33] = 68;
        trace[34] = 69;
      }
      else if (iface ==4 )
      {
        // v = 0 trace
        trace[4] = 5;
        trace[5] = 6;
        trace[6] = 8;
        trace[7] = 9;
        trace[8] = 10;
        trace[9] = 12;
        trace[10] = 13;
        trace[11] = 15;
        trace[12] = 18;
        trace[13] = 19;
        trace[14] = 20;
        trace[15] = 22;
        trace[16] = 23;
        trace[17] = 25;
        trace[18] = 28;
        trace[19] = 29;
        trace[20] = 31;
        trace[21] = 34;
        trace[22] = 38;
        trace[23] = 39;
        trace[24] = 41;
        trace[25] = 42;
        trace[26] = 44;
        trace[27] = 47;
        trace[28] = 48;
        trace[29] = 50;
        trace[30] = 53;
        trace[31] = 57;
        trace[32] = 59;
        trace[33] = 62;
        trace[34] = 66;
      }
    }
    else
    {
      printf("use the function dumpTraceDOFs in ");
      printf("BasisFunction/BasisFunctionSpacetime_Pentatope_Lagrange.cpp ");
      printf("to determine the trace dof for p = %d\n",order);
      SANS_DEVELOPER_EXCEPTION("Unsupported order = %d", order);
    }
    return trace;
  }


  static std::vector<int>
  getCellMap(const int order)
  {
    SANS_DEVELOPER_EXCEPTION("implement");
  }

  // returns the P1 nodal functions with non-zero support at the node
  // i.e. if a canonical node, returns the node
  // if an edge, returns the two end nodes, triangle -> 3 nodes,
  // tet -> 4 nodes, interior return all 5 nodes
  static std::vector<int>
  getNodalSupport( const int dof, const int order)
  {
   SANS_DEVELOPER_EXCEPTION("implement");
   return {}; // shouldn't reach here

  }

}; // end pentatope

}

#endif //LAGRANGEDOFMAP_H
