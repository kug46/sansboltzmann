// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// reference element operators

#include <algorithm> // std::find
#include <sstream>   // stringstream

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "TraceToCellRefCoord.h"
#include "CanonicalTraceToCell.h"

namespace SANS
{

CanonicalTraceToCell
CellToTrace<Node,TopoD1>::
getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell )
{
  if (cellTopo == eLine)
  {
    SANS_ASSERT( trace.size() == 1 );
    if (trace[0] == cell[0] )
      return CanonicalTraceToCell(1,0);
    else
      return CanonicalTraceToCell(0,0);
  }

  SANS_DEVELOPER_EXCEPTION("Incorrect TraceTopology = Line and cell TopologyType = $d for TopoD2", cellTopo);
  return CanonicalTraceToCell();
}

CanonicalTraceToCell
CellToTrace<Line,TopoD2>::
getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell )
{
  if (cellTopo == eTriangle)
  {
    return TraceToCellRefCoord<Line,TopoD2,Triangle>::getCanonicalTrace( trace.data(), trace.size(), cell.data(), cell.size() );
  }
  else if (cellTopo == eQuad)
  {
    return TraceToCellRefCoord<Line,TopoD2,Quad>::getCanonicalTrace( trace.data(), trace.size(), cell.data(), cell.size() );
  }

  SANS_DEVELOPER_EXCEPTION("Incorrect TraceTopology = Line and cell TopologyType = $d for TopoD2", cellTopo);
  return CanonicalTraceToCell();
}


CanonicalTraceToCell
CellToTrace<Triangle,TopoD3>::
getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell )
{
  if (cellTopo == eTet)
  {
    return TraceToCellRefCoord<Triangle,TopoD3,Tet>::getCanonicalTrace( trace.data(), trace.size(), cell.data(), cell.size() );
  }

  SANS_DEVELOPER_EXCEPTION("Incorrect TraceTopology = Triangle and cell TopologyType = $d for TopoD3", cellTopo);
  return CanonicalTraceToCell();
}


CanonicalTraceToCell
CellToTrace<Quad,TopoD3>::
getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell )
{
  if (cellTopo == eHex)
  {
    return TraceToCellRefCoord<Quad,TopoD3,Hex>::getCanonicalTrace( trace.data(), trace.size(), cell.data(), cell.size() );
  }

  SANS_DEVELOPER_EXCEPTION("Incorrect TraceTopology = Quad and cell TopologyType = $d for TopoD3", cellTopo);
  return CanonicalTraceToCell();
}

CanonicalTraceToCell
CellToTrace<Tet,TopoD4>::
getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell )
{
  if (cellTopo == ePentatope)
  {
    return TraceToCellRefCoord<Tet,TopoD4,Pentatope>::getCanonicalTrace( trace.data(), trace.size(), cell.data(), cell.size() );
  }

  SANS_DEVELOPER_EXCEPTION("Incorrect TraceTopology = Tet and cell TopologyType = $d for TopoD4", cellTopo);
  return CanonicalTraceToCell();
}

}
