// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONSPACETIME_PROJECTION_H
#define BASISFUNCTIONSPACETIME_PROJECTION_H

// projection of basis spacetime functions

#include "BasisFunctionSpacetime.h"
#include "Topology/ElementTopology.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionSpacetime_projectTo( const BasisFunctionSpacetimeBase<Pentatope>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                    const BasisFunctionSpacetimeBase<Pentatope>* basisTo  ,       T dofTo[]  , const int nDOFTo );

}

#endif // BASISFUNCTIONVOLUME_PROJECTION_H
