// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionArea_Projection.h"

// projection of area basis functions

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "BasisFunctionArea.h"
#include "BasisFunctionCategory.h"
#include "BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunctionArea_Quad_Lagrange.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "Surreal/SurrealS.h"

#include "UserVariables/BoltzmannNVar.h"
namespace SANS
{

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionArea_projectTo( const BasisFunctionAreaBase<Triangle>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                  const BasisFunctionAreaBase<Triangle>* basisTo  ,       T dofTo[]  , const int nDOFTo )
{
  SANS_ASSERT( nDOFFrom == basisFrom->nBasis() );
  SANS_ASSERT( nDOFTo == basisTo->nBasis() );

  if ( basisFrom->category() == BasisFunctionCategory_Legendre &&
       basisTo->category()   == BasisFunctionCategory_Legendre )
  {
    //Copy Cell DOFs for Legendre
    int nCellTo = basisTo->nBasisCell();
    int nCell = std::min(basisFrom->nBasisCell(), nCellTo);
    for (int n = 0; n < nCell; n++)
      dofTo[n] = dofFrom[n];

    for (int n = nCell; n < nCellTo; n++)
      dofTo[n] = 0;
  }
  else if ( ( basisFrom->category() == BasisFunctionCategory_Hierarchical ||
             (basisFrom->category() == BasisFunctionCategory_Lagrange && basisFrom->order() == 1) ) &&
            basisTo->category()   == BasisFunctionCategory_Hierarchical )
  {
    //Prolongating by arbitrary increment
    if ( basisTo->order() >= basisFrom->order() )
    {
      //Zero out everything first
      for (int n = 0; n < nDOFTo; n++)
        dofTo[n] = 0;

      //Copy Nodes
      const int nNode = basisFrom->nBasisNode();
      for (int n = 0; n < nNode; n++)
        dofTo[n] = dofFrom[n];
      int offsetFrom = nNode;
      int offsetTo   = nNode;

      //Copy edge DOFs
      int nBasisEdgeFrom = basisFrom->nBasisEdge() / 3;
      int nBasisEdgeTo   = basisTo->nBasisEdge() / 3;
      for (int edge = 0; edge < 3; edge++)
      {
        for (int n = 0; n < nBasisEdgeFrom; n++)
          dofTo[offsetTo + n] = dofFrom[offsetFrom + n];

        offsetFrom += nBasisEdgeFrom;
        offsetTo   += nBasisEdgeTo;
      }

      //Copy cell DOFs
      int nBasisCellFrom = basisFrom->nBasisCell();
      for (int n = 0; n < nBasisCellFrom; n++)
        dofTo[offsetTo + n] = dofFrom[offsetFrom + n];
    }
    else
      SANS_DEVELOPER_EXCEPTION( "BasisFunctionArea_projectTo<T> - Hierarchical: order %d to %d not implemented",
                                basisFrom->order(), basisTo->order() );
  }
  else if ( basisTo->category() == BasisFunctionCategory_Lagrange )
  {
    //Prolongating by arbitrary increment
    std::vector<Real> coords_s, coords_t;
    getLagrangeNodes_Triangle(basisTo->order(), coords_s, coords_t);

    std::vector<Real> phi(nDOFFrom);
    std::array<int,Triangle::NEdge> edgeSign; //Leaving edgeSign empty since Lagrange basis doesn't need it

    for (int i = 0; i < nDOFTo; i++)
    {
      basisFrom->evalBasis(coords_s[i], coords_t[i], edgeSign, phi.data(), phi.size() );

      //Evaluate the solution in elemFrom at current node
      dofTo[i] = 0;
      for (int n = 0; n < nDOFFrom; n++)
        dofTo[i] += phi[n]*dofFrom[n];
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION( "BasisFunctionArea_projectTo<T> - Unknown basis function category." );
}

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionArea_projectTo( const BasisFunctionAreaBase<Quad>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                  const BasisFunctionAreaBase<Quad>* basisTo  ,       T dofTo[]  , const int nDOFTo )
{
  if ( basisFrom->category() == BasisFunctionCategory_Legendre &&
       basisTo->category()   == BasisFunctionCategory_Legendre )
  {
    //Prolongating by arbitrary increment
    if ( basisTo->order() >= basisFrom->order() )
    {
      //Copy Cell DOFs for Legendre
      int nCellTo = basisTo->nBasisCell();
      int nCell = std::min(basisFrom->nBasisCell(), nCellTo);
      for (int n = 0; n < nCell; n++)
        dofTo[n] = dofFrom[n];

      for (int n = nCell; n < nCellTo; n++)
        dofTo[n] = 0;
    }
    else
      SANS_DEVELOPER_EXCEPTION( "BasisFunctionArea_projectTo<T> - Legendre: order %d to %d not implemented.",
                                basisFrom->order(), basisTo->order() );
  }
  else if ( basisTo->category() == BasisFunctionCategory_Lagrange )
  {
    //Prolongating by arbitrary increment
    std::vector<Real> coords_s, coords_t;
    getLagrangeNodes_Quad(basisTo->order(), coords_s, coords_t);

    std::vector<Real> phi(nDOFFrom);
    std::array<int,Quad::NEdge> edgeSign; //Leaving edgeSign empty since Lagrange basis doesn't need it

    for (int i = 0; i < nDOFTo; i++)
    {
      basisFrom->evalBasis(coords_s[i], coords_t[i], edgeSign, phi.data(), phi.size() );

      //Evaluate the solution in elemFrom at current node
      dofTo[i] = 0;
      for (int n = 0; n < nDOFFrom; n++)
        dofTo[i] += phi[n]*dofFrom[n];
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION( "BasisFunctionArea_projectTo<T> - Unknown basis function category." );

  //SANS_DEVELOPER_EXCEPTION( "BasisFunctionArea_projectTo<T> - Quad basis function projection not implemented yet" );
}


#define INSTANTIATE_TRI(T) \
template void BasisFunctionArea_projectTo( const BasisFunctionAreaBase<Triangle>* basisFrom, const T dofFrom[], const int nDOFFrom, \
                                           const BasisFunctionAreaBase<Triangle>* basisTo  ,       T dofTo[]  , const int nDOFTo )

#define INSTANTIATE_QUAD(T) \
template void BasisFunctionArea_projectTo( const BasisFunctionAreaBase<Quad>* basisFrom, const T dofFrom[], const int nDOFFrom, \
                                           const BasisFunctionAreaBase<Quad>* basisTo  ,       T dofTo[]  , const int nDOFTo )

INSTANTIATE_TRI( Real );
INSTANTIATE_QUAD( Real );

INSTANTIATE_TRI( SurrealS<1> );
INSTANTIATE_QUAD( SurrealS<1> );


typedef DLA::VectorS<1, Real> VectorS1;
INSTANTIATE_TRI( VectorS1 );
INSTANTIATE_QUAD( VectorS1 );

typedef DLA::VectorS<2, Real> VectorS2;
INSTANTIATE_TRI( VectorS2 );
INSTANTIATE_QUAD( VectorS2 );

typedef DLA::VectorS<3, Real> VectorS3;
INSTANTIATE_TRI( VectorS3 );
INSTANTIATE_QUAD( VectorS3 );

typedef DLA::VectorS<4, Real> VectorS4;
INSTANTIATE_TRI( VectorS4 );
INSTANTIATE_QUAD( VectorS4 );

typedef DLA::VectorS<5, Real> VectorS5;
INSTANTIATE_TRI( VectorS5 );
INSTANTIATE_QUAD( VectorS5 );

typedef DLA::VectorS<6, Real> VectorS6;
INSTANTIATE_TRI( VectorS6 );
INSTANTIATE_QUAD( VectorS6 );

typedef DLA::VectorS<7, Real> VectorS7;
INSTANTIATE_TRI( VectorS7 );
INSTANTIATE_QUAD( VectorS7 );

// For Boltzmann implementation
typedef DLA::VectorS<9, Real> VectorS9;
INSTANTIATE_TRI( VectorS9 );
INSTANTIATE_QUAD( VectorS9 );
/*
typedef DLA::VectorS<13, Real> VectorS13;
INSTANTIATE_TRI( VectorS13 );
INSTANTIATE_QUAD( VectorS13 );

typedef DLA::VectorS<16, Real> VectorS16;
INSTANTIATE_TRI( VectorS16 );
INSTANTIATE_QUAD( VectorS16 );
*/
typedef DLA::VectorS<NVar, Real> VectorSNVar;
INSTANTIATE_TRI( VectorSNVar );
INSTANTIATE_QUAD( VectorSNVar );

typedef DLA::VectorS<2, VectorS1> Vector2VectorS1;
INSTANTIATE_TRI( Vector2VectorS1 );
INSTANTIATE_QUAD( Vector2VectorS1 );

typedef DLA::VectorS<2, VectorS2> Vector2VectorS2;
INSTANTIATE_TRI( Vector2VectorS2 );
INSTANTIATE_QUAD( Vector2VectorS2 );

typedef DLA::VectorS<2, VectorS3> Vector2VectorS3;
INSTANTIATE_TRI( Vector2VectorS3 );
INSTANTIATE_QUAD( Vector2VectorS3 );

typedef DLA::VectorS<2, VectorS4> Vector2VectorS4;
INSTANTIATE_TRI( Vector2VectorS4 );
INSTANTIATE_QUAD( Vector2VectorS4 );

typedef DLA::VectorS<2, VectorS5> Vector2VectorS5;
INSTANTIATE_TRI( Vector2VectorS5 );
INSTANTIATE_QUAD( Vector2VectorS5 );

// For Boltzmann Implementation
typedef DLA::VectorS<2, VectorS9> Vector2VectorS9;
INSTANTIATE_TRI( Vector2VectorS9 );
INSTANTIATE_QUAD( Vector2VectorS9 );
/*
typedef DLA::VectorS<2, VectorS13> Vector2VectorS13;
INSTANTIATE_TRI( Vector2VectorS13 );
INSTANTIATE_QUAD( Vector2VectorS13 );

typedef DLA::VectorS<2, VectorS16> Vector2VectorS16;
INSTANTIATE_TRI( Vector2VectorS16 );
INSTANTIATE_QUAD( Vector2VectorS16 );
*/
typedef DLA::VectorS<2, VectorSNVar> Vector2VectorSNVar;
INSTANTIATE_TRI( Vector2VectorSNVar );
INSTANTIATE_QUAD( Vector2VectorSNVar );


typedef DLA::VectorS<3, VectorS2> Vector3VectorS2;
INSTANTIATE_TRI( Vector3VectorS2 );
INSTANTIATE_QUAD( Vector3VectorS2 );

typedef DLA::VectorS<3, VectorS3> Vector3VectorS3;
INSTANTIATE_TRI( Vector3VectorS3 );
INSTANTIATE_QUAD( Vector3VectorS3 );

typedef DLA::VectorS<3, VectorS4> Vector3VectorS4;
INSTANTIATE_TRI( Vector3VectorS4 );
INSTANTIATE_QUAD( Vector3VectorS4 );

typedef DLA::VectorS<3, VectorS5> Vector3VectorS5;
INSTANTIATE_TRI( Vector3VectorS5 );
INSTANTIATE_QUAD( Vector3VectorS5 );

typedef DLA::VectorS<3, VectorS6> Vector3VectorS6;
INSTANTIATE_TRI( Vector3VectorS6 );
INSTANTIATE_QUAD( Vector3VectorS6 );

typedef DLA::VectorS<3, VectorS7> Vector3VectorS7;
INSTANTIATE_TRI( Vector3VectorS7 );
INSTANTIATE_QUAD( Vector3VectorS7 );


typedef DLA::MatrixSymS<2, Real> MatrixSymS2;
INSTANTIATE_TRI( MatrixSymS2 );
INSTANTIATE_QUAD( MatrixSymS2 );

typedef DLA::MatrixSymS<3, Real> MatrixSymS3;
INSTANTIATE_TRI( MatrixSymS3 );
INSTANTIATE_QUAD( MatrixSymS3 );


typedef DLA::MatrixS<3, 3, Real> MatrixS33;
INSTANTIATE_TRI( MatrixS33 );
INSTANTIATE_QUAD( MatrixS33 );

typedef DLA::MatrixS<3, 3, SurrealS<1>> MatrixS33S1;
INSTANTIATE_TRI( MatrixS33S1 );
INSTANTIATE_QUAD( MatrixS33S1 );

}
