// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISPOINTDERIVATIVE_H
#define BASISPOINTDERIVATIVE_H

#include "tools/SANSnumerics.h"

#include "BasisFunctionBase.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for representing the derivatives of a basis function evaluated at a point
//
// template parameters:
//   D:  number of parametric coordinates
//----------------------------------------------------------------------------//

template<int D>
class BasisPointDerivative
{
public:
  BasisPointDerivative( const BasisPointDerivative& ) = delete;
  BasisPointDerivative& operator=( const BasisPointDerivative& ) = delete;

  explicit BasisPointDerivative( const BasisFunctionBase* basis ) : nphi_(basis->nBasis())
  {
    for (int d = 0; d < D; d++)
      phis_[d] = new Real[nphi_];
  }

  ~BasisPointDerivative()
  {
    for (int d = 0; d < D; d++)
      delete [] phis_[d];
  }

        SANS::DLA::VectorS<D,Real>& ref()       { return Ref_; }
  const SANS::DLA::VectorS<D,Real>& ref() const { return Ref_; }

        Real* deriv(const int d)       { return phis_[d]; }
  const Real* deriv(const int d) const { return phis_[d]; }

  int size() const { return nphi_; }

protected:
  const int nphi_;
  Real* phis_[D];  // derivative of basis function wrt parametric coordinates (s,t,u)
  SANS::DLA::VectorS<D,Real> Ref_;
};

}

#endif // BASISPOINTDERIVATIVE_H
