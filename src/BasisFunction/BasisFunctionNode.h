// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONNODE_H
#define BASISFUNCTIONNODE_H

// 0-dimensional node basis functions (used for 1D calculations)
// implemented:
//   P0 single constant value at the node

// NOTES:
// - Implemented via abstract base class (ABC)
// - Derived classes only contain static data. Hence, they are
//   constructed to be singletons. Using templates instead of ABC
//   would lead to code bloat


#include <iostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "BasisFunctionBase.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

namespace SANS
{

static const int BasisFunctionNode_PMax = 0;

template<class Topology>
struct LagrangeNodes;

template<>
struct LagrangeNodes<Node>
{
  static const int PMax = 0;
  static void get(const int order, std::vector<DLA::VectorS<TopoD1::D,Real>>& sRef);
};

//----------------------------------------------------------------------------//
// node basis functions
//
// reference line element defined: s in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edge interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinate
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

class BasisFunctionNodeBase : public BasisFunctionBase
{
public:
  static const int D = 0;     // topological dimensions

  ~BasisFunctionNodeBase() {}

  int order() const override { return 0; }
  int nBasis() const override { return 1; }
  int nBasisNode() const { return 1; }
  BasisFunctionCategory category() const override { return BasisFunctionCategory_Legendre; }

  void evalBasis( Real phi[], int nphi ) const { SANS_ASSERT(nphi == 1); phi[0] = 1; }
  void evalBasisDerivative( Real phis[], int nphi ) const { SANS_ASSERT(nphi == 1); phis[0] = 0; }
  void evalBasisHessianDerivative( Real phiss[], int nphi ) const { SANS_ASSERT(nphi == 1); phiss[0] = 0; }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

  static const BasisFunctionNodeBase* getBasisFunction( const int order, const BasisFunctionCategory& category )
  {
    if (category == BasisFunctionCategory_Legendre ||
        category == BasisFunctionCategory_RBS )
    {
      switch (order)
      {
      case 0:
        return self;
      default:
        SANS_DEVELOPER_EXCEPTION( "unexpected order = %d\n", order );
        break;
      }
    }
    else if (category == BasisFunctionCategory_Lagrange ||
             category == BasisFunctionCategory_Hierarchical ||
             category == BasisFunctionCategory_Bernstein )
    {
      switch (order)
      {
      case 1:
        return self;
      default:
        SANS_DEVELOPER_EXCEPTION( "unexpected order = %d\n", order );
        break;
      }
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unexpected basis function category = %d\n",
                                               static_cast<int>(category) );

    return self;
  }

protected:
  //Singleton!
  BasisFunctionNodeBase() {}
  static const BasisFunctionNodeBase self_;
public:
  static const BasisFunctionNodeBase *self;
  static const BasisFunctionNodeBase *P0;
};

}
#endif  // BASISFUNCTIONNODE_H
