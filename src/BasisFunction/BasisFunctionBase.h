// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTION_BASE_H
#define BASISFUNCTION_BASE_H

#include "BasisFunctionCategory.h"
#include "Topology/ElementTopology.h"

//----------------------------------------------------------------------------//
// general basis function base class for all basis functions
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .category        basis function category
//
//----------------------------------------------------------------------------//

namespace SANS
{

class BasisFunctionBase
{
public:
  virtual ~BasisFunctionBase() {}

  virtual int order() const = 0;
  virtual int nBasis() const = 0;
  virtual BasisFunctionCategory category() const = 0;

protected:
  BasisFunctionBase() {}  // abstract base class
};

//----------------------------------------------------------------------------//
// Forward declarations
class BasisFunctionNodeBase;
class BasisFunctionLineBase;
template <class Topology>
class BasisFunctionAreaBase;
template <class Topology>
class BasisFunctionVolumeBase;
template <class Topology>
class BasisFunctionSpacetimeBase;

//----------------------------------------------------------------------------//
template<class TopoDim, class Topology>
struct BasisFunctionTopologyBase;

template<>               struct BasisFunctionTopologyBase<TopoD0, Node>     { typedef BasisFunctionNodeBase             type; };
template<>               struct BasisFunctionTopologyBase<TopoD1, Line>     { typedef BasisFunctionLineBase             type; };
template<class Topology> struct BasisFunctionTopologyBase<TopoD2, Topology> { typedef BasisFunctionAreaBase<Topology>   type; };
template<class Topology> struct BasisFunctionTopologyBase<TopoD3, Topology> { typedef BasisFunctionVolumeBase<Topology> type; };
template<class Topology> struct BasisFunctionTopologyBase<TopoD4, Topology> { typedef BasisFunctionSpacetimeBase<Topology> type; };

}

#endif //BASISFUNCTION_BASE_H
