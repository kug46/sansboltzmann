// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONVOLUME_HEXAHEDRON_LAGRANGE_H
#define BASISFUNCTIONVOLUME_HEXAHEDRON_LAGRANGE_H

// tetrahedron basis functions: Langrange

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionVolume.h


#include <array>
#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionVolume.h"
#include "BasisFunctionCategory.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// volume basis functions: Langrange
//
// reference triangle element defined: s in [0, 1], t in [0, 1], u in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionVolume_Hex_LagrangePMax = 4;

template<class Topology>
struct LagrangeNodes;

template<>
struct LagrangeNodes<Hex>
{
  static const int PMax = BasisFunctionVolume_Hex_LagrangePMax;

  static void get(const int order, std::vector<DLA::VectorS<TopoD3::D,Real>>& sRef);
};

void getLagrangeNodes_Hex(const int order, std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u);

//----------------------------------------------------------------------------//
// Langrange: linear

template <>
class BasisFunctionVolume<Hex,Lagrange,1> :
  public BasisFunctionVolumeBase<Hex>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,6> Int6;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 8; }
  virtual int nBasisNode() const { return 8; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6& sgn,
      Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
  static const std::vector<Real> coords_u_;
public:
  static const BasisFunctionVolume* self();
};

//----------------------------------------------------------------------------//
// Langrange: Quadratic

template <>
class BasisFunctionVolume<Hex,Lagrange,2> :
  public BasisFunctionVolumeBase<Hex>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,6> Int6;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 2; }
  virtual int nBasis() const { return 27; }
  virtual int nBasisNode() const { return 8; }
  virtual int nBasisEdge() const { return 12; }
  virtual int nBasisFace() const { return 6; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6& sgn,
      Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
  static const std::vector<Real> coords_u_;
public:
  static const BasisFunctionVolume* self();
};

//----------------------------------------------------------------------------//
// Langrange: Cubic

template <>
class BasisFunctionVolume<Hex,Lagrange,3> :
  public BasisFunctionVolumeBase<Hex>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,6> Int6;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 3; }
  virtual int nBasis() const { return 64; }
  virtual int nBasisNode() const { return 8; }
  virtual int nBasisEdge() const { return 24; }
  virtual int nBasisFace() const { return 24; }
  virtual int nBasisCell() const { return 8; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6& sgn,
      Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
  static const std::vector<Real> coords_u_;
public:
  static const BasisFunctionVolume* self();
};

//----------------------------------------------------------------------------//
// Langrange: 4th order

template <>
class BasisFunctionVolume<Hex,Lagrange,4> :
  public BasisFunctionVolumeBase<Hex>
{
public:
  static const int D = 3;     // physical dimensions

  typedef std::array<int,6> Int6;

  virtual ~BasisFunctionVolume() {}

  virtual int order() const { return 4; }
  virtual int nBasis() const { return 125; }
  virtual int nBasisNode() const { return 8; }
  virtual int nBasisEdge() const { return 36; }
  virtual int nBasisFace() const { return 54; }
  virtual int nBasisCell() const { return 27; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6&,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Int6& sgn,
      Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionVolume() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
  static const std::vector<Real> coords_u_;
public:
  static const BasisFunctionVolume* self();
};


}

#endif  // BASISFUNCTIONVOLUME_HEXAHEDRON_LAGRANGE_H
