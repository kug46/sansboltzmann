// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunction/BasisFunction_RefElement_Split.h"
#include "BasisFunction/ElementEdges.h"

#include "tools/SANSException.h"

namespace SANS
{


void
BasisFunction_RefElement_Split<TopoD1, Line>::transform(const RefCoordType& Refcoord_sub, ElementSplitType split_type,
                                                        int split_edge_index, int sub_cell_index, RefCoordType& Refcoord_main)
{
  SANS_ASSERT( sub_cell_index >=0 && sub_cell_index <= 1 ); // Only two sub-cells possible - 0 (Left) or 1 (Right)

  if (split_type == ElementSplitType::Isotropic)
  {
    if (split_edge_index == -1)
    {
      Refcoord_main[0] = sub_cell_index*Line::centerRef[0] + Line::centerRef[0]*Refcoord_sub[0];
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD1,Line> - "
                               "Split edge index (=%d) should be -1 for an isotropic-split.", split_edge_index);
  }
  else if (split_type == ElementSplitType::Edge)
  {
    SANS_ASSERT_MSG( split_edge_index == 0, "Split edge index (=%d) should be 0 for an edge-split.", split_edge_index);
    Refcoord_main[0] = sub_cell_index*Line::centerRef[0] + Line::centerRef[0]*Refcoord_sub[0];
  }
  else
    SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD1,Line> - Invalid ElementSplitType.");
}

void
BasisFunction_RefElement_Split<TopoD2, Triangle>::transform(const RefCoordType& Refcoord_sub, ElementSplitType split_type,
                                                            int split_edge_index, int sub_cell_index, RefCoordType& Refcoord_main)
{

  Real u = Refcoord_sub[0];
  Real v = Refcoord_sub[1];

  if (split_type==ElementSplitType::Edge)
  {
    if (split_edge_index == 0) //Splitting trace0
    {
      if (sub_cell_index==0) //(0,0)-(1,0)-(0.5,0.5)
      {
        Refcoord_main[0] = u + 0.5*v;
        Refcoord_main[1] = 0.5*v;
      }
      else if (sub_cell_index==1) //(0,0)-(0.5,0.5)-(0,1)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*u + v;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD2,Triangle> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 1) //Splitting trace1
    {
      if (sub_cell_index==0) //(0.0,0.5)-(1,0)-(0,1)
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = -0.5*u + 0.5*v + 0.5;
      }
      else if (sub_cell_index==1) //(0,0)-(1,0)-(0.0,0.5)
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = 0.5*v;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD2,Triangle> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 2) //Splitting trace2
    {
      if (sub_cell_index==0) //(0,0)-(0.5,0)-(0,1)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = v;
      }
      else if (sub_cell_index==1) //(0.5,0.0)-(1,0)-(0,1)
      {
        Refcoord_main[0] = 0.5*u - 0.5*v + 0.5;
        Refcoord_main[1] = v;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD2,Triangle> - Invalid sub_cell_index.");
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD2,Triangle> - "
                               "Split edge index (=%d) should be 0, 1 or 2 for an edge-split.", split_edge_index);
  }
  //-------------------------------------------------------------------------------------------------------------------
  else if (split_type == ElementSplitType::Isotropic)
  {
    if (split_edge_index == -1)
    {
      if (sub_cell_index==0) //(0.5,0.0)-(1,0)-(0.5,0.5)
      {
        Refcoord_main[0] = 0.5*u + 0.5;
        Refcoord_main[1] = 0.5*v;
      }
      else if (sub_cell_index==1) //(0.0,0.5)-(0.5,0.5)-(0,1)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*v + 0.5;
      }
      else if (sub_cell_index==2) //(0,0)-(0.5,0)-(0,0.5)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*v;
      }
      else if (sub_cell_index==3) //(0.5,0.5)-(0.0,0.5)-(0.5,0.0)
      {
        Refcoord_main[0] = 0.5 - 0.5*u;
        Refcoord_main[1] = 0.5 - 0.5*v;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD2,Triangle> - Invalid sub_cell_index.");
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD2,Triangle> - "
                               "Split edge index (=%d) should be -1 for an isotropic-split.", split_edge_index);
  }
  else
    SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD2,Triangle> - Invalid ElementSplitType.");
}

void
BasisFunction_RefElement_Split<TopoD2, Quad>::transform(const RefCoordType& Refcoord_sub, ElementSplitType split_type,
                                                        int split_edge_index, int sub_cell_index, RefCoordType& Refcoord_main)
{
  SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD2,Quad> - Quad splitting not supported yet.");
}

void
BasisFunction_RefElement_Split<TopoD3, Tet>::transform(const RefCoordType& Refcoord_sub, ElementSplitType split_type,
                                                       int split_edge_index, int sub_cell_index, RefCoordType& Refcoord_main)
{

  Real u = Refcoord_sub[0];
  Real v = Refcoord_sub[1];
  Real w = Refcoord_sub[2];

  if (split_type==ElementSplitType::Edge)
  {
    if (split_edge_index == 0) //Splitting edge0
    {
      if (sub_cell_index==0) //(0,0,0)-(1,0,0)-(0,1,0)-(0.0,0.5,0.5)
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = v + 0.5*w;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==1) //(0,0,0)-(1,0,0)-(0.0,0.5,0.5)-(0,0,1)
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = 0.5*v + w;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 1) //Splitting edge1
    {
      if (sub_cell_index==0) //(0,0,0)-(0.5,0.0,0.5)-(0,1,0)-(0,0,1)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = v;
        Refcoord_main[2] = 0.5*u + w;
      }
      else if (sub_cell_index==1) //(0,0,0)-(1,0,0)-(0,1,0)-(0.5,0.0,0.5)
      {
        Refcoord_main[0] = u + 0.5*w;
        Refcoord_main[1] = v;
        Refcoord_main[2] = 0.5*w;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 2) //Splitting edge2
    {
      if (sub_cell_index==0) //(0,0,0)-(1,0,0)-(0.5,0.5,0)-(0,0,1)
      {
        Refcoord_main[0] = u + 0.5*v;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = w;
      }
      else if (sub_cell_index==1) //(0,0,0)-(0.5,0.5,0.0)-(0,1,0)-(0,0,1)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*u + v;
        Refcoord_main[2] = w;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 3) //Splitting edge3
    {
      if (sub_cell_index==0) //(0.0,0.5,0.0)-(1,0,0)-(0,1,0)-(0,0,1)
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = 0.5*(-u + v - w) + 0.5;
        Refcoord_main[2] = w;
      }
      else if (sub_cell_index==1) //(0,0,0)-(1,0,0)-(0.0,0.5,0.0)-(0,0,1)
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = w;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 4) //Splitting edge4
    {
      if (sub_cell_index==0) //(0,0,0)-(1,0,0)-(0,1,0)-(0,0,0.5)
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = v;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==1) //(0.0,0.0,0.5)-(1,0,0)-(0,1,0)-(0,0,1)
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = v;
        Refcoord_main[2] = 0.5*(-u - v + w) + 0.5;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 5) //Splitting edge5
    {
      if (sub_cell_index==0) //(0,0,0)-(0.5,0.0,0.0)-(0,1,0)-(0,0,1)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = v;
        Refcoord_main[2] = w;
      }
      else if (sub_cell_index==1) //(0.5,0.0,0.0)-(1,0,0)-(0,1,0)-(0,0,1)
      {
        Refcoord_main[0] = 0.5*(u - v - w) + 0.5;
        Refcoord_main[1] = v;
        Refcoord_main[2] = w;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - "
                               "Split edge index (=%d) should be 0, 1, 2, 3, 4 or 5 for an edge-split.", split_edge_index);
  }
  //-------------------------------------------------------------------------------------------------------------------
  else if (split_type == ElementSplitType::Isotropic)
  {
    if (split_edge_index == -1)
    {
      if (sub_cell_index==0) //(0.0,0.5,0.0)-(0.5,0.5,0)-(0,1,0)-(0.0,0.5,0.5)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*v + 0.5;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==1) //(0.0,0.0,0.5)-(0.5,0.0,0.5)-(0.0,0.5,0.5)-(0,0,1)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = 0.5*w + 0.5;
      }
      else if (sub_cell_index==2) //(0.5,0.0,0.0)-(1,0,0)-(0.5,0.5,0)-(0.5,0.0,0.5)
      {
        Refcoord_main[0] = 0.5*u + 0.5;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==3) //(0,0,0)-(0.5,0.0,0.0)-(0.0,0.5,0.0)-(0.0,0.0,0.5)
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==4) //(0.0,0.5,0)-(0.5,0.5,0.0)-(0.0,0.5,0.5)-(0.5,0.0,0.5)
      {
        Refcoord_main[0] =  0.5*(u + w);
        Refcoord_main[1] = -0.5*w + 0.5;
        Refcoord_main[2] =  0.5*(v + w);
      }
      else if (sub_cell_index==5) //(0.0,0.0,0.5)-(0.0,0.5,0.5)-(0.5,0.0,0.5)-(0.0,0.5,0.0)
      {
        Refcoord_main[0] =  0.5*v;
        Refcoord_main[1] =  0.5*(u + w);
        Refcoord_main[2] = -0.5*w + 0.5;
      }
      else if (sub_cell_index==6) //(0.5,0.0,0.0)-(0.5,0.0,0.5)-(0.5,0.5,0.0)-(0.0,0.5,0.0)
      {
        Refcoord_main[0] = -0.5*w + 0.5;
        Refcoord_main[1] =  0.5*(v + w);
        Refcoord_main[2] =  0.5*u;
      }
      else if (sub_cell_index==7) //(0.5,0.0,0.0)-(0.0,0.5,0.0)-(0.0,0.0,0.5)-(0.5,0.0,0.5)
      {
        Refcoord_main[0] = -0.5*(u + v) + 0.5;
        Refcoord_main[1] =  0.5*u;
        Refcoord_main[2] =  0.5*(v + w);
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - "
                               "Split edge index (=%d) should be -1 for an isotropic-split.", split_edge_index);
  }
  //-------------------------------------------------------------------------------------------------------------------
  else if (split_type==ElementSplitType::IsotropicFace)
  {
    if (split_edge_index == 0) //Splitting face0
    {
      if (sub_cell_index==0) //vertices: 0-6-2-4
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*u + v + 0.5*w;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==1) //vertices: 0-5-4-3
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = 0.5*u + 0.5*v + w;
      }
      else if (sub_cell_index==2) //vertices: 0-1-6-5
      {
        Refcoord_main[0] = u + 0.5*v + 0.5*w;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==3) //vertices: 0-4-5-6
      {
        Refcoord_main[0] = 0.5*(v + w);
        Refcoord_main[1] = 0.5*(u + w);
        Refcoord_main[2] = 0.5*(u + v);
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 1) //Splitting face1
    {
      if (sub_cell_index==0) //vertices: 8-1-4-3
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = 0.5*(-u + w + 1.0);
      }
      else if (sub_cell_index==1) //vertices: 7-1-2-4
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = 0.5*(-u + v + 1.0);
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==2) //vertices: 0-1-7-8
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==3) //vertices: 4-1-8-7
      {
        Refcoord_main[0] = u;
        Refcoord_main[1] = 0.5*(-u - v + 1.0);
        Refcoord_main[2] = 0.5*(-u - w + 1.0);
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 2) //Splitting face2
    {
      if (sub_cell_index==0) //vertices: 9-1-2-5
      {
        Refcoord_main[0] = 0.5*(u - v + 1.0);
        Refcoord_main[1] = v;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==1) //vertices: 8-5-2-3
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = v;
        Refcoord_main[2] = 0.5*(-v + w + 1.0);
      }
      else if (sub_cell_index==2) //vertices: 0-9-2-8
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = v;
        Refcoord_main[2] = 0.5*w;
      }
      else if (sub_cell_index==3) //vertices: 5-8-2-9
      {
        Refcoord_main[0] = 0.5*(-u - v + 1.0);
        Refcoord_main[1] = v;
        Refcoord_main[2] = 0.5*(-v - w + 1.0);
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else if (split_edge_index == 3) //Splitting face3
    {
      if (sub_cell_index==0) //vertices: 7-6-2-3
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*(v - w + 1.0);
        Refcoord_main[2] = w;
      }
      else if (sub_cell_index==1) //vertices: 9-1-6-3
      {
        Refcoord_main[0] = 0.5*(u - w + 1.0);
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = w;
      }
      else if (sub_cell_index==2) //vertices: 0-9-7-3
      {
        Refcoord_main[0] = 0.5*u;
        Refcoord_main[1] = 0.5*v;
        Refcoord_main[2] = w;
      }
      else if (sub_cell_index==3) //vertices: 6-7-9-3
      {
        Refcoord_main[0] = 0.5*(-u - w + 1.0);
        Refcoord_main[1] = 0.5*(-v - w + 1.0);
        Refcoord_main[2] = w;
      }
      else
        SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid sub_cell_index.");
    }
    else
      SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - "
                               "Split face index (=%d) should be 0, 1, 2, 3 for an isotropic-face-split.", split_edge_index);
  }
  else
    SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Tet> - Invalid ElementSplitType.");
}

void
BasisFunction_RefElement_Split<TopoD3, Hex>::transform(const RefCoordType& Refcoord_sub, ElementSplitType split_type,
                                                       int split_edge_index, int sub_cell_index, RefCoordType& Refcoord_main)
{
  SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD3,Hex> - Hex splitting not supported yet.");
}

void
BasisFunction_RefElement_Split<TopoD4, Pentatope>::transform(const RefCoordType& Refcoord_sub, ElementSplitType split_type,
                                                       int split_edge_index, int sub_cell_index, RefCoordType& Refcoord_main)
{
  SANS_ASSERT( split_edge_index >= 0 || split_edge_index < Pentatope::NEdge );

  const int (*edges)[Line::NNode] = ElementEdges<Pentatope>::EdgeNodes;

  // barycentric coordinates for the split cell
  Real phi0 = 1 -Refcoord_sub[0] -Refcoord_sub[1] -Refcoord_sub[2] -Refcoord_sub[3];
  Real phi1 = Refcoord_sub[0];
  Real phi2 = Refcoord_sub[1];
  Real phi3 = Refcoord_sub[2];
  Real phi4 = Refcoord_sub[3];

  if (split_type==ElementSplitType::Edge)
  {
    // edge splits create two subcells
    SANS_ASSERT( sub_cell_index >= 0 && sub_cell_index < 2 );

    // reference pentatope with respect to the coordinates below
    int pentatope[5] = {0,1,2,3,4};

    // reference coordinates of the pentatope with one extra entry for the split vertex
    Real X[6][4] = { {0,0,0,0} , {1,0,0,0} , {0,1,0,0} , {0,0,1,0} , {0,0,0,1} , {-1,-1,-1,-1} };
    const int ns = 5; // index of the split vertex

    // retrieve the edge indices
    int e0 = edges[ split_edge_index ][0];
    int e1 = edges[ split_edge_index ][1];

    // the subcell index defines which vertex gets replaced in the canonical (unsplit) simplex
    if (sub_cell_index==0) pentatope[e1] = ns;
    else pentatope[e0] = ns;

    // compute the coordinates of split vertex
    for (int d=0;d<TopoD4::D;d++)
      X[5][d] = 0.5*( X[e0][d] +X[e1][d] );

    // now compute the barycentric coordinates of the main cell
    for (int d=0;d<TopoD4::D;d++)
      Refcoord_main[d] =  phi0*X[ pentatope[0] ][d]
                         +phi1*X[ pentatope[1] ][d]
                         +phi2*X[ pentatope[2] ][d]
                         +phi3*X[ pentatope[3] ][d]
                         +phi4*X[ pentatope[4] ][d];
  }
  else
    SANS_DEVELOPER_EXCEPTION("BasisFunction_Split_Local<TopoD4,Pentatope> - only edge splits are currently supported");
}

}
