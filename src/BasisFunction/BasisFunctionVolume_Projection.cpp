// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionVolume_Projection.h"

// projection of basis volume functions

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "BasisFunctionVolume.h"
#include "BasisFunctionCategory.h"
#include "BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunctionVolume_Hexahedron_Lagrange.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionVolume_projectTo( const BasisFunctionVolumeBase<Tet>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                    const BasisFunctionVolumeBase<Tet>* basisTo  ,       T dofTo[]  , const int nDOFTo )
{
  SANS_ASSERT( nDOFFrom == basisFrom->nBasis() );
  SANS_ASSERT( nDOFTo == basisTo->nBasis() );

  if ( basisFrom->category() == BasisFunctionCategory_Legendre &&
       basisTo->category()   == BasisFunctionCategory_Legendre )
  {
    //Copy Cell DOFs for Legendre
    int nCellTo = basisTo->nBasisCell();
    int nCell = std::min(basisFrom->nBasisCell(), nCellTo);
    for (int n = 0; n < nCell; n++)
      dofTo[n] = dofFrom[n];

    for (int n = nCell; n < nCellTo; n++)
      dofTo[n] = 0;
  }

  else if ( ( basisFrom->category() == BasisFunctionCategory_Hierarchical ||
             (basisFrom->category() == BasisFunctionCategory_Lagrange && basisFrom->order() == 1) ) &&
            basisTo->category()   == BasisFunctionCategory_Hierarchical )
  {
    //Prolongating by arbitrary increment
    if ( basisTo->order() >= basisFrom->order() )
    {
      //Zero out everything first
      for (int n = 0; n < nDOFTo; n++)
        dofTo[n] = 0;

      //Copy Nodes
      const int nNode = basisFrom->nBasisNode();
      for (int n = 0; n < nNode; n++)
        dofTo[n] = dofFrom[n];

      int offsetFrom = nNode;
      int offsetTo   = nNode;

      //Copy edge DOFs
      int nBasisEdgeFrom = basisFrom->nBasisEdge() / 6;
      int nBasisEdgeTo   = basisTo->nBasisEdge() / 6;
      for (int edge = 0; edge < 6; edge++)
      {
        for (int n = 0; n < nBasisEdgeFrom; n++)
          dofTo[offsetTo + n] = dofFrom[offsetFrom + n];

        offsetFrom += nBasisEdgeFrom;
        offsetTo   += nBasisEdgeTo;
      }

      //Copy face DOFs
      int nBasisFaceFrom = basisFrom->nBasisFace() / 4;
      int nBasisFaceTo   = basisTo->nBasisFace() / 4;
      for (int face = 0; face < 4; face++)
      {
        for (int n = 0; n < nBasisFaceFrom; n++)
          dofTo[offsetTo + n] = dofFrom[offsetFrom + n];

        offsetFrom += nBasisFaceFrom;
        offsetTo   += nBasisFaceTo;
      }

      //Copy cell DOFs
      int nBasisCellFrom = basisFrom->nBasisCell();
      for (int n = 0; n < nBasisCellFrom; n++)
        dofTo[offsetTo + n] = dofFrom[offsetFrom + n];
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Hierarchical: order %d to %d not implemented", basisFrom->order(), basisTo->order() );
  }
  else if ( basisTo->category() == BasisFunctionCategory_Lagrange )
  {
    //Prolongating by arbitrary increment
    std::vector<Real> coords_s, coords_t, coords_u;
    getLagrangeNodes_Tet(basisTo->order(), coords_s, coords_t, coords_u);

    std::vector<Real> phi(nDOFFrom);
    std::array<int,Tet::NFace> faceSign; //Leaving faceSign empty since Lagrange basis doesn't need it

    for (int i = 0; i < nDOFTo; i++)
    {
      basisFrom->evalBasis(coords_s[i], coords_t[i], coords_u[i], faceSign, phi.data(), phi.size() );

      //Evaluate the solution in elemFrom at current node
      dofTo[i] = 0;
      for (int n = 0; n < nDOFFrom; n++)
        dofTo[i] += phi[n]*dofFrom[n];
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION( "unknown basis function category" );
}

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionVolume_projectTo( const BasisFunctionVolumeBase<Hex>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                    const BasisFunctionVolumeBase<Hex>* basisTo  ,       T dofTo[]  , const int nDOFTo )
{
  SANS_ASSERT( nDOFFrom == basisFrom->nBasis() );
  SANS_ASSERT( nDOFTo == basisTo->nBasis() );

  if ( basisFrom->category() == BasisFunctionCategory_Legendre &&
       basisTo->category()   == BasisFunctionCategory_Legendre )
  {
    //Copy Cell DOFs for Legendre
    int nCellTo = basisTo->nBasisCell();
    int nCell = std::min(basisFrom->nBasisCell(), nCellTo);
    for (int n = 0; n < nCell; n++)
      dofTo[n] = dofFrom[n];

    for (int n = nCell; n < nCellTo; n++)
      dofTo[n] = 0;
  }
  else if ( ( basisFrom->category() == BasisFunctionCategory_Hierarchical ||
             (basisFrom->category() == BasisFunctionCategory_Lagrange && basisFrom->order() == 1) ) &&
            basisTo->category()   == BasisFunctionCategory_Hierarchical )
  {
    SANS_DEVELOPER_EXCEPTION( "Hierarchical Hex: order %d to %d not implemented", basisFrom->order(), basisTo->order() );
  }
  else if ( basisTo->category() == BasisFunctionCategory_Lagrange )
  {
    //Prolongating by arbitrary increment
    std::vector<Real> coords_s, coords_t, coords_u;
    getLagrangeNodes_Hex(basisTo->order(), coords_s, coords_t, coords_u);

    std::vector<Real> phi(nDOFFrom);
    std::array<int,Hex::NFace> faceSign; //Leaving faceSign empty since Lagrange basis doesn't need it

    for (int i = 0; i < nDOFTo; i++)
    {
      basisFrom->evalBasis(coords_s[i], coords_t[i], coords_u[i], faceSign, phi.data(), phi.size() );

      //Evaluate the solution in elemFrom at current node
      dofTo[i] = 0;
      for (int n = 0; n < nDOFFrom; n++)
        dofTo[i] += phi[n]*dofFrom[n];
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION( "unknown basis function category" );
}

#define INSTANTIATE_TET(T) \
template void BasisFunctionVolume_projectTo( const BasisFunctionVolumeBase<Tet>* basisFrom, const T dofFrom[], const int nDOFFrom, \
                                             const BasisFunctionVolumeBase<Tet>* basisTo  ,       T dofTo[]  , const int nDOFTo )

#define INSTANTIATE_HEX(T) \
template void BasisFunctionVolume_projectTo( const BasisFunctionVolumeBase<Hex>* basisFrom, const T dofFrom[], const int nDOFFrom, \
                                             const BasisFunctionVolumeBase<Hex>* basisTo  ,       T dofTo[]  , const int nDOFTo )

INSTANTIATE_TET( Real );
INSTANTIATE_HEX( Real );

typedef DLA::VectorS<1, Real> VectorS1;
INSTANTIATE_TET( VectorS1 );
INSTANTIATE_HEX( VectorS1 );

typedef DLA::VectorS<2, Real> VectorS2;
INSTANTIATE_TET( VectorS2 );
INSTANTIATE_HEX( VectorS2 );

typedef DLA::VectorS<3, Real> VectorS3;
INSTANTIATE_TET( VectorS3 );
INSTANTIATE_HEX( VectorS3 );

typedef DLA::VectorS<4, Real> VectorS4;
INSTANTIATE_TET( VectorS4 );
INSTANTIATE_HEX( VectorS4 );

typedef DLA::VectorS<5, Real> VectorS5;
INSTANTIATE_TET( VectorS5 );
INSTANTIATE_HEX( VectorS5 );

typedef DLA::VectorS<6, Real> VectorS6;
INSTANTIATE_TET( VectorS6 );
INSTANTIATE_HEX( VectorS6 );

typedef DLA::VectorS<7, Real> VectorS7;
INSTANTIATE_TET( VectorS7 );
INSTANTIATE_HEX( VectorS7 );

typedef DLA::VectorS<3, VectorS2> Vector3VectorS2;
INSTANTIATE_TET( Vector3VectorS2 );
INSTANTIATE_HEX( Vector3VectorS2 );

typedef DLA::VectorS<3, VectorS3> Vector3VectorS3;
INSTANTIATE_TET( Vector3VectorS3 );
INSTANTIATE_HEX( Vector3VectorS3 );

typedef DLA::VectorS<3, VectorS4> Vector3VectorS4;
INSTANTIATE_TET( Vector3VectorS4 );
INSTANTIATE_HEX( Vector3VectorS4 );

typedef DLA::VectorS<3, VectorS5> Vector3VectorS5;
INSTANTIATE_TET( Vector3VectorS5 );
INSTANTIATE_HEX( Vector3VectorS5 );

typedef DLA::VectorS<3, VectorS6> Vector3VectorS6;
INSTANTIATE_TET( Vector3VectorS6 );
INSTANTIATE_HEX( Vector3VectorS6 );

typedef DLA::VectorS<3, VectorS7> Vector3VectorS7;
INSTANTIATE_TET( Vector3VectorS7 );
INSTANTIATE_HEX( Vector3VectorS7 );

typedef DLA::VectorS<4, VectorS2> Vector4Vector2;
INSTANTIATE_TET( Vector4Vector2 );

typedef DLA::MatrixSymS<3, Real> MatrixSymS3;
INSTANTIATE_TET( MatrixSymS3 );
INSTANTIATE_HEX( MatrixSymS3 );

typedef DLA::MatrixSymS<4, Real> MatrixSymS4;
INSTANTIATE_TET( MatrixSymS4 );

}
