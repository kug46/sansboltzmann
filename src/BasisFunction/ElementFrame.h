// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CELLFRAME_H
#define CELLFRAME_H

#include <vector>
#include <array>

#include "Topology/ElementTopology.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class CellTopology>
struct ElementFrame
{
  typedef typename CellTopology::TopologyFrame FrameTopology;

  // A map for node pairs that make up all frames of the cell
  static const int FrameNodes[ CellTopology::NFrame ][ FrameTopology::NNode ];

  static int getCanonicalFrame( const int *frameNodes, const int nFrameNodes,
                               const int *cellNodes, const int nCellNodes);

  static int getCanonicalFrame( const std::vector<int>& frameNodes,
                               const std::vector<int>& cellNodes )
  {
    return getCanonicalFrame( &frameNodes[0], frameNodes.size(), cellNodes.data(), cellNodes.size() );
  }

  static int getCanonicalFrame( const int *frameNodes, const int nFrameNodes,
                               const std::vector<int>& cellNodes)
  {
    return getCanonicalFrame( frameNodes, nFrameNodes, cellNodes.data(), cellNodes.size() );
  }

  static int getCanonicalFrame( const std::vector<int>& frameNodes,
                               const int *cellNodes, const int nCellNodes)
  {
    return getCanonicalFrame( frameNodes.data(), frameNodes.size(), cellNodes, nCellNodes );
  }
};

}

#endif  // CELLFRAME_H
