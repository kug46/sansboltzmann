// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRACETOCELLREFCOORD_H
#define TRACETOCELLREFCOORD_H

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

#include "CanonicalTraceToCell.h"

namespace SANS
{

template<class TopoDim>
struct QuadraturePoint; // forward declare

template<class TopoDim>
struct QuadratureCellTracePoint;

//----------------------------------------------------------------------------//
template<class TraceTopology, class TopoDimCell, class CellTopology>
struct TraceToCellRefCoord;

template<class TraceTopology, class TopoDimCell>
struct CanonicalOrientation;

//----------------------------------------------------------------------------//
// cell reference coordinates (s) given canonical node number
/*
       (0)
    0 ----- 1
*/
// parameters:
//  canonicalNode      node on line (N0=1, N1=0)

template<>
struct TraceToCellRefCoord<Node, TopoD1, Line>
{
  static const int TraceNodes[ Line::NNode ][ 1 ];

  static void
  eval( const CanonicalTraceToCell& canonicalNode, Real& sRefLine );
  static void
  eval( const CanonicalTraceToCell& canonicalNode, const DLA::VectorS<1,Real>& sRefTrace, DLA::VectorS<1,Real>& sRefCell );
  static void
  eval( const CanonicalTraceToCell& canonicalNode, const QuadraturePoint<TopoD0>& sRefTrace, QuadratureCellTracePoint<TopoD1>& sRefCell );

  // Gets the canonical orientation of an edge
  static CanonicalTraceToCell getCanonicalTrace(const int *edgeNodes, const int nEdgeNodes,
                                                const int *cellNodes, const int nCellNodes);

  // Retrieves the canonical node that contains node
  // This is is basically a dummy
  static CanonicalTraceToCell getCanonicalTraceLeft(const int *nodeNodes, const int nNodeNodes,
                                                    const int *lineNodes, const int nLineNodes,
                                                    int *canonicalNode, const int ncanonicalNode );

  static CanonicalTraceToCell getCanonicalTraceLeft(const std::vector<int>& nodeNodes,
                                                    const int *lineNodes, const int nLineNodes,
                                                    int *canonicalNode, const int ncanonicalNode )
  {
    return getCanonicalTraceLeft(nodeNodes.data(), nodeNodes.size(), lineNodes, nLineNodes, canonicalNode, ncanonicalNode);
  }
};

//----------------------------------------------------------------------------//
// cell reference coordinates (s,t) given edge reference coordinate (s)
/*
    2
    |\
    | \
    |  \
    |   \
    |    \
    | (0) \
    |      \
    0 ----- 1
*/
// parameters:
//  canonicalEdge      edge in triangle (E0=1-2, E1=2-0, E2=0-1); positive is CCW
/*
    3-------2
    |       |
    |       |
    |       |
    0 ----- 1
*/
// parameters:
//  canonicalEdge      edge in quad (E0=0-1, E1=2-2, E2=2-3, E3=3-0); positive orientation is CCW


template<class CellTopology>
struct TraceToCellRefCoord<Line, TopoD2, CellTopology>
{
  static const int TraceNodes[ CellTopology::NEdge ][ Line::NNode ];

  // Mapping to adjust the orientation of an area element to the canonical face of tetrahedron
  static const int OrientPos[ Line::NNode ]; // Positive orientations
  static const int OrientNeg[ Line::NNode ]; // Negative orientations

  static void
  eval( const CanonicalTraceToCell& canonicalEdge, const Real& sRef, Real& sRefArea, Real& tRefArea );
  static void
  eval( const CanonicalTraceToCell& canonicalEdge, const DLA::VectorS<1,Real>& sRefTrace, DLA::VectorS<2,Real>& sRefCell );
  static void
  eval( const CanonicalTraceToCell& canonicalEdge, const QuadraturePoint<TopoD1>& sRefTrace, QuadratureCellTracePoint<TopoD2>& sRefCell );

  // Retrieves the canonical edge that contains lineNodes
  static CanonicalTraceToCell getCanonicalTraceLeft(const int *lineNodes, const int nLineNodes,
                                                    const int *cellNodes, const int nCellNodes,
                                                    int *canonicalEdge, const int ncanonicalEdge);

  static CanonicalTraceToCell getCanonicalTraceLeft(const std::vector<int>& lineNodes,
                                                    const int *triNodes, const int nTriNodes,
                                                    int *canonicalEdge, const int ncanonicalEdge)
  {
    return getCanonicalTraceLeft(lineNodes.data(), lineNodes.size(), triNodes, nTriNodes, canonicalEdge, ncanonicalEdge);
  }

  static CanonicalTraceToCell getCanonicalTrace(const std::vector<int>& lineNodes,
                                                const std::vector<int>& triNodes)
  {
    return getCanonicalTrace(lineNodes.data(), lineNodes.size(), triNodes.data(), triNodes.size());
  }

  // Gets the canonical orientation of an edge
  static CanonicalTraceToCell getCanonicalTrace(const int *edgeNodes, const int nEdgeNodes,
                                                const int *cellNodes, const int nCellNodes);

};

//----------------------------------------------------------------------------//
// cell reference coordinates (s,t,u) given face reference coordinate (s,t)
/*
        t
        ^
        |
        2
        |\
        | \
        |  \
        |   \
        |    \
        | (0) \
        |      \
        0 ----- 1----> s
       /     .
      /   .
     / .
     3
   /
  u
*/
// parameters:
//  canonicalFace      face in tet (F0=1-2-3, F1=0-3-2, F2=0-1-3, F3=0-2-1); positive is outward normal
/*
//         t
//  3----------2
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   7------+---6
//  |   |  +-- |-- | -> s
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      u  \|
//      4----------5
 */
// parameters:
//  canonicalFace face in hex (F0=0-3-2-1, u-min
//                             F1=0-1-5-4, t-min
//                             F2=1-2-6-5, s-max
//                             F3=3-7-6-2, t-max
//                             F4=0-4-7-3, s-min
//                             F5=4-5-6-7  u-max ); positive is outward normal

template<class TraceTopology, class CellTopology>
struct TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>
{
  // A map that gives the nodes that define each canonical face of the cell
  static const int TraceNodes[ CellTopology::NFace ][ TraceTopology::NNode ];

  // A map from the edges of each canonical face of the cell, to the edge index of the cell itself
  static const int TraceEdges[ CellTopology::NFace ][ TraceTopology::NEdge ];

  // Mapping to adjust the orientation of an area element to the canonical face of the cell
  static const int OrientPos[ TraceTopology::NPermutation ][ TraceTopology::NNode ]; // Positive orientations
  static const int OrientNeg[ TraceTopology::NPermutation ][ TraceTopology::NNode ]; // Negative orientations

  static void
  eval( const CanonicalTraceToCell& canonicalFace, const Real& sRefArea, const Real& tRefArea, Real& sRefVol, Real& tRefVol, Real& uRefVol );
  static void
  eval( const CanonicalTraceToCell& canonicalFace, const DLA::VectorS<2,Real>& sRefTrace, DLA::VectorS<3,Real>& sRefCell );
  static void
  eval( const CanonicalTraceToCell& canonicalFace, const QuadraturePoint<TopoD2>& sRefTrace, QuadratureCellTracePoint<TopoD3>& sRefCell );

  // Retrieves the canonical face that contains traceNodes
  static CanonicalTraceToCell getCanonicalTraceLeft(const int *traceNodes, const int nTraceNodes,
                                                    const int *cellNodes, const int nCellNodes,
                                                    int *canonicalTrace, const int nCanonicalTrace);

  static CanonicalTraceToCell getCanonicalTraceLeft(const std::vector<int>& traceNodes,
                                                    const std::vector<int>& cellNodes,
                                                    int *canonicalTrace, const int nCanonicalTrace)
  {
    return getCanonicalTraceLeft(traceNodes.data(), traceNodes.size(), cellNodes.data(), cellNodes.size(), canonicalTrace, nCanonicalTrace);
  }

  static CanonicalTraceToCell getCanonicalTraceLeft(const std::vector<int>& traceNodes,
                                                    const int *cellNodes, const int nCellNodes,
                                                    int *canonicalTrace, const int nCanonicalTrace)
  {
    return getCanonicalTraceLeft(traceNodes.data(), traceNodes.size(), cellNodes, nCellNodes, canonicalTrace, nCanonicalTrace);
  }

  // Gets the canonical orientation of a face
  static CanonicalTraceToCell getCanonicalTrace(const int *traceNodes, const int nTraceNodes,
                                                const int *cellNodes, const int nCellNodes);

  static CanonicalTraceToCell getCanonicalTrace(const std::vector<int>& traceNodes,
                                                const std::vector<int>& cellNodes)
  {
    return getCanonicalTrace(traceNodes.data(), traceNodes.size(), cellNodes.data(), cellNodes.size());
  }

  static CanonicalTraceToCell getCanonicalTrace(const int *traceNodes, const int nTraceNodes,
                                                const std::vector<int>& cellNodes)
  {
    return getCanonicalTrace(traceNodes, nTraceNodes, cellNodes.data(), cellNodes.size());
  }

  // Gets the cell edge index given the canonical frame of a canonical trace (i.e. canonical edge of a canonical face in 3D)
  static int getCellEdge(const CanonicalTraceToCell& canonicalTrace, const int canonicalFrame );

};

template<class TraceTopology>
struct CanonicalOrientation<TraceTopology, TopoD3>
{
  static int get(const std::vector<int>& nodes, const std::vector<int>& canonicalNodes)
  {
    return get(nodes.data(), nodes.size(), canonicalNodes.data(), canonicalNodes.size());
  }

  static int get(const int *nodes, const int nNodes,
                 const int *canonicalNodes, const int nCanonicalNodes);
};

template<class TraceTopology, class CellTopology>
struct TraceToCellRefCoord<TraceTopology, TopoD4, CellTopology>
{
  // A map that gives the nodes that define each canonical face of the cell
  static const int TraceNodes[ CellTopology::NFace ][ TraceTopology::NNode ];

  // A map from the edges of each canonical face of the cell, to the edge index of the cell itself
  static const int TraceEdges[ CellTopology::NFace ][ TraceTopology::NEdge ];

  static const int OrientPos[ TraceTopology::NPermutation ][ TraceTopology::NNode ]; // Positive orientations
  static const int OrientNeg[ TraceTopology::NPermutation ][ TraceTopology::NNode ]; // Negative orientations

  // evaluation functions
  static void
  eval( const CanonicalTraceToCell& canonicalFace,
        const Real& sRefVol, const Real& tRefVol , const Real& uRevVol,
        Real& sRefSpacetime, Real& tRefSpacetime, Real& uRefSpacetime , Real& vRefSpacetime);
  static void
  eval( const CanonicalTraceToCell& canonicalFace, const DLA::VectorS<3,Real>& sRefTrace, DLA::VectorS<4,Real>& sRefCell );
  static void
  eval( const CanonicalTraceToCell& canonicalFace, const QuadraturePoint<TopoD3>& sRefTrace, QuadratureCellTracePoint<TopoD4>& sRefCell );

  // Retrieves the canonical face that contains traceNodes
  static CanonicalTraceToCell getCanonicalTraceLeft(const int *traceNodes, const int nTraceNodes,
                                                    const int *cellNodes, const int nCellNodes,
                                                    int *canonicalTrace, const int nCanonicalTrace);

  static CanonicalTraceToCell getCanonicalTraceLeft(const std::vector<int>& traceNodes,
                                                    const std::vector<int>& cellNodes,
                                                    int *canonicalTrace, const int nCanonicalTrace)
  {
    return getCanonicalTraceLeft(traceNodes.data(), traceNodes.size(), cellNodes.data(), cellNodes.size(), canonicalTrace, nCanonicalTrace);
  }

  static CanonicalTraceToCell getCanonicalTraceLeft(const std::vector<int>& traceNodes,
                                                    const int *cellNodes, const int nCellNodes,
                                                    int *canonicalTrace, const int nCanonicalTrace)
  {
    return getCanonicalTraceLeft(traceNodes.data(), traceNodes.size(), cellNodes, nCellNodes, canonicalTrace, nCanonicalTrace);
  }

  // Gets the canonical orientation of a face
  static CanonicalTraceToCell getCanonicalTrace(const int *traceNodes, const int nTraceNodes,
                                                const int *cellNodes, const int nCellNodes);

  static CanonicalTraceToCell getCanonicalTrace(const std::vector<int>& traceNodes,
                                                const std::vector<int>& cellNodes)
  {
    return getCanonicalTrace(traceNodes.data(), traceNodes.size(), cellNodes.data(), cellNodes.size());
  }

  static CanonicalTraceToCell getCanonicalTrace(const int *traceNodes, const int nTraceNodes,
                                                const std::vector<int>& cellNodes)
  {
    return getCanonicalTrace(traceNodes, nTraceNodes, cellNodes.data(), cellNodes.size());
  }

  // Gets the cell edge index given the canonical frame of a canonical trace (i.e. canonical edge of a canonical face in 3D)
  static int getCellEdge(const CanonicalTraceToCell& canonicalTrace, const int canonicalFrame );

};

template<class TraceTopology>
struct CanonicalOrientation<TraceTopology, TopoD4>
{
  static int get(const std::vector<int>& nodes, const std::vector<int>& canonicalNodes)
  {
    return get(nodes.data(), nodes.size(), canonicalNodes.data(), canonicalNodes.size());
  }

  static int get(const int *nodes, const int nNodes,
                 const int *canonicalNodes, const int nCanonicalNodes);
};


//----------------------------------------------------------------------------//
template<typename TopologyTrace, class TopoDimCell>
struct CellToTrace;

template<>
struct CellToTrace<Node,TopoD1>
{
  static CanonicalTraceToCell
  getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell );
};

template<>
struct CellToTrace<Line,TopoD2>
{
  static CanonicalTraceToCell
  getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell );
};

template<>
struct CellToTrace<Triangle,TopoD3>
{
  static CanonicalTraceToCell
  getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell );
};

template<>
struct CellToTrace<Quad,TopoD3>
{
  static CanonicalTraceToCell
  getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell );
};

template<>
struct CellToTrace<Tet,TopoD4>
{
  static CanonicalTraceToCell
  getCanonicalTrace( const TopologyTypes cellTopo, const std::vector<int>& trace, const std::vector<int>& cell );
};


// declare explicit instantiations
extern template struct TraceToCellRefCoord<Node, TopoD1, Line>;

extern template struct TraceToCellRefCoord<Line, TopoD2, Triangle>;
extern template struct TraceToCellRefCoord<Line, TopoD2, Quad>;

extern template struct TraceToCellRefCoord<Triangle, TopoD3, Tet>;
extern template struct TraceToCellRefCoord<Quad, TopoD3, Hex>;

extern template struct TraceToCellRefCoord<Tet,TopoD4,Pentatope>;

}

#endif  // TRACETOCELLREFCOORD_H
