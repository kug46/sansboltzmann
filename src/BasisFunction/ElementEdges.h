// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTEDGES_H
#define ELEMENTEDGES_H

#include <vector>
#include <array>

#include "Topology/ElementTopology.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class CellTopology>
struct ElementEdges
{
  // A map for node pairs that make up all edges of the cell
  static const int EdgeNodes[ CellTopology::NEdge ][ Line::NNode ];

  static int getCanonicalEdge( const int *edgeNodes, const int nEdgeNodes,
                               const int *cellNodes, const int nCellNodes);

  static int getCanonicalEdge( const std::vector<int>& edgeNodes,
                               const std::vector<int>& cellNodes )
  {
    return getCanonicalEdge( &edgeNodes[0], edgeNodes.size(), cellNodes.data(), cellNodes.size() );
  }

  static int getCanonicalEdge( const std::array<int,2>& edgeNodes,
                               const std::array<int,CellTopology::NNode>& cellNodes )
  {
    return getCanonicalEdge( &edgeNodes[0], edgeNodes.size(), cellNodes.data(), cellNodes.size() );
  }

  static int getCanonicalEdge( const std::array<int,2>& edgeNodes,
                               const std::vector<int>& cellNodes )
  {
    return getCanonicalEdge( edgeNodes.data(), edgeNodes.size(), cellNodes.data(), cellNodes.size() );
  }

  static int getCanonicalEdge( const int *edgeNodes, const int nEdgeNodes,
                               const std::vector<int>& cellNodes)
  {
    return getCanonicalEdge( edgeNodes, nEdgeNodes, cellNodes.data(), cellNodes.size() );
  }

  static int getCanonicalEdge( const std::vector<int>& edgeNodes,
                               const int *cellNodes, const int nCellNodes)
  {
    return getCanonicalEdge( edgeNodes.data(), edgeNodes.size(), cellNodes, nCellNodes );
  }

  static int getCanonicalEdge( const std::array<int,2>& edgeNodes,
                               const int *cellNodes, const int nCellNodes)
  {
    return getCanonicalEdge( edgeNodes.data(), edgeNodes.size(), cellNodes, nCellNodes );
  }
};

//===========================================================================//
// Runtime function to retrieve edges
void
elementEdges(const TopologyTypes topo, const int (*&EdgeNodes)[Line::NNode], int& nEdge);

}

#endif  // ELEMENTEDGES_H
