// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// line basis functions

#include <string>
#include <iostream>

#include "BasisFunctionLine.h"
#include "tools/SANSException.h"

#include "Quadrature/QuadratureLine.h"
#include "Quadrature_Cache.h"

#include "TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//Instantiate the singletons
const BasisFunctionLine<Hierarchical,1>* BasisFunctionLine<Hierarchical,1>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Hierarchical,2>* BasisFunctionLine<Hierarchical,2>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Hierarchical,3>* BasisFunctionLine<Hierarchical,3>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Hierarchical,4>* BasisFunctionLine<Hierarchical,4>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Hierarchical,5>* BasisFunctionLine<Hierarchical,5>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Hierarchical,6>* BasisFunctionLine<Hierarchical,6>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Hierarchical,7>* BasisFunctionLine<Hierarchical,7>::self() { static const BasisFunctionLine singleton; return &singleton; }

const BasisFunctionLine<Legendre,0>* BasisFunctionLine<Legendre,0>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Legendre,1>* BasisFunctionLine<Legendre,1>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Legendre,2>* BasisFunctionLine<Legendre,2>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Legendre,3>* BasisFunctionLine<Legendre,3>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Legendre,4>* BasisFunctionLine<Legendre,4>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Legendre,5>* BasisFunctionLine<Legendre,5>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Legendre,6>* BasisFunctionLine<Legendre,6>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Legendre,7>* BasisFunctionLine<Legendre,7>::self() { static const BasisFunctionLine singleton; return &singleton; }

const BasisFunctionLine<Lagrange,1>* BasisFunctionLine<Lagrange,1>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Lagrange,2>* BasisFunctionLine<Lagrange,2>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Lagrange,3>* BasisFunctionLine<Lagrange,3>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Lagrange,4>* BasisFunctionLine<Lagrange,4>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Lagrange,5>* BasisFunctionLine<Lagrange,5>::self() { static const BasisFunctionLine singleton; return &singleton; }

const BasisFunctionLine<Bernstein,0>* BasisFunctionLine<Bernstein,0>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Bernstein,1>* BasisFunctionLine<Bernstein,1>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Bernstein,2>* BasisFunctionLine<Bernstein,2>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Bernstein,3>* BasisFunctionLine<Bernstein,3>::self() { static const BasisFunctionLine singleton; return &singleton; }
const BasisFunctionLine<Bernstein,4>* BasisFunctionLine<Bernstein,4>::self() { static const BasisFunctionLine singleton; return &singleton; }

const BasisFunctionLine<RBS,1>* BasisFunctionLine<RBS,1>::self() { static const BasisFunctionLine singleton; return &singleton; }

//----------------------------------------------------------------------------//
//Initialize the base class pointers
const BasisFunctionLine<Hierarchical,1>* BasisFunctionLineBase::HierarchicalP1 = BasisFunctionLine<Hierarchical,1>::self();
const BasisFunctionLine<Hierarchical,2>* BasisFunctionLineBase::HierarchicalP2 = BasisFunctionLine<Hierarchical,2>::self();
const BasisFunctionLine<Hierarchical,3>* BasisFunctionLineBase::HierarchicalP3 = BasisFunctionLine<Hierarchical,3>::self();
const BasisFunctionLine<Hierarchical,4>* BasisFunctionLineBase::HierarchicalP4 = BasisFunctionLine<Hierarchical,4>::self();
const BasisFunctionLine<Hierarchical,5>* BasisFunctionLineBase::HierarchicalP5 = BasisFunctionLine<Hierarchical,5>::self();
const BasisFunctionLine<Hierarchical,6>* BasisFunctionLineBase::HierarchicalP6 = BasisFunctionLine<Hierarchical,6>::self();
const BasisFunctionLine<Hierarchical,7>* BasisFunctionLineBase::HierarchicalP7 = BasisFunctionLine<Hierarchical,7>::self();

const BasisFunctionLine<Legendre,0>* BasisFunctionLineBase::LegendreP0 = BasisFunctionLine<Legendre,0>::self();
const BasisFunctionLine<Legendre,1>* BasisFunctionLineBase::LegendreP1 = BasisFunctionLine<Legendre,1>::self();
const BasisFunctionLine<Legendre,2>* BasisFunctionLineBase::LegendreP2 = BasisFunctionLine<Legendre,2>::self();
const BasisFunctionLine<Legendre,3>* BasisFunctionLineBase::LegendreP3 = BasisFunctionLine<Legendre,3>::self();
const BasisFunctionLine<Legendre,4>* BasisFunctionLineBase::LegendreP4 = BasisFunctionLine<Legendre,4>::self();
const BasisFunctionLine<Legendre,5>* BasisFunctionLineBase::LegendreP5 = BasisFunctionLine<Legendre,5>::self();
const BasisFunctionLine<Legendre,6>* BasisFunctionLineBase::LegendreP6 = BasisFunctionLine<Legendre,6>::self();
const BasisFunctionLine<Legendre,7>* BasisFunctionLineBase::LegendreP7 = BasisFunctionLine<Legendre,7>::self();

const BasisFunctionLine<Lagrange,1>* BasisFunctionLineBase::LagrangeP1 = BasisFunctionLine<Lagrange,1>::self();
const BasisFunctionLine<Lagrange,2>* BasisFunctionLineBase::LagrangeP2 = BasisFunctionLine<Lagrange,2>::self();
const BasisFunctionLine<Lagrange,3>* BasisFunctionLineBase::LagrangeP3 = BasisFunctionLine<Lagrange,3>::self();
const BasisFunctionLine<Lagrange,4>* BasisFunctionLineBase::LagrangeP4 = BasisFunctionLine<Lagrange,4>::self();
const BasisFunctionLine<Lagrange,5>* BasisFunctionLineBase::LagrangeP5 = BasisFunctionLine<Lagrange,5>::self();

const BasisFunctionLine<Bernstein,0>* BasisFunctionLineBase::BernsteinP0 = BasisFunctionLine<Bernstein,0>::self();
const BasisFunctionLine<Bernstein,1>* BasisFunctionLineBase::BernsteinP1 = BasisFunctionLine<Bernstein,1>::self();
const BasisFunctionLine<Bernstein,2>* BasisFunctionLineBase::BernsteinP2 = BasisFunctionLine<Bernstein,2>::self();
const BasisFunctionLine<Bernstein,3>* BasisFunctionLineBase::BernsteinP3 = BasisFunctionLine<Bernstein,3>::self();
const BasisFunctionLine<Bernstein,4>* BasisFunctionLineBase::BernsteinP4 = BasisFunctionLine<Bernstein,4>::self();

const BasisFunctionLine<RBS,1>* BasisFunctionLineBase::RBSP1 = BasisFunctionLine<RBS,1>::self();

// The Hessians could be sped up using Horner Form for Serial, or Estrin's for Parallelism


//----------------------------------------------------------------------------//
const BasisFunctionLineBase*
BasisFunctionLineBase::getBasisFunction( const int order, const BasisFunctionCategory& category )
{

  if (category == BasisFunctionCategory_Hierarchical)
  {
    switch (order)
    {
    case 1:
      return BasisFunctionLine<Hierarchical,1>::self();
    case 2:
      return BasisFunctionLine<Hierarchical,2>::self();
    case 3:
      return BasisFunctionLine<Hierarchical,3>::self();
    case 4:
      return BasisFunctionLine<Hierarchical,4>::self();
    case 5:
      return BasisFunctionLine<Hierarchical,5>::self();
    case 6:
      return BasisFunctionLine<Hierarchical,6>::self();
    case 7:
      return BasisFunctionLine<Hierarchical,7>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionLine::getBasisFunction: unexpected hierarchical order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Legendre)
  {
    switch (order)
    {
    case 0:
      return BasisFunctionLine<Legendre,0>::self();
    case 1:
      return BasisFunctionLine<Legendre,1>::self();
    case 2:
      return BasisFunctionLine<Legendre,2>::self();
    case 3:
      return BasisFunctionLine<Legendre,3>::self();
    case 4:
      return BasisFunctionLine<Legendre,4>::self();
    case 5:
      return BasisFunctionLine<Legendre,5>::self();
    case 6:
      return BasisFunctionLine<Legendre,6>::self();
    case 7:
      return BasisFunctionLine<Legendre,7>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionLine::getBasisFunction: unexpected Legendre order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Lagrange)
  {
    switch (order)
    {
    case 1:
      return BasisFunctionLine<Lagrange,1>::self();
    case 2:
      return BasisFunctionLine<Lagrange,2>::self();
    case 3:
      return BasisFunctionLine<Lagrange,3>::self();
    case 4:
      return BasisFunctionLine<Lagrange,4>::self();
    case 5:
      return BasisFunctionLine<Lagrange,5>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionLine::getBasisFunction: unexpected Lagrange order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Bernstein)
  {
    switch (order)
    {
    case 0:
      return BasisFunctionLine<Bernstein,0>::self();
    case 1:
      return BasisFunctionLine<Bernstein,1>::self();
    case 2:
      return BasisFunctionLine<Bernstein,2>::self();
    case 3:
      return BasisFunctionLine<Bernstein,3>::self();
    case 4:
      return BasisFunctionLine<Bernstein,4>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionLine::getBasisFunction: unexpected Bernstein order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_RBS)
  {
    switch (order)
    {
;
    case 1:
      return BasisFunctionLine<RBS,1>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionLine::getBasisFunction: unexpected RBS order = %d\n", order );
      break;
    }
  }
  SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionLine::getBasisFunction: unexpected basis function category = %d\n",
                                             static_cast<int>(category) );

  //Just so the compiler will not complain
  return NULL;
}


//----------------------------------------------------------------------------//
void
BasisFunctionLineBase::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BasisFunctionLine: order = " << order()
                << "  nBasis = " << nBasis() << std::endl;
}

//===========================================================================//

template<class Topology>
QuadratureCache<Topology>::QuadratureCache()
{
  if ( BasisFunctionCategory_Hierarchical != 4 )
  {
    // Exceptions don't get printed during static initialization, probably a boost test thing
    std::cout << "You need to add the maximum order for the new basis" << std::endl;
    SANS_ASSERT(false);
  }

  BasisFunctionCategory categores[BasisFunctionCategory_Hierarchical];
  int Pmin[BasisFunctionCategory_Hierarchical];
  int Pmax[BasisFunctionCategory_Hierarchical];

  categores[BasisFunctionCategory_Legendre] = BasisFunctionCategory_Legendre;
  categores[BasisFunctionCategory_Lagrange] = BasisFunctionCategory_Lagrange;
  categores[BasisFunctionCategory_Bernstein] = BasisFunctionCategory_Bernstein;
  categores[BasisFunctionCategory_RBS] = BasisFunctionCategory_RBS;

  Pmin[BasisFunctionCategory_Legendre] = 0;
  Pmin[BasisFunctionCategory_Lagrange] = 1;
  Pmin[BasisFunctionCategory_Bernstein] = 0;
  Pmin[BasisFunctionCategory_RBS] = 1;

  Pmax[BasisFunctionCategory_Legendre] = BasisFunctionLine_LegendrePMax+1;
  Pmax[BasisFunctionCategory_Lagrange] = BasisFunctionLine_LagrangePMax+1;
  Pmax[BasisFunctionCategory_Bernstein] = BasisFunctionLine_BernsteinPMax+1;
  Pmax[BasisFunctionCategory_RBS] = BasisFunctionLine_RBSPMax+1;


  cell.resize(BasisFunctionCategory_Hierarchical);
  trace.resize(BasisFunctionCategory_Hierarchical);
  for (int icat = 0; icat < BasisFunctionCategory_Hierarchical; icat++)
  {
    BasisFunctionCategory cat = categores[icat];

    cell[cat].resize(Pmax[cat]);
    trace[cat].resize(Pmax[cat]);
    for (int poly_order = Pmin[cat]; poly_order < Pmax[cat]; poly_order++)
    {
//      const BasisFunctionLineBase* basis = BasisFunctionLineBase::getBasisFunction(poly_order, cat);
//      const int nBasis = basis->nBasis();

      QuadratureBasisPointStore<D>& pointStore = cell[cat][poly_order];
      pointStore.category = cat;
      pointStore.poly_order = poly_order;
      pointStore.eval.resize(QuadratureRule::eNone);

      pointStore.eval[QuadratureRule::eGauss].resize(QuadratureLine::nOrderIdx);
#if 0
      for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
      {
        QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );
        pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

        Real s;
        for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
        {
          QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

          point.phi.resize(nBasis);
          point.dphi[0].resize(nBasis);

          quadrature.coordinate(iquad, s);
          basis->evalBasis( s, point.phi.data(), point.phi.size() );

          basis->evalBasisDerivative( s, point.dphi[0].data(),
                                      point.dphi[0].size() );
        } // iquad
      } // orderidx
#endif

      trace[cat][poly_order].resize(Line::NTrace);
      for (int itrace = 0; itrace < Line::NTrace; itrace++)
      {
        QuadratureBasisPointStore<D>& pointStore = trace[cat][poly_order][itrace];
        pointStore.category = cat;
        pointStore.poly_order = poly_order;
        pointStore.eval.resize(QuadratureRule::eNone);

        CanonicalTraceToCell canonicalTrace(itrace, 1);

        pointStore.eval[QuadratureRule::eGauss].resize(1); // orderidx
#if 0
        pointStore.eval[QuadratureRule::eGauss][0].resize(1); // nQuadrature

        QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][0][0];

        point.phi.resize(basis->nBasis());
        point.dphi[0].resize(basis->nBasis());

        Real sCell;
        TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalTrace, sCell );

        basis->evalBasis( sCell, point.phi.data(), point.phi.size() );

        basis->evalBasisDerivative( sCell, point.dphi[0].data(),
                                    point.dphi[0].size() );
#endif
      } // itrace

    } // poly_order
  } // icat


  //-------------------------------------------------------------------------//
  // Hierarchical basis functions are special with the edge sign
  //-------------------------------------------------------------------------//

  // Node signs are actually not needed for Lines, but this allows for the same data structures
  const int nNodeSign = 1;

  IntTrace nodeSigns[nNodeSign] = { {{ 1, 1}} };

  hierarchicalCell.resize(BasisFunctionLine_HierarchicalPMax+1);
  hierarchicalTrace.resize(BasisFunctionLine_HierarchicalPMax+1);
  for (int poly_order = 1; poly_order < BasisFunctionLine_HierarchicalPMax+1; poly_order++)
  {
//    const BasisFunctionLineBase* basis = BasisFunctionLineBase::getBasisFunction(poly_order, BasisFunctionCategory_Hierarchical);
//    const int nBasis = basis->nBasis();

    for (int isgn = 0; isgn < nNodeSign; isgn++)
    {
      IntTrace sgn = nodeSigns[isgn];
      QuadratureBasisPointStore<D>& pointStore = hierarchicalCell[poly_order][sgn];
      pointStore.category = BasisFunctionCategory_Hierarchical;
      pointStore.poly_order = poly_order;
      pointStore.eval.resize(QuadratureRule::eNone);

      pointStore.eval[QuadratureRule::eGauss].resize(QuadratureLine::nOrderIdx);
#if 0
      for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
      {
        QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );
        pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

        Real s;
        for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
        {
          QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

          point.phi.resize(nBasis);
          point.dphi[0].resize(nBasis);

          quadrature.coordinate(iquad, s);
          basis->evalBasis( s, point.phi.data(), point.phi.size() );

          basis->evalBasisDerivative( s, point.dphi[0].data(),
                                         point.dphi[0].size() );
        } //iqaud
      } // orderidx
#endif

      hierarchicalTrace[poly_order][sgn].resize(Line::NTrace);
      for (int itrace = 0; itrace < Line::NTrace; itrace++)
      {
        QuadratureBasisPointStore<D>& pointStore = hierarchicalTrace[poly_order][sgn][itrace];
        pointStore.category = BasisFunctionCategory_Hierarchical;
        pointStore.poly_order = poly_order;
        pointStore.eval.resize(QuadratureRule::eNone);

        CanonicalTraceToCell canonicalTrace(itrace, 1);

        pointStore.eval[QuadratureRule::eGauss].resize(1); // orderidx
#if 0
        pointStore.eval[QuadratureRule::eGauss][0].resize(1); // nQuadrature

        QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][0][0];

        point.phi.resize(nBasis);
        point.dphi[0].resize(nBasis);

        Real sCell;
        TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalTrace, sCell );

        basis->evalBasis( sCell, point.phi.data(), point.phi.size() );

        basis->evalBasisDerivative( sCell, point.dphi[0].data(),
                                    point.dphi[0].size() );
#endif
      } // itrace

    } // isgn
  } // poly_order

  // traceOrientMap is not needed for Line
}

//----------------------------------------------------------------------------//

template<class Topology>
void
QuadratureCache<Topology>::
fillCellQuadrature(const QuadraturePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreCell)
{
  BasisFunctionCategory cat = pointStoreCell.category;
  int poly_order = pointStoreCell.poly_order;

  QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(ref.orderidx) );

  std::vector<QuadratureBasisPointValues<D>>& points = pointStoreCell.eval[ref.rule][ref.orderidx];
  SANS_ASSERT(points.size() == 0);
  points.resize( quadrature.nQuadrature() );

  const BasisFunctionLineBase* basis =
      BasisFunctionLineBase::getBasisFunction(poly_order, cat);
  const int nBasis = basis->nBasis();

  Real s;
  for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
  {
    QuadratureBasisPointValues<D>& point = points[iquad];

    point.phi.resize(nBasis);
    point.dphi[0].resize(nBasis);
    point.d2phi[0].resize(nBasis);

    quadrature.coordinate(iquad, s);
    basis->evalBasis( s, point.phi.data(), point.phi.size() );

    Real *phis = point.dphi[0].data();
    basis->evalBasisDerivative( s, phis, nBasis );

    Real *phiss = point.d2phi[0].data();
    try
    {
      basis->evalBasisHessianDerivative( s, phiss, nBasis );
    }
    catch (const DeveloperException& e)
    {
      // hessian not implemented, hopefully it's not needed...
      point.d2phi[0].clear(); point.d2phi[0].shrink_to_fit();
    }
  } // iquad
}

//----------------------------------------------------------------------------//

template<class Topology>
void
QuadratureCache<Topology>::
fillTraceQuadrature(const QuadratureCellTracePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreTrace)
{
  BasisFunctionCategory cat = pointStoreTrace.category;
  int poly_order = pointStoreTrace.poly_order;

  std::vector<QuadratureBasisPointValues<D>>& points = pointStoreTrace.eval[QuadratureRule::eGauss][ref.orderidx];
  SANS_ASSERT(points.size() == 0);
  points.resize( 1 );

  const BasisFunctionLineBase* basis = BasisFunctionLineBase::getBasisFunction(poly_order, cat);

  QuadratureBasisPointValues<D>& point = pointStoreTrace.eval[QuadratureRule::eGauss][0][0];

  CanonicalTraceToCell canonicalTrace(ref.canonicalTrace.trace, 1);

  point.phi.resize(basis->nBasis());
  point.dphi[0].resize(basis->nBasis());

  Real sCell;
  TraceToCellRefCoord<Node, TopoD1, Line>::eval( canonicalTrace, sCell );

  basis->evalBasis( sCell, point.phi.data(), point.phi.size() );

  basis->evalBasisDerivative( sCell, point.dphi[0].data(),
                              point.dphi[0].size() );
}

// instantiate the class and singleton
template<class Topology>
const QuadratureCache<Topology> QuadratureCache<Topology>::cache;

// The QuadratureCache singleton must be instantiated in this translation unit (cpp file) to guarantee
// that the basis function singletons are initialized before the QuadratureCache
template struct QuadratureCache<Line>;

} // namespace SANS

// instantiate the standard library classes
template class std::vector<SANS::QuadratureBasisPointValues<1>>;
template class std::vector<std::vector<SANS::QuadratureBasisPointValues<1>>>;
template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointValues<1>>>>;

template class std::map<std::array<int,2>,SANS::QuadratureBasisPointStore<1>>;
template class std::vector<std::map<std::array<int,2>,SANS::QuadratureBasisPointStore<1>>>;
template class std::vector<std::map<std::array<int,2>,std::vector<SANS::QuadratureBasisPointStore<1>>>>;

template class std::vector<SANS::QuadratureBasisPointStore<1>>;
template class std::vector<std::vector<SANS::QuadratureBasisPointStore<1>>>;
template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointStore<1>>>>;

template class std::vector<SANS::TraceOrientMap>;
