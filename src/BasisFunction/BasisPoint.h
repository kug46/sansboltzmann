// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISPOINT_H
#define BASISPOINT_H

#include "tools/SANSnumerics.h"

#include "BasisFunctionBase.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for representing a basis function evaluated at a point
//
// template parameters:
//   D:  number of parametric coordinates
//
//----------------------------------------------------------------------------//

template<int D>
class BasisPoint
{
public:
  BasisPoint( const BasisPoint& ) = delete;
  BasisPoint& operator=( const BasisPoint& ) = delete;

  explicit BasisPoint( const BasisFunctionBase* basis ) : nphi_(basis->nBasis()), phi_(new Real[nphi_]) {}
  ~BasisPoint()
  {
    delete [] phi_;
  }

        SANS::DLA::VectorS<D,Real>& ref()       { return Ref_; }
  const SANS::DLA::VectorS<D,Real>& ref() const { return Ref_; }

  operator       Real*()       { return phi_; }
  operator const Real*() const { return phi_; }

  int size() const { return nphi_; }

protected:
  const int nphi_;
  Real* phi_;
  SANS::DLA::VectorS<D,Real> Ref_;
};

}

#endif //BASISPOINT_H
