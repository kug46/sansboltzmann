// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionArea_Quad_Bernstein.h"

#include <cmath>  // sqrt
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Bernstein: constant

void
BasisFunctionArea<Quad,Bernstein,1>::evalBasis( const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phi[0] = 1;
}


void
BasisFunctionArea<Quad,Bernstein,1>::evalBasisDerivative(
    const Real&, const Real&, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phis[0] = 0;
  phit[0] = 0;
}


void
BasisFunctionArea<Quad,Bernstein,1>::evalBasisHessianDerivative(
    const Real&, const Real&, const Int4&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phiss[0] = 0;
  phist[0] = 0;
  phitt[0] = 0;

}

}
