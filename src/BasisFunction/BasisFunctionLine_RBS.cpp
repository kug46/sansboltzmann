// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <cmath>

#include "BasisFunctionLine_RBS.h"
#include "tools/SANSException.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// RBS: linear

void
BasisFunctionLine<RBS,1>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phi[0] = 1 - s;
  phi[1] =     s;
}

void
BasisFunctionLine<RBS,1>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phis[0] = -1;
  phis[1] =  1;
}

void
BasisFunctionLine<RBS,1>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phiss[0] = 0;
  phiss[1] = 0;
}

}
