// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONLINE_LAGRANGE_H
#define BASISFUNCTIONLINE_LAGRANGE_H

// line basis functions
// implemented:
//   Lagrange: P1 - P5

// NOTES:
// - Implemented via abstract base class (ABC)
// - Derived classes only contain static data. Hence, they are
//   constructed to be singletons. Using templates instead of ABC
//   would lead to code bloat


#include <iostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "BasisFunctionLineBase.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// line basis functions
//
// reference line element defined: s in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edge interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinate
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionLine_LagrangePMax = 5;

// Lagrange basis functions

template<class Topology>
struct LagrangeNodes;

template<>
struct LagrangeNodes<Line>
{
  static const int PMax = BasisFunctionLine_LagrangePMax;

  static void get(const int order, std::vector<DLA::VectorS<TopoD1::D,Real>>& sRef);
};

void getLagrangeNodes_Line(const int order, std::vector<Real>& s);

//----------------------------------------------------------------------------//
// Lagrange: linear
//
// orthonormal
// basis functions normalized to give unit L2 norm over s in [0, 1]
template<>
class BasisFunctionLine<Lagrange,1> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 1; }
  virtual int nBasis() const override { return 2; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 0; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

  void coordinates( std::vector<Real>& s ) const;

protected:
  //Singleton!
  BasisFunctionLine() {}
  static const std::vector<Real> coords_s_;
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Lagrange: quadratic
//
// orthonormal
// basis functions normalized to give unit L2 norm over s in [0, 1]
template<>
class BasisFunctionLine<Lagrange,2> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 2; }
  virtual int nBasis() const override { return 3; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 1; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

  void coordinates( std::vector<Real>& s ) const;

protected:
  //Singleton!
  BasisFunctionLine() {}
  static const std::vector<Real> coords_s_;
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Lagrange: cubic
//
// orthonormal
// basis functions normalized to give unit L2 norm over s in [0, 1]
template<>
class BasisFunctionLine<Lagrange,3> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 3; }
  virtual int nBasis() const override { return 4; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 2; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

  void coordinates( std::vector<Real>& s ) const;

protected:
  //Singleton!
  BasisFunctionLine() {}
  static const std::vector<Real> coords_s_;
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Lagrange: quartic
//
// orthonormal
// basis functions normalized to give unit L2 norm over s in [0, 1]
template<>
class BasisFunctionLine<Lagrange,4> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 4; }
  virtual int nBasis() const override { return 5; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 3; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

  void coordinates( std::vector<Real>& s ) const;

protected:
  //Singleton!
  BasisFunctionLine() {}
  static const std::vector<Real> coords_s_;
public:
  static const BasisFunctionLine* self();
};


//----------------------------------------------------------------------------//
// Lagrange: quintic
//
// orthonormal
// basis functions normalized to give unit L2 norm over s in [0, 1]
template<>
class BasisFunctionLine<Lagrange,5> : public BasisFunctionLineBase
{
public:
  using BasisFunctionLineBase::D;     // physical dimensions

  virtual ~BasisFunctionLine() {}

  virtual int order() const override { return 5; }
  virtual int nBasis() const override { return 6; }
  virtual int nBasisNode() const override { return 2; }
  virtual int nBasisEdge() const override { return 4; }
  virtual BasisFunctionCategory category() const override { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const override;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const override;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const override;

  void coordinates( std::vector<Real>& s ) const;

protected:
  //Singleton!
  BasisFunctionLine() {}
  static const std::vector<Real> coords_s_;
public:
  static const BasisFunctionLine* self();
};

}

#endif  // BASISFUNCTIONLINE_LAGRANGE_H
