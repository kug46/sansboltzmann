// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONAREA_QUAD_RBS_H
#define BASISFUNCTIONAREA_QUAD_RBS_H

// quad basis functions: RBS

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionLine.h


#include <array>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionArea.h"
#include "BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// area basis functions: RBS
//
// reference triangle element defined: s in [0, 1], t in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionArea_Quad_RBSPMax = 1;


//----------------------------------------------------------------------------//
// RBS: constant

template <>
class BasisFunctionArea<Quad,RBS,1> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 1; }
  virtual int nBasisNode() const { return 0; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_RBS; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const IntNedge& sgn, Real phi[], int nphi) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};

}

#endif  // BASISFUNCTIONAREA_QUAD_RBS_H
