// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// reference element operators

#include <sstream> // std::stringstream

#include "ElementFrame.h"
#include "tools/SANSException.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// The frame node map is defined in TraceToCellRefCoord_*.cpp
//
// template<class CellTopology>
// const int ElementEdges<CellTopology>::EdgeNodes
//
//
//  See TraceToCellRefCoord cpp files for Edge numbering data
//
//----------------------------------------------------------------------------//

template<class CellTopology>
int
ElementFrame<CellTopology>::getCanonicalFrame( const int *frameNodes, const int nFrameNodes,
                                           const int *cellNodes, const int nCellNodes)
{
  typedef typename CellTopology::TopologyFrame FrameTopology;

  SANS_ASSERT(nFrameNodes == FrameTopology::NNode);
  SANS_ASSERT(nCellNodes == CellTopology::NNode);

  for (int i = 0; i < CellTopology::NFrame; i++)
  {
    // inspecting the cell Node triplets as taken from the list above
    if ( ( cellNodes[FrameNodes[i][0]] == frameNodes[0] && cellNodes[FrameNodes[i][1]] == frameNodes[1]
            && cellNodes[FrameNodes[i][2]] == frameNodes[2] ) ||
         ( cellNodes[FrameNodes[i][0]] == frameNodes[1] && cellNodes[FrameNodes[i][1]] == frameNodes[0]
            && cellNodes[FrameNodes[i][2]] == frameNodes[2] ) ||
         ( cellNodes[FrameNodes[i][0]] == frameNodes[0] && cellNodes[FrameNodes[i][1]] == frameNodes[2]
            && cellNodes[FrameNodes[i][2]] == frameNodes[1] ) ||
         ( cellNodes[FrameNodes[i][0]] == frameNodes[2] && cellNodes[FrameNodes[i][1]] == frameNodes[1]
            && cellNodes[FrameNodes[i][2]] == frameNodes[0] ) ||
         ( cellNodes[FrameNodes[i][0]] == frameNodes[2] && cellNodes[FrameNodes[i][1]] == frameNodes[0]
            && cellNodes[FrameNodes[i][2]] == frameNodes[1] ) ||
         ( cellNodes[FrameNodes[i][0]] == frameNodes[1] && cellNodes[FrameNodes[i][1]] == frameNodes[2]
            && cellNodes[FrameNodes[i][2]] == frameNodes[0] )
    )
    {
      // this is the frame
      return i;
    }
  }

  std::stringstream msg;
  msg << "Cannot find Cell canonical frame." << std::endl;

  msg << "Cell nodes = { ";
  for (int n = 0; n < nCellNodes -1; n++)
    msg << cellNodes[n] << ", ";
  msg << cellNodes[nCellNodes-1] << " } " << std::endl;

  msg << "Frame nodes = { ";
  for (int n = 0; n < nFrameNodes-1; n++)
    msg << frameNodes[n] << ", ";
  msg << frameNodes[nFrameNodes-1] << " } " << std::endl;

  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return -1;
}



// Explicitly instantiate the class
template struct ElementFrame<Pentatope>;

} //namespace SANS
