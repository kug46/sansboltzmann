// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionVolume_Hexahedron_Lagrange.h"
#include "tools/SANSException.h"

// Construct tensor products with the linear Lagrange basis
#include "BasisFunctionLine_Lagrange.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

#define O3 1./3. // One over 3
#define T3 2./3. // Two over 3

#define Q4 1./4. // Quarter
#define H2 1./2. // Half
#define T4 3./4. // Three quarters

#define COORD(P, c) const std::vector<Real> BasisFunctionVolume<Hex,Lagrange,P>::coords_ ## c ## _

/*
// SANS numbering
//
//  Hexahedron Q1               Hexahedron Q2
//
//         t
//  3----------2                3-----9----2
//  |\     ^   |\               |\         |\
//  | \    |   | \              |18    23  | 15
//  |  \   |   |  \             8  \ 20    10 \
//  |   7------+---6            |   7----19+---6
//  |   |  +-- |-- | -> s       |24 |  26  | 22|
//  0---+---\--1   |            0---+-11---1   |
//   \  |    \  \  |             \ 17    25 \  16
//    \ |     \  \ |             14 |  21    12|
//     \|      u  \|               \|         \|
//      4----------5                4----13----5
*/

COORD(1, s) = {0, 1, 1, 0, 0, 1, 1, 0};
COORD(1, t) = {0, 0, 1, 1, 0, 0, 1, 1};
COORD(1, u) = {0, 0, 0, 0, 1, 1, 1, 1};

      //SANS:  0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26
COORD(2, s) = {0., 1., 1., 0., 0., 1., 1., 0., 0., H2, 1., H2, 1., H2, 0., 1., 1., 0., 0., H2, H2, H2, 1., H2, 0., H2, H2};
COORD(2, t) = {0., 0., 1., 1., 0., 0., 1., 1., H2, 1., H2, 0., 0., 0., 0., 1., H2, H2, 1., 1., H2, 0., H2, 1., H2, H2, H2};
COORD(2, u) = {0., 0., 0., 0., 1., 1., 1., 1., 0., 0., 0., 0., H2, 1., H2, H2, 1., 1., H2, 1., 0., H2, H2, H2, H2, 1., H2};
      //       N0  N1  N2  N3  N4  N5  N6  N7  E0  E1  E2  E3  E4  E5  E6  E7  E8  E9  E10 E11 F0  F1  F2  F3  F4  F5  C0


/*
   SANS numbering

    Hexahedron Q3

       F0
  3--10--11--2
  |          |
  9  33  34  12
  |          |
  8  32  35  13
  |          |
  0--15--14--1
                     F3
                 3--10--11--2
                  \          \
                   28  44  47  22
                      \          \
                       29  45  46  23
                          \          \
     3                     7--30--31--6       2
     |\                                       |\
     9  28                                   12  22
 F4  |    \           59  58                  |    \     F2
     8  51  29                               13  41  23
     |        \       56  57                  |        \
     0  48  50  7                             1  40  42  6
      \         |           63  62             \         |
        21  49  27                               16  43  24
          \     |           60  61                 \     |
            20  26                                   17  25
              \ |                                      \ |
                4         0--15--14--1                   5
                           \          \
                            21  36  37  16
                               \          \
                                20  39  38  17
                                   \          \
                                     4--19--18--5
                                          F1
                                                   F5
                                              7--30--31--6
                                              |          |
                                             27  55  54  24
                                              |          |
                                             26  52  53  25
                                              |          |
                                              4--19--18--5
*/

      //SANS:  0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44  45  46  47  48  49  50  51  52  53  54  55  56  57  58  59  60  61  62  63
COORD(3, s) = {0., 1., 1., 0., 0., 1., 1., 0., 0., 0., O3, T3, 1., 1., T3, O3, 1., 1., T3, O3, 0., 0., 1., 1., 1., 1., 0., 0., 0., 0., O3, T3, O3, O3, T3, T3, O3, T3, T3, O3, 1., 1., 1., 1., O3, O3, T3, T3, 0., 0., 0., 0., O3, T3, T3, O3, O3, T3, T3, O3, O3, T3, T3, O3};
COORD(3, t) = {0., 0., 1., 1., 0., 0., 1., 1., O3, T3, 1., 1., T3, O3, 0., 0., 0., 0., 0., 0., 0., 0., 1., 1., T3, O3, O3, T3, 1., 1., 1., 1., O3, T3, T3, O3, 0., 0., 0., 0., O3, T3, T3, O3, 1., 1., 1., 1., O3, O3, T3, T3, O3, O3, T3, T3, O3, O3, T3, T3, O3, O3, T3, T3};
COORD(3, u) = {0., 0., 0., 0., 1., 1., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0., O3, T3, 1., 1., T3, O3, O3, T3, 1., 1., 1., 1., O3, T3, 1., 1., 0., 0., 0., 0., O3, O3, T3, T3, O3, O3, T3, T3, O3, T3, T3, O3, O3, T3, T3, O3, 1., 1., 1., 1., O3, O3, O3, O3, T3, T3, T3, T3};
//             |   |   |   |   |   |   |   |   |----|  |----|  |----|  |----|  |----|  |----|  |----|  |----|  |----|  |----|  |----|  |----|  |------------|  |------------|  |------------|  |------------|  |------------|  |------------|  |----------------------------|
//             N0  N1  N2  N3  N4  N5  N6  N7    E0      E1      E2      E3      E4      E5      E6      E7      E8      E9      E10     E11         F0              F1              F2              F3              F4              F5                   Cell


/*
   SANS numbering

    Hexahedron Q4

       F0
  3--11--12--13--2
  |              |
  10 45  49  46  14
  |              |
  9  48  34  50  15
  |              |
  8  44  51  47  16
  |              |
  0--19--18--17--1
                     F3
                 3--11--12--13--2
                  \              \
                   38  71 78 74  29
                    \              \
                     39  75 79 77  30
                      \              \
                       40  72 76 73  31
                        \              \
                         7--41--42--43--6

     3                                       2
     |\                                      |\
    10 38                                   14 29
 F4  |   \                 (u = 1/4)         |   \    F2
     9 83 39          101   108  100        15 63 30
     |      \                                |      \
     8 87 86 40       109   110  107        16 66 67 31
     |         \                             |         \
     0 80 88 82 7     98    107   99         0 62 70 64 7
      \         |          (u = 2/4)          \         |
       28 84 85 37      119  122  118          20 69 68 32
         \      |                                \      |
          27 81 36      123  124  121             21 65 33
            \   |                                   \   |
             26 35      116  120  117                22 34
               \|           (u = 3/4)                  \|
                4        105  113  104                  5

                         114  115  112

                         102  111  103



                         0--19--18--17--1
                          \              \
                           28  53 57 54  20
                            \              \
                             27  60 61 58  21
                              \              \
                               26  55 59 55  22
                                \              \
                                 4--25--24--23--5
                                          F1
                                                   F5
                                            7--41--42--43--6
                                            |              |
                                           37  92  95  91  32
                                            |              |
                                           36  96  97  94  33
                                            |              |
                                           35  89  93  90  34
                                            |              |
                                            4--25--24--23--5
*/

      //SANS:  0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44  45  46  47  48  49  50  51  52  53  54  55  56  57  58  59  60  61  62  63  64  65  66  67  68  69  70  71  72  73  74  75  76  77  78  79  80  81  82  83  84  85  86  87  88  89  90  91  92  93  94  95  96  97  98  99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124
COORD(4, s) = {0., 1., 1., 0., 0., 1., 1., 0., 0., 0., 0., Q4, H2, T4, 1., 1., 1., T4, H2, Q4, 1., 1., 1., T4, H2, Q4, 0., 0., 0., 1., 1., 1., 1., 1., 1., 0., 0., 0., 0., 0., 0., Q4, H2, T4, Q4, Q4, T4, T4, Q4, H2, T4, H2, H2, Q4, T4, T4, Q4, H2, T4, H2, Q4, H2, 1., 1., 1., 1., 1., 1., 1., 1., 1., Q4, Q4, T4, T4, Q4, H2, T4, H2, H2, 0., 0., 0., 0., 0., 0., 0., 0., 0., Q4, T4, T4, Q4, H2, T4, H2, Q4, H2, Q4, T4, T4, Q4, Q4, T4, T4, Q4, H2, T4, H2, Q4, H2, H2, T4, H2, Q4, H2, Q4, T4, T4, Q4, H2, T4, H2, Q4, H2};
COORD(4, t) = {0., 0., 1., 1., 0., 0., 1., 1., Q4, H2, T4, 1., 1., 1., T4, H2, Q4, 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 1., 1., 1., T4, H2, Q4, Q4, H2, T4, 1., 1., 1., 1., 1., 1., Q4, T4, T4, Q4, H2, T4, H2, Q4, H2, 0., 0., 0., 0., 0., 0., 0., 0., 0., Q4, T4, T4, Q4, H2, T4, H2, Q4, H2, 1., 1., 1., 1., 1., 1., 1., 1., 1., Q4, Q4, T4, T4, Q4, H2, T4, H2, H2, Q4, Q4, T4, T4, Q4, H2, T4, H2, H2, Q4, Q4, T4, T4, Q4, Q4, T4, T4, Q4, H2, T4, H2, H2, Q4, H2, T4, H2, H2, Q4, Q4, T4, T4, Q4, H2, T4, H2, H2};
COORD(4, u) = {0., 0., 0., 0., 1., 1., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., Q4, H2, T4, 1., 1., 1., T4, H2, Q4, Q4, H2, T4, 1., 1., 1., 1., 1., 1., Q4, H2, T4, 1., 1., 1., 0., 0., 0., 0., 0., 0., 0., 0., 0., Q4, Q4, T4, T4, Q4, H2, T4, H2, H2, Q4, Q4, T4, T4, Q4, H2, T4, H2, H2, Q4, T4, T4, Q4, H2, T4, H2, Q4, H2, Q4, T4, T4, Q4, H2, T4, H2, Q4, H2, 1., 1., 1., 1., 1., 1., 1., 1., 1., Q4, Q4, Q4, Q4, T4, T4, T4, T4, Q4, Q4, Q4, Q4, Q4, T4, T4, T4, T4, T4, H2, H2, H2, H2, H2, H2, H2, H2, H2};
//             |   |   |   |   |   |   |   |   |--------|  |--------|  |-------|   |-------|   |--------|  |--------|  |--------|  |--------|  |--------|  |--------|  |--------|  |--------|  |---------------------------------|  |--------------------------------|  |-------------------------------|  |---------------------------------|  |--------------------------------|  |--------------------------------|  |---------------------------------------------------------------------------------------------------------|
//             N0  N1  N2  N3  N4  N5  N6  N7      E0          E1          E2         E3           E4          E5          E6          E7          E8          E9          E10         E11                    F0                                   F1                                F2                                 F3                                   F4                                  F5                                                                           Cell

void getLagrangeNodes_Hex(const int order, std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u)
{
  switch (order)
  {
    case 1:
    {
      const BasisFunctionVolume<Hex,Lagrange,1>* basisP1 = BasisFunctionVolume<Hex,Lagrange,1>::self();
      basisP1->coordinates(s, t, u);
      break;
    }
    case 2:
    {
      const BasisFunctionVolume<Hex,Lagrange,2>* basisP2 = BasisFunctionVolume<Hex,Lagrange,2>::self();
      basisP2->coordinates(s, t, u);
      break;
    }
    case 3:
    {
      const BasisFunctionVolume<Hex,Lagrange,3>* basisP3 = BasisFunctionVolume<Hex,Lagrange,3>::self();
      basisP3->coordinates(s, t, u);
      break;
    }
    case 4:
    {
      const BasisFunctionVolume<Hex,Lagrange,4>* basisP4 = BasisFunctionVolume<Hex,Lagrange,4>::self();
      basisP4->coordinates(s, t, u);
      break;
    }
    default:
      SANS_DEVELOPER_EXCEPTION( "getLagrangeNodes_Hex - Unsupported order = %d.", order );
  }
}

//---------------------------------------------------------------------------//
const int LagrangeNodes<Hex>::PMax;

//---------------------------------------------------------------------------//
void
LagrangeNodes<Hex>::
get(const int order, std::vector<DLA::VectorS<TopoD3::D,Real>>& sRef)
{
  std::vector<Real> s, t, u;
  getLagrangeNodes_Hex(order, s, t, u);
  sRef.resize(s.size());

  for (std::size_t i = 0; i < s.size(); i++)
  {
    sRef[i][0] = s[i];
    sRef[i][1] = t[i];
    sRef[i][2] = u[i];
  }
}

//----------------------------------------------------------------------------//
// Lagrange: linear
void
BasisFunctionVolume<Hex,Lagrange,1>::tensorProduct(
    const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const
{
  // *phi[0] @ 0 , *phi[1] @ 1

  phi[0] = sphi[0]*tphi[0]*uphi[0]; // 1 @ node 0 (s = 0, t = 0, u = 0)
  phi[1] = sphi[1]*tphi[0]*uphi[0]; // 1 @ node 1 (s = 1, t = 0, u = 0)
  phi[2] = sphi[1]*tphi[1]*uphi[0]; // 1 @ node 2 (s = 1, t = 1, u = 0)
  phi[3] = sphi[0]*tphi[1]*uphi[0]; // 1 @ node 3 (s = 0, t = 1, u = 0)

  phi[4] = sphi[0]*tphi[0]*uphi[1]; // 1 @ node 4 (s = 0, t = 0, u = 1)
  phi[5] = sphi[1]*tphi[0]*uphi[1]; // 1 @ node 5 (s = 1, t = 0, u = 1)
  phi[6] = sphi[1]*tphi[1]*uphi[1]; // 1 @ node 6 (s = 1, t = 1, u = 1)
  phi[7] = sphi[0]*tphi[1]*uphi[1]; // 1 @ node 7 (s = 0, t = 1, u = 1)
}

void
BasisFunctionVolume<Hex,Lagrange,1>::evalBasis( const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real sphi[2], tphi[2], uphi[2];
  BasisFunctionLine<Lagrange,1>::self()->evalBasis(s, sphi, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasis(t, tphi, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasis(u, uphi, 2);

  tensorProduct(sphi, tphi, uphi, phi);
}


void
BasisFunctionVolume<Hex,Lagrange,1>::evalBasisDerivative( const Real& s, const Real& t, const Real& u, const Int6&,
                                                          Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real sphi[2], tphi[2], uphi[2];
  BasisFunctionLine<Lagrange,1>::self()->evalBasis(s, sphi, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasis(t, tphi, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasis(u, uphi, 2);

  Real sphis[2], tphit[2], uphiu[2];
  BasisFunctionLine<Lagrange,1>::self()->evalBasisDerivative(s, sphis, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasisDerivative(t, tphit, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasisDerivative(u, uphiu, 2);

  tensorProduct(sphis, tphi , uphi , phis);
  tensorProduct(sphi , tphit, uphi , phit);
  tensorProduct(sphi , tphi , uphiu, phiu);
}

void
BasisFunctionVolume<Hex,Lagrange,1>::evalBasisHessianDerivative( const Real& s, const Real& t, const Real& u, const Int6& sgn,
                                                                 Real phiss[],
                                                                 Real phist[], Real phitt[],
                                                                 Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real sphi[2], tphi[2], uphi[2];
  BasisFunctionLine<Lagrange,1>::self()->evalBasis(s, sphi, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasis(t, tphi, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasis(u, uphi, 2);

  Real sphis[2], tphit[2], uphiu[2];
  BasisFunctionLine<Lagrange,1>::self()->evalBasisDerivative(s, sphis, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasisDerivative(t, tphit, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasisDerivative(u, uphiu, 2);

  Real sphiss[2], tphitt[2], uphiuu[2];
  BasisFunctionLine<Lagrange,1>::self()->evalBasisHessianDerivative(s, sphiss, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasisHessianDerivative(t, tphitt, 2);
  BasisFunctionLine<Lagrange,1>::self()->evalBasisHessianDerivative(u, uphiuu, 2);

  tensorProduct(sphiss, tphi  , uphi  , phiss);
  tensorProduct(sphis , tphit , uphi  , phist);
  tensorProduct(sphi  , tphitt, uphi  , phitt);
  tensorProduct(sphis , tphi  , uphiu , phisu);
  tensorProduct(sphi  , tphit , uphiu , phitu);
  tensorProduct(sphi  , tphi  , uphiuu, phiuu);
}

void
BasisFunctionVolume<Hex,Lagrange,1>::coordinates( std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u ) const
{
  s = coords_s_;
  t = coords_t_;
  u = coords_u_;
}

//----------------------------------------------------------------------------//
// Lagrange: Quadratic

void
BasisFunctionVolume<Hex,Lagrange,2>::tensorProduct(
    const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const
{
  // *phi[0] @ 0 , *phi[1] @ 1 , *phi[2] @ 1/2

  phi[0] = sphi[0]*tphi[0]*uphi[0]; // 1 @ node 0 (s = 0, t = 0, u = 0)
  phi[1] = sphi[1]*tphi[0]*uphi[0]; // 1 @ node 1 (s = 1, t = 0, u = 0)
  phi[2] = sphi[1]*tphi[1]*uphi[0]; // 1 @ node 2 (s = 1, t = 1, u = 0)
  phi[3] = sphi[0]*tphi[1]*uphi[0]; // 1 @ node 3 (s = 0, t = 1, u = 0)

  phi[4] = sphi[0]*tphi[0]*uphi[1]; // 1 @ node 4 (s = 0, t = 0, u = 1)
  phi[5] = sphi[1]*tphi[0]*uphi[1]; // 1 @ node 5 (s = 1, t = 0, u = 1)
  phi[6] = sphi[1]*tphi[1]*uphi[1]; // 1 @ node 6 (s = 1, t = 1, u = 1)
  phi[7] = sphi[0]*tphi[1]*uphi[1]; // 1 @ node 7 (s = 0, t = 1, u = 1)

  phi[8]  = sphi[0]*tphi[2]*uphi[0]; // 1 @ node 8  (s = 0.0, t = 0.5, u = 0.0)  E0
  phi[9]  = sphi[2]*tphi[1]*uphi[0]; // 1 @ node 9  (s = 0.5, t = 1.0, u = 0.0)  E1
  phi[10] = sphi[1]*tphi[2]*uphi[0]; // 1 @ node 10 (s = 1.0, t = 0.5, u = 0.0)  E2
  phi[11] = sphi[2]*tphi[0]*uphi[0]; // 1 @ node 11 (s = 0.5, t = 0.0, u = 0.0)  E3
  phi[12] = sphi[1]*tphi[0]*uphi[2]; // 1 @ node 12 (s = 1.0, t = 0.0, u = 0.5)  E4
  phi[13] = sphi[2]*tphi[0]*uphi[1]; // 1 @ node 13 (s = 0.5, t = 0.0, u = 1.0)  E5
  phi[14] = sphi[0]*tphi[0]*uphi[2]; // 1 @ node 14 (s = 1.0, t = 0.0, u = 0.5)  E6
  phi[15] = sphi[1]*tphi[1]*uphi[2]; // 1 @ node 15 (s = 1.0, t = 1.0, u = 0.5)  E7
  phi[16] = sphi[1]*tphi[2]*uphi[1]; // 1 @ node 16 (s = 1.0, t = 0.5, u = 1.0)  E8
  phi[17] = sphi[0]*tphi[2]*uphi[1]; // 1 @ node 17 (s = 0.0, t = 0.5, u = 1.0)  E9
  phi[18] = sphi[0]*tphi[1]*uphi[2]; // 1 @ node 18 (s = 0.0, t = 1.0, u = 0.5)  E10
  phi[19] = sphi[2]*tphi[1]*uphi[1]; // 1 @ node 19 (s = 0.5, t = 1.0, u = 1.0)  E11

  phi[20] = sphi[2]*tphi[2]*uphi[0]; // 1 @ node 20 (s = 0.5, t = 0.5, u = 0.0)  F0
  phi[21] = sphi[2]*tphi[0]*uphi[2]; // 1 @ node 21 (s = 0.5, t = 0.0, u = 0.5)  F1
  phi[22] = sphi[1]*tphi[2]*uphi[2]; // 1 @ node 22 (s = 1.0, t = 0.5, u = 0.5)  F2
  phi[23] = sphi[2]*tphi[1]*uphi[2]; // 1 @ node 23 (s = 0.5, t = 1.0, u = 0.5)  F3
  phi[24] = sphi[0]*tphi[2]*uphi[2]; // 1 @ node 24 (s = 0.0, t = 0.5, u = 0.5)  F4
  phi[25] = sphi[2]*tphi[2]*uphi[1]; // 1 @ node 25 (s = 0.5, t = 0.5, u = 1.0)  F5

  phi[26] = sphi[2]*tphi[2]*uphi[2]; // 1 @ node 26 (s = 0.5, t = 0.5, u = 0.5)  C0
}

void
BasisFunctionVolume<Hex,Lagrange,2>::evalBasis(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 27);

  Real sphi[3], tphi[3], uphi[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(t, tphi, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(u, uphi, 3);

  tensorProduct(sphi, tphi, uphi, phi);
}


void
BasisFunctionVolume<Hex,Lagrange,2>::evalBasisDerivative(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 27);

  Real sphi[3], tphi[3], uphi[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(t, tphi, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(u, uphi, 3);

  Real sphis[3], tphit[3], uphiu[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(s, sphis, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(t, tphit, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(u, uphiu, 3);


  tensorProduct(sphis, tphi , uphi, phis);
  tensorProduct(sphi , tphit, uphi, phit);
  tensorProduct(sphi , tphi, uphiu, phiu);
}

void
BasisFunctionVolume<Hex,Lagrange,2>::evalBasisHessianDerivative(const Real& s, const Real& t, const Real& u, const Int6& sgn,
                                                                Real phiss[],
                                                                Real phist[], Real phitt[],
                                                                Real phisu[], Real phitu[], Real phiuu[], int nphi) const
{
  SANS_ASSERT(nphi == 27);

  Real sphi[3], tphi[3], uphi[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(t, tphi, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(u, uphi, 3);

  Real sphis[3], tphit[3], uphiu[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(s, sphis, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(t, tphit, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(u, uphiu, 3);

  Real sphiss[3], tphitt[3], uphiuu[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(s, sphiss, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(t, tphitt, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(u, uphiuu, 3);

  tensorProduct(sphiss, tphi  , uphi  , phiss);
  tensorProduct(sphis , tphit , uphi  , phist);
  tensorProduct(sphi  , tphitt, uphi  , phitt);
  tensorProduct(sphis , tphi  , uphiu , phisu);
  tensorProduct(sphi  , tphit , uphiu , phitu);
  tensorProduct(sphi  , tphi  , uphiuu, phiuu);
}

void
BasisFunctionVolume<Hex,Lagrange,2>::coordinates( std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u ) const
{
  s = coords_s_;
  t = coords_t_;
  u = coords_u_;
}

//----------------------------------------------------------------------------//
// Lagrange: Cubic

void
BasisFunctionVolume<Hex,Lagrange,3>::tensorProduct(
    const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const
{
  // *phi[0] @ 0 , *phi[1] @ 1 , *phi[2] @ 1/3 , *phi[3] @ 2/3

  phi[0] = sphi[0]*tphi[0]*uphi[0]; // 1 @ node 0 (s = 0, t = 0, u = 0)
  phi[1] = sphi[1]*tphi[0]*uphi[0]; // 1 @ node 1 (s = 1, t = 0, u = 0)
  phi[2] = sphi[1]*tphi[1]*uphi[0]; // 1 @ node 2 (s = 1, t = 1, u = 0)
  phi[3] = sphi[0]*tphi[1]*uphi[0]; // 1 @ node 3 (s = 0, t = 1, u = 0)

  phi[4] = sphi[0]*tphi[0]*uphi[1]; // 1 @ node 4 (s = 0, t = 0, u = 1)
  phi[5] = sphi[1]*tphi[0]*uphi[1]; // 1 @ node 5 (s = 1, t = 0, u = 1)
  phi[6] = sphi[1]*tphi[1]*uphi[1]; // 1 @ node 6 (s = 1, t = 1, u = 1)
  phi[7] = sphi[0]*tphi[1]*uphi[1]; // 1 @ node 7 (s = 0, t = 1, u = 1)

  // Edge 0 (s = 0,         u = 0)
  phi[8]  = sphi[0]*tphi[2]*uphi[0]; // 1 @ node 8 (s = 0.0, t = 1/3, u = 0.0)
  phi[9]  = sphi[0]*tphi[3]*uphi[0]; // 1 @ node 9 (s = 0.0, t = 2/3, u = 0.0)

  // Edge 1 (       t = 1,  u = 0)
  phi[10] = sphi[2]*tphi[1]*uphi[0]; // 1 @ node 10 (s = 1/3, t = 1.0, u = 0.0)
  phi[11] = sphi[3]*tphi[1]*uphi[0]; // 1 @ node 11 (s = 2/3, t = 1.0, u = 0.0)

  // Edge 2 (s = 1,         u = 0)
  phi[12] = sphi[1]*tphi[3]*uphi[0]; // 1 @ node 12 (s = 1.0, t = 2/3, u = 0.0)
  phi[13] = sphi[1]*tphi[2]*uphi[0]; // 1 @ node 13 (s = 1.0, t = 1/3, u = 0.0)

  // Edge 3 (       t = 0,  u = 0)
  phi[14] = sphi[3]*tphi[0]*uphi[0]; // 1 @ node 14 (s = 2/3, t = 0.0, u = 0.0)
  phi[15] = sphi[2]*tphi[0]*uphi[0]; // 1 @ node 15 (s = 1/3, t = 0.0, u = 0.0)

  // Edge 4 (s = 1, t = 0        )
  phi[16] = sphi[1]*tphi[0]*uphi[2]; // 1 @ node 16 (s = 1.0, t = 0.0, u = 1/3)
  phi[17] = sphi[1]*tphi[0]*uphi[3]; // 1 @ node 17 (s = 1.0, t = 0.0, u = 2/3)

  // Edge 5 (t = 0,         u = 1)
  phi[18] = sphi[3]*tphi[0]*uphi[1]; // 1 @ node 18 (s = 2/3, t = 0.0, u = 1.0)
  phi[19] = sphi[2]*tphi[0]*uphi[1]; // 1 @ node 19 (s = 1/3, t = 0.0, u = 1.0)

  // Edge 6 (s = 0,  t = 0       )
  phi[20] = sphi[0]*tphi[0]*uphi[3]; // 1 @ node 20 (s = 0.0, t = 0.0, u = 2/3)
  phi[21] = sphi[0]*tphi[0]*uphi[2]; // 1 @ node 21 (s = 0.0, t = 0.0, u = 1/3)

  // Edge 7 (s = 1,  t = 1       )
  phi[22] = sphi[1]*tphi[1]*uphi[2]; // 1 @ node 22 (s = 1.0, t = 1.0, u = 1/3)
  phi[23] = sphi[1]*tphi[1]*uphi[3]; // 1 @ node 23 (s = 1.0, t = 1.0, u = 2/3)

  // Edge 8 (s = 1,         u = 1)
  phi[24] = sphi[1]*tphi[3]*uphi[1]; // 1 @ node 24 (s = 1.0, t = 2/3, u = 1.0)
  phi[25] = sphi[1]*tphi[2]*uphi[1]; // 1 @ node 25 (s = 1.0, t = 1/3, u = 1.0)

  // Edge 9 (s = 0,         u = 1)
  phi[26] = sphi[0]*tphi[2]*uphi[1]; // 1 @ node 26 (s = 0.0, t = 1/3, u = 1.0)
  phi[27] = sphi[0]*tphi[3]*uphi[1]; // 1 @ node 27 (s = 0.0, t = 2/3, u = 1.0)

  // Edge 10 (s = 0,        t = 1)
  phi[28] = sphi[0]*tphi[1]*uphi[2]; // 1 @ node 28 (s = 0.0, t = 1.0, u = 1/3)
  phi[29] = sphi[0]*tphi[1]*uphi[3]; // 1 @ node 29 (s = 0.0, t = 1.0, u = 2/3)

  // Edge 11 (      t = 1,  u = 1)
  phi[30] = sphi[2]*tphi[1]*uphi[1]; // 1 @ node 30 (s = 1/3, t = 1.0, u = 1.0)
  phi[31] = sphi[3]*tphi[1]*uphi[1]; // 1 @ node 31 (s = 2/3, t = 1.0, u = 1.0)

  // Face 0 (u = 0)
  phi[32] = sphi[2]*tphi[2]*uphi[0]; // 1 @ node 32 (s = 1/3, t = 1/3, u = 0.0)
  phi[33] = sphi[2]*tphi[3]*uphi[0]; // 1 @ node 33 (s = 1/3, t = 2/3, u = 0.0)
  phi[34] = sphi[3]*tphi[3]*uphi[0]; // 1 @ node 34 (s = 2/3, t = 2/3, u = 0.0)
  phi[35] = sphi[3]*tphi[2]*uphi[0]; // 1 @ node 35 (s = 2/3, t = 1/3, u = 0.0)

  // Face 1 (t = 0)
  phi[36] = sphi[2]*tphi[0]*uphi[2]; // 1 @ node 36 (s = 1/3, t = 0.0, u = 1/3)
  phi[37] = sphi[3]*tphi[0]*uphi[2]; // 1 @ node 37 (s = 2/3, t = 0.0, u = 1/3)
  phi[38] = sphi[3]*tphi[0]*uphi[3]; // 1 @ node 38 (s = 2/3, t = 0.0, u = 2/3)
  phi[39] = sphi[2]*tphi[0]*uphi[3]; // 1 @ node 39 (s = 1/3, t = 0.0, u = 2/3)

  // Edge 2 (s = 1)
  phi[40] = sphi[1]*tphi[2]*uphi[2]; // 1 @ node 40 (s = 1.0, t = 1/3, u = 1/3)
  phi[41] = sphi[1]*tphi[3]*uphi[2]; // 1 @ node 41 (s = 1.0, t = 2/3, u = 1/3)
  phi[42] = sphi[1]*tphi[3]*uphi[3]; // 1 @ node 42 (s = 1.0, t = 2/3, u = 2/3)
  phi[43] = sphi[1]*tphi[2]*uphi[3]; // 1 @ node 43 (s = 1.0, t = 1/3, u = 2/3)

  // Face 3 (t = 1)
  phi[44] = sphi[2]*tphi[1]*uphi[2]; // 1 @ node 44 (s = 1/3, t = 1.0, u = 1/3)
  phi[45] = sphi[2]*tphi[1]*uphi[3]; // 1 @ node 45 (s = 1/3, t = 1.0, u = 2/3)
  phi[46] = sphi[3]*tphi[1]*uphi[3]; // 1 @ node 46 (s = 2/3, t = 1.0, u = 2/3)
  phi[47] = sphi[3]*tphi[1]*uphi[2]; // 1 @ node 47 (s = 2/3, t = 1.0, u = 1/3)

  // Face 4 (s = 0)
  phi[48] = sphi[0]*tphi[2]*uphi[2]; // 1 @ node 48 (s = 0.0, t = 1/3, u = 1/3)
  phi[49] = sphi[0]*tphi[2]*uphi[3]; // 1 @ node 49 (s = 0.0, t = 1/3, u = 2/3)
  phi[50] = sphi[0]*tphi[3]*uphi[3]; // 1 @ node 50 (s = 0.0, t = 2/3, u = 2/3)
  phi[51] = sphi[0]*tphi[3]*uphi[2]; // 1 @ node 51 (s = 0.0, t = 2/3, u = 1/3)

  // Face 5 (u = 1)
  phi[52] = sphi[2]*tphi[2]*uphi[1]; // 1 @ node 52 (s = 1/3, t = 1/3, u = 1.0)
  phi[53] = sphi[3]*tphi[2]*uphi[1]; // 1 @ node 53 (s = 2/3, t = 1/3, u = 1.0)
  phi[54] = sphi[3]*tphi[3]*uphi[1]; // 1 @ node 54 (s = 2/3, t = 2/3, u = 1.0)
  phi[55] = sphi[2]*tphi[3]*uphi[1]; // 1 @ node 55 (s = 1/3, t = 2/3, u = 1.0)

  // Cell
  phi[56] = sphi[2]*tphi[2]*uphi[2]; // 1 @ node 56 (s = 1/3, t = 1/3, u = 1/3)
  phi[57] = sphi[3]*tphi[2]*uphi[2]; // 1 @ node 57 (s = 2/3, t = 1/3, u = 1/3)
  phi[58] = sphi[3]*tphi[3]*uphi[2]; // 1 @ node 58 (s = 2/3, t = 2/3, u = 1/3)
  phi[59] = sphi[2]*tphi[3]*uphi[2]; // 1 @ node 59 (s = 1/3, t = 2/3, u = 1/3)
  phi[60] = sphi[2]*tphi[2]*uphi[3]; // 1 @ node 60 (s = 1/3, t = 1/3, u = 2/3)
  phi[61] = sphi[3]*tphi[2]*uphi[3]; // 1 @ node 61 (s = 2/3, t = 1/3, u = 2/3)
  phi[62] = sphi[3]*tphi[3]*uphi[3]; // 1 @ node 62 (s = 2/3, t = 2/3, u = 2/3)
  phi[63] = sphi[2]*tphi[3]*uphi[3]; // 1 @ node 63 (s = 1/3, t = 2/3, u = 2/3)
}

void
BasisFunctionVolume<Hex,Lagrange,3>::evalBasis(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 64);

  Real sphi[4], tphi[4], uphi[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(t, tphi, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(u, uphi, 4);

  tensorProduct(sphi, tphi, uphi, phi);
}


void
BasisFunctionVolume<Hex,Lagrange,3>::evalBasisDerivative(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 64);

  Real sphi[4], tphi[4], uphi[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(t, tphi, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(u, uphi, 4);

  Real sphis[4], tphit[4], uphiu[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(s, sphis, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(t, tphit, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(u, uphiu, 4);


  tensorProduct(sphis, tphi , uphi, phis);
  tensorProduct(sphi , tphit, uphi, phit);
  tensorProduct(sphi , tphi, uphiu, phiu);
}

void
BasisFunctionVolume<Hex,Lagrange,3>::evalBasisHessianDerivative(const Real& s, const Real& t, const Real& u, const Int6& sgn,
                                                                Real phiss[],
                                                                Real phist[], Real phitt[],
                                                                Real phisu[], Real phitu[], Real phiuu[], int nphi) const
{
  SANS_ASSERT(nphi == 64);

  Real sphi[4], tphi[4], uphi[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(t, tphi, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(u, uphi, 4);

  Real sphis[4], tphit[4], uphiu[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(s, sphis, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(t, tphit, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(u, uphiu, 4);

  Real sphiss[4], tphitt[4], uphiuu[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(s, sphiss, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(t, tphitt, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(u, uphiuu, 4);

  tensorProduct(sphiss, tphi  , uphi  , phiss);
  tensorProduct(sphis , tphit , uphi  , phist);
  tensorProduct(sphi  , tphitt, uphi  , phitt);
  tensorProduct(sphis , tphi  , uphiu , phisu);
  tensorProduct(sphi  , tphit , uphiu , phitu);
  tensorProduct(sphi  , tphi  , uphiuu, phiuu);
}

void
BasisFunctionVolume<Hex,Lagrange,3>::coordinates( std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u ) const
{
  s = coords_s_;
  t = coords_t_;
  u = coords_u_;
}

//----------------------------------------------------------------------------//
// Lagrange: 4th order

void
BasisFunctionVolume<Hex,Lagrange,4>::tensorProduct(
    const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const
{
  // *phi[0] @ 0 , *phi[1] @ 1 , *phi[2] @ 1/4 , *phi[3] @ 2/4 , *phi[4] @ 3/4

  phi[0] = sphi[0]*tphi[0]*uphi[0]; // 1 @ node 0 (s = 0, t = 0, u = 0)
  phi[1] = sphi[1]*tphi[0]*uphi[0]; // 1 @ node 1 (s = 1, t = 0, u = 0)
  phi[2] = sphi[1]*tphi[1]*uphi[0]; // 1 @ node 2 (s = 1, t = 1, u = 0)
  phi[3] = sphi[0]*tphi[1]*uphi[0]; // 1 @ node 3 (s = 0, t = 1, u = 0)

  phi[4] = sphi[0]*tphi[0]*uphi[1]; // 1 @ node 4 (s = 0, t = 0, u = 1)
  phi[5] = sphi[1]*tphi[0]*uphi[1]; // 1 @ node 5 (s = 1, t = 0, u = 1)
  phi[6] = sphi[1]*tphi[1]*uphi[1]; // 1 @ node 6 (s = 1, t = 1, u = 1)
  phi[7] = sphi[0]*tphi[1]*uphi[1]; // 1 @ node 7 (s = 0, t = 1, u = 1)

  // Edge 0 (s = 0,         u = 0)
  phi[8]  = sphi[0]*tphi[2]*uphi[0]; // 1 @ node 8 (s = 0.0, t = 1/4, u = 0.0)
  phi[9]  = sphi[0]*tphi[3]*uphi[0]; // 1 @ node 9 (s = 0.0, t = 2/4, u = 0.0)
  phi[10] = sphi[0]*tphi[4]*uphi[0]; // 1 @ node 10 (s = 0.0, t = 3/4, u = 0.0)

  // Edge 1 (       t = 1,  u = 0)
  phi[11] = sphi[2]*tphi[1]*uphi[0]; // 1 @ node 11 (s = 1/4, t = 1.0, u = 0.0)
  phi[12] = sphi[3]*tphi[1]*uphi[0]; // 1 @ node 12 (s = 2/4, t = 1.0, u = 0.0)
  phi[13] = sphi[4]*tphi[1]*uphi[0]; // 1 @ node 13 (s = 3/4, t = 1.0, u = 0.0)

  // Edge 2 (s = 1,         u = 0)
  phi[14] = sphi[1]*tphi[4]*uphi[0]; // 1 @ node 14 (s = 1.0, t = 3/4, u = 0.0)
  phi[15] = sphi[1]*tphi[3]*uphi[0]; // 1 @ node 15 (s = 1.0, t = 2/4, u = 0.0)
  phi[16] = sphi[1]*tphi[2]*uphi[0]; // 1 @ node 16 (s = 1.0, t = 1/4, u = 0.0)

  // Edge 3 (       t = 0,  u = 0)
  phi[17] = sphi[4]*tphi[0]*uphi[0]; // 1 @ node 17 (s = 3/4, t = 0.0, u = 0.0)
  phi[18] = sphi[3]*tphi[0]*uphi[0]; // 1 @ node 18 (s = 2/4, t = 0.0, u = 0.0)
  phi[19] = sphi[2]*tphi[0]*uphi[0]; // 1 @ node 19 (s = 1/4, t = 0.0, u = 0.0)

  // Edge 4 (s = 1, t = 0        )
  phi[20] = sphi[1]*tphi[0]*uphi[2]; // 1 @ node 20 (s = 1.0, t = 0.0, u = 1/4)
  phi[21] = sphi[1]*tphi[0]*uphi[3]; // 1 @ node 21 (s = 1.0, t = 0.0, u = 2/4)
  phi[22] = sphi[1]*tphi[0]*uphi[4]; // 1 @ node 22 (s = 1.0, t = 0.0, u = 3/4)

  // Edge 5 (t = 0,         u = 1)
  phi[23] = sphi[4]*tphi[0]*uphi[1]; // 1 @ node 23 (s = 3/4, t = 0.0, u = 1.0)
  phi[24] = sphi[3]*tphi[0]*uphi[1]; // 1 @ node 24 (s = 2/4, t = 0.0, u = 1.0)
  phi[25] = sphi[2]*tphi[0]*uphi[1]; // 1 @ node 25 (s = 1/4, t = 0.0, u = 1.0)

  // Edge 6 (s = 0,  t = 0       )
  phi[26] = sphi[0]*tphi[0]*uphi[4]; // 1 @ node 26 (s = 0.0, t = 0.0, u = 3/4)
  phi[27] = sphi[0]*tphi[0]*uphi[3]; // 1 @ node 27 (s = 0.0, t = 0.0, u = 2/4)
  phi[28] = sphi[0]*tphi[0]*uphi[2]; // 1 @ node 28 (s = 0.0, t = 0.0, u = 1/4)

  // Edge 7 (s = 1,  t = 1       )
  phi[29] = sphi[1]*tphi[1]*uphi[2]; // 1 @ node 29 (s = 1.0, t = 1.0, u = 1/4)
  phi[30] = sphi[1]*tphi[1]*uphi[3]; // 1 @ node 30 (s = 1.0, t = 1.0, u = 2/4)
  phi[31] = sphi[1]*tphi[1]*uphi[4]; // 1 @ node 31 (s = 1.0, t = 1.0, u = 3/4)

  // Edge 8 (s = 1,         u = 1)
  phi[32] = sphi[1]*tphi[4]*uphi[1]; // 1 @ node 32 (s = 1.0, t = 3/4, u = 1.0)
  phi[33] = sphi[1]*tphi[3]*uphi[1]; // 1 @ node 33 (s = 1.0, t = 2/4, u = 1.0)
  phi[34] = sphi[1]*tphi[2]*uphi[1]; // 1 @ node 34 (s = 1.0, t = 1/4, u = 1.0)

  // Edge 9 (s = 0,         u = 1)
  phi[35] = sphi[0]*tphi[2]*uphi[1]; // 1 @ node 35 (s = 0.0, t = 1/4, u = 1.0)
  phi[36] = sphi[0]*tphi[3]*uphi[1]; // 1 @ node 36 (s = 0.0, t = 2/4, u = 1.0)
  phi[37] = sphi[0]*tphi[4]*uphi[1]; // 1 @ node 37 (s = 0.0, t = 3/4, u = 1.0)

  // Edge 10 (s = 0,        t = 1)
  phi[38] = sphi[0]*tphi[1]*uphi[2]; // 1 @ node 38 (s = 0.0, t = 1.0, u = 1/4)
  phi[39] = sphi[0]*tphi[1]*uphi[3]; // 1 @ node 39 (s = 0.0, t = 1.0, u = 2/4)
  phi[40] = sphi[0]*tphi[1]*uphi[4]; // 1 @ node 40 (s = 0.0, t = 1.0, u = 3/4)

  // Edge 11 (      t = 1,  u = 1)
  phi[41] = sphi[2]*tphi[1]*uphi[1]; // 1 @ node 41 (s = 1/4, t = 1.0, u = 1.0)
  phi[42] = sphi[3]*tphi[1]*uphi[1]; // 1 @ node 42 (s = 2/4, t = 1.0, u = 1.0)
  phi[43] = sphi[4]*tphi[1]*uphi[1]; // 1 @ node 43 (s = 3/4, t = 1.0, u = 1.0)

  // Face 0 (u = 0)
  phi[44] = sphi[2]*tphi[2]*uphi[0]; // 1 @ node 44 (s = 1/4, t = 1/4, u = 0.0)
  phi[45] = sphi[2]*tphi[4]*uphi[0]; // 1 @ node 45 (s = 1/4, t = 3/4, u = 0.0)
  phi[46] = sphi[4]*tphi[4]*uphi[0]; // 1 @ node 46 (s = 3/4, t = 3/4, u = 0.0)
  phi[47] = sphi[4]*tphi[2]*uphi[0]; // 1 @ node 47 (s = 3/4, t = 1/4, u = 0.0)
  phi[48] = sphi[2]*tphi[3]*uphi[0]; // 1 @ node 48 (s = 1/4, t = 2/4, u = 0.0)
  phi[49] = sphi[3]*tphi[4]*uphi[0]; // 1 @ node 49 (s = 2/4, t = 3/4, u = 0.0)
  phi[50] = sphi[4]*tphi[3]*uphi[0]; // 1 @ node 50 (s = 3/4, t = 2/4, u = 0.0)
  phi[51] = sphi[3]*tphi[2]*uphi[0]; // 1 @ node 51 (s = 2/4, t = 1/4, u = 0.0)
  phi[52] = sphi[3]*tphi[3]*uphi[0]; // 1 @ node 52 (s = 2/4, t = 2/4, u = 0.0)

  // Face 1 (t = 0)
  phi[53] = sphi[2]*tphi[0]*uphi[2]; // 1 @ node 53 (s = 1/4, t = 0.0, u = 1/4)
  phi[54] = sphi[4]*tphi[0]*uphi[2]; // 1 @ node 54 (s = 3/4, t = 0.0, u = 1/4)
  phi[55] = sphi[4]*tphi[0]*uphi[4]; // 1 @ node 55 (s = 3/4, t = 0.0, u = 3/4)
  phi[56] = sphi[2]*tphi[0]*uphi[4]; // 1 @ node 56 (s = 1/4, t = 0.0, u = 3/4)
  phi[57] = sphi[3]*tphi[0]*uphi[2]; // 1 @ node 57 (s = 2/4, t = 0.0, u = 1/4)
  phi[58] = sphi[4]*tphi[0]*uphi[3]; // 1 @ node 58 (s = 3/4, t = 0.0, u = 2/4)
  phi[59] = sphi[3]*tphi[0]*uphi[4]; // 1 @ node 59 (s = 2/4, t = 0.0, u = 3/4)
  phi[60] = sphi[2]*tphi[0]*uphi[3]; // 1 @ node 60 (s = 1/4, t = 0.0, u = 2/4)
  phi[61] = sphi[3]*tphi[0]*uphi[3]; // 1 @ node 61 (s = 2/4, t = 0.0, u = 2/4)

  // Face 2 (s = 1)
  phi[62] = sphi[1]*tphi[2]*uphi[2]; // 1 @ node 62 (s = 1.0, t = 1/4, u = 1/4)
  phi[63] = sphi[1]*tphi[4]*uphi[2]; // 1 @ node 63 (s = 1.0, t = 3/4, u = 1/4)
  phi[64] = sphi[1]*tphi[4]*uphi[4]; // 1 @ node 64 (s = 1.0, t = 3/4, u = 3/4)
  phi[65] = sphi[1]*tphi[2]*uphi[4]; // 1 @ node 65 (s = 1.0, t = 1/4, u = 3/4)
  phi[66] = sphi[1]*tphi[3]*uphi[2]; // 1 @ node 66 (s = 1.0, t = 2/4, u = 1/4)
  phi[67] = sphi[1]*tphi[4]*uphi[3]; // 1 @ node 67 (s = 1.0, t = 3/4, u = 2/4)
  phi[68] = sphi[1]*tphi[3]*uphi[4]; // 1 @ node 68 (s = 1.0, t = 2/4, u = 3/4)
  phi[69] = sphi[1]*tphi[2]*uphi[3]; // 1 @ node 69 (s = 1.0, t = 1/4, u = 2/4)
  phi[70] = sphi[1]*tphi[3]*uphi[3]; // 1 @ node 70 (s = 1.0, t = 2/4, u = 2/4)

  // Face 3 (t = 1)
  phi[71] = sphi[2]*tphi[1]*uphi[2]; // 1 @ node 71 (s = 1/4, t = 1.0, u = 1/4)
  phi[72] = sphi[2]*tphi[1]*uphi[4]; // 1 @ node 72 (s = 1/4, t = 1.0, u = 3/4)
  phi[73] = sphi[4]*tphi[1]*uphi[4]; // 1 @ node 73 (s = 3/4, t = 1.0, u = 3/4)
  phi[74] = sphi[4]*tphi[1]*uphi[2]; // 1 @ node 74 (s = 3/4, t = 1.0, u = 1/4)
  phi[75] = sphi[2]*tphi[1]*uphi[3]; // 1 @ node 75 (s = 1/4, t = 1.0, u = 2/4)
  phi[76] = sphi[3]*tphi[1]*uphi[4]; // 1 @ node 76 (s = 2/4, t = 1.0, u = 3/4)
  phi[77] = sphi[4]*tphi[1]*uphi[3]; // 1 @ node 77 (s = 3/4, t = 1.0, u = 2/4)
  phi[78] = sphi[3]*tphi[1]*uphi[2]; // 1 @ node 78 (s = 2/4, t = 1.0, u = 1/4)
  phi[79] = sphi[3]*tphi[1]*uphi[3]; // 1 @ node 79 (s = 2/4, t = 1.0, u = 2/4)

  // Face 4 (s = 0)
  phi[80] = sphi[0]*tphi[2]*uphi[2]; // 1 @ node 80 (s = 0.0, t = 1/4, u = 1/4)
  phi[81] = sphi[0]*tphi[2]*uphi[4]; // 1 @ node 81 (s = 0.0, t = 1/4, u = 3/4)
  phi[82] = sphi[0]*tphi[4]*uphi[4]; // 1 @ node 82 (s = 0.0, t = 3/4, u = 3/4)
  phi[83] = sphi[0]*tphi[4]*uphi[2]; // 1 @ node 83 (s = 0.0, t = 3/4, u = 1/4)
  phi[84] = sphi[0]*tphi[2]*uphi[3]; // 1 @ node 84 (s = 0.0, t = 1/4, u = 2/4)
  phi[85] = sphi[0]*tphi[3]*uphi[4]; // 1 @ node 85 (s = 0.0, t = 2/4, u = 3/4)
  phi[86] = sphi[0]*tphi[4]*uphi[3]; // 1 @ node 86 (s = 0.0, t = 3/4, u = 2/4)
  phi[87] = sphi[0]*tphi[3]*uphi[2]; // 1 @ node 87 (s = 0.0, t = 2/4, u = 1/4)
  phi[88] = sphi[0]*tphi[3]*uphi[3]; // 1 @ node 88 (s = 0.0, t = 2/4, u = 2/4)

  // Face 5 (u = 1)
  phi[89] = sphi[2]*tphi[2]*uphi[1]; // 1 @ node 89 (s = 1/4, t = 1/4, u = 1.0)
  phi[90] = sphi[4]*tphi[2]*uphi[1]; // 1 @ node 90 (s = 3/4, t = 1/4, u = 1.0)
  phi[91] = sphi[4]*tphi[4]*uphi[1]; // 1 @ node 91 (s = 3/4, t = 3/4, u = 1.0)
  phi[92] = sphi[2]*tphi[4]*uphi[1]; // 1 @ node 92 (s = 1/4, t = 3/4, u = 1.0)
  phi[93] = sphi[3]*tphi[2]*uphi[1]; // 1 @ node 93 (s = 2/4, t = 1/4, u = 1.0)
  phi[94] = sphi[4]*tphi[3]*uphi[1]; // 1 @ node 94 (s = 3/4, t = 2/4, u = 1.0)
  phi[95] = sphi[3]*tphi[4]*uphi[1]; // 1 @ node 95 (s = 2/4, t = 3/4, u = 1.0)
  phi[96] = sphi[2]*tphi[3]*uphi[1]; // 1 @ node 96 (s = 1/4, t = 2/4, u = 1.0)
  phi[97] = sphi[3]*tphi[3]*uphi[1]; // 1 @ node 97 (s = 2/4, t = 2/4, u = 1.0)

  // Cell
  phi[98]  = sphi[2]*tphi[2]*uphi[2]; // 1 @ node 98  (s = 1/4, t = 1/4, u = 1/4)
  phi[99]  = sphi[4]*tphi[2]*uphi[2]; // 1 @ node 99  (s = 3/4, t = 1/4, u = 1/4)
  phi[100] = sphi[4]*tphi[4]*uphi[2]; // 1 @ node 100 (s = 3/4, t = 3/4, u = 1/4)
  phi[101] = sphi[2]*tphi[4]*uphi[2]; // 1 @ node 101 (s = 1/4, t = 3/4, u = 1/4)

  phi[102] = sphi[2]*tphi[2]*uphi[4]; // 1 @ node 102 (s = 1/4, t = 1/4, u = 3/4)
  phi[103] = sphi[4]*tphi[2]*uphi[4]; // 1 @ node 103 (s = 3/4, t = 1/4, u = 3/4)
  phi[104] = sphi[4]*tphi[4]*uphi[4]; // 1 @ node 104 (s = 3/4, t = 3/4, u = 3/4)
  phi[105] = sphi[2]*tphi[4]*uphi[4]; // 1 @ node 105 (s = 1/4, t = 3/4, u = 3/4)

  phi[106] = sphi[3]*tphi[2]*uphi[2]; // 1 @ node 106 (s = 2/4, t = 1/4, u = 1/4)
  phi[107] = sphi[4]*tphi[3]*uphi[2]; // 1 @ node 107 (s = 3/4, t = 2/4, u = 1/4)
  phi[108] = sphi[3]*tphi[4]*uphi[2]; // 1 @ node 108 (s = 2/4, t = 3/4, u = 1/4)
  phi[109] = sphi[2]*tphi[3]*uphi[2]; // 1 @ node 109 (s = 1/4, t = 2/4, u = 1/4)
  phi[110] = sphi[3]*tphi[3]*uphi[2]; // 1 @ node 110 (s = 2/4, t = 2/4, u = 1/4)

  phi[111] = sphi[3]*tphi[2]*uphi[4]; // 1 @ node 111 (s = 2/4, t = 1/4, u = 3/4)
  phi[112] = sphi[4]*tphi[3]*uphi[4]; // 1 @ node 112 (s = 3/4, t = 2/4, u = 3/4)
  phi[113] = sphi[3]*tphi[4]*uphi[4]; // 1 @ node 113 (s = 2/4, t = 3/4, u = 3/4)
  phi[114] = sphi[2]*tphi[3]*uphi[4]; // 1 @ node 114 (s = 1/4, t = 2/4, u = 3/4)
  phi[115] = sphi[3]*tphi[3]*uphi[4]; // 1 @ node 115 (s = 2/4, t = 2/4, u = 3/4)

  phi[116] = sphi[2]*tphi[2]*uphi[3]; // 1 @ node 116 (s = 1/4, t = 1/4, u = 2/4)
  phi[117] = sphi[4]*tphi[2]*uphi[3]; // 1 @ node 117 (s = 3/4, t = 1/4, u = 2/4)
  phi[118] = sphi[4]*tphi[4]*uphi[3]; // 1 @ node 118 (s = 3/4, t = 3/4, u = 2/4)
  phi[119] = sphi[2]*tphi[4]*uphi[3]; // 1 @ node 119 (s = 1/4, t = 3/4, u = 2/4)
  phi[120] = sphi[3]*tphi[2]*uphi[3]; // 1 @ node 120 (s = 2/4, t = 1/4, u = 2/4)
  phi[121] = sphi[4]*tphi[3]*uphi[3]; // 1 @ node 121 (s = 3/4, t = 2/4, u = 2/4)
  phi[122] = sphi[3]*tphi[4]*uphi[3]; // 1 @ node 122 (s = 2/4, t = 3/4, u = 2/4)
  phi[123] = sphi[2]*tphi[3]*uphi[3]; // 1 @ node 123 (s = 1/4, t = 2/4, u = 2/4)
  phi[124] = sphi[3]*tphi[3]*uphi[3]; // 1 @ node 124 (s = 2/4, t = 2/4, u = 2/4)

}

void
BasisFunctionVolume<Hex,Lagrange,4>::evalBasis(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 125);

  Real sphi[5], tphi[5], uphi[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(s, sphi, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(t, tphi, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(u, uphi, 5);

  tensorProduct(sphi, tphi, uphi, phi);
}


void
BasisFunctionVolume<Hex,Lagrange,4>::evalBasisDerivative(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 125);

  Real sphi[5], tphi[5], uphi[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(s, sphi, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(t, tphi, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(u, uphi, 5);

  Real sphis[5], tphit[5], uphiu[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(s, sphis, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(t, tphit, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(u, uphiu, 5);

  tensorProduct(sphis, tphi , uphi, phis);
  tensorProduct(sphi , tphit, uphi, phit);
  tensorProduct(sphi , tphi, uphiu, phiu);
}

void
BasisFunctionVolume<Hex,Lagrange,4>::evalBasisHessianDerivative(const Real& s, const Real& t, const Real& u, const Int6& sgn,
                                                                Real phiss[],
                                                                Real phist[], Real phitt[],
                                                                Real phisu[], Real phitu[], Real phiuu[], int nphi) const
{
  SANS_ASSERT(nphi == 125);

  Real sphi[5], tphi[5], uphi[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(s, sphi, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(t, tphi, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(u, uphi, 5);

  Real sphis[5], tphit[5], uphiu[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(s, sphis, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(t, tphit, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(u, uphiu, 5);

  Real sphiss[5], tphitt[5], uphiuu[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(s, sphiss, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(t, tphitt, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(u, uphiuu, 5);

  tensorProduct(sphiss, tphi  , uphi  , phiss);
  tensorProduct(sphis , tphit , uphi  , phist);
  tensorProduct(sphi  , tphitt, uphi  , phitt);
  tensorProduct(sphis , tphi  , uphiu , phisu);
  tensorProduct(sphi  , tphit , uphiu , phitu);
  tensorProduct(sphi  , tphi  , uphiuu, phiuu);
}

void
BasisFunctionVolume<Hex,Lagrange,4>::coordinates( std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u ) const
{
  s = coords_s_;
  t = coords_t_;
  u = coords_u_;
}

} // namespace SANS
