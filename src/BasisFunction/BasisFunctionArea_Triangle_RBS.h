// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONAREA_TRIANGLE_RBS_H
#define BASISFUNCTIONAREA_TRIANGLE_RBS_H

// triangle basis functions: RBS

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionLine.h
// - duplicate functionality to BasisFunctionTriangle.h


#include <array>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionArea.h"
#include "BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// area basis functions: RBS
//
// reference triangle element defined: s in [0, 1], t in [0, 1-s]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionArea_Triangle_RBSPMax = 2;


//----------------------------------------------------------------------------//
// RBS: Linear

template <>
class BasisFunctionArea<Triangle,RBS,1> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 3; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_RBS; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const IntNedge& sgn, Real phi[], int nphi) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int3&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};

//----------------------------------------------------------------------------//
// RBS: Quadratic

template <>
class BasisFunctionArea<Triangle,RBS,2> :
    public BasisFunctionAreaBase<Triangle>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,3> Int3;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 2; }
  virtual int nBasis() const { return 6; }
  virtual int nBasisNode() const { return 3; }
  virtual int nBasisEdge() const { return 3; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_RBS; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const IntNedge& sgn, Real phi[], int nphi) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int3&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int3&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
public:
  static const BasisFunctionArea* self();
};
}

#endif  // BASISFUNCTIONAREA_TRIANGLE_RBS_H
