// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <cmath>

#include "BasisFunctionLine_Hierarchical.h"
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Hierarchical: linear

void
BasisFunctionLine<Hierarchical,1>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phi[0] = 1 - s;
  phi[1] =     s;
}

void
BasisFunctionLine<Hierarchical,1>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phis[0] = -1;
  phis[1] =  1;
}

void
BasisFunctionLine<Hierarchical,1>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phiss[0] = 0;
  phiss[1] = 0;
}


//----------------------------------------------------------------------------//
// Hierarchical: quadratic

void
BasisFunctionLine<Hierarchical,2>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phi[0] = 1 - s;
  phi[1] =     s;
  phi[2] = 4*s*(1 - s);
}

void
BasisFunctionLine<Hierarchical,2>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  4*(1 - 2*s);
}

void
BasisFunctionLine<Hierarchical,2>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = -8;
}


//----------------------------------------------------------------------------//
// Hierarchical: cubic

void
BasisFunctionLine<Hierarchical,3>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real t = 1 - s;
  Real u = 1 - 2*s;

  phi[0] = 1 - s;
  phi[1] =     s;
  phi[2] = 4*s*t;
  phi[3] = 6*sqrt(3)*s*t*u;
}

void
BasisFunctionLine<Hierarchical,3>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  4*(1 - 2*s);
  phis[3] =  6*sqrt(3)*(1 - 6*s*(1 - s));
}

void
BasisFunctionLine<Hierarchical,3>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = -8;
  phiss[3] = -36*sqrt(3)*(1 - 2*s);
}


//----------------------------------------------------------------------------//
// Hierarchical: quartic

void
BasisFunctionLine<Hierarchical,4>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 5);

  Real t = 1 - s;
  Real u = 1 - 2*s;

  phi[0] = t;
  phi[1] = s;
  phi[2] = 4*s*t;
  phi[3] = 6*sqrt(3)*s*t*u;
  phi[4] = 16*s*s*t*t;
}

void
BasisFunctionLine<Hierarchical,4>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 5);

  Real t = 1 - s;
  Real u = 1 - 2*s;

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  4*u;
  phis[3] =  6*sqrt(3)*(1 - 6*s*t);
  phis[4] =  32*s*t*u;
}

void
BasisFunctionLine<Hierarchical,4>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 5);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = -8;
  phiss[3] = -36*sqrt(3)*(1 - 2*s);
  phiss[4] = 32*(1-6*s*(1-s));
}


//----------------------------------------------------------------------------//
// Hierarchical: quintic

void
BasisFunctionLine<Hierarchical,5>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  Real t = 1 - s;
  Real u = 1 - 2*s;

  phi[0] = t;
  phi[1] = s;
  phi[2] = 4*s*t;
  phi[3] = 6*sqrt(3)*s*t*u;
  phi[4] = 16*s*s*t*t;
  phi[5] = 25*sqrt(5)*s*s*t*t*u;
}

void
BasisFunctionLine<Hierarchical,5>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  Real t = 1 - s;
  Real u = 1 - 2*s;

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  4*u;
  phis[3] =  6*sqrt(3)*(1 - 6*s*t);
  phis[4] =  32*s*t*u;
  phis[5] =  50*sqrt(5)*s*t*(1 - 5*s*t);
}

void
BasisFunctionLine<Hierarchical,5>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = -8;
  phiss[3] = -36*sqrt(3)*(1 - 2*s);
  phiss[4] = 32*(1-6*s*(1-s));
  phiss[5] = 50*sqrt(5)*(1-2*s)*(1-10*s*(1-s));
}


//----------------------------------------------------------------------------//
// Hierarchical: P6

void
BasisFunctionLine<Hierarchical,6>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 7);

  Real t = 1 - s;
  Real u = 1 - 2*s;

  phi[0] = t;
  phi[1] = s;
  phi[2] = 4*s*t;
  phi[3] = 6*sqrt(3)*s*t*u;
  phi[4] = 16*s*s*t*t;
  phi[5] = 25*sqrt(5)*s*s*t*t*u;
  phi[6] = 64*s*s*s*t*t*t;
}

void
BasisFunctionLine<Hierarchical,6>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 7);

  Real t = 1 - s;
  Real u = 1 - 2*s;

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  4*u;
  phis[3] =  6*sqrt(3)*(1 - 6*s*t);
  phis[4] =  32*s*t*u;
  phis[5] =  50*sqrt(5)*s*t*(1 - 5*s*t);
  phis[6] =  192*s*s*t*t*u;
}

void
BasisFunctionLine<Hierarchical,6>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 7);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = -8;
  phiss[3] = -36*sqrt(3)*(1 - 2*s);
  phiss[4] = 32*(1-6*s+6*s*s);
  phiss[5] = 50*sqrt(5)*(1-2*s)*(1-10*s*(1-s));
  phiss[6] = 384*s*(1-s)*(1-5*s*(1-s));
}


//----------------------------------------------------------------------------//
// Hierarchical: P7

void
BasisFunctionLine<Hierarchical,7>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real t = 1 - s;

  phi[0] = t;
  phi[1] = s;
  phi[2] = 4*s*t;
  phi[3] = 6*sqrt(3)*s*t*(t - s);
  phi[4] = 16*s*s*t*t;
  phi[5] = 25*sqrt(5)*s*s*t*t*(t - s);
  phi[6] = 64*s*s*s*t*t*t;
  phi[7] = 2744*sqrt(7)*s*s*s*t*t*t*(t - s)/27.;
}

void
BasisFunctionLine<Hierarchical,7>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real t = 1 - s;

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  4*(t - s);
  phis[3] =  6*sqrt(3)*(1 - 6*s*t);
  phis[4] =  32*s*t*(t - s);
  phis[5] =  50*sqrt(5)*s*t*(1 - 5*s*t);
  phis[6] =  192*s*s*t*t*(t - s);
  phis[7] =  2744*sqrt(7)*s*s*t*t*(3 - 14*s*t)/27.;
}

void
BasisFunctionLine<Hierarchical,7>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real t = 1 - s;

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = -8;
  phiss[3] = -36*sqrt(3)*(t - s);
  phiss[4] = 32*(1 - 6*s*t);
  phiss[5] = 50*sqrt(5)*(1 - 10*s*t)*(t - s);
  phiss[6] = 384*s*t*(1 - 5*s*t);
  phiss[7] = 5488*sqrt(7)*s*t*(1 - 7*s*t)*(t - s)/9.;
}


}
