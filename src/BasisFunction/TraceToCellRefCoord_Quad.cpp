// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// reference element operators

#include <algorithm> // std::find
#include <sstream>   // stringstream

#include "ElementEdges.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "TraceToCellRefCoord.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Quadrature_Cache.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// cell reference coordinates (s,t) given edge reference coordinate (s)
/*
    3-------2
    |       |
    |       |
    |       |
    0 ----- 1
*/
// parameters:
//  canonicalEdge      edge in quad (E0=0-1, E1=1-2, E2=2-3, E3=3-0); positive orientation is CCW

template<class CellTopology>
const int TraceToCellRefCoord<Line, TopoD2, CellTopology>::TraceNodes[ CellTopology::NEdge ][ Line::NNode ] = { {0,1}, {1,2}, {2,3}, {3,0} };

template<>
const int ElementEdges<Quad>::EdgeNodes[ Quad::NEdge ][ Line::NNode ] = { {0,1}, {1,2}, {2,3}, {3,0} };

template<class CellTopology> const int TraceToCellRefCoord<Line, TopoD2, CellTopology>::OrientPos[ Line::NNode ] = {0,1};
template<class CellTopology> const int TraceToCellRefCoord<Line, TopoD2, CellTopology>::OrientNeg[ Line::NNode ] = {1,0};

template<class CellTopology>
void
TraceToCellRefCoord<Line, TopoD2, CellTopology>::
eval( const CanonicalTraceToCell& canonicalEdge, const DLA::VectorS<1,Real>& sRefTrace, DLA::VectorS<2,Real>& sRefCell )
{
  eval(canonicalEdge, sRefTrace[0], sRefCell[0], sRefCell[1]);
}

template<class CellTopology>
void
TraceToCellRefCoord<Line, TopoD2, CellTopology>::
eval( const CanonicalTraceToCell& canonicalEdge, const QuadraturePoint<TopoD1>& sRefTrace, QuadratureCellTracePoint<TopoD2>& sRefCell )
{
  sRefCell.set(canonicalEdge, sRefTrace);
  eval(canonicalEdge, sRefTrace.ref[0], sRefCell.ref[0], sRefCell.ref[1]);
}

template<class CellTopology>
void
TraceToCellRefCoord<Line, TopoD2, CellTopology>::eval( const CanonicalTraceToCell& canonicalEdge, const Real& sRef, Real& sRefArea, Real& tRefArea )
{

  Real s = sRef;

  // Reverse s if the orientation is negative so the normal is outward
  if ( canonicalEdge.orientation < 0 )
    s = 1 - s;

  switch (canonicalEdge.trace)
  {
  case 0:      // 0-1
  {
    sRefArea = s;
    tRefArea = 0;
    break;
  }
  case 1:      // 1-2
  {
    sRefArea = 1;
    tRefArea = s;
    break;
  }
  case 2:      // 2-3
  {
    sRefArea = 1-s;
    tRefArea = 1;
    break;
  }
  case 3:      // 3-0
  {
    sRefArea = 0;
    tRefArea = 1-s;
    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION( "Unexpected canonicalEdge.trace = %d", canonicalEdge.trace );
  }

}

template<class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<Line, TopoD2, CellTopology>::getCanonicalTraceLeft(const int *edgeNodes, const int nEdgeNodes,
                                                                       const int *quadNodes, const int nQuadNodes,
                                                                       int *canonicalLine, const int ncanonicalLine)
{
  SANS_ASSERT(nEdgeNodes == 2);
  SANS_ASSERT(nQuadNodes == 4);
  SANS_ASSERT(ncanonicalLine == 2);

  // Find the edge that contains both line nodes
  int canonicalEdge = -1;
  for ( int edge = 0; edge < Quad::NEdge; edge++ )
  {
    int cnt = 0;
    for ( int n = 0; n < Line::NNode; n++ )
    {
      // If the node canonical node is not in the edge, then stop counting
      if ( std::find( edgeNodes, edgeNodes+nEdgeNodes, quadNodes[TraceNodes[edge][n]] ) == edgeNodes+nEdgeNodes )
        break;
      cnt++;
    }

    if (cnt == Line::NNode)
    {
      canonicalEdge = edge;
      break;
    }
  }

  if ( canonicalEdge == -1 )
  {
    std::stringstream msg;
    msg << "Line nodes do not make up an edge of the quad." << std::endl;

    msg << "Line nodes = { ";
    msg << edgeNodes[0] << ", ";
    msg << edgeNodes[1] << " } " << std::endl;

    msg << "Quad nodes = { ";
    for (int n = 0; n < 3; n++)
      msg << quadNodes[n] << ", ";
    msg << quadNodes[3] << " } " << std::endl;

    msg << "Valid edges are:" << std::endl;

    for ( int edge = 0; edge < Quad::NEdge; edge++ )
    {
      msg << "edge[" << edge << "] = { ";
      msg << quadNodes[TraceNodes[edge][0]] << ", ";
      msg << quadNodes[TraceNodes[edge][1]] << " }" << std::endl;
    }

    SANS_DEVELOPER_EXCEPTION(msg.str());
  }

  // Get the canonical edge nodes
  for ( int n = 0; n < Line::NNode; n++)
    canonicalLine[n] = quadNodes[TraceNodes[canonicalEdge][n]];

  return CanonicalTraceToCell(canonicalEdge, 1);
}

template<class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<Line, TopoD2, CellTopology>::getCanonicalTrace(const int *edgeNodes, const int nEdgeNodes,
                                                                   const int *quadNodes, const int nQuadNodes)
{
  SANS_ASSERT(nEdgeNodes == 2);
  SANS_ASSERT(nQuadNodes == 4);

  int canonicalLine[2];
  int canonicalEdge = getCanonicalTraceLeft(edgeNodes, nEdgeNodes, quadNodes, nQuadNodes, canonicalLine, 2).trace;

  // Find the positive or negative orientation where the nodes on the triangle match the canonical triangle
  if ( edgeNodes[0] == canonicalLine[OrientPos[0]] &&
       edgeNodes[1] == canonicalLine[OrientPos[1]] ) return CanonicalTraceToCell(canonicalEdge,  1);

  if ( edgeNodes[0] == canonicalLine[OrientNeg[0]] &&
       edgeNodes[1] == canonicalLine[OrientNeg[1]] ) return CanonicalTraceToCell(canonicalEdge, -1);

  SANS_DEVELOPER_EXCEPTION( "Cannot find Line Quad canonical orientation" );
  return CanonicalTraceToCell();
}

// Explicitly instantiate the class
template struct TraceToCellRefCoord<Line, TopoD2, Quad>;

} //namespace SANS
