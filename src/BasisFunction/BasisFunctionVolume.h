// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONVOLUME_H
#define BASISFUNCTIONVOLUME_H

// volume basis functions (tetrahedron/hexahedron)

// NOTES:
// - implemented via abstract base class

#include <iostream>
#include <array>

#include "tools/SANSnumerics.h"     // Real

#include "BasisFunctionCategory.h"
#include "BasisFunctionBase.h"

namespace SANS
{

//Class for specialization of basis functions
template <class Topology, class BasisPolynomial, int BasisOrder>
class BasisFunctionVolume;

//----------------------------------------------------------------------------//
// volume basis functions
//
// reference tetrahedron element defined: s in [0, 1]; t in [0, 1-s]; u in [0, 1-s-t]
// reference hexahedron element defined: s in [0, 1]; t in [0, 1]; u in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

template <class Topology>
class BasisFunctionVolumeBase : public BasisFunctionBase
{
public:
  typedef std::array<int,Topology::NFace> IntNface;

  static const int D = 3;     // topological dimensions

  virtual ~BasisFunctionVolumeBase() {}

  virtual int order() const override = 0;
  virtual int nBasis() const override = 0;
  virtual int nBasisNode() const = 0;
  virtual int nBasisEdge() const = 0;
  virtual int nBasisFace() const = 0;
  virtual int nBasisCell() const = 0;
  virtual BasisFunctionCategory category() const override = 0;

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const IntNface& sgn, Real phi[], int nphi ) const = 0;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const IntNface& sgn,
                                    Real phis[], Real phit[], Real phiu[], int nphi ) const = 0;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const IntNface& sgn,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[], int nphi ) const = 0;

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

  static const BasisFunctionVolumeBase* getBasisFunction( const int order, const BasisFunctionCategory& category );
protected:
  BasisFunctionVolumeBase() {}  // abstract base class

public:
  static const BasisFunctionVolume<Topology, Hierarchical, 1> *HierarchicalP1;
  static const BasisFunctionVolume<Topology, Hierarchical, 2> *HierarchicalP2;
//  static const BasisFunctionVolume<Topology, Hierarchical, 3> *HierarchicalP3;
//  static const BasisFunctionVolume<Topology, Hierarchical, 4> *HierarchicalP4;
//  static const BasisFunctionVolume<Topology, Hierarchical, 5> *HierarchicalP5;
//  static const BasisFunctionVolume<Topology, Hierarchical, 6> *HierarchicalP6;
//  static const BasisFunctionVolume<Topology, Hierarchical, 7> *HierarchicalP7;

  static const BasisFunctionVolume<Topology, Legendre, 0> *LegendreP0;
  static const BasisFunctionVolume<Topology, Legendre, 1> *LegendreP1;
  static const BasisFunctionVolume<Topology, Legendre, 2> *LegendreP2;
  static const BasisFunctionVolume<Topology, Legendre, 3> *LegendreP3;
  static const BasisFunctionVolume<Topology, Legendre, 4> *LegendreP4;
//  static const BasisFunctionVolume<Topology, Legendre, 5> *LegendreP5;
//  static const BasisFunctionVolume<Topology, Legendre, 6> *LegendreP6;
//  static const BasisFunctionVolume<Topology, Legendre, 7> *LegendreP7;

  static const BasisFunctionVolume<Topology, Lagrange, 1> *LagrangeP1;
  static const BasisFunctionVolume<Topology, Lagrange, 2> *LagrangeP2;
  static const BasisFunctionVolume<Topology, Lagrange, 3> *LagrangeP3;
  static const BasisFunctionVolume<Topology, Lagrange, 4> *LagrangeP4;

  static const BasisFunctionVolume<Topology, Bernstein, 1> *BernsteinP1;
  static const BasisFunctionVolume<Topology, Bernstein, 2> *BernsteinP2;
  static const BasisFunctionVolume<Topology, Bernstein, 3> *BernsteinP3;
  static const BasisFunctionVolume<Topology, Bernstein, 4> *BernsteinP4;

  static const BasisFunctionVolume<Topology, RBS, 0> *RBSP0;
};


template <class Topology>
void
BasisFunctionVolumeBase<Topology>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent
      << "BasisFunctionVolume<...>: order = " << order()
      << "  nBasis = " << nBasis()
      << "  nBasisNode = " << nBasisNode()
      << "  nBasisEdge = " << nBasisEdge()
      << "  nBasisCell = " << nBasisCell()
      << std::endl;
}

}

#endif  // BASISFUNCTIONVOLUME_H
