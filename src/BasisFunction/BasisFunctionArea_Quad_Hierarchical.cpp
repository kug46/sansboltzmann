// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionArea_Quad_Hierarchical.h"

#include <cmath>  // sqrt
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Hierarchical: linear

void
BasisFunctionArea<Quad,Hierarchical,1>::evalBasis( const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phi[0] = (1 - s)*(1 - t); // 1 @ node 0 (s = 0, t = 0)
  phi[1] = (    s)*(1 - t); // 1 @ node 1 (s = 1, t = 0)
  phi[2] = (    s)*(    t); // 1 @ node 2 (s = 1, t = 1)
  phi[3] = (1 - s)*(    t); // 1 @ node 3 (s = 0, t = 1)
}


void
BasisFunctionArea<Quad,Hierarchical,1>::evalBasisDerivative( const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phis[0] = (  - 1)*(1 - t); // 1 @ node 0 (s = 0, t = 0)
  phis[1] = (    1)*(1 - t); // 1 @ node 1 (s = 1, t = 0)
  phis[2] = (    1)*(    t); // 1 @ node 2 (s = 1, t = 1)
  phis[3] = (  - 1)*(    t); // 1 @ node 3 (s = 0, t = 1)

  phit[0] = (1 - s)*(  - 1); // 1 @ node 0 (s = 0, t = 0)
  phit[1] = (    s)*(  - 1); // 1 @ node 1 (s = 1, t = 0)
  phit[2] = (    s)*(    1); // 1 @ node 2 (s = 1, t = 1)
  phit[3] = (1 - s)*(    1); // 1 @ node 3 (s = 0, t = 1)
}

void
BasisFunctionArea<Quad,Hierarchical,1>::evalBasisHessianDerivative( const Real& s, const Real& t, const Int4&,
    Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phiss[0] = 0; // 1 @ node 0 (s = 0, t = 0)
  phiss[1] = 0; // 1 @ node 1 (s = 1, t = 0)
  phiss[2] = 0; // 1 @ node 2 (s = 1, t = 1)
  phiss[3] = 0; // 1 @ node 3 (s = 0, t = 1)

  phist[0] =  1; // 1 @ node 0 (s = 0, t = 0)
  phist[1] = -1; // 1 @ node 1 (s = 1, t = 0)
  phist[2] =  1; // 1 @ node 2 (s = 1, t = 1)
  phist[3] = -1; // 1 @ node 3 (s = 0, t = 1)

  phitt[0] = 0; // 1 @ node 0 (s = 0, t = 0)
  phitt[1] = 0; // 1 @ node 1 (s = 1, t = 0)
  phitt[2] = 0; // 1 @ node 2 (s = 1, t = 1)
  phitt[3] = 0; // 1 @ node 3 (s = 0, t = 1)
}



#if 0
//----------------------------------------------------------------------------//
// Hierarchical: quadratic

void
BasisFunctionArea<Quad,Hierarchical,2>::evalBasis(
    const Real& s, const Real& t, const Int3&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  Real u = 1 - s - t;

  phi[0] = u;      // 1 @ node 1 (s = 0, t = 0)
  phi[1] = s;      // 1 @ node 2 (s = 1, t = 0)
  phi[2] = t;      // 1 @ node 3 (s = 0, t = 1)

  phi[3] = 4*s*t;  // 1 @ edge 1, s = 1/2, t = 1 - s

  phi[4] = 4*t*u;  // 1 @ edge 2, t = 1/2, s = 0

  phi[5] = 4*u*s;  // 1 @ edge 3, s = 1/2, t = 0
}


void
BasisFunctionArea<Quad,Hierarchical,2>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  0;
  phis[3] =  4*t;
  phis[4] = -4*t;
  phis[5] =  4*(1 - 2*s - t);

  phit[0] = -1;
  phit[1] =  0;
  phit[2] =  1;
  phit[3] =  4*s;
  phit[4] =  4*(1 - s - 2*t);
  phit[5] = -4*s;
}


//----------------------------------------------------------------------------//
// Hierarchical: cubic

void
BasisFunctionArea<Quad,Hierarchical,3>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  Real u = 1 - s - t;

  phi[0] = u;                                 // 1 @ node 1 (s = 0, t = 0)
  phi[1] = s;                                 // 1 @ node 2 (s = 1, t = 0)
  phi[2] = t;                                 // 1 @ node 3 (s = 0, t = 1)

  phi[3] = 4*s*t;                             // max=1 on edge 1 @ s = 1/2, t = 1/2
  phi[4] = 6*sqrt(3)*s*t*(t - s) * sgn[0];    // max=1 on edge 1 (asymmetric)

  phi[5] = 4*t*u;                             // max=1 on edge 2 @ t = 1/2, s = 0 (u = 1/2)
  phi[6] = 6*sqrt(3)*t*u*(u - t) * sgn[1];    // max=1 on edge 2 (asymmetric)

  phi[7] = 4*u*s;                             // max=1 on edge 3 @ s = 1/2, t = 0 (u = 1/2)
  phi[8] = 6*sqrt(3)*u*s*(s - u) * sgn[2];    // max=1 on edge 3 (asymmetric)

  phi[9] = 27*s*t*u;                          // max=1 @ centroid (s,t) = 1/3
}


void
BasisFunctionArea<Quad,Hierarchical,3>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  Real u = 1 - s - t;

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  0;

  phis[3] =  4*t;
  phis[4] =  6*sqrt(3)*t*(t - 2*s) * sgn[0];

  phis[5] = -4*t;
  phis[6] =  6*sqrt(3)*t*(t - 2*u) * sgn[1];

  phis[7] =  4*(u - s);
  phis[8] = -6*sqrt(3)*(s*(s - 2*u) + u*(u - 2*s)) * sgn[2];

  phis[9] =  27*t*(u - s);


  phit[0] = -1;
  phit[1] =  0;
  phit[2] =  1;

  phit[3] =  4*s;
  phit[4] = -6*sqrt(3)*s*(s - 2*t) * sgn[0];

  phit[5] =  4*(u - t);
  phit[6] =  6*sqrt(3)*(u*(u - 2*t) + t*(t - 2*u)) * sgn[1];

  phit[7] = -4*s;
  phit[8] = -6*sqrt(3)*s*(s - 2*u) * sgn[2];

  phit[9] =  27*s*(u - t);
}


//----------------------------------------------------------------------------//
// Hierarchical: quartic

void
BasisFunctionArea<Quad,Hierarchical,4>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 15);

  Real u = 1 - s - t;

  phi[ 0] = u;                                // 1 @ node 1 (s = 0, t = 0)
  phi[ 1] = s;                                // 1 @ node 2 (s = 1, t = 0)
  phi[ 2] = t;                                // 1 @ node 3 (s = 0, t = 1)

  phi[ 3] = 4*s*t;                            // max=1 on edge 1 @ s = 1/2, t = 1/2
  phi[ 4] = 6*sqrt(3)*s*t*(t - s) * sgn[0];   // max=1 on edge 1 (asymmetric)
  phi[ 5] = 16*s*s*t*t;                       // max=1 on edge 1

  phi[ 6] = 4*t*u;                            // max=1 on edge 2 @ t = 1/2, s = 0 (u = 1/2)
  phi[ 7] = 6*sqrt(3)*t*u*(u - t) * sgn[1];   // max=1 on edge 2 (asymmetric)
  phi[ 8] = 16*t*t*u*u;                       // max=1 on edge 2

  phi[ 9] = 4*u*s;                            // max=1 on edge 3 @ s = 1/2, t = 0 (u = 1/2)
  phi[10] = 6*sqrt(3)*u*s*(s - u) * sgn[2];   // max=1 on edge 3 (asymmetric)
  phi[11] = 16*u*u*s*s;                       // max=1 on edge 3

  phi[12] = 27*s*t*u;                          // max=1 @ centroid (s,t) = 1/3
  phi[13] = 512*s*t*u*(u - s)/(3*sqrt(3));     // max=1 @ s = (3 - sqrt(3))/8, t = 1/4
  phi[14] = 512*s*t*u*(u - t)/(3*sqrt(3));     // max=1 @ s = 1/4, t = (3 - sqrt(3))/8
}


void
BasisFunctionArea<Quad,Hierarchical,4>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 15);

  Real u = 1 - s - t;

  phis[ 0] = -1;
  phis[ 1] =  1;
  phis[ 2] =  0;

  phis[ 3] =  4*t;
  phis[ 4] =  6*sqrt(3)*t*(t - 2*s) * sgn[0];
  phis[ 5] =  32*s*t*t;

  phis[ 6] = -4*t;
  phis[ 7] =  6*sqrt(3)*t*(t - 2*u) * sgn[1];
  phis[ 8] = -32*t*t*u;

  phis[ 9] =  4*(u - s);
  phis[10] = -6*sqrt(3)*(s*(s - 2*u) + u*(u - 2*s)) * sgn[2];
  phis[11] =  32*u*s*(u - s);

  phis[12] =  27*t*(u - s);
  phis[13] =  512*t*(u*(u - 2*s) + s*(s - 2*u))/(3*sqrt(3));
  phis[14] =  512*t*(u*(u - t) + s*(t - 2*u))/(3*sqrt(3));


  phit[ 0] = -1;
  phit[ 1] =  0;
  phit[ 2] =  1;

  phit[ 3] =  4*s;
  phit[ 4] = -6*sqrt(3)*s*(s - 2*t) * sgn[0];
  phit[ 5] =  32*s*s*t;

  phit[ 6] =  4*(u - t);
  phit[ 7] =  6*sqrt(3)*(u*(u - 2*t) + t*(t - 2*u)) * sgn[1];
  phit[ 8] =  32*t*u*(u - t);

  phit[ 9] = -4*s;
  phit[10] = -6*sqrt(3)*s*(s - 2*u) * sgn[2];
  phit[11] = -32*s*s*u;

  phit[12] =  27*s*(u - t);
  phit[13] =  512*s*(u*(u - 2*t) + s*(t - u))/(3*sqrt(3));
  phit[14] =  512*s*(u*(u - 2*t) + t*(t - 2*u))/(3*sqrt(3));
}


//----------------------------------------------------------------------------//
// Hierarchical: quintic

void
BasisFunctionArea<Quad,Hierarchical,5>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 21);

  Real u = 1 - s - t;

  phi[ 0] = u;                                      // 1 @ node 1 (s = 0, t = 0)
  phi[ 1] = s;                                      // 1 @ node 2 (s = 1, t = 0)
  phi[ 2] = t;                                      // 1 @ node 3 (s = 0, t = 1)

  phi[ 3] = 4*s*t;                                  // max=1 on edge 1 @ s = 1/2, t = 1/2
  phi[ 4] = 6*sqrt(3)*s*t*(t - s) * sgn[0];         // max=1 on edge 1 (asymmetric)
  phi[ 5] = 16*s*s*t*t;                             // max=1 on edge 1
  phi[ 6] = 25*sqrt(5)*s*s*t*t*(t - s) * sgn[0];    // max=1 on edge 1 (asymmetric)

  phi[ 7] = 4*t*u;                                  // max=1 on edge 2 @ t = 1/2, s = 0 (u = 1/2)
  phi[ 8] = 6*sqrt(3)*t*u*(u - t) * sgn[1];         // max=1 on edge 2 (asymmetric)
  phi[ 9] = 16*t*t*u*u;                             // max=1 on edge 2
  phi[10] = 25*sqrt(5)*t*t*u*u*(u - t) * sgn[1];    // max=1 on edge 2 (asymmetric)

  phi[11] = 4*u*s;                                  // max=1 on edge 3 @ s = 1/2, t = 0 (u = 1/2)
  phi[12] = 6*sqrt(3)*u*s*(s - u) * sgn[2];         // max=1 on edge 3 (asymmetric)
  phi[13] = 16*u*u*s*s;                             // max=1 on edge 3
  phi[14] = 25*sqrt(5)*u*u*s*s*(s - u) * sgn[2];    // max=1 on edge 3 (asymmetric)

  phi[15] = 27*s*t*u;                               // max=1 @ centroid (s,t) = 1/3
  phi[16] = 512*s*t*u*(u - s)/(3*sqrt(3));          // max=1 @ s = (3 - sqrt(3))/8, t = 1/4
  phi[17] = 512*s*t*u*(u - t)/(3*sqrt(3));          // max=1 @ s = 1/4, t = (3 - sqrt(3))/8
  phi[18] = 3125*s*s*t*t*u/16.;                     // max=1 @ s = 2/5, t = 2/5 (u = 1/5)
  phi[19] = 3125*t*t*u*u*s/16.;                     // max=1 @ s = 1/5, t = 2/5 (u = 2/5)
  phi[20] = 3125*u*u*s*s*t/16.;                     // max=1 @ s = 2/5, t = 1/5 (u = 2/5)
}


void
BasisFunctionArea<Quad,Hierarchical,5>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 21);

  Real u = 1 - s - t;

  phis[ 0] = -1;
  phis[ 1] =  1;
  phis[ 2] =  0;

  phis[ 3] =  4*t;
  phis[ 4] =  6*sqrt(3)*t*(t - 2*s) * sgn[0];
  phis[ 5] =  32*s*t*t;
  phis[ 6] =  25*sqrt(5)*s*t*t*(2*t - 3*s) * sgn[0];

  phis[ 7] = -4*t;
  phis[ 8] =  6*sqrt(3)*t*(t - 2*u) * sgn[1];
  phis[ 9] = -32*t*t*u;
  phis[10] =  25*sqrt(5)*t*t*u*(2*t - 3*u) * sgn[1];

  phis[11] =  4*(u - s);
  phis[12] = -6*sqrt(3)*(s*(s - 2*u) + u*(u - 2*s)) * sgn[2];
  phis[13] =  32*u*s*(u - s);
  phis[14] = -50*sqrt(5)*u*s*(u*u - 3*u*s + s*s) * sgn[2];

  phis[15] =  27*t*(u - s);
  phis[16] =  512*t*(u*(u - 2*s) + s*(s - 2*u))/(3*sqrt(3));
  phis[17] =  512*t*(u*(u - t) + s*(t - 2*u))/(3*sqrt(3));
  phis[18] = -3125*s*t*t*(s - 2*u)/16.;
  phis[19] =  3125*t*t*u*(u - 2*s)/16.;
  phis[20] =  6250*s*t*u*(u - s)/16.;


  phit[ 0] = -1;
  phit[ 1] =  0;
  phit[ 2] =  1;

  phit[ 3] =  4*s;
  phit[ 4] = -6*sqrt(3)*s*(s - 2*t) * sgn[0];
  phit[ 5] =  32*s*s*t;
  phit[ 6] =  25*sqrt(5)*s*s*t*(3*t - 2*s) * sgn[0];

  phit[ 7] =  4*(u - t);
  phit[ 8] =  6*sqrt(3)*(u*(u - 2*t) + t*(t - 2*u)) * sgn[1];
  phit[ 9] =  32*t*u*(u - t);
  phit[10] =  50*sqrt(5)*t*u*(t*t - 3*u*t + u*u) * sgn[1];

  phit[11] = -4*s;
  phit[12] = -6*sqrt(3)*s*(s - 2*u) * sgn[2];
  phit[13] = -32*s*s*u;
  phit[14] = -25*sqrt(5)*s*s*u*(2*s - 3*u) * sgn[2];

  phit[15] =  27*s*(u - t);
  phit[16] =  512*s*(u*(u - 2*t) + s*(t - u))/(3*sqrt(3));
  phit[17] =  512*s*(u*(u - 2*t) + t*(t - 2*u))/(3*sqrt(3));
  phit[18] = -3125*s*s*t*(t - 2*u)/16.;
  phit[19] =  6250*s*t*u*(u - t)/16.;
  phit[20] =  3125*s*s*u*(u - 2*t)/16.;
}


//----------------------------------------------------------------------------//
// Hierarchical: P6

void
BasisFunctionArea<Quad,Hierarchical,6>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 28);

  Real u = 1 - s - t;

  phi[ 0] = u;                                      // 1 @ node 1 (s = 0, t = 0)
  phi[ 1] = s;                                      // 1 @ node 2 (s = 1, t = 0)
  phi[ 2] = t;                                      // 1 @ node 3 (s = 0, t = 1)

  phi[ 3] = 4*s*t;                                  // max=1 on edge 1 @ midpoint s = 1/2, t = 1/2
  phi[ 4] = 6*sqrt(3)*s*t*(t - s) * sgn[0];         // max=1 on edge 1 (asymmetric)
  phi[ 5] = 16*s*s*t*t;                             // max=1 on edge 1 @ midpoint
  phi[ 6] = 25*sqrt(5)*s*s*t*t*(t - s) * sgn[0];    // max=1 on edge 1 (asymmetric)
  phi[ 7] = 64*s*s*s*t*t*t;                         // max=1 on edge 1 @ midpoint

  phi[ 8] = 4*t*u;                                  // max=1 on edge 2 @ midpoint t = 1/2, s = 0 (u = 1/2)
  phi[ 9] = 6*sqrt(3)*t*u*(u - t) * sgn[1];         // max=1 on edge 2 (asymmetric)
  phi[10] = 16*t*t*u*u;                             // max=1 on edge 2 @ midpoint
  phi[11] = 25*sqrt(5)*t*t*u*u*(u - t) * sgn[1];    // max=1 on edge 2 (asymmetric)
  phi[12] = 64*t*t*t*u*u*u;                         // max=1 on edge 2 @ midpoint

  phi[13] = 4*u*s;                                  // max=1 on edge 3 @ midpoint s = 1/2, t = 0 (u = 1/2)
  phi[14] = 6*sqrt(3)*u*s*(s - u) * sgn[2];         // max=1 on edge 3 (asymmetric)
  phi[15] = 16*u*u*s*s;                             // max=1 on edge 3 @ midpoint
  phi[16] = 25*sqrt(5)*u*u*s*s*(s - u) * sgn[2];    // max=1 on edge 3 (asymmetric)
  phi[17] = 64*u*u*u*s*s*s;                         // max=1 on edge 3 @ midpoint

  phi[18] = 27*s*t*u;                               // max=1 @ centroid (s,t) = 1/3
  phi[19] = 512*s*t*u*(u - s)/(3*sqrt(3));          // max=1 @ s = (3 - sqrt(3))/8, t = 1/4
  phi[20] = 512*s*t*u*(u - t)/(3*sqrt(3));          // max=1 @ s = 1/4, t = (3 - sqrt(3))/8
  phi[21] = 3125*s*s*t*t*u/16.;                     // max=1 @ s = 2/5, t = 2/5 (u = 1/5)
  phi[22] = 3125*t*t*u*u*s/16.;                     // max=1 @ s = 1/5, t = 2/5 (u = 2/5)
  phi[23] = 3125*u*u*s*s*t/16.;                     // max=1 @ s = 2/5, t = 1/5 (u = 2/5)

  phi[24] = 729*s*s*t*t*u*u;                        // max=1 @ centroid
  phi[25] = 46656*s*s*t*t*u*(t - s)/(25*sqrt(5));   // max=1 @ s = (5 - sqrt(5))/12, t = (5 + sqrt(5))/12 (u = 1/6)
  phi[26] = 46656*t*t*u*u*s*(u - t)/(25*sqrt(5));   // max=1 @ s = 1/6, t = (5 - sqrt(5))/12 (u = (5 + sqrt(5))/12)
  phi[27] = 46656*u*u*s*s*t*(s - u)/(25*sqrt(5));   // max=1 @ s = (5 + sqrt(5))/12, t = 1/6 (u = (5 - sqrt(5))/12)
}


void
BasisFunctionArea<Quad,Hierarchical,6>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 28);

  Real u = 1 - s - t;

  phis[ 0] = -1;
  phis[ 1] =  1;
  phis[ 2] =  0;

  phis[ 3] =  4*t;
  phis[ 4] =  6*sqrt(3)*t*(t - 2*s) * sgn[0];
  phis[ 5] =  32*s*t*t;
  phis[ 6] =  25*sqrt(5)*s*t*t*(2*t - 3*s) * sgn[0];
  phis[ 7] =  192*s*s*t*t*t;

  phis[ 8] = -4*t;
  phis[ 9] =  6*sqrt(3)*t*(t - 2*u) * sgn[1];
  phis[10] = -32*t*t*u;
  phis[11] =  25*sqrt(5)*t*t*u*(2*t - 3*u) * sgn[1];
  phis[12] = -192*t*t*t*u*u;

  phis[13] =  4*(u - s);
  phis[14] = -6*sqrt(3)*(s*(s - 2*u) + u*(u - 2*s)) * sgn[2];
  phis[15] =  32*u*s*(u - s);
  phis[16] = -50*sqrt(5)*u*s*(u*u - 3*u*s + s*s) * sgn[2];
  phis[17] =  192*u*u*s*s*(u - s);

  phis[18] =  27*t*(u - s);
  phis[19] =  512*t*(u*(u - 2*s) + s*(s - 2*u))/(3*sqrt(3));
  phis[20] =  512*t*(u*(u - t) + s*(t - 2*u))/(3*sqrt(3));
  phis[21] = -3125*s*t*t*(s - 2*u)/16.;
  phis[22] =  3125*t*t*u*(u - 2*s)/16.;
  phis[23] =  6250*s*t*u*(u - s)/16.;

  phis[24] =  1458*s*t*t*u*(u - s);
  phis[25] =  46656*s*t*t*(s*(s - 3*u) - t*(s - 2*u))/(25*sqrt(5));
  phis[26] =  46656*t*t*u*(u*(u - 3*s) - t*(u - 2*s))/(25*sqrt(5));
  phis[27] = -93312*s*t*u*(s*s - 3*s*u + u*u)/(25*sqrt(5));


  phit[ 0] = -1;
  phit[ 1] =  0;
  phit[ 2] =  1;

  phit[ 3] =  4*s;
  phit[ 4] = -6*sqrt(3)*s*(s - 2*t) * sgn[0];
  phit[ 5] =  32*s*s*t;
  phit[ 6] =  25*sqrt(5)*s*s*t*(3*t - 2*s) * sgn[0];
  phit[ 7] =  192*s*s*s*t*t;

  phit[ 8] =  4*(u - t);
  phit[ 9] =  6*sqrt(3)*(u*(u - 2*t) + t*(t - 2*u)) * sgn[1];
  phit[10] =  32*t*u*(u - t);
  phit[11] =  50*sqrt(5)*t*u*(t*t - 3*u*t + u*u) * sgn[1];
  phit[12] =  192*t*t*u*u*(u - t);

  phit[13] = -4*s;
  phit[14] = -6*sqrt(3)*s*(s - 2*u) * sgn[2];
  phit[15] = -32*s*s*u;
  phit[16] = -25*sqrt(5)*s*s*u*(2*s - 3*u) * sgn[2];
  phit[17] = -192*s*s*s*u*u;

  phit[18] =  27*s*(u - t);
  phit[19] =  512*s*(u*(u - 2*t) + s*(t - u))/(3*sqrt(3));
  phit[20] =  512*s*(u*(u - 2*t) + t*(t - 2*u))/(3*sqrt(3));
  phit[21] = -3125*s*s*t*(t - 2*u)/16.;
  phit[22] =  6250*s*t*u*(u - t)/16.;
  phit[23] =  3125*s*s*u*(u - 2*t)/16.;

  phit[24] =  1458*s*s*t*u*(u - t);
  phit[25] = -46656*s*s*t*(t*(t - 3*u) - s*(t - 2*u))/(25*sqrt(5));
  phit[26] =  93312*s*t*u*(t*t - 3*t*u + u*u)/(25*sqrt(5));
  phit[27] = -46656*u*s*s*(u*(u - 3*t) - s*(u - 2*t))/(25*sqrt(5));
}


//----------------------------------------------------------------------------//
// Hierarchical: P7

void
BasisFunctionArea<Quad,Hierarchical,7>::evalBasis(
    const Real& s, const Real& t, const Int3& sgn, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 36);

  Real u = 1 - s - t;

  phi[ 0] = u;                                              // 1 @ node 1 (s = 0, t = 0)
  phi[ 1] = s;                                              // 1 @ node 2 (s = 1, t = 0)
  phi[ 2] = t;                                              // 1 @ node 3 (s = 0, t = 1)

  phi[ 3] = 4*s*t;                                          // max=1 on edge 1 @ midpoint s = 1/2, t = 1/2
  phi[ 4] = 6*sqrt(3)*s*t*(t - s) * sgn[0];                 // max=1 on edge 1 (asymmetric)
  phi[ 5] = 16*s*s*t*t;                                     // max=1 on edge 1 @ midpoint
  phi[ 6] = 25*sqrt(5)*s*s*t*t*(t - s) * sgn[0];            // max=1 on edge 1 (asymmetric)
  phi[ 7] = 64*s*s*s*t*t*t;                                 // max=1 on edge 1 @ midpoint
  phi[ 8] = 2744*sqrt(7)*s*s*s*t*t*t*(t - s)/27. * sgn[0];  // max=1 on edge 1 (asymmetric)

  phi[ 9] = 4*t*u;                                          // max=1 on edge 2 @ midpoint t = 1/2, s = 0 (u = 1/2)
  phi[10] = 6*sqrt(3)*t*u*(u - t) * sgn[1];                 // max=1 on edge 2 (asymmetric)
  phi[11] = 16*t*t*u*u;                                     // max=1 on edge 2 @ midpoint
  phi[12] = 25*sqrt(5)*t*t*u*u*(u - t) * sgn[1];            // max=1 on edge 2 (asymmetric)
  phi[13] = 64*t*t*t*u*u*u;                                 // max=1 on edge 2 @ midpoint
  phi[14] = 2744*sqrt(7)*t*t*t*u*u*u*(u - t)/27. * sgn[1];  // max=1 on edge 2 (asymmetric)

  phi[15] = 4*u*s;                                          // max=1 on edge 3 @ midpoint s = 1/2, t = 0 (u = 1/2)
  phi[16] = 6*sqrt(3)*u*s*(s - u) * sgn[2];                 // max=1 on edge 3 (asymmetric)
  phi[17] = 16*u*u*s*s;                                     // max=1 on edge 3 @ midpoint
  phi[18] = 25*sqrt(5)*u*u*s*s*(s - u) * sgn[2];            // max=1 on edge 3 (asymmetric)
  phi[19] = 64*u*u*u*s*s*s;                                 // max=1 on edge 3 @ midpoint
  phi[20] = 2744*sqrt(7)*u*u*u*s*s*s*(s - u)/27. * sgn[2];  // max=1 on edge 3 (asymmetric)

  phi[21] = 27*s*t*u;                                       // max=1 @ centroid (s,t) = 1/3
  phi[22] = 512*s*t*u*(u - s)/(3*sqrt(3));                  // max=1 @ s = (3 - sqrt(3))/8, t = 1/4
  phi[23] = 512*s*t*u*(u - t)/(3*sqrt(3));                  // max=1 @ s = 1/4, t = (3 - sqrt(3))/8
  phi[24] = 3125*s*s*t*t*u/16.;                             // max=1 @ s = 2/5, t = 2/5 (u = 1/5)
  phi[25] = 3125*t*t*u*u*s/16.;                             // max=1 @ s = 1/5, t = 2/5 (u = 2/5)
  phi[26] = 3125*u*u*s*s*t/16.;                             // max=1 @ s = 2/5, t = 1/5 (u = 2/5)

  phi[27] = 729*s*s*t*t*u*u;                                // max=1 @ centroid
  phi[28] = 46656*s*s*t*t*u*(t - s)/(25*sqrt(5));           // max=1 @ s = (5 - sqrt(5))/12, t = (5 + sqrt(5))/12 (u = 1/6)
  phi[29] = 46656*t*t*u*u*s*(u - t)/(25*sqrt(5));           // max=1 @ s = 1/6, t = (5 - sqrt(5))/12 (u = (5 + sqrt(5))/12)
  phi[30] = 46656*u*u*s*s*t*(s - u)/(25*sqrt(5));           // max=1 @ s = (5 + sqrt(5))/12, t = 1/6 (u = (5 - sqrt(5))/12)

  phi[31] = 823543*s*s*t*t*u*u*(u - s)/(100*sqrt(5));       // max=1 @ s = (5 - sqrt(5))/14, t = 2/7, u = (5 + sqrt(5))/14
  phi[32] = 823543*s*s*t*t*u*u*(u - t)/(100*sqrt(5));       // max=1 @ s = 2/7, t = (5 + sqrt(5))/14, u = (5 - sqrt(5))/14
  phi[33] = 823543*s*s*s*t*t*t*u/729.;                      // max=1 @ s = 3/7, t = 3/7 (u = 1/7)
  phi[34] = 823543*t*t*t*u*u*u*s/729.;                      // max=1 @ s = 1/7, t = 3/7 (u = 3/7)
  phi[35] = 823543*u*u*u*s*s*s*t/729.;                      // max=1 @ s = 3/7, t = 1/7 (u = 3/7)
}


void
BasisFunctionArea<Quad,Hierarchical,7>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3& sgn, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 36);

  Real u = 1 - s - t;

  phis[ 0] = -1;
  phis[ 1] =  1;
  phis[ 2] =  0;

  phis[ 3] =  4*t;
  phis[ 4] =  6*sqrt(3)*t*(t - 2*s) * sgn[0];
  phis[ 5] =  32*s*t*t;
  phis[ 6] =  25*sqrt(5)*s*t*t*(2*t - 3*s) * sgn[0];
  phis[ 7] =  192*s*s*t*t*t;
  phis[ 8] =  2744*sqrt(7)*s*s*t*t*t*(3*t - 4*s)/27. * sgn[0];

  phis[ 9] = -4*t;
  phis[10] =  6*sqrt(3)*t*(t - 2*u) * sgn[1];
  phis[11] = -32*t*t*u;
  phis[12] =  25*sqrt(5)*t*t*u*(2*t - 3*u) * sgn[1];
  phis[13] = -192*t*t*t*u*u;
  phis[14] =  2744*sqrt(7)*t*t*t*u*u*(3*t - 4*u)/27. * sgn[1];

  phis[15] =  4*(u - s);
  phis[16] = -6*sqrt(3)*(s*(s - 2*u) + u*(u - 2*s)) * sgn[2];
  phis[17] =  32*u*s*(u - s);
  phis[18] = -50*sqrt(5)*u*s*(u*u - 3*u*s + s*s) * sgn[2];
  phis[19] =  192*u*u*s*s*(u - s);
  phis[20] = -2744*sqrt(7)*u*u*s*s*(3*u*u - 8*u*s + 3*s*s)/27. * sgn[2];

  phis[21] =  27*t*(u - s);
  phis[22] =  512*t*(u*(u - 2*s) + s*(s - 2*u))/(3*sqrt(3));
  phis[23] =  512*t*(u*(u - t) + s*(t - 2*u))/(3*sqrt(3));
  phis[24] = -3125*s*t*t*(s - 2*u)/16.;
  phis[25] =  3125*t*t*u*(u - 2*s)/16.;
  phis[26] =  6250*s*t*u*(u - s)/16.;

  phis[27] =  1458*s*t*t*u*(u - s);
  phis[28] =  46656*s*t*t*(s*(s - 3*u) - t*(s - 2*u))/(25*sqrt(5));
  phis[29] =  46656*t*t*u*(u*(u - 3*s) - t*(u - 2*s))/(25*sqrt(5));
  phis[30] = -93312*s*t*u*(s*s - 3*s*u + u*u)/(25*sqrt(5));

  phis[31] =  823543*s*t*t*u*(u*u - 3*u*s + s*s)/(50*sqrt(5));
  phis[32] =  823543*s*t*t*u*(u*(2*u - 3*s) - 2*t*(u - s))/(100*sqrt(5));
  phis[33] = -823543*s*s*t*t*t*(s - 3*u)/729.;
  phis[34] =  823543*t*t*t*u*u*(u - 3*s)/729.;
  phis[35] = -823543*u*u*s*s*t*(s - u)/243.;


  phit[ 0] = -1;
  phit[ 1] =  0;
  phit[ 2] =  1;

  phit[ 3] =  4*s;
  phit[ 4] = -6*sqrt(3)*s*(s - 2*t) * sgn[0];
  phit[ 5] =  32*s*s*t;
  phit[ 6] =  25*sqrt(5)*s*s*t*(3*t - 2*s) * sgn[0];
  phit[ 7] =  192*s*s*s*t*t;
  phit[ 8] =  2744*sqrt(7)*s*s*s*t*t*(4*t - 3*s)/27. * sgn[0];

  phit[ 9] =  4*(u - t);
  phit[10] =  6*sqrt(3)*(u*(u - 2*t) + t*(t - 2*u)) * sgn[1];
  phit[11] =  32*t*u*(u - t);
  phit[12] =  50*sqrt(5)*t*u*(t*t - 3*u*t + u*u) * sgn[1];
  phit[13] =  192*t*t*u*u*(u - t);
  phit[14] =  2744*sqrt(7)*t*t*u*u*(3*t*t - 8*t*u + 3*u*u)/27. * sgn[1];

  phit[15] = -4*s;
  phit[16] = -6*sqrt(3)*s*(s - 2*u) * sgn[2];
  phit[17] = -32*s*s*u;
  phit[18] = -25*sqrt(5)*s*s*u*(2*s - 3*u) * sgn[2];
  phit[19] = -192*s*s*s*u*u;
  phit[20] = -2744*sqrt(7)*s*s*s*u*u*(3*s - 4*u)/27. * sgn[2];

  phit[21] =  27*s*(u - t);
  phit[22] =  512*s*(u*(u - 2*t) + s*(t - u))/(3*sqrt(3));
  phit[23] =  512*s*(u*(u - 2*t) + t*(t - 2*u))/(3*sqrt(3));
  phit[24] = -3125*s*s*t*(t - 2*u)/16.;
  phit[25] =  6250*s*t*u*(u - t)/16.;
  phit[26] =  3125*s*s*u*(u - 2*t)/16.;

  phit[27] =  1458*s*s*t*u*(u - t);
  phit[28] = -46656*s*s*t*(t*(t - 3*u) - s*(t - 2*u))/(25*sqrt(5));
  phit[29] =  93312*s*t*u*(t*t - 3*t*u + u*u)/(25*sqrt(5));
  phit[30] = -46656*u*s*s*(u*(u - 3*t) - s*(u - 2*t))/(25*sqrt(5));

  phit[31] =  823543*s*s*t*u*(u*(2*u - 3*t) - 2*s*(u - t))/(100*sqrt(5));
  phit[32] =  823543*s*s*t*u*(u*u - 3*u*t + t*t)/(50*sqrt(5));
  phit[33] = -823543*s*s*s*t*t*(t - 3*u)/729.;
  phit[34] =  823543*s*t*t*u*u*(u - t)/243.;
  phit[35] =  823543*u*u*s*s*s*(u - 3*t)/729.;
}
#endif

}
