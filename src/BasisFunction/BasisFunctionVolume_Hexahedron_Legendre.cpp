// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionVolume_Hexahedron_Legendre.h"

// Construct tensor products with the linear Legendre basis
#include "BasisFunctionLine_Legendre.h"

#include "tools/SANSException.h"

namespace SANS
{

void
BasisFunctionVolume<Hex,Legendre,0>::evalBasis( const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phi[0] = 1;
}


void
BasisFunctionVolume<Hex,Legendre,0>::evalBasisDerivative( const Real&, const Real&, const Real&, const Int6&,
                                                              Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phis[0] = 0;

  phit[0] = 0;

  phiu[0] = 0;
}

void
BasisFunctionVolume<Hex,Legendre,0>::evalBasisHessianDerivative( const Real&, const Real&, const Real&, const Int6&,
    Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi==1);
  phiss[0] = 0;
  phist[0] = 0;
  phitt[0] = 0;
  phisu[0] = 0;
  phitu[0] = 0;
  phiuu[0] = 0;
}


//----------------------------------------------------------------------------//
// Legendre: linear
void
BasisFunctionVolume<Hex,Legendre,1>::tensorProduct(
    const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const
{
  // P0
  phi[0] = sphi[0]*tphi[0]*uphi[0];

  // P1
  phi[1] = sphi[1]*tphi[0]*uphi[0];
  phi[2] = sphi[0]*tphi[1]*uphi[0];
  phi[3] = sphi[1]*tphi[1]*uphi[0];

  phi[4] = sphi[0]*tphi[0]*uphi[1];
  phi[5] = sphi[1]*tphi[0]*uphi[1];
  phi[6] = sphi[0]*tphi[1]*uphi[1];
  phi[7] = sphi[1]*tphi[1]*uphi[1];
}

void
BasisFunctionVolume<Hex,Legendre,1>::evalBasis(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real sphi[2], tphi[2], uphi[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasis(s, sphi, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasis(t, tphi, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasis(u, uphi, 2);

  tensorProduct(sphi, tphi, uphi, phi);
}


void
BasisFunctionVolume<Hex,Legendre,1>::evalBasisDerivative(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real sphi[2], tphi[2], uphi[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasis(s, sphi, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasis(t, tphi, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasis(u, uphi, 2);

  Real sphis[2], tphit[2], uphiu[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(s, sphis, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(t, tphit, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(u, uphiu, 2);

  tensorProduct(sphis, tphi , uphi , phis);
  tensorProduct(sphi , tphit, uphi , phit);
  tensorProduct(sphi , tphi , uphiu, phiu);
}


void
BasisFunctionVolume<Hex,Legendre,1>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Real& u, const Int6&,
    Real phiss[],
    Real phist[], Real phitt[],
    Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real sphi[2], tphi[2], uphi[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasis(s, sphi, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasis(t, tphi, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasis(u, uphi, 2);

  Real sphis[2], tphit[2], uphiu[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(s, sphis, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(t, tphit, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasisDerivative(u, uphiu, 2);

  Real sphiss[2], tphitt[2], uphiuu[2];
  BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(s, sphiss, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(t, tphitt, 2);
  BasisFunctionLine<Legendre,1>::self()->evalBasisHessianDerivative(u, uphiuu, 2);

  tensorProduct(sphiss, tphi  , uphi  , phiss);
  tensorProduct(sphis , tphit , uphi  , phist);
  tensorProduct(sphi  , tphitt, uphi  , phitt);
  tensorProduct(sphis , tphi  , uphiu , phisu);
  tensorProduct(sphi  , tphit , uphiu , phitu);
  tensorProduct(sphi  , tphi  , uphiuu, phiuu);
}


//----------------------------------------------------------------------------//
// Legendre: quadratic
void
BasisFunctionVolume<Hex,Legendre,2>::tensorProduct(
    const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const
{
  // P0
  phi[0] = sphi[0]*tphi[0]*uphi[0];

  // P1
  phi[1] = sphi[1]*tphi[0]*uphi[0];
  phi[2] = sphi[0]*tphi[1]*uphi[0];
  phi[3] = sphi[1]*tphi[1]*uphi[0];

  phi[4] = sphi[0]*tphi[0]*uphi[1];
  phi[5] = sphi[1]*tphi[0]*uphi[1];
  phi[6] = sphi[0]*tphi[1]*uphi[1];
  phi[7] = sphi[1]*tphi[1]*uphi[1];

  // P2
  phi[8]  = sphi[2]*tphi[0]*uphi[0];
  phi[9]  = sphi[0]*tphi[2]*uphi[0];
  phi[10] = sphi[2]*tphi[1]*uphi[0];
  phi[11] = sphi[1]*tphi[2]*uphi[0];
  phi[12] = sphi[2]*tphi[2]*uphi[0];

  phi[13] = sphi[2]*tphi[0]*uphi[1];
  phi[14] = sphi[0]*tphi[2]*uphi[1];
  phi[15] = sphi[2]*tphi[1]*uphi[1];
  phi[16] = sphi[1]*tphi[2]*uphi[1];
  phi[17] = sphi[2]*tphi[2]*uphi[1];

  phi[18] = sphi[0]*tphi[0]*uphi[2];
  phi[19] = sphi[1]*tphi[0]*uphi[2];
  phi[20] = sphi[0]*tphi[1]*uphi[2];
  phi[21] = sphi[1]*tphi[1]*uphi[2];
  phi[22] = sphi[2]*tphi[0]*uphi[2];
  phi[23] = sphi[0]*tphi[2]*uphi[2];
  phi[24] = sphi[2]*tphi[1]*uphi[2];
  phi[25] = sphi[1]*tphi[2]*uphi[2];
  phi[26] = sphi[2]*tphi[2]*uphi[2];
}

void
BasisFunctionVolume<Hex,Legendre,2>::evalBasis(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 27);

  Real sphi[3], tphi[3], uphi[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasis(t, tphi, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasis(u, uphi, 3);

  tensorProduct(sphi, tphi, uphi, phi);
}


void
BasisFunctionVolume<Hex,Legendre,2>::evalBasisDerivative(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 27);

  Real sphi[3], tphi[3], uphi[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasis(t, tphi, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasis(u, uphi, 3);

  Real sphis[3], tphit[3], uphiu[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(s, sphis, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(t, tphit, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(u, uphiu, 3);

  tensorProduct(sphis, tphi , uphi , phis);
  tensorProduct(sphi , tphit, uphi , phit);
  tensorProduct(sphi , tphi , uphiu, phiu);
}


void
BasisFunctionVolume<Hex,Legendre,2>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Real& u, const Int6&,
    Real phiss[],
    Real phist[], Real phitt[],
    Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 27);

  Real sphi[3], tphi[3], uphi[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasis(t, tphi, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasis(u, uphi, 3);

  Real sphis[3], tphit[3], uphiu[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(s, sphis, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(t, tphit, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasisDerivative(u, uphiu, 3);

  Real sphiss[3], tphitt[3], uphiuu[3];
  BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(s, sphiss, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(t, tphitt, 3);
  BasisFunctionLine<Legendre,2>::self()->evalBasisHessianDerivative(u, uphiuu, 3);

  tensorProduct(sphiss, tphi  , uphi  , phiss);
  tensorProduct(sphis , tphit , uphi  , phist);
  tensorProduct(sphi  , tphitt, uphi  , phitt);
  tensorProduct(sphis , tphi  , uphiu , phisu);
  tensorProduct(sphi  , tphit , uphiu , phitu);
  tensorProduct(sphi  , tphi  , uphiuu, phiuu);
}


//----------------------------------------------------------------------------//
// Legendre: cubic
void
BasisFunctionVolume<Hex,Legendre,3>::tensorProduct(
    const Real sphi[], const Real tphi[], const Real uphi[], Real phi[] ) const
{
  // P0
  phi[0] = sphi[0]*tphi[0]*uphi[0];

  // P1
  phi[1] = sphi[1]*tphi[0]*uphi[0];
  phi[2] = sphi[0]*tphi[1]*uphi[0];
  phi[3] = sphi[1]*tphi[1]*uphi[0];

  phi[4] = sphi[0]*tphi[0]*uphi[1];
  phi[5] = sphi[1]*tphi[0]*uphi[1];
  phi[6] = sphi[0]*tphi[1]*uphi[1];
  phi[7] = sphi[1]*tphi[1]*uphi[1];

  // P2
  phi[8]  = sphi[2]*tphi[0]*uphi[0];
  phi[9]  = sphi[0]*tphi[2]*uphi[0];
  phi[10] = sphi[2]*tphi[1]*uphi[0];
  phi[11] = sphi[1]*tphi[2]*uphi[0];
  phi[12] = sphi[2]*tphi[2]*uphi[0];

  phi[13] = sphi[2]*tphi[0]*uphi[1];
  phi[14] = sphi[0]*tphi[2]*uphi[1];
  phi[15] = sphi[2]*tphi[1]*uphi[1];
  phi[16] = sphi[1]*tphi[2]*uphi[1];
  phi[17] = sphi[2]*tphi[2]*uphi[1];

  phi[18] = sphi[0]*tphi[0]*uphi[2];
  phi[19] = sphi[1]*tphi[0]*uphi[2];
  phi[20] = sphi[0]*tphi[1]*uphi[2];
  phi[21] = sphi[1]*tphi[1]*uphi[2];
  phi[22] = sphi[2]*tphi[0]*uphi[2];
  phi[23] = sphi[0]*tphi[2]*uphi[2];
  phi[24] = sphi[2]*tphi[1]*uphi[2];
  phi[25] = sphi[1]*tphi[2]*uphi[2];
  phi[26] = sphi[2]*tphi[2]*uphi[2];

  // P3
  phi[27] = sphi[3]*tphi[0]*uphi[0];
  phi[28] = sphi[0]*tphi[3]*uphi[0];
  phi[29] = sphi[3]*tphi[1]*uphi[0];
  phi[30] = sphi[1]*tphi[3]*uphi[0];
  phi[31] = sphi[3]*tphi[2]*uphi[0];
  phi[32] = sphi[2]*tphi[3]*uphi[0];
  phi[33] = sphi[3]*tphi[3]*uphi[0];

  phi[34] = sphi[3]*tphi[0]*uphi[1];
  phi[35] = sphi[0]*tphi[3]*uphi[1];
  phi[36] = sphi[3]*tphi[1]*uphi[1];
  phi[37] = sphi[1]*tphi[3]*uphi[1];
  phi[38] = sphi[3]*tphi[2]*uphi[1];
  phi[39] = sphi[2]*tphi[3]*uphi[1];
  phi[40] = sphi[3]*tphi[3]*uphi[1];

  phi[41] = sphi[3]*tphi[0]*uphi[2];
  phi[42] = sphi[0]*tphi[3]*uphi[2];
  phi[43] = sphi[3]*tphi[1]*uphi[2];
  phi[44] = sphi[1]*tphi[3]*uphi[2];
  phi[45] = sphi[3]*tphi[2]*uphi[2];
  phi[46] = sphi[2]*tphi[3]*uphi[2];
  phi[47] = sphi[3]*tphi[3]*uphi[2];

  phi[48] = sphi[0]*tphi[0]*uphi[3];
  phi[49] = sphi[1]*tphi[0]*uphi[3];
  phi[50] = sphi[2]*tphi[0]*uphi[3];
  phi[51] = sphi[3]*tphi[0]*uphi[3];
  phi[52] = sphi[0]*tphi[1]*uphi[3];
  phi[53] = sphi[0]*tphi[2]*uphi[3];
  phi[54] = sphi[0]*tphi[3]*uphi[3];
  phi[55] = sphi[1]*tphi[1]*uphi[3];
  phi[56] = sphi[2]*tphi[1]*uphi[3];
  phi[57] = sphi[3]*tphi[1]*uphi[3];
  phi[58] = sphi[1]*tphi[2]*uphi[3];
  phi[59] = sphi[2]*tphi[2]*uphi[3];
  phi[60] = sphi[3]*tphi[2]*uphi[3];
  phi[61] = sphi[1]*tphi[3]*uphi[3];
  phi[62] = sphi[2]*tphi[3]*uphi[3];
  phi[63] = sphi[3]*tphi[3]*uphi[3];
}

void
BasisFunctionVolume<Hex,Legendre,3>::evalBasis(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 64);

  Real sphi[4], tphi[4], uphi[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasis(t, tphi, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasis(u, uphi, 4);

  tensorProduct(sphi, tphi, uphi, phi);
}


void
BasisFunctionVolume<Hex,Legendre,3>::evalBasisDerivative(
    const Real& s, const Real& t, const Real& u, const Int6&, Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 64);

  Real sphi[4], tphi[4], uphi[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasis(t, tphi, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasis(u, uphi, 4);

  Real sphis[4], tphit[4], uphiu[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(s, sphis, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(t, tphit, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(u, uphiu, 4);

  tensorProduct(sphis, tphi , uphi , phis);
  tensorProduct(sphi , tphit, uphi , phit);
  tensorProduct(sphi , tphi , uphiu, phiu);
}


void
BasisFunctionVolume<Hex,Legendre,3>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Real& u, const Int6&,
    Real phiss[],
    Real phist[], Real phitt[],
    Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 64);

  Real sphi[4], tphi[4], uphi[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasis(t, tphi, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasis(u, uphi, 4);

  Real sphis[4], tphit[4], uphiu[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(s, sphis, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(t, tphit, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasisDerivative(u, uphiu, 4);

  Real sphiss[4], tphitt[4], uphiuu[4];
  BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(s, sphiss, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(t, tphitt, 4);
  BasisFunctionLine<Legendre,3>::self()->evalBasisHessianDerivative(u, uphiuu, 4);

  tensorProduct(sphiss, tphi  , uphi  , phiss);
  tensorProduct(sphis , tphit , uphi  , phist);
  tensorProduct(sphi  , tphitt, uphi  , phitt);
  tensorProduct(sphis , tphi  , uphiu , phisu);
  tensorProduct(sphi  , tphit , uphiu , phitu);
  tensorProduct(sphi  , tphi  , uphiuu, phiuu);
}
}
