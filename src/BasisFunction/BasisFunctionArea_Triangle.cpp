// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// area basis functions triangle

#include <string>
#include <iostream>
#include <limits>

#include "BasisFunctionArea.h"
#include "BasisFunctionArea_Triangle.h"
#include "tools/SANSException.h"

#include "Quadrature/QuadratureArea.h"
#include "Quadrature/QuadratureLine.h"
#include "Quadrature_Cache.h"

#include "TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//Instantiate the singletons
const BasisFunctionArea<Triangle, Hierarchical, 1>*
      BasisFunctionArea<Triangle, Hierarchical, 1>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Hierarchical, 2>*
      BasisFunctionArea<Triangle, Hierarchical, 2>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Hierarchical, 3>*
      BasisFunctionArea<Triangle, Hierarchical, 3>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Hierarchical, 4>*
      BasisFunctionArea<Triangle, Hierarchical, 4>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Hierarchical, 5>*
      BasisFunctionArea<Triangle, Hierarchical, 5>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Hierarchical, 6>*
      BasisFunctionArea<Triangle, Hierarchical, 6>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Hierarchical, 7>*
      BasisFunctionArea<Triangle, Hierarchical, 7>::self() { static const BasisFunctionArea singleton; return &singleton; }

const BasisFunctionArea<Triangle, Legendre, 0>*
      BasisFunctionArea<Triangle, Legendre, 0>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Legendre, 1>*
      BasisFunctionArea<Triangle, Legendre, 1>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Legendre, 2>*
      BasisFunctionArea<Triangle, Legendre, 2>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Legendre, 3>*
      BasisFunctionArea<Triangle, Legendre, 3>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Legendre, 4>*
      BasisFunctionArea<Triangle, Legendre, 4>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Legendre, 5>*
      BasisFunctionArea<Triangle, Legendre, 5>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Legendre, 6>*
      BasisFunctionArea<Triangle, Legendre, 6>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Legendre, 7>*
      BasisFunctionArea<Triangle, Legendre, 7>::self() { static const BasisFunctionArea singleton; return &singleton; }

const BasisFunctionArea<Triangle, Lagrange, 1>*
      BasisFunctionArea<Triangle, Lagrange, 1>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Lagrange, 2>*
      BasisFunctionArea<Triangle, Lagrange, 2>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Lagrange, 3>*
      BasisFunctionArea<Triangle, Lagrange, 3>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Lagrange, 4>*
      BasisFunctionArea<Triangle, Lagrange, 4>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Lagrange, 5>*
      BasisFunctionArea<Triangle, Lagrange, 5>::self() { static const BasisFunctionArea singleton; return &singleton; }

const BasisFunctionArea<Triangle, Bernstein, 1>*
      BasisFunctionArea<Triangle, Bernstein, 1>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Bernstein, 2>*
      BasisFunctionArea<Triangle, Bernstein, 2>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Bernstein, 3>*
      BasisFunctionArea<Triangle, Bernstein, 3>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, Bernstein, 4>*
      BasisFunctionArea<Triangle, Bernstein, 4>::self() { static const BasisFunctionArea singleton; return &singleton; }

const BasisFunctionArea<Triangle, RBS, 1>*
      BasisFunctionArea<Triangle, RBS, 1>::self() { static const BasisFunctionArea singleton; return &singleton; }
const BasisFunctionArea<Triangle, RBS, 2>*
      BasisFunctionArea<Triangle, RBS, 2>::self() { static const BasisFunctionArea singleton; return &singleton; }

//----------------------------------------------------------------------------//
//Initialize the base class pointers
// Hierarchical
template<>
const BasisFunctionArea<Triangle, Hierarchical, 1>*
BasisFunctionAreaBase<Triangle>::HierarchicalP1 = BasisFunctionArea<Triangle, Hierarchical, 1>::self();

template<>
const BasisFunctionArea<Triangle, Hierarchical, 2>*
BasisFunctionAreaBase<Triangle>::HierarchicalP2 = BasisFunctionArea<Triangle, Hierarchical, 2>::self();

template<>
const BasisFunctionArea<Triangle, Hierarchical, 3>*
BasisFunctionAreaBase<Triangle>::HierarchicalP3 = BasisFunctionArea<Triangle, Hierarchical, 3>::self();

template<>
const BasisFunctionArea<Triangle, Hierarchical, 4>*
BasisFunctionAreaBase<Triangle>::HierarchicalP4 = BasisFunctionArea<Triangle, Hierarchical, 4>::self();

template<>
const BasisFunctionArea<Triangle, Hierarchical, 5>*
BasisFunctionAreaBase<Triangle>::HierarchicalP5 = BasisFunctionArea<Triangle, Hierarchical, 5>::self();

template<>
const BasisFunctionArea<Triangle, Hierarchical, 6>*
BasisFunctionAreaBase<Triangle>::HierarchicalP6 = BasisFunctionArea<Triangle, Hierarchical, 6>::self();

template<>
const BasisFunctionArea<Triangle, Hierarchical, 7>*
BasisFunctionAreaBase<Triangle>::HierarchicalP7 = BasisFunctionArea<Triangle, Hierarchical, 7>::self();


// Legendre
template<>
const BasisFunctionArea<Triangle, Legendre, 0>*
BasisFunctionAreaBase<Triangle>::LegendreP0 = BasisFunctionArea<Triangle, Legendre, 0>::self();

template<>
const BasisFunctionArea<Triangle, Legendre, 1>*
BasisFunctionAreaBase<Triangle>::LegendreP1 = BasisFunctionArea<Triangle, Legendre, 1>::self();

template<>
const BasisFunctionArea<Triangle, Legendre, 2>*
BasisFunctionAreaBase<Triangle>::LegendreP2 = BasisFunctionArea<Triangle, Legendre, 2>::self();

template<>
const BasisFunctionArea<Triangle, Legendre, 3>*
BasisFunctionAreaBase<Triangle>::LegendreP3 = BasisFunctionArea<Triangle, Legendre, 3>::self();

template<>
const BasisFunctionArea<Triangle, Legendre, 4>*
BasisFunctionAreaBase<Triangle>::LegendreP4 = BasisFunctionArea<Triangle, Legendre, 4>::self();

template<>
const BasisFunctionArea<Triangle, Legendre, 5>*
BasisFunctionAreaBase<Triangle>::LegendreP5 = BasisFunctionArea<Triangle, Legendre, 5>::self();

template<>
const BasisFunctionArea<Triangle, Legendre, 6>*
BasisFunctionAreaBase<Triangle>::LegendreP6 = BasisFunctionArea<Triangle, Legendre, 6>::self();

template<>
const BasisFunctionArea<Triangle, Legendre, 7>*
BasisFunctionAreaBase<Triangle>::LegendreP7 = BasisFunctionArea<Triangle, Legendre, 7>::self();

// Lagrange
template<>
const BasisFunctionArea<Triangle, Lagrange, 1>*
BasisFunctionAreaBase<Triangle>::LagrangeP1 = BasisFunctionArea<Triangle, Lagrange, 1>::self();

template<>
const BasisFunctionArea<Triangle, Lagrange, 2>*
BasisFunctionAreaBase<Triangle>::LagrangeP2 = BasisFunctionArea<Triangle, Lagrange, 2>::self();

template<>
const BasisFunctionArea<Triangle, Lagrange, 3>*
BasisFunctionAreaBase<Triangle>::LagrangeP3 = BasisFunctionArea<Triangle, Lagrange, 3>::self();

template<>
const BasisFunctionArea<Triangle, Lagrange, 4>*
BasisFunctionAreaBase<Triangle>::LagrangeP4 = BasisFunctionArea<Triangle, Lagrange, 4>::self();

template<>
const BasisFunctionArea<Triangle, Lagrange, 5>*
BasisFunctionAreaBase<Triangle>::LagrangeP5 = BasisFunctionArea<Triangle, Lagrange, 5>::self();


// Bernstein
template<>
const BasisFunctionArea<Triangle, Bernstein, 1>*
BasisFunctionAreaBase<Triangle>::BernsteinP1 = BasisFunctionArea<Triangle, Bernstein, 1>::self();

template<>
const BasisFunctionArea<Triangle, Bernstein, 2>*
BasisFunctionAreaBase<Triangle>::BernsteinP2 = BasisFunctionArea<Triangle, Bernstein, 2>::self();

template<>
const BasisFunctionArea<Triangle, Bernstein, 3>*
BasisFunctionAreaBase<Triangle>::BernsteinP3 = BasisFunctionArea<Triangle, Bernstein, 3>::self();

template<>
const BasisFunctionArea<Triangle, Bernstein, 4>*
BasisFunctionAreaBase<Triangle>::BernsteinP4 = BasisFunctionArea<Triangle, Bernstein, 4>::self();

// Rational Bezier Splies
template<>
const BasisFunctionArea<Triangle, RBS, 1>*
BasisFunctionAreaBase<Triangle>::RBSP1 = BasisFunctionArea<Triangle, RBS, 1>::self();

template<>
const BasisFunctionArea<Triangle, RBS, 2>*
BasisFunctionAreaBase<Triangle>::RBSP2 = BasisFunctionArea<Triangle, RBS, 2>::self();


//----------------------------------------------------------------------------//
template<>
const BasisFunctionAreaBase<Triangle>*
BasisFunctionAreaBase<Triangle>::getBasisFunction( const int order, const BasisFunctionCategory& category )
{
  if (category == BasisFunctionCategory_Hierarchical)
  {
    switch (order)
    {
    case 1:
      return BasisFunctionArea<Triangle, Hierarchical, 1>::self();
    case 2:
      return BasisFunctionArea<Triangle, Hierarchical, 2>::self();
    case 3:
      return BasisFunctionArea<Triangle, Hierarchical, 3>::self();
    case 4:
      return BasisFunctionArea<Triangle, Hierarchical, 4>::self();
    case 5:
      return BasisFunctionArea<Triangle, Hierarchical, 5>::self();
    case 6:
      return BasisFunctionArea<Triangle, Hierarchical, 6>::self();
    case 7:
      return BasisFunctionArea<Triangle, Hierarchical, 7>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionAreaBase<Triangle>::getBasisFunction: unexpected hierarchical order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Legendre)
  {
    switch (order)
    {
    case 0:
      return BasisFunctionArea<Triangle, Legendre, 0>::self();
    case 1:
      return BasisFunctionArea<Triangle, Legendre, 1>::self();
    case 2:
      return BasisFunctionArea<Triangle, Legendre, 2>::self();
    case 3:
      return BasisFunctionArea<Triangle, Legendre, 3>::self();
    case 4:
      return BasisFunctionArea<Triangle, Legendre, 4>::self();
    case 5:
      return BasisFunctionArea<Triangle, Legendre, 5>::self();
    case 6:
      return BasisFunctionArea<Triangle, Legendre, 6>::self();
    case 7:
      return BasisFunctionArea<Triangle, Legendre, 7>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionAreaBase<Triangle>::getBasisFunction: unexpected Legendre order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Lagrange)
  {
    switch (order)
    {
    case 1:
      return BasisFunctionArea<Triangle, Lagrange, 1>::self();
    case 2:
      return BasisFunctionArea<Triangle, Lagrange, 2>::self();
    case 3:
      return BasisFunctionArea<Triangle, Lagrange, 3>::self();
    case 4:
      return BasisFunctionArea<Triangle, Lagrange, 4>::self();
    case 5:
      return BasisFunctionArea<Triangle, Lagrange, 5>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionAreaBase<Triangle>::getBasisFunction: unexpected Lagrange order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Bernstein)
  {
    switch (order)
    {
    case 1:
      return BasisFunctionArea<Triangle, Bernstein, 1>::self();
    case 2:
      return BasisFunctionArea<Triangle, Bernstein, 2>::self();
    case 3:
      return BasisFunctionArea<Triangle, Bernstein, 3>::self();
    case 4:
      return BasisFunctionArea<Triangle, Bernstein, 4>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionAreaBase<Triangle>::getBasisFunction: unexpected Bernstein order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_RBS)
    {
      switch (order)
      {
      case 1:
        return BasisFunctionArea<Triangle, RBS, 1>::self();
      case 2:
        return BasisFunctionArea<Triangle, RBS, 2>::self();
      default:
        SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionAreaBase<Triangle>::getBasisFunction: unexpected RBS order = %d\n", order );
        break;
      }
    }
  SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionAreaBase<Triangle>::getBasisFunction: unexpected basis function category = %d\n",
                            static_cast<int>(category) );

  //Just so the compiler will not complain
  return NULL;
}

//===========================================================================//

template<class Topology>
QuadratureCache<Topology>::QuadratureCache()
{
  if ( BasisFunctionCategory_Hierarchical != 4 )
  {
    // Exceptions don't get printed during static initialization, probably a boost test thing
    std::cout << "You need to add the maximum order for the new basis" << std::endl;
    SANS_ASSERT(false);
  }

  BasisFunctionCategory categores[BasisFunctionCategory_Hierarchical];
  int Pmin[BasisFunctionCategory_Hierarchical];
  int Pmax[BasisFunctionCategory_Hierarchical];

  categores[BasisFunctionCategory_Legendre] = BasisFunctionCategory_Legendre;
  categores[BasisFunctionCategory_Lagrange] = BasisFunctionCategory_Lagrange;
  categores[BasisFunctionCategory_Bernstein] = BasisFunctionCategory_Bernstein;
  categores[BasisFunctionCategory_RBS] = BasisFunctionCategory_RBS;

  Pmin[BasisFunctionCategory_Legendre] = 0;
  Pmin[BasisFunctionCategory_Lagrange] = 1;
  Pmin[BasisFunctionCategory_Bernstein] = 1;
  Pmin[BasisFunctionCategory_RBS] = 1;

  Pmax[BasisFunctionCategory_Legendre] = BasisFunctionArea_Triangle_LegendrePMax+1;
  Pmax[BasisFunctionCategory_Lagrange] = BasisFunctionArea_Triangle_LagrangePMax+1;
  Pmax[BasisFunctionCategory_Bernstein] = BasisFunctionArea_Triangle_BernsteinPMax+1;
  Pmax[BasisFunctionCategory_RBS] = BasisFunctionArea_Triangle_RBSPMax+1;
  cell.resize(BasisFunctionCategory_Hierarchical);
  trace.resize(BasisFunctionCategory_Hierarchical);
  for (int icat = 0; icat < BasisFunctionCategory_Hierarchical; icat++)
  {
//    IntTrace sgn; // Should not be used by these basis functions

    BasisFunctionCategory cat = categores[icat];

    cell[cat].resize(Pmax[cat]);
    trace[cat].resize(Pmax[cat]);
    for (int poly_order = Pmin[cat]; poly_order < Pmax[cat]; poly_order++)
    {
//      const BasisFunctionAreaBase<Triangle>* basis =
//          BasisFunctionAreaBase<Triangle>::getBasisFunction(poly_order, cat);

      QuadratureBasisPointStore<D>& pointStore = cell[cat][poly_order];
      pointStore.category = cat;
      pointStore.poly_order = poly_order;
      pointStore.eval.resize(QuadratureRule::eNone);

      pointStore.eval[QuadratureRule::eGauss].resize(QuadratureArea<Triangle>::nOrderIdx);
#if 0
      for (int orderidx = 0; orderidx < QuadratureArea<Triangle>::nOrderIdx; orderidx++)
      {
        QuadratureArea<Triangle> quadrature( QuadratureArea<Triangle>::getOrderFromIndex(orderidx) );
        pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

        Real s, t;
        for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
        {
          QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

          point.phi.resize(basis->nBasis());
          point.dphi[0].resize(basis->nBasis());
          point.dphi[1].resize(basis->nBasis());

          quadrature.coordinates(iquad, s, t);
          basis->evalBasis( s, t, sgn, point.phi.data(), point.phi.size() );

          basis->evalBasisDerivative( s, t, sgn, point.dphi[0].data(), point.dphi[1].data(),
                                                 point.dphi[0].size() );
        } // iquad
      } // orderidx
#endif

      trace[cat][poly_order].resize(Triangle::NTrace);
      for (int itrace = 0; itrace < Triangle::NTrace; itrace++)
      {
        QuadratureBasisPointStore<D>& pointStore = trace[cat][poly_order][itrace];
        pointStore.category = cat;
        pointStore.poly_order = poly_order;
        pointStore.eval.resize(QuadratureRule::eNone);

        CanonicalTraceToCell canonicalTrace(itrace, 1);

        pointStore.eval[QuadratureRule::eGauss].resize(QuadratureLine::nOrderIdx);
#if 0
        for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
        {
          QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );
          pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

          Real sTrace;
          Real sCell, tCell;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

            point.phi.resize(basis->nBasis());
            point.dphi[0].resize(basis->nBasis());
            point.dphi[1].resize(basis->nBasis());

            quadrature.coordinate(iquad, sTrace);

            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sTrace, sCell, tCell );

            basis->evalBasis( sCell, tCell, sgn, point.phi.data(), point.phi.size() );

            basis->evalBasisDerivative( sCell, tCell, sgn, point.dphi[0].data(), point.dphi[1].data(),
                                                           point.dphi[0].size() );
          } //iquad
        } // orderidx
#endif
      } // itrace

    } // poly_order
  } // icat

  //-------------------------------------------------------------------------//
  // Hierarchical basis functions are special with the edge sign
  //-------------------------------------------------------------------------//

  const int nEdgeSign = 8;

  IntTrace edgeSigns[nEdgeSign] = { {{ 1, 1, 1}},
                                    {{-1, 1, 1}},
                                    {{ 1,-1, 1}},
                                    {{ 1, 1,-1}},
                                    {{-1,-1, 1}},
                                    {{ 1,-1,-1}},
                                    {{-1, 1,-1}},
                                    {{-1,-1,-1}} };

  hierarchicalCell.resize(BasisFunctionArea_Triangle_HierarchicalPMax+1);
  hierarchicalTrace.resize(BasisFunctionArea_Triangle_HierarchicalPMax+1);
  for (int poly_order = 1; poly_order < BasisFunctionArea_Triangle_HierarchicalPMax+1; poly_order++)
  {
//    const BasisFunctionAreaBase<Triangle>* basis =
//        BasisFunctionAreaBase<Triangle>::getBasisFunction(poly_order, BasisFunctionCategory_Hierarchical);

    for (int isgn = 0; isgn < nEdgeSign; isgn++)
    {
      IntTrace sgn = edgeSigns[isgn];
      QuadratureBasisPointStore<D>& pointStore = hierarchicalCell[poly_order][sgn];
      pointStore.category = BasisFunctionCategory_Hierarchical;
      pointStore.poly_order = poly_order;
      pointStore.eval.resize(QuadratureRule::eNone);

      pointStore.eval[QuadratureRule::eGauss].resize(QuadratureArea<Triangle>::nOrderIdx);
#if 0
      for (int orderidx = 0; orderidx < QuadratureArea<Triangle>::nOrderIdx; orderidx++)
      {
        QuadratureArea<Triangle> quadrature( QuadratureArea<Triangle>::getOrderFromIndex(orderidx) );
        pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

        Real s, t;
        for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
        {
          QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

          point.phi.resize(basis->nBasis());
          point.dphi[0].resize(basis->nBasis());
          point.dphi[1].resize(basis->nBasis());

          quadrature.coordinates(iquad, s, t);
          basis->evalBasis( s, t, sgn, point.phi.data(), point.phi.size() );

          basis->evalBasisDerivative( s, t, sgn, point.dphi[0].data(), point.dphi[1].data(),
                                                 point.dphi[0].size() );
        } //iqaud
      } // orderidx
#endif

      hierarchicalTrace[poly_order][sgn].resize(Triangle::NTrace);
      for (int itrace = 0; itrace < Triangle::NTrace; itrace++)
      {
        QuadratureBasisPointStore<D>& pointStore = hierarchicalTrace[poly_order][sgn][itrace];
        pointStore.category = BasisFunctionCategory_Hierarchical;
        pointStore.poly_order = poly_order;
        pointStore.eval.resize(QuadratureRule::eNone);

        CanonicalTraceToCell canonicalTrace(itrace, 1);

        pointStore.eval[QuadratureRule::eGauss].resize(QuadratureLine::nOrderIdx);
#if 0
        for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
        {
          QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );
          pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

          Real sTrace;
          Real sCell, tCell;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

            point.phi.resize(basis->nBasis());
            point.dphi[0].resize(basis->nBasis());
            point.dphi[1].resize(basis->nBasis());

            quadrature.coordinate(iquad, sTrace);

            TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTrace, sTrace, sCell, tCell );

            basis->evalBasis( sCell, tCell, sgn, point.phi.data(), point.phi.size() );

            basis->evalBasisDerivative( sCell, tCell, sgn, point.dphi[0].data(), point.dphi[1].data(),
                                                           point.dphi[0].size() );
          } //iquad
        } // orderidx
#endif
      } // itrace

    } // isgn
  } // poly_order

  //-------------------------------------------------------------------------//
  // Trace orientation map that maps any orientation back to the canonical
  //-------------------------------------------------------------------------//

  Real reftol = std::numeric_limits<Real>::max();

  {
    // find the smallest distance between any two quadrature points
    // this will be the tolerance for the equality check between cell quadrature points
    QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(QuadratureLine::nOrderIdx-1) );
    Real s0, s1;
    for (int iquad0 = 0; iquad0 < quadrature.nQuadrature(); iquad0++)
    {
      quadrature.coordinate(iquad0, s0);

      for (int iquad1 = 0; iquad1 < quadrature.nQuadrature(); iquad1++)
      {
        if (iquad0 == iquad1) continue; // the same point will have zero difference...

        quadrature.coordinate(iquad1, s1);

        reftol = std::min( reftol, fabs(s1 - s0) );
      }
    }

    // give a nice margin
    reftol /= 10;
  }

  traceOrientMap.resize(1); // orientation
  for (int orientation : {-1})
  {
    TraceOrientMap& orientMap = traceOrientMap[0];
    orientMap.canonical.resize(QuadratureRule::eNone);

    orientMap.canonical[QuadratureRule::eGauss].resize(QuadratureLine::nOrderIdx);
    for (int orderidx = 0; orderidx < QuadratureLine::nOrderIdx; orderidx++)
    {
      QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(orderidx) );
      orientMap.canonical[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature(), -1 );

      // Left canonical trace with canonical orientation of 1 (trace does not matter)
      CanonicalTraceToCell canonicalTraceL(0, 1);
      CanonicalTraceToCell canonicalTraceR(0, orientation); // other orientation

      Real sTrace;
      Real sCellL, tCellL;
      for (int iquadL = 0; iquadL < quadrature.nQuadrature(); iquadL++)
      {
        // Get the quadrature point on the trace
        quadrature.coordinate(iquadL, sTrace);

        // get the cell reference coordiantes
        TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTraceL, sTrace, sCellL, tCellL );

        // loop over all quadrature points again, and find the same cell point with the right orientation
        int iquadR;
        for (iquadR = 0; iquadR < quadrature.nQuadrature(); iquadR++)
        {
          quadrature.coordinate(iquadR, sTrace);

          Real sCellR, tCellR;
          TraceToCellRefCoord<Line, TopoD2, Triangle>::eval( canonicalTraceR, sTrace, sCellR, tCellR );
          if ( fabs(sCellR - sCellL) < reftol && fabs(tCellR - tCellL) < reftol )
          {
            if ( orientMap.canonical[QuadratureRule::eGauss][orderidx][iquadR] != -1 )
            {
              std::cout << "Trying to set orientation map twice! iqaudL = " << iquadL << ", iqaudR = " << iquadR << std::endl;
              SANS_ASSERT(false);
            }
            // set the map and exit
            orientMap.canonical[QuadratureRule::eGauss][orderidx][iquadR] = iquadL;
            break;
          }
        } // iquadR

        if (iquadR == quadrature.nQuadrature())
        {
          std::cout << "Failed to fiend orientation quadrature. iqaudL = " << iquadL << ", iqaudR = " << iquadR << std::endl;
          SANS_ASSERT(false);
        }
      }
    } // orderidx
  } // orientation
}

//----------------------------------------------------------------------------//

template<class Topology>
void
QuadratureCache<Topology>::
fillCellQuadrature(const QuadraturePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreCell)
{
  BasisFunctionCategory cat = pointStoreCell.category;
  int poly_order = pointStoreCell.poly_order;

  QuadratureArea<Topology> quadrature( QuadratureArea<Topology>::getOrderFromIndex(ref.orderidx) );

  std::vector<QuadratureBasisPointValues<D>>& points = pointStoreCell.eval[ref.rule][ref.orderidx];
  SANS_ASSERT(points.size() == 0);
  points.resize( quadrature.nQuadrature() );

  const BasisFunctionAreaBase<Topology>* basis =
      BasisFunctionAreaBase<Topology>::getBasisFunction(poly_order, cat);

  const int nBasis = basis->nBasis();

  Real s, t;
  for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
  {
    QuadratureBasisPointValues<D>& point = points[iquad];

    point.phi.resize(nBasis);
    point.dphi[0].resize(nBasis);
    point.dphi[1].resize(nBasis);
    point.d2phi[0].resize(nBasis);
    point.d2phi[1].resize(nBasis);
    point.d2phi[2].resize(nBasis);

    quadrature.coordinates(iquad, s, t);
    basis->evalBasis( s, t, sgn, point.phi.data(), point.phi.size() );

    Real *phis = point.dphi[0].data();
    Real *phit = point.dphi[1].data();
    basis->evalBasisDerivative( s, t, sgn, phis, phit, nBasis );

    Real *phiss = point.d2phi[0].data();
    Real *phist = point.d2phi[1].data();
    Real *phitt = point.d2phi[2].data();
    try
    {
      basis->evalBasisHessianDerivative( s, t, sgn, phiss, phist, phitt, nBasis );
    }
    catch (const DeveloperException& e)
    {
      // hessian not implemented, hopefully it's not needed...
      point.d2phi[0].clear(); point.d2phi[0].shrink_to_fit();
      point.d2phi[1].clear(); point.d2phi[1].shrink_to_fit();
      point.d2phi[2].clear(); point.d2phi[2].shrink_to_fit();
    }
  } // iquad
}

//----------------------------------------------------------------------------//

template<class Topology>
void
QuadratureCache<Topology>::
fillTraceQuadrature(const QuadratureCellTracePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreTrace)
{
  BasisFunctionCategory cat = pointStoreTrace.category;
  int poly_order = pointStoreTrace.poly_order;

  QuadratureLine quadrature( QuadratureLine::getOrderFromIndex(ref.orderidx) );

  std::vector<QuadratureBasisPointValues<D>>& points = pointStoreTrace.eval[QuadratureRule::eGauss][ref.orderidx];
  SANS_ASSERT(points.size() == 0);
  points.resize( quadrature.nQuadrature() );

  const BasisFunctionAreaBase<Topology>* basis =
      BasisFunctionAreaBase<Topology>::getBasisFunction(poly_order, cat);

  CanonicalTraceToCell canonicalTrace(ref.canonicalTrace.trace, 1);

  const int nBasis = basis->nBasis();

  Real sTrace;
  Real sCell, tCell;
  for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
  {
    QuadratureBasisPointValues<D>& point = points[iquad];

    point.phi.resize(nBasis);
    point.dphi[0].resize(nBasis);
    point.dphi[1].resize(nBasis);

    quadrature.coordinate(iquad, sTrace);

    TraceToCellRefCoord<Line, TopoD2, Topology>::eval( canonicalTrace, sTrace, sCell, tCell );

    basis->evalBasis( sCell, tCell, sgn, point.phi.data(), point.phi.size() );

    Real *phis = point.dphi[0].data();
    Real *phit = point.dphi[1].data();
    basis->evalBasisDerivative( sCell, tCell, sgn, phis, phit, nBasis );
  } //iquad
}

// instantiate the class and singleton
template<class Topology>
const QuadratureCache<Topology> QuadratureCache<Topology>::cache;

// The QuadratureCache singleton must be instantiated in this translation unit (cpp file) to guarantee
// that the basis function singletons are initialized before the QuadratureCache
template struct QuadratureCache<Triangle>;

}

// instantiate the standard library classes
template class std::vector<SANS::QuadratureBasisPointValues<2>>;
template class std::vector<std::vector<SANS::QuadratureBasisPointValues<2>>>;
template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointValues<2>>>>;

template class std::map<std::array<int,3>,SANS::QuadratureBasisPointStore<2>>;
template class std::vector<std::map<std::array<int,3>,SANS::QuadratureBasisPointStore<2>>>;
template class std::vector<std::map<std::array<int,3>,std::vector<SANS::QuadratureBasisPointStore<2>>>>;

template class std::vector<SANS::QuadratureBasisPointStore<2>>;
template class std::vector<std::vector<SANS::QuadratureBasisPointStore<2>>>;
template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointStore<2>>>>;

template class std::vector<SANS::TraceOrientMap>;
