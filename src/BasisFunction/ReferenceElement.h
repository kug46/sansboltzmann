// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef REFERENCEELEMENT_H
#define REFERENCEELEMENT_H

#include "Topology/ElementTopology.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

namespace SANS
{

/*
   Struct for querying information about reference elements
*/

template <class Topology>
struct ReferenceElement;

template <>
struct ReferenceElement<Node>
{
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType; //Can't have zero-length arrays
  static const RefCoordType centerRef;

  // Matrix used for a constrained optimization
  typedef DLA::MatrixSymS<TopoD1::D,Real> MatrixConst;
  typedef DLA::VectorS<   TopoD1::D,Real> VectorConst;

  static bool isInside(const RefCoordType& sRef);
  static bool getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol);
  static void constraintJacobian(const int itrace, MatrixConst& jac);
  static void constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res);
};

template <>
struct ReferenceElement<Line>
{
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordType;
  static const RefCoordType centerRef;
  static const Real lengthRef;

  // Matrix used for a constrained optimization
  typedef DLA::MatrixSymS<TopoD1::D+1,Real> MatrixConst;
  typedef DLA::VectorS<   TopoD1::D+1,Real> VectorConst;

  static bool isInside(const RefCoordType& sRef);
  static bool getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol);
  static void constraintJacobian(const int itrace, MatrixConst& jac);
  static void constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res);
};

template <>
struct ReferenceElement<Triangle>
{
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  static const RefCoordType centerRef;
  static const Real areaRef;

  // Matrix used for a constrained optimization
  typedef DLA::MatrixSymS<TopoD2::D+1,Real> MatrixConst;
  typedef DLA::VectorS<   TopoD2::D+1,Real> VectorConst;

  static bool isInside(const RefCoordType& sRef);
  static bool getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol);
  static void constraintJacobian(const int itrace, MatrixConst& jac);
  static void constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res);
};

template <>
struct ReferenceElement<Quad>
{
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordType;
  static const RefCoordType centerRef;
  static const Real areaRef;

  // Matrix used for a constrained optimization
  typedef DLA::MatrixSymS<TopoD2::D+1,Real> MatrixConst;
  typedef DLA::VectorS<   TopoD2::D+1,Real> VectorConst;

  static bool isInside(const RefCoordType& sRef);
  static bool getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol);
  static void constraintJacobian(const int itrace, MatrixConst& jac);
  static void constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res);
};

template <>
struct ReferenceElement<Tet>
{
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;
  static const RefCoordType centerRef;
  static const Real volumeRef;

  // Matrix used for a constrained optimization
  typedef DLA::MatrixSymS<TopoD3::D+1,Real> MatrixConst;
  typedef DLA::VectorS<   TopoD3::D+1,Real> VectorConst;

  static bool isInside(const RefCoordType& sRef);
  static bool getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol);
  static void constraintJacobian(const int itrace, MatrixConst& jac);
  static void constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res);
};

template <>
struct ReferenceElement<Hex>
{
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordType;
  static const RefCoordType centerRef;
  static const Real volumeRef;

  // Matrix used for a constrained optimization
  typedef DLA::MatrixSymS<TopoD3::D+1,Real> MatrixConst;
  typedef DLA::VectorS<   TopoD3::D+1,Real> VectorConst;

  static bool isInside(const RefCoordType& sRef);
  static bool getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol);
  static void constraintJacobian(const int itrace, MatrixConst& jac);
  static void constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res);
};

template <>
struct ReferenceElement<Pentatope>
{
  typedef DLA::VectorS<TopoD4::D,Real> RefCoordType;
  static const RefCoordType centerRef;
  static const Real volumeRef;

  // Matrix used for a constrained optimization
  typedef DLA::MatrixSymS<TopoD4::D+1,Real> MatrixConst;
  typedef DLA::VectorS<   TopoD4::D+1,Real> VectorConst;

  static bool isInside(const RefCoordType& sRef);
  static bool getInteriorSolution(const RefCoordType& sRef0, const RefCoordType& dsRef, RefCoordType& sRef_sol);
  static void constraintJacobian(const int itrace, MatrixConst& jac);
  static void constraintResidual(const int itrace, const RefCoordType& sRef, VectorConst& res);
};

}

#endif //REFERENCEELEMENT_H
