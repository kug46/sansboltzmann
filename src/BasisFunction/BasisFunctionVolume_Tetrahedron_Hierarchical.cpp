// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionVolume_Tetrahedron_Hierarchical.h"
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Hierarchical: linear

void
BasisFunctionVolume<Tet,Hierarchical,1>::evalBasis( const Real& s, const Real& t, const Real& u, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phi[0] = 1 - s - t - u; // 1 @ node 0 (s = 0, t = 0, u = 0)
  phi[1] =     s        ; // 1 @ node 1 (s = 1, t = 0, u = 0)
  phi[2] =         t    ; // 1 @ node 2 (s = 0, t = 1, u = 0)
  phi[3] =             u; // 1 @ node 3 (s = 0, t = 0, u = 1)
}


void
BasisFunctionVolume<Tet,Hierarchical,1>::evalBasisDerivative( const Real&, const Real&, const Real&, const Int4&,
                                                              Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  0;
  phis[3] =  0;

  phit[0] = -1;
  phit[1] =  0;
  phit[2] =  1;
  phit[3] =  0;

  phiu[0] = -1;
  phiu[1] =  0;
  phiu[2] =  0;
  phiu[3] =  1;
}

void
BasisFunctionVolume<Tet,Hierarchical,1>::evalBasisHessianDerivative( const Real&, const Real&, const Real&, const Int4&,
          Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 0;
  phiss[3] = 0;

  phist[0] = 0;
  phist[1] = 0;
  phist[2] = 0;
  phist[3] = 0;

  phitt[0] = 0;
  phitt[1] = 0;
  phitt[2] = 0;
  phitt[3] = 0;

  phisu[0] = 0;
  phisu[1] = 0;
  phisu[2] = 0;
  phisu[3] = 0;

  phitu[0] = 0;
  phitu[1] = 0;
  phitu[2] = 0;
  phitu[3] = 0;

  phiuu[0] = 0;
  phiuu[1] = 0;
  phiuu[2] = 0;
  phiuu[3] = 0;
}

//----------------------------------------------------------------------------//
// Hierarchical: quadratic

void
BasisFunctionVolume<Tet,Hierarchical,2>::evalBasis( const Real& s, const Real& t, const Real& u, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  Real v = 1 - s - t - u;

  phi[0] = v; // 1 @ node 0 (s = 0, t = 0, u = 0)
  phi[1] = s; // 1 @ node 1 (s = 1, t = 0, u = 0)
  phi[2] = t; // 1 @ node 2 (s = 0, t = 1, u = 0)
  phi[3] = u; // 1 @ node 3 (s = 0, t = 0, u = 1)

  phi[4] = 4*t*u; // 1 @ face 0, edge 0 (s = 0.0, t = 0.5, u = 0.5)
  phi[5] = 4*s*u; // 1 @ face 0, edge 1 (s = 0.5, t = 0.0, u = 0.5)
  phi[6] = 4*s*t; // 1 @ face 0, edge 2 (s = 0.5, t = 0.5, u = 0.0)

  phi[7] = 4*t*v; // 1 @ face 1, edge 1 (s = 0.0, t = 0.5, u = 0.0)
  phi[8] = 4*u*v; // 1 @ face 1, edge 2 (s = 0.0, t = 0.0, u = 0.5)

  phi[9] = 4*s*v; // 1 @ face 2, edge 2 (s = 0.5, t = 0.0, u = 0.0)

}


void
BasisFunctionVolume<Tet,Hierarchical,2>::evalBasisDerivative( const Real& s, const Real& t , const Real& u, const Int4&,
                                                              Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  Real v = 1 - s - t - u;

  phis[0] = -1;
  phis[1] =  1;
  phis[2] =  0;
  phis[3] =  0;
  phis[4] =  0;
  phis[5] =  4*u;
  phis[6] =  4*t;
  phis[7] = -4*t;
  phis[8] = -4*u;
  phis[9] =  4*(v-s);

  phit[0] = -1;
  phit[1] =  0;
  phit[2] =  1;
  phit[3] =  0;
  phit[4] =  4*u;
  phit[5] =  0;
  phit[6] =  4*s;
  phit[7] =  4*(v-t);
  phit[8] = -4*u;
  phit[9] = -4*s;

  phiu[0] = -1;
  phiu[1] =  0;
  phiu[2] =  0;
  phiu[3] =  1;
  phiu[4] =  4*t;
  phiu[5] =  4*s;
  phiu[6] =  0;
  phiu[7] = -4*t;
  phiu[8] =  4*(v-u);
  phiu[9] = -4*s;
}

void
BasisFunctionVolume<Tet,Hierarchical,2>::evalBasisHessianDerivative( const Real& s, const Real& t, const Real& u, const Int4&,
          Real phiss[],
          Real phist[], Real phitt[],
          Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  phiss[0] =  0;
  phiss[1] =  0;
  phiss[2] =  0;
  phiss[3] =  0;
  phiss[4] =  0;
  phiss[5] =  0;
  phiss[6] =  0;
  phiss[7] =  0;
  phiss[8] =  0;
  phiss[9] = -8;

  phist[0] =  0;
  phist[1] =  0;
  phist[2] =  0;
  phist[3] =  0;
  phist[4] =  0;
  phist[5] =  0;
  phist[6] =  4;
  phist[7] = -4;
  phist[8] =  0;
  phist[9] = -4;

  phitt[0] =  0;
  phitt[1] =  0;
  phitt[2] =  0;
  phitt[3] =  0;
  phitt[4] =  0;
  phitt[5] =  0;
  phitt[6] =  0;
  phitt[7] = -8;
  phitt[8] =  0;
  phitt[9] =  0;

  phisu[0] =  0;
  phisu[1] =  0;
  phisu[2] =  0;
  phisu[3] =  0;
  phisu[4] =  0;
  phisu[5] =  4;
  phisu[6] =  0;
  phisu[7] =  0;
  phisu[8] = -4;
  phisu[9] = -4;

  phitu[0] =  0;
  phitu[1] =  0;
  phitu[2] =  0;
  phitu[3] =  0;
  phitu[4] =  4;
  phitu[5] =  0;
  phitu[6] =  0;
  phitu[7] = -4;
  phitu[8] = -4;
  phitu[9] =  0;

  phiuu[0] =  0;
  phiuu[1] =  0;
  phiuu[2] =  0;
  phiuu[3] =  0;
  phiuu[4] =  0;
  phiuu[5] =  0;
  phiuu[6] =  0;
  phiuu[7] =  0;
  phiuu[8] = -8;
  phiuu[9] =  0;
}

}
