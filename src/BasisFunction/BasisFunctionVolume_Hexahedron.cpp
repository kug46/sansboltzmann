// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// volume basis functions hexahedron

#include <string>
#include <iostream>
#include <limits>

#include "tools/SANSException.h"
#include "BasisFunctionVolume.h"
#include "BasisFunctionVolume_Hexahedron.h"

#include "Quadrature/QuadratureVolume.h"
#include "Quadrature/QuadratureArea.h"
#include "Quadrature_Cache.h"

#include "TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//Instantiate the singletons
const BasisFunctionVolume<Hex, Hierarchical, 1>*
      BasisFunctionVolume<Hex, Hierarchical, 1>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Hierarchical, 2>*
//      BasisFunctionVolume<Hex, Hierarchical, 2>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Hierarchical, 3>*
//      BasisFunctionVolume<Hex, Hierarchical, 3>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Hierarchical, 4>*
//      BasisFunctionVolume<Hex, Hierarchical, 4>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Hierarchical, 5>*
//      BasisFunctionVolume<Hex, Hierarchical, 5>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Hierarchical, 6>*
//      BasisFunctionVolume<Hex, Hierarchical, 6>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Hierarchical, 7>*
//      BasisFunctionVolume<Hex, Hierarchical, 7>::self() { static const BasisFunctionVolume singleton; return &singleton; }

const BasisFunctionVolume<Hex, Legendre, 0>*
      BasisFunctionVolume<Hex, Legendre, 0>::self() { static const BasisFunctionVolume singleton; return &singleton; }
const BasisFunctionVolume<Hex, Legendre, 1>*
      BasisFunctionVolume<Hex, Legendre, 1>::self() { static const BasisFunctionVolume singleton; return &singleton; }
const BasisFunctionVolume<Hex, Legendre, 2>*
      BasisFunctionVolume<Hex, Legendre, 2>::self() { static const BasisFunctionVolume singleton; return &singleton; }
const BasisFunctionVolume<Hex, Legendre, 3>*
      BasisFunctionVolume<Hex, Legendre, 3>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Legendre, 4>*
//      BasisFunctionVolume<Hex, Legendre, 4>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Legendre, 5>*
//      BasisFunctionVolume<Hex, Legendre, 5>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Legendre, 6>*
//      BasisFunctionVolume<Hex, Legendre, 6>::self() { static const BasisFunctionVolume singleton; return &singleton; }
//const BasisFunctionVolume<Hex, Legendre, 7>*
//      BasisFunctionVolume<Hex, Legendre, 7>::self() { static const BasisFunctionVolume singleton; return &singleton; }

const BasisFunctionVolume<Hex, Lagrange, 1>*
      BasisFunctionVolume<Hex, Lagrange, 1>::self() { static const BasisFunctionVolume singleton; return &singleton; }
const BasisFunctionVolume<Hex, Lagrange, 2>*
      BasisFunctionVolume<Hex, Lagrange, 2>::self() { static const BasisFunctionVolume singleton; return &singleton; }
const BasisFunctionVolume<Hex, Lagrange, 3>*
      BasisFunctionVolume<Hex, Lagrange, 3>::self() { static const BasisFunctionVolume singleton; return &singleton; }
const BasisFunctionVolume<Hex, Lagrange, 4>*
      BasisFunctionVolume<Hex, Lagrange, 4>::self() { static const BasisFunctionVolume singleton; return &singleton; }

const BasisFunctionVolume<Hex, Bernstein, 1>*
      BasisFunctionVolume<Hex, Bernstein, 1>::self() { static const BasisFunctionVolume singleton; return &singleton; }
const BasisFunctionVolume<Hex, Bernstein, 2>*
      BasisFunctionVolume<Hex, Bernstein, 2>::self() { static const BasisFunctionVolume singleton; return &singleton; }
const BasisFunctionVolume<Hex, Bernstein, 3>*
      BasisFunctionVolume<Hex, Bernstein, 3>::self() { static const BasisFunctionVolume singleton; return &singleton; }
const BasisFunctionVolume<Hex, Bernstein, 4>*
      BasisFunctionVolume<Hex, Bernstein, 4>::self() { static const BasisFunctionVolume singleton; return &singleton; }

const BasisFunctionVolume<Hex, RBS, 0>*
      BasisFunctionVolume<Hex, RBS, 0>::self() { static const BasisFunctionVolume singleton; return &singleton; }


//----------------------------------------------------------------------------//
//Initialize the base class pointers
template<>
const BasisFunctionVolume<Hex, Hierarchical, 1>*
BasisFunctionVolumeBase<Hex>::HierarchicalP1 = BasisFunctionVolume<Hex, Hierarchical, 1>::self();

#if 0
template<>
const BasisFunctionVolume<Hex, Hierarchical, 2>*
BasisFunctionVolumeBase<Hex>::HierarchicalP2 = BasisFunctionVolume<Hex, Hierarchical, 2>::self();

template<>
const BasisFunctionVolume<Hex, Hierarchical, 3>*
BasisFunctionVolumeBase<Hex>::HierarchicalP3 = BasisFunctionVolume<Hex, Hierarchical, 3>::self();

template<>
const BasisFunctionVolume<Hex, Hierarchical, 4>*
BasisFunctionVolumeBase<Hex>::HierarchicalP4 = BasisFunctionVolume<Hex, Hierarchical, 4>::self();

template<>
const BasisFunctionVolume<Hex, Hierarchical, 5>*
BasisFunctionVolumeBase<Hex>::HierarchicalP5 = BasisFunctionVolume<Hex, Hierarchical, 5>::self();

template<>
const BasisFunctionVolume<Hex, Hierarchical, 6>*
BasisFunctionVolumeBase<Hex>::HierarchicalP6 = BasisFunctionVolume<Hex, Hierarchical, 6>::self();

template<>
const BasisFunctionVolume<Hex, Hierarchical, 7>*
BasisFunctionVolumeBase<Hex>::HierarchicalP7 = BasisFunctionVolume<Hex, Hierarchical, 7>::self();
#endif

template<>
const BasisFunctionVolume<Hex, Legendre, 0>*
BasisFunctionVolumeBase<Hex>::LegendreP0 = BasisFunctionVolume<Hex, Legendre, 0>::self();

template<>
const BasisFunctionVolume<Hex, Legendre, 1>*
BasisFunctionVolumeBase<Hex>::LegendreP1 = BasisFunctionVolume<Hex, Legendre, 1>::self();

template<>
const BasisFunctionVolume<Hex, Legendre, 2>*
BasisFunctionVolumeBase<Hex>::LegendreP2 = BasisFunctionVolume<Hex, Legendre, 2>::self();

template<>
const BasisFunctionVolume<Hex, Legendre, 3>*
BasisFunctionVolumeBase<Hex>::LegendreP3 = BasisFunctionVolume<Hex, Legendre, 3>::self();
#if 0
template<>
const BasisFunctionVolume<Hex, Legendre, 4>*
BasisFunctionVolumeBase<Hex>::LegendreP4 = BasisFunctionVolume<Hex, Legendre, 4>::self();

template<>
const BasisFunctionVolume<Hex, Legendre, 5>*
BasisFunctionVolumeBase<Hex>::LegendreP5 = BasisFunctionVolume<Hex, Legendre, 5>::self();

template<>
const BasisFunctionVolume<Hex, Legendre, 6>*
BasisFunctionVolumeBase<Hex>::LegendreP6 = BasisFunctionVolume<Hex, Legendre, 6>::self();

template<>
const BasisFunctionVolume<Hex, Legendre, 7>*
BasisFunctionVolumeBase<Hex>::LegendreP7 = BasisFunctionVolume<Hex, Legendre, 7>::self();
#endif

template<>
const BasisFunctionVolume<Hex, Lagrange, 1>*
BasisFunctionVolumeBase<Hex>::LagrangeP1 = BasisFunctionVolume<Hex, Lagrange, 1>::self();

template<>
const BasisFunctionVolume<Hex, Lagrange, 2>*
BasisFunctionVolumeBase<Hex>::LagrangeP2 = BasisFunctionVolume<Hex, Lagrange, 2>::self();

template<>
const BasisFunctionVolume<Hex, Lagrange, 3>*
BasisFunctionVolumeBase<Hex>::LagrangeP3 = BasisFunctionVolume<Hex, Lagrange, 3>::self();

template<>
const BasisFunctionVolume<Hex, Lagrange, 4>*
BasisFunctionVolumeBase<Hex>::LagrangeP4 = BasisFunctionVolume<Hex, Lagrange, 4>::self();

template<>
const BasisFunctionVolume<Hex, Bernstein, 1>*
BasisFunctionVolumeBase<Hex>::BernsteinP1 = BasisFunctionVolume<Hex, Bernstein, 1>::self();
template<>
const BasisFunctionVolume<Hex, Bernstein, 2>*
BasisFunctionVolumeBase<Hex>::BernsteinP2 = BasisFunctionVolume<Hex, Bernstein, 2>::self();
template<>
const BasisFunctionVolume<Hex, Bernstein, 3>*
BasisFunctionVolumeBase<Hex>::BernsteinP3 = BasisFunctionVolume<Hex, Bernstein, 3>::self();
template<>
const BasisFunctionVolume<Hex, Bernstein, 4>*
BasisFunctionVolumeBase<Hex>::BernsteinP4 = BasisFunctionVolume<Hex, Bernstein, 4>::self();

template<>
const BasisFunctionVolume<Hex, RBS, 0>*
BasisFunctionVolumeBase<Hex>::RBSP0 = BasisFunctionVolume<Hex, RBS, 0>::self();

//----------------------------------------------------------------------------//
template<>
const BasisFunctionVolumeBase<Hex>*
BasisFunctionVolumeBase<Hex>::getBasisFunction( const int order, const BasisFunctionCategory& category )
{
  if (category == BasisFunctionCategory_Hierarchical)
  {
    switch (order)
    {
    case 1:
      return BasisFunctionVolume<Hex, Hierarchical, 1>::self();
#if 0
    case 2:
      return BasisFunctionVolume<Hex, Hierarchical, 2>::self();
    case 3:
      return BasisFunctionVolume<Hex, Hierarchical, 3>::self();
    case 4:
      return BasisFunctionVolume<Hex, Hierarchical, 4>::self();
    case 5:
      return BasisFunctionVolume<Hex, Hierarchical, 5>::self();
    case 6:
      return BasisFunctionVolume<Hex, Hierarchical, 6>::self();
    case 7:
      return BasisFunctionVolume<Hex, Hierarchical, 7>::self();
#endif
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionVolumeBase<Hex>::getBasisFunction: unexpected hierarchical order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Legendre)
  {
    switch (order)
    {
    case 0:
      return BasisFunctionVolume<Hex, Legendre, 0>::self();
    case 1:
      return BasisFunctionVolume<Hex, Legendre, 1>::self();
    case 2:
      return BasisFunctionVolume<Hex, Legendre, 2>::self();
    case 3:
      return BasisFunctionVolume<Hex, Legendre, 3>::self();
#if 0
    case 4:
      return BasisFunctionVolume<Hex, Legendre, 4>::self();
    case 5:
      return BasisFunctionVolume<Hex, Legendre, 5>::self();
    case 6:
      return BasisFunctionVolume<Hex, Legendre, 6>::self();
    case 7:
      return BasisFunctionVolume<Hex, Legendre, 7>::self();
#endif
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionVolumeBase<Hex>::getBasisFunction: unexpected Legendre order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Lagrange)
  {
    switch (order)
    {
    case 1:
      return BasisFunctionVolume<Hex, Lagrange, 1>::self();
    case 2:
      return BasisFunctionVolume<Hex, Lagrange, 2>::self();
    case 3:
      return BasisFunctionVolume<Hex, Lagrange, 3>::self();
    case 4:
      return BasisFunctionVolume<Hex, Lagrange, 4>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionVolumeBase<Hex>::getBasisFunction: unexpected Lagrange order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_Bernstein)
  {
    switch (order)
    {
    case 1:
      return BasisFunctionVolume<Hex, Bernstein, 1>::self();
    case 2:
      return BasisFunctionVolume<Hex, Bernstein, 2>::self();
    case 3:
      return BasisFunctionVolume<Hex, Bernstein, 3>::self();
    case 4:
      return BasisFunctionVolume<Hex, Bernstein, 4>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionVolumeBase<Hex>::getBasisFunction: unexpected Bernstein order = %d\n", order );
      break;
    }
  }
  else if (category == BasisFunctionCategory_RBS)
  {
    switch (order)
    {
    case 0:
      return BasisFunctionVolume<Hex, RBS, 0>::self();
    default:
      SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionVolumeBase<Hex>::getBasisFunction: unexpected RBS order = %d\n", order );
      break;
    }
  }

  SANS_DEVELOPER_EXCEPTION( "Error in BasisFunctionVolumeBase<Hex>::getBasisFunction: unexpected basis function category = %d\n",
                            static_cast<int>(category) );

  //Just so the compiler will not complain
  return NULL;
}

//===========================================================================//

template<class Topology>
QuadratureCache<Topology>::QuadratureCache()
{
  if ( BasisFunctionCategory_Hierarchical != 4 )
  {
    // Exceptions don't get printed during static initialization, probably a boost test thing
    std::cout << "You need to add the maximum order for the new basis" << std::endl;
    SANS_ASSERT(false);
  }

  BasisFunctionCategory categores[BasisFunctionCategory_Hierarchical];
  int Pmin[BasisFunctionCategory_Hierarchical];
  int Pmax[BasisFunctionCategory_Hierarchical];

  categores[BasisFunctionCategory_Legendre] = BasisFunctionCategory_Legendre;
  categores[BasisFunctionCategory_Lagrange] = BasisFunctionCategory_Lagrange;
  categores[BasisFunctionCategory_Bernstein] = BasisFunctionCategory_Bernstein;
  categores[BasisFunctionCategory_RBS] = BasisFunctionCategory_RBS;

  Pmin[BasisFunctionCategory_Legendre] = 0;
  Pmin[BasisFunctionCategory_Lagrange] = 1;
  Pmin[BasisFunctionCategory_Bernstein] = 1;
  Pmin[BasisFunctionCategory_RBS] = 0;

  Pmax[BasisFunctionCategory_Legendre] = BasisFunctionVolume_Hex_LegendrePMax+1;
  Pmax[BasisFunctionCategory_Lagrange] = BasisFunctionVolume_Hex_LagrangePMax+1;
  Pmax[BasisFunctionCategory_Bernstein] = BasisFunctionVolume_Hex_BernsteinPMax+1;
  Pmax[BasisFunctionCategory_RBS] = BasisFunctionVolume_Hex_RBSPMax+1;


  cell.resize(BasisFunctionCategory_Hierarchical);
  trace.resize(BasisFunctionCategory_Hierarchical);
  for (int icat = 0; icat < BasisFunctionCategory_Hierarchical; icat++)
  {
//    IntTrace sgn; // Should not be used by these basis functions

    BasisFunctionCategory cat = categores[icat];

    cell[cat].resize(Pmax[cat]);
    trace[cat].resize(Pmax[cat]);
    for (int poly_order = Pmin[cat]; poly_order < Pmax[cat]; poly_order++)
    {
//      const BasisFunctionVolumeBase<Hex>* basis =
//          BasisFunctionVolumeBase<Hex>::getBasisFunction(poly_order, cat);

      QuadratureBasisPointStore<D>& pointStore = cell[cat][poly_order];
      pointStore.category = cat;
      pointStore.poly_order = poly_order;
      pointStore.eval.resize(QuadratureRule::eNone);

      pointStore.eval[QuadratureRule::eGauss].resize(QuadratureVolume<Hex>::nOrderIdx);
#if 0
      for (int orderidx = 0; orderidx < QuadratureVolume<Hex>::nOrderIdx; orderidx++)
      {
        QuadratureVolume<Hex> quadrature( QuadratureVolume<Hex>::getOrderFromIndex(orderidx) );
        pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

        Real s, t, u;
        for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
        {
          QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

          point.phi.resize(basis->nBasis());
          point.dphi[0].resize(basis->nBasis());
          point.dphi[1].resize(basis->nBasis());
          point.dphi[2].resize(basis->nBasis());

          quadrature.coordinates(iquad, s, t, u);
          basis->evalBasis( s, t, u, sgn, point.phi.data(), point.phi.size() );

          basis->evalBasisDerivative( s, t, u, sgn, point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(),
                                                    point.dphi[0].size() );
        } // iquad
      } // orderidx
#endif

      trace[cat][poly_order].resize(Hex::NTrace);
      for (int itrace = 0; itrace < Hex::NTrace; itrace++)
      {
        QuadratureBasisPointStore<D>& pointStore = trace[cat][poly_order][itrace];
        pointStore.category = cat;
        pointStore.poly_order = poly_order;
        pointStore.eval.resize(QuadratureRule::eNone);

        CanonicalTraceToCell canonicalTrace(itrace, 1);

        pointStore.eval[QuadratureRule::eGauss].resize(QuadratureArea<Quad>::nOrderIdx);
#if 0
        for (int orderidx = 0; orderidx < QuadratureArea<Quad>::nOrderIdx; orderidx++)
        {
          QuadratureArea<Quad> quadrature( QuadratureArea<Quad>::getOrderFromIndex(orderidx) );
          pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

          Real sTrace, tTrace;
          Real sCell, tCell, uCell;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

            point.phi.resize(basis->nBasis());
            point.dphi[0].resize(basis->nBasis());
            point.dphi[1].resize(basis->nBasis());
            point.dphi[2].resize(basis->nBasis());

            quadrature.coordinates(iquad, sTrace, tTrace);

            TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTrace, sTrace, tTrace, sCell, tCell, uCell );

            basis->evalBasis( sCell, tCell, uCell, sgn, point.phi.data(), point.phi.size() );

            basis->evalBasisDerivative( sCell, tCell, uCell, sgn, point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(),
                                                                  point.dphi[0].size() );
          } //iquad
        } // orderidx
#endif
      } // itrace

    } // poly_order
  } // icat


  //-------------------------------------------------------------------------//
  // Hierarchical basis functions are special with the edge sign
  //-------------------------------------------------------------------------//

  const int nFaceSign = 1;

  // TODO: Not sure how the face sign work out in 3D, so this is just dummy now.
  IntTrace faceSigns[nFaceSign] = { {{ 1, 1, 1, 1, 1, 1}} };

  // TODO: This assertion is here until the face sign is sorted out. Then it should be removed
  SANS_ASSERT( BasisFunctionVolume_Hex_HierarchicalPMax == 1 );

  hierarchicalCell.resize(BasisFunctionVolume_Hex_HierarchicalPMax+1);
  hierarchicalTrace.resize(BasisFunctionVolume_Hex_HierarchicalPMax+1);
  for (int poly_order = 1; poly_order < BasisFunctionVolume_Hex_HierarchicalPMax+1; poly_order++)
  {
//    const BasisFunctionVolumeBase<Hex>* basis =
//        BasisFunctionVolumeBase<Hex>::getBasisFunction(poly_order, BasisFunctionCategory_Hierarchical);

    for (int isgn = 0; isgn < nFaceSign; isgn++)
    {
      IntTrace sgn = faceSigns[isgn];
      QuadratureBasisPointStore<D>& pointStore = hierarchicalCell[poly_order][sgn];
      pointStore.category = BasisFunctionCategory_Hierarchical;
      pointStore.poly_order = poly_order;
      pointStore.eval.resize(QuadratureRule::eNone);

      pointStore.eval[QuadratureRule::eGauss].resize(QuadratureVolume<Hex>::nOrderIdx);
#if 0
      for (int orderidx = 0; orderidx < QuadratureVolume<Hex>::nOrderIdx; orderidx++)
      {
        QuadratureVolume<Hex> quadrature( QuadratureVolume<Hex>::getOrderFromIndex(orderidx) );
        pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

        Real s, t, u;
        for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
        {
          QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

          point.phi.resize(basis->nBasis());
          point.dphi[0].resize(basis->nBasis());
          point.dphi[1].resize(basis->nBasis());
          point.dphi[2].resize(basis->nBasis());

          quadrature.coordinates(iquad, s, t, u);
          basis->evalBasis( s, t, u, sgn, point.phi.data(), point.phi.size() );

          basis->evalBasisDerivative( s, t, u, sgn, point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(),
                                                    point.dphi[0].size() );
        } //iqaud
      } // orderidx
#endif

      hierarchicalTrace[poly_order][sgn].resize(Hex::NTrace);
      for (int itrace = 0; itrace < Hex::NTrace; itrace++)
      {
        QuadratureBasisPointStore<D>& pointStore = hierarchicalTrace[poly_order][sgn][itrace];
        pointStore.category = BasisFunctionCategory_Hierarchical;
        pointStore.poly_order = poly_order;
        pointStore.eval.resize(QuadratureRule::eNone);

        CanonicalTraceToCell canonicalTrace(itrace, 1);

        pointStore.eval[QuadratureRule::eGauss].resize(QuadratureArea<Quad>::nOrderIdx);
#if 0
        for (int orderidx = 0; orderidx < QuadratureArea<Quad>::nOrderIdx; orderidx++)
        {
          QuadratureArea<Quad> quadrature( QuadratureArea<Quad>::getOrderFromIndex(orderidx) );
          pointStore.eval[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature() );

          Real sTrace, tTrace;
          Real sCell, tCell, uCell;
          for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
          {
            QuadratureBasisPointValues<D>& point = pointStore.eval[QuadratureRule::eGauss][orderidx][iquad];

            point.phi.resize(basis->nBasis());
            point.dphi[0].resize(basis->nBasis());
            point.dphi[1].resize(basis->nBasis());
            point.dphi[2].resize(basis->nBasis());

            quadrature.coordinates(iquad, sTrace, tTrace);

            TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTrace, sTrace, tTrace, sCell, tCell, uCell );

            basis->evalBasis( sCell, tCell, uCell, sgn, point.phi.data(), point.phi.size() );

            basis->evalBasisDerivative( sCell, tCell, uCell, sgn, point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(),
                                                                  point.dphi[0].size() );
          } //iquad
        } // orderidx
#endif
      } // itrace

    } // isgn
  } // poly_order


  //-------------------------------------------------------------------------//
  // Trace orientation map that maps any orientation back to the canonical
  //-------------------------------------------------------------------------//

  Real reftol = std::numeric_limits<Real>::max();

  {
    // find the smallest distance between any two quadrature points
    // this will be the tolerance for the equality check between cell quadrature points
    QuadratureArea<Quad> quadrature( QuadratureArea<Quad>::getOrderFromIndex(QuadratureArea<Quad>::nOrderIdx-1) );
    Real s0, t0, s1, t1;
    for (int iquad0 = 0; iquad0 < quadrature.nQuadrature(); iquad0++)
    {
      quadrature.coordinates(iquad0, s0, t0);

      for (int iquad1 = 0; iquad1 < quadrature.nQuadrature(); iquad1++)
      {
        if (iquad0 == iquad1) continue; // the same point will have zero difference...

        quadrature.coordinates(iquad1, s1, t1);

        reftol = std::min( reftol, sqrt( (s1 - s0)*(s1 - s0) + (t1 - t0)*(t1 - t0) ) );
      }
    }

    // give a nice margin
    reftol /= 10;
  }

  traceOrientMap.resize(4); // 4 non-canonical orientations

  // By design, any mesh can only have orientation 1, and negative orientations
  for (int orientation : {-4, -3, -2, -1} )
  {
    TraceOrientMap& orientMap = traceOrientMap[ QuadCacheVolumeTraceOrientIdx(orientation) ];
    orientMap.canonical.resize(QuadratureRule::eNone);

    orientMap.canonical[QuadratureRule::eGauss].resize(QuadratureArea<Quad>::nOrderIdx);
    for (int orderidx = 0; orderidx < QuadratureArea<Quad>::nOrderIdx; orderidx++)
    {
      QuadratureArea<Quad> quadrature( QuadratureArea<Quad>::getOrderFromIndex(orderidx) );
      orientMap.canonical[QuadratureRule::eGauss][orderidx].resize( quadrature.nQuadrature(), -1 );

      // Left canonical trace with canonical orientation of 1 (trace does not matter)
      CanonicalTraceToCell canonicalTraceL(0, 1);
      CanonicalTraceToCell canonicalTraceR(0, orientation); // other orientation

      Real sTrace, tTrace;
      Real sCellL, tCellL, uCellL;
      for (int iquadL = 0; iquadL < quadrature.nQuadrature(); iquadL++)
      {
        // Get the quadrature point on the trace
        quadrature.coordinates(iquadL, sTrace, tTrace);

        // get the cell reference coordiantes
        TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTraceL, sTrace, tTrace, sCellL, tCellL, uCellL );

        // loop over all quadrature points again, and find the same cell point with the right orientation
        int iquadR;
        for (iquadR = 0; iquadR < quadrature.nQuadrature(); iquadR++)
        {
          quadrature.coordinates(iquadR, sTrace, tTrace);

          Real sCellR, tCellR, uCellR;
          TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTraceR, sTrace, tTrace, sCellR, tCellR, uCellR );
          if ( fabs(sCellR - sCellL) < reftol && fabs(tCellR - tCellL) < reftol && fabs(uCellR - uCellL) < reftol )
          {
            if ( orientMap.canonical[QuadratureRule::eGauss][orderidx][iquadR] != -1 )
            {
              std::cout << "Trying to set orientation map twice! iqaudL = " << iquadL << ", iqaudR = " << iquadR << std::endl;
              SANS_ASSERT(false);
            }
            // set the map and exit
            orientMap.canonical[QuadratureRule::eGauss][orderidx][iquadR] = iquadL;
            break;
          }
        } // iquadR

        if (iquadR == quadrature.nQuadrature())
        {
          std::cout << "Failed to fiend orientation quadrature. iqaudL = " << iquadL << ", iqaudR = " << iquadR << std::endl;
          SANS_ASSERT(false);
        }
      }
    } // orderidx
  } // orientation
}

//----------------------------------------------------------------------------//

template<class Topology>
void
QuadratureCache<Topology>::
fillCellQuadrature(const QuadraturePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreCell)
{
  BasisFunctionCategory cat = pointStoreCell.category;
  int poly_order = pointStoreCell.poly_order;

  QuadratureVolume<Topology> quadrature( QuadratureVolume<Topology>::getOrderFromIndex(ref.orderidx) );

  std::vector<QuadratureBasisPointValues<D>>& points = pointStoreCell.eval[ref.rule][ref.orderidx];
  SANS_ASSERT(points.size() == 0);
  points.resize( quadrature.nQuadrature() );

  const BasisFunctionVolumeBase<Topology>* basis =
      BasisFunctionVolumeBase<Topology>::getBasisFunction(poly_order, cat);

  const int nBasis = basis->nBasis();

  Real s, t, u;
  for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
  {
    QuadratureBasisPointValues<D>& point = points[iquad];

    point.phi.resize(nBasis);
    point.dphi[0].resize(nBasis);
    point.dphi[1].resize(nBasis);
    point.dphi[2].resize(nBasis);
    for (int i = 0; i < 6; i++)
      point.d2phi[i].resize(nBasis);

    quadrature.coordinates(iquad, s, t, u);
    basis->evalBasis( s, t, u, sgn, point.phi.data(), point.phi.size() );

    Real *phis = point.dphi[0].data();
    Real *phit = point.dphi[1].data();
    Real *phiu = point.dphi[2].data();
    basis->evalBasisDerivative( s, t, u, sgn, phis, phit, phiu, nBasis );

    Real *phiss = point.d2phi[0].data();
    Real *phist = point.d2phi[1].data();
    Real *phitt = point.d2phi[2].data();
    Real *phisu = point.d2phi[3].data();
    Real *phitu = point.d2phi[4].data();
    Real *phiuu = point.d2phi[5].data();
    try
    {
      basis->evalBasisHessianDerivative( s, t, u, sgn,
                                         phiss,
                                         phist, phitt,
                                         phisu, phitu, phiuu, nBasis );
    }
    catch (const DeveloperException& e)
    {
      // hessian not implemented, hopefully it's not needed...
      for (int i = 0; i < 6; i++)
      {
        point.d2phi[i].clear(); point.d2phi[i].shrink_to_fit();
      }
    }
  } // iquad
}

//----------------------------------------------------------------------------//

template<class Topology>
void
QuadratureCache<Topology>::
fillTraceQuadrature(const QuadratureCellTracePoint<TopoDim>& ref, const IntTrace& sgn, QuadratureBasisPointStore<D>& pointStoreTrace)
{
  BasisFunctionCategory cat = pointStoreTrace.category;
  int poly_order = pointStoreTrace.poly_order;

  QuadratureArea<Quad> quadrature( QuadratureArea<Quad>::getOrderFromIndex(ref.orderidx) );

  std::vector<QuadratureBasisPointValues<D>>& points = pointStoreTrace.eval[QuadratureRule::eGauss][ref.orderidx];
  SANS_ASSERT(points.size() == 0);
  points.resize( quadrature.nQuadrature() );

  const BasisFunctionVolumeBase<Hex>* basis =
      BasisFunctionVolumeBase<Hex>::getBasisFunction(poly_order, cat);

  CanonicalTraceToCell canonicalTrace(ref.canonicalTrace.trace, 1);

  Real sTrace, tTrace;
  Real sCell, tCell, uCell;
  for (int iquad = 0; iquad < quadrature.nQuadrature(); iquad++)
  {
    QuadratureBasisPointValues<D>& point = points[iquad];

    point.phi.resize(basis->nBasis());
    point.dphi[0].resize(basis->nBasis());
    point.dphi[1].resize(basis->nBasis());
    point.dphi[2].resize(basis->nBasis());

    quadrature.coordinates(iquad, sTrace, tTrace);

    TraceToCellRefCoord<Quad, TopoD3, Hex>::eval( canonicalTrace, sTrace, tTrace, sCell, tCell, uCell );

    basis->evalBasis( sCell, tCell, uCell, sgn, point.phi.data(), point.phi.size() );

    basis->evalBasisDerivative( sCell, tCell, uCell, sgn, point.dphi[0].data(), point.dphi[1].data(), point.dphi[2].data(),
                                                          point.dphi[0].size() );
  } //iquad
}

// instantiate the class and singleton
template<class Topology>
const QuadratureCache<Topology> QuadratureCache<Topology>::cache;

// The QuadratureCache singleton must be instantiated in this translation unit (cpp file) to guarantee
// that the basis function singletons are initialized before the QuadratureCache
template struct QuadratureCache<Hex>;

}

// instantiate the standard library classes
template class std::vector<SANS::QuadratureBasisPointValues<3>>;
template class std::vector<std::vector<SANS::QuadratureBasisPointValues<3>>>;
template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointValues<3>>>>;

template class std::map<std::array<int,6>,SANS::QuadratureBasisPointStore<3>>;
template class std::vector<std::map<std::array<int,6>,SANS::QuadratureBasisPointStore<3>>>;
template class std::vector<std::map<std::array<int,6>,std::vector<SANS::QuadratureBasisPointStore<3>>>>;

template class std::vector<SANS::QuadratureBasisPointStore<3>>;
template class std::vector<std::vector<SANS::QuadratureBasisPointStore<3>>>;
template class std::vector<std::vector<std::vector<SANS::QuadratureBasisPointStore<3>>>>;

template class std::vector<SANS::TraceOrientMap>;
