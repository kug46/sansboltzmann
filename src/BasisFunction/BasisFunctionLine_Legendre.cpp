// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <cmath> // sqrt

#include "BasisFunctionLine_Legendre.h"
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Legendre: constant

void
BasisFunctionLine<Legendre,0>::evalBasis( const Real, Real phi[], int nphi ) const
{
  SANS_ASSERT_MSG(nphi == 1, "nphi = %d", nphi);

  phi[0] = 1;
}


void
BasisFunctionLine<Legendre,0>::evalBasisDerivative( const Real, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phis[0] = 0;
}

void
BasisFunctionLine<Legendre,0>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phiss[0] = 0;
}


//----------------------------------------------------------------------------//
// Legendre: linear

void
BasisFunctionLine<Legendre,1>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  Real t = 1 - s;

  phi[0] = 1;
  phi[1] = sqrt(3)*(s - t);
}

void
BasisFunctionLine<Legendre,1>::evalBasisDerivative( const Real, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phis[0] =  0;
  phis[1] =  2*sqrt(3);
}

void
BasisFunctionLine<Legendre,1>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phiss[0] = 0;
  phiss[1] = 0;
}


//----------------------------------------------------------------------------//
// Legendre: quadratic

void
BasisFunctionLine<Legendre,2>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  Real t = 1 - s;

  phi[0] = 1;
  phi[1] = sqrt(3)*(s - t);
  phi[2] = sqrt(5)*(1 - 6*s*t);
}

void
BasisFunctionLine<Legendre,2>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  Real t = 1 - s;

  phis[0] =  0;
  phis[1] =  2*sqrt(3);
  phis[2] =  6*sqrt(5)*(s - t);
}

void
BasisFunctionLine<Legendre,2>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 12*sqrt(5);
}


//----------------------------------------------------------------------------//
// Legendre: cubic

void
BasisFunctionLine<Legendre,3>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real t = 1 - s;

  phi[0] = 1;
  phi[1] = sqrt(3)*(s - t);
  phi[2] = sqrt(5)*(1 - 6*s*t);
  phi[3] = sqrt(7)*(1 - 10*s*t)*(s - t);
}

void
BasisFunctionLine<Legendre,3>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real t = 1 - s;

  phis[0] =  0;
  phis[1] =  2*sqrt(3);
  phis[2] =  6*sqrt(5)*(s - t);
  phis[3] = 12*sqrt(7)*(1 - 5*s*t);
}

void
BasisFunctionLine<Legendre,3>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real t = 1 - s;

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 12*sqrt(5);
  phiss[3] = 60*sqrt(7)*(s - t);
}


//----------------------------------------------------------------------------//
// Legendre: quartic

void
BasisFunctionLine<Legendre,4>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 5);

  Real t = 1 - s;

  phi[0] = 1;
  phi[1] = sqrt(3)*(s - t);
  phi[2] = sqrt(5)*(1 - 6*s*t);
  phi[3] = sqrt(7)*(1 - 10*s*t)*(s - t);
  phi[4] = 3*(1 - 10*s*t*(2 - 7*s*t));
}

void
BasisFunctionLine<Legendre,4>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 5);

  Real t = 1 - s;

  phis[0] =  0;
  phis[1] =  2*sqrt(3);
  phis[2] =  6*sqrt(5)*(s - t);
  phis[3] = 12*sqrt(7)*(1 - 5*s*t);
  phis[4] = 60*(1 - 7*s*t)*(s - t);
}

void
BasisFunctionLine<Legendre,4>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 5);

  Real t = 1 - s;

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 12*sqrt(5);
  phiss[3] = 60*sqrt(7)*(s - t);
  phiss[4] = 180*(3 - 14*s*t);
}


//----------------------------------------------------------------------------//
// Legendre: quintic

void
BasisFunctionLine<Legendre,5>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  Real t = 1 - s;

  phi[0] = 1;
  phi[1] = sqrt(3)*(s - t);
  phi[2] = sqrt(5)*(1 - 6*s*t);
  phi[3] = sqrt(7)*(1 - 10*s*t)*(s - t);
  phi[4] = 3*(1 - 10*s*t*(2 - 7*s*t));
  phi[5] = sqrt(11)*(1 - 14*s*t*(s - 2*t)*(2*s - t))*(s - t);
}

void
BasisFunctionLine<Legendre,5>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  Real t = 1 - s;

  phis[0] =  0;
  phis[1] =  2*sqrt(3);
  phis[2] =  6*sqrt(5)*(s - t);
  phis[3] = 12*sqrt(7)*(1 - 5*s*t);
  phis[4] = 60*(1 - 7*s*t)*(s - t);
  phis[5] = 30*sqrt(11)*(1 - 14*s*t*(1 - 3*s*t));
}

void
BasisFunctionLine<Legendre,5>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  Real t = 1 - s;

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 12*sqrt(5);
  phiss[3] = 60*sqrt(7)*(s - t);
  phiss[4] = 180*(3 - 14*s*t);
  phiss[5] = 420*sqrt(11)*(1 - 6*s*t)*(s - t);
}


//----------------------------------------------------------------------------//
// Legendre: P6

void
BasisFunctionLine<Legendre,6>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 7);

  Real t = 1 - s;

  phi[0] = 1;
  phi[1] = sqrt(3)*(s - t);
  phi[2] = sqrt(5)*(1 - 6*s*t);
  phi[3] = sqrt(7)*(1 - 10*s*t)*(s - t);
  phi[4] = 3*(1 - 10*s*t*(2 - 7*s*t));
  phi[5] = sqrt(11)*(1 - 14*s*t*(s - 2*t)*(2*s - t))*(s - t);
  phi[6] = sqrt(13)*(1 - 42*s*t*(1 - s*t*(9 - 22*s*t)));
}

void
BasisFunctionLine<Legendre,6>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 7);

  Real t = 1 - s;

  phis[0] =  0;
  phis[1] =  2*sqrt(3);
  phis[2] =  6*sqrt(5)*(s - t);
  phis[3] = 12*sqrt(7)*(1 - 5*s*t);
  phis[4] = 60*(1 - 7*s*t)*(s - t);
  phis[5] = 30*sqrt(11)*(1 - 14*s*t*(1 - 3*s*t));
  phis[6] = 42*sqrt(13)*(1 - 6*s*t*(3 - 11*s*t))*(s - t);
}

void
BasisFunctionLine<Legendre,6>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 7);

  Real t = 1 - s;

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 12*sqrt(5);
  phiss[3] = 60*sqrt(7)*(s - t);
  phiss[4] = 180*(3 - 14*s*t);
  phiss[5] = 420*sqrt(11)*(1 - 6*s*t)*(s - t);
  phiss[6] = 840*sqrt(13)*(1 - 3*s*t*(4 - 11*s*t));
}


//----------------------------------------------------------------------------//
// Legendre: P7

void
BasisFunctionLine<Legendre,7>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real t = 1 - s;

  phi[0] = 1;
  phi[1] = sqrt(3)*(s - t);
  phi[2] = sqrt(5)*(1 - 6*s*t);
  phi[3] = sqrt(7)*(1 - 10*s*t)*(s - t);
  phi[4] = 3*(1 - 10*s*t*(2 - 7*s*t));
  phi[5] = sqrt(11)*(1 - 14*s*t*(s - 2*t)*(2*s - t))*(s - t);
  phi[6] = sqrt(13)*(1 - 42*s*t*(1 - s*t*(9 - 22*s*t)));
  phi[7] = sqrt(15)*(1 - 6*s*t*(9 - 11*s*t*(9 - 26*s*t)))*(s - t);
}

void
BasisFunctionLine<Legendre,7>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real t = 1 - s;

  phis[0] =  0;
  phis[1] =  2*sqrt(3);
  phis[2] =  6*sqrt(5)*(s - t);
  phis[3] = 12*sqrt(7)*(1 - 5*s*t);
  phis[4] = 60*(1 - 7*s*t)*(s - t);
  phis[5] = 30*sqrt(11)*(1 - 14*s*t*(1 - 3*s*t));
  phis[6] = 42*sqrt(13)*(1 - 6*s*t*(3 - 11*s*t))*(s - t);
  phis[7] = 56*sqrt(15)*(1 - 3*s*t*(9 - 11*s*t*(6 - 13*s*t)));
}

void
BasisFunctionLine<Legendre,7>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  Real t = 1 - s;

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 12*sqrt(5);
  phiss[3] = 60*sqrt(7)*(s - t);
  phiss[4] = 180*(3 - 14*s*t);
  phiss[5] = 420*sqrt(11)*(1 - 6*s*t)*(s - t);
  phiss[6] = 840*sqrt(13)*(1 - 3*s*t*(4 - 11*s*t));
  phiss[7] = 504*sqrt(15)*(3 - 11*s*t*(4 - 13*s*t))*(s - t);
}

}
