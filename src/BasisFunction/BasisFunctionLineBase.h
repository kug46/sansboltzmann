// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONLINEBASE_H
#define BASISFUNCTIONLINEBASE_H

// NOTES:
// - Implemented via abstract base class (ABC)
// - Derived classes only contain static data. Hence, they are
//   constructed to be singletons. Using templates instead of ABC
//   would lead to code bloat


#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "BasisFunctionBase.h"

namespace SANS
{

template <class BasisPolynomial, int BasisOrder>
class BasisFunctionLine;

//----------------------------------------------------------------------------//
// line basis functions
//
// reference line element defined: s in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edge interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinate
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

class BasisFunctionLineBase : public BasisFunctionBase
{
public:
  static const int D = 1;     // topological dimensions

  virtual ~BasisFunctionLineBase() {}

  virtual int order() const override = 0;
  virtual int nBasis() const override = 0;
  virtual int nBasisNode() const = 0;
  virtual int nBasisEdge() const = 0;
  virtual BasisFunctionCategory category() const override = 0;

  virtual void evalBasis( const Real s, Real phi[], int nphi ) const = 0;
  virtual void evalBasisDerivative( const Real s, Real phis[], int nphi ) const = 0;
  virtual void evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const = 0;

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

  static const BasisFunctionLineBase* getBasisFunction( const int order, const BasisFunctionCategory& category );

protected:
  BasisFunctionLineBase() {}      // abstract base class

public:
  static const BasisFunctionLine<Hierarchical,1> *HierarchicalP1;
  static const BasisFunctionLine<Hierarchical,2> *HierarchicalP2;
  static const BasisFunctionLine<Hierarchical,3> *HierarchicalP3;
  static const BasisFunctionLine<Hierarchical,4> *HierarchicalP4;
  static const BasisFunctionLine<Hierarchical,5> *HierarchicalP5;
  static const BasisFunctionLine<Hierarchical,6> *HierarchicalP6;
  static const BasisFunctionLine<Hierarchical,7> *HierarchicalP7;

  static const BasisFunctionLine<Legendre,0> *LegendreP0;
  static const BasisFunctionLine<Legendre,1> *LegendreP1;
  static const BasisFunctionLine<Legendre,2> *LegendreP2;
  static const BasisFunctionLine<Legendre,3> *LegendreP3;
  static const BasisFunctionLine<Legendre,4> *LegendreP4;
  static const BasisFunctionLine<Legendre,5> *LegendreP5;
  static const BasisFunctionLine<Legendre,6> *LegendreP6;
  static const BasisFunctionLine<Legendre,7> *LegendreP7;

  static const BasisFunctionLine<Lagrange,1> *LagrangeP1;
  static const BasisFunctionLine<Lagrange,2> *LagrangeP2;
  static const BasisFunctionLine<Lagrange,3> *LagrangeP3;
  static const BasisFunctionLine<Lagrange,4> *LagrangeP4;
  static const BasisFunctionLine<Lagrange,5> *LagrangeP5;

  static const BasisFunctionLine<Bernstein,0> *BernsteinP0;
  static const BasisFunctionLine<Bernstein,1> *BernsteinP1;
  static const BasisFunctionLine<Bernstein,2> *BernsteinP2;
  static const BasisFunctionLine<Bernstein,3> *BernsteinP3;
  static const BasisFunctionLine<Bernstein,4> *BernsteinP4;

  static const BasisFunctionLine<RBS,1> *RBSP1;
};

}

#endif  // BASISFUNCTIONLINEBASE_H
