// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionVolume_Tetrahedron_Bernstein.h"

#include <cmath>  // sqrt
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Bernstein: Linear
void
BasisFunctionVolume<Tet,Bernstein,1>::evalBasis( const Real& s, const Real& t, const Real& u, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real b1 = 1 - s - t - u;
  Real b2 = s;
  Real b3 = t;
  Real b4 = u;

  phi[0] = b1;
  phi[1] = b2;
  phi[2] = b3;
  phi[3] = b4;

}

void
BasisFunctionVolume<Tet,Bernstein,1>::evalBasisDerivative( const Real& s, const Real& t, const Real& u, const Int4&,
                                                              Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phis[0] = -1;
  phis[1] = 1;
  phis[2] = 0;
  phis[3] = 0;

  phit[0] = -1;
  phit[1] = 0;
  phit[2] = 1;
  phit[3] = 0;

  phiu[0] = -1;
  phiu[1] = 0;
  phiu[2] = 0;
  phiu[3] = 1;

}

void
BasisFunctionVolume<Tet,Bernstein,1>::evalBasisHessianDerivative( const Real&, const Real&, const Real&, const Int4&,
    Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phiss[0] = 0;
  phist[0] = 0;
  phitt[0] = 0;
  phisu[0] = 0;
  phitu[0] = 0;
  phiuu[0] = 0;
}

//----------------------------------------------------------------------------//
// Bernstein: Quadratic
void
BasisFunctionVolume<Tet,Bernstein,2>::evalBasis( const Real& s, const Real& t, const Real& u, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  Real b1 = 1 - s - t - u;
  Real b2 = s;
  Real b3 = t;
  Real b4 = u;

  phi[0] = b1*b1;
  phi[1] = b2*b2;
  phi[2] = b3*b3;
  phi[3] = b4*b4;
  phi[4] = 2*b1*b2;
  phi[5] = 2*b2*b3;
  phi[6] = 2*b3*b1;
  phi[7] = 2*b1*b4;
  phi[8] = 2*b2*b4;
  phi[9] = 2*b3*b4;


}

void
BasisFunctionVolume<Tet,Bernstein,2>::evalBasisDerivative( const Real& s, const Real& t, const Real& u, const Int4&,
                                                              Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  phis[0] = -2*(1 - s - t - u);
  phis[1] = 2*s;
  phis[2] = 0;
  phis[3] = 0;
  phis[4] = -2* s +2 *(1 - s - t - u);
  phis[5] = 2*t;
  phis[6] = -2*t;
  phis[7] = -2*u;
  phis[8] = 2*u;
  phis[9] = 0;

  phit[0] = -2*(1-s-t-u);
  phit[1] = 0;
  phit[2] = 2*t;
  phit[3] = 0;
  phit[4] = -2*s;
  phit[5] = 2*s;
  phit[6] = -2*t+2*(1-s-t-u);
  phit[7] = -2*u;
  phit[8] = 0;
  phit[9] = 2*u;


  phiu[0] = -2*(1-s-t-u);
  phiu[1] = 0;
  phiu[2] = 0;
  phiu[3] = 2*u;
  phiu[4] = -2*s;
  phiu[5] = 0;
  phiu[6] = -2*t;
  phiu[7] = 2*(1-s-t-u)-2*u;
  phiu[8] = 2*s;
  phiu[9] = 2*t;


}

void
BasisFunctionVolume<Tet,Bernstein,2>::evalBasisHessianDerivative( const Real& s, const Real& t, const Real& u, const Int4&,
    Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 10);

  phiss[0] = 0;
  phist[0] = 0;
  phitt[0] = 0;
  phisu[0] = 0;
  phitu[0] = 0;
  phiuu[0] = 0;
}
//----------------------------------------------------------------------------//
// Bernstein: Cubic
void
BasisFunctionVolume<Tet,Bernstein,3>::evalBasis( const Real& s, const Real& t, const Real& u, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 20);

  Real b1 = 1 - s - t - u;
  Real b2 = s;
  Real b3 = t;
  Real b4 = u;

  phi[0] = b1*b1*b1;
  phi[1] = b2*b2*b2;
  phi[2] = b3*b3*b3;
  phi[3] = b4*b4*b4;
  phi[4] = 3*b1*b1*b2;
  phi[5] = 3*b1*b2*b2;
  phi[6] = 3*b2*b2*b3;
  phi[7] = 3*b2*b3*b3;
  phi[8] = 3*b1*b3*b3;
  phi[9] = 3*b1*b1*b3;
  phi[10] = 3*b1*b1*b4;
  phi[11] = 3*b1*b4*b4;
  phi[12] = 3*b3*b3*b4;
  phi[13] = 3*b3*b4*b4;
  phi[14] = 3*b2*b2*b4;
  phi[15] = 3*b2*b4*b4;
  phi[16] = 6*b1*b2*b3;
  phi[17] = 6*b1*b3*b4;
  phi[18] = 6*b1*b2*b4;
  phi[19] = 6*b2*b3*b4;

}

void
BasisFunctionVolume<Tet,Bernstein,3>::evalBasisDerivative( const Real& s, const Real& t, const Real& u, const Int4&,
                                                              Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 20);

  phis[0] = -3*(1-s-t-u)*(1-s-t-u);
  phis[1] = 3*s*s;
  phis[2] = 0;
  phis[3] = 0;
  phis[4] = -6*s*(1-s-t-u)+3*(1-s-t-u)*(1-s-t-u);
  phis[5] = -3*s*s+6*s*(1-s-t-u);
  phis[6] = 6*s*t;
  phis[7] = 3*t*t;
  phis[8] = -3*t*t;
  phis[9] = -6*t*(1-s-t-u);
  phis[10] = -6*(1-s-t-u)*u;
  phis[11] = -3*u*u;
  phis[12] = 0;
  phis[13] = 0;
  phis[14] = 6*s*u;
  phis[15] = 3*u*u;
  phis[16] = -6*s*t+6*t*(1-s-t-u);
  phis[17] = -6*t*u;
  phis[18] = -6*s*u+6*(1-s-t-u)*u;
  phis[19] = 6*t*u;

  phit[0] = -3*(1-s-t-u)*(1-s-t-u);
  phit[1] = 0;
  phit[2] = 3*t*t;
  phit[3] = 0;
  phit[4] = -6*s*(1-s-t-u);
  phit[5] = -3*s*s;
  phit[6] = 3*s*s;
  phit[7] = 6*s*t;
  phit[8] = -3*t*t+6*t*(1-s-t-u);
  phit[9] = -6*t*(1-s-t-u)+3*(1-s-t-u)*(1-s-t-u);
  phit[10] = -6*(1-s-t-u)*u;
  phit[11] = -3*u*u;
  phit[12] = 6*t*u;
  phit[13] = 3*u*u;
  phit[14] = 0;
  phit[15] = 0;
  phit[16] = -6*s*t+6*s*(1-s-t-u);
  phit[17] = -6*t*u+6*(1-s-t-u)*u;
  phit[18] = -6*s*u;
  phit[19] = 6*s*u;

  phiu[0] = -3*(1-s-t-u)*(1-s-t-u);
  phiu[1] = 0;
  phiu[2] = 0;
  phiu[3] = 3*u*u;
  phiu[4] = -6*s*(1-s-t-u);
  phiu[5] = -3*s*s;
  phiu[6] = 0;
  phiu[7] = 0;
  phiu[8] = -3*t*t;
  phiu[9] = -6*t*(1-s-t-u);
  phiu[10] = 3*(1-s-t-u)*(1-s-t-u)-6*(1-s-t-u)*u;
  phiu[11] = 6*(1-s-t-u)*u-3*u*u;
  phiu[12] = 3*t*t;
  phiu[13] = 6*t*u;
  phiu[14] = 3*s*s;
  phiu[15] = 6*s*u;
  phiu[16] = -6*s*t;
  phiu[17] = 6*t*(1-s-t-u)-6*t*u;
  phiu[18] = 6*s*(1-s-t-u)-6*s*u;
  phiu[19] = 6*s*t;

}

void
BasisFunctionVolume<Tet,Bernstein,3>::evalBasisHessianDerivative( const Real&, const Real&, const Real&, const Int4&,
    Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 20);

  phiss[0] = 0;
  phist[0] = 0;
  phitt[0] = 0;
  phisu[0] = 0;
  phitu[0] = 0;
  phiuu[0] = 0;
}
//----------------------------------------------------------------------------//
// Bernstein: Quintic
void
BasisFunctionVolume<Tet,Bernstein,4>::evalBasis( const Real& s, const Real& t, const Real& u, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 35);

  Real b1 = 1 - s - t - u;
  Real b2 = s;
  Real b3 = t;
  Real b4 = u;

  phi[0] = b1*b1*b1*b1;
  phi[1] = b2*b2*b2*b2;
  phi[2] = b3*b3*b3*b3;
  phi[3] = b4*b4*b4*b4;
  phi[4] = 4*b1*b1*b1*b2;
  phi[5] = 6*b1*b1*b2*b2;
  phi[6] = 4*b1*b2*b2*b2;
  phi[7] = 4*b2*b2*b2*b3;
  phi[8] = 6*b2*b2*b3*b3;
  phi[9] = 4*b2*b3*b3*b3;
  phi[10] = 4*b3*b3*b3*b1;
  phi[11] = 6*b3*b3*b1*b1;
  phi[12] = 4*b3*b1*b1*b1;
  phi[13] = 4*b3*b3*b3*b4;
  phi[14] = 6*b3*b3*b4*b4;
  phi[15] = 4*b3*b4*b4*b4;
  phi[16] = 4*b1*b1*b1*b4;
  phi[17] = 6*b1*b1*b4*b4;
  phi[18] = 4*b1*b4*b4*b4;
  phi[19] = 4*b2*b2*b2*b4;
  phi[20] = 6*b2*b2*b4*b4;
  phi[21] = 4*b2*b4*b4*b4;
  phi[22] = 12*b1*b1*b2*b3;
  phi[23] = 12*b1*b2*b2*b3;
  phi[24] = 12*b1*b2*b3*b3;
  phi[25] = 12*b1*b1*b3*b4;
  phi[26] = 12*b1*b4*b4*b3;
  phi[27] = 12*b1*b4*b3*b3;
  phi[28] = 12*b1*b1*b2*b4;
  phi[29] = 12*b1*b2*b2*b4;
  phi[30] = 12*b1*b2*b4*b4;
  phi[31] = 12*b2*b2*b3*b4;
  phi[32] = 12*b2*b3*b3*b4;
  phi[33] = 12*b2*b3*b4*b4;
  phi[34] = 24*b1*b2*b3*b4;

}

void
BasisFunctionVolume<Tet,Bernstein,4>::evalBasisDerivative( const Real& s, const Real& t, const Real& u, const Int4&,
                                                              Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 35);

  phis[0] = -4 * (1 - s - t - u) * (1 - s - t - u) * (1 - s - t - u);
  phis[1] = 4 * s * s * s;
  phis[2] = 0;
  phis[3] = 0;
  phis[4] = -12 * s * (1 - s - t - u) * (1 - s - t - u) + 4 * (1 - s - t - u) * (1 - s - t - u) * (1 - s - t - u);
  phis[5] = -12 * s * s * (1 - s - t - u) + 12 * s * (1 - s - t - u) * (1 - s - t - u);
  phis[6] = -4 * s * s * s + 12 * s * s * (1 - s - t - u);
  phis[7] = 12 * s * s * t;
  phis[8] = 12 * s * t * t;
  phis[9] = 4 * t * t * t;
  phis[10] = -4 * t * t * t;
  phis[11] = -12 * t * t * (1 - s - t - u);
  phis[12] = -12 * t * (1 - s - t - u) * (1 - s - t - u);
  phis[13] = 0;
  phis[14] = 0;
  phis[15] = 0;
  phis[16] = -12 * (1 - s - t - u) * (1 - s - t - u) * u;
  phis[17] = -12 * (1 - s - t - u) * u * u;
  phis[18] = -4 * u * u * u;
  phis[19] = 12 * s * s * u;
  phis[20] = 12 * s * u * u;
  phis[21] = 4 * u * u * u;
  phis[22] = -24 * s * t * (1 - s - t - u) + 12 * t * (1 - s - t - u) * (1 - s - t - u);
  phis[23] = -12 * s * s * t + 24 * s * t * (1 - s - t - u);
  phis[24] = -12 * s * t * t + 12 * t * t * (1 - s - t - u);
  phis[25] = -24 * t * (1 - s - t - u) * u;
  phis[26] = -12 * t * u * u;
  phis[27] = -12 * t * t * u;
  phis[28] = -24 * s * (1 - s - t - u) * u + 12 * (1 - s - t - u) * (1 - s - t - u) * u;
  phis[29] = -12 * s * s * u + 24 * s * (1 - s - t - u) * u;
  phis[30] = -12 * s * u * u + 12 * (1 - s - t - u) * u * u;
  phis[31] = 24 * s * t * u;
  phis[32] = 12 * t * t * u;
  phis[33] = 12 * t * u * u;
  phis[34] = -24 * s * t * u + 24 * t * (1 - s - t - u) * u;

  phit[0] = -4 * (1 - s - t - u) * (1 - s - t - u) * (1 - s - t - u);
  phit[1] = 0;
  phit[2] = 4 * t * t * t;
  phit[3] = 0;
  phit[4] = -12 * s * (1 - s - t - u) * (1 - s - t - u);
  phit[5] = -12 * s * s * (1 - s - t - u);
  phit[6] = -4 * s * s * s;
  phit[7] = 4 * s * s * s;
  phit[8] = 12 * s * s * t;
  phit[9] = 12 * s * t * t;
  phit[10] = -4 * t * t * t + 12 * t * t * (1 - s - t - u);
  phit[11] = -12 * t * t * (1 - s - t - u) + 12 * t * (1 - s - t - u) * (1 - s - t - u);
  phit[12] = -12 * t * (1 - s - t - u) * (1 - s - t - u) + 4 * (1 - s - t - u) * (1 - s - t - u) * (1 - s - t - u);
  phit[13] = 12 * t * t * u;
  phit[14] = 12 * t * u * u;
  phit[15] = 4 * u * u * u;
  phit[16] = -12 * (1 - s - t - u) * (1 - s - t - u) * u;
  phit[17] = -12 * (1 - s - t - u) * u * u;
  phit[18] = -4 * u * u * u;
  phit[19] = 0;
  phit[20] = 0;
  phit[21] = 0;
  phit[22] = -24 * s * t * (1 - s - t - u) + 12 * s * (1 - s - t - u) * (1 - s - t - u);
  phit[23] = -12 * s * s * t + 12 * s * s * (1 - s - t - u);
  phit[24] = -12 * s * t * t + 24 * s * t * (1 - s - t - u);
  phit[25] = -24 * t * (1 - s - t - u) * u + 12 * (1 - s - t - u) * (1 - s - t - u) * u;
  phit[26] = -12 * t * u * u + 12 * (1 - s - t - u) * u * u;
  phit[27] = -12 * t * t * u + 24 * t * (1 - s - t - u) * u;
  phit[28] = -24 * s * (1 - s - t - u) * u;
  phit[29] = -12 * s * s * u;
  phit[30] = -12 * s * u * u;
  phit[31] = 12 * s * s * u;
  phit[32] = 24 * s * t * u;
  phit[33] = 12 * s * u * u;
  phit[34] = -24 * s * t * u + 24 * s * (1 - s - t - u) * u;

  phiu[0] = -4 * (1 - s - t - u) * (1 - s - t - u) * (1 - s - t - u);
  phiu[1] = 0;
  phiu[2] = 0;
  phiu[3] = 4 * u * u * u;
  phiu[4] = -12 * s * (1 - s - t - u) * (1 - s - t - u);
  phiu[5] = -12 * s * s * (1 - s - t - u);
  phiu[6] = -4 * s * s * s;
  phiu[7] = 0;
  phiu[8] = 0;
  phiu[9] = 0;
  phiu[10] = -4 * t * t * t;
  phiu[11] = -12 * t * t * (1 - s - t - u);
  phiu[12] = -12 * t * (1 - s - t - u) * (1 - s - t - u);
  phiu[13] = 4 * t * t * t;
  phiu[14] = 12 * t * t * u;
  phiu[15] = 12 * t * u * u;
  phiu[16] = 4 * (1 - s - t - u) * (1 - s - t - u) * (1 - s - t - u) - 12 * (1 - s - t - u) * (1 - s - t - u) * u;
  phiu[17] = 12 * (1 - s - t - u) * (1 - s - t - u) * u - 12 * (1 - s - t - u) * u * u;
  phiu[18] = 12 * (1 - s - t - u) * u * u - 4 * u * u * u;
  phiu[19] = 4 * s * s * s;
  phiu[20] = 12 * s * s * u;
  phiu[21] = 12 * s * u * u;
  phiu[22] = -24 * s * t * (1 - s - t - u);
  phiu[23] = -12 * s * s * t;
  phiu[24] = -12 * s * t * t;
  phiu[25] = 12 * t * (1 - s - t - u) * (1 - s - t - u) - 24 * t * (1 - s - t - u) * u;
  phiu[26] = 24 * t * (1 - s - t - u) * u - 12 * t * u * u;
  phiu[27] = 12 * t * t * (1 - s - t - u) - 12 * t * t * u;
  phiu[28] = 12 * s * (1 - s - t - u) * (1 - s - t - u) - 24 * s * (1 - s - t - u) * u;
  phiu[29] = 12 * s * s * (1 - s - t - u) - 12 * s * s * u;
  phiu[30] = 24 * s * (1 - s - t - u) * u - 12 * s * u * u;
  phiu[31] = 12 * s * s * t;
  phiu[32] = 12 * s * t * t;
  phiu[33] = 24 * s * t * u;
  phiu[34] = 24 * s * t * (1 - s - t - u) - 24 * s * t * u;

}

void
BasisFunctionVolume<Tet,Bernstein,4>::evalBasisHessianDerivative( const Real&, const Real&, const Real&, const Int4&,
    Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 35);

  phiss[0] = 0;
  phist[0] = 0;
  phitt[0] = 0;
  phisu[0] = 0;
  phitu[0] = 0;
  phiuu[0] = 0;
}

}
