// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONVOLUME_PROJECTION_H
#define BASISFUNCTIONVOLUME_PROJECTION_H

// projection of basis volume functions

#include "BasisFunctionVolume.h"
#include "Topology/ElementTopology.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionVolume_projectTo( const BasisFunctionVolumeBase<Tet>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                    const BasisFunctionVolumeBase<Tet>* basisTo  ,       T dofTo[]  , const int nDOFTo );

//----------------------------------------------------------------------------//
template<class T>
void BasisFunctionVolume_projectTo( const BasisFunctionVolumeBase<Hex>* basisFrom, const T dofFrom[], const int nDOFFrom,
                                    const BasisFunctionVolumeBase<Hex>* basisTo  ,       T dofTo[]  , const int nDOFTo );

}

#endif // BASISFUNCTIONVOLUME_PROJECTION_H
