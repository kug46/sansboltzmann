//----------------------------------------------------------------------------//
// Lagrange: P=1

void
BasisFunctionSpacetime<Pentatope,Lagrange,1>::evalBasis(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phi[], int nphi) const
{
SANS_ASSERT(nphi==5);

  // phi
phi[0] =  -s-t-u+1.0;
phi[1] =  s;
phi[2] =  t;
phi[3] =  u;
phi[4] =  v;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,1>::evalBasisDerivative(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi) const
{
SANS_ASSERT(nphi==5);

  // phis
phis[0] =  -1.0;
phis[1] =  1.0;
phis[2] =  0.0;
phis[3] =  0.0;
phis[4] =  0.0;

  // phit
phit[0] =  -1.0;
phit[1] =  0.0;
phit[2] =  1.0;
phit[3] =  0.0;
phit[4] =  0.0;

  // phiu
phiu[0] =  -1.0;
phiu[1] =  0.0;
phiu[2] =  0.0;
phiu[3] =  1.0;
phiu[4] =  0.0;

  // phiv
phiv[0] =  0.0;
phiv[1] =  0.0;
phiv[2] =  0.0;
phiv[3] =  0.0;
phiv[4] =  1.0;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,1>::evalBasisHessianDerivative(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi) const
{
SANS_ASSERT(nphi==5);

  // phiss
phiss[0] =  0.0;
phiss[1] =  0.0;
phiss[2] =  0.0;
phiss[3] =  0.0;
phiss[4] =  0.0;

  // phist
phist[0] =  0.0;
phist[1] =  0.0;
phist[2] =  0.0;
phist[3] =  0.0;
phist[4] =  0.0;

  // phitt
phitt[0] =  0.0;
phitt[1] =  0.0;
phitt[2] =  0.0;
phitt[3] =  0.0;
phitt[4] =  0.0;

  // phisu
phisu[0] =  0.0;
phisu[1] =  0.0;
phisu[2] =  0.0;
phisu[3] =  0.0;
phisu[4] =  0.0;

  // phitu
phitu[0] =  0.0;
phitu[1] =  0.0;
phitu[2] =  0.0;
phitu[3] =  0.0;
phitu[4] =  0.0;

  // phiuu
phiuu[0] =  0.0;
phiuu[1] =  0.0;
phiuu[2] =  0.0;
phiuu[3] =  0.0;
phiuu[4] =  0.0;

  // phisv
phisv[0] =  0.0;
phisv[1] =  0.0;
phisv[2] =  0.0;
phisv[3] =  0.0;
phisv[4] =  0.0;

  // phitv
phitv[0] =  0.0;
phitv[1] =  0.0;
phitv[2] =  0.0;
phitv[3] =  0.0;
phitv[4] =  0.0;

  // phiuv
phiuv[0] =  0.0;
phiuv[1] =  0.0;
phiuv[2] =  0.0;
phiuv[3] =  0.0;
phiuv[4] =  0.0;

  // phivv
phivv[0] =  0.0;
phivv[1] =  0.0;
phivv[2] =  0.0;
phivv[3] =  0.0;
phivv[4] =  0.0;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,1>::coordinates(std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u, std::vector<Real>& v) const
{
  s = coords_s_;
  t = coords_t_;
  u = coords_u_;
  v = coords_v_;
}

//----------------------------------------------------------------------------//
// Lagrange: P=2

void
BasisFunctionSpacetime<Pentatope,Lagrange,2>::evalBasis(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phi[], int nphi) const
{
SANS_ASSERT(nphi==15);

  // phi
phi[0] =  s*-3.0-t*3.0-u*3.0-v*3.0+s*t*4.0+s*u*4.0+s*v*4.0+t*u*4.0+t*v*4.0+u*v*4.0+(s*s)*2.0+(t*t)*2.0+
           (u*u)*2.0+(v*v)*2.0+1.0;
phi[1] =  s*(s*2.0-1.0);
phi[2] =  t*(t*2.0-1.0);
phi[3] =  u*(u*2.0-1.0);
phi[4] =  v*(v*2.0-1.0);
phi[5] =  s*(s+t+u+v-1.0)*-4.0;
phi[6] =  t*(s+t+u+v-1.0)*-4.0;
phi[7] =  u*(s+t+u+v-1.0)*-4.0;
phi[8] =  v*(s+t+u+v-1.0)*-4.0;
phi[9] =  s*t*4.0;
phi[10] =  s*u*4.0;
phi[11] =  s*v*4.0;
phi[12] =  t*u*4.0;
phi[13] =  t*v*4.0;
phi[14] =  u*v*4.0;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,2>::evalBasisDerivative(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi) const
{
SANS_ASSERT(nphi==15);

  // phis
phis[0] =  s*4.0+t*4.0+u*4.0+v*4.0-3.0;
phis[1] =  s*4.0-1.0;
phis[2] =  0.0;
phis[3] =  0.0;
phis[4] =  0.0;
phis[5] =  s*-8.0-t*4.0-u*4.0-v*4.0+4.0;
phis[6] =  t*-4.0;
phis[7] =  u*-4.0;
phis[8] =  v*-4.0;
phis[9] =  t*4.0;
phis[10] =  u*4.0;
phis[11] =  v*4.0;
phis[12] =  0.0;
phis[13] =  0.0;
phis[14] =  0.0;

  // phit
phit[0] =  s*4.0+t*4.0+u*4.0+v*4.0-3.0;
phit[1] =  0.0;
phit[2] =  t*4.0-1.0;
phit[3] =  0.0;
phit[4] =  0.0;
phit[5] =  s*-4.0;
phit[6] =  s*-4.0-t*8.0-u*4.0-v*4.0+4.0;
phit[7] =  u*-4.0;
phit[8] =  v*-4.0;
phit[9] =  s*4.0;
phit[10] =  0.0;
phit[11] =  0.0;
phit[12] =  u*4.0;
phit[13] =  v*4.0;
phit[14] =  0.0;

  // phiu
phiu[0] =  s*4.0+t*4.0+u*4.0+v*4.0-3.0;
phiu[1] =  0.0;
phiu[2] =  0.0;
phiu[3] =  u*4.0-1.0;
phiu[4] =  0.0;
phiu[5] =  s*-4.0;
phiu[6] =  t*-4.0;
phiu[7] =  s*-4.0-t*4.0-u*8.0-v*4.0+4.0;
phiu[8] =  v*-4.0;
phiu[9] =  0.0;
phiu[10] =  s*4.0;
phiu[11] =  0.0;
phiu[12] =  t*4.0;
phiu[13] =  0.0;
phiu[14] =  v*4.0;

  // phiv
phiv[0] =  s*4.0+t*4.0+u*4.0+v*4.0-3.0;
phiv[1] =  0.0;
phiv[2] =  0.0;
phiv[3] =  0.0;
phiv[4] =  v*4.0-1.0;
phiv[5] =  s*-4.0;
phiv[6] =  t*-4.0;
phiv[7] =  u*-4.0;
phiv[8] =  s*-4.0-t*4.0-u*4.0-v*8.0+4.0;
phiv[9] =  0.0;
phiv[10] =  0.0;
phiv[11] =  s*4.0;
phiv[12] =  0.0;
phiv[13] =  t*4.0;
phiv[14] =  u*4.0;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,2>::evalBasisHessianDerivative(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi) const
{
SANS_ASSERT(nphi==15);

  // phiss
phiss[0] =  4.0;
phiss[1] =  4.0;
phiss[2] =  0.0;
phiss[3] =  0.0;
phiss[4] =  0.0;
phiss[5] =  -8.0;
phiss[6] =  0.0;
phiss[7] =  0.0;
phiss[8] =  0.0;
phiss[9] =  0.0;
phiss[10] =  0.0;
phiss[11] =  0.0;
phiss[12] =  0.0;
phiss[13] =  0.0;
phiss[14] =  0.0;

  // phist
phist[0] =  4.0;
phist[1] =  0.0;
phist[2] =  0.0;
phist[3] =  0.0;
phist[4] =  0.0;
phist[5] =  -4.0;
phist[6] =  -4.0;
phist[7] =  0.0;
phist[8] =  0.0;
phist[9] =  4.0;
phist[10] =  0.0;
phist[11] =  0.0;
phist[12] =  0.0;
phist[13] =  0.0;
phist[14] =  0.0;

  // phitt
phitt[0] =  4.0;
phitt[1] =  0.0;
phitt[2] =  4.0;
phitt[3] =  0.0;
phitt[4] =  0.0;
phitt[5] =  0.0;
phitt[6] =  -8.0;
phitt[7] =  0.0;
phitt[8] =  0.0;
phitt[9] =  0.0;
phitt[10] =  0.0;
phitt[11] =  0.0;
phitt[12] =  0.0;
phitt[13] =  0.0;
phitt[14] =  0.0;

  // phisu
phisu[0] =  4.0;
phisu[1] =  0.0;
phisu[2] =  0.0;
phisu[3] =  0.0;
phisu[4] =  0.0;
phisu[5] =  -4.0;
phisu[6] =  0.0;
phisu[7] =  -4.0;
phisu[8] =  0.0;
phisu[9] =  0.0;
phisu[10] =  4.0;
phisu[11] =  0.0;
phisu[12] =  0.0;
phisu[13] =  0.0;
phisu[14] =  0.0;

  // phitu
phitu[0] =  4.0;
phitu[1] =  0.0;
phitu[2] =  0.0;
phitu[3] =  0.0;
phitu[4] =  0.0;
phitu[5] =  0.0;
phitu[6] =  -4.0;
phitu[7] =  -4.0;
phitu[8] =  0.0;
phitu[9] =  0.0;
phitu[10] =  0.0;
phitu[11] =  0.0;
phitu[12] =  4.0;
phitu[13] =  0.0;
phitu[14] =  0.0;

  // phiuu
phiuu[0] =  4.0;
phiuu[1] =  0.0;
phiuu[2] =  0.0;
phiuu[3] =  4.0;
phiuu[4] =  0.0;
phiuu[5] =  0.0;
phiuu[6] =  0.0;
phiuu[7] =  -8.0;
phiuu[8] =  0.0;
phiuu[9] =  0.0;
phiuu[10] =  0.0;
phiuu[11] =  0.0;
phiuu[12] =  0.0;
phiuu[13] =  0.0;
phiuu[14] =  0.0;

  // phisv
phisv[0] =  4.0;
phisv[1] =  0.0;
phisv[2] =  0.0;
phisv[3] =  0.0;
phisv[4] =  0.0;
phisv[5] =  -4.0;
phisv[6] =  0.0;
phisv[7] =  0.0;
phisv[8] =  -4.0;
phisv[9] =  0.0;
phisv[10] =  0.0;
phisv[11] =  4.0;
phisv[12] =  0.0;
phisv[13] =  0.0;
phisv[14] =  0.0;

  // phitv
phitv[0] =  4.0;
phitv[1] =  0.0;
phitv[2] =  0.0;
phitv[3] =  0.0;
phitv[4] =  0.0;
phitv[5] =  0.0;
phitv[6] =  -4.0;
phitv[7] =  0.0;
phitv[8] =  -4.0;
phitv[9] =  0.0;
phitv[10] =  0.0;
phitv[11] =  0.0;
phitv[12] =  0.0;
phitv[13] =  4.0;
phitv[14] =  0.0;

  // phiuv
phiuv[0] =  4.0;
phiuv[1] =  0.0;
phiuv[2] =  0.0;
phiuv[3] =  0.0;
phiuv[4] =  0.0;
phiuv[5] =  0.0;
phiuv[6] =  0.0;
phiuv[7] =  -4.0;
phiuv[8] =  -4.0;
phiuv[9] =  0.0;
phiuv[10] =  0.0;
phiuv[11] =  0.0;
phiuv[12] =  0.0;
phiuv[13] =  0.0;
phiuv[14] =  4.0;

  // phivv
phivv[0] =  4.0;
phivv[1] =  0.0;
phivv[2] =  0.0;
phivv[3] =  0.0;
phivv[4] =  4.0;
phivv[5] =  0.0;
phivv[6] =  0.0;
phivv[7] =  0.0;
phivv[8] =  -8.0;
phivv[9] =  0.0;
phivv[10] =  0.0;
phivv[11] =  0.0;
phivv[12] =  0.0;
phivv[13] =  0.0;
phivv[14] =  0.0;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,2>::coordinates(std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u, std::vector<Real>& v) const
{
  s = coords_s_;
  t = coords_t_;
  u = coords_u_;
  v = coords_v_;
}

//----------------------------------------------------------------------------//
// Lagrange: P=3

void
BasisFunctionSpacetime<Pentatope,Lagrange,3>::evalBasis(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phi[], int nphi) const
{
SANS_ASSERT(nphi==35);

  // phi
phi[0] =  s*(-1.1E1/2.0)-t*(1.1E1/2.0)-u*(1.1E1/2.0)-v*(1.1E1/2.0)+s*t*1.8E1+s*u*1.8E1+s*v*1.8E1+t*u*
           1.8E1+t*v*1.8E1+u*v*1.8E1-s*(t*t)*(2.7E1/2.0)-(s*s)*t*(2.7E1/2.0)-s*(u*u)*(2.7E1/2.0)-(s*s)*u*(2.7E1/
           2.0)-s*(v*v)*(2.7E1/2.0)-t*(u*u)*(2.7E1/2.0)-(s*s)*v*(2.7E1/2.0)-(t*t)*u*(2.7E1/2.0)-t*(v*v)*(2.7E1/2.0)-
           (t*t)*v*(2.7E1/2.0)-u*(v*v)*(2.7E1/2.0)-(u*u)*v*(2.7E1/2.0)+(s*s)*9.0-(s*s*s)*(9.0/2.0)+(t*t)*9.0-(t*
           t*t)*(9.0/2.0)+(u*u)*9.0-(u*u*u)*(9.0/2.0)+(v*v)*9.0-(v*v*v)*(9.0/2.0)-s*t*u*2.7E1-s*t*v*2.7E1-s*u*v*
           2.7E1-t*u*v*2.7E1+1.0;
phi[1] =  (s*(s*-9.0+(s*s)*9.0+2.0))/2.0;
phi[2] =  (t*(t*-9.0+(t*t)*9.0+2.0))/2.0;
phi[3] =  (u*(u*-9.0+(u*u)*9.0+2.0))/2.0;
phi[4] =  (v*(v*-9.0+(v*v)*9.0+2.0))/2.0;
phi[5] =  s*(s*-5.0-t*5.0-u*5.0-v*5.0+s*t*6.0+s*u*6.0+s*v*6.0+t*u*6.0+t*v*6.0+u*v*6.0+(s*s)*3.0+(t*t)*
           3.0+(u*u)*3.0+(v*v)*3.0+2.0)*(9.0/2.0);
phi[6] =  s*(s*3.0-1.0)*(s+t+u+v-1.0)*(-9.0/2.0);
phi[7] =  t*(s*-5.0-t*5.0-u*5.0-v*5.0+s*t*6.0+s*u*6.0+s*v*6.0+t*u*6.0+t*v*6.0+u*v*6.0+(s*s)*3.0+(t*t)*
           3.0+(u*u)*3.0+(v*v)*3.0+2.0)*(9.0/2.0);
phi[8] =  t*(t*3.0-1.0)*(s+t+u+v-1.0)*(-9.0/2.0);
phi[9] =  u*(s*-5.0-t*5.0-u*5.0-v*5.0+s*t*6.0+s*u*6.0+s*v*6.0+t*u*6.0+t*v*6.0+u*v*6.0+(s*s)*3.0+(t*t)*
           3.0+(u*u)*3.0+(v*v)*3.0+2.0)*(9.0/2.0);
phi[10] =  u*(u*3.0-1.0)*(s+t+u+v-1.0)*(-9.0/2.0);
phi[11] =  v*(s*-5.0-t*5.0-u*5.0-v*5.0+s*t*6.0+s*u*6.0+s*v*6.0+t*u*6.0+t*v*6.0+u*v*6.0+(s*s)*3.0+(t*t)*
           3.0+(u*u)*3.0+(v*v)*3.0+2.0)*(9.0/2.0);
phi[12] =  v*(v*3.0-1.0)*(s+t+u+v-1.0)*(-9.0/2.0);
phi[13] =  s*t*(s*3.0-1.0)*(9.0/2.0);
phi[14] =  s*t*(t*3.0-1.0)*(9.0/2.0);
phi[15] =  s*u*(s*3.0-1.0)*(9.0/2.0);
phi[16] =  s*u*(u*3.0-1.0)*(9.0/2.0);
phi[17] =  s*v*(s*3.0-1.0)*(9.0/2.0);
phi[18] =  s*v*(v*3.0-1.0)*(9.0/2.0);
phi[19] =  t*u*(t*3.0-1.0)*(9.0/2.0);
phi[20] =  t*u*(u*3.0-1.0)*(9.0/2.0);
phi[21] =  t*v*(t*3.0-1.0)*(9.0/2.0);
phi[22] =  t*v*(v*3.0-1.0)*(9.0/2.0);
phi[23] =  u*v*(u*3.0-1.0)*(9.0/2.0);
phi[24] =  u*v*(v*3.0-1.0)*(9.0/2.0);
phi[25] =  s*t*(s+t+u+v-1.0)*-2.7E1;
phi[26] =  s*u*(s+t+u+v-1.0)*-2.7E1;
phi[27] =  s*v*(s+t+u+v-1.0)*-2.7E1;
phi[28] =  t*u*(s+t+u+v-1.0)*-2.7E1;
phi[29] =  t*v*(s+t+u+v-1.0)*-2.7E1;
phi[30] =  u*v*(s+t+u+v-1.0)*-2.7E1;
phi[31] =  s*t*u*2.7E1;
phi[32] =  s*t*v*2.7E1;
phi[33] =  s*u*v*2.7E1;
phi[34] =  t*u*v*2.7E1;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,3>::evalBasisDerivative(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi) const
{
SANS_ASSERT(nphi==35);

  // phis
phis[0] =  s*1.8E1+t*1.8E1+u*1.8E1+v*1.8E1-s*t*2.7E1-s*u*2.7E1-s*v*2.7E1-t*u*2.7E1-t*v*2.7E1-u*v*2.7E1-
           (s*s)*(2.7E1/2.0)-(t*t)*(2.7E1/2.0)-(u*u)*(2.7E1/2.0)-(v*v)*(2.7E1/2.0)-1.1E1/2.0;
phis[1] =  s*(-9.0/2.0)+(s*(s*1.8E1-9.0))/2.0+(s*s)*(9.0/2.0)+1.0;
phis[2] =  0.0;
phis[3] =  0.0;
phis[4] =  0.0;
phis[5] =  s*(-4.5E1/2.0)-t*(4.5E1/2.0)-u*(4.5E1/2.0)-v*(4.5E1/2.0)+s*t*2.7E1+s*u*2.7E1+s*v*2.7E1+t*u*
           2.7E1+t*v*2.7E1+u*v*2.7E1+s*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0)+(s*s)*(2.7E1/2.0)+(t*t)*(2.7E1/2.0)+
           (u*u)*(2.7E1/2.0)+(v*v)*(2.7E1/2.0)+9.0;
phis[6] =  s*(s+t+u+v-1.0)*(-2.7E1/2.0)-s*(s*3.0-1.0)*(9.0/2.0)-(s*3.0-1.0)*(s+t+u+v-1.0)*(9.0/2.0);
phis[7] =  t*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phis[8] =  t*(t*3.0-1.0)*(-9.0/2.0);
phis[9] =  u*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phis[10] =  u*(u*3.0-1.0)*(-9.0/2.0);
phis[11] =  v*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phis[12] =  v*(v*3.0-1.0)*(-9.0/2.0);
phis[13] =  t*(s*3.0-1.0)*(9.0/2.0)+s*t*(2.7E1/2.0);
phis[14] =  t*(t*3.0-1.0)*(9.0/2.0);
phis[15] =  u*(s*3.0-1.0)*(9.0/2.0)+s*u*(2.7E1/2.0);
phis[16] =  u*(u*3.0-1.0)*(9.0/2.0);
phis[17] =  v*(s*3.0-1.0)*(9.0/2.0)+s*v*(2.7E1/2.0);
phis[18] =  v*(v*3.0-1.0)*(9.0/2.0);
phis[19] =  0.0;
phis[20] =  0.0;
phis[21] =  0.0;
phis[22] =  0.0;
phis[23] =  0.0;
phis[24] =  0.0;
phis[25] =  t*(s+t+u+v-1.0)*-2.7E1-s*t*2.7E1;
phis[26] =  u*(s+t+u+v-1.0)*-2.7E1-s*u*2.7E1;
phis[27] =  v*(s+t+u+v-1.0)*-2.7E1-s*v*2.7E1;
phis[28] =  t*u*-2.7E1;
phis[29] =  t*v*-2.7E1;
phis[30] =  u*v*-2.7E1;
phis[31] =  t*u*2.7E1;
phis[32] =  t*v*2.7E1;
phis[33] =  u*v*2.7E1;
phis[34] =  0.0;

  // phit
phit[0] =  s*1.8E1+t*1.8E1+u*1.8E1+v*1.8E1-s*t*2.7E1-s*u*2.7E1-s*v*2.7E1-t*u*2.7E1-t*v*2.7E1-u*v*2.7E1-
           (s*s)*(2.7E1/2.0)-(t*t)*(2.7E1/2.0)-(u*u)*(2.7E1/2.0)-(v*v)*(2.7E1/2.0)-1.1E1/2.0;
phit[1] =  0.0;
phit[2] =  t*(-9.0/2.0)+(t*(t*1.8E1-9.0))/2.0+(t*t)*(9.0/2.0)+1.0;
phit[3] =  0.0;
phit[4] =  0.0;
phit[5] =  s*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phit[6] =  s*(s*3.0-1.0)*(-9.0/2.0);
phit[7] =  s*(-4.5E1/2.0)-t*(4.5E1/2.0)-u*(4.5E1/2.0)-v*(4.5E1/2.0)+s*t*2.7E1+s*u*2.7E1+s*v*2.7E1+t*u*
           2.7E1+t*v*2.7E1+u*v*2.7E1+t*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0)+(s*s)*(2.7E1/2.0)+(t*t)*(2.7E1/2.0)+
           (u*u)*(2.7E1/2.0)+(v*v)*(2.7E1/2.0)+9.0;
phit[8] =  t*(s+t+u+v-1.0)*(-2.7E1/2.0)-t*(t*3.0-1.0)*(9.0/2.0)-(t*3.0-1.0)*(s+t+u+v-1.0)*(9.0/2.0);
phit[9] =  u*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phit[10] =  u*(u*3.0-1.0)*(-9.0/2.0);
phit[11] =  v*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phit[12] =  v*(v*3.0-1.0)*(-9.0/2.0);
phit[13] =  s*(s*3.0-1.0)*(9.0/2.0);
phit[14] =  s*(t*3.0-1.0)*(9.0/2.0)+s*t*(2.7E1/2.0);
phit[15] =  0.0;
phit[16] =  0.0;
phit[17] =  0.0;
phit[18] =  0.0;
phit[19] =  u*(t*3.0-1.0)*(9.0/2.0)+t*u*(2.7E1/2.0);
phit[20] =  u*(u*3.0-1.0)*(9.0/2.0);
phit[21] =  v*(t*3.0-1.0)*(9.0/2.0)+t*v*(2.7E1/2.0);
phit[22] =  v*(v*3.0-1.0)*(9.0/2.0);
phit[23] =  0.0;
phit[24] =  0.0;
phit[25] =  s*(s+t+u+v-1.0)*-2.7E1-s*t*2.7E1;
phit[26] =  s*u*-2.7E1;
phit[27] =  s*v*-2.7E1;
phit[28] =  u*(s+t+u+v-1.0)*-2.7E1-t*u*2.7E1;
phit[29] =  v*(s+t+u+v-1.0)*-2.7E1-t*v*2.7E1;
phit[30] =  u*v*-2.7E1;
phit[31] =  s*u*2.7E1;
phit[32] =  s*v*2.7E1;
phit[33] =  0.0;
phit[34] =  u*v*2.7E1;

  // phiu
phiu[0] =  s*1.8E1+t*1.8E1+u*1.8E1+v*1.8E1-s*t*2.7E1-s*u*2.7E1-s*v*2.7E1-t*u*2.7E1-t*v*2.7E1-u*v*2.7E1-
           (s*s)*(2.7E1/2.0)-(t*t)*(2.7E1/2.0)-(u*u)*(2.7E1/2.0)-(v*v)*(2.7E1/2.0)-1.1E1/2.0;
phiu[1] =  0.0;
phiu[2] =  0.0;
phiu[3] =  u*(-9.0/2.0)+(u*(u*1.8E1-9.0))/2.0+(u*u)*(9.0/2.0)+1.0;
phiu[4] =  0.0;
phiu[5] =  s*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phiu[6] =  s*(s*3.0-1.0)*(-9.0/2.0);
phiu[7] =  t*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phiu[8] =  t*(t*3.0-1.0)*(-9.0/2.0);
phiu[9] =  s*(-4.5E1/2.0)-t*(4.5E1/2.0)-u*(4.5E1/2.0)-v*(4.5E1/2.0)+s*t*2.7E1+s*u*2.7E1+s*v*2.7E1+t*u*
           2.7E1+t*v*2.7E1+u*v*2.7E1+u*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0)+(s*s)*(2.7E1/2.0)+(t*t)*(2.7E1/2.0)+
           (u*u)*(2.7E1/2.0)+(v*v)*(2.7E1/2.0)+9.0;
phiu[10] =  u*(s+t+u+v-1.0)*(-2.7E1/2.0)-u*(u*3.0-1.0)*(9.0/2.0)-(u*3.0-1.0)*(s+t+u+v-1.0)*(9.0/2.0);
phiu[11] =  v*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phiu[12] =  v*(v*3.0-1.0)*(-9.0/2.0);
phiu[13] =  0.0;
phiu[14] =  0.0;
phiu[15] =  s*(s*3.0-1.0)*(9.0/2.0);
phiu[16] =  s*(u*3.0-1.0)*(9.0/2.0)+s*u*(2.7E1/2.0);
phiu[17] =  0.0;
phiu[18] =  0.0;
phiu[19] =  t*(t*3.0-1.0)*(9.0/2.0);
phiu[20] =  t*(u*3.0-1.0)*(9.0/2.0)+t*u*(2.7E1/2.0);
phiu[21] =  0.0;
phiu[22] =  0.0;
phiu[23] =  v*(u*3.0-1.0)*(9.0/2.0)+u*v*(2.7E1/2.0);
phiu[24] =  v*(v*3.0-1.0)*(9.0/2.0);
phiu[25] =  s*t*-2.7E1;
phiu[26] =  s*(s+t+u+v-1.0)*-2.7E1-s*u*2.7E1;
phiu[27] =  s*v*-2.7E1;
phiu[28] =  t*(s+t+u+v-1.0)*-2.7E1-t*u*2.7E1;
phiu[29] =  t*v*-2.7E1;
phiu[30] =  v*(s+t+u+v-1.0)*-2.7E1-u*v*2.7E1;
phiu[31] =  s*t*2.7E1;
phiu[32] =  0.0;
phiu[33] =  s*v*2.7E1;
phiu[34] =  t*v*2.7E1;

  // phiv
phiv[0] =  s*1.8E1+t*1.8E1+u*1.8E1+v*1.8E1-s*t*2.7E1-s*u*2.7E1-s*v*2.7E1-t*u*2.7E1-t*v*2.7E1-u*v*2.7E1-
           (s*s)*(2.7E1/2.0)-(t*t)*(2.7E1/2.0)-(u*u)*(2.7E1/2.0)-(v*v)*(2.7E1/2.0)-1.1E1/2.0;
phiv[1] =  0.0;
phiv[2] =  0.0;
phiv[3] =  0.0;
phiv[4] =  v*(-9.0/2.0)+(v*(v*1.8E1-9.0))/2.0+(v*v)*(9.0/2.0)+1.0;
phiv[5] =  s*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phiv[6] =  s*(s*3.0-1.0)*(-9.0/2.0);
phiv[7] =  t*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phiv[8] =  t*(t*3.0-1.0)*(-9.0/2.0);
phiv[9] =  u*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0);
phiv[10] =  u*(u*3.0-1.0)*(-9.0/2.0);
phiv[11] =  s*(-4.5E1/2.0)-t*(4.5E1/2.0)-u*(4.5E1/2.0)-v*(4.5E1/2.0)+s*t*2.7E1+s*u*2.7E1+s*v*2.7E1+t*
           u*2.7E1+t*v*2.7E1+u*v*2.7E1+v*(s*6.0+t*6.0+u*6.0+v*6.0-5.0)*(9.0/2.0)+(s*s)*(2.7E1/2.0)+(t*t)*(2.7E1/
           2.0)+(u*u)*(2.7E1/2.0)+(v*v)*(2.7E1/2.0)+9.0;
phiv[12] =  v*(s+t+u+v-1.0)*(-2.7E1/2.0)-v*(v*3.0-1.0)*(9.0/2.0)-(v*3.0-1.0)*(s+t+u+v-1.0)*(9.0/2.0);
phiv[13] =  0.0;
phiv[14] =  0.0;
phiv[15] =  0.0;
phiv[16] =  0.0;
phiv[17] =  s*(s*3.0-1.0)*(9.0/2.0);
phiv[18] =  s*(v*3.0-1.0)*(9.0/2.0)+s*v*(2.7E1/2.0);
phiv[19] =  0.0;
phiv[20] =  0.0;
phiv[21] =  t*(t*3.0-1.0)*(9.0/2.0);
phiv[22] =  t*(v*3.0-1.0)*(9.0/2.0)+t*v*(2.7E1/2.0);
phiv[23] =  u*(u*3.0-1.0)*(9.0/2.0);
phiv[24] =  u*(v*3.0-1.0)*(9.0/2.0)+u*v*(2.7E1/2.0);
phiv[25] =  s*t*-2.7E1;
phiv[26] =  s*u*-2.7E1;
phiv[27] =  s*(s+t+u+v-1.0)*-2.7E1-s*v*2.7E1;
phiv[28] =  t*u*-2.7E1;
phiv[29] =  t*(s+t+u+v-1.0)*-2.7E1-t*v*2.7E1;
phiv[30] =  u*(s+t+u+v-1.0)*-2.7E1-u*v*2.7E1;
phiv[31] =  0.0;
phiv[32] =  s*t*2.7E1;
phiv[33] =  s*u*2.7E1;
phiv[34] =  t*u*2.7E1;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,3>::evalBasisHessianDerivative(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi) const
{
SANS_ASSERT(nphi==35);

  // phiss
phiss[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phiss[1] =  s*2.7E1-9.0;
phiss[2] =  0.0;
phiss[3] =  0.0;
phiss[4] =  0.0;
phiss[5] =  s*8.1E1+t*5.4E1+u*5.4E1+v*5.4E1-4.5E1;
phiss[6] =  s*-8.1E1-t*2.7E1-u*2.7E1-v*2.7E1+3.6E1;
phiss[7] =  t*2.7E1;
phiss[8] =  0.0;
phiss[9] =  u*2.7E1;
phiss[10] =  0.0;
phiss[11] =  v*2.7E1;
phiss[12] =  0.0;
phiss[13] =  t*2.7E1;
phiss[14] =  0.0;
phiss[15] =  u*2.7E1;
phiss[16] =  0.0;
phiss[17] =  v*2.7E1;
phiss[18] =  0.0;
phiss[19] =  0.0;
phiss[20] =  0.0;
phiss[21] =  0.0;
phiss[22] =  0.0;
phiss[23] =  0.0;
phiss[24] =  0.0;
phiss[25] =  t*-5.4E1;
phiss[26] =  u*-5.4E1;
phiss[27] =  v*-5.4E1;
phiss[28] =  0.0;
phiss[29] =  0.0;
phiss[30] =  0.0;
phiss[31] =  0.0;
phiss[32] =  0.0;
phiss[33] =  0.0;
phiss[34] =  0.0;

  // phist
phist[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phist[1] =  0.0;
phist[2] =  0.0;
phist[3] =  0.0;
phist[4] =  0.0;
phist[5] =  s*5.4E1+t*2.7E1+u*2.7E1+v*2.7E1-4.5E1/2.0;
phist[6] =  s*-2.7E1+9.0/2.0;
phist[7] =  s*2.7E1+t*5.4E1+u*2.7E1+v*2.7E1-4.5E1/2.0;
phist[8] =  t*-2.7E1+9.0/2.0;
phist[9] =  u*2.7E1;
phist[10] =  0.0;
phist[11] =  v*2.7E1;
phist[12] =  0.0;
phist[13] =  s*2.7E1-9.0/2.0;
phist[14] =  t*2.7E1-9.0/2.0;
phist[15] =  0.0;
phist[16] =  0.0;
phist[17] =  0.0;
phist[18] =  0.0;
phist[19] =  0.0;
phist[20] =  0.0;
phist[21] =  0.0;
phist[22] =  0.0;
phist[23] =  0.0;
phist[24] =  0.0;
phist[25] =  s*-5.4E1-t*5.4E1-u*2.7E1-v*2.7E1+2.7E1;
phist[26] =  u*-2.7E1;
phist[27] =  v*-2.7E1;
phist[28] =  u*-2.7E1;
phist[29] =  v*-2.7E1;
phist[30] =  0.0;
phist[31] =  u*2.7E1;
phist[32] =  v*2.7E1;
phist[33] =  0.0;
phist[34] =  0.0;

  // phitt
phitt[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phitt[1] =  0.0;
phitt[2] =  t*2.7E1-9.0;
phitt[3] =  0.0;
phitt[4] =  0.0;
phitt[5] =  s*2.7E1;
phitt[6] =  0.0;
phitt[7] =  s*5.4E1+t*8.1E1+u*5.4E1+v*5.4E1-4.5E1;
phitt[8] =  s*-2.7E1-t*8.1E1-u*2.7E1-v*2.7E1+3.6E1;
phitt[9] =  u*2.7E1;
phitt[10] =  0.0;
phitt[11] =  v*2.7E1;
phitt[12] =  0.0;
phitt[13] =  0.0;
phitt[14] =  s*2.7E1;
phitt[15] =  0.0;
phitt[16] =  0.0;
phitt[17] =  0.0;
phitt[18] =  0.0;
phitt[19] =  u*2.7E1;
phitt[20] =  0.0;
phitt[21] =  v*2.7E1;
phitt[22] =  0.0;
phitt[23] =  0.0;
phitt[24] =  0.0;
phitt[25] =  s*-5.4E1;
phitt[26] =  0.0;
phitt[27] =  0.0;
phitt[28] =  u*-5.4E1;
phitt[29] =  v*-5.4E1;
phitt[30] =  0.0;
phitt[31] =  0.0;
phitt[32] =  0.0;
phitt[33] =  0.0;
phitt[34] =  0.0;

  // phisu
phisu[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phisu[1] =  0.0;
phisu[2] =  0.0;
phisu[3] =  0.0;
phisu[4] =  0.0;
phisu[5] =  s*5.4E1+t*2.7E1+u*2.7E1+v*2.7E1-4.5E1/2.0;
phisu[6] =  s*-2.7E1+9.0/2.0;
phisu[7] =  t*2.7E1;
phisu[8] =  0.0;
phisu[9] =  s*2.7E1+t*2.7E1+u*5.4E1+v*2.7E1-4.5E1/2.0;
phisu[10] =  u*-2.7E1+9.0/2.0;
phisu[11] =  v*2.7E1;
phisu[12] =  0.0;
phisu[13] =  0.0;
phisu[14] =  0.0;
phisu[15] =  s*2.7E1-9.0/2.0;
phisu[16] =  u*2.7E1-9.0/2.0;
phisu[17] =  0.0;
phisu[18] =  0.0;
phisu[19] =  0.0;
phisu[20] =  0.0;
phisu[21] =  0.0;
phisu[22] =  0.0;
phisu[23] =  0.0;
phisu[24] =  0.0;
phisu[25] =  t*-2.7E1;
phisu[26] =  s*-5.4E1-t*2.7E1-u*5.4E1-v*2.7E1+2.7E1;
phisu[27] =  v*-2.7E1;
phisu[28] =  t*-2.7E1;
phisu[29] =  0.0;
phisu[30] =  v*-2.7E1;
phisu[31] =  t*2.7E1;
phisu[32] =  0.0;
phisu[33] =  v*2.7E1;
phisu[34] =  0.0;

  // phitu
phitu[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phitu[1] =  0.0;
phitu[2] =  0.0;
phitu[3] =  0.0;
phitu[4] =  0.0;
phitu[5] =  s*2.7E1;
phitu[6] =  0.0;
phitu[7] =  s*2.7E1+t*5.4E1+u*2.7E1+v*2.7E1-4.5E1/2.0;
phitu[8] =  t*-2.7E1+9.0/2.0;
phitu[9] =  s*2.7E1+t*2.7E1+u*5.4E1+v*2.7E1-4.5E1/2.0;
phitu[10] =  u*-2.7E1+9.0/2.0;
phitu[11] =  v*2.7E1;
phitu[12] =  0.0;
phitu[13] =  0.0;
phitu[14] =  0.0;
phitu[15] =  0.0;
phitu[16] =  0.0;
phitu[17] =  0.0;
phitu[18] =  0.0;
phitu[19] =  t*2.7E1-9.0/2.0;
phitu[20] =  u*2.7E1-9.0/2.0;
phitu[21] =  0.0;
phitu[22] =  0.0;
phitu[23] =  0.0;
phitu[24] =  0.0;
phitu[25] =  s*-2.7E1;
phitu[26] =  s*-2.7E1;
phitu[27] =  0.0;
phitu[28] =  s*-2.7E1-t*5.4E1-u*5.4E1-v*2.7E1+2.7E1;
phitu[29] =  v*-2.7E1;
phitu[30] =  v*-2.7E1;
phitu[31] =  s*2.7E1;
phitu[32] =  0.0;
phitu[33] =  0.0;
phitu[34] =  v*2.7E1;

  // phiuu
phiuu[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phiuu[1] =  0.0;
phiuu[2] =  0.0;
phiuu[3] =  u*2.7E1-9.0;
phiuu[4] =  0.0;
phiuu[5] =  s*2.7E1;
phiuu[6] =  0.0;
phiuu[7] =  t*2.7E1;
phiuu[8] =  0.0;
phiuu[9] =  s*5.4E1+t*5.4E1+u*8.1E1+v*5.4E1-4.5E1;
phiuu[10] =  s*-2.7E1-t*2.7E1-u*8.1E1-v*2.7E1+3.6E1;
phiuu[11] =  v*2.7E1;
phiuu[12] =  0.0;
phiuu[13] =  0.0;
phiuu[14] =  0.0;
phiuu[15] =  0.0;
phiuu[16] =  s*2.7E1;
phiuu[17] =  0.0;
phiuu[18] =  0.0;
phiuu[19] =  0.0;
phiuu[20] =  t*2.7E1;
phiuu[21] =  0.0;
phiuu[22] =  0.0;
phiuu[23] =  v*2.7E1;
phiuu[24] =  0.0;
phiuu[25] =  0.0;
phiuu[26] =  s*-5.4E1;
phiuu[27] =  0.0;
phiuu[28] =  t*-5.4E1;
phiuu[29] =  0.0;
phiuu[30] =  v*-5.4E1;
phiuu[31] =  0.0;
phiuu[32] =  0.0;
phiuu[33] =  0.0;
phiuu[34] =  0.0;

  // phisv
phisv[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phisv[1] =  0.0;
phisv[2] =  0.0;
phisv[3] =  0.0;
phisv[4] =  0.0;
phisv[5] =  s*5.4E1+t*2.7E1+u*2.7E1+v*2.7E1-4.5E1/2.0;
phisv[6] =  s*-2.7E1+9.0/2.0;
phisv[7] =  t*2.7E1;
phisv[8] =  0.0;
phisv[9] =  u*2.7E1;
phisv[10] =  0.0;
phisv[11] =  s*2.7E1+t*2.7E1+u*2.7E1+v*5.4E1-4.5E1/2.0;
phisv[12] =  v*-2.7E1+9.0/2.0;
phisv[13] =  0.0;
phisv[14] =  0.0;
phisv[15] =  0.0;
phisv[16] =  0.0;
phisv[17] =  s*2.7E1-9.0/2.0;
phisv[18] =  v*2.7E1-9.0/2.0;
phisv[19] =  0.0;
phisv[20] =  0.0;
phisv[21] =  0.0;
phisv[22] =  0.0;
phisv[23] =  0.0;
phisv[24] =  0.0;
phisv[25] =  t*-2.7E1;
phisv[26] =  u*-2.7E1;
phisv[27] =  s*-5.4E1-t*2.7E1-u*2.7E1-v*5.4E1+2.7E1;
phisv[28] =  0.0;
phisv[29] =  t*-2.7E1;
phisv[30] =  u*-2.7E1;
phisv[31] =  0.0;
phisv[32] =  t*2.7E1;
phisv[33] =  u*2.7E1;
phisv[34] =  0.0;

  // phitv
phitv[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phitv[1] =  0.0;
phitv[2] =  0.0;
phitv[3] =  0.0;
phitv[4] =  0.0;
phitv[5] =  s*2.7E1;
phitv[6] =  0.0;
phitv[7] =  s*2.7E1+t*5.4E1+u*2.7E1+v*2.7E1-4.5E1/2.0;
phitv[8] =  t*-2.7E1+9.0/2.0;
phitv[9] =  u*2.7E1;
phitv[10] =  0.0;
phitv[11] =  s*2.7E1+t*2.7E1+u*2.7E1+v*5.4E1-4.5E1/2.0;
phitv[12] =  v*-2.7E1+9.0/2.0;
phitv[13] =  0.0;
phitv[14] =  0.0;
phitv[15] =  0.0;
phitv[16] =  0.0;
phitv[17] =  0.0;
phitv[18] =  0.0;
phitv[19] =  0.0;
phitv[20] =  0.0;
phitv[21] =  t*2.7E1-9.0/2.0;
phitv[22] =  v*2.7E1-9.0/2.0;
phitv[23] =  0.0;
phitv[24] =  0.0;
phitv[25] =  s*-2.7E1;
phitv[26] =  0.0;
phitv[27] =  s*-2.7E1;
phitv[28] =  u*-2.7E1;
phitv[29] =  s*-2.7E1-t*5.4E1-u*2.7E1-v*5.4E1+2.7E1;
phitv[30] =  u*-2.7E1;
phitv[31] =  0.0;
phitv[32] =  s*2.7E1;
phitv[33] =  0.0;
phitv[34] =  u*2.7E1;

  // phiuv
phiuv[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phiuv[1] =  0.0;
phiuv[2] =  0.0;
phiuv[3] =  0.0;
phiuv[4] =  0.0;
phiuv[5] =  s*2.7E1;
phiuv[6] =  0.0;
phiuv[7] =  t*2.7E1;
phiuv[8] =  0.0;
phiuv[9] =  s*2.7E1+t*2.7E1+u*5.4E1+v*2.7E1-4.5E1/2.0;
phiuv[10] =  u*-2.7E1+9.0/2.0;
phiuv[11] =  s*2.7E1+t*2.7E1+u*2.7E1+v*5.4E1-4.5E1/2.0;
phiuv[12] =  v*-2.7E1+9.0/2.0;
phiuv[13] =  0.0;
phiuv[14] =  0.0;
phiuv[15] =  0.0;
phiuv[16] =  0.0;
phiuv[17] =  0.0;
phiuv[18] =  0.0;
phiuv[19] =  0.0;
phiuv[20] =  0.0;
phiuv[21] =  0.0;
phiuv[22] =  0.0;
phiuv[23] =  u*2.7E1-9.0/2.0;
phiuv[24] =  v*2.7E1-9.0/2.0;
phiuv[25] =  0.0;
phiuv[26] =  s*-2.7E1;
phiuv[27] =  s*-2.7E1;
phiuv[28] =  t*-2.7E1;
phiuv[29] =  t*-2.7E1;
phiuv[30] =  s*-2.7E1-t*2.7E1-u*5.4E1-v*5.4E1+2.7E1;
phiuv[31] =  0.0;
phiuv[32] =  0.0;
phiuv[33] =  s*2.7E1;
phiuv[34] =  t*2.7E1;

  // phivv
phivv[0] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*2.7E1+1.8E1;
phivv[1] =  0.0;
phivv[2] =  0.0;
phivv[3] =  0.0;
phivv[4] =  v*2.7E1-9.0;
phivv[5] =  s*2.7E1;
phivv[6] =  0.0;
phivv[7] =  t*2.7E1;
phivv[8] =  0.0;
phivv[9] =  u*2.7E1;
phivv[10] =  0.0;
phivv[11] =  s*5.4E1+t*5.4E1+u*5.4E1+v*8.1E1-4.5E1;
phivv[12] =  s*-2.7E1-t*2.7E1-u*2.7E1-v*8.1E1+3.6E1;
phivv[13] =  0.0;
phivv[14] =  0.0;
phivv[15] =  0.0;
phivv[16] =  0.0;
phivv[17] =  0.0;
phivv[18] =  s*2.7E1;
phivv[19] =  0.0;
phivv[20] =  0.0;
phivv[21] =  0.0;
phivv[22] =  t*2.7E1;
phivv[23] =  0.0;
phivv[24] =  u*2.7E1;
phivv[25] =  0.0;
phivv[26] =  0.0;
phivv[27] =  s*-5.4E1;
phivv[28] =  0.0;
phivv[29] =  t*-5.4E1;
phivv[30] =  u*-5.4E1;
phivv[31] =  0.0;
phivv[32] =  0.0;
phivv[33] =  0.0;
phivv[34] =  0.0;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,3>::coordinates(std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u, std::vector<Real>& v) const
{
  s = coords_s_;
  t = coords_t_;
  u = coords_u_;
  v = coords_v_;
}

//----------------------------------------------------------------------------//
// Lagrange: P=4

void
BasisFunctionSpacetime<Pentatope,Lagrange,4>::evalBasis(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phi[], int nphi) const
{
SANS_ASSERT(nphi==70);

  // phi
phi[0] =  s*(-2.5E1/3.0)-t*(2.5E1/3.0)-u*(2.5E1/3.0)-v*(2.5E1/3.0)+(s*s)*(t*t)*6.4E1+(s*s)*(u*u)*6.4E1+
           (s*s)*(v*v)*6.4E1+(t*t)*(u*u)*6.4E1+(t*t)*(v*v)*6.4E1+(u*u)*(v*v)*6.4E1+s*t*(1.4E2/3.0)+s*u*(1.4E2/3.0)+
           s*v*(1.4E2/3.0)+t*u*(1.4E2/3.0)+t*v*(1.4E2/3.0)+u*v*(1.4E2/3.0)-s*(t*t)*8.0E1-(s*s)*t*8.0E1+s*(t*t*t)*
           (1.28E2/3.0)+(s*s*s)*t*(1.28E2/3.0)-s*(u*u)*8.0E1-(s*s)*u*8.0E1+s*(u*u*u)*(1.28E2/3.0)+(s*s*s)*u*(1.28E2/
           3.0)-s*(v*v)*8.0E1-t*(u*u)*8.0E1-(s*s)*v*8.0E1-(t*t)*u*8.0E1+s*(v*v*v)*(1.28E2/3.0)+t*(u*u*u)*(1.28E2/
           3.0)+(s*s*s)*v*(1.28E2/3.0)+(t*t*t)*u*(1.28E2/3.0)-t*(v*v)*8.0E1-(t*t)*v*8.0E1+t*(v*v*v)*(1.28E2/3.0)+
           (t*t*t)*v*(1.28E2/3.0)-u*(v*v)*8.0E1-(u*u)*v*8.0E1+u*(v*v*v)*(1.28E2/3.0)+(u*u*u)*v*(1.28E2/3.0)+(s*s)*
           (7.0E1/3.0)-(s*s*s)*(8.0E1/3.0)+(s*s*s*s)*(3.2E1/3.0)+(t*t)*(7.0E1/3.0)-(t*t*t)*(8.0E1/3.0)+(t*t*t*t)*
           (3.2E1/3.0)+(u*u)*(7.0E1/3.0)-(u*u*u)*(8.0E1/3.0)+(u*u*u*u)*(3.2E1/3.0)+(v*v)*(7.0E1/3.0)-(v*v*v)*(8.0E1/
           3.0)+(v*v*v*v)*(3.2E1/3.0)+s*t*(u*u)*1.28E2+s*(t*t)*u*1.28E2+(s*s)*t*u*1.28E2+s*t*(v*v)*1.28E2+s*(t*t)*
           v*1.28E2+(s*s)*t*v*1.28E2+s*u*(v*v)*1.28E2+s*(u*u)*v*1.28E2+(s*s)*u*v*1.28E2+t*u*(v*v)*1.28E2+t*(u*u)*
           v*1.28E2+(t*t)*u*v*1.28E2-s*t*u*1.6E2-s*t*v*1.6E2-s*u*v*1.6E2-t*u*v*1.6E2+s*t*u*v*2.56E2+1.0;
phi[1] =  (s*(s*2.2E1-(s*s)*4.8E1+(s*s*s)*3.2E1-3.0))/3.0;
phi[2] =  (t*(t*2.2E1-(t*t)*4.8E1+(t*t*t)*3.2E1-3.0))/3.0;
phi[3] =  (u*(u*2.2E1-(u*u)*4.8E1+(u*u*u)*3.2E1-3.0))/3.0;
phi[4] =  (v*(v*2.2E1-(v*v)*4.8E1+(v*v*v)*3.2E1-3.0))/3.0;
phi[5] =  s*(s*1.3E1+t*1.3E1+u*1.3E1+v*1.3E1-s*t*3.6E1-s*u*3.6E1-s*v*3.6E1-t*u*3.6E1-t*v*3.6E1-u*v*3.6E1+
           s*(t*t)*2.4E1+(s*s)*t*2.4E1+s*(u*u)*2.4E1+(s*s)*u*2.4E1+s*(v*v)*2.4E1+t*(u*u)*2.4E1+(s*s)*v*2.4E1+(t*
           t)*u*2.4E1+t*(v*v)*2.4E1+(t*t)*v*2.4E1+u*(v*v)*2.4E1+(u*u)*v*2.4E1-(s*s)*1.8E1+(s*s*s)*8.0-(t*t)*1.8E1+
           (t*t*t)*8.0-(u*u)*1.8E1+(u*u*u)*8.0-(v*v)*1.8E1+(v*v*v)*8.0+s*t*u*4.8E1+s*t*v*4.8E1+s*u*v*4.8E1+t*u*v*
           4.8E1-3.0)*(-1.6E1/3.0);
phi[6] =  s*(s*4.0-1.0)*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*
           s)*4.0+(t*t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*4.0;
phi[7] =  s*(s*-6.0+(s*s)*8.0+1.0)*(s+t+u+v-1.0)*(-1.6E1/3.0);
phi[8] =  t*(s*1.3E1+t*1.3E1+u*1.3E1+v*1.3E1-s*t*3.6E1-s*u*3.6E1-s*v*3.6E1-t*u*3.6E1-t*v*3.6E1-u*v*3.6E1+
           s*(t*t)*2.4E1+(s*s)*t*2.4E1+s*(u*u)*2.4E1+(s*s)*u*2.4E1+s*(v*v)*2.4E1+t*(u*u)*2.4E1+(s*s)*v*2.4E1+(t*
           t)*u*2.4E1+t*(v*v)*2.4E1+(t*t)*v*2.4E1+u*(v*v)*2.4E1+(u*u)*v*2.4E1-(s*s)*1.8E1+(s*s*s)*8.0-(t*t)*1.8E1+
           (t*t*t)*8.0-(u*u)*1.8E1+(u*u*u)*8.0-(v*v)*1.8E1+(v*v*v)*8.0+s*t*u*4.8E1+s*t*v*4.8E1+s*u*v*4.8E1+t*u*v*
           4.8E1-3.0)*(-1.6E1/3.0);
phi[9] =  t*(t*4.0-1.0)*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*
           s)*4.0+(t*t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*4.0;
phi[10] =  t*(t*-6.0+(t*t)*8.0+1.0)*(s+t+u+v-1.0)*(-1.6E1/3.0);
phi[11] =  u*(s*1.3E1+t*1.3E1+u*1.3E1+v*1.3E1-s*t*3.6E1-s*u*3.6E1-s*v*3.6E1-t*u*3.6E1-t*v*3.6E1-u*v*3.6E1+
           s*(t*t)*2.4E1+(s*s)*t*2.4E1+s*(u*u)*2.4E1+(s*s)*u*2.4E1+s*(v*v)*2.4E1+t*(u*u)*2.4E1+(s*s)*v*2.4E1+(t*
           t)*u*2.4E1+t*(v*v)*2.4E1+(t*t)*v*2.4E1+u*(v*v)*2.4E1+(u*u)*v*2.4E1-(s*s)*1.8E1+(s*s*s)*8.0-(t*t)*1.8E1+
           (t*t*t)*8.0-(u*u)*1.8E1+(u*u*u)*8.0-(v*v)*1.8E1+(v*v*v)*8.0+s*t*u*4.8E1+s*t*v*4.8E1+s*u*v*4.8E1+t*u*v*
           4.8E1-3.0)*(-1.6E1/3.0);
phi[12] =  u*(u*4.0-1.0)*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*
           s)*4.0+(t*t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*4.0;
phi[13] =  u*(u*-6.0+(u*u)*8.0+1.0)*(s+t+u+v-1.0)*(-1.6E1/3.0);
phi[14] =  v*(s*1.3E1+t*1.3E1+u*1.3E1+v*1.3E1-s*t*3.6E1-s*u*3.6E1-s*v*3.6E1-t*u*3.6E1-t*v*3.6E1-u*v*3.6E1+
           s*(t*t)*2.4E1+(s*s)*t*2.4E1+s*(u*u)*2.4E1+(s*s)*u*2.4E1+s*(v*v)*2.4E1+t*(u*u)*2.4E1+(s*s)*v*2.4E1+(t*
           t)*u*2.4E1+t*(v*v)*2.4E1+(t*t)*v*2.4E1+u*(v*v)*2.4E1+(u*u)*v*2.4E1-(s*s)*1.8E1+(s*s*s)*8.0-(t*t)*1.8E1+
           (t*t*t)*8.0-(u*u)*1.8E1+(u*u*u)*8.0-(v*v)*1.8E1+(v*v*v)*8.0+s*t*u*4.8E1+s*t*v*4.8E1+s*u*v*4.8E1+t*u*v*
           4.8E1-3.0)*(-1.6E1/3.0);
phi[15] =  v*(v*4.0-1.0)*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*
           s)*4.0+(t*t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*4.0;
phi[16] =  v*(v*-6.0+(v*v)*8.0+1.0)*(s+t+u+v-1.0)*(-1.6E1/3.0);
phi[17] =  s*t*(s*-6.0+(s*s)*8.0+1.0)*(1.6E1/3.0);
phi[18] =  s*t*(s*4.0-1.0)*(t*4.0-1.0)*4.0;
phi[19] =  s*t*(t*-6.0+(t*t)*8.0+1.0)*(1.6E1/3.0);
phi[20] =  s*u*(s*-6.0+(s*s)*8.0+1.0)*(1.6E1/3.0);
phi[21] =  s*u*(s*4.0-1.0)*(u*4.0-1.0)*4.0;
phi[22] =  s*u*(u*-6.0+(u*u)*8.0+1.0)*(1.6E1/3.0);
phi[23] =  s*v*(s*-6.0+(s*s)*8.0+1.0)*(1.6E1/3.0);
phi[24] =  s*v*(s*4.0-1.0)*(v*4.0-1.0)*4.0;
phi[25] =  s*v*(v*-6.0+(v*v)*8.0+1.0)*(1.6E1/3.0);
phi[26] =  t*u*(t*-6.0+(t*t)*8.0+1.0)*(1.6E1/3.0);
phi[27] =  t*u*(t*4.0-1.0)*(u*4.0-1.0)*4.0;
phi[28] =  t*u*(u*-6.0+(u*u)*8.0+1.0)*(1.6E1/3.0);
phi[29] =  t*v*(t*-6.0+(t*t)*8.0+1.0)*(1.6E1/3.0);
phi[30] =  t*v*(t*4.0-1.0)*(v*4.0-1.0)*4.0;
phi[31] =  t*v*(v*-6.0+(v*v)*8.0+1.0)*(1.6E1/3.0);
phi[32] =  u*v*(u*-6.0+(u*u)*8.0+1.0)*(1.6E1/3.0);
phi[33] =  u*v*(u*4.0-1.0)*(v*4.0-1.0)*4.0;
phi[34] =  u*v*(v*-6.0+(v*v)*8.0+1.0)*(1.6E1/3.0);
phi[35] =  s*t*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1;
phi[36] =  s*t*(s*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[37] =  s*t*(t*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[38] =  s*u*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1;
phi[39] =  s*u*(s*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[40] =  s*u*(u*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[41] =  s*v*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1;
phi[42] =  s*v*(s*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[43] =  s*v*(v*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[44] =  t*u*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1;
phi[45] =  t*u*(t*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[46] =  t*u*(u*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[47] =  t*v*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1;
phi[48] =  t*v*(t*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[49] =  t*v*(v*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[50] =  u*v*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1;
phi[51] =  u*v*(u*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[52] =  u*v*(v*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1;
phi[53] =  s*t*u*(s*4.0-1.0)*3.2E1;
phi[54] =  s*t*u*(t*4.0-1.0)*3.2E1;
phi[55] =  s*t*u*(u*4.0-1.0)*3.2E1;
phi[56] =  s*t*v*(s*4.0-1.0)*3.2E1;
phi[57] =  s*t*v*(t*4.0-1.0)*3.2E1;
phi[58] =  s*t*v*(v*4.0-1.0)*3.2E1;
phi[59] =  s*u*v*(s*4.0-1.0)*3.2E1;
phi[60] =  s*u*v*(u*4.0-1.0)*3.2E1;
phi[61] =  s*u*v*(v*4.0-1.0)*3.2E1;
phi[62] =  t*u*v*(t*4.0-1.0)*3.2E1;
phi[63] =  t*u*v*(u*4.0-1.0)*3.2E1;
phi[64] =  t*u*v*(v*4.0-1.0)*3.2E1;
phi[65] =  s*t*u*v*2.56E2;
phi[66] =  t*u*v*(s+t+u+v-1.0)*-2.56E2;
phi[67] =  s*u*v*(s+t+u+v-1.0)*-2.56E2;
phi[68] =  s*t*v*(s+t+u+v-1.0)*-2.56E2;
phi[69] =  s*t*u*(s+t+u+v-1.0)*-2.56E2;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,4>::evalBasisDerivative(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi) const
{
SANS_ASSERT(nphi==70);

  // phis
phis[0] =  s*(1.4E2/3.0)+t*(1.4E2/3.0)+u*(1.4E2/3.0)+v*(1.4E2/3.0)-s*t*1.6E2-s*u*1.6E2-s*v*1.6E2-t*u*
           1.6E2-t*v*1.6E2-u*v*1.6E2+s*(t*t)*1.28E2+(s*s)*t*1.28E2+s*(u*u)*1.28E2+(s*s)*u*1.28E2+s*(v*v)*1.28E2+
           t*(u*u)*1.28E2+(s*s)*v*1.28E2+(t*t)*u*1.28E2+t*(v*v)*1.28E2+(t*t)*v*1.28E2+u*(v*v)*1.28E2+(u*u)*v*1.28E2-
           (s*s)*8.0E1+(s*s*s)*(1.28E2/3.0)-(t*t)*8.0E1+(t*t*t)*(1.28E2/3.0)-(u*u)*8.0E1+(u*u*u)*(1.28E2/3.0)-(v*
           v)*8.0E1+(v*v*v)*(1.28E2/3.0)+s*t*u*2.56E2+s*t*v*2.56E2+s*u*v*2.56E2+t*u*v*2.56E2-2.5E1/3.0;
phis[1] =  s*(2.2E1/3.0)+(s*(s*-9.6E1+(s*s)*9.6E1+2.2E1))/3.0-(s*s)*1.6E1+(s*s*s)*(3.2E1/3.0)-1.0;
phis[2] =  0.0;
phis[3] =  0.0;
phis[4] =  0.0;
phis[5] =  s*(-2.08E2/3.0)-t*(2.08E2/3.0)-u*(2.08E2/3.0)-v*(2.08E2/3.0)-s*(s*-3.6E1-t*3.6E1-u*3.6E1-v*
           3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+
           (v*v)*2.4E1+1.3E1)*(1.6E1/3.0)+s*t*1.92E2+s*u*1.92E2+s*v*1.92E2+t*u*1.92E2+t*v*1.92E2+u*v*1.92E2-s*(t*
           t)*1.28E2-(s*s)*t*1.28E2-s*(u*u)*1.28E2-(s*s)*u*1.28E2-s*(v*v)*1.28E2-t*(u*u)*1.28E2-(s*s)*v*1.28E2-(t*
           t)*u*1.28E2-t*(v*v)*1.28E2-(t*t)*v*1.28E2-u*(v*v)*1.28E2-(u*u)*v*1.28E2+(s*s)*9.6E1-(s*s*s)*(1.28E2/3.0)+
           (t*t)*9.6E1-(t*t*t)*(1.28E2/3.0)+(u*u)*9.6E1-(u*u*u)*(1.28E2/3.0)+(v*v)*9.6E1-(v*v*v)*(1.28E2/3.0)-s*
           t*u*2.56E2-s*t*v*2.56E2-s*u*v*2.56E2-t*u*v*2.56E2+1.6E1;
phis[6] =  s*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*t)*
           4.0+(u*u)*4.0+(v*v)*4.0+3.0)*1.6E1+(s*4.0-1.0)*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*
           8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*4.0+s*(s*4.0-1.0)*(s*8.0+t*8.0+u*8.0+
           v*8.0-7.0)*4.0;
phis[7] =  s*(s*-6.0+(s*s)*8.0+1.0)*(-1.6E1/3.0)-(s*-6.0+(s*s)*8.0+1.0)*(s+t+u+v-1.0)*(1.6E1/3.0)-s*(s*
           1.6E1-6.0)*(s+t+u+v-1.0)*(1.6E1/3.0);
phis[8] =  t*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phis[9] =  t*(t*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phis[10] =  t*(t*-6.0+(t*t)*8.0+1.0)*(-1.6E1/3.0);
phis[11] =  u*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phis[12] =  u*(u*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phis[13] =  u*(u*-6.0+(u*u)*8.0+1.0)*(-1.6E1/3.0);
phis[14] =  v*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phis[15] =  v*(v*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phis[16] =  v*(v*-6.0+(v*v)*8.0+1.0)*(-1.6E1/3.0);
phis[17] =  t*(s*-6.0+(s*s)*8.0+1.0)*(1.6E1/3.0)+s*t*(s*1.6E1-6.0)*(1.6E1/3.0);
phis[18] =  t*(s*4.0-1.0)*(t*4.0-1.0)*4.0+s*t*(t*4.0-1.0)*1.6E1;
phis[19] =  t*(t*-6.0+(t*t)*8.0+1.0)*(1.6E1/3.0);
phis[20] =  u*(s*-6.0+(s*s)*8.0+1.0)*(1.6E1/3.0)+s*u*(s*1.6E1-6.0)*(1.6E1/3.0);
phis[21] =  u*(s*4.0-1.0)*(u*4.0-1.0)*4.0+s*u*(u*4.0-1.0)*1.6E1;
phis[22] =  u*(u*-6.0+(u*u)*8.0+1.0)*(1.6E1/3.0);
phis[23] =  v*(s*-6.0+(s*s)*8.0+1.0)*(1.6E1/3.0)+s*v*(s*1.6E1-6.0)*(1.6E1/3.0);
phis[24] =  v*(s*4.0-1.0)*(v*4.0-1.0)*4.0+s*v*(v*4.0-1.0)*1.6E1;
phis[25] =  v*(v*-6.0+(v*v)*8.0+1.0)*(1.6E1/3.0);
phis[26] =  0.0;
phis[27] =  0.0;
phis[28] =  0.0;
phis[29] =  0.0;
phis[30] =  0.0;
phis[31] =  0.0;
phis[32] =  0.0;
phis[33] =  0.0;
phis[34] =  0.0;
phis[35] =  t*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+s*t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phis[36] =  t*(s*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*t*(s+t+u+v-1.0)*1.28E2-s*t*(s*4.0-1.0)*3.2E1;
phis[37] =  t*(t*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*t*(t*4.0-1.0)*3.2E1;
phis[38] =  u*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+s*u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phis[39] =  u*(s*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*u*(s+t+u+v-1.0)*1.28E2-s*u*(s*4.0-1.0)*3.2E1;
phis[40] =  u*(u*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*u*(u*4.0-1.0)*3.2E1;
phis[41] =  v*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+s*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phis[42] =  v*(s*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*v*(s+t+u+v-1.0)*1.28E2-s*v*(s*4.0-1.0)*3.2E1;
phis[43] =  v*(v*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*v*(v*4.0-1.0)*3.2E1;
phis[44] =  t*u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phis[45] =  t*u*(t*4.0-1.0)*-3.2E1;
phis[46] =  t*u*(u*4.0-1.0)*-3.2E1;
phis[47] =  t*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phis[48] =  t*v*(t*4.0-1.0)*-3.2E1;
phis[49] =  t*v*(v*4.0-1.0)*-3.2E1;
phis[50] =  u*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phis[51] =  u*v*(u*4.0-1.0)*-3.2E1;
phis[52] =  u*v*(v*4.0-1.0)*-3.2E1;
phis[53] =  t*u*(s*4.0-1.0)*3.2E1+s*t*u*1.28E2;
phis[54] =  t*u*(t*4.0-1.0)*3.2E1;
phis[55] =  t*u*(u*4.0-1.0)*3.2E1;
phis[56] =  t*v*(s*4.0-1.0)*3.2E1+s*t*v*1.28E2;
phis[57] =  t*v*(t*4.0-1.0)*3.2E1;
phis[58] =  t*v*(v*4.0-1.0)*3.2E1;
phis[59] =  u*v*(s*4.0-1.0)*3.2E1+s*u*v*1.28E2;
phis[60] =  u*v*(u*4.0-1.0)*3.2E1;
phis[61] =  u*v*(v*4.0-1.0)*3.2E1;
phis[62] =  0.0;
phis[63] =  0.0;
phis[64] =  0.0;
phis[65] =  t*u*v*2.56E2;
phis[66] =  t*u*v*-2.56E2;
phis[67] =  u*v*(s+t+u+v-1.0)*-2.56E2-s*u*v*2.56E2;
phis[68] =  t*v*(s+t+u+v-1.0)*-2.56E2-s*t*v*2.56E2;
phis[69] =  t*u*(s+t+u+v-1.0)*-2.56E2-s*t*u*2.56E2;

  // phit
phit[0] =  s*(1.4E2/3.0)+t*(1.4E2/3.0)+u*(1.4E2/3.0)+v*(1.4E2/3.0)-s*t*1.6E2-s*u*1.6E2-s*v*1.6E2-t*u*
           1.6E2-t*v*1.6E2-u*v*1.6E2+s*(t*t)*1.28E2+(s*s)*t*1.28E2+s*(u*u)*1.28E2+(s*s)*u*1.28E2+s*(v*v)*1.28E2+
           t*(u*u)*1.28E2+(s*s)*v*1.28E2+(t*t)*u*1.28E2+t*(v*v)*1.28E2+(t*t)*v*1.28E2+u*(v*v)*1.28E2+(u*u)*v*1.28E2-
           (s*s)*8.0E1+(s*s*s)*(1.28E2/3.0)-(t*t)*8.0E1+(t*t*t)*(1.28E2/3.0)-(u*u)*8.0E1+(u*u*u)*(1.28E2/3.0)-(v*
           v)*8.0E1+(v*v*v)*(1.28E2/3.0)+s*t*u*2.56E2+s*t*v*2.56E2+s*u*v*2.56E2+t*u*v*2.56E2-2.5E1/3.0;
phit[1] =  0.0;
phit[2] =  t*(2.2E1/3.0)+(t*(t*-9.6E1+(t*t)*9.6E1+2.2E1))/3.0-(t*t)*1.6E1+(t*t*t)*(3.2E1/3.0)-1.0;
phit[3] =  0.0;
phit[4] =  0.0;
phit[5] =  s*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phit[6] =  s*(s*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phit[7] =  s*(s*-6.0+(s*s)*8.0+1.0)*(-1.6E1/3.0);
phit[8] =  s*(-2.08E2/3.0)-t*(2.08E2/3.0)-u*(2.08E2/3.0)-v*(2.08E2/3.0)-t*(s*-3.6E1-t*3.6E1-u*3.6E1-v*
           3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+
           (v*v)*2.4E1+1.3E1)*(1.6E1/3.0)+s*t*1.92E2+s*u*1.92E2+s*v*1.92E2+t*u*1.92E2+t*v*1.92E2+u*v*1.92E2-s*(t*
           t)*1.28E2-(s*s)*t*1.28E2-s*(u*u)*1.28E2-(s*s)*u*1.28E2-s*(v*v)*1.28E2-t*(u*u)*1.28E2-(s*s)*v*1.28E2-(t*
           t)*u*1.28E2-t*(v*v)*1.28E2-(t*t)*v*1.28E2-u*(v*v)*1.28E2-(u*u)*v*1.28E2+(s*s)*9.6E1-(s*s*s)*(1.28E2/3.0)+
           (t*t)*9.6E1-(t*t*t)*(1.28E2/3.0)+(u*u)*9.6E1-(u*u*u)*(1.28E2/3.0)+(v*v)*9.6E1-(v*v*v)*(1.28E2/3.0)-s*
           t*u*2.56E2-s*t*v*2.56E2-s*u*v*2.56E2-t*u*v*2.56E2+1.6E1;
phit[9] =  t*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*t)*
           4.0+(u*u)*4.0+(v*v)*4.0+3.0)*1.6E1+(t*4.0-1.0)*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*
           8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*4.0+t*(t*4.0-1.0)*(s*8.0+t*8.0+u*8.0+
           v*8.0-7.0)*4.0;
phit[10] =  t*(t*-6.0+(t*t)*8.0+1.0)*(-1.6E1/3.0)-(t*-6.0+(t*t)*8.0+1.0)*(s+t+u+v-1.0)*(1.6E1/3.0)-t*
           (t*1.6E1-6.0)*(s+t+u+v-1.0)*(1.6E1/3.0);
phit[11] =  u*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phit[12] =  u*(u*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phit[13] =  u*(u*-6.0+(u*u)*8.0+1.0)*(-1.6E1/3.0);
phit[14] =  v*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phit[15] =  v*(v*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phit[16] =  v*(v*-6.0+(v*v)*8.0+1.0)*(-1.6E1/3.0);
phit[17] =  s*(s*-6.0+(s*s)*8.0+1.0)*(1.6E1/3.0);
phit[18] =  s*(s*4.0-1.0)*(t*4.0-1.0)*4.0+s*t*(s*4.0-1.0)*1.6E1;
phit[19] =  s*(t*-6.0+(t*t)*8.0+1.0)*(1.6E1/3.0)+s*t*(t*1.6E1-6.0)*(1.6E1/3.0);
phit[20] =  0.0;
phit[21] =  0.0;
phit[22] =  0.0;
phit[23] =  0.0;
phit[24] =  0.0;
phit[25] =  0.0;
phit[26] =  u*(t*-6.0+(t*t)*8.0+1.0)*(1.6E1/3.0)+t*u*(t*1.6E1-6.0)*(1.6E1/3.0);
phit[27] =  u*(t*4.0-1.0)*(u*4.0-1.0)*4.0+t*u*(u*4.0-1.0)*1.6E1;
phit[28] =  u*(u*-6.0+(u*u)*8.0+1.0)*(1.6E1/3.0);
phit[29] =  v*(t*-6.0+(t*t)*8.0+1.0)*(1.6E1/3.0)+t*v*(t*1.6E1-6.0)*(1.6E1/3.0);
phit[30] =  v*(t*4.0-1.0)*(v*4.0-1.0)*4.0+t*v*(v*4.0-1.0)*1.6E1;
phit[31] =  v*(v*-6.0+(v*v)*8.0+1.0)*(1.6E1/3.0);
phit[32] =  0.0;
phit[33] =  0.0;
phit[34] =  0.0;
phit[35] =  s*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+s*t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phit[36] =  s*(s*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*t*(s*4.0-1.0)*3.2E1;
phit[37] =  s*(t*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*t*(s+t+u+v-1.0)*1.28E2-s*t*(t*4.0-1.0)*3.2E1;
phit[38] =  s*u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phit[39] =  s*u*(s*4.0-1.0)*-3.2E1;
phit[40] =  s*u*(u*4.0-1.0)*-3.2E1;
phit[41] =  s*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phit[42] =  s*v*(s*4.0-1.0)*-3.2E1;
phit[43] =  s*v*(v*4.0-1.0)*-3.2E1;
phit[44] =  u*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+t*u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phit[45] =  u*(t*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-t*u*(s+t+u+v-1.0)*1.28E2-t*u*(t*4.0-1.0)*3.2E1;
phit[46] =  u*(u*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-t*u*(u*4.0-1.0)*3.2E1;
phit[47] =  v*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+t*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phit[48] =  v*(t*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-t*v*(s+t+u+v-1.0)*1.28E2-t*v*(t*4.0-1.0)*3.2E1;
phit[49] =  v*(v*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-t*v*(v*4.0-1.0)*3.2E1;
phit[50] =  u*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phit[51] =  u*v*(u*4.0-1.0)*-3.2E1;
phit[52] =  u*v*(v*4.0-1.0)*-3.2E1;
phit[53] =  s*u*(s*4.0-1.0)*3.2E1;
phit[54] =  s*u*(t*4.0-1.0)*3.2E1+s*t*u*1.28E2;
phit[55] =  s*u*(u*4.0-1.0)*3.2E1;
phit[56] =  s*v*(s*4.0-1.0)*3.2E1;
phit[57] =  s*v*(t*4.0-1.0)*3.2E1+s*t*v*1.28E2;
phit[58] =  s*v*(v*4.0-1.0)*3.2E1;
phit[59] =  0.0;
phit[60] =  0.0;
phit[61] =  0.0;
phit[62] =  u*v*(t*4.0-1.0)*3.2E1+t*u*v*1.28E2;
phit[63] =  u*v*(u*4.0-1.0)*3.2E1;
phit[64] =  u*v*(v*4.0-1.0)*3.2E1;
phit[65] =  s*u*v*2.56E2;
phit[66] =  u*v*(s+t+u+v-1.0)*-2.56E2-t*u*v*2.56E2;
phit[67] =  s*u*v*-2.56E2;
phit[68] =  s*v*(s+t+u+v-1.0)*-2.56E2-s*t*v*2.56E2;
phit[69] =  s*u*(s+t+u+v-1.0)*-2.56E2-s*t*u*2.56E2;

  // phiu
phiu[0] =  s*(1.4E2/3.0)+t*(1.4E2/3.0)+u*(1.4E2/3.0)+v*(1.4E2/3.0)-s*t*1.6E2-s*u*1.6E2-s*v*1.6E2-t*u*
           1.6E2-t*v*1.6E2-u*v*1.6E2+s*(t*t)*1.28E2+(s*s)*t*1.28E2+s*(u*u)*1.28E2+(s*s)*u*1.28E2+s*(v*v)*1.28E2+
           t*(u*u)*1.28E2+(s*s)*v*1.28E2+(t*t)*u*1.28E2+t*(v*v)*1.28E2+(t*t)*v*1.28E2+u*(v*v)*1.28E2+(u*u)*v*1.28E2-
           (s*s)*8.0E1+(s*s*s)*(1.28E2/3.0)-(t*t)*8.0E1+(t*t*t)*(1.28E2/3.0)-(u*u)*8.0E1+(u*u*u)*(1.28E2/3.0)-(v*
           v)*8.0E1+(v*v*v)*(1.28E2/3.0)+s*t*u*2.56E2+s*t*v*2.56E2+s*u*v*2.56E2+t*u*v*2.56E2-2.5E1/3.0;
phiu[1] =  0.0;
phiu[2] =  0.0;
phiu[3] =  u*(2.2E1/3.0)+(u*(u*-9.6E1+(u*u)*9.6E1+2.2E1))/3.0-(u*u)*1.6E1+(u*u*u)*(3.2E1/3.0)-1.0;
phiu[4] =  0.0;
phiu[5] =  s*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phiu[6] =  s*(s*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phiu[7] =  s*(s*-6.0+(s*s)*8.0+1.0)*(-1.6E1/3.0);
phiu[8] =  t*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phiu[9] =  t*(t*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phiu[10] =  t*(t*-6.0+(t*t)*8.0+1.0)*(-1.6E1/3.0);
phiu[11] =  s*(-2.08E2/3.0)-t*(2.08E2/3.0)-u*(2.08E2/3.0)-v*(2.08E2/3.0)-u*(s*-3.6E1-t*3.6E1-u*3.6E1-
           v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+
           (v*v)*2.4E1+1.3E1)*(1.6E1/3.0)+s*t*1.92E2+s*u*1.92E2+s*v*1.92E2+t*u*1.92E2+t*v*1.92E2+u*v*1.92E2-s*(t*
           t)*1.28E2-(s*s)*t*1.28E2-s*(u*u)*1.28E2-(s*s)*u*1.28E2-s*(v*v)*1.28E2-t*(u*u)*1.28E2-(s*s)*v*1.28E2-(t*
           t)*u*1.28E2-t*(v*v)*1.28E2-(t*t)*v*1.28E2-u*(v*v)*1.28E2-(u*u)*v*1.28E2+(s*s)*9.6E1-(s*s*s)*(1.28E2/3.0)+
           (t*t)*9.6E1-(t*t*t)*(1.28E2/3.0)+(u*u)*9.6E1-(u*u*u)*(1.28E2/3.0)+(v*v)*9.6E1-(v*v*v)*(1.28E2/3.0)-s*
           t*u*2.56E2-s*t*v*2.56E2-s*u*v*2.56E2-t*u*v*2.56E2+1.6E1;
phiu[12] =  u*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*1.6E1+(u*4.0-1.0)*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*
           u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*4.0+u*(u*4.0-1.0)*(s*8.0+t*8.0+u*8.0+
           v*8.0-7.0)*4.0;
phiu[13] =  u*(u*-6.0+(u*u)*8.0+1.0)*(-1.6E1/3.0)-(u*-6.0+(u*u)*8.0+1.0)*(s+t+u+v-1.0)*(1.6E1/3.0)-u*
           (u*1.6E1-6.0)*(s+t+u+v-1.0)*(1.6E1/3.0);
phiu[14] =  v*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phiu[15] =  v*(v*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phiu[16] =  v*(v*-6.0+(v*v)*8.0+1.0)*(-1.6E1/3.0);
phiu[17] =  0.0;
phiu[18] =  0.0;
phiu[19] =  0.0;
phiu[20] =  s*(s*-6.0+(s*s)*8.0+1.0)*(1.6E1/3.0);
phiu[21] =  s*(s*4.0-1.0)*(u*4.0-1.0)*4.0+s*u*(s*4.0-1.0)*1.6E1;
phiu[22] =  s*(u*-6.0+(u*u)*8.0+1.0)*(1.6E1/3.0)+s*u*(u*1.6E1-6.0)*(1.6E1/3.0);
phiu[23] =  0.0;
phiu[24] =  0.0;
phiu[25] =  0.0;
phiu[26] =  t*(t*-6.0+(t*t)*8.0+1.0)*(1.6E1/3.0);
phiu[27] =  t*(t*4.0-1.0)*(u*4.0-1.0)*4.0+t*u*(t*4.0-1.0)*1.6E1;
phiu[28] =  t*(u*-6.0+(u*u)*8.0+1.0)*(1.6E1/3.0)+t*u*(u*1.6E1-6.0)*(1.6E1/3.0);
phiu[29] =  0.0;
phiu[30] =  0.0;
phiu[31] =  0.0;
phiu[32] =  v*(u*-6.0+(u*u)*8.0+1.0)*(1.6E1/3.0)+u*v*(u*1.6E1-6.0)*(1.6E1/3.0);
phiu[33] =  v*(u*4.0-1.0)*(v*4.0-1.0)*4.0+u*v*(v*4.0-1.0)*1.6E1;
phiu[34] =  v*(v*-6.0+(v*v)*8.0+1.0)*(1.6E1/3.0);
phiu[35] =  s*t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiu[36] =  s*t*(s*4.0-1.0)*-3.2E1;
phiu[37] =  s*t*(t*4.0-1.0)*-3.2E1;
phiu[38] =  s*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+s*u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiu[39] =  s*(s*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*u*(s*4.0-1.0)*3.2E1;
phiu[40] =  s*(u*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*u*(s+t+u+v-1.0)*1.28E2-s*u*(u*4.0-1.0)*3.2E1;
phiu[41] =  s*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiu[42] =  s*v*(s*4.0-1.0)*-3.2E1;
phiu[43] =  s*v*(v*4.0-1.0)*-3.2E1;
phiu[44] =  t*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+t*u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiu[45] =  t*(t*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-t*u*(t*4.0-1.0)*3.2E1;
phiu[46] =  t*(u*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-t*u*(s+t+u+v-1.0)*1.28E2-t*u*(u*4.0-1.0)*3.2E1;
phiu[47] =  t*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiu[48] =  t*v*(t*4.0-1.0)*-3.2E1;
phiu[49] =  t*v*(v*4.0-1.0)*-3.2E1;
phiu[50] =  v*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+u*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiu[51] =  v*(u*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-u*v*(s+t+u+v-1.0)*1.28E2-u*v*(u*4.0-1.0)*3.2E1;
phiu[52] =  v*(v*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-u*v*(v*4.0-1.0)*3.2E1;
phiu[53] =  s*t*(s*4.0-1.0)*3.2E1;
phiu[54] =  s*t*(t*4.0-1.0)*3.2E1;
phiu[55] =  s*t*(u*4.0-1.0)*3.2E1+s*t*u*1.28E2;
phiu[56] =  0.0;
phiu[57] =  0.0;
phiu[58] =  0.0;
phiu[59] =  s*v*(s*4.0-1.0)*3.2E1;
phiu[60] =  s*v*(u*4.0-1.0)*3.2E1+s*u*v*1.28E2;
phiu[61] =  s*v*(v*4.0-1.0)*3.2E1;
phiu[62] =  t*v*(t*4.0-1.0)*3.2E1;
phiu[63] =  t*v*(u*4.0-1.0)*3.2E1+t*u*v*1.28E2;
phiu[64] =  t*v*(v*4.0-1.0)*3.2E1;
phiu[65] =  s*t*v*2.56E2;
phiu[66] =  t*v*(s+t+u+v-1.0)*-2.56E2-t*u*v*2.56E2;
phiu[67] =  s*v*(s+t+u+v-1.0)*-2.56E2-s*u*v*2.56E2;
phiu[68] =  s*t*v*-2.56E2;
phiu[69] =  s*t*(s+t+u+v-1.0)*-2.56E2-s*t*u*2.56E2;

  // phiv
phiv[0] =  s*(1.4E2/3.0)+t*(1.4E2/3.0)+u*(1.4E2/3.0)+v*(1.4E2/3.0)-s*t*1.6E2-s*u*1.6E2-s*v*1.6E2-t*u*
           1.6E2-t*v*1.6E2-u*v*1.6E2+s*(t*t)*1.28E2+(s*s)*t*1.28E2+s*(u*u)*1.28E2+(s*s)*u*1.28E2+s*(v*v)*1.28E2+
           t*(u*u)*1.28E2+(s*s)*v*1.28E2+(t*t)*u*1.28E2+t*(v*v)*1.28E2+(t*t)*v*1.28E2+u*(v*v)*1.28E2+(u*u)*v*1.28E2-
           (s*s)*8.0E1+(s*s*s)*(1.28E2/3.0)-(t*t)*8.0E1+(t*t*t)*(1.28E2/3.0)-(u*u)*8.0E1+(u*u*u)*(1.28E2/3.0)-(v*
           v)*8.0E1+(v*v*v)*(1.28E2/3.0)+s*t*u*2.56E2+s*t*v*2.56E2+s*u*v*2.56E2+t*u*v*2.56E2-2.5E1/3.0;
phiv[1] =  0.0;
phiv[2] =  0.0;
phiv[3] =  0.0;
phiv[4] =  v*(2.2E1/3.0)+(v*(v*-9.6E1+(v*v)*9.6E1+2.2E1))/3.0-(v*v)*1.6E1+(v*v*v)*(3.2E1/3.0)-1.0;
phiv[5] =  s*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phiv[6] =  s*(s*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phiv[7] =  s*(s*-6.0+(s*s)*8.0+1.0)*(-1.6E1/3.0);
phiv[8] =  t*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phiv[9] =  t*(t*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phiv[10] =  t*(t*-6.0+(t*t)*8.0+1.0)*(-1.6E1/3.0);
phiv[11] =  u*(s*-3.6E1-t*3.6E1-u*3.6E1-v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*
           4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+(v*v)*2.4E1+1.3E1)*(-1.6E1/3.0);
phiv[12] =  u*(u*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0;
phiv[13] =  u*(u*-6.0+(u*u)*8.0+1.0)*(-1.6E1/3.0);
phiv[14] =  s*(-2.08E2/3.0)-t*(2.08E2/3.0)-u*(2.08E2/3.0)-v*(2.08E2/3.0)-v*(s*-3.6E1-t*3.6E1-u*3.6E1-
           v*3.6E1+s*t*4.8E1+s*u*4.8E1+s*v*4.8E1+t*u*4.8E1+t*v*4.8E1+u*v*4.8E1+(s*s)*2.4E1+(t*t)*2.4E1+(u*u)*2.4E1+
           (v*v)*2.4E1+1.3E1)*(1.6E1/3.0)+s*t*1.92E2+s*u*1.92E2+s*v*1.92E2+t*u*1.92E2+t*v*1.92E2+u*v*1.92E2-s*(t*
           t)*1.28E2-(s*s)*t*1.28E2-s*(u*u)*1.28E2-(s*s)*u*1.28E2-s*(v*v)*1.28E2-t*(u*u)*1.28E2-(s*s)*v*1.28E2-(t*
           t)*u*1.28E2-t*(v*v)*1.28E2-(t*t)*v*1.28E2-u*(v*v)*1.28E2-(u*u)*v*1.28E2+(s*s)*9.6E1-(s*s*s)*(1.28E2/3.0)+
           (t*t)*9.6E1-(t*t*t)*(1.28E2/3.0)+(u*u)*9.6E1-(u*u*u)*(1.28E2/3.0)+(v*v)*9.6E1-(v*v*v)*(1.28E2/3.0)-s*
           t*u*2.56E2-s*t*v*2.56E2-s*u*v*2.56E2-t*u*v*2.56E2+1.6E1;
phiv[15] =  v*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*1.6E1+(v*4.0-1.0)*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*
           u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*4.0+v*(v*4.0-1.0)*(s*8.0+t*8.0+u*8.0+
           v*8.0-7.0)*4.0;
phiv[16] =  v*(v*-6.0+(v*v)*8.0+1.0)*(-1.6E1/3.0)-(v*-6.0+(v*v)*8.0+1.0)*(s+t+u+v-1.0)*(1.6E1/3.0)-v*
           (v*1.6E1-6.0)*(s+t+u+v-1.0)*(1.6E1/3.0);
phiv[17] =  0.0;
phiv[18] =  0.0;
phiv[19] =  0.0;
phiv[20] =  0.0;
phiv[21] =  0.0;
phiv[22] =  0.0;
phiv[23] =  s*(s*-6.0+(s*s)*8.0+1.0)*(1.6E1/3.0);
phiv[24] =  s*(s*4.0-1.0)*(v*4.0-1.0)*4.0+s*v*(s*4.0-1.0)*1.6E1;
phiv[25] =  s*(v*-6.0+(v*v)*8.0+1.0)*(1.6E1/3.0)+s*v*(v*1.6E1-6.0)*(1.6E1/3.0);
phiv[26] =  0.0;
phiv[27] =  0.0;
phiv[28] =  0.0;
phiv[29] =  t*(t*-6.0+(t*t)*8.0+1.0)*(1.6E1/3.0);
phiv[30] =  t*(t*4.0-1.0)*(v*4.0-1.0)*4.0+t*v*(t*4.0-1.0)*1.6E1;
phiv[31] =  t*(v*-6.0+(v*v)*8.0+1.0)*(1.6E1/3.0)+t*v*(v*1.6E1-6.0)*(1.6E1/3.0);
phiv[32] =  u*(u*-6.0+(u*u)*8.0+1.0)*(1.6E1/3.0);
phiv[33] =  u*(u*4.0-1.0)*(v*4.0-1.0)*4.0+u*v*(u*4.0-1.0)*1.6E1;
phiv[34] =  u*(v*-6.0+(v*v)*8.0+1.0)*(1.6E1/3.0)+u*v*(v*1.6E1-6.0)*(1.6E1/3.0);
phiv[35] =  s*t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiv[36] =  s*t*(s*4.0-1.0)*-3.2E1;
phiv[37] =  s*t*(t*4.0-1.0)*-3.2E1;
phiv[38] =  s*u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiv[39] =  s*u*(s*4.0-1.0)*-3.2E1;
phiv[40] =  s*u*(u*4.0-1.0)*-3.2E1;
phiv[41] =  s*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+s*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiv[42] =  s*(s*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*v*(s*4.0-1.0)*3.2E1;
phiv[43] =  s*(v*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-s*v*(s+t+u+v-1.0)*1.28E2-s*v*(v*4.0-1.0)*3.2E1;
phiv[44] =  t*u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiv[45] =  t*u*(t*4.0-1.0)*-3.2E1;
phiv[46] =  t*u*(u*4.0-1.0)*-3.2E1;
phiv[47] =  t*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+t*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiv[48] =  t*(t*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-t*v*(t*4.0-1.0)*3.2E1;
phiv[49] =  t*(v*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-t*v*(s+t+u+v-1.0)*1.28E2-t*v*(v*4.0-1.0)*3.2E1;
phiv[50] =  u*(s*-7.0-t*7.0-u*7.0-v*7.0+s*t*8.0+s*u*8.0+s*v*8.0+t*u*8.0+t*v*8.0+u*v*8.0+(s*s)*4.0+(t*
           t)*4.0+(u*u)*4.0+(v*v)*4.0+3.0)*3.2E1+u*v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiv[51] =  u*(u*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-u*v*(u*4.0-1.0)*3.2E1;
phiv[52] =  u*(v*4.0-1.0)*(s+t+u+v-1.0)*-3.2E1-u*v*(s+t+u+v-1.0)*1.28E2-u*v*(v*4.0-1.0)*3.2E1;
phiv[53] =  0.0;
phiv[54] =  0.0;
phiv[55] =  0.0;
phiv[56] =  s*t*(s*4.0-1.0)*3.2E1;
phiv[57] =  s*t*(t*4.0-1.0)*3.2E1;
phiv[58] =  s*t*(v*4.0-1.0)*3.2E1+s*t*v*1.28E2;
phiv[59] =  s*u*(s*4.0-1.0)*3.2E1;
phiv[60] =  s*u*(u*4.0-1.0)*3.2E1;
phiv[61] =  s*u*(v*4.0-1.0)*3.2E1+s*u*v*1.28E2;
phiv[62] =  t*u*(t*4.0-1.0)*3.2E1;
phiv[63] =  t*u*(u*4.0-1.0)*3.2E1;
phiv[64] =  t*u*(v*4.0-1.0)*3.2E1+t*u*v*1.28E2;
phiv[65] =  s*t*u*2.56E2;
phiv[66] =  t*u*(s+t+u+v-1.0)*-2.56E2-t*u*v*2.56E2;
phiv[67] =  s*u*(s+t+u+v-1.0)*-2.56E2-s*u*v*2.56E2;
phiv[68] =  s*t*(s+t+u+v-1.0)*-2.56E2-s*t*v*2.56E2;
phiv[69] =  s*t*u*-2.56E2;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,4>::evalBasisHessianDerivative(
  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,
  Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi) const
{
SANS_ASSERT(nphi==70);

  // phiss
phiss[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phiss[1] =  s*-6.4E1+(s*(s*1.92E2-9.6E1))/3.0+(s*s)*6.4E1+4.4E1/3.0;
phiss[2] =  0.0;
phiss[3] =  0.0;
phiss[4] =  0.0;
phiss[5] =  s*3.84E2+t*3.84E2+u*3.84E2+v*3.84E2-s*t*5.12E2-s*u*5.12E2-s*v*5.12E2-t*u*5.12E2-t*v*5.12E2-
           u*v*5.12E2-s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*2.56E2-(t*t)*2.56E2-(u*u)*2.56E2-
           (v*v)*2.56E2-4.16E2/3.0;
phiss[6] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+s*(s*4.0-1.0)*3.2E1+(s*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*8.0+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*v*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phiss[7] =  s*6.4E1-s*(s+t+u+v-1.0)*(2.56E2/3.0)-s*(s*1.6E1-6.0)*(3.2E1/3.0)-(s*1.6E1-6.0)*(s+t+u+v-1.0)*
           (3.2E1/3.0)-(s*s)*(2.56E2/3.0)-3.2E1/3.0;
phiss[8] =  t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phiss[9] =  t*(t*4.0-1.0)*3.2E1;
phiss[10] =  0.0;
phiss[11] =  u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phiss[12] =  u*(u*4.0-1.0)*3.2E1;
phiss[13] =  0.0;
phiss[14] =  v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phiss[15] =  v*(v*4.0-1.0)*3.2E1;
phiss[16] =  0.0;
phiss[17] =  t*(s*1.6E1-6.0)*(3.2E1/3.0)+s*t*(2.56E2/3.0);
phiss[18] =  t*(t*4.0-1.0)*3.2E1;
phiss[19] =  0.0;
phiss[20] =  u*(s*1.6E1-6.0)*(3.2E1/3.0)+s*u*(2.56E2/3.0);
phiss[21] =  u*(u*4.0-1.0)*3.2E1;
phiss[22] =  0.0;
phiss[23] =  v*(s*1.6E1-6.0)*(3.2E1/3.0)+s*v*(2.56E2/3.0);
phiss[24] =  v*(v*4.0-1.0)*3.2E1;
phiss[25] =  0.0;
phiss[26] =  0.0;
phiss[27] =  0.0;
phiss[28] =  0.0;
phiss[29] =  0.0;
phiss[30] =  0.0;
phiss[31] =  0.0;
phiss[32] =  0.0;
phiss[33] =  0.0;
phiss[34] =  0.0;
phiss[35] =  s*t*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phiss[36] =  t*(s+t+u+v-1.0)*-2.56E2-t*(s*4.0-1.0)*6.4E1-s*t*2.56E2;
phiss[37] =  t*(t*4.0-1.0)*-6.4E1;
phiss[38] =  s*u*2.56E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phiss[39] =  u*(s+t+u+v-1.0)*-2.56E2-u*(s*4.0-1.0)*6.4E1-s*u*2.56E2;
phiss[40] =  u*(u*4.0-1.0)*-6.4E1;
phiss[41] =  s*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phiss[42] =  v*(s+t+u+v-1.0)*-2.56E2-v*(s*4.0-1.0)*6.4E1-s*v*2.56E2;
phiss[43] =  v*(v*4.0-1.0)*-6.4E1;
phiss[44] =  t*u*2.56E2;
phiss[45] =  0.0;
phiss[46] =  0.0;
phiss[47] =  t*v*2.56E2;
phiss[48] =  0.0;
phiss[49] =  0.0;
phiss[50] =  u*v*2.56E2;
phiss[51] =  0.0;
phiss[52] =  0.0;
phiss[53] =  t*u*2.56E2;
phiss[54] =  0.0;
phiss[55] =  0.0;
phiss[56] =  t*v*2.56E2;
phiss[57] =  0.0;
phiss[58] =  0.0;
phiss[59] =  u*v*2.56E2;
phiss[60] =  0.0;
phiss[61] =  0.0;
phiss[62] =  0.0;
phiss[63] =  0.0;
phiss[64] =  0.0;
phiss[65] =  0.0;
phiss[66] =  0.0;
phiss[67] =  u*v*-5.12E2;
phiss[68] =  t*v*-5.12E2;
phiss[69] =  t*u*-5.12E2;

  // phist
phist[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phist[1] =  0.0;
phist[2] =  0.0;
phist[3] =  0.0;
phist[4] =  0.0;
phist[5] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phist[6] =  s*(s*4.0-1.0)*3.2E1+(s*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+s*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phist[7] =  s*3.2E1-s*(s*1.6E1-6.0)*(1.6E1/3.0)-(s*s)*(1.28E2/3.0)-1.6E1/3.0;
phist[8] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phist[9] =  t*(t*4.0-1.0)*3.2E1+(t*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+t*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phist[10] =  t*3.2E1-t*(t*1.6E1-6.0)*(1.6E1/3.0)-(t*t)*(1.28E2/3.0)-1.6E1/3.0;
phist[11] =  u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phist[12] =  u*(u*4.0-1.0)*3.2E1;
phist[13] =  0.0;
phist[14] =  v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phist[15] =  v*(v*4.0-1.0)*3.2E1;
phist[16] =  0.0;
phist[17] =  s*-3.2E1+s*(s*1.6E1-6.0)*(1.6E1/3.0)+(s*s)*(1.28E2/3.0)+1.6E1/3.0;
phist[18] =  s*(t*4.0-1.0)*1.6E1+t*(s*4.0-1.0)*1.6E1+s*t*6.4E1+(s*4.0-1.0)*(t*4.0-1.0)*4.0;
phist[19] =  t*-3.2E1+t*(t*1.6E1-6.0)*(1.6E1/3.0)+(t*t)*(1.28E2/3.0)+1.6E1/3.0;
phist[20] =  0.0;
phist[21] =  0.0;
phist[22] =  0.0;
phist[23] =  0.0;
phist[24] =  0.0;
phist[25] =  0.0;
phist[26] =  0.0;
phist[27] =  0.0;
phist[28] =  0.0;
phist[29] =  0.0;
phist[30] =  0.0;
phist[31] =  0.0;
phist[32] =  0.0;
phist[33] =  0.0;
phist[34] =  0.0;
phist[35] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+s*t*5.12E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+
           u*v*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*
           t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phist[36] =  s*(s+t+u+v-1.0)*-1.28E2-s*(s*4.0-1.0)*3.2E1-t*(s*4.0-1.0)*3.2E1-s*t*1.28E2-(s*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phist[37] =  t*(s+t+u+v-1.0)*-1.28E2-s*(t*4.0-1.0)*3.2E1-t*(t*4.0-1.0)*3.2E1-s*t*1.28E2-(t*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phist[38] =  s*u*2.56E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phist[39] =  u*(s*4.0-1.0)*-3.2E1-s*u*1.28E2;
phist[40] =  u*(u*4.0-1.0)*-3.2E1;
phist[41] =  s*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phist[42] =  v*(s*4.0-1.0)*-3.2E1-s*v*1.28E2;
phist[43] =  v*(v*4.0-1.0)*-3.2E1;
phist[44] =  t*u*2.56E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phist[45] =  u*(t*4.0-1.0)*-3.2E1-t*u*1.28E2;
phist[46] =  u*(u*4.0-1.0)*-3.2E1;
phist[47] =  t*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phist[48] =  v*(t*4.0-1.0)*-3.2E1-t*v*1.28E2;
phist[49] =  v*(v*4.0-1.0)*-3.2E1;
phist[50] =  u*v*2.56E2;
phist[51] =  0.0;
phist[52] =  0.0;
phist[53] =  u*(s*4.0-1.0)*3.2E1+s*u*1.28E2;
phist[54] =  u*(t*4.0-1.0)*3.2E1+t*u*1.28E2;
phist[55] =  u*(u*4.0-1.0)*3.2E1;
phist[56] =  v*(s*4.0-1.0)*3.2E1+s*v*1.28E2;
phist[57] =  v*(t*4.0-1.0)*3.2E1+t*v*1.28E2;
phist[58] =  v*(v*4.0-1.0)*3.2E1;
phist[59] =  0.0;
phist[60] =  0.0;
phist[61] =  0.0;
phist[62] =  0.0;
phist[63] =  0.0;
phist[64] =  0.0;
phist[65] =  u*v*2.56E2;
phist[66] =  u*v*-2.56E2;
phist[67] =  u*v*-2.56E2;
phist[68] =  v*(s+t+u+v-1.0)*-2.56E2-s*v*2.56E2-t*v*2.56E2;
phist[69] =  u*(s+t+u+v-1.0)*-2.56E2-s*u*2.56E2-t*u*2.56E2;

  // phitt
phitt[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phitt[1] =  0.0;
phitt[2] =  t*-6.4E1+(t*(t*1.92E2-9.6E1))/3.0+(t*t)*6.4E1+4.4E1/3.0;
phitt[3] =  0.0;
phitt[4] =  0.0;
phitt[5] =  s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phitt[6] =  s*(s*4.0-1.0)*3.2E1;
phitt[7] =  0.0;
phitt[8] =  s*3.84E2+t*3.84E2+u*3.84E2+v*3.84E2-s*t*5.12E2-s*u*5.12E2-s*v*5.12E2-t*u*5.12E2-t*v*5.12E2-
           u*v*5.12E2-t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*2.56E2-(t*t)*2.56E2-(u*u)*2.56E2-
           (v*v)*2.56E2-4.16E2/3.0;
phitt[9] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+t*(t*4.0-1.0)*3.2E1+(t*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*8.0+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*v*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phitt[10] =  t*6.4E1-t*(s+t+u+v-1.0)*(2.56E2/3.0)-t*(t*1.6E1-6.0)*(3.2E1/3.0)-(t*1.6E1-6.0)*(s+t+u+v-
           1.0)*(3.2E1/3.0)-(t*t)*(2.56E2/3.0)-3.2E1/3.0;
phitt[11] =  u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phitt[12] =  u*(u*4.0-1.0)*3.2E1;
phitt[13] =  0.0;
phitt[14] =  v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phitt[15] =  v*(v*4.0-1.0)*3.2E1;
phitt[16] =  0.0;
phitt[17] =  0.0;
phitt[18] =  s*(s*4.0-1.0)*3.2E1;
phitt[19] =  s*(t*1.6E1-6.0)*(3.2E1/3.0)+s*t*(2.56E2/3.0);
phitt[20] =  0.0;
phitt[21] =  0.0;
phitt[22] =  0.0;
phitt[23] =  0.0;
phitt[24] =  0.0;
phitt[25] =  0.0;
phitt[26] =  u*(t*1.6E1-6.0)*(3.2E1/3.0)+t*u*(2.56E2/3.0);
phitt[27] =  u*(u*4.0-1.0)*3.2E1;
phitt[28] =  0.0;
phitt[29] =  v*(t*1.6E1-6.0)*(3.2E1/3.0)+t*v*(2.56E2/3.0);
phitt[30] =  v*(v*4.0-1.0)*3.2E1;
phitt[31] =  0.0;
phitt[32] =  0.0;
phitt[33] =  0.0;
phitt[34] =  0.0;
phitt[35] =  s*t*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phitt[36] =  s*(s*4.0-1.0)*-6.4E1;
phitt[37] =  s*(s+t+u+v-1.0)*-2.56E2-s*(t*4.0-1.0)*6.4E1-s*t*2.56E2;
phitt[38] =  s*u*2.56E2;
phitt[39] =  0.0;
phitt[40] =  0.0;
phitt[41] =  s*v*2.56E2;
phitt[42] =  0.0;
phitt[43] =  0.0;
phitt[44] =  t*u*2.56E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phitt[45] =  u*(s+t+u+v-1.0)*-2.56E2-u*(t*4.0-1.0)*6.4E1-t*u*2.56E2;
phitt[46] =  u*(u*4.0-1.0)*-6.4E1;
phitt[47] =  t*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phitt[48] =  v*(s+t+u+v-1.0)*-2.56E2-v*(t*4.0-1.0)*6.4E1-t*v*2.56E2;
phitt[49] =  v*(v*4.0-1.0)*-6.4E1;
phitt[50] =  u*v*2.56E2;
phitt[51] =  0.0;
phitt[52] =  0.0;
phitt[53] =  0.0;
phitt[54] =  s*u*2.56E2;
phitt[55] =  0.0;
phitt[56] =  0.0;
phitt[57] =  s*v*2.56E2;
phitt[58] =  0.0;
phitt[59] =  0.0;
phitt[60] =  0.0;
phitt[61] =  0.0;
phitt[62] =  u*v*2.56E2;
phitt[63] =  0.0;
phitt[64] =  0.0;
phitt[65] =  0.0;
phitt[66] =  u*v*-5.12E2;
phitt[67] =  0.0;
phitt[68] =  s*v*-5.12E2;
phitt[69] =  s*u*-5.12E2;

  // phisu
phisu[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phisu[1] =  0.0;
phisu[2] =  0.0;
phisu[3] =  0.0;
phisu[4] =  0.0;
phisu[5] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phisu[6] =  s*(s*4.0-1.0)*3.2E1+(s*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+s*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phisu[7] =  s*3.2E1-s*(s*1.6E1-6.0)*(1.6E1/3.0)-(s*s)*(1.28E2/3.0)-1.6E1/3.0;
phisu[8] =  t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phisu[9] =  t*(t*4.0-1.0)*3.2E1;
phisu[10] =  0.0;
phisu[11] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phisu[12] =  u*(u*4.0-1.0)*3.2E1+(u*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+u*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phisu[13] =  u*3.2E1-u*(u*1.6E1-6.0)*(1.6E1/3.0)-(u*u)*(1.28E2/3.0)-1.6E1/3.0;
phisu[14] =  v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phisu[15] =  v*(v*4.0-1.0)*3.2E1;
phisu[16] =  0.0;
phisu[17] =  0.0;
phisu[18] =  0.0;
phisu[19] =  0.0;
phisu[20] =  s*-3.2E1+s*(s*1.6E1-6.0)*(1.6E1/3.0)+(s*s)*(1.28E2/3.0)+1.6E1/3.0;
phisu[21] =  s*(u*4.0-1.0)*1.6E1+u*(s*4.0-1.0)*1.6E1+s*u*6.4E1+(s*4.0-1.0)*(u*4.0-1.0)*4.0;
phisu[22] =  u*-3.2E1+u*(u*1.6E1-6.0)*(1.6E1/3.0)+(u*u)*(1.28E2/3.0)+1.6E1/3.0;
phisu[23] =  0.0;
phisu[24] =  0.0;
phisu[25] =  0.0;
phisu[26] =  0.0;
phisu[27] =  0.0;
phisu[28] =  0.0;
phisu[29] =  0.0;
phisu[30] =  0.0;
phisu[31] =  0.0;
phisu[32] =  0.0;
phisu[33] =  0.0;
phisu[34] =  0.0;
phisu[35] =  s*t*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phisu[36] =  t*(s*4.0-1.0)*-3.2E1-s*t*1.28E2;
phisu[37] =  t*(t*4.0-1.0)*-3.2E1;
phisu[38] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+s*t*2.56E2+s*u*5.12E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+
           u*v*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*
           t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phisu[39] =  s*(s+t+u+v-1.0)*-1.28E2-s*(s*4.0-1.0)*3.2E1-u*(s*4.0-1.0)*3.2E1-s*u*1.28E2-(s*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phisu[40] =  u*(s+t+u+v-1.0)*-1.28E2-s*(u*4.0-1.0)*3.2E1-u*(u*4.0-1.0)*3.2E1-s*u*1.28E2-(u*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phisu[41] =  s*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phisu[42] =  v*(s*4.0-1.0)*-3.2E1-s*v*1.28E2;
phisu[43] =  v*(v*4.0-1.0)*-3.2E1;
phisu[44] =  t*u*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phisu[45] =  t*(t*4.0-1.0)*-3.2E1;
phisu[46] =  t*(u*4.0-1.0)*-3.2E1-t*u*1.28E2;
phisu[47] =  t*v*2.56E2;
phisu[48] =  0.0;
phisu[49] =  0.0;
phisu[50] =  u*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phisu[51] =  v*(u*4.0-1.0)*-3.2E1-u*v*1.28E2;
phisu[52] =  v*(v*4.0-1.0)*-3.2E1;
phisu[53] =  t*(s*4.0-1.0)*3.2E1+s*t*1.28E2;
phisu[54] =  t*(t*4.0-1.0)*3.2E1;
phisu[55] =  t*(u*4.0-1.0)*3.2E1+t*u*1.28E2;
phisu[56] =  0.0;
phisu[57] =  0.0;
phisu[58] =  0.0;
phisu[59] =  v*(s*4.0-1.0)*3.2E1+s*v*1.28E2;
phisu[60] =  v*(u*4.0-1.0)*3.2E1+u*v*1.28E2;
phisu[61] =  v*(v*4.0-1.0)*3.2E1;
phisu[62] =  0.0;
phisu[63] =  0.0;
phisu[64] =  0.0;
phisu[65] =  t*v*2.56E2;
phisu[66] =  t*v*-2.56E2;
phisu[67] =  v*(s+t+u+v-1.0)*-2.56E2-s*v*2.56E2-u*v*2.56E2;
phisu[68] =  t*v*-2.56E2;
phisu[69] =  t*(s+t+u+v-1.0)*-2.56E2-s*t*2.56E2-t*u*2.56E2;

  // phitu
phitu[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phitu[1] =  0.0;
phitu[2] =  0.0;
phitu[3] =  0.0;
phitu[4] =  0.0;
phitu[5] =  s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phitu[6] =  s*(s*4.0-1.0)*3.2E1;
phitu[7] =  0.0;
phitu[8] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phitu[9] =  t*(t*4.0-1.0)*3.2E1+(t*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+t*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phitu[10] =  t*3.2E1-t*(t*1.6E1-6.0)*(1.6E1/3.0)-(t*t)*(1.28E2/3.0)-1.6E1/3.0;
phitu[11] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phitu[12] =  u*(u*4.0-1.0)*3.2E1+(u*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+u*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phitu[13] =  u*3.2E1-u*(u*1.6E1-6.0)*(1.6E1/3.0)-(u*u)*(1.28E2/3.0)-1.6E1/3.0;
phitu[14] =  v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phitu[15] =  v*(v*4.0-1.0)*3.2E1;
phitu[16] =  0.0;
phitu[17] =  0.0;
phitu[18] =  0.0;
phitu[19] =  0.0;
phitu[20] =  0.0;
phitu[21] =  0.0;
phitu[22] =  0.0;
phitu[23] =  0.0;
phitu[24] =  0.0;
phitu[25] =  0.0;
phitu[26] =  t*-3.2E1+t*(t*1.6E1-6.0)*(1.6E1/3.0)+(t*t)*(1.28E2/3.0)+1.6E1/3.0;
phitu[27] =  t*(u*4.0-1.0)*1.6E1+u*(t*4.0-1.0)*1.6E1+t*u*6.4E1+(t*4.0-1.0)*(u*4.0-1.0)*4.0;
phitu[28] =  u*-3.2E1+u*(u*1.6E1-6.0)*(1.6E1/3.0)+(u*u)*(1.28E2/3.0)+1.6E1/3.0;
phitu[29] =  0.0;
phitu[30] =  0.0;
phitu[31] =  0.0;
phitu[32] =  0.0;
phitu[33] =  0.0;
phitu[34] =  0.0;
phitu[35] =  s*t*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phitu[36] =  s*(s*4.0-1.0)*-3.2E1;
phitu[37] =  s*(t*4.0-1.0)*-3.2E1-s*t*1.28E2;
phitu[38] =  s*u*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phitu[39] =  s*(s*4.0-1.0)*-3.2E1;
phitu[40] =  s*(u*4.0-1.0)*-3.2E1-s*u*1.28E2;
phitu[41] =  s*v*2.56E2;
phitu[42] =  0.0;
phitu[43] =  0.0;
phitu[44] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*5.12E2+t*v*2.56E2+
           u*v*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*
           t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phitu[45] =  t*(s+t+u+v-1.0)*-1.28E2-t*(t*4.0-1.0)*3.2E1-u*(t*4.0-1.0)*3.2E1-t*u*1.28E2-(t*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phitu[46] =  u*(s+t+u+v-1.0)*-1.28E2-t*(u*4.0-1.0)*3.2E1-u*(u*4.0-1.0)*3.2E1-t*u*1.28E2-(u*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phitu[47] =  t*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phitu[48] =  v*(t*4.0-1.0)*-3.2E1-t*v*1.28E2;
phitu[49] =  v*(v*4.0-1.0)*-3.2E1;
phitu[50] =  u*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phitu[51] =  v*(u*4.0-1.0)*-3.2E1-u*v*1.28E2;
phitu[52] =  v*(v*4.0-1.0)*-3.2E1;
phitu[53] =  s*(s*4.0-1.0)*3.2E1;
phitu[54] =  s*(t*4.0-1.0)*3.2E1+s*t*1.28E2;
phitu[55] =  s*(u*4.0-1.0)*3.2E1+s*u*1.28E2;
phitu[56] =  0.0;
phitu[57] =  0.0;
phitu[58] =  0.0;
phitu[59] =  0.0;
phitu[60] =  0.0;
phitu[61] =  0.0;
phitu[62] =  v*(t*4.0-1.0)*3.2E1+t*v*1.28E2;
phitu[63] =  v*(u*4.0-1.0)*3.2E1+u*v*1.28E2;
phitu[64] =  v*(v*4.0-1.0)*3.2E1;
phitu[65] =  s*v*2.56E2;
phitu[66] =  v*(s+t+u+v-1.0)*-2.56E2-t*v*2.56E2-u*v*2.56E2;
phitu[67] =  s*v*-2.56E2;
phitu[68] =  s*v*-2.56E2;
phitu[69] =  s*(s+t+u+v-1.0)*-2.56E2-s*t*2.56E2-s*u*2.56E2;

  // phiuu
phiuu[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phiuu[1] =  0.0;
phiuu[2] =  0.0;
phiuu[3] =  u*-6.4E1+(u*(u*1.92E2-9.6E1))/3.0+(u*u)*6.4E1+4.4E1/3.0;
phiuu[4] =  0.0;
phiuu[5] =  s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phiuu[6] =  s*(s*4.0-1.0)*3.2E1;
phiuu[7] =  0.0;
phiuu[8] =  t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phiuu[9] =  t*(t*4.0-1.0)*3.2E1;
phiuu[10] =  0.0;
phiuu[11] =  s*3.84E2+t*3.84E2+u*3.84E2+v*3.84E2-s*t*5.12E2-s*u*5.12E2-s*v*5.12E2-t*u*5.12E2-t*v*5.12E2-
           u*v*5.12E2-u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*2.56E2-(t*t)*2.56E2-(u*u)*2.56E2-
           (v*v)*2.56E2-4.16E2/3.0;
phiuu[12] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+u*(u*4.0-1.0)*3.2E1+(u*4.0-1.0)*(s*8.0+t*8.0+u*8.0+
           v*8.0-7.0)*8.0+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*v*2.56E2+u*(s*8.0+t*8.0+u*8.0+
           v*8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phiuu[13] =  u*6.4E1-u*(s+t+u+v-1.0)*(2.56E2/3.0)-u*(u*1.6E1-6.0)*(3.2E1/3.0)-(u*1.6E1-6.0)*(s+t+u+v-
           1.0)*(3.2E1/3.0)-(u*u)*(2.56E2/3.0)-3.2E1/3.0;
phiuu[14] =  v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phiuu[15] =  v*(v*4.0-1.0)*3.2E1;
phiuu[16] =  0.0;
phiuu[17] =  0.0;
phiuu[18] =  0.0;
phiuu[19] =  0.0;
phiuu[20] =  0.0;
phiuu[21] =  s*(s*4.0-1.0)*3.2E1;
phiuu[22] =  s*(u*1.6E1-6.0)*(3.2E1/3.0)+s*u*(2.56E2/3.0);
phiuu[23] =  0.0;
phiuu[24] =  0.0;
phiuu[25] =  0.0;
phiuu[26] =  0.0;
phiuu[27] =  t*(t*4.0-1.0)*3.2E1;
phiuu[28] =  t*(u*1.6E1-6.0)*(3.2E1/3.0)+t*u*(2.56E2/3.0);
phiuu[29] =  0.0;
phiuu[30] =  0.0;
phiuu[31] =  0.0;
phiuu[32] =  v*(u*1.6E1-6.0)*(3.2E1/3.0)+u*v*(2.56E2/3.0);
phiuu[33] =  v*(v*4.0-1.0)*3.2E1;
phiuu[34] =  0.0;
phiuu[35] =  s*t*2.56E2;
phiuu[36] =  0.0;
phiuu[37] =  0.0;
phiuu[38] =  s*u*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phiuu[39] =  s*(s*4.0-1.0)*-6.4E1;
phiuu[40] =  s*(s+t+u+v-1.0)*-2.56E2-s*(u*4.0-1.0)*6.4E1-s*u*2.56E2;
phiuu[41] =  s*v*2.56E2;
phiuu[42] =  0.0;
phiuu[43] =  0.0;
phiuu[44] =  t*u*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phiuu[45] =  t*(t*4.0-1.0)*-6.4E1;
phiuu[46] =  t*(s+t+u+v-1.0)*-2.56E2-t*(u*4.0-1.0)*6.4E1-t*u*2.56E2;
phiuu[47] =  t*v*2.56E2;
phiuu[48] =  0.0;
phiuu[49] =  0.0;
phiuu[50] =  u*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phiuu[51] =  v*(s+t+u+v-1.0)*-2.56E2-v*(u*4.0-1.0)*6.4E1-u*v*2.56E2;
phiuu[52] =  v*(v*4.0-1.0)*-6.4E1;
phiuu[53] =  0.0;
phiuu[54] =  0.0;
phiuu[55] =  s*t*2.56E2;
phiuu[56] =  0.0;
phiuu[57] =  0.0;
phiuu[58] =  0.0;
phiuu[59] =  0.0;
phiuu[60] =  s*v*2.56E2;
phiuu[61] =  0.0;
phiuu[62] =  0.0;
phiuu[63] =  t*v*2.56E2;
phiuu[64] =  0.0;
phiuu[65] =  0.0;
phiuu[66] =  t*v*-5.12E2;
phiuu[67] =  s*v*-5.12E2;
phiuu[68] =  0.0;
phiuu[69] =  s*t*-5.12E2;

  // phisv
phisv[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phisv[1] =  0.0;
phisv[2] =  0.0;
phisv[3] =  0.0;
phisv[4] =  0.0;
phisv[5] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phisv[6] =  s*(s*4.0-1.0)*3.2E1+(s*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+s*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phisv[7] =  s*3.2E1-s*(s*1.6E1-6.0)*(1.6E1/3.0)-(s*s)*(1.28E2/3.0)-1.6E1/3.0;
phisv[8] =  t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phisv[9] =  t*(t*4.0-1.0)*3.2E1;
phisv[10] =  0.0;
phisv[11] =  u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phisv[12] =  u*(u*4.0-1.0)*3.2E1;
phisv[13] =  0.0;
phisv[14] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phisv[15] =  v*(v*4.0-1.0)*3.2E1+(v*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+v*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phisv[16] =  v*3.2E1-v*(v*1.6E1-6.0)*(1.6E1/3.0)-(v*v)*(1.28E2/3.0)-1.6E1/3.0;
phisv[17] =  0.0;
phisv[18] =  0.0;
phisv[19] =  0.0;
phisv[20] =  0.0;
phisv[21] =  0.0;
phisv[22] =  0.0;
phisv[23] =  s*-3.2E1+s*(s*1.6E1-6.0)*(1.6E1/3.0)+(s*s)*(1.28E2/3.0)+1.6E1/3.0;
phisv[24] =  s*(v*4.0-1.0)*1.6E1+v*(s*4.0-1.0)*1.6E1+s*v*6.4E1+(s*4.0-1.0)*(v*4.0-1.0)*4.0;
phisv[25] =  v*-3.2E1+v*(v*1.6E1-6.0)*(1.6E1/3.0)+(v*v)*(1.28E2/3.0)+1.6E1/3.0;
phisv[26] =  0.0;
phisv[27] =  0.0;
phisv[28] =  0.0;
phisv[29] =  0.0;
phisv[30] =  0.0;
phisv[31] =  0.0;
phisv[32] =  0.0;
phisv[33] =  0.0;
phisv[34] =  0.0;
phisv[35] =  s*t*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phisv[36] =  t*(s*4.0-1.0)*-3.2E1-s*t*1.28E2;
phisv[37] =  t*(t*4.0-1.0)*-3.2E1;
phisv[38] =  s*u*2.56E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phisv[39] =  u*(s*4.0-1.0)*-3.2E1-s*u*1.28E2;
phisv[40] =  u*(u*4.0-1.0)*-3.2E1;
phisv[41] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+s*t*2.56E2+s*u*2.56E2+s*v*5.12E2+t*u*2.56E2+t*v*2.56E2+
           u*v*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*
           t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phisv[42] =  s*(s+t+u+v-1.0)*-1.28E2-s*(s*4.0-1.0)*3.2E1-v*(s*4.0-1.0)*3.2E1-s*v*1.28E2-(s*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phisv[43] =  v*(s+t+u+v-1.0)*-1.28E2-s*(v*4.0-1.0)*3.2E1-v*(v*4.0-1.0)*3.2E1-s*v*1.28E2-(v*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phisv[44] =  t*u*2.56E2;
phisv[45] =  0.0;
phisv[46] =  0.0;
phisv[47] =  t*v*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phisv[48] =  t*(t*4.0-1.0)*-3.2E1;
phisv[49] =  t*(v*4.0-1.0)*-3.2E1-t*v*1.28E2;
phisv[50] =  u*v*2.56E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phisv[51] =  u*(u*4.0-1.0)*-3.2E1;
phisv[52] =  u*(v*4.0-1.0)*-3.2E1-u*v*1.28E2;
phisv[53] =  0.0;
phisv[54] =  0.0;
phisv[55] =  0.0;
phisv[56] =  t*(s*4.0-1.0)*3.2E1+s*t*1.28E2;
phisv[57] =  t*(t*4.0-1.0)*3.2E1;
phisv[58] =  t*(v*4.0-1.0)*3.2E1+t*v*1.28E2;
phisv[59] =  u*(s*4.0-1.0)*3.2E1+s*u*1.28E2;
phisv[60] =  u*(u*4.0-1.0)*3.2E1;
phisv[61] =  u*(v*4.0-1.0)*3.2E1+u*v*1.28E2;
phisv[62] =  0.0;
phisv[63] =  0.0;
phisv[64] =  0.0;
phisv[65] =  t*u*2.56E2;
phisv[66] =  t*u*-2.56E2;
phisv[67] =  u*(s+t+u+v-1.0)*-2.56E2-s*u*2.56E2-u*v*2.56E2;
phisv[68] =  t*(s+t+u+v-1.0)*-2.56E2-s*t*2.56E2-t*v*2.56E2;
phisv[69] =  t*u*-2.56E2;

  // phitv
phitv[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phitv[1] =  0.0;
phitv[2] =  0.0;
phitv[3] =  0.0;
phitv[4] =  0.0;
phitv[5] =  s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phitv[6] =  s*(s*4.0-1.0)*3.2E1;
phitv[7] =  0.0;
phitv[8] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phitv[9] =  t*(t*4.0-1.0)*3.2E1+(t*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+t*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phitv[10] =  t*3.2E1-t*(t*1.6E1-6.0)*(1.6E1/3.0)-(t*t)*(1.28E2/3.0)-1.6E1/3.0;
phitv[11] =  u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phitv[12] =  u*(u*4.0-1.0)*3.2E1;
phitv[13] =  0.0;
phitv[14] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phitv[15] =  v*(v*4.0-1.0)*3.2E1+(v*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+v*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phitv[16] =  v*3.2E1-v*(v*1.6E1-6.0)*(1.6E1/3.0)-(v*v)*(1.28E2/3.0)-1.6E1/3.0;
phitv[17] =  0.0;
phitv[18] =  0.0;
phitv[19] =  0.0;
phitv[20] =  0.0;
phitv[21] =  0.0;
phitv[22] =  0.0;
phitv[23] =  0.0;
phitv[24] =  0.0;
phitv[25] =  0.0;
phitv[26] =  0.0;
phitv[27] =  0.0;
phitv[28] =  0.0;
phitv[29] =  t*-3.2E1+t*(t*1.6E1-6.0)*(1.6E1/3.0)+(t*t)*(1.28E2/3.0)+1.6E1/3.0;
phitv[30] =  t*(v*4.0-1.0)*1.6E1+v*(t*4.0-1.0)*1.6E1+t*v*6.4E1+(t*4.0-1.0)*(v*4.0-1.0)*4.0;
phitv[31] =  v*-3.2E1+v*(v*1.6E1-6.0)*(1.6E1/3.0)+(v*v)*(1.28E2/3.0)+1.6E1/3.0;
phitv[32] =  0.0;
phitv[33] =  0.0;
phitv[34] =  0.0;
phitv[35] =  s*t*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phitv[36] =  s*(s*4.0-1.0)*-3.2E1;
phitv[37] =  s*(t*4.0-1.0)*-3.2E1-s*t*1.28E2;
phitv[38] =  s*u*2.56E2;
phitv[39] =  0.0;
phitv[40] =  0.0;
phitv[41] =  s*v*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phitv[42] =  s*(s*4.0-1.0)*-3.2E1;
phitv[43] =  s*(v*4.0-1.0)*-3.2E1-s*v*1.28E2;
phitv[44] =  t*u*2.56E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phitv[45] =  u*(t*4.0-1.0)*-3.2E1-t*u*1.28E2;
phitv[46] =  u*(u*4.0-1.0)*-3.2E1;
phitv[47] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*5.12E2+
           u*v*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*
           t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phitv[48] =  t*(s+t+u+v-1.0)*-1.28E2-t*(t*4.0-1.0)*3.2E1-v*(t*4.0-1.0)*3.2E1-t*v*1.28E2-(t*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phitv[49] =  v*(s+t+u+v-1.0)*-1.28E2-t*(v*4.0-1.0)*3.2E1-v*(v*4.0-1.0)*3.2E1-t*v*1.28E2-(v*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phitv[50] =  u*v*2.56E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phitv[51] =  u*(u*4.0-1.0)*-3.2E1;
phitv[52] =  u*(v*4.0-1.0)*-3.2E1-u*v*1.28E2;
phitv[53] =  0.0;
phitv[54] =  0.0;
phitv[55] =  0.0;
phitv[56] =  s*(s*4.0-1.0)*3.2E1;
phitv[57] =  s*(t*4.0-1.0)*3.2E1+s*t*1.28E2;
phitv[58] =  s*(v*4.0-1.0)*3.2E1+s*v*1.28E2;
phitv[59] =  0.0;
phitv[60] =  0.0;
phitv[61] =  0.0;
phitv[62] =  u*(t*4.0-1.0)*3.2E1+t*u*1.28E2;
phitv[63] =  u*(u*4.0-1.0)*3.2E1;
phitv[64] =  u*(v*4.0-1.0)*3.2E1+u*v*1.28E2;
phitv[65] =  s*u*2.56E2;
phitv[66] =  u*(s+t+u+v-1.0)*-2.56E2-t*u*2.56E2-u*v*2.56E2;
phitv[67] =  s*u*-2.56E2;
phitv[68] =  s*(s+t+u+v-1.0)*-2.56E2-s*t*2.56E2-s*v*2.56E2;
phitv[69] =  s*u*-2.56E2;

  // phiuv
phiuv[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phiuv[1] =  0.0;
phiuv[2] =  0.0;
phiuv[3] =  0.0;
phiuv[4] =  0.0;
phiuv[5] =  s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phiuv[6] =  s*(s*4.0-1.0)*3.2E1;
phiuv[7] =  0.0;
phiuv[8] =  t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phiuv[9] =  t*(t*4.0-1.0)*3.2E1;
phiuv[10] =  0.0;
phiuv[11] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phiuv[12] =  u*(u*4.0-1.0)*3.2E1+(u*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+u*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phiuv[13] =  u*3.2E1-u*(u*1.6E1-6.0)*(1.6E1/3.0)-(u*u)*(1.28E2/3.0)-1.6E1/3.0;
phiuv[14] =  s*1.92E2+t*1.92E2+u*1.92E2+v*1.92E2-s*t*2.56E2-s*u*2.56E2-s*v*2.56E2-t*u*2.56E2-t*v*2.56E2-
           u*v*2.56E2-v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*1.28E2-(t*t)*1.28E2-(u*u)*1.28E2-
           (v*v)*1.28E2-2.08E2/3.0;
phiuv[15] =  v*(v*4.0-1.0)*3.2E1+(v*4.0-1.0)*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*4.0+v*(s*8.0+t*8.0+u*8.0+v*
           8.0-7.0)*1.6E1;
phiuv[16] =  v*3.2E1-v*(v*1.6E1-6.0)*(1.6E1/3.0)-(v*v)*(1.28E2/3.0)-1.6E1/3.0;
phiuv[17] =  0.0;
phiuv[18] =  0.0;
phiuv[19] =  0.0;
phiuv[20] =  0.0;
phiuv[21] =  0.0;
phiuv[22] =  0.0;
phiuv[23] =  0.0;
phiuv[24] =  0.0;
phiuv[25] =  0.0;
phiuv[26] =  0.0;
phiuv[27] =  0.0;
phiuv[28] =  0.0;
phiuv[29] =  0.0;
phiuv[30] =  0.0;
phiuv[31] =  0.0;
phiuv[32] =  u*-3.2E1+u*(u*1.6E1-6.0)*(1.6E1/3.0)+(u*u)*(1.28E2/3.0)+1.6E1/3.0;
phiuv[33] =  u*(v*4.0-1.0)*1.6E1+v*(u*4.0-1.0)*1.6E1+u*v*6.4E1+(u*4.0-1.0)*(v*4.0-1.0)*4.0;
phiuv[34] =  v*-3.2E1+v*(v*1.6E1-6.0)*(1.6E1/3.0)+(v*v)*(1.28E2/3.0)+1.6E1/3.0;
phiuv[35] =  s*t*2.56E2;
phiuv[36] =  0.0;
phiuv[37] =  0.0;
phiuv[38] =  s*u*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiuv[39] =  s*(s*4.0-1.0)*-3.2E1;
phiuv[40] =  s*(u*4.0-1.0)*-3.2E1-s*u*1.28E2;
phiuv[41] =  s*v*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiuv[42] =  s*(s*4.0-1.0)*-3.2E1;
phiuv[43] =  s*(v*4.0-1.0)*-3.2E1-s*v*1.28E2;
phiuv[44] =  t*u*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiuv[45] =  t*(t*4.0-1.0)*-3.2E1;
phiuv[46] =  t*(u*4.0-1.0)*-3.2E1-t*u*1.28E2;
phiuv[47] =  t*v*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1;
phiuv[48] =  t*(t*4.0-1.0)*-3.2E1;
phiuv[49] =  t*(v*4.0-1.0)*-3.2E1-t*v*1.28E2;
phiuv[50] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+
           u*v*5.12E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+v*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*
           t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phiuv[51] =  u*(s+t+u+v-1.0)*-1.28E2-u*(u*4.0-1.0)*3.2E1-v*(u*4.0-1.0)*3.2E1-u*v*1.28E2-(u*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phiuv[52] =  v*(s+t+u+v-1.0)*-1.28E2-u*(v*4.0-1.0)*3.2E1-v*(v*4.0-1.0)*3.2E1-u*v*1.28E2-(v*4.0-1.0)*(s+
           t+u+v-1.0)*3.2E1;
phiuv[53] =  0.0;
phiuv[54] =  0.0;
phiuv[55] =  0.0;
phiuv[56] =  0.0;
phiuv[57] =  0.0;
phiuv[58] =  0.0;
phiuv[59] =  s*(s*4.0-1.0)*3.2E1;
phiuv[60] =  s*(u*4.0-1.0)*3.2E1+s*u*1.28E2;
phiuv[61] =  s*(v*4.0-1.0)*3.2E1+s*v*1.28E2;
phiuv[62] =  t*(t*4.0-1.0)*3.2E1;
phiuv[63] =  t*(u*4.0-1.0)*3.2E1+t*u*1.28E2;
phiuv[64] =  t*(v*4.0-1.0)*3.2E1+t*v*1.28E2;
phiuv[65] =  s*t*2.56E2;
phiuv[66] =  t*(s+t+u+v-1.0)*-2.56E2-t*u*2.56E2-t*v*2.56E2;
phiuv[67] =  s*(s+t+u+v-1.0)*-2.56E2-s*u*2.56E2-s*v*2.56E2;
phiuv[68] =  s*t*-2.56E2;
phiuv[69] =  s*t*-2.56E2;

  // phivv
phivv[0] =  s*-1.6E2-t*1.6E2-u*1.6E2-v*1.6E2+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*
           v*2.56E2+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+1.4E2/3.0;
phivv[1] =  0.0;
phivv[2] =  0.0;
phivv[3] =  0.0;
phivv[4] =  v*-6.4E1+(v*(v*1.92E2-9.6E1))/3.0+(v*v)*6.4E1+4.4E1/3.0;
phivv[5] =  s*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phivv[6] =  s*(s*4.0-1.0)*3.2E1;
phivv[7] =  0.0;
phivv[8] =  t*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phivv[9] =  t*(t*4.0-1.0)*3.2E1;
phivv[10] =  0.0;
phivv[11] =  u*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(-1.6E1/3.0);
phivv[12] =  u*(u*4.0-1.0)*3.2E1;
phivv[13] =  0.0;
phivv[14] =  s*3.84E2+t*3.84E2+u*3.84E2+v*3.84E2-s*t*5.12E2-s*u*5.12E2-s*v*5.12E2-t*u*5.12E2-t*v*5.12E2-
           u*v*5.12E2-v*(s*4.8E1+t*4.8E1+u*4.8E1+v*4.8E1-3.6E1)*(1.6E1/3.0)-(s*s)*2.56E2-(t*t)*2.56E2-(u*u)*2.56E2-
           (v*v)*2.56E2-4.16E2/3.0;
phivv[15] =  s*-2.24E2-t*2.24E2-u*2.24E2-v*2.24E2+v*(v*4.0-1.0)*3.2E1+(v*4.0-1.0)*(s*8.0+t*8.0+u*8.0+
           v*8.0-7.0)*8.0+s*t*2.56E2+s*u*2.56E2+s*v*2.56E2+t*u*2.56E2+t*v*2.56E2+u*v*2.56E2+v*(s*8.0+t*8.0+u*8.0+
           v*8.0-7.0)*3.2E1+(s*s)*1.28E2+(t*t)*1.28E2+(u*u)*1.28E2+(v*v)*1.28E2+9.6E1;
phivv[16] =  v*6.4E1-v*(s+t+u+v-1.0)*(2.56E2/3.0)-v*(v*1.6E1-6.0)*(3.2E1/3.0)-(v*1.6E1-6.0)*(s+t+u+v-
           1.0)*(3.2E1/3.0)-(v*v)*(2.56E2/3.0)-3.2E1/3.0;
phivv[17] =  0.0;
phivv[18] =  0.0;
phivv[19] =  0.0;
phivv[20] =  0.0;
phivv[21] =  0.0;
phivv[22] =  0.0;
phivv[23] =  0.0;
phivv[24] =  s*(s*4.0-1.0)*3.2E1;
phivv[25] =  s*(v*1.6E1-6.0)*(3.2E1/3.0)+s*v*(2.56E2/3.0);
phivv[26] =  0.0;
phivv[27] =  0.0;
phivv[28] =  0.0;
phivv[29] =  0.0;
phivv[30] =  t*(t*4.0-1.0)*3.2E1;
phivv[31] =  t*(v*1.6E1-6.0)*(3.2E1/3.0)+t*v*(2.56E2/3.0);
phivv[32] =  0.0;
phivv[33] =  u*(u*4.0-1.0)*3.2E1;
phivv[34] =  u*(v*1.6E1-6.0)*(3.2E1/3.0)+u*v*(2.56E2/3.0);
phivv[35] =  s*t*2.56E2;
phivv[36] =  0.0;
phivv[37] =  0.0;
phivv[38] =  s*u*2.56E2;
phivv[39] =  0.0;
phivv[40] =  0.0;
phivv[41] =  s*v*2.56E2+s*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phivv[42] =  s*(s*4.0-1.0)*-6.4E1;
phivv[43] =  s*(s+t+u+v-1.0)*-2.56E2-s*(v*4.0-1.0)*6.4E1-s*v*2.56E2;
phivv[44] =  t*u*2.56E2;
phivv[45] =  0.0;
phivv[46] =  0.0;
phivv[47] =  t*v*2.56E2+t*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phivv[48] =  t*(t*4.0-1.0)*-6.4E1;
phivv[49] =  t*(s+t+u+v-1.0)*-2.56E2-t*(v*4.0-1.0)*6.4E1-t*v*2.56E2;
phivv[50] =  u*v*2.56E2+u*(s*8.0+t*8.0+u*8.0+v*8.0-7.0)*6.4E1;
phivv[51] =  u*(u*4.0-1.0)*-6.4E1;
phivv[52] =  u*(s+t+u+v-1.0)*-2.56E2-u*(v*4.0-1.0)*6.4E1-u*v*2.56E2;
phivv[53] =  0.0;
phivv[54] =  0.0;
phivv[55] =  0.0;
phivv[56] =  0.0;
phivv[57] =  0.0;
phivv[58] =  s*t*2.56E2;
phivv[59] =  0.0;
phivv[60] =  0.0;
phivv[61] =  s*u*2.56E2;
phivv[62] =  0.0;
phivv[63] =  0.0;
phivv[64] =  t*u*2.56E2;
phivv[65] =  0.0;
phivv[66] =  t*u*-5.12E2;
phivv[67] =  s*u*-5.12E2;
phivv[68] =  s*t*-5.12E2;
phivv[69] =  0.0;

}


void
BasisFunctionSpacetime<Pentatope,Lagrange,4>::coordinates(std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u, std::vector<Real>& v) const
{
  s = coords_s_;
  t = coords_t_;
  u = coords_u_;
  v = coords_v_;
}

