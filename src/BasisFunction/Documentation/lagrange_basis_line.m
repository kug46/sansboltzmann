function lagrange_basis_line()
clc;
s = sym('s','real');

fid1 = fopen('BasisFunctionLine_Lagrange_autogen.h','w');
fid2 = fopen('BasisFunctionLine_Lagrange_known.h','w');

ONETHIRD = sym('ONETHIRD');
ONETWENTYFOURTH = sym('ONETWENTYFOURTH');

% p1
phi = setup_phi(1);
x = s;
phi(1) = 1.0 - x;
phi(2) = x;
[phis,phiss] = get_derivs(phi,s);
print_c(fid1,1,phi,phis,phiss);
print_c_unit(fid2,'lagrange_p1_test1',.25,phi,phis,phiss,s);

% p2
x2=x*x;
phi(1) = 1.0 - 3.0*x + 2.0*x2;
phi(2) =       4.0*x - 4.0*x2;
phi(3) =     - 1.0*x + 2.0*x2;

% re-orient from PX to SANS
tmp = phi(2);
phi(2) = phi(3);
phi(3) = tmp;
[phis,phiss] = get_derivs(phi,s);
print_c(fid1,2,phi,phis,phiss);
print_c_unit(fid2,'lagrange_p2_test1',.25,phi,phis,phiss,s);

% p3
x3=x2*x;
phi(1) =  1.0 - 5.5*x +  9.0*x2 -  4.5*x3;
phi(2) =        9.0*x - 22.5*x2 + 13.5*x3;
phi(3) =      - 4.5*x + 18.0*x2 - 13.5*x3;
phi(4) =        1.0*x -  4.5*x2 +  4.5*x3;

% re-orient from PX to SANS
tmp = phi(2:3);
phi(2) = phi(4);
phi(3:4) = tmp;
[phis,phiss] = get_derivs(phi,s);
print_c(fid1,3,phi,phis,phiss);
print_c_unit(fid2,'lagrange_p3_test1',.25,phi,phis,phiss,s);

% p4
x4=x3*x;
phi(1) = ( 3.0 - 25.0*x +  70.0*x2 -  80.0*x3 +  32.0*x4 )*ONETHIRD;
phi(2) = (       48.0*x - 208.0*x2 + 288.0*x3 - 128.0*x4 )*ONETHIRD;
phi(3) = (     - 36.0*x + 228.0*x2 - 384.0*x3 + 192.0*x4 )*ONETHIRD;
phi(4) = (       16.0*x - 112.0*x2 + 224.0*x3 - 128.0*x4 )*ONETHIRD;
phi(5) = (     -  3.0*x +  22.0*x2 -  48.0*x3 +  32.0*x4 )*ONETHIRD;

% re-orient from PX to SANS
tmp = phi(2:4);
phi(2) = phi(5);
phi(3:5) = tmp;
[phis,phiss] = get_derivs(phi,s);
print_c(fid1,4,phi,phis,phiss);
print_c_unit(fid2,'lagrange_p4_test1',.25,phi,phis,phiss,s);

% p5
x5=x4*x;
phi(1) = ( 24.0 - 274.0*x + 1125.0*x2 -  2125.0*x3 +  1875.0*x4 -  625.0*x5 ) * ONETWENTYFOURTH;
phi(2) = (        600.0*x - 3850.0*x2 +  8875.0*x3 -  8750.0*x4 + 3125.0*x5 ) * ONETWENTYFOURTH;
phi(3) = (      - 600.0*x + 5350.0*x2 - 14750.0*x3 + 16250.0*x4 - 6250.0*x5 ) * ONETWENTYFOURTH;
phi(4) = (        400.0*x - 3900.0*x2 + 12250.0*x3 - 15000.0*x4 + 6250.0*x5 ) * ONETWENTYFOURTH;
phi(5) = (      - 150.0*x + 1525.0*x2 -  5125.0*x3 +  6875.0*x4 - 3125.0*x5 ) * ONETWENTYFOURTH;
phi(6) = (         24.0*x -  250.0*x2 +   875.0*x3 -  1250.0*x4 +  625.0*x5 ) * ONETWENTYFOURTH;
% re-orient from PX to SANS
tmp = phi(2:5);
phi(2) = phi(6);
phi(3:6) = tmp;
[phis,phiss] = get_derivs(phi,s);
print_c(fid1,5,phi,phis,phiss);
print_c_unit(fid2,'lagrange_p5_test1',.25,phi,phis,phiss,s);

fclose(fid1);
fclose(fid2);

end

function phi = setup_phi(p)
np  = (p +1);
phi = sym('phi',[np,1]); 
end

function [phis,phiss] = get_derivs(phi,s)
phis = diff(phi,s);
phiss = diff(phis,s);
end

function print_header(fid,voltype,basistype,p,nderiv)
if nderiv==0
    func_name = 'evalBasis';
    args_name = 'Real phi[]';
elseif nderiv==1
    func_name = 'evalBasisDerivative';
    args_name = 'Real phis[]';
elseif nderiv==2
    func_name = 'evalBasisHessianDerivative';
    args_name = 'Real phiss[]';
end
fprintf(fid,'\n\nvoid\nBasisFunction%s<%s,%d>::%s(\n',voltype,basistype,p,func_name);
fprintf(fid,'const Real s, %s, int nphi) const\n',args_name);
fprintf(fid,'{\nSANS_ASSERT(nphi==%d);\n',p+1);
end

function print_closing(fid)
fprintf(fid,'}\n\n');
end

function print_c(fid,p,phi,phis,phiss)

print_header(fid,'Line','Lagrange',p,0)
print_ccode(fid,'phi',phi);
print_closing(fid);

print_header(fid,'Line','Lagrange',p,1)
print_ccode(fid,'phis',phis);
print_closing(fid);

print_header(fid,'Line','Lagrange',p,2)
print_ccode(fid,'phiss',phiss);
print_closing(fid);

end

function print_inline(fid,x,name)
fprintf(fid,'\n  // %s\n',name);
for i=1:length(x)
    fprintf(fid,'  %s[%d] = %1.14e;\n',name,i-1,x(i));
end
end

function result = is_operator( s )
result = false;
if strcmp(s,'-')==1
    result = true;
elseif strcmp(s,'+')==1
    result = true;
elseif strcmp(s,'*')==1
    result = true;
elseif strcmp(s,'/')==1
    result = true;
end
end

function print_ccode(fid,name,expr0)
maxline = 100; % max number of characters per line so vera doesn't complain
fprintf(fid,'\n  // %s\n',name);
for i=1:length(expr0)
    expr = strsplit(ccode(expr0(i)),'=');
    expr = expr{2};
    % the full expression
    expr = sprintf('%s[%d] = %s\n',name,i-1,expr);
    %disp(expr)
    n = length(expr);
    pos = 1;
    while pos<n
        if pos+maxline>n
            limit = n;
        else
            limit = pos +maxline;
            while (true)
                if limit==n
                    break;
                end
                if is_operator( expr(limit) )
                    break;
                end
                limit = limit +1;
            end
        end
        s = expr(pos:limit);
        fprintf(fid,'%s\n',s);
        pos = limit +1;
    end
    %fprintf(fid,'  %s[%d] = %s\n',name,i-1,expr);
end
end

function print_c_unit(fid,func_name,s0,phi,phis,phiss,s)

phi = subs(phi,s,s0);
phis = subs(phis,s,s0);
phiss = subs(phiss,s,s0);

% in case we have ONETHIRD or ONESIXTH lingering..
phi = subs(subs(subs(phi,'ONETHIRD',1./3.),'ONESIXTH',1./6.),'ONETWENTYFOURTH',1./24.);
phis = subs(subs(subs(phis,'ONETHIRD',1./3.),'ONESIXTH',1./6.),'ONETWENTYFOURTH',1./24.);
phiss = subs(subs(subs(phiss,'ONETHIRD',1./3.),'ONESIXTH',1./6.),'ONETWENTYFOURTH',1./24.);

np = length(phi);
fprintf(fid,'void\n%s_true(Real phi[%d], Real phis[%d], Real phiss[%d])\n',func_name,np,np,np);
fprintf(fid,'{\n');
print_inline(fid,phi,'phi');
print_inline(fid,phis,'phis');
print_inline(fid,phiss,'phiss');
fprintf(fid,'}\n');

end