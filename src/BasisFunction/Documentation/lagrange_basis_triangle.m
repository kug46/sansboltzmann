function lagrange_basis_triangle()
clc;
s = sym('s','real');
t = sym('t','real');

fid1 = fopen('BasisFunctionArea_Triangle_Lagrange_autogen.h','w');
fid2 = fopen('BasisFunctionArea_Triangle_Lagrange_known.h','w');

% p1
phi = setup_phi(1);
phi(1) = 1 -s -t;
phi(2) = s;
phi(3) = t;

[phis,phit,phiss,phist,phitt] = get_derivs(phi,s,t);
print_c(fid1,1,phi,phis,phit,phiss,phist,phitt);

% unit test 1
s0 = .25;
t0 = .5;
print_c_unit(fid2,'lagrange_p1_test1',s0,t0,phi,phis,phit,phiss,phist,phitt,s,t);


% p2
phi = setup_phi(2);
phi(1) = 1 -3*s -3*t +2*s*s +4*s*t +2*t*t;
phi(2) = -s +2*s*s;
phi(3) = -t +2*t*t;
phi(4) = 4*s*t;
phi(5) = 4*t*(1 -s -t);
phi(6) = 4*(1 -s -t)*s;
[phis,phit,phiss,phist,phitt] = get_derivs(phi,s,t);
print_c(fid1,2,phi,phis,phit,phiss,phist,phitt);

% unit test 1
s0 = .25;
t0 = .5;
print_c_unit(fid2,'lagrange_p2_test1',s0,t0,phi,phis,phit,phiss,phist,phitt,s,t);


% p3
phi = setup_phi(3);
x = s;
y = t;
xx = s*s;
xy = s*t;
yy = t*t;
xxx = xx*s;
xxy = xx*t;
xyy = xy*t;
yyy = yy*t;
phi(1) = 1.0-5.5*x-5.5*y +9.0*xx+18.0*xy +9.0*yy -4.5*xxx-13.5*xxy-13.5*xyy -4.5*yyy;
phi(2) = x       -4.5*xx                 +4.5*xxx;
phi(3) =   y                -4.5*yy                  +4.5*yyy;
phi(4) =     -4.5*xy                 +13.5*xxy;
phi(5) =   -4.5*xy                          +13.5*xyy;
phi(6) =   -4.5*y         +4.5*xy+18.0*yy                  -13.5*xyy-13.5*yyy;
phi(7) =           9.0*y        -22.5*xy-22.5*yy         +13.5*xxy+27.0*xyy+13.5*yyy;
phi(8) =     9.0*x      -22.5*xx-22.5*xy        +13.5*xxx+27.0*xxy+13.5*xyy;
phi(9) =    -4.5*x      +18.0*xx +4.5*xy        -13.5*xxx-13.5*xxy;
phi(10) =                         27.0*xy                 -27.0*xxy-27.0*xyy;
[phis,phit,phiss,phist,phitt] = get_derivs(phi,s,t);
print_c(fid1,3,phi,phis,phit,phiss,phist,phitt);

% unit test 1
s0 = .25;
t0 = .5;
print_c_unit(fid2,'lagrange_p3_test1',s0,t0,phi,phis,phit,phiss,phist,phitt,s,t);

% p4
phi = lagrange_p4(s,t);
[phis,phit,phiss,phist,phitt] = get_derivs(phi,s,t);
print_c(fid1,4,phi,phis,phit,phiss,phist,phitt);

% unit test 1
s0 = .25;
t0 = .5;
print_c_unit(fid2,'lagrange_p4_test1',s0,t0,phi,phis,phit,phiss,phist,phitt,s,t);


% p5
phi = lagrange_p5(s,t); 
[phis,phit,phiss,phist,phitt] = get_derivs(phi,s,t);
print_c(fid1,5,phi,phis,phit,phiss,phist,phitt);

% unit test 1
s0 = .25;
t0 = .5;
print_c_unit(fid2,'lagrange_p5_test1',s0,t0,phi,phis,phit,phiss,phist,phitt,s,t);

fclose(fid1);
fclose(fid2);

end

function phi = setup_phi(p)
np  = (p +1)*(p +2)/2;
phi = sym('phi',[np,1]); 
end

function [phis,phit,phiss,phist,phitt] = get_derivs(phi,s,t)
phis = diff(phi,s);
phit = diff(phi,t);
phiss = diff(phis,s);
phist = diff(phis,t);
phitt = diff(phit,t);
end

function print_header(fid,voltype,elemtype,basistype,p,nderiv)
if nderiv==0
    func_name = 'evalBasis';
    args_name = 'Real phi[]';
elseif nderiv==1
    func_name = 'evalBasisDerivative';
    args_name = 'Real phis[], Real phit[]';
elseif nderiv==2
    func_name = 'evalBasisHessianDerivative';
    args_name = 'Real phiss[], Real phist[], Real phitt[]';
end
fprintf(fid,'\n\nvoid\nBasisFunction%s<%s,%s,%d>::%s(\n',voltype,elemtype,basistype,p,func_name);
fprintf(fid,'const Real& s, const Real& t, const Int3& sgn , %s, int nphi) const\n',args_name);
fprintf(fid,'{\nSANS_ASSERT(nphi==%d);\n',(p+1)*(p+2)/2);
end

function print_closing(fid)
fprintf(fid,'}\n\n');
end

function print_c(fid,p,phi,phis,phit,phiss,phist,phitt)

print_header(fid,'Area','Triangle','Lagrange',p,0)
print_ccode(fid,'phi',phi);
print_closing(fid);

print_header(fid,'Area','Triangle','Lagrange',p,1)
print_ccode(fid,'phis',phis);
print_ccode(fid,'phit',phit);
print_closing(fid);

print_header(fid,'Area','Triangle','Lagrange',p,2)
print_ccode(fid,'phiss',phiss);
print_ccode(fid,'phist',phist);
print_ccode(fid,'phitt',phitt);
print_closing(fid);

end

function print_inline(fid,x,name)
fprintf(fid,'\n  // %s\n',name);
for i=1:length(x)
    fprintf(fid,'  %s[%d] = %1.14e;\n',name,i-1,x(i));
end
end

function result = is_operator( s )
result = false;
if strcmp(s,'-')==1
    result = true;
elseif strcmp(s,'+')==1
    result = true;
elseif strcmp(s,'*')==1
    result = true;
elseif strcmp(s,'/')==1
    result = true;
end
end

function print_ccode(fid,name,expr0)
maxline = 100; % max number of characters per line so vera doesn't complain
fprintf(fid,'\n  // %s\n',name);
for i=1:length(expr0)
    expr = strsplit(ccode(expr0(i)),'=');
    expr = expr{2};
    % the full expression
    expr = sprintf('%s[%d] = %s\n',name,i-1,expr);
    %disp(expr)
    n = length(expr);
    pos = 1;
    while pos<n
        if pos+maxline>n
            limit = n;
        else
            limit = pos +maxline;
            while (true)
                if limit==n
                    break;
                end
                if is_operator( expr(limit) )
                    break;
                end
                limit = limit +1;
            end
        end
        s = expr(pos:limit);
        fprintf(fid,'%s\n',s);
        pos = limit +1;
    end
    %fprintf(fid,'  %s[%d] = %s\n',name,i-1,expr);
end
end

function print_c_unit(fid,func_name,s0,t0,phi,phis,phit,phiss,phist,phitt,s,t)

phi = subs(subs(phi,s,s0),t,t0);
phis = subs(subs(phis,s,s0),t,t0);
phit = subs(subs(phit,s,s0),t,t0);
phiss = subs(subs(phiss,s,s0),t,t0);
phist = subs(subs(phist,s,s0),t,t0);
phitt = subs(subs(phitt,s,s0),t,t0);

% in case we have ONETHIRD or ONESIXTH lingering..
phi = subs(subs(phi,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phis = subs(subs(phis,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phit = subs(subs(phit,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phiss = subs(subs(phiss,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phist = subs(subs(phist,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phitt = subs(subs(phitt,'ONETHIRD',1./3.),'ONESIXTH',1./6.);

np = length(phi);
fprintf(fid,'void\n%s_true(Real phi[%d], Real phis[%d], Real phit[%d], Real phiss[%d], Real phist[%d], Real phitt[%d])\n',func_name,np,np,np,np,np,np);
fprintf(fid,'{\n');
print_inline(fid,phi,'phi');
print_inline(fid,phis,'phis');
print_inline(fid,phit,'phit');
print_inline(fid,phiss,'phiss');
print_inline(fid,phist,'phist');
print_inline(fid,phitt,'phitt');
fprintf(fid,'}\n');

end

function phi = lagrange_p4(s,t)
    ONETHIRD = sym('ONETHIRD','real');
    phi = setup_phi(4);
    x = s;
    y = t;
    xx=x*x;
    xy=x*y;
    yy=y*y;
    xxx=xx*x;
    xxy=xx*y;
    xyy=xy*y;
    yyy=yy*y;
    xxxx=xxx*x;
    xxxy=xxx*y;
    xxyy=xxy*y;
    xyyy=xyy*y;
    yyyy=yyy*y;

    phi(1) = 1.0-25.0*ONETHIRD*x-25.0*ONETHIRD*y+70.0*ONETHIRD*xx+140.0*ONETHIRD*xy+70.0*ONETHIRD*yy- ...
      80.0*ONETHIRD*xxx-80.0*xxy-80.0*xyy-80.0*ONETHIRD*yyy+32.0*ONETHIRD*xxxx+...
      128.0*ONETHIRD*xxxy+64.0*xxyy+128.0*ONETHIRD*xyyy+32.0*ONETHIRD*yyyy;
    phi(2) = -x+22.0*ONETHIRD*xx-16.0*xxx+32.0*ONETHIRD*xxxx;
    phi(3) = -y+22.0*ONETHIRD*yy-16.0*yyy+32.0*ONETHIRD*yyyy;
    phi(4) = 16.0*ONETHIRD*xy-32.0*xxy+128.0*ONETHIRD*xxxy;
    phi(5) = 4.0*xy-16.0*xxy-16.0*xyy+64.0*xxyy;
    phi(6) = 16.0*ONETHIRD*xy-32.0*xyy+128.0*ONETHIRD*xyyy;
    phi(7) = 16.0*ONETHIRD*y-16.0*ONETHIRD*xy-112.0*ONETHIRD*yy+32.0*xyy+224.0*ONETHIRD*yyy-128.0*ONETHIRD*xyyy-128.0*ONETHIRD*yyyy;
    phi(8) = -12.0*y+28.0*xy+76.0*yy-16.0*xxy-144.0*xyy-128.0*yyy+64.0*xxyy+128.0*xyyy+64.0*yyyy;
    phi(9) = 16.0*y-208.0*ONETHIRD*xy-208.0*ONETHIRD*yy+96.0*xxy+192.0*xyy+96.0*yyy-128.0*ONETHIRD*xxxy-...
      128.0*xxyy-128.0*xyyy-128.0*ONETHIRD*yyyy;
    phi(10) = 16.0*x-208.0*ONETHIRD*xx-208.0*ONETHIRD*xy+96.0*xxx+192.0*xxy+96.0*xyy-128.0*ONETHIRD*xxxx-...
      128.0*xxxy-128.0*xxyy-128.0*ONETHIRD*xyyy;
    phi(11) = -12.0*x+76.0*xx+28.0*xy-128.0*xxx-144.0*xxy-16.0*xyy+64.0*xxxx+128.0*xxxy+64.0*xxyy;
    phi(12) = 16.0*ONETHIRD*x-112.0*ONETHIRD*xx-16.0*ONETHIRD*xy+224.0*ONETHIRD*xxx+32.0*xxy-128.0*ONETHIRD*xxxx-128.0*ONETHIRD*xxxy;
    phi(13) = 96.0*xy-224.0*xxy-224.0*xyy+128.0*xxxy+256.0*xxyy+128.0*xyyy;
    phi(14) = -32.0*xy+160.0*xxy+32.0*xyy-128.0*xxxy-128.0*xxyy;
    phi(15) = -32.0*xy+32.0*xxy+160.0*xyy-128.0*xxyy-128.0*xyyy;
end

function phi = lagrange_p5(s,t)
    ONETHIRD = sym('ONETHIRD','real');
    ONESIXTH = sym('ONESIXTH','real');
    phi = setup_phi(5);
    x = s;
    y = t;
    xx=x*x;
    xy=x*y;
    yy=y*y;
    xxx=xx*x;
    xxy=xx*y;
    xyy=xy*y;
    yyy=yy*y;
    xxxx=xxx*x;
    xxxy=xxx*y;
    xxyy=xxy*y;
    xyyy=xyy*y;
    yyyy=yyy*y;
    xxxxx=xxxx*x;
    xxxxy=xxxx*y;
    xxxyy=xxxy*y;
    xxyyy=xxyy*y;
    xyyyy=xyyy*y;
    yyyyy=yyyy*y;

    phi(1)  = 1.0-137.0/12.0*x-137.0/12.0*y+375.0/8.0*xx+375.0*0.25*xy+375.0/8.0*yy- ...
                 2125.0/24.0*xxx-2125.0/8.0*xxy-2125.0/8.0*xyy-2125.0/24.0*yyy+...
                 625.0/8.0*xxxx+312.5*xxxy+1875.0*0.25*xxyy+312.5*xyyy+...
                 625.0/8.0*yyyy-625.0/24.0*xxxxx-3125.0/24.0*xxxxy-3125.0/12.0*xxxyy-...
                 3125.0/12.0*xxyyy-3125.0/24.0*xyyyy-625.0/24.0*yyyyy;
    phi(2)  = x-125.0/12.0*xx+875.0/24.0*xxx-625.0/12.0*xxxx+625.0/24.0*xxxxx;
    phi(3)  = y-125.0/12.0*yy+875.0/24.0*yyy-625.0/12.0*yyyy+625.0/24.0*yyyyy;
    phi(4)  = -25.0*0.25*xy+1375.0/24.0*xxy-625.0*0.25*xxxy+3125.0/24.0*xxxxy;
    phi(5)  = -25.0*ONESIXTH*xy+125.0*0.25*xxy+125.0*ONESIXTH*xyy-625.0/12.0*xxxy-625.0*0.25*xxyy+3125.0/12.0*xxxyy;
    phi(6)  = -25.0*ONESIXTH*xy+125.0*ONESIXTH*xxy+125.0*0.25*xyy-625.0*0.25*xxyy-625.0/12.0*xyyy+3125.0/12.0*xxyyy;
    phi(7)  = -25.0*0.25*xy+1375.0/24.0*xyy-625.0*0.25*xyyy+3125.0/24.0*xyyyy;
    phi(8)  = -25.0*0.25*y+25.0*0.25*xy+1525.0/24.0*yy-1375.0/24.0*xyy-5125.0/24.0*yyy+...
                625.0*0.25*xyyy+6875.0/24.0*yyyy-3125.0/24.0*xyyyy-3125.0/24.0*yyyyy;
    phi(9)  = 50.0*ONETHIRD*y-37.5*xy-162.5*yy+125.0*ONESIXTH*xxy+3875.0/12.0*xyy+6125.0/12.0*yyy-...
                625.0*0.25*xxyy-3125.0*0.25*xyyy-625.0*yyyy+3125.0/12.0*xxyyy+3125.0*ONESIXTH*xyyyy+3125.0/12.0*yyyyy;
    phi(10)  = -25.0*y+1175.0/12.0*xy+2675.0/12.0*yy-125.0*xxy-8875.0/12.0*xyy-7375.0/12.0*yyy+...
                625.0/12.0*xxxy+3125.0*0.25*xxyy+5625.0*0.25*xyyy+8125.0/12.0*yyyy-3125.0/12.0*xxxyy-...
                3125.0*0.25*xxyyy-3125.0*0.25*xyyyy-3125.0/12.0*yyyyy;
    phi(11) = 25.0*y-1925.0/12.0*xy-1925.0/12.0*yy+8875.0/24.0*xxy+8875.0/12.0*xyy+8875.0/24.0*yyy-...
                4375.0/12.0*xxxy-4375.0*0.25*xxyy-4375.0*0.25*xyyy-4375.0/12.0*yyyy+3125.0/24.0*xxxxy+ ...
                3125.0*ONESIXTH*xxxyy+3125.0*0.25*xxyyy+3125.0*ONESIXTH*xyyyy+3125.0/24.0*yyyyy;
    phi(12) = 25.0*x-1925.0/12.0*xx-1925.0/12.0*xy+8875.0/24.0*xxx+8875.0/12.0*xxy+8875.0/24.0*xyy-...
                4375.0/12.0*xxxx-4375.0*0.25*xxxy-4375.0*0.25*xxyy-4375.0/12.0*xyyy+3125.0/24.0*xxxxx+...
                3125.0*ONESIXTH*xxxxy+3125.0*0.25*xxxyy+3125.0*ONESIXTH*xxyyy+3125.0/24.0*xyyyy;
    phi(13) = -25.0*x+2675.0/12.0*xx+1175.0/12.0*xy-7375.0/12.0*xxx-8875.0/12.0*xxy-125.0*xyy+...
                 8125.0/12.0*xxxx+5625.0*0.25*xxxy+3125.0*0.25*xxyy+625.0/12.0*xyyy-3125.0/12.0*xxxxx-...
                 3125.0*0.25*xxxxy-3125.0*0.25*xxxyy-3125.0/12.0*xxyyy;
    phi(14) = 50.0*ONETHIRD*x-162.5*xx-37.5*xy+6125.0/12.0*xxx+3875.0/12.0*xxy+125.0*ONESIXTH*xyy-625.0*xxxx-...
                 3125.0*0.25*xxxy-625.0*0.25*xxyy+3125.0/12.0*xxxxx+3125.0*ONESIXTH*xxxxy+3125.0/12.0*xxxyy;
    phi(15) = -25.0*0.25*x+1525.0/24.0*xx+25.0*0.25*xy-5125.0/24.0*xxx-1375.0/24.0*xxy+6875.0/24.0*xxxx+...
                 625.0*0.25*xxxy-3125.0/24.0*xxxxx-3125.0/24.0*xxxxy;
    phi(16) = 250.0*xy-5875.0*ONESIXTH*xxy-5875.0*ONESIXTH*xyy+1250.0*xxxy+2500.0*xxyy+1250.0*xyyy-3125.0*ONESIXTH*xxxxy-...
                 1562.5*xxxyy-1562.5*xxyyy-3125.0*ONESIXTH*xyyyy;
    phi(17) = -125.0*xy+3625.0*0.25*xxy+1125.0*0.25*xyy-1562.5*xxxy-6875.0*0.25*xxyy-625.0*0.25*xyyy+...
                 3125.0*0.25*xxxxy+1562.5*xxxyy+3125.0*0.25*xxyyy;
    phi(18) = 125.0*ONETHIRD*xy-2125.0*ONESIXTH*xxy-125.0*ONETHIRD*xyy+2500.0*ONETHIRD*xxxy+312.5*xxyy-3125.0*ONESIXTH*xxxxy-3125.0*ONESIXTH*xxxyy;
    phi(19) = -125.0*xy+1125.0*0.25*xxy+3625.0*0.25*xyy-625.0*0.25*xxxy-6875.0*0.25*xxyy-1562.5*xyyy+...
                 3125.0*0.25*xxxyy+1562.5*xxyyy+3125.0*0.25*xyyyy;
    phi(20) = 125.0*0.25*xy-187.5*xxy-187.5*xyy+625.0*0.25*xxxy+4375.0*0.25*xxyy+...
                 625.0*0.25*xyyy-3125.0*0.25*xxxyy-3125.0*0.25*xxyyy;
    phi(21) = 125.0*ONETHIRD*xy-125.0*ONETHIRD*xxy-2125.0*ONESIXTH*xyy+312.5*xxyy+2500.0*ONETHIRD*xyyy-...
                 3125.0*ONESIXTH*xxyyy-3125.0*ONESIXTH*xyyyy;
end
