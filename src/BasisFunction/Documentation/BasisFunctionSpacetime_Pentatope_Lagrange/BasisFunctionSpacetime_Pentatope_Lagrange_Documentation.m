
filename_base= './BasisFunctionSpacetime_Pentatope_Lagrange_Ordering';

for p= 1:4
    
    writeDocFilesGivenOrder(p, [filename_base '_P' num2str(p)]);
    
end

function writeDocFilesGivenOrder(p, filename_base)

if p > 2
    for i= 1:10
        
        writeDocFile(p, i - 1, [filename_base '_Frame' num2str(i - 1) '.tex']);
        
    end
end

writeDocFile(p, -1, [filename_base '.tex']);

end

function writeDocFile(p, active_frame, filename)

if p > 4
    error("can't generate tikz documentation for p > 4 :(");
end

fid= fopen(filename, "w");

DOFcounter= 0;
nDOFperEdge= (p > 1)*(p - 1);
nDOFperFrame= (p > 2)*(p - 1)*(p - 2)/2;
nDOFperTrace= (p > 3)*(p - 1)*(p - 2)*(p - 3)/6;
nDOFperCell= (p > 4)*(p - 1)*(p - 2)*(p - 3)*(p - 4)/24;

SANS_NODE= [0; 1; 2; 3; 4];

SANS_EDGE= [0   1
            0   2
            0   3
            0   4
            1   2
            1   3
            1   4
            2   3
            2   4
            3   4];

SANS_FRAME= [0    1   2
           0    1   3
           0    1   4
           0    2   3
           0    2   4
           0    3   4
           1    2   3
           1    2   4
           1    3   4
           2    3   4];

SANS_TRACE= [1  2   4   3
             0  2   3   4
             0  1   4   3
             0  1   2   4
             0  1   3   2];

% active_frame= -1; % using 0-indexing

% fprintf(fid, "\n");
% 
% fprintf(fid, "\\usetikzlibrary{calc}\n");
% fprintf(fid, "\\usetikzlibrary{shapes}\n\n");

fprintf(fid, "\\begin{tikzpicture}\n\n");
% fprintf(fid, "\\begin{tikzpicture}[mystyle/.style={draw, shape= rectangle, fill= white}]\n\n");

fprintf(fid, "\\def \\radius{4cm}\n\n");

fprintf(fid, "\n");

% loop over the nodes
for i= 1:size(SANS_NODE, 1)
    
    % print out the line we have generated to print the node coordinate!
    fprintf(fid, ['\\coordinate (c%d) at (%d:\\radius);\n'], ...
        SANS_NODE(i), 90 - 360/5*(i - 1));
    
end

fprintf(fid, "\n");

% loop over the nodes
for i= 1:size(SANS_NODE, 1)
    
    % default to black
    color_key= 'black';
    
    % gray out anything that's not on the frame of interest
    if active_frame ~= -1 && ismember(i - 1, setdiff(SANS_NODE, ...
            SANS_FRAME(active_frame + 1, :)))
        color_key= 'gray';
    end
    
    % print out the line we have generated to print the node!
    fprintf(fid, ['\\node[draw= %s, shape= circle, fill= white, text= %s] ' ...
        '(p%d) at (c%d) {$%d$};\n'], color_key, color_key, ...
        SANS_NODE(i), SANS_NODE(i), SANS_NODE(i));
    
    % we've added a DOF, increment the DOF counter
    DOFcounter= DOFcounter + 1;
    
end

fprintf(fid, "\n");

% loop over the nodes
for i= 1:size(SANS_EDGE, 1)
    
    % default edge color to black
    color_key= 'black';
    
    % gray out anything that's not on the frame of interest
    if active_frame ~= -1 && any(ismember(SANS_EDGE(i, :), ...
            setdiff(SANS_NODE, SANS_FRAME(active_frame + 1, :))))
        color_key= 'gray';
    end

    % default location to near start
    location_key= 'near start';
    
    if (p == 3)
        location_key= 'midway';
    elseif (p == 4)
        location_key= 'pos=0.32';
    end
    
    position_key= 'above';
    
%     if (p == 4 && (i == 1 || i == 3 || i == 4 || i == 6 || i == 7))
%         position_key= 'below';
%     end
    
    % print out the line we have generated to print the edge
    fprintf(fid, ['\\draw[color= %s] (p%d) -- (p%d) node[%s, sloped, ' ...
        '%s] (e%d) {$e%d$};\n'], color_key, ...
        SANS_NODE(SANS_EDGE(i, 1) + 1), ...
        SANS_NODE(SANS_EDGE(i, 2) + 1), ...
        location_key, position_key, i - 1, i - 1);
    
end

fprintf(fid, "\n");

% loop over the edges
for i= 1:size(SANS_EDGE, 1)
    
    % default edge color to black
    color_key= 'black';
    
    % gray out anything that's not on the frame of interest
    if active_frame ~= -1 && any(ismember(SANS_EDGE(i, :), ...
            setdiff(SANS_NODE, SANS_FRAME(active_frame + 1, :))))
        color_key= 'gray';
    end
    
    % default location to near start
    location_key= 'near start';
    
    % number of edge-interior DOFs
    for j= 1:nDOFperEdge
        
        % default the location of the new DOFs to halfway
        dist_key= '0.5';
        
        % slide around the p= 3 case
        if p == 3 & j == 1
            dist_key= '0.25';
        elseif p == 3 & j == 2
            dist_key= '0.75';
        elseif p == 4 & j == 1
            dist_key= '0.25';
        elseif p == 4 & j == 2
            dist_key= '0.5';
        elseif p == 4 & j == 3
            dist_key= '0.75';
        elseif p > 4
            error('not ready yet!');
        end
        
        % print out the line we have generated to print the edge DOF
        fprintf(fid, ['\\node[draw= %s, shape= rectangle, fill= white, ' ...
            'text= %s] (p%d) at ($(p%d)!%s!(p%d)$) {$%d$};\n'], ...
            color_key, color_key, DOFcounter, ...
            SANS_NODE(SANS_EDGE(i, 1) + 1), dist_key, ...
            SANS_NODE(SANS_EDGE(i, 2) + 1), DOFcounter);
        
        % we've added a DOF increment the counter
        DOFcounter= DOFcounter + 1;
        
    end
    
end

fprintf(fid, "\n");

if active_frame ~= -1
    
    DOFcounter= DOFcounter + active_frame*nDOFperFrame;
    
    for i= active_frame
        
        if p == 3
            
            fprintf(fid, ['\\node[draw= %s, shape= diamond, fill= white, ' ...
                'text= %s] at (barycentric cs:c%d=%s,c%d=%s,c%d=%s) ' ...
                '{%d};'], 'black', 'black', ...
                SANS_FRAME(active_frame + 1, 1), '1', ...
                SANS_FRAME(active_frame + 1, 2), '1', ...
                SANS_FRAME(active_frame + 1, 3), '1', DOFcounter);
            DOFcounter= DOFcounter + 1;
            
        elseif p == 4
            
            for j= 1:nDOFperFrame
                
                b= 0.25*[1 1 1];
                b(j)= b(j)*2;
                
                fprintf(fid, ['\\node[draw= %s, shape= diamond, fill= white, ' ...
                    'text= %s] at (barycentric cs:c%d=%s,c%d=%s,c%d=%s) ' ...
                    '{%d};'], 'black', 'black', ...
                    SANS_FRAME(active_frame + 1, 1), num2str(b(1)), ...
                    SANS_FRAME(active_frame + 1, 2), num2str(b(2)), ...
                    SANS_FRAME(active_frame + 1, 3), num2str(b(3)), ...
                    DOFcounter);
                DOFcounter= DOFcounter + 1;
                
            end
            
        else
            
            error('problem');
            
        end
        
    end
    
    fprintf(fid, "\n");
    
end

fprintf(fid, "\\end{tikzpicture}\n");

fclose(fid);

end
