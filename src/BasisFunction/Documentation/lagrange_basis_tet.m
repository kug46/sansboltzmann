function lagrange_basis_tet()
clc;
s = sym('s','real');
t = sym('t','real');
u = sym('u','real');

fid1 = fopen('BasisFunctionVolume_Tetraherdon_Lagrange_autogen.h','w');
fid2 = fopen('BasisFunctionVolume_Tetraherdon_Lagrange_known.h','w');

fprintf(fid2,'// Solution Adaptive Numerical Simulator (SANS)\n');
fprintf(fid2,'// Copyright 2013-2016, Massachusetts Institute of Technology\n');
fprintf(fid2,'// Licensed under The GNU Lesser General Public License, version 2.1\n');
fprintf(fid2,'// See http://www.opensource.org/licenses/lgpl-2.1.php\n');
fprintf(fid2,'\n');

%PX node ordering
qorder = 4;
l=1;
for i = 0:qorder
    didivqorder = i/qorder;
    for j = 0:qorder-i
        djdivqorder = j/qorder;
        dk = 0.0;
        for k=0:qorder-i-j
            x(l) = dk/qorder;
            y(l) = djdivqorder;
            z(l) = didivqorder;
            l = l+1;
            dk = dk + 1.0;
        end
    end
end

% print the PX node ordering
x
y
z


% p1
p=1;
phi = setup_phi(1);
phi(1) = 1 - s - t - u;
phi(2) = s;
phi(3) = t;
phi(4) = u;

[phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu] = get_derivs(phi,s,t,u);
print_c(fid1,p,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu);

% unit test 1
s0 = .25;
t0 = .5;
u0 = 0.2;
print_c_unit(fid2,'lagrange_tet_p1_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);

% p2
p=2;
phi = lagrange_p2(s,t,u);
[phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu] = get_derivs(phi,s,t,u);
print_c(fid1,p,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu);

% unit test 1
s0 = .25;
t0 = .5;
u0 = 0.2;
print_c_unit(fid2,'lagrange_tet_p2_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);


% p3
p=3;
phi = lagrange_p3(s,t,u);
[phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu] = get_derivs(phi,s,t,u);
print_c(fid1,p,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu);

% unit test 1
s0 = .25;
t0 = .5;
u0 = 0.2;
print_c_unit(fid2,'lagrange_tet_p3_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);

% p4
p=4;
phi = lagrange_p4(s,t,u);
[phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu] = get_derivs(phi,s,t,u);
print_c(fid1,p,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu);

% unit test 1
s0 = .25;
t0 = .5;
u0 = 0.2;
print_c_unit(fid2,'lagrange_tet_p4_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);


% % p5
% p=5;
% phi = lagrange_p5(s,t,u); 
% [phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu] = get_derivs(phi,s,t,u);
% print_c(fid1,p,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu);
% 
% % unit test 1
% s0 = .25;
% t0 = .5;
% u0 = 0.2;
% print_c_unit(fid2,'lagrange_tet_p5_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);

fclose(fid1);
fclose(fid2);

end

function phi = setup_phi(p)
np  = (p +1)*(p +2)*(p +3)/6;
phi = sym('phi',[np,1]); 
end

function [phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu] = get_derivs(phi,s,t,u)
phis = diff(phi,s);
phit = diff(phi,t);
phiu = diff(phi,u);
phiss = diff(phis,s);
phist = diff(phis,t);
phitt = diff(phit,t);
phisu = diff(phis,u);
phitu = diff(phit,u);
phiuu = diff(phiu,u);
end

function print_header(fid,voltype,elemtype,basistype,p,nderiv)
if nderiv==0
    func_name = 'evalBasis';
    args_name = 'Real phi[]';
elseif nderiv==1
    func_name = 'evalBasisDerivative';
    args_name = 'Real phis[], Real phit[], Real phiu[]';
elseif nderiv==2
    func_name = 'evalBasisHessianDerivative';
    args_name = 'Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[]';
end
fprintf(fid,'\nvoid\nBasisFunction%s<%s,%s,%d>::%s(\n',voltype,elemtype,basistype,p,func_name);
fprintf(fid,'  const Real& s, const Real& t, const Real& u, const Int4&,\n  %s, int nphi) const\n',args_name);
fprintf(fid,'{\nSANS_ASSERT(nphi==%d);\n',(p+1)*(p+2)*(p+3)/6);
end

function print_closing(fid)
fprintf(fid,'\n}\n\n');
end

function print_coordintes(fid,voltype,elemtype,basistype,p)
fprintf(fid,'\nvoid\n');
fprintf(fid,'BasisFunction%s<%s,%s,%d>::coordinates(std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u) const\n',voltype,elemtype,basistype,p);
fprintf(fid,'{\n');
fprintf(fid,'  s = coords_s_;\n');
fprintf(fid,'  t = coords_t_;\n');
fprintf(fid,'  u = coords_u_;\n');
fprintf(fid,'}\n\n');
end

function print_c(fid,p,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu)

fprintf(fid,'//----------------------------------------------------------------------------//\n');
fprintf(fid,'// Lagrange: P=%d\n',p);

print_header(fid,'Volume','Tet','Lagrange',p,0)
print_ccode(fid,'phi',phi);
print_closing(fid);

print_header(fid,'Volume','Tet','Lagrange',p,1)
print_ccode(fid,'phis',phis);
print_ccode(fid,'phit',phit);
print_ccode(fid,'phiu',phiu);
print_closing(fid);

print_header(fid,'Volume','Tet','Lagrange',p,2)
print_ccode(fid,'phiss',phiss);
print_ccode(fid,'phist',phist);
print_ccode(fid,'phitt',phitt);
print_ccode(fid,'phisu',phisu);
print_ccode(fid,'phitu',phitu);
print_ccode(fid,'phiuu',phiuu);
print_closing(fid);

print_coordintes(fid,'Volume','Tet','Lagrange',p);

end

function print_inline(fid,x,name)
fprintf(fid,'\n  // %s\n',name);
for i=1:length(x)
    fprintf(fid,'  %s[%d] = %1.14e;\n',name,i-1,x(i));
end
end

function result = is_operator( s )
result = false;
if strcmp(s,'-')==1
    result = true;
elseif strcmp(s,'+')==1
    result = true;
elseif strcmp(s,'*')==1
    result = true;
elseif strcmp(s,'/')==1
    result = true;
end
end

function print_ccode(fid,name,expr0)
maxline = 100; % max number of characters per line so vera doesn't complain
fprintf(fid,'\n  // %s\n',name);
for i=1:length(expr0)
    expr = strsplit(ccode(expr0(i)),'=');
    expr = expr{2};
    % the full expression
    expr = sprintf('%s[%d] = %s\n',name,i-1,expr);
    %disp(expr)
    n = length(expr);
    pos = 1;
    while pos<n
        if pos+maxline>n
            limit = n;
        else
            limit = pos +maxline;
            while (true)
                if limit==n
                    break;
                end
                if is_operator( expr(limit) )
                    break;
                end
                limit = limit +1;
            end
        end
        s = expr(pos:limit);
        if limit == n
            fprintf(fid,'%s',s);
        else
            fprintf(fid,'%s\n           ',s);
        end
        pos = limit +1;
    end
    %fprintf(fid,'  %s[%d] = %s\n',name,i-1,expr);
end
end

function print_c_unit(fid,func_name,s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u)

phi   = subs(subs(subs(phi  ,s,s0),t,t0),u,u0);
phis  = subs(subs(subs(phis ,s,s0),t,t0),u,u0);
phit  = subs(subs(subs(phit ,s,s0),t,t0),u,u0);
phiu  = subs(subs(subs(phiu ,s,s0),t,t0),u,u0);
phiss = subs(subs(subs(phiss,s,s0),t,t0),u,u0);
phist = subs(subs(subs(phist,s,s0),t,t0),u,u0);
phitt = subs(subs(subs(phitt,s,s0),t,t0),u,u0);
phisu = subs(subs(subs(phisu,s,s0),t,t0),u,u0);
phitu = subs(subs(subs(phitu,s,s0),t,t0),u,u0);
phiuu = subs(subs(subs(phiuu,s,s0),t,t0),u,u0);

% in case we have ONETHIRD or ONESIXTH lingering..
phi   = subs(subs(phi  ,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phis  = subs(subs(phis ,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phit  = subs(subs(phit ,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phiu  = subs(subs(phiu ,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phiss = subs(subs(phiss,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phist = subs(subs(phist,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phitt = subs(subs(phitt,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phisu = subs(subs(phisu,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phitu = subs(subs(phitu,'ONETHIRD',1./3.),'ONESIXTH',1./6.);
phiuu = subs(subs(phiuu,'ONETHIRD',1./3.),'ONESIXTH',1./6.);

np = length(phi);
fprintf(fid,'void\n%s_true(Real phi[%d],\n    Real phis[%d], Real phit[%d], Real phiu[%d],\n    Real phiss[%d], Real phist[%d], Real phitt[%d], Real phisu[%d], Real phitu[%d], Real phiuu[%d])\n',func_name,np,np,np,np,np,np,np,np,np,np);
fprintf(fid,'{\n');
print_inline(fid,phi,'phi');
print_inline(fid,phis,'phis');
print_inline(fid,phit,'phit');
print_inline(fid,phiu,'phiu');
print_inline(fid,phiss,'phiss');
print_inline(fid,phist,'phist');
print_inline(fid,phitt,'phitt');
print_inline(fid,phisu,'phisu');
print_inline(fid,phitu,'phitu');
print_inline(fid,phiuu,'phiuu');
fprintf(fid,'}\n');

end

function phi = lagrange_p2(s,t,u)
    phiPX = setup_phi(2);
    x = s;
    y = t;
    z = u;

    PXtoSANS = [0  2  5  9  8    7    4    3    6    1];
    PXtoSANS = PXtoSANS + 1;

    phiPX(1)  = 1.0-3.0*z-3.0*y-3.0*x+2.0*z*z+4.0*y*z+4.0*x*z+2.0*y*y+4.0*x*y+2.0*x*x;
    phiPX(2)  = 4.0*x-4.0*x*z-4.0*x*y-4.0*x*x;
    phiPX(3)  = -x+2.0*x*x;
    phiPX(4)  = 4.0*y-4.0*y*z-4.0*y*y-4.0*x*y;
    phiPX(5)  = 4.0*x*y;
    phiPX(6)  = -y+2.0*y*y;
    phiPX(7)  = 4.0*z-4.0*z*z-4.0*y*z-4.0*x*z;
    phiPX(8)  = 4.0*x*z;
    phiPX(9)  = 4.0*y*z;
    phiPX(10) = -z+2.0*z*z;
    
    phi = setup_phi(2);
    for i = 1:length(phiPX)
        phi(i) = phiPX(PXtoSANS(i));
    end
end

function phi = lagrange_p3(s,t,u)
    phiPX = setup_phi(3);
    x = s;
    y = t;
    z = u;

    PXtoSANS = [0  3  9 19  15  18  17  12  6   8   7   4   10  16  1   2   14  13  11  5];
    PXtoSANS = PXtoSANS + 1;

    phiPX(1)  = 1.0-5.5*z-5.5*y-5.5*x+9.0*z*z+18.0*z*y+18.0*z*x+9.0*y*y+18.0*y*x+9.0*x*x-4.5*z*z*z-13.5*z*z*y-13.5*z*z*x-13.5*z*y*y-27.0*z*y*x-13.5*z*x*x-4.5*y*y*y-13.5*y*y*x-13.5*y*x*x-4.5*x*x*x;
    phiPX(2)  = 9.0*x-22.5*z*x-22.5*y*x-22.5*x*x+13.5*z*z*x+27.0*z*y*x+27.0*z*x*x+13.5*y*y*x+27.0*y*x*x+13.5*x*x*x;
    phiPX(3)  = -4.5*x+4.5*z*x+4.5*y*x+18.0*x*x-13.5*z*x*x-13.5*y*x*x-13.5*x*x*x;
    phiPX(4)  = x-4.5*x*x+4.5*x*x*x;
    phiPX(5)  = 9.0*y-22.5*z*y-22.5*y*y-22.5*y*x+13.5*z*z*y+27.0*z*y*y+27.0*z*y*x+13.5*y*y*y+27.0*y*y*x+13.5*y*x*x;
    phiPX(6)  = 27.0*y*x-27.0*z*y*x-27.0*y*y*x-27.0*y*x*x;
    phiPX(7)  = -4.5*y*x+13.5*y*x*x;
    phiPX(8)  = -4.5*y+4.5*z*y+18.0*y*y+4.5*y*x-13.5*z*y*y-13.5*y*y*y-13.5*y*y*x;
    phiPX(9)  = -4.5*y*x+13.5*y*y*x;
    phiPX(10) = y-4.5*y*y+4.5*y*y*y;
    phiPX(11) = 9.0*z-22.5*z*z-22.5*z*y-22.5*z*x+13.5*z*z*z+27.0*z*z*y+27.0*z*z*x+13.5*z*y*y+27.0*z*y*x+13.5*z*x*x;
    phiPX(12) = 27.0*z*x-27.0*z*z*x-27.0*z*y*x-27.0*z*x*x;
    phiPX(13) = -4.5*z*x+13.5*z*x*x;
    phiPX(14) = 27.0*z*y-27.0*z*z*y-27.0*z*y*y-27.0*z*y*x;
    phiPX(15) = 27.0*z*y*x;
    phiPX(16) = -4.5*z*y+13.5*z*y*y;
    phiPX(17) = -4.5*z+18.0*z*z+4.5*z*y+4.5*z*x-13.5*z*z*z-13.5*z*z*y-13.5*z*z*x;
    phiPX(18) = -4.5*z*x+13.5*z*z*x;
    phiPX(19) = -4.5*z*y+13.5*z*z*y;
    phiPX(20) = z-4.5*z*z+4.5*z*z*z;
    
    phi = setup_phi(3);
    for i = 1:length(phiPX)
        phi(i) = phiPX(PXtoSANS(i));
    end
end

function phi = lagrange_p4(s,t,u)
    ONETHIRD = sym('ONETHIRD','real');
    phiPX = setup_phi(4);
    x = s;
    y = t;
    z = u;

    PXtoSANS = [0  4 14 34  24  30  33  32  27  18  8   11  13  12  9   5   15  25  31  1   2   3   21  23  29  19  28  22  16  17  26  6   10  7   20];
    PXtoSANS = PXtoSANS + 1;
    
    phiPX(1) = 1.0+128.0*z*y*x*x+128.0*z*z*y*x-160.0*z*y*x+128.0*ONETHIRD*z*z*z*y-25.0*ONETHIRD*x-25.0*ONETHIRD*y+128.0*z*y*y*x-80.0*y*x*x-80.0*z*y*y-80.0*y*y*x+140.0*ONETHIRD*y*x-80.0*z*z*y-25.0*ONETHIRD*z+32.0*ONETHIRD*y*y*y*y+32.0*ONETHIRD*x*x*x*x+32.0*ONETHIRD*z*z*z*z+70.0*ONETHIRD*z*z+70.0*ONETHIRD*y*y-80.0*ONETHIRD*z*z*z-80.0*ONETHIRD*y*y*y+140.0*ONETHIRD*z*y+70.0*ONETHIRD*x*x-80.0*ONETHIRD*x*x*x+128.0*ONETHIRD*y*x*x*x+64.0*y*y*x*x+128.0*ONETHIRD*y*y*y*x+64.0*z*z*x*x+128.0*ONETHIRD*z*x*x*x+128.0*ONETHIRD*z*z*z*x-80.0*z*z*x+140.0*ONETHIRD*z*x+128.0*ONETHIRD*z*y*y*y-80.0*z*x*x+64.0*z*z*y*y;
    phiPX(2) = 192.0*z*y*x+16.0*x-128.0*z*y*y*x-128.0*z*z*y*x-208.0*ONETHIRD*y*x-208.0*ONETHIRD*x*x-128.0*z*x*x*x-128.0*ONETHIRD*x*x*x*x-208.0*ONETHIRD*z*x-128.0*z*z*x*x-256.0*z*y*x*x+96.0*x*x*x-128.0*ONETHIRD*z*z*z*x+96.0*z*z*x-128.0*ONETHIRD*y*y*y*x-128.0*y*y*x*x-128.0*y*x*x*x+192.0*y*x*x+96.0*y*y*x+192.0*z*x*x;
    phiPX(3) = -32.0*z*y*x-12.0*x+28.0*y*x+76.0*x*x+128.0*z*x*x*x+64.0*x*x*x*x+28.0*z*x+64.0*z*z*x*x+128.0*z*y*x*x-128.0*x*x*x-16.0*z*z*x+64.0*y*y*x*x+128.0*y*x*x*x-144.0*y*x*x-16.0*y*y*x-144.0*z*x*x;
    phiPX(4) = 16.0*ONETHIRD*x-16.0*ONETHIRD*y*x-112.0*ONETHIRD*x*x-128.0*ONETHIRD*z*x*x*x-128.0*ONETHIRD*x*x*x*x-16.0*ONETHIRD*z*x+224.0*ONETHIRD*x*x*x-128.0*ONETHIRD*y*x*x*x+32.0*y*x*x+32.0*z*x*x;
    phiPX(5) = -x+22.0*ONETHIRD*x*x+32.0*ONETHIRD*x*x*x*x-16.0*x*x*x;
    phiPX(6) = 192.0*z*y*x+16.0*y-256.0*z*y*y*x-128.0*z*z*y*x-208.0*ONETHIRD*y*x-128.0*z*y*y*y-208.0*ONETHIRD*y*y+96.0*y*y*y-128.0*ONETHIRD*y*y*y*y+96.0*z*z*y-128.0*z*z*y*y-128.0*z*y*x*x+192.0*z*y*y-128.0*y*y*y*x-128.0*y*y*x*x-128.0*ONETHIRD*y*x*x*x-128.0*ONETHIRD*z*z*z*y+96.0*y*x*x+192.0*y*y*x-208.0*ONETHIRD*z*y;
    phiPX(7) = -224.0*z*y*x+256.0*z*y*y*x+128.0*z*z*y*x+96.0*y*x+256.0*z*y*x*x+128.0*y*y*y*x+256.0*y*y*x*x+128.0*y*x*x*x-224.0*y*x*x-224.0*y*y*x;
    phiPX(8) = 32.0*z*y*x-32.0*y*x-128.0*z*y*x*x-128.0*y*y*x*x-128.0*y*x*x*x+160.0*y*x*x+32.0*y*y*x;
    phiPX(9) = 16.0*ONETHIRD*y*x+128.0*ONETHIRD*y*x*x*x-32.0*y*x*x;
    phiPX(10) = -32.0*z*y*x-12.0*y+128.0*z*y*y*x+28.0*y*x+128.0*z*y*y*y+76.0*y*y-128.0*y*y*y+64.0*y*y*y*y-16.0*z*z*y+64.0*z*z*y*y-144.0*z*y*y+128.0*y*y*y*x+64.0*y*y*x*x-16.0*y*x*x-144.0*y*y*x+28.0*z*y;
    phiPX(11) = 32.0*z*y*x-128.0*z*y*y*x-32.0*y*x-128.0*y*y*y*x-128.0*y*y*x*x+32.0*y*x*x+160.0*y*y*x;
    phiPX(12) = 4.0*y*x+64.0*y*y*x*x-16.0*y*x*x-16.0*y*y*x;
    phiPX(13) = 16.0*ONETHIRD*y-16.0*ONETHIRD*y*x-128.0*ONETHIRD*z*y*y*y-112.0*ONETHIRD*y*y+224.0*ONETHIRD*y*y*y-128.0*ONETHIRD*y*y*y*y+32.0*z*y*y-128.0*ONETHIRD*y*y*y*x+32.0*y*y*x-16.0*ONETHIRD*z*y;
    phiPX(14) = 16.0*ONETHIRD*y*x+128.0*ONETHIRD*y*y*y*x-32.0*y*y*x;
    phiPX(15) = -y+22.0*ONETHIRD*y*y-16.0*y*y*y+32.0*ONETHIRD*y*y*y*y;
    phiPX(16) = -128.0*z*y*x*x-256.0*z*z*y*x+192.0*z*y*x-128.0*z*y*y*x+16.0*z-128.0*ONETHIRD*z*y*y*y+192.0*z*z*y-128.0*z*z*y*y+96.0*z*y*y-128.0*z*z*x*x-128.0*ONETHIRD*z*x*x*x-128.0*z*z*z*y-128.0*z*z*z*x+192.0*z*z*x-208.0*ONETHIRD*z*z+96.0*z*z*z-208.0*ONETHIRD*z*x+96.0*z*x*x-128.0*ONETHIRD*z*z*z*z-208.0*ONETHIRD*z*y;
    phiPX(17) = 256.0*z*y*x*x+256.0*z*z*y*x-224.0*z*y*x+128.0*z*y*y*x+256.0*z*z*x*x+128.0*z*x*x*x+128.0*z*z*z*x-224.0*z*z*x+96.0*z*x-224.0*z*x*x;
    phiPX(18) = -128.0*z*y*x*x+32.0*z*y*x-128.0*z*z*x*x-128.0*z*x*x*x+32.0*z*z*x-32.0*z*x+160.0*z*x*x;
    phiPX(19) = 128.0*ONETHIRD*z*x*x*x+16.0*ONETHIRD*z*x-32.0*z*x*x;
    phiPX(20) = 128.0*z*y*x*x+256.0*z*z*y*x-224.0*z*y*x+128.0*z*z*z*y+256.0*z*y*y*x-224.0*z*y*y-224.0*z*z*y+96.0*z*y+128.0*z*y*y*y+256.0*z*z*y*y;
    phiPX(21) = -256.0*z*y*x*x-256.0*z*z*y*x+256.0*z*y*x-256.0*z*y*y*x;
    phiPX(22) = 128.0*z*y*x*x-32.0*z*y*x;
    phiPX(23) = 32.0*z*y*x-128.0*z*y*y*x+160.0*z*y*y+32.0*z*z*y-32.0*z*y-128.0*z*y*y*y-128.0*z*z*y*y;
    phiPX(24) = -32.0*z*y*x+128.0*z*y*y*x;
    phiPX(25) = -32.0*z*y*y+16.0*ONETHIRD*z*y+128.0*ONETHIRD*z*y*y*y;
    phiPX(26) = 128.0*z*z*y*x-32.0*z*y*x+128.0*z*z*z*y-16.0*z*y*y-144.0*z*z*y-12.0*z+64.0*z*z*z*z+76.0*z*z-128.0*z*z*z+28.0*z*y+64.0*z*z*x*x+128.0*z*z*z*x-144.0*z*z*x+28.0*z*x-16.0*z*x*x+64.0*z*z*y*y;
    phiPX(27) = -128.0*z*z*y*x+32.0*z*y*x-128.0*z*z*x*x-128.0*z*z*z*x+160.0*z*z*x-32.0*z*x+32.0*z*x*x;
    phiPX(28) = 64.0*z*z*x*x-16.0*z*z*x+4.0*z*x-16.0*z*x*x;
    phiPX(29) = -128.0*z*z*y*x+32.0*z*y*x-128.0*z*z*z*y+32.0*z*y*y+160.0*z*z*y-32.0*z*y-128.0*z*z*y*y;
    phiPX(30) = 128.0*z*z*y*x-32.0*z*y*x;
    phiPX(31) = -16.0*z*y*y-16.0*z*z*y+4.0*z*y+64.0*z*z*y*y;
    phiPX(32) = -128.0*ONETHIRD*z*z*z*y+32.0*z*z*y+16.0*ONETHIRD*z-128.0*ONETHIRD*z*z*z*z-112.0*ONETHIRD*z*z+224.0*ONETHIRD*z*z*z-16.0*ONETHIRD*z*y-128.0*ONETHIRD*z*z*z*x+32.0*z*z*x-16.0*ONETHIRD*z*x;
    phiPX(33) = 128.0*ONETHIRD*z*z*z*x-32.0*z*z*x+16.0*ONETHIRD*z*x;
    phiPX(34) = 128.0*ONETHIRD*z*z*z*y-32.0*z*z*y+16.0*ONETHIRD*z*y;
    phiPX(35) = -z+32.0*ONETHIRD*z*z*z*z+22.0*ONETHIRD*z*z-16.0*z*z*z;
    
    phi = setup_phi(4);
    for i = 1:length(phiPX)
        phi(i) = phiPX(PXtoSANS(i));
    end
end

function phi = lagrange_p5(s,t)
    ONETHIRD = sym('ONETHIRD','real');
    ONESIXTH = sym('ONESIXTH','real');
    phi = setup_phi(5);
    x = s;
    y = t;
    xx=x*x;
    xy=x*y;
    yy=y*y;
    xxx=xx*x;
    xxy=xx*y;
    xyy=xy*y;
    yyy=yy*y;
    xxxx=xxx*x;
    xxxy=xxx*y;
    xxyy=xxy*y;
    xyyy=xyy*y;
    yyyy=yyy*y;
    xxxxx=xxxx*x;
    xxxxy=xxxx*y;
    xxxyy=xxxy*y;
    xxyyy=xxyy*y;
    xyyyy=xyyy*y;
    yyyyy=yyyy*y;

    phi(1)  = 1.0-137.0/12.0*x-137.0/12.0*y+375.0/8.0*xx+375.0*0.25*xy+375.0/8.0*yy- ...
                 2125.0/24.0*xxx-2125.0/8.0*xxy-2125.0/8.0*xyy-2125.0/24.0*yyy+...
                 625.0/8.0*xxxx+312.5*xxxy+1875.0*0.25*xxyy+312.5*xyyy+...
                 625.0/8.0*yyyy-625.0/24.0*xxxxx-3125.0/24.0*xxxxy-3125.0/12.0*xxxyy-...
                 3125.0/12.0*xxyyy-3125.0/24.0*xyyyy-625.0/24.0*yyyyy;
    phi(2)  = x-125.0/12.0*xx+875.0/24.0*xxx-625.0/12.0*xxxx+625.0/24.0*xxxxx;
    phi(3)  = y-125.0/12.0*yy+875.0/24.0*yyy-625.0/12.0*yyyy+625.0/24.0*yyyyy;
    phi(4)  = -25.0*0.25*xy+1375.0/24.0*xxy-625.0*0.25*xxxy+3125.0/24.0*xxxxy;
    phi(5)  = -25.0*ONESIXTH*xy+125.0*0.25*xxy+125.0*ONESIXTH*xyy-625.0/12.0*xxxy-625.0*0.25*xxyy+3125.0/12.0*xxxyy;
    phi(6)  = -25.0*ONESIXTH*xy+125.0*ONESIXTH*xxy+125.0*0.25*xyy-625.0*0.25*xxyy-625.0/12.0*xyyy+3125.0/12.0*xxyyy;
    phi(7)  = -25.0*0.25*xy+1375.0/24.0*xyy-625.0*0.25*xyyy+3125.0/24.0*xyyyy;
    phi(8)  = -25.0*0.25*y+25.0*0.25*xy+1525.0/24.0*yy-1375.0/24.0*xyy-5125.0/24.0*yyy+...
                625.0*0.25*xyyy+6875.0/24.0*yyyy-3125.0/24.0*xyyyy-3125.0/24.0*yyyyy;
    phi(9)  = 50.0*ONETHIRD*y-37.5*xy-162.5*yy+125.0*ONESIXTH*xxy+3875.0/12.0*xyy+6125.0/12.0*yyy-...
                625.0*0.25*xxyy-3125.0*0.25*xyyy-625.0*yyyy+3125.0/12.0*xxyyy+3125.0*ONESIXTH*xyyyy+3125.0/12.0*yyyyy;
    phi(10)  = -25.0*y+1175.0/12.0*xy+2675.0/12.0*yy-125.0*xxy-8875.0/12.0*xyy-7375.0/12.0*yyy+...
                625.0/12.0*xxxy+3125.0*0.25*xxyy+5625.0*0.25*xyyy+8125.0/12.0*yyyy-3125.0/12.0*xxxyy-...
                3125.0*0.25*xxyyy-3125.0*0.25*xyyyy-3125.0/12.0*yyyyy;
    phi(11) = 25.0*y-1925.0/12.0*xy-1925.0/12.0*yy+8875.0/24.0*xxy+8875.0/12.0*xyy+8875.0/24.0*yyy-...
                4375.0/12.0*xxxy-4375.0*0.25*xxyy-4375.0*0.25*xyyy-4375.0/12.0*yyyy+3125.0/24.0*xxxxy+ ...
                3125.0*ONESIXTH*xxxyy+3125.0*0.25*xxyyy+3125.0*ONESIXTH*xyyyy+3125.0/24.0*yyyyy;
    phi(12) = 25.0*x-1925.0/12.0*xx-1925.0/12.0*xy+8875.0/24.0*xxx+8875.0/12.0*xxy+8875.0/24.0*xyy-...
                4375.0/12.0*xxxx-4375.0*0.25*xxxy-4375.0*0.25*xxyy-4375.0/12.0*xyyy+3125.0/24.0*xxxxx+...
                3125.0*ONESIXTH*xxxxy+3125.0*0.25*xxxyy+3125.0*ONESIXTH*xxyyy+3125.0/24.0*xyyyy;
    phi(13) = -25.0*x+2675.0/12.0*xx+1175.0/12.0*xy-7375.0/12.0*xxx-8875.0/12.0*xxy-125.0*xyy+...
                 8125.0/12.0*xxxx+5625.0*0.25*xxxy+3125.0*0.25*xxyy+625.0/12.0*xyyy-3125.0/12.0*xxxxx-...
                 3125.0*0.25*xxxxy-3125.0*0.25*xxxyy-3125.0/12.0*xxyyy;
    phi(14) = 50.0*ONETHIRD*x-162.5*xx-37.5*xy+6125.0/12.0*xxx+3875.0/12.0*xxy+125.0*ONESIXTH*xyy-625.0*xxxx-...
                 3125.0*0.25*xxxy-625.0*0.25*xxyy+3125.0/12.0*xxxxx+3125.0*ONESIXTH*xxxxy+3125.0/12.0*xxxyy;
    phi(15) = -25.0*0.25*x+1525.0/24.0*xx+25.0*0.25*xy-5125.0/24.0*xxx-1375.0/24.0*xxy+6875.0/24.0*xxxx+...
                 625.0*0.25*xxxy-3125.0/24.0*xxxxx-3125.0/24.0*xxxxy;
    phi(16) = 250.0*xy-5875.0*ONESIXTH*xxy-5875.0*ONESIXTH*xyy+1250.0*xxxy+2500.0*xxyy+1250.0*xyyy-3125.0*ONESIXTH*xxxxy-...
                 1562.5*xxxyy-1562.5*xxyyy-3125.0*ONESIXTH*xyyyy;
    phi(17) = -125.0*xy+3625.0*0.25*xxy+1125.0*0.25*xyy-1562.5*xxxy-6875.0*0.25*xxyy-625.0*0.25*xyyy+...
                 3125.0*0.25*xxxxy+1562.5*xxxyy+3125.0*0.25*xxyyy;
    phi(18) = 125.0*ONETHIRD*xy-2125.0*ONESIXTH*xxy-125.0*ONETHIRD*xyy+2500.0*ONETHIRD*xxxy+312.5*xxyy-3125.0*ONESIXTH*xxxxy-3125.0*ONESIXTH*xxxyy;
    phi(19) = -125.0*xy+1125.0*0.25*xxy+3625.0*0.25*xyy-625.0*0.25*xxxy-6875.0*0.25*xxyy-1562.5*xyyy+...
                 3125.0*0.25*xxxyy+1562.5*xxyyy+3125.0*0.25*xyyyy;
    phi(20) = 125.0*0.25*xy-187.5*xxy-187.5*xyy+625.0*0.25*xxxy+4375.0*0.25*xxyy+...
                 625.0*0.25*xyyy-3125.0*0.25*xxxyy-3125.0*0.25*xxyyy;
    phi(21) = 125.0*ONETHIRD*xy-125.0*ONETHIRD*xxy-2125.0*ONESIXTH*xyy+312.5*xxyy+2500.0*ONETHIRD*xyyy-...
                 3125.0*ONESIXTH*xxyyy-3125.0*ONESIXTH*xyyyy;
end
