
clc;
s = sym('s','real');
t = sym('t','real');
u = sym('u','real');
v = sym('v','real');

fid1 = fopen('BasisFunctionSpacetime_Pentatope_Lagrange_autogen.h','w');
fid2 = fopen('BasisFunctionSpacetime_Pentatope_Lagrange_known.h','w');

fprintf(fid2,'// Solution Adaptive Numerical Simulator (SANS)\n');
fprintf(fid2,'// Copyright 2013-2016, Massachusetts Institute of Technology\n');
fprintf(fid2,'// Licensed under The GNU Lesser General Public License, version 2.1\n');
fprintf(fid2,'// See http://www.opensource.org/licenses/lgpl-2.1.php\n');
fprintf(fid2,'\n');

% PX node ordering
PX_DOF_P1= PX_ORDER(1);
PX_DOF_P2= PX_ORDER(2);
PX_DOF_P3= PX_ORDER(3);
PX_DOF_P4= PX_ORDER(4);

% generate the SANS ordering
SANS_DOF_P1= zeros(5, 4);
SANS_DOF_P1(2:end, :)= eye(4);

% based on edge ordering
SANS_EDGE= [0   1
            0   2
            0   3
            0   4
            1   2
            1   3
            1   4
            2   3
            2   4
            3   4];
SANS_TRI= [0    1   2
           0    1   3
           0    1   4
           0    2   3
           0    2   4
           0    3   4
           1    2   3
           1    2   4
           1    3   4
           2    3   4];
SANS_TRACE= [1  2   4   3
             0  2   3   4
             0  1   4   3
             0  1   2   4
             0  1   3   2];

% add the dofs for P2
SANS_DOF_P2= zeros(10, 4);
for i= 1:size(SANS_EDGE, 1)
    SANS_DOF_P2(i, :)= 0.5*(SANS_DOF_P1(SANS_EDGE(i, 1) + 1, :) ...
        + SANS_DOF_P1(SANS_EDGE(i, 2) + 1, :));
end

% add the edge dofs for P3
SANS_DOF_P3_EDGE= zeros(20, 4);
for i= 1:size(SANS_EDGE, 1)
    SANS_DOF_P3_EDGE(2*(i - 1) + 1, :)= 2/3*SANS_DOF_P1(SANS_EDGE(i, 1) + 1, :) ...
        + 1/3*SANS_DOF_P1(SANS_EDGE(i, 2) + 1, :);
    SANS_DOF_P3_EDGE(2*(i - 1) + 2, :)= 1/3*SANS_DOF_P1(SANS_EDGE(i, 1) + 1, :) ...
        + 2/3*SANS_DOF_P1(SANS_EDGE(i, 2) + 1, :);
end

% add the triangle dofs for P3
SANS_DOF_P3_TRI= zeros(10, 4);
for i= 1:size(SANS_TRI, 1)
    SANS_DOF_P3_TRI(i, :)= 1/3*(SANS_DOF_P1(SANS_TRI(i, 1) + 1, :) ...
        + SANS_DOF_P1(SANS_TRI(i, 2) + 1, :) + SANS_DOF_P1(SANS_TRI(i, 3) + 1, :));
end

% add the edge dofs for P4
SANS_DOF_P4_EDGE= zeros(30, 4);
for i= 1:size(SANS_EDGE, 1)
    SANS_DOF_P4_EDGE(3*(i - 1) + 1, :)= 3/4*SANS_DOF_P1(SANS_EDGE(i, 1) + 1, :) ...
        + 1/4*SANS_DOF_P1(SANS_EDGE(i, 2) + 1, :);
    SANS_DOF_P4_EDGE(3*(i - 1) + 2, :)= 1/2*SANS_DOF_P1(SANS_EDGE(i, 1) + 1, :) ...
        + 1/2*SANS_DOF_P1(SANS_EDGE(i, 2) + 1, :);
    SANS_DOF_P4_EDGE(3*(i - 1) + 3, :)= 1/4*SANS_DOF_P1(SANS_EDGE(i, 1) + 1, :) ...
        + 3/4*SANS_DOF_P1(SANS_EDGE(i, 2) + 1, :);
end

% add the triangle dofs for P4
SANS_DOF_P4_TRI= zeros(30, 4); % 3 per triangle, 10 triangles
for i= 1:size(SANS_TRI, 1)
    SANS_DOF_P4_TRI(3*(i - 1) + 1, :)= 3/4*mean(SANS_DOF_P1(SANS_TRI(i, :) + 1, :)) ...
        + 1/4*SANS_DOF_P1(SANS_TRI(i, 1) + 1, :);
    SANS_DOF_P4_TRI(3*(i - 1) + 2, :)= 3/4*mean(SANS_DOF_P1(SANS_TRI(i, :) + 1, :)) ...
        + 1/4*SANS_DOF_P1(SANS_TRI(i, 2) + 1, :);
    SANS_DOF_P4_TRI(3*(i - 1) + 3, :)= 3/4*mean(SANS_DOF_P1(SANS_TRI(i, :) + 1, :)) ...
        + 1/4*SANS_DOF_P1(SANS_TRI(i, 3) + 1, :);
end

% add the tet dofs for P4
SANS_DOF_P4_TET= zeros(5, 4);
for i= 1:size(SANS_TRACE, 1)
    SANS_DOF_P4_TET(i, :)= 1/4*(SANS_DOF_P1(SANS_TRACE(i, 1) + 1, :) ...
        + SANS_DOF_P1(SANS_TRACE(i, 2) + 1, :) ...
        + SANS_DOF_P1(SANS_TRACE(i, 3) + 1, :) ...
        + SANS_DOF_P1(SANS_TRACE(i, 4) + 1, :));
end

% stack nodes then edges etc
SANS_DOF_P2= [SANS_DOF_P1; SANS_DOF_P2];
SANS_DOF_P3= [SANS_DOF_P1; SANS_DOF_P3_EDGE; SANS_DOF_P3_TRI];
SANS_DOF_P4= [SANS_DOF_P1; SANS_DOF_P4_EDGE; SANS_DOF_P4_TRI; SANS_DOF_P4_TET];

% make sure we're matching sizes
assert(size(PX_DOF_P2, 1) == size(SANS_DOF_P2, 1));
assert(size(PX_DOF_P2, 2) == size(SANS_DOF_P2, 2));
assert(size(PX_DOF_P3, 1) == size(SANS_DOF_P3, 1));
assert(size(PX_DOF_P3, 2) == size(SANS_DOF_P3, 2));
assert(size(PX_DOF_P4, 1) == size(SANS_DOF_P4, 1));
assert(size(PX_DOF_P4, 2) == size(SANS_DOF_P4, 2));

% create a map from PX to SANS ordering (1-indexed)
SANS_PX_LEGEND_P2= zeros(size(PX_DOF_P2, 1), 1);
SANS_PX_LEGEND_P3= zeros(size(PX_DOF_P3, 1), 1);
SANS_PX_LEGEND_P4= zeros(size(PX_DOF_P4, 1), 1);

for i= 1:length(SANS_PX_LEGEND_P2)
    [~, idx]= ismember(PX_DOF_P2, SANS_DOF_P2(i, :), 'rows');
    SANS_PX_LEGEND_P2(i)= find(idx);
end

for i= 1:length(SANS_PX_LEGEND_P3)
    [~, idx]= ismember(PX_DOF_P3, SANS_DOF_P3(i, :), 'rows');
    SANS_PX_LEGEND_P3(i)= find(idx);
end

for i= 1:length(SANS_PX_LEGEND_P4)
    [~, idx]= ismember(PX_DOF_P4, SANS_DOF_P4(i, :), 'rows');
    SANS_PX_LEGEND_P4(i)= find(idx);
end

assert(all(sort(SANS_PX_LEGEND_P2) == (1:15)'));
assert(all(sort(SANS_PX_LEGEND_P3) == (1:35)'));
assert(all(sort(SANS_PX_LEGEND_P4) == (1:70)'));

% p1
p=1;
phi = setup_phi(1);
phi(1) = 1 - s - t - u;
phi(2) = s;
phi(3) = t;
phi(4) = u;
phi(5) = v;

[phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv] = get_derivs(phi,s,t,u,v);
print_c(fid1,p,phi,phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv);

% % unit test 1
% s0 = .25;
% t0 = .5;
% u0 = 0.2;
% print_c_unit(fid2,'lagrange_tet_p1_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);

% p2
p=2;
phi= lagrange_P2(s,t,u,v, SANS_PX_LEGEND_P2);
[phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv]= get_derivs(phi,s,t,u,v);
print_c(fid1,p,phi,phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv);

% % unit test 1
% s0 = .25;
% t0 = .5;
% u0 = 0.2;
% print_c_unit(fid2,'lagrange_tet_p2_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);

% p3
p=3;
phi = lagrange_P3(s,t,u,v, SANS_PX_LEGEND_P3);
[phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv]= get_derivs(phi,s,t,u,v);
print_c(fid1,p,phi,phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv);

% % % unit test 1
% % s0 = .25;
% % t0 = .5;
% % u0 = 0.2;
% % print_c_unit(fid2,'lagrange_tet_p3_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);

% p4
p=4;
phi = lagrange_P4(s,t,u,v, SANS_PX_LEGEND_P4);
[phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv]= get_derivs(phi,s,t,u,v);
print_c(fid1,p,phi,phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv);

% % % unit test 1
% % s0 = .25;
% % t0 = .5;
% % u0 = 0.2;
% % print_c_unit(fid2,'lagrange_tet_p4_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);
% 
% 
% % % p5
% % p=5;
% % phi = lagrange_p5(s,t,u);
% % [phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu] = get_derivs(phi,s,t,u);
% % print_c(fid1,p,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu);
% %
% % % unit test 1
% % s0 = .25;
% % t0 = .5;
% % u0 = 0.2;
% % print_c_unit(fid2,'lagrange_tet_p5_test1',s0,t0,u0,phi,phis,phit,phiu,phiss,phist,phitt,phisu,phitu,phiuu,s,t,u);

fclose(fid1);
fclose(fid2);

fprintf(['thanks for running this script. if you''re doing so, you might\n' ...
    'be changing the 4d basis functions. if that''s the case, you need\n' ...
    'to guarantee consistency everywhere that is dependent on the choice\n' ...
    'of ordering herein implied. for everything to match the resulting\n' ...
    'output files, in BasisFunctionSpacetime_Lagrange_autogen.h, the\n' ...
    'following conditions should be met:\n\n']);

fprintf(['first off, in SANS/src/BasisFunction/TraceToCellRefCoord_Pentatope\n' ...
    'you should have:\n\n']);

fprintf(['TraceToCellRefCoord::TraceNodes:\n\t{']);
fprintf('{%d, %d, %d, %d}, ', SANS_TRACE');
fprintf('\b\b}\naround line 28.\n\n');

fprintf(['ElementEdges<Pentatope>::EdgeNodes:\n\t{']);
fprintf('{%d, %d}, ', SANS_EDGE');
fprintf('\b\b}\naround line 38.\n\n');

fprintf(['ElementFrame<Pentatope>::FrameNodes:\n\t{']);
fprintf('{%d, %d, %d}, ', SANS_TRI');
fprintf('\b\b}\naround line 43.\n\n');

fprintf(['in SANS/src/BasisFunction/LagrangeDOFMap.h, you should have:\n\n']);
fprintf('p= 2, itrace= 0\n');
fprintf('\ttrace[%d]= %d\n', [3 + (1:6)', find(sum(SANS_DOF_P2(6:end, :), 2) == 1) + 4]');
fprintf('\np= 2, itrace= 1\n');
fprintf('\ttrace[%d]= %d\n', [3 + (1:6)', find(SANS_DOF_P2(6:end, 1) == 0) + 4]');
fprintf('\np= 2, itrace= 2\n');
fprintf('\ttrace[%d]= %d\n', [3 + (1:6)', find(SANS_DOF_P2(6:end, 2) == 0) + 4]');
fprintf('\np= 2, itrace= 3\n');
fprintf('\ttrace[%d]= %d\n', [3 + (1:6)', find(SANS_DOF_P2(6:end, 3) == 0) + 4]');
fprintf('\np= 2, itrace= 4\n');
fprintf('\ttrace[%d]= %d\n', [3 + (1:6)', find(SANS_DOF_P2(6:end, 4) == 0) + 4]');

function phi = setup_phi(p)

np  = (p + 1)*(p + 2)*(p + 3)*(p + 4)/24;
phi = sym('phi',[np,1]);

end

function [phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv] = get_derivs(phi,s,t,u,v)
phis = diff(phi,s);
phit = diff(phi,t);
phiu = diff(phi,u);
phiv = diff(phi,v);
phiss = diff(phis,s);
phist = diff(phis,t);
phitt = diff(phit,t);
phisu = diff(phis,u);
phitu = diff(phit,u);
phiuu = diff(phiu,u);
phisv = diff(phis,v);
phitv = diff(phit,v);
phiuv = diff(phiu,v);
phivv = diff(phiv,v);
end

function phi= lagrange_P2(s,t,u,v,PXtoSANS)

phiPX= setup_phi(2);
x= s;
y= t;
z= u;
w= v;

phiPX(1) = w*-3.0-x*3.0-y*3.0-z*3.0+w*x*4.0+w*y*4.0+w*z*4.0+x*y*4.0+x*z*4.0+y*z*4.0+(w*w)*2.0+(x*x)*2.0+(y*y)*2.0+(z*z)*2.0+1.0;
phiPX(2) = x*(w+x+y+z-1.0)*-4.0;
phiPX(3) = x*(x*2.0-1.0);
phiPX(4) = y*(w+x+y+z-1.0)*-4.0;
phiPX(5) = x*y*4.0;
phiPX(6) = y*(y*2.0-1.0);
phiPX(7) = z*(w+x+y+z-1.0)*-4.0;
phiPX(8) = x*z*4.0;
phiPX(9) = y*z*4.0;
phiPX(10) = z*(z*2.0-1.0);
phiPX(11) = w*(w+x+y+z-1.0)*-4.0;
phiPX(12) = w*x*4.0;
phiPX(13) = w*y*4.0;
phiPX(14) = w*z*4.0;
phiPX(15) = w*(w*2.0-1.0);

phi= setup_phi(2);
for i= 1:length(phiPX)
    phi(i)= phiPX(PXtoSANS(i));
end

end

function phi= lagrange_P3(s,t,u,v, PXtoSANS)

phiPX= setup_phi(3);
x= s;
y= t;
z= u;
w= v;

phiPX(1)= w*(-1.1E1/2.0)-x*(1.1E1/2.0)-y*(1.1E1/2.0)-z*(1.1E1/2.0)+w*x*1.8E1+w*y*1.8E1+w*z*1.8E1+x*y*1.8E1+x*z*1.8E1+y*z*1.8E1-w*(x*x)*(2.7E1/2.0)-(w*w)*x*(2.7E1/2.0)-w*(y*y)*(2.7E1/2.0)-(w*w)*y*(2.7E1/2.0)-w*(z*z)*(2.7E1/2.0)-x*(y*y)*(2.7E1/2.0)-(w*w)*z*(2.7E1/2.0)-(x*x)*y*(2.7E1/2.0)-x*(z*z)*(2.7E1/2.0)-(x*x)*z*(2.7E1/2.0)-y*(z*z)*(2.7E1/2.0)-(y*y)*z*(2.7E1/2.0)+(w*w)*9.0-(w*w*w)*(9.0/2.0)+(x*x)*9.0-(x*x*x)*(9.0/2.0)+(y*y)*9.0-(y*y*y)*(9.0/2.0)+(z*z)*9.0-(z*z*z)*(9.0/2.0)-w*x*y*2.7E1-w*x*z*2.7E1-w*y*z*2.7E1-x*y*z*2.7E1+1.0;
phiPX(2)= x*(w*-5.0-x*5.0-y*5.0-z*5.0+w*x*6.0+w*y*6.0+w*z*6.0+x*y*6.0+x*z*6.0+y*z*6.0+(w*w)*3.0+(x*x)*3.0+(y*y)*3.0+(z*z)*3.0+2.0)*(9.0/2.0);
phiPX(3)= x*(x*3.0-1.0)*(w+x+y+z-1.0)*(-9.0/2.0);
phiPX(4)= (x*(x*-9.0+(x*x)*9.0+2.0))/2.0;
phiPX(5)= y*(w*-5.0-x*5.0-y*5.0-z*5.0+w*x*6.0+w*y*6.0+w*z*6.0+x*y*6.0+x*z*6.0+y*z*6.0+(w*w)*3.0+(x*x)*3.0+(y*y)*3.0+(z*z)*3.0+2.0)*(9.0/2.0);
phiPX(6)= x*y*(w+x+y+z-1.0)*-2.7E1;
phiPX(7)= x*y*(x*3.0-1.0)*(9.0/2.0);
phiPX(8)= y*(y*3.0-1.0)*(w+x+y+z-1.0)*(-9.0/2.0);
phiPX(9)= x*y*(y*3.0-1.0)*(9.0/2.0);
phiPX(10)= (y*(y*-9.0+(y*y)*9.0+2.0))/2.0;
phiPX(11)= z*(w*-5.0-x*5.0-y*5.0-z*5.0+w*x*6.0+w*y*6.0+w*z*6.0+x*y*6.0+x*z*6.0+y*z*6.0+(w*w)*3.0+(x*x)*3.0+(y*y)*3.0+(z*z)*3.0+2.0)*(9.0/2.0);
phiPX(12)= x*z*(w+x+y+z-1.0)*-2.7E1;
phiPX(13)= x*z*(x*3.0-1.0)*(9.0/2.0);
phiPX(14)= y*z*(w+x+y+z-1.0)*-2.7E1;
phiPX(15)= x*y*z*2.7E1;
phiPX(16)= y*z*(y*3.0-1.0)*(9.0/2.0);
phiPX(17)= z*(z*3.0-1.0)*(w+x+y+z-1.0)*(-9.0/2.0);
phiPX(18)= x*z*(z*3.0-1.0)*(9.0/2.0);
phiPX(19)= y*z*(z*3.0-1.0)*(9.0/2.0);
phiPX(20)= (z*(z*-9.0+(z*z)*9.0+2.0))/2.0;
phiPX(21)= w*(w*-5.0-x*5.0-y*5.0-z*5.0+w*x*6.0+w*y*6.0+w*z*6.0+x*y*6.0+x*z*6.0+y*z*6.0+(w*w)*3.0+(x*x)*3.0+(y*y)*3.0+(z*z)*3.0+2.0)*(9.0/2.0);
phiPX(22)= w*x*(w+x+y+z-1.0)*-2.7E1;
phiPX(23)= w*x*(x*3.0-1.0)*(9.0/2.0);
phiPX(24)= w*y*(w+x+y+z-1.0)*-2.7E1;
phiPX(25)= w*x*y*2.7E1;
phiPX(26)= w*y*(y*3.0-1.0)*(9.0/2.0);
phiPX(27)= w*z*(w+x+y+z-1.0)*-2.7E1;
phiPX(28)= w*x*z*2.7E1;
phiPX(29)= w*y*z*2.7E1;
phiPX(30)= w*z*(z*3.0-1.0)*(9.0/2.0);
phiPX(31)= w*(w*3.0-1.0)*(w+x+y+z-1.0)*(-9.0/2.0);
phiPX(32)= w*x*(w*3.0-1.0)*(9.0/2.0);
phiPX(33)= w*y*(w*3.0-1.0)*(9.0/2.0);
phiPX(34)= w*z*(w*3.0-1.0)*(9.0/2.0);
phiPX(35)= (w*(w*-9.0+(w*w)*9.0+2.0))/2.0;

phi= setup_phi(3);
for i= 1:length(phi)
    phi(i)= phiPX(PXtoSANS(i));
end

end

function phi= lagrange_P4(s,t,u,v,PXtoSANS)

phiPX= setup_phi(4);
x= s;
y= t;
z= u;
w= v;

phiPX(1) = w*(-2.5E1/3.0)-x*(2.5E1/3.0)-y*(2.5E1/3.0)-z*(2.5E1/3.0)+(w*w)*(x*x)*6.4E1+(w*w)*(y*y)*6.4E1+(w*w)*(z*z)*6.4E1+(x*x)*(y*y)*6.4E1+(x*x)*(z*z)*6.4E1+(y*y)*(z*z)*6.4E1+w*x*(1.4E2/3.0)+w*y*(1.4E2/3.0)+w*z*(1.4E2/3.0)+x*y*(1.4E2/3.0)+x*z*(1.4E2/3.0)+y*z*(1.4E2/3.0)-w*(x*x)*8.0E1-(w*w)*x*8.0E1+w*(x*x*x)*(1.28E2/3.0)+(w*w*w)*x*(1.28E2/3.0)-w*(y*y)*8.0E1-(w*w)*y*8.0E1+w*(y*y*y)*(1.28E2/3.0)+(w*w*w)*y*(1.28E2/3.0)-w*(z*z)*8.0E1-x*(y*y)*8.0E1-(w*w)*z*8.0E1-(x*x)*y*8.0E1+w*(z*z*z)*(1.28E2/3.0)+x*(y*y*y)*(1.28E2/3.0)+(w*w*w)*z*(1.28E2/3.0)+(x*x*x)*y*(1.28E2/3.0)-x*(z*z)*8.0E1-(x*x)*z*8.0E1+x*(z*z*z)*(1.28E2/3.0)+(x*x*x)*z*(1.28E2/3.0)-y*(z*z)*8.0E1-(y*y)*z*8.0E1+y*(z*z*z)*(1.28E2/3.0)+(y*y*y)*z*(1.28E2/3.0)+(w*w)*(7.0E1/3.0)-(w*w*w)*(8.0E1/3.0)+(w*w*w*w)*(3.2E1/3.0)+(x*x)*(7.0E1/3.0)-(x*x*x)*(8.0E1/3.0)+(x*x*x*x)*(3.2E1/3.0)+(y*y)*(7.0E1/3.0)-(y*y*y)*(8.0E1/3.0)+(y*y*y*y)*(3.2E1/3.0)+(z*z)*(7.0E1/3.0)-(z*z*z)*(8.0E1/3.0)+(z*z*z*z)*(3.2E1/3.0)+w*x*(y*y)*1.28E2+w*(x*x)*y*1.28E2+(w*w)*x*y*1.28E2+w*x*(z*z)*1.28E2+w*(x*x)*z*1.28E2+(w*w)*x*z*1.28E2+w*y*(z*z)*1.28E2+w*(y*y)*z*1.28E2+(w*w)*y*z*1.28E2+x*y*(z*z)*1.28E2+x*(y*y)*z*1.28E2+(x*x)*y*z*1.28E2-w*x*y*1.6E2-w*x*z*1.6E2-w*y*z*1.6E2-x*y*z*1.6E2+w*x*y*z*2.56E2+1.0;
phiPX(2) = x*(w*1.3E1+x*1.3E1+y*1.3E1+z*1.3E1-w*x*3.6E1-w*y*3.6E1-w*z*3.6E1-x*y*3.6E1-x*z*3.6E1-y*z*3.6E1+w*(x*x)*2.4E1+(w*w)*x*2.4E1+w*(y*y)*2.4E1+(w*w)*y*2.4E1+w*(z*z)*2.4E1+x*(y*y)*2.4E1+(w*w)*z*2.4E1+(x*x)*y*2.4E1+x*(z*z)*2.4E1+(x*x)*z*2.4E1+y*(z*z)*2.4E1+(y*y)*z*2.4E1-(w*w)*1.8E1+(w*w*w)*8.0-(x*x)*1.8E1+(x*x*x)*8.0-(y*y)*1.8E1+(y*y*y)*8.0-(z*z)*1.8E1+(z*z*z)*8.0+w*x*y*4.8E1+w*x*z*4.8E1+w*y*z*4.8E1+x*y*z*4.8E1-3.0)*(-1.6E1/3.0);
phiPX(3) = x*(x*4.0-1.0)*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*4.0;
phiPX(4) = x*(x*-6.0+(x*x)*8.0+1.0)*(w+x+y+z-1.0)*(-1.6E1/3.0);
phiPX(5) = (x*(x*2.2E1-(x*x)*4.8E1+(x*x*x)*3.2E1-3.0))/3.0;
phiPX(6) = y*(w*1.3E1+x*1.3E1+y*1.3E1+z*1.3E1-w*x*3.6E1-w*y*3.6E1-w*z*3.6E1-x*y*3.6E1-x*z*3.6E1-y*z*3.6E1+w*(x*x)*2.4E1+(w*w)*x*2.4E1+w*(y*y)*2.4E1+(w*w)*y*2.4E1+w*(z*z)*2.4E1+x*(y*y)*2.4E1+(w*w)*z*2.4E1+(x*x)*y*2.4E1+x*(z*z)*2.4E1+(x*x)*z*2.4E1+y*(z*z)*2.4E1+(y*y)*z*2.4E1-(w*w)*1.8E1+(w*w*w)*8.0-(x*x)*1.8E1+(x*x*x)*8.0-(y*y)*1.8E1+(y*y*y)*8.0-(z*z)*1.8E1+(z*z*z)*8.0+w*x*y*4.8E1+w*x*z*4.8E1+w*y*z*4.8E1+x*y*z*4.8E1-3.0)*(-1.6E1/3.0);
phiPX(7) = x*y*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*3.2E1;
phiPX(8) = x*y*(x*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(9) = x*y*(x*-6.0+(x*x)*8.0+1.0)*(1.6E1/3.0);
phiPX(10) = y*(y*4.0-1.0)*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*4.0;
phiPX(11) = x*y*(y*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(12) = x*y*(x*4.0-1.0)*(y*4.0-1.0)*4.0;
phiPX(13) = y*(y*-6.0+(y*y)*8.0+1.0)*(w+x+y+z-1.0)*(-1.6E1/3.0);
phiPX(14) = x*y*(y*-6.0+(y*y)*8.0+1.0)*(1.6E1/3.0);
phiPX(15) = (y*(y*2.2E1-(y*y)*4.8E1+(y*y*y)*3.2E1-3.0))/3.0;
phiPX(16) = z*(w*1.3E1+x*1.3E1+y*1.3E1+z*1.3E1-w*x*3.6E1-w*y*3.6E1-w*z*3.6E1-x*y*3.6E1-x*z*3.6E1-y*z*3.6E1+w*(x*x)*2.4E1+(w*w)*x*2.4E1+w*(y*y)*2.4E1+(w*w)*y*2.4E1+w*(z*z)*2.4E1+x*(y*y)*2.4E1+(w*w)*z*2.4E1+(x*x)*y*2.4E1+x*(z*z)*2.4E1+(x*x)*z*2.4E1+y*(z*z)*2.4E1+(y*y)*z*2.4E1-(w*w)*1.8E1+(w*w*w)*8.0-(x*x)*1.8E1+(x*x*x)*8.0-(y*y)*1.8E1+(y*y*y)*8.0-(z*z)*1.8E1+(z*z*z)*8.0+w*x*y*4.8E1+w*x*z*4.8E1+w*y*z*4.8E1+x*y*z*4.8E1-3.0)*(-1.6E1/3.0);
phiPX(17) = x*z*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*3.2E1;
phiPX(18) = x*z*(x*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(19) = x*z*(x*-6.0+(x*x)*8.0+1.0)*(1.6E1/3.0);
phiPX(20) = y*z*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*3.2E1;
phiPX(21) = x*y*z*(w+x+y+z-1.0)*-2.56E2;
phiPX(22) = x*y*z*(x*4.0-1.0)*3.2E1;
phiPX(23) = y*z*(y*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(24) = x*y*z*(y*4.0-1.0)*3.2E1;
phiPX(25) = y*z*(y*-6.0+(y*y)*8.0+1.0)*(1.6E1/3.0);
phiPX(26) = z*(z*4.0-1.0)*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*4.0;
phiPX(27) = x*z*(z*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(28) = x*z*(x*4.0-1.0)*(z*4.0-1.0)*4.0;
phiPX(29) = y*z*(z*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(30) = x*y*z*(z*4.0-1.0)*3.2E1;
phiPX(31) = y*z*(y*4.0-1.0)*(z*4.0-1.0)*4.0;
phiPX(32) = z*(z*-6.0+(z*z)*8.0+1.0)*(w+x+y+z-1.0)*(-1.6E1/3.0);
phiPX(33) = x*z*(z*-6.0+(z*z)*8.0+1.0)*(1.6E1/3.0);
phiPX(34) = y*z*(z*-6.0+(z*z)*8.0+1.0)*(1.6E1/3.0);
phiPX(35) = (z*(z*2.2E1-(z*z)*4.8E1+(z*z*z)*3.2E1-3.0))/3.0;
phiPX(36) = w*(w*1.3E1+x*1.3E1+y*1.3E1+z*1.3E1-w*x*3.6E1-w*y*3.6E1-w*z*3.6E1-x*y*3.6E1-x*z*3.6E1-y*z*3.6E1+w*(x*x)*2.4E1+(w*w)*x*2.4E1+w*(y*y)*2.4E1+(w*w)*y*2.4E1+w*(z*z)*2.4E1+x*(y*y)*2.4E1+(w*w)*z*2.4E1+(x*x)*y*2.4E1+x*(z*z)*2.4E1+(x*x)*z*2.4E1+y*(z*z)*2.4E1+(y*y)*z*2.4E1-(w*w)*1.8E1+(w*w*w)*8.0-(x*x)*1.8E1+(x*x*x)*8.0-(y*y)*1.8E1+(y*y*y)*8.0-(z*z)*1.8E1+(z*z*z)*8.0+w*x*y*4.8E1+w*x*z*4.8E1+w*y*z*4.8E1+x*y*z*4.8E1-3.0)*(-1.6E1/3.0);
phiPX(37) = w*x*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*3.2E1;
phiPX(38) = w*x*(x*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(39) = w*x*(x*-6.0+(x*x)*8.0+1.0)*(1.6E1/3.0);
phiPX(40) = w*y*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*3.2E1;
phiPX(41) = w*x*y*(w+x+y+z-1.0)*-2.56E2;
phiPX(42) = w*x*y*(x*4.0-1.0)*3.2E1;
phiPX(43) = w*y*(y*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(44) = w*x*y*(y*4.0-1.0)*3.2E1;
phiPX(45) = w*y*(y*-6.0+(y*y)*8.0+1.0)*(1.6E1/3.0);
phiPX(46) = w*z*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*3.2E1;
phiPX(47) = w*x*z*(w+x+y+z-1.0)*-2.56E2;
phiPX(48) = w*x*z*(x*4.0-1.0)*3.2E1;
phiPX(49) = w*y*z*(w+x+y+z-1.0)*-2.56E2;
phiPX(50) = w*x*y*z*2.56E2;
phiPX(51) = w*y*z*(y*4.0-1.0)*3.2E1;
phiPX(52) = w*z*(z*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(53) = w*x*z*(z*4.0-1.0)*3.2E1;
phiPX(54) = w*y*z*(z*4.0-1.0)*3.2E1;
phiPX(55) = w*z*(z*-6.0+(z*z)*8.0+1.0)*(1.6E1/3.0);
phiPX(56) = w*(w*4.0-1.0)*(w*-7.0-x*7.0-y*7.0-z*7.0+w*x*8.0+w*y*8.0+w*z*8.0+x*y*8.0+x*z*8.0+y*z*8.0+(w*w)*4.0+(x*x)*4.0+(y*y)*4.0+(z*z)*4.0+3.0)*4.0;
phiPX(57) = w*x*(w*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(58) = w*x*(w*4.0-1.0)*(x*4.0-1.0)*4.0;
phiPX(59) = w*y*(w*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(60) = w*x*y*(w*4.0-1.0)*3.2E1;
phiPX(61) = w*y*(w*4.0-1.0)*(y*4.0-1.0)*4.0;
phiPX(62) = w*z*(w*4.0-1.0)*(w+x+y+z-1.0)*-3.2E1;
phiPX(63) = w*x*z*(w*4.0-1.0)*3.2E1;
phiPX(64) = w*y*z*(w*4.0-1.0)*3.2E1;
phiPX(65) = w*z*(w*4.0-1.0)*(z*4.0-1.0)*4.0;
phiPX(66) = w*(w*-6.0+(w*w)*8.0+1.0)*(w+x+y+z-1.0)*(-1.6E1/3.0);
phiPX(67) = w*x*(w*-6.0+(w*w)*8.0+1.0)*(1.6E1/3.0);
phiPX(68) = w*y*(w*-6.0+(w*w)*8.0+1.0)*(1.6E1/3.0);
phiPX(69) = w*z*(w*-6.0+(w*w)*8.0+1.0)*(1.6E1/3.0);
phiPX(70) = (w*(w*2.2E1-(w*w)*4.8E1+(w*w*w)*3.2E1-3.0))/3.0;

phi= setup_phi(4);

for i= 1:length(phi)
    phi(i)= phiPX(PXtoSANS(i));
end

end

function print_c(fid,p,phi,phis,phit,phiu,phiv,phiss,phist,phitt,phisu,phitu,phiuu,phisv,phitv,phiuv,phivv)

fprintf(fid,'//----------------------------------------------------------------------------//\n');
fprintf(fid,'// Lagrange: P=%d\n',p);

print_header(fid,'Spacetime','Pentatope','Lagrange',p,0)
print_ccode(fid,'phi',phi);
print_closing(fid);

print_header(fid,'Spacetime','Pentatope','Lagrange',p,1)
print_ccode(fid,'phis',phis);
print_ccode(fid,'phit',phit);
print_ccode(fid,'phiu',phiu);
print_ccode(fid,'phiv',phiv);
print_closing(fid);

print_header(fid,'Spacetime','Pentatope','Lagrange',p,2)
print_ccode(fid,'phiss',phiss);
print_ccode(fid,'phist',phist);
print_ccode(fid,'phitt',phitt);
print_ccode(fid,'phisu',phisu);
print_ccode(fid,'phitu',phitu);
print_ccode(fid,'phiuu',phiuu);
print_ccode(fid,'phisv',phisv);
print_ccode(fid,'phitv',phitv);
print_ccode(fid,'phiuv',phiuv);
print_ccode(fid,'phivv',phivv);
print_closing(fid);

print_coordintes(fid,'Spacetime','Pentatope','Lagrange',p);

end

function print_header(fid,voltype,elemtype,basistype,p,nderiv)
if nderiv==0
    func_name = 'evalBasis';
    args_name = 'Real phi[]';
elseif nderiv==1
    func_name = 'evalBasisDerivative';
    args_name = 'Real phis[], Real phit[], Real phiu[], Real phiv[]';
elseif nderiv==2
    func_name = 'evalBasisHessianDerivative';
    args_name = 'Real phiss[], Real phist[], Real phitt[], Real phisu[], Real phitu[], Real phiuu[], Real phisv[], Real phitv[], Real phiuv[], Real phivv[]';
end
fprintf(fid,'\nvoid\nBasisFunction%s<%s,%s,%d>::%s(\n',voltype,elemtype,basistype,p,func_name);
fprintf(fid,'  const Real& s, const Real& t, const Real& u, const Real& v, const Int5&,\n  %s, int nphi) const\n',args_name);
fprintf(fid,'{\nSANS_ASSERT(nphi==%d);\n',(p+1)*(p+2)*(p+3)*(p+4)/24);
end

function print_closing(fid)
fprintf(fid,'\n}\n\n');
end

function print_coordintes(fid,voltype,elemtype,basistype,p)
fprintf(fid,'\nvoid\n');
fprintf(fid,'BasisFunction%s<%s,%s,%d>::coordinates(std::vector<Real>& s, std::vector<Real>& t, std::vector<Real>& u, std::vector<Real>& v) const\n',voltype,elemtype,basistype,p);
fprintf(fid,'{\n');
fprintf(fid,'  s = coords_s_;\n');
fprintf(fid,'  t = coords_t_;\n');
fprintf(fid,'  u = coords_u_;\n');
fprintf(fid,'  v = coords_v_;\n');
fprintf(fid,'}\n\n');
end

function print_ccode(fid,name,expr0)
maxline = 100; % max number of characters per line so vera doesn't complain
fprintf(fid,'\n  // %s\n',name);
for i=1:length(expr0)
    expr = strsplit(ccode(expr0(i)),'=');
    expr = expr{2};
    % the full expression
    expr = sprintf('%s[%d] = %s\n',name,i-1,expr);
    %disp(expr)
    n = length(expr);
    pos = 1;
    while pos<n
        if pos+maxline>n
            limit = n;
        else
            limit = pos +maxline;
            while (true)
                if limit==n
                    break;
                end
                if is_operator( expr(limit) )
                    break;
                end
                limit = limit +1;
            end
        end
        s = expr(pos:limit);
        if limit == n
            fprintf(fid,'%s',s);
        else
            fprintf(fid,'%s\n           ',s);
        end
        pos = limit +1;
    end
    %fprintf(fid,'  %s[%d] = %s\n',name,i-1,expr);
end
end

function result = is_operator( s )
result = false;
if strcmp(s,'-')==1
    result = true;
elseif strcmp(s,'+')==1
    result = true;
elseif strcmp(s,'*')==1
    result = true;
elseif strcmp(s,'/')==1
    result = true;
end
end

function PX_DOF= PX_ORDER(qorder)

l =1;
for i= 0:qorder
    didivqorder= i/qorder;
    for j= 0:qorder - i
        djdivqorder= j/qorder;
        for k= 0:qorder - i - j
            dkdivqorder= k/qorder;
            dk= 0.0;
            for m= 0:qorder - i - j - k
                x(l) = dk/qorder;
                y(l) = dkdivqorder;
                z(l) = djdivqorder;
                w(l) = didivqorder;
                l= l + 1;
                dk= dk + 1.0;
            end
        end
    end
end

% print the PX node ordering
PX_DOF= [x' y' z' w'];

end
