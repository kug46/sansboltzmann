// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTION_REFELEMENT_SPLIT_TRACETOCELL_H
#define BASISFUNCTION_REFELEMENT_SPLIT_TRACETOCELL_H

// Transformation of reference coordinates on traces between sub-element to the
// reference coordinates on the main-element

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"

#include "RefElement_Split_Types.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

template <class TopoDim, class Topology>
class BasisFunction_RefElement_Split_TraceToCell;


template <>
class BasisFunction_RefElement_Split_TraceToCell<TopoD0, Node>
{
public:
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordTrace;
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordCell;

  static void
  transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type, int split_edge_index,
            int sub_cell_index, RefCoordCell& Refcoord_cell)
  {
    SANS_DEVELOPER_EXCEPTION("BasisFunction_RefElement_Split_TraceToCell<TopoD0,Node> - Cannot split a node. Code should not get here.");
  }
};

template <>
class BasisFunction_RefElement_Split_TraceToCell<TopoD1, Line>
{
public:
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordTrace;
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordCell;

  static void
  transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type, int split_edge_index,
            int sub_cell_index, RefCoordCell& Refcoord_cell);
};

template <>
class BasisFunction_RefElement_Split_TraceToCell<TopoD2, Triangle>
{
public:
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordTrace;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordCell;

  static void
  transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type, int split_edge_index,
            int sub_cell_index, RefCoordCell& Refcoord_cell);
};

template <>
class BasisFunction_RefElement_Split_TraceToCell<TopoD2, Quad>
{
public:
  typedef DLA::VectorS<TopoD1::D,Real> RefCoordTrace;
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordCell;

  static void
  transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type, int split_edge_index,
            int sub_cell_index, RefCoordCell& Refcoord_cell);
};

template <>
class BasisFunction_RefElement_Split_TraceToCell<TopoD3, Tet>
{
public:
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordTrace;
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordCell;

  static void
  transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type, int split_edge_index,
            int sub_cell_index, RefCoordCell& Refcoord_cell);
};

template <>
class BasisFunction_RefElement_Split_TraceToCell<TopoD3, Hex>
{
public:
  typedef DLA::VectorS<TopoD2::D,Real> RefCoordTrace;
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordCell;

  static void
  transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type, int split_edge_index,
            int sub_cell_index, RefCoordCell& Refcoord_cell);
};

template <>
class BasisFunction_RefElement_Split_TraceToCell<TopoD4, Pentatope>
{
public:
  typedef DLA::VectorS<TopoD3::D,Real> RefCoordTrace;
  typedef DLA::VectorS<TopoD4::D,Real> RefCoordCell;

  static void
  transform(const RefCoordTrace& Refcoord_trace, ElementSplitType split_type, int split_edge_index,
            int sub_cell_index, RefCoordCell& Refcoord_cell);
};

}

#endif  // BASISFUNCTION_REFELEMENT_SPLIT_TRACETOCELL_H
