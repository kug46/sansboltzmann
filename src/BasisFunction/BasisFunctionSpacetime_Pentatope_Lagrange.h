// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONSPACETIME_PENTATOPE_LAGRANGE_H
#define BASISFUNCTIONSPACETIME_PENTATOPE_LAGRANGE_H

// pentatope basis functions: Lagrange

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionSpacetime.h

#include <array>
#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionSpacetime.h"
#include "BasisFunctionCategory.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// volume basis functions: Lagrange
//
// reference pentatope element defined: s in [0, 1], t in [0, 1-s], u in [0, 1-s-t], v in [0, 1-s-t-u]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisArea      # basis functions associated with areas
//   .nBasisFace      # basis functions associated with traces
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionSpacetime_Pentatope_LagrangePMax= 4;

template<class Topology>
struct LagrangeNodes;

template<>
struct LagrangeNodes<Pentatope>
{
  static const int PMax= BasisFunctionSpacetime_Pentatope_LagrangePMax;

  static void get(const int order, std::vector<DLA::VectorS<TopoD4::D, Real>> &sRef);
};

void getLagrangeNodes_Pentatope(const int order, std::vector<Real> &s, std::vector<Real> &t, std::vector<Real> &u, std::vector<Real> &v);

//----------------------------------------------------------------------------//
// Lagrange: linear

template <>
class BasisFunctionSpacetime<Pentatope, Lagrange, 1> :
  public BasisFunctionSpacetimeBase<Pentatope>
{
public:
  static const int D= 4;     // physical dimensions

  typedef std::array<int, 5> Int5;

  virtual ~BasisFunctionSpacetime() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 5; }
  virtual int nBasisNode() const { return 5; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisArea() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis(const Real &sRef,
                         const Real &tRef,
                         const Real &uRef,
                         const Real &vRef,
                         const Int5 &,
                         Real phi[],
                         int nphi) const;
  virtual void evalBasisDerivative(const Real &sRef,
                                   const Real &tRef,
                                   const Real &uRef,
                                   const Real &vRef,
                                   const Int5 &,
                                   Real phis[],
                                   Real phit[],
                                   Real phiu[],
                                   Real phiv[],
                                   int nphi) const;
  virtual void evalBasisHessianDerivative(const Real &sRef,
                                          const Real &tRef,
                                          const Real &uRef,
                                          const Real &vRef,
                                          const Int5 &,
                                          Real phiss[],
                                          Real phist[], Real phitt[],
                                          Real phisu[], Real phitu[], Real phiuu[],
                                          Real phisv[], Real phitv[], Real phiuv[], Real phivv[],
                                          int nphi) const;

  void coordinates(std::vector<Real> &s,
                   std::vector<Real> &t,
                   std::vector<Real> &u,
                   std::vector<Real> &v) const;

protected:
  //Singleton!
  BasisFunctionSpacetime() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
  static const std::vector<Real> coords_u_;
  static const std::vector<Real> coords_v_;
public:
  static const BasisFunctionSpacetime* self();
};

//----------------------------------------------------------------------------//
// Lagrange: P=2

template <>
class BasisFunctionSpacetime<Pentatope, Lagrange, 2> :
  public BasisFunctionSpacetimeBase<Pentatope>
{
public:
  static const int D= 4;     // physical dimensions

  typedef std::array<int, 5> Int5;

  virtual ~BasisFunctionSpacetime() {}

  virtual int order() const { return 2; }
  virtual int nBasis() const { return 15; }
  virtual int nBasisNode() const { return 5; }
  virtual int nBasisEdge() const { return 10; }
  virtual int nBasisArea() const { return 0; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis(const Real &sRef,
                         const Real &tRef,
                         const Real &uRef,
                         const Real &vRef,
                         const Int5 &,
                         Real phi[],
                         int nphi) const;
  virtual void evalBasisDerivative(const Real &sRef,
                                   const Real &tRef,
                                   const Real &uRef,
                                   const Real &vRef,
                                   const Int5 &,
                                   Real phis[],
                                   Real phit[],
                                   Real phiu[],
                                   Real phiv[],
                                   int nphi) const;
  virtual void evalBasisHessianDerivative(const Real &sRef,
                                          const Real &tRef,
                                          const Real &uRef,
                                          const Real &vRef,
                                          const Int5 &,
                                          Real phiss[],
                                          Real phist[], Real phitt[],
                                          Real phisu[], Real phitu[], Real phiuu[],
                                          Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi) const;

  void coordinates(std::vector<Real> &s,
                   std::vector<Real> &t,
                   std::vector<Real> &u,
                   std::vector<Real> &v) const;

protected:
  //Singleton!
  BasisFunctionSpacetime() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
  static const std::vector<Real> coords_u_;
  static const std::vector<Real> coords_v_;
public:
  static const BasisFunctionSpacetime* self();
};

//----------------------------------------------------------------------------//
// Lagrange: P=3

template <>
class BasisFunctionSpacetime<Pentatope, Lagrange, 3> :
  public BasisFunctionSpacetimeBase<Pentatope>
{
public:
  static const int D= 4;     // physical dimensions

  typedef std::array<int, 5> Int5;

  virtual ~BasisFunctionSpacetime() {}

  virtual int order() const { return 3; }
  virtual int nBasis() const { return 35; }
  virtual int nBasisNode() const { return 5; }
  virtual int nBasisEdge() const { return 20; }
  virtual int nBasisArea() const { return 10; }
  virtual int nBasisFace() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis(const Real &sRef, const Real &tRef, const Real &uRef, const Real &vRef, const Int5 &,
                         Real phi[], int nphi) const;
  virtual void evalBasisDerivative(const Real &sRef, const Real &tRef, const Real &uRef, const Real &vRef, const Int5 &,
                                   Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi) const;
  virtual void evalBasisHessianDerivative(const Real & sRef, const Real &tRef, const Real &uRef, const Real &vRef, const Int5 &,
                                          Real phiss[],
                                          Real phist[], Real phitt[],
                                          Real phisu[], Real phitu[], Real phiuu[],
                                          Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi) const;

  void coordinates(std::vector<Real> &s, std::vector<Real> &t, std::vector<Real> &u, std::vector<Real> &v) const;

protected:
  //Singleton!
  BasisFunctionSpacetime() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
  static const std::vector<Real> coords_u_;
  static const std::vector<Real> coords_v_;
public:
  static const BasisFunctionSpacetime* self();
};

//----------------------------------------------------------------------------//
// Lagrange: P=4

template <>
class BasisFunctionSpacetime<Pentatope, Lagrange, 4> :
 public BasisFunctionSpacetimeBase<Pentatope>
{
public:
 static const int D= 4;     // physical dimensions

 typedef std::array<int, 5> Int5;

 virtual ~BasisFunctionSpacetime() {}

 virtual int order() const { return 4; }
 virtual int nBasis() const { return 70; }
 virtual int nBasisNode() const { return 5; }
 virtual int nBasisEdge() const { return 30; }
 virtual int nBasisArea() const { return 30; }
 virtual int nBasisFace() const { return 5; }
 virtual int nBasisCell() const { return 0; }
 virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

 virtual void evalBasis(const Real &sRef, const Real &tRef, const Real &uRef, const Real &vRef, const Int5 &,
                        Real phi[], int nphi) const;
 virtual void evalBasisDerivative(const Real &sRef, const Real &tRef, const Real &uRef, const Real &vRef, const Int5 &,
                                  Real phis[], Real phit[], Real phiu[], Real phiv[], int nphi) const;
 virtual void evalBasisHessianDerivative(const Real & sRef, const Real &tRef, const Real &uRef, const Real &vRef, const Int5 &,
                                         Real phiss[],
                                         Real phist[], Real phitt[],
                                         Real phisu[], Real phitu[], Real phiuu[],
                                         Real phisv[], Real phitv[], Real phiuv[], Real phivv[], int nphi) const;

 void coordinates(std::vector<Real> &s, std::vector<Real> &t, std::vector<Real> &u, std::vector<Real> &v) const;

protected:
 //Singleton!
 BasisFunctionSpacetime() {}
 static const std::vector<Real> coords_s_;
 static const std::vector<Real> coords_t_;
 static const std::vector<Real> coords_u_;
 static const std::vector<Real> coords_v_;
public:
 static const BasisFunctionSpacetime* self();
};

}

#endif  // BASISFUNCTIONVOLUME_TETRAHEDRON_LAGRANGE_H
