// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionArea_Triangle_RBS.h"

#include <cmath>  // sqrt
#include "tools/SANSException.h"

namespace SANS
{
// Node Coordinates are in Barycentric Coordinates based on the referance triangle
/*

Triangle Q1:            Triangle Q2:         Triangle Q3:          Triangle Q4:

t
^                                                                   2
|                                                                   | \
2                       2                    2                      6   5
|`\                     |`\                  | \                    |     \
|  `\                   |  `\                5   4                  7 (14)  4
|    `\                 4    `3              |     \                |         \
|      `\               |      `\            6  (9)  3              8 (12) (13) 3
|        `\             |        `\          |         \            |             \
0----------1 --> s      0-----5----1         0---7---8---1          0---9--10--11---1

*/


//---------------------------------------------------------------------------//

// RBS: Linear

void
BasisFunctionArea<Triangle,RBS,1>::evalBasis( const Real& s, const Real& t, const Int3&, Real phi[], int nphi) const
{
  SANS_ASSERT(nphi == 3);

  Real b1;
  Real b2;
  Real b3;

  b1 = -s + (1 - t);
  b2 = s;
  b3 = 1 - b1 - b2;

  phi[0] = b1;
  phi[1] = b2;
  phi[2] = b3;



}

void
BasisFunctionArea<Triangle,RBS,1>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phis[0] = -1;
  phis[1] = 1;
  phis[2] = 0;

  phit[0] = -1;
  phit[1] = 0;
  phit[2] = 1;
}

void
BasisFunctionArea<Triangle,RBS,1>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 0;

  phist[0] = 0;
  phist[1] = 0;
  phist[2] = 0;

  phitt[0] = 0;
  phitt[1] = 0;
  phitt[2] = 0;
}


//---------------------------------------------------------------------------//

// RBS: Quadratic

void
BasisFunctionArea<Triangle,RBS,2>::evalBasis( const Real& s, const Real& t, const Int3&, Real phi[], int nphi) const
{
  SANS_ASSERT(nphi == 6);

  Real b1;
  Real b2;
  Real b3;

  b1 = -s + (1 - t);
  b2 = s;
  b3 = 1 - b1 - b2;
  Real Denom = b1*b1 + b2*b2 + b3*b3 + 1/3* 2*b2*b3 + 1/3*2*b1*b3 + 1/3*2*b1*b2;


  phi[0] = b1*b1/Denom;
  phi[1] = b2*b2/Denom;
  phi[2] = b3*b3/Denom;
  phi[3] = 1/3*2*b2*b3/Denom;
  phi[4] = 1/3*2*b1*b3/Denom;
  phi[5] = 1/3*2*b1*b2/Denom;


}

void
BasisFunctionArea<Triangle,RBS,2>::evalBasisDerivative(
    const Real& s, const Real& t, const Int3&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 6);

  Real b1;
  Real b2;
  Real b3;

  b1 = -s + (1 - t);
  b2 = s;
  b3 = 1 - b1 - b2;

  phis[0] = -(6*b1*(3*b2*b2 + 3*b3*b3 + 2*b3*b1 + b1*b1 + 2*b2*(b3 + 2*b1)))/((3*b2*b2 +
            3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 +
            3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
  phis[1] = (6*b2*(b2*b2 + 3*b3*b3 + 2*b3/b1 + 3*b1*b1 + 2*b2*(b3 + 2*b1)))/((3*b2*b2 + 3*b3*b3 +
            2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 + 3*b3*b3 +
            2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
  phis[2] = -(12*b3*b3*(b2 - b1))/((3*b2*b2 + 3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*
             (3*b2*b2 + 3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
  phis[3] = (2*b3*(-b2*b2 + 3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + 3*b1)))/((3*b2*b2 + 3*b3*b3 +
      2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 + 3*b3*b3 +
          2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
  phis[4] = -(2*b3*(3*b2*b2 + 3*b3*b3 + 2*b3*b1 - b1*b1 + 2*b2*(b3 + 3*b1)))/((3*b2*b2 +
      3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 +
          3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
  phis[5] = (2*(b2 - b1)*(3*b2*b2 + 3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + 3*b1)))/((3*b2*b2 +
      3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 +
          3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));

  phit[0] = -((6*b1*(3*b2*b2 + 3*b3*b3 + 4*b3*b1 + b1*b1 + 2*b2*(b3 + b1)))/((3*b2*b2 + 3*b3*b3 +
      2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 + 3*b3*b3 +
          2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))));
  phit[1] = -(12*b2*b2*(b3 - b1))/((3*b2*b2 + 3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*
             (3*b2*b2 + 3*b3*b3 + 2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
  phit[2] = (6*b3*(3*b2*b2 + b3*b3 + 4*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)))/((3*b2*b2 + 3*b3*b3 +
            2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 + 3*b3*b3 +
            2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
  phit[3] = (2*b2*(3*b2*b2 - b3*b3 + 6*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)))/((3*b2*b2 + 3*b3*b3 +
      2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 + 3*b3*b3 +
          2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
  phit[4] = -(2*(b3 - b1)*(3*b2*b2 + 2*b2*(b3 + b1) + 3*(b3 + b1)*(b3 + b1)))/((3*b2*b2 + 3*b3*b3 +
      2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 + 3*b3*b3 +
          2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
  phit[5] = -(2*b2*(3*b2*b2 + 3*b3*b3 + 6*b3*b1 - b1*b1 + 2*b2*(b3 + b1)))/((3*b2*b2 + 3*b3*b3 +
          2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1))*(3*b2*b2 + 3*b3*b3 +
          2*b3*b1 + 3*b1*b1 + 2*b2*(b3 + b1)));
}

void
BasisFunctionArea<Triangle,RBS,2>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int3&, Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phiss[0] = 0;
  phiss[1] = 0;
  phiss[2] = 0;

  phist[0] = 0;
  phist[1] = 0;
  phist[2] = 0;

  phitt[0] = 0;
  phitt[1] = 0;
  phitt[2] = 0;
}
}
