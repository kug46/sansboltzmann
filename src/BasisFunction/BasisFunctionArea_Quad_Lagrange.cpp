// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionArea_Quad_Lagrange.h"

// Construct tensor products with the linear Lagrange basis
#include "BasisFunctionLine_Lagrange.h"

#include "tools/SANSException.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

/*
//    Quad Q1:                  Quad Q2               Quad Q3                 Quad Q4
//
//       t
//       ^                                                                 3--12--11--10---2
//       |                                                                 |               |
//  3----------2             3----6-----2          3---9----8---2         13  19  22  18   9
//  |    |     |             |          |          |            |          |               |
//  |    |     |             |          |         10  15   14   7         14  23  24  21   8
//  |    +-----|---> s       7    8     5          |            |          |               |
//  |          |             |          |         11  12   13   6         15  16  20  17   7
//  |          |             |          |          |            |          |               |
//  0----------1             0----4-----1          0---4----5---1          0---4---5---6---1
 */

#define O3 1./3. // One over 3
#define T3 2./3. // Two over 3

#define Q4 1./4. // Quarter
#define H2 1./2. // Half
#define T4 3./4. // Three quarters

#define COORD(P, c) const std::vector<Real> BasisFunctionArea<Quad,Lagrange,P>::coords_ ## c ## _

COORD(1, s) = {0.0, 1.0, 1.0, 0.0};
COORD(1, t) = {0.0, 0.0, 1.0, 1.0};

COORD(2, s) = {0.0, 1.0, 1.0, 0.0, 0.5, 1.0, 0.5, 0.0, 0.5};
COORD(2, t) = {0.0, 0.0, 1.0, 1.0, 0.0, 0.5, 1.0, 0.5, 0.5};

       //SANS:  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15
COORD(3, s) = {0., 1., 1., 0., O3, T3, 1., 1., T3, O3, 0., 0., O3, T3, T3, O3};
COORD(3, t) = {0., 0., 1., 1., 0., 0., O3, T3, 1., 1., T3, O3, O3, O3, T3, T3};

       //SANS:  0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24
COORD(4, s) = {0., 1., 1., 0., Q4, H2, T4, 1., 1., 1., T4, H2, Q4, 0., 0., 0., Q4, T4, T4, Q4, H2, T4, H2, Q4, H2};
COORD(4, t) = {0., 0., 1., 1., 0., 0., 0., Q4, H2, T4, 1., 1., 1., T4, H2, Q4, Q4, Q4, T4, T4, Q4, H2, T4, H2, H2};

void getLagrangeNodes_Quad(const int order, std::vector<Real>& s, std::vector<Real>& t)
{
  switch (order)
  {
    case 1:
    {
      const BasisFunctionArea<Quad,Lagrange,1>* basisP1 = BasisFunctionArea<Quad,Lagrange,1>::self();
      basisP1->coordinates(s, t);
      break;
    }
    case 2:
    {
      const BasisFunctionArea<Quad,Lagrange,2>* basisP2 = BasisFunctionArea<Quad,Lagrange,2>::self();
      basisP2->coordinates(s, t);
      break;
    }
    case 3:
    {
      const BasisFunctionArea<Quad,Lagrange,3>* basisP3 = BasisFunctionArea<Quad,Lagrange,3>::self();
      basisP3->coordinates(s, t);
      break;
    }
    case 4:
    {
      const BasisFunctionArea<Quad,Lagrange,4>* basisP4 = BasisFunctionArea<Quad,Lagrange,4>::self();
      basisP4->coordinates(s, t);
      break;
    }

    default:
      SANS_DEVELOPER_EXCEPTION( "getLagrangeNodes_Quad - Unsupported order = %d.", order );
  }
}

//---------------------------------------------------------------------------//
const int LagrangeNodes<Quad>::PMax;

//---------------------------------------------------------------------------//
void
LagrangeNodes<Quad>::
get(const int order, std::vector<DLA::VectorS<TopoD2::D,Real>>& sRef)
{
  std::vector<Real> s, t;
  getLagrangeNodes_Quad(order, s, t);
  sRef.resize(s.size());

  for (std::size_t i = 0; i < s.size(); i++)
  {
    sRef[i][0] = s[i];
    sRef[i][1] = t[i];
  }
}

//----------------------------------------------------------------------------//
// Lagrange: linear

void
BasisFunctionArea<Quad,Lagrange,1>::evalBasis( const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phi[0] = (1 - s)*(1 - t); // 1 @ node 0 (s = 0, t = 0)
  phi[1] = (    s)*(1 - t); // 1 @ node 1 (s = 1, t = 0)
  phi[2] = (    s)*(    t); // 1 @ node 2 (s = 1, t = 1)
  phi[3] = (1 - s)*(    t); // 1 @ node 3 (s = 0, t = 1)
}


void
BasisFunctionArea<Quad,Lagrange,1>::evalBasisDerivative( const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phis[0] = (  - 1)*(1 - t); // 1 @ node 0 (s = 0, t = 0)
  phis[1] = (    1)*(1 - t); // 1 @ node 1 (s = 1, t = 0)
  phis[2] = (    1)*(    t); // 1 @ node 2 (s = 1, t = 1)
  phis[3] = (  - 1)*(    t); // 1 @ node 3 (s = 0, t = 1)

  phit[0] = (1 - s)*(  - 1); // 1 @ node 0 (s = 0, t = 0)
  phit[1] = (    s)*(  - 1); // 1 @ node 1 (s = 1, t = 0)
  phit[2] = (    s)*(    1); // 1 @ node 2 (s = 1, t = 1)
  phit[3] = (1 - s)*(    1); // 1 @ node 3 (s = 0, t = 1)
}

void
BasisFunctionArea<Quad,Lagrange,1>::evalBasisHessianDerivative( const Real& s, const Real& t, const Int4&,
    Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  phiss[0] = 0; // 1 @ node 0 (s = 0, t = 0)
  phiss[1] = 0; // 1 @ node 1 (s = 1, t = 0)
  phiss[2] = 0; // 1 @ node 2 (s = 1, t = 1)
  phiss[3] = 0; // 1 @ node 3 (s = 0, t = 1)

  phist[0] =  1; // 1 @ node 0 (s = 0, t = 0)
  phist[1] = -1; // 1 @ node 1 (s = 1, t = 0)
  phist[2] =  1; // 1 @ node 2 (s = 1, t = 1)
  phist[3] = -1; // 1 @ node 3 (s = 0, t = 1)

  phitt[0] = 0; // 1 @ node 0 (s = 0, t = 0)
  phitt[1] = 0; // 1 @ node 1 (s = 1, t = 0)
  phitt[2] = 0; // 1 @ node 2 (s = 1, t = 1)
  phitt[3] = 0; // 1 @ node 3 (s = 0, t = 1)
}

void
BasisFunctionArea<Quad,Lagrange,1>::coordinates( std::vector<Real>& s, std::vector<Real>& t ) const
{
  s = coords_s_;
  t = coords_t_;
}


//----------------------------------------------------------------------------//
// Lagrange: Quadratic

void
BasisFunctionArea<Quad,Lagrange,2>::tensorProduct(
    const Real sphi[], const Real tphi[], Real phi[] ) const
{
  phi[0] = sphi[0]*tphi[0]; // 1 @ node 0 (s = 0, t = 0)
  phi[1] = sphi[1]*tphi[0]; // 1 @ node 1 (s = 1, t = 0)
  phi[2] = sphi[1]*tphi[1]; // 1 @ node 2 (s = 1, t = 1)
  phi[3] = sphi[0]*tphi[1]; // 1 @ node 3 (s = 0, t = 1)

  phi[4] = sphi[2]*tphi[0]; // 1 @ node 4 (s = 0.5, t = 0.0)
  phi[5] = sphi[1]*tphi[2]; // 1 @ node 5 (s = 1.0, t = 0.5)
  phi[6] = sphi[2]*tphi[1]; // 1 @ node 6 (s = 0.5, t = 1.0)
  phi[7] = sphi[0]*tphi[2]; // 1 @ node 7 (s = 0.0, t = 0.5)

  phi[8] = sphi[2]*tphi[2]; // 1 @ node 8 (s = 0.5, t = 0.5)
}

void
BasisFunctionArea<Quad,Lagrange,2>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 9);

  Real sphi[3], tphi[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(t, tphi, 3);

  tensorProduct(sphi, tphi, phi);
}


void
BasisFunctionArea<Quad,Lagrange,2>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 9);

  Real sphi[3], tphi[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(t, tphi, 3);

  Real sphis[3], tphit[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(s, sphis, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(t, tphit, 3);

  tensorProduct(sphis, tphi , phis);
  tensorProduct(sphi , tphit, phit);
}

void
BasisFunctionArea<Quad,Lagrange,2>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int4&,
    Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 9);

  Real sphi[3], tphi[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(s, sphi, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasis(t, tphi, 3);

  Real sphis[3], tphit[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(s, sphis, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasisDerivative(t, tphit, 3);

  Real sphiss[3], tphitt[3];
  BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(s, sphiss, 3);
  BasisFunctionLine<Lagrange,2>::self()->evalBasisHessianDerivative(t, tphitt, 3);

  tensorProduct(sphiss, tphi  , phiss);
  tensorProduct(sphis , tphit , phist);
  tensorProduct(sphi  , tphitt, phitt);
}

void
BasisFunctionArea<Quad,Lagrange,2>::coordinates( std::vector<Real>& s, std::vector<Real>& t ) const
{
  s = coords_s_;
  t = coords_t_;
}


//----------------------------------------------------------------------------//
// Lagrange: Cubic

void
BasisFunctionArea<Quad,Lagrange,3>::tensorProduct(
    const Real sphi[], const Real tphi[], Real phi[] ) const
{
  phi[0] = sphi[0]*tphi[0]; // 1 @ node 0 (s = 0, t = 0)
  phi[1] = sphi[1]*tphi[0]; // 1 @ node 1 (s = 1, t = 0)
  phi[2] = sphi[1]*tphi[1]; // 1 @ node 2 (s = 1, t = 1)
  phi[3] = sphi[0]*tphi[1]; // 1 @ node 3 (s = 0, t = 1)

  // Edge 0 (t = 0)
  phi[4]  = sphi[2]*tphi[0]; // 1 @ node 4 (s = 1/3, t = 0.0)
  phi[5]  = sphi[3]*tphi[0]; // 1 @ node 5 (s = 2/3, t = 0.0)

  // Edge 1 (s = 1)
  phi[6]  = sphi[1]*tphi[2]; // 1 @ node 6 (s = 1.0, t = 1/3)
  phi[7]  = sphi[1]*tphi[3]; // 1 @ node 7 (s = 1.0, t = 2/3)

  // Edge 2 (t = 1)
  phi[8]  = sphi[3]*tphi[1]; // 1 @ node 8 (s = 2/3, t = 1.0)
  phi[9]  = sphi[2]*tphi[1]; // 1 @ node 9 (s = 1/3, t = 1.0)

  // Edge 3 (s = 0)
  phi[10] = sphi[0]*tphi[3]; // 1 @ node 10 (s = 0.0, t = 2/3)
  phi[11] = sphi[0]*tphi[2]; // 1 @ node 11 (s = 0.0, t = 1/3)

  // Face
  phi[12] = sphi[2]*tphi[2]; // 1 @ node 8 (s = 1/3, t = 1/3)
  phi[13] = sphi[3]*tphi[2]; // 1 @ node 8 (s = 2/3, t = 1/3)
  phi[14] = sphi[3]*tphi[3]; // 1 @ node 8 (s = 2/3, t = 2/3)
  phi[15] = sphi[2]*tphi[3]; // 1 @ node 8 (s = 1/3, t = 2/3)
}

void
BasisFunctionArea<Quad,Lagrange,3>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 16);

  Real sphi[4], tphi[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(t, tphi, 4);

  tensorProduct(sphi, tphi, phi);
}


void
BasisFunctionArea<Quad,Lagrange,3>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 16);

  Real sphi[4], tphi[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(t, tphi, 4);

  Real sphis[4], tphit[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(s, sphis, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(t, tphit, 4);

  tensorProduct(sphis, tphi , phis);
  tensorProduct(sphi , tphit, phit);
}

void
BasisFunctionArea<Quad,Lagrange,3>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int4&,
    Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 16);

  Real sphi[4], tphi[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(s, sphi, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasis(t, tphi, 4);

  Real sphis[4], tphit[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(s, sphis, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasisDerivative(t, tphit, 4);

  Real sphiss[4], tphitt[4];
  BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(s, sphiss, 4);
  BasisFunctionLine<Lagrange,3>::self()->evalBasisHessianDerivative(t, tphitt, 4);

  tensorProduct(sphiss, tphi  , phiss);
  tensorProduct(sphis , tphit , phist);
  tensorProduct(sphi  , tphitt, phitt);
}

void
BasisFunctionArea<Quad,Lagrange,3>::coordinates( std::vector<Real>& s, std::vector<Real>& t ) const
{
  s = coords_s_;
  t = coords_t_;
}


//----------------------------------------------------------------------------//
// Lagrange: P=4

void
BasisFunctionArea<Quad,Lagrange,4>::tensorProduct(
    const Real sphi[], const Real tphi[], Real phi[] ) const
{
  phi[0] = sphi[0]*tphi[0]; // 1 @ node 0 (s = 0, t = 0)
  phi[1] = sphi[1]*tphi[0]; // 1 @ node 1 (s = 1, t = 0)
  phi[2] = sphi[1]*tphi[1]; // 1 @ node 2 (s = 1, t = 1)
  phi[3] = sphi[0]*tphi[1]; // 1 @ node 3 (s = 0, t = 1)

  // Edge 0 (t = 0)
  phi[4]  = sphi[2]*tphi[0]; // 1 @ node 4 (s = 1/4, t = 0.0)
  phi[5]  = sphi[3]*tphi[0]; // 1 @ node 5 (s = 2/4, t = 0.0)
  phi[6]  = sphi[4]*tphi[0]; // 1 @ node 6 (s = 3/4, t = 0.0)

  // Edge 1 (s = 1)
  phi[7]  = sphi[1]*tphi[2]; // 1 @ node 7 (s = 1.0, t = 1/4)
  phi[8]  = sphi[1]*tphi[3]; // 1 @ node 8 (s = 1.0, t = 2/4)
  phi[9]  = sphi[1]*tphi[4]; // 1 @ node 9 (s = 1.0, t = 3/4)

  // Edge 2 (t = 1)
  phi[10]  = sphi[4]*tphi[1]; // 1 @ node 10 (s = 3/4, t = 1.0)
  phi[11]  = sphi[3]*tphi[1]; // 1 @ node 11 (s = 2/4, t = 1.0)
  phi[12]  = sphi[2]*tphi[1]; // 1 @ node 12 (s = 1/4, t = 1.0)

  // Edge 3 (s = 0)
  phi[13] = sphi[0]*tphi[4]; // 1 @ node 13 (s = 0.0, t = 3/4)
  phi[14] = sphi[0]*tphi[3]; // 1 @ node 14 (s = 0.0, t = 2/4)
  phi[15] = sphi[0]*tphi[2]; // 1 @ node 15 (s = 0.0, t = 1/4)

  // Face
  phi[16] = sphi[2]*tphi[2]; // 1 @ node 16 (s = 1/4, t = 1/4)
  phi[17] = sphi[4]*tphi[2]; // 1 @ node 17 (s = 3/4, t = 1/4)
  phi[18] = sphi[4]*tphi[4]; // 1 @ node 18 (s = 3/4, t = 3/4)
  phi[19] = sphi[2]*tphi[4]; // 1 @ node 19 (s = 1/4, t = 3/4)

  phi[20] = sphi[3]*tphi[2]; // 1 @ node 20 (s = 2/4, t = 1/4)
  phi[21] = sphi[4]*tphi[3]; // 1 @ node 21 (s = 3/4, t = 2/4)
  phi[22] = sphi[3]*tphi[4]; // 1 @ node 22 (s = 2/4, t = 3/4)
  phi[23] = sphi[2]*tphi[3]; // 1 @ node 23 (s = 1/4, t = 2/4)

  phi[24] = sphi[3]*tphi[3]; // 1 @ node 24 (s = 2/4, t = 2/4)
}

void
BasisFunctionArea<Quad,Lagrange,4>::evalBasis(
    const Real& s, const Real& t, const Int4&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 25);

  Real sphi[5], tphi[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(s, sphi, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(t, tphi, 5);

  tensorProduct(sphi, tphi, phi);
}


void
BasisFunctionArea<Quad,Lagrange,4>::evalBasisDerivative(
    const Real& s, const Real& t, const Int4&, Real phis[], Real phit[], int nphi ) const
{
  SANS_ASSERT(nphi == 25);

  Real sphi[5], tphi[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(s, sphi, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(t, tphi, 5);

  Real sphis[5], tphit[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(s, sphis, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(t, tphit, 5);

  tensorProduct(sphis, tphi , phis);
  tensorProduct(sphi , tphit, phit);
}

void
BasisFunctionArea<Quad,Lagrange,4>::evalBasisHessianDerivative(
    const Real& s, const Real& t, const Int4&,
    Real phiss[], Real phist[], Real phitt[], int nphi ) const
{
  SANS_ASSERT(nphi == 25);

  Real sphi[5], tphi[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(s, sphi, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasis(t, tphi, 5);

  Real sphis[5], tphit[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(s, sphis, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasisDerivative(t, tphit, 5);

  Real sphiss[5], tphitt[5];
  BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(s, sphiss, 5);
  BasisFunctionLine<Lagrange,4>::self()->evalBasisHessianDerivative(t, tphitt, 5);

  tensorProduct(sphiss, tphi  , phiss);
  tensorProduct(sphis , tphit , phist);
  tensorProduct(sphi  , tphitt, phitt);
}

void
BasisFunctionArea<Quad,Lagrange,4>::coordinates( std::vector<Real>& s, std::vector<Real>& t ) const
{
  s = coords_s_;
  t = coords_t_;
}
}
