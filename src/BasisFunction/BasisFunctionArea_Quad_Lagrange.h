// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONAREA_QUAD_LAGRANGE_H
#define BASISFUNCTIONAREA_QUAD_LAGRANGE_H

// quad basis functions: Lagrange

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionLine.h


#include <array>
#include <vector>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionArea.h"
#include "BasisFunctionCategory.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// area basis functions: Lagrange
//
// reference triangle element defined: s in [0, 1], t in [0, 1]
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionArea_Quad_LagrangePMax = 4;

template<class Topology>
struct LagrangeNodes;

template<>
struct LagrangeNodes<Quad>
{
  static const int PMax = BasisFunctionArea_Quad_LagrangePMax;

  static void get(const int order, std::vector<DLA::VectorS<TopoD2::D,Real>>& sRef);
};

void getLagrangeNodes_Quad(const int order, std::vector<Real>& s, std::vector<Real>& t);


//----------------------------------------------------------------------------//
// Lagrange: linear
//
// orthonormal (within factor of 2)
// basis functions normalized to give L2 norm equal quad area (1)

template <>
class BasisFunctionArea<Quad,Lagrange,1> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 1; }
  virtual int nBasis() const { return 4; }
  virtual int nBasisNode() const { return 4; }
  virtual int nBasisEdge() const { return 0; }
  virtual int nBasisCell() const { return 0; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
public:
  static const BasisFunctionArea* self();
};


template <>
class BasisFunctionArea<Quad,Lagrange,2> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 2; }
  virtual int nBasis() const { return 9; }
  virtual int nBasisNode() const { return 4; }
  virtual int nBasisEdge() const { return 4; }
  virtual int nBasisCell() const { return 1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
public:
  static const BasisFunctionArea* self();
};


template <>
class BasisFunctionArea<Quad,Lagrange,3> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 3; }
  virtual int nBasis() const { return 16; }
  virtual int nBasisNode() const { return 4; }
  virtual int nBasisEdge() const { return 8; }
  virtual int nBasisCell() const { return 4; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
public:
  static const BasisFunctionArea* self();
};

template <>
class BasisFunctionArea<Quad,Lagrange,4> :
    public BasisFunctionAreaBase<Quad>
{
public:
  static const int D = 2;     // physical dimensions

  typedef std::array<int,4> Int4;

  virtual ~BasisFunctionArea() {}

  virtual int order() const { return 4; }
  virtual int nBasis() const { return 25; }
  virtual int nBasisNode() const { return 4; }
  virtual int nBasisEdge() const { return 12; }
  virtual int nBasisCell() const { return 9; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Lagrange; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Int4&, Real phis[], Real phit[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Int4&,
      Real phiss[], Real phist[], Real phitt[], int nphi ) const;

  void coordinates( std::vector<Real>& s, std::vector<Real>& t ) const;

  void tensorProduct( const Real sphi[], const Real tphi[], Real phi[] ) const;

protected:
  //Singleton!
  BasisFunctionArea() {}
  static const std::vector<Real> coords_s_;
  static const std::vector<Real> coords_t_;
public:
  static const BasisFunctionArea* self();
};
}

#endif  // BASISFUNCTIONAREA_QUAD_LAGRANGE_H
