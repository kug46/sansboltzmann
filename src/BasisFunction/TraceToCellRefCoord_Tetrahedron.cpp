// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// reference element operators

#include <algorithm> // std::find
#include <sstream>

#include "ElementEdges.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "TraceToCellRefCoord.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Quadrature_Cache.h"

namespace SANS
{

// Each canonical trace is numbered based on the opposing node number

//----------------------------------------------------------------------------//
// cell reference coordinates (s,t,u) given triangle reference coordinate (s,t)
/*
        2
        |\
        | \
        |  \
        |   \
        |    \
        |     \
        |      \
        0 ----- 1
       /     .
      /   .
     / .
     3
*/
// parameters:
//  canonicalFace      face  in tet (F0=1-2-3, F1=0-3-2, F2=0-1-3, F3=0-2-1); positive is outward normal
//                     edges in tet (E0=2-3, E1=3-1, E2=1-2, E3=2-0, E4=0-3, E5=0-1);


template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
    TraceNodes[ CellTopology::NFace ][ TraceTopology::NNode ] = { {1,2,3}, {0,3,2}, {0,1,3}, {0,2,1} };

template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
    TraceEdges[ CellTopology::NFace ][ TraceTopology::NEdge ] = { {0,1,2}, {0,3,4}, {1,4,5}, {2,5,3} };

template<>
const int ElementEdges<Tet>::EdgeNodes[ Tet::NEdge ][ Line::NNode ] = { {2,3}, {3,1}, {1,2}, {2,0}, {0,3}, {0,1} };

template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
    OrientPos[ TraceTopology::NPermutation ][ TraceTopology::NNode ] = { {0,1,2}, {1,2,0}, {2,0,1} };

template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
    OrientNeg[ TraceTopology::NPermutation ][ TraceTopology::NNode ] = { {0,2,1}, {1,0,2}, {2,1,0} };

template<class TraceTopology, class CellTopology>
void
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
eval( const CanonicalTraceToCell& canonicalFace, const DLA::VectorS<2,Real>& sRefTrace, DLA::VectorS<3,Real>& sRefCell )
{
  eval(canonicalFace, sRefTrace[0], sRefTrace[1], sRefCell[0], sRefCell[1], sRefCell[2]);
}

template<class TraceTopology, class CellTopology>
void
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
eval( const CanonicalTraceToCell& canonicalFace, const QuadraturePoint<TopoD2>& sRefTrace, QuadratureCellTracePoint<TopoD3>& sRefCell )
{
  sRefCell.set(canonicalFace, sRefTrace);
  eval(canonicalFace, sRefTrace.ref[0], sRefTrace.ref[1], sRefCell.ref[0], sRefCell.ref[1], sRefCell.ref[2]);
}

template<class TraceTopology, class CellTopology>
void
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::eval( const CanonicalTraceToCell& canonicalFace,
                                                                const Real& sRefArea, const Real& tRefArea,
                                                                Real& sRefVol, Real& tRefVol, Real& uRefVol )
{
  Real s, t;
  Real s0 = sRefArea;
  Real t0 = tRefArea;

  // Swap s and t to correct the normal if needed
  if ( canonicalFace.orientation < 0 )
    std::swap(s0,t0);

  // Change the Area coordinates to match a face with the cannonical node order
  switch ( abs(canonicalFace.orientation) )
  {
  case 1:
  {
    s = s0;
    t = t0;
    break;
  }
  case 2:
  {
    s = 1.0 - s0 - t0;
    t = s0;
    break;
  }
  case 3:
  {
    s = t0;
    t = 1.0 - s0 - t0;
    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown orientation = %d", canonicalFace.orientation);
  }

  switch (canonicalFace.trace)
  {
  case 0:      // 1-2-3
  {
    sRefVol = 1 - s - t;
    tRefVol = s;
    uRefVol = t;
    break;
  }
  case 1:      // 0-3-2
  {
    sRefVol = 0;
    tRefVol = t;
    uRefVol = s;
    break;
  }
  case 2:      // 0-1-3
  {
    sRefVol = s;
    tRefVol = 0;
    uRefVol = t;
    break;
  }
  case 3:      // 0-2-1
  {
    sRefVol = t;
    tRefVol = s;
    uRefVol = 0;
    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION( "Unexpected canonicalFace.trace = %d", canonicalFace.trace );
  }

}

template<class TraceTopology, class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::getCanonicalTraceLeft(const int *triNodes, const int nTriNodes,
                                                                                const int *tetNodes, const int nTetNodes,
                                                                                int *canonicalTri, const int ncanonicalTri)
{
  SANS_ASSERT(nTriNodes == 3);
  SANS_ASSERT(nTetNodes == 4);
  SANS_ASSERT(ncanonicalTri == 3);

  // Find the node that is not part of the tetrahedron to define the canonical face
  int canonicalFace = -1;
  for ( int face = 0; face < Tet::NFace; face++ )
  {
    if ( std::find(triNodes, triNodes+nTriNodes, tetNodes[face] ) == triNodes+nTriNodes )
    {
      canonicalFace = face;
      break;
    }
  }

  SANS_ASSERT( canonicalFace != -1 );

  // Get the canonical triangle nodes
  for ( int n = 0; n < Triangle::NNode; n++)
    canonicalTri[n] = tetNodes[TraceNodes[canonicalFace][n]];

  return CanonicalTraceToCell(canonicalFace, 1);
}

template<class TraceTopology, class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::getCanonicalTrace(const int *triNodes, const int nTriNodes,
                                                                            const int *tetNodes, const int nTetNodes)
{
  SANS_ASSERT(nTriNodes == 3);
  SANS_ASSERT(nTetNodes == 4);

  int canonicalTri[3];
  int canonicalFace = getCanonicalTraceLeft(triNodes, nTriNodes, tetNodes, nTetNodes, canonicalTri, 3).trace;

  // Find the positive or negative orientation where the nodes on the triangle match the canonical triangle
  for ( int orient = 0; orient < TraceTopology::NEdge; orient++)
  {
    if ( triNodes[0] == canonicalTri[OrientPos[orient][0]] &&
         triNodes[1] == canonicalTri[OrientPos[orient][1]] &&
         triNodes[2] == canonicalTri[OrientPos[orient][2]] ) return CanonicalTraceToCell(canonicalFace, orient+1);

    if ( triNodes[0] == canonicalTri[OrientNeg[orient][0]] &&
         triNodes[1] == canonicalTri[OrientNeg[orient][1]] &&
         triNodes[2] == canonicalTri[OrientNeg[orient][2]] ) return CanonicalTraceToCell(canonicalFace, -(orient+1));
  }

  std::stringstream msg;
  msg << "Cannot find Triangle Tet canonical orientation." << std::endl;

  msg << "Triangle nodes = { ";
  for (int n = 0; n < Triangle::NNode-1; n++)
    msg << triNodes[n] << ", ";
  msg << triNodes[Triangle::NNode-1] << " } " << std::endl;

  msg << "Canonical Trace = " << canonicalFace << std::endl;
  msg << "Canonical Trace nodes = { ";
  for (int n = 0; n < Triangle::NNode-1; n++)
    msg << tetNodes[TraceNodes[canonicalFace][n]] << ", ";
  msg << tetNodes[TraceNodes[canonicalFace][Triangle::NNode-1]] << " } " << std::endl;

  msg << "Tet nodes = { ";
  for (int n = 0; n < Tet::NNode-1; n++)
    msg << tetNodes[n] << ", ";
  msg << tetNodes[Tet::NNode-1] << " } " << std::endl;


  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return CanonicalTraceToCell();
}

template<class TraceTopology>
int
CanonicalOrientation<TraceTopology, TopoD3>::get(const int *triNodes, const int nTriNodes,
                                                 const int *canonicalTriNodes, const int nCanonicalTriNodes)
{
  SANS_ASSERT(nTriNodes == 3);
  SANS_ASSERT(nCanonicalTriNodes == 3);

  const int (*OrientPos)[ TraceTopology::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientPos;
  const int (*OrientNeg)[ TraceTopology::NTrace ] = TraceToCellRefCoord<Triangle, TopoD3, Tet>::OrientNeg;

  // Find the positive or negative orientation where the nodes on the triangle match the canonical triangle
  for ( int orient = 0; orient < TraceTopology::NEdge; orient++)
  {
    if ( triNodes[0] == canonicalTriNodes[OrientPos[orient][0]] &&
         triNodes[1] == canonicalTriNodes[OrientPos[orient][1]] &&
         triNodes[2] == canonicalTriNodes[OrientPos[orient][2]] ) return orient+1;

    if ( triNodes[0] == canonicalTriNodes[OrientNeg[orient][0]] &&
         triNodes[1] == canonicalTriNodes[OrientNeg[orient][1]] &&
         triNodes[2] == canonicalTriNodes[OrientNeg[orient][2]] ) return -(orient+1);
  }

  std::stringstream msg;
  msg << "Cannot find Triangle canonical orientation." << std::endl;

  msg << "Triangle nodes = { ";
  for (int n = 0; n < Triangle::NNode-1; n++)
    msg << triNodes[n] << ", ";
  msg << triNodes[Triangle::NNode-1] << " } " << std::endl;

  msg << "Canonical Triangle nodes = { ";
  for (int n = 0; n < Triangle::NNode-1; n++)
    msg << canonicalTriNodes[n] << ", ";
  msg << canonicalTriNodes[Triangle::NNode-1] << " } " << std::endl;

  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return 0;
}

template<class TraceTopology, class CellTopology>
int
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::getCellEdge(const CanonicalTraceToCell& canonicalTrace,
                                                                      const int canonicalFrame)
{
  int face   = canonicalTrace.trace;
  int orient = canonicalTrace.orientation;

       if (orient==1)
    return TraceEdges[face][canonicalFrame];
  else if (orient==2)
    return TraceEdges[face][(canonicalFrame+1)%3];
  else if (orient==3)
    return TraceEdges[face][(canonicalFrame+2)%3];
  else if (orient== -1)
    return TraceEdges[face][(3-canonicalFrame)%3];
  else if (orient== -2)
    return TraceEdges[face][(4-canonicalFrame)%3];
  else if (orient== -3)
    return TraceEdges[face][(5-canonicalFrame)%3];

  std::stringstream msg;
  msg << "Cannot find Tet edge for given Triangle orientation." << std::endl;
  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return -1;
}

// Explicitly instantiate the class
template struct TraceToCellRefCoord<Triangle, TopoD3, Tet>;
template struct CanonicalOrientation<Triangle, TopoD3>;

} //namespace SANS
