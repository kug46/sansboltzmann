// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// reference element operators

#include <algorithm> // std::find
#include <sstream>   // stringstream

#include "ElementEdges.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "TraceToCellRefCoord.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "Quadrature_Cache.h"

namespace SANS
{

//----------------------------------------------------------------------------//
/*
//         t
//  3----------2
//  |\     ^   |\
//  | \    |   | \
//  |  \   |   |  \
//  |   7------+---6
//  |   |  +-- |-- | -> s
//  0---+---\--1   |
//   \  |    \  \  |
//    \ |     \  \ |
//     \|      u  \|
//      4----------5
 */
// parameters:
//  canonicalFace face in hex (F0=0-3-2-1, u-min
//                             F1=0-1-5-4, t-min
//                             F2=1-2-6-5, s-max
//                             F3=3-7-6-2, t-max
//                             F4=0-4-7-3, s-min
//                             F5=4-5-6-7  u-max ); positive is outward normal
//                 edges in hex (E0=0-3,
//                               E1=3-2,
//                               E2=2-1,
//                               E3=1-0,
//                               E4=1-5,
//                               E5=5-4,
//                               E6=4-0,
//                               E7=2-6,
//                               E8=6-5,
//                               E9=4-7,
//                               E10=3-7,
//                               E11=7-6);


template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
    TraceNodes[ CellTopology::NFace ][ TraceTopology::NNode ] = { {0,3,2,1}, {0,1,5,4}, {1,2,6,5}, {3,7,6,2}, {0,4,7,3}, {4,5,6,7} };

template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
   TraceEdges[ CellTopology::NFace ][ TraceTopology::NEdge ] = { { 0,  1,  2,  3},   // F0
                                                                 { 3,  4,  5,  6},   // F1
                                                                 { 2,  7,  8,  4},   // F2
                                                                 {10, 11,  7,  1},   // F3
                                                                 { 6,  9, 10,  0},   // F4
                                                                 { 5,  8, 11,  9} }; // F5

template<>
const int ElementEdges<Hex>::
    EdgeNodes[ Hex::NEdge ][ Line::NNode ] = { {0,3},   // E0
                                               {3,2},   // E1
                                               {2,1},   // E2
                                               {1,0},   // E3
                                               {1,5},   // E4
                                               {5,4},   // E5
                                               {4,0},   // E6
                                               {2,6},   // E7
                                               {6,5},   // E8
                                               {4,7},   // E9
                                               {3,7},   // E10
                                               {7,6} }; // E11

template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
    OrientPos[ TraceTopology::NPermutation ][ TraceTopology::NNode ] = { {0,1,2,3}, {1,2,3,0}, {2,3,0,1}, {3,0,1,2} };

template<class TraceTopology, class CellTopology>
const int TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
    OrientNeg[ TraceTopology::NPermutation ][ TraceTopology::NNode ] = { {0,3,2,1}, {1,0,3,2}, {2,1,0,3}, {3,2,1,0} };

template<class TraceTopology, class CellTopology>
void
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
eval( const CanonicalTraceToCell& canonicalFace, const DLA::VectorS<2,Real>& sRefTrace, DLA::VectorS<3,Real>& sRefCell )
{
  eval(canonicalFace, sRefTrace[0], sRefTrace[1], sRefCell[0], sRefCell[1], sRefCell[2]);
}

template<class TraceTopology, class CellTopology>
void
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::
eval( const CanonicalTraceToCell& canonicalFace, const QuadraturePoint<TopoD2>& sRefTrace, QuadratureCellTracePoint<TopoD3>& sRefCell )
{
  sRefCell.set(canonicalFace, sRefTrace);
  eval(canonicalFace, sRefTrace.ref[0], sRefTrace.ref[1], sRefCell.ref[0], sRefCell.ref[1], sRefCell.ref[2]);
}

template<class TraceTopology, class CellTopology>
void
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::eval( const CanonicalTraceToCell& canonicalFace,
                                                                const Real& sRefArea, const Real& tRefArea,
                                                                Real& sRefVol, Real& tRefVol, Real& uRefVol )
{
  Real s, t;
  Real s0 = sRefArea;
  Real t0 = tRefArea;

  // Swap s and t to correct the normal if needed
  if ( canonicalFace.orientation < 0 )
    std::swap(s0,t0);

  // Change the Area coordinates to match a face with the cannonical node order
  switch ( abs(canonicalFace.orientation) )
  {
  case 1:
  {
    s = s0;
    t = t0;
    break;
  }
  case 2:
  {
    s = 1.0 - t0;
    t = s0;
    break;
  }
  case 3:
  {
    s = 1.0 - s0;
    t = 1.0 - t0;
    break;
  }
  case 4:
  {
    s = t0;
    t = 1.0 - s0;
    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown orientation = %d", canonicalFace.orientation);
  }


  switch (canonicalFace.trace)
  {
  case 0:      // F0=0-3-2-1, u-min
  {
    sRefVol = t;
    tRefVol = s;
    uRefVol = 0;
    break;
  }
  case 1:      // F1=0-1-5-4, t-min
  {
    sRefVol = s;
    tRefVol = 0;
    uRefVol = t;
    break;
  }
  case 2:      // F2=1-2-6-5, s-max
  {
    sRefVol = 1;
    tRefVol = s;
    uRefVol = t;
    break;
  }
  case 3:      // F3=3-7-6-2, t-max
  {
    sRefVol = t;
    tRefVol = 1;
    uRefVol = s;
    break;
  }
  case 4:      // F4=0-4-7-3, s-min
  {
    sRefVol = 0;
    tRefVol = t;
    uRefVol = s;
    break;
  }
  case 5:      // F5=4-5-6-7 u-max
  {
    sRefVol = s;
    tRefVol = t;
    uRefVol = 1;
    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION( "Unexpected canonicalFace.trace = %d", canonicalFace.trace );
  }

}

template<class TraceTopology, class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::getCanonicalTraceLeft(const int *quadNodes, const int nQuadNodes,
                                                                                const int *hexNodes, const int nHexNodes,
                                                                                int *canonicalQuad, const int nCanonicalQuad)
{
  SANS_ASSERT(nQuadNodes == 4);
  SANS_ASSERT(nHexNodes == 8);
  SANS_ASSERT(nCanonicalQuad == 4);

  // Find the node that is not part of the tetrahedron to define the canonical face
  int canonicalFace = -1;
  for ( int face = 0; face < Hex::NFace; face++ )
  {
    int cnt = 0;
    for ( int n = 0; n < Quad::NNode; n++ )
    {
      // If the node canonical node is not in the quad, then stop counting
      if ( std::find( quadNodes, quadNodes+nQuadNodes, hexNodes[TraceNodes[face][n]] ) == quadNodes+nQuadNodes )
        break;
      cnt++;
    }

    // If all nodes were found then we have the face
    if (cnt == Quad::NNode)
    {
      canonicalFace = face;
      break;
    }
  }

  if ( canonicalFace == -1 )
  {
    std::stringstream msg;
    msg << "Quad nodes do not make up a face of the hexahedron." << std::endl;

    msg << "Quad nodes = { ";
    for (int n = 0; n < 3; n++)
      msg << quadNodes[n] << ", ";
    msg << quadNodes[3] << " } " << std::endl;

    msg << "Hex nodes = { ";
    for (int n = 0; n < 7; n++)
      msg << hexNodes[n] << ", ";
    msg << hexNodes[7] << " } " << std::endl;

    msg << "Valid faces are:" << std::endl;

    for ( int face = 0; face < Hex::NFace; face++ )
    {
      msg << "face[" << face << "] = { ";
      for (int n = 0; n < 3; n++)
        msg << hexNodes[TraceNodes[face][n]] << ", ";
      msg << hexNodes[TraceNodes[face][3]] << " }" << std::endl;

    }
    SANS_DEVELOPER_EXCEPTION(msg.str());
  }

  // Get the canonical triangle nodes
  for ( int n = 0; n < Quad::NNode; n++)
    canonicalQuad[n] = hexNodes[TraceNodes[canonicalFace][n]];

  return CanonicalTraceToCell(canonicalFace, 1);
}

template<class TraceTopology, class CellTopology>
CanonicalTraceToCell
TraceToCellRefCoord<TraceTopology, TopoD3, CellTopology>::getCanonicalTrace(const int *quadNodes, const int nQuadNodes,
                                                                            const int *hexNodes, const int nHexNodes)
{
  SANS_ASSERT(nQuadNodes == 4);
  SANS_ASSERT(nHexNodes == 8);

  int canonicalQuad[4];
  int canonicalFace = getCanonicalTraceLeft(quadNodes, nQuadNodes, hexNodes, nHexNodes, canonicalQuad, 4).trace;

  // Find the positive or negative orientation where the nodes on the triangle match the canonical triangle
  for ( int orient = 0; orient < TraceTopology::NEdge; orient++)
  {
    if ( quadNodes[0] == canonicalQuad[OrientPos[orient][0]] &&
         quadNodes[1] == canonicalQuad[OrientPos[orient][1]] &&
         quadNodes[2] == canonicalQuad[OrientPos[orient][2]] &&
         quadNodes[3] == canonicalQuad[OrientPos[orient][3]]) return CanonicalTraceToCell(canonicalFace, orient+1);

    if ( quadNodes[0] == canonicalQuad[OrientNeg[orient][0]] &&
         quadNodes[1] == canonicalQuad[OrientNeg[orient][1]] &&
         quadNodes[2] == canonicalQuad[OrientNeg[orient][2]] &&
         quadNodes[3] == canonicalQuad[OrientNeg[orient][3]] ) return CanonicalTraceToCell(canonicalFace, -(orient+1));
  }

  std::stringstream msg;
  msg << "Cannot find Quad Hex canonical orientation." << std::endl;

  msg << "Quad nodes = { ";
  for (int n = 0; n < Quad::NNode-1; n++)
    msg << quadNodes[n] << ", ";
  msg << quadNodes[Quad::NNode-1] << " } " << std::endl;

  msg << "Canonical Trace = " << canonicalFace << std::endl;
  msg << "Canonical Trace nodes = { ";
  for (int n = 0; n < Quad::NNode-1; n++)
    msg << hexNodes[TraceNodes[canonicalFace][n]] << ", ";
  msg << hexNodes[TraceNodes[canonicalFace][Quad::NNode-1]] << " } " << std::endl;

  msg << "Hex nodes = { ";
  for (int n = 0; n < Hex::NNode-1; n++)
    msg << hexNodes[n] << ", ";
  msg << hexNodes[Hex::NNode-1] << " } " << std::endl;

  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return CanonicalTraceToCell();
}


template<class TraceTopology>
int
CanonicalOrientation<TraceTopology, TopoD3>::get(const int *quadNodes, const int nQuadNodes,
                                                 const int *canonicalQuadNodes, const int nCanonicalQuadNodes)
{
  SANS_ASSERT(nQuadNodes == 4);
  SANS_ASSERT(nCanonicalQuadNodes == 4);

  const int (*OrientPos)[ TraceTopology::NTrace ] = TraceToCellRefCoord<Quad, TopoD3, Hex>::OrientPos;
  const int (*OrientNeg)[ TraceTopology::NTrace ] = TraceToCellRefCoord<Quad, TopoD3, Hex>::OrientNeg;

  // Find the positive or negative orientation where the nodes on the triangle match the canonical triangle
  for ( int orient = 0; orient < TraceTopology::NEdge; orient++)
  {
    if ( quadNodes[0] == canonicalQuadNodes[OrientPos[orient][0]] &&
         quadNodes[1] == canonicalQuadNodes[OrientPos[orient][1]] &&
         quadNodes[2] == canonicalQuadNodes[OrientPos[orient][2]] &&
         quadNodes[3] == canonicalQuadNodes[OrientPos[orient][3]]) return orient+1;

    if ( quadNodes[0] == canonicalQuadNodes[OrientNeg[orient][0]] &&
         quadNodes[1] == canonicalQuadNodes[OrientNeg[orient][1]] &&
         quadNodes[2] == canonicalQuadNodes[OrientNeg[orient][2]] &&
         quadNodes[3] == canonicalQuadNodes[OrientNeg[orient][3]] ) return -(orient+1);
  }

  std::stringstream msg;
  msg << "Cannot find Quad Hex canonical orientation." << std::endl;

  msg << "Quad nodes = { ";
  for (int n = 0; n < Quad::NNode-1; n++)
    msg << quadNodes[n] << ", ";
  msg << quadNodes[Quad::NNode-1] << " } " << std::endl;

  msg << "Canonical Quad nodes = { ";
  for (int n = 0; n < Quad::NNode-1; n++)
    msg << canonicalQuadNodes[n] << ", ";
  msg << canonicalQuadNodes[Quad::NNode-1] << " } " << std::endl;

  SANS_DEVELOPER_EXCEPTION( msg.str() );
  return 0;
}

// Explicitly instantiate the class
template struct TraceToCellRefCoord<Quad, TopoD3, Hex>;
template struct CanonicalOrientation<Quad, TopoD3>;

} //namespace SANS
