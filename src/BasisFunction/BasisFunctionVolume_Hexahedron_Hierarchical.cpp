// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BasisFunctionVolume_Hexahedron_Hierarchical.h"
#include "tools/SANSException.h"

namespace SANS
{

void
BasisFunctionVolume<Hex,Hierarchical,1>::evalBasis( const Real& s, const Real& t, const Real& u, const Int6&, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  phi[0] = (1 - s)*(1 - t)*(1 - u); // 1 @ node 0 (s = 0, t = 0, u = 0)
  phi[1] = (    s)*(1 - t)*(1 - u); // 1 @ node 1 (s = 1, t = 0, u = 0)
  phi[2] = (    s)*(    t)*(1 - u); // 1 @ node 2 (s = 1, t = 1, u = 0)
  phi[3] = (1 - s)*(    t)*(1 - u); // 1 @ node 3 (s = 0, t = 1, u = 0)

  phi[4] = (1 - s)*(1 - t)*(    u); // 1 @ node 4 (s = 0, t = 0, u = 1)
  phi[5] = (    s)*(1 - t)*(    u); // 1 @ node 5 (s = 1, t = 0, u = 1)
  phi[6] = (    s)*(    t)*(    u); // 1 @ node 6 (s = 1, t = 1, u = 1)
  phi[7] = (1 - s)*(    t)*(    u); // 1 @ node 7 (s = 0, t = 1, u = 1)
}


void
BasisFunctionVolume<Hex,Hierarchical,1>::evalBasisDerivative( const Real& s, const Real& t, const Real& u, const Int6&,
                                                              Real phis[], Real phit[], Real phiu[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  phis[0] = (  - 1)*(1 - t)*(1 - u); // 1 @ node 0 (s = 0, t = 0, u = 0)
  phis[1] = (    1)*(1 - t)*(1 - u); // 1 @ node 1 (s = 1, t = 0, u = 0)
  phis[2] = (    1)*(    t)*(1 - u); // 1 @ node 2 (s = 1, t = 1, u = 0)
  phis[3] = (  - 1)*(    t)*(1 - u); // 1 @ node 3 (s = 0, t = 1, u = 0)
  phis[4] = (  - 1)*(1 - t)*(    u); // 1 @ node 4 (s = 0, t = 0, u = 1)
  phis[5] = (    1)*(1 - t)*(    u); // 1 @ node 5 (s = 1, t = 0, u = 1)
  phis[6] = (    1)*(    t)*(    u); // 1 @ node 6 (s = 1, t = 1, u = 1)
  phis[7] = (  - 1)*(    t)*(    u); // 1 @ node 7 (s = 0, t = 1, u = 1)

  phit[0] = (1 - s)*(  - 1)*(1 - u); // 1 @ node 0 (s = 0, t = 0, u = 0)
  phit[1] = (    s)*(  - 1)*(1 - u); // 1 @ node 1 (s = 1, t = 0, u = 0)
  phit[2] = (    s)*(    1)*(1 - u); // 1 @ node 2 (s = 1, t = 1, u = 0)
  phit[3] = (1 - s)*(    1)*(1 - u); // 1 @ node 3 (s = 0, t = 1, u = 0)
  phit[4] = (1 - s)*(  - 1)*(    u); // 1 @ node 4 (s = 0, t = 0, u = 1)
  phit[5] = (    s)*(  - 1)*(    u); // 1 @ node 5 (s = 1, t = 0, u = 1)
  phit[6] = (    s)*(    1)*(    u); // 1 @ node 6 (s = 1, t = 1, u = 1)
  phit[7] = (1 - s)*(    1)*(    u); // 1 @ node 7 (s = 0, t = 1, u = 1)

  phiu[0] = (1 - s)*(1 - t)*(  - 1); // 1 @ node 0 (s = 0, t = 0, u = 0)
  phiu[1] = (    s)*(1 - t)*(  - 1); // 1 @ node 1 (s = 1, t = 0, u = 0)
  phiu[2] = (    s)*(    t)*(  - 1); // 1 @ node 2 (s = 1, t = 1, u = 0)
  phiu[3] = (1 - s)*(    t)*(  - 1); // 1 @ node 3 (s = 0, t = 1, u = 0)
  phiu[4] = (1 - s)*(1 - t)*(    1); // 1 @ node 4 (s = 0, t = 0, u = 1)
  phiu[5] = (    s)*(1 - t)*(    1); // 1 @ node 5 (s = 1, t = 0, u = 1)
  phiu[6] = (    s)*(    t)*(    1); // 1 @ node 6 (s = 1, t = 1, u = 1)
  phiu[7] = (1 - s)*(    t)*(    1); // 1 @ node 7 (s = 0, t = 1, u = 1)
}

void
BasisFunctionVolume<Hex,Hierarchical,1>::evalBasisHessianDerivative( const Real& s, const Real& t, const Real& u, const Int6& sgn,
                                         Real phiss[],
                                         Real phist[], Real phitt[],
                                         Real phisu[], Real phitu[], Real phiuu[], int nphi ) const
{
  SANS_ASSERT(nphi == 8);

  phiss[0] = 0;                       // 1 @ node 0 (s = 0, t = 0, u = 0)
  phiss[1] = 0;                       // 1 @ node 1 (s = 1, t = 0, u = 0)
  phiss[2] = 0;                       // 1 @ node 2 (s = 1, t = 1, u = 0)
  phiss[3] = 0;                       // 1 @ node 3 (s = 0, t = 1, u = 0)
  phiss[4] = 0;                       // 1 @ node 4 (s = 0, t = 0, u = 1)
  phiss[5] = 0;                       // 1 @ node 5 (s = 1, t = 0, u = 1)
  phiss[6] = 0;                       // 1 @ node 6 (s = 1, t = 1, u = 1)
  phiss[7] = 0;                       // 1 @ node 7 (s = 0, t = 1, u = 1)

  phist[0] = (  - 1)*(  - 1)*(1 - u); // 1 @ node 0 (s = 0, t = 0, u = 0)
  phist[1] = (    1)*(  - 1)*(1 - u); // 1 @ node 1 (s = 1, t = 0, u = 0)
  phist[2] = (    1)*(    1)*(1 - u); // 1 @ node 2 (s = 1, t = 1, u = 0)
  phist[3] = (  - 1)*(    1)*(1 - u); // 1 @ node 3 (s = 0, t = 1, u = 0)
  phist[4] = (  - 1)*(  - 1)*(    u); // 1 @ node 4 (s = 0, t = 0, u = 1)
  phist[5] = (    1)*(  - 1)*(    u); // 1 @ node 5 (s = 1, t = 0, u = 1)
  phist[6] = (    1)*(    1)*(    u); // 1 @ node 6 (s = 1, t = 1, u = 1)
  phist[7] = (  - 1)*(    1)*(    u); // 1 @ node 7 (s = 0, t = 1, u = 1)

  phitt[0] = 0;                       // 1 @ node 0 (s = 0, t = 0, u = 0)
  phitt[1] = 0;                       // 1 @ node 1 (s = 1, t = 0, u = 0)
  phitt[2] = 0;                       // 1 @ node 2 (s = 1, t = 1, u = 0)
  phitt[3] = 0;                       // 1 @ node 3 (s = 0, t = 1, u = 0)
  phitt[4] = 0;                       // 1 @ node 4 (s = 0, t = 0, u = 1)
  phitt[5] = 0;                       // 1 @ node 5 (s = 1, t = 0, u = 1)
  phitt[6] = 0;                       // 1 @ node 6 (s = 1, t = 1, u = 1)
  phitt[7] = 0;                       // 1 @ node 7 (s = 0, t = 1, u = 1)

  phisu[0] = (  - 1)*(1 - t)*(  - 1); // 1 @ node 0 (s = 0, t = 0, u = 0)
  phisu[1] = (    1)*(1 - t)*(  - 1); // 1 @ node 1 (s = 1, t = 0, u = 0)
  phisu[2] = (    1)*(    t)*(  - 1); // 1 @ node 2 (s = 1, t = 1, u = 0)
  phisu[3] = (  - 1)*(    t)*(  - 1); // 1 @ node 3 (s = 0, t = 1, u = 0)
  phisu[4] = (  - 1)*(1 - t)*(    1); // 1 @ node 4 (s = 0, t = 0, u = 1)
  phisu[5] = (    1)*(1 - t)*(    1); // 1 @ node 5 (s = 1, t = 0, u = 1)
  phisu[6] = (    1)*(    t)*(    1); // 1 @ node 6 (s = 1, t = 1, u = 1)
  phisu[7] = (  - 1)*(    t)*(    1); // 1 @ node 7 (s = 0, t = 1, u = 1)

  phitu[0] = (1 - s)*(  - 1)*(  - 1); // 1 @ node 0 (s = 0, t = 0, u = 0)
  phitu[1] = (    s)*(  - 1)*(  - 1); // 1 @ node 1 (s = 1, t = 0, u = 0)
  phitu[2] = (    s)*(    1)*(  - 1); // 1 @ node 2 (s = 1, t = 1, u = 0)
  phitu[3] = (1 - s)*(    1)*(  - 1); // 1 @ node 3 (s = 0, t = 1, u = 0)
  phitu[4] = (1 - s)*(  - 1)*(    1); // 1 @ node 4 (s = 0, t = 0, u = 1)
  phitu[5] = (    s)*(  - 1)*(    1); // 1 @ node 5 (s = 1, t = 0, u = 1)
  phitu[6] = (    s)*(    1)*(    1); // 1 @ node 6 (s = 1, t = 1, u = 1)
  phitu[7] = (1 - s)*(    1)*(    1); // 1 @ node 7 (s = 0, t = 1, u = 1)

  phiuu[0] = 0;                       // 1 @ node 0 (s = 0, t = 0, u = 0)
  phiuu[1] = 0;                       // 1 @ node 1 (s = 1, t = 0, u = 0)
  phiuu[2] = 0;                       // 1 @ node 2 (s = 1, t = 1, u = 0)
  phiuu[3] = 0;                       // 1 @ node 3 (s = 0, t = 1, u = 0)
  phiuu[4] = 0;                       // 1 @ node 4 (s = 0, t = 0, u = 1)
  phiuu[5] = 0;                       // 1 @ node 5 (s = 1, t = 0, u = 1)
  phiuu[6] = 0;                       // 1 @ node 6 (s = 1, t = 1, u = 1)
  phiuu[7] = 0;                       // 1 @ node 7 (s = 0, t = 1, u = 1)
}

}
