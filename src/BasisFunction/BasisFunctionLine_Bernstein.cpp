// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <cmath>

#include "BasisFunctionLine_Bernstein.h"
#include "tools/SANSException.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Bernstein: constant

void
BasisFunctionLine<Bernstein,0>::evalBasis( const Real, Real phi[], int nphi ) const
{
  SANS_ASSERT_MSG(nphi == 1, "nphi = %d", nphi);

  phi[0] = 1;
}


void
BasisFunctionLine<Bernstein,0>::evalBasisDerivative( const Real, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phis[0] = 0;
}

void
BasisFunctionLine<Bernstein,0>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 1);

  phiss[0] = 0;
}

//----------------------------------------------------------------------------//
// Bernstein: linear

void
BasisFunctionLine<Bernstein,1>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phi[0] = 1 - s;
  phi[1] =     s;
}

void
BasisFunctionLine<Bernstein,1>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phis[0] = -1;
  phis[1] =  1;
}

void
BasisFunctionLine<Bernstein,1>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 2);

  phiss[0] = 0;
  phiss[1] = 0;
}


//----------------------------------------------------------------------------//
// Bernstein: quadratic

void
BasisFunctionLine<Bernstein,2>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phi[0] = (1 - s)*(1 - s);
  phi[1] = 2*s*(1 - s);
  phi[2] = s*s;
}

void
BasisFunctionLine<Bernstein,2>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phis[0] = 2*(s - 1);
  phis[1] = 2 - 4*s;
  phis[2] = 2*s;
}

void
BasisFunctionLine<Bernstein,2>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 3);

  phiss[0] =  2;
  phiss[1] = -4;
  phiss[2] =  2;
}


//----------------------------------------------------------------------------//
// Bernstein: cubic

void
BasisFunctionLine<Bernstein,3>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real t = 1 - s;

  phi[0] =   t*t*t;
  phi[1] = 3*t*t*s;
  phi[2] = 3*t*s*s;
  phi[3] =   s*s*s;
}

void
BasisFunctionLine<Bernstein,3>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real t = 1 - s;

  phis[0] = -3*t*t;
  phis[1] =  9*s*s - 12*s + 3;
  phis[2] =  3*(2 - 3*s)*s;
  phis[3] =  3*s*s;
}

void
BasisFunctionLine<Bernstein,3>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 4);

  Real t = 1 - s;

  phiss[0] =  6*t;
  phiss[1] = -3*(2 - 3*s);
  phiss[2] =  3*(2 - 3*s);
  phiss[3] =  6*s;
}


//----------------------------------------------------------------------------//
// Bernstein: quartic

void
BasisFunctionLine<Bernstein,4>::evalBasis( const Real s, Real phi[], int nphi ) const
{
  SANS_ASSERT(nphi == 5);

  Real t = 1 - s;

  phi[0] =   t*t*t*t;
  phi[1] = 4*s*t*t*t;
  phi[2] = 6*s*s*t*t;
  phi[3] = 4*s*s*s*t;
  phi[4] = s*s*s*s;
}

void
BasisFunctionLine<Bernstein,4>::evalBasisDerivative( const Real s, Real phis[], int nphi ) const
{
  SANS_ASSERT(nphi == 5);

  Real t = 1 - s;

  phis[0] = -4*t*t*t;
  phis[1] = -4*t*t*(4*s - 1);
  phis[2] =  8*s*(2*s*s - 3*s + 1);
  phis[3] =  4*(3 - 4*s)*s*s;
  phis[4] =  4*s*s*s;
}

void
BasisFunctionLine<Bernstein,4>::evalBasisHessianDerivative( const Real s, Real phiss[], int nphi ) const
{
  SANS_ASSERT(nphi == 5);

  Real t = 1 - s;

  phiss[0] = 12*t*t;
  phiss[1] = 24*(2*s*s - 3*s + 1);
  phiss[2] = 8*(6*s*s - 6*s + 1);
  phiss[3] = 24*s*(1 - 2*s);
  phiss[4] = 12*s*s;
}

}
