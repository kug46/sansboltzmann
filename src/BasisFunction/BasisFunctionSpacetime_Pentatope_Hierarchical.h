// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BASISFUNCTIONVOLUME_TETRAHEDRON_HIERARCHICAL_H
#define BASISFUNCTIONVOLUME_TETRAHEDRON_HIERARCHICAL_H

// tetrahedron basis functions: Hierarchical

// NOTES:
// - implemented via abstract base class
// - see notes in BasisFunctionVolume.h


#include <array>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/ElementTopology.h"

#include "BasisFunctionSpacetime.h"
#include "BasisFunctionCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// volume basis functions: Hierarchical
//
// reference pentatope element
//
// member functions:
//   .order           polynomial degree of basis function
//   .nBasis          total number of basis functions in element
//   .nBasisNode      # basis functions associated with nodes
//   .nBasisEdge      # basis functions associated with edges
//   .nBasisCell      # basis functions associated with cell interior
//   .evalBasis       basis function
//   .evalBasisDerivative  basis function derivatives wrt reference coordinates
//
//   .dump            debug dump of private data
//----------------------------------------------------------------------------//

static const int BasisFunctionSpacetime_Pentatope_HierarchicalPMax = 2;


//----------------------------------------------------------------------------//
// Hierarchical: linear

template <int P>
class BasisFunctionSpacetime<Pentatope,Hierarchical,P> :
  public BasisFunctionSpacetimeBase<Pentatope>
{
public:
  static const int D = 4;     // physical dimensions

  typedef std::array<int,5> Int4;

  virtual ~BasisFunctionSpacetime() {}

  virtual int order() const { return P; }
  virtual int nBasis() const { return -1; }
  virtual int nBasisNode() const { return -1; }
  virtual int nBasisEdge() const { return -1; }
  virtual int nBasisFace() const { return -1; }
  virtual int nBasisCell() const { return -1; }
  virtual BasisFunctionCategory category() const { return BasisFunctionCategory_Hierarchical; }

  virtual void evalBasis( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                          const Int4&, Real phi[], int nphi ) const;
  virtual void evalBasisDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                                    const Int4&, Real phis[], Real phit[], Real phiu[], int nphi ) const;
  virtual void evalBasisHessianDerivative( const Real& sRef, const Real& tRef, const Real& uRef, const Real& vRef,
                                           const Int4&,
                                           Real phiss[],
                                           Real phist[], Real phitt[],
                                           Real phisu[], Real phitu[], Real phiuu[], int nphi ) const;

protected:
  //Singleton!
  BasisFunctionSpacetime() {}
public:
  static const BasisFunctionSpacetime* self();
};

}

#endif  // BASISFUNCTIONVOLUME_TETRAHEDRON_HIERARCHICAL_H
