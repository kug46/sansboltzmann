// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CANONICALTRACETOCELL_H
#define CANONICALTRACETOCELL_H

#include <ostream>

namespace SANS
{

struct CanonicalTraceToCell
{
  CanonicalTraceToCell() : trace(-1), orientation(0) {}

  CanonicalTraceToCell(const int trace, const int orientation)
    : trace(trace), orientation(orientation) {}

  void set(const int itrace, const int iorientation)
  {
    trace = itrace;
    orientation = iorientation;
  }

  int trace; // canonical index of a trace w.r.t. the cell
  int orientation; // orientiation of the cell (which determines DOF ordering)
};

inline std::ostream& operator<< (std::ostream& stream, const CanonicalTraceToCell& canonical)
{
  stream << canonical.trace << " " << canonical.orientation;
  return stream;
}

} //namespace SANS

#endif //CANONICALTRACETOCELL_H
