// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_COMMUNICATOR_FWD_H_
#define SANS_COMMUNICATOR_FWD_H_

// forward declares the SANS::mpi::communicator
// DO NOT include boost/mpi/communicator.h here

#ifdef SANS_MPI
namespace boost { namespace mpi { class communicator; } }
namespace SANS { namespace mpi { using boost::mpi::communicator; } }
#else
namespace SANS { namespace mpi { class communicator; } }
#endif


#endif // SANS_COMMUNICATOR_FWD_H_
