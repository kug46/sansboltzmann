// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_OPERATIONS_H
#define SANS_OPERATIONS_H

#ifdef SANS_MPI

#include "tools/SANSException.h"

#include <vector>
#include <boost/mpi/operations.hpp>

// Creates specializations for performing reductions on std::vector

namespace boost
{
namespace mpi
{

template<typename T>
struct maximum<std::vector<T>> : public std::binary_function<std::vector<T>, std::vector<T>, std::vector<T>>
{
  /* returns the maximum of x and y. */
  std::vector<T> operator()(const std::vector<T>& x, const std::vector<T>& y) const
  {
    SANS_ASSERT_MSG(x.size() == y.size(), "%d != %d", x.size(), y.size());
    std::vector<T> max(x.size());
      for (std::size_t i = 0; i < x.size(); i++)
        max[i] = y[i] > x[i] ? y[i] : x[i];

    return max;
  }
};

template<typename T>
struct minimum<std::vector<T>> : public std::binary_function<std::vector<T>, std::vector<T>, std::vector<T>>
{
  /* returns the maximum of x and y. */
  std::vector<T> operator()(const std::vector<T>& x, const std::vector<T>& y) const
  {
    SANS_ASSERT_MSG(x.size() == y.size(), "%d != %d", x.size(), y.size());
    std::vector<T> min(x.size());
      for (std::size_t i = 0; i < x.size(); i++)
        min[i] = x[i] < y[i] ? x[i] : y[i];

    return min;
  }
};

} // namespace mpi
} // namespace boost

#endif //SANS_MPI

#endif //SANS_OPERATIONS_H
