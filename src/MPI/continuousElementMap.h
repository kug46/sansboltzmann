// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CONINUOUSELEMENTMAP_H
#define CONINUOUSELEMENTMAP_H

#include <map>
#include <set>
#include <vector>
#include <numeric> // std::accumulate

#include "communicator_fwd.h"

#include "tools/minmax.h"

#ifdef SANS_MPI

//#define SLEEP_FOR_OUTPUT // for debugging
#ifdef SLEEP_FOR_OUTPUT
#define SLEEP_MILLISECONDS 500
#include <chrono>
#include <thread>
#endif

#include "MPI/serialize_DenseLinAlg_MatrixS.h"

#include <boost/serialization/vector.hpp>
#include <boost/serialization/set.hpp>
#include <boost/serialization/map.hpp>
#include <boost/mpi/collectives/gather.hpp>
#include <boost/mpi/collectives/all_gather.hpp>
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/mpi/collectives/all_to_all.hpp>
#include <boost/mpi/operations.hpp>
#endif

namespace SANS
{

//---------------------------------------------------------------------------//
template<class XFieldElemGroupType>
int
nElementsPossessed( mpi::communicator& comm,
                    const XFieldElemGroupType& xfldGroup )
{
#ifdef SANS_MPI
  const int comm_rank = comm.rank();

  // count the number of possessed elements
  int nElemPossessed = 0;
  const int nElemLocal = xfldGroup.nElem();

  for (int elem = 0; elem < nElemLocal; elem++)
    if (xfldGroup.associativity(elem).rank() == comm_rank)
      nElemPossessed++;

  // sum across all processors for the total number of elements
  return boost::mpi::all_reduce(comm, nElemPossessed, std::plus<int>());
#else
  return xfldGroup.nElem();
#endif
}

//---------------------------------------------------------------------------//
template<class XFieldElemGroupType>
void
continuousElementMap( mpi::communicator& comm,
    const std::vector<int>& elemIDs,
    const XFieldElemGroupType& xfldGroup,
    int& nElem_global,
    std::map<int,int>& globalElemMap)
{
#ifdef SANS_MPI

  globalElemMap.clear();

  // construct a map from global element numbering to a continuous numbering
  // on each processor
  int comm_rank = comm.rank();
  int comm_size = comm.size();

  const int nElemLocal = xfldGroup.nElem();

  // get the maximum number of local elements (needed for boundary traces mostly)
  const int nMaxElemLocal = boost::mpi::all_reduce(comm, nElemLocal, boost::mpi::maximum<int>());

  // count the number of possessed elements
  int nElemPossessed = 0;
  int maxElemID = 0;
  for (int elem = 0; elem < nElemLocal; elem++)
  {
    maxElemID = std::max(maxElemID, elemIDs[elem]);
    if (xfldGroup.associativity(elem).rank() == comm_rank)
      nElemPossessed++;
  }

  // sum across all processors for the total number of elements
  nElem_global = boost::mpi::all_reduce(comm, nElemPossessed, std::plus<int>());

  // get the maximum element ID accross all processors
  maxElemID = boost::mpi::all_reduce(comm, maxElemID, boost::mpi::maximum<int>());

  // number of elements per processor in the implicit partition used to create the continuous map
  int ElemperProc = std::max(nMaxElemLocal, maxElemID / comm_size);

  // used to inform which processors need which parts of the map
  std::vector<std::set<int>> needElem(comm_size);
  std::vector<std::set<int>> sendElem(comm_size);

  // compute the number of cells possessed by each processor
  for (int elem = 0; elem < nElemLocal; elem++)
    if (xfldGroup.associativity(elem).rank() == comm_rank)
    {
      // Make sure the rank does not go beyond the
      // total number of processors due to the remainders in the partition
      int rank = MIN(comm_size-1, elemIDs[elem] / ElemperProc);

      needElem[rank].insert(elemIDs[elem]);
    }

  // send element IDs to other processors that 'possess' them based on the implicit partitioning
  boost::mpi::all_to_all(comm, needElem, sendElem);
  needElem.clear();

  // collapse down all global IDs so they are now sorted on each rank
  std::set<int> implicitElemPart;
  for (int rank = 0; rank < comm_size; rank++)
    implicitElemPart.insert(sendElem[rank].begin(), sendElem[rank].end());

  // send the count to all other processors
  std::vector<std::size_t> nElemOnRank;
  boost::mpi::all_gather(comm, implicitElemPart.size(), nElemOnRank);

  // offset from lower ranks
  int elem_offset = std::accumulate(nElemOnRank.begin(), nElemOnRank.begin() + comm_rank, 0);

  // create a map from sparse global cell index to a continuous index
  for ( const int elemID : implicitElemPart )
    globalElemMap[elemID] = elem_offset++;

  // redistribute relevant parts of the element map to the other processors
  std::vector<std::map<int,int>> globalElemMapSend(comm_size);
  std::vector<std::map<int,int>> globalElemMapRecv(comm_size);

  for (int rank = 0; rank < comm_size; rank++)
    for (const int& elemID : sendElem[rank] )
      globalElemMapSend[rank][elemID] = globalElemMap.at(elemID);

  // send the cell maps back to the other ranks
  boost::mpi::all_to_all(comm, globalElemMapSend, globalElemMapRecv);
  globalElemMapSend.clear();

  // complete the global map using parts of the map from the other ranks
  globalElemMap.clear();
  for (int rank = 0; rank < comm_size; rank++)
    globalElemMap.insert(globalElemMapRecv[rank].begin(), globalElemMapRecv[rank].end());

#if 0 // for debugging
  for (int rank = 0; rank < comm_size; rank++)
  {
    std::cout << std::flush;
#ifdef SLEEP_FOR_OUTPUT
    std::this_thread::sleep_for( std::chrono::milliseconds(SLEEP_MILLISECONDS) );
#endif
    comm.barrier();
    if (comm_rank != rank) continue;

    std::cout << " globalElemMap rank " << rank << std::endl;
    for (auto map : globalElemMap )
      std::cout << map.first << " " << map.second << std::endl;
    std::cout << std::flush;
  }
  comm.barrier();
#endif

#else // Serial implementation

  nElem_global = xfldGroup.nElem();

  // create an identity map for serial implementation
  for (int cellElem = 0; cellElem < xfldGroup.nElem(); cellElem++)
    globalElemMap[cellElem] = cellElem;

#endif
}

} // namespace SANS

#endif // CONINUOUSELEMENTMAP_H
