// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php


#if !defined(SANS_MPI) && !defined(SANS_HEADERCOMPILE_CHECK)
  #error "This file should only be included within #ifdef SANS_MPI"
#endif

#ifndef BOOST_SERIALIZATION_ARRAY_H
#define BOOST_SERIALIZATION_ARRAY_H

#include <boost/version.hpp>

#if BOOST_VERSION < 105600
#include <array>
#include <boost/serialization/nvp.hpp>
// serialization of std::array is not available in older versions of boost, so created it here
namespace boost
{
namespace serialization
{
template <class Archive, class T, std::size_t N>
void serialize(Archive& ar, std::array<T,N>& a, const unsigned int /* version */)
{
    ar & boost::serialization::make_nvp(
        "elems",
        *static_cast<T (*)[N]>(static_cast<void *>(a.data()))
    );
}
} // namespace serialization
} // namespace boost
#else
#include <boost/serialization/array.hpp>
#endif // BOOST_VERSION

#endif // BOOST_SERIALIZATION_ARRAY_H
