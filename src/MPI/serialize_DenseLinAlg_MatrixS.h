// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SERIALIZE_DENSELINALG_MATRIXS_H
#define SERIALIZE_DENSELINALG_MATRIXS_H

#ifdef SANS_MPI
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

#include <boost/mpl/bool.hpp>
#include <boost/mpi/datatype_fwd.hpp>
#include <boost/serialization/access.hpp>

namespace boost
{
namespace serialization
{
template<class Archive, int M, class T>
void serialize(Archive & ar, SANS::DLA::VectorS<M,T>& v, const unsigned int version)
{
  for (int i = 0; i < M; i++)
    ar & v[i];
}

template<class Archive, int M, int N, class T>
void serialize(Archive & ar, SANS::DLA::MatrixS<M,N,T>& m, const unsigned int version)
{
  for (int i = 0; i < M; i++)
    for (int j = 0; j < N; j++)
      ar & m(i,j);
}

template<class Archive, int M, class T>
void serialize(Archive & ar, SANS::DLA::MatrixSymS<M,T>& m, const unsigned int version)
{
  for (int i = 0; i < SANS::DLA::MatrixSymS<M,T>::SIZE; i++)
    ar & m.value(i);
}
} // namespace serialization

namespace mpi
{
// This allows Boost.MPI to avoid extraneous copy operations
// Only works when the datatype is of fixed size (no pointers or dynamically sized objects)
template <int M, class T>
struct is_mpi_datatype< SANS::DLA::VectorS<M,T> > : mpl::true_ {};

template <int M, int N, class T>
struct is_mpi_datatype< SANS::DLA::MatrixS<M,N,T> > : mpl::true_ {};

template <int M, class T>
struct is_mpi_datatype< SANS::DLA::MatrixSymS<M,T> > : mpl::true_ {};
} // namespace mpi
} // namespace boost
#endif

#endif //SERIALIZE_DENSELINALG_MATRIXS_H
