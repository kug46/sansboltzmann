// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SERIALIZE_SURREALS_H
#define SERIALIZE_SURREALS_H

#ifdef SANS_MPI
#include "Surreal/SurrealS.h"

#include <boost/mpl/bool.hpp>
#include <boost/mpi/datatype_fwd.hpp>
#include <boost/serialization/access.hpp>

namespace boost
{
namespace serialization
{
template<class Archive, int M, class T>
void serialize(Archive & ar, SurrealS<M,T>& s, const unsigned int version)
{
  ar & s.value();
  for (int i = 0; i < M; i++)
    ar & s.deriv(i);
}

} // namespace serialization

namespace mpi
{
// This allows Boost.MPI to avoid extraneous copy operations
// Only works when the datatype is of fixed size (no pointers or dynamically sized objects)
template <int M, class T>
struct is_mpi_datatype< SurrealS<M,T> > : mpl::true_ {};
} // namespace mpi
} // namespace boost
#endif

#endif //SERIALIZE_SURREALS_H
