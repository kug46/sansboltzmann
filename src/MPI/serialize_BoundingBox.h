// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SERIALIZE_BOUNDINGBOX_H
#define SERIALIZE_BOUNDINGBOX_H

#ifdef SANS_MPI
#include "Field/Element/BoundingBox.h"

#include <boost/mpl/bool.hpp>
#include <boost/mpi/datatype_fwd.hpp>
#include <boost/serialization/access.hpp>

#include "serialize_DenseLinAlg_MatrixS.h"

namespace boost
{
namespace serialization
{
template<class Archive, class PhysDim>
void serialize(Archive & ar, SANS::BoundingBox<PhysDim>& bbox, const unsigned int version)
{
  ar & bbox.low_bounds_;
  ar & bbox.high_bounds_;
}
} // namespace serialization

namespace mpi
{
// This allows Boost.MPI to avoid extraneous copy operations
// Only works when the datatype is of fixed size (no pointers or dynamically sized objects)
template <class PhysDim>
struct is_mpi_datatype< SANS::BoundingBox<PhysDim> > : mpl::true_ {};
} // namespace mpi
} // namespace boost
#endif

#endif //SERIALIZE_BOUNDINGBOX_H
