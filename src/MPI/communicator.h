// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#if !defined(MPI_COMMUNICATOR_IN_CPP) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file."
#endif

// if compiling with MPI, SANS::mpi::communicator is just the boost::mpi::communicator
#ifdef SANS_MPI

#include <boost/mpi/communicator.hpp>

namespace SANS
{
namespace mpi
{
  using boost::mpi::comm_attach;
  using boost::mpi::comm_create_kind;
  using boost::mpi::communicator;
}
}

#else

#ifndef SANS_MPI_COMMUNICATOR_H
#define SANS_MPI_COMMUNICATOR_H

#ifdef SANS_PETSC
#include <petscsys.h>
#else
typedef int MPI_Comm;
#endif

namespace SANS
{
namespace mpi
{

// --------------------------------------------------------------------------//
//
// A communicator class modeled on the Boost.MPI library
//
// --------------------------------------------------------------------------//

/*
 * Enumeration used to describe how to adopt a C MPI_Comm into
 * a SANS communicator.
 *
 * The values for this enumeration determine how a Boost.MPI
 * communicator will behave when constructed with an MPI
 * communicator. The options are:
 *
 *   - comm_duplicate: Duplicate the MPI_Comm communicator to
 *   create a new communicator (e.g., with MPI_Comm_dup). This new
 *   MPI_Comm communicator will be automatically freed when the
 *   SANS communicator (and all copies of it) is destroyed.
 *
 *   - comm_take_ownership: Take ownership of the communicator. It
 *   will be freed automatically when all of the SANS
 *   communicators go out of scope. This option must not be used with
 *   MPI_COMM_WORLD.
 *
 *   - comm_attach: The SANS communicator will reference the
 *   existing MPI communicator but will not free it when the Boost.MPI
 *   communicator goes out of scope. This option should only be used
 *   when the communicator is managed by the user or MPI library
 *   (e.g., MPI_COMM_WORLD).
 */
enum comm_create_kind { comm_duplicate, comm_take_ownership, comm_attach };


class communicator
{
public:
  // Creates a new communicator for MPI_COMM_WORLD
  communicator() {}

  /*
   * Build a new SANS communicator based on the MPI communicator
   * comm.
   *
   * comm may be any valid MPI communicator. If comm is
   * MPI_COMM_NULL, an empty communicator (that cannot be used for
   * communication) is created and the kind parameter is
   * ignored. Otherwise, the kind parameters determines how the
   * SANS communicator will be related to comm:
   *
   *   - If kind is comm_duplicate, duplicate comm to create
   *   a new communicator. This new communicator will be freed when
   *   the SANS communicator (and all copies of it) is destroyed.
   *   This option is only permitted if comm is a valid MPI
   *   intracommunicator or if the underlying MPI implementation
   *   supports MPI 2.0 (which supports duplication of
   *   intercommunicators).
   *
   *   - If kind is comm_take_ownership, take ownership of
   *   comm. It will be freed automatically when all of the SANS
   *   communicators go out of scope. This option must not be used
   *   when comm is MPI_COMM_WORLD.
   *
   *   - If kind is comm_attach, this SANS communicator
   *   will reference the existing MPI communicator comm but will
   *   not free comm when the Boost.MPI communicator goes out of
   *   scope. This option should only be used when the communicator is
   *   managed by the user or MPI library (e.g., MPI_COMM_WORLD).
   */
  communicator(const MPI_Comm& comm, comm_create_kind kind) {}

  // Gives the rank and size of the communicator
  int rank() const { return 0; }
  int size() const { return 1; }

  /*
   * Split the communicator into multiple, disjoint communicators
   * each of which is based on a particular color. This is a
   * collective operation that returns a new communicator that is a
   * subgroup of this. This routine is functionally equivalent to
   * MPI_Comm_split.
   *
   *   color The color of this process. All processes with the
   *   same color value will be placed into the same group.
   *
   *   returns A new communicator containing all of the processes in
   *   this that have the same color.
   */
  communicator split(int color) const { return communicator(); }

  /*
   * Split the communicator into multiple, disjoint communicators
   * each of which is based on a particular color. This is a
   * collective operation that returns a new communicator that is a
   * subgroup of @p this. This routine is functionally equivalent to
   * MPI_Comm_split.
   *
   *   color The color of this process. All processes with the
   *   same color value will be placed into the same group.
   *
   *   key A key value that will be used to determine the
   *   ordering of processes with the same color in the resulting
   *   communicator. If omitted, the rank of the processes in @p this
   *   will determine the ordering of processes in the resulting
   *   group.
   *
   *   returns A new communicator containing all of the processes in
   *   this that have the same color.
   */
  communicator split(int color, int key) const { return communicator(); }

#ifdef barrier
  // Linux defines a function-like macro named "barrier". So, we need
  // to avoid expanding the macro when we define our barrier()
  // function. However, some C++ parsers (Doxygen, for instance) can't
  // handle this syntax, so we only use it when necessary.
  void (barrier)() const {}
#else
  /*
   * Wait for all processes within a communicator to reach the
   * barrier.
   *
   * This routine is a collective operation that blocks each process
   * until all processes have entered it, then releases all of the
   * processes "simultaneously". It is equivalent to MPI_Barrier.
   */
  void barrier() const {}
#endif

  template<typename T>
  void send(int dest, int tag, const T& value) const {}

  template<typename T>
  void send(int dest, int tag, const T* values, int n) const {}

  void send(int dest, int tag) const {}

  template<typename T>
  int recv(int source, int tag, T& value) const { return 0; }

  template<typename T>
  int recv(int source, int tag, T* values, int n) const { return 0; }

  int recv(int source, int tag) const { return 0; }

#ifdef SANS_PETSC
  // Implicit conversion top MPI_Comm for use in MPI functions
  operator MPI_Comm() const { return PETSC_COMM_WORLD; }
#else
  operator MPI_Comm() const { return 0; }
#endif
};


} // namespace mpi
} // namespace SANS
#endif // SANS_MPI_COMMUNICATOR_H
#endif // SANS_MPI
