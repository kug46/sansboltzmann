// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "MPI_sleep.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "communicator.h"

#ifdef SANS_MPI
#include <chrono>
#include <thread>
#endif

#define SLEEP_TAG 4242

namespace SANS
{
#ifdef SANS_MPI

void MPI_sleep(mpi::communicator& comm, const int rank, const int milliseconds)
{
  if (comm.rank() == rank)
  {
    // let all the processor know the main processor is done
    for (int i = 0; i < comm.size(); i++)
    {
      if (i == rank) continue; // don't send to self
      comm.send(i, SLEEP_TAG);
    }
  }
  else
  {
    // sleep for while probing from the main processor
    while (!comm.iprobe(rank, SLEEP_TAG))
      std::this_thread::sleep_for( std::chrono::milliseconds(milliseconds) );

    comm.recv(rank, SLEEP_TAG);
  }

  // just to make sure everyone is really now in sync
  comm.barrier();
}

#else
// Nothing to do in serial
void MPI_sleep(mpi::communicator& comm, const int rank, const int milliseconds) {}
#endif
}
