// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_MPI_SLEEP_H
#define SANS_MPI_SLEEP_H

#include "communicator_fwd.h"

namespace SANS
{
// All processors other than 'rank' sleep for milliseconds repeatedly
// until 'rank' calls this function
void MPI_sleep(mpi::communicator& comm, const int rank, const int milliseconds);
}

#endif // SANS_MPI_SLEEP_H
