// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QUATERNION_H
#define QUATERNION_H

#include <cmath> // cos, sin
#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"
#include "tools/SANSException.h"
#include "tools/AlignMem.h"
#include "tools/always_inline.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Quaternion<T>
//
// q = w + xi + yj + zk = { w | v } = { w | x y z }
//
// Quaternion mathematics class, template T can be T or Surreal
//
//----------------------------------------------------------------------------//



template<class T>
class Quaternion
{
public:
  static const int N = 4; // Length of quaternion vector

  typedef DLA::VectorS<3,T> Vector;

  //The default constructor is intentionally empty here. This means Quaternion is
  //not initialized when declared, which is consistent with regular numbers. This also
  //improves performance.
  ALWAYS_INLINE Quaternion() {}
  Quaternion( const Quaternion& q );
  Quaternion( const T& w, const T& x, const T& y, const T& z );
  Quaternion( const T& w, const T v[], const int& n);
  Quaternion( const T& w, const Vector& v );
  Quaternion( const T axis[], const int& n, const T& theta );
  Quaternion( const Vector& axis, const T& theta );

  ALWAYS_INLINE ~Quaternion() {}

  // value accessor operators
  ALWAYS_INLINE T w() const { return w_; }
  ALWAYS_INLINE T x() const { return x_; }
  ALWAYS_INLINE T y() const { return y_; }
  ALWAYS_INLINE T z() const { return z_; }
  ALWAYS_INLINE bool isUnit() const { return isUnit_; }

  // assignment
  Quaternion& operator=( const Quaternion& );

  // member functions
  void normalize();
  Vector rotate( const Vector& );
  Vector invRotate( const Vector& );
  T getAngle();
  Vector getAxis();
  static inline T sinc( const T& x );
  static inline T vectorMagnitude( const Quaternion<T>& );

  // quaternion operations
  template<class Z> friend Quaternion<Z> unit( const Quaternion<Z>& );
  template<class Z> friend Quaternion<Z> conj( const Quaternion<Z>& );

  // vector operations
  template<class Z> friend Z magnitude( const Quaternion<Z>& );
  template<class Z> friend Z dot( const Quaternion<Z>&, const Quaternion<Z>& );

  // binary operators
  template<class Z> friend Quaternion<Z> operator+( const Quaternion<Z>&, const Quaternion<Z>& );
  template<class Z> friend Quaternion<Z> operator-( const Quaternion<Z>&, const Quaternion<Z>& );
  template<class Z> friend Quaternion<Z> operator*( const Quaternion<Z>&, const Quaternion<Z>& );
  template<class Z> friend Quaternion<Z> operator*( const Quaternion<Z>&, const Z& );
  template<class Z> friend Quaternion<Z> operator*( const Z&, const Quaternion<Z>& );
  template<class Z> friend Quaternion<Z> operator*( const Quaternion<Z>&, const int& );
  template<class Z> friend Quaternion<Z> operator*( const int&, const Quaternion<Z>& );

  // relational operators
  template<class Z> friend bool operator==( const Quaternion<Z>&, const Quaternion<Z>& );
  template<class Z> friend bool operator!=( const Quaternion<Z>&, const Quaternion<Z>& );

  // debug dump
  void dump( int indentSize=0 ) const;

private:
  T w_;
  T x_;
  T y_;
  T z_;
  bool isUnit_;

  inline void init( const T& w, const T& x, const T& y, const T& z);

};


//____________________________________________________________________________
// Constructors

template<class T>
ALWAYS_INLINE
Quaternion<T>::Quaternion( const Quaternion& q ) : w_(q.w_), x_(q.x_), y_(q.y_), z_(q.z_), isUnit_(q.isUnit_) {}

template<class T>
ALWAYS_INLINE
Quaternion<T>::Quaternion( const T& w, const T& x, const T& y, const T& z )
{
  init( w, x, y, z );
}

template<class T>
ALWAYS_INLINE
Quaternion<T>::Quaternion( const T& w, const T v[], const int& n )
{
  SANS_ASSERT( n == 3 );
  init( w, v[0], v[1], v[2] );
}

template<class T>
ALWAYS_INLINE
Quaternion<T>::Quaternion( const T& w, const Vector& v )
{
  init( w, v[0], v[1], v[2] );
}

template<class T>
ALWAYS_INLINE
Quaternion<T>::Quaternion( const T axis[], const int& n, const T& theta )
{
  SANS_ASSERT( n == 3 );
  T x = axis[0];
  T y = axis[1];
  T z = axis[2];

  T axisNorm = sqrt( pow(x,2) + pow(y,2) + pow(z,2) );

  w_ = cos( theta/2 );
  x_ = sin( theta/2 ) * x/axisNorm;
  y_ = sin( theta/2 ) * y/axisNorm;
  z_ = sin( theta/2 ) * z/axisNorm;
  isUnit_ = true;
}

template<class T>
ALWAYS_INLINE
Quaternion<T>::Quaternion( const Vector& axis, const T& theta )
{
  T x = axis[0];
  T y = axis[1];
  T z = axis[2];

  T axisNorm = sqrt( pow(x,2) + pow(y,2) + pow(z,2) );

  w_ = cos( theta/2 );
  x_ = sin( theta/2 ) * x/axisNorm;
  y_ = sin( theta/2 ) * y/axisNorm;
  z_ = sin( theta/2 ) * z/axisNorm;
  isUnit_ = true;
}

//____________________________________________________________________________
// Assignment

template<class T>
ALWAYS_INLINE Quaternion<T>&
Quaternion<T>::operator=( const Quaternion<T>& q )
{
  //Do nothing if assigning self to self
  if ( &q == this ) return *this;

  w_      = q.w_;
  x_      = q.x_;
  y_      = q.y_;
  z_      = q.z_;
  isUnit_ = q.isUnit_;
  return *this;
}


//____________________________________________________________________________
// Private methods

template<class T>
inline void
Quaternion<T>::init( const T& w, const T& x, const T& y, const T& z )
{
  w_ = w;
  x_ = x;
  y_ = y;
  z_ = z;
  isUnit_ = false;
}

//____________________________________________________________________________
// Member functions

template<class T>
ALWAYS_INLINE void
Quaternion<T>::normalize()
{
  T N = magnitude( *this );

  w_ /= N;
  x_ /= N;
  y_ /= N;
  z_ /= N;
  isUnit_ = true;
}

template<class T>
ALWAYS_INLINE typename Quaternion<T>::Vector
Quaternion<T>::rotate( const Vector& r )
{
  SANS_ASSERT( isUnit_ );

  Quaternion<T> rExt;
  rExt.w_ = 0;
  rExt.x_ = r[0];
  rExt.y_ = r[1];
  rExt.z_ = r[2];
  rExt.isUnit_ = false;

  Quaternion<T> rRotq = (*this * rExt) * conj(*this); // Order important (go left->right)

  return { rRotq.x_, rRotq.y_, rRotq.z_ };
}

template<class T>
ALWAYS_INLINE typename Quaternion<T>::Vector
Quaternion<T>::invRotate( const Vector& r )
{
  SANS_ASSERT( isUnit_ );

  Quaternion<T> rExt;
  rExt.w_ = 0;
  rExt.x_ = r[0];
  rExt.y_ = r[1];
  rExt.z_ = r[2];
  rExt.isUnit_ = false;

  Quaternion<T> rRotq = (conj(*this) * rExt) * *this; // Order important (go left->right)

  return { rRotq.x_, rRotq.y_, rRotq.z_ };
}

template<class T>
ALWAYS_INLINE T
Quaternion<T>::getAngle()
{
  SANS_ASSERT( isUnit_ );

  return 2*atan2( vectorMagnitude(*this), w_ );
}

template<class T>
ALWAYS_INLINE typename Quaternion<T>::Vector
Quaternion<T>::getAxis()
{
  SANS_ASSERT( isUnit_ );

  T vMag = vectorMagnitude(*this);

  return { x_/vMag, y_/vMag, z_/vMag};
}

template<class T>
inline T
Quaternion<T>::sinc( const T& x )
{
  if (x == 0) return 1;
  return sin(x)/x;
}

template<class T>
inline T
Quaternion<T>::vectorMagnitude( const Quaternion<T>& q )
{
  return sqrt( pow(q.x_,2) + pow(q.y_,2) + pow(q.z_,2) );
}

//____________________________________________________________________________
// Quaternion operations

template<class T>
ALWAYS_INLINE Quaternion<T>
unit( const Quaternion<T>& q )
{
  Quaternion<T> qNorm(q);

  qNorm.normalize();

  return qNorm;
}

template<class T>
ALWAYS_INLINE Quaternion<T>
conj( const Quaternion<T>& q )
{
  Quaternion<T> qConj;

  qConj.w_ =  q.w_;
  qConj.x_ = -q.x_;
  qConj.y_ = -q.y_;
  qConj.z_ = -q.z_;
  qConj.isUnit_ = q.isUnit_;

  return qConj;
}



//____________________________________________________________________________
// Vector operations

template<class T>
ALWAYS_INLINE T
magnitude( const Quaternion<T>& q )
{
  if ( q.isUnit() ) return 1;
  return sqrt( pow(q.w_,2) + pow(q.x_,2) + pow(q.y_,2) + pow(q.z_,2) );
}

template<class T>
ALWAYS_INLINE T
dot( const Quaternion<T>& a, const Quaternion<T>& b )
{
  return a.w_*b.w_ + a.x_*b.x_ + a.y_*b.y_ + a.z_*b.z_;
}


//____________________________________________________________________________
// Binary operators

template<class T>
ALWAYS_INLINE Quaternion<T>
operator+ ( const Quaternion<T>& a, const Quaternion<T>& b )
{
  Quaternion<T> c;

  c.w_ = a.w_ + b.w_;
  c.x_ = a.x_ + b.x_;
  c.y_ = a.y_ + b.y_;
  c.z_ = a.z_ + b.z_;
  c.isUnit_ = false;

  return c;
}

template<class T>
ALWAYS_INLINE Quaternion<T>
operator- ( const Quaternion<T>& a, const Quaternion<T>& b )
{
  Quaternion<T> c;

  c.w_ = a.w_ - b.w_;
  c.x_ = a.x_ - b.x_;
  c.y_ = a.y_ - b.y_;
  c.z_ = a.z_ - b.z_;
  c.isUnit_ = false;

  return c;
}

template<class T>
ALWAYS_INLINE Quaternion<T>
operator* ( const Quaternion<T>& a, const Quaternion<T>& b )
{
  Quaternion<T> c;

  c.w_ = a.w_*b.w_ - a.x_*b.x_ - a.y_*b.y_ - a.z_*b.z_;
  c.x_ = a.x_*b.w_ + a.w_*b.x_ - a.z_*b.y_ + a.y_*b.z_;
  c.y_ = a.y_*b.w_ + a.z_*b.x_ + a.w_*b.y_ - a.x_*b.z_;
  c.z_ = a.z_*b.w_ - a.y_*b.x_ + a.x_*b.y_ + a.w_*b.z_;

  c.isUnit_ = a.isUnit_ && b.isUnit_;

  return c;
}

template<class T>
ALWAYS_INLINE Quaternion<T>
operator* ( const Quaternion<T>& a, const T& b )
{
  Quaternion<T> c;

  c.w_ = a.w_ * b;
  c.x_ = a.x_ * b;
  c.y_ = a.y_ * b;
  c.z_ = a.z_ * b;

  c.isUnit_ = a.isUnit_ && (b == 1);

  return c;
}

template<class T>
ALWAYS_INLINE Quaternion<T>
operator* ( const T& a, const Quaternion<T>& b )
{
  Quaternion<T> c;

  c.w_ = a * b.w_;
  c.x_ = a * b.x_;
  c.y_ = a * b.y_;
  c.z_ = a * b.z_;

  c.isUnit_ = (a == 1) && b.isUnit_;

  return c;
}

template<class T>
ALWAYS_INLINE Quaternion<T>
operator* ( const Quaternion<T>& a, const int& b )
{
  Quaternion<T> c;

  c.w_ = a.w_ * b;
  c.x_ = a.x_ * b;
  c.y_ = a.y_ * b;
  c.z_ = a.z_ * b;

  c.isUnit_ = a.isUnit_ && (b == 1);

  return c;
}

template<class T>
ALWAYS_INLINE Quaternion<T>
operator* ( const int& a, const Quaternion<T>& b )
{
  Quaternion<T> c;

  c.w_ = a * b.w_;
  c.x_ = a * b.x_;
  c.y_ = a * b.y_;
  c.z_ = a * b.z_;

  c.isUnit_ = (a == 1) && b.isUnit_;

  return c;
}

//____________________________________________________________________________
// relational operators

template<class T>
ALWAYS_INLINE bool
operator==( const Quaternion<T>& lhs, const Quaternion<T>& rhs )
{
  return ( (lhs.w_==rhs.w_) &&
            (lhs.x_==rhs.x_) &&
            (lhs.y_==rhs.y_) &&
            (lhs.z_==rhs.z_) &&
            (lhs.z_==rhs.z_) &&
            (lhs.isUnit_==rhs.isUnit_) );
}

template<class T>
ALWAYS_INLINE bool
operator!=( const Quaternion<T>& lhs, const Quaternion<T>& rhs )
{
  return !(lhs == rhs);
}


//____________________________________________________________________________
// debug dump of private data

template<class T>
void
Quaternion<T>::dump( int indentSize ) const
{
  std::string indent(indentSize, ' ');
  std::cout << indent << "Quaternion<" << N << ">:";
  std::cout << "   w_ = " << w_;
  std::cout << "   x_ = " << x_;
  std::cout << "   y_ = " << y_;
  std::cout << "   z_ = " << z_ << std::endl;

}

} // namespace SANS


//____________________________________________________________________________
// exp and log functions

template<class T>
ALWAYS_INLINE SANS::Quaternion<T>
exp( const SANS::Quaternion<T>& q )
{
  typedef SANS::Quaternion<T> Q;

  T vMag = Q::vectorMagnitude( q );

  T ew = exp(q.w());

  T w = ew * cos(vMag);
  T x = ew * Q::sinc(vMag) * q.x();
  T y = ew * Q::sinc(vMag) * q.y();
  T z = ew * Q::sinc(vMag) * q.z();

  Q expq( w, x, y, z );

  // exp({0,0,0,0}) = {1,0,0,0} and is a unit quat, set unit w/ normalize
  if ( (vMag==0) && (q.w()==0) ) expq.normalize();

  return expq;
}

template<class T>
ALWAYS_INLINE SANS::Quaternion<T>
log( const SANS::Quaternion<T>& q )
{
  typedef SANS::Quaternion<T> Q;

  T vMag = Q::vectorMagnitude( q );
  T qMag = magnitude( q );
  T halfTheta = atan2( vMag, q.w() );

  T w = log(qMag);
  T x = q.x()/vMag * halfTheta;
  T y = q.y()/vMag * halfTheta;
  T z = q.z()/vMag * halfTheta;

  return { w, x, y, z };
}

//____________________________________________________________________________
// Power functions

template<class T>
ALWAYS_INLINE SANS::Quaternion<T>
inverse( const SANS::Quaternion<T>& q )
{
  return conj(q) * pow(magnitude(q),-2);
}

template<class T>
ALWAYS_INLINE SANS::Quaternion<T>
pow( const SANS::Quaternion<T>& q, const SANS::Quaternion<T>& p )
{
  return exp( log(q)*p );
}

template<class T>
ALWAYS_INLINE SANS::Quaternion<T>
pow( const SANS::Quaternion<T>& q, const T& p )
{
  typedef SANS::Quaternion<T> Q;

  if ( p == -1 ) return inverse(q);
  Q powq = exp( log(q)*p );

  if ( q.isUnit() && (p==1)) powq.normalize();
  return powq;
}

template<class T>
ALWAYS_INLINE SANS::Quaternion<T>
pow( const SANS::Quaternion<T>& q, const int& p )
{
  typedef SANS::Quaternion<T> Q;

  if ( p == -1 ) return inverse(q);
  Q powq = exp( log(q)*p );

  if ( q.isUnit() && (p==1)) powq.normalize();
  return powq;
}


#endif // QUATERNION_H
