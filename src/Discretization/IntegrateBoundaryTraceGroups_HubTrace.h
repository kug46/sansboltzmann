// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATEBOUNDARYTRACEGROUPS_HUBTRACE_H
#define INTEGRATEBOUNDARYTRACEGROUPS_HUBTRACE_H

// boundary-trace integral residual functions

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/GroupElementType.h"
#include "GroupIntegral_Type.h"

namespace SANS
{


template <class TopologyTrace, class TopologyL, class TopoDim, class IntegralBoundaryTraceGroup,
class XFieldType, class FieldCellType, class FieldHubTraceType >
void
IntegrateBoundaryTraceGroups_HubTrace_Integral(
    IntegralBoundaryTraceGroup& integral,
    const XFieldType& xfld,
    const FieldCellType& fldCell,
    const int traceGroupGlobal,
    const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const FieldHubTraceType& fldHubTrace,
    const int quadratureorder )
{
  const int cellGroupGlobalL = xfldTrace.getGroupLeft();

  // Integrate over the trace group
  integral.template integrate<TopologyTrace, TopologyL, TopoDim, XFieldType>(
      cellGroupGlobalL,
      xfld.template getCellGroup<TopologyL>(cellGroupGlobalL),
      fldCell.template getCellGroupGlobal<TopologyL>(cellGroupGlobalL),
      traceGroupGlobal,
      xfldTrace, fldHubTrace,
      quadratureorder );
}


template<class TopDim>
class IntegrateBoundaryTraceGroups_HubTrace;

template<>
class IntegrateBoundaryTraceGroups_HubTrace<TopoD1>
{
public:
  typedef TopoD1 TopoDim;
private:
  template <class TopologyTrace, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType, class FieldHubTraceType >
  static void
  LeftTopology(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const FieldCellType& fldCell,
      const FieldHubTraceType& fldTrace,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    const int cellGroupL = xfldTrace.getGroupLeft();
    const int hubGroupR = xfldTrace.getGroupRight();
    SANS_ASSERT( xfldTrace.getGroupRightType() == eHubTraceGroup );

    // determine topology for R
    if ( fldCell.getCellGroupBase(cellGroupL).topoTypeID() == typeid(Line) )
    {
      IntegrateBoundaryTraceGroups_HubTrace_Integral<TopologyTrace, Line, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal,
          xfldTrace,
          fldTrace.template getHubTraceGroupGlobal<TopologyTrace>(hubGroupR),
          quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType, class FieldHubTraceType>
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<FieldCellType>& fldCellType,
      const FieldType<FieldHubTraceType>& fldTraceType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const FieldCellType& fldCell = fldCellType.cast();
    const FieldHubTraceType& fldTrace = fldTraceType.cast();

    integral.template check<TopoDim>( fldCell, fldTrace );

    SANS_ASSERT( &xfld.getXField() == &fldCell.getXField() );
    SANS_ASSERT( &fldCell.getXField() == &fldTrace.getXField() );

    SANS_ASSERT_MSG( ngroup == xfld.getXField().nBoundaryTraceGroups(),
                     "ngroup = %d, xfld.getXField().nBoundaryTraceGroups() = %d", ngroup, xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);
      if ( xfld.getXField().getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node>( integral,
                            xfld, fldCell, fldTrace,
                            boundaryGroup,
                            xfld.getXField().template getBoundaryTraceGroup<Node>(boundaryGroup),
                            quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class IntegrateBoundaryTraceGroups_HubTrace<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

private:
  template <class TopologyTrace, class HubTraceType, class IntegralBoundaryTraceGroup,
            class XFieldType, class FieldCellType>
  static void
  LeftTopology(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const FieldCellType& fldCell,
      const TopologyTrace& fldTrace,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    const int cellGroupL = xfldTrace.getGroupLeft();
    const int hubGroupR = xfldTrace.getGroupRight();
    SANS_ASSERT( xfldTrace.getGroupRightType() == eHubTraceGroup );

    // determine topology for R
    if ( fldCell.getCellGroupBase(cellGroupL).topoTypeID() == typeid(Triangle) )
    {
      IntegrateBoundaryTraceGroups_HubTrace_Integral<TopologyTrace, Triangle, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal,
          xfldTrace,
          fldTrace.template getHubTraceGroupGlobal<TopologyTrace>(hubGroupR),
          quadratureorder );
    }
    else if ( fldCell.getCellGroupBase(cellGroupL).topoTypeID() == typeid(Quad) )
    {
      IntegrateBoundaryTraceGroups_HubTrace_Integral<TopologyTrace, Quad, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal,
          xfldTrace,
          fldTrace.template getHubTraceGroupGlobal<TopologyTrace>(hubGroupR),
          quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }
public:
  template <class HubTraceType, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType >
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<FieldCellType>& fldCellType,
      const FieldType<HubTraceType>& fldTraceType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const FieldCellType& fldCell = fldCellType.cast();
    const HubTraceType& fldTrace = fldTraceType.cast();

    integral.template check<TopoDim>( fldCell, fldTrace );

    SANS_ASSERT( &xfld.getXField() == &fldCell.getXField() );
    SANS_ASSERT( &xfld.getXField() == &fldTrace.getXField() );

    SANS_ASSERT_MSG( ngroup == xfld.getXField().nBoundaryTraceGroups(),
                     "ngroup = %d, xfld.getXField().nBoundaryTraceGroups() = %d", ngroup, xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);

      if ( xfld.getXField().getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line>( integral,
                            xfld, fldCell, fldTrace,
                            boundaryGroup,
                            xfld.getXField().template getBoundaryTraceGroup<Line>(boundaryGroup),
                            quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class IntegrateBoundaryTraceGroups_HubTrace<TopoD3>
{
public:
  typedef TopoD3 TopoDim;
private:
  template <class HubTraceType, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType>
  static void
  LeftTopology_Triangle(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const FieldCellType& fldCell,
      const HubTraceType& fldTrace,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    const int cellGroupL = xfldTrace.getGroupLeft();
    const int hubGroupR = xfldTrace.getGroupRight();
    SANS_ASSERT( xfldTrace.getGroupRightType() == eHubTraceGroup );

    if ( fldCell.getCellGroupBase(cellGroupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      IntegrateBoundaryTraceGroups_HubTrace_Integral<Triangle, Tet, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal,
          xfldTrace,
          fldTrace.template getHubTraceGroupGlobal<Triangle>(hubGroupR),
          quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }
public:
  template <class HubTraceType, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType>
  static void
  LeftTopology_Quad(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const FieldCellType& fldCell,
      const HubTraceType& fldTrace,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    const int cellGroupL = xfldTrace.getGroupLeft();
    const int hubGroupR = xfldTrace.getGroupRight();
    SANS_ASSERT( xfldTrace.getGroupRightType() == eHubTraceGroup );

    if ( fldCell.getCellGroupBase(cellGroupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      IntegrateBoundaryTraceGroups_HubTrace_Integral<Quad, Hex, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal,
          xfldTrace,
          fldTrace.template getHubTraceGroupGlobal<Quad>(hubGroupR),
          quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template <class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType, class HubTraceType>
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<FieldCellType>& fldCellType,
      const FieldType<HubTraceType>& fldTraceType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const FieldCellType& fldCell = fldCellType.cast();
    const HubTraceType& fldTrace = fldTraceType.cast();

    integral.template check<TopoDim>( fldCell, fldTrace );

    SANS_ASSERT( &xfld.getXField() == &fldCell.getXField() );
    SANS_ASSERT( &fldCell.getXField() == &fldTrace.getXField() );

    SANS_ASSERT_MSG( ngroup == xfld.getXField().nBoundaryTraceGroups(),
                     "ngroup = %d, xfld.getXField().nBoundaryTraceGroups() = %d", ngroup, xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);

      if ( xfld.getXField().getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle<HubTraceType>( integral,
                                               xfld, fldCell, fldTrace,
                                               boundaryGroup,
                                               xfld.getXField().template getBoundaryTraceGroup<Triangle>(boundaryGroup),
                                               quadratureorder[boundaryGroup] );
      }
      else if ( xfld.getXField().getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad<HubTraceType>( integral,
                                           xfld, fldCell, fldTrace,
                                           boundaryGroup,
                                           xfld.getXField().template getBoundaryTraceGroup<Quad>(boundaryGroup),
                                           quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

}

#endif  // INTEGRATEBOUNDARYTRACEGROUPS_HUBTRACE_H
