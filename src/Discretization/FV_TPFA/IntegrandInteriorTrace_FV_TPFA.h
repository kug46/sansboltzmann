// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDINTERIORTRACE_FV_TPFA_H
#define INTEGRANDINTERIORTRACE_FV_TPFA_H

// interior trace integrand operator: finite volume

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// interior trace integrand: FV TPFA
//
// integrandL = + [[phiL]].{F}
// integrandR =   [[phiR]].{F}
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective, viscous)
//
template <class PDE_>
class IntegrandInteriorTrace_FV_TPFA : public IntegrandInteriorTraceType<IntegrandInteriorTrace_FV_TPFA<PDE_> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  // cppcheck-suppress noExplicitConstructor
  IntegrandInteriorTrace_FV_TPFA(const PDE& pde,
                            const std::vector<int>& InteriorTraceGroups) :
    pde_(pde),
    interiorTraceGroups_(InteriorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                                        class ElementParamL, class ElementParamR>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldTypeL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldTypeR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldTypeL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldTypeR;

    typedef typename ElementXFieldTraceType::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_Galerkin<MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldTypeL& qfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldTypeR& qfldElemR ) :
      pde_(pde),
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), paramfldElemL_( paramfldElemL ),
      xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), paramfldElemR_( paramfldElemR ),
      nDOFL_(qfldElemL_.nDOF()),
      nDOFR_(qfldElemR_.nDOF()),
      phiL_( new Real[nDOFL_] ),
      phiR_( new Real[nDOFR_] ) {}

    ~BasisWeighted()
    {
      delete [] phiL_;
      delete [] phiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int neqnL,
                                                     ArrayQ<Ti> integrandR[], const int neqnR ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemR ) const;

  protected:
    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const VectorX& nL,
                            const ParamTL& paramL, const ParamTR& paramR,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                            const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                            ArrayQ<Ti> integrandL[], const int neqnL,
                            ArrayQ<Ti> integrandR[], const int neqnR,
                            bool gradientOnly = false ) const;

    const PDE& pde_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldTypeL& xfldElemL_;
    const ElementQFieldTypeL& qfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldTypeR& xfldElemR_;
    const ElementQFieldTypeR& qfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
  };

  //Factory function that returns the basis weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,      TopologyR,
                                  ElementParamL, ElementParamR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& qfldElemR) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL,     TopologyR,
                                          ElementParamL, ElementParamR>(pde_,
                                                                        xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                                                        paramfldElemL, qfldElemL,
                                                                        paramfldElemR, qfldElemR);
  }

protected:
  const PDE& pde_;
  const std::vector<int> interiorTraceGroups_;
};


template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR >
bool
IntegrandInteriorTrace_FV_TPFA<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}

template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR>
template <class Ti>
void
IntegrandInteriorTrace_FV_TPFA<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], const int neqnL,
                                                 ArrayQ<Ti> integrandR[], const int neqnR ) const
{
  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  //This is finite volume, so check if there's only 1 DOF per elem
  SANS_ASSERT(nDOFL_ == 1);
  SANS_ASSERT(nDOFR_ == 1);

  VectorX centroidL, centroidR;    // Cell centroids

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradq;           // solution gradients - computed from difference between left and right cells

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Cell centroids
  xfldElemL_.coordinates( TopologyL::centerRef, centroidL );
  xfldElemR_.coordinates( TopologyR::centerRef, centroidR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );
  if (needsSolutionGradient)
  {
    for (int d = 0; d < PhysDim::D; d++)
    {
      Real ds = centroidR[d] - centroidL[d];
      Real invds = ds == 0.0 ? 0.0 : 1.0 / ds;

      gradq[d] = (qR - qL)*invds;
    }
  }

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // compute the integrand
  weightedIntegrand( nL,
                     paramL, paramR,
                     qL, gradq,
                     qR, gradq,
                     integrandL, neqnL,
                     integrandR, neqnR );
}


//---------------------------------------------------------------------------//
template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_FV_TPFA<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  //This is finite volume, so check if there's only 1 DOF per elem
  SANS_ASSERT(nDOFL_ == 1);
  SANS_ASSERT(nDOFR_ == 1);

  VectorX centroidL, centroidR;    // Cell centroids
  VectorX invds;                   // 1/(centroidR - centroidL)

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradq;           // solution gradient - computed from difference between left and right cells

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Cell centroids
  xfldElemL_.coordinates( TopologyL::centerRef, centroidL );
  xfldElemR_.coordinates( TopologyR::centerRef, centroidR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL, qSurrealR;  // solution
  VectorArrayQ<SurrealClass> gradqSurreal;    // gradient

  qSurrealL = qL;
  qSurrealR = qR;

  if (needsSolutionGradient)
  {
    for (int d = 0; d < PhysDim::D; d++)
    {
      Real ds = centroidR[d] - centroidL[d];

      if (ds == 0.0)
        invds[d] = 0.0;
      else
        invds[d] = 1.0 / ds;

      gradq[d] = (qR - qL)*invds[d];
    }

    gradqSurreal = gradq;
  }

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandRSurreal( nDOFR_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL, paramR,
                       qSurrealL, gradq,
                       qSurrealR, gradq,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemR.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemL.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemR.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk


  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandLSurreal = 0;
      integrandRSurreal = 0;

      // compute the integrand
      weightedIntegrand( nL,
                         paramL, paramR,
                         qL, gradqSurreal,
                         qR, gradqSurreal,
                         integrandLSurreal.data(), nDOFL_,
                         integrandRSurreal.data(), nDOFR_, true );

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDE_qL(i,j) += dJ*(-phiL_[j]*invds[d])*PDE_gradq;

            for (int j = 0; j < nDOFR_; j++)
              mtxElemL.PDE_qR(i,j) += dJ*( phiR_[j]*invds[d])*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemR.PDE_qL(i,j) += dJ*(-phiL_[j]*invds[d])*PDE_gradq;

            for (int j = 0; j < nDOFR_; j++)
              mtxElemR.PDE_qR(i,j) += dJ*( phiR_[j]*invds[d])*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient
}

template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
template<class Tq, class Tg, class Ti>
void
IntegrandInteriorTrace_FV_TPFA<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
weightedIntegrand( const VectorX& nL,
                   const ParamTL& paramL, const ParamTR& paramR,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                   const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                   ArrayQ<Ti> integrandL[], const int neqnL,
                   ArrayQ<Ti> integrandR[], const int neqnR,
                   bool gradientOnly ) const
{
  ArrayQ<Ti> Fn; // normal flux

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0;

  // advective flux term: +[[phi]].flux = (phi@L - phi@R) nL.flux
  //                                    = +phiL nL.flux; -phiR nL.flux
  if (pde_.hasFluxAdvective() && !gradientOnly)
  {
    Fn = 0;
    pde_.fluxAdvectiveUpwind( paramL, qL, qR, nL, Fn );

    for (int k = 0; k < neqnL; k++)
      integrandL[k] +=  phiL_[k]*Fn;

    for (int k = 0; k < neqnR; k++)
      integrandR[k] += -phiR_[k]*Fn;
  }

  // viscous flux term: +[[phi]].flux = (phi@L - phi@R) nL.flux
  //                                  = +phiL nL.flux; -phiR nL.flux
  if (pde_.hasFluxViscous())
  {
    Fn = 0;
    pde_.fluxViscous( paramL, paramR, qL, gradqL, qR, gradqR, nL, Fn );

    // add normal flux
    for (int k = 0; k < neqnL; k++)
      integrandL[k] +=  phiL_[k]*Fn;

    for (int k = 0; k < neqnR; k++)
      integrandR[k] += -phiR_[k]*Fn;
  }

  // source trace term: +phi S
  if (pde_.hasSourceTrace())
  {
    ArrayQ<Ti> sourceL=0, sourceR=0; // PDE source S(X, Q, QX)

    pde_.sourceTrace( paramL, paramR, qL, gradqL, qR, gradqR, sourceL, sourceR );

    for (int k = 0; k < neqnL; k++)
      integrandL[k] += phiL_[k]*sourceL;

    for (int k = 0; k < neqnR; k++)
      integrandR[k] += phiR_[k]*sourceR;
  }
}

} // namespace SANS
#endif //INTEGRANDINTERIORTRACE_FV_TPFA_H
