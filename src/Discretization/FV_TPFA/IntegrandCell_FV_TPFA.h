// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_FV_TPFA_H
#define INTEGRANDCELL_FV_TPFA_H

// cell integrand operator: Finite volume with two point flux approximation

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template <class PDE_>
class IntegrandCell_FV_TPFA : public IntegrandCellType< IntegrandCell_FV_TPFA<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_FV_TPFA( const PDE& pde, const std::vector<int>& CellGroups )
    : pde_(pde), cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted(  const PDE& pde,
                    const ElementParam& paramfldElem,
                    const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem ) :
                      pde_(pde),
                      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                      qfldElem_(qfldElem),
                      paramfldElem_( paramfldElem ),
                      nDOF_( qfldElem_.nDOF() ),
                      phi_( new Real[nDOF_] )
    {
    }

    ~BasisWeighted()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element residual integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn ) const;

    template<class Ti>
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

  protected:
    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const ParamT& param,
                            const ArrayQ<Tq>& q,
                            const VectorArrayQ<Tg>& gradq,
                            ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_FV_TPFA<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasSource() || pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_FV_TPFA<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn) const
{
  SANS_ASSERT(neqn == nDOF_);
  SANS_ASSERT(nDOF_ == 1); //This is finite volume, so check if there's only 1 DOF per elem

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                 // solution
  VectorArrayQ<T> gradq = 0;   // gradient - this is always set to zero for now (no access to gradient on cell)

  if ( pde_.hasSource() && pde_.needsSolutionGradientforSource() )
  {
    SANS_DEVELOPER_EXCEPTION("IntegrandCell_FV_TPFA - Source cannot depend on state gradient yet.");
  }

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );

  // compute the residual
  weightedIntegrand( param, q, gradq, integrand, neqn);
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_FV_TPFA<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti>>& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOF_);
  SANS_ASSERT(nDOF_ == 1); //This is finite volume, so check if there's only 1 DOF per elem

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                 // solution
  VectorArrayQ<T> gradq = 0;   // gradient - this is always set to zero for now (no access to gradient on cell)

  if ( pde_.hasSource() && pde_.needsSolutionGradientforSource() )
  {
    SANS_DEVELOPER_EXCEPTION("IntegrandCell_FV_TPFA - Source cannot depend on state gradient yet.");
  }

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );

  // compute the residual
  DLA::VectorD<ArrayQ<Ti>> integrand(nDOF_);

  weightedIntegrand( param, q, gradq, integrand.data(), integrand.size());

  for (int k = 0; k < nDOF_; ++k)
    rsdPDEElem(k) += dJ*integrand(k);
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_FV_TPFA<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxPDEElem.m() == nDOF_);
  SANS_ASSERT(mtxPDEElem.n() == nDOF_);
  SANS_ASSERT(nDOF_ == 1); //This is finite volume, so check if there's only 1 DOF per elem

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;                 // solution
  VectorArrayQ<Real> gradq = 0;   // gradient - this is always set to zero for now (no access to gradient on cell)

  ArrayQ<SurrealClass> qSurreal = 0;               // solution
//  VectorArrayQ<SurrealClass> gradqSurreal = 0;     // gradient

  MatrixQ<Real> PDE_q = 0; // temporary storage

  if ( pde_.hasSource() && pde_.needsSolutionGradientforSource() )
  {
    SANS_DEVELOPER_EXCEPTION("IntegrandCell_FV_TPFA - Source cannot depend on state gradient yet.");
  }

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qSurreal = q;

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    weightedIntegrand( param, qSurreal, gradq, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;
  } // nchunk
}

// Cell integrand
//
// integrand = phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tg, class Ti>
void
IntegrandCell_FV_TPFA<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
weightedIntegrand( const ParamT& param,
                   const ArrayQ<Tq>& q,
                   const VectorArrayQ<Tg>& gradq,
                   ArrayQ<Ti> integrand[], int neqn ) const
{
  VectorArrayQ<Ti> F;           // PDE flux F(X, Q), Fv(X, Q, QX)
  ArrayQ<Ti> source;            // PDE source S(X, Q, QX)

  // using neqn here suppresses clang analyzer
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // Galerkin source term: +phi S

  if (pde_.hasSource())
  {
    SANS_ASSERT( !pde_.needsSolutionGradientforSource() ); //Source term cannot depend on state gradient yet

    source = 0;
    pde_.source( param, q, gradq, source );

    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < neqn; k++)
      integrand[k] -= phi_[k]*forcing;
  }
}

}

#endif  // INTEGRANDCELL_FV_TPFA_H
