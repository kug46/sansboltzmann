// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_FV_TPFA_H
#define ALGEBRAICEQUATIONSET_FV_TPFA_H

#include <vector>
#include <map>
#include <string>

#include "tools/SANSnumerics.h"
#include "tools/KahanSum.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "pde/BCParameters.h"

#include "Field/FieldTypes.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/QuadratureOrder.h"
#include "Discretization/ResidualNormType.h"

#include "Discretization/FV_TPFA/IntegrandCell_FV_TPFA.h"
#include "Discretization/FV_TPFA/IntegrandInteriorTrace_FV_TPFA.h"
#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualInteriorTrace_Galerkin.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"

#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/UpdateFraction/UpdateFractionCell.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>
#include "tools/plus_std_vector.h"
#endif

namespace SANS
{

// AlgebraicEquationSet for a finite volume method with a two-point flux approximation (TPFA)

//----------------------------------------------------------------------------//
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class XFieldType>
class AlgebraicEquationSet_FV_TPFA : public AlgebraicEquationSet_Debug<NDPDEClass_, Traits>
{
public:
  typedef NDPDEClass_ NDPDEClass;
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef BCParameters<BCVector> BCParams;

  typedef IntegrandCell_FV_TPFA<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_FV_TPFA<NDPDEClass> IntegrandTraceClass;
//  typedef IntegrateBoundaryTrace_Dispatch_DGBR2<NDPDEClass, BCNDConvert, BCVector, DiscTag> IntegrateBoundaryTrace_DispatchClass;

  template< class... BCArgs >
  AlgebraicEquationSet_FV_TPFA(const XFieldType&                         xfld,
                                Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                const NDPDEClass& pde,
                                const QuadratureOrder& quadratureOrder,
                                const ResidualNormType& resNormType,
                                const std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                const std::vector<int>& InteriorTraceGroups,
                                PyDict& BCList,
                                const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                BCArgs&... args) :
                                  DebugBaseType(pde, tol),
                                  fcnCell_(pde, CellGroups),
                                  fcnTrace_( pde, InteriorTraceGroups ),
                                  BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
//                                  dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups),
                                  xfld_(xfld), qfld_(qfld),
                                  pde_(pde),
                                  quadratureOrder_(quadratureOrder),
                                  quadratureOrderMin_(get<-1>(xfld), 0),
                                  resNormType_(resNormType), tol_(tol)
  {
    SANS_ASSERT(tol_.size() == 1);

    //Ensure 1-point quadrature for all interior and boundary traces
    for (int i = 0; i < (int) quadratureOrder_.interiorTraceOrders.size(); i++)
      SANS_ASSERT(quadratureOrder_.interiorTraceOrders[i] == 0);

    for (int i = 0; i < (int) quadratureOrder_.boundaryTraceOrders.size(); i++)
      SANS_ASSERT(quadratureOrder_.boundaryTraceOrders[i] == 0);
  }

  virtual ~AlgebraicEquationSet_FV_TPFA() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz) override;

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override;
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override;

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector and matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // update fraction needed for physically valid state
  virtual Real updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const override;

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override;

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override;

  virtual void syncDOFs_MPI() override
  {
    qfld_.syncDOFs_MPI_Cached();
  }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  virtual void dumpSolution(const std::string& filename) const override
  {
    output_Tecplot(qfld_, filename);
  }

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iq = 0;

//  const IntegrateBoundaryTrace_DispatchClass& dispatchBC() const { return dispatchBC_; }

  const QuadratureOrder& quadratureOrder() const { return quadratureOrder_; }
  const QuadratureOrder& quadratureOrderMin() const { return quadratureOrderMin_; }

protected:
  template<class SparseMatrixType>
  void jacobian( SparseMatrixType mtx, const QuadratureOrder& quadratureOrder,
                 const std::vector<int>& interiorTraceCellJac = {} );

  IntegrandCellClass fcnCell_;
  IntegrandTraceClass fcnTrace_;
  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
//  IntegrateBoundaryTrace_DispatchClass dispatchBC_;

  const XFieldType& xfld_;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  const NDPDEClass& pde_;
  QuadratureOrder quadratureOrder_;
  QuadratureOrder quadratureOrderMin_;
  const ResidualNormType resNormType_;
  const std::vector<Real> tol_;
};


//Fills jacobian or the non-zero pattern of a jacobian
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SystemMatrixView& mtx)
{
  this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SystemNonZeroPatternView& nz)
{
  this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
}

//Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobianTranspose(SystemMatrixView& mtxT)
{
  jacobian(Transpose(mtxT), quadratureOrder_ );
}
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobianTranspose(SystemNonZeroPatternView& nzT)
{
  jacobian(Transpose(nzT), quadratureOrderMin_ );
}


//Evaluate Residual Norm
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
std::vector<std::vector<Real>>
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residualNorm( const SystemVectorView& rsd ) const
{
  const int nDOFPDEpossessed = rsd[iPDE].m();
  const int nMon = pde_.nMonitor();

  DLA::VectorD<Real> rsdPDEtmp(nMon);
  DLA::VectorD<Real> rsdBCtmp(nMon);

  rsdPDEtmp = 0;

  std::vector<std::vector<Real>> rsdNorm(nResidNorm(), std::vector<Real>(nMon, 0));
  std::vector<KahanSum<Real>> rsdNormKahan(nMon, 0);

  //PDE residual norm
  if (resNormType_ == ResidualNorm_L2)
  {
    for (int n = 0; n < nDOFPDEpossessed; n++)
    {
      pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = rsdNormKahan[j];

#ifdef SANS_MPI
    rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
#endif

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);
  }
  else
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_FV_TPFA::residualNorm - Unknown residual norm type!");

  return rsdNorm;
}

// prints a residual decrease failure
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const
{
  titles = {"PDE : "};
  idx = {iPDE};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{

  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd(iq)),
                                           xfld_, qfld_,
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );


  IntegrateInteriorTraceGroups<TopoDim>::integrate(ResidualInteriorTrace_Galerkin(fcnTrace_, rsd(iq)),
                                                   xfld_, qfld_,
                                                   quadratureOrder_.interiorTraceOrders.data(),
                                                   quadratureOrder_.interiorTraceOrders.size() );

#if 0
  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin( xfld_, qfld_, lgfld_,
                                                          quadratureOrder_.boundaryTraceOrders.data(),
                                                          quadratureOrder_.boundaryTraceOrders.size(),
                                                          rsd(iq), rsd(ilg) ),
      ResidualBoundaryTrace_Dispatch_DGBR2( xfld_, qfld_, rfld_,
                                            quadratureOrder_.boundaryTraceOrders.data(),
                                            quadratureOrder_.boundaryTraceOrders.size(),
                                            rsd(iq) ),
      ResidualBoundaryTrace_Dispatch_Galerkin( xfld_, qfld_,
                                               quadratureOrder_.boundaryTraceOrders.data(),
                                               quadratureOrder_.boundaryTraceOrders.size(),
                                               rsd(iq) ) );
#endif
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian( SparseMatrixType jac, const QuadratureOrder& quadratureOrder,
          const std::vector<int>& interiorTraceCellJac )
{

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE,iq);

  IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin(fcnCell_, jacPDE_q),
                                           xfld_, qfld_, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  IntegrateInteriorTraceGroups<TopoDim>::integrate( JacobianInteriorTrace_Galerkin(fcnTrace_, jacPDE_q),
                                                    xfld_, qfld_,
                                                    quadratureOrder.interiorTraceOrders.data(),
                                                    quadratureOrder.interiorTraceOrders.size() );

#if 0
  typedef SurrealS<NDPDEClass::N> SurrealClass;

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_mitLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_, lgfld_,
                                                                   quadratureOrder.boundaryTraceOrders.data(),
                                                                   quadratureOrder.boundaryTraceOrders.size(),
                                                                   jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg ),
      JacobianBoundaryTrace_Dispatch_DGBR2<SurrealClass>( xfld_, qfld_, rfld_, jacPDE_R,
                                                          quadratureOrder.boundaryTraceOrders.data(),
                                                          quadratureOrder.boundaryTraceOrders.size(),
                                                          jacPDE_q ),
      JacobianBoundaryTrace_sansLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_,
                                                                    quadratureOrder.boundaryTraceOrders.data(),
                                                                    quadratureOrder.boundaryTraceOrders.size(),
                                                                    jacPDE_q ) );
#endif
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld_.DOF(k);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();

  return {nDOFPDEpos};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorStateSize() const
{
  static_assert(iPDE == 0,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();

  return {nDOFPDE};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iq == 0,"");

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();

  return {{ {nDOFPDEpos, nDOFPDE} }};
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
Real
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const
{
  Field<PhysDim, TopoDim, ArrayQ> dqfld(qfld_, FieldCopy());

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  SANS_ASSERT( nDOFPDE == dq[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
  {
    qfld_.DOF(k) = q[iq][k];
    dqfld.DOF(k) = dq[iq][k];
  }

  Real updateFraction = 1;

  IntegrateCellGroups<TopoDim>::integrate( UpdateFractionCell(pde_, fcnCell_.cellGroups(), maxChangeFraction, updateFraction),
                                           xfld_, (qfld_, dqfld),
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );

#ifdef SANS_MPI
  return boost::mpi::all_reduce(*qfld_.comm(), updateFraction, boost::mpi::minimum<Real>());
#else
  return updateFraction;
#endif
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
bool
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  // Update the solution field
  setSolutionField(q);

  bool isValidState = true;

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                           xfld_, qfld_,
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );

#ifdef SANS_MPI
  int validstate = isValidState ? 1 : 0;
  isValidState = (boost::mpi::all_reduce(*qfld_.comm(), validstate, std::plus<int>()) == qfld_.comm()->size());
#endif

  return (isValidState);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
std::vector<GlobalContinuousMap>
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
continuousGlobalMap() const
{
  return {qfld_.continuousGlobalMap()};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
std::shared_ptr<mpi::communicator>
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
comm() const
{
  return qfld_.comm();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
int
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
nResidNorm() const
{
  return 1;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_FV_TPFA<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_FV_TPFA_H
