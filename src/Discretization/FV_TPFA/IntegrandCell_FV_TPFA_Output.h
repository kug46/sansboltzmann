// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_FV_TPFA_OUTPUT
#define INTEGRANDCELL_FV_TPFA_OUTPUT

#include <vector>

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand for generic output functional

template <class OutputFunctional>
class IntegrandCell_FV_TPFA_Output :
    public IntegrandCellType< IntegrandCell_FV_TPFA_Output<OutputFunctional> >
{
public:
  typedef typename OutputFunctional::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename OutputFunctional::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename OutputFunctional::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = typename OutputFunctional::template ArrayJ<T>;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = typename OutputFunctional::template MatrixJ<T>;

  // cppcheck-suppress noExplicitConstructor
  IntegrandCell_FV_TPFA_Output( const OutputFunctional& outputFcn, const std::vector<int>& cellGroups ) :
    outputFcn_(outputFcn), cellGroups_(cellGroups)
  {
    // Nothing
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim        , TopoDim, Topology> ElementXFieldType;
    typedef Element      <ArrayQ<T>      , TopoDim, Topology> ElementQFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;

    Functor( const OutputFunctional& outputFcn,
             const ElementParam& paramfldElem,
             const ElementQFieldType& qfldElem) :
      outputFcn_(outputFcn),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
      qfldElem_(qfldElem)
    {
    }

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // element output integrand
    void operator()( const QuadPointType& sRef, ArrayJ<T>& integrand ) const
    {
      VectorX X;       // physical coordinates
      ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

      ArrayQ<T> q;               // solution
      VectorArrayQ<T> gradq = 0; // gradient

      if ( outputFcn_.needsSolutionGradient() )
        SANS_DEVELOPER_EXCEPTION("IntegrandCell_FV_TPFA_Output - Output cannot depend on state gradient yet.");

      // Elemental parameters
      paramfldElem_.eval( sRef, param );

      // solution value, gradient
      qfldElem_.eval( sRef, q );

      outputFcn_(param, q, gradq, integrand);
    }

  protected:
    const OutputFunctional& outputFcn_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  Functor<T,TopoDim,Topology,ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return Functor<T,TopoDim,Topology,ElementParam>(outputFcn_,
                                                    paramfldElem,
                                                    qfldElem);
  }

private:
  const OutputFunctional& outputFcn_;
  const std::vector<int> cellGroups_;
};

}

#endif //INTEGRANDCELL_FV_TPFA_OUTPUT
