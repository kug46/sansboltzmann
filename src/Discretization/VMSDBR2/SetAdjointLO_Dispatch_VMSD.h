// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETADJOINTLO_DISPATCH_VMSD_H
#define SETADJOINTLO_DISPATCH_VMSD_H

// boundary-trace integral adjoint lifting operator functions

#include "SetFieldBoundaryTrace_VMSD_AdjointLO.h"

#include "Field/FieldData/FieldDataMatrixD_CellLift.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_BoundaryCell.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without field trace
//
//---------------------------------------------------------------------------//
template<class SurrealClass, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
class SetAdjointLO_Dispatch_VMSD_impl
{
public:
  SetAdjointLO_Dispatch_VMSD_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld,
      const FieldDataInvMassMatrix_Cell& mmfld,
      const int* quadratureorder, int ngroup )
    : xfld_(xfld), qfld_(qfld), qpfld_(qpfld), rbfld_(rbfld), wfld_(wfld), sbfld_(sbfld), mmfld_(mmfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_BoundaryCell<TopoDim>::integrate(
        SetFieldBoundaryTrace_VMSD_AdjointLO<SurrealClass>(fcn, mmfld_),
        xfld_, (qfld_, qpfld_, wfld_), (rbfld_, sbfld_), quadratureorder_, ngroup_);
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template<class SurrealClass, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
SetAdjointLO_Dispatch_VMSD_impl<SurrealClass, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>
SetAdjointLO_Dispatch_VMSD(const XFieldType& xfld,
                                         const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                         const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                         const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
                                         const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                         const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld,
                                         const FieldDataInvMassMatrix_Cell& mmfld,
                                         const int* quadratureorder, int ngroup )
{
  return SetAdjointLO_Dispatch_VMSD_impl<SurrealClass, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
      xfld, qfld, qpfld, rbfld, wfld, sbfld, mmfld, quadratureorder, ngroup);
}

}

#endif //SETADJOINTLO_DISPATCH_VMSD_H
