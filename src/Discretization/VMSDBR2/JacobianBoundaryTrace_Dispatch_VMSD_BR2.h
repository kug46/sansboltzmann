// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DISPATCH_VMSD_BR2_H
#define JACOBIANBOUNDARYTRACE_DISPATCH_VMSD_BR2_H

// boundary-trace integral jacobian functions

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_BoundaryCell.h"

#include "JacobianBoundaryTrace_VMSD_BR2.h"

namespace SANS
{


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class MatrixQ>
class JacobianBoundaryTrace_Dispatch_VMSD_BR2_impl
{
public:
  JacobianBoundaryTrace_Dispatch_VMSD_BR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
      const int* quadratureorder, int ngroup,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
      const FieldDataInvMassMatrix_Cell& mmfld,
      FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDE_qp,
      FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDEp_q )
    : xfld_(xfld), qfld_(qfld), qpfld_(qpfld), rbfld_(rbfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_q_(mtxGlobalPDE_q), mmfld_(mmfld),
      boundJacPDE_qp_(boundJacPDE_qp),
      boundJacPDEp_q_(boundJacPDEp_q)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    boundJacPDE_qp_ = 0;
    boundJacPDEp_q_ = 0;

    IntegrateBoundaryTraceGroups_BoundaryCell<TopoDim>::integrate(
        JacobianBoundaryTrace_VMSD_BR2<Surreal>(fcn, mtxGlobalPDE_q_, mmfld_, boundJacPDE_qp_, boundJacPDEp_q_),
        xfld_, (qfld_, qpfld_), rbfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDE_qp_;
  FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDEp_q_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class MatrixQ>
JacobianBoundaryTrace_Dispatch_VMSD_BR2_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, MatrixQ>
JacobianBoundaryTrace_Dispatch_VMSD_BR2(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                             const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
                                             const int* quadratureorder, int ngroup,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                             const FieldDataInvMassMatrix_Cell& mmfld,
                                             FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDE_qp,
                                             FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDEp_q )
{
  return { xfld, qfld, qpfld, rbfld, quadratureorder, ngroup, mtxGlobalPDE_q, mmfld, boundJacPDE_qp, boundJacPDEp_q  };
}


}

#endif //JACOBIANBOUNDARYTRACE_DISPATCH_VMSD_H
