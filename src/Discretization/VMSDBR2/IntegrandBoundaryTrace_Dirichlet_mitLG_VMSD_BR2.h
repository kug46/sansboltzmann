// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_DIRICHLET_MITLG_VMSD_BR2_H
#define INTEGRANDBOUNDARYTRACE_DIRICHLET_MITLG_VMSD_BR2_H

// boundary integrand operators

#include <ostream>
#include <vector>
#include <type_traits> //std::is_same
#include <cmath>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Integrand_VMSDBR2_fwd.h"
#include "Discretization/VMSD/Discretization_VMSD.h"

namespace SANS
{

class VMSDBR2;

// forward declaration
class BCTypeReflect_mitState;

//----------------------------------------------------------------------------//
// element boundary integrand: PDE


template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, VMSDBR2> :
public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, VMSDBR2> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Dirichlet_mitLG Category;
  typedef VMSDBR2 DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;    // solution/residual jacobians

  static const int N = PDE::N;
  static const int D = PDE::D;

  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const DiscretizationVMSD& stab)
  : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups), stab_(stab)
  {
    SANS_DEVELOPER_EXCEPTION("VMSDBR2 DIRICHLET MITLG NOT IMPLEMENTED\n");
  }

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template<class T, class Tp, class Tr, class TopoDimTrace, class TopologyTrace,
  class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<ArrayQ<Tp>, TopoDimCell, Topology> ElementQPFieldCell;
    typedef Element<VectorArrayQ<Tr>, TopoDimCell, Topology> ElementRFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template<class Ti>
    using IntegrandType = ArrayQ<Ti>;

    BasisWeighted( const PDE& pde, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementQPFieldCell& qpfldElem,
                   const ElementRFieldCell& rfldElem,
                   const DiscretizationVMSD& stab) :
                     pde_(pde), bc_(bc),
                     xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem),
                     qpfldElem_(qpfldElem),
                     rfldElem_(rfldElem),
                     paramfldElem_( paramfldElem ),
                     nDOF_(qfldElem_.nDOF()),
                     nDOFp_(qpfldElem_.nDOF()),
                     phi_( new Real[nDOF_] ),
                     gradphi_( new VectorX[nDOF_] ),
                     gradphip_( new VectorX[nDOF_] ),
                     stab_(stab)
    {}

    BasisWeighted( BasisWeighted&& bw ) :
                     pde_(bw.pde_), bc_(bw.bc_),
                     xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                     xfldElem_(bw.xfldElem_),
                     qfldElem_(bw.qfldElem_),
                     qpfldElem_(bw.qpfldElem_),
                     rfldElem_(bw.rfldElem_),
                     paramfldElem_( bw.paramfldElem_ ),
                     nDOF_( bw.nDOF_ ),
                     nDOFp_( bw.nDOFp_ ),
                     phi_( bw.phi_ ),
                     gradphi_( bw.gradphi_ ),
                     gradphip_( bw.gradphip_ ),
                     stab_(bw.stab_)
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;  bw.gradphip_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandType<Ti> integrand[], int neqn,
                                                          IntegrandType<Ti> integrandp[], int neqnp) const;

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Tq, class Tg, class Ti>
    void operator()( const BC& bc, const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
                     const ParamT& paramI, const VectorX& nL,
                     ArrayQ<Ti> integrand[], int neqn,
                     ArrayQ<Ti> integrandp[], int neqnp) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQPFieldCell& qpfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nDOFp_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable VectorX *gradphip_;
    const DiscretizationVMSD& stab_;
  };



  template <class Tq, class Tr,
            class TopoDimTrace, class TopologyTrace,
            class TopoDimCell,  class Topology, class ElementParam >
  class BasisWeighted_LO
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Tr>, TopoDimCell, Topology> ElementRFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef DLA::MatrixD<VectorMatrixQ<Real>> MatrixLOElemClass;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef VectorArrayQ<Tr> IntegrandType;

    BasisWeighted_LO( const PDE& pde,
                      const BCBase& bc,
                      const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                      const ElementParam& paramfldElem, // Xfield must be the last parameter
                      const ElementQFieldCell& qfldElem,
                      const ElementRFieldCell& rfldElem ) :
                      pde_(pde), bc_(bc),
                      xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                      xfldElem_(get<-1>(paramfldElem)),
                      qfldElem_(qfldElem), rfldElem_(rfldElem),
                      paramfldElem_(paramfldElem),
                      nDOF_(qfldElem_.nDOF()),
                      phi_( new Real[nDOF_] )

    {}

    BasisWeighted_LO( BasisWeighted_LO&& bw ):
                      pde_(bw.pde_), bc_(bw.bc_),
                      xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                      xfldElem_(bw.xfldElem_),
                      qfldElem_(bw.qfldElem_),
                      rfldElem_(bw.rfldElem_),
                      paramfldElem_(bw.paramfldElem_),
                      nDOF_(bw.nDOF_),
                      phi_(bw.phi_)

    {
      bw.phi_ = nullptr;
    }

    ~BasisWeighted_LO()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& RefTrace, VectorArrayQ<Ti> integrandLO[], int neqn ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandLO, neqn);
    }

    // boundary element Jacobian integrand wrt r
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace, MatrixLOElemClass& mtxElemL_rL ) const {}

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, VectorArrayQ<Ti> integrandLO[], int neqn ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
  };

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
  class TopoDimCell,  class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Tq>, TopoDimCell, Topology> ElementRFieldCell;

    typedef Element<ArrayQ<Tw>, TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<VectorArrayQ<Tw>, TopoDimCell, Topology> ElementSFieldCell;

    typedef Element<Real,       TopoDimCell, Topology> ElementEFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type IntegrandType;

    FieldWeighted( const PDE& pde, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementQFieldCell& qpfldElem,
                   const ElementRFieldCell& rfldElem,
                   const ElementWFieldCell& wfldElem,
                   const ElementWFieldCell& wpfldElem,
                   const ElementSFieldCell& sfldElem,
                   const ElementEFieldCell& efldElem,
                   const DiscretizationVMSD& stab ) :
                     pde_(pde), bc_(bc),
                     xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem), qpfldElem_(qpfldElem), rfldElem_(rfldElem),
                     wfldElem_(wfldElem), wpfldElem_(wpfldElem), sfldElem_(sfldElem),
                     efldElem_(efldElem),
                     paramfldElem_( paramfldElem ),
                     nDOF_(qfldElem_.nDOF()),
                     nPhi_(efldElem_.nDOF()),
                     ephi_( new Real[nPhi_] ),
                     gradephi_( new VectorX[nPhi_] ),
                     weight_( new ArrayQ<Tw>[nPhi_] ),
                     gradWeight_( new VectorArrayQ<Tw>[nPhi_] ),
                     gradphi_( new VectorX[nDOF_] ),
                     stab_(stab)
    {
      // Nothing
    }

    FieldWeighted( FieldWeighted&& fw ) :
                     pde_(fw.pde_), bc_(fw.bc_),
                     xfldElemTrace_(fw.xfldElemTrace_), canonicalTrace_(fw.canonicalTrace_),
                     xfldElem_(fw.xfldElem_),
                     qfldElem_(fw.qfldElem_),
                     qpfldElem_(fw.qpfldElem_), rfldElem_(fw.rfldElem_),
                     wfldElem_(fw.wfldElem_),
                     wpfldElem_(fw.wpfldElem_), sfldElem_(fw.sfldElem_),
                     efldElem_(fw.efldElem_),
                     paramfldElem_( fw.paramfldElem_ ),
                     nDOF_(fw.nDOF_),
                     nPhi_(fw.nPhi_),
                     ephi_( fw.ephi_ ),
                     gradephi_( fw.gradephi_ ),
                     weight_( fw.weight_ ),
                     gradWeight_( fw.gradWeight_ ),
                     gradphi_( fw.gradphi_ ),
                     stab_(fw.stab_)
    {
      fw.ephi_       = nullptr;
      fw.gradephi_   = nullptr;
      fw.weight_     = nullptr;
      fw.gradWeight_ = nullptr;
      fw.gradphi_    = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] ephi_;
      delete [] gradephi_;
      delete [] weight_;
      delete [] gradWeight_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // element trace integrand
    void operator()( const QuadPointTraceType& refTrace,  IntegrandType integrandL[], const int nphi) const
    {
      call_with_derived<NDBCVector>(*this, bc_, refTrace, integrandL, nphi);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType integrand[], const int nphi ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldCell& qpfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementWFieldCell& wpfldElem_;
    const ElementSFieldCell& sfldElem_;
    const ElementEFieldCell& efldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_, nPhi_;
    mutable Real *ephi_;
    mutable VectorX *gradephi_;
    mutable ArrayQ<Tw> *weight_;
    mutable VectorArrayQ<Tw> *gradWeight_;
    mutable VectorX *gradphi_;
    const DiscretizationVMSD& stab_;
  };

  template<class Tq, class Tw, class TopoDimCell,  class Topology, class ElementParam>
  class FieldWeightedCellLO
  {
  public:
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Tq>, TopoDimCell, Topology> ElementRFieldCell;

    typedef Element<ArrayQ<Tw>, TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<VectorArrayQ<Tw>, TopoDimCell, Topology> ElementSFieldCell;

    typedef Element<Real,       TopoDimCell, Topology> ElementEFieldCell;

    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimCell> QuadPointType;

    typedef typename promote_Surreal<Tq, Tw>::type IntegrandType;

    FieldWeightedCellLO( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const ElementRFieldCell& rfldElem,
                   const ElementSFieldCell& sfldElem,
                   const ElementEFieldCell& efldElem) :
                     pde_(pde),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     rfldElem_(rfldElem),
                     sfldElem_(sfldElem),
                     efldElem_(efldElem),
                     paramfldElem_( paramfldElem ),
                     nPhi_(efldElem_.nDOF()),
                     ephi_( new Real[nPhi_] )
    {
      // Nothing
    }

    FieldWeightedCellLO( FieldWeightedCellLO&& fw ) :
                     pde_(fw.pde_),
                     xfldElem_(fw.xfldElem_),
                     rfldElem_(fw.rfldElem_),
                     sfldElem_(fw.sfldElem_),
                     efldElem_(fw.efldElem_),
                     paramfldElem_( fw.paramfldElem_ ),
                     nPhi_(fw.nPhi_),
                     ephi_( fw.ephi_ )
    {
      fw.ephi_       = nullptr;
    }

    ~FieldWeightedCellLO()
    {
      delete [] ephi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nPhi_; }

    // cell element integrand
    template <class Ti>
    void operator()( const QuadPointType& sRef, Ti integrand[], int nphi ) const
    {
      SANS_ASSERT( nphi == nPhi_ );

      ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

      // estimate basis, gradient
      efldElem_.evalBasis( sRef, ephi_, nPhi_ );

      //LIFTING OPERATOR TERM
      VectorArrayQ<Tq> rLift;
      VectorArrayQ<Tw> sLift;

        // Lifting Operator
        // eval lifting operators from basis
        rfldElem_.eval( sRef, rLift );
        sfldElem_.eval( sRef, sLift );

        Ti tmpdot = dot(rLift,sLift);

        for (int k = 0; k < nPhi_; k++)
          integrand[k] = tmpdot*ephi_[k];
    }


  protected:

    const PDE& pde_;
    const ElementXFieldCell& xfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementSFieldCell& sfldElem_;
    const ElementEFieldCell& efldElem_;
    const ElementParam& paramfldElem_;

    const int nPhi_;
    mutable Real *ephi_;
  };


  template<class T, class Tp, class Tr, class TopoDimTrace, class TopologyTrace,
  class TopoDimCell, class Topology, class ElementParam>
  BasisWeighted<T, Tp, Tr, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology>& qfldElem,
            const Element<ArrayQ<Tp>    , TopoDimCell , Topology>& qpfldElem,
            const Element<VectorArrayQ<Tr>, TopoDimCell , Topology>& rfldElem) const
  {
    return {pde_, bc_,
      xfldElemTrace, canonicalTrace, paramfldElem, qfldElem, qpfldElem, rfldElem, stab_ };
  }

  template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
  class TopoDimCell, class Topology, class ElementParam>
  BasisWeighted_LO<T, Tr, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand_LO(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem,
            const Element<VectorArrayQ<Tr>    , TopoDimCell , Topology>& rfldElem) const
  {
    return {pde_, bc_,
      xfldElemTrace, canonicalTrace, paramfldElem, qfldElem, rfldElem };
  }

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
  class TopoDimCell, class Topology, class ElementParam>
  FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<Tq>, TopoDimCell, Topology>& qfldElem,
            const Element<ArrayQ<Tq>, TopoDimCell, Topology>& qpfldElem,
            const Element<VectorArrayQ<Tq>    , TopoDimCell , Topology>& rfldElem,
            const Element<ArrayQ<Tw>, TopoDimCell, Topology>& wfldElem,
            const Element<ArrayQ<Tw>, TopoDimCell, Topology>& wpfldElem,
            const Element<VectorArrayQ<Tw>    , TopoDimCell , Topology>& sfldElem,
            const Element<Real,       TopoDimCell, Topology>& efldElem) const
  {
    return {pde_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem,
            qfldElem, qpfldElem, rfldElem,
            wfldElem, wpfldElem, sfldElem, efldElem, stab_};
  }


  template<class Tq, class Tw, class TopoDimCell, class Topology, class ElementParam>
  FieldWeightedCellLO<Tq, Tw, TopoDimCell, Topology, ElementParam>
  integrandCellLO_FW(const ElementParam& paramfldElem,
            const Element<VectorArrayQ<Tq>    , TopoDimCell , Topology>& rfldElem,
            const Element<VectorArrayQ<Tw>    , TopoDimCell , Topology>& sfldElem,
            const Element<Real,       TopoDimCell, Topology>& efldElem) const
  {
    return {pde_, paramfldElem, rfldElem, sfldElem, efldElem};
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const DiscretizationVMSD& stab_;
};

template <class PDE, class NDBCVector>
template<class T, class Tp, class Tr, class TopoDimTrace, class TopologyTrace,
class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, VMSDBR2>::
BasisWeighted<T, Tp, Tr, TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrand[], int neqn, ArrayQ<Ti> integrandp[], int neqnp) const
{
//  SANS_ASSERT( neqn == nDOF_ );
//
//  ParamT paramL;             // Elemental parameters (such as grid coordinates and distance functions)
//  VectorX nL;                // unit normal (points out of domain)
//
//  ArrayQ<T> qI;              // interior solution
////  ArrayQ<T> qpI;              // interior perturbation not needed
//  VectorArrayQ<T> gradqI;    // interior gradient
//  VectorArrayQ<Tr> rLift;    // interior gradient
//
//  QuadPointCellType sRef;    // reference-element coordinates (s,t)
//
//  // adjacent area-element reference coords
//  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );
//
//  // Elemental parameters
//  paramfldElem_.eval( sRef, paramL );
//
//  // unit normal: points out of domain
//  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL);
//
//  // basis value, gradient
//  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
//
//  // gradients
//  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
//
//  // solution value
//  // solution value
//  qfldElem_.evalFromBasis( phi_, nDOF_, qI );
//  rfldElem_.evalFromBasis( phi_, nDOF_, rLift );
//
//  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradqI );
//
//  Real eta = 2.0*Topology::NTrace;
//  gradqI += eta*rLift;
//
//  call_with_derived<NDBCVector>(*this, bc_, qI, gradqI, paramL, nL, integrand, neqn);
}

template <class PDE, class NDBCVector>
template <class T, class Tp, class Tr, class TopoDimTrace, class TopologyTrace,
class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, VMSDBR2>::
BasisWeighted<T, Tp, Tr, TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
            const ParamT& paramL, const VectorX& nL,
            ArrayQ<Ti> integrand[], int neqn,
            ArrayQ<Ti> integrandp[], int neqnp ) const
{
//  //  SANS_ASSERT_MSG( !(std::is_same<typename BC::BCType, BCTypeReflect_mitState>::value), "Not tested for reflection BC yet" );
//  /// BOUNDARY CONDITION
//  // BC state
//  ArrayQ<T> qB;
//  bc.state( paramL, nL, qI, qB );
//
//  // normal flux
//  ArrayQ<T> Fn = 0;
//  bc.fluxNormal( paramL, nL, qI, gradqI, qB, Fn );
//
//  // PDE residual: weak form boundary integral
//  for (int k = 0; k < neqn; k++)
//    integrand[k] = phi_[k]*Fn;
//
//  if (bc.hasFluxViscous())
//  {
//
//    // viscous flux dual consistency term
//    ArrayQ<T> dq = qI - qB;
//    VectorArrayQ<Ti> Kndu = 0;
//    if (pde_.fluxViscousLinearInGradient())
//    {
//      VectorArrayQ<Tq> dqpN = 0;
//      for (int i=0; i<PhysDim::D; i++)
//        dqpN[i] -= dq*nL[i];  //negate because fluxViscous returns -K*duN
//
//      //THIS IS A FUNCTION THAT DOES K*(uI -uB)
//      pde_.fluxViscous(paramL, qB, dqpN, Kndu);
//    }
//    else
//    {
//      // viscous flux dual consistency term
//      MatrixQ<T> dudq = 0;
//      pde_.jacobianMasterState( paramL, qB, dudq );
//
//      ArrayQ<T> du = dudq*dq;
//      VectorArrayQ<Tq> duN = 0;
//
//      for (int i=0; i<PhysDim::D; i++)
//        duN[i] += du*nL[i];  //negate because perturbedGradFluxViscous returns -K*duN
//
//      DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<Ti>> K = 0;   // diffusion matrix
//      pde_.diffusionViscous( paramL, qB, gradqI, K );
//
//      Kndu = K*duN;
//    }
//
//    //DUAL CONSISTENCY
//    for (int k = 0; k < neqn; k++)
//      integrand[k] -= dot(gradphi_[k],Kndu);
//
//    //SIP
//    ArrayQ<Ti> tmpC = Cb_*invL_*dot(nL, Kndu);
//
//    for (int k = 0; k < neqn; k++)
//      integrand[k] += phi_[k]*tmpC;
//
//
////    dual consistent source term?
//#if 0
//    VectorMatrixQ<Ti> dSgradu = 0;
//    pde_.jacobianGradientSource(paramL, qI, gradqI, dSgradu);
//
//    MatrixQ<Ti> ndSgradu = 0;
//    for (int i=0; i<D; i++)
//      ndSgradu = nL[i]*dSgradu[i];
//
//    MatrixQ<T> dudq = 0;
//    pde_.jacobianMasterState( paramL, qI, dudq );
//
//    ArrayQ<T> du = dudq*dq;
//
//    ArrayQ<Ti> tmp = ndSgradu*du;
//
//    //DUAL CONSISTENCY
//    for (int k = 0; k < neqn; k++)
//      integrand[k] -= phi_[k]*tmp;
//#endif
//  }

}



template <class PDE, class NDBCVector>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, VMSDBR2>::
BasisWeighted_LO<T, Tr, TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandLO[], int neqn ) const
{
//  SANS_ASSERT( neqn == nDOF_ );
//
//  ParamT paramL;               // Elemental parameters (such as grid coordinates and distance functions)
//
//  VectorX nL;                   // unit normal (points out of domain)
//
//  ArrayQ<Tq> qL;                // interior solution
//
//  QuadPointCellType sRef;       // reference-element coordinates (s,t)
//
//  // adjacent area-element reference coords
//  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );
//
//  // Elemental parameters
//  paramfldElem_.eval( sRef, paramL );
//
//  // unit normal: points out of domain
//  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL );
//
//  // basis value, gradient
//  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
//
//  // solution value, gradient, lifting operators, viscous eta parameter
//  qfldElem_.evalFromBasis( phi_, nDOF_, qL );
//
//  // BC state
//  ArrayQ<Tq> qB;
//  bc.state( paramL, nL, qL, qB );
//
//  //jump in q
//  VectorArrayQ<Tq> dqn;
//  for (int d = 0; d < D; d++)
//    dqn[d] = nL[d]*(qL - qB);
//
//  Real avg = std::is_same<typename BC::BCType, BCTypeReflect_mitState>::value ? 0.5 : 1;
//
//  //Lifting operator residuals:
//  for (int k = 0; k < neqn; k++)
//    integrandLO[k] = avg*phi_[k]*dqn;
}


template <class PDE, class NDBCVector>
template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
class TopoDimCell,  class Topology, class ElementParam >
template<class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, VMSDBR2>::
FieldWeighted<Tq, Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType integrand[], const int nphi ) const
{
//
////  WORK THIS OUT IN A LITTLE BIT...
//
//  SANS_ASSERT_MSG( !(std::is_same<typename BC::BCType, BCTypeReflect_mitState>::value), "Not tested for reflection BC yet" );
//  SANS_ASSERT( nphi == nPhi_ );
//
//  ParamT paramL;             // Elemental parameters (such as grid coordinates and distance functions)
//  VectorX nL;                // unit normal (points out of domain)
//
//  ArrayQ<Tq> qI;             // interior solution
//  ArrayQ<Tw> w;              // interior weight
//  VectorArrayQ<Tq> gradqI;   // interior gradient
//  VectorArrayQ<Tq> gradqpI;   // interior gradient
//  ArrayQ<Tq> qB;             // boundary solution
//
//  QuadPointCellType sRef;    // reference-element coordinates (s,t)
//
//  const bool needsSolutionGradient = pde_.hasFluxViscous();
//
//  // adjacent area-element reference coords
//  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );
//
//  // Elemental paramters
//  paramfldElem_.eval( sRef, paramL );
//
//  // unit normal: points out of domain
//  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL);
//
//  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
//
//  // basis value, gradient
//  qfldElem_.eval( sRef, qI );
//  wfldElem_.eval( sRef, w );
//
//  // solution value, gradient
//  if (needsSolutionGradient)
//  {
//    xfldElem_.evalGradient( sRef, qfldElem_, gradqI );
////    xfldElem_.evalGradient( sRef, qpfldElem_, gradqpI );
//  }
//  else
//  {
//    gradqI = 0;
//  }
//  gradqpI = 0;
//
////  sum the gradients...
////  gradqI += gradqpI;
//
//  VectorArrayQ<Tw> gradwI;   // interior gradient
//  xfldElem_.evalGradient( sRef, wfldElem_, gradwI );
//
//  // estimate basis
//  efldElem_.evalBasis( sRef, ephi_, nPhi_ );
//  xfldElem_.evalBasisGradient( sRef, efldElem_, gradephi_, nPhi_ );
//
//  for (int k = 0; k < nPhi_; k++)
//  {
//    weight_[k] = ephi_[k]*w;
//    gradWeight_[k] = gradephi_[k]*w + ephi_[k]*gradwI;
//  }
//
//  // BC state
//  bc.state( paramL, nL, qI, qB );
//
//  // normal flux
//  ArrayQ<Tq> Fn = 0;
//  bc.fluxNormal( paramL, nL, qI, gradqI, qB, Fn );
//
//  // PDE residual: weak form boundary integral
//  for (int k = 0; k < nPhi_; k++)
//    integrand[k] = dot(weight_[k],Fn);
//
//  if ( bc.hasFluxViscous() )
//  {
//
//    VectorArrayQ<Tq> Kndu = 0;
//    ArrayQ<Tq> dq = qI - qB;
//    if (pde_.fluxViscousLinearInGradient())
//    {
//      // if flux is linear in gradient, can just call the viscous flux
//      VectorArrayQ<Tq> dqpN = 0;
//      for (int i=0; i<PhysDim::D; i++)
//        dqpN[i] -= dq*nL[i];  //negate because perturbedGradFluxViscous returns -K*duN
//
//      //THIS IS A FUNCTION THAT DOES K*(uI -uB)
//      pde_.fluxViscous(paramL, qB, dqpN, Kndu);
//    }
//    else
//    {
//      // if flux isn't linear in gradient, have to construct K*du
//      MatrixQ<Tq> dudq = 0;
//      pde_.jacobianMasterState( paramL, qB, dudq );
//      ArrayQ<Tq> du = dudq*dq;
//      VectorArrayQ<Tq> duN = 0;
//
//      for (int i=0; i<PhysDim::D; i++)
//        duN[i] += du*nL[i];
//
//      DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<Tq>> K = 0;   // diffusion matrix
//      pde_.diffusionViscous( paramL, qB, gradqI, K );
//
//      Kndu = K*duN;
//    }
//
//    for (int k = 0; k < nPhi_; k++)
//      integrand[k] -= dot(gradWeight_[k],Kndu);
//
//    ArrayQ<Tq> tmpC = Cb_*invL_*dot(nL,Kndu);
//
//    for (int k = 0; k < nPhi_; k++)
//      integrand[k] += dot(weight_[k],tmpC);
//
//////    dual consistent source term?
//
//#if 0
//    VectorMatrixQ<Tq> dSgradu = 0;
//    pde_.jacobianGradientSource(paramL, qI, gradqI, dSgradu);
//
//    MatrixQ<Tq> ndSgradu = 0;
//    for (int i=0; i<D; i++)
//      ndSgradu = nL[i]*dSgradu[i];
//
//    MatrixQ<Tq> dudq = 0;
//    pde_.jacobianMasterState( paramL, qI, dudq );
//
//    ArrayQ<Tq> du = dudq*dq;
//
//    ArrayQ<Tq> tmp = ndSgradu*du;
//
//    //DUAL CONSISTENCY
//    for (int k = 0; k < nPhi_; k++)
//      integrand[k] -= dot(weight_[k],tmp);
//#endif
//  }




}

}

#endif  // INTEGRANDBOUNDARYFUNCTOR_FLUX_MITSTATE_VMSDBR2_H
