// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDBOUNDARYTRACE_VMSD_ADJOINTLO_H
#define SETFIELDBOUNDARYTRACE_VMSD_ADJOINTLO_H

// jacobian boundary-trace integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/Element/ElementalMassMatrix.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  DGBR2 boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class IntegrandBoundaryTrace>
class SetFieldBoundaryTrace_VMSD_AdjointLO_impl :
    public GroupIntegralBoundaryTraceType< SetFieldBoundaryTrace_VMSD_AdjointLO_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,1,ArrayQ> RowArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  SetFieldBoundaryTrace_VMSD_AdjointLO_impl(const IntegrandBoundaryTrace& fcn,
                                            const FieldDataInvMassMatrix_Cell& mmfld) :
    fcn_(fcn), mmfld_(mmfld), comm_rank_(0) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                   Field<PhysDim,TopoDim,ArrayQ>, // qp
                                                   Field<PhysDim,TopoDim,ArrayQ>>::type // w
                                                   & flds,
              const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,VectorArrayQ>, // r
                                                   Field<PhysDim,TopoDim,VectorArrayQ>  // s
                                                                           >::type& fldLift) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,           // q
                                                  Field<PhysDim,TopoDim,ArrayQ>,           // qp
                                                  Field<PhysDim,TopoDim,ArrayQ>           // w
                                     >::type::template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,VectorArrayQ>, // r
                                                  Field<PhysDim,TopoDim,VectorArrayQ>  // s
                                     >::type::template FieldCellGroupType<TopologyL>& fldLiftCellL,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<Surreal> ElementRFieldSurrealClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<>        ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<2>(fldsCellL);

    const RFieldCellGroupTypeL& rbfldCellL = get<0>(fldLiftCellL);
          RFieldCellGroupTypeL& sbfldCellL = const_cast<RFieldCellGroupTypeL&>(get<1>(fldLiftCellL));

    // element field variables
    ElementXFieldClassL        xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldSurrealClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldSurrealClassL rbfldElemL( rbfldCellL.basis() );
    ElementQFieldClassL        wfldElemL( wfldCellL.basis() );
    ElementRFieldClassL        sbfldElemL( sbfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();
    int nDOFpL = qpfldElemL.nDOF();

//    const DLA::MatrixDView_Array<Real>& mmfldCellL = mmfld_.getCellGroupGlobal(cellGroupGlobalL);
    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL,-1);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal> integralPDE(quadratureorder, nDOFL, nDOFpL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralLO(quadratureorder, nDOFL);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
    std::vector<ArrayQSurreal> rsdPDEpElemL( nDOFpL );
    std::vector<VectorArrayQSurreal> rsdLOElemL( nDOFL );

    // temporary storage for matrix-vector multiplication
    DLA::VectorD<VectorArrayQ> tmpL( nDOFL );

    // element jacobians
    MatrixLOElemClass mtxTPDEElemL_rL(nDOFL, nDOFL);

    DLA::MatrixD< DLA::MatrixS<D,D,Real> > mtxTLOElemL_rL(nDOFL, nDOFL);

    // computes the elemental mass matrix
    ElementalMassMatrix<TopoDim, TopologyL> massMtx(get<-1>(xfldElemL), qfldElemL);

    // Provide a vector of the primal adjoint
    DLA::VectorD<RowArrayQ> wL( nDOFL );

    // Provide a vector view of the adjoint lifting operator DOFs
    DLA::VectorDView<VectorArrayQ> sL( sbfldElemL.vectorViewDOF() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxTPDEElemL_rL = 0;
      mtxTLOElemL_rL  = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      qpfldCellL.getElement( qpfldElemL, elemL );
      wfldCellL.getElement( wfldElemL, elemL );

      //trace numbering
      rbfldCellL.getElement( rbfldElemL, elem );
      sbfldCellL.getElement( sbfldElemL, elem );
      xfldTrace.getElement( xfldElemTrace, elem );

      // only need to perform the integral on processors that possess the element
//      if ( qfldElemL.rank() != comm_rank_ ) continue;

      // Compute the elemental mass matrix and then add the lifting operator term from the BC
      // Mass matrix is symmetric, so no need for transpose
      massMtx(get<-1>(xfldElemL), mtxTLOElemL_rL);

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(D*nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rbfldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }

        // line integration for canonical element
//
//        for (int n = 0; n < nDOFL; n++)
//          rsdLOElemL[n] = 0;
//
//        integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL,
//                                      xfldElemL, qfldElemL, rbfldElemL),
//                    get<-1>(xfldElemTrace),
//                    rsdLOElemL.data(), nDOFL );

        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        for (int n = 0; n < nDOFpL; n++)
          rsdPDEpElemL[n] = 0;

        integralPDE( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                                        xfldElemL, qfldElemL, qpfldElemL, rbfldElemL),
                     get<-1>(xfldElemTrace),
                     rsdPDEElemL.data(), nDOFL,
                     rsdPDEpElemL.data(), nDOFpL );

        // accumulate derivatives into element jacobian

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d1 = 0; d1 < D; d1++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = nEqn*(D*j + d1) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(rbfldElemL.DOF(j)[d1],n).deriv(slot - nchunk) = 0;

                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    // Fill in as transposed (j,i)[d1](n,m)
                    DLA::index(mtxTPDEElemL_rL(j,i)[d1],n,m) += DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);

                    // Fill in as transposed (j,i)(d1,d0)(n,m)
//                    for (int d0 = 0; d0 < D; d0++)
//                      DLA::index(mtxTLOElemL_rL(j,i)(d1,d0),n,m) += DLA::index(rsdLOElemL[i][d0],m).deriv(slot - nchunk);
                  }
              }
            } //n loop
          } //d1 loop
        } //j loop

      }   // nchunk

      // Copy over adjoints for matrix-vector multiplication
      for (int i = 0; i < nDOFL; i++) wL[i] = wfldElemL.DOF(i);

      tmpL = mtxTPDEElemL_rL*wL;

      tmpL = -DLA::InverseLU::Solve(mtxTLOElemL_rL, tmpL);

      sL += tmpL;

      // Save off the second component of lifting operator adjoint
      sbfldCellL.setElement( sbfldElemL, elem );
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  mutable int comm_rank_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace>
SetFieldBoundaryTrace_VMSD_AdjointLO_impl<Surreal, IntegrandBoundaryTrace>
SetFieldBoundaryTrace_VMSD_AdjointLO( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                      const FieldDataInvMassMatrix_Cell& mmfld )
{
  return {fcn.cast(), mmfld};
}


}

#endif  // SETFIELDBOUNDARYTRACE_VMSD_ADJOINTLO_H
