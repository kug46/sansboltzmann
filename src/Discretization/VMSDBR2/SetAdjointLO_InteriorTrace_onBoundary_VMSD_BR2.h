// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SET_ADJOINTLO_INTERIORTRACE_ONBOUNDARY_VMSD_BR2_H
#define SET_ADJOINTLO_INTERIORTRACE_ONBOUNDARY_VMSD_BR2_H

// Computes lifting operators on specified interior trace groups

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "JacobianInteriorTrace_VMSD_BR2_Element.h"

#include "IntegrandTrace_VMSD_BR2.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG BR2 interior-trace integral
//

template<class IntegrandInteriorTrace>
class SetAdjointLO_InteriorTrace_onBoundary_VMSD_BR2_impl :
    public GroupIntegralBoundaryTraceType< SetAdjointLO_InteriorTrace_onBoundary_VMSD_BR2_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PDE PDE;
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,1,ArrayQ> RowArrayQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef JacobianElemCell_VMSDBR2<PhysDim,MatrixQ> JacobianElemCellType;

  // Save off the interor trace integrand and inverse mass matrix field
  SetAdjointLO_InteriorTrace_onBoundary_VMSD_BR2_impl( const IntegrandInteriorTrace& fcn,
                                                       const std::map<int,std::vector<int>>& cellBTraceGroups,
                                     const int& cellgroup, const int& cellelem,
                                     const FieldDataInvMassMatrix_Cell& mmfld,
                                     JacobianElemCellType& mtxElem ) :
     fcn_(fcn), cellBTraceGroups_(cellBTraceGroups),
     cellgroup_(cellgroup), cellelem_(cellelem), mmfld_(mmfld), mtxElem_(mtxElem)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellBTraceGroups_.begin(); it != cellBTraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nBoundaryGroups() const { return traceGroupIndices_.size(); }
  std::size_t boundaryGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // Nothing to check
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type& flds ) const
  {
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                           Field<PhysDim,TopoDim,ArrayQ>, // q'
                                           FieldLift<PhysDim, TopoDim, VectorArrayQ>, //r
                                           Field<PhysDim,TopoDim,ArrayQ>, //w
                                           Field<PhysDim,TopoDim,ArrayQ>, //w'
                                           FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type:: //s
                     template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;


    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<2>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<3>(fldsCellL);
    const QFieldCellGroupTypeL& wpfldCellL = get<4>(fldsCellL);
    RFieldCellGroupTypeL& sfldCellL = const_cast<RFieldCellGroupTypeL&>(get<5>(fldsCellL));

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );
    ElementQFieldClassL wfldElemL( wfldCellL.basis() );
    ElementQFieldClassL wpfldElemL( wpfldCellL.basis() );
    ElementRFieldClassL sfldElemL( sfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

//    typedef JacobianElemTrace_VMSD_PDE<PhysDim, MatrixQ> JacobianElemInteriorTraceType;
    typedef JacobianElemTrace_Transpose_VMSD_PDE<PhysDim, MatrixQ> JacobianElemInteriorTraceTransposeType;

    // number of integrals evaluated per element
    const int nIntegrandPL = qpfldElemL.nDOF();

    const int nDOFL = qfldElemL.nDOF();
    const int nDOFpL = qpfldElemL.nDOF();

    const DLA::MatrixDView_Array<Real>& mmfldCellL = mmfld_.getCellGroupGlobal(cellGroupGlobalL);

    // trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceTransposeType > integral(quadratureorder);

    // element integrand/residuals
    DLA::VectorD< VectorArrayQ > rsdElemL( nIntegrandPL );

    DLA::VectorDView<VectorArrayQ> rL( rfldElemL.vectorViewDOF() );

    JacobianElemTraceSizeVMSD sizeL(nDOFL, nDOFpL);

    // temporary storage for matrix-vector multiplication
    DLA::VectorD<VectorArrayQ> tmpL( nDOFpL );

    JacobianElemInteriorTraceTransposeType mtxElemL(sizeL);

    // Provide a vector of the primal adjoint DOFs
    DLA::VectorD<RowArrayQ> wL( nDOFL );
    DLA::VectorD<RowArrayQ> wpL( nDOFpL );

    // Provide a vector view of the lifting operator DOFs
    DLA::VectorDView<VectorArrayQ> sL( sfldElemL.vectorViewDOF() );

    const std::vector<int>& traceElemList = cellBTraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );

      xfldTrace.getElement( xfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        mtxElemL = 0;

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        qpfldCellL.getElement( qpfldElemL, elemL );
        rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );
        wfldCellL.getElement( wfldElemL, elemL );
        wpfldCellL.getElement( wpfldElemL, elemL );
        sfldCellL.getElement( sfldElemL, elemL, canonicalTraceL.trace );

        integral( fcn_.integrand(xfldElemTrace, canonicalTraceL, +1, xfldElemL, qfldElemL, qpfldElemL, rfldElemL),
                                      xfldElemTrace, mtxElemL );

        // Copy over adjoints for matrix-vector multiplication
        for (int i = 0; i < nDOFL; i++) wL[i] = wfldElemL.DOF(i);
        for (int i = 0; i < nDOFpL; i++) wpL[i] = wpfldElemL.DOF(i);

        // Add jacobian contributions from the cell integral
        if (fcn_.needsSolutionGradientforSource())
        {
          mtxElemL.PDET_r += Transpose(mtxElem_.PDE_LO);
          mtxElemL.PDEpT_r += Transpose(mtxElem_.PDEp_LO);
        }

        tmpL = mtxElemL.PDET_r*wL + mtxElemL.PDEpT_r*wpL;

        // Compute the adjoint lifting operator
        sL = -mmfldCellL[elemL]*tmpL;

        // Set the adjoint lifting operators
        sfldCellL.setElement( sfldElemL, elemL, canonicalTraceL.trace );
      }
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;

  const std::map<int,std::vector<int>>& cellBTraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;

  const FieldDataInvMassMatrix_Cell& mmfld_;
  JacobianElemCellType& mtxElem_; //jacobian of PDE residual wrt q for the central cell

  std::vector<int> traceGroupIndices_;
};


// Factory function

template<class IntegrandInteriorTrace, class PhysDim, class MatrixQ>
SetAdjointLO_InteriorTrace_onBoundary_VMSD_BR2_impl<IntegrandInteriorTrace>
SetAdjointLO_InteriorTrace_onBoundary_VMSD_BR2( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                                const std::map<int,std::vector<int>>& cellBTraceGroups,
                                                const int& cellgroup, const int& cellelem,
                                             const FieldDataInvMassMatrix_Cell& mmfld,
                                             JacobianElemCell_VMSDBR2<PhysDim,MatrixQ>& mtxElem)
{
  return SetAdjointLO_InteriorTrace_onBoundary_VMSD_BR2_impl<IntegrandInteriorTrace>
  (fcn.cast(),  cellBTraceGroups, cellgroup, cellelem, mmfld, mtxElem);
}

}

#endif  // SET_LO_INTERIORTRACE_VMSD_BR2_H
