// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_VMSD_BR2_H
#define INTEGRANDCELL_VMSD_BR2_H

// cell integrand operator: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/ElementLift.h"

#include "Discretization/Integrand_Type.h"
#include "JacobianCell_VMSD_BR2_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template <class PDE_>
class IntegrandCell_VMSD_BR2 : public IntegrandCellType< IntegrandCell_VMSD_BR2<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays
  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_VMSD_BR2( const PDE& pde, const std::vector<int>& CellGroups )
    : pde_(pde), cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  //weight functions are coarse scale basis functions
  template<class T, class Tr, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<VectorArrayQ<Tr>, TopoDim, Topology> ElementRFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef JacobianElemCell_VMSDBR2<PhysDim,MatrixQ<Real>> JacobianElemCellType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted(  const PDE& pde,
                    const ElementParam& paramfldElem,
                    const ElementQFieldType& qfldElem,
                    const ElementQFieldType& qpfldElem,
                    const ElementRFieldType& rfldElem,
                    const bool liftedOnly = false) :
                      pde_(pde),
                      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                      qfldElem_(qfldElem),
                      qpfldElem_(qpfldElem),
                      rfldElem_(rfldElem),
                      paramfldElem_( paramfldElem ),
                      nDOF_( qfldElem_.nDOF() ),
                      nDOFp_( qpfldElem_.nDOF() ),
                      phi_( new Real[nDOF_] ),
                      phip_( new Real[nDOFp_] ),
                      gradphi_( new VectorX[nDOF_] ),
                      gradphip_( new VectorX[nDOFp_] ),
                      liftedOnly_(liftedOnly),
                      basisEqual_((qfldElem_.basis()->category() == qpfldElem_.basis()->category()) &&
                                  (qfldElem_.order() == qpfldElem_.order() )),
                      isVMSD0_(qpfldElem_.order() == 0)
    {}

    BasisWeighted( BasisWeighted&& bw ) :
                      pde_(bw.pde_),
                      xfldElem_(bw.xfldElem_),
                      qfldElem_(bw.qfldElem_),
                      qpfldElem_(bw.qpfldElem_),
                      rfldElem_(bw.rfldElem_ ),
                      paramfldElem_( bw.paramfldElem_ ),
                      nDOF_( bw.nDOF_ ),
                      nDOFp_( bw.nDOFp_ ),
                      phi_( bw.phi_ ),
                      phip_( bw.phip_ ),
                      gradphi_( bw.gradphi_ ),
                      gradphip_( bw.gradphip_ ),
                      liftedOnly_(bw.liftedOnly_),
                      basisEqual_(bw.basisEqual_),
                      isVMSD0_(bw.isVMSD0_)
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
      bw.phip_ = nullptr; bw.gradphip_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] phip_;
      delete [] gradphi_;
      delete [] gradphip_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }
    int nDOFp() const { return nDOFp_; }

    // cell element residual integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                                                ArrayQ<Ti> integrandFine[], int neqnFine) const;

    template<class Ti>
    void operator()( const Real dJ, const QuadPointType& sRef,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemCoarse, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemFine ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxPDEElem ) const;

  protected:
    template<class Tq, class Tqp, class Tg, class Tgp, class Ti>
    void weightedIntegrand( const ParamT& param,
                            const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                            const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                            ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                            ArrayQ<Ti> integrandFine[], int neqnFine ) const;

    template<class Tq, class Tqp, class Tg, class Tgp, class Ti>
    void sourceIntegrand( const ParamT& param,
                          const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                          const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                          ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                          ArrayQ<Ti> integrandFine[], int neqnFine ) const;

    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& qpfldElem_;
    const ElementRFieldType& rfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nDOFp_;
    mutable Real *phi_;
    mutable Real *phip_;
    mutable VectorX *gradphi_;
    mutable VectorX *gradphip_;

    const bool liftedOnly_;
    const bool basisEqual_;
    const bool isVMSD0_;
  };

  template<class T, class Tr, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, Tr, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qpfldElem,
            const Element<VectorArrayQ<Tr>, TopoDim, Topology>& rfldElem,
            const bool liftedOnly = false) const
  {
    return BasisWeighted<T, Tr, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, qpfldElem, rfldElem, liftedOnly);
  }



  template<class T, class TopoDim, class Topology>
  class BasisWeighted_LO
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementLift<VectorArrayQ<T>, TopoDim, Topology> ElementRFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;

    typedef DLA::VectorS<nTrace, VectorArrayQ<T>> IntegrandType;

    BasisWeighted_LO( const PDE& pde,
                      const ElementXFieldType& xfldElem,
                      const ElementRFieldType& rfldElems ) :
                        pde_(pde),
                        xfldElem_(xfldElem),
                        rfldElems_(rfldElems),
                        nDOF_( rfldElems_.nDOFElem() ),
                        phi_( new Real[nDOF_] )
    {}

    BasisWeighted_LO( BasisWeighted_LO&& bw ) :
                        pde_(bw.pde_),
                        xfldElem_(bw.xfldElem_),
                        rfldElems_(bw.rfldElems_),
                        nDOF_( bw.nDOF_ ),
                        phi_( bw.phi_ )
    {
      bw.phi_ = nullptr;
    }

    ~BasisWeighted_LO()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return ( pde_.hasFluxViscous() ||
             ( pde_.hasSource() && pde_.needsSolutionGradientforSource()) );
    }


    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    void operator()( const QuadPointType& s, IntegrandType integrandLO[], int neqn ) const;
  private:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementRFieldType& rfldElems_;  // lifting operators, one per trace

    const int nDOF_;
    mutable Real *phi_;
  };


  template<class T, class TopoDim, class Topology>
  BasisWeighted_LO<T,TopoDim,Topology>
  integrand_LO(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
               const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& rfldElems) const
  {
    return {pde_, xfldElem, rfldElems};
  }


  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementLift<VectorArrayQ<T>, TopoDim, Topology> ElementRFieldType;
    typedef Element<Real, TopoDim, Topology> ElementEFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    FieldWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const ElementQFieldType& qfldElem,
                   const ElementQFieldType& qpfldElem,
                   const ElementRFieldType& rfldElems,
                   const ElementQFieldType& wfldElem,
                   const ElementQFieldType& wpfldElems,
                   const ElementRFieldType& sfldElems,
                   const ElementEFieldType& efldElem) :
                     pde_(pde),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem),
                     qpfldElem_(qpfldElem),
                     rfldElems_(rfldElems),
                     wfldElem_(wfldElem),
                     wpfldElem_(wpfldElems),
                     sfldElems_(sfldElems),
                     efldElem_(efldElem),
                     paramfldElem_(paramfldElem),
                     nPhi_( efldElem_.nDOF() ),
                     phi_( new Real[nPhi_] ),
                     gradphi_( new VectorX[nPhi_] ),
                     weight_( new ArrayQ<T>[nPhi_] ),
                     gradWeight_( new VectorArrayQ<T>[nPhi_] )
    {}

    FieldWeighted( FieldWeighted&& fw ) :
                     pde_(fw.pde_),
                     xfldElem_(fw.xfldElem_),
                     qfldElem_(fw.qfldElem_),
                     qpfldElem_(fw.qpfldElem_),
                     rfldElems_(fw.rfldElems_),
                     wfldElem_(fw.wfldElem_),
                     wpfldElem_(fw.wpfldElem_),
                     sfldElems_(fw.sfldElems_),
                     efldElem_(fw.efldElem_),
                     paramfldElem_(fw.paramfldElem_),
                     nPhi_( fw.nPhi_ ),
                     phi_( fw.phi_ ),
                     gradphi_( fw.gradphi_ ),
                     weight_( fw.weight_ ),
                     gradWeight_( fw.gradWeight_ )
    {
      fw.phi_ = nullptr; fw.gradphi_ = nullptr;
      fw.weight_ = nullptr; fw.gradWeight_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
      delete [] weight_;
      delete [] gradWeight_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }
    int nDOFp() const { return qpfldElem_.nDOF(); }

    // cell element integrand
    template <class Ti>
    void operator()( const QuadPointType& sRef, Ti integrand[], int nphi ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& qpfldElem_;
    const ElementRFieldType& rfldElems_;
    const ElementQFieldType& wfldElem_;
    const ElementQFieldType& wpfldElem_;
    const ElementRFieldType& sfldElems_;
    const ElementEFieldType& efldElem_;
    const ElementParam& paramfldElem_;

    const int nPhi_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable ArrayQ<T> *weight_;
    mutable VectorArrayQ<T> *gradWeight_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qpfldElem,
            const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& rfldElems,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& wpfldElem,
            const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& sfldElems,
            const Element<Real, TopoDim, Topology>& efldElem) const
  {
    return FieldWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, qpfldElem, rfldElems,
                                                                                 wfldElem, wpfldElem, sfldElems, efldElem);
  }



protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};

template <class PDE>
template <class T, class Tr, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_VMSD_BR2<PDE>::BasisWeighted<T,Tr,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_VMSD_BR2<PDE>::FieldWeighted<T, TopoDim, Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}


template <class PDE>
template <class T, class Tr, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_VMSD_BR2<PDE>::BasisWeighted<T,Tr,TopoDim,Topology, ElementParam>::
operator()(const QuadPointType& sRef, ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                                      ArrayQ<Ti> integrandFine[], int neqnFine) const
{
  SANS_ASSERT(neqnCoarse == nDOF_);
  SANS_ASSERT(neqnFine == nDOFp_);

  //clear integrands
  for (int k = 0; k < neqnCoarse; k++)
    integrandCoarse[k] = 0;

  for (int k = 0; k < neqnFine; k++)
    integrandFine[k] = 0;

  ParamT param;                  // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                   // solution
  ArrayQ<T> qp;                  // solution
  VectorArrayQ<T> gradq;         // gradient
  VectorArrayQ<T> gradqp;        // gradient
  VectorArrayQ<T> gradqpLifted;  // gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );


  if (basisEqual_)
  {
    for (int k=0; k< nDOF_; k++)
    {
      phip_[k] = phi_[k];
      gradphip_[k] = gradphi_[k];
    }

  }
  else
  {
    qpfldElem_.evalBasis( sRef, phip_, nDOFp_ );
    xfldElem_.evalBasisGradient( sRef, qpfldElem_, gradphip_, nDOFp_ );
  }

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qpfldElem_.evalFromBasis( phip_, nDOFp_, qp );

  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    qpfldElem_.evalFromBasis( gradphip_, nDOFp_, gradqp );
  }

//  q += qp;
//  gradq += gradqp;

  // compute the residual
  weightedIntegrand( param, q, qp, gradq, gradqp,
                     integrandCoarse, neqnCoarse,
                     integrandFine, neqnFine);


  if (pde_.hasSource())
  {
    if (pde_.needsSolutionGradientforSource())
    {
      //augment gradq with r for asymptotic dual consistency..
      VectorArrayQ<Tr> r;
      rfldElem_.evalFromBasis(phip_, nDOFp_, r );
      gradqpLifted = gradqp + r;

      sourceIntegrand(param, q, qp, gradq, gradqpLifted,
                      integrandCoarse, neqnCoarse,
                      integrandFine, neqnFine);
    }
    else
    {
      sourceIntegrand(param, q, qp, gradq, gradqp,
                      integrandCoarse, neqnCoarse,
                      integrandFine, neqnFine);

    }
  }

}

template <class PDE>
template <class T, class Tr, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_VMSD_BR2<PDE>::BasisWeighted<T,Tr,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef,
           DLA::VectorD<ArrayQ<Ti>>& rsdPDEElemCoarse,
           DLA::VectorD<ArrayQ<Ti>>& rsdPDEElemFine) const
{
  SANS_ASSERT(rsdPDEElemCoarse.m() == nDOF_);
  SANS_ASSERT(rsdPDEElemFine.m() == nDOFp_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                 // solution
  ArrayQ<T> qp;                 // solution
  VectorArrayQ<T> gradq;       // gradient
  VectorArrayQ<T> gradqp;       // gradient
  VectorArrayQ<T> gradqpLifted;       // gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  if (basisEqual_)
  {
    for (int k=0; k< nDOF_; k++)
    {
      phip_[k] = phi_[k];
      gradphip_[k] = gradphi_[k];
    }

  }
  else
  {
    qpfldElem_.evalBasis( sRef, phip_, nDOFp_ );
    xfldElem_.evalBasisGradient( sRef, qpfldElem_, gradphip_, nDOFp_ );
  }

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qpfldElem_.evalFromBasis( phip_, nDOFp_, qp );
  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    qpfldElem_.evalFromBasis( gradphip_, nDOFp_, gradqp );
  }


//  q += qp;
//  gradq += gradqp;

  // compute the residual
  DLA::VectorD<ArrayQ<Ti>> integrandCoarse(nDOF_);
  DLA::VectorD<ArrayQ<Ti>> integrandFine(nDOFp_);

  //initialize integrands
  for (int k = 0; k < nDOF_; k++) integrandCoarse[k] = 0;
  for (int k = 0; k < nDOFp_; k++) integrandFine[k] = 0;

  weightedIntegrand( param, q, qp, gradq, gradqp,
                     integrandCoarse.data(), integrandCoarse.size(),
                     integrandFine.data(), integrandFine.size());

  if (pde_.hasSource())
  {
    if (pde_.needsSolutionGradientforSource())
    {
      //augment gradq with r for asymptotic dual consistency..
      VectorArrayQ<Tr> r;            // lifting operator stabilizing gradient
      rfldElem_.evalFromBasis( phip_, nDOFp_, r );
      gradqpLifted = gradqp + r;

      sourceIntegrand(param, q, qp, gradq, gradqpLifted,
                      integrandCoarse.data(), integrandCoarse.size(),
                      integrandFine.data(), integrandFine.size());
    }
    else
    {
      sourceIntegrand(param, q, qp, gradq, gradqp,
                      integrandCoarse.data(), integrandCoarse.size(),
                      integrandFine.data(), integrandFine.size());

    }
  }

  for (int k = 0; k < nDOF_; k++)
    rsdPDEElemCoarse(k) += dJ*integrandCoarse(k);

  for (int k = 0; k < nDOFp_; k++)
    rsdPDEElemFine(k) += dJ*integrandFine(k);
}

template <class PDE>
template <class T, class Tr, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_VMSD_BR2<PDE>::BasisWeighted<T,Tr,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxElem.nDOF == nDOF_);
  SANS_ASSERT(mtxElem.nDOFp == nDOFp_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;                  // solution
  ArrayQ<Real> qp;                 // solution
  VectorArrayQ<Real> gradq;        // gradient
  VectorArrayQ<Real> gradqp;       // gradient
  VectorArrayQ<Real> gradqpLifted; // gradient

  ArrayQ<SurrealClass> qSurreal = 0;               // solution
  ArrayQ<SurrealClass> qpSurreal = 0;              // solution
  VectorArrayQ<SurrealClass> gradqSurreal = 0;     // gradient
  VectorArrayQ<SurrealClass> gradqpSurreal = 0;    // gradient

  MatrixQ<Real> PDE_q =0 , PDE_gradq = 0; // temporary storage
  MatrixQ<Real> S_gradq = 0, Sp_gradq = 0; // temporary storage
//  MatrixQ<Real> PDE_qp =0; // temporary storage
  MatrixQ<Real> PDEp_q =0 , PDEp_gradq = 0; // temporary storage
//  MatrixQ<Real> PDEp_qp =0 , PDEp_gradqp = 0; // temporary storage

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  if (basisEqual_)
  {
    for (int k=0; k< nDOF_; k++)
    {
      phip_[k] = phi_[k];
      gradphip_[k] = gradphi_[k];
    }

  }
  else
  {
    qpfldElem_.evalBasis( sRef, phip_, nDOFp_ );
    xfldElem_.evalBasisGradient( sRef, qpfldElem_, gradphip_, nDOFp_ );
  }

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qpfldElem_.evalFromBasis( phip_, nDOFp_, qp );

  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    qpfldElem_.evalFromBasis( gradphip_, nDOFp_, gradqp );
  }

//  q += qp;
//  gradq += gradqp;


  gradqpLifted = 0;       // gradient
  // if necessary for source, augment gradq with lifting operator
  VectorArrayQ<T> r = 0;            // lifting operator stabilizing gradient
  if (pde_.hasSource() && pde_.needsSolutionGradientforSource())
  {
    rfldElem_.evalFromBasis( phip_, nDOFp_, r ); //assume same basis for q, r
    gradqpLifted = gradqp + r;
  }

  qSurreal = q;
  qpSurreal = qp;
  gradqSurreal = gradq;
  gradqpSurreal = gradqp;
  VectorArrayQ<SurrealClass> gradqpliftedSurreal = gradqpLifted;


  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurrealp( nDOFp_ );

  DLA::VectorD<ArrayQ<SurrealClass>> sourceSurreal( nDOF_ );
  DLA::VectorD<ArrayQ<SurrealClass>> sourceSurrealp( nDOFp_ );

  // loop over derivative chunks - q
  for (int nchunk = 0; nchunk < PDE::N  && !liftedOnly_; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;
    integrandSurrealp = 0;

    weightedIntegrand( param, qSurreal, qp, gradq, gradqp,
                       integrandSurreal.data(), integrandSurreal.size(),
                       integrandSurrealp.data(), integrandSurrealp.size());

    if (pde_.hasSource())
    {
      sourceIntegrand( param, qSurreal, qp, gradq, gradqpLifted,
                       integrandSurreal.data(), integrandSurreal.size(),
                       integrandSurrealp.data(), integrandSurrealp.size());
    }

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxElem.PDE_q(i,j) += dJ*phi_[j]*PDE_q;

//        for (int j = 0; j < nDOFp_; j++)
//          mtxElem.PDE_qp(i,j) += dJ*phip_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFp_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDEp_q,m,n) = DLA::index(integrandSurrealp[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxElem.PDEp_q(i,j) += dJ*phi_[j]*PDEp_q;

//        for (int j = 0; j < nDOFp_; j++)
//          mtxElem.PDEp_qp(i,j) += dJ*phip_[j]*PDEp_q;
      }
    }
    slot += PDE::N;
  } // nchunk

  // loop over derivative chunks - qp
  for (int nchunk = 0; nchunk < PDE::N  && !liftedOnly_; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qpSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;
    integrandSurrealp = 0;

    weightedIntegrand( param, q, qpSurreal, gradq, gradqp,
                       integrandSurreal.data(), integrandSurreal.size(),
                       integrandSurrealp.data(), integrandSurrealp.size());

    if (pde_.hasSource())
    {
      sourceIntegrand( param, q, qpSurreal, gradq, gradqpLifted,
                       integrandSurreal.data(), integrandSurreal.size(),
                       integrandSurrealp.data(), integrandSurrealp.size());
    }

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qpSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

//        for (int j = 0; j < nDOF_; j++)
//          mtxElem.PDE_q(i,j) += dJ*phi_[j]*PDE_q;

        for (int j = 0; j < nDOFp_; j++)
          mtxElem.PDE_qp(i,j) += dJ*phip_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFp_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDEp_q,m,n) = DLA::index(integrandSurrealp[i], m).deriv(slot + n - nchunk);

//        for (int j = 0; j < nDOF_; j++)
//          mtxElem.PDEp_q(i,j) += dJ*phi_[j]*PDEp_q;

        for (int j = 0; j < nDOFp_; j++)
          mtxElem.PDEp_qp(i,j) += dJ*phip_[j]*PDEp_q;
      }
    }
    slot += PDE::N;
  } // nchunk

  // qbar part
  if (needsSolutionGradient)
  {
    S_gradq = 0; Sp_gradq = 0;
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
          {
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
            DLA::index(gradqpliftedSurreal[d], n).deriv(slot + n - nchunk) = 1;
          }
        slot += PDE::N;
      }

      integrandSurreal = 0;
      integrandSurrealp = 0;

      if (!liftedOnly_)
        weightedIntegrand( param, q, qp, gradqSurreal, gradqp,
                           integrandSurreal.data(), integrandSurreal.size(),
                           integrandSurrealp.data(), integrandSurrealp.size());

      sourceSurreal = 0;
      sourceSurrealp = 0;
      if (pde_.hasSource())
      {
        sourceIntegrand( param, q, qp, gradqSurreal, gradqpLifted,
                         sourceSurreal.data(), sourceSurreal.size(),
                         sourceSurrealp.data(), sourceSurrealp.size());

      }


      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
          {
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative
            DLA::index(gradqpliftedSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative
          }

          for (int i = 0; i < nDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
              {
                DLA::index(S_gradq,m,n) = DLA::index(sourceSurreal[i], m).deriv(slot + n - nchunk);
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk)
                                              + DLA::index(S_gradq,m,n);
              }

            for (int j = 0; j < nDOF_; j++)
              mtxElem.PDE_q(i,j) += dJ*gradphi_[j][d]*PDE_gradq;

//            for (int j = 0; j < nDOFp_; j++)
//              mtxElem.PDE_qp(i,j) += dJ*gradphip_[j][d]*PDE_gradq;
//
//            for (int j = 0; j < nDOFp_; j++)
//              mtxElem.PDE_LO(i,j)(0,d) += dJ*phip_[j]*S_gradq;
          }

          for (int i = 0; i < nDOFp_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
              {
                DLA::index(Sp_gradq,m,n) = DLA::index(sourceSurrealp[i], m).deriv(slot + n - nchunk);
                DLA::index(PDEp_gradq,m,n) = DLA::index(integrandSurrealp[i], m).deriv(slot + n - nchunk)
                                               + DLA::index(Sp_gradq,m,n);
              }

            for (int j = 0; j < nDOF_; j++)
              mtxElem.PDEp_q(i,j) += dJ*gradphi_[j][d]*PDEp_gradq;

//            for (int j = 0; j < nDOFp_; j++)
//              mtxElem.PDEp_qp(i,j) += dJ*gradphip_[j][d]*PDEp_gradq;
//
//            for (int j = 0; j < nDOFp_; j++)
//              mtxElem.PDEp_LO(i,j)(0,d) += dJ*phip_[j]*Sp_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  }

  // qp part
  if (needsSolutionGradient)
  {
    S_gradq = 0; Sp_gradq = 0;
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
          {
            DLA::index(gradqpSurreal[d], n).deriv(slot + n - nchunk) = 1;
            DLA::index(gradqpliftedSurreal[d], n).deriv(slot + n - nchunk) = 1;
          }
        slot += PDE::N;
      }

      integrandSurreal = 0;
      integrandSurrealp = 0;

      if (!liftedOnly_)
        weightedIntegrand( param, q, qp, gradq, gradqpSurreal,
                           integrandSurreal.data(), integrandSurreal.size(),
                           integrandSurrealp.data(), integrandSurrealp.size());

      sourceSurreal = 0;
      sourceSurrealp = 0;
      if (pde_.hasSource())
      {
        sourceIntegrand( param, q, qp, gradq, gradqpliftedSurreal,
                         sourceSurreal.data(), sourceSurreal.size(),
                         sourceSurrealp.data(), sourceSurrealp.size());

      }


      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
          {
            DLA::index(gradqpSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative
            DLA::index(gradqpliftedSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative
          }

          for (int i = 0; i < nDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
              {
                DLA::index(S_gradq,m,n) = DLA::index(sourceSurreal[i], m).deriv(slot + n - nchunk);
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk)
                                              + DLA::index(S_gradq,m,n);
              }

//            for (int j = 0; j < nDOF_; j++)
//              mtxElem.PDE_q(i,j) += dJ*gradphi_[j][d]*PDE_gradq;

            for (int j = 0; j < nDOFp_; j++)
              mtxElem.PDE_qp(i,j) += dJ*gradphip_[j][d]*PDE_gradq;

            for (int j = 0; j < nDOFp_; j++)
              mtxElem.PDE_LO(i,j)(0,d) += dJ*phip_[j]*S_gradq;
          }

          for (int i = 0; i < nDOFp_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
              {
                DLA::index(Sp_gradq,m,n) = DLA::index(sourceSurrealp[i], m).deriv(slot + n - nchunk);
                DLA::index(PDEp_gradq,m,n) = DLA::index(integrandSurrealp[i], m).deriv(slot + n - nchunk)
                                               + DLA::index(Sp_gradq,m,n);
              }

//            for (int j = 0; j < nDOF_; j++)
//              mtxElem.PDEp_q(i,j) += dJ*gradphi_[j][d]*PDEp_gradq;

            for (int j = 0; j < nDOFp_; j++)
              mtxElem.PDEp_qp(i,j) += dJ*gradphip_[j][d]*PDEp_gradq;

            for (int j = 0; j < nDOFp_; j++)
              mtxElem.PDEp_LO(i,j)(0,d) += dJ*phip_[j]*Sp_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  }

}

// Cell integrand
//
// integrand = - grad(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE>
template <class T, class Tr, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tqp, class Tg, class Tgp, class Ti>
void
IntegrandCell_VMSD_BR2<PDE>::BasisWeighted<T,Tr,TopoDim,Topology, ElementParam>::
weightedIntegrand( const ParamT& param,
                   const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                   const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                   ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                   ArrayQ<Ti> integrandFine[], int neqnFine ) const
{
  typedef typename promote_Surreal<Tq,Tqp>::type TqS;
  typedef typename promote_Surreal<Tg,Tgp>::type TgS;

  ArrayQ<TqS> qq = q + qp;
  VectorArrayQ<TgS> gradqq = gradq + gradqp;

  // Galerkin flux term: -gradient(phi) . F
  // PDE flux F(X, Q), Fv(X, Q, QX)
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, qq, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, qq, gradqq, F );

    for (int k = 0; k < neqnCoarse; k++)
      integrandCoarse[k] -= dot(gradphi_[k],F);

    if (!isVMSD0_)
      for (int k = 0; k < neqnFine; k++)
        integrandFine[k] -= dot(gradphip_[k],F);

  }

  // Galerkin right-hand-side forcing function: -phi RHS
  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < neqnCoarse; k++)
      integrandCoarse[k] -= phi_[k]*forcing;

    for (int k = 0; k < neqnFine; k++)
      integrandFine[k] -= phip_[k]*forcing;
  }
}


template <class PDE>
template <class T, class Tr, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tqp, class Tg, class Tgp, class Ti>
void
IntegrandCell_VMSD_BR2<PDE>::BasisWeighted<T,Tr,TopoDim,Topology, ElementParam>::
sourceIntegrand( const ParamT& param,
                 const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
                 const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Tgp>& gradqp,
                 ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                 ArrayQ<Ti> integrandFine[], int neqnFine ) const
{
//  typedef typename promote_Surreal<Tq,Tqp>::type TqS;
//  typedef typename promote_Surreal<Tg,Tgp>::type TgS;

  // Galerkin source term: +phi S
  ArrayQ<Ti> sourceCoarse = 0;            // PDE source S(X, Q, QX)
  ArrayQ<Ti> sourceFine = 0;            // PDE source S(X, Q, QX)

  pde_.sourceCoarse( param, q, qp, gradq, gradqp, sourceCoarse );
  pde_.sourceFine( param, q, qp, gradq, gradqp, sourceFine );

  for (int k = 0; k < neqnCoarse; k++)
    integrandCoarse[k] += phi_[k]*sourceCoarse;

  for (int k = 0; k < neqnFine; k++)
    integrandFine[k] += phip_[k]*sourceFine;


}


template <class PDE>
template <class T, class TopoDim, class Topology>
void
IntegrandCell_VMSD_BR2<PDE>::BasisWeighted_LO<T,TopoDim,Topology>::operator()(
    const QuadPointType& sRef, IntegrandType integrandLO[], int neqn ) const
{
  SANS_ASSERT(neqn == nDOF_);

  VectorArrayQ<T> rlift;     // Lifting operators

  // basis value, gradient
  rfldElems_.evalBasis( sRef, phi_, nDOF_ );

  // Lifting Operator Integrand
  for (int trace = 0; trace < nTrace; trace++)
  {
    // lifting operators
    rfldElems_[trace].evalFromBasis( phi_, nDOF_, rlift );

    // volume term: phi*rlift
    for (int k = 0; k < neqn; k++)
      integrandLO[k][trace] = phi_[k]*rlift;
  }
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_VMSD_BR2<PDE>::
FieldWeighted<T, TopoDim, Topology, ElementParam>::
operator()( const QuadPointType& sRef, Ti integrand[], int nphi ) const
{
  SANS_ASSERT( nphi == nPhi_ );

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;               // Coarse scale solution
  ArrayQ<T> qp;               // Fine scale solution
  VectorArrayQ<T> gradq;     // Coarse solution gradient
  VectorArrayQ<T> gradqp;     // Fine solution gradient

  ArrayQ<T> w;               // weight
  ArrayQ<T> wp;               // weight
  VectorArrayQ<T> gradw;     // weight gradient
  VectorArrayQ<T> gradwp;     // weight gradient

  const bool needsSolutionGradient =
      ( pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // solution value
  qfldElem_.eval( sRef, q );
  qpfldElem_.eval( sRef, qp );

  // solution gradient, hessian
  if (needsSolutionGradient)
  {
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );
    xfldElem_.evalGradient( sRef, qpfldElem_, gradqp );
  }

  q += qp;
  gradq += gradqp;

  // weight value, gradient
  wfldElem_.eval( sRef, w );
  wpfldElem_.eval( sRef, wp );
  xfldElem_.evalGradient( sRef, wfldElem_, gradw );
  xfldElem_.evalGradient( sRef, wpfldElem_, gradwp );

  // estimate basis, gradient
  efldElem_.evalBasis( sRef, phi_, nPhi_ );
  xfldElem_.evalBasisGradient( sRef, efldElem_, gradphi_, nPhi_ );

  for (int k = 0; k < nPhi_; k++)
    integrand[k] = 0;

  //ASSUMING THAT efldElem_ AND epfldElem_ HAVE THE SAME BASIS!
  // for (int n = 0; n < N; n++)
  for (int k = 0; k < nPhi_; k++)
  {
    //CAN COMBINE WEIGHT FUNCTIONS BECAUSE VOLUME TERMS ARE ALL THE SAME
    // DLA::index(weight_[k],n) = DLA::index(w,n) * phi_[k];
    weight_[k] = (w + wp) * phi_[k];

    // for (int d = 0; d < PhysDim::D; d++)
    //   DLA::index(gradWeight_[k][d],n)  = DLA::index(w,n) * gradphi_[k][d] + phi_[k]*DLA::index(gradw[d],n);
    gradWeight_[k] = gradphi_[k]*(w +wp) + phi_[k]*(gradw + gradwp);
  }

  //LIFTING OPERATOR TERM
  VectorArrayQ<T> tmp1,tmp2;

  VectorArrayQ<T> Rlift = 0;
  int nTrace = Topology::NTrace;
  for (int trace = 0; trace < nTrace; trace++)
  {
    // Lifting Operator
    // eval lifting operators from basis
    rfldElems_[trace].eval( sRef, tmp1 );
    sfldElems_[trace].eval( sRef, tmp2 );

    T tmpdot = dot(tmp1,tmp2);

    for (int k = 0; k < nPhi_; k++)
      integrand[k] += tmpdot*phi_[k];

    Rlift += tmp1;
  }

  // weighting the coarse-scale fluxes:
  // Galerkin flux term: -(phi grad w + w grad phi, F)
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;
    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, q, gradq, F );

    for (int d = 0; d < PhysDim::D; d++)
      for (int k = 0; k < nPhi_; k++)
        integrand[k] -= dot(gradWeight_[k][d],F[d]);
  }

  // Galerkin source term: +w S

  if (pde_.hasSource())
  {
    ArrayQ<Ti> source=0;   // PDE source S(X, D, U, UX), S(X)

    if (pde_.needsSolutionGradientforSource())
    {
      //augment gradq with r for asymptotic dual consistency..
      gradq += Rlift;

      pde_.source( param, q, gradq, source );
    }
    else
      pde_.source( param, q, gradq, source );


    for (int k = 0; k < nPhi_; k++)
      integrand[k] += dot(weight_[k],source);
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < nPhi_; k++)
      integrand[k] -= dot(weight_[k],forcing);
  }

}


}

#endif  // INTEGRANDCELL_VMSD_BR2_H
