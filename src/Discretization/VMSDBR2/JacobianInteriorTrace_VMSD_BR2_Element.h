// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_VMSD_ELEMENT_H
#define JACOBIANINTERIORTRACE_VMSD_ELEMENT_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Discretization/JacobianElementMatrix.h"

namespace SANS
{


template<class PhysDim, class MatrixQ>
struct JacobianElemTrace_VMSD_PDE : JacElemMatrixType< JacobianElemTrace_VMSD_PDE<PhysDim,MatrixQ> >
{
  // PDE Jacobian wrt q
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Lifting Operator (r) wrt q
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;
  typedef DLA::MatrixD<VectorX> MatrixLOElemClass;

  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianElemTrace_VMSD_PDE(const JacobianElemTraceSizeVMSD& size)
   : nDOFL(size.nDOFL),
     nDOFp(size.nDOFp),
     PDE_q(size.nDOFL, size.nDOFL),
     PDE_qp(size.nDOFL, size.nDOFp),
     PDEp_q(size.nDOFp, size.nDOFL),
     PDEp_qp(size.nDOFp, size.nDOFp),
//     PDE_LO(size.nDOFL, size.nDOFL), //don't need these, just for testing
//     PDEp_LO(size.nDOFp, size.nDOFL),
     r_qp(size.nDOFp, size.nDOFp)
  {}

  const int nDOFL;
  const int nDOFp;

  // element PDE jacobian matrices wrt q
  MatrixElemClass PDE_q;
  MatrixElemClass PDE_qp;
  MatrixElemClass PDEp_q;
  MatrixElemClass PDEp_qp;
//  MatrixRElemClass PDE_LO;
//  MatrixRElemClass PDEp_LO;

  // element Lifting Operator (r) jacobian matrices wrt q
  MatrixLOElemClass r_qp;

  inline Real operator=( const Real s )
  {
    PDE_q = s;
    PDE_qp = s;
    PDEp_q = s;
    PDEp_qp = s;
//    PDE_LO = s;
//    PDEp_LO = s;

    // Do NOT set this (r_qp is just passed through)
    //r_qp = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemTrace_VMSD_PDE& operator+=(
      const JacElemMulScalar< JacobianElemTrace_VMSD_PDE >& mul )
  {
    PDE_q += mul.s*mul.mtx.PDE_q;
    PDE_qp += mul.s*mul.mtx.PDE_qp;
    PDEp_q += mul.s*mul.mtx.PDEp_q;
    PDEp_qp += mul.s*mul.mtx.PDEp_qp;
//    PDE_LO += mul.s*mul.mtx.PDE_LO;
//    PDEp_LO += mul.s*mul.mtx.PDEp_LO;

    // Do NOT set this (r_qp is just passed through)
    //r_qp += mul.s*mul.mtx.r_qp;

    return *this;
  }


  // needed for Galerkin weighted integrand
  inline JacobianElemTrace_VMSD_PDE& operator+=(
      const JacobianElemTrace_VMSD_PDE& mtx )
  {
    PDE_q += mtx.PDE_q;
    PDE_qp += mtx.PDE_q;
    PDEp_q += mtx.PDE_q;
    PDEp_qp += mtx.PDE_q;

//    PDE_LO += mtx.PDE_LO;
//    PDEp_LO += mtx.PDEp_LO;

    // Do NOT set this (r_qp is just passed through)
    //r_qp += mtx.r_qp;

    return *this;
  }
};

template<class PhysDim>
struct JacobianElemTrace_VMSD_LO : JacElemMatrixType< JacobianElemTrace_VMSD_LO<PhysDim> >
{
  // Lifting Operator Equation (LO) wrt q
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;
  typedef DLA::MatrixD<VectorX> MatrixLOElemClass;

  JacobianElemTrace_VMSD_LO(const JacobianElemTraceSizeVMSD& size)
   : nDOFL(size.nDOFL), nDOFp(size.nDOFp),
     _qp(size.nDOFp, size.nDOFp)
  {}

  const int nDOFL;
  const int nDOFp;

  // element LO jacobian matrices wrt q
  MatrixLOElemClass _qp;

  inline Real operator=( const Real s )
  {
    _qp = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemTrace_VMSD_LO& operator+=(
      const JacElemMulScalar< JacobianElemTrace_VMSD_LO >& mul )
  {
    _qp += mul.s*mul.mtx._qp;

    return *this;
  }


  // needed for Galerkin weighted integrand
  inline JacobianElemTrace_VMSD_LO& operator+=(
      const JacobianElemTrace_VMSD_LO& mtx )
  {
    _qp += mtx._qp;

    return *this;
  }
};



template<class PhysDim, class MatrixQ>
struct JacobianElemTrace_Transpose_VMSD_PDE : JacElemMatrixType< JacobianElemTrace_Transpose_VMSD_PDE<PhysDim,MatrixQ> >
{
  // PDE Jacobian wrt q
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Lifting Operator (r) wrt q
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;
  typedef DLA::MatrixD<VectorX> MatrixLOElemClass;

  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  typedef DLA::VectorS<PhysDim::D,MatrixQ> VectorMatrixQ;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixTLOElemClass;

  JacobianElemTrace_Transpose_VMSD_PDE(const JacobianElemTraceSizeVMSD& size)
   : nDOFL(size.nDOFL),
     nDOFp(size.nDOFp),
     PDET_r(size.nDOFp, size.nDOFL), // NOT: transposed
     PDEpT_r(size.nDOFp, size.nDOFp) // NOT: transposed
  {}

  const int nDOFL;
  const int nDOFp;

  // element PDE jacobian matrices wrt q
  MatrixTLOElemClass PDET_r;
  MatrixTLOElemClass PDEpT_r;

  inline Real operator=( const Real s )
  {
    PDET_r = s;
    PDEpT_r = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemTrace_Transpose_VMSD_PDE& operator+=(
      const JacElemMulScalar< JacobianElemTrace_Transpose_VMSD_PDE >& mul )
  {
    PDET_r += mul.s*mul.mtx.PDET_r;
    PDEpT_r += mul.s*mul.mtx.PDEpT_r;

    return *this;
  }


  // needed for Galerkin weighted integrand
  inline JacobianElemTrace_Transpose_VMSD_PDE& operator+=(
      const JacobianElemTrace_Transpose_VMSD_PDE& mtx )
  {
    PDET_r += mtx.PDET_r;
    PDEpT_r += mtx.PDEpT_r;

    return *this;
  }
};

}
#endif // JACOBIANINTERIORTRACE_VMSD_ELEMENT_H
