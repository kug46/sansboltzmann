// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRAND_VMSDBR2_FWD_H
#define INTEGRAND_VMSDBR2_FWD_H

namespace SANS
{

//----------------------------------------------------------------------------//
// Tag to indicate VMSD type integrands

class VMSDBR2 {};

//----------------------------------------------------------------------------//
// Cell integrand: VMSD

template <class PDE>
class IntegrandCell_VMSD_BR2;
//----------------------------------------------------------------------------//
// Trace integrand: VMSD

template <class PDE>
class IntegrandTrace_VMSD_BR2;


}

#endif // INTEGRAND_VMSD_FWD_H
