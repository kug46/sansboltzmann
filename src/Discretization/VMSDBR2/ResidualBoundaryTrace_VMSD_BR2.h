// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_VMSD_BR2_H
#define RESIDUALBOUNDARYTRACE_VMSD_BR2_H

// boundary-trace integral residual functions

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/FieldData/FieldDataReal_Cell.h"

#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class IntegrandBoundaryTrace, template<class> class Vector, class TR>
class ResidualBoundaryTrace_VMSD_BR2_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_VMSD_BR2_impl<IntegrandBoundaryTrace, Vector, TR> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<TR> ArrayQR;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_VMSD_BR2_impl( const IntegrandBoundaryTrace& fcn,
                                       Vector<ArrayQR>& rsdPDEGlobal,
                                       Vector<ArrayQR>& rsdPDEpGlobal) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), rsdPDEpGlobal_(rsdPDEpGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
              Field<PhysDim,TopoDim,ArrayQ> >::type& flds,
              const Field<PhysDim, TopoDim, VectorArrayQ>& rbfld) const // qp
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( rsdPDEpGlobal_.m() == qpfld.nDOFpossessed() ||
                 rsdPDEpGlobal_.m() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                  Field<PhysDim,TopoDim,ArrayQ>  >::type:: // qp
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL>& rbfldCellL,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
    //const RFieldCellGroupTypeL& rfldCellL = get<2>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rbfldElemL( rbfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    int nDOF = qfldElemL.nDOF();
    int nDOFp = qpfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nDOF, -1 );
    std::vector<int> mapDOFLocalL( nDOF, -1 );
    std::vector<int> maprsdL( nDOF, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = rsdPDEGlobal_.m();
    const int nDOFppossessed = rsdPDEpGlobal_.m();

    std::vector<int> mapDOFGlobalpL( nDOFp, -1 );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQR, ArrayQR> integral(quadratureorder, nDOF, nDOFp);

    // element integrand/residuals
    std::vector<ArrayQR> rsdPDEElemL( nDOF );
    std::vector<ArrayQR> rsdPDEpElemL( nDOFp );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOF );
      qpfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalpL.data(), nDOFp );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOF; n++)
        if (mapDOFGlobalL[n] < nDOFpossessed)
        {
          maprsdL[nDOFLocal] = n;
          mapDOFLocalL[nDOFLocal] = mapDOFGlobalL[n];
          nDOFLocal++;
        }

      // no residuals are possessed by this processor
      if (nDOFLocal == 0 && mapDOFGlobalpL[0] >= nDOFppossessed ) continue;

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      qpfldCellL.getElement( qpfldElemL, elemL );

      //trace numbering
      rbfldCellL.getElement( rbfldElemL, elem );
      xfldTrace.getElement( xfldElemTrace, elem );

      for (int n = 0; n < nDOF; n++)
        rsdPDEElemL[n] = 0;

      for (int n = 0; n < nDOFp; n++)
        rsdPDEpElemL[n] = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL, qpfldElemL, rbfldElemL),
                xfldElemTrace,
                rsdPDEElemL.data(), nDOF,
                rsdPDEpElemL.data(), nDOFp );

      for (int n = 0; n < nDOFLocal; n++)
        rsdPDEGlobal_[ mapDOFLocalL[n] ] += rsdPDEElemL[ maprsdL[n] ];


      // HACK TO SET BUBBLE RESIDUALS TO ZERO; REMOVES OVERLAP IN BASIS
      const int nBubble = qfldCellL.associativity( elem ).nBubble();
      if (nBubble > 0 &&  (qpfldElemL.order() > PhysDim::D) )
      {
        SANS_ASSERT( qpfldElemL.basis()->category() == BasisFunctionCategory_Lagrange );
        for (int ind=(nDOFp - nBubble); ind<nDOFp; ind++) //CELL DOFS WILL BE LAST?
        {
          rsdPDEpElemL[ind] = 0;
        }
      }

      int nGlobal;
      for (int n = 0; n < nDOFp; n++)
      {
        nGlobal = mapDOFGlobalpL[n];
        rsdPDEpGlobal_[nGlobal] += rsdPDEpElemL[n];
      }



    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  Vector<ArrayQR>& rsdPDEGlobal_;
  Vector<ArrayQR>& rsdPDEpGlobal_;
};

// Factory function

template<class IntegrandBoundaryTrace, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_VMSD_BR2_impl<IntegrandBoundaryTrace, Vector, typename Scalar<ArrayQ>::type>
ResidualBoundaryTrace_VMSD_BR2( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                Vector<ArrayQ>& rsdPDEGlobal,
                                Vector<ArrayQ>& rsdPDEpGlobal)
{
  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same<ArrayQ, typename IntegrandBoundaryTrace::template ArrayQ<T> >::value, "These should be the same.");
  return ResidualBoundaryTrace_VMSD_BR2_impl<IntegrandBoundaryTrace, Vector, T>(fcn.cast(), rsdPDEGlobal, rsdPDEpGlobal);
}


}

#endif  // RESIDUALBOUNDARYTRACE_VMSD_BR2_H
