// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_VMSD_BR2_H
#define JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_VMSD_BR2_H

// boundary-trace integral residual functions


//#include "Discretization/DG/JacobianFunctionalBoundaryTrace_FieldTrace_DGBR2.h"
#include "Discretization/VMSDBR2/JacobianFunctionalBoundaryTrace_VMSD_BR2.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups_BoundaryCell.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, template<class> class Vector, class MatrixJ>
class JacobianFunctionalBoundaryTrace_Dispatch_VMSD_BR2_impl
{
public:
  JacobianFunctionalBoundaryTrace_Dispatch_VMSD_BR2_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld,
      const int* quadratureorder, int ngroup,
      Vector< MatrixJ >& func_q,
      Vector< MatrixJ >& func_qp )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), qpfld_(qpfld), rbfld_(rbfld), sbfld_(sbfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      func_q_(func_q), func_qp_(func_qp)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_BoundaryCell<TopoDim>::integrate(
        JacobianFunctionalBoundaryTrace_VMSD_BR2<Surreal>( fcnOutput_, fcn.cast(), func_q_, func_qp_ ),
                            xfld_, (qfld_, qpfld_), (rbfld_, sbfld_), quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector< MatrixJ >& func_q_;
  Vector< MatrixJ >& func_qp_;
};

// Factory function

template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_VMSD_BR2_impl<Surreal, FunctionalIntegrand, XFieldType, PhysDim, TopoDim,
                                                    ArrayQ, VectorArrayQ, Vector, MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_VMSD_BR2(const FunctionalIntegrand& fcnOutput,
                                               const XFieldType& xfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                               const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
                                               const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& sbfld,
                                               const int* quadratureorder, int ngroup,
                                               Vector< MatrixJ >& func_q,
                                               Vector< MatrixJ >& func_qp )
{
  return JacobianFunctionalBoundaryTrace_Dispatch_VMSD_BR2_impl<Surreal, FunctionalIntegrand, XFieldType, PhysDim, TopoDim,
                                                             ArrayQ, VectorArrayQ, Vector, MatrixJ>(
      fcnOutput, xfld, qfld, qpfld, rbfld, sbfld, quadratureorder, ngroup, func_q, func_qp);
}


}

#endif //JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_DGBR2_H
