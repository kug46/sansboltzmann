// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_VMSD_BR2_H
#define JACOBIANBOUNDARYTRACE_VMSD_BR2_H

// jacobian boundary-trace integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementalMassMatrix.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/FieldData/FieldDataReal_Cell.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_VMSD_BR2_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_VMSD_BR2_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianBoundaryTrace_VMSD_BR2_impl(const IntegrandBoundaryTrace& fcn,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                      const FieldDataInvMassMatrix_Cell& mmfld,
                                      FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDE_qp,
                                      FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDEp_q) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q),
    mmfld_(mmfld),
    boundJacPDE_qp_(boundJacPDE_qp),
    boundJacPDEp_q_(boundJacPDEp_q)  {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check(  const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                    Field<PhysDim,TopoDim,ArrayQ> >::type& flds,
               const Field<PhysDim, TopoDim, VectorArrayQ>& rbfld) //qp
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                  Field<PhysDim,TopoDim,ArrayQ> >::type:: //qp
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL>& rbfldCellL,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassSurrealL;
    typedef typename RFieldCellGroupTypeL::template ElementType<Surreal> ElementRFieldClassSurrealL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
    //const RFieldCellGroupTypeL& rfldCellL = get<2>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rbfldElemL( rbfldCellL.basis() );
    ElementQFieldClassSurrealL qfldElemSurrealL( qfldCellL.basis() );
    ElementQFieldClassSurrealL qpfldElemSurrealL( qpfldCellL.basis() );
    ElementRFieldClassSurrealL rbfldElemSurrealL( rbfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    const int D = PhysDim::D;

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();
    int nDOFpL = qpfldElemL.nDOF();

//    const DLA::MatrixDView_Array<Real>& mmfldCellL = mmfld_.getCellGroupGlobal(cellGroupGlobalL);

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL, -1);
    std::vector<int> mapDOFLocalL( nDOFL, -1 );
    std::vector<int> maprsdL( nDOFL, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = mtxGlobalPDE_q_.m();

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal> integralPDE(quadratureorder, nDOFL, nDOFpL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralLO(quadratureorder, nDOFL);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
    std::vector<ArrayQSurreal> rsdPDEpElemL( nDOFpL );
    std::vector<VectorArrayQSurreal> rsdLOElemL( nDOFL );

    // element jacobians
    MatrixElemClass mtxPDEElemL_qL(nDOFL, nDOFL);
    MatrixElemClass mtxPDEElemLocalL_qL(nDOFL, nDOFL);
    MatrixRElemClass mtxPDEElemL_rL(nDOFL, nDOFL);
    MatrixElemClass mtxPDEElemL_qpL(nDOFL, nDOFpL);

    MatrixElemClass mtxPDEpElemL_qL(nDOFpL, nDOFL);

    MatrixLOElemClass mtxLOElemL_qL(nDOFL, nDOFL);
    DLA::MatrixD< DLA::MatrixS<D,D,Real> > mtxLOElemL_rL(nDOFL, nDOFL);

    // lifting operator jacobian wrt q
    MatrixLOElemClass rL_qL(nDOFL, nDOFL);

    // computes the elemental mass matrix
    ElementalMassMatrix<TopoDim, TopologyL> massMtx(get<-1>(xfldElemL), qfldElemL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxPDEElemL_qL = 0;
      mtxPDEElemLocalL_qL = 0;

      mtxPDEElemL_rL = 0;
      mtxLOElemL_qL  = 0;

      mtxPDEElemL_qpL = 0;
      mtxPDEpElemL_qL = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), mapDOFGlobalL.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOFL; n++)
        if (mapDOFGlobalL[n] < nDOFpossessed)
        {
          maprsdL[nDOFLocal] = n;
          mapDOFLocalL[nDOFLocal] = mapDOFGlobalL[n];
          nDOFLocal++;
        }

      // no residuals are possessed by this processor
//      if (nDOFLocal == 0) continue;

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      qpfldCellL.getElement( qpfldElemL, elemL );

      qfldCellL.getElement( qfldElemSurrealL, elemL );
      qpfldCellL.getElement( qpfldElemSurrealL, elemL );

      // use boundary element numbering
      rbfldCellL.getElement( rbfldElemL, elem );
      rbfldCellL.getElement( rbfldElemSurrealL, elem);
      xfldTrace.getElement(  xfldElemTrace, elem );

//       Compute the elemental mass matrix and then add the lifting operator term from the BC
      massMtx(get<-1>(xfldElemL), mtxLOElemL_rL);

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot, slotOffset = 0;

        //wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemSurrealL.DOF(j), n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemSurrealL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // line integration for canonical element

        for (int n = 0; n < nDOFL; n++)
          rsdLOElemL[n] = 0;

        integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                      xfldElemL, qfldElemSurrealL, rbfldElemL),
                    get<-1>(xfldElemTrace),
                    rsdLOElemL.data(), nDOFL );

        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        for (int n = 0; n < nDOFpL; n++)
          rsdPDEpElemL[n] = 0;

        integralPDE(
            fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemSurrealL, qpfldElemL, rbfldElemL),
            get<-1>(xfldElemTrace),
            rsdPDEElemL.data(), nDOFL, rsdPDEpElemL.data(), nDOFpL );

        // accumulate derivatives into element jacobian
        slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxPDEElemL_qL(i,j), m,n) += DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

                  for (int d = 0; d < D; d++)
                    DLA::index(mtxLOElemL_qL(i,j)[d],m,n) += DLA::index(rsdLOElemL[i][d],m).deriv(slot - nchunk);
                }


              for (int i = 0; i < nDOFpL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxPDEpElemL_qL(i,j), m,n) += DLA::index(rsdPDEpElemL[i], m).deriv(slot - nchunk);
                }
            }
          }
        }
        slotOffset += nEqn*nDOFL;

      }   // nchunk


      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*((D)*nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot, slotOffset = 0;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int k = 0; k < nDeriv; k++)
                DLA::index(rbfldElemSurrealL.DOF(j)[d],n).deriv(k) = 0;

              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rbfldElemSurrealL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }

        // line integration for canonical element
        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        // line integration for canonical element
        for (int n = 0; n < nDOFpL; n++)
          rsdPDEpElemL[n] = 0;

        integralPDE(
            fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL, qpfldElemL, rbfldElemSurrealL),
            get<-1>(xfldElemTrace),
            rsdPDEElemL.data(), nDOFL, rsdPDEpElemL.data(), nDOFpL  );

        // accumulate derivatives into element jacobian
        slotOffset = 0;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d1 = 0; d1 < D; d1++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d1) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxPDEElemL_rL(i,j)(0,d1),m,n) += DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);

//                    for (int d0 = 0; d0 < D; d0++)
//                      DLA::index(mtxLOElemL_rL(i,j)(d0,d1),m,n) += DLA::index(rsdLOElemL[i][d0],m).deriv(slot - nchunk);
                  }
              }
            } //n loop
          } //d1 loop
        } //j loop

      }   // nchunk

      // Compute the lifting operator jacobian
      rL_qL = -DLA::InverseLU::Solve(mtxLOElemL_rL, mtxLOElemL_qL);

      // Add the chain rule from the lifting operators to the PDE jacobian
      mtxPDEElemL_qL += mtxPDEElemL_rL*rL_qL;

      // scatter-add element jacobian to global
      if (nDOFLocal == nDOFL)
        scatterAdd( mapDOFGlobalL, nDOFLocal, mapDOFGlobalL, mtxPDEElemL_qL, mtxGlobalPDE_q_ );
      else
      {
        // compress in only the DOFs possessed by this processor
        for (int i = 0; i < nDOFLocal; i++)
          for (int j = 0; j < nDOFL; j++)
            mtxPDEElemLocalL_qL(i,j) = mtxPDEElemL_qL(maprsdL[i],j);

        scatterAdd( mapDOFLocalL, nDOFLocal, mapDOFGlobalL, mtxPDEElemLocalL_qL, mtxGlobalPDE_q_ );
      }

      //JACOBIAN WRT Q'
      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(nDOFpL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot, slotOffset = 0;

        //wrt qL
        for (int j = 0; j < nDOFpL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qpfldElemSurrealL.DOF(j), n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qpfldElemSurrealL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element
        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        // line integration for canonical element
        for (int n = 0; n < nDOFpL; n++)
          rsdPDEpElemL[n] = 0;

        integralPDE(
            fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL, qpfldElemSurrealL, rbfldElemL),
            get<-1>(xfldElemTrace),
            rsdPDEElemL.data(), nDOFL, rsdPDEpElemL.data(), nDOFpL  );

        // accumulate derivatives into element jacobian
        slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFpL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxPDEElemL_qpL(i,j), m,n) += DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);
                }
            }
          }
        }
        slotOffset += nEqn*nDOFL;

      }   // nchunk

      boundJacPDE_qp_.getCell(cellGroupGlobalL, elemL) += mtxPDEElemL_qpL;
      boundJacPDEp_q_.getCell(cellGroupGlobalL, elemL) += mtxPDEpElemL_qL;

    }
  }

protected:

//----------------------------------------------------------------------------//
  template <template <class> class SparseMatrixType>
  void
  scatterAdd(
      std::vector<int>& mapDOFLocalL,
      const int nDOFLocal,
      std::vector<int>& mapDOFGlobalL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, mapDOFLocalL.data(), nDOFLocal, mapDOFGlobalL.data(), mapDOFGlobalL.size() );
  }

#if 0
//----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class TopoDim,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int elemL,
      int mapDOFGlobalL[], const int nDOFL,
      DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
  }
#endif
protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDE_qp_;
  FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDEp_q_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class MatrixQ>
JacobianBoundaryTrace_VMSD_BR2_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_VMSD_BR2( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                const FieldDataInvMassMatrix_Cell& mmfld,
                                FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDE_qp,
                                FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDEp_q)
{
  return { fcn.cast(), mtxGlobalPDE_q, mmfld, boundJacPDE_qp, boundJacPDEp_q  };
}


}

#endif  // JACOBIANBOUNDARYTRACE_VMSD_H
