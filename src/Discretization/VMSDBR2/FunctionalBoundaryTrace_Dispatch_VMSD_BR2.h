// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTINONALBOUNDARYTRACE_DISPATCH_VMSDBR2_H
#define FUNCTINONALBOUNDARYTRACE_DISPATCH_VMSDBR2_H

// boundary-trace integral residual functions


//#include "Discretization/DG/FunctionalBoundaryTrace_FieldTrace_DGBR2.h"
#include "Discretization/VMSDBR2/FunctionalBoundaryTrace_VMSD_BR2.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_BoundaryCell.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class ArrayJ>
class FunctionalBoundaryTrace_Dispatch_VMSD_BR2_impl
{
public:
  FunctionalBoundaryTrace_Dispatch_VMSD_BR2_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
      const int* quadratureorder, int ngroup,
      ArrayJ& functional )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), qpfld_(qpfld), rbfld_(rbfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      functional_(functional)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_BoundaryCell<TopoDim>::integrate(
      FunctionalBoundaryTrace_VMSD_BR2( fcnOutput_, fcn.cast(), functional_ ), xfld_, (qfld_, qpfld_), rbfld_, quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld_;
  const int* quadratureorder_;
  const int ngroup_;
  ArrayJ& functional_;
};

// Factory function

template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class ArrayJ>
FunctionalBoundaryTrace_Dispatch_VMSD_BR2_impl<FunctionalIntegrand, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, ArrayJ>
FunctionalBoundaryTrace_Dispatch_VMSD_BR2(const FunctionalIntegrand& fcnOutput,
                                       const FieldType<XFieldType>& xfld,
                                       const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                       const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                       const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
                                       const int* quadratureorder, int ngroup,
                                       ArrayJ& functional )
{
  return {fcnOutput, xfld.cast(), qfld, qpfld, rbfld, quadratureorder, ngroup, functional};
}


}

#endif //FUNCTINOALBOUNDARYTRACE_DISPATCH_DGBR2_H
