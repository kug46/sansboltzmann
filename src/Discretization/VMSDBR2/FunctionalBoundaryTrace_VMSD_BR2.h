// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONALBOUNDARYTRACE_VMSDBR2_H
#define FUNCTIONALBOUNDARYTRACE_VMSDBR2_H

// boundary-trace integral functional

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Functional boundary-trace integral
//

template<class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, class T>
class FunctionalBoundaryTrace_VMSD_BR2_impl :
    public GroupIntegralBoundaryTraceType< FunctionalBoundaryTrace_VMSD_BR2_impl<FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, T> >
{
public:
  typedef typename FunctionalIntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayQ<T> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ > VectorArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<T> ArrayJ;

  // Save off the boundary trace integrand and the residual vectors
  FunctionalBoundaryTrace_VMSD_BR2_impl( const FunctionalIntegrandBoundaryTrace& fcnJ,
                                      const BCIntegrandBoundaryTrace& fcnBC,
                                      ArrayJ& functional ) :
    fcnJ_(fcnJ), fcnBC_(fcnBC), functional_(functional), comm_rank_(-1)
  {
    // Find all groups that are common between the BC and functional
    for (std::size_t i = 0; i < fcnJ_.nBoundaryGroups(); i++)
    {
      for (std::size_t j = 0; j < fcnBC_.nBoundaryGroups(); j++)
      {
        std::size_t iBoundaryGroupJ = fcnJ_.boundaryGroup(i);
        if (iBoundaryGroupJ == fcnBC_.boundaryGroup(j))
          boundaryTraceGroups_.push_back(iBoundaryGroupJ);
      }
    }
  }

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return boundaryTraceGroups_; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                          Field<PhysDim,TopoDim,ArrayQ>>::type& flds,
              const Field<PhysDim, TopoDim, VectorArrayQ>& rbfld) const //rbfld
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                                 Field<PhysDim,TopoDim,ArrayQ> >::type:: //qp
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL>& rbfldCellL,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rbfldElemL( rbfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // trace element integral
    ElementIntegral<TopoDimTrace, TopologyTrace, ArrayJ> integral(quadratureorder);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      qfldCellL.getElement( qfldElemL, elemL );

      // only need to perform the volume integral on processors that possess the element
      if ( qfldElemL.rank() != comm_rank_ ) continue;

      qpfldCellL.getElement( qpfldElemL, elemL );
      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );

      // use boundary element numbering
      rbfldCellL.getElement( rbfldElemL, elem );
      xfldTrace.getElement( xfldElemTrace, elem );

      ArrayJ result = 0;

      integral( fcnJ_.integrand(fcnBC_,
                                xfldElemTrace, canonicalTraceL,
                                xfldElemL, qfldElemL, qpfldElemL, rbfldElemL),
                xfldElemTrace, result );

      // sum up the functional
      functional_ += result;
    }
  }

protected:
  const FunctionalIntegrandBoundaryTrace& fcnJ_;
  const BCIntegrandBoundaryTrace& fcnBC_;
  ArrayJ& functional_;
  std::vector<int> boundaryTraceGroups_;
  mutable int comm_rank_;
};

// Factory function

template<class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, class ArrayJ>
FunctionalBoundaryTrace_VMSD_BR2_impl<FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, typename Scalar<ArrayJ>::type>
FunctionalBoundaryTrace_VMSD_BR2( const IntegrandBoundaryTraceType<FunctionalIntegrandBoundaryTrace>& fcnJ,
                               const IntegrandBoundaryTraceType<BCIntegrandBoundaryTrace>& fcnBC,
                               ArrayJ& functional )
{
  typedef typename Scalar<ArrayJ>::type T;
  static_assert( std::is_same<ArrayJ, typename FunctionalIntegrandBoundaryTrace::template ArrayJ<T> >::value, "These should be the same.");
  return {fcnJ.cast(), fcnBC.cast(), functional};
}

}

#endif  // FUNCTIONALBOUNDARYTRACE_VMSDBR2_H
