// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALBOUNDARYTRACE_VMSD_BR2_H
#define JACOBIANFUNCTIONALBOUNDARYTRACE_VMSD_BR2_H

// jacobian boundary-trace functional jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementalMassMatrix.h"

#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/Integrand_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Functional boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, template<class> class Vector>
class JacobianFunctionalBoundaryTrace_VMSD_BR2_impl :
    public GroupIntegralBoundaryTraceType<
       JacobianFunctionalBoundaryTrace_VMSD_BR2_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector> >
{
public:
  typedef typename FunctionalIntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Real> ArrayJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template MatrixJ<Real> MatrixJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Surreal> ArrayJSurreal;
  typedef DLA::VectorS<PhysDim::D, MatrixJ> VectorMatrixJ;

  typedef typename BCIntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianFunctionalBoundaryTrace_VMSD_BR2_impl( const FunctionalIntegrandBoundaryTrace& fcnJ,
                                              const BCIntegrandBoundaryTrace& fcnBC,
                                              Vector<MatrixJ>& jacFunctional_q,
                                              Vector<MatrixJ>& jacFunctional_qp ) :
    fcnJ_(fcnJ),
    fcnBC_(fcnBC),
    jacFunctional_q_(jacFunctional_q),
    jacFunctional_qp_(jacFunctional_qp), comm_rank_(0)
  {
    // Find all groups that are common between the BC and functional
    for (std::size_t i = 0; i < fcnJ_.nBoundaryGroups(); i++)
    {
      for (std::size_t j = 0; j < fcnBC_.nBoundaryGroups(); j++)
      {
        std::size_t iBoundaryGroupJ = fcnJ_.boundaryGroup(i);
        if (iBoundaryGroupJ == fcnBC_.boundaryGroup(j))
          boundaryTraceGroups_.push_back(iBoundaryGroupJ);
      }
    }
  }

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,           // q
                                                   Field<PhysDim,TopoDim,ArrayQ>           // qp
                                      >::type & flds,
              const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, VectorArrayQ>,  // r
                                                   Field<PhysDim, TopoDim, VectorArrayQ>   // s
                                                              >::type& fldsLiftL) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    SANS_ASSERT_MSG(qfld.nDOFpossessed() == jacFunctional_q_.m(), "qfld.nDOF() = %d, jacFunctional_q_.m() = %d", qfld.nDOF(), jacFunctional_q_.m());

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType, class XFieldTraceGroupType>
  void
  integrate( const int cellGroupGloalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,            // q
                                                  Field<PhysDim, TopoDim, ArrayQ>            // qp
                                     >::type::template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const XFieldTraceGroupType& xfldTrace,
             const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, VectorArrayQ>,  // rb
                                                  Field<PhysDim, TopoDim, VectorArrayQ>   // sb
                                     >::type::template FieldCellGroupType<TopologyL>& fldsLiftCellL,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<Surreal> ElementRFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<>        ElementSFieldClassL;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);

    const RFieldCellGroupTypeL& rbfldCellL = get<0>(fldsLiftCellL);
          RFieldCellGroupTypeL& sbfldCellL = const_cast<RFieldCellGroupTypeL&>(get<1>(fldsLiftCellL));

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rbfldElemL( rbfldCellL.basis() );
    ElementSFieldClassL sbfldElemL( sbfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // total number of entries in ArrayQ
    const int nVar = DLA::VectorSize<ArrayQ>::M;

    // total number of outputs in the functional
    const int nJEqn = DLA::VectorSize<ArrayJ>::M;
    static_assert( nJEqn == 1 , "number of output functionals must be 1 for now");

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();
    int nDOFpL = qpfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFLocal( nDOFL, -1 );
    std::vector<int> maprsd( nDOFL, -1 );
    int nDOFLocal = 0;
    std::vector<int> mapDOFGlobal_qpL(nDOFpL, -1);
    const int nDOFpossessed = jacFunctional_q_.m();
    const int nDOFppossessed = jacFunctional_qp_.m();

    // element integrand/residuals
    std::vector<VectorArrayQSurreal> rsdLOElemL( nDOFL );

    // trace element integral
    ElementIntegral<TopoDimTrace, TopologyTrace, ArrayJSurreal> integralJ(quadratureorder);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralLO(quadratureorder, nDOFL);

    MatrixRElemClass mtxTLOElemL_qL(nDOFL, nDOFL);
    DLA::MatrixD< DLA::MatrixS<D,D,Real> > mtxTLOElemL_rL(nDOFL, nDOFL);

    // computes the elemental mass matrix
    ElementalMassMatrix<TopoDim, TopologyL> massMtx(get<-1>(xfldElemL), qfldElemL);

    // element functional jacobian vector
    DLA::VectorD<MatrixJ> jacFunctionalElem_q(nDOFL);
    DLA::VectorD<MatrixJ> jacFunctionalElem_qp(nDOFpL);
    DLA::VectorD<VectorMatrixJ> jacFunctionalElem_r(nDOFL);

    // Provide a vector view of the adjoint lifting operator DOFs
    DLA::VectorDView<VectorArrayQ> sL( sbfldElemL.vectorViewDOF() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {

      // reset the element jacobian
      jacFunctionalElem_q = 0;
      jacFunctionalElem_qp = 0;
      jacFunctionalElem_r = 0;
      mtxTLOElemL_qL = 0;
      mtxTLOElemL_rL = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL.data(), nDOFL );
      qpfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qpL.data(), nDOFpL );
      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOFL; n++)
        if (mapDOFGlobal_qL[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal_qL[n];
          nDOFLocal++;
        }

      // copy global grid/solution DOFs to element
      qfldCellL.getElement( qfldElemL, elemL );

      // no need if all DOFs are possessed by other processors
      if (nDOFLocal == 0 && mapDOFGlobal_qpL[0] >= nDOFppossessed ) continue;

      qpfldCellL.getElement( qpfldElemL, elemL );
      xfldCellL.getElement( xfldElemL, elemL );

      //trace numbering
      rbfldCellL.getElement( rbfldElemL, elem );
      xfldTrace.getElement(  xfldElemTrace, elem );

      // Compute the elemental mass matrix and then add the lifting operator term from the BC
      massMtx(get<-1>(xfldElemL), mtxTLOElemL_rL);

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*((D+1)*nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot = 0;
        int slotOffset = 0;
        //wrt q
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*nDOFL;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nVar; n++)
            {
              slot = slotOffset + nVar*D*j + nVar*d + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rbfldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }

        // integration for canonical element
        for (int n = 0; n < nDOFL; n++)
          rsdLOElemL[n] = 0;

        integralLO( fcnBC_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                        xfldElemL, qfldElemL, rbfldElemL),
                    get<-1>(xfldElemTrace),
                    rsdLOElemL.data(), nDOFL );

        // trace integration for canonical element
        ArrayJSurreal functional = 0;
        integralJ( fcnJ_.integrand(fcnBC_,
                                   xfldElemTrace, canonicalTraceL,
                                   xfldElemL, qfldElemL, qpfldElemL, rbfldElemL),
                   get<-1>(xfldElemTrace),
                   functional );

        // accumulate derivatives into element jacobian

        //wrt qL
        slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 0; // Reset the derivative

              // Note that n,m are transposed intentionally
              // TODO: Test where m > 1
              //for (int m = 0; m < nJEqn; m++)
              //  DLA::index(jacFunctionalElem_q[j],n,m) += DLA::index(functional, m).deriv(slot - nchunk);
              DLA::index(jacFunctionalElem_q[j], n) = DLA::index(functional, n).deriv(slot - nchunk);

              for (int i = 0; i < nDOFL; i++)
              {
                for (int m = 0; m < nVar; m++)
                {
                  for (int d = 0; d < D; d++)
                    // Construct transpose (j,i)(0,d)(n,m)
                    DLA::index(mtxTLOElemL_qL(j,i)(0,d),n,m) += DLA::index(rsdLOElemL[i][d],m).deriv(slot - nchunk);
                }
              }
            }
          }
        }
//
        slotOffset += nVar*nDOFL;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d1 = 0; d1 < D; d1++)
          {
            for (int n = 0; n < nVar; n++)
            {
              slot = slotOffset + nVar*D*j + nVar*d1 + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(rbfldElemL.DOF(j)[d1],n).deriv(slot - nchunk) = 0; // Reset the derivative

                DLA::index(jacFunctionalElem_r[j][d1],n) = DLA::index(functional, n).deriv(slot - nchunk);

                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nVar; m++)
                    for (int d0 = 0; d0 < D; d0++)
                      // Construct transpose (j,i)(d1,d0)(n,m)
                      DLA::index(mtxTLOElemL_rL(j,i)(d1,d0),n,m) += DLA::index(rsdLOElemL[i][d0],m).deriv(slot - nchunk);
              }
            } //n loop
          } //d1 loop
        } //j loop

      }   // nchunk


      for (int j = 0; j < nDOFL; j++)
        for (int n = 0; n < nVar; n++)
          for (int k = 0; k < nDeriv; k++)
          {
            DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

            for (int d = 0; d < D; d++)
              DLA::index(rbfldElemL.DOF(j)[d],n).deriv(k) = 0;
          }


      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*(nDOFpL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot = 0;
        int slotOffset = 0;
        //wrt qp
        for (int j = 0; j < nDOFpL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qpfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*nDOFpL;

        // trace integration for canonical element
        ArrayJSurreal functional = 0;
        integralJ( fcnJ_.integrand(fcnBC_,
                                   xfldElemTrace, canonicalTraceL,
                                   xfldElemL, qfldElemL, qpfldElemL, rbfldElemL),
                   get<-1>(xfldElemTrace),
                   functional );

        // accumulate derivatives into element jacobian

        //wrt qL
        slotOffset = 0;
        for (int j = 0; j < nDOFpL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qpfldElemL.DOF(j), n).deriv(slot - nchunk) = 0; // Reset the derivative

              // Note that n,m are transposed intentionally
              // TODO: Test where m > 1
              //for (int m = 0; m < nJEqn; m++)
              //  DLA::index(jacFunctionalElem_q[j],n,m) += DLA::index(functional, m).deriv(slot - nchunk);
              DLA::index(jacFunctionalElem_qp[j], n) = DLA::index(functional, n).deriv(slot - nchunk);
            }
          }
        }
//
        slotOffset += nVar*nDOFpL;

      }   // nchunk



      // Chain rule to add lifting operator contribution to functional jacobian
      // [dJ(q,r)/dq]^T = [dJ/dq]^T + [dr/dq]^T * [dJ/dr]^T
      //                = [dJ/dq]^T - [dLO/dq]^T * [dLO/dr]^-T * [dJ/dr]^T
      sL = DLA::InverseLU::Solve(mtxTLOElemL_rL, jacFunctionalElem_r);
      jacFunctionalElem_q -= mtxTLOElemL_qL*sL;

      // scatter-add element jacobian to global
      for (int n = 0; n < nDOFLocal; n++)
        jacFunctional_q_[ mapDOFLocal[n] ] += jacFunctionalElem_q[ maprsd[n] ];


      // HACK TO SET BUBBLE RESIDUALS TO ZERO; REMOVES OVERLAP IN BASIS
      const int nBubble = qfldCellL.associativity( elemL ).nBubble();
      if (nBubble > 0 &&  (qpfldElemL.order() > PhysDim::D) )
      {
        SANS_ASSERT( qpfldElemL.basis()->category() == BasisFunctionCategory_Lagrange );
        for (int ind=(nDOFpL - nBubble); ind<nDOFpL; ind++) //CELL DOFS WILL BE LAST?
        {
          jacFunctionalElem_qp[ind] = 0;
        }
      }

      for (int i = 0; i < nDOFpL; i++)
        jacFunctional_qp_[mapDOFGlobal_qpL[i]] += jacFunctionalElem_qp[i];

      // Set the first component of the lifting operator adjoint
      // sL = [dLO/dr]^-T * ([dJ/dr]^T - [dPDE/dr]^T * w)
      sbfldCellL.setElement( sbfldElemL, elem );

    } // elem
  }

protected:
  const FunctionalIntegrandBoundaryTrace& fcnJ_;
  const BCIntegrandBoundaryTrace& fcnBC_;
  std::vector<int> boundaryTraceGroups_;
  Vector<MatrixJ>& jacFunctional_q_;
  Vector<MatrixJ>& jacFunctional_qp_;
  mutable int comm_rank_;
};

// Factory function

template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_VMSD_BR2_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector>
JacobianFunctionalBoundaryTrace_VMSD_BR2( const IntegrandBoundaryTraceType<FunctionalIntegrandBoundaryTrace>& fcnJ,
                                              const IntegrandBoundaryTraceType<BCIntegrandBoundaryTrace>& fcnBC,
                                              Vector< MatrixJ >& func_q,
                                              Vector< MatrixJ >& func_qp)
{
  typedef typename Scalar<MatrixJ>::type T;
  static_assert( std::is_same<typename FunctionalIntegrandBoundaryTrace::template MatrixJ<T>, MatrixJ>::value, "These should be the same");
  return JacobianFunctionalBoundaryTrace_VMSD_BR2_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector>(
      fcnJ.cast(), fcnBC.cast(), func_q, func_qp );
}

}

#endif  // JACOBIANFUNCTIONALBOUNDARYTRACE_DGBR2_H
