// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_DISPATCH_VMSD_BR2_H
#define RESIDUALBOUNDARYTRACE_DISPATCH_VMSD_BR2_H

// boundary-trace integral residual functions

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "ResidualBoundaryTrace_VMSD_BR2.h"

#include "Discretization/IntegrateBoundaryTraceGroups_BoundaryCell.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ, class ArrayQR>
class ResidualBoundaryTrace_Dispatch_VMSD_BR2_impl
{
public:
  ResidualBoundaryTrace_Dispatch_VMSD_BR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQR>& rsdPDEGlobal,
      Vector<ArrayQR>& rsdPDEpGlobal)
    : xfld_(xfld), qfld_(qfld), qpfld_(qpfld), rbfld_(rbfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal),
      rsdPDEpGlobal_(rsdPDEpGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_BoundaryCell<TopoDim>::integrate(
        ResidualBoundaryTrace_VMSD_BR2(fcn, rsdPDEGlobal_, rsdPDEpGlobal_), xfld_, (qfld_, qpfld_), rbfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQR>& rsdPDEGlobal_;
  Vector<ArrayQR>& rsdPDEpGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ, class ArrayQR>
ResidualBoundaryTrace_Dispatch_VMSD_BR2_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, VectorArrayQ, ArrayQR>
ResidualBoundaryTrace_Dispatch_VMSD_BR2(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                             const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
                                             const int* quadratureorder, int ngroup,
                                             Vector<ArrayQR>& rsdPDEGlobal,
                                             Vector<ArrayQR>& rsdPDEpGlobal)
{
  static_assert( DLA::VectorSize<ArrayQ>::M == DLA::VectorSize<ArrayQR>::M, "These should be the same size.");

  return { xfld, qfld, qpfld, rbfld, quadratureorder, ngroup, rsdPDEGlobal, rsdPDEpGlobal };
}

}

#endif //RESIDUALBOUNDARYTRACE_DISPATCH_VMSD_BR2_H
