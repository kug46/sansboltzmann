// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIAN_INTERIORTRACE_BOUNDARY_VMSD_H
#define JACOBIAN_INTERIORTRACE_BOUNDARY_VMSD_H

// HDG trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/CellToTraceAssociativity.h"

#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateCellGroups.h"
#include "JacobianCell_VMSD_BR2_Element.h"
#include "JacobianInteriorTrace_VMSD_BR2_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary trace group integral without lagrange multipliers
//

template<class Surreal, class IntegrandITrace>
class Jacobian_InteriorTrace_onBoundary_VMSD_BR2_impl :
    public GroupIntegralBoundaryTraceType< Jacobian_InteriorTrace_onBoundary_VMSD_BR2_impl<Surreal, IntegrandITrace> >
{
public:
  typedef typename IntegrandITrace::PhysDim PhysDim;
  typedef typename IntegrandITrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef JacobianElemCell_VMSDBR2<PhysDim,MatrixQ> JacobianElemCellType;

  // Save off the boundary trace integrand and the residual vectors
  Jacobian_InteriorTrace_onBoundary_VMSD_BR2_impl(const IntegrandITrace& fcnITrace,
                                  const std::map<int,std::vector<int>>& cellBTraceGroups,
                                  const int& cellgroup, const int& cellelem,
                                  const FieldDataInvMassMatrix_Cell& mmfld,
                                  JacobianElemCellType& mtxElem ) :
    fcnITrace_(fcnITrace), cellBTraceGroups_(cellBTraceGroups),
    cellgroup_(cellgroup), cellelem_(cellelem),
    mmfld_(mmfld), mtxElem_(mtxElem)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellBTraceGroups_.begin(); it != cellBTraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nBoundaryGroups() const { return traceGroupIndices_.size(); }
  std::size_t boundaryGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the matrices
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type& flds ) const
  {
    //CHECKS NOT APPLICABLE

//    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
//    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
//
//    SANS_ASSERT( mtxElemPDE_q_.m() == qfld.nDOF() );
//    SANS_ASSERT( mtxElemPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDE_qp_.m() == qfld.nDOF() );
//    SANS_ASSERT( mtxElemPDE_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDEp_q_.m() == qpfld.nDOF() );
//    SANS_ASSERT( mtxElemPDEp_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDEp_qp_.m() == qpfld.nDOF() );
//    SANS_ASSERT( mtxElemPDEp_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                           Field<PhysDim,TopoDim,ArrayQ>,
                                           FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type::
                     template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<>        ElementRFieldClassL;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<>        ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;
    typedef JacobianElemTrace_VMSD_PDE<PhysDim, MatrixQ> JacobianElemInteriorTraceType;
    typedef JacobianElemTrace_VMSD_LO<PhysDim> JacobianElemInteriorTraceLOType;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<2>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rfldElemsL( rfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFpL = qpfldElemL.nDOF();

    // element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType > integral(quadratureorder);

    //lifting operator integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceLOType> integralLO(quadratureorder);

    // element integrand/residual
    JacobianElemTraceSizeVMSD sizeL(nDOFL, nDOFpL);

    JacobianElemInteriorTraceType mtxElemTmp(sizeL);
    JacobianElemInteriorTraceLOType mtxElemLOL(sizeL);


    const std::vector<int>& traceElemList = cellBTraceGroups_.at(traceGroupGlobal);
    // loop over elements within group
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];
      const int elemL = xfldTrace.getElementLeft( elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        qpfldCellL.getElement( qpfldElemL, elemL );
        rfldCellL.getElement( rfldElemsL, elemL, canonicalTraceL.trace );

        xfldTrace.getElement( xfldElemTrace, elem );

        mtxElemLOL = 0;

        // lifting operator trace integration for canonical element
        integralLO( fcnITrace_.integrand_LO(xfldElemTrace, canonicalTraceL, +1, xfldElemL,
                                            qfldElemL, qpfldElemL, rfldElemsL), xfldElemTrace, mtxElemLOL);

        mtxElemTmp = 0;
        mtxElemTmp.r_qp = -mmfld_.getCellGroupGlobal(cellGroupGlobalL)[elemL]*mtxElemLOL._qp;

        // PDE trace integration for canonical element
        integral( fcnITrace_.integrand(xfldElemTrace, canonicalTraceL, +1, xfldElemL, qfldElemL, qpfldElemL, rfldElemsL),
                                      xfldElemTrace, mtxElemTmp );

        if (fcnITrace_.needsSolutionGradientforSource())
        {
          mtxElem_.PDE_qp += mtxElem_.PDE_LO*mtxElemTmp.r_qp;
          mtxElem_.PDEp_qp += mtxElem_.PDEp_LO*mtxElemTmp.r_qp;
        }

        mtxElem_.PDE_q += mtxElemTmp.PDE_q;
        mtxElem_.PDE_qp += mtxElemTmp.PDE_qp;
        mtxElem_.PDEp_q += mtxElemTmp.PDEp_q;
        mtxElem_.PDEp_qp += mtxElemTmp.PDEp_qp;
      }
    }

  }



protected:
  const IntegrandITrace& fcnITrace_;
  const std::map<int,std::vector<int>>& cellBTraceGroups_;
  const int& cellgroup_;

  const int& cellelem_;
  const FieldDataInvMassMatrix_Cell& mmfld_;

  JacobianElemCellType& mtxElem_; //jacobian of PDE residual wrt q for the central cell

  std::vector<int> traceGroupIndices_;
};

// Factory function

template<class Surreal, class IntegrandITrace, class PhysDim, class MatrixQ>
Jacobian_InteriorTrace_onBoundary_VMSD_BR2_impl<Surreal, IntegrandITrace>
Jacobian_InteriorTrace_onBoundary_VMSD_BR2( const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                           const std::map<int,std::vector<int>>& cellBTraceGroups,
                           const int& cellgroup, const int& cellelem,
                           const FieldDataInvMassMatrix_Cell& mmfld,
                           JacobianElemCell_VMSDBR2<PhysDim,MatrixQ>& mtxElem)
{
  return { fcnITrace.cast(), cellBTraceGroups, cellgroup, cellelem, mmfld, mtxElem };
}

}

#endif  // JACOBIAN_INTERIORTRACE_BOUNDARY_VMSD_H
