// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDTRACE_VMSD_BR2_H
#define INTEGRANDTRACE_VMSD_BR2_H

// interior trace integrand operator: VMSD
//#define VMSDBOUNDARYSOURCEGRAD

#include <ostream>
#include <vector>

#include "JacobianCell_VMSD_BR2_Element.h"
#include "JacobianInteriorTrace_VMSD_BR2_Element.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/smoothmath.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/Galerkin/Stabilization_Nitsche.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// interior trace integrand: VMSD - upwind and viscous stabilization between DG and CG fields
//
// integrandL = + [[phiL]].{F}
// integrandR =   [[phiR]].{F}
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective, viscous)
//
template <class PDE_>
class IntegrandTrace_VMSD_BR2 : public IntegrandInteriorTraceType<IntegrandTrace_VMSD_BR2<PDE_> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  // cppcheck-suppress noExplicitConstructor
  IntegrandTrace_VMSD_BR2(const PDE& pde, const StabilizationNitsche& stab, const std::vector<int>& interiorTraceGroups,
                          const std::vector<int> periodicTraceGroups = {} ) :
    pde_(pde),
    stab_(stab),
    interiorTraceGroups_(interiorTraceGroups),
    periodicTraceGroups_(periodicTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  std::size_t nPeriodicTraceGroups() const          { return periodicTraceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return periodicTraceGroups_[n]; }
  std::vector<int> periodicTraceGroups() const      { return periodicTraceGroups_; }

  // used to know if the source needs the gradient (i.e. lifting operator)
  bool needsSolutionGradientforSource() const
  {
    return pde_.needsSolutionGradientforSource();
  }

  template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL, class ElementParamL>
  class BasisWeighted
  {
  public:

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<VectorArrayQ<Tr>, TopoDimCell, TopologyL> ElementRFieldL;

    typedef typename ElementXFieldTraceType::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;

    typedef JacobianElemTrace_VMSD_PDE<PhysDim,MatrixQ<Real>> JacobianElemInteriorTraceType;
    typedef JacobianElemTrace_Transpose_VMSD_PDE<PhysDim,MatrixQ<Real>> JacobianElemInteriorTraceTransposeType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde,
                   const StabilizationNitsche& stab,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldL& qfldElemL,
                   const ElementQFieldL& qpfldElemL,
                   const ElementRFieldL& rfldElemL ) :
      pde_(pde), stab_(stab),
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), qpfldElemL_(qpfldElemL),
      rfldElemL_(rfldElemL),
      paramfldElemL_( paramfldElemL ),
      nDOFL_(qfldElemL_.nDOF()),
      nDOFpL_(qpfldElemL_.nDOF()),
      phiL_( new Real[nDOFL_] ),
      phipL_( new Real[nDOFpL_] ),
      gradphiL_( new VectorX[nDOFL_] ),
      gradphipL_( new VectorX[nDOFpL_] ),
      basisEqual_((qfldElemL_.basis()->category() == qpfldElemL_.basis()->category()) &&
                  (qfldElemL_.order() == qpfldElemL_.order() )),
      isVMSD0_(qpfldElemL_.order() == 0)
    {}

    BasisWeighted( BasisWeighted&& bw ) :
      pde_(bw.pde_), stab_(bw.stab_),
      xfldElemTrace_(bw.xfldElemTrace_), canonicalTraceL_(bw.canonicalTraceL_), normalSign_(bw.normalSign_),
      xfldElemL_(bw.xfldElemL_),
      qfldElemL_(bw.qfldElemL_), qpfldElemL_(bw.qpfldElemL_), rfldElemL_(bw.rfldElemL_),
      paramfldElemL_( bw.paramfldElemL_ ),
      nDOFL_(bw.nDOFL_),
      nDOFpL_(bw.nDOFpL_),
      phiL_( bw.phiL_ ),
      phipL_( bw.phipL_ ),
      gradphiL_( bw.gradphiL_ ),
      gradphipL_( bw.gradphipL_ ),
      basisEqual_(bw.basisEqual_),
      isVMSD0_(qpfldElemL_.order() == 0)

    {
      bw.phiL_ = nullptr; bw.gradphiL_ = nullptr;
      bw.phipL_ = nullptr; bw.gradphipL_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phiL_;
      delete [] phipL_;

      delete [] gradphiL_;
      delete [] gradphipL_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFpLeft() const { return nDOFpL_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int neqnL,
                                                     ArrayQ<Ti> integrandPL[], const int neqnLP ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL ) const;

    // trace element Jacobian integrand transposed
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceTransposeType& mtxElemL ) const;

  protected:
    template<class Tq, class Tqp, class Tg, class Tgp, class Ti>
    void weightedIntegrand( const VectorX& nL,
        const ParamTL& paramL,
        const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& qpL,
        const VectorArrayQ<Tg>& gradqL,
        const VectorArrayQ<Tgp>& gradqpL,
        ArrayQ<Ti> integrandL[], const int neqnL,
        ArrayQ<Ti> integrandPL[], const int neqnPL,
        bool gradientOnly = false ) const;

    const PDE& pde_;
    const StabilizationNitsche& stab_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementQFieldL& qpfldElemL_;
    const ElementRFieldL& rfldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFpL_;
    mutable Real *phiL_;
    mutable Real *phipL_;
    mutable VectorX *gradphiL_;
    mutable VectorX *gradphipL_;

    const bool basisEqual_;
    const bool isVMSD0_;

  };


  template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL, class ElementParamL>
  class BasisWeighted_LO
  {
  public:

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<VectorArrayQ<Tr>, TopoDimCell, TopologyL> ElementRFieldL;

    typedef typename ElementXFieldTraceType::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;

    typedef JacobianElemTrace_VMSD_LO<PhysDim> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted_LO( const PDE& pde,
                   const StabilizationNitsche& stab,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldL& qfldElemL,
                   const ElementQFieldL& qpfldElemL,
                   const ElementRFieldL& rfldElemL ) :
      pde_(pde),
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), qpfldElemL_(qpfldElemL),
      rfldElemL_(rfldElemL),
      paramfldElemL_( paramfldElemL ),
      nDOFL_(qfldElemL_.nDOF()),
      nDOFpL_(qpfldElemL_.nDOF()),
      phiL_( new Real[nDOFL_] ),
      phipL_( new Real[nDOFpL_] ),
      basisEqual_((qfldElemL_.basis()->category() == qpfldElemL_.basis()->category()) &&
                  (qfldElemL_.order() == qpfldElemL_.order() ))
    {}

    BasisWeighted_LO( BasisWeighted_LO&& bw ) :
      pde_(bw.pde_),
      xfldElemTrace_(bw.xfldElemTrace_), canonicalTraceL_(bw.canonicalTraceL_), normalSign_(bw.normalSign_),
      xfldElemL_(bw.xfldElemL_),
      qfldElemL_(bw.qfldElemL_), qpfldElemL_(bw.qpfldElemL_), rfldElemL_(bw.rfldElemL_),
      paramfldElemL_( bw.paramfldElemL_ ),
      nDOFL_(bw.nDOFL_),
      nDOFpL_(bw.nDOFpL_),
      phiL_( bw.phiL_ ),
      phipL_( bw.phipL_ ),
      basisEqual_(bw.basisEqual_)

    {
      bw.phiL_ = nullptr;
      bw.phipL_ = nullptr;
    }

    ~BasisWeighted_LO()
    {
      delete [] phiL_;
      delete [] phipL_;

    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFpLeft() const { return nDOFpL_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, VectorArrayQ<Ti> integrandLO[], const int neqnLO ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementQFieldL& qpfldElemL_;
    const ElementRFieldL& rfldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFpL_;
    mutable Real *phiL_;
    mutable Real *phipL_;

    const bool basisEqual_;

  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL, class ElementParamL>
  class FieldWeighted
  {
  public:

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL   > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementRFieldL;

    typedef Element<Real, TopoDimCell, TopologyL> ElementEFieldL;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef typename ElementParamL::T ParamTL;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    FieldWeighted( const PDE& pde,
                   const StabilizationNitsche& stab,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter
                   const ElementQFieldL& qfldElemL,
                   const ElementQFieldL& qpfldElemL,
                   const ElementRFieldL& rfldElemL,
                   const ElementQFieldL& wfldElemL,
                   const ElementQFieldL& wpfldElemL,
                   const ElementRFieldL& sfldElemL,
                   const ElementEFieldL& efldElemL ) :
                   pde_(pde), stab_(stab),
                   xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
                   xfldElemL_(get<-1>(paramfldElemL)),
                   qfldElemL_(qfldElemL), qpfldElemL_(qpfldElemL), rfldElemL_(rfldElemL),
                   wfldElemL_(wfldElemL), wpfldElemL_(wpfldElemL), sfldElemL_(sfldElemL),
                   efldElemL_(efldElemL),
                   paramfldElemL_( paramfldElemL ),
                   nDOFL_(qfldElemL_.nDOF()), nDOFpL_(qpfldElemL_.nDOF()),
                   nPhiL_(efldElemL_.nDOF()),
                   phiL_( new Real[nPhiL_] ),
                   gradphiL_( new VectorX[nPhiL_] ),
                   weightL_( new ArrayQ<T>[nPhiL_] ),
                   gradWeightL_( new VectorArrayQ<T>[nPhiL_] ),
                   weightpL_( new ArrayQ<T>[nPhiL_] ),
                   gradWeightpL_( new VectorArrayQ<T>[nPhiL_] )
    {}

    FieldWeighted( FieldWeighted&& fw ) :
                   pde_(fw.pde_), stab_(fw.stab_),
                   xfldElemTrace_(fw.xfldElemTrace_), canonicalTraceL_(fw.canonicalTraceL_), normalSign_(fw.normalSign_),
                   xfldElemL_(fw.xfldElemL_),
                   qfldElemL_(fw.qfldElemL_), qpfldElemL_(fw.qpfldElemL_),  rfldElemL_(fw.rfldElemL_),
                   wfldElemL_(fw.wfldElemL_), wpfldElemL_(fw.wpfldElemL_), sfldElemL_(fw.sfldElemL_),
                   efldElemL_(fw.efldElemL_),
                   paramfldElemL_( fw.paramfldElemL_ ),
                   nDOFL_(fw.nDOFL_), nDOFpL_(fw.nDOFpL_),
                   nPhiL_(fw.nPhiL_),
                   phiL_( fw.phiL_ ),
                   gradphiL_( fw.gradphiL_ ),
                   weightL_( fw.weightL_ ),
                   gradWeightL_( fw.gradWeightL_ ),
                   weightpL_( fw.weightpL_ ),
                   gradWeightpL_( fw.gradWeightpL_ )
    {
      fw.phiL_ = nullptr; fw.gradphiL_ = nullptr;
      fw.weightL_ = nullptr; fw.gradWeightL_ = nullptr;
      fw.weightpL_ = nullptr; fw.gradWeightpL_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phiL_;
      delete [] gradphiL_;
      delete [] weightL_;
      delete [] gradWeightL_;
      delete [] weightpL_;
      delete [] gradWeightpL_;
    }


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFpLeft() const { return nDOFpL_; }

    // element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, Ti integrandL[], const int nphiL ) const;

  protected:
    const PDE& pde_;
    const StabilizationNitsche& stab_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementQFieldL& qpfldElemL_;
    const ElementRFieldL& rfldElemL_;
    const ElementQFieldL& wfldElemL_;
    const ElementQFieldL& wpfldElemL_;
    const ElementRFieldL& sfldElemL_;
    const ElementEFieldL& efldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFpL_;
    const int nPhiL_;
    mutable Real *phiL_;
    mutable VectorX *gradphiL_;
    mutable ArrayQ<T> *weightL_;
    mutable VectorArrayQ<T> *gradWeightL_;
    mutable ArrayQ<T> *weightpL_;
    mutable VectorArrayQ<T> *gradWeightpL_;
  };

  //Factory function that returns the basis weighted integrand class
  template< class T, class Tr, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL, class ElementParamL >
  BasisWeighted<T, Tr, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL, ElementParamL>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qpfldElemL,
            const Element<VectorArrayQ<Tr>, TopoDimCell, TopologyL>& rfldElemL) const
  {
    return BasisWeighted<T, Tr, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL, ElementParamL>(pde_, stab_, xfldElemTrace, canonicalTraceL, normalSign,
                                                                    paramfldElemL, qfldElemL, qpfldElemL, rfldElemL);
  }

  //Factory function that returns the basis weighted integrand class
  template< class T, class Tr, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL, class ElementParamL >
  BasisWeighted_LO<T, Tr, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL, ElementParamL>
  integrand_LO(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qpfldElemL,
            const Element<VectorArrayQ<Tr>, TopoDimCell, TopologyL>& rfldElemL) const
  {
    return BasisWeighted_LO<T, Tr, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL, ElementParamL>(pde_, stab_, xfldElemTrace, canonicalTraceL, normalSign,
                                                                    paramfldElemL, qfldElemL, qpfldElemL, rfldElemL);
  }

  //Factory function that returns the field weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL, class ElementParamL >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell, TopologyL, ElementParamL  >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qpfldElemL,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& rfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& wfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& wpfldElemL,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& sfldElemL,
            const Element<Real, TopoDimCell, TopologyL>& efldElemL) const
  {
    return FieldWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL, ElementParamL>(pde_, stab_, xfldElemTrace, canonicalTraceL, normalSign,
                                                                    paramfldElemL, qfldElemL, qpfldElemL, rfldElemL,
                                                                                   wfldElemL, wpfldElemL, sfldElemL, efldElemL);
  }


protected:
  const PDE& pde_;
  const StabilizationNitsche& stab_;
  std::vector<int> interiorTraceGroups_;
  std::vector<int> periodicTraceGroups_;
};


template<class PDE>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class ElementParamL >
bool
IntegrandTrace_VMSD_BR2<PDE>::
BasisWeighted<T, Tr, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}


template<class PDE>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class ElementParamL >
bool
IntegrandTrace_VMSD_BR2<PDE>::
BasisWeighted_LO<T, Tr, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}

template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class ElementParamL>
bool
IntegrandTrace_VMSD_BR2<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}


template<class PDE>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class ElementParamL>
template <class Ti>
void
IntegrandTrace_VMSD_BR2<PDE>::
BasisWeighted<T, Tr, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], const int neqnL,
                                                 ArrayQ<Ti> integrandPL[], const int neqnPL ) const
{
  SANS_ASSERT(neqnL == nDOFL_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> qL, qpL;                // solutions
  VectorArrayQ<T> gradqL, gradqpL;  // solution gradients
  VectorArrayQ<Tr> gradqpLiftedL;  // solution gradients
  VectorArrayQ<Tr> rliftL;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)

  const bool needsSolutionGradient =
      (pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );

  if (basisEqual_)
  { // q and q' share the same basis.. copy over instead of recomputing
    for (int k=0; k<neqnL; k++)
    {
      phipL_[k] = phiL_[k];
      gradphipL_[k] = gradphiL_[k];
    }
  }
  else
  {
    qpfldElemL_.evalBasis( sRefL, phipL_, nDOFpL_ );
    xfldElemL_.evalBasisGradient( sRefL, qpfldElemL_, gradphipL_, nDOFpL_ );
  }

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qpfldElemL_.evalFromBasis( phipL_, nDOFpL_, qpL );

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qpfldElemL_.evalFromBasis( gradphipL_, nDOFpL_, gradqpL );

    rfldElemL_.evalFromBasis( phipL_, nDOFpL_, rliftL );
//    rfldElemL_.eval( sRefL, rliftL );

//    Real eta = 2.0*TopologyL::NTrace;
    Real eta = 4.0;
//    gradqL += gradqpL;
    gradqpLiftedL = gradqpL + eta*rliftL;
  }


  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  nL *= normalSign_;

  // compute the integrand
  weightedIntegrand( nL, paramL,
                     qL, qpL,
                     gradqL, gradqpLiftedL,
                     integrandL, neqnL,
                     integrandPL, neqnPL );
}


//---------------------------------------------------------------------------//
template<class PDE>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class ElementParamL>
void
IntegrandTrace_VMSD_BR2<PDE>::
BasisWeighted<T, Tr, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace, JacobianElemInteriorTraceType& mtxElemL ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFp == nDOFpL_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> qL = 0, qpL = 0;                // solutions
  VectorArrayQ<T> gradqL = 0, gradqpL = 0;  // solution gradients
  VectorArrayQ<Tr> gradqpLiftedL = 0;  // solution gradients
  VectorArrayQ<Tr> rliftL = 0;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)

  const bool needsSolutionGradient =
      (pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );

  if (basisEqual_)
  {
    for (int k=0; k<nDOFL_; k++)
    {
      phipL_[k] = phiL_[k];
      gradphipL_[k] = gradphiL_[k];
    }
  }
  else
  {
    qpfldElemL_.evalBasis( sRefL, phipL_, nDOFpL_ );
    xfldElemL_.evalBasisGradient( sRefL, qpfldElemL_, gradphipL_, nDOFpL_ );
  }

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qpfldElemL_.evalFromBasis( phipL_, nDOFpL_, qpL );

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  nL *= normalSign_;

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL = qL;
  ArrayQ<SurrealClass> qpSurrealL = qpL;

//  Real eta = 2.0*TopologyL::NTrace;
  Real eta = 4.0;
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qpfldElemL_.evalFromBasis( gradphipL_, nDOFpL_, gradqpL );

//    rfldElemL_.eval( sRefL, rliftL );
    rfldElemL_.evalFromBasis( phipL_, nDOFpL_, rliftL );

//    gradqL += gradqpL;
    gradqpLiftedL = gradqpL + eta*rliftL;
  }

  VectorArrayQ<SurrealClass> gradqSurrealL = gradqL;
  VectorArrayQ<SurrealClass> gradqpliftedSurrealL = gradqpLiftedL;

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0; // temporary storage
  MatrixQ<Real> PDE_qp = 0; // temporary storage
  MatrixQ<Real> PDEp_q = 0, PDEp_gradq = 0; // temporary storage
  MatrixQ<Real> PDEp_qp = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurrealP( nDOFpL_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandLSurreal = 0; //this happens inside
    integrandLSurrealP = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL,
                       qSurrealL, qpL,
                       gradqL, gradqpLiftedL,
                       integrandLSurreal.data(), nDOFL_,
                       integrandLSurrealP.data(), nDOFpL_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_q(i,j) += dJ*phiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFpL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDEp_q,m,n) = DLA::index(integrandLSurrealP[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDEp_q(i,j) += dJ*phiL_[j]*PDEp_q;
      }

    } // if slot
    slot += PDE::N;

  } // nchunk


  // compute the lifting operator jacobians at the quadrature point
  DLA::MatrixDView<Real> phipL(phipL_, 1, nDOFpL_);
  DLA::MatrixD<VectorX> r_qp = eta*(phipL*mtxElemL.r_qp);

  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandLSurreal = 0; //this happens inside
      integrandLSurrealP = 0;

      // compute the integrand
      weightedIntegrand( nL, paramL,
                         qL, qpL,
                         gradqSurrealL, gradqpLiftedL,
                         integrandLSurreal.data(), nDOFL_,
                         integrandLSurrealP.data(), nDOFpL_ );

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDE_q(i,j) += dJ*gradphiL_[j][d]*PDE_gradq;

//            for (int j = 0; j < nDOFpL_; j++)
//              mtxElemL.PDE_qp(i,j) += dJ*(gradphipL_[j][d] + r_qp(0,j)[d])*PDE_gradq;
          }

          for (int i = 0; i < nDOFpL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDEp_gradq,m,n) = DLA::index(integrandLSurrealP[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDEp_q(i,j) += dJ*gradphiL_[j][d]*PDEp_gradq;

//            //this includes dPDE/dr
//            for (int j = 0; j < nDOFpL_; j++)
//              mtxElemL.PDEp_qp(i,j) += dJ*(gradphipL_[j][d] +  r_qp(0,j)[d])*PDEp_gradq;
          }

        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient
//
  //derivatives w.r.t to q'
  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qpSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandLSurreal = 0; //this happens inside
    integrandLSurrealP = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL,
                       qL, qpSurrealL,
                       gradqL, gradqpLiftedL,
                       integrandLSurreal.data(), nDOFL_,
                       integrandLSurrealP.data(), nDOFpL_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qpSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_qp,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFpL_; j++)
          mtxElemL.PDE_qp(i,j) += dJ*phipL_[j]*PDE_qp;
      }


      for (int i = 0; i < nDOFpL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDEp_qp,m,n) = DLA::index(integrandLSurrealP[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFpL_; j++)
          mtxElemL.PDEp_qp(i,j) += dJ*phipL_[j]*PDEp_qp;
      }

    } // if slot
    slot += PDE::N;

  } // nchunk

  // loop over derivative chunks to computes derivatives w.r.t gradq'
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqpliftedSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandLSurreal = 0; //this happens inside
      integrandLSurrealP = 0;

      // compute the integrand
      weightedIntegrand( nL, paramL,
                         qL, qpL,
                         gradqL, gradqpliftedSurrealL,
                         integrandLSurreal.data(), nDOFL_,
                         integrandLSurrealP.data(), nDOFpL_ );

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqpliftedSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

//            for (int j = 0; j < nDOFL_; j++)
//              mtxElemL.PDE_q(i,j) += dJ*gradphiL_[j][d]*PDE_gradq;

            for (int j = 0; j < nDOFpL_; j++)
              mtxElemL.PDE_qp(i,j) += dJ*(gradphipL_[j][d] + r_qp(0,j)[d])*PDE_gradq;
          }

          for (int i = 0; i < nDOFpL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDEp_gradq,m,n) = DLA::index(integrandLSurrealP[i], m).deriv(slot + n - nchunk);

//            for (int j = 0; j < nDOFL_; j++)
//              mtxElemL.PDEp_q(i,j) += dJ*gradphiL_[j][d]*PDEp_gradq;

            //this includes dPDE/dr
            for (int j = 0; j < nDOFpL_; j++)
              mtxElemL.PDEp_qp(i,j) += dJ*(gradphipL_[j][d] +  r_qp(0,j)[d])*PDEp_gradq;
          }

        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient

}


//---------------------------------------------------------------------------//
template<class PDE>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class ElementParamL>
void
IntegrandTrace_VMSD_BR2<PDE>::
BasisWeighted<T, Tr, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace, JacobianElemInteriorTraceTransposeType& mtxElemL ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFp == nDOFpL_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> qL = 0, qpL = 0;                // solutions
  VectorArrayQ<T> gradqL = 0, gradqpL = 0;  // solution gradients
  VectorArrayQ<Tr> gradqpLiftedL = 0;  // solution gradients
  VectorArrayQ<Tr> rliftL = 0;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)

  const bool needsSolutionGradient =
      (pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );

  if (basisEqual_)
  {
    for (int k=0; k<nDOFL_; k++)
    {
      phipL_[k] = phiL_[k];
      gradphipL_[k] = gradphiL_[k];
    }
  }
  else
  {
    qpfldElemL_.evalBasis( sRefL, phipL_, nDOFpL_ );
    xfldElemL_.evalBasisGradient( sRefL, qpfldElemL_, gradphipL_, nDOFpL_ );
  }

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qpfldElemL_.evalFromBasis( phipL_, nDOFpL_, qpL );

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  nL *= normalSign_;

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
//  Real eta = 2.0*TopologyL::NTrace;
  Real eta = 4.0;
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qpfldElemL_.evalFromBasis( gradphipL_, nDOFpL_, gradqpL );

//    rfldElemL_.eval( sRefL, rliftL );
    rfldElemL_.evalFromBasis( phipL_, nDOFpL_, rliftL );

    gradqL += gradqpL;
    gradqpLiftedL = gradqL + eta*rliftL;
  }

  VectorArrayQ<SurrealClass> gradqSurrealL = gradqL;
  VectorArrayQ<SurrealClass> gradqpliftedSurrealL = gradqpLiftedL;

  MatrixQ<Real> PDET_r = 0;
  MatrixQ<Real> PDEpT_r = 0;

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurrealP( nDOFpL_ );

  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqpliftedSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      // compute the integrand
      weightedIntegrand( nL, paramL,
                         qL, qpL,
                         gradqL, gradqpliftedSurrealL,
                         integrandLSurreal.data(), nDOFL_,
                         integrandLSurrealP.data(), nDOFpL_ );

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqpliftedSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
              {
                //n,m deliberately switched here
                DLA::index(PDET_r,n,m) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);
              }

            for (int j = 0; j < nDOFpL_; j++)
              mtxElemL.PDET_r(j,i)[d] += dJ*phipL_[j]*PDET_r;

          }

          for (int i = 0; i < nDOFpL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++) //n,m deliberately switched here
                DLA::index(PDEpT_r,n,m) = DLA::index(integrandLSurrealP[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFpL_; j++)
              mtxElemL.PDEpT_r(j,i)[d] += dJ*phipL_[j]*PDEpT_r;

          }

        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient
//


}

template<class PDE>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class ElementParamL>
template<class Tq, class Tqp, class Tg, class Tgp, class Ti>
void
IntegrandTrace_VMSD_BR2<PDE>::
BasisWeighted<T, Tr, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
weightedIntegrand( const VectorX& nL, const ParamTL& paramL,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& qpL,
                   const VectorArrayQ<Tg>& gradqL,
                   const VectorArrayQ<Tgp>& gradqpL,
                   ArrayQ<Ti> integrandL[], const int neqnL,
                   ArrayQ<Ti> integrandPL[], const int neqnPL,
                   bool gradientOnly ) const
{
  typedef typename promote_Surreal<Tq,Tqp>::type TqS;
  typedef typename promote_Surreal<Tg,Tgp>::type TgS;

  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnPL == nDOFpL_);

  //zero integrands
  for (int k = 0; k < neqnL; k++) integrandL[k] = 0;
  for (int k = 0; k < neqnPL; k++) integrandPL[k] = 0;

  //NO R1 viscous flux

  // R2 fluxes: DUAL CONSISTENCY TERM

  ArrayQ<TqS> qtot = qL + qpL;
  VectorArrayQ<TgS> gradqtot = gradqL + gradqpL;

  //constructing lengthscale from P1 basis
  //ensures sufficiently large h^-1
  if (pde_.fluxViscousLinearInGradient())
  {

    VectorArrayQ<Tqp> dqpN = 0;
    for (int i=0; i<PhysDim::D; i++)
      dqpN[i] -= qpL*nL[i];  //negate because perturbedGradFluxViscous returns -K*duN

    VectorArrayQ<TqS> Kndu = 0;
    pde_.fluxViscous(paramL, qtot, dqpN, Kndu);

    for (int k = 0; k < neqnL; k++)
      integrandL[k] -= dot(gradphiL_[k],Kndu);

    // R3 term: <v', -K.grad u>
    VectorArrayQ<Ti> FvL = 0;
    ArrayQ<TqS> FaL = 0; // normal advective flux
    ArrayQ<TqS> qLS = qL;

    Real scale = 1.0;
    pde_.fluxAdvectiveUpwind( paramL, qtot, qLS, nL, FaL, scale );
    pde_.fluxViscous( paramL, qtot, gradqtot, FvL );
    ArrayQ<Ti> FnL = dot(nL,FvL);
    FnL += FaL;

    for (int k = 0; k < neqnPL; k++)
      integrandPL[k] +=  phipL_[k]*FnL;

    //DUAL CONSISTENCY
    if (!isVMSD0_)
      for (int k = 0; k < neqnPL; k++)
        integrandPL[k] -= dot(gradphipL_[k],Kndu);
  }
  else
  {
    VectorArrayQ<Ti> Kndu = 0;
    // viscous flux dual consistency term
    MatrixQ<TqS> dudq = 0;
    pde_.jacobianMasterState( paramL, qtot, dudq );

    ArrayQ<TqS> du = dudq*qpL;
    VectorArrayQ<TqS> duN = 0;

    for (int i=0; i<PhysDim::D; i++)
      duN[i] += du*nL[i];  //negate because perturbedGradFluxViscous returns -K*duN

    DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<Ti>> K = 0;   // diffusion matrix
    pde_.diffusionViscous( paramL, qtot, gradqtot, K );

    Kndu = K*duN;

    for (int k = 0; k < neqnL; k++)
      integrandL[k] -= dot(gradphiL_[k],Kndu);

    // R3 term: <v', -K.grad u>
    VectorArrayQ<Ti> FvL = 0;
    ArrayQ<TqS> FaL = 0; // normal advective flux
    ArrayQ<TqS> qLS = qL;

    Real scale = 1.0;
    pde_.fluxAdvectiveUpwind( paramL, qtot, qLS, nL, FaL, scale );
    pde_.fluxViscous( paramL, qtot, gradqtot, FvL );
    ArrayQ<Ti> FnL = dot(nL,FvL);
    FnL += FaL;

    //R4 TERMS:
    for (int k = 0; k < neqnPL; k++)
      integrandPL[k] +=  phipL_[k]*FnL;

    //DUAL CONSISTENCY
    if (!isVMSD0_)
      for (int k = 0; k < neqnPL; k++)
        integrandPL[k] -= dot(gradphipL_[k],Kndu);
  }
}


template<class PDE>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class ElementParamL>
template <class Ti>
void
IntegrandTrace_VMSD_BR2<PDE>::
BasisWeighted_LO<T, Tr, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandLO[], const int neqnLO) const
{
  SANS_ASSERT(neqnLO == nDOFpL_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> qpL;                // solutions
  QuadPointCellType sRefL;         // reference-element coordinates (s,t)

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // basis value, gradient
  qpfldElemL_.evalBasis( sRefL, phipL_, nDOFpL_ );
  qpfldElemL_.evalFromBasis( phipL_, nDOFpL_, qpL );

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  nL *= normalSign_;

  VectorArrayQ<T> dqn = 0;
  for (int d=0; d<D; d++)
    dqn[d] = nL[d]*qpL;

  for (int i=0; i<neqnLO; i++)
    integrandLO[i] = phipL_[i]*dqn;


}


template<class PDE>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class ElementParamL>
void
IntegrandTrace_VMSD_BR2<PDE>::
BasisWeighted_LO<T, Tr, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemLO ) const
{
  SANS_ASSERT(mtxElemLO.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemLO.nDOFp == nDOFpL_);

  VectorX nL;                        // unit normal for left element (points to right element)

  QuadPointCellType sRefL;         // reference-element coordinates of the cell
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  nL *= normalSign_;

  // computes derivatives of the lifting operator equation wrt q
  // the equation is linear, so no Surreal
  // Note that the Jacobian is diagonal MatrixQ with all identical entries.
  // This is reduced to a scalar matrix for computational efficiency

  qpfldElemL_.evalBasis( sRefL, phipL_, nDOFpL_ );

  for (int i = 0; i < nDOFpL_; i++)
    for (int j = 0; j < nDOFpL_; j++)
      mtxElemLO._qp(i,j) +=  dJ*phipL_[i]*phipL_[j]*nL;

}



template<class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class ElementParamL>
template <class Ti>
void
IntegrandTrace_VMSD_BR2<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
operator()(const QuadPointTraceType& sRefTrace, Ti integrandL[], const int nphiL ) const
{
  SANS_ASSERT( nphiL == nPhiL_ );

  ParamTL paramL;   // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;       // unit normal for left element (points to right element)

  ArrayQ<T> qL = 0, qpL = 0;                // solution
  VectorArrayQ<T> gradqL = 0, gradqpL = 0;  // solution gradients
  VectorArrayQ<T> rLiftL = 0, sLiftL = 0;

  ArrayQ<T> wL = 0, wpL = 0;                // weight
  VectorArrayQ<T> gradwL = 0, gradwpL = 0;  // weight gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  nL *= normalSign_;

  // solution value, gradient
  qfldElemL_.eval( sRefL, qL );
  qpfldElemL_.eval( sRefL, qpL );
  rfldElemL_.eval( sRefL, rLiftL );

  if (pde_.hasFluxViscous() || pde_.hasSourceTrace())
  {
    xfldElemL_.evalGradient( sRefL, qfldElemL_, gradqL );
    xfldElemL_.evalGradient( sRefL, qpfldElemL_, gradqpL );
  }

  // weight value,
  wfldElemL_.eval( sRefL, wL );
  wpfldElemL_.eval( sRefL, wpL );
  sfldElemL_.eval( sRefL, sLiftL );

  xfldElemL_.evalGradient( sRefL, wfldElemL_, gradwL );
  xfldElemL_.evalGradient( sRefL, wpfldElemL_, gradwpL );

  // estimate basis, gradient
  efldElemL_.evalBasis( sRefL, phiL_, nPhiL_);
  xfldElemL_.evalBasisGradient( sRefL, efldElemL_, gradphiL_, nPhiL_ );

  // Grad (w phi) needed
  for (int k = 0; k < nPhiL_; k++)
  {
    integrandL[k] = 0;
    gradWeightL_[k] = gradphiL_[k]*wL + phiL_[k]*gradwL;

    weightpL_[k] = phiL_[k]*wpL;
    gradWeightpL_[k] = gradphiL_[k]*wpL + phiL_[k]*gradwpL;
  }

  ArrayQ<T> qtot = qL + qpL;

//  Real eta = 2.0*TopologyL::NTrace;
  Real eta = 4.0;
  VectorArrayQ<T> gradqtot = gradqL + gradqpL + eta*rLiftL;

  //constructing lengthscale from P1 basis
  //ensures sufficiently large h^-1

  VectorArrayQ<T> dqpN = 0;
  for (int i=0; i<PhysDim::D; i++)
    dqpN[i] -= qpL*nL[i];  //negate because perturbedGradFluxViscous returns -K*duN

  if (pde_.fluxViscousLinearInGradient())
  {

    VectorArrayQ<T> Kndu = 0;
    pde_.fluxViscous(paramL, qtot, dqpN, Kndu);

    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] -= dot(gradWeightL_[k],Kndu);

    // R3 term: <v', -K.grad u>
    VectorArrayQ<Ti> FvL = 0;
    ArrayQ<T> FaL = 0; // normal advective flux
    ArrayQ<T> qLS = qL;

    Real scale = 1.0;
    pde_.fluxAdvectiveUpwind( paramL, qtot, qLS, nL, FaL, scale );
    pde_.fluxViscous( paramL, qtot, gradqtot, FvL );
    ArrayQ<Ti> FnL = dot(nL,FvL);
    FnL += FaL;

    //R4 TERMS:

    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] +=  dot(weightpL_[k],FnL);

    //DUAL CONSISTENCY
    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] -= dot(gradWeightpL_[k],Kndu);
  }
  else
  {
    VectorArrayQ<Ti> Kndu = 0;
    // viscous flux dual consistency term
    MatrixQ<T> dudq = 0;
    pde_.jacobianMasterState( paramL, qtot, dudq );

    ArrayQ<T> du = dudq*qpL;
    VectorArrayQ<T> duN = 0;

    for (int i=0; i<PhysDim::D; i++)
      duN[i] += du*nL[i];  //negate because perturbedGradFluxViscous returns -K*duN

    DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<Ti>> K = 0;   // diffusion matrix
    pde_.diffusionViscous( paramL, qtot, gradqtot, K );

    Kndu = K*duN;

    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] -= dot(gradWeightL_[k],Kndu);

    // R3 term: <v', -K.grad u>
    VectorArrayQ<Ti> FvL = 0;
    ArrayQ<T> FaL = 0; // normal advective flux
    ArrayQ<T> qLS = qL;

    Real scale = 1.0;
    pde_.fluxAdvectiveUpwind( paramL, qtot, qLS, nL, FaL, scale );
    pde_.fluxViscous( paramL, qtot, gradqtot, FvL );
    ArrayQ<Ti> FnL = dot(nL,FvL);
    FnL += FaL;

    //R4 TERMS:

    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] +=  dot(weightpL_[k],FnL);

    //DUAL CONSISTENCY
    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] -= dot(gradWeightpL_[k],Kndu);
  }

  // SOURCE TERM STABILiZATION
//  MatrixQ<T> dSdu = 0;
//  pde_.jacobianSourceAbsoluteValue(paramL, qL, gradqL, dSdu);
//
//  // viscous flux dual consistency term
//  MatrixQ<T> dudq = 0;
//  pde_.jacobianMasterState( paramL, qL, dudq );
//  ArrayQ<T> du = dudq*qpL;
//
//  ArrayQ<Ti> dS = dSdu*du;
//
//  Real length = xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
//  dS *= length;
//
//  for (int k = 0; k < nPhiL_; k++)
//    integrandL[k] +=  dot(weightpL_[k],dS);

  //lifting operator term
  T tmpdot = dot(sLiftL,dqpN);
  for (int k = 0; k < nPhiL_; k++)
  {
    integrandL[k] -= tmpdot*phiL_[k];
  }



}

} // namespace SANS
#endif //INTEGRANDTRACE_VMSD_BR2_H
