// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_VMSD_BR2_ELEMENT_H
#define JACOBIANCELL_VMSD_BR2_ELEMENT_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Discretization/JacobianElementMatrix.h"

namespace SANS
{


template<class PhysDim, class MatrixQ>
struct JacobianElemCell_VMSDBR2 : JacElemMatrixType< JacobianElemCell_VMSDBR2<PhysDim,MatrixQ> >
{
  // PDE Jacobian wrt q
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianElemCell_VMSDBR2(const int nDOF, const int nDOFp)
   : nDOF(nDOF), nDOFp(nDOFp), nDOFLO(PhysDim::D),
     PDE_q(nDOF, nDOF),
     PDE_qp(nDOF, nDOFp),
     PDEp_q(nDOFp, nDOF),
     PDEp_qp(nDOFp, nDOFp),
     PDE_LO(nDOF, nDOFp),
     PDEp_LO(nDOFp, nDOFp)
  {}

  const int nDOF;
  const int nDOFp;
  const int nDOFLO;

  // element PDE jacobian matrices wrt q
  MatrixElemClass PDE_q;
  MatrixElemClass PDE_qp;
  MatrixElemClass PDEp_q;
  MatrixElemClass PDEp_qp;
  MatrixRElemClass PDE_LO;
  MatrixRElemClass PDEp_LO;

  inline Real operator=( const Real s )
  {
    PDE_q = s;
    PDE_qp = s;

    PDEp_q = s;
    PDEp_qp = s;

    PDE_LO = s;
    PDEp_LO = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemCell_VMSDBR2& operator+=(
      const JacElemMulScalar< JacobianElemCell_VMSDBR2 >& mul )
  {
    PDE_q += mul.s*mul.mtx.PDE_q;
    PDE_qp += mul.s*mul.mtx.PDE_qp;

    PDEp_q += mul.s*mul.mtx.PDEp_q;
    PDEp_qp += mul.s*mul.mtx.PDEp_qp;

    PDE_LO +=  mul.s*mul.mtx.PDE_LO;
    PDEp_LO +=  mul.s*mul.mtx.PDEp_LO;

    return *this;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemCell_VMSDBR2& operator+=(
      const JacobianElemCell_VMSDBR2& mtx )
  {
    PDE_q += mtx.PDE_q;
    PDE_qp += mtx.PDE_qp;

    PDEp_q += mtx.PDEp_q;
    PDEp_qp += mtx.PDEp_qp;

    PDE_LO += mtx.PDE_LO;
    PDEp_LO += mtx.PDEp_LO;

    return *this;
  }

};

}
#endif // JACOBIANCELL_VMSD_BR2_ELEMENT_H
