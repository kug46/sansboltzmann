// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SET_LO_INTERIORTRACE_VMSD_BR2_H
#define SET_LO_INTERIORTRACE_VMSD_BR2_H

// Computes lifting operators on specified interior trace groups

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "IntegrandTrace_VMSD_BR2.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG BR2 interior-trace integral
//

template<class IntegrandInteriorTrace>
class SetLO_InteriorTrace_VMSD_BR2_impl :
    public GroupIntegralInteriorTraceType< SetLO_InteriorTrace_VMSD_BR2_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PDE PDE;
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the interor trace integrand and inverse mass matrix field
  SetLO_InteriorTrace_VMSD_BR2_impl( const IntegrandInteriorTrace& fcn,
                                     const std::map<int,std::vector<int>>& cellITraceGroups,
                                     const int& cellgroup, const int& cellelem,
                                     const FieldDataInvMassMatrix_Cell& mmfld ) :
     fcn_(fcn), cellITraceGroups_(cellITraceGroups),
     cellgroup_(cellgroup), cellelem_(cellelem), mmfld_(mmfld)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // Nothing to check
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type& flds ) const
  {
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                           Field<PhysDim,TopoDim,ArrayQ>,
                                           FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type::
                     template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                           Field<PhysDim,TopoDim,ArrayQ>,
                                           FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type::
                     template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename RFieldCellGroupTypeR::template ElementType<> ElementRFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
          RFieldCellGroupTypeL& rfldCellL = const_cast<RFieldCellGroupTypeL&>(get<2>(fldsCellL));

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const QFieldCellGroupTypeR& qpfldCellR = get<1>(fldsCellR);
          RFieldCellGroupTypeR& rfldCellR = const_cast<RFieldCellGroupTypeR&>(get<2>(fldsCellR));


    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementQFieldClassR qpfldElemR( qpfldCellR.basis() );
    ElementRFieldClassR rfldElemR( rfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    const int nIntegrandPL = qpfldElemL.nDOF();
    const int nIntegrandPR = qpfldElemR.nDOF();

    const DLA::MatrixDView_Array<Real>& mmfldCellL = mmfld_.getCellGroupGlobal(cellGroupGlobalL);
    const DLA::MatrixDView_Array<Real>& mmfldCellR = mmfld_.getCellGroupGlobal(cellGroupGlobalR);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQ> integralL(quadratureorder, nIntegrandPL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQ> integralR(quadratureorder, nIntegrandPR);

    // element integrand/residuals
    DLA::VectorD< VectorArrayQ > rsdElemL( nIntegrandPL );
    DLA::VectorD< VectorArrayQ > rsdElemR( nIntegrandPR );

    DLA::VectorDView<VectorArrayQ> rL( rfldElemL.vectorViewDOF() );
    DLA::VectorDView<VectorArrayQ> rR( rfldElemR.vectorViewDOF() );

    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        qpfldCellL.getElement( qpfldElemL, elemL );
        rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

        for (int n = 0; n < nIntegrandPL; n++) rsdElemL[n] = 0;

        integralL( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL, +1, xfldElemL,
                                     qfldElemL, qpfldElemL, rfldElemL),
                   xfldElemTrace, rsdElemL.data(), nIntegrandPL );

        rL = -mmfldCellL[elemL]*rsdElemL;

        rfldCellL.setElement( rfldElemL, elemL, canonicalTraceL.trace );
      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {

        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        qpfldCellR.getElement( qpfldElemR, elemR );
        rfldCellR.getElement( rfldElemR, elemR, canonicalTraceR.trace );


        for (int n = 0; n < nIntegrandPR; n++) rsdElemR[n] = 0;


        integralR( fcn_.integrand_LO(xfldElemTrace, canonicalTraceR, -1, xfldElemR,
                                     qfldElemR, qpfldElemR, rfldElemR),
                   xfldElemTrace, rsdElemR.data(), nIntegrandPR );

        // Compute the lifting operator DOFs
        rR = -mmfldCellR[elemR]*rsdElemR;

        // Set the lifting operators
        rfldCellR.setElement( rfldElemR, elemR, canonicalTraceR.trace );
      }
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;

  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;

  const FieldDataInvMassMatrix_Cell& mmfld_;
  std::vector<int> traceGroupIndices_;
};


// Factory function

template<class IntegrandInteriorTrace>
SetLO_InteriorTrace_VMSD_BR2_impl<IntegrandInteriorTrace>
SetLO_InteriorTrace_VMSD_BR2( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                                const std::map<int,std::vector<int>>& cellITraceGroups,
                                                const int& cellgroup, const int& cellelem,
                                             const FieldDataInvMassMatrix_Cell& mmfld)
{
  return SetLO_InteriorTrace_VMSD_BR2_impl<IntegrandInteriorTrace>
  (fcn.cast(),  cellITraceGroups, cellgroup, cellelem, mmfld);
}

}

#endif  // SET_LO_INTERIORTRACE_VMSD_BR2_H
