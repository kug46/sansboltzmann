// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_VMSD_BR2_H
#define JACOBIANINTERIORTRACE_VMSD_BR2_H

// HDG trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "JacobianCell_VMSD_BR2_Element.h"
#include "JacobianInteriorTrace_VMSD_BR2_Element.h"


namespace SANS
{


//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class Surreal, class IntegrandInteriorTrace>
class JacobianInteriorTrace_VMSD_BR2_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_VMSD_BR2_impl<Surreal, IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  typedef JacobianElemCell_VMSDBR2<PhysDim,MatrixQ> JacobianElemCellType;

  // Save off the boundary trace integrand and the residual vectors
  JacobianInteriorTrace_VMSD_BR2_impl( const IntegrandInteriorTrace& fcn,
                                  const std::map<int,std::vector<int>>& cellITraceGroups,
                                  const int& cellgroup, const int& cellelem,
                                  const FieldDataInvMassMatrix_Cell& mmfld,
                                  JacobianElemCellType& mtxElem) :
    fcn_(fcn), cellITraceGroups_(cellITraceGroups),
    cellgroup_(cellgroup), cellelem_(cellelem),
    mmfld_(mmfld), mtxElem_(mtxElem)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type& flds ) const
  {
    //CHECKS NOT APPLICABLE
//    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
//    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
//
//    SANS_ASSERT( mtxElemPDE_q_.m() == qfld.nDOFpossessed() );
//    SANS_ASSERT( mtxElemPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDE_qp_.m() == qfld.nDOFpossessed() );
//    SANS_ASSERT( mtxElemPDE_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDEp_q_.m() == qpfld.nDOFpossessed() );
//    SANS_ASSERT( mtxElemPDEp_q_.n() == qfld.nDOFpossessed() + qpfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDEp_qp_.m() == qpfld.nDOFpossessed() );
//    SANS_ASSERT( mtxElemPDEp_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                           Field<PhysDim,TopoDim,ArrayQ>,
                                           FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type::
                     template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                           Field<PhysDim,TopoDim,ArrayQ>,
                                           FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type::
                     template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename RFieldCellGroupTypeR::template ElementType<> ElementRFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef JacobianElemTrace_VMSD_PDE<PhysDim, MatrixQ> JacobianElemInteriorTraceType;
    typedef JacobianElemTrace_VMSD_LO<PhysDim> JacobianElemInteriorTraceLOType;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<2>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const QFieldCellGroupTypeR& qpfldCellR = get<1>(fldsCellR);
    const RFieldCellGroupTypeR& rfldCellR = get<2>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rfldElemsL( rfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementQFieldClassR qpfldElemR( qpfldCellR.basis() );
    ElementRFieldClassR rfldElemsR( rfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFpL = qpfldElemL.nDOF();

    const int nDOFR = qfldElemR.nDOF();
    const int nDOFpR = qpfldElemR.nDOF();

    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType > integral(quadratureorder);
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceLOType> integralLO(quadratureorder);

    // element jacobian matrices
    JacobianElemTraceSizeVMSD sizeL(nDOFL, nDOFpL);
    JacobianElemTraceSizeVMSD sizeR(nDOFR, nDOFpR);

    JacobianElemInteriorTraceType mtxElemTmpL(sizeL);
    JacobianElemInteriorTraceLOType mtxElemLOL(sizeL);

    JacobianElemInteriorTraceType mtxElemTmpR(sizeR);
    JacobianElemInteriorTraceLOType mtxElemLOR(sizeR);

    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        qpfldCellL.getElement( qpfldElemL, elemL );
        rfldCellL.getElement( rfldElemsL, elemL, canonicalTraceL.trace );

        xfldTrace.getElement( xfldElemTrace, elem );

        mtxElemLOL._qp = 0;

        // lifting operator trace integration for canonical element
        integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL, +1, xfldElemL,
                                            qfldElemL, qpfldElemL, rfldElemsL), xfldElemTrace, mtxElemLOL);

        mtxElemTmpL = 0;
        mtxElemTmpL.r_qp = -mmfld_.getCellGroupGlobal(cellGroupGlobalL)[elemL]*mtxElemLOL._qp;

        // PDE trace integration for canonical element
        integral( fcn_.integrand(xfldElemTrace, canonicalTraceL, +1, xfldElemL, qfldElemL, qpfldElemL, rfldElemsL),
                                      xfldElemTrace, mtxElemTmpL );

        if (fcn_.needsSolutionGradientforSource())
        {
          mtxElem_.PDE_qp += mtxElem_.PDE_LO*mtxElemTmpL.r_qp;
          mtxElem_.PDEp_qp += mtxElem_.PDEp_LO*mtxElemTmpL.r_qp;
        }

//        mtxElem_.PDE_qp += mtxElemTmpL.PDE_LO*mtxElemTmpL.r_qp;
//        mtxElem_.PDEp_qp += mtxElemTmpL.PDEp_LO*mtxElemTmpL.r_qp;

        mtxElem_.PDE_q += mtxElemTmpL.PDE_q;
        mtxElem_.PDE_qp += mtxElemTmpL.PDE_qp;
        mtxElem_.PDEp_q += mtxElemTmpL.PDEp_q;
        mtxElem_.PDEp_qp += mtxElemTmpL.PDEp_qp;

      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        qpfldCellR.getElement( qpfldElemR, elemR );
        rfldCellR.getElement( rfldElemsR, elemR, canonicalTraceR.trace );

        xfldTrace.getElement( xfldElemTrace, elem );

        mtxElemLOR._qp = 0;

        // lifting operator trace integration for canonical element
        integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceR, -1, xfldElemR, qfldElemR, qpfldElemR, rfldElemsR),
                    xfldElemTrace, mtxElemLOR);

        mtxElemTmpR = 0;
        mtxElemTmpR.r_qp = -mmfld_.getCellGroupGlobal(cellGroupGlobalR)[elemR]*mtxElemLOR._qp;

        // PDE trace integration for canonical element
        integral( fcn_.integrand(xfldElemTrace, canonicalTraceR, -1, xfldElemR, qfldElemR, qpfldElemR, rfldElemsR),
                  xfldElemTrace, mtxElemTmpR );

        if (fcn_.needsSolutionGradientforSource())
        {
          mtxElem_.PDE_qp += mtxElem_.PDE_LO*mtxElemTmpR.r_qp;
          mtxElem_.PDEp_qp += mtxElem_.PDEp_LO*mtxElemTmpR.r_qp;
        }

        mtxElem_.PDE_q += mtxElemTmpR.PDE_q;
        mtxElem_.PDE_qp += mtxElemTmpR.PDE_qp;
        mtxElem_.PDEp_q += mtxElemTmpR.PDEp_q;
        mtxElem_.PDEp_qp += mtxElemTmpR.PDEp_qp;

      }
      else
        SANS_DEVELOPER_EXCEPTION("JacobianInteriorTrace_VMSD_BR2_impl::integrate - Invalid configuration!");

    } //elem loop
  }


protected:
  const IntegrandInteriorTrace& fcn_;
  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;
  const FieldDataInvMassMatrix_Cell& mmfld_;

  JacobianElemCellType& mtxElem_; //jacobian of PDE residual wrt q for the central cell
  std::vector<int> traceGroupIndices_;
};


// Factory function

template<class Surreal, class IntegrandITrace, class PhysDim, class MatrixQ>
JacobianInteriorTrace_VMSD_BR2_impl<Surreal, IntegrandITrace>
JacobianInteriorTrace_VMSD_BR2( const IntegrandInteriorTraceType<IntegrandITrace>& fcn,
                           const std::map<int,std::vector<int>>& cellITraceGroups,
                           const int& cellgroup, const int& cellelem,
                           const FieldDataInvMassMatrix_Cell& mmfld,
                           JacobianElemCell_VMSDBR2<PhysDim,MatrixQ>& mtxElem)
{
  return { fcn.cast(), cellITraceGroups, cellgroup, cellelem, mmfld, mtxElem };
}

}


#endif  // JACOBIANINTERIORTRACE_VMSD_BR2_H
