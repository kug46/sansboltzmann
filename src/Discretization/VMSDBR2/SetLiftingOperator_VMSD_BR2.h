// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETLIFTINGOPERATOR_VMSD_BR2
#define SETLIFTINGOPERATOR_VMSD_BR2

// HDG cell integral jacobian functions

#include "SetLO_InteriorTrace_onBoundary_VMSD_BR2.h"
#include "SetLO_InteriorTrace_VMSD_BR2.h"

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/Identity.h"
#include "LinearAlgebra/Transpose.h"

#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/XField_CellToTrace.h"
#include "Field/FieldData/FieldDataMatrixD_Cell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"

#include "Discretization/QuadratureOrder.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateGhostBoundaryTraceGroups.h"
#include "JacobianInteriorTrace_onBoundary_VMSD_BR2.h"
#include "JacobianInteriorTrace_VMSD_BR2.h"



namespace SANS
{

//----------------------------------------------------------------------------//
//  VMSD cell group integral jacobian with static condensation
//  THIS VERSION TAKES INTO ACCOUNT THE BOUNDARY CONDITION STUFF

template<class IntegrandCell, class IntegrandITrace, class XFieldType_, class TopoDim_>
class SetLiftingOperator_VMSD_BR2_impl :
    public GroupIntegralCellType< SetLiftingOperator_VMSD_BR2_impl<IntegrandCell, IntegrandITrace, XFieldType_, TopoDim_> >
{
public:
  typedef typename IntegrandITrace::PhysDim PhysDim;
  typedef typename IntegrandITrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;


  // Save off the boundary trace integrand and the residual vectors
  SetLiftingOperator_VMSD_BR2_impl( const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace, const IntegrandITrace& fcnBTrace,
                         const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace,
                         const XFieldType_& xfld,
                         const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                         const Field<PhysDim, TopoDim_, ArrayQ>& qpfld,
                         const FieldLift<PhysDim, TopoDim_, VectorArrayQ>& rfld,
                         const QuadratureOrder& quadOrder,
                         const FieldDataInvMassMatrix_Cell& mmfld
                         ) :
                           fcnCell_(fcnCell), fcnITrace_(fcnITrace), fcnBTrace_(fcnBTrace),
                           xfldCellToTrace_(xfldCellToTrace),
                           xfld_(xfld), qfld_(qfld), qpfld_(qpfld), rfld_(rfld),
                           quadOrder_(quadOrder),
                           nITraceGroup_( xfld.getXField().nInteriorTraceGroups() ),
                           nBTraceGroup_( xfld.getXField().nBoundaryTraceGroups()),
                           nGTraceGroup_( xfld.getXField().nGhostBoundaryTraceGroups() ),
                           mmfld_(mmfld),
                           comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                   Field<PhysDim,TopoDim,ArrayQ>,
                                                   FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type& flds ) const
  {
//    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

//    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                  Field<PhysDim,TopoDim,ArrayQ>,
                                                  FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                          ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementrFieldClass;
    typedef Element<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;

    typedef JacobianElemCell_VMSDBR2<PhysDim,MatrixQ> JacobianElemCellType;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qpfldCell = get<1>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<2>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );
    ElementrFieldClass rfldElems( rfldCell.basis() );
    ElementRFieldClass RfldElem( rfldCell.basis() );

    // DOFs per element
    const int qDOF = qfldElem.nDOF();
    const int qDOFp = qpfldElem.nDOF();


    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == qpfldCell.nElem() );

    // element jacobian matrices
    JacobianElemCellType mtxElem(qDOF, qDOFp);
    JacobianElemCellType mtxElemTmp(qDOF, qDOFp);

    // loop over elements within group and solve for the auxiliary variables first
    for (int elem = 0; elem < nelem; elem++)
    {

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qpfldCell.getElement( qpfldElem, elem );
      rfldCell.getElement( rfldElems, elem );
//
//      // accumulate the lifting operator DOFs into a single element
//      RfldElem.vectorViewDOF() = 0;
//      for (int trace = 0; trace < Topology::NTrace; trace++)
//        RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

      std::map<int,std::vector<int>> cellITraceGroups;
      std::map<int,std::vector<int>> cellBTraceGroups;
      std::map<int,std::vector<int>> cellGTraceGroups;

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, elem, trace);

        if (traceinfo.type == TraceInfo::Interior)
        {
          //add this traceGroup and traceElem to the set of interior traces attached to this cell
          cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::Boundary)
        {
          cellBTraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::GhostBoundary)
        {
          cellGTraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
      }

      //-----------------------------------------------------------------------------------------

      //Go the the traces of this element and integrate the trace contributions!
      if (nITraceGroup_ > 0)
      {
        std::vector<int> quadOrderITrace;

        for (int i=0; i< nITraceGroup_; i++)
          quadOrderITrace.push_back(quadOrder_.interiorTraceOrders[0]);

        IntegrateInteriorTraceGroups<TopoDim>::integrate(
              SetLO_InteriorTrace_VMSD_BR2(fcnITrace_, cellITraceGroups, cellGroupGlobal, elem, mmfld_),
                                                           xfld_, (qfld_, qpfld_, rfld_),
                                                           quadOrder_.interiorTraceOrders.data(),
                                                           quadOrder_.interiorTraceOrders.size() );
      }

      if (nBTraceGroup_ > 0)
      {
        std::vector<int> quadOrderBTrace;

        for (int i=0; i< nBTraceGroup_; i++)
          quadOrderBTrace.push_back(quadOrder_.interiorTraceOrders[0]);

        IntegrateBoundaryTraceGroups<TopoDim>::integrate(
            SetLO_InteriorTrace_onBoundary_VMSD_BR2(fcnBTrace_, cellBTraceGroups, cellGroupGlobal, elem, mmfld_),
                                               xfld_, (qfld_, qpfld_, rfld_),
                                               quadOrderBTrace.data(), quadOrderBTrace.size());

      }

      //WE NEED TO INTEGRATE ON GHOST TRACES FOR VMSD
      if (nGTraceGroup_ > 0)
      {
        std::vector<int> quadOrderGTrace;

        for (int i=0; i< nGTraceGroup_; i++)
          quadOrderGTrace.push_back(quadOrder_.interiorTraceOrders[0]);

        IntegrateGhostBoundaryTraceGroups<TopoDim>::integrate(
            SetLO_InteriorTrace_onBoundary_VMSD_BR2(fcnBTrace_, cellGTraceGroups, cellGroupGlobal, elem, mmfld_),
                                               xfld_, (qfld_, qpfld_, rfld_),
                                               quadOrderGTrace.data(), quadOrderGTrace.size());
      }

    } //elem

  }


protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const IntegrandITrace& fcnBTrace_;

  const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace_;
  const XFieldType_& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qpfld_;
  const FieldLift<PhysDim, TopoDim_, VectorArrayQ>& rfld_;

  const QuadratureOrder& quadOrder_;
  const int nITraceGroup_;
  const int nBTraceGroup_;
  const int nGTraceGroup_;

  const FieldDataInvMassMatrix_Cell& mmfld_;

  mutable int comm_rank_;
};

// Factory function

template< class IntegrandCell, class IntegrandITrace,
         class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
SetLiftingOperator_VMSD_BR2_impl<IntegrandCell, IntegrandITrace, XFieldType, TopoDim>
SetLiftingOperator_VMSD_BR2( const IntegrandCellType<IntegrandCell>& fcnCell,
                  const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                  const IntegrandInteriorTraceType<IntegrandITrace>& fcnBTrace,
                  const XField_CellToTrace<PhysDim, TopoDim>& xfldCellToTrace,
                  const XFieldType& xfld,
                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                  const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                  const QuadratureOrder& quadOrder,
                  const FieldDataInvMassMatrix_Cell& mmfld )
{
  return { fcnCell.cast(), fcnITrace.cast(), fcnBTrace.cast(),
           xfldCellToTrace, xfld, qfld, qpfld, rfld, quadOrder, mmfld };
}

}

#endif  // SETLIFTINGOPERATOR_VMSD_BR2
