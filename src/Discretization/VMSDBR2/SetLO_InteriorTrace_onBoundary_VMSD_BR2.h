// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SET_LO_INTERIORTRACE_ONBOUNDARY_VMSD_BR2_H
#define SET_LO_INTERIORTRACE_ONBOUNDARY_VMSD_BR2_H

// boundary-trace integral residual functions

#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "tools/Tuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  VMSD boundary-trace integral imposing stabilization on q' in the interior
//  (this does not apply the boundary condition!)

template<class IntegrandBTrace>
class SetLO_InteriorTrace_onBoundary_VMSD_BR2_impl :
    public GroupIntegralBoundaryTraceType< SetLO_InteriorTrace_onBoundary_VMSD_BR2_impl<IntegrandBTrace> >
{
public:
  typedef typename IntegrandBTrace::PhysDim PhysDim;
  typedef typename IntegrandBTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  SetLO_InteriorTrace_onBoundary_VMSD_BR2_impl( const IntegrandBTrace& fcn,
                                  const std::map<int,std::vector<int>>& cellBTraceGroups,
                                  const int& cellgroup, const int& cellelem,
                                  const FieldDataInvMassMatrix_Cell& mmfld) :
                                    fcn_(fcn), cellBTraceGroups_(cellBTraceGroups),
                                    cellgroup_(cellgroup), cellelem_(cellelem), mmfld_(mmfld)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellBTraceGroups_.begin(); it != cellBTraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nBoundaryGroups() const { return traceGroupIndices_.size(); }
  std::size_t boundaryGroup(const int n) const { return traceGroupIndices_[n]; }


//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
              Field<PhysDim,TopoDim,ArrayQ>,
              FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type& flds ) const
  {

    //CHECKS NOT APPLICABLE HERE
//    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
//    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
//    SANS_ASSERT( rsdPDEElemCell_.m() == qfld.nDOFpossessed() );
//    SANS_ASSERT( rsdPDEpElemCell_.m() == qpfld.nDOFpossessed() );
  }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                           Field<PhysDim,TopoDim,ArrayQ>,
                                           FieldLift<PhysDim, TopoDim, VectorArrayQ>>::type::
                                           template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);
    RFieldCellGroupTypeL& rfldCellL = const_cast<RFieldCellGroupTypeL&>(get<2>(fldsCellL));

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
//    const int nIntegrandL = qfldElemL.nDOF();
    const int nIntegrandPL = qpfldElemL.nDOF();

    // trace element integral
//    typedef ArrayQ PDEIntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQ> integralL(quadratureorder, nIntegrandPL);

    // element integrand/residuals
    // element integrand/residuals
    DLA::VectorD< VectorArrayQ > rsdElemL( nIntegrandPL );
    // Provide a vector view of the lifting operator DOFs
    DLA::VectorDView<VectorArrayQ> rL( rfldElemL.vectorViewDOF() );

    const std::vector<int>& traceElemList = cellBTraceGroups_.at(traceGroupGlobal);

    // loop over elements within group
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

//      std::cout << "attempting to get elem " << elem << " in tracegroup " << traceGroupGlobal << "\n";
      const int elemL = xfldTrace.getElementLeft( elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        qpfldCellL.getElement( qpfldElemL, elemL );
        rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

        xfldTrace.getElement( xfldElemTrace, elem );

        for (int n = 0; n < nIntegrandPL; n++) rsdElemL[n] = 0;

        integralL( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL, +1, xfldElemL,
                                    qfldElemL, qpfldElemL, rfldElemL),
                  xfldElemTrace, rsdElemL.data(), nIntegrandPL );

        // Compute the lifting operator DOFs
        rL = -mmfld_.getCellGroupGlobal(cellGroupGlobalL)[elemL]*rsdElemL;

        // Set the lifting operators
        rfldCellL.setElement( rfldElemL, elemL, canonicalTraceL.trace );
      }

    }
  }

protected:
  const IntegrandBTrace& fcn_;

  const std::map<int,std::vector<int>>& cellBTraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;

  const FieldDataInvMassMatrix_Cell& mmfld_;

  std::vector<int> traceGroupIndices_;
};

// Factory function

template<class IntegrandBTrace>
SetLO_InteriorTrace_onBoundary_VMSD_BR2_impl<IntegrandBTrace>
SetLO_InteriorTrace_onBoundary_VMSD_BR2( const IntegrandInteriorTraceType<IntegrandBTrace>& fcn,
                           const std::map<int,std::vector<int>>& cellBTraceGroups,
                           const int& cellgroup, const int& cellelem,
                           const FieldDataInvMassMatrix_Cell& mmfld)
{
//  typedef typename Scalar<ArrayQ>::type T;
  return SetLO_InteriorTrace_onBoundary_VMSD_BR2_impl<IntegrandBTrace>(fcn.cast(), cellBTraceGroups, cellgroup, cellelem, mmfld);
}

}

#endif  // SET_LO_INTERIORTRACE_ONBOUNDARY_VMSD_BR2_H
