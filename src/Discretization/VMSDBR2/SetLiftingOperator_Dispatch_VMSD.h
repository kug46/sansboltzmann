// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETBOUNDARYTRACELIFTINGOPERATOR_DISPATCH_VMSD
#define SETBOUNDARYTRACELIFTINGOPERATOR_DISPATCH_VMSD

// boundary-trace integral residual functions

#include "SetFieldBoundaryTrace_VMSD_LiftingOperator.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_BoundaryCell.h"

namespace SANS
{
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without field trace
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
class SetBoundaryTraceLiftingOperator_Dispatch_VMSD_impl
{
public:
  SetBoundaryTraceLiftingOperator_Dispatch_VMSD_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
      const FieldDataInvMassMatrix_Cell& mmfld,
      const int* quadratureorder, int ngroup )
    : xfld_(xfld), qfld_(qfld), rbfld_(rbfld), mmfld_(mmfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_BoundaryCell<TopoDim>::integrate(
        SetFieldBoundaryTrace_VMSD_LiftingOperator(fcn, mmfld_),
        xfld_, qfld_, rbfld_, quadratureorder_, ngroup_);
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
SetBoundaryTraceLiftingOperator_Dispatch_VMSD_impl<XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>
SetBoundaryTraceLiftingOperator_Dispatch_VMSD(const XFieldType& xfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                               const FieldLift_DG_BoundaryTrace<PhysDim, TopoDim, VectorArrayQ>& rbfld,
                                               const FieldDataInvMassMatrix_Cell& mmfld,
                                               const int* quadratureorder, int ngroup )
{
  return {xfld, qfld, rbfld, mmfld, quadratureorder, ngroup};
}

}

#endif //SETBOUNDARYTRACELIFTINGOPERATOR_DISPATCH_VMSD
