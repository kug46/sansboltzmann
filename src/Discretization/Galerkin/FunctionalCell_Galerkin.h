// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONALCELL_GALERKIN_H
#define FUNCTIONALCELL_GALERKIN_H

// Cell integral functional
#include <ostream>
#include "tools/Tuple.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/FieldLift.h"

#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementLift.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include "MPI/serialize_DenseLinAlg_MatrixS.h"
#endif

#include "IntegrandCell_Galerkin_Output_Stabilized.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group functional
//

template<class IntegrandCell, class T>
class FunctionalCell_Galerkin_impl :
    public GroupIntegralCellType< FunctionalCell_Galerkin_impl<IntegrandCell, T> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<T> ArrayQ;
  typedef typename IntegrandCell::template ArrayJ<T> ArrayJ;

  // Save off the cell integrand and the residual vector
  FunctionalCell_Galerkin_impl( const IntegrandCell& fcn,
                                ArrayJ& functional ) :
                                fcn_(fcn), functional_(functional), comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    comm_rank_ = qfld.comm()->rank();
    comm_ = qfld.comm();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
             const int quadratureorder );

protected:
  const IntegrandCell& fcn_;
  ArrayJ& functional_;
  mutable int comm_rank_;
  mutable std::shared_ptr<mpi::communicator> comm_;
};

template<class IntegrandCell, class T>
template <class Topology, class TopoDim, class XFieldType>
void
FunctionalCell_Galerkin_impl<IntegrandCell, T>::
integrate( const int cellGroupGlobal,
           const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
           const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
           const int quadratureorder )
{
  typedef typename XFieldType                                ::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim, TopoDim, ArrayQ          >::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // element integral
  ElementIntegral<TopoDim, Topology, ArrayJ> integral(quadratureorder);

  // just to make sure things are consistent
  SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );

  // loop over elements within group
  const int nelem = xfldCell.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    // only need to perform the volume integral on processors that possess the element
    if ( qfldElem.rank() != comm_rank_ ) continue;

    ArrayJ result = 0;

    // cell integration for canonical element
    integral( fcn_.integrand(xfldElem, qfldElem), get<-1>(xfldElem), result );

    // sum up the functional
    functional_ += result;

    //std::cout << "elem integral = " << result << std::endl;
  }

  // sum accross processors once the last group has been visitied
#ifdef SANS_MPI
  if ( (int)fcn_.cellGroup(fcn_.nCellGroups()-1) == cellGroupGlobal )
    functional_ = boost::mpi::all_reduce( *comm_, functional_, std::plus<ArrayJ>() );
#endif
}


// Factory function

template<class IntegrandCell, class ArrayJ>
FunctionalCell_Galerkin_impl<IntegrandCell,typename Scalar<ArrayJ>::type>
FunctionalCell_Galerkin( const IntegrandCellType<IntegrandCell>& fcn,
                         ArrayJ& functional )
{
  typedef typename Scalar<ArrayJ>::type T;
  static_assert( std::is_same<typename IntegrandCell::template ArrayJ<T>, ArrayJ>::value, "These should be the same");
  return {fcn.cast(), functional};
}

}

#endif  // FUNCTIONALCELL_GALERKIN_H
