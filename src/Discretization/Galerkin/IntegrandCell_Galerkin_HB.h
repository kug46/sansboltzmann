// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_HB_H
#define INTEGRANDCELL_GALERKIN_HB_H

// cell integrand operators: Harmonic Balance

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "Field/Element/Element.h"
#include "Field/Element/ElementSequence.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: BDF
// Cell-group-level integrand object
//
// integrandPDE = phi*sum( w_j * U_j)/dt
//
// where
//   phi              basis function
//   U_j(x,y)         conservative solution vector at time step j = n, n-1, ...
//   w_j              backwards difference weight for time step j = n, n-1, ...

template <class PDE>
class IntegrandCell_Galerkin_HB : public IntegrandCellType< IntegrandCell_Galerkin_HB<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobian

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin_HB( const PDE& pde, const std::vector<int>& CellGroups,
                             const Real& period, const int& ntimes)
  : pde_(pde), cellGroups_(CellGroups), period_(period), ntimes_(ntimes)
  {
    // No point in using BDF with a PDE that does not have a time term (i.e. conservative flux)...
    SANS_ASSERT(pde_.hasFluxAdvectiveTime());
  }

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  // Element-level integrand object
  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementSequence<ArrayQ<T>, TopoDim, Topology> ElementQFieldVecType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef ArrayQ<T> IntegrandType;

    BasisWeighted(  const PDE& pde,
                    const Real& period,
                    const int& ntimes,
                    const ElementParam& paramfldElem,
//                    const ElementQFieldType& qfldElem1,
                    const ElementQFieldType& qfldElem2,
                    const int& t1,
                    const int& t2) :
                      pde_(pde), period_(period), ntimes_(ntimes), weights_(0),
                      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                      paramfldElem_( paramfldElem ),
//                      qfldElem1_(qfldElem1),
                      qfldElem2_(qfldElem2),
                      t1_(t1), t2_(t2),
                      nDOF_( qfldElem2.nDOF() ),
                      phi_( new Real[nDOF_] )
    {
      if (t1_ != t2_)
        weights_ = (PI / period) * ( pow(-1.0, (Real)(t1_-t2_) ) / sin( PI*(Real)(t1_-t2_)/(Real)(ntimes_) ) );
      else
        weights_ = 0;
//      for (int j=0; j<ntimes; j++)
//        if (mytime_ != j)
//          weights_[j] = (PI / period) * ( pow(-1, mytime_-j) / sin( PI*(mytime_-j)/ntimes ) );
    }

    BasisWeighted( BasisWeighted&& bw ) :
                      pde_(bw.pde_), period_(bw.period_), ntimes_(bw.ntimes_), weights_(bw.weights_),
                      xfldElem_(bw.xfldElem_),
                      paramfldElem_( bw.paramfldElem_ ),
                      qfldElem2_(bw.qfldElem2_),
                      t1_(bw.t1_), t2_(bw.t2_),
                      nDOF_( bw.nDOF_ ),
                      phi_( bw.phi_ )
    {
      bw.phi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    void operator()( const QuadPointType& sRef, IntegrandType integrand[], int neqn ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

  protected:
    template<class Tq, class Ti>
    void weightedIntegrand( const ParamT& param,
//                            const ArrayQ<Tq> & u1,
                            const ArrayQ<Tq> & u2,
                            ArrayQ<Ti> integrand[], int neqn ) const;
    //Note that these members have the same counterparts in the enclosing class but need to be defined here,
    //since, as of now, there is no straightforward to access those members of the enclosing class.
    const PDE& pde_;
    const Real& period_;
    const int& ntimes_;
//    mutable DLA::VectorD<Real> weights_; // HB weights: weights_[j] corresponds weighting between t_{mytime} and t_{j}
    mutable Real weights_;

    const ElementXFieldType& xfldElem_;
//    const ElementQFieldVecType& qfldElems_;
//    const ElementQFieldType& qfldElem1_;
    const ElementParam& paramfldElem_;
    const ElementQFieldType& qfldElem2_;

    const int& t1_;
    const int& t2_;

    const int nDOF_;
    mutable Real *phi_;
  };

  // Element-level integrand object
  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem2, const int& t1, const int& t2) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, period_, ntimes_, paramfldElem, qfldElem2, t1, t2);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
  const Real& period_;
  const int& ntimes_;
};


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_HB<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
      pde_.hasFluxViscous() ||
      pde_.hasSource() ||
      pde_.hasForcingFunction() );
}

// Cell integrand
//
// integrand = phi*sum(w_n*U_n)/dt
//
// where
//   phi                basis function
//   U_n(x,y)           conservative solution vector at timestep n
//   w_n                BDF weight for timestep n
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_HB<PDE>::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()(const QuadPointType& sRef, IntegrandType integrand[], int neqn) const
{

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)
  ArrayQ<Real> q2;

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // evaluate basis fcn
  qfldElem2_.evalBasis( sRef, phi_, nDOF_ );
  qfldElem2_.evalFromBasis( phi_, nDOF_, q2 );

  weightedIntegrand( param, q2, integrand, neqn);
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_HB<PDE>::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

    SANS_ASSERT(mtxPDEElem.m() == nDOF_);
    SANS_ASSERT(mtxPDEElem.n() == nDOF_);

    // number of simultaneous derivatives per functor call
    static const int nDeriv = SurrealClass::N;
    static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

//    const int ntimes = qfldElemPast_.nElem();  // number of previous time instances in BDF

    ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

    ArrayQ<SurrealClass> qSurreal = 0; // solution
    ArrayQ<Real> q2;                // previous solution

//    ArrayQ<Real> u2;

    MatrixQ<Real> PDE_q = 0; // temporary storage

    // Elemental parameters
    paramfldElem_.eval( sRef, param );

    // basis value, gradient
    qfldElem2_.evalBasis( sRef, phi_, nDOF_ );

    // evaluate solution at all time steps
//    qfldElem1_.evalFromBasis( phi_, nDOF_, q1);
    qfldElem2_.evalFromBasis( phi_, nDOF_, q2);
    qSurreal = q2;

    //pde_.masterState(param, q2, u2);

    // element integrand/residual
    DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;

      integrandSurreal = 0;

      weightedIntegrand( param, qSurreal, integrandSurreal.data(), integrandSurreal.size());

      // accumulate derivatives into element jacobian

      slot = 0;
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOF_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOF_; j++)
            mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
        }
      }
      slot += PDE::N;
    } // nchunk


}

// Cell integrand
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Tq, class Ti>
void
IntegrandCell_Galerkin_HB<PDE>::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
weightedIntegrand( const ParamT& param,
//                   const ArrayQ<Tq>& q1,
                   const ArrayQ<Tq>& q2,
                   ArrayQ<Ti> integrand[], int neqn ) const
{

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // evaluate solution at different timesteps
  ArrayQ<Ti> dudt = 0;

  ArrayQ<Tq> u2 = 0;
  pde_.fluxAdvectiveTime(param, q2, u2);

  if (t1_ != t2_)
    dudt += weights_ * u2;

  for (int k=0; k < neqn; k++)
    integrand[k] += phi_[k]*dudt;

}

}

#endif  // INTEGRANDCELL_GALERKIN_HB_H
