// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_PROJECT_H
#define ALGEBRAICEQUATIONSET_PROJECT_H

#include <type_traits>
#include <iomanip> // std::setprecision

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>
#include "tools/plus_std_vector.h"
#endif

#include "tools/SANSnumerics.h"
#include "tools/KahanSum.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Field/Field.h"
#include "Field/output_Tecplot.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"
#include "Discretization/ResidualNormType.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Algebraic equation set for L^2 projection onto a field
//
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
class AlgebraicEquationSet_Project : public AlgebraicEquationSetTraits< typename IntegrandCellClass::template MatrixQ<Real>,
                                                                        typename IntegrandCellClass::template ArrayQ<Real>,
                                                                        Traits>::AlgebraicEquationSetBaseClass
{
public:
  typedef typename IntegrandCellClass::PhysDim PhysDim;

  typedef typename IntegrandCellClass::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCellClass::template MatrixQ<Real> MatrixQ;
  static const int N = DLA::VectorSize<ArrayQ>::M;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  template<class ArrayQT>
  using SystemVectorTemplate = typename TraitsType::template SystemVectorTemplate<ArrayQT>;

  // Indexes to order the equations and the solution vectors
  static const int iProj = 0;
  static const int iq = 0;

  AlgebraicEquationSet_Project(const ParamFieldType& paramfld,
                               Field<PhysDim, TopoDim, ArrayQ>& qfld,
                               const IntegrandCellClass& fcnCell,
                               const QuadratureOrder& quadratureOrder,
                               const std::array<Real,1> tol,
                               const ResidualNormType resNormType = ResidualNorm_L2) :
    fcnCell_(fcnCell),
    paramfld_(paramfld),
    qfld_(qfld),
    quadratureOrder_(quadratureOrder),
    quadratureOrderMin_(get<-1>(paramfld), 0),
    tol_(tol),
    resNormType_(resNormType) {}

  virtual ~AlgebraicEquationSet_Project() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override { this->template jacobian<        SystemMatrixView&>(mtx, quadratureOrder_ );    }
  virtual void jacobian(SystemNonZeroPatternView& nz) override { this->template jacobian<SystemNonZeroPatternView&>( nz, quadratureOrderMin_ ); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override { jacobian(Transpose(mtxT), quadratureOrder_ );    }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override { jacobian(Transpose( nzT), quadratureOrderMin_ ); }

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real> > residualNorm( const SystemVectorView& rsd ) const override;

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const;

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real> >& rsdNorm) const override;

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override;

  //Check whether residual decreased, for purposes of linesearch
  virtual bool decreasedResidual(const std::vector<std::vector<Real> >& rsdNormOld,
                                 const std::vector<std::vector<Real> >& rsdNormNew) const override;

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real> >& rsdNorm,
                                            std::ostream& os = std::cout) const override;

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // update fraction needed for physically valid state
  virtual Real updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const override
  {
    return 1.0; // full Newton update is used by default
  }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override { return true; } // assume the projection maintains state validity

  // Returns the size of the residual norm outer vector
  virtual int nResidNorm() const override { return 1; }

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    return {qfld_.continuousGlobalMap()};
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return qfld_.comm(); }

  virtual void syncDOFs_MPI() override
  {
    qfld_.syncDOFs_MPI_Cached();
  }

  //Update the localized linesearch parameter info (used for debugging purposes)
  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented...");
    return true;
  }

  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented...");
  }


  // Are we having machine precision issues - assume no issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    return false;
  }

protected:
  template<class SparseMatrixType>
  void jacobian( SparseMatrixType mtx, const QuadratureOrder& quadratureOrder );

  const IntegrandCellClass& fcnCell_;

  const ParamFieldType& paramfld_;
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const QuadratureOrder quadratureOrder_;
  const QuadratureOrder quadratureOrderMin_;

  const std::array<Real,1> tol_;
  const ResidualNormType resNormType_;
};


template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
void
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
residual(SystemVectorView& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd(iProj)),
                                           paramfld_, qfld_, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );
}


template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
template<class SparseMatrixType>
void
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder)
{
  SANS_ASSERT(jac.m() == 1);
  SANS_ASSERT(jac.n() == 1);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacProj_q  = jac(iProj,iq);

  IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin(fcnCell_, jacProj_q),
                                           paramfld_, qfld_, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
std::vector<std::vector<Real> >
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
residualNorm( const SystemVectorView& rsd ) const
{
  const int nDOF = qfld_.nDOF();

  std::vector<KahanSum<Real> > rsdNormKahan(N, 0);
  std::vector<std::vector<Real> > rsdNorm(nResidNorm(), std::vector<Real>(N, 0));

  //compute residual norm
  if (resNormType_ == ResidualNorm_L2)
  {
    for (int i = 0; i < nDOF; i++)
      for (int n = 0; n < N; n++)
        rsdNormKahan[n] += pow(DLA::index(rsd[iProj][i],n),2);

    for (int n = 0; n < N; n++)
      rsdNorm[iProj][n] = (Real)rsdNormKahan[n];

#ifdef SANS_MPI
    rsdNorm[iProj] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iProj], std::plus<std::vector<Real> >());
#endif

    for (int n = 0; n < N; n++)
      rsdNorm[iProj][n] = sqrt(rsdNorm[iProj][n]);
  }
  else
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Project::residualNorm - Unknown residual norm type!");

  return rsdNorm;
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
void
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const
{
  titles = {"Projection : "}; idx = {iProj};
}

//Convergence check of the residual
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
inline bool
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
convergedResidual(const std::vector<std::vector<Real> >& rsdNorm) const
{
  SANS_ASSERT( tol_.size() == rsdNorm.size() );

  // is every component of rsdNorm converged?
  bool converged = true;
  for (std::size_t iEq = 0; iEq < rsdNorm.size() && converged; ++iEq)
    for (std::size_t iMon = 0; iMon < rsdNorm[iEq].size() && converged; ++iMon)
     if ( rsdNorm[iEq][iMon] > tol_[iEq] )
     {
       converged = false;
#if 0 // The printout is suppressed unless it is needed for debugging
       std::cout<< "rsdNorm[" << iEq << "][" << iMon << "] = " << rsdNorm[iEq][iMon] << std::endl;
#endif
     }
  return converged;
}

//Convergence check of the residual
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
inline bool
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
convergedResidual(const std::vector<std::vector<Real> >& rsdNorm, int iEq, int iMon) const
{
  SANS_ASSERT( tol_.size() == rsdNorm.size() );

  // is every component of rsdNorm converged?
  bool converged = true;
  if ( rsdNorm[iEq][iMon] > tol_[iEq] )
  {
   converged = false;
#if 0 // The printout is suppressed unless it is needed for debugging
   std::cout<< "rsdNorm[" << iEq << "][" << iMon << "] = " << rsdNorm[iEq][iMon] << std::endl;
#endif
  }
  return converged;
}

// is the residual norm decreased?
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
inline bool
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
decreasedResidual(const std::vector<std::vector<Real> >& rsdNormOld,
                  const std::vector<std::vector<Real> >& rsdNormNew) const
{
  SANS_ASSERT( tol_.size() == rsdNormNew.size() );

  //is the residual actually decreased or converged?
  bool decreased = true;
  for (std::size_t iEq = 0; iEq < rsdNormNew.size() && decreased; ++iEq)
    for (std::size_t iMon = 0; iMon < rsdNormNew[iEq].size() && decreased; ++iMon)
      if (rsdNormOld[iEq][iMon] < rsdNormNew[iEq][iMon] && rsdNormOld[iEq][iMon] > tol_[iEq])
        decreased = false;

  return decreased;
}

//prints out a residual that could not be decreased and the convergence tolerances
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
inline void
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
printDecreaseResidualFailure(const std::vector<std::vector<Real> >& rsdNorm, std::ostream& os) const
{
  std::vector<std::string> titles;
  std::vector<int> idx;

  residualInfo(titles, idx);

  SANS_ASSERT( titles.size() == idx.size() );
  SANS_ASSERT( titles.size() == tol_.size() );

  os << std::scientific;
  for ( std::size_t ir = 0; ir < idx.size(); ir++)
  {
    os << titles[ir];
    for (std::size_t i = 0; i < rsdNorm[idx[ir]].size(); i++)
      os << std::setprecision(12) << rsdNorm[idx[ir]][i] << " ";
    os << "> " << std::setprecision(3) << tol_[idx[ir]];
    os << std::endl;
  }
}
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
void
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOF = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOF == q[iq].m() );
  for (int k = 0; k < nDOF; k++)
    qfld_.DOF(k) = q[iq][k];
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
void
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOF = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOF == q[iq].m() );
  for (int k = 0; k < nDOF; k++)
    q[iq][k] = qfld_.DOF(k);
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
typename AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::VectorSizeClass
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
vectorEqSize() const
{
  static_assert(iProj == 0,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFpos = qfld_.nDOFpossessed();

  return {nDOFpos};
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
typename AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::VectorSizeClass
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
vectorStateSize() const
{
  static_assert(iProj == 0,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOF = qfld_.nDOFpossessed() + qfld_.nDOFghost();

  return {nDOF};
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
typename AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::MatrixSizeClass
AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>::
matrixSize() const
{
  static_assert(iProj == 0,"");
  static_assert(iq == 0,"");

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFpos = qfld_.nDOFpossessed();

  const int nDOF = qfld_.nDOFpossessed() + qfld_.nDOFghost();

  return {{ {nDOFpos, nDOF}  }};
}

} //namespace SANS

#endif //AlgebraicEquationSet_Project_H
