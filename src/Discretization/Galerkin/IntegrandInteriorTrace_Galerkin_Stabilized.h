// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDINTERIORTRACE_GALERKIN_STABILIZED_H
#define INTEGRANDINTERIORTRACE_GALERKIN_STABILIZED_H

// interior trace integrand operator: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "JacobianInteriorTrace_Galerkin_Element.h"

#include "Discretization/Galerkin/Stabilization_Galerkin.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// interior trace integrand: Galerkin
//
// integrandL = + [[phiL]].{F}
// integrandR =   [[phiR]].{F}
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective, viscous)
//
template <class PDE_>
class IntegrandInteriorTrace_Galerkin_Stabilized : public IntegrandInteriorTraceType<IntegrandInteriorTrace_Galerkin_Stabilized<PDE_> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;

  template <class T>
  using TensorMatrixQ = typename PDE::template TensorMatrixQ<T>;    // diffusion matrix

  template <class T>
  using TensorArrayQ = typename DLA::MatrixS<D, D, ArrayQ<T> >;

  template <class T>
  using TensorSymArrayQ = typename PDE::template TensorSymArrayQ<T>;

  typedef typename PDE::VectorX VectorX;
  typedef DLA::MatrixSymS<PhysDim::D, Real> TensorSymX;

  // cppcheck-suppress noExplicitConstructor
  IntegrandInteriorTrace_Galerkin_Stabilized(const PDE& pde,
                                  const std::vector<int>& InteriorTraceGroups, const StabilizationMatrix& tau) :
    pde_(pde),
    interiorTraceGroups_(InteriorTraceGroups),
    tau_(tau) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                                        class ElementParamL, class ElementParamR>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_Galerkin<MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldL& qfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldR& qfldElemR,
                   const StabilizationMatrix& tau ) :
      pde_(pde),
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), paramfldElemL_( paramfldElemL ),
      xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), paramfldElemR_( paramfldElemR ),
      nDOFL_(qfldElemL_.nDOF()),
      nDOFR_(qfldElemR_.nDOF()),
      phiL_( new Real[nDOFL_] ),
      phiR_( new Real[nDOFR_] ),
      gradphiL_( new VectorX[nDOFL_] ),
      gradphiR_( new VectorX[nDOFR_] ),
      hessphiL_( new TensorSymX[nDOFL_] ),
      hessphiR_( new TensorSymX[nDOFR_] ),
      lengthL_(xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant()),
      lengthR_(xfldElemR_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant()),
      orderL_(qfldElemL_.order()), orderR_(qfldElemL_.order()),
      tau_(tau) {}

    ~BasisWeighted()
    {
      delete [] phiL_;
      delete [] phiR_;

      delete [] gradphiL_;
      delete [] gradphiR_;

      delete [] hessphiL_;
      delete [] hessphiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int neqnL,
                                                     ArrayQ<Ti> integrandR[], const int neqnR ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemR ) const;

  protected:
    template<class Tq, class Tg, class Th, class Tpq, class Tt, class Ti>
    void weightedIntegrand( const VectorX& nL,
                            const ParamTL& paramL, const ParamTR& paramR,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL, const TensorSymArrayQ<Th>& hessqL,
                            const StabilizationMatrix::TauInverse<PDE::N, Tpq, Tt>& tauL,
                            const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR, const TensorSymArrayQ<Th>& hessqR,
                            const StabilizationMatrix::TauInverse<PDE::N, Tpq, Tt>& tauR,
                            ArrayQ<Ti> integrandL[], const int neqnL,
                            ArrayQ<Ti> integrandR[], const int neqnR,
                            bool gradientOnly = false ) const;

    const PDE& pde_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
    mutable VectorX *gradphiL_;
    mutable VectorX *gradphiR_;
    mutable TensorSymX *hessphiL_;
    mutable TensorSymX *hessphiR_;
    const Real lengthL_;
    const Real lengthR_;
    const int orderL_; const int orderR_;
    const StabilizationMatrix& tau_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL   > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR   > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef Element<Real, TopoDimCell, TopologyL> ElementEFieldL;
    typedef Element<Real, TopoDimCell, TopologyR> ElementEFieldR;

    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    FieldWeighted( const PDE& pde,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter
                   const ElementQFieldL& qfldElemL,
                   const ElementQFieldL& wfldElemL,
                   const ElementEFieldL& efldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldR& qfldElemR,
                   const ElementQFieldR& wfldElemR,
                   const ElementEFieldR& efldElemR ) :
                   pde_(pde),
                   xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                   xfldElemL_(get<-1>(paramfldElemL)),
                   qfldElemL_(qfldElemL), wfldElemL_(wfldElemL), efldElemL_(efldElemL),
                   paramfldElemL_( paramfldElemL ),
                   xfldElemR_(get<-1>(paramfldElemR)),
                   qfldElemR_(qfldElemR), wfldElemR_(wfldElemR), efldElemR_(efldElemR),
                   paramfldElemR_( paramfldElemR ),
                   nDOFL_(qfldElemL_.nDOF()), nDOFR_(qfldElemR_.nDOF()),
                   nPhiL_(efldElemL_.nDOF()), nPhiR_(efldElemR_.nDOF()),
                   phiL_( new Real[nPhiL_] ), phiR_( new Real[nPhiR_] ),
                   gradphiL_( new VectorX[nPhiL_] ), gradphiR_( new VectorX[nPhiR_] ),
                   weightL_( new ArrayQ<T>[nPhiL_] ), weightR_( new ArrayQ<T>[nPhiR_] ),
                   gradWeightL_( new VectorArrayQ<T>[nPhiL_] ), gradWeightR_( new VectorArrayQ<T>[nPhiR_] )

    {
    }

    ~FieldWeighted()
    {
      delete [] phiL_;
      delete [] phiR_;
      delete [] gradphiL_;
      delete [] gradphiR_;
      delete [] weightL_;
      delete [] weightR_;
      delete [] gradWeightL_;
      delete [] gradWeightR_;
    }


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, Ti integrandL[], const int nphiL,
      Ti integrandR[], const int nphiR ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementQFieldL& wfldElemL_;
    const ElementEFieldL& efldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementQFieldR& wfldElemR_;
    const ElementEFieldR& efldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    const int nPhiL_, nPhiR_;
    mutable Real *phiL_, *phiR_;
    mutable VectorX *gradphiL_, *gradphiR_;
    mutable ArrayQ<T> *weightL_, *weightR_;
    mutable VectorArrayQ<T> *gradWeightL_, *gradWeightR_;
  };


  //Factory function that returns the basis weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,      TopologyR,
                                  ElementParamL, ElementParamR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& qfldElemR) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL,     TopologyR,
                                          ElementParamL, ElementParamR>(pde_,
                                                                        xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                                                        paramfldElemL, qfldElemL,
                                                                        paramfldElemR, qfldElemR, tau_);
  }

  //Factory function that returns the field weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell, TopologyL,     TopologyR,
                                ElementParamL, ElementParamR  >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& wfldElemL,
            const Element<Real, TopoDimCell, TopologyL>& efldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& qfldElemR,
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& wfldElemR,
            const Element<Real, TopoDimCell, TopologyR>& efldElemR) const
  {
    return FieldWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL,     TopologyR,
                                          ElementParamL, ElementParamR>(pde_,
                                                                        xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                                                        paramfldElemL, qfldElemL, wfldElemL, efldElemL,
                                                                        paramfldElemR, qfldElemR, wfldElemR, efldElemR);
  }

protected:
  const PDE& pde_;
  const std::vector<int> interiorTraceGroups_;
  const StabilizationMatrix& tau_;
};


template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR >
bool
IntegrandInteriorTrace_Galerkin_Stabilized<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}

template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
bool
IntegrandInteriorTrace_Galerkin_Stabilized<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}


template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR>
template <class Ti>
void
IntegrandInteriorTrace_Galerkin_Stabilized<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], const int neqnL,
                                                 ArrayQ<Ti> integrandR[], const int neqnR ) const
{
  typedef typename Scalar<typename promote_Surreal<ParamTL, Real>::type>::type Tpq;

  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients
  TensorSymArrayQ<T> hessqL, hessqR;  // solution hessian

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );


  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
  qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

  if (pde_.hasFluxViscous())
  {
    if ( orderL_ > 1)
    {
      xfldElemL_.evalBasisHessian( sRefL, qfldElemL_, hessphiL_, nDOFL_ );
      qfldElemL_.evalFromBasis( hessphiL_, nDOFL_, hessqL );
    }
    if (orderR_ > 1)
    {
      xfldElemR_.evalBasisHessian( sRefR, qfldElemR_, hessphiR_, nDOFR_ );
      qfldElemR_.evalFromBasis( hessphiR_, nDOFR_, hessqR );
    }
  }


  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // Compute and factorize the tau matrix (doesn't matter here)
  TensorMatrixQ<Real> KL = 0, KR = 0;
  if (pde_.hasFluxViscous())
  {
    pde_.diffusionViscous( paramL, qL, gradqL, KL );
    pde_.diffusionViscous( paramR, qR, gradqR, KR );
  }

  StabilizationMatrix::TauInverse<PDE::N, Tpq, Real> tauL
    = tau_.template factorize<Tpq, Real, Real, Real>(pde_, paramL, qL, KL, gradphiL_, orderL_, nDOFL_, nDOFL_);

  StabilizationMatrix::TauInverse<PDE::N, Tpq, Real> tauR
    = tau_.template factorize<Tpq, Real, Real, Real>(pde_, paramR, qR, KR, gradphiR_, orderR_, nDOFR_, nDOFR_);

  // compute the integrand
  weightedIntegrand( nL,
                     paramL, paramR,
                     qL, gradqL, hessqL, tauL,
                     qR, gradqR, hessqR, tauR,
                     integrandL, neqnL,
                     integrandR, neqnR, true );
}


//---------------------------------------------------------------------------//
template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_Galerkin_Stabilized<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients
  TensorSymArrayQ<T> hessqL, hessqR;

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
  qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

  if (pde_.hasFluxViscous())
  {
    if ( orderL_ > 1)
    {
      xfldElemL_.evalBasisHessian( sRefL, qfldElemL_, hessphiL_, nDOFL_ );
      qfldElemL_.evalFromBasis( hessphiL_, nDOFL_, hessqL );
    }
    if (orderR_ > 1)
    {
      xfldElemR_.evalBasisHessian( sRefR, qfldElemR_, hessphiR_, nDOFR_ );
      qfldElemR_.evalFromBasis( hessphiR_, nDOFR_, hessqR );
    }
  }


  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0, PDE_hessq = 0; // temporary storage
  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL, qSurrealR;                // solution
  VectorArrayQ<SurrealClass> gradqSurrealL, gradqSurrealR;  // gradient
  TensorSymArrayQ<SurrealClass> hessqSurrealL, hessqSurrealR;

  qSurrealL = qL; gradqSurrealL = gradqL; hessqSurrealL = hessqL;
  qSurrealR = qR; gradqSurrealR = gradqR; hessqSurrealR = hessqR;

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandRSurreal( nDOFR_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // Compute and factorize the tau matrix
    TensorMatrixQ<SurrealClass> KL = 0, KR = 0;

    if (pde_.hasFluxViscous())
    {
      pde_.diffusionViscous( paramL, qSurrealL, gradqL, KL );
      pde_.diffusionViscous( paramR, qSurrealR, gradqR, KR );
    }

    StabilizationMatrix::TauInverse<PDE::N, SurrealClass, SurrealClass> tauL
      = tau_.template factorize<SurrealClass, SurrealClass, SurrealClass, SurrealClass>
        (pde_, paramL, qSurrealL, KL, gradphiL_, orderL_, nDOFL_, nDOFL_);

    StabilizationMatrix::TauInverse<PDE::N, SurrealClass, SurrealClass> tauR
      = tau_.template factorize<SurrealClass, SurrealClass, SurrealClass, SurrealClass>
        (pde_, paramR, qSurrealR, KR, gradphiR_, orderR_, nDOFR_, nDOFR_);

    // compute the integrand
    weightedIntegrand( nL,
                       paramL, paramR,
                       qSurrealL, gradqL, hessqL, tauL,
                       qSurrealR, gradqR, hessqR, tauR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemR.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemL.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemR.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk


  // Compute and factorize the tau matrix
  TensorMatrixQ<Real> KL = 0, KR = 0;

  if (pde_.hasFluxViscous())
  {
    pde_.diffusionViscous( paramL, qL, gradqL, KL );
    pde_.diffusionViscous( paramR, qR, gradqR, KR );
  }

  StabilizationMatrix::TauInverse<PDE::N, Real, Real> tauL
    = tau_.template factorize<Real, Real, Real, Real>
      (pde_, paramL, qL, KL, gradphiL_, orderL_, nDOFL_, nDOFL_);

  StabilizationMatrix::TauInverse<PDE::N, Real, Real> tauR
    = tau_.template factorize<Real, Real, Real, Real>
      (pde_, paramR, qR, KR, gradphiR_, orderR_, nDOFR_, nDOFR_);

  // loop over derivative chunks to computes derivatives w.r.t gradq

  for (int nchunk = 0; nchunk < PhysDim::D*2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL, paramR,
                       qL, gradqSurrealL, hessqL, tauL,
                       qR, gradqSurrealR, hessqR, tauR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_, true );

    // accumulate derivatives into element jacobian
    slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOFL_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOFL_; j++)
            mtxElemL.PDE_qL(i,j) += dJ*gradphiL_[j][d]*PDE_gradq;
        }

        for (int i = 0; i < nDOFR_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOFL_; j++)
            mtxElemR.PDE_qL(i,j) += dJ*gradphiL_[j][d]*PDE_gradq;
        }
      }
      slot += PDE::N;
    }

    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOFL_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOFR_; j++)
            mtxElemL.PDE_qR(i,j) += dJ*gradphiR_[j][d]*PDE_gradq;
        }

        for (int i = 0; i < nDOFR_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOFR_; j++)
            mtxElemR.PDE_qR(i,j) += dJ*gradphiR_[j][d]*PDE_gradq;
        }
      }
      slot += PDE::N;
    }
  } // nchunk



  if (pde_.hasFluxViscous() && orderL_ > 1  && orderR_ > 1)
  {
    const int nHess = PhysDim::D*(PhysDim::D + 1)/2;

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nHess*PDE::N*2; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurrealL(ii,jj), n).deriv(slot + n - nchunk) = 1;
          slot += PDE::N;
        }

      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurrealR(ii,jj), n).deriv(slot + n - nchunk) = 1;
          slot += PDE::N;
        }

      integrandLSurreal = 0;
      integrandRSurreal = 0;

      // compute the integrand
      weightedIntegrand( nL,
                         paramL, paramR,
                         qL, gradqL, hessqSurrealL, tauL,
                         qR, gradqR, hessqSurrealR, tauR,
                         integrandLSurreal.data(), nDOFL_,
                         integrandRSurreal.data(), nDOFR_, true );
      // accumulate derivatives into element jacobian

      slot = 0;
      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurrealL(ii,jj), n).deriv(slot + n - nchunk) = 0; // Reset the derivative

            for (int i = 0; i < nDOFL_; i++)
            {
              // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
              for (int m = 0; m < PDE::N; m++)
                for (int n = 0; n < PDE::N; n++)
                  DLA::index(PDE_hessq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

              for (int j = 0; j < nDOFL_; j++)
                mtxElemL.PDE_qL(i,j) += dJ*hessphiL_[j](ii,jj)*PDE_hessq;
            }

            for (int i = 0; i < nDOFR_; i++)
            {
              // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
              for (int m = 0; m < PDE::N; m++)
                for (int n = 0; n < PDE::N; n++)
                  DLA::index(PDE_hessq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

              for (int j = 0; j < nDOFL_; j++)
                mtxElemR.PDE_qL(i,j) += dJ*hessphiL_[j](ii,jj)*PDE_hessq;
            }
          }
          slot += PDE::N;
        }

      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurrealR(ii,jj), n).deriv(slot + n - nchunk) = 0; // Reset the derivative

            for (int i = 0; i < nDOFL_; i++)
            {
              // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
              for (int m = 0; m < PDE::N; m++)
                for (int n = 0; n < PDE::N; n++)
                  DLA::index(PDE_hessq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

              for (int j = 0; j < nDOFR_; j++)
                mtxElemL.PDE_qR(i,j) += dJ*hessphiR_[j](ii,jj)*PDE_hessq;
            }

            for (int i = 0; i < nDOFR_; i++)
            {
              // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
              for (int m = 0; m < PDE::N; m++)
                for (int n = 0; n < PDE::N; n++)
                  DLA::index(PDE_hessq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

              for (int j = 0; j < nDOFR_; j++)
                mtxElemR.PDE_qR(i,j) += dJ*hessphiR_[j](ii,jj)*PDE_hessq;
            }
          }
          slot += PDE::N;
        }
    } // nchunk
  }

}

template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
template<class Tq, class Tg, class Th, class Tpq, class Tt, class Ti>
void
IntegrandInteriorTrace_Galerkin_Stabilized<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
weightedIntegrand( const VectorX& nL,
                   const ParamTL& paramL, const ParamTR& paramR,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL, const TensorSymArrayQ<Th>& hessqL,
                   const StabilizationMatrix::TauInverse<PDE::N, Tpq, Tt>& tauL,
                   const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR, const TensorSymArrayQ<Th>& hessqR,
                   const StabilizationMatrix::TauInverse<PDE::N, Tpq, Tt>& tauR,
                   ArrayQ<Ti> integrandL[], const int neqnL,
                   ArrayQ<Ti> integrandR[], const int neqnR,
                   bool gradientOnly ) const
{

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0;

  // advective flux term: +[[phi]].flux = (phi@L - phi@R) nL.flux
  //                                    = +phiL nL.flux; -phiR nL.flux

  //Construct strong PDE
  ArrayQ<Ti> strongPDEL = 0, strongPDER = 0, dstrongPDE = 0;

  if (pde_.hasFluxAdvective())
  {
    pde_.strongFluxAdvective( paramL, qL, gradqL, strongPDEL );
    pde_.strongFluxAdvective( paramL, qR, gradqR, strongPDER );
  }

  if (pde_.hasFluxViscous() && orderL_ > 1)
  {
    pde_.strongFluxViscous( paramL, qL, gradqR, hessqL, strongPDEL );
    pde_.strongFluxViscous( paramR, qR, gradqL, hessqR, strongPDER );
  }


  ArrayQ<Ti> source = 0;            // PDE source S(X, Q, QX)
  if (pde_.hasSource())
  {
    pde_.source( paramL, qL, gradqL, source );
    strongPDEL += source;
    strongPDER += source;
  }

  ArrayQ<Real> forcing = 0;            // PDE source S(X, Q, QX)
  if (pde_.hasForcingFunction())
  {
    pde_.forcingFunction( paramL, forcing );
    strongPDEL -= forcing;
    strongPDER -= forcing;
  }

  //don't need to compute source/forcing since they will be equal

  tau_.backsolve(tauL, strongPDEL);
  tau_.backsolve(tauR, strongPDER);

  dstrongPDE = strongPDEL - strongPDER;

  MatrixQ<Ti> dfduabs = 0;
  pde_.jacobianFluxAdvectiveAbsoluteValue(paramL, qL, nL, dfduabs);

  ArrayQ<Ti> dstrongPDEL = dfduabs*dstrongPDE;
  ArrayQ<Ti> dstrongPDER = dstrongPDEL;

  tau_.backsolve(tauL, dstrongPDEL);
  tau_.backsolve(tauR, dstrongPDER);

  // weighting: advective piece
  VectorMatrixQ<Ti> dFduL = 0, dFduR = 0;
  VectorArrayQ<Ti> strongFL= 0, strongFR = 0;
  if (pde_.hasFluxAdvective())
  {
    pde_.jacobianFluxAdvective( paramL, qL, dFduL );
    dFduR = dFduL;
  }

  // dFdU Term
  if (pde_.hasFluxViscous())
  {
    pde_.jacobianFluxViscous( paramL, qL, gradqL, dFduL );
    pde_.jacobianFluxViscous( paramR, qR, gradqR, dFduR );
  }

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    for (int d = 0; d < PDE::D; d++)
    {
      strongFL[d] = dFduL[d] * dstrongPDEL;
      strongFR[d] = dFduR[d] * dstrongPDER;
    }

    for (int k = 0; k < neqnL; k++)
      integrandL[k] +=  dot(gradphiL_[k], strongFL);

    for (int k = 0; k < neqnR; k++)
      integrandR[k] -=  dot(gradphiR_[k], strongFR);

  }


  TensorMatrixQ<Ti> KL = 0, KR = 0;

  if (pde_.hasFluxViscous())
  {
    pde_.diffusionViscous( paramL, qL, gradqL, KL );
    pde_.diffusionViscous( paramR, qR, gradqR, KR );
  }

  // Hessian Term
  TensorArrayQ<Ti> strongKL = 0, strongKR = 0;
  if (pde_.hasFluxViscous() && orderL_ > 1)
  {
    for (int i = 0; i < PDE::D; i++)
      for (int j = 0; j < PDE::D; j++)
      {
        strongKL(i,j) = KL(i,j) * dstrongPDEL;
        strongKR(i,j) = KR(i,j) * dstrongPDER;

        for (int k = 0; k < neqnL; k++)
          integrandL[k] -=  hessphiL_[k](i,j)*strongKL(i,j);

        for (int k = 0; k < neqnR; k++)
          integrandR[k] +=  hessphiR_[k](i,j)*strongKR(i,j);

      }



  }



}

template<class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR >
template <class Ti>
void
IntegrandInteriorTrace_Galerkin_Stabilized<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()(const QuadPointTraceType& sRefTrace, Ti integrandL[], const int nphiL,
                                                Ti integrandR[], const int nphiR ) const
{
  SANS_ASSERT( nphiL == nPhiL_ && nphiR == nPhiR_ );

  ParamTL paramL;   // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  VectorX nL;       // unit normal for left element (points to right element)

  ArrayQ<T> qL, qR;                // solution
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients
  VectorArrayQ<T> dqn;             // jump in q.n

  ArrayQ<T> wL, wR;                // weight
  VectorArrayQ<T> gradwL, gradwR;  // weight gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // solution value, gradient
  qfldElemL_.eval( sRefL, qL );
  qfldElemR_.eval( sRefR, qR );
  if (pde_.hasFluxViscous() || pde_.hasSourceTrace())
  {
    xfldElemL_.evalGradient( sRefL, qfldElemL_, gradqL );
    xfldElemR_.evalGradient( sRefR, qfldElemR_, gradqR );
  }

  // weight value,
  wfldElemL_.eval( sRefL, wL );
  wfldElemR_.eval( sRefR, wR );
  if (pde_.hasFluxViscous())
  {
    xfldElemL_.evalGradient( sRefL, wfldElemL_, gradwL );
    xfldElemR_.evalGradient( sRefR, wfldElemR_, gradwR );
  }

  // estimate basis, gradient
  efldElemL_.evalBasis( sRefL, phiL_, nPhiL_);
  efldElemR_.evalBasis( sRefR, phiR_, nPhiR_);
  xfldElemL_.evalBasisGradient( sRefL, efldElemL_, gradphiL_, nPhiL_ );
  xfldElemR_.evalBasisGradient( sRefR, efldElemR_, gradphiR_, nPhiR_ );

  for (int k = 0; k < nPhiL_; k++)
  {
    integrandL[k] = 0;
    weightL_[k] = phiL_[k]*wL;
  }

  for (int k = 0; k < nPhiR_; k++)
  {
    integrandR[k] = 0;
    weightR_[k] = phiR_[k]*wR;
  }

  if (pde_.hasFluxViscous() )
  {
    // Grad (w phi) needed
    for (int k = 0; k < nPhiL_; k++)
      gradWeightL_[k] = gradphiL_[k]*wL + phiL_[k]*gradwL;

    for (int k = 0; k < nPhiR_; k++)
      gradWeightR_[k] = gradphiR_[k]*wR + phiR_[k]*gradwR;
  }

  // advective flux term: +[[w]].flux = (wL - wR) nL.flux
  //                                  = +wL nL.flux; -wR nL.flux

  ArrayQ<Ti> Fn; // normal advective flux
  if (pde_.hasFluxAdvective())
  {
    Fn = 0;
    pde_.fluxAdvectiveUpwind( paramL, qL, qR, nL, Fn );

    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] +=  dot(weightL_[k],Fn);
    for (int k = 0; k < nPhiR_; k++)
      integrandR[k] += -dot(weightR_[k],Fn);
  }
  // viscous flux term: +[[w]].flux = (w@L - w@R) nL.flux
  //                                = +wL nL.flux; -wR nL.flux

//  if (pde_.hasFluxViscous())
//  {
//    VectorArrayQ<Ti> Favg;
//    VectorArrayQ<Ti> FL = 0;
//    VectorArrayQ<Ti> FR = 0;
//
//    pde_.fluxViscous( paramL, qL, gradqL, FL );
//    pde_.fluxViscous( paramR, qR, gradqR, FR );
//
//    Favg = 0.5*(FR + FL);
//    Fn = dot(nL,Favg);
//
//    for (int k = 0; k < nPhiL_; k++)
//      integrandL[k] +=  dot(weightL_[k],Fn);
//    for (int k = 0; k < nPhiR_; k++)
//      integrandR[k] += -dot(weightR_[k],Fn);
//  }

  // dual consistency term: -{gradphi^t (dgradU/dgradQ K) }.[[q]]
  //                      = - 0.5*( gradphi@L^t (K*dgradUR/dgradQR) + gradphi@R^t (KR*dgradUR/dgradQR) ). nL*( qL - qR )

//  if (pde_.hasFluxViscous())
//  {
//
//    // Jump in primitive variable [ nL*( qL - qR ) ]
//    for (int d = 0; d < D; d++)
//      dqn[d] = nL[d]*(qL - qR);
//
//    MatrixQ<T> uL_qL = 0; // conservation solution jacobians dU(Q)/dQ
//    MatrixQ<T> uR_qR = 0;
//    pde_.jacobianMasterState( paramL, qL, uL_qL );
//    pde_.jacobianMasterState( paramR, qR, uR_qR );
//
//    VectorArrayQ<T> dunL, dunR;
//    for ( int i = 0; i < D; i++ )
//    {
//      dunL[i] = uL_qL*dqn[i];
//      dunR[i] = uR_qR*dqn[i];
//    }
//
//    DLA::MatrixS< D, D, MatrixQ<Ti> > KL = 0, KR = 0;   // diffusion matrix
//
//    pde_.diffusionViscous( paramL, qL, gradqL, KL );
//    pde_.diffusionViscous( paramR, qR, gradqR, KR );
//
//    VectorArrayQ<Ti> tmpL = KL*dunL;
//    VectorArrayQ<Ti> tmpR = KR*dunR;
//
//    for (int d = 0; d < PhysDim::D; d++)
//    {
//      for (int k = 0; k < nPhiL_; k++)
//        integrandL[k] -= 0.5*dot(gradWeightL_[k][d],tmpL[d]);
//      for (int k = 0; k < nPhiR_; k++)
//        integrandR[k] -= 0.5*dot(gradWeightR_[k][d],tmpR[d]);
//    }
//  }
//
//  // dual-consistent source term: +w S
//  if (pde_.hasSourceTrace())
//  {
//    ArrayQ<Ti> sourceL=0, sourceR=0;      // PDE source S(X, Q, QX)
//    pde_.sourceTrace( paramL, paramR, qL, gradqL, qR, gradqR, sourceL, sourceR );
//
//    Real hL = xfldElemL_.jacobianDeterminant() / xfldElemTrace_.jacobianDeterminant();
//    Real hR = xfldElemR_.jacobianDeterminant() / xfldElemTrace_.jacobianDeterminant();
//    for (int k = 0; k < nPhiL_; k++)
//      integrandL[k] += dot(weightL_[k],sourceL) * hL;
//    for (int k = 0; k < nPhiR_; k++)
//      integrandR[k] += dot(weightR_[k],sourceR) * hR;
//  }
}

} // namespace SANS
#endif //INTEGRANDINTERIORTRACE_GALERKIN_STABILIZED_H
