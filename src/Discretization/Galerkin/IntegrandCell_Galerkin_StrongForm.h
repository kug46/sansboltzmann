// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_STRONG_H
#define INTEGRANDCELL_GALERKIN_STRONG_H

// cell integrand operator: CG

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"     // Real
#include "tools/SANSException.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "Field/Element/Element.h"
#include "Discretization/Integrand_Type.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Galerkin/Stabilization_Galerkin.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template <class PDE_>
class IntegrandCell_Galerkin_StrongForm : public IntegrandCellType< IntegrandCell_Galerkin_StrongForm<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual arrays

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;    // solution/residual arrays

  template <class T>
  using TensorMatrixQ = typename PDE::template TensorMatrixQ<T>;    // diffusion matrix

  template <class T>
  using VectorTensorMatrixQ = typename PDE::template VectorTensorMatrixQ<T>;    // div.diffusion matrix

  template <class T>
  using TensorSymArrayQ = typename PDE::template TensorSymArrayQ<T>;    // solution/residual arrays

  typedef typename PDE::VectorX VectorX;
  typedef DLA::MatrixSymS<PhysDim::D, Real> TensorSymX;

  static const int N = PDE::N;              // total PDE equations

  // total PDE equations
  int nEqn() const { return PDE::N; }

  IntegrandCell_Galerkin_StrongForm( const PDE& pde, const std::vector<int>& CellGroups, const StabilizationMatrix& tau )
    : pde_(pde), cellGroups_(CellGroups), tau_(tau) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<Real, TopoDim, Topology> ElementEFieldType;

    typedef typename ElementParam::T ParamT;
    typedef typename ElementParam::gradT ParamGradT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef ArrayQ<T> IntegrandType;

    BasisWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
                   const StabilizationMatrix& tau ) :
                   pde_(pde),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem),
                   paramfldElem_( paramfldElem ),
                   nDOF_( qfldElem_.nDOF() ),
                   nNodes_( qfldElem_.basis()->nBasisNode() ),
                   phi_( new Real[nDOF_] ),
                   gradphi_( new VectorX[nDOF_] ),
                   hessphi_( new TensorSymX[nDOF_] ),
                   order_( qfldElem_.basis()->order() ),
                   tau_(tau),
                   stabType_( tau_.getStabType()),
                   stabElem_( tau_.getStabOrder(), qfldElem_.basis()->category() ),
                   phiLin_( new Real[stabElem_.nDOF()] ),
                   gradphiLin_( new VectorX[stabElem_.nDOF()] )
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                   pde_(bw.pde_),
                   xfldElem_(bw.xfldElem_),
                   qfldElem_(bw.qfldElem_),
                   paramfldElem_( bw.paramfldElem_ ),
                   nDOF_( bw.nDOF_ ),
                   nNodes_( bw.nNodes_ ),
                   phi_( bw.phi_ ),
                   gradphi_( bw.gradphi_ ),
                   hessphi_( bw.hessphi_ ),
                   order_( bw.order_ ),
                   tau_(bw.tau_),
                   stabType_( tau_.getStabType()),
                   stabElem_( tau_.getStabOrder(), qfldElem_.basis()->category() ),
                   phiLin_( bw.phiLin_ ),
                   gradphiLin_( bw.gradphiLin_ )
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr; bw.hessphi_ = nullptr;
      bw.phiLin_ = nullptr; bw.gradphiLin_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
      delete [] hessphi_;
      delete [] phiLin_;
      delete [] gradphiLin_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn ) const;

    // cell element integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

    void operator()( const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<T> >& rsdPDEElem) const;

  protected:
    template<class Tq, class Tg, class Th, class Ti>
    void weightedIntegrand( const ParamT& param, const ParamGradT& gradparam,
                            const ArrayQ<Tq>& q,
                            const VectorArrayQ<Tg>& gradq,
                            const TensorSymArrayQ<Th>& hessq,
                            ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nNodes_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable TensorSymX *hessphi_;
    const int order_;
    const StabilizationMatrix& tau_;
    const StabilizationType stabType_;
    const ElementEFieldType stabElem_;
    mutable Real *phiLin_;
    mutable VectorX *gradphiLin_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, tau_);
  }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<Real, TopoDim, Topology> ElementEFieldType;

    typedef typename ElementParam::T ParamT;
    typedef typename ElementParam::gradT ParamGradT;
    typedef QuadraturePoint<TopoDim> QuadPointType;

    FieldWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
                   const Element<Real, TopoDim, Topology>& efldElem,
                   const StabilizationMatrix& tau) :
                   pde_(pde),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem),
                   wfldElem_(wfldElem),
                   efldElem_(efldElem),
                   paramfldElem_(paramfldElem),
                   tau_(tau),
                   order_(qfldElem.basis()->order()),
                   nDOF_(qfldElem_.nDOF()),
                   nNodes_( qfldElem_.basis()->nBasisNode() ),
                   nPhi_( efldElem_.nDOF()),
                   ephi_( new Real[nPhi_] ), // basis associated with efld
                   gradephi_( new VectorX[nPhi_] ),
                   hessephi_( new TensorSymX[nPhi_] ),
                   weight_( new ArrayQ<T>[nPhi_] ),
                   gradWeight_( new VectorArrayQ<T>[nPhi_] ),
                   hessWeight_( new TensorSymArrayQ<T>[nPhi_] ),
                   gradphi_( new VectorX[nDOF_] ), // basis associated with qfld
                   stabType_( tau_.getStabType()),
                   stabElem_( tau_.getStabOrder(), qfldElem_.basis()->category() ),
                   phiLin_( new Real[stabElem_.nDOF()] ),
                   gradphiLin_( new VectorX[stabElem_.nDOF()] )
    {
    }

    FieldWeighted( FieldWeighted&& fw ) :
                   pde_(fw.pde_),
                   xfldElem_(fw.xfldElem_),
                   qfldElem_(fw.qfldElem_),
                   wfldElem_(fw.wfldElem_),
                   efldElem_(fw.efldElem_),
                   paramfldElem_(fw.paramfldElem_),
                   tau_(fw.tau_),
                   order_(fw.order_),
                   nDOF_(fw.nDOF_),
                   nNodes_( fw.nNodes_ ),
                   nPhi_( fw.nPhi_ ),
                   ephi_( fw.ephi_ ),
                   gradephi_( fw.gradephi_ ),
                   hessephi_( fw.hessephi_ ),
                   weight_( fw.weight_ ),
                   gradWeight_( fw.gradWeight_ ),
                   hessWeight_( fw.hessWeight_ ),
                   gradphi_( fw.gradphi_ ),
                   stabType_( tau_.getStabType()),
                   stabElem_( tau_.getStabOrder(), qfldElem_.basis()->category() ),
                   phiLin_( fw.phiLin_ ),
                   gradphiLin_( fw.gradphiLin_ )
    {
      fw.ephi_ = nullptr; fw.gradephi_ = nullptr; fw.hessephi_ = nullptr;
      fw.weight_ = nullptr; fw.gradWeight_ = nullptr; fw.hessWeight_ = nullptr;
      fw.gradphi_ = nullptr;
      fw.phiLin_ = nullptr; fw.gradphiLin_=nullptr;
    }

    ~FieldWeighted()
    {
      delete [] ephi_;
      delete [] gradephi_;
      delete [] hessephi_;
      delete [] weight_;
      delete [] gradWeight_;
      delete [] hessWeight_;
      delete [] gradphi_;
      delete [] phiLin_;
      delete [] gradphiLin_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    template <class Ti>
    void operator()( const QuadPointType& sRef, Ti integrand[], const int nphi ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& wfldElem_;
    const ElementEFieldType& efldElem_;
    const ElementParam& paramfldElem_;
    const StabilizationMatrix& tau_;
    const int order_;
    const int nDOF_;
    const int nNodes_;

    const int nPhi_;
    mutable Real *ephi_; // efld basis
    mutable VectorX *gradephi_;
    mutable TensorSymX *hessephi_;
    mutable ArrayQ<T> *weight_;
    mutable VectorArrayQ<T> *gradWeight_;
    mutable TensorSymArrayQ<T> *hessWeight_;
    mutable VectorX *gradphi_; // basis gradient
    const StabilizationType stabType_;

    const ElementEFieldType stabElem_;
    mutable Real *phiLin_;
    mutable VectorX *gradphiLin_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const Element<Real, TopoDim, Topology>& efldElem) const
  {
    return FieldWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, wfldElem, efldElem, tau_);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
  const StabilizationMatrix& tau_;
};

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_StrongForm<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() || pde_.hasSource() || pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_StrongForm<PDE>::
FieldWeighted<T, TopoDim, Topology, ElementParam>::
needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() || pde_.hasSource() || pde_.hasForcingFunction() );
}


// Residual interface
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin_StrongForm<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn) const
{
  //const int neqn = integrand.m();
  SANS_ASSERT(neqn == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)
  ParamGradT gradparam;

  ArrayQ<T> q = 0;                 // solution
  VectorArrayQ<T> gradq = 0;       // gradient
  TensorSymArrayQ<T> hessq = 0;    // solution hessian

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  if (pde_.hasFluxViscous() && order_ > 1)
  {
    xfldElem_.evalBasisHessian( sRef, qfldElem_, hessphi_, nDOF_ );
    qfldElem_.evalFromBasis( hessphi_, nDOF_, hessq );
  }

  if ( tau_.getStabType() != StabilizationType::Unstabilized )
  {
    stabElem_.evalBasis(sRef, phiLin_, stabElem_.nDOF());
    xfldElem_.evalBasisGradient( sRef, stabElem_, gradphiLin_, stabElem_.nDOF() );
  }

  weightedIntegrand(param, gradparam, q, gradq, hessq, integrand, neqn);
}

// Jacobian interface
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_StrongForm<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  //const int neqn = integrand.m();
  SANS_ASSERT(mtxPDEElem.m() == nDOF_);
  SANS_ASSERT(mtxPDEElem.n() == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)
  ParamGradT gradparam;

  ArrayQ<T> q = 0;                 // solution
  VectorArrayQ<T> gradq = 0;       // gradient
  TensorSymArrayQ<T> hessq = 0;    // solution hessian

  ArrayQ<SurrealClass> qSurreal = 0;               // solution
  VectorArrayQ<SurrealClass> gradqSurreal = 0;     // gradient
  TensorSymArrayQ<SurrealClass> hessqSurreal = 0;  // solution hessian

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0, PDE_hessq = 0; // temporary storage

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  qSurreal = q;
  gradqSurreal = gradq;

  if (pde_.hasFluxViscous() && order_ > 1)
  {
    xfldElem_.evalBasisHessian(  sRef, qfldElem_, hessphi_, nDOF_ );
    qfldElem_.evalFromBasis( hessphi_, nDOF_, hessq );
    hessqSurreal = hessq;
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );


  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    weightedIntegrand(param, gradparam, qSurreal, gradq, hessq, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;
  } // nchunk



  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    integrandSurreal = 0;

    weightedIntegrand(param, gradparam, q, gradqSurreal, hessq, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOF_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOF_; j++)
            mtxPDEElem(i,j) += dJ*gradphi_[j][d]*PDE_gradq;
        }
      }
      slot += PDE::N;
    }
  } // nchunk

  if (pde_.hasFluxViscous() && order_ > 1)
  {
    const int nHess = PhysDim::D*(PhysDim::D + 1)/2;

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nHess*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurreal(ii,jj), n).deriv(slot + n - nchunk) = 1;
          slot += PDE::N;
        }

      integrandSurreal = 0;

      weightedIntegrand(param, gradparam, q, gradq, hessqSurreal, integrandSurreal.data(), integrandSurreal.size());

      // accumulate derivatives into element jacobian

      slot = 0;
      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurreal(ii,jj), n).deriv(slot + n - nchunk) = 0; // Reset the derivative

            for (int i = 0; i < nDOF_; i++)
            {
              // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
              for (int m = 0; m < PDE::N; m++)
                for (int n = 0; n < PDE::N; n++)
                  DLA::index(PDE_hessq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

              for (int j = 0; j < nDOF_; j++)
                mtxPDEElem(i,j) += dJ*hessphi_[j](ii,jj)*PDE_hessq;
            }
          }
          slot += PDE::N;
        }
    } // nchunk
  }
}

// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Tq, class Tg, class Th, class Ti>
void
IntegrandCell_Galerkin_StrongForm<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
weightedIntegrand(const ParamT& param, const ParamGradT& gradparam,
                  const ArrayQ<Tq>& q,
                  const VectorArrayQ<Tg>& gradq,
                  const TensorSymArrayQ<Th>& hessq,
                  ArrayQ<Ti> integrand[], int neqn) const
{
  ArrayQ<Ti> Fs = 0;           // PDE flux F(X, Q), Fv(X, Q, QX)
  ArrayQ<Ti> source = 0;            // PDE source S(X, Q, QX)

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // Galerkin flux term: w . div(F)

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.strongFluxAdvective( param, q, gradq, Fs );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.strongFluxViscous( param, q, gradq, hessq, Fs );

    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*Fs;
  }

  // Galerkin source term: +phi S

  if (pde_.hasSource())
  {
    pde_.source( param, q, gradq, source );

    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  ArrayQ<Real> forcing = 0;
  if (pde_.hasForcingFunction())
  {
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < neqn; k++)
      integrand[k] -= phi_[k]*forcing;
  }

  TensorMatrixQ<Ti> K = 0;
  if (pde_.hasFluxViscous())
    pde_.diffusionViscous( param, q, gradq, K );

  MatrixQ<Ti> S = 0;
  if (pde_.hasSource())
    pde_.jacobianSourceAbsoluteValue(param, q, gradq, S);

  // adjoint stabilization: - adjointPDE(phi) . tau . primalPDE(q)
  if (stabType_ != StabilizationType::Unstabilized)
  {
    // strong form PDE residual
    ArrayQ<Ti> strongPDE = 0;
    if (stabType_ == StabilizationType::AGLSAdjoint) //technically should be using qReal for some of this, but hopefully never have to use
    {
      SANS_DEVELOPER_EXCEPTION("NEED TO IMPLEMENT qR etc...");
//      tau_.template constructAdjointPDE<Tq, Tg, Th, Ti>(pde_, param, q, qR, gradq, gradqR, hessq, hessqR, K, order_, strongPDE);
    }
    else
      tau_.template constructStrongPDE<Tq, Tg, Th, Ti>(pde_, param, q, gradq, hessq, source, forcing, strongPDE);

    // apply stabilization matrix tau
    tau_.template solve<Tq,Ti,Ti>(pde_, param, q, K, S, phiLin_, gradphiLin_, stabElem_.order(),
                               nNodes_, stabElem_.nDOF(), strongPDE);

    if (stabType_ == StabilizationType::SUPG)
    {
      tau_.template operatorSUPG<Tq, Tg, Ti>(pde_, param, q, gradq, gradphi_, strongPDE, neqn, integrand);
    }
    else if (stabType_ == StabilizationType::GLS || stabType_ == StabilizationType::AGLSPrimal)
    {
      tau_.template operatorGLS<Tq, Tg, Th, Ti, Ti>(pde_, param, q, gradq, hessq, K, phi_, gradphi_, hessphi_, strongPDE, neqn, order_, integrand);
    }
    else if (stabType_ == StabilizationType::Adjoint || stabType_ == StabilizationType::AGLSAdjoint)
    {
#if 1
//      tau_.template operatorAdjoint<Tq, Tg, Th, Ti, Ti>
//        (pde_, param, q, gradq, hessq, K, phi_, gradphi_, hessphi_, strongPDE, neqn, order_, integrand);

      tau_.template operatorAdjoint<Tq, Tg, Th, Ti, Ti>
        (pde_, param, gradparam, q, gradq, hessq, K, phi_, gradphi_, hessphi_, strongPDE, neqn, order_, integrand);
#else
      tau_.template operatorAdjoint<Real, Real, Real, Real, Ti>
        (pde_, param, q, gradq, hessq, K, phi_, gradphi_, hessphi_, strongPDE, neqn, order_, integrand);
#endif
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION("INVALID STABILIZATION");
    }

  }

}


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_StrongForm<PDE>::
FieldWeighted<T, TopoDim, Topology, ElementParam>::
operator()( const QuadPointType& sRef, Ti integrand[], const int nphi ) const
{
  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)
  ParamGradT gradparam;                // Gradient of elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                                    // solution
  DLA::VectorS<PhysDim::D, ArrayQ<T> > gradq;     // solution gradient
  DLA::MatrixSymS<PhysDim::D, ArrayQ<T> > hessq;  // solution hessian

  ArrayQ<T> w;               // weight
  VectorArrayQ<T> gradw;     // weight gradient
  TensorSymArrayQ<T> hessw;  // weight hessian

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // solution value, gradient
  qfldElem_.eval( sRef, q );
  xfldElem_.evalGradient( sRef, qfldElem_, gradq );

  // weight value, gradient
  wfldElem_.eval( sRef, w );
  xfldElem_.evalGradient( sRef, wfldElem_, gradw );

  // solution and weight hessian
  if ( pde_.hasFluxViscous() )
  {
    if ( order_ > 1 )
      xfldElem_.evalHessian( sRef, qfldElem_, hessq );
    else
      hessq = 0;

    if ( wfldElem_.basis()->order() > 1 )
      xfldElem_.evalHessian( sRef, wfldElem_, hessw );
    else
      hessw = 0;
  }

  // evaluate basis function gradient
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // estimate basis, gradient
  efldElem_.evalBasis( sRef, ephi_, nPhi_ );
  xfldElem_.evalBasisGradient( sRef, efldElem_, gradephi_, nPhi_ );

  if (tau_.getStabType() != StabilizationType::Unstabilized )
  {
    stabElem_.evalBasis(sRef, phiLin_, stabElem_.nDOF());
    xfldElem_.evalBasisGradient( sRef, stabElem_, gradphiLin_, stabElem_.nDOF() );
  }

  for (int k = 0; k < nPhi_; k++)
  {
    integrand[k] = 0;
    weight_[k] = ephi_[k]*w;
    gradWeight_[k] = gradephi_[k]*w + ephi_[k]*gradw;
  }

  // Galerkin strong flux term: +(w, Fs)

  ArrayQ<Ti> Fs=0;       // PDE strong form flux F_s(X, U), Fv_s(X, U, UX)

  // strong advective flux
  if (pde_.hasFluxAdvective())
    pde_.strongFluxAdvective( param, q, gradq, Fs );

    // strong viscous flux
  if (pde_.hasFluxViscous())
    pde_.strongFluxViscous( param, q, gradq, hessq, Fs );

  for (int k = 0; k < nPhi_; k++)
    integrand[k] += dot(weight_[k],Fs);
#if 0
  std::cout<< "IntegrandCellStrong:" << integrand <<std::endl;
#endif

  // Galerkin source term: +phi S

  ArrayQ<Ti> source=0;   // PDE source S(X, D, U, UX), S(X)
  if (pde_.hasSource())
  {
    pde_.source( param, q, gradq, source );

    for (int k = 0; k < nPhi_; k++)
      integrand[k] += dot(weight_[k],source);
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  ArrayQ<Real> forcing = 0;
  if (pde_.hasForcingFunction())
  {
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < nPhi_; k++)
      integrand[k] -= dot(weight_[k],forcing);
  }

  if (stabType_ == StabilizationType::SUPG || stabType_== StabilizationType::GLS || stabType_== StabilizationType::Adjoint )
  {
    // stabilization: - P(phi) . tau . primalPDE(q)
    // test function weighting done at whole integrand
    // integrand[k] = ephi[k]* - P(phi) . tau . primalPDE(q)

    //NO STABILIZATION TERM NECESSARY FOR UNSTABILIZED, AGLS

    //contruct strongPDE
    ArrayQ<T> strongPDE = 0;
    tau_.template constructStrongPDE<T, T, T, T>(pde_, param, q, gradq, hessq, source, forcing, strongPDE);

    // adjoint PDE
    TensorMatrixQ<Ti> K = 0;

    if (pde_.hasFluxViscous())
      pde_.diffusionViscous( param, q, gradq, K );

    MatrixQ<Ti> S = 0;
    if (pde_.hasSource())
      pde_.jacobianSourceAbsoluteValue(param, q, gradq, S);

    tau_.template solve<T,T,T>(pde_, param, q, K, S, phiLin_, gradphiLin_, stabElem_.order(),
                               nNodes_, stabElem_.nDOF(), strongPDE);

    Ti stabIntegrand = 0;
    if (stabType_ == StabilizationType::SUPG)
    {
      tau_.template operatorSUPGFW<T, Ti>(pde_, param, q, gradq, gradw, strongPDE, stabIntegrand);
    }
    else if (stabType_ == StabilizationType::GLS)
    {
      tau_.template operatorGLSFW<T, Ti>(pde_, param, q, gradq, hessq, K, w, gradw, hessw, strongPDE, wfldElem_.order(), stabIntegrand);
    }
    else if (stabType_ == StabilizationType::Adjoint)
    {
//      tau_.template operatorAdjointFW<T, Ti>
//        (pde_, param, q, gradq, hessq, K, w, gradw, hessw, strongPDE, wfldElem_.order(), stabIntegrand);

      tau_.template operatorAdjointFW<T, Ti>
        (pde_, param, gradparam, q, gradq, hessq, K, w, gradw, hessw, strongPDE, wfldElem_.order(), stabIntegrand);
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION("INVALID STABILIZATION");
    }

    // sum the terms into the integrand
    for (int k = 0; k < nPhi_; k++)
      integrand[k] += ephi_[k]*stabIntegrand;
  }

}

}

#endif  // INTEGRANDCELL_GALERKIN_STRONG_H
