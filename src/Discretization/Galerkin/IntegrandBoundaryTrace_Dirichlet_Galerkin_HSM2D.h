// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_DIRICHLET_GALERKIN_HSM2D_H
#define INTEGRANDBOUNDARYTRACE_DIRICHLET_GALERKIN_HSM2D_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"
#include "pde/HSM/PDEHSM2D.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Integrand_Galerkin_fwd.h"
#include "Stabilization_Nitsche.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE/BC
//
// conventional (sans Lagrange) formulation for Dirichlet BC specific to PDEHSM2D

template<class VarType, template <class, class > class PDENDConvert, class NDBCVector >
class IntegrandBoundaryTrace<PDENDConvert<PhysD2,PDEHSM2D<VarType>>,
                             NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin> :
  public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDENDConvert<PhysD2,PDEHSM2D<VarType>>,
                                                            NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin> >
{
public:
  typedef BCCategory::Dirichlet_sansLG Category;
  typedef Galerkin DiscTag;

  typedef PDENDConvert<PhysD2,PDEHSM2D<VarType>> PDE;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;        // matrices

  static const int N = PDE::N;

  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const StabilizationNitsche& stab)
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups) {}

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem ) :
                   pde_(pde), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   qfldElem_(qfldElem),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   paramfldElem_( paramfldElem ),
                   nDOF_(qfldElem_.nDOF()),
                   phi_( new Real[nDOF_] ),
                   gradphi_( new VectorX[nDOF_] )
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                   pde_(bw.pde_), bc_(bw.bc_),
                   xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                   qfldElem_(bw.qfldElem_),
                   xfldElem_(bw.xfldElem_),
                   paramfldElem_( bw.paramfldElem_ ),
                   nDOF_( bw.nDOF_ ),
                   phi_( bw.phi_ ),
                   gradphi_( bw.gradphi_ )
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrand[], int neqn ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrand, neqn);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementQFieldCell& qfldElem_;
    const ElementXFieldCell& xfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem) const
  {
    return {pde_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem, qfldElem};
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
};


template<class VarType, template <class, class > class PDENDConvert, class NDBCVector >
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class Topology, class ElementParam>
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDENDConvert<PhysD2,PDEHSM2D<VarType>>, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT param;             // Elemental parameters (such as grid coordinates and distance functions)

  VectorX Nrm;              // unit normal (points out of domain)

  ArrayQ<T> q;              // solution
  VectorArrayQ<T> gradq;    // gradient
  DLA::VectorS<TopoDimCell::D, ArrayQ<T> > q_sRef; // solution derivative wrt reference coordinates

  DLA::VectorS<TopoDimCell::D, VectorX > X_sRef;   // X derivative wrt reference coordinates

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, param );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, Nrm);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // derivative of solution and coordinates wrt reference coordinates
  qfldElem_.evalDerivative( sRef, q_sRef );
  xfldElem_.evalDerivative( sRef, X_sRef );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // PDE residual: weak form boundary integral

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;     // PDE flux

    // advective flux
    pde_.fluxAdvective( param, q, F );

    // viscous flux
    SANS_ASSERT( !pde_.hasFluxViscous() );
    //pde_.fluxViscous( param, q, gradq, F ); (turned off for now.... need to update arguments in NDConvert)

    ArrayQ<Ti> Fn = dot(Nrm,F);
    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*Fn;
  }

  // PDE residual: weighted strong-form BC

  ArrayQ<Ti> rsdBC = 0;
  MatrixQ<Ti> wghtBC = 0;
  bc.strongBC( param, X_sRef, Nrm, q, gradq, q_sRef, rsdBC );
  bc.weightBC( param, X_sRef, Nrm, q, gradq, q_sRef, wghtBC );

  ArrayQ<Ti> term = wghtBC*rsdBC;
  for (int k = 0; k < neqn; k++)
    integrand[k] -= phi_[k]*term;
}

}

#endif  // INTEGRANDBOUNDARYTRACE_DIRICHLET_GALERKIN_HSM2D_H
