// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDINTERIORTRACE_GALERKIN_STRONGFORM_H
#define INTEGRANDINTERIORTRACE_GALERKIN_STRONGFORM_H

// trace integrand operator: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Trace integrand: Galerkin

template <class PDE_>
class IntegrandInteriorTrace_Galerkin_StrongForm : public IntegrandInteriorTraceType< IntegrandInteriorTrace_Galerkin_StrongForm<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  static const int N = PDE::N;

  // total PDE equations
  int nEqn() const { return PDE::N; }

  // cppcheck-suppress noExplicitConstructor
  IntegrandInteriorTrace_Galerkin_StrongForm(const PDE& pde,
                                         const std::vector<int>& InteriorTraceGroups) :
                                           pde_(pde),
                                           interiorTraceGroups_(InteriorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldTypeL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldTypeR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandType;

    BasisWeighted( const PDE& pde,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter
                   const ElementQFieldTypeL& qfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldTypeR& qfldElemR ) :
                   pde_(pde),
                   xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                   xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), paramfldElemL_( paramfldElemL ),
                   xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), paramfldElemR_( paramfldElemR ),
                   nDOFL_(qfldElemL.nDOF()),
                   nDOFR_(qfldElemR.nDOF()),
                   phiL_( new Real[nDOFL_] ),
                   phiR_( new Real[nDOFR_] ),
                   gradphiL_( new VectorX[nDOFL_] ),
                   gradphiR_( new VectorX[nDOFR_] )
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                   pde_(bw.pde_),
                   xfldElemTrace_(bw.xfldElemTrace_), canonicalTraceL_(bw.canonicalTraceL_), canonicalTraceR_(bw.canonicalTraceR_),
                   xfldElemL_(bw.xfldElemL_), qfldElemL_(bw.qfldElemL_), paramfldElemL_( bw.paramfldElemL_ ),
                   xfldElemR_(bw.xfldElemR_), qfldElemR_(bw.qfldElemR_), paramfldElemR_( bw.paramfldElemR_ ),
                   nDOFL_(bw.nDOFL_),
                   nDOFR_(bw.nDOFR_),
                   phiL_( bw.phiL_ ),
                   phiR_( bw.phiR_ ),
                   gradphiL_( bw.gradphiL_ ),
                   gradphiR_( bw.gradphiR_ )
    {
      bw.phiL_     = nullptr; bw.phiR_     = nullptr;
      bw.gradphiL_ = nullptr; bw.gradphiR_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phiL_;
      delete [] phiR_;

      delete [] gradphiL_;
      delete [] gradphiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRef, IntegrandType integrandL[], const int neqnL,
                                                     IntegrandType integrandR[], const int neqnR ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldTypeL& qfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldTypeR& qfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
    mutable VectorX *gradphiL_;
    mutable VectorX *gradphiR_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL   > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR   > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldTypeL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldTypeR;

    typedef Element<Real, TopoDimCell, TopologyL> ElementEFieldTypeL;
    typedef Element<Real, TopoDimCell, TopologyR> ElementEFieldTypeR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef Real IntegrandType;

    FieldWeighted( const PDE& pde,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter
                   const ElementQFieldTypeL& qfldElemL,
                   const ElementQFieldTypeL& wfldElemL,
                   const ElementEFieldTypeL& efldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldTypeR& qfldElemR,
                   const ElementQFieldTypeR& wfldElemR,
                   const ElementEFieldTypeR& efldElemR ) :
                   pde_(pde),
                   xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                   xfldElemL_(get<-1>(paramfldElemL)),
                   qfldElemL_(qfldElemL), wfldElemL_(wfldElemL), efldElemL_(efldElemL),
                   paramfldElemL_( paramfldElemL ),
                   xfldElemR_(get<-1>(paramfldElemR)),
                   qfldElemR_(qfldElemR), wfldElemR_(wfldElemR), efldElemR_(efldElemR),
                   paramfldElemR_( paramfldElemR ),
                   nDOFL_(qfldElemL_.nDOF()), nDOFR_(qfldElemR_.nDOF()),
                   nPhiL_(efldElemL_.nDOF()), nPhiR_(efldElemR_.nDOF()),
                   phiL_( new Real[nPhiL_] ), phiR_( new Real[nPhiR_] ),
                   gradphiL_( new VectorX[nPhiL_] ), gradphiR_( new VectorX[nPhiR_] ),
                   weightL_( new ArrayQ<T>[nPhiL_] ), weightR_( new ArrayQ<T>[nPhiR_] ),
                   gradWeightL_( new VectorArrayQ<T>[nPhiL_] ), gradWeightR_( new VectorArrayQ<T>[nPhiR_] )
                   {}

    FieldWeighted( FieldWeighted&& fw ) :
                   pde_(fw.pde_),
                   xfldElemTrace_(fw.xfldElemTrace_), canonicalTraceL_(fw.canonicalTraceL_), canonicalTraceR_(fw.canonicalTraceR_),
                   xfldElemL_(fw.xfldElemL_),
                   qfldElemL_(fw.qfldElemL_), wfldElemL_(fw.wfldElemL_), efldElemL_(fw.efldElemL_),
                   paramfldElemL_( fw.paramfldElemL_ ),
                   xfldElemR_(fw.xfldElemR_),
                   qfldElemR_(fw.qfldElemR_), wfldElemR_(fw.wfldElemR_), efldElemR_(fw.efldElemR_),
                   paramfldElemR_( fw.paramfldElemR_ ),
                   nDOFL_(fw.nDOFL_), nDOFR_(fw.nDOFR_),
                   nPhiL_(fw.nPhiL_), nPhiR_(fw.nPhiR_),
                   phiL_( fw.phiL_ ), phiR_( fw.phiR_ ),
                   gradphiL_( fw.gradphiL_ ), gradphiR_( fw.gradphiR_ ),
                   weightL_( fw.weightL_ ), weightR_( fw.weightR_ ),
                   gradWeightL_( fw.gradWeightL_ ), gradWeightR_( fw.gradWeightR_ )
    {
       fw.phiL_        = nullptr; fw.phiR_        = nullptr;
       fw.gradphiL_    = nullptr; fw.gradphiR_    = nullptr;
       fw.weightL_     = nullptr; fw.weightR_     = nullptr;
       fw.gradWeightL_ = nullptr; fw.gradWeightR_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phiL_;
      delete [] phiR_;
      delete [] gradphiL_;
      delete [] gradphiR_;
      delete [] weightL_;
      delete [] weightR_;
      delete [] gradWeightL_;
      delete [] gradWeightR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRef, IntegrandType integrandL[], const int nphiL,
      IntegrandType integrandR[], const int nphiR ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldTypeL& qfldElemL_;
    const ElementQFieldTypeL& wfldElemL_;
    const ElementEFieldTypeL& efldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldTypeR& qfldElemR_;
    const ElementQFieldTypeR& wfldElemR_;
    const ElementEFieldTypeR& efldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    const int nPhiL_, nPhiR_;
    mutable Real *phiL_, *phiR_;
    mutable VectorX *gradphiL_, *gradphiR_;
    mutable ArrayQ<T> *weightL_, *weightR_;
    mutable VectorArrayQ<T> *gradWeightL_, *gradWeightR_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL,     TopologyR,
                                                             ElementParamL, ElementParamR  >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>    , TopoDimCell , TopologyR    >& qfldElemR) const
  {
    return {pde_,
            xfldElemTrace, canonicalTraceL, canonicalTraceR,
            paramfldElemL, qfldElemL,
            paramfldElemR, qfldElemR};
  }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,     TopologyR,
                                 ElementParamL, ElementParamR  >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& wfldElemL,
            const Element<Real,      TopoDimCell, TopologyL>& efldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& qfldElemR,
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& wfldElemR,
            const Element<Real,      TopoDimCell, TopologyR>& efldElemR) const
  {
    return {pde_,
            xfldElemTrace, canonicalTraceL, canonicalTraceR,
            paramfldElemL, qfldElemL, wfldElemL, efldElemL,
            paramfldElemR, qfldElemR, wfldElemR, efldElemR };
  }

protected:
  const PDE& pde_;
  const std::vector<int> interiorTraceGroups_;
};


template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR >
bool
IntegrandInteriorTrace_Galerkin_StrongForm<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}

template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR >
bool
IntegrandInteriorTrace_Galerkin_StrongForm<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}


template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR >
void
IntegrandInteriorTrace_Galerkin_StrongForm<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::operator()(
    const QuadPointTraceType& sRefTrace, IntegrandType integrandL[], const int neqnL,
                                         IntegrandType integrandR[], const int neqnR ) const
{
  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  VectorX X;            // physical coordinates
  VectorX nL;           // unit normal for left element (points to right element)

  ArrayQ<T> qL, qR;                // solution
  VectorArrayQ<T> gradqL, gradqR;  // gradient

  QuadPointCellType sRefL;   // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );
  }

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0;

  // viscous flux term: +[[phi]].flux = (phi@L - phi@R) nL.flux
  //                                  = +phiL nL.flux; -phiR nL.flux
  if (pde_.hasFluxViscous())
  {
    ArrayQ<T> Fn = 0; //jump term
    VectorArrayQ<T> Fd = 0; //difference
    VectorArrayQ<T> FL = 0;
    VectorArrayQ<T> FR = 0;

    pde_.fluxViscous( X, qL, gradqL, FL );
    pde_.fluxViscous( X, qR, gradqR, FR );

    Fd = FL-FR;
    Fn = dot(nL, Fd);

    for (int k = 0; k < neqnL; k++)
      integrandL[k] -= phiL_[k]*Fn/2;

    for (int k = 0; k < neqnR; k++)
      integrandR[k] -= phiR_[k]*Fn/2;
  }

  // dual-consistent viscous flux term: -{grad(phi)^t K . n} [[u]]
  //                                    = - 1/2 ( grad(phi)@L K@L + grad(phi)@R K@R ).nL (uL - uR)
  // ref: Oliver_2007_Dual_consistency_for_DG_source_terms

  if (pde_.hasFluxViscous() && pde_.hasFluxAdvectiveTime())
  {
    ArrayQ<T> uL = 0;
    ArrayQ<T> uR = 0;
    DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<T> > KL = 0, KR = 0;
    DLA::VectorS< PhysDim::D, ArrayQ<T> > tmp;
    //MatrixQ<T> kxxL = 0, kxyL = 0, kyxL = 0, kyyL = 0;
    //MatrixQ<T> kxxR = 0, kxyR = 0, kyxR = 0, kyyR = 0;

    pde_.diffusionViscous( X, qL, gradqL, KL );
    pde_.diffusionViscous( X, qR, gradqR, KR );
    pde_.masterState( X, qL, uL );
    pde_.masterState( X, qR, uR );

    ArrayQ<T> du = (uL - uR);

    //tmpx =  0.5*(nxL*kxxL + nyL*kxyL)*(uL - uR);
    //tmpy =  0.5*(nxL*kyxL + nyL*kyyL)*(uL - uR);

    tmp = 0;
    for ( int i = 0; i < PhysDim::D; i++ )
      for ( int j = 0; j < PhysDim::D; j++ )
        tmp[i] += (0.5*KL(i,j)*nL[j])*du;

    for (int k = 0; k < neqnL; k++)
      integrandL[k] -= dot(gradphiL_[k],tmp);

    //tmpx = 0.5*(nxL*kxxR + nyL*kxyR)*(uL - uR);
    //tmpy = 0.5*(nxL*kyxR + nyL*kyyR)*(uL - uR);

    tmp = 0;
    for ( int i = 0; i < PhysDim::D; i++ )
      for ( int j = 0; j < PhysDim::D; j++ )
        tmp[i] += (0.5*KR(i,j)*nL[j])*du;

    for (int k = 0; k < neqnR; k++)
      integrandR[k] -= dot(gradphiR_[k],tmp);
  }
  else if (pde_.hasFluxViscous() && !pde_.hasFluxAdvectiveTime())
  {
    SANS_DEVELOPER_EXCEPTION("dual-consistent viscous flux needs conservation too\n");
  }

  // dual-consistent source term: +phi S

//  if (pde_.hasSource())
//  {
//    ArrayQ<T> sourceL=0, sourceR=0;      // PDE source S(X, D, U, UX), S(X)
//    pde_.sourceTrace( X, X, qL, gradqL, qR, gradqR, sourceL, sourceR );
//
//    for (int k = 0; k < neqnL; k++)
//      integrandL[k] += phiL_[k]*sourceL;
//
//    for (int k = 0; k < neqnR; k++)
//      integrandR[k] += phiR_[k]*sourceR;
//  }
}

template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR >
void
IntegrandInteriorTrace_Galerkin_StrongForm<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::operator()(
    const QuadPointTraceType& sRefTrace, IntegrandType integrandL[], const int nphiL,
    IntegrandType integrandR[], const int nphiR ) const
{
  SANS_ASSERT( nphiL == nPhiL_ && nphiR == nPhiR_ );
  ParamTL paramL;   // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  VectorX X;            // physical coordinates
  VectorX nL;           // unit normal for left element (points to right element)

  ArrayQ<T> qL, qR;                // solution
  VectorArrayQ<T> gradqL=0, gradqR=0;  // solution gradients

  ArrayQ<T> wL, wR;                // weight
  VectorArrayQ<T> gradwL, gradwR;  // weight gradients

  QuadPointCellType sRefL;   // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // solution value, gradient
  qfldElemL_.eval( sRefL, qL );
  qfldElemR_.eval( sRefR, qR );
  if (needsSolutionGradient)
  {
  xfldElemL_.evalGradient( sRefL, qfldElemL_, gradqL );
  xfldElemR_.evalGradient( sRefR, qfldElemR_, gradqR );
  }
  // weight value,
  wfldElemL_.eval( sRefL, wL );
  wfldElemR_.eval( sRefR, wR );
  if (pde_.hasFluxViscous() && pde_.hasFluxAdvectiveTime())
  {
    xfldElemL_.evalGradient( sRefL, wfldElemL_, gradwL );
    xfldElemR_.evalGradient( sRefR, wfldElemR_, gradwR );
  }

  // estimate basis, gradient
  efldElemL_.evalBasis( sRefL, phiL_, nPhiL_);
  efldElemR_.evalBasis( sRefR, phiR_, nPhiR_);
  xfldElemL_.evalBasisGradient( sRefL, efldElemL_, gradphiL_, nPhiL_ );
  xfldElemR_.evalBasisGradient( sRefR, efldElemR_, gradphiR_, nPhiR_ );

  for (int k = 0; k < nPhiL_; k++)
  {
    integrandL[k] = 0;
    weightL_[k] = phiL_[k]*wL;
  }

  for (int k = 0; k < nPhiR_; k++)
  {
    integrandR[k] = 0;
    weightR_[k] = phiR_[k]*wR;
  }

  if (pde_.hasFluxViscous() )
  {
    // Grad (w phi) needed
    for (int k = 0; k < nPhiL_; k++)
      gradWeightL_[k] = gradphiL_[k]*wL + phiL_[k]*gradwL;

    for (int k = 0; k < nPhiR_; k++)
      gradWeightR_[k] = gradphiR_[k]*wR + phiR_[k]*gradwR;
  }

  // viscous flux term:  - [[F]]. {w} =  (FL - FR).nL [ wL/2 ; wR/2 ]
  if (pde_.hasFluxViscous())
  {
    ArrayQ<T> Fn = 0; //jump term
    VectorArrayQ<T> Fd = 0; //difference
    VectorArrayQ<T> FL = 0;
    VectorArrayQ<T> FR = 0;

    pde_.fluxViscous( X, qL, gradqL, FL );
    pde_.fluxViscous( X, qR, gradqR, FR );

    Fd = FL-FR;
    Fn = dot(nL, Fd);
    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] -= dot(weightL_[k],Fn)/2;
    for (int k = 0; k < nPhiR_; k++)
      integrandR[k] -= dot(weightR_[k],Fn)/2;
  }

  // dual-consistent viscous flux term: -{grad(phi)^t K . n} [[u]]
  //                                    = - 1/2 ( grad(phi)@L K@L + grad(phi)@R K@R ).nL (uL - uR)
  // ref: Oliver_2007_Dual_consistency_for_DG_source_terms

  if (pde_.hasFluxViscous() && pde_.hasFluxAdvectiveTime())
  {
    ArrayQ<T> uL = 0;
    ArrayQ<T> uR = 0;
    DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<T> > KL = 0, KR = 0;
    DLA::VectorS< PhysDim::D, ArrayQ<T> > tmp;
    //MatrixQ<T> kxxL = 0, kxyL = 0, kyxL = 0, kyyL = 0;
    //MatrixQ<T> kxxR = 0, kxyR = 0, kyxR = 0, kyyR = 0;

    pde_.diffusionViscous( X, qL, gradqL, KL );
    pde_.diffusionViscous( X, qR, gradqR, KR );
    pde_.masterState( X, qL, uL );
    pde_.masterState( X, qR, uR );

    ArrayQ<T> du = (uL - uR);

    //tmpx =  0.5*(nxL*kxxL + nyL*kxyL)*(uL - uR);
    //tmpy =  0.5*(nxL*kyxL + nyL*kyyL)*(uL - uR);

    tmp = 0;
    for ( int i = 0; i < PhysDim::D; i++ )
      for ( int j = 0; j < PhysDim::D; j++ )
        tmp[i] += (0.5*KL(i,j)*nL[j])*du;

    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] -= dot(gradWeightL_[k],tmp);

    //tmpx = 0.5*(nxL*kxxR + nyL*kxyR)*(uL - uR);
    //tmpy = 0.5*(nxL*kyxR + nyL*kyyR)*(uL - uR);

    tmp = 0;
    for ( int i = 0; i < PhysDim::D; i++ )
      for ( int j = 0; j < PhysDim::D; j++ )
        tmp[i] += (0.5*KR(i,j)*nL[j])*du;

    for (int k = 0; k < nPhiR_; k++)
      integrandR[k] -= dot(gradWeightR_[k],tmp);
  }
  else if (pde_.hasFluxViscous() && !pde_.hasFluxAdvectiveTime())
  {
    SANS_DEVELOPER_EXCEPTION("dual-consistent viscous flux needs conservation too\n");
  }

  // This messes up reaction diffusion for Galerkin -- Hugh
  // Run the ErrorEstimateOrder_2D_CG_RD case to see
//  // dual-consistent source term: +w S
//  if (pde_.hasSource())
//  {
//    ArrayQ<T> sourceL=0, sourceR=0;      // PDE source S(X, D, U, UX), S(X)
//    pde_.sourceTrace( X, X, qL, gradqL, qR, gradqR, sourceL, sourceR );
//
//    integrandL += dot(wL,sourceL) * xfldElemL_.jacobianDeterminant() / xfldElemTrace_.jacobianDeterminant();
//    integrandR += dot(wR,sourceR) * xfldElemR_.jacobianDeterminant() / xfldElemTrace_.jacobianDeterminant();
//  }
}

}

#endif //IntegrandInteriorTrace_Galerkin_StrongForm_H
