// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_CUTCELLTRANSITIONIBLUNIFIELD_H_
#define SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_CUTCELLTRANSITIONIBLUNIFIELD_H_

#include "Discretization/GroupIntegral_Type.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/SurrealizedElementTuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Surreal/SurrealS.h"

#include "JacobianCell_Galerkin.h"
#include "IntegrandCell_Galerkin_CutCellTransitionIBLUniField.h"

namespace SANS
{
//----------------------------------------------------------------------------//
//  Galerkin cell integral
//
template<class VarType, template<class, class> class PDENDConvert>
class JacobianCell_Galerkin_impl<
        IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > >
                                > :
    public GroupIntegralCellType<
             JacobianCell_Galerkin_impl<
               IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > >
                                       >
                                >
{
public:
  typedef PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > NDPDEClass;
  typedef IntegrandCell_Galerkin_cutCellTransitionIBLUniField<NDPDEClass> IntegrandCellClass;

  typedef typename IntegrandCellClass::PhysDim PhysDim;
  typedef typename IntegrandCellClass::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCellClass::template MatrixQ<Real> MatrixQ;

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  typedef typename IntegrandCellClass::template ArrayQ<SurrealClass> ArrayQSurreal;

  typedef typename DLA::VectorD<ArrayQSurreal> ResidualElemSurrealClass;
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  static const int iParam = 0; // TODO: hard coded index for parameter field (Qauxv) in the tuple field of IBL

  JacobianCell_Galerkin_impl(const IntegrandCellClass& fcn,
                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template<class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                     ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename SurrealizedElementTuple<XFieldCellGroupType, SurrealClass, iParam>::type ElementXFieldClass;
    // TODO: always Surrealizing the tuple field element is easy to implement but expensive to run
    typedef typename QFieldCellGroupType::template ElementType<SurrealClass> ElementQFieldClass;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // DOFs per element
    const int nDOFPDE = qfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nDOFPDE, -1 );
    std::vector<int> mapDOFLocal( nDOFPDE, -1 );
    std::vector<int> maprsd( nDOFPDE, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = mtxGlobalPDE_q_.m();

    // element jacobian matrix
    ResidualElemSurrealClass rsdElemPDE( nDOFPDE );

    MatrixElemClass mtxPDEElem(nDOFPDE, nDOFPDE);
    MatrixElemClass mtxPDEElemLocal(nDOFPDE, nDOFPDE);

    // element integral
    GalerkinWeightedIntegral_New_TransitionIBL<TopoDim, Topology, ResidualElemSurrealClass> integral(quadratureorder);

    // The integrand to be integrated
    auto integrand = fcn_.integrand( xfldElem, qfldElem );

    // variables/equations per DOF
    const int nEqn = IntegrandCellClass::N;

    // number of simultaneous derivatives (i.e. chunk) automatic differentiation functor call
    SANS_ASSERT_MSG(DLA::index(qfldElem.DOF(0), 0).size() == nEqn, "Assumed AD derivative count fails!");
    const int nDeriv = DLA::index(qfldElem.DOF(0), 0).size();

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), mapDOFGlobal.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOFPDE; n++)
        if (mapDOFGlobal[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
          nDOFLocal++;
        }

      // no residuals are possessed by this processor
      if (nDOFLocal == 0) continue;

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // compute jacobians via automatic differentiation & loop over derivative chunks &
      for (int nchunk = 0; nchunk < nEqn*nDOFPDE; nchunk += nDeriv)
      {
        // associate derivative slots with solution variables
        int slot;
        for (int j = 0; j < nDOFPDE; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElem.DOF(j), n).deriv(k) = 0.0; // TODO: this is safe but can be slow

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElem.DOF(j), n).deriv(slot - nchunk) = 1.0;
          }
        }

        // reset residuals
        rsdElemPDE = 0.0;

        // evaluate surrealized element residual
        integral( integrand, get<-1>(xfldElem), rsdElemPDE );
        // xfld should always be the last component of tuplefldElem

        // accumulate derivatives into element jacobians

        for (int j = 0; j < nDOFPDE; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFLocal; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElemLocal(i,j), m,n) = DLA::index(rsdElemPDE[i], m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global
      if (nDOFLocal == nDOFPDE)
      {
        scatterAdd( elem, mapDOFLocal, mapDOFGlobal, mtxPDEElemLocal, mtxGlobalPDE_q_ );
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION("Not fully implemented yet!"); //TODO

//        // compress in only the DOFs possessed by this processor
//        for (int i = 0; i < nDOFPDELocal; i++)
//          for (int j = 0; j < nDOFPDE; j++)
//            mtxElemLocalPDE_q(i,j) = mtxElemPDE_q(maprsdPDE[i],j);
//
//        scatterAdd( elem, mapDOFPDELocal, mapDOFPDEGlobal, mtxElemLocalPDE_q, mtxGlobalPDE_q_ );
      }
    }
  }

protected:
  //----------------------------------------------------------------------------//
  template <class MatrixQ, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const int elem,
      std::vector<int>& mapDOFLocal,
      std::vector<int>& mapDOFGlobal,
      SANS::DLA::MatrixD<MatrixQ>& mtxElem,
      SparseMatrixType<MatrixQ>& mtxGlobal )
  {
    mtxGlobal.scatterAdd( mtxElem, mapDOFLocal.data(), mapDOFLocal.size(), mapDOFGlobal.data(), mapDOFGlobal.size() );
  }

protected:
  const IntegrandCellClass& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
};

} //namespace SANS


#endif /* SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_CUTCELLTRANSITIONIBLUNIFIELD_H_ */
