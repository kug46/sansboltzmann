// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFUNCTOR_FLUX_MITSTATE_GALERKIN_H
#define INTEGRANDBOUNDARYFUNCTOR_FLUX_MITSTATE_GALERKIN_H

// boundary integrand operators

#include <ostream>
#include <vector>
#include <type_traits> //std::is_same
#include <cmath>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Integrand_Galerkin_fwd.h"
#include "Stabilization_Nitsche.h"

namespace SANS
{

// forward declaration
class BCTypeReflect_mitState;

//----------------------------------------------------------------------------//
// element boundary integrand: PDE


template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, Galerkin> :
public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, Galerkin> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Flux_mitState Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;    // solution/residual jacobians

  static const int N = PDE::N;
  static const int D = PDE::D;

  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const StabilizationNitsche& stab)
  : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups), stab_(stab) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
  class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template<class Ti>
    using IntegrandType = ArrayQ<Ti>;

    BasisWeighted( const PDE& pde, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const StabilizationNitsche& stab) :
                     pde_(pde), bc_(bc),
                     xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem),
                     paramfldElem_( paramfldElem ),
                     nDOF_(qfldElem_.nDOF()),
                     phi_( new Real[nDOF_] ),
                     gradphi_( new VectorX[nDOF_] ),
                     stab_(stab),
                     invL_(xfldElemTrace_.jacobianDeterminant()/xfldElem_.jacobianDeterminant()),
                     Cb_(stab_.getNitscheConstant(D))
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                     pde_(bw.pde_), bc_(bw.bc_),
                     xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                     xfldElem_(bw.xfldElem_),
                     qfldElem_(bw.qfldElem_),
                     paramfldElem_( bw.paramfldElem_ ),
                     nDOF_( bw.nDOF_ ),
                     phi_( bw.phi_ ),
                     gradphi_( bw.gradphi_ ),
                     stab_(bw.stab_),
                     invL_(bw.invL_),
                     Cb_(bw.Cb_)
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandType<Ti> integrand[], int neqn ) const;

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Tq, class Tg, class Ti>
    void operator()( const BC& bc, const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
                     const ParamT& paramI, const VectorX& nL, ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    const StabilizationNitsche& stab_;
    const Real invL_;
    const Real Cb_;
  };

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
  class TopoDimCell,  class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<ArrayQ<Tw>, TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<Real,       TopoDimCell, Topology> ElementEFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type IntegrandType;

    FieldWeighted( const PDE& pde, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementWFieldCell& wfldElem,
                   const ElementEFieldCell& efldElem,
                   const StabilizationNitsche& stab ) :
                     pde_(pde), bc_(bc),
                     xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem), wfldElem_(wfldElem), efldElem_(efldElem),
                     paramfldElem_( paramfldElem ),
                     nDOF_(qfldElem_.nDOF()),
                     nPhi_(efldElem_.nDOF()),
                     ephi_( new Real[nPhi_] ),
                     gradephi_( new VectorX[nPhi_] ),
                     weight_( new ArrayQ<Tw>[nPhi_] ),
                     gradWeight_( new VectorArrayQ<Tw>[nPhi_] ),
                     gradphi_( new VectorX[nDOF_] ),
                     stab_(stab),
                     invL_(xfldElemTrace_.jacobianDeterminant()/xfldElem_.jacobianDeterminant()),
                     Cb_(stab_.getNitscheConstant(D))
    {
      // Nothing
    }

    FieldWeighted( FieldWeighted&& fw ) :
                     pde_(fw.pde_), bc_(fw.bc_),
                     xfldElemTrace_(fw.xfldElemTrace_), canonicalTrace_(fw.canonicalTrace_),
                     xfldElem_(fw.xfldElem_),
                     qfldElem_(fw.qfldElem_), wfldElem_(fw.wfldElem_), efldElem_(fw.efldElem_),
                     paramfldElem_( fw.paramfldElem_ ),
                     nDOF_(fw.nDOF_),
                     nPhi_(fw.nPhi_),
                     ephi_( fw.ephi_ ),
                     gradephi_( fw.gradephi_ ),
                     weight_( fw.weight_ ),
                     gradWeight_( fw.gradWeight_ ),
                     gradphi_( fw.gradphi_ ),
                     stab_(fw.stab_),
                     invL_(fw.invL_),
                     Cb_(fw.Cb_)
    {
      fw.ephi_       = nullptr;
      fw.gradephi_   = nullptr;
      fw.weight_     = nullptr;
      fw.gradWeight_ = nullptr;
      fw.gradphi_    = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] ephi_;
      delete [] gradephi_;
      delete [] weight_;
      delete [] gradWeight_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // element trace integrand
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType integrand[], const int nphi ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrand, nphi);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType integrand[], const int nphi ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementEFieldCell& efldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_, nPhi_;
    mutable Real *ephi_;
    mutable VectorX *gradephi_;
    mutable ArrayQ<Tw> *weight_;
    mutable VectorArrayQ<Tw> *gradWeight_;
    mutable VectorX *gradphi_;
    const StabilizationNitsche& stab_;
    const Real invL_;
    const Real Cb_;
  };


  template<class T, class TopoDimTrace, class TopologyTrace,
  class TopoDimCell, class Topology, class ElementParam>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem) const
            {
    return {pde_, bc_,
      xfldElemTrace, canonicalTrace,
      paramfldElem, qfldElem, stab_ };
            }

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
  class TopoDimCell, class Topology, class ElementParam>
  FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<Tq>, TopoDimCell, Topology>& qfldElem,
            const Element<ArrayQ<Tw>, TopoDimCell, Topology>& wfldElem,
            const Element<Real,       TopoDimCell, Topology>& efldElem) const
  {
    return {pde_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem,
            qfldElem, wfldElem, efldElem, stab_};
  }
private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const StabilizationNitsche& stab_;
};

template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT paramL;             // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;                // unit normal (points out of domain)

  ArrayQ<T> qI;              // interior solution
  VectorArrayQ<T> gradqI;    // interior gradient

  QuadPointCellType sRef;    // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // solution value
  qfldElem_.evalFromBasis( phi_, nDOF_, qI );

  // gradients
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradqI );

  call_with_derived<NDBCVector>(*this, bc_, qI, gradqI, paramL, nL, integrand, neqn);
}

template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
            const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrand[], int neqn ) const
{
//  SANS_ASSERT_MSG( !(std::is_same<typename BC::BCType, BCTypeReflect_mitState>::value), "Not tested for reflection BC yet" );

  // BC state
  ArrayQ<T> qB;
  bc.state( paramL, nL, qI, qB );

  // normal flux
  ArrayQ<T> Fn = 0;
  bc.fluxNormal( paramL, nL, qI, gradqI, qB, Fn );

  // PDE residual: weak form boundary integral
  for (int k = 0; k < neqn; k++)
    integrand[k] = phi_[k]*Fn;

  if (bc.hasFluxViscous())
  {

    // viscous flux dual consistency term
    ArrayQ<T> dq = qI - qB;
    VectorArrayQ<Ti> Kndu = 0;
    if (pde_.fluxViscousLinearInGradient())
    {
      VectorArrayQ<Tq> dqpN = 0;
      for (int i=0; i<PhysDim::D; i++)
        dqpN[i] -= dq*nL[i];  //negate because fluxViscous returns -K*duN

      //THIS IS A FUNCTION THAT DOES K*(uI -uB)
      pde_.fluxViscous(paramL, qB, dqpN, Kndu);
    }
    else
    {
      // viscous flux dual consistency term
      MatrixQ<T> dudq = 0;
      pde_.jacobianMasterState( paramL, qB, dudq );

      ArrayQ<T> du = dudq*dq;
      VectorArrayQ<Tq> duN = 0;

      for (int i=0; i<PhysDim::D; i++)
        duN[i] += du*nL[i];  //negate because perturbedGradFluxViscous returns -K*duN

      DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<Ti>> K = 0;   // diffusion matrix
      pde_.diffusionViscous( paramL, qB, gradqI, K );

      Kndu = K*duN;
    }

    //DUAL CONSISTENCY
    for (int k = 0; k < neqn; k++)
      integrand[k] -= dot(gradphi_[k],Kndu);

    //SIP
    ArrayQ<Ti> tmpC = Cb_*invL_*dot(nL, Kndu);

    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*tmpC;
  }

}


template <class PDE, class NDBCVector>
template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
class TopoDimCell,  class Topology, class ElementParam >
template<class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, Galerkin>::
FieldWeighted<Tq, Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType integrand[], const int nphi ) const
{
//  SANS_ASSERT_MSG( !(std::is_same<typename BC::BCType, BCTypeReflect_mitState>::value), "Not tested for reflection BC yet" );
  SANS_ASSERT( nphi == nPhi_ );

  ParamT paramL;             // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;                // unit normal (points out of domain)

  ArrayQ<Tq> qI;             // interior solution
  ArrayQ<Tw> w;              // interior weight
  VectorArrayQ<Tq> gradqI;   // interior gradient
  ArrayQ<Tq> qB;             // boundary solution

  QuadPointCellType sRef;    // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental paramters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL);

  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // basis value, gradient
  qfldElem_.eval( sRef, qI );
  wfldElem_.eval( sRef, w );

  // solution value, gradient
  if (needsSolutionGradient)
    xfldElem_.evalGradient( sRef, qfldElem_, gradqI );
  else
    gradqI = 0;

  VectorArrayQ<Tw> gradwI;   // interior gradient
  xfldElem_.evalGradient( sRef, wfldElem_, gradwI );

  // estimate basis
  efldElem_.evalBasis( sRef, ephi_, nPhi_ );
  xfldElem_.evalBasisGradient( sRef, efldElem_, gradephi_, nPhi_ );

  for (int k = 0; k < nPhi_; k++)
  {
    weight_[k] = ephi_[k]*w;
    gradWeight_[k] = gradephi_[k]*w + ephi_[k]*gradwI;
  }

  // BC state
  bc.state( paramL, nL, qI, qB );

  // normal flux
  ArrayQ<Tq> Fn = 0;
  bc.fluxNormal( paramL, nL, qI, gradqI, qB, Fn );

  // PDE residual: weak form boundary integral
  for (int k = 0; k < nPhi_; k++)
    integrand[k] = dot(weight_[k],Fn);

  if ( bc.hasFluxViscous() )
  {

    VectorArrayQ<Tq> Kndu = 0;
    ArrayQ<Tq> dq = qI - qB;
    if (pde_.fluxViscousLinearInGradient())
    {
      // if flux is linear in gradient, can just call the viscous flux
      VectorArrayQ<Tq> dqpN = 0;
      for (int i=0; i<PhysDim::D; i++)
        dqpN[i] -= dq*nL[i];  //negate because perturbedGradFluxViscous returns -K*duN

      //THIS IS A FUNCTION THAT DOES K*(uI -uB)
      pde_.fluxViscous(paramL, qB, dqpN, Kndu);
    }
    else
    {
      // if flux isn't linear in gradient, have to construct K*du
      MatrixQ<Tq> dudq = 0;
      pde_.jacobianMasterState( paramL, qB, dudq );
      ArrayQ<Tq> du = dudq*dq;
      VectorArrayQ<Tq> duN = 0;

      for (int i=0; i<PhysDim::D; i++)
        duN[i] += du*nL[i];

      DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<Tq>> K = 0;   // diffusion matrix
      pde_.diffusionViscous( paramL, qB, gradqI, K );

      Kndu = K*duN;
    }

      for (int k = 0; k < nPhi_; k++)
        integrand[k] -= dot(gradWeight_[k],Kndu);

      ArrayQ<Tq> tmpC = Cb_*invL_*dot(nL,Kndu);

      for (int k = 0; k < nPhi_; k++)
        integrand[k] += dot(weight_[k],tmpC);

  }




}

}

#endif  // INTEGRANDBOUNDARYFUNCTOR_FLUX_MITSTATE_GALERKIN_H
