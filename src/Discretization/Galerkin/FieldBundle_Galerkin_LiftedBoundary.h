// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDBUNDLE_GALERKIN_LIFTEDBOUNDARY_H_
#define FIELDBUNDLE_GALERKIN_LIFTEDBOUNDARY_H_

#include "Field/Field.h"
#include "Field/FieldTypes.h"
#include "Field/FieldLift.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/FieldLift_Local.h"

#include "tools/split_cat_std_vector.h" // SANS::cat in derived constructors

namespace SANS
{

/** FieldBundleBase_Galerkin is a container for the continuous solution field bundles
 *
 * FieldBundleBase_Galerkin is a container for continous solution fields and it gives
 * a user the ability to stash the solution field, an associated Lagrange field on a
 * boundary, as well as the metadata (order, spacetype, etc) and the mesh field that
 * describe the field that is stored within it, and some tools to work with it.
 */
template< class PhysD, class TopoD, class ArrayQ_>
struct FieldBundleBase_Galerkin_LiftedBoundary
{

  // typedef off the inputs
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  // the constructor: initializes everything
  FieldBundleBase_Galerkin_LiftedBoundary( const XField<PhysDim, TopoDim>& xfld,
                            Field_CG_Cell< PhysDim, TopoDim, ArrayQ >& qfld,
                            FieldLift_DG_Cell< PhysDim, TopoDim, VectorArrayQ >& rfld,
                            Field_CG_BoundaryTrace< PhysDim, TopoDim, ArrayQ >& lgfld,
                            const int order,
                            const BasisFunctionCategory basis_cell,
                            const BasisFunctionCategory basis_trace ) :
  xfld(xfld), qfld(qfld), rfld(rfld), lgfld(lgfld), order(order),
  basis_cell(basis_cell), basis_trace(basis_trace) {}

  // ???
  void projectTo(FieldBundleBase_Galerkin_LiftedBoundary& bundleTo)
  {
    qfld.projectTo(bundleTo.qfld);
    rfld.projectTo(bundleTo.rfld);
    lgfld.projectTo(bundleTo.lgfld);
  }

  // the mesh field on which the solution is stored
  const XField<PhysDim,TopoDim>& xfld;

  // the CG cell field for storing the solution
  Field_CG_Cell<PhysDim,TopoDim,ArrayQ>& qfld;
  FieldLift_DG_Cell<PhysDim,TopoDim,VectorArrayQ>& rfld;
  // the CG boundary field for storing Lagrange multiplier fields (if necessary)
  Field_CG_BoundaryTrace<PhysDim, TopoDim, ArrayQ>& lgfld;

  // polynomial order stored in this field bundle
  const int order;

  // specify the basis functions used here
  const BasisFunctionCategory basis_cell;
  const BasisFunctionCategory basis_trace;

  // the type of solution space stored in this field bundle (e.g. continuous v. discontinuous)
  static constexpr SpaceType spaceType = SpaceType::Continuous;

//  SpaceType spaceType() { return SpaceType::Continuous; }

  void dump() { qfld.dump(); }
};

// forward declare the class specialization so input args for local work
template< class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_Galerkin_LiftedBoundary;

// public of GlobalBundle is so that AlgebraicEquationSet Basetype works with it
template<class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_Galerkin_LiftedBoundary_Local :
    public FieldBundleBase_Galerkin_LiftedBoundary<PhysD, TopoD, ArrayQ_>
{
  typedef FieldBundleBase_Galerkin_LiftedBoundary<PhysD, TopoD, ArrayQ_> BaseType;

  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef Field_CG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef FieldLift_DG_Cell<PhysDim,TopoDim,VectorArrayQ> RFieldType;
  typedef Field_CG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef Field_Local<QFieldType> QFieldType_Local;
  typedef FieldLift_Local<RFieldType> RFieldType_Local;
  typedef Field_Local<LGFieldType> LGFieldType_Local;

  // active_local_BGroup_list is a vec of vec so that boundaries that came from the same original global trace
  // continue to have shared dofs in the local field.
  FieldBundle_Galerkin_LiftedBoundary_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_Galerkin_LiftedBoundary<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<std::vector<int>>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_, rfld_, lgfld_,order, globalfields.basis_cell, globalfields.basis_trace),
    qfld_( xfld_local, globalfields.qfld, order, globalfields.basis_cell ),
    rfld_( xfld_local, globalfields.rfld , order, globalfields.basis_cell ),
    lgfld_( active_local_BGroup_list, xfld_local, globalfields.lgfld, order, globalfields.basis_trace ) // Broken Groups
//    lgfld_( xfld_local, globalfields.lgfld, order, SANS::cat(active_local_BGroup_list) ) // Unbroken Groups
  {
  }

  // Breaks up the groups in-situ
  FieldBundle_Galerkin_LiftedBoundary_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_Galerkin_LiftedBoundary<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<int>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_, rfld_, lgfld_,order,globalfields.basis_cell,globalfields.basis_trace),
    qfld_( xfld_local, globalfields.qfld, order, globalfields.basis_cell ),
    rfld_ ( xfld_local, globalfields.rfld , order  , globalfields.basis_cell ),
//    lgfld_( SANS::split(active_local_BGroup_list), xfld_local, globalfields.lgfld, order ) // Broken Groups
    lgfld_( xfld_local, globalfields.lgfld, order, globalfields.basis_trace, active_local_BGroup_list ) // Unbroken Groups
  {
  }

  // promote base variable for order
  using BaseType::order;

  using BaseType::basis_cell;
  using BaseType::basis_trace;

protected:

  // the solution field
  QFieldType_Local qfld_;
  RFieldType_Local rfld_;
  // the trace Lagrange field
  LGFieldType_Local lgfld_;
};

template<class PhysD, class TopoD, class ArrayQ_>
struct FieldBundle_Galerkin_LiftedBoundary :
    public FieldBundleBase_Galerkin_LiftedBoundary<PhysD, TopoD, ArrayQ_>
{
  // base class
  typedef FieldBundleBase_Galerkin_LiftedBoundary<PhysD, TopoD, ArrayQ_> BaseType;

  // shorthand references for the stuff passed in
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef Field_CG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef FieldLift_DG_Cell<PhysDim,TopoDim,VectorArrayQ> RFieldType;
  typedef Field_CG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef FieldBundle_Galerkin_LiftedBoundary_Local<PhysD,TopoD,ArrayQ> FieldBundle_Local;

  // constructor: initializes everything, including the base class
  FieldBundle_Galerkin_LiftedBoundary( const XField<PhysDim, TopoDim>& xfld, const int order,
                        const BasisFunctionCategory basis_cell, const BasisFunctionCategory basis_trace,
                        const std::vector<int>& active_BGroup_list )
  : BaseType(xfld, qfld_, rfld_, lgfld_,order, basis_cell, basis_trace),
    qfld_( xfld, order, basis_cell ),
    rfld_( xfld, order, basis_cell ),
    lgfld_( xfld, order, basis_trace, active_BGroup_list ) // Unbroken groups
  {
  }

  // a pseudo copy constructor
  FieldBundle_Galerkin_LiftedBoundary( const XField<PhysDim,TopoDim>&xfld, const int order,
                                   const BaseType& flds,
                                   const std::vector<int>& active_BGroup_list )
  : BaseType(xfld, qfld_, rfld_, lgfld_, order, flds.basis_cell, flds.basis_trace ),
    qfld_( xfld, order, flds.basis_cell),
    rfld_( xfld, order, flds.basis_cell ),
    lgfld_( xfld, order, flds.basis_trace, active_BGroup_list)
  {
  }


  // promote base variable order (no need to duplicate it)
  using BaseType::order;

  using BaseType::basis_cell;
  using BaseType::basis_trace;

protected:
  // the solution field
  QFieldType qfld_;
  RFieldType rfld_;
  // the trace Lagrange field
  LGFieldType lgfld_;
};



}

#endif /* FIELDBUNDLE_GALERKIN_LIFTEDBOUNDARY_H_ */
