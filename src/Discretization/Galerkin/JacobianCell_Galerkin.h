// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_GALERKIN_H
#define JACOBIANCELL_GALERKIN_H

// cell integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{
// forward declaration
template<class IntegrandCell>
class JacobianCell_Galerkin_impl;

//----------------------------------------------------------------------------//
//  Galerkin cell integral
//

template<class IntegrandCell>
class JacobianCell_Galerkin_impl :
    public GroupIntegralCellType< JacobianCell_Galerkin_impl<IntegrandCell> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianCell_Galerkin_impl(const IntegrandCell& fcn,
                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    const int qm = qfld.nDOFpossessed();
    const int qn = qfld.nDOFpossessed() + qfld.nDOFghost();

    SANS_ASSERT_MSG(mtxGlobalPDE_q_.m() == qfld.nDOFpossessed()                   , "%d != %d", mtxGlobalPDE_q_.m(), qm);
    SANS_ASSERT_MSG(mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost(), "%d != %d", mtxGlobalPDE_q_.n(), qn);
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template<class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                     ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // DOFs per element
    const int nDOF = qfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nDOF, -1 );
    std::vector<int> mapDOFLocal( nDOF, -1 );
    std::vector<int> maprsd( nDOF, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = mtxGlobalPDE_q_.m();

    // element jacobian matrix
    MatrixElemClass mtxPDEElem(nDOF, nDOF);
    MatrixElemClass mtxPDEElemLocal(nDOF, nDOF);

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, MatrixElemClass> integral(quadratureorder);

    // The integrand to be integrated
    auto integrand = fcn_.integrand( xfldElem, qfldElem );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), mapDOFGlobal.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOF; n++)
        if (mapDOFGlobal[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
          nDOFLocal++;
        }

      // no residuals are possessed by this processor
      if (nDOFLocal == 0) continue;

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // cell integration for canonical element
      integral( integrand, get<-1>(xfldElem), mtxPDEElem );

      // scatter-add element jacobian to global
      if (nDOFLocal == nDOF)
        scatterAdd( elem, mapDOFGlobal, nDOFLocal, mapDOFGlobal, mtxPDEElem, mtxGlobalPDE_q_ );
      else
      {
        // compress in only the DOFs possessed by this processor
        for (int i = 0; i < nDOFLocal; i++)
          for (int j = 0; j < nDOF; j++)
            mtxPDEElemLocal(i,j) = mtxPDEElem(maprsd[i],j);

        scatterAdd( elem, mapDOFLocal, nDOFLocal, mapDOFGlobal, mtxPDEElemLocal, mtxGlobalPDE_q_ );
      }
    }
  }

protected:
  //----------------------------------------------------------------------------//
  template <class MatrixQ, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const int elem,
      std::vector<int>& mapDOFLocal,
      const int nDOFLocal,
      std::vector<int>& mapDOFGlobal,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElem,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElem, mapDOFLocal.data(), nDOFLocal, mapDOFGlobal.data(), mapDOFGlobal.size() );
  }

#if 0
  //----------------------------------------------------------------------------//
  template <class MatrixQ, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const int elem,
      std::vector<int>& mapDOFLocal,
      std::vector<int>& mapDOFGlobal,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElem,
      SparseMatrixType< SANS::DLA::MatrixD< MatrixQ > >& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElem, elem, elem );
  }
#endif
protected:
  const IntegrandCell& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
};


// Factory function

template<class IntegrandCell, class MatrixQ>
JacobianCell_Galerkin_impl<IntegrandCell>
JacobianCell_Galerkin( const IntegrandCellType<IntegrandCell>& fcn,
                       MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q)
{
  return { fcn.cast(), mtxGlobalPDE_q };
}


} //namespace SANS

#endif  // JACOBIANCELL_GALERKIN_H
