// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONDATA_GALERKIN_STABILIZED_H_
#define SOLUTIONDATA_GALERKIN_STABILIZED_H_

#include "Adaptation/MOESS/ParamFieldBuilder.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/ProjectSoln/ProjectConstTrace.h"
#include "Field/ProjectSoln/ProjectGlobalField.h"
#include "Field/ProjectSoln/InterpolateFunctionCell_Continuous.h"
#include "FieldBundle_Galerkin.h"
#include "Stabilization_Galerkin.h"
#include "Field/output_Tecplot.h"

namespace SANS
{

/** SolutionData_Galerkin_Stabilized is a container for all CG data needed to solve
 *
 * SolutionData_Galerkin_Stabilized is a container that holds all of the data needed
 * to solve a continuous Galerkin system with stabilization as well as holding the
 * solutions and the data needed to render them useful!
 */
template<class PhysDim, class TopoDim, class NDPDEClass, class ParamBuilderType>
struct SolutionData_Galerkin_Stabilized
{
  // typedef the solution type
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  // typedef the fieldbundles we work with here
  typedef FieldBundle_Galerkin<PhysDim, TopoDim, ArrayQ> FieldBundle;

  typedef ParamFieldBuilder<ParamBuilderType, PhysDim, TopoDim,
                            typename FieldBundle::QFieldType> ParamFieldBuilderType;
  typedef ParamFieldBuilder_Local<ParamBuilderType, PhysDim, TopoDim,
                                  typename FieldBundle::QFieldType> ParamFieldBuilderLocalType;

  typedef typename ParamFieldBuilderType::FieldType ParamFieldType;
  typedef Field_CG_Cell<PhysDim, TopoDim, Real> LiftedQuantityFieldType;

  // constructor to create solution data
  template<class XFieldType>
  SolutionData_Galerkin_Stabilized(const XFieldType& xfld_, const NDPDEClass& pde,
                     const int primal_order, const int adjoint_order,
                     const BasisFunctionCategory basis_cell, const BasisFunctionCategory basis_trace,
                     const std::vector<int>& mitlg_boundarygroups,
                     const StabilizationMatrix& stab)
  : SolutionData_Galerkin_Stabilized(xfld_, pde, primal_order, adjoint_order,
                       basis_cell, basis_trace, mitlg_boundarygroups, PyDict(), stab) {}

  // constructor to create solution data
  template<class XFieldType>
  SolutionData_Galerkin_Stabilized(const XFieldType& xfld_, const NDPDEClass& pde,
                     const int primal_order, const int adjoint_order,
                     const BasisFunctionCategory basis_cell, const BasisFunctionCategory basis_trace,
                     const std::vector<int>& mitlg_boundarygroups, const PyDict& parambuilderDict,
                     const StabilizationMatrix& stab)
  : xfld(get<-1>(xfld_)),
    pde(pde),
    primal (xfld, primal_order , basis_cell, basis_trace, mitlg_boundarygroups),
    adjoint(xfld, adjoint_order, basis_cell, basis_trace, mitlg_boundarygroups),
    mitlg_boundarygroups(mitlg_boundarygroups),
    parambuilder(xfld_, primal.qfld, parambuilderDict),
    paramfld(parambuilder.fld),
    disc(stab)
  {
  }

  // set the solution fields to an input q
  void setSolution(const typename FieldBundle::ArrayQ& q)
  {
    primal.qfld = q;
    primal.lgfld = q;
  }


  template <class SolutionFunctionType>
  void setSolution(const SolutionFunctionType& fcn, const std::vector<int>& cellGroups)
  {
    for_each_CellGroup<TopoDim>::apply( InterpolateFunctionCell_Continuous(fcn, cellGroups), (xfld, primal.qfld) );
  }

  /** setSolution sets a solution from another CG SolutionData
   *
   * setSolution takes another SolutionData_Galerkin_Stabilized and sets the primal solution
   * here equal to the solution on the other object by local projection _if the grids are
   * the same_, otherwise it projects from the other grid's primal field  onto thIS primal
   * CG field!
   */
  void setSolution(const SolutionData_Galerkin_Stabilized& solFrom)
  {
    if (&xfld == &solFrom.xfld)
    {
      solFrom.primal.qfld.projectTo(primal.qfld);
    }
    else //need to project between meshes
    {
      ProjectGlobalField(solFrom.primal.qfld, primal.qfld);
    }
  }

  /** setBoundarySolution sets the solution on the boundary group (needed before essential BCs get applied)
   *
   * setBoundarySolution sets the solution on the boundary group. This is needed before essential BCs get
   * applied. It does so by locally projecting onto each boundary trace group.
   */
  void setBoundarySolution( const typename FieldBundle::ArrayQ& q, const std::vector<int>& boundaryGroups )
  {
    // Assign the value to the trace
    for_each_BoundaryTraceGroup<TopoDim>::apply( ProjectConstTrace<PhysDim>(q, boundaryGroups), *this );
  }

  /** setBoundarySolution sets the solution on the boundary group (needed before essential BCs get applied)
   *
   * setBoundarySolution sets the solution on the boundary group. This is needed before essential BCs get
   * applied. It does so by interpolating the solution function onto each boundary trace group.
   */
  template <class SolutionFunctionType>
  void setBoundarySolution( const SolutionFunctionType& fcn, const std::vector<int>& boundaryGroups )
  {
    // Assign the value to the trace
    for_each_BoundaryTraceGroup<TopoDim>::apply( InterpolateFunctionTrace_Continuous(fcn,boundaryGroups), (xfld, primal.qfld) );
  }

  /**
   *
   */
  void createLiftedQuantityField(const int order, const BasisFunctionCategory& category)
  {
    orderLiftedQuantity = order;
    basisCategoryLiftedQuantity = category;
    pliftedQuantityfld = std::make_shared<LiftedQuantityFieldType>(xfld, orderLiftedQuantity, basisCategoryLiftedQuantity);
  }

  void dumpPrimalSolution( const std::string filebase, const std::string tag) const
  {
    output_Tecplot( primal.qfld, filebase + "qfld" + tag );
  }


  // the underlying mesh field!
  const XField<PhysDim, TopoDim>& xfld;

  // the type of space that we're working with here
  static constexpr SpaceType spaceType = SpaceType::Continuous;

  // the PDE that is being solved here
  const NDPDEClass& pde;

  // solution field
  FieldBundle primal;

  // adjoint fields in richer space (P+1)
  FieldBundle adjoint;

  // lifted quantity field (i.e. shock sensor field)
  std::shared_ptr<LiftedQuantityFieldType> pliftedQuantityfld;
  int orderLiftedQuantity = 1;
  BasisFunctionCategory basisCategoryLiftedQuantity= BasisFunctionCategory_Hierarchical;

  // lagrange boundary groups
  std::vector<int> mitlg_boundarygroups;

  ParamFieldBuilderType parambuilder;
  const ParamFieldType& paramfld; //parameter fields

  // stabilization specification
  const StabilizationMatrix& disc;
};

}

#endif /* SOLUTIONDATA_GALERKIN_STABILIZED_H_ */
