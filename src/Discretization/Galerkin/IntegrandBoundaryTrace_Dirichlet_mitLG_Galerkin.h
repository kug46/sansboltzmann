// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_DIRICHLET_MITLG_GALERKIN_H
#define INTEGRANDBOUNDARYTRACE_DIRICHLET_MITLG_GALERKIN_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/LagrangeDOFMap.h"

#include "Discretization/Integrand_Type.h"

#include "Integrand_Galerkin_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, Galerkin> :
  public IntegrandBoundaryTraceType<
      IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, Galerkin> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Dirichlet_mitLG Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;        // matrices

  static const int N = PDE::N;

  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups )
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups), isStrongBC_(false)
  {
    call_with_derived<NDBCVector>(*this, bc_ );
  }

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }
  bool isStrongBC() const { return isStrongBC_; }

  FRIEND_CALL_WITH_DERIVED
protected:
  template<class BC>
  void operator()( const BC& bc )
  {
    isStrongBC_ = bc.isStrongBC();
  }

public:

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology      > ElementQFieldCell;
    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template <class Z>
    using IntegrandCellType = ArrayQ<Z>;

    template <class Z>
    using IntegrandTraceType = ArrayQ<Z>;

    BasisWeighted(  const PDE& pde, const BCBase& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem, // Xfield must be the last parameter
                    const ElementQFieldCell& qfldElem,
                    const ElementQFieldTrace& lgfldTrace ) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)),
                    qfldElem_(qfldElem),
                    lgfldTrace_(lgfldTrace),
                    paramfldElem_(paramfldElem),
                    nDOFCell_(qfldElem_.nDOF()),
                    nDOFTrace_(lgfldTrace_.nDOF()),
                    phi_( new Real[nDOFCell_] ),
                    phiT_( new Real[nDOFTrace_] ),
                    gradphi_( new VectorX[nDOFCell_] ),
                    traces_(LagrangeDOFMap<Topology>::getTracesCanonicalNodes())
    {
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] phiT_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFCell_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType<Ti>  integrandCell[] , int neqnCell,
                                                          IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const;

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Tq, class Tg, class Ti>
    void operator()( const BC& bc,
                     const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI, const ArrayQ<Tq>& lg,
                     const ParamT& paramI, const VectorX& nL,
                     IntegrandCellType<Ti> integrandCell[], int neqnCell,
                     IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldTrace& lgfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFCell_;
    const int nDOFTrace_;
    mutable Real *phi_;
    mutable Real *phiT_;
    mutable VectorX *gradphi_;
    std::vector< std::vector<int> > traces_;
  };


  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  class BasisWeightedFlux
  {
  public:
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology      > ElementQFieldCell;
    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template <class Z>
    using IntegrandCellType = ArrayQ<Z>;

    template <class Z>
    using IntegrandTraceType = ArrayQ<Z>;

    BasisWeightedFlux( const PDE& pde,
                       const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                       const ElementParam& paramfldElem, // Xfield must be the last parameter
                       const ElementQFieldCell& qfldElem ) :
                         pde_(pde),
                         xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                         xfldElem_(get<-1>(paramfldElem)),
                         qfldElem_(qfldElem),
                         paramfldElem_(paramfldElem),
                         nDOFCell_(qfldElem_.nDOF()),
                         phi_( new Real[nDOFCell_] )
    {
    }

    ~BasisWeightedFlux()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFCell_; }

    // boundary element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType<Ti> integrandCell[], int neqnCell ) const;
  protected:

    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOFCell_;
    mutable Real *phi_;
  };

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<ArrayQ<Tw>, TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<Real      , TopoDimCell, Topology> ElementEFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<Tw>, TopoDimTrace, TopologyTrace> ElementWFieldTrace;
    typedef Element<Real      , TopoDimTrace, TopologyTrace> ElementEFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type IntegrandType;

    typedef IntegrandType IntegrandCellType;
    typedef IntegrandType IntegrandTraceType;

    FieldWeighted(  const PDE& pde, const BCBase& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem, // Xfield must be the last parameter
                    const ElementQFieldCell& qfldElem,
                    const ElementWFieldCell& wfldElem,
                    const ElementEFieldCell& efldElem,
                    const ElementQFieldTrace& lgfldElem,
                    const ElementWFieldTrace& mufldElem,
                    const ElementEFieldTrace& eBfldElem ) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)),
                    qfldElem_(qfldElem), wfldElem_(wfldElem), efldElem_(efldElem),
                    lgfldElem_(lgfldElem), mufldElem_(mufldElem), eBfldElem_(eBfldElem),
                    paramfldElem_(paramfldElem),
                    nDOF_(qfldElem_.nDOF()),
                    nPhi_(efldElem.nDOF()),
                    phi_( new Real[nDOF_] ),
                    gradphi_( new VectorX[nDOF_] )
    {
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& RefTrace, Ti integrandPDE[], const int nphiCell,
                                                         Ti integrandBC[], const int nphiTrace ) const;

  protected:
    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementEFieldCell& efldElem_;
    const ElementQFieldTrace& lgfldElem_;
    const ElementWFieldTrace& mufldElem_;
    const ElementEFieldTrace& eBfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_, nPhi_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand( const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementParam& paramfldElem, // Xfield must be the last parameter
             const Element<ArrayQ<T>, TopoDimCell , Topology >& qfldElem,
             const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& lgfldTrace ) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTrace,
             paramfldElem,
             qfldElem,
             lgfldTrace };
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeightedFlux<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrandFlux( const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                 const ElementParam& paramfldElem, // Xfield must be the last parameter
                 const Element<ArrayQ<T>, TopoDimCell , Topology >& qfldElem ) const
  {
    return { pde_,
             xfldElemTrace, canonicalTrace,
             paramfldElem,
             qfldElem };
  }


  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted< Tq, Tw, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<Tq>, TopoDimCell , Topology >& qfldElem,
            const Element<ArrayQ<Tw>, TopoDimCell , Topology >& wfldElem,
            const Element<Real,       TopoDimCell , Topology >& efldElem,
            const Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace>& lgfldTrace,
            const Element<ArrayQ<Tw>, TopoDimTrace, TopologyTrace>& mufldTrace,
            const Element<Real,       TopoDimTrace, TopologyTrace>& eBfldTrace) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTrace,
             paramfldElem,
             qfldElem, wfldElem, efldElem,
             lgfldTrace, mufldTrace, eBfldTrace };
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  bool isStrongBC_;
};

template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam>
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType<Ti>  integrandCell[] , int neqnCell,
                                                 IntegrandTraceType<Ti> integrandTrace[], int neqnTrace  ) const
{
  SANS_ASSERT( (neqnCell == nDOFCell_) && (neqnTrace == nDOFTrace_) );

  ParamT paramI;              // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;                 // unit normal (points out of domain)

  ArrayQ<T> qI;               // solution
  VectorArrayQ<T> gradqI;     // gradient
  ArrayQ<T> lg;               // Lagrange multiplier

  QuadPointCellType sRefCell; // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRefCell );

  // Elemental parameters
  paramfldElem_.eval( sRefCell, paramI );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRefCell, xfldElemTrace_, sRefTrace, nL);

  // basis value
  qfldElem_.evalBasis( sRefCell, phi_, nDOFCell_ );
  xfldElem_.evalBasisGradient( sRefCell, qfldElem_, gradphi_, nDOFCell_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOFCell_, qI );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradphi_, nDOFCell_, gradqI );
  else
    gradqI = 0;

  // Lagrange multiplier basis
  lgfldTrace_.evalBasis( sRefTrace, phiT_, nDOFTrace_ );

  // Lagrange multiplier
  lgfldTrace_.evalFromBasis( phiT_, nDOFTrace_, lg );

  call_with_derived<NDBCVector>(*this, bc_,
                                qI, gradqI, lg,
                                paramI, nL,
                                integrandCell, neqnCell,
                                integrandTrace, neqnTrace );
}


template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc,
            const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI, const ArrayQ<Tq>& lg,
            const ParamT& paramI, const VectorX& nL,
            IntegrandCellType<Ti> integrandCell[], int neqnCell,
            IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const
{
  // stabilization term
  //Real tau = sqrt(dot(nL,nL));
  //ArrayQ<Ti> stab = tau*lg; //tau*(lg + dot(gradqI,nL));

  // PDE residual: BC Lagrange multiplier term

  ArrayQ<Ti> lgterm = 0;
  bc.lagrangeTerm( paramI, nL, qI, gradqI, lg, lgterm );

  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] = phi_[k]*lgterm; // + dot(gradphi_[k], nL)*stab;

  //SANS_ASSERT(neqnCell >= 2);
  //integrandCell[traces_[canonicalTrace_.trace][0]] += phi_[traces_[canonicalTrace_.trace][0]]*stab;
  //integrandCell[traces_[canonicalTrace_.trace][1]] -= phi_[traces_[canonicalTrace_.trace][1]]*stab;

  // BC residual: if strong form then phiT_[k] is either 0 or 1

  ArrayQ<Ti> residualBC = 0;
  bc.strongBC( paramI, nL, qI, gradqI, lg, residualBC );

  for (int k = 0; k < neqnTrace; k++)
    integrandTrace[k] = phiT_[k]*residualBC;// + phiT_[k]*stab;
}


template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam>
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, Galerkin>::
BasisWeightedFlux<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType<Ti>  integrandCell[], int neqnCell ) const
{
  SANS_ASSERT( neqnCell == nDOFCell_ );

  ParamT paramL;             // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;                // unit normal (points out of domain)

  ArrayQ<T> qI;              // solution
  VectorArrayQ<T> gradqI;    // gradient

  QuadPointCellType sRefCell;   // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRefCell );

  // Elemental parameters
  paramfldElem_.eval( sRefCell, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRefCell, xfldElemTrace_, sRefTrace, nL);

  // basis value
  qfldElem_.evalBasis( sRefCell, phi_, nDOFCell_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOFCell_, qI );
  if (needsSolutionGradient)
    xfldElem_.evalGradient( sRefCell, qfldElem_, gradqI );
  else
    gradqI = 0;

  // PDE residual: weak form boundary integral

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( paramL, qI, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( paramL, qI, gradqI, F );

    ArrayQ<Ti> Fn = dot(nL,F);
    for (int k = 0; k < neqnCell; k++)
      integrandCell[k] = phi_[k]*Fn;
  }
}


template <class PDE, class NDBCVector>
template <class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                              class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, Galerkin>::
FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, Ti integrandCell[], const int nphiCell,
                                                 Ti integrandBC[], const int nphiTrace ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEs are made by God, the boundary conditions by the Devil -- Alan Turing");
#if 0
  ParamT paramL; // Elemental parameters (such as grid coordinates and distance functions)

  VectorX N;                // unit normal (points out of domain)

  ArrayQ<Tq> q;              // solution
  VectorArrayQ<Tq> gradq;    // gradient

  ArrayQ<Tw> w;              // weight

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  const bool needsSolutionGradient = false; //pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, N);

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  qfldElem_.evalFromBasis( phi_, nDOF_, q );

  if (needsSolutionGradient)
  {
    // basis and solution gradient
    xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
  }
  else
    gradq = 0;

  wfldElem_.eval( sRef, w );

  integrand = 0;

  // PDE residual: weak form boundary integral
  if (pde_.hasFluxAdvective())// || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( paramL, q, F );

    // no viscous flux (talk to Prof. Darmofal about it)
    //if ( pde_.hasFluxViscous() )
    //  pde_.fluxViscous( paramL, q, gradq, F );

    ArrayQ<Ti> Fn = dot(N,F);
    integrand += dot(w,Fn);
  }

  // PDE residual: BC Lagrange multiplier term

  // No-op for Strong

  // BC residual: BC weak form

  // No-op for Strong
#endif
}

}

#endif  // INTEGRANDBOUNDARYTRACE_DIRICHLET_MITLG_GALERKIN_H
