// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_STABILIZED_CORRECTED_H
#define INTEGRANDCELL_GALERKIN_STABILIZED_CORRECTED_H

// cell integrand operator: Galerkin with Adjoint Stabilization

#include <ostream>
#include <iostream>
#include <iomanip>
#include <vector>

#include "Discretization/Galerkin/Stabilization_Galerkin.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"     // Real
#include "tools/SANSException.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "Field/Element/Element.h"
#include "Discretization/Integrand_Type.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin with Adjoint Stabilization


template <class PDE_>
class IntegrandCell_Galerkin_Stabilized_Corrected : public IntegrandCellType< IntegrandCell_Galerkin_Stabilized_Corrected<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>;

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;

  template <class T>
  using TensorMatrixQ = typename PDE::template TensorMatrixQ<T>;    // diffusion matrix

  template <class T>
  using VectorTensorMatrixQ = typename PDE::template VectorTensorMatrixQ<T>;    // diffusion matrix
  template <class T>

  using TensorArrayQ = typename DLA::MatrixS<D, D, ArrayQ<T> >;

  template <class T>
  using TensorSymArrayQ = typename PDE::template TensorSymArrayQ<T>;

  typedef DLA::MatrixSymS<PhysDim::D, Real> TensorSymX;

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin_Stabilized_Corrected( const PDE& pde, const std::vector<int>& CellGroups, const StabilizationMatrix& tau )
    : pde_(pde), cellGroups_(CellGroups), tau_(tau) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<Real, TopoDim, Topology> ElementEFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;

    typedef typename ElementParam::T ParamT;
    typedef typename ElementParam::gradT ParamGradT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    FieldWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
                   const Element<Real, TopoDim, Topology>& efldElem,
                   const StabilizationMatrix& tau) :
                     pde_(pde),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem),
                     wfldElem_(wfldElem),
                     efldElem_(efldElem),
                     paramfldElem_(paramfldElem),
                     tau_(tau),
                     order_(qfldElem.basis()->order()),
                     nDOF_(qfldElem.nDOF()),
                     nNodes_( qfldElem_.basis()->nBasisNode() ),
                     nPhi_( efldElem_.nDOF()),
                     ephi_( new Real[nPhi_] ), // basis associated with efld
                     stabType_( tau.getStabType()),
                     stabElem_( tau.getStabOrder(), qfldElem_.basis()->category() ),
                     phiLin_( new Real[stabElem_.nDOF()] ),
                     gradphiLin_( new VectorX[stabElem_.nDOF()] )
    {
    }

    FieldWeighted( FieldWeighted&& fw ) :
                     pde_(fw.pde_),
                     xfldElem_(fw.xfldElem_),
                     qfldElem_(fw.qfldElem_),
                     wfldElem_(fw.wfldElem_),
                     efldElem_(fw.efldElem_),
                     paramfldElem_(fw.paramfldElem_),
                     tau_(fw.tau_),
                     order_(fw.order_),
                     nDOF_(fw.nDOF_),
                     nNodes_( fw.nNodes_ ),
                     nPhi_( fw.nPhi_ ),
                     ephi_( fw.ephi_ ),
                     stabType_( fw.stabType_ ),
                     stabElem_( std::move(fw.stabElem_) ),
                     phiLin_( fw.phiLin_ ),
                     gradphiLin_( fw.gradphiLin_ )
    {
      fw.ephi_ = nullptr;
      fw.phiLin_ = nullptr; fw.gradphiLin_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] ephi_;
      delete [] phiLin_;
      delete [] gradphiLin_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    template <class Ti>
    void operator()( const QuadPointType& sRef, Ti integrand[], const int nphi ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& wfldElem_;
    const ElementEFieldType& efldElem_;
    const ElementParam& paramfldElem_;
    const StabilizationMatrix& tau_;
    const int order_;
    const int nDOF_;
    const int nNodes_;

    const int nPhi_;
    mutable Real *ephi_; // efld basis
    const StabilizationType stabType_;

    const ElementEFieldType stabElem_;
    mutable Real *phiLin_;
    mutable VectorX *gradphiLin_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const Element<Real, TopoDim, Topology>& efldElem) const
  {
    return {pde_, paramfldElem, qfldElem, wfldElem, efldElem, tau_};
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
  const StabilizationMatrix& tau_;
};

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_Stabilized_Corrected<PDE>::FieldWeighted<T, TopoDim, Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_Stabilized_Corrected<PDE>::
FieldWeighted<T, TopoDim, Topology, ElementParam>::
operator()( const QuadPointType& sRef, Ti integrand[], const int nphi ) const
{
  if (stabType_ != StabilizationType::Unstabilized)
  {
    SANS_ASSERT( nphi == nPhi_ );

    ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)
    ParamGradT gradparam;

    ArrayQ<T> q = 0;               // solution
    VectorArrayQ<T> gradq = 0;     // solution gradient
    TensorSymArrayQ<T> hessq = 0;  // solution hessian

    ArrayQ<T> w = 0;               // weight
    VectorArrayQ<T> gradw = 0;     // weight gradient
    TensorSymArrayQ<T> hessw = 0;  // weight hessian

    // Elemental parameters
    paramfldElem_.eval( sRef, param );
    paramfldElem_.evalDerivative( sRef, gradparam );

    // solution value, gradient
    qfldElem_.eval( sRef, q );
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );

    // weight value, gradient
    wfldElem_.eval( sRef, w );
    xfldElem_.evalGradient( sRef, wfldElem_, gradw );

    // solution and weight hessian
    if (pde_.hasFluxViscous() && order_ > 1)
      xfldElem_.evalHessian( sRef, qfldElem_, hessq );

    if (pde_.hasFluxViscous() && wfldElem_.order() > 1)
      xfldElem_.evalHessian( sRef, wfldElem_, hessw );

    // estimate basis, gradient
    efldElem_.evalBasis( sRef, ephi_, nPhi_ );
    xfldElem_.evalBasisGradient( sRef, stabElem_, gradphiLin_, stabElem_.nDOF() );

    // PDE source S(X, D, U, UX), S(X)
    ArrayQ<T> source=0;
    if (pde_.hasSource())
      pde_.source( param, q, gradq, source );

    // Galerkin right-hand-side forcing function: -phi RHS
    ArrayQ<Real> forcing = 0;
    if (pde_.hasForcingFunction())
      pde_.forcingFunction( param, forcing );

    //contruct strongPDE
    ArrayQ<T> strongPDE = 0;
    tau_.template constructStrongPDE<T, T, T, T>(pde_, param, q, gradq, hessq, source, forcing, strongPDE);


    // adjoint PDE
    TensorMatrixQ<Ti> K = 0;

    if (pde_.hasFluxViscous())
      pde_.diffusionViscous( param, q, gradq, K );

    MatrixQ<Ti> S = 0;
    if (pde_.hasSource())
      pde_.jacobianSourceAbsoluteValue(param, q, gradq, S);

    tau_.template solve<T,T,T>(pde_, param, q, K, S, phiLin_, gradphiLin_, stabElem_.order(),
                               nNodes_, stabElem_.nDOF(), strongPDE);

    Ti stabIntegrand = 0;
    if (stabType_ == StabilizationType::SUPG)
    {
      tau_.template operatorSUPGFW<T, Ti>(pde_, param, q, gradq, gradw, strongPDE, stabIntegrand);
    }
    else if (stabType_ == StabilizationType::GLS || stabType_ == StabilizationType::AGLSPrimal)
    {
      tau_.template operatorGLSFW<T, Ti>(pde_, param, q, gradq, hessq, K, w, gradw, hessw, strongPDE, wfldElem_.order(), stabIntegrand);
    }
    else if (stabType_ == StabilizationType::Adjoint || stabType_ == StabilizationType::AGLSAdjoint)
    {
      tau_.template operatorAdjointFW<T, Ti>
        (pde_, param, gradparam, q, gradq, hessq, K, w, gradw, hessw, strongPDE, wfldElem_.order(), stabIntegrand);
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION("INVALID STABILIZATION");
    }

    for (int k = 0; k < nPhi_; k++)
    {
      integrand[k] = ephi_[k]*stabIntegrand;
    }

  }
}

}// namepace SANS

#endif  // INTEGRANDCELL_GALERKIN_STABILIZED_H
