// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_JACOBIANBOUNDARYTRACE_DISPATCH_GALERKIN_PARAM_H_
#define SRC_DISCRETIZATION_GALERKIN_JACOBIANBOUNDARYTRACE_DISPATCH_GALERKIN_PARAM_H_

#include "JacobianBoundaryTrace_FieldTrace_Galerkin_Param.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups_HubTrace.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "JacobianBoundaryTrace_Galerkin_Param.h"


namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
class JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_Param_impl
{
public:
  JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_Param_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      SparseMatrix& mtxGlobalPDE_p,
      SparseMatrix& mtxGlobalBC_p )
    : xfld_(xfld), qfld_(qfld), lgfld_(lgfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_p_(mtxGlobalPDE_p), mtxGlobalBC_p_(mtxGlobalBC_p) {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_FieldTrace_Galerkin_Param<Surreal,iParam>(fcn, mtxGlobalPDE_p_, mtxGlobalBC_p_),
        xfld_, qfld_, lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  SparseMatrix& mtxGlobalPDE_p_;
  SparseMatrix& mtxGlobalBC_p_;
};

// Factory function

template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_Param_impl<Surreal, iParam, XFieldType, PhysDim, TopoDim, ArrayQ, SparseMatrix>
JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_Param(const XFieldType& xfld,
                                           const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                           const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                           const int* quadratureorder, int ngroup,
                                           SparseMatrix& mtxGlobalPDE_p,
                                           SparseMatrix& mtxGlobalBC_p )
{
  return {xfld, qfld, lgfld, quadratureorder, ngroup, mtxGlobalPDE_p, mtxGlobalBC_p};
}


//---------------------------------------------------------------------------//
//
// Dispatch class for hub trace equations
//
//---------------------------------------------------------------------------//
template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
class JacobianBoundaryTrace_mitHT_Dispatch_Galerkin_Param_impl
{
public:
  JacobianBoundaryTrace_mitHT_Dispatch_Galerkin_Param_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& htfld,
      const int* quadratureorder, int ngroup,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_p,
      MatrixScatterAdd<MatrixQ>& mtxGlobalHT_p ) :
    xfld_(xfld), qfld_(qfld), htfld_(htfld),
    quadratureorder_(quadratureorder), ngroup_(ngroup),
    mtxGlobalPDE_p_(mtxGlobalPDE_p), mtxGlobalHT_p_(mtxGlobalHT_p) {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_HubTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_FieldTrace_Galerkin_Param<Surreal,iParam>(fcn, mtxGlobalPDE_p_, mtxGlobalHT_p_),
        xfld_, qfld_, htfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& htfld_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_p_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalHT_p_;
};

// Factory function

template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
JacobianBoundaryTrace_mitHT_Dispatch_Galerkin_Param_impl<Surreal, iParam, XFieldType, PhysDim, TopoDim, ArrayQ, MatrixQ>
JacobianBoundaryTrace_mitHT_Dispatch_Galerkin_Param(const XFieldType& xfld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                    const Field<PhysDim, TopoDim, ArrayQ>& htfld,
                                                    const int* quadratureorder, int ngroup,
                                                    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_p,
                                                    MatrixScatterAdd<MatrixQ>& mtxGlobalHT_p )
  // TODO: is SparseMatrix different from MatrixScatterAdd<MatrixQ>
{
  return {xfld, qfld, htfld, quadratureorder, ngroup, mtxGlobalPDE_p, mtxGlobalHT_p};
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
class JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_Param_impl
{
public:
  JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_Param_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      SparseMatrix& mtxGlobalPDE_p )
    : xfld_(xfld), qfld_(qfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_p_(mtxGlobalPDE_p)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        JacobianBoundaryTrace_Galerkin_Param<Surreal,iParam>(fcn, mtxGlobalPDE_p_),
        xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  SparseMatrix& mtxGlobalPDE_p_;
};

// Factory function

template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_Param_impl<Surreal, iParam, XFieldType, PhysDim, TopoDim, ArrayQ, SparseMatrix>
JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_Param(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const int* quadratureorder, int ngroup,
                                             SparseMatrix& mtxGlobalPDE_p)
{
  return {xfld, qfld, quadratureorder, ngroup, mtxGlobalPDE_p};
}


}


#endif /* SRC_DISCRETIZATION_GALERKIN_JACOBIANBOUNDARYTRACE_DISPATCH_GALERKIN_PARAM_H_ */
