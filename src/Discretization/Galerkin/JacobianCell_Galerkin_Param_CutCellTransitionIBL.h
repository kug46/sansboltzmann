// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_PARAM_CUTCELLTRANSITIONIBL_H_
#define SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_PARAM_CUTCELLTRANSITIONIBL_H_

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/SurrealizedElementTuple.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "IntegrandCell_Galerkin_CutCellTransitionIBL.h"

namespace SANS
{
// forward declaration
template<class Surreal, int iParam, class IntegrandCell, class MatrixQP_>
class JacobianCell_Galerkin_Param_impl;

//----------------------------------------------------------------------------//
template<class Surreal, int iParam,
         class VarType, template<class, class> class PDENDConvert,
         class MatrixQP_>
class JacobianCell_Galerkin_Param_impl<
        Surreal, iParam,
        IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >,
        MatrixQP_ > :
    public GroupIntegralCellType<
             JacobianCell_Galerkin_Param_impl<
               Surreal, iParam,
               IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >,
               MatrixQP_ > >
{
public:
  typedef IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > > IntegrandCellClass;

  typedef typename IntegrandCellClass::PhysDim PhysDim;

  typedef typename IntegrandCellClass::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCellClass::template ArrayQ<Surreal> ArrayQSurreal;

  typedef typename DLA::VectorD<ArrayQSurreal> ResidualElemClass;


  JacobianCell_Galerkin_Param_impl(const IntegrandCellClass& fcn,
                                   MatrixScatterAdd<MatrixQP_>& mtxGlobalMATCH_p,
                                   MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p ) :
    fcn_(fcn),
    mtxGlobalMATCH_p_(mtxGlobalMATCH_p),
    mtxGlobalPDE_p_(mtxGlobalPDE_p) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim, class QBundleFieldType>
  void check( const QBundleFieldType& qfld )
  {
    SANS_ASSERT( mtxGlobalMATCH_p_.m() == qfld.left().nDOFpossessed() );

    SANS_ASSERT( mtxGlobalPDE_p_.m() == qfld.right().nDOFpossessed() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template<class Topology, class TopoDim, class TupleFieldType, class QBundleFieldCellGroupType>
  void
  integrate( const int cellGroupGlobal,
             const typename TupleFieldType::template FieldCellGroupType<Topology>& tuplefldCell,
             const QBundleFieldCellGroupType& qbundlefldCell,
             const int quadratureorder )
  {
    typedef typename TupleFieldType::template FieldCellGroupType<Topology> TupleFieldCellGroupType;
    static_assert(std::is_same<Topology, typename QBundleFieldCellGroupType::TopologyType>::value, "Wrong topology type of qbundlefldCell!");

    typedef typename QBundleFieldCellGroupType::FieldGroupTypeL QMatchFieldCellGroupType;
    typedef typename QBundleFieldCellGroupType::FieldGroupTypeR QFieldCellGroupType;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupType, Surreal, iParam>::type ElementTupleFieldClass;
    typedef typename QMatchFieldCellGroupType::template ElementType<> ElementQMatchFieldClass;
    typedef typename QFieldCellGroupType    ::template ElementType<> ElementQFieldClass;

    typedef typename TupleType<iParam, TupleFieldCellGroupType>::type ParamCellGroupType;
    typedef typename ParamCellGroupType::template ElementType<Surreal> ElementParamFieldClass;

    typedef typename ElementParamFieldClass::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP; // jacobian type associated with each DOF

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass; // jacobian type associated with each element

    // field cell groups
    const ParamCellGroupType& paramfldCell = get<iParam>(tuplefldCell);
    const QMatchFieldCellGroupType& qmatchfldCell = qbundlefldCell.left();
    const QFieldCellGroupType& qfldCell = qbundlefldCell.right();

    // field elements
    ElementTupleFieldClass  tuplefldElem( tuplefldCell.basis() );
    ElementParamFieldClass& paramfldElem = set<iParam>(tuplefldElem);
    ElementQMatchFieldClass qmatchfldElem( qmatchfldCell.basis() );
    ElementQFieldClass      qfldElem( qfldCell.basis() );

    // DOFs per element
    const int nDOFMATCH = qmatchfldElem.nDOF();
    const int nDOFPDE = qfldElem.nDOF();
    const int nParamDOF = paramfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFMATCHGlobal(nDOFMATCH,-1);
    std::vector<int> mapDOFPDEGlobal(nDOFPDE,-1);
    std::vector<int> mapDOFParamGlobal(nParamDOF,-1);

    // Surrealized element integrand/residuals
    ResidualElemClass rsdElemMATCH( nDOFMATCH );
    ResidualElemClass rsdElemPDE( nDOFPDE );

    // element jacobian matrix
    MatrixElemClass mtxElemMATCH(nDOFMATCH, nParamDOF);
    MatrixElemClass mtxElemPDE(nDOFPDE, nParamDOF);

    // element integral
    GalerkinWeightedIntegral_New_TransitionIBL<TopoDim, Topology, ResidualElemClass> integral(quadratureorder);

    // just to make sure things are consistent
    SANS_ASSERT( tuplefldCell.nElem() == qbundlefldCell.nElem() );

    // number of simultaneous derivatives per functor call
    SANS_ASSERT_MSG(nArrayP == Surreal::N,
                    "Assumption that all the parameter derivatives are calculated simultaneously is violated!");
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = tuplefldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobian to zero
      mtxElemMATCH = 0.0;
      mtxElemPDE = 0.0;

      // copy global parameter/solution DOFs to element
      tuplefldCell.getElement( tuplefldElem, elem );
      qmatchfldCell.getElement( qmatchfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // loop over derivative chunks (i.e. each entry of each parameter DOF in both left and right elements)
      for (int nchunk = 0; nchunk < nArrayP*nParamDOF; nchunk += nDeriv)
      {
        // associate derivative slots with parameter DOFs

        int slot = 0;
        for (int j = 0; j < nParamDOF; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(paramfldElem.DOF(j), n).deriv(k) = 0.0; // TODO: this is safe but can be slow

            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElem.DOF(j), n).deriv(slot - nchunk) = 1.0;
          }
        }

        // reset residuals
        rsdElemMATCH = 0.0;
        rsdElemPDE = 0.0;

        // evaluate surrealized element residual
        integral(fcn_.integrand( tuplefldElem, qmatchfldElem, qfldElem ), get<-1>(tuplefldElem),
                 rsdElemMATCH, rsdElemPDE );
        // xfld should always be the last component of tuplefldElem

        // accumulate derivatives into element jacobians

        for (int j = 0; j < nParamDOF; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFMATCH; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxElemMATCH(i,j),m,n) = DLA::index(rsdElemMATCH[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFPDE; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxElemPDE(i,j),m,n) = DLA::index(rsdElemPDE[i],m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global
      scatterAdd( qmatchfldCell, paramfldCell, elem,
                  mapDOFMATCHGlobal.data(), nDOFMATCH,
                  mapDOFParamGlobal.data(), nParamDOF,
                  mtxElemMATCH, mtxGlobalMATCH_p_ );

      scatterAdd( qfldCell, paramfldCell, elem,
                  mapDOFPDEGlobal.data(), nDOFPDE,
                  mapDOFParamGlobal.data(), nParamDOF,
                  mtxElemPDE, mtxGlobalPDE_p_ );
    }
  }

protected:
  //----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, class ParamFieldCellGroupType, class MatrixQP, class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfldCell,
      const ParamFieldCellGroupType& paramfldCell,
      const int elem,
      int mapDOFEqnGlobal[], const int nDOFEqn,
      int mapParamDOFGlobal[], const int nParamDOF,
      SANS::DLA::MatrixD<MatrixQP>& mtxElemEqn_param,
      SparseMatrixType& mtxGlobalEqn_param )
  {
    qfldCell.associativity( elem ).getGlobalMapping( mapDOFEqnGlobal, nDOFEqn );
    paramfldCell.associativity( elem ).getGlobalMapping( mapParamDOFGlobal, nParamDOF );

    mtxGlobalEqn_param.scatterAdd( mtxElemEqn_param, mapDOFEqnGlobal, nDOFEqn, mapParamDOFGlobal, nParamDOF );
  }

protected:
  const IntegrandCellClass& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalMATCH_p_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
};

// Factory function

template<class Surreal, int iParam, class IntegrandCell, class MatrixQP>
JacobianCell_Galerkin_Param_impl<Surreal, iParam, IntegrandCell, MatrixQP>
JacobianCell_Galerkin_Param( const IntegrandCellType<IntegrandCell>& fcn,
                             MatrixScatterAdd<MatrixQP>& mtxGlobalMATCH_p,
                             MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p )
{
  return { fcn.cast(), mtxGlobalMATCH_p, mtxGlobalPDE_p };
}

}

#endif /* SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_PARAM_CUTCELLTRANSITIONIBL_H_ */
