// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_OUTPUT
#define INTEGRANDCELL_GALERKIN_OUTPUT

#include <vector>

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand for generic output functional

template <class OutputFunctional>
class IntegrandCell_Galerkin_Output :
    public IntegrandCellType< IntegrandCell_Galerkin_Output<OutputFunctional> >
{
public:
  typedef typename OutputFunctional::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename OutputFunctional::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename OutputFunctional::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = typename OutputFunctional::template ArrayJ<T>;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = typename OutputFunctional::template MatrixJ<T>;

  // cppcheck-suppress noExplicitConstructor
  IntegrandCell_Galerkin_Output( const OutputFunctional& outputFcn, const std::vector<int>& cellGroups ) :
    outputFcn_(outputFcn), cellGroups_(cellGroups)
  {
    // Nothing
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim        , TopoDim, Topology> ElementXFieldType;
    typedef Element      <ArrayQ<T>      , TopoDim, Topology> ElementQFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;
    static const int N = DLA::VectorSize<ArrayQ<T>>::M;
    static const int nOut = DLA::VectorSize<ArrayJ<T>>::M;

    Functor( const OutputFunctional& outputFcn,
             const ElementParam& paramfldElem,
             const ElementQFieldType& qfldElem) :
      outputFcn_(outputFcn),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
      qfldElem_(qfldElem),
      nDOF_(qfldElem_.nDOF()),
      phi_( new Real[nDOF_] ),
      gradphi_( new VectorX[nDOF_] )
    {
    }

    Functor( Functor&& f ) :
      outputFcn_(f.outputFcn_),
      paramfldElem_(f.paramfldElem_),
      xfldElem_(f.xfldElem_),
      qfldElem_(f.qfldElem_),
      nDOF_(f.nDOF_),
      phi_( f.phi_ ),
      gradphi_( f.gradphi_ )
    {
      f.phi_ = nullptr; f.gradphi_ = nullptr;
    }

    ~Functor()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // element output integrand
    void operator()( const QuadPointType& sRef, ArrayJ<T>& integrand ) const
    {
      VectorX X;       // physical coordinates
      ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

      ArrayQ<T> q;              // solution
      VectorArrayQ<T> gradq;    // gradient

      const bool needsSolutionGradient = outputFcn_.needsSolutionGradient();

      // Elemental parameters
      paramfldElem_.eval( sRef, param );

      // solution value, gradient
      qfldElem_.eval( sRef, q );

      if (needsSolutionGradient)
        xfldElem_.evalGradient( sRef, qfldElem_, gradq );

      outputFcn_(param, q, gradq, integrand);
    }


    // element output integrand
    void operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<MatrixJ<Real>>& mtxPDEElem ) const
    {
      typedef SurrealS<N> SurrealClass;
      static const int nDeriv = SurrealClass::N;

      VectorX X;       // physical coordinates
      ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

      ArrayQ<Real> q;              // solution
      VectorArrayQ<Real> gradq;    // gradient

      ArrayQ<SurrealClass> qSurreal;              // solution
      VectorArrayQ<SurrealClass> gradqSurreal;    // gradient

      const bool needsSolutionGradient = outputFcn_.needsSolutionGradient();

      // Elemental parameters
      paramfldElem_.eval( sRef, param );

      // basis value, gradient
      qfldElem_.evalBasis( sRef, phi_, nDOF_ );
      xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

      // solution value, gradient
      qfldElem_.eval( sRef, q );

      if (needsSolutionGradient)
        qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

      //surrealize
      qSurreal = q;
      gradqSurreal = gradq;

      // element integrand/residual
      ArrayJ<SurrealClass> integrandSurreal;
      MatrixJ<Real> J_q =0, J_gradq = 0; // temporary storage

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < N; nchunk += nDeriv)
      {

        // associate derivative slots with solution variables
        int slot = 0;
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < N; n++)
            DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
        slot += N;

        integrandSurreal = 0;

        outputFcn_(param, qSurreal, gradq, integrandSurreal);

        // accumulate derivatives into element jacobian

        slot = 0;
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < N; n++)
          {
            DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative
          }

          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < nOut; m++)
            for (int n = 0; n < N; n++)
            {
              // Note that n,m are transposed intentionally
              DLA::index(J_q,n,m) = DLA::index(integrandSurreal, m).deriv(slot + n - nchunk);
            }

          for (int j = 0; j < nDOF_; j++)
            mtxPDEElem[j] += dJ*phi_[j]*J_q;

        }
        slot += N;
      } // nchunk


      if (needsSolutionGradient)
      {
        // loop over derivative chunks
        for (int nchunk = 0; nchunk < PhysDim::D*N; nchunk += nDeriv)
        {

          // associate derivative slots with solution variables

          int slot = 0;
          for (int d = 0; d < PhysDim::D; d++)
          {
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              for (int n = 0; n < N; n++)
                DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
            slot += N;
          }

          integrandSurreal = 0;

          outputFcn_(param, q, gradqSurreal, integrandSurreal);

          // accumulate derivatives into element jacobian

          slot = 0;
          for (int d = 0; d < PhysDim::D; d++)
          {
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int n = 0; n < N; n++)
              {
                DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative
              }

              // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
              for (int m = 0; m < nOut; m++)
                for (int n = 0; n < N; n++)
                {
                  // Note that n,m are transposed intentionally
                  DLA::index(J_gradq,n,m) = DLA::index(integrandSurreal, m).deriv(slot + n - nchunk);
                }

              for (int j = 0; j < nDOF_; j++)
                mtxPDEElem[j] += dJ*gradphi_[j][d]*J_gradq;

            }
            slot += N;
          }
        } // nchunk
      }

    }


  protected:
    const OutputFunctional& outputFcn_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  Functor<T,TopoDim,Topology,ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return Functor<T,TopoDim,Topology,ElementParam>(outputFcn_,
                                                    paramfldElem,
                                                    qfldElem);
  }

private:
  const OutputFunctional& outputFcn_;
  const std::vector<int> cellGroups_;
};

}

#endif //INTEGRANDCELL_GALERKIN_OUTPUT
