// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDBUNDLE_GALERKIN_H_
#define FIELDBUNDLE_GALERKIN_H_

#include "Field/Field.h"
#include "Field/FieldTypes.h"

#include "Field/Local/Field_Local.h"

#include "tools/split_cat_std_vector.h" // SANS::cat in derived constructors

#include "tools/make_unique.h"
#include "tools/linspace.h"

namespace SANS
{

/** FieldBundleBase_Galerkin is a container for the continuous solution field bundles
 *
 * FieldBundleBase_Galerkin is a container for continous solution fields and it gives
 * a user the ability to stash the solution field, an associated Lagrange field on a
 * boundary, as well as the metadata (order, spacetype, etc) and the mesh field that
 * describe the field that is stored within it, and some tools to work with it.
 */
template< class PhysD, class TopoD, class ArrayQ_>
struct FieldBundleBase_Galerkin
{

  // typedef off the inputs
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;

  // the constructor: initializes everything
  FieldBundleBase_Galerkin( const XField<PhysDim, TopoDim>& xfld,
                            Field_CG_Cell< PhysDim, TopoDim, ArrayQ >& qfld,
                            Field_CG_BoundaryTrace< PhysDim, TopoDim, ArrayQ >& lgfld,
                            std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld,
                            const int order,
                            const BasisFunctionCategory basis_cell,
                            const BasisFunctionCategory basis_trace ) :
  xfld(xfld), qfld(qfld), lgfld(lgfld), up_resfld(up_resfld), order(order),
  basis_cell(basis_cell), basis_trace(basis_trace) {}

  // ???
  void projectTo(FieldBundleBase_Galerkin& bundleTo)
  {
    qfld.projectTo(bundleTo.qfld);
    lgfld.projectTo(bundleTo.lgfld);
  }

  // the mesh field on which the solution is stored
  const XField<PhysDim,TopoDim>& xfld;

  // the CG cell field for storing the solution
  Field_CG_Cell<PhysDim,TopoDim,ArrayQ>& qfld;
  // the CG boundary field for storing Lagrange multiplier fields (if necessary)
  Field_CG_BoundaryTrace<PhysDim, TopoDim, ArrayQ>& lgfld;
  // a pointer to the residual field used for estimation and local solves later
  // This is a reference because the pointer is owned by the derived class (below)
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld;

  // polynomial order stored in this field bundle
  const int order;

  // specify the basis functions used here
  const BasisFunctionCategory basis_cell;
  const BasisFunctionCategory basis_trace;

  // the type of solution space stored in this field bundle (e.g. continuous v. discontinuous)
  static constexpr SpaceType spaceType = SpaceType::Continuous;

  void dump() { qfld.dump(); }
};

// forward declare the class specialization so input args for local work
template< class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_Galerkin;

// public of GlobalBundle is so that AlgebraicEquationSet Basetype works with it
template<class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_Galerkin_Local :
    public FieldBundleBase_Galerkin<PhysD, TopoD, ArrayQ_>
{
  typedef FieldBundleBase_Galerkin<PhysD, TopoD, ArrayQ_> BaseType;

  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;

  typedef Field_CG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef Field_CG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef Field_Local<QFieldType> QFieldType_Local;
  typedef Field_Local<LGFieldType> LGFieldType_Local;

  // Broken groups means that the fields are discontinuous between cell groups.
  // For instance, interior to cell group 0 will be CG, but from 0 to 1, there will be a DG style interface

#if defined(WHOLEPATCH) || defined(INNERPATCH)
  // If the local patch is WHOLEPATCH or INNERPATCH, then do not use broken field!

  FieldBundle_Galerkin_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_Galerkin<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<std::vector<int>>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_,lgfld_,up_resfld_,order,globalfields.basis_cell,globalfields.basis_trace),
    qfld_( xfld_local, globalfields.qfld, order, globalfields.basis_cell ), // unbroken groups
    lgfld_( active_local_BGroup_list, xfld_local, globalfields.lgfld, order, globalfields.basis_trace ) // Broken Groups
  { }

  // Breaks up the groups in-situ
  FieldBundle_Galerkin_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_Galerkin<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<int>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_,lgfld_,up_resfld_,order, globalfields.basis_cell, globalfields.basis_trace),
    qfld_( xfld_local, globalfields.qfld, order, globalfields.basis_cell ), // unbroken groups
    lgfld_( xfld_local, globalfields.lgfld, order, globalfields.basis_trace, active_local_BGroup_list ) // Unbroken Groups
  { }
#else
  // If the local patch is neither WHOLEPATCH nor INNERPATCH, then use the broken field!

  // active_local_BGroup_list is a vec of vec so that boundaries that came from the same original global trace
  // continue to have shared dofs in the local field.
  FieldBundle_Galerkin_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_Galerkin<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<std::vector<int>>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_,lgfld_,up_resfld_,order, globalfields.basis_cell, globalfields.basis_trace),
    qfld_(  SANS::split(linspace(0,xfld_local.nCellGroups()-1)), xfld_local, globalfields.qfld, order, globalfields.basis_cell ), // broken groups
    lgfld_( active_local_BGroup_list, xfld_local, globalfields.lgfld, order, globalfields.basis_trace ) // Broken Groups
  {
    SANS_ASSERT_MSG( active_local_BGroup_list.empty(), "Local Patches with Lagrange multipliers not supported");

    if (globalfields.up_resfld) // if there is a residual field in the global field bundle
    {
      // create a new local residual field and transfer the global residual field onto it
      up_resfld_ = SANS::make_unique<Field_Local<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>>
        ( xfld_local, *globalfields.up_resfld, order, globalfields.basis_cell );
    }
  }

  // Breaks up the groups in-situ
  FieldBundle_Galerkin_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_Galerkin<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<int>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_,lgfld_,up_resfld_,order,globalfields.basis_cell,globalfields.basis_trace),
    qfld_(  SANS::split(linspace(0,xfld_local.nCellGroups()-1)), xfld_local, globalfields.qfld, order, globalfields.basis_cell ), // broken groups
    lgfld_( xfld_local, globalfields.lgfld, order, globalfields.basis_trace, active_local_BGroup_list ) // Unbroken Groups
  {
    SANS_ASSERT_MSG( active_local_BGroup_list.empty(), "Local Patches with Lagrange multipliers not supported");

    if (globalfields.up_resfld) // if there is a residual field in the global field bundle
    {
      // create a new local residual field and transfer the global residual field onto it
      up_resfld_ = SANS::make_unique<Field_Local<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>>
        ( xfld_local, *globalfields.up_resfld, order, globalfields.basis_cell );
    }
  }
#endif

  using BaseType::order;
  using BaseType::basis_cell;
  using BaseType::basis_trace;


protected:
  // the solution field
  QFieldType_Local qfld_;
  // the trace Lagrange field
  LGFieldType_Local lgfld_;
  // pointer to a resfld, this is an actual field rather than a local. It is declared then with a local version above
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>> up_resfld_;
  // pointer to an estfld, this is an actual field rather than a local. It is declared with a local version above
};

template<class PhysD, class TopoD, class ArrayQ_>
struct FieldBundle_Galerkin :
    public FieldBundleBase_Galerkin<PhysD, TopoD, ArrayQ_>
{
  // base class
  typedef FieldBundleBase_Galerkin<PhysD, TopoD, ArrayQ_> BaseType;

  // shorthand references for the stuff passed in
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;

  typedef Field_CG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef Field_CG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef FieldBundle_Galerkin_Local<PhysD,TopoD,ArrayQ> FieldBundle_Local;

  // constructor: initializes everything, including the base class
  FieldBundle_Galerkin( const XField<PhysDim, TopoDim>& xfld, const int order,
                        const BasisFunctionCategory basis_cell, const BasisFunctionCategory basis_trace,
                        const std::vector<int>& active_BGroup_list )
  : BaseType(xfld,qfld_,lgfld_,up_resfld_,order, basis_cell, basis_trace),
    qfld_( xfld, order, basis_cell ),
    lgfld_( xfld, order, basis_trace, active_BGroup_list ) // Unbroken groups
  {
    // the up_resfld_ isn't declared here, it will be filled in the SolverInterface class.
  }

  // a pseudo copy constructor
  FieldBundle_Galerkin( const XField<PhysDim,TopoDim>&xfld, const int order,
                        const BaseType& flds,
                        const std::vector<int>& active_BGroup_list )
  : BaseType(xfld,qfld_,lgfld_, up_resfld_, order, flds.basis_cell, flds.basis_trace ),
    qfld_( xfld, order, flds.basis_cell),
    lgfld_( xfld, order, flds.basis_trace, active_BGroup_list)
  {
    // the up_resfld_ isn't declared here, it will be filled in the SolverInterface class.
  }


  // promote base variable order (no need to duplicate it)
  using BaseType::order;
  using BaseType::basis_cell;
  using BaseType::basis_trace;

protected:
  // the solution field
  QFieldType qfld_;
  // the trace Lagrange field
  LGFieldType lgfld_;
  // pointer to the Residual field
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>> up_resfld_;
};



}

#endif /* FIELDBUNDLE_GALERKIN_H_ */
