// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_BDF_H
#define INTEGRANDCELL_GALERKIN_BDF_H

// cell integrand operators: BDF

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "Field/Element/Element.h"
#include "Field/Element/ElementSequence.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: BDF
// Cell-group-level integrand object
//
// integrandPDE = phi*sum( w_j * U_j)/dt
//
// where
//   phi              basis function
//   U_j(x,y)         conservative solution vector at time step j = n, n-1, ...
//   w_j              backwards difference weight for time step j = n, n-1, ...

template <class PDE>
class IntegrandCell_Galerkin_BDF : public IntegrandCellType< IntegrandCell_Galerkin_BDF<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobian

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin_BDF( const PDE& pde, const std::vector<int>& CellGroups,
                              const Real& dt, const std::vector<Real>& weights)
    : pde_(pde), cellGroups_(CellGroups), dt_(dt), weights_(weights)
  {
    // No point in using BDF with a PDE that does not have a time term (i.e. conservative flux)...
    SANS_ASSERT(pde_.hasFluxAdvectiveTime());
  }

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  // Element-level integrand object
  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementSequence<ArrayQ<Real>, TopoDim, Topology> ElementQFieldVecType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef ArrayQ<T> IntegrandType;

    BasisWeighted(  const PDE& pde,
                    const Real& dt,
                    const std::vector<Real>& weights,
                    const ElementParam& paramfldElem,
                    const ElementQFieldType& qfldElem,
                    const ElementQFieldVecType& qfldElemPast) :
      pde_(pde), dt_(dt), weights_(weights),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
      qfldElem_(qfldElem),
      qfldElemPast_(qfldElemPast),
      paramfldElem_( paramfldElem ),
      nDOF_( qfldElem_.nDOF() ),
      phi_( new Real[nDOF_] )
    {
      SANS_ASSERT( qfldElemPast_.nElem()+1 == (int)weights_.size());
      SANS_ASSERT( dt_ > 0 );
    }

    BasisWeighted( BasisWeighted&& bw ) :
      pde_(bw.pde_), dt_(bw.dt_), weights_(bw.weights_),
      xfldElem_(bw.xfldElem_),
      qfldElem_(bw.qfldElem_),
      qfldElemPast_(bw.qfldElemPast_),
      paramfldElem_( bw.paramfldElem_ ),
      nDOF_( bw.nDOF_ ),
      phi_( bw.phi_ )
    {
      bw.phi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    void operator()( const QuadPointType& sRef, IntegrandType integrand[], int neqn ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

  protected:
    template<class Tq, class Ti>
    void weightedIntegrand( const ParamT& param,
                            const ArrayQ<Tq>& q,
                            const std::vector<ArrayQ<Real>>& uPast,
                            ArrayQ<Ti> integrand[], int neqn ) const;
    //Note that these members have the same counterparts in the enclosing class but need to be defined here,
    //since, as of now, there is no straightforward to access those members of the enclosing class.
    const PDE& pde_;
    const Real& dt_;
    const std::vector<Real>& weights_; // BDF weights: weights_[i] corresponds to t_{n-i}, where t_{n} is the most updated time step

    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldVecType& qfldElemPast_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
  };

  // Element-level integrand object
  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const ElementSequence<ArrayQ<Real>, TopoDim, Topology>& qfldElemPast) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, dt_, weights_, paramfldElem, qfldElem, qfldElemPast);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
  const Real& dt_;
  const std::vector<Real>& weights_;
  //weights and solution vector start at current timestep and go backwards
  //i.e. weights_[0] is the weight for timestep n, weights_[1] is the weight for timestep n-1, etc...
};


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_BDF<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

// Cell integrand
//
// integrand = phi*sum(w_n*U_n)/dt
//
// where
//   phi                basis function
//   U_n(x,y)           conservative solution vector at timestep n
//   w_n                BDF weight for timestep n
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_BDF<PDE>::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()(const QuadPointType& sRef, IntegrandType integrand[], int neqn) const
{
  const int ntimes = qfldElemPast_.nElem();  // number of previous time instances in BDF

  SANS_ASSERT( neqn == nDOF_ );

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;           // solution
  ArrayQ<Real> qPast;    // previous solution

  std::vector<ArrayQ<Real>> uPast(ntimes, 0);

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // evaluate basis fcn
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // evaluate solution at all time steps
  qfldElem_.evalFromBasis( phi_, nDOF_, q);

  for (int k = 0; k < ntimes; k++)
  {
    qfldElemPast_[k].evalFromBasis( phi_, nDOF_, qPast );

    pde_.fluxAdvectiveTime(param, qPast, uPast[k]);
  }

  // compute integrand
  weightedIntegrand( param, q, uPast, integrand, neqn);
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_BDF<PDE>::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxPDEElem.m() == nDOF_);
  SANS_ASSERT(mtxPDEElem.n() == nDOF_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  const int ntimes = qfldElemPast_.nElem();  // number of previous time instances in BDF

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;                    // solution
  ArrayQ<SurrealClass> qSurreal = 0; // solution
  ArrayQ<Real> qPast;                // previous solution

  std::vector<ArrayQ<Real>> uPast(ntimes, 0);

  MatrixQ<Real> PDE_q = 0; // temporary storage

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // evaluate solution at all time steps
  qfldElem_.evalFromBasis( phi_, nDOF_, q);
  qSurreal = q;

  for (int k = 0; k < ntimes; k++)
  {
    qfldElemPast_[k].evalFromBasis( phi_, nDOF_, qPast );

    pde_.fluxAdvectiveTime(param, qPast, uPast[k]);
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    weightedIntegrand( param, qSurreal, uPast, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;
  } // nchunk
}

// Cell integrand
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Tq, class Ti>
void
IntegrandCell_Galerkin_BDF<PDE>::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
weightedIntegrand( const ParamT& param,
                   const ArrayQ<Tq>& q,
                   const std::vector<ArrayQ<Real>>& uPast,
                   ArrayQ<Ti> integrand[], int neqn ) const
{
  const int ntimes = qfldElemPast_.nElem();  // number of previous time instances in BDF

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  ArrayQ<Tq> u = 0;
  pde_.fluxAdvectiveTime(param, q, u);

  // BDF term
  ArrayQ<Tq> dudt = weights_[0]*u/dt_;

  for (int k = 0; k < ntimes; k++)
    dudt += weights_[k+1]*uPast[k]/dt_;

  // weight by basis functions
  for (int k=0; k < neqn; k++)
    integrand[k] += phi_[k]*dudt;
}

}

#endif  // INTEGRANDCELL_GALERKIN_BDF_H
