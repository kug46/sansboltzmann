// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_STABILIZED_H
#define INTEGRANDCELL_GALERKIN_STABILIZED_H

// cell integrand operator: Galerkin with Adjoint Stabilization

#include <ostream>
#include <iostream>
#include <iomanip>
#include <vector>

#include "Discretization/Galerkin/Stabilization_Galerkin.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"     // Real
#include "tools/SANSException.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "Field/Element/Element.h"
#include "Discretization/Integrand_Type.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin with Adjoint Stabilization


template <class PDE_>
class IntegrandCell_Galerkin_Stabilized : public IntegrandCellType< IntegrandCell_Galerkin_Stabilized<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>;

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;

  template <class T>
  using TensorMatrixQ = typename PDE::template TensorMatrixQ<T>;    // diffusion matrix

  template <class T>
  using VectorTensorMatrixQ = typename PDE::template VectorTensorMatrixQ<T>;    // diffusion matrix

  template <class T>
  using TensorArrayQ = typename PDE::template TensorArrayQ<T>;

  template <class T>
  using TensorSymArrayQ = typename PDE::template TensorSymArrayQ<T>;

  typedef DLA::MatrixSymS<PhysDim::D, Real> TensorSymX;

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin_Stabilized( const PDE& pde, const std::vector<int>& CellGroups, const StabilizationMatrix& tau )
    : pde_(pde), cellGroups_(CellGroups), tau_(tau) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<Real, TopoDim, Topology> ElementEFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;

    typedef typename ElementParam::T ParamT;
    typedef typename ElementParam::gradT ParamGradT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
                   const StabilizationMatrix& tau) :
                      pde_(pde),
                      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                      qfldElem_(qfldElem),
                      paramfldElem_( paramfldElem ),
                      nDOF_( qfldElem_.nDOF() ),
                      nNodes_( qfldElem_.basis()->nBasisNode() ),
                      phi_( new Real[nDOF_] ),
                      gradphi_( new VectorX[nDOF_] ),
                      hessphi_( new TensorSymX[nDOF_] ),
                      order_( qfldElem_.basis()->order() ),
                      tau_(tau),
                      stabType_( tau_.getStabType()),
                      stabElem_( tau.getStabOrder(), qfldElem_.basis()->category() ),
                      phiLin_( new Real[stabElem_.nDOF()] ),
                      gradphiLin_( new VectorX[stabElem_.nDOF()] )
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                      pde_(bw.pde_),
                      xfldElem_(bw.xfldElem_),
                      qfldElem_(bw.qfldElem_),
                      paramfldElem_( bw.paramfldElem_ ),
                      nDOF_( bw.nDOF_ ),
                      nNodes_( bw.nNodes_ ),
                      phi_( bw.phi_ ),
                      gradphi_( bw.gradphi_ ),
                      hessphi_( bw.hessphi_ ),
                      order_( bw.order_ ),
                      tau_(bw.tau_),
                      stabType_( bw.stabType_ ),
                      stabElem_( tau_.getStabOrder(), qfldElem_.basis()->category() ),
                      phiLin_( bw.phiLin_ ),
                      gradphiLin_( bw.gradphiLin_ )
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr; bw.hessphi_ = nullptr;
      bw.phiLin_ = nullptr; bw.gradphiLin_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
      delete [] hessphi_;
      delete [] phiLin_;
      delete [] gradphiLin_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn ) const;

    template<class Ti>
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem ) const;

    // cell element integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

  protected:
    template <class Tq, class Tg, class Th, class Tk, class Tpq, class Tt, class Ti>
    void weightedIntegrand( const ParamT& param, const ParamGradT& gradparam,
                            const ArrayQ<Tq>& q, const ArrayQ<Real>& qR,
                            const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Real>& gradqR,
                            const TensorSymArrayQ<Th>& hessq, const TensorSymArrayQ<Real>& hessqR,
                            const TensorMatrixQ<Tk>& K, const TensorMatrixQ<Real>& KR,
                            const StabilizationMatrix::TauInverse<PDE::N, Tpq, Tt>& tau,
                            ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nNodes_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable TensorSymX *hessphi_;
    const int order_;
    const StabilizationMatrix& tau_;
    const StabilizationType stabType_;

    const ElementEFieldType stabElem_;
    mutable Real *phiLin_;
    mutable VectorX *gradphiLin_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, tau_);
  }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<Real, TopoDim, Topology> ElementEFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;

    typedef typename ElementParam::T ParamT;
    typedef typename ElementParam::gradT ParamGradT;
    typedef QuadraturePoint<TopoDim> QuadPointType;

    FieldWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
                   const Element<Real, TopoDim, Topology>& efldElem,
                   const StabilizationMatrix& tau) :
                     pde_(pde),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem),
                     wfldElem_(wfldElem),
                     efldElem_(efldElem),
                     paramfldElem_(paramfldElem),
                     tau_(tau),
                     order_(qfldElem_.basis()->order()),
                     nDOF_(qfldElem_.nDOF()),
                     nNodes_( qfldElem_.basis()->nBasisNode() ),
                     nPhi_( efldElem_.nDOF()),
                     ephi_( new Real[nPhi_] ), // basis associated with efld
                     gradephi_( new VectorX[nPhi_] ),
                     hessephi_( new TensorSymX[nPhi_] ),
                     weight_( new ArrayQ<T>[nPhi_] ),
                     gradWeight_( new VectorArrayQ<T>[nPhi_] ),
                     // gradWeightStab_( new VectorArrayQ<T>[nPhi_] ),
                     // hessWeightStab_( new TensorSymArrayQ<T>[nPhi_] ),
                     gradphi_( new VectorX[nDOF_] ), // basis associated with qfld
                     stabType_( tau_.getStabType()),
                     stabElem_( tau_.getStabOrder(), qfldElem_.basis()->category() ),
                     phiLin_( new Real[stabElem_.nDOF()] ),
                     gradphiLin_( new VectorX[stabElem_.nDOF()] )
    {
    }

    FieldWeighted( FieldWeighted&& fw ) :
                     pde_(fw.pde_),
                     xfldElem_(fw.xfldElem_),
                     qfldElem_(fw.qfldElem_),
                     wfldElem_(fw.wfldElem_),
                     efldElem_(fw.efldElem_),
                     paramfldElem_(fw.paramfldElem_),
                     tau_(fw.tau_),
                     order_(fw.order_),
                     nDOF_(fw.nDOF_),
                     nNodes_( fw.nNodes_ ),
                     nPhi_( fw.nPhi_ ),
                     ephi_( fw.ephi_ ),
                     gradephi_( fw.gradephi_ ),
                     hessephi_( fw.hessephi_ ),
                     weight_( fw.weight_ ),
                     gradWeight_( fw.gradWeight_ ),
                     // gradWeightStab_( fw.gradWeightStab_ ),
                     // hessWeightStab_( fw.hessWeightStab_ ),
                     gradphi_( fw.gradphi_ ),
                     stabType_( tau_.getStabType()),
                     stabElem_( tau_.getStabOrder(), qfldElem_.basis()->category() ),
                     phiLin_( fw.phiLin_ ),
                     gradphiLin_( fw.gradphiLin_ )
    {
      fw.ephi_ = nullptr; fw.gradephi_ = nullptr; fw.hessephi_ = nullptr;
      fw.weight_ = nullptr; fw.gradWeight_ = nullptr;
      fw.gradphi_ = nullptr;
      fw.phiLin_ = nullptr; fw.gradphiLin_ = nullptr;
      //fw.gradWeightStab_ = nullptr; fw.hessWeightStab_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] ephi_;
      delete [] gradephi_;
      delete [] hessephi_;
      delete [] weight_;
      delete [] gradWeight_;
      // delete [] gradWeightStab_;
      // delete [] hessWeightStab_;
      delete [] gradphi_;
      delete [] phiLin_;
      delete [] gradphiLin_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    template <class Ti>
    void operator()( const QuadPointType& sRef, Ti integrand[], const int nphi ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& wfldElem_;
    const ElementEFieldType& efldElem_;
    const ElementParam& paramfldElem_;
    const StabilizationMatrix& tau_;
    const int order_;
    const int nDOF_;
    const int nNodes_;

    const int nPhi_;
    mutable Real *ephi_; // efld basis
    mutable VectorX *gradephi_;
    mutable TensorSymX *hessephi_;
    mutable ArrayQ<T> *weight_;
    mutable VectorArrayQ<T> *gradWeight_;
    // mutable VectorArrayQ<T> *gradWeightStab_;
    // mutable TensorSymArrayQ<T> *hessWeightStab_;
    mutable VectorX *gradphi_; // qfld basis gradient
    const StabilizationType stabType_;

    const ElementEFieldType stabElem_;
    mutable Real *phiLin_;
    mutable VectorX *gradphiLin_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const Element<Real, TopoDim, Topology>& efldElem) const
  {
    return FieldWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, wfldElem, efldElem, tau_);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
  const StabilizationMatrix& tau_;
};

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_Stabilized<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_Stabilized<PDE>::FieldWeighted<T, TopoDim, Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

// Residual interface
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin_Stabilized<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn) const
{
  typedef typename Scalar<typename promote_Surreal<ParamT, Real>::type>::type Tpq;

  //const int neqn = integrand.m();
  SANS_ASSERT(neqn == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)
  ParamGradT gradparam;

  ArrayQ<T> q = 0;                 // solution
  VectorArrayQ<T> gradq = 0;       // gradient
  TensorSymArrayQ<T> hessq = 0;    // solution hessian

  // Elemental parameters
  paramfldElem_.eval( sRef, param );
  paramfldElem_.evalDerivative(sRef, gradparam);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  if (pde_.hasFluxViscous() && order_ > 1)
  {
    xfldElem_.evalBasisHessian( sRef, qfldElem_, hessphi_, nDOF_ );
    qfldElem_.evalFromBasis( hessphi_, nDOF_, hessq );
  }

  if ( tau_.getStabType() != StabilizationType::Unstabilized )
  {
    stabElem_.evalBasis(sRef, phiLin_, stabElem_.nDOF());
    xfldElem_.evalBasisGradient( sRef, stabElem_, gradphiLin_, stabElem_.nDOF() );
  }

  // Compute and factorize the tau matrix
  TensorMatrixQ<Real> K = 0;

  if (pde_.hasFluxViscous())
    pde_.diffusionViscous( param, q, gradq, K );

  MatrixQ<Real> S = 0;
  if (pde_.hasSource())
    pde_.jacobianSourceAbsoluteValue(param, q, gradq, S);

  StabilizationMatrix::TauInverse<PDE::N, Tpq, Real> tau
    = tau_.template factorize<Tpq, Real, Real, Real>(pde_, param, q, K, S, phiLin_, gradphiLin_, stabElem_.order(),
                                                     nNodes_, stabElem_.nDOF());

  weightedIntegrand(param, gradparam, q, q, gradq, gradq, hessq, hessq, K, K, tau, integrand, neqn);
}

// Residual interface
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin_Stabilized<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti>>& rsdPDEElem) const
{
  typedef typename Scalar<typename promote_Surreal<ParamT, Real>::type>::type Tpq;

  SANS_ASSERT(rsdPDEElem.m() == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)
  ParamGradT gradparam;

  ArrayQ<T> q = 0;                 // solution
  VectorArrayQ<T> gradq = 0;       // gradient
  TensorSymArrayQ<T> hessq = 0;    // solution hessian

  // Elemental parameters
  paramfldElem_.eval( sRef, param );
  paramfldElem_.evalDerivative(sRef, gradparam);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  if (pde_.hasFluxViscous() && order_ > 1)
  {
    xfldElem_.evalBasisHessian( sRef, qfldElem_, hessphi_, nDOF_ );
    qfldElem_.evalFromBasis( hessphi_, nDOF_, hessq );
  }

  DLA::VectorD<ArrayQ<Ti>> integrand(nDOF_);

  if ( tau_.getStabType() != StabilizationType::Unstabilized )
  {
    stabElem_.evalBasis(sRef, phiLin_, stabElem_.nDOF());
    xfldElem_.evalBasisGradient( sRef, stabElem_, gradphiLin_, stabElem_.nDOF() );
  }

  // Compute and factorize the tau matrix
  TensorMatrixQ<Real> K = 0;

  if (pde_.hasFluxViscous())
    pde_.diffusionViscous( param, q, gradq, K );

  MatrixQ<Real> S = 0;
  if (pde_.hasSource())
    pde_.jacobianSourceAbsoluteValue(param, q, gradq, S);

  StabilizationMatrix::TauInverse<PDE::N, Tpq, Real> tau
    = tau_.template factorize<Tpq, Real, Real, Real>(pde_, param, q, K, S, phiLin_, gradphiLin_, stabElem_.order(),
                                                     nNodes_, stabElem_.nDOF());

  weightedIntegrand(param, gradparam, q, q, gradq, gradq, hessq, hessq, K, K, tau, integrand.data(), integrand.size());

  for (int k = 0; k < nDOF_; ++k)
    rsdPDEElem(k) += dJ*integrand(k);
}

// Jacobian interface
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_Stabilized<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  //const int neqn = integrand.m();
  SANS_ASSERT(mtxPDEElem.m() == nDOF_);
  SANS_ASSERT(mtxPDEElem.n() == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)
  ParamGradT gradparam;

  ArrayQ<T> q = 0;                 // solution
  VectorArrayQ<T> gradq = 0;       // gradient
  TensorSymArrayQ<T> hessq = 0;    // solution hessian

  ArrayQ<SurrealClass> qSurreal = 0;               // solution
  VectorArrayQ<SurrealClass> gradqSurreal = 0;     // gradient
  TensorSymArrayQ<SurrealClass> hessqSurreal = 0;  // solution hessian

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0, PDE_hessq = 0; // temporary storage

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  // Elemental parameters
  paramfldElem_.eval( sRef, param );
  paramfldElem_.evalDerivative(sRef, gradparam);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  if (tau_.getStabType() != StabilizationType::Unstabilized )
  {
    stabElem_.evalBasis(sRef, phiLin_, stabElem_.nDOF());
    xfldElem_.evalBasisGradient( sRef, stabElem_, gradphiLin_, stabElem_.nDOF() );
  }

  qSurreal = q;
  gradqSurreal = gradq;

  // only need hess if used
  if (pde_.hasFluxViscous() && order_ > 1 && tau_.getStabType() != StabilizationType::Unstabilized)
  {
    xfldElem_.evalBasisHessian(  sRef, qfldElem_, hessphi_, nDOF_ );
    qfldElem_.evalFromBasis( hessphi_, nDOF_, hessq );
    hessqSurreal = hessq;
  }

  TensorMatrixQ<Real> KR = 0;
  if (pde_.hasFluxViscous())
    pde_.diffusionViscous( param, q, gradq, KR );

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  MatrixQ<Real> SR = 0;
  if (pde_.hasSource())
    pde_.jacobianSourceAbsoluteValue(param, q, gradq, SR);

  StabilizationMatrix::TauInverse<PDE::N, Real, Real> tauR
    = tau_.template factorize<Real, Real, Real, Real>(pde_, param, q, KR, SR, phiLin_, gradphiLin_, stabElem_.order(),
                                                      nNodes_, stabElem_.nDOF());

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;


    integrandSurreal = 0;

    if (tau_.getStabType() == StabilizationType::AGLSAdjoint)
    {
      weightedIntegrand(param, gradparam, qSurreal, q, gradq, gradq, hessq, hessq, KR, KR, tauR, integrandSurreal.data(), integrandSurreal.size());
    }
    else
    {
      // Compute and factorize the tau matrix
      TensorMatrixQ<SurrealClass> K = 0;
      if (pde_.hasFluxViscous())
        pde_.diffusionViscous( param, qSurreal, gradq, K );

      MatrixQ<SurrealClass> S = 0;
      if (pde_.hasSource())
        pde_.jacobianSourceAbsoluteValue(param, qSurreal, gradq, S);

      StabilizationMatrix::TauInverse<PDE::N, SurrealClass, SurrealClass> tau = tau_.template factorize
          <SurrealClass, SurrealClass, SurrealClass, SurrealClass>(pde_, param, qSurreal, K, S, phiLin_, gradphiLin_, stabElem_.order(),
                                                                   nNodes_, stabElem_.nDOF());

      weightedIntegrand(param, gradparam,  qSurreal, q, gradq, gradq, hessq, hessq, K, KR, tau, integrandSurreal.data(), integrandSurreal.size());
    }


    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;
  } // nchunk

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    integrandSurreal = 0;

    if (tau_.getStabType() == StabilizationType::AGLSAdjoint)
    {
      weightedIntegrand(param, gradparam, q, q, gradqSurreal, gradq, hessq, hessq, KR, KR, tauR, integrandSurreal.data(), integrandSurreal.size());
    }
    else
    {
      // Compute and factorize the tau matrix
      TensorMatrixQ<SurrealClass> K = 0;
      if (pde_.hasFluxViscous())
        pde_.diffusionViscous( param, q, gradqSurreal, K );

      MatrixQ<SurrealClass> S = 0;
      if (pde_.hasSource())
        pde_.jacobianSourceAbsoluteValue(param, q, gradqSurreal, S);

      StabilizationMatrix::TauInverse<PDE::N, SurrealClass, SurrealClass> tau
      = tau_.template factorize<SurrealClass, Real, SurrealClass, SurrealClass>(pde_, param, q, K, S, phiLin_, gradphiLin_, stabElem_.order(),
                                                                                nNodes_, stabElem_.nDOF());

      weightedIntegrand(param, gradparam, q, q, gradqSurreal, gradq, hessq, hessq, K, KR, tau, integrandSurreal.data(), integrandSurreal.size());
    }



    // accumulate derivatives into element jacobian

    slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOF_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOF_; j++)
            mtxPDEElem(i,j) += dJ*gradphi_[j][d]*PDE_gradq;
        }
      }
      slot += PDE::N;
    }
  } // nchunk


  if (pde_.hasFluxViscous() && order_ > 1 && tau_.getStabType() != StabilizationType::Unstabilized )
  {
    const int nHess = PhysDim::D*(PhysDim::D + 1)/2;

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nHess*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurreal(ii,jj), n).deriv(slot + n - nchunk) = 1;
          slot += PDE::N;
        }

      integrandSurreal = 0;

      weightedIntegrand(param, gradparam, q, q, gradq, gradq, hessqSurreal, hessq, KR, KR, tauR, integrandSurreal.data(), integrandSurreal.size());

      // accumulate derivatives into element jacobian

      slot = 0;
      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurreal(ii,jj), n).deriv(slot + n - nchunk) = 0; // Reset the derivative

            for (int i = 0; i < nDOF_; i++)
            {
              // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
              for (int m = 0; m < PDE::N; m++)
                for (int n = 0; n < PDE::N; n++)
                  DLA::index(PDE_hessq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

              for (int j = 0; j < nDOF_; j++)
                mtxPDEElem(i,j) += dJ*hessphi_[j](ii,jj)*PDE_hessq;
            }
          }
          slot += PDE::N;
        }
    } // nchunk
  }

//  std::cout << "mtx:" << mtxPDEElem << "\n\n";

}

// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Tq, class Tg, class Th, class Tk, class Tpq, class Tt, class Ti>
void
IntegrandCell_Galerkin_Stabilized<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
weightedIntegrand(const ParamT& param, const ParamGradT& gradparam,
                  const ArrayQ<Tq>& q, const ArrayQ<Real>& qR,
                  const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Real>& gradqR,
                  const TensorSymArrayQ<Th>& hessq, const TensorSymArrayQ<Real>& hessqR,
                  const TensorMatrixQ<Tk>& K, const TensorMatrixQ<Real>& KR,
                  const StabilizationMatrix::TauInverse<PDE::N, Tpq, Tt>& tau,
                  ArrayQ<Ti> integrand[], int neqn) const
{
  typedef typename promote_Surreal<Tg, Tq>::type Tqg;

  VectorArrayQ<Tqg> F = 0;           // PDE flux F(X, Q), Fv(X, Q, QX)
  ArrayQ<Tqg> source = 0;            // PDE source S(X, Q, QX)

  // using neqn here suppresses clang analyzer
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // Galerkin flux term: -gradient(phi) . F

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, q, gradq, F );

    for (int k = 0; k < neqn; k++)
      integrand[k] -= dot(gradphi_[k],F);
  }

  // Galerkin source term: +phi S

  if (pde_.hasSource())
  {
    pde_.source( param, q, gradq, source );

    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  ArrayQ<Real> forcing = 0;
  if (pde_.hasForcingFunction())
  {
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < neqn; k++)
      integrand[k] -= phi_[k]*forcing;
  }

  // adjoint stabilization: - adjointPDE(phi) . tau . primalPDE(q)
  if (stabType_ != StabilizationType::Unstabilized)
  {
    // strong form PDE residual
    ArrayQ<Ti> strongPDE = 0;
    if (stabType_ == StabilizationType::AGLSAdjoint)
      tau_.template constructAdjointPDE<Tq, Tg, Th, Ti>(pde_, param, q, qR, gradq, gradqR, hessq, hessqR, KR, order_, strongPDE);
    else
      tau_.template constructStrongPDE<Tq, Tg, Th, Ti>(pde_, param, q, gradq, hessq, source, forcing, strongPDE);

    // apply stabilization matrix tau
    tau_.backsolve(tau, strongPDE);

    if (stabType_ == StabilizationType::SUPG)
    {
      tau_.template operatorSUPG<Tq, Tg, Ti>(pde_, param, q, gradq, gradphi_, strongPDE, neqn, integrand);
    }
    else if (stabType_ == StabilizationType::GLS || stabType_ == StabilizationType::AGLSPrimal)
    {
      tau_.template operatorGLS<Tq, Tg, Th, Tk, Ti>(pde_, param, q, gradq, hessq, K, phi_, gradphi_, hessphi_, strongPDE, neqn, order_, integrand);
    }
    else if (stabType_ == StabilizationType::Adjoint || stabType_ == StabilizationType::AGLSAdjoint)
    {
#if 0
      tau_.template operatorAdjoint<Tq, Tg, Th, Tk, Ti>
        (pde_, param, q, gradq, hessq, K, phi_, gradphi_, hessphi_, strongPDE, neqn, order_, integrand);
#else
      tau_.template operatorAdjoint<Real, Real, Real, Real, Ti>
        (pde_, param, gradparam, qR, gradqR, hessqR, KR, phi_, gradphi_, hessphi_, strongPDE, neqn, order_, integrand);
#endif
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION("INVALID STABILIZATION");
    }

  }

}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_Stabilized<PDE>::
FieldWeighted<T, TopoDim, Topology, ElementParam>::
operator()( const QuadPointType& sRef, Ti integrand[], const int nphi ) const
{
  SANS_ASSERT( nphi == nPhi_ );
  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)
  ParamGradT gradparam;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q = 0;               // solution
  VectorArrayQ<T> gradq = 0;     // solution gradient
  TensorSymArrayQ<T> hessq = 0;  // solution hessian

  ArrayQ<T> w = 0;               // weight
  VectorArrayQ<T> gradw = 0;     // weight gradient
  TensorSymArrayQ<T> hessw = 0;  // weight hessian

  // Elemental parameters
  paramfldElem_.eval( sRef, param );
  paramfldElem_.evalDerivative( sRef, gradparam );

  // solution value, gradient
  qfldElem_.eval( sRef, q );
  xfldElem_.evalGradient( sRef, qfldElem_, gradq );

  // weight value, gradient
  wfldElem_.eval( sRef, w );
  xfldElem_.evalGradient( sRef, wfldElem_, gradw );

  // solution and weight hessian
  if ( pde_.hasFluxViscous() &&  order_ > 1 )
    xfldElem_.evalHessian( sRef, qfldElem_, hessq );

  if (  pde_.hasFluxViscous() && wfldElem_.basis()->order() > 1 )
    xfldElem_.evalHessian( sRef, wfldElem_, hessw );

  // evaluate basis function gradient
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // estimate basis, gradient
  efldElem_.evalBasis( sRef, ephi_, nPhi_ );
  xfldElem_.evalBasisGradient( sRef, efldElem_, gradephi_, nPhi_ );

  if (tau_.getStabType() != StabilizationType::Unstabilized )
  {
    stabElem_.evalBasis(sRef, phiLin_, stabElem_.nDOF());
    xfldElem_.evalBasisGradient( sRef, stabElem_, gradphiLin_, stabElem_.nDOF() );
  }

  for (int k = 0; k < nPhi_; k++)
  {
    integrand[k] = 0;
    weight_[k] = ephi_[k]*w;
    gradWeight_[k] = gradephi_[k]*w + ephi_[k]*gradw;
  }

#if 0
  std::cout<< "IntegrandCell:" << integrand <<std::endl;
#endif
  // Galerkin flux term: -(phi grad w + w grad phi, F)
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;
    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, q, gradq, F );

    for (int d = 0; d < PhysDim::D; d++)
      for (int k = 0; k < nPhi_; k++)
        integrand[k] -= dot(gradWeight_[k][d],F[d]);
  }

  // Galerkin source term: +phi S

  ArrayQ<Ti> source=0;   // PDE source S(X, D, U, UX), S(X)
  if (pde_.hasSource())
  {
    pde_.source( param, q, gradq, source );

    for (int k = 0; k < nPhi_; k++)
      integrand[k] += dot(weight_[k],source);
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  ArrayQ<Real> forcing = 0;
  if (pde_.hasForcingFunction())
  {
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < nPhi_; k++)
      integrand[k] -= dot(weight_[k],forcing);
  }


  if (stabType_ == StabilizationType::SUPG || stabType_== StabilizationType::GLS || stabType_== StabilizationType::Adjoint )
  {
    // stabilization: - P(phi) . tau . primalPDE(q)
    // test function weighting done at whole integrand
    // integrand[k] = ephi[k]* - P(phi) . tau . primalPDE(q)


    //NO STABILIZATION TERM NECESSARY FOR UNSTABILIZED, AGLS

    //contruct strongPDE
    ArrayQ<T> strongPDE = 0;
    tau_.template constructStrongPDE<T, T, T, T>(pde_, param, q, gradq, hessq, source, forcing, strongPDE);

    // adjoint PDE
    TensorMatrixQ<Ti> K = 0;

    if (pde_.hasFluxViscous())
      pde_.diffusionViscous( param, q, gradq, K );

    MatrixQ<Ti> S = 0;
    if (pde_.hasSource())
      pde_.jacobianSourceAbsoluteValue(param, q, gradq, S);

    tau_.template solve<T,T,T>(pde_, param, q, K, S, phiLin_, gradphiLin_, stabElem_.order(),
                               nNodes_, stabElem_.nDOF(), strongPDE);

    Ti stabIntegrand = 0;
    if (stabType_ == StabilizationType::SUPG)
    {
      tau_.template operatorSUPGFW<T, Ti>(pde_, param, q, gradq, gradw, strongPDE, stabIntegrand);
    }
    else if (stabType_ == StabilizationType::GLS)
    {
      tau_.template operatorGLSFW<T, Ti>(pde_, param, q, gradq, hessq, K, w, gradw, hessw, strongPDE, wfldElem_.order(), stabIntegrand);
    }
    else if (stabType_ == StabilizationType::Adjoint)
    {
      tau_.template operatorAdjointFW<T, Ti>
        (pde_, param, gradparam, q, gradq, hessq, K, w, gradw, hessw, strongPDE, wfldElem_.order(), stabIntegrand);
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION("INVALID STABILIZATION");
    }

    // sum the terms into the integrand
    for (int k = 0; k < nPhi_; k++)
      integrand[k] += ephi_[k]*stabIntegrand;
  }

}


} // namepace SANS

#endif  // INTEGRANDCELL_GALERKIN_STABILIZED_H
