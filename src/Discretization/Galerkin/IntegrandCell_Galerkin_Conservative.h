// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_CONSERVATIVE_H
#define INTEGRANDCELL_GALERKIN_CONSERVATIVE_H

// cell integrand operators: Integrates conservative variables (used for time marching)

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Field/Element/Element.h"
#include "Field/Element/ElementSequence.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Conservative
//
// integrandPDE = phi*U(x,y)
//
// where
//   phi            basis function
//   U(x,y)         conservative solution vector

template <class PDE>
class IntegrandCell_Galerkin_Conservative
    : public IntegrandCellType< IntegrandCell_Galerkin_Conservative<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobian

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin_Conservative( const PDE& pde, const std::vector<int>& CellGroups, const Real& weight )
    : pde_(pde), cellGroups_(CellGroups), weight_(weight) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted( const PDE& pde,
                   const Real& weight,
                   const ElementParam& paramfldElem,
                   const ElementQFieldType& qfldElem ) :
                     pde_(pde),
                     weight_(weight),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem),
                     paramfldElem_( paramfldElem ),
                     nDOF_( qfldElem_.nDOF() ),
                     phi_( new Real[nDOF_] )
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                     pde_(bw.pde_),
                     weight_(bw.weight_),
                     xfldElem_(bw.xfldElem_),
                     qfldElem_(bw.qfldElem_),
                     paramfldElem_( bw.paramfldElem_ ),
                     nDOF_( bw.nDOF_ ),
                     phi_( bw.phi_ )
    {
      bw.phi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    void operator()( const QuadPointType& sRef, ArrayQ<T> integrand[], int neqn ) const;

    void operator()( const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<T> >& rsdPDEElem ) const;


    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

  protected:
    template<class Tq, class Ti>
    void weightedIntegrand( const ParamT& param,
                            const ArrayQ<Tq>& q,
                            ArrayQ<Ti> integrand[],
                            int neqn ) const;

    const PDE& pde_;
    const Real& weight_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem ) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, weight_, paramfldElem, qfldElem);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
  const Real& weight_;
};


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_Conservative<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

// Cell integrand
//
// integrand = phi*w_n*U_n
//
// where
//   phi              basis function
//   U(x,y)           conservative solution vector at timestep n
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_Conservative<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()( const QuadPointType& sRef, ArrayQ<T> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                  // solution

  // evaluate basis fcn
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // evaluate solution
  qfldElem_.evalFromBasis( phi_, nDOF_, q);

  weightedIntegrand( param, q, integrand, neqn );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_Conservative<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<T> >& rsdPDEElem) const
{
  SANS_ASSERT( rsdPDEElem.m() == nDOF_ );

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                  // solution

  // evaluate basis fcn
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // evaluate solution
  qfldElem_.evalFromBasis( phi_, nDOF_, q);

  DLA::VectorD<ArrayQ<T>> integrand(nDOF_);

  weightedIntegrand( param, q, integrand.data(), integrand.size() );

  for (int k = 0; k < nDOF_; ++k)
    rsdPDEElem(k) += dJ*integrand(k);
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_Conservative<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT( mtxPDEElem.m() == nDOF_ );
  SANS_ASSERT( mtxPDEElem.n() == nDOF_ );

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                   // solution
  ArrayQ<SurrealClass> qSurreal; // solution surrealized
  MatrixQ<Real> PDE_q = 0;       // temporary storage

  // evaluate basis fcn
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // evaluate solution
  qfldElem_.evalFromBasis( phi_, nDOF_, q); qSurreal = q;

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    weightedIntegrand( param, qSurreal, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;
  } // nchunk
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Ti>
void
IntegrandCell_Galerkin_Conservative<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
weightedIntegrand( const ParamT& param,
                   const ArrayQ<Tq>& q,
                   ArrayQ<Ti> integrand[],
                   int neqn ) const
{
  ArrayQ<Tq> u;  // conservative state solution

  // Zero out the integrand
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  if (pde_.hasFluxAdvectiveTime())
  {
    u = 0;
    pde_.fluxAdvectiveTime(param, q, u);

    for (int k=0; k < neqn; k++)
      integrand[k] += phi_[k]*weight_*u;
  }
}

}

#endif  // INTEGRANDCELL_GALERKIN_CONSERVATIVE_H
