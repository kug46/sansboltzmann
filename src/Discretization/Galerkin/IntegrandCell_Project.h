// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_PROJECT_H
#define INTEGRANDCELL_PROJECT_H

// cell integrand operator: global cell L2 projection of a field

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "Field/Element/Element.h"
#include "Field/Tuple/ElementTuple.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin Project
// integrand inside the integral \int_cell phi*(qTo - qFrom) = 0

template < class PDE_ >
class IntegrandCell_Project : public IntegrandCellType< IntegrandCell_Project<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template< class Z = Real >
  using ArrayQ = typename PDE::template ArrayQ<Z>;

  template< class Z = Real >
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  IntegrandCell_Project( const std::vector<int>& CellGroups )
    : cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class TopoDim, class Topology, class ElementParamType>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<Real>, TopoDim, Topology> ElementFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted(  const ElementParamType& paramfldElem,
                    const ElementFieldType& qTofldElem
                 ) :
                    xfldElem_(get<-1>(paramfldElem)),
                    qFromfldElem_(get<0>(paramfldElem)),
                    qTofldElem_(qTofldElem),
                    nDOFTo_( qTofldElem_.nDOF() ),
                    nDOFFrom_( qFromfldElem_.nDOF() ),
                    phiTo_( nDOFTo_ ),
                    phiFrom_( nDOFFrom_ )
    {
    }

    ~BasisWeighted()
    {
    }

    // total DOFs
    int nDOF() const { return nDOFTo_; }

    // evaluate integrand at reference coordinate (e.g. of quadrature points)
    void operator()( const QuadPointType& sRef, ArrayQ<> integrand[], int neqn ) const;

    void operator()( const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<> >& rsdPDEElem) const;

    // evaluate jacobian of integrand w.r.t. elemental DOFs of solution q (i.e. mass matrix) at reference coordinate (e.g. of quadrature points)
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<>>& mtxMass ) const;

  protected:
    const ElementXFieldType& xfldElem_;
    const ElementFieldType& qFromfldElem_;
    const ElementFieldType& qTofldElem_;

    const int nDOFTo_, nDOFFrom_;
    mutable std::vector<Real> phiTo_, phiFrom_;
  };

  template<class TopoDim, class Topology, class ElementParamType>
  BasisWeighted<TopoDim, Topology, ElementParamType>
  integrand(const ElementParamType& paramfldElem,
            const Element<ArrayQ<Real>, TopoDim, Topology>& qTofldElem) const
  {
    return BasisWeighted<TopoDim, Topology, ElementParamType>(paramfldElem, qTofldElem);
  }

protected:
  const std::vector<int> cellGroups_;
};

//
// solving \int phi*(qTo - qFrom) = 0
//

template <class PDE>
template <class TopoDim, class Topology, class ElementParamType>
void
IntegrandCell_Project<PDE>::
BasisWeighted<TopoDim,Topology,ElementParamType>::
operator()(const QuadPointType& sRef, ArrayQ<> integrand[], int neqn) const
{
  SANS_ASSERT(neqn == nDOFTo_);

  // basis value
  ArrayQ<Real> qTo;
  qTofldElem_.evalBasis( sRef, phiTo_.data(), phiTo_.size() );
  qTofldElem_.evalFromBasis( phiTo_.data(), phiTo_.size(), qTo );

  // basis value
  ArrayQ<Real> qFrom;
  qFromfldElem_.evalBasis( sRef, phiFrom_.data(), phiFrom_.size() );
  qFromfldElem_.evalFromBasis( phiFrom_.data(), phiFrom_.size(), qFrom );


  for (int k = 0; k < neqn; k++)
    integrand[k] = phiTo_[k]*(qFrom- qTo);
}

template <class PDE>
template <class TopoDim, class Topology, class ElementParamType>
void
IntegrandCell_Project<PDE>::
BasisWeighted<TopoDim,Topology,ElementParamType>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<> >& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOFTo_);

  // basis value
  ArrayQ<Real> qTo;
  qTofldElem_.evalBasis( sRef, phiTo_.data(), phiTo_.size() );
  qTofldElem_.evalFromBasis( phiTo_.data(), phiTo_.size(), qTo );

  // basis value
  ArrayQ<Real> qFrom;
  qFromfldElem_.evalBasis( sRef, phiFrom_.data(), phiFrom_.size() );
  qFromfldElem_.evalFromBasis( phiFrom_.data(), phiFrom_.size(), qFrom );

  // compute the residual
  for (int k = 0; k < nDOFTo_; k++)
    rsdPDEElem[k] += dJ*phiTo_[k]*(qFrom- qTo);
}

template <class PDE>
template <class TopoDim, class Topology, class ElementParamType>
void
IntegrandCell_Project<PDE>::
BasisWeighted<TopoDim,Topology,ElementParamType>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<>>& mtxMass) const
{
  SANS_ASSERT(mtxMass.m() == nDOFTo_);
  SANS_ASSERT(mtxMass.n() == nDOFTo_);

  // basis value
  qTofldElem_.evalBasis( sRef, phiTo_.data(), phiTo_.size() );

  Real proj_q; // temporary storage

  // add to the mass matrix
  for (int i = 0; i < nDOFTo_; i++)
  {
    proj_q = dJ*phiTo_[i];
    for (int j = 0; j < nDOFTo_; j++)
      mtxMass(i,j) += proj_q*phiTo_[j];
  }
}

}

#endif  // INTEGRANDCELL_PROJECT_H
