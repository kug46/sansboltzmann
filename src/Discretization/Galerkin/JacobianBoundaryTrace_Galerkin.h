// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_GALERKIN_H
#define JACOBIANBOUNDARYTRACE_GALERKIN_H

// jacobian boundary-trace integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_Galerkin_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_Galerkin_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianBoundaryTrace_Galerkin_impl(const IntegrandBoundaryTrace& fcn,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q){}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL, -1);
    std::vector<int> mapDOFLocalL( nDOFL, -1 );
    std::vector<int> maprsdL( nDOFL, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = mtxGlobalPDE_q_.m();

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal> integral(quadratureorder, nDOFL);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );

    // element jacobians
    MatrixElemClass mtxPDEElemL_qL(nDOFL, nDOFL);
    MatrixElemClass mtxPDEElemLocalL_qL(nDOFL, nDOFL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxPDEElemL_qL = 0.0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), mapDOFGlobalL.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOFL; n++)
        if (mapDOFGlobalL[n] < nDOFpossessed)
        {
          maprsdL[nDOFLocal] = n;
          mapDOFLocalL[nDOFLocal] = mapDOFGlobalL[n];
          nDOFLocal++;
        }

      // no residuals are possessed by this processor
      if (nDOFLocal == 0) continue;

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement(  xfldElemTrace, elem );

      // number of simultaneous derivatives per functor call
      const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(nDOFL); nchunk += nDeriv)
      {
        // associate derivative slots with solution variables

        int slot = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element

        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        integral(
            fcn_.integrand(xfldElemTrace, canonicalTraceL,
                           xfldElemL, qfldElemL),
            get<-1>(xfldElemTrace),
            rsdPDEElemL.data(), nDOFL );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElemL_qL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global
      if (nDOFLocal == nDOFL)
        scatterAdd( mapDOFGlobalL, nDOFLocal, mapDOFGlobalL, mtxPDEElemL_qL, mtxGlobalPDE_q_ );
      else
      {
        // compress in only the DOFs possessed by this processor
        for (int i = 0; i < nDOFLocal; i++)
          for (int j = 0; j < nDOFL; j++)
            mtxPDEElemLocalL_qL(i,j) = mtxPDEElemL_qL(maprsdL[i],j);

        scatterAdd( mapDOFLocalL, nDOFLocal, mapDOFGlobalL, mtxPDEElemLocalL_qL, mtxGlobalPDE_q_ );
      }
    }
  }

protected:

//----------------------------------------------------------------------------//
  template <template <class> class SparseMatrixType>
  void
  scatterAdd(
      std::vector<int>& mapDOFLocalL,
      const int nDOFLocal,
      std::vector<int>& mapDOFGlobalL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, mapDOFLocalL.data(), nDOFLocal, mapDOFGlobalL.data(), mapDOFGlobalL.size() );
  }

#if 0
//----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class TopoDim,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int elemL,
      int mapDOFGlobalL[], const int nDOFL,
      DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
  }
#endif
protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class MatrixQ>
JacobianBoundaryTrace_Galerkin_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_Galerkin( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q)
{
  return { fcn.cast(), mtxGlobalPDE_q };
}


}

#endif  // JACOBIANBOUNDARYTRACE_GALERKIN_H
