// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_RESIDUALCELL_GALERKIN_CUTCELLTRANSITIONIBLUNIFIELD_H_
#define SRC_DISCRETIZATION_GALERKIN_RESIDUALCELL_GALERKIN_CUTCELLTRANSITIONIBLUNIFIELD_H_

// Cell integral residual functions customized for UniField IBL with cut-cell transition

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/GroupIntegral_Type.h"

#include "ResidualCell_Galerkin.h"
#include "IntegrandCell_Galerkin_CutCellTransitionIBLUniField.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field Cell group residual
//
// topology specific group residual
//
// template parameters:
//   Topology                                             element topology (e.g. Triangle)
//   IntegrandCell_Galerkin_cutCellTransitionIBLUniField  integrand class
//   XFieldType                                           Grid data type, possibly a tuple with parameters

//----------------------------------------------------------------------------//
//  Galerkin cell group integral class specialized for cut-cell transition IBL with a matching field (for transition front conservation laws)

template<class VarType,
         template<class, class> class PDENDConvert,
         template<class> class Vector, class TR>
class ResidualCell_Galerkin_impl<
        IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > >,
        Vector, TR > :
    public GroupIntegralCellType<
             ResidualCell_Galerkin_impl<
               IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > >,
               Vector,TR> >
{
public:
  typedef IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > > IntegrandCellClass;

  typedef typename IntegrandCellClass::PhysDim PhysDim;
  typedef typename IntegrandCellClass::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCellClass::template ArrayQ<TR> ArrayQR;

  typedef typename DLA::VectorD<ArrayQR> ResidualElemClass;

  // Save off the cell integrand and the residual vector
  ResidualCell_Galerkin_impl( const IntegrandCellClass& fcn,
                              Vector<ArrayQR>& rsdPDEGlobal) :
    fcn_(fcn),
    rsdPDEGlobal_(rsdPDEGlobal) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim, class QBundleFieldType>
  void check( const QBundleFieldType& qfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // number of integrals evaluated
    const int nDOFPDE = qfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFPDEGlobal( nDOFPDE, -1 );
    std::vector<int> mapDOFPDELocal( nDOFPDE, -1 );
    std::vector<int> maprsdElemPDE( nDOFPDE, -1 );
    int nDOFPDELocal = 0;
    const int nDOFPDEpossessed = rsdPDEGlobal_.m();

    // residual array
    ResidualElemClass rsdElemPDE( nDOFPDE );

    // element integral
    GalerkinWeightedIntegral_New_TransitionIBL<TopoDim, Topology, ResidualElemClass> integral(quadratureorder);

    auto integrandFunctor = fcn_.integrand( xfldElem, qfldElem );
    // Create a functor only once for speed.  Note that the parameters are passed into the functor as const references,
    // but their values can still be modified by agents other than this functor.

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFPDEGlobal.data(), mapDOFPDEGlobal.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFPDELocal = 0;
      for (int n = 0; n < nDOFPDE; n++)
      {
        if (mapDOFPDEGlobal[n] < nDOFPDEpossessed)
        {
          maprsdElemPDE[nDOFPDELocal] = n;
          mapDOFPDELocal[nDOFPDELocal] = mapDOFPDEGlobal[n];
          nDOFPDELocal++;
        }
      }

      // no need to compute residuals if all the equations are possessed by other processors
      if (nDOFPDELocal == 0) continue;

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // reset residuals
      rsdElemPDE = 0.0;

      // calculate element integral residuals
      integral( integrandFunctor, get<-1>(xfldElem), rsdElemPDE );

      // add element residuals to global
      for (int n = 0; n < nDOFPDELocal; n++)
        rsdPDEGlobal_[ mapDOFPDELocal[n] ] += rsdElemPDE[ maprsdElemPDE[n] ];
    }
  }

protected:
  const IntegrandCellClass& fcn_;
  Vector<ArrayQR>& rsdPDEGlobal_;
};

} //namespace SANS

#endif /* SRC_DISCRETIZATION_GALERKIN_RESIDUALCELL_GALERKIN_CUTCELLTRANSITIONIBLUNIFIELD_H_ */
