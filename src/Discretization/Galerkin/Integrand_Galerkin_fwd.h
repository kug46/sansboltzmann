// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRAND_GALERKIN_FWD_H
#define INTEGRAND_GALERKIN_FWD_H

namespace SANS
{

//----------------------------------------------------------------------------//
// Tags to indiciate a Galerkin type integrands

class Galerkin {};
class Galerkin_manifold {};

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template <class PDE>
class IntegrandCell_Galerkin;

template<class PDE_>
class IntegrandCell_Galerkin_manifold;

//----------------------------------------------------------------------------//
// Trace integrand: Galerkin

template <class PDE>
class IntegrandInteriorTrace_Galerkin;

// Forward declaration
class avgTraceNormal_GalerkinManifoldInteriorTrace;
class sepTraceNormal_GalerkinManifoldInteriorTrace;

#if 0
template<class PDE_, class TraceNormalType = avgTraceNormal_GalerkinManifoldInteriorTrace>
class IntegrandInteriorTrace_Galerkin_manifold;
#else
template<class PDE_, class TraceNormalType = sepTraceNormal_GalerkinManifoldInteriorTrace>
class IntegrandInteriorTrace_Galerkin_manifold;
#endif
}

#endif
