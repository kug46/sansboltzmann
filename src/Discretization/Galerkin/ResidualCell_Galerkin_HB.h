// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ResidualCell_Galerkin_HB_H
#define ResidualCell_Galerkin_HB_H

// Cell BDF integral residual functions

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/FieldSequence.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementSequence.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  BDF cell group integral
//

template<class IntegrandCell, template<class> class Vector>
class ResidualCell_Galerkin_HB_impl :
    public GroupIntegralCellType< ResidualCell_Galerkin_HB_impl<IntegrandCell, Vector> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualCell_Galerkin_HB_impl( const IntegrandCell& fcn,
                                  Vector<ArrayQ>& rsdPDEGlobal,
                                  const int& mytime) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), mytime_(mytime) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldSequence<PhysDim, TopoDim, ArrayQ>& flds ) const
  {
//    SANS_ASSERT( rsdPDEGlobal_.m() == flds.nDOFpossessed() );
  }


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class ParamFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename ParamFieldType::template FieldCellGroupType<Topology>& pfldCell,
             const typename FieldSequence<PhysDim, TopoDim, ArrayQ>::
                            template FieldCellGroupType<Topology>& qfldsCell,
             const int quadratureorder )
  {
    typedef typename ParamFieldType                           ::template FieldCellGroupType<Topology> PFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ         >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename XField< PhysDim, TopoDim                >::template FieldCellGroupType<Topology> XFieldCellGroupType;

    typedef typename PFieldCellGroupType::template ElementType<> ElementPFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;


    SANS_ASSERT( pfldCell.nElem() == qfldsCell[0].nElem() );
    // element field variables
    ElementPFieldClass pfldElem( pfldCell.basis() );
    ElementQFieldClass qfldElem( qfldsCell[0].basis() );

    // number of integrals evaluated
    const int nflds = qfldsCell.nFieldGroups();

    const int nDOF = qfldElem.nDOF();
    const int nIntegrand = nDOF;

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, ArrayQ > integral(quadratureorder, nIntegrand);

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nIntegrand, -1 );
    std::vector<int> mapDOFLocal( nIntegrand, -1 );
    std::vector<int> maprsd( nIntegrand, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = rsdPDEGlobal_.m();

    // residual array
    std::vector< ArrayQ > rsdElem( nIntegrand );
    std::vector< ArrayQ > rsdElemtmp( nIntegrand );

    // loop over elements within group
    const int nelem = pfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qfldsCell[mytime_].associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nIntegrand );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nIntegrand; n++)
        if (mapDOFGlobal[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
          nDOFLocal++;
        }

      // no need if all DOFs are possessed by other processors
      if (nDOFLocal == 0) continue;

      // copy global grid/solution DOFs to element
      pfldCell.getElement( pfldElem, elem );

      for (int n = 0; n < nIntegrand; n++)
      {
        rsdElem[n] = 0;
        rsdElemtmp[n] = 0;
      }

      // cell integration for canonical element

      for (int i=0; i<nflds; i++)
      {
        if (i != mytime_)
        {
          qfldsCell[i].getElement( qfldElem, elem );

          integral( fcn_.integrand(pfldElem, qfldElem, mytime_, i),
                    get<ElementXFieldClass>(pfldElem), rsdElemtmp.data(), nIntegrand );

          for (int n = 0; n < nIntegrand; n++)
            rsdElem[n] += rsdElemtmp[n];

        }
      }

      // scatter-add element integral to global
      qfldsCell[mytime_].associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nDOF );

      // scatter-add element integral to global
      for (int n = 0; n < nDOFLocal; n++)
        rsdPDEGlobal_[ mapDOFLocal[n] ] += rsdElem[ maprsd[n] ];
    }
  }

protected:
  const IntegrandCell& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  const int& mytime_;
};

// Factory function

template<class IntegrandCell, class ArrayQ, template<class> class Vector>
ResidualCell_Galerkin_HB_impl<IntegrandCell, Vector>
ResidualCell_Galerkin_HB( const IntegrandCellType<IntegrandCell>& fcn,
                           Vector<ArrayQ>& rsdPDEGlobal, const int& mytime)
{
  static_assert( std::is_same< ArrayQ, typename IntegrandCell::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualCell_Galerkin_HB_impl<IntegrandCell, Vector>(fcn.cast(), rsdPDEGlobal, mytime);
}

}

#endif  // ResidualCell_Galerkin_HB_H
