// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_GALERKIN_H
#define JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_GALERKIN_H

// boundary-trace integral residual functions


//#include "JacobianFunctionalBoundaryTrace_FieldTrace_Galerkin.h"
//#include "JacobianFunctionalBoundaryTrace_Galerkin.h"
#include "JacobianFunctionalBoundaryTrace_WeightedResidual_Galerkin.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace (e.g. with Lagrange multipliers)
//
//---------------------------------------------------------------------------//
template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, template<class> class Vector, class MatrixJ>
class JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl
{
public:
  JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQ>& rsdPDEGlobal,
      Vector<ArrayQ>& rsdBCGlobal )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), lgfld_(lgfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Functionals with field trace not implemented yet.");
#if 0
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianFunctionalBoundaryTrace_FieldTrace_Galerkin(fcn, rsdPDEGlobal_, rsdBCGlobal_),
        xfld_, (qfld_, rfld_), lgfld_, quadratureorder_, ngroup_ );
#endif
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdBCGlobal_;
};

// Factory function

template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl<Surreal, FunctionalIntegrand, XFieldType, PhysDim, TopoDim,
                                                                  ArrayQ, Vector, MatrixJ>
JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin(const FunctionalIntegrand& fcnOutput,
                                                             const FieldType<XFieldType>& xfld,
                                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                             const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                             const int* quadratureorder, int ngroup,
                                                             Vector<MatrixJ>& rsdPDEGlobal,
                                                             Vector<MatrixJ>& rsdBCGlobal)
{
  return {fcnOutput, xfld.cast(), qfld, lgfld, quadratureorder, ngroup, rsdPDEGlobal, rsdBCGlobal};
}

//---------------------------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, template<class> class Vector, class MatrixJ>
class JacobianFunctionalBoundaryTrace_Dispatch_Galerkin_impl
{
public:
  JacobianFunctionalBoundaryTrace_Dispatch_Galerkin_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      Vector< MatrixJ >& func_q )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      func_q_(func_q)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
      JacobianFunctionalBoundaryTrace_WeightedResidual_Galerkin<Surreal>( fcnOutput_, fcn.cast(), func_q_ ),
                            xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector< MatrixJ >& func_q_;
};

// Factory function

template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_Galerkin_impl<Surreal, FunctionalIntegrand, XFieldType, PhysDim, TopoDim,
                                                       ArrayQ, Vector, MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_Galerkin(const FunctionalIntegrand& fcnOutput,
                                                  const FieldType<XFieldType>& xfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                  const int* quadratureorder, int ngroup,
                                                  Vector< MatrixJ >& func_q )
{
  return {fcnOutput, xfld.cast(), qfld, quadratureorder, ngroup, func_q};
}


}

#endif //JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_GALERKIN_H
