// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define EXTRACTCGLOCALBOUNDARIES_INSTANTIATE
// #include "../ExtractCGLocalBoundaries.h"
#include "ExtractCGLocalBoundaries_impl.h"

#include "Field/XFieldSpacetime.h"
#include "Field/FieldSpacetime.h"

namespace SANS
{

  //===========================================================================//
  //Explicit instantiations
  template void extractFreeDOFs(const Field<PhysD4,TopoD4,Real>&,
                                const Field<PhysD4,TopoD4,Real>&,std::array<std::vector<int>,2>&, Patch );

  // without Lagrange Field
  template void extractFreeDOFs(const Field<PhysD4,TopoD4,Real>&,std::vector<int>&, Patch );

  template std::map<int,int> constructDOFMap( const Field<PhysD4,TopoD4,Real>&,
                                              const Field<PhysD4,TopoD4,Real>&,
                                              const int, const int);

  template std::map<int,int> constructDGtoCGDOFMap(const Field<PhysD4, TopoD4, Real>& dfld,
                                                   const Field<PhysD4, TopoD4, Real>& qfld,
                                                   const std::map<int,int>& brokenDOFMap,
                                                   const int cellGroup_From );
}
