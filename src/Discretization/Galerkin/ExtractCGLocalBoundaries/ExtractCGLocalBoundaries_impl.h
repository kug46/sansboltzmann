// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#if !defined(EXTRACTCGLOCALBOUNDARIES_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "../ExtractCGLocalBoundaries.h"

#include <map>

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/tools/for_each_InteriorTraceGroup_Cell.h"
#include "Field/tools/for_each_GhostBoundaryTraceGroup_Cell.h"

#include "Field/FieldTypes.h"

#include "BasisFunction/LagrangeDOFMap.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "tools/linspace.h"

namespace SANS
{
  // Function for extracting fixed dofs off of an existing Field. Used in CG local patch
  template< class PhysDim, class TopoDim, class ArrayQ>
  void extractFreeDOFs( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                        const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                        std::array<std::vector<int>,2>& freeDOFs,
                        const Patch patch  )
  {
    const int iPDE = 0, iBC = 1;
    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // map of whether xNodes are free
    std::map<int,bool> xNodeFree;

    // map of whether DOF is free to be solved for, start assuming all free
    std::array<std::map<int,bool>,2> isDOFfree;

    // set all BC dofs (Lagrange multipliers) to frozen
    for (int i = 0; i < lgfld.nDOF(); i++)
      isDOFfree[iBC][i] = false;

    // If WHOLEPATCH or INNERPATCH - the frozen dofs are specified after freeing all
    if (patch == Patch::Whole || patch == Patch::Inner)
    {
        // Freeze only those dof which have incomplete support on the patch
        // std::cout<< "freeing all DOF" << std::endl;

        // set all to free
        for (int i = 0; i < xfld.nDOF(); i++)
          xNodeFree[i] = true;

        // set all to free
        for (int i = 0; i < qfld.nDOF(); i++)
          isDOFfree[iPDE][i] = true;

      if (patch == Patch::Whole)
      {
        // std::cout<< "freezing Ghost Boundaries" << std::endl;
        for_each_GhostBoundaryTraceGroup_Cell<TopoDim>::apply(
          FreezeGhostBoundaryTrace<PhysDim,ArrayQ>(xNodeFree, isDOFfree[iPDE], linspace(0,xfld.nGhostBoundaryTraceGroups()-1) ), qfld );
      }

      if (patch == Patch::Inner)
      {
        // std::cout<< "freezing cell groups greater than 0" << std::endl;
        // freeze all dofs that are in cell group 1!
        for_each_CellGroup<TopoDim>::apply(
          ApplyMarkCellGroup<PhysDim,ArrayQ>(isDOFfree[iPDE],linspace(1,xfld.nCellGroups()-1),false), qfld );
      }
    }
    else if ( patch == Patch::Broken )
    {
      // If SIP patch, the frozen dofs are specified by freeing cell group 0

      // std::cout << "freeing cell group 0!" << std::endl;

      // set all to frozen
      for (int i = 0; i < xfld.nDOF(); i++)
        xNodeFree[i] = false;

      // set all to frozen
      for (int i = 0; i < qfld.nDOF(); i++)
        isDOFfree[iPDE][i] = false;

      // free dofs associated group cell group 0
      for_each_CellGroup<TopoDim>::apply( MarkFreeCellGroup<PhysDim,ArrayQ>(xNodeFree, isDOFfree[iPDE], {0} ), (xfld,qfld)  );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unrecognized Patch");

    std::vector<int> LGboundaryGroups = lgfld.getGlobalBoundaryTraceGroups();

    for_each_BoundaryTraceGroup<TopoDim>::apply( FreeLGBTraceGroups<PhysDim,ArrayQ>( xNodeFree, isDOFfree[iBC], LGboundaryGroups), (xfld, lgfld) );

    // stack everything into std::vector for outputting
    for (const std::pair<int,bool>& it : isDOFfree[iBC])
      if ( it.second ) // has the
        freeDOFs[iBC].push_back(it.first);

    // stack everything into std::vector for outputting
    for (const std::pair<int,bool>& it : isDOFfree[iPDE])
      if ( it.second )
        freeDOFs[iPDE].push_back(it.first);


  }


  // Function for extracting fixed dofs off of an existing Field. Used in CG local patch
  template< class PhysDim, class TopoDim, class ArrayQ>
  void extractFreeDOFs( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                        std::vector<int>& freeDOFs,
                        const Patch patch )
  {
    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // map of whether xNodes are free
    std::map<int,bool> xNodeFree;

    // map of whether DOF is free to be solved for, start assuming all free
    std::map<int,bool> isDOFfree;

    // If WHOLEPATCH or INNERPATCH - the frozen dofs are specified after freeing all
    if (patch == Patch::Whole || patch == Patch::Inner)
    {
      // set all to free
      for (int i = 0; i < xfld.nDOF(); i++)
        xNodeFree[i] = true;

      // set all to free
      for (int i = 0; i < qfld.nDOF(); i++)
        isDOFfree[i] = true;

      if (patch == Patch::Whole)
      {
        for_each_GhostBoundaryTraceGroup_Cell<TopoDim>::apply(
          FreezeGhostBoundaryTrace<PhysDim,ArrayQ>(xNodeFree, isDOFfree, linspace(0,xfld.nGhostBoundaryTraceGroups()-1) ), qfld );
      }

      if (patch == Patch::Inner)
      {
        // freeze all dofs that are in cell group 1!
        for_each_CellGroup<TopoDim>::apply(
          ApplyMarkCellGroup<PhysDim,ArrayQ>(isDOFfree,linspace(1,xfld.nCellGroups()-1),false), qfld );
      }
    }
    else if ( patch == Patch::Broken )
    {
      // set all to frozen
      for (int i = 0; i < xfld.nDOF(); i++)
        xNodeFree[i] = false;

      // set all to frozen
      for (int i = 0; i < qfld.nDOF(); i++)
        isDOFfree[i] = false;

      // free dofs associated group cell group 0
      for_each_CellGroup<TopoDim>::apply( MarkFreeCellGroup<PhysDim,ArrayQ>(xNodeFree, isDOFfree, {0} ), (xfld,qfld)  );
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unrecognized Patch");

    // stack everything into std::vector for outputting
    for (const std::pair<int,bool>& it : isDOFfree)
      if ( it.second )
        freeDOFs.push_back(it.first);
  }

  // Function for creating maps between dofs of a given cell group to another in a broken group
  // For instance, mapping from cell group 1 to cell group 0, for those dofs in the interface
  template <class PhysDim, class TopoDim, class ArrayQ, class ArrayT>
  std::map<int,int> constructDOFMap( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                     const Field<PhysDim, TopoDim, ArrayT>& cfld,
                                     const int cellGroup_From, const int cellGroup_To )
  {

    // mark up the continuous field dofs corresponding to the from group
    std::vector<int> continuousDOFVector(cfld.nDOF(),-1); // initialize to -1 so we know if they get assigned
    for_each_CellGroup<TopoDim>::apply(MarkContinuousFieldDOFs<PhysDim,ArrayT,ArrayQ>(continuousDOFVector,{cellGroup_From}), (cfld,qfld));

    // label the broken dofs in the 'to' group
    std::map<int,int> brokenDOFMap;
    for_each_CellGroup<TopoDim>::apply(LabelBrokenFieldDOFs<PhysDim,ArrayT,ArrayQ>(continuousDOFVector,brokenDOFMap,{cellGroup_To}), (cfld,qfld));

    return brokenDOFMap;
  }

  // Function for creating maps between DG dofs from one group, to those of another. Only maps dofs that appear in the shared trace
  template< class PhysDim, class TopoDim, class ArrayQ>
  std::map<int,int> constructDGtoCGDOFMap( const Field<PhysDim, TopoDim, ArrayQ>& dfld, // discontinuous field going from
                                           const Field<PhysDim, TopoDim, ArrayQ>& qfld, // broken field that discontinuous field maps to
                                           const std::map<int,int>& brokenDOFMap, // map between broken dofs across trace
                                           const int cellGroup )
  {
    // goes from dfld dofs to cfld dofs
    std::map<int,int> DG_to_FromGroup_DOFMap;
    for_each_CellGroup<TopoDim>::apply(PairFieldDOFs<PhysDim,ArrayQ>(DG_to_FromGroup_DOFMap,{cellGroup}), (dfld,qfld) );

    // new map matching values from the first to keys of the second
    std::map<int,int> DG_to_CG_DOFMap;
    for (const auto& keyVal : DG_to_FromGroup_DOFMap )
    {
      auto it = brokenDOFMap.find( keyVal.second ); // look for the broken field dof in the interior trace
      if ( it != brokenDOFMap.end() ) // found it
        DG_to_CG_DOFMap.insert( std::pair<int,int>(keyVal.first, it->second) ); // connect the key from the first map to the value of second map

    }

    return DG_to_CG_DOFMap;
  }


  template< class PhysDim, class ArrayQ>
  template< class Topology >
  void
  MarkFreeCellGroup<PhysDim,ArrayQ>::
  apply(const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>,
                                  Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
        template FieldCellGroupType<Topology>& fldsCell,
        const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename XField< PhysDim, typename Topology::TopoDim        >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, typename Topology::TopoDim, ArrayQ >::template FieldCellGroupType<Topology> QFieldCellGroupType;

    const XFieldCellGroupType& xfldCell = get<0>( fldsCell );
    const QFieldCellGroupType& qfldCell = get<1>( fldsCell );

    std::vector<int> qfldCellGlobalMap( qfldCell.nBasis() ), xfldCellGlobalMap( xfldCell.nBasis() );

    for (int elem = 0; elem < xfldCell.nElem(); elem++ )
    {
      qfldCell.associativity(elem).getGlobalMapping( qfldCellGlobalMap.data(), qfldCellGlobalMap.size() );
      xfldCell.associativity(elem).getGlobalMapping( xfldCellGlobalMap.data(), xfldCellGlobalMap.size() );

      // mark the qfield dofs as free
      for (int i = 0; i < qfldCell.nBasis(); i++)
        qfld_free_dofmap_.at( qfldCellGlobalMap[i] ) = true;

      // mark the xfield dofs as the support
      for (int i = 0; i < xfldCell.nBasis(); i++)
        xfld_free_dofmap_.at( xfldCellGlobalMap[i] ) = true;
    }
  }

  //----------------------------------------------------------------------------//

  template< class PhysDim, class ArrayQ>
  template< class TopologyTrace >
  void
  MarkFreeBTraceGroup<PhysDim,ArrayQ>::
  apply(const typename FieldTuple<XField<PhysDim, typename TopologyTrace::CellTopoDim>,
                                  Field<PhysDim,typename TopologyTrace::CellTopoDim,ArrayQ>,TupleClass<>>::
        template FieldTraceGroupType<TopologyTrace>& fldsTrace,
       const int traceGroupGlobal)
  {
    // Trace Group Types
    typedef typename XField<PhysDim, typename TopologyTrace::CellTopoDim        >::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field <PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    const XFieldTraceGroupType& xfldTrace = get<0>(fldsTrace);
    const QFieldTraceGroupType& qfldTrace = get<1>(fldsTrace);

    std::vector<int> xfldTraceGlobalMap( xfldTrace.nBasis() ), qfldTraceGlobalMap( qfldTrace.nBasis() );

    // extract canonical nodes for the traces

    std::vector<int> traceDOFs, nodeDOFs;

    for (int elem = 0; elem < xfldTrace.nElem(); elem++ )
    {
      xfldTrace.associativity(elem).getGlobalMapping( xfldTraceGlobalMap.data(), xfldTraceGlobalMap.size() );
      qfldTrace.associativity(elem).getGlobalMapping( qfldTraceGlobalMap.data(), qfldTraceGlobalMap.size() );

      // mark the xfields dofs as in the support
      for (std::size_t i =0; i < xfldTraceGlobalMap.size(); i++)
        xfld_free_dofmap_.at( xfldTraceGlobalMap[ i ] ) = true;

      for (std::size_t i = 0; i < qfldTraceGlobalMap.size(); i++)
        qfld_free_dofmap_.at( qfldTraceGlobalMap[ i ] ) = true;
    }
  }

  // //----------------------------------------------------------------------------//
  // template< class PhysDim, class ArrayQ>
  // template< class Topology >
  // void
  // ApplyMarkCellGroup<PhysDim,ArrayQ>::
  // apply(const typename Field<PhysDim, typename Topology::TopoDim, ArrayQ>:: template FieldCellGroupType<Topology>& qfldCell,
  //       const int cellGroupGlobal)
  // {
  //   std::vector<int> qfldCellGlobalMap( qfldCell.nBasis() );
  //
  //   for (int elem = 0; elem < qfldCell.nElem(); elem++ )
  //   {
  //     qfldCell.associativity(elem).getGlobalMapping( qfldCellGlobalMap.data(), qfldCellGlobalMap.size() );
  //
  //     // mark the qfield dofs as free
  //     for (int i = 0; i < qfldCell.nBasis(); i++)
  //       qfld_free_dofmap_.at( qfldCellGlobalMap[i] ) = cellGroupMark_; // apply the mark from the constructor
  //   }
  // }


  //----------------------------------------------------------------------------//
  template< class PhysDim, class ArrayQ>
  template< class TopologyTrace, class TopologyL >
  void
  FreezeGhostBoundaryTrace<PhysDim,ArrayQ>::
  apply( const typename Field<PhysDim, typename TopologyL::TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCell,
         const int cellGroupGlobalL,
         const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int traceGroupGlobal)
  {
    const int qorder = qfldCell.order();

    std::vector<int> qfldCellGlobalMap( qfldCell.nBasis() ), xfldTraceGlobalMap( xfldTrace.nBasis() );

    for (int elem = 0; elem < xfldTrace.nElem(); elem++ )
    {
      int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      qfldCell.associativity(elemL).getGlobalMapping(  qfldCellGlobalMap.data(),  qfldCellGlobalMap.size() );
      xfldTrace.associativity(elem).getGlobalMapping( xfldTraceGlobalMap.data(), xfldTraceGlobalMap.size() );

      std::vector<int> traceMap = LagrangeDOFMap<TopologyL>::getCanonicalTraceMap( canonicalTraceL.trace, qorder );

      // Freeze the xNodes
      for (std::size_t i = 0; i < xfldTraceGlobalMap.size(); i++)
        xfld_free_dofmap_.at( xfldTraceGlobalMap[i] ) = false;

      // Freeze the qfld nodes
      for (std::size_t i = 0; i < traceMap.size(); i++)
        qfld_free_dofmap_.at( qfldCellGlobalMap[traceMap[i]] ) = false;
    }
  }

  //----------------------------------------------------------------------------//
  template< class PhysDim, class ArrayQ>
  template< class TopologyTrace >
  void
  FreeLGBTraceGroups<PhysDim,ArrayQ>::
  apply(const typename FieldTuple<XField<PhysDim, typename TopologyTrace::CellTopoDim>,
                                   Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>, TupleClass<>>::
        template FieldTraceGroupType<TopologyTrace>& fldsTrace,
        const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename XField< PhysDim, typename TopologyTrace::CellTopoDim        >::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field< PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ >::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    const XFieldTraceGroupType& xfldTrace = get<0>( fldsTrace );
    const QFieldTraceGroupType& lgfldTrace = get<1>( fldsTrace );

    std::vector<int> lgfldTraceGlobalMap( lgfldTrace.nBasis() ), xfldTraceGlobalMap( xfldTrace.nBasis() );

    // extract canonical nodes for the traces
    std::vector<int>  canonicalNodes = LagrangeDOFMap<TopologyTrace>::getCanonicalNodes();
    std::vector<bool> xCanonicalNodes( canonicalNodes.size() );

    std::vector<int> traceDOFs, nodeDOFs;

    for (int elem = 0; elem < xfldTrace.nElem(); elem++ )
    {
      lgfldTrace.associativity(elem).getGlobalMapping( lgfldTraceGlobalMap.data(), lgfldTraceGlobalMap.size() );
      xfldTrace.associativity(elem).getGlobalMapping( xfldTraceGlobalMap.data(), xfldTraceGlobalMap.size() );

      // extract the relevant xfield support nodes
      for (std::size_t i =0; i < xCanonicalNodes.size(); i++)
        xCanonicalNodes[i] = xfld_free_dofmap_.at( xfldTraceGlobalMap[ canonicalNodes[i] ] );

      // is the trace in the partition of unity?
      const bool anyNodesFree = std::any_of(xCanonicalNodes.begin(), xCanonicalNodes.end(), [](bool v) { return v; });

      if ( anyNodesFree )
      {
        // Set them to free
        for (std::size_t i = 0; i < lgfldTraceGlobalMap.size(); i++)
          lgfld_free_dofmap_.at( lgfldTraceGlobalMap[i] ) = true;

        // set the canonical Nodes to match the state of the xFieldNodes
        for (std::size_t i = 0; i < xCanonicalNodes.size(); i++)
          lgfld_free_dofmap_.at( lgfldTraceGlobalMap[canonicalNodes[i]] ) = xCanonicalNodes[i];
      }

    }
  }


  // Save off the continuous field DOFs that correspond to the broken field dofs, in a vector
  template< class PhysDim, class ArrayT, class ArrayQ>
  template< class Topology >
  void
  MarkContinuousFieldDOFs<PhysDim,ArrayT,ArrayQ>::
  apply(const typename FieldTuple<Field<PhysDim, typename Topology::TopoDim, ArrayT>,
                                  Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
        template FieldCellGroupType<Topology>& fldsCell,
        const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field< PhysDim, typename Topology::TopoDim, ArrayT>::template FieldCellGroupType<Topology> CFieldCellGroupType;
    typedef typename Field< PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    const CFieldCellGroupType& cfldCell = get<0>( fldsCell );
    const QFieldCellGroupType& qfldCell = get<1>( fldsCell );

    std::vector<int> qfldCellGlobalMap( qfldCell.nBasis() ), cfldCellGlobalMap( cfldCell.nBasis() );
    SANS_ASSERT_MSG( qfldCell.nBasis() == cfldCell.nBasis(), "must use with same basis");

    for (int elem = 0; elem < cfldCell.nElem(); elem++ )
    {
      qfldCell.associativity(elem).getGlobalMapping( qfldCellGlobalMap.data(), qfldCellGlobalMap.size() );
      cfldCell.associativity(elem).getGlobalMapping( cfldCellGlobalMap.data(), cfldCellGlobalMap.size() );

      for (std::size_t i = 0; i < cfldCellGlobalMap.size(); i++)
        continuousDOFVector_[ cfldCellGlobalMap[i] ] = qfldCellGlobalMap[i];
    }
  }


  // Save off the continuous field DOFs that correspond to the broken field dofs, in a vector
  template< class PhysDim, class ArrayT, class ArrayQ>
  template< class Topology >
  void
  LabelBrokenFieldDOFs<PhysDim,ArrayT,ArrayQ>::
  apply(const typename FieldTuple<Field<PhysDim, typename Topology::TopoDim, ArrayT>,
                                  Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
        template FieldCellGroupType<Topology>& fldsCell,
        const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field< PhysDim, typename Topology::TopoDim, ArrayT>::template FieldCellGroupType<Topology> CFieldCellGroupType;
    typedef typename Field< PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    const CFieldCellGroupType& cfldCell = get<0>( fldsCell );
    const QFieldCellGroupType& qfldCell = get<1>( fldsCell );

    std::vector<int> qfldCellGlobalMap( qfldCell.nBasis() ), cfldCellGlobalMap( cfldCell.nBasis() );
    SANS_ASSERT_MSG( qfldCell.nBasis()        == cfldCell.nBasis(),        "must use basis with same number of DOFs");
    SANS_ASSERT_MSG( qfldCell.basisCategory() == cfldCell.basisCategory(), "must use the same basis function category" );
    // TODO: add an assert that the basis categories are the same

    for (int elem = 0; elem < cfldCell.nElem(); elem++ )
    {
      qfldCell.associativity(elem).getGlobalMapping( qfldCellGlobalMap.data(), qfldCellGlobalMap.size() );
      cfldCell.associativity(elem).getGlobalMapping( cfldCellGlobalMap.data(), cfldCellGlobalMap.size() );

      for (std::size_t i = 0; i < cfldCellGlobalMap.size(); i++)
        if (continuousDOFVector_[cfldCellGlobalMap[i]] >= 0) // only insert those dofs that were marked in the from group
          brokenDOFMap_[continuousDOFVector_[cfldCellGlobalMap[i]]] = qfldCellGlobalMap[i];
    }
  }

} //namespace SANS
