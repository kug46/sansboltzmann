// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define EXTRACTCGLOCALBOUNDARIES_INSTANTIATE
// #include "../ExtractCGLocalBoundaries.h"
#include "ExtractCGLocalBoundaries_impl.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume.h"

namespace SANS
{

  //===========================================================================//
  //Explicit instantiations

  template void extractFreeDOFs(const Field<PhysD3,TopoD3,Real>&,
                                const Field<PhysD3,TopoD3,Real>&,std::array<std::vector<int>,2>&, Patch );

  template void extractFreeDOFs(const Field<PhysD3,TopoD3,DLA::VectorS<5,Real>>&,
                                const Field<PhysD3,TopoD3,DLA::VectorS<5,Real>>&,std::array<std::vector<int>,2>&, Patch );

  template void extractFreeDOFs(const Field<PhysD3,TopoD3,DLA::VectorS<6,Real>>&,
                                const Field<PhysD3,TopoD3,DLA::VectorS<6,Real>>&,std::array<std::vector<int>,2>&, Patch );

  template void extractFreeDOFs(const Field<PhysD3,TopoD3,DLA::VectorS<7,Real>>&,
                                const Field<PhysD3,TopoD3,DLA::VectorS<7,Real>>&,std::array<std::vector<int>,2>&, Patch );

  // without Lagrange Field
  template void extractFreeDOFs(const Field<PhysD3,TopoD3,Real>&,std::vector<int>&, Patch );


  template std::map<int,int> constructDOFMap( const Field<PhysD3,TopoD3,Real>&,
                                              const Field<PhysD3,TopoD3,Real>&,
                                              const int, const int);

  template std::map<int,int> constructDOFMap( const Field<PhysD3,TopoD3,DLA::VectorS<2,Real>>&,
                                              const Field<PhysD3,TopoD3,Real>&,
                                              const int, const int);

  template std::map<int,int> constructDOFMap( const Field<PhysD3,TopoD3,DLA::VectorS<5,Real>>&,
                                              const Field<PhysD3,TopoD3,Real>&,
                                              const int, const int);

  template std::map<int,int> constructDOFMap( const Field<PhysD3,TopoD3,DLA::VectorS<6,Real>>&,
                                              const Field<PhysD3,TopoD3,Real>&,
                                              const int, const int);

  template std::map<int,int> constructDOFMap( const Field<PhysD3,TopoD3,DLA::VectorS<7,Real>>&,
                                              const Field<PhysD3,TopoD3,Real>&,
                                              const int, const int);

  template std::map<int,int> constructDGtoCGDOFMap(const Field<PhysD3, TopoD3, Real>& dfld,
                                                   const Field<PhysD3, TopoD3, Real>& qfld,
                                                   const std::map<int,int>& brokenDOFMap,
                                                   const int cellGroup_From );

  template std::map<int,int> constructDGtoCGDOFMap(const Field<PhysD3, TopoD3, DLA::VectorS<2,Real>>& dfld,
                                                   const Field<PhysD3, TopoD3, DLA::VectorS<2,Real>>& qfld,
                                                   const std::map<int,int>& brokenDOFMap,
                                                   const int cellGroup_From );

  template std::map<int,int> constructDGtoCGDOFMap(const Field<PhysD3, TopoD3, DLA::VectorS<5,Real>>& dfld,
                                                   const Field<PhysD3, TopoD3, DLA::VectorS<5,Real>>& qfld,
                                                   const std::map<int,int>& brokenDOFMap,
                                                   const int cellGroup_From );

  template std::map<int,int> constructDGtoCGDOFMap(const Field<PhysD3, TopoD3, DLA::VectorS<6,Real>>& dfld,
                                                   const Field<PhysD3, TopoD3, DLA::VectorS<6,Real>>& qfld,
                                                   const std::map<int,int>& brokenDOFMap,
                                                   const int cellGroup_From );

  template std::map<int,int> constructDGtoCGDOFMap(const Field<PhysD3, TopoD3, DLA::VectorS<7,Real>>& dfld,
                                                   const Field<PhysD3, TopoD3, DLA::VectorS<7,Real>>& qfld,
                                                   const std::map<int,int>& brokenDOFMap,
                                                   const int cellGroup_From );
}
