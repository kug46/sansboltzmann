// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define EXTRACTCGLOCALBOUNDARIES_INSTANTIATE
// #include "../ExtractCGLocalBoundaries.h"
#include "ExtractCGLocalBoundaries_impl.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine.h"


namespace SANS
{

  //===========================================================================//
  //Explicit instantiations

  template void extractFreeDOFs(const Field<PhysD1,TopoD1,Real>&,
                                const Field<PhysD1,TopoD1,Real>&,std::array<std::vector<int>,2>&, Patch );
  template void extractFreeDOFs(const Field<PhysD1,TopoD1,Real>&,std::vector<int>&, Patch );

  template void extractFreeDOFs(const Field<PhysD1,TopoD1,DLA::VectorS<3, Real>> &,
                                const Field<PhysD1,TopoD1,DLA::VectorS<3, Real>> &, std::array<std::vector<int>, 2> &, Patch);

  template std::map<int,int> constructDOFMap( const Field<PhysD1,TopoD1,Real>&,
                                              const Field<PhysD1,TopoD1,Real>&,
                                              const int, const int);
  template std::map<int,int> constructDOFMap( const Field<PhysD1,TopoD1,DLA::VectorS<2,Real>>&,
                                              const Field<PhysD1,TopoD1,Real>&,
                                              const int, const int);
  template std::map<int,int> constructDOFMap( const Field<PhysD1,TopoD1,DLA::VectorS<3,Real>>&,
                                              const Field<PhysD1,TopoD1,Real>&,
                                              const int, const int);

  template std::map<int,int> constructDGtoCGDOFMap(const Field<PhysD1, TopoD1, Real>& dfld,
                                                   const Field<PhysD1, TopoD1, Real>& qfld,
                                                   const std::map<int,int>& brokenDOFMap,
                                                   const int cellGroup_From );

  template std::map<int,int> constructDGtoCGDOFMap(const Field<PhysD1, TopoD1, DLA::VectorS<2,Real>>& dfld,
                                                   const Field<PhysD1, TopoD1, DLA::VectorS<2,Real>>& qfld,
                                                   const std::map<int,int>& brokenDOFMap,
                                                   const int cellGroup_From );

  template std::map<int,int> constructDGtoCGDOFMap(const Field<PhysD1, TopoD1, DLA::VectorS<3,Real>>& dfld,
                                                   const Field<PhysD1, TopoD1, DLA::VectorS<3,Real>>& qfld,
                                                   const std::map<int,int>& brokenDOFMap,
                                                   const int cellGroup_From );

// template class MapInteriorTraceDOFs<PhysD1,Real>;
// template void MapInteriorTraceDOFs<PhysD1,Real>::apply<Node,Line,Line>(
//       const typename Field<PhysD1, typename Line::TopoDim, Real>::template FieldCellGroupType<Line>&,
//       const int,
//       const typename Field<PhysD1, typename Line::TopoDim, Real>::template FieldCellGroupType<Line>&,
//       const int,
//       const typename XField<PhysD1, typename Node::CellTopoDim>::template FieldTraceGroupType<Node>&,
//       const int );

}
