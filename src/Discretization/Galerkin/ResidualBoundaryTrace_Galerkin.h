// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_SANSLG_GALERKIN_H
#define RESIDUALBOUNDARYTRACE_SANSLG_GALERKIN_H

// boundary-trace integral residual functions

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class IntegrandBoundaryTrace, template<class> class Vector, class TR>
class ResidualBoundaryTrace_Galerkin_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_Galerkin_impl<IntegrandBoundaryTrace, Vector, TR> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<TR> ArrayQR;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_Galerkin_impl( const IntegrandBoundaryTrace& fcn,
                                       Vector<ArrayQR>& rsdPDEGlobal) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;


    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    int nIntegrandL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL, -1 );
    std::vector<int> mapDOFLocalL( nIntegrandL, -1 );
    std::vector<int> maprsdL( nIntegrandL, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = rsdPDEGlobal_.m();

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQR> integral(quadratureorder, nIntegrandL);

    // element integrand/residuals
    std::vector<ArrayQR> rsdPDEElemL( nIntegrandL );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nIntegrandL; n++)
        if (mapDOFGlobalL[n] < nDOFpossessed)
        {
          maprsdL[nDOFLocal] = n;
          mapDOFLocalL[nDOFLocal] = mapDOFGlobalL[n];
          nDOFLocal++;
        }

      // no residuals are possessed by this processor
      if (nDOFLocal == 0) continue;

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );

      for (int n = 0; n < nIntegrandL; n++)
        rsdPDEElemL[n] = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL),
                xfldElemTrace,
                rsdPDEElemL.data(), nIntegrandL );

      for (int n = 0; n < nDOFLocal; n++)
        rsdPDEGlobal_[ mapDOFLocalL[n] ] += rsdPDEElemL[ maprsdL[n] ];
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  Vector<ArrayQR>& rsdPDEGlobal_;
};

// Factory function

template<class IntegrandBoundaryTrace, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_Galerkin_impl<IntegrandBoundaryTrace, Vector, typename Scalar<ArrayQ>::type>
ResidualBoundaryTrace_Galerkin( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                Vector<ArrayQ>& rsdPDEGlobal)
{
  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same<ArrayQ, typename IntegrandBoundaryTrace::template ArrayQ<T> >::value, "These should be the same.");
  return ResidualBoundaryTrace_Galerkin_impl<IntegrandBoundaryTrace, Vector, T>(fcn.cast(), rsdPDEGlobal);
}


}

#endif  // RESIDUALBOUNDARYTRACE_GALERKIN_H
