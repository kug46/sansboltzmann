// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_GALERKIN_STABILIZED_LIFTEDBOUNDARY_H
#define ALGEBRAICEQUATIONSET_GALERKIN_STABILIZED_LIFTEDBOUNDARY_H

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/KahanSum.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/output_Tecplot.h"
#include "Field/FieldTypes.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"
#include "Discretization/ResidualNormType.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_Stabilized.h"
#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"
#include "Discretization/DG/Integrand_DGBR2_fwd.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin_Param.h"
#include "Discretization/DG/JacobianBoundaryTrace_Dispatch_DGBR2_Param.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin_Param.h"
#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch.h"


//Boundary trace integrands - DG
#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2_manifold.h"
#include "Discretization/DG/IntegrateBoundaryTrace_Dispatch_DGBR2.h"

#include "Discretization/DG/ResidualBoundaryTrace_Dispatch_DGBR2.h"
#include "Discretization/DG/JacobianBoundaryTrace_Dispatch_DGBR2.h"

//Compute lifting operator field
#include "Discretization/DG/SetFieldBoundaryTrace_DGBR2_LiftingOperator.h"
#include "Discretization/DG/SetFieldBoundaryTrace_Dispatch_LiftingOperator.h"
#include "Discretization/DG/SetFieldBoundaryTrace_DGBR2_AdjointLO.h"
#include "Discretization/DG/SetFieldBoundaryTrace_Dispatch_AdjointLO.h"
#include "Discretization/DG/JacobianCell_DGBR2_LiftingOperator.h"
#include "IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "Discretization/DG/IntegrandCell_DGBR2.h"

#include "ErrorEstimate/ErrorEstimate_fwd.h"
#include "FieldBundle_Galerkin_LiftedBoundary.h"

namespace SANS
{

// Forward declare
template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;

#ifndef ALGEBRAICEQUATIONSET_DGBR2_H
// refers to src/Discretization/DiscretizationBCTag.h
template<> struct DiscBCTag<BCCategory::Dirichlet_mitLG, DGBR2>     { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::Dirichlet_sansLG, DGBR2>    { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_mitLG, DGBR2>  { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG, DGBR2> { typedef DGBR2 type; };
template<> struct DiscBCTag<BCCategory::Flux_mitState, DGBR2>       { typedef DGBR2 type; };
template<> struct DiscBCTag<BCCategory::None, DGBR2>                { typedef Galerkin type; };

#if 1 //TODO: not implemented yet but are here for compilation to succeed
template<> struct DiscBCTag<BCCategory::Dirichlet_sansLG, DGBR2_manifold>    { typedef Galerkin_manifold type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_mitLG, DGBR2_manifold>  { typedef Galerkin_manifold type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG, DGBR2_manifold> { typedef Galerkin_manifold type; };
#endif
template<> struct DiscBCTag<BCCategory::Flux_mitState, DGBR2_manifold>       { typedef DGBR2_manifold type; };
template<> struct DiscBCTag<BCCategory::None, DGBR2_manifold>                { typedef Galerkin_manifold type; };
#endif
//----------------------------------------------------------------------------//
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary : public AlgebraicEquationSet_Debug<NDPDEClass_, Traits>
{
public:
  typedef NDPDEClass_ NDPDEClass;
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  template<class ArrayQT>
  using SystemVectorTemplate = typename TraitsType::template SystemVectorTemplate<ArrayQT>;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin_Stabilized<NDPDEClass> IntegrandCellClass;
  typedef IntegrateBoundaryTrace_Dispatch_DGBR2<NDPDEClass, BCNDConvert, BCVector, DGBR2> IntegrateBoundaryTrace_DispatchClass;
//  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_DispatchClass;
  //TODO: disc tag to be updated


  // Base must be used in constructor so that Local can use the BaseType Constructor
  // The global bundle must also be visible, so that SolverInterface can find it
  typedef FieldBundleBase_Galerkin_LiftedBoundary<PhysDim,TopoDim,ArrayQ> FieldBundleBase;
  typedef FieldBundle_Galerkin_LiftedBoundary<PhysDim,TopoDim,ArrayQ> FieldBundle;

  typedef ErrorEstimateNodal_Galerkin_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType> ErrorEstimateClass;
  typedef OutputCorrection_Galerkin_Stabilized<NDPDEClass, BCNDConvert, BCVector, XFieldType> OutputCorrectionClass;

  template< class... BCArgs >
  AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary(const XFieldType& xfld,
                                Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                const NDPDEClass& pde,
                                const StabilizationMatrix& stab,
                                const QuadratureOrder& quadratureOrder,
                                const ResidualNormType& resNormType,
                                const std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                PyDict& BCList,
                                const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                BCArgs&... args ):
    DebugBaseType(pde, tol),
    disc_(0, stab.getNitscheConstant(1) ), // hide eta in cBB
    fcnCell_(pde, CellGroups, stab),
    fcnCellDG_(pde, disc_, CellGroups),
    BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, std::forward<BCArgs>(args)...)),
    dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, disc_),
    xfld_(xfld),
    qfld_(qfld),
    rfld_(rfld),
    lgfld_(lgfld), mmfld_(qfld),
    pde_(pde),
    quadratureOrder_(quadratureOrder),
    quadratureOrderMin_(get<-1>(xfld), 0),
    resNormType_(resNormType), tol_(tol)
  {
    SANS_ASSERT( tol_.size() == 2 );
  }


  // Field Bundle Constructor for using in hiding the specific AlgEqSet in SolverInterface
  template< class... BCArgs>
  AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary( const XFieldType& xfld,
                                            FieldBundleBase& flds,
                                            std::shared_ptr<Field_CG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                            const NDPDEClass& pde,
                                            const StabilizationMatrix& stab,
                                            const QuadratureOrder& quadratureOrder,
                                            const ResidualNormType& resNormType,
                                            const std::vector<Real>& tol,
                                            const std::vector<int>& CellGroups,
                                            PyDict& BCList,
                                            const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                            BCArgs&&... args )
                                            : AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary
                                              (xfld, flds.qfld, flds.rfld, flds.lgfld,
                                               pde, stab, quadratureOrder, resNormType, tol,
                                               CellGroups, BCList, BCBoundaryGroups, args...) {}

  // Dummy to hide the lack of use of interiorTraceGroups
  template< class... BCArgs>
  AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary( const XFieldType& xfld,
                                            FieldBundleBase& flds,
                                            std::shared_ptr<Field_CG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                            const NDPDEClass& pde,
                                            const StabilizationMatrix& stab,
                                            const QuadratureOrder& quadratureOrder,
                                            const ResidualNormType& resNormType,
                                            const std::vector<Real>& tol,
                                            const std::vector<int>& CellGroups,
                                            const std::vector<int>& interiorTraceGroups,
                                            PyDict& BCList,
                                            const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                            BCArgs&&... args )
                                            : AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary
                                              (xfld, flds, pLiftedQuantityfld,
                                               pde, stab, quadratureOrder, resNormType, tol,
                                               CellGroups, BCList, BCBoundaryGroups, args...){}

  virtual ~AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override { this->template jacobian<        SystemMatrixView&>(mtx, quadratureOrder_ );    }
  virtual void jacobian(SystemNonZeroPatternView& nz) override { this->template jacobian<SystemNonZeroPatternView&>( nz, quadratureOrderMin_ ); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override { jacobian(Transpose(mtxT), quadratureOrder_ );    }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override { jacobian(Transpose( nzT), quadratureOrderMin_ ); }

  // Used to compute jacobians wrt. parameters
  template<int iParam, class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override
  {
    const int nDOFPDE = qfld_.nDOF();
    const int nDOFBC = lgfld_.nDOF();
    const int nMon = pde_.nMonitor();

    DLA::VectorD<Real> rsdPDEtmp(nMon);
    DLA::VectorD<Real> rsdBCtmp(nMon);

    rsdPDEtmp = 0;
    rsdBCtmp = 0;

    std::vector<std::vector<Real>> rsdNorm(2, std::vector<Real>(nMon, 0));
    std::vector<KahanSum<Real>> rsdNormKahan(nMon, 0);

    //compute residual norm
    if (resNormType_ == ResidualNorm_L2)
    {
      for (int n = 0; n < nDOFPDE; n++)
      {
        pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

        for (int j = 0; j < nMon; j++)
          rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
      }

      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] = sqrt((Real)rsdNormKahan[j]);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Unknown residual norm type!");

    //BC residual
    for (int n = 0; n < nDOFBC; n++)
    {
      pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

    return rsdNorm;

  }

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override
  {
    titles = {"PDE : ",
              "BC  : "};
    idx = {iPDE, iBC};
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Sets the primal adjoint field and computes the lifting operator adjoint
  void setAdjointField(const SystemVectorView& adj,
                       Field_CG_Cell<PhysDim, TopoDim, ArrayQ>&           wfld,
                       FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& sfld,
                       Field<PhysDim, TopoDim, ArrayQ>&                   mufld );

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  void fillSystemVector( SystemVectorView& q,
                         const Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                         const Field<PhysDim, TopoDim, ArrayQ>& lgfld) const;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;
  virtual VectorSizeClass vectorStateSize() const override;
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    return {qfld_.continuousGlobalMap(0, lgfld_.nDOFpossessed()),
            lgfld_.continuousGlobalMap(qfld_.nDOFpossessed(), 0)};
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return qfld_.comm(); }

  virtual void syncDOFs_MPI() override
  {
    qfld_.syncDOFs_MPI_Cached();
    lgfld_.syncDOFs_MPI_Cached();
  }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  virtual void dumpSolution(const std::string& filename) const override
  {
    output_Tecplot(qfld_, filename);
  }

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iBC = 1;
  static const int iq = 0;
  static const int ilg = 1;

  const std::map< std::string, std::shared_ptr<BCBase> >& BCs() const { return BCs_; }
  const IntegrateBoundaryTrace_DispatchClass& dispatchBC() const { return dispatchBC_; }

protected:
  template<class SparseMatrixType>
  void jacobian( SparseMatrixType mtx, const QuadratureOrder& quadratureOrder );

  void computeLiftingOperators();

  const DiscretizationDGBR2 disc_;
  IntegrandCellClass fcnCell_;
  IntegrandCell_DGBR2<NDPDEClass> fcnCellDG_;
  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_DispatchClass dispatchBC_;

  const XFieldType& xfld_;
  Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const FieldDataInvMassMatrix_Cell mmfld_; // Inverse mass matrix field
  const NDPDEClass& pde_;
  const QuadratureOrder quadratureOrder_;
  const QuadratureOrder quadratureOrderMin_;
  const ResidualNormType resNormType_;
  const std::vector<Real> tol_;
};

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd(iPDE)),
                                           xfld_, qfld_, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin( xfld_, qfld_, lgfld_,
                                                          quadratureOrder_.boundaryTraceOrders.data(),
                                                          quadratureOrder_.boundaryTraceOrders.size(),
                                                          rsd(iq), rsd(ilg) ),
      ResidualBoundaryTrace_Dispatch_DGBR2( xfld_, qfld_, rfld_,
                                            quadratureOrder_.boundaryTraceOrders.data(),
                                            quadratureOrder_.boundaryTraceOrders.size(),
                                            rsd(iq) ),
      ResidualBoundaryTrace_Dispatch_Galerkin( xfld_, qfld_,
                                               quadratureOrder_.boundaryTraceOrders.data(),
                                               quadratureOrder_.boundaryTraceOrders.size(),
                                               rsd(iq) ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder)
{
  SANS_ASSERT(jac.m() == 2);
  SANS_ASSERT(jac.n() == 2);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE,iq);
  Matrix jacPDE_lg = jac(iPDE,ilg);
  Matrix jacBC_q   = jac(iBC,iq);
  Matrix jacBC_lg  = jac(iBC,ilg);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld_); // Lifting operator jacobian from cell integral
  jacPDE_R = 0;

  IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin(fcnCell_, jacPDE_q),
                                           xfld_, qfld_, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_mitLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_, lgfld_,
                                                                   quadratureOrder.boundaryTraceOrders.data(),
                                                                   quadratureOrder.boundaryTraceOrders.size(),
                                                                   jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg ),
      JacobianBoundaryTrace_Dispatch_DGBR2<SurrealClass>( xfld_, qfld_, rfld_, jacPDE_R,
                                                          quadratureOrder.boundaryTraceOrders.data(),
                                                          quadratureOrder.boundaryTraceOrders.size(),
                                                          jacPDE_q ),
      JacobianBoundaryTrace_sansLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_,
                                                                    quadratureOrder.boundaryTraceOrders.data(),
                                                                    quadratureOrder.boundaryTraceOrders.size(),
                                                                    jacPDE_q ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<int iParam, class SparseMatrixType>
void
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const
{
  SANS_ASSERT(jac.m() >= 2);
  SANS_ASSERT(jac.n() > ip);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_p  = jac(iPDE,ip);
  Matrix jacBC_p = jac(iBC,ip);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_Galerkin_Param<SurrealClass, iParam>(fcnCell_, jacPDE_p),
      xfld_, qfld_,
      quadratureOrder_.cellOrders.data(),
      quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatch(
    JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_Param<SurrealClass, iParam>( xfld_, qfld_, lgfld_,
                                                                               quadratureOrder.boundaryTraceOrders.data(),
                                                                               quadratureOrder.boundaryTraceOrders.size(),
                                                                               jacPDE_p, jacBC_p ),
    JacobianBoundaryTrace_Dispatch_DGBR2_Param<SurrealClass, iParam>( xfld_, qfld_, rfld_,
                                                                      quadratureOrder.boundaryTraceOrders.data(),
                                                                      quadratureOrder.boundaryTraceOrders.size(),
                                                                      jacPDE_p ),
    JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_Param<SurrealClass, iParam>( xfld_, qfld_,
                                                                                quadratureOrder.boundaryTraceOrders.data(),
                                                                                quadratureOrder.boundaryTraceOrders.size(),
                                                                                jacPDE_p ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    lgfld_.DOF(k) = q[ilg][k];

  computeLiftingOperators();
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
computeLiftingOperators()
{
  // Compute the lifting operator field
  dispatchBC_.dispatch_DGBR2(
      SetFieldBoundaryTrace_Dispatch_LiftingOperator( xfld_, qfld_, rfld_, mmfld_,
                                                      quadratureOrder_.boundaryTraceOrders.data(),
                                                      quadratureOrder_.boundaryTraceOrders.size() ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
setAdjointField(const SystemVectorView& adj,
                Field_CG_Cell<PhysDim, TopoDim, ArrayQ>&           wfld,
                FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& sfld,
                Field<PhysDim, TopoDim, ArrayQ>&                   mufld )
{

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = wfld.nDOFpossessed() + wfld.nDOFghost();
  SANS_ASSERT( nDOFPDE == adj[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    wfld.DOF(k) = adj[iq][k];

  const int nDOFBC = mufld.nDOFpossessed() + mufld.nDOFghost();
  SANS_ASSERT( nDOFBC == adj[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    mufld.DOF(k) = adj[ilg][k];

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld_); // Lifting operator jacobian from cell integral

  // PDE jacobian wrt R (this should be saved off somehow to improve efficiency)
  if ( pde_.needsSolutionGradientforSource() )
  {
    if ( pde_.hasSourceLiftedQuantity() )
    {
      SANS_DEVELOPER_EXCEPTION("Lifted Source Quantities Not Implemented for Galerkin Stabilized w BR2");
//      SANS_ASSERT_MSG(pLiftedQuantityfld_ != nullptr,
//                      "AlgebraicEquationSet_DGBR2::setAdjointField - The lifted quantity field has not been initialized!");
//
//      IntegrateCellGroups<TopoDim>::integrate( JacobianCell_DGBR2_LiftingOperator(fcnCell_, jacPDE_R),
//                                               xfld_, (qfld_, rfld_, *pLiftedQuantityfld_),
//                                               quadratureOrder_.cellOrders.data(),
//                                               quadratureOrder_.cellOrders.size() );
    }
    else
    {
      IntegrateCellGroups<TopoDim>::integrate( JacobianCell_DGBR2_LiftingOperator(fcnCellDG_, jacPDE_R),
                                               xfld_, (qfld_, rfld_),
                                               quadratureOrder_.cellOrders.data(),
                                               quadratureOrder_.cellOrders.size() );
    }
  }
  else
    jacPDE_R = 0;

  // Compute the lifting operator field on the boundary
  dispatchBC_.dispatch_DGBR2(
      SetFieldBoundaryTrace_Dispatch_AdjointLO<SurrealClass>( xfld_, qfld_, rfld_, wfld, sfld, jacPDE_R,
                                                              quadratureOrder_.boundaryTraceOrders.data(),
                                                              quadratureOrder_.boundaryTraceOrders.size() ) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld_.DOF(k);

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld_.DOF(k);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
fillSystemVector(SystemVectorView& q,
                 const Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                 const Field<PhysDim, TopoDim, ArrayQ>& lgfld) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld.nDOFpossessed() + qfld.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld.DOF(k);

  const int nDOFBC = lgfld.nDOFpossessed() + lgfld.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld.DOF(k);
}



template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  return {nDOFPDEpos,
          nDOFBCpos};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorStateSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {nDOFPDE,
          nDOFBC};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");
  static_assert(iq == 0,"");
  static_assert(ilg == 1,"");

  // jacobian nonzero pattern
  //
  //           q  lg
  //   PDE     X   X
  //   BC      X   0

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {{ {nDOFPDEpos, nDOFPDE}, {nDOFPDEpos, nDOFBC} },
          { {nDOFBCpos , nDOFPDE}, {nDOFBCpos , nDOFBC} }};
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
bool
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  bool isValidState = true;
  // Update the solution field
  setSolutionField(q);

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                             xfld_, qfld_, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );
  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                         quadratureOrder_.boundaryTraceOrders.data(),
                                                         quadratureOrder_.boundaryTraceOrders.size(),
                                                         isValidState ),
      isValidStateBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_,
                                                          quadratureOrder_.boundaryTraceOrders.data(),
                                                          quadratureOrder_.boundaryTraceOrders.size(),
                                                          isValidState )
    );

  return isValidState;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
int
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
nResidNorm() const
{
  return 2;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);

//  std::string filename_iBC = filenamebase + "_iBC_iter" + std::to_string(nonlinear_iter) + ".plt";
//  this->dumpLinesearchField(lgfld_, *pStepData, iBC, filename_iBC);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_GALERKIN_STABILIZED_LIFTEDBOUNDARY_H
