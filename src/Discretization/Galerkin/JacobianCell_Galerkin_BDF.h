// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_GALERKIN_BDF_H
#define JACOBIANCELL_GALERKIN_BDF_H

// HDG cell integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/FieldSequence.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral jacobian
//

template<class IntegrandCell>
class JacobianCell_Galerkin_BDF_impl :
    public GroupIntegralCellType< JacobianCell_Galerkin_BDF_impl<IntegrandCell> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianCell_Galerkin_BDF_impl( const IntegrandCell& fcn,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q ) :
    fcn_(fcn), mtxGlobalPDE_q_(mtxGlobalPDE_q) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
                               FieldSequence<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld               = get<0>(flds);
    const FieldSequence< PhysDim, TopoDim, ArrayQ >& qfldPast = get<1>(flds);

    const int qm = qfld.nDOFpossessed();
    const int qn = qfld.nDOFpossessed() + qfld.nDOFghost();

    const int qpastm = qfldPast.nDOFpossessed();
    const int qpastn = qfldPast.nDOFpossessed() + qfldPast.nDOFghost();

    SANS_ASSERT_MSG(mtxGlobalPDE_q_.m() == qfld.nDOFpossessed()                   , "%d != %d", mtxGlobalPDE_q_.m(), qm);
    SANS_ASSERT_MSG(mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost(), "%d != %d", mtxGlobalPDE_q_.n(), qn);

    SANS_ASSERT_MSG(mtxGlobalPDE_q_.m() == qfldPast.nDOFpossessed()                       , "%d != %d", mtxGlobalPDE_q_.m(), qpastm);
    SANS_ASSERT_MSG(mtxGlobalPDE_q_.n() == qfldPast.nDOFpossessed() + qfldPast.nDOFghost(), "%d != %d", mtxGlobalPDE_q_.n(), qpastn);
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class ParamFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename ParamFieldType::template FieldCellGroupType<Topology>& pfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
                                       FieldSequence<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {

    typedef typename ParamFieldType                           ::template FieldCellGroupType<Topology> PFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ         >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldSequence< PhysDim, TopoDim, ArrayQ >::template FieldCellGroupType<Topology> QFieldSeqCellGroupType;
    typedef typename XField< PhysDim, TopoDim                >::template FieldCellGroupType<Topology> XFieldCellGroupType;

    typedef typename PFieldCellGroupType::template ElementType<> ElementPFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename QFieldSeqCellGroupType::template ElementType<> ElementQFieldSeqClass;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    const QFieldCellGroupType&    qfldCell     = get<0>(fldsCell);
    const QFieldSeqCellGroupType& qfldCellPast = get<1>(fldsCell);

    // element field variables
    ElementPFieldClass pfldElem( pfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldSeqClass qfldElemPast( qfldCellPast.basis() );

    // DOFs per element
    const int nDOF = qfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nDOF, -1 );
    std::vector<int> mapDOFLocal( nDOF, -1 );
    std::vector<int> maprsd( nDOF, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = mtxGlobalPDE_q_.m();

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, MatrixElemClass >
      integral(quadratureorder);

    // element jacobian matrices
    MatrixElemClass mtxElemPDE_q(nDOF, nDOF);
    MatrixElemClass mtxElemPDELocal_q(nDOF, nDOF);

    // loop over elements within group
    const int nelem = pfldCell.nElem();

    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), mapDOFGlobal.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOF; n++)
        if (mapDOFGlobal[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
          nDOFLocal++;
        }

      // no residuals are possessed by this processor
      if (nDOFLocal == 0) continue;

      // zero element Jacobians
      mtxElemPDE_q = 0;
      mtxElemPDELocal_q = 0;

      // copy global grid/solution DOFs to element
      pfldCell.getElement( pfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qfldCellPast.getElement( qfldElemPast, elem );

      // cell integration for canonical element
      integral( fcn_.integrand( pfldElem, qfldElem, qfldElemPast ),
                get<ElementXFieldClass>(pfldElem), mtxElemPDE_q );

      // scatter-add element jacobian to global
      if (nDOFLocal == nDOF)
        scatterAdd( elem, mapDOFGlobal, nDOFLocal, mapDOFGlobal, mtxElemPDE_q, mtxGlobalPDE_q_ );
      else
      {
        // compress in only the DOFs possessed by this processor
        for (int i = 0; i < nDOFLocal; i++)
          for (int j = 0; j < nDOF; j++)
            mtxElemPDELocal_q(i,j) = mtxElemPDE_q(maprsd[i],j);

        scatterAdd( elem, mapDOFLocal, nDOFLocal, mapDOFGlobal, mtxElemPDELocal_q, mtxGlobalPDE_q_ );
      }

    } // elem
  }


//----------------------------------------------------------------------------//
  template <template <class> class SparseMatrixType>
  void
  scatterAdd(
      const int elem,
      std::vector<int>& mapDOFLocal,
      const int nDOFLocal,
      std::vector<int>& mapDOFGlobal,
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
  {
    // jacobian wrt q
    mtxGlobalPDE_q.scatterAdd( mtxElemPDE_q, mapDOFLocal.data(), nDOFLocal, mapDOFGlobal.data(), mapDOFGlobal.size() );
  }

  #if 0
  //----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QField2DArea<Topology,PDE>& ufld,
      const QField2DArea<Topology,PDE>& qxfld,
      const QField2DArea<Topology,PDE>& qyfld,
      const int elem, const int nDOF,
      int mapDOFGlobal[],
      const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemPDE_u,
      const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemPDE_qx,
      const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemPDE_qy,
      const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAux_u,
      const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAux_qx,
      const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAux_qy,
      const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAuy_u,
      const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAuy_qx,
      const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAuy_qy,
      SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalPDE_u,
      SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalPDE_qx,
      SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalPDE_qy,
      SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAux_u,
      SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAux_qx,
      SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAux_qy,
      SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAuy_u,
      SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAuy_qx,
      SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAuy_qy )
  {
    mtxGlobalPDE_u.scatterAdd(  mtxElemPDE_u,  elem, elem );
    mtxGlobalAux_u.scatterAdd(  mtxElemAux_u,  elem, elem );
    mtxGlobalAuy_u.scatterAdd(  mtxElemAuy_u,  elem, elem );

    mtxGlobalPDE_qx.scatterAdd( mtxElemPDE_qx, elem, elem );
    mtxGlobalAux_qx.scatterAdd( mtxElemAux_qx, elem, elem );
    mtxGlobalAuy_qx.scatterAdd( mtxElemAuy_qx, elem, elem );

    mtxGlobalPDE_qy.scatterAdd( mtxElemPDE_qy, elem, elem );
    mtxGlobalAux_qy.scatterAdd( mtxElemAux_qy, elem, elem );
    mtxGlobalAuy_qy.scatterAdd( mtxElemAuy_qy, elem, elem );
  }
  #endif


protected:
  const IntegrandCell& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
};

// Factory function

template<class IntegrandCell, class MatrixQ>
JacobianCell_Galerkin_BDF_impl<IntegrandCell>
JacobianCell_Galerkin_BDF( const IntegrandCellType<IntegrandCell>& fcn,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q)
{
  return {fcn.cast(), mtxGlobalPDE_q};
}

}


#endif  // JACOBIANCELL_HDG_BDF_H
