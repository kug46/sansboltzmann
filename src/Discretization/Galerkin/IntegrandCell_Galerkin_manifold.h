// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_INTEGRANDCELL_GALERKIN_MANIFOLD_H_
#define SRC_DISCRETIZATION_GALERKIN_INTEGRANDCELL_GALERKIN_MANIFOLD_H_

// Galerkin cell integrand operator for manifolds

#include <ostream>
#include <vector>

#include "BasisFunction/BasisFunctionCategory.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template<class PDE_>
class IntegrandCell_Galerkin_manifold : public IntegrandCellType<IntegrandCell_Galerkin_manifold<PDE_> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE_::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  IntegrandCell_Galerkin_manifold(const PDE& pde,
                                  const std::vector<int>& cellGroups) :
    pde_(pde),
    cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  // Basis-weighted integrand class: i.e. weighting function = basis function of solution q
  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology>     ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef DLA::VectorS<TopoDim::D, VectorX> LocalAxes;         // manifold local axes type
    typedef DLA::VectorS<PhysDim::D, LocalAxes> VectorLocalAxes; // gradient of local axes type

    template<class Z>
    using IntegrandType = ArrayQ<Z>;

    BasisWeighted(const PDE& pde,
                  const ElementParam& paramfldElem,
                  const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) :
      pde_(pde),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem_)), // XField must be the last parameter
      qfldElem_(qfldElem),
      nDOF_(qfldElem_.nDOF()),
      phi_( new Real[nDOF_] ),
      gradphi_( new VectorX[nDOF_] ) {}

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element residual integrand
    template <class Ti>
    void operator()(const QuadPointType& sRef, IntegrandType<Ti> integrand[], int neqn) const;

    template<class Ti>
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real> >& mtxPDEElem ) const;

  private:
    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const LocalAxes& e0,
                            const VectorLocalAxes& e0_X,
                            const ParamT& param,
                            const ArrayQ<Tq>& q,
                            const VectorArrayQ<Tg>& gradq,
                            IntegrandType<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};


template <class PDE_>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_manifold<PDE_>::
BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return (pde_.hasFluxAdvective() ||
          pde_.hasFluxViscous() ||
          pde_.hasSource() ||
          pde_.hasForcingFunction());
}


// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE_>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_manifold<PDE_>::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const QuadPointType& sRef, IntegrandType<Ti> integrand[], int neqn) const
{
  static_assert(TopoDim::D == 1, "Only TopoD1 is implemented so far."); // TODO: generalize

  SANS_ASSERT(neqn == nDOF_);

  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                // solution
  VectorArrayQ<T> gradq;      // solution gradient

  VectorX e01;                // basis direction vector
  LocalAxes e0;               // basis direction vector
  VectorLocalAxes e0_X;       // manifold local axes tangential cartesian gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Basis vector and its gradients
  xfldElem_.unitTangent(sRef, e01);
  e0[0] = e01;

  xfldElem_.evalUnitTangentGradient(sRef, e0_X);

  // Elemental parameters (includes X)
  paramfldElem_.eval(sRef, param);

  // basis value, gradient
  qfldElem_.evalBasis(sRef, phi_, nDOF_);
  xfldElem_.evalBasisGradient(sRef, qfldElem_, gradphi_, nDOF_);

  // solution value, gradient
  qfldElem_.evalFromBasis(phi_, nDOF_, q);
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis(gradphi_, nDOF_, gradq);

  // compute the residual
  weightedIntegrand( e0, e0_X, param, q, gradq, integrand, neqn);
}

template <class PDE_>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_manifold<PDE_>::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const
{
  static_assert(TopoDim::D == 1, "Only TopoD1 is implemented so far."); // TODO: generalize

  SANS_ASSERT(rsdPDEElem.m() == nDOF_);

  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                // solution
  VectorArrayQ<T> gradq;      // solution gradient

  VectorX e01;                // basis direction vector
  LocalAxes e0;               // basis direction vector
  VectorLocalAxes e0_X;       // manifold local axes tangential cartesian gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Basis vector and its gradients
  xfldElem_.unitTangent(sRef, e01);
  e0[0] = e01;

  xfldElem_.evalUnitTangentGradient(sRef, e0_X);

  // Elemental parameters (includes X)
  paramfldElem_.eval(sRef, param);

  // basis value, gradient
  qfldElem_.evalBasis(sRef, phi_, nDOF_);
  xfldElem_.evalBasisGradient(sRef, qfldElem_, gradphi_, nDOF_);

  // solution value, gradient
  qfldElem_.evalFromBasis(phi_, nDOF_, q);
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis(gradphi_, nDOF_, gradq);

  // compute the residual
  DLA::VectorD<ArrayQ<Ti>> integrand(nDOF_);

  weightedIntegrand( e0, e0_X, param, q, gradq, integrand.data(), integrand.size());

  for (int k = 0; k < nDOF_; ++k)
    rsdPDEElem(k) += dJ*integrand(k);
}

template <class PDE_>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_manifold<PDE_>::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxPDEElem.m() == nDOF_);
  SANS_ASSERT(mtxPDEElem.n() == nDOF_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;                 // solution
  VectorArrayQ<Real> gradq;       // gradient

  ArrayQ<SurrealClass> qSurreal = 0;           // solution
  VectorArrayQ<SurrealClass> gradqSurreal = 0; // gradient

  MatrixQ<Real> PDE_q, PDE_gradq; // temporary storage

  VectorX e01;                // basis direction vector
  LocalAxes e0;               // basis direction vector
  VectorLocalAxes e0_X;       // manifold local axes tangential cartesian gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // basis vector and its gradients
  xfldElem_.unitTangent( sRef, e01 );
  e0[0] = e01;

  xfldElem_.evalUnitTangentGradient( sRef, e0_X );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qSurreal = q;

  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    gradqSurreal = gradq;
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1.0;
    slot += PDE::N;

    integrandSurreal = 0.0;

    weightedIntegrand(e0, e0_X, param, qSurreal, gradq, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0.0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;

  } // nchunk


  if (needsSolutionGradient)
  {
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d],n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandSurreal = 0;

      weightedIntegrand(e0, e0_X, param, q, gradqSurreal, integrandSurreal.data(), integrandSurreal.size());

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d],n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOF_; j++)
              mtxPDEElem(i,j) += dJ*gradphi_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  }
}

template <class PDE_>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tg, class Ti>
void
IntegrandCell_Galerkin_manifold<PDE_>::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
weightedIntegrand( const LocalAxes& e0,
                   const VectorLocalAxes& e0_X,
                   const ParamT& param,
                   const ArrayQ<Tq>& q,
                   const VectorArrayQ<Tg>& gradq,
                   IntegrandType<Ti> integrand[], int neqn ) const
{
  // PDE integrand
  for (int k = 0; k < neqn; ++k)
    integrand[k] = 0;

  // Galerkin flux term: -gradient(phi) . F
  VectorArrayQ<Ti> F = 0;          // PDE flux F(X, U), Fv(X, U, UX)

  // advective flux
  if (pde_.hasFluxAdvective())
    pde_.fluxAdvective(param, e0, q, F);

  // viscous flux
  if (pde_.hasFluxViscous())
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented");
    //pde_.fluxViscous(param, q, gradq, F);
  }

  for (int k = 0; k < neqn; k++)
    integrand[k] -= dot(gradphi_[k],F); // TODO: need to include grid jacobian if it is to be generalized for TopoDim > 1

  // Galerkin source term: +phi S
  if (pde_.hasSource())
  {
    ArrayQ<Ti> source = 0;                            // PDE source S(X, Q, QX)

    pde_.source(param, e0, e0_X, q, gradq, source);

    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS
  if (pde_.hasForcingFunction())
  {
    SANS_DEVELOPER_EXCEPTION("Not used/implemented yet");

    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction(param, forcing);

    for (int k = 0; k < neqn; k++)
      integrand[k] -= phi_[k]*forcing;
  }
}

}

#endif /* SRC_DISCRETIZATION_GALERKIN_INTEGRANDCELL_GALERKIN_MANIFOLD_H_ */
