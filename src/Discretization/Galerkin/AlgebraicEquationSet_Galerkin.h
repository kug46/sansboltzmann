// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_GALERKIN_H
#define ALGEBRAICEQUATIONSET_GALERKIN_H

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/KahanSum.h"
#include "tools/make_unique.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/output_Tecplot.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"
#include "Discretization/ResidualNormType.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "Discretization/Galerkin/JacobianCell_Galerkin_Param.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin_Param.h"
#include "Discretization/Galerkin/SolutionData_Galerkin_Stabilized.h"

#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch.h"

//#include "ErrorEstimate/Galerkin/ErrorEstimate_StrongForm_Galerkin.h"
#include "ErrorEstimate/ErrorEstimate_fwd.h"
#include "FieldBundle_Galerkin.h"
#include "IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>
#include "tools/plus_std_vector.h"
#endif

namespace SANS
{

// Forward declare
template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;


//----------------------------------------------------------------------------//
template< class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
          class Traits, class XFieldType>
class AlgebraicEquationSet_Galerkin : public AlgebraicEquationSet_Debug<NDPDEClass, Traits>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  template<class ArrayQT>
  using SystemVectorTemplate = typename TraitsType::template SystemVectorTemplate<ArrayQT>;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandCellClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_DispatchClass;
  //TODO: disc tag to be updated

  // Base must be used in constructor so that Local can use the BaseType Constructor
  // The global bundle must also be visible, so that SolverInterface can find it
  typedef FieldBundleBase_Galerkin<PhysDim,TopoDim,ArrayQ> FieldBundleBase;
  typedef FieldBundle_Galerkin<PhysDim,TopoDim,ArrayQ> FieldBundle;

  template<class ParamBuilderType>
  using SolutionDataClass = SolutionData_Galerkin_Stabilized<PhysDim, TopoDim, NDPDEClass, ParamBuilderType>;

  typedef ErrorEstimate_StrongForm_Galerkin<NDPDEClass, BCNDConvert, BCVector, XFieldType> ErrorEstimateClass;

  template< class... BCArgs >
  AlgebraicEquationSet_Galerkin(const XFieldType& xfld,
                                Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                const NDPDEClass& pde,
                                const StabilizationNitsche& stab,
                                const QuadratureOrder& quadratureOrder,
                                const ResidualNormType& resNormType,
                                const std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                PyDict& BCList,
                                const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                BCArgs&&... args ) :
    DebugBaseType(pde, tol),
    fcnCell_(pde, CellGroups),
    BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, std::forward<BCArgs>(args)...)),
    dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab),
    xfld_(xfld),
    qfld_(qfld),
    lgfld_(lgfld),
    pde_(pde),
    quadratureOrder_(quadratureOrder),
    quadratureOrderMin_(get<-1>(xfld), 0),
    resNormType_(resNormType), tol_(tol)
  {
    SANS_ASSERT( tol_.size() == 2 );
    for (auto it = CellGroups.begin(); it != CellGroups.end(); ++it) // loop over groups
      for (auto itt = std::next(it,1); itt != CellGroups.end(); ++itt) // loop over remaining groups
        SANS_ASSERT_MSG( *it != *itt, "A cell group can not be specified twice" );

    for (auto mit = BCBoundaryGroups.begin(); mit != BCBoundaryGroups.end(); ++mit)
      for (auto it = mit->second.begin(); it != mit->second.end(); ++it) // loop over groups
        for (auto itt = std::next(it,1); itt != mit->second.end(); ++itt) // loop over remaining groups
          SANS_ASSERT_MSG( *it != *itt, "A boundary group can not be specified twice in one boundary condition" );

  }

  // Solution Data Constructor for using in hiding the specific AlgEqSet in SolverInterface
  // with dummy discretization object
  template< class... BCArgs>
  AlgebraicEquationSet_Galerkin( const XFieldType& xfld,
                                 FieldBundleBase& flds,
                                 const NDPDEClass& pde,
                                 const StabilizationNitsche& stab,
                                 const QuadratureOrder& quadratureOrder,
                                 const ResidualNormType& resNormType,
                                 const std::vector<Real>& tol,
                                 const std::vector<int>& CellGroups,
                                 PyDict& BCList,
                                 const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                 BCArgs&&... args ) :
                                 AlgebraicEquationSet_Galerkin(xfld, flds.qfld, flds.lgfld, pde, stab, quadratureOrder, resNormType,
                                                               tol, CellGroups, BCList, BCBoundaryGroups, args...) {}

  virtual ~AlgebraicEquationSet_Galerkin() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override { this->template jacobian<        SystemMatrixView&>(mtx, quadratureOrder_ );    }
  virtual void jacobian(SystemNonZeroPatternView& nz) override { this->template jacobian<SystemNonZeroPatternView&>( nz, quadratureOrderMin_ ); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override { jacobian(Transpose(mtxT), quadratureOrder_ );    }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override { jacobian(Transpose( nzT), quadratureOrderMin_ ); }

  // Used to compute jacobians wrt. parameters
  template<int iParam, class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override
  {
    const int nDOFPDE = qfld_.nDOFpossessed();
    const int nDOFBC = lgfld_.nDOFpossessed();
    const int nMon = pde_.nMonitor();

    DLA::VectorD<Real> rsdPDEtmp(nMon);
    DLA::VectorD<Real> rsdBCtmp(nMon);

    rsdPDEtmp = 0;
    rsdBCtmp = 0;

    std::vector<std::vector<Real>> rsdNorm(2, std::vector<Real>(nMon, 0));
    std::vector<KahanSum<Real>> rsdNormKahan(nMon, 0);

    //compute residual norm
    //HACKED Simple L2 norm
    //TODO: Allow for non-L2 norms
    if (resNormType_ == ResidualNorm_L2)
    {
      for (int n = 0; n < nDOFPDE; n++)
      {
        pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

        for (int j = 0; j < nMon; j++)
          rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
      }

      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] = rsdNormKahan[j];

#ifdef SANS_MPI
      rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
#endif

      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);
    }
    else
      SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Galerkin::residualNorm - Unknown residual norm type!");

    //BC residual
    for (int n = 0; n < nDOFBC; n++)
    {
      pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

    return rsdNorm;

  }

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override
  {
    titles = {"PDE : ",
              "BC  : "};
    idx = {iPDE, iBC};
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    return {qfld_.continuousGlobalMap(0, lgfld_.nDOFpossessed()),
            lgfld_.continuousGlobalMap(qfld_.nDOFpossessed(), 0)};
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return qfld_.comm(); }

  virtual void syncDOFs_MPI() override
  {
    qfld_.syncDOFs_MPI_Cached();
    lgfld_.syncDOFs_MPI_Cached();
  }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iBC = 1;
  static const int iq = 0;
  static const int ilg = 1;

  const std::map< std::string, std::shared_ptr<BCBase> >& BCs() const { return BCs_; }
  const IntegrateBoundaryTrace_DispatchClass& dispatchBC() const { return dispatchBC_; }

  void fillResidualField( const Field_DG_Cell<PhysDim,TopoDim,ArrayQ>& qfld_dg,
                          std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld ) const;

protected:
  template<class SparseMatrixType>
  void jacobian( SparseMatrixType mtx, const QuadratureOrder& quadratureOrder );

  IntegrandCellClass fcnCell_;
  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_DispatchClass dispatchBC_;

  const XFieldType& xfld_;
  Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const NDPDEClass& pde_;
  const QuadratureOrder quadratureOrder_;
  const QuadratureOrder quadratureOrderMin_;
  const ResidualNormType resNormType_;
  const std::vector<Real> tol_;

};

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd(iPDE)),
                                           xfld_, qfld_, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin( xfld_, qfld_, lgfld_,
                                                          quadratureOrder_.boundaryTraceOrders.data(),
                                                          quadratureOrder_.boundaryTraceOrders.size(),
                                                          rsd(iPDE), rsd(iBC) ),
      ResidualBoundaryTrace_Dispatch_Galerkin( xfld_, qfld_,
                                               quadratureOrder_.boundaryTraceOrders.data(),
                                               quadratureOrder_.boundaryTraceOrders.size(),
                                               rsd(iPDE) ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder)
{
  SANS_ASSERT(jac.m() == 2);
  SANS_ASSERT(jac.n() == 2);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE,iq);
  Matrix jacPDE_lg = jac(iPDE,ilg);
  Matrix jacBC_q   = jac(iBC,iq);
  Matrix jacBC_lg  = jac(iBC,ilg);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate( JacobianCell_Galerkin(fcnCell_, jacPDE_q ),
                                           xfld_, qfld_, quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_mitLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_, lgfld_,
                                                                   quadratureOrder.boundaryTraceOrders.data(),
                                                                   quadratureOrder.boundaryTraceOrders.size(),
                                                                   jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg ),
      JacobianBoundaryTrace_sansLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_,
                                                                    quadratureOrder.boundaryTraceOrders.data(),
                                                                    quadratureOrder.boundaryTraceOrders.size(),
                                                                    jacPDE_q ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<int iParam, class SparseMatrixType>
void
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const
{
  SANS_ASSERT(jac.m() >= 2);
  SANS_ASSERT(jac.n() > ip);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_p  = jac(iPDE,ip);
  Matrix jacBC_p = jac(iBC,ip);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_Galerkin_Param<SurrealClass, iParam>(fcnCell_, jacPDE_p),
      xfld_, qfld_,
      quadratureOrder_.cellOrders.data(),
      quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatch(
    JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_Param<SurrealClass, iParam>( xfld_, qfld_, lgfld_,
                                                                               quadratureOrder_.boundaryTraceOrders.data(),
                                                                               quadratureOrder_.boundaryTraceOrders.size(),
                                                                               jacPDE_p, jacBC_p ),
    JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_Param<SurrealClass, iParam>( xfld_, qfld_,
                                                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                                                quadratureOrder_.boundaryTraceOrders.size(),
                                                                                jacPDE_p ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    lgfld_.DOF(k) = q[ilg][k];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld_.DOF(k);

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld_.DOF(k);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  return {nDOFPDEpos,
          nDOFBCpos};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorStateSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {nDOFPDE,
          nDOFBC};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");
  static_assert(iq == 0,"");
  static_assert(ilg == 1,"");

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {{ {nDOFPDEpos, nDOFPDE}, {nDOFPDEpos, nDOFBC} },
          { {nDOFBCpos , nDOFPDE}, {nDOFBCpos , nDOFBC} }};
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
bool
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  bool isValidState = true;

  // Update the solution field
  setSolutionField(q);

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                             xfld_, qfld_, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );
  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                quadratureOrder_.boundaryTraceOrders.size(),
                                                isValidState ),
      isValidStateBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size(),
                                                 isValidState )
    );

#ifdef SANS_MPI
  int validstate = isValidState ? 1 : 0;
  isValidState = (boost::mpi::all_reduce(*qfld_.comm(), validstate, std::plus<int>()) == qfld_.comm()->size());
#endif

  return isValidState;
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
fillResidualField(const Field_DG_Cell<PhysDim,TopoDim,ArrayQ>& qfld_dg,
                  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld) const
{
  DLA::VectorD<ArrayQ> rsd_dg(qfld_dg.nDOFpossessed());
  rsd_dg = 0;

  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd_dg),
                                           xfld_, qfld_dg, quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatchSANSFT(
      ResidualBoundaryTrace_Dispatch_Galerkin( xfld_, qfld_dg,
                                               quadratureOrder_.boundaryTraceOrders.data(),
                                               quadratureOrder_.boundaryTraceOrders.size(),
                                               rsd_dg ) );

  for (int i=0; i<qfld_dg.nDOFpossessed(); i++)
    up_resfld->DOF(i) = rsd_dg[i];

}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
int
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
nResidNorm() const
{
  return 2;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Galerkin<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);

//  std::string filename_iBC = filenamebase + "_iBC_iter" + std::to_string(nonlinear_iter) + ".plt";
//  this->dumpLinesearchField(lgfld_, *pStepData, iBC, filename_iBC);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_GALERKIN_H
