// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_STRONG_MITLG_GALERKIN_H
#define RESIDUALBOUNDARYTRACE_STRONG_MITLG_GALERKIN_H

// specialization boundary-trace integral residual functions for strong (essential) boundary conditions

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "BasisFunction/LagrangeDOFMap.h"
#include "Discretization/GroupIntegral_Type.h"

#include "IntegrandBoundaryTrace_None_Galerkin.h"

namespace SANS
{

template<class Topology>
struct LagrangeNodes;

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//
template<class IntegrandBoundaryTrace, template<class> class VectorRsdGlobal, class TR>
class ResidualBoundaryTrace_mitLG_Galerkin_impl;

template<class PDE_, class NDBCVector, template<class> class VectorRsdGlobal, class TR>
class ResidualBoundaryTrace_mitLG_Galerkin_impl<
        IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, Galerkin>, VectorRsdGlobal, TR> :
    public GroupIntegralBoundaryTraceType<
      ResidualBoundaryTrace_mitLG_Galerkin_impl<
        IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, Galerkin>, VectorRsdGlobal, TR> >
{
public:
  typedef IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_mitLG>, Galerkin> IntegrandBoundaryTraceClass;
  typedef typename IntegrandBoundaryTraceClass::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTraceClass::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTraceClass::template ArrayQ<TR> ArrayQR;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_mitLG_Galerkin_impl( const IntegrandBoundaryTraceClass& fcn,
                                             VectorRsdGlobal<ArrayQR>& rsdPDEGlobal,
                                             VectorRsdGlobal<ArrayQR>& rsdBCGlobal) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& lgfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( rsdBCGlobal_.m() == lgfld.nDOFpossessed() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename TopologyTrace::TopoDim TopoDimTrace;
    typedef          ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceClass;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

    if (fcn_.isStrongBC() && false)
    {
      SANS_ASSERT_MSG(lgfldTrace.basis()->category() == BasisFunctionCategory_Lagrange,
                      "Strong mitLG BC only works with Lagrange multipliers with Lagrange basis functions");

      // This is another assumption for strong mitLG
      SANS_ASSERT_MSG(qfldCellL.basis()->order() == lgfldTrace.basis()->order(),
                      "qfldCellL.basis()->order() = %d, lgfldTrace.basis()->order() = %d",
                      qfldCellL.basis()->order(), lgfldTrace.basis()->order());
    }

    // number of integrals evaluated per element
    int nIntegrandL  = qfldElemL.nDOF();
    int nIntegrandBC = lgfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL, -1 );
    std::vector<int> mapDOFGlobalBC( nIntegrandBC, -1 );

    // the number of times each DOF occurs on the boundary. Always 1 for DG, multiple times for CG
    std::map<int,int> DOFLcount;
    std::map<int,int> DOFBCcount;

    // loop over elements within group
    const int nelem = xfldTrace.nElem();

    if (fcn_.isStrongBC())
    {
      for (int elem = 0; elem < nelem; elem++)
      {
        const int elemL = xfldTrace.getElementLeft( elem );
        qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );
        lgfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalBC.data(), nIntegrandBC );

        for (int n = 0; n < nIntegrandL; n++)
        {
          std::map<int,int>::iterator it = DOFLcount.find(mapDOFGlobalL[n]);
          if (it == DOFLcount.end())
            DOFLcount[mapDOFGlobalL[n]] = 1;
          else
            it->second += 1;
        }

        for (int n = 0; n < nIntegrandBC; n++)
        {
          std::map<int,int>::iterator it = DOFBCcount.find(mapDOFGlobalBC[n]);
          if (it == DOFBCcount.end())
            DOFBCcount[mapDOFGlobalBC[n]] = 1;
          else
            it->second += 1;
        }
      }
    }

    std::vector<DLA::VectorS<MAX(1,TopoDimTrace::D),Real>> sRef;          // Lagrange point reference coordinates
    if (lgfldTrace.basis()->order() == 0)
    {
      sRef.resize(1);
      sRef[0] = TopologyTrace::centerRef;
    }
    else
      LagrangeNodes<TopologyTrace>::get(lgfldTrace.basis()->order(), sRef);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQR> integralFlux(quadratureorder, nIntegrandL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQR, ArrayQR> integralLG(quadratureorder, nIntegrandL, nIntegrandBC);

    // element integrand/residuals
    DLA::VectorD<ArrayQR> rsdPDEElemL( nIntegrandL ), tmpPDEElemL( nIntegrandL ), rsdPDEFluxElemL( nIntegrandL );
    DLA::VectorD<ArrayQR> rsdBCElem( nIntegrandBC ), tmpBCElem( nIntegrandBC );

    // loop over elements within group
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      lgfldTrace.getElement( lgfldElemTrace, elem );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );
      lgfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalBC.data(), nIntegrandBC );

      // set the residuals to zero
      rsdPDEFluxElemL = 0;

      integralFlux( fcn_.integrandFlux(xfldElemTrace, canonicalTraceL,
                                       xfldElemL, qfldElemL),
                    xfldElemTrace,
                    rsdPDEFluxElemL.data(), nIntegrandL );

      for (int n = 0; n < nIntegrandL; n++)
        rsdPDEGlobal_[mapDOFGlobalL[n]] += rsdPDEFluxElemL[n];


       // set the residuals to zero
      rsdPDEElemL = 0;
      rsdBCElem = 0;

      auto integrand = fcn_.integrand(xfldElemTrace, canonicalTraceL,
                                      xfldElemL, qfldElemL, lgfldElemTrace );

      if (fcn_.isStrongBC())
      {
        const std::size_t npoint = sRef.size();
        for (std::size_t ipoint = 0; ipoint < npoint; ipoint++)
        {
          integrand(sRef[ipoint], tmpPDEElemL.data(), nIntegrandL,
                                  tmpBCElem.data(), nIntegrandBC );

          rsdPDEElemL += tmpPDEElemL;
          rsdBCElem += tmpBCElem;
        }

        for (int n = 0; n < nIntegrandL; n++)
          rsdPDEGlobal_[mapDOFGlobalL[n]] += rsdPDEElemL[n];//DOFLcount[mapDOFGlobalL[n]];

        for (int n = 0; n < nIntegrandBC; n++)
          rsdBCGlobal_[mapDOFGlobalBC[n]] += rsdBCElem[n];//DOFBCcount[mapDOFGlobalBC[n]];
      }
      else
      {
        integralLG( integrand,
                    xfldElemTrace,
                    rsdPDEElemL.data(), nIntegrandL,
                    rsdBCElem.data(), nIntegrandBC );

        for (int n = 0; n < nIntegrandL; n++)
          rsdPDEGlobal_[mapDOFGlobalL[n]] += rsdPDEElemL[n];

        for (int n = 0; n < nIntegrandBC; n++)
          rsdBCGlobal_[mapDOFGlobalBC[n]] += rsdBCElem[n];
      }
    }
  }

protected:
  const IntegrandBoundaryTraceClass& fcn_;
  VectorRsdGlobal<ArrayQR>& rsdPDEGlobal_;
  VectorRsdGlobal<ArrayQR>& rsdBCGlobal_;
};

}

#endif  // RESIDUALBOUNDARYTRACE_STRONG_MITLG_GALERKIN_H
