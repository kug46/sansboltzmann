// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_HSMKL_H
#define INTEGRANDCELL_GALERKIN_HSMKL_H

// cell integrand operator: Galerkin for HSM/KL

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/Tuple.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "Discretization/Integrand_Type.h"

#include "pde/HSM/PDEHSMKL.h"

namespace SANS
{

// Forward declaration
template<class PDE>
class IntegrandCell_Galerkin;

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template<class VarType >
class IntegrandCell_Galerkin< PDEHSMKL<VarType> >
  : public IntegrandCellType< IntegrandCell_Galerkin< PDEHSMKL<VarType> > >
{
public:
  typedef PDEHSMKL<VarType> PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  template <class Z>
  using VectorX = typename PDE::template VectorX<Z>;

  template <class Z>
  using MatrixX = typename PDE::template MatrixX<Z>;

  template <class Z>
  using MatrixSymX = typename PDE::template MatrixSymX<Z>;     // force/moment/stress/curvature tensors

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin( const PDE& pde, const std::vector<int>& CellGroups )
    : pde_(pde), cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef typename ElementQFieldType::RefCoordType RefCoordType;

    typedef Element<MatrixSymX<T>, TopoDim, Topology> ElementForceFieldType;

    typedef typename ElementParam::ElementTypeL HSMElementParam;
    typedef typename HSMElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef typename TupleType<PDE::iAijInv,HSMElementParam>::type::T::Ttype Tc;
    typedef typename TupleType<PDE::imu,HSMElementParam>::type::T Tm;
    typedef typename TupleType<PDE::iqe,HSMElementParam>::type::T::Ttype Tq;

    typedef typename promote_Surreal<T,Tc>::type Tcs;
    typedef typename promote_Surreal<T,Tm,Tq>::type Ts;

    typedef Element<    VectorX<Ts>, TopoDim, Topology> ElementVectorFieldType;
    typedef Element<MatrixSymX<Tcs>, TopoDim, Topology> ElementStressFieldType;

    BasisWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem );

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neq ) const;

  protected:
    const PDE& pde_;
    const ElementQFieldType& qfldElem_;
    const ElementXFieldType& xfldElem_;
    const HSMElementParam& paramfldElem_;

    const int nDOF_;
    mutable std::vector<Real> phi_;
    mutable std::vector<DLA::VectorS<PhysDim::D,Real> > gradphi_;

    ElementForceFieldType forcefldElem_;
    //ElementTensorFieldType momentfldElem_;
    ElementStressFieldType epsforcefldElem_;
    //ElementTensorFieldType kapfldElem_;
    ElementVectorFieldType loadfldElem_;
    std::vector<MatrixX<T>> e_nodes_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};


template<class VarType>
template <class T, class TopoDim, class Topology, class ElementParam>
IntegrandCell_Galerkin< PDEHSMKL<VarType> >::BasisWeighted<T,TopoDim,Topology, ElementParam>::
BasisWeighted( const PDE& pde,
               const ElementParam& paramfldElem,
               const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem ) :
  pde_(pde),
  qfldElem_(qfldElem),
  xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
  paramfldElem_( paramfldElem.left() ),
  nDOF_(qfldElem_.nDOF() ),
  phi_(nDOF_,0),
  gradphi_(nDOF_,0),
  forcefldElem_(qfldElem.basis()),
  epsforcefldElem_(qfldElem.basis()),
  loadfldElem_(qfldElem.basis()),
  e_nodes_(nDOF_,0)
{

  for (int i = 0; i < qfldElem.nDOF(); i++)
  {
    const ArrayQ<T>& var = qfldElem.DOF(i);

    // Deformed local basis vectors
    // This assumes a nodal basis function
    pde_.calc_ehat( var, e_nodes_[i] );

    // Force resultant matrix
    pde_.calc_fbar(var, e_nodes_[i], forcefldElem_.DOF(i));

    // Calculate the force dependent strain tensor
    pde_.calc_epsbar_force(paramfldElem_.DOF(i), var, e_nodes_[i], epsforcefldElem_.DOF(i));

    // Calculate the load
    pde_.calc_load(paramfldElem_.DOF(i), e_nodes_[i], loadfldElem_.DOF(i));
  }

}

template<class VarType>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin< PDEHSMKL<VarType> >::BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return true;
}


// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template<class VarType>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin< PDEHSMKL<VarType> >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::operator()(
    const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn ) const
{
  SANS_ASSERT(neqn == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                                 // solution
  DLA::VectorS<PhysDim::D, ArrayQ<T> > gradq;  // gradient
  DLA::VectorS<TopoDim::D, ArrayQ<T> > q_sRef; // solution derivative wrt reference coordinates

  DLA::VectorS<TopoDim::D, VectorX<Real> > X_sRef; // X derivative wrt reference coordinates
  DLA::VectorS<TopoDim::D, VectorX<T> > r_sRef;

  DLA::VectorS<PhysDim::D, ArrayQ<Ti> > F;     // PDE flux F(X, Q), Fv(X, Q, QX)
  ArrayQ<Ti> source;                           // PDE source S(X, Q, QX)

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_.data(), gradphi_.size() );

  // derivative of solution and coordinates wrt reference coordinates
  qfldElem_.evalDerivative( sRef, q_sRef );
  xfldElem_.evalDerivative( sRef, X_sRef );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  qfldElem_.evalFromBasis( gradphi_.data(), gradphi_.size(), gradq );

  MatrixSymX<T> fbar;
  forcefldElem_.evalFromBasis( phi_.data(), phi_.size(), fbar );

  MatrixSymX<Tcs> epsbar_force;
  epsforcefldElem_.evalFromBasis( phi_.data(), phi_.size(), epsbar_force );

  VectorX<Ts> load;
  loadfldElem_.evalFromBasis( phi_.data(), phi_.size(), load );

  pde_.getPositionRefDerivs( q, q_sRef, r_sRef );

  MatrixSymX<T> epsbar_geom;
  pde_.calc_epsbar_geom( X_sRef, r_sRef, epsbar_geom );

  // Compute the delta between the force strain and geometric strain
  MatrixSymX<Tcs> depsbar = epsbar_geom - epsbar_force;

  // Jacobian of element coordinates xi,eta, expressed as 1/J0^2
  Real J0 = xfldElem_.jacobianDeterminant( sRef );
  Real J0invsq = pow( J0, -2 );

  for (int k = 0; k < neqn; k++)
  {
    VectorX<T> w1 = e_nodes_[k].col(0);
    VectorX<T> w2 = e_nodes_[k].col(1);

    // Strain displacement residuals
    integrand[k][pde_.iReps11] = Transpose(w1)*depsbar*w1 * phi_[k];
    integrand[k][pde_.iReps22] = Transpose(w2)*depsbar*w2 * phi_[k];
    integrand[k][pde_.iReps12] = Transpose(w1)*depsbar*w2 * phi_[k];

    // Force equlibrium residuals
    integrand[k][pde_.iRf1]  = -Transpose(w1)*fbar*gradphi_[k];
    integrand[k][pde_.iRf2]  = -Transpose(w2)*fbar*gradphi_[k];
    integrand[k][pde_.iRf1] += dot(w1, load) * phi_[k];
    integrand[k][pde_.iRf2] += dot(w2, load) * phi_[k];


    // In-plane rotation constraint
    const MatrixX<Real>& w0 = get<PDE::ie0>(paramfldElem_).DOF(k);

    VectorX<Real> w01 = w0.col(0);
    VectorX<Real> w02 = w0.col(1);

    VectorX<Real>& s0 = X_sRef[0];

    VectorX<T>& stld = r_sRef[0];

    VectorX<T> s0tld = s0 + epsbar_geom * s0;

    T theta0 = atan2(dot(s0tld,w02), dot(s0tld,w01));
    T theta  = atan2(dot(stld, w2 ), dot(stld, w1 ));

    if (theta >  2*PI) theta -= 2*PI;
    if (theta < -2*PI) theta += 2*PI;

    // Term corresponding to R^e3
    integrand[k][pde_.iRlam3] = J0invsq * (theta - theta0) * phi_[k];
  }
}


}

#endif  // INTEGRANDCELL_GALERKIN_HSMKL_H
