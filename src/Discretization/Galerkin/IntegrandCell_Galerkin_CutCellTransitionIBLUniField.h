// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_IntegrandCell_Galerkin_cutCellTransitionIBLUniFieldUNIFIELD_H_
#define SRC_DISCRETIZATION_GALERKIN_IntegrandCell_Galerkin_cutCellTransitionIBLUniFieldUNIFIELD_H_

// cell integrand operator: Galerkin specialized for UniField IBL in cut-cell transition formulation

#include <ostream>
#include <vector>

#include "BasisFunction/BasisFunctionCategory.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "Discretization/Integrand_Type.h"

#include "pde/IBL/PDEIBLUniField.h"

namespace SANS
{

template<class PDE_>
class IntegrandCell_Galerkin_cutCellTransitionIBLUniField;

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template<class VarType,
         template<class, class> class PDENDConvert>
class IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > > :
    public IntegrandCellType<IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > > >
{
public:
  typedef PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::VarInterpType VarInterpType;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  IntegrandCell_Galerkin_cutCellTransitionIBLUniField(const PDE& pde,
                                  const std::vector<int>& CellGroups) :
    pde_(pde),
    cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  // Basis-weighted integrand class: i.e. weighting function = basis function of solution q
  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > PDE;

    typedef Element<ArrayQ<T>, TopoDim, Topology>     ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;
    template <class TVectorX>
    using VectorXT = DLA::VectorS<PhysD2::D,TVectorX>;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef DLA::VectorS<TopoDim::D, VectorX> LocalAxes;         // manifold local axes type
    typedef DLA::VectorS<PhysDim::D, LocalAxes> VectorLocalAxes; // gradient of local axes type

    template<class Z>
    using IntegrandType = ArrayQ<Z>;

    BasisWeighted(const PDE& pde,
                  const ElementParam& paramfldElem,
                  const ElementQFieldType& qfldElem) :
      pde_(pde),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem_)), // XField must be the last parameter
      qfldElem_(qfldElem),
      nDOFPDE_(qfldElem_.nDOF()),
      phi_( new T[nDOFPDE_] ),
      gradphi_( new VectorXT<T>[nDOFPDE_] )
    {
      static_assert(TopoDim::D == 1, "Only TopoD1 is implemented so far."); // TODO: generalize
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOF() const { return nDOFPDE_; }

    // cell element residual integrand
    template<class Ti>
    void operator()( const Real dJ_sRef, const T& sRef, const T& dsRef_dsRefRef,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const;

    // residuals for transition front (i.e. interface between cut-cell elements)
    template<class Ti>
    void transitionFront( const VectorX& nrm_LtoT, const T& sRefTr,
                          DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const;

    const PDE& getpde() const { return pde_; }
    const ElementQFieldType& getqfldElem() const { return qfldElem_; }

#if 1 //TODO: these need to be relocated to a more proper place
    template<class Tscalar>
    Real getScalarValuePOD(const Tscalar& x) const { return x.value(); }

    Real getScalarValuePOD(const Real& x) const { return x; }
#endif

  private:
    const PDE& pde_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;

    const int nDOFPDE_;
    mutable T *phi_; // TODO: always Surrealizing is slow
    mutable VectorXT<T> *gradphi_; // TODO: always Surrealizing is slow
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};


template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return (pde_.hasFluxAdvective() ||
          pde_.hasFluxViscous() ||
          pde_.hasSource() ||
          pde_.hasForcingFunction());
}


// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//

template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ_sRef, const T& sRef, const T& dsRef_dsRefRef,
           DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOFPDE_);

  const bool needsSolutionGradient = pde_.hasFluxViscous() || pde_.needsSolutionGradientforSource();

  const Real sRefPOD = getScalarValuePOD(sRef); //TODO: assuming linear grid; revert to Real from Surreal

  // Basis vector and its gradients
  VectorX e01; // basis direction vector
  xfldElem_.unitTangent(sRefPOD, e01);

  LocalAxes e0; // basis direction vector
  e0[0] = e01;

  VectorLocalAxes e0_X; // manifold local axes tangential Cartesian gradient
  xfldElem_.evalUnitTangentGradient(sRefPOD, e0_X); //TODO: assuming linear grid;

  // Elemental parameters (includes X)
  ParamT param; // Elemental parameters (such as grid coordinates and distance functions)
  paramfldElem_.left().eval(sRef, param.left()); //TODO: this assumes the entire left element is Surrealized
  paramfldElem_.right().eval(sRefPOD, param.right()); // right() = XFieldElement is not Surrealized

  // original basis value, gradient
  qfldElem_.evalBasis(sRef, phi_, nDOFPDE_);
  xfldElem_.evalBasisGradient(sRef, qfldElem_, gradphi_, nDOFPDE_);

  // solution value, gradient
  ArrayQ<T> q = 0.0; // solution
  qfldElem_.evalFromBasis(phi_, nDOFPDE_, q); // interpret DOFs with original basis function phi_

  VectorArrayQ<T> gradq = 0.0; // solution gradient
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis(gradphi_, nDOFPDE_, gradq);

  // compute the residual

  const T dJ_dsRefRef = dJ_sRef*dsRef_dsRefRef;

  // Galerkin flux term: -gradient(phi) . F.  Currently, only consider advective flux
  if (pde_.hasFluxAdvective())
  {
    VectorArrayQ<Ti> F = 0.0; // PDE flux F(X, U)

    pde_.fluxAdvective(param, e0, q, F);

    for (int k = 0; k < nDOFPDE_; ++k)
      rsdPDEElem(k) -= dot(gradphi_[k],F)*dJ_dsRefRef;
  }

  // Galerkin source term: +phi S
  if (pde_.hasSource())
  {
    ArrayQ<Ti> source = 0;                            // PDE source S(X, Q, QX)

    pde_.source(param, e0, e0_X, q, gradq, source);

    for (int k = 0; k < nDOFPDE_; ++k)
      rsdPDEElem(k) += phi_[k]*source*dJ_dsRefRef;
  }
}


template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
transitionFront(const VectorX& nrm_LtoT, const T& sRefTr,
                DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOFPDE_);

  const Real sRefTrPOD = getScalarValuePOD(sRefTr); //TODO: assuming linear grid

  // Basis vector and its gradients
  VectorX e01; // basis direction vector
  xfldElem_.unitTangent(sRefTrPOD, e01);

  LocalAxes e0; // basis direction vector
  e0[0] = e01;

  // Elemental parameters (includes X)
  ParamT param; // Elemental parameters (such as grid coordinates and distance functions)
  paramfldElem_.left().eval(sRefTr, param.left()); //TODO: this assumes the entire left element is Surrealized
  paramfldElem_.right().eval(sRefTrPOD, param.right()); // right() = XFieldElement is not Surrealized

  // basis value, gradient
  qfldElem_.evalBasis(sRefTr, phi_, nDOFPDE_);

  // solution value
  ArrayQ<T> q = 0.0; // solution
  qfldElem_.evalFromBasis(phi_, nDOFPDE_, q);

  // compute residual

  ArrayQ<Ti> pointSource = 0.0; // flux at transition front
  pde_.transitionFrontPointSource(param, e0, nrm_LtoT, q, pointSource);

  for (int k = 0; k < nDOFPDE_; ++k)
    rsdPDEElem(k) += phi_[k]*pointSource;
}

} // namespace SANS

#endif /* SRC_DISCRETIZATION_GALERKIN_IntegrandCell_Galerkin_cutCellTransitionIBLUniFieldUNIFIELD_H_ */
