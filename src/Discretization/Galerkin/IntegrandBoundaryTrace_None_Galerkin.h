// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_NONE_GALERKIN_H
#define INTEGRANDBOUNDARYTRACE_NONE_GALERKIN_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCNone.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Integrand_Galerkin_fwd.h"

#include "Stabilization_Nitsche.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::None>, Galerkin> :
  public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::None>, Galerkin> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::None Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;        // matrices

  static const int N = PDE::N;


  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc,
                          const std::vector<int>& BoundaryGroups )
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups) {}

  //agnostic constructor for stabilized CG
  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc,
                          const std::vector<int>& BoundaryGroups, const StabilizationNitsche& stab )
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups) {}

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology     > ElementQFieldCell;
    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamTL;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted(  const PDE& pde, const BCBase& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem, // Xfield must be the last parameter
                    const ElementQFieldCell& qfldElem ) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)),
                    qfldElem_(qfldElem),
                    paramfldElem_(paramfldElem),
                    useFluxViscous_((static_cast<const BCNone<PhysDim,PDE::N>*>(&bc_))->useFluxViscous()),
                    nDOF_(qfldElem.nDOF()),
                    phi_( new Real[nDOF_] ),
                    gradphi_( new VectorX[nDOF_] )
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                    pde_(bw.pde_), bc_(bw.bc_),
                    xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                    xfldElem_(bw.xfldElem_),
                    qfldElem_(bw.qfldElem_),
                    paramfldElem_(bw.paramfldElem_),
                    useFluxViscous_(bw.useFluxViscous_),
                    nDOF_(bw.nDOF_),
                    phi_( bw.phi_ ),
                    gradphi_( bw.gradphi_ )
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrand[], int neqn ) const;

  protected:
    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementParam& paramfldElem_;
    const bool useFluxViscous_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<ArrayQ<Tw>, TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<Real, TopoDimCell, Topology> ElementEFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type IntegrandType;

    FieldWeighted(  const PDE& pde, const BCBase& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem, // Xfield must be the last parameter
                    const ElementQFieldCell& qfldElem,
                    const ElementWFieldCell& wfldElem,
                    const ElementEFieldCell& efldElem) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)),
                    qfldElem_(qfldElem),
                    wfldElem_(wfldElem),
                    efldElem_(efldElem),
                    paramfldElem_(paramfldElem),
                    useFluxViscous_((static_cast<const BCNone<PhysDim,PDE::N>*>(&bc_))->useFluxViscous()),
                    nDOF_(qfldElem_.nDOF()),
                    nPhi_(efldElem.nDOF()),
                    ephi_( new Real[nPhi_] ),
                    phi_( new Real[nDOF_] ),
                    gradphi_( new VectorX[nDOF_] ),
                    weight_( new ArrayQ<Tw>[nPhi_] )
    {
    }

    FieldWeighted( FieldWeighted&& fw ) :
                    pde_(fw.pde_), bc_(fw.bc_),
                    xfldElemTrace_(fw.xfldElemTrace_), canonicalTrace_(fw.canonicalTrace_),
                    xfldElem_(fw.xfldElem_),
                    qfldElem_(fw.qfldElem_),
                    wfldElem_(fw.wfldElem_),
                    efldElem_(fw.efldElem_),
                    paramfldElem_(fw.paramfldElem_),
                    useFluxViscous_(fw.useFluxViscous_),
                    nDOF_(fw.nDOF_),
                    nPhi_(fw.nPhi_),
                    ephi_( fw.ephi_ ),
                    phi_( fw.phi_ ),
                    gradphi_( fw.gradphi_ ),
                    weight_( fw.weight_ )
    {
      fw.ephi_    = nullptr;
      fw.phi_     = nullptr;
      fw.gradphi_ = nullptr;
      fw.weight_  = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] ephi_;
      delete [] phi_;
      delete [] gradphi_;
      delete [] weight_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& RefTrace, Ti integrand[], const int nphi ) const;

  protected:
    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementEFieldCell& efldElem_;
    const ElementParam& paramfldElem_;
    const bool useFluxViscous_;

    const int nDOF_, nPhi_;
    mutable Real *ephi_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable ArrayQ<Tw> *weight_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell , Topology >& qfldElem) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(pde_, bc_,
                                                                  xfldElemTrace, canonicalTrace,
                                                                  paramfldElem,
                                                                  qfldElem);
  }

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted< Tq, Tw, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<Tq>, TopoDimCell , Topology >& qfldElem,
            const Element<ArrayQ<Tw>, TopoDimCell , Topology >& wfldElem,
            const Element<Real, TopoDimCell , Topology >& efldElem) const
  {
    return FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace,
                                 TopoDimCell,  Topology, ElementParam >( pde_, bc_,
                                                                         xfldElemTrace, canonicalTrace,
                                                                         paramfldElem,
                                                                         qfldElem, wfldElem, efldElem);
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
};

template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam>
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::None>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );
  ParamTL paramL;           // Elemental parameters (such as grid coordinates and distance functions)

  VectorX N;                // unit normal (points out of domain)

  ArrayQ<T> q;              // solution
  VectorArrayQ<T> gradq;    // gradient

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  const bool needsSolutionGradient = false; //pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, N);

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  qfldElem_.evalFromBasis( phi_, nDOF_, q );

  if (needsSolutionGradient)
  {
    // basis and solution gradient
    xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
  }
  else
    gradq = 0;

  VectorArrayQ<Ti> F = 0;     // PDE flux

  // PDE residual: weak form boundary integral
  // advective flux
  if ( pde_.hasFluxAdvective() )
    pde_.fluxAdvective( paramL, q, F );

  // Generally no viscous flux (talk to Prof. Darmofal about it)
  if ( pde_.hasFluxViscous() && useFluxViscous_ )
   pde_.fluxViscous( paramL, q, gradq, F );

  ArrayQ<Ti> Fn = dot(N,F);
  for (int k = 0; k < neqn; k++)
    integrand[k] = phi_[k]*Fn;


  // PDE residual: BC Lagrange multiplier term

  // No-op for None

  // BC residual: BC weak form

  // No-op for None
}


template <class PDE, class NDBCVector>
template <class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                              class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::None>, Galerkin>::
FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, Ti integrand[], const int nphi ) const
{
  SANS_ASSERT( nphi == nPhi_ );
  ParamT paramL; // Elemental parameters (such as grid coordinates and distance functions)

  VectorX N;                // unit normal (points out of domain)

  ArrayQ<Tq> q;              // solution
  VectorArrayQ<Tq> gradq;    // gradient

  ArrayQ<Tw> w;              // weight

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  const bool needsSolutionGradient = false; //pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, N);

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  qfldElem_.evalFromBasis( phi_, nDOF_, q );

  if (needsSolutionGradient)
  {
    // basis and solution gradient
    xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
  }
  else
    gradq = 0;

  wfldElem_.eval( sRef, w );

  efldElem_.evalBasis( sRef, ephi_, nPhi_ );

  for (int k = 0; k < nPhi_; k++)
  {
    integrand[k] = 0;
    weight_[k] = ephi_[k]*w;
  }

  VectorArrayQ<Ti> F = 0;     // PDE flux

  // PDE residual: weak form boundary integral
  // advective flux
  if ( pde_.hasFluxAdvective() )// || pde_.hasFluxViscous())
    pde_.fluxAdvective( paramL, q, F );

  // Generally no viscous flux (talk to Prof. Darmofal about it)
  if ( pde_.hasFluxViscous() && useFluxViscous_ )
    pde_.fluxViscous( paramL, q, gradq, F );


  ArrayQ<Ti> Fn = dot(N,F);
  for (int k = 0; k < nPhi_; k++)
    integrand[k] += dot(weight_[k],Fn);

  // PDE residual: BC Lagrange multiplier term

  // No-op for None

  // BC residual: BC weak form

  // No-op for None
}

}

#endif  // INTEGRANDBOUNDARYTRACE_NONE_GALERKIN_H
