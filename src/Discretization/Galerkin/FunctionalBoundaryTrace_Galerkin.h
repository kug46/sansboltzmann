// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONALBOUNDARYTRACE_GALERKIN_H
#define FUNCTIONALBOUNDARYTRACE_GALERKIN_H

// boundary-trace integral functional

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Element/ElementIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Functional boundary-trace integral
//

template<class FunctionalIntegrandBoundaryTrace, class T>
class FunctionalBoundaryTrace_Galerkin_impl :
    public GroupIntegralBoundaryTraceType< FunctionalBoundaryTrace_Galerkin_impl<FunctionalIntegrandBoundaryTrace, T> >
{
public:
  typedef typename FunctionalIntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<T   > ArrayJ;

  // Save off the boundary trace integrand and the residual vectors
  FunctionalBoundaryTrace_Galerkin_impl( const FunctionalIntegrandBoundaryTrace& fcnJ,
                                         ArrayJ& functional ) :
    fcnJ_(fcnJ), functional_(functional), comm_rank_(0)
  {}

  std::size_t nBoundaryGroups() const { return fcnJ_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnJ_.boundaryGroup(n); }
  std::vector<int> getBoundaryGroups() const { return fcnJ_.getBoundaryGroups(); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>:: template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // trace element integral
    ElementIntegral<TopoDimTrace, TopologyTrace, ArrayJ> integral(quadratureorder);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      qfldCellL.getElement( qfldElemL, elemL );

      // only need to perform the volume integral on processors that possess the element
      if ( qfldElemL.rank() != comm_rank_ ) continue;

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );

      ArrayJ result = 0;

      integral( fcnJ_.integrand(xfldElemTrace, canonicalTraceL,
                                xfldElemL, qfldElemL),
                xfldElemTrace, result );

      // sum up the functional
      functional_ += result;
    }
  }

protected:
  const FunctionalIntegrandBoundaryTrace& fcnJ_;
  ArrayJ& functional_;
  std::vector<int> boundaryTraceGroups_;
  mutable int comm_rank_;
};

// Factory function

template<class FunctionalIntegrandBoundaryTrace, class ArrayJ>
FunctionalBoundaryTrace_Galerkin_impl<FunctionalIntegrandBoundaryTrace, typename Scalar<ArrayJ>::type>
FunctionalBoundaryTrace_Galerkin( const IntegrandBoundaryTraceType<FunctionalIntegrandBoundaryTrace>& fcnJ,
                                  ArrayJ& functional )
{
  typedef typename Scalar<ArrayJ>::type T;
  static_assert( std::is_same<ArrayJ, typename FunctionalIntegrandBoundaryTrace::template ArrayJ<T> >::value, "These should be the same.");
  return {fcnJ.cast(), functional};
}

}

#endif  // FUNCTIONALBOUNDARYTRACE_GALERKIN_H
