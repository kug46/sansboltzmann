// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_OUTPUT_STABILIZED
#define INTEGRANDCELL_GALERKIN_OUTPUT_STABILIZED

#include <vector>

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Discretization/Integrand_Type.h"

#include "Discretization/Galerkin/Stabilization_Galerkin.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand for generic output functional


template <class OutputFunctional, class PDE>
class IntegrandCell_Galerkin_Output_Stabilized :
    public IntegrandCellType< IntegrandCell_Galerkin_Output_Stabilized<OutputFunctional, PDE> >
{
public:
  typedef typename OutputFunctional::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual arrays

  template <class T>
  using TensorMatrixQ = typename PDE::template TensorMatrixQ<T>;    // diffusion matrix

  template <class T>
  using TensorSymArrayQ = typename PDE::template TensorSymArrayQ<T>;    // solution/residual arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = typename OutputFunctional::template ArrayJ<T>;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = typename OutputFunctional::template MatrixJ<T>;

  // cppcheck-suppress noExplicitConstructor
  IntegrandCell_Galerkin_Output_Stabilized( const PDE& pde, const OutputFunctional& outputFcn,
                                            const std::vector<int>& cellGroups, const StabilizationMatrix& tau ) :
    pde_(pde), outputFcn_(outputFcn), cellGroups_(cellGroups), tau_(tau)
  {
    // Nothing
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  const OutputFunctional& getOutputFcn() const {return outputFcn_;}

  template<class T, class TopoDim, class Topology, class ElementParam>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim  , TopoDim, Topology> ElementXFieldType;
    typedef Element      <ArrayQ<Real>, TopoDim, Topology> ElementQRFieldType;
    typedef Element      <ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element      <Real     , TopoDim, Topology> ElementEFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef DLA::MatrixSymS<PhysDim::D, Real> TensorSymX;
    typedef typename ElementParam::T ParamT;
    typedef typename ElementParam::gradT ParamGradT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;

    static const int N = PDE::N;
    static const int nOut = DLA::VectorSize<ArrayJ<T>>::M;

    Functor( const PDE& pde,
             const OutputFunctional& outputFcn,
             const ElementParam& paramfldElem,
             const ElementQFieldType& qfldElem,
             const StabilizationMatrix& tau) :
      pde_(pde),
      outputFcn_(outputFcn),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
      qfldElem_(qfldElem),
      nDOF_(qfldElem.nDOF()),
      order_( qfldElem_.basis()->order() ),
      nNodes_( qfldElem_.basis()->nBasisNode() ),
      phi_( new Real[nDOF_] ),
      gradphi_( new VectorX[nDOF_] ),
      hessphi_( new TensorSymX[nDOF_] ),
      tau_(tau),
      stabType_(tau.getStabType()),
      stabElem_( tau.getStabOrder(), qfldElem_.basis()->category() ),
      phiLin_( new Real[stabElem_.nDOF()] ),
      gradphiLin_( new VectorX[stabElem_.nDOF()] )
    {
    }

    Functor( Functor&& f ) :
      pde_(f.pde_),
      outputFcn_(f.outputFcn_),
      paramfldElem_(f.paramfldElem_),
      xfldElem_(f.xfldElem_),
      qfldElem_(f.qfldElem_),
      nDOF_(f.nDOF_),
      order_( f.order_ ),
      nNodes_( f.nNodes_ ),
      phi_( f.phi_ ),
      gradphi_( f.gradphi_ ),
      hessphi_( f.hessphi_ ),
      tau_(f.tau_),
      stabType_(f.stabType_),
      stabElem_( f.tau_.getStabOrder(), f.qfldElem_.basis()->category() ),
      phiLin_( f.phiLin_ ),
      gradphiLin_( f.gradphiLin_ )
    {
      f.phi_ = nullptr; f.gradphi_ = nullptr; f.hessphi_ = nullptr;
      f.phiLin_ = nullptr; f.gradphiLin_ = nullptr;
    }

    ~Functor()
    {
      delete [] phi_;
      delete [] gradphi_;
      delete [] hessphi_;
      delete [] phiLin_;
      delete [] gradphiLin_;
    }

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }


    void operator()( const QuadPointType& sRef, ArrayJ<T>& integrand ) const;

    void operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<MatrixJ<Real>>& mtxPDEElem ) const;

  protected:
    template <class Tq, class Tg, class Th, class Ti>
    void evalIntegrand( const ParamT& param,
                            const ArrayQ<Tq>& q, const ArrayQ<Real>& qR,
                            const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Real>& gradqR,
                            const TensorSymArrayQ<Th>& hessq, const TensorSymArrayQ<Real>& hessqR,
                            ArrayJ<Ti>& integrand ) const;

    const PDE& pde_;
    const OutputFunctional& outputFcn_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const int nDOF_;
    const int order_;
    const int nNodes_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable TensorSymX *hessphi_;
    const StabilizationMatrix& tau_;
    const StabilizationType stabType_;

    const ElementEFieldType stabElem_;
    mutable Real *phiLin_;
    mutable VectorX *gradphiLin_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  Functor<T,TopoDim,Topology,ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return Functor<T,TopoDim,Topology,ElementParam>(pde_, outputFcn_,
                                                    paramfldElem, qfldElem, tau_);
  }

private:
  const PDE& pde_;
  const OutputFunctional& outputFcn_;
  const std::vector<int> cellGroups_;
  const StabilizationMatrix& tau_;
};

template<class OutputFunctional, class PDE>
template<class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_Output_Stabilized<OutputFunctional, PDE>::
Functor<T, TopoDim, Topology, ElementParam>::
operator()( const QuadPointType& sRef, ArrayJ<T>& integrand ) const
{
  VectorX X;       // physical coordinates
  ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q = 0;               // solution
  VectorArrayQ<T> gradq = 0;     // gradient
  TensorSymArrayQ<T> hessq = 0;  // solution hessian

  const bool needsSolutionGradient = (outputFcn_.needsSolutionGradient() || stabType_ != StabilizationType::Unstabilized );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // solution value, gradient
  qfldElem_.eval( sRef, q );

  if (needsSolutionGradient )
  {
    xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
  }

  if (pde_.hasFluxViscous() && stabType_ != StabilizationType::Unstabilized && order_ > 1)
  {
    xfldElem_.evalBasisHessian(  sRef, qfldElem_, hessphi_, nDOF_ );
    xfldElem_.evalHessian( sRef, qfldElem_, hessq );
  }

  // evaluate stabilization weight
  stabElem_.evalBasis(sRef, phiLin_, stabElem_.nDOF());
  xfldElem_.evalBasisGradient( sRef, stabElem_, gradphiLin_, stabElem_.nDOF() );

  evalIntegrand(param, q, q, gradq, gradq, hessq, hessq, integrand);

}


template<class OutputFunctional, class PDE>
template<class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_Output_Stabilized<OutputFunctional, PDE>::
Functor<T, TopoDim, Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<MatrixJ<Real>>& mtxPDEElem ) const
{
  typedef SurrealS<N> SurrealClass;
  static const int nDeriv = SurrealClass::N;

  VectorX X;       // physical coordinates
  ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q = 0;              // solution
  VectorArrayQ<Real> gradq = 0;    // gradient
  TensorSymArrayQ<Real> hessq = 0;    // solution hessian

  ArrayQ<SurrealClass> qSurreal;              // solution
  VectorArrayQ<SurrealClass> gradqSurreal;    // gradient
  TensorSymArrayQ<SurrealClass> hessqSurreal;    // solution hessian

  const bool needsSolutionGradient = (outputFcn_.needsSolutionGradient() || stabType_ != StabilizationType::Unstabilized );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // solution value, gradient
  qfldElem_.eval( sRef, q );

  if (needsSolutionGradient )
  {
    xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
  }

  if (pde_.hasFluxViscous() && stabType_ != StabilizationType::Unstabilized && order_ > 1)
  {
    xfldElem_.evalBasisHessian(  sRef, qfldElem_, hessphi_, nDOF_ );
    xfldElem_.evalHessian( sRef, qfldElem_, hessq );
  }

  // evaluate stabilization weight
  stabElem_.evalBasis(sRef, phiLin_, stabElem_.nDOF());
  xfldElem_.evalBasisGradient( sRef, stabElem_, gradphiLin_, stabElem_.nDOF() );

  //surrealize
  qSurreal = q;
  gradqSurreal = gradq;
  hessqSurreal = hessq;

  // element integrand/residual
  ArrayJ<SurrealClass> integrandSurreal;
  MatrixJ<Real> J_q =0, J_gradq = 0, J_hessq; // temporary storage

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables
    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += N;

    integrandSurreal = 0;

    evalIntegrand(param, qSurreal, q, gradq, gradq, hessq, hessq, integrandSurreal);

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
      for (int m = 0; m < nOut; m++)
        for (int n = 0; n < N; n++)
        {
          // Note that n,m are transposed intentionally
          DLA::index(J_q,n,m) = DLA::index(integrandSurreal, m).deriv(slot + n - nchunk);
        }

      for (int j = 0; j < nDOF_; j++)
        mtxPDEElem[j] += dJ*phi_[j]*J_q;

    }
    slot += N;
  } // nchunk


  if (needsSolutionGradient)
  {
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
        slot += N;
      }

      integrandSurreal = 0;

      evalIntegrand(param, q, q, gradqSurreal, gradq, hessq, hessq, integrandSurreal);

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < nOut; m++)
            for (int n = 0; n < N; n++)
            {
              // Note that n,m are transposed intentionally
              DLA::index(J_gradq,n,m) = DLA::index(integrandSurreal, m).deriv(slot + n - nchunk);
            }

          for (int j = 0; j < nDOF_; j++)
            mtxPDEElem[j] += dJ*gradphi_[j][d]*J_gradq;

        }
        slot += N;
      }
    } // nchunk
  }

  if (pde_.hasFluxViscous() && qfldElem_.order() > 1 && stabType_ != StabilizationType::Unstabilized)
  {
    const int nHess = PhysDim::D*(PhysDim::D + 1)/2;

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nHess*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurreal(ii,jj), n).deriv(slot + n - nchunk) = 1;
          slot += PDE::N;
        }

      integrandSurreal = 0;

      evalIntegrand(param, q, q, gradq, gradq, hessqSurreal, hessq, integrandSurreal);

      // accumulate derivatives into element jacobian

      slot = 0;
      for ( int ii = 0; ii < PDE::D; ii++ )
        for ( int jj = 0; jj <= ii; jj++ )
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int n = 0; n < PDE::N; n++)
              DLA::index(hessqSurreal(ii,jj), n).deriv(slot + n - nchunk) = 0; // Reset the derivative

            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < nOut; m++)
              for (int n = 0; n < PDE::N; n++)
              {
                // Note that n,m are transposed intentionally
                DLA::index(J_hessq,n,m) = DLA::index(integrandSurreal, m).deriv(slot + n - nchunk);
              }

            for (int j = 0; j < nDOF_; j++)
              mtxPDEElem[j] += dJ*hessphi_[j](ii,jj)*J_hessq;
          }
          slot += PDE::N;
        }
    }
  } // nchunk

}


template<class OutputFunctional, class PDE>
template<class T, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tg, class Th, class Ti>
void
IntegrandCell_Galerkin_Output_Stabilized<OutputFunctional, PDE>::
Functor<T, TopoDim, Topology, ElementParam>::
evalIntegrand(const ParamT& param,
    const ArrayQ<Tq>& q, const ArrayQ<Real>& qR,
    const VectorArrayQ<Tg>& gradq, const VectorArrayQ<Real>& gradqR,
    const TensorSymArrayQ<Th>& hessq, const TensorSymArrayQ<Real>& hessqR,
    ArrayJ<Ti>& integrand) const
{

  StabilizationType stabType = tau_.getStabType();

  outputFcn_(param, q, gradq, integrand);

  //CORRECTION TERM u* = u - tau.strongPDE
  if (stabType == StabilizationType::Unstabilized || stabType == StabilizationType::AGLSPrimal )
  {
    //NO CORRECTION TERM NECESSARY FOR UNSTABILIZED, AGLS
  }
  else
  {

    ArrayQ<Ti> source = 0;            // PDE source S(X, Q, QX)
    if (pde_.hasSource())
      pde_.source( param, q, gradq, source );

    ArrayQ<Real> forcing = 0;
    if (pde_.hasForcingFunction())
      pde_.forcingFunction( param, forcing );


    //contruct strongPDE
    ArrayQ<Ti> strongPDE = 0;
    if (stabType_ == StabilizationType::AGLSAdjoint)
    {
      TensorMatrixQ<Real> KR = 0;
      if (pde_.hasFluxViscous())
        pde_.diffusionViscous( param, qR, gradqR, KR );

      tau_.template constructAdjointPDE<Tq, Tg, Th, Ti>(pde_, param, q, qR, gradq, gradqR, hessq, hessqR, KR, order_, strongPDE);
    }
    else
      tau_.template constructStrongPDE<Tq, Tg, Th, Ti>(pde_, param, q, gradq, hessq, source, forcing, strongPDE);


    //CONSTRUCT TAU
    TensorMatrixQ<Ti> K = 0;
    if (pde_.hasFluxViscous())
      pde_.diffusionViscous( param, q, gradq, K );

    MatrixQ<Ti> S = 0;
    if (pde_.hasSource())
      pde_.jacobianSourceAbsoluteValue(param, q, gradq, S);

    //APPLY STABILIZATION MATRIX

    tau_.template solve<Tq,Ti,Ti>(pde_, param, q, K, S, phiLin_, gradphiLin_, stabElem_.order(),
                                  nNodes_, stabElem_.nDOF(), strongPDE);

    MatrixJ<Ti> dJdu = 0;
    outputFcn_.outputJacobian(param, q, gradq, dJdu);

    integrand -= dot(dJdu, strongPDE);
  }
}



}

#endif //INTEGRANDCELL_GALERKIN_OUTPUT_STABILIZED
