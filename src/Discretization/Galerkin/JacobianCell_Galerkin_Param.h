// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_GALERKIN_PARAM_H
#define JACOBIANCELL_GALERKIN_PARAM_H

// cell integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/Tuple/SurrealizedElementTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

// forward declaration
template<class Surreal, int iParam, class IntegrandCell, class MatrixQP_>
class JacobianCell_Galerkin_Param_impl;

//----------------------------------------------------------------------------//
//  Galerkin cell integral
//

template<class Surreal, int iParam, class IntegrandCell, class MatrixQP_>
class JacobianCell_Galerkin_Param_impl :
    public GroupIntegralCellType< JacobianCell_Galerkin_Param_impl<Surreal, iParam, IntegrandCell, MatrixQP_> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;

  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template ArrayQ<Surreal> ArrayQSurreal;

  typedef DLA::VectorD<ArrayQSurreal> ResidualElemClass;

  JacobianCell_Galerkin_Param_impl(const IntegrandCell& fcn,
                                   MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p ) :
    fcn_(fcn),
    mtxGlobalPDE_p_(mtxGlobalPDE_p) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template<class Topology, class TopoDim, class TupleFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename TupleFieldType                 ::template FieldCellGroupType<Topology>& tuplefldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
             const int quadratureorder )
  {
    typedef typename TupleFieldType                 ::template FieldCellGroupType<Topology> TupleFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupType, Surreal, iParam>::type ElementTupleFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    typedef typename TupleType<iParam, TupleFieldCellGroupType>::type ParamCellGroupType;
    typedef typename ParamCellGroupType::template ElementType<Surreal> ElementParamFieldClass;

    typedef typename ElementParamFieldClass::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP; // jacobian type associated with each DOF

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass; // jacobian type associated with each element

    // field cell groups
    const ParamCellGroupType& paramfldCell = get<iParam>(tuplefldCell);

    // field elements
    ElementTupleFieldClass  tuplefldElem( tuplefldCell.basis() );
    ElementQFieldClass      qfldElem( qfldCell.basis() );
    ElementParamFieldClass& paramfldElem = set<iParam>(tuplefldElem);

    // DOFs per element
    const int nDOF = qfldElem.nDOF();
    const int paramDOF = paramfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapQDOFGlobal(nDOF,-1);
    std::vector<int> mapParamDOFGlobal(paramDOF,-1);

    // element integrand/residual
    ResidualElemClass rsdPDEElem( nDOF );

    // element jacobian matrix
    MatrixElemClass mtxPDEElem(nDOF, paramDOF);

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, ResidualElemClass> integral(quadratureorder);

    // The integrand to be integrated
    auto integrand = fcn_.integrand( tuplefldElem, qfldElem );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = tuplefldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobian to zero
      mtxPDEElem = 0;

      // copy global parameter/solution DOFs to element
      tuplefldCell.getElement( tuplefldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // loop over derivative chunks (i.e. each entry of each parameter DOF in both left and right elements)
      for (int nchunk = 0; nchunk < nArrayP*paramDOF; nchunk += nDeriv)
      {
        // associate derivative slots with parameter DOFs
        
        int slot = 0;
        for (int j = 0; j < paramDOF; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElem.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // reset PDE residuals to zero
        rsdPDEElem = 0.0;

        // cell integration for canonical element
        integral(integrand, get<-1>(tuplefldElem), rsdPDEElem);
        // xfld should always be the last component of the tuple

        // accumulate derivatives into element jacobian
        
        for (int j = 0; j < paramDOF; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElem.DOF(j), n).deriv(slot - nchunk) = 0; // Reset the derivative

              for (int i = 0; i < nDOF; i++)
              {
                for (int m = 0; m < nArrayQ; m++)
                {
                  DLA::index(mtxPDEElem(i,j),m,n) = DLA::index(rsdPDEElem[i],m).deriv(slot - nchunk);
                }
              }
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global
      scatterAdd( qfldCell, paramfldCell, elem,
                  mapQDOFGlobal.data(), nDOF,
                  mapParamDOFGlobal.data(), paramDOF,
                  mtxPDEElem, mtxGlobalPDE_p_ );
    }
  }

protected:
  //----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, class ParamFieldCellGroupType, class MatrixQP, class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfldCell,
      const ParamFieldCellGroupType& paramfldCell,
      const int elem,
      int mapQDOFGlobal[], const int nDOF,
      int mapParamDOFGlobal[], const int paramDOF,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElem,
      SparseMatrixType& mtxGlobalPDE_p )
  {
    qfldCell.associativity( elem ).getGlobalMapping( mapQDOFGlobal, nDOF );

    paramfldCell.associativity( elem ).getGlobalMapping( mapParamDOFGlobal, paramDOF );

    mtxGlobalPDE_p.scatterAdd( mtxPDEElem, mapQDOFGlobal, nDOF, mapParamDOFGlobal, paramDOF );
  }

#if 0
  //----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, class MatrixQ, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfld,
      const int elem,
      int mapDOFGlobal[], const int nDOF,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElem,
      SparseMatrixType< SANS::DLA::MatrixD< MatrixQ > >& mtxGlobalPDE_p )
  {
    mtxGlobalPDE_p.scatterAdd( mtxPDEElem, elem, elem );
  }
#endif
protected:
  const IntegrandCell& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
};


// Factory function

template<class Surreal, int iParam, class IntegrandCell, class MatrixQP>
JacobianCell_Galerkin_Param_impl<Surreal, iParam, IntegrandCell, MatrixQP>
JacobianCell_Galerkin_Param( const IntegrandCellType<IntegrandCell>& fcn,
                             MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p )
{
  return { fcn.cast(), mtxGlobalPDE_p };
}

} //namespace SANS

#endif  // JACOBIANCELL_GALERKIN_PARAM_H
