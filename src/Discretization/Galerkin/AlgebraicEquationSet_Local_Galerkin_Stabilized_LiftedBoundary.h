// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_LOCAL_GALERKIN_LIFTED_H
#define ALGEBRAICEQUATIONSET_LOCAL_GALERKIN_LIFTED_H

#include "tools/SANSnumerics.h"

#include "Field/FieldTypes.h"
#include "Field/XField.h"

#include "Discretization/ResidualNormType.h"
#include "Discretization/DG/JacobianDetInvResidualNorm.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary.h"
#include "Discretization/DG/JacobianDetInvResidualNorm.h"
#include "BasisFunction/LagrangeDOFMap.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/tools/for_each_GhostBoundaryTraceGroup.h"
#include "Field/tools/for_each_GhostBoundaryTraceGroup_Cell.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Sub.h"

#include "ExtractCGLocalBoundaries.h"

namespace SANS
{

/* This class refers to two types of local solutions/systems, full and sub.
 * The full local solution contains all the solution DOFs in the local mesh:
 *  - qfld DOFs in both cellgroups 0 and 1
 *  - all lgfld DOFs
 *
 * The sub local solution contains only the DOFs that are solved for in the local solve:
 *  - qfld DOFs in only cellgroup 0 (i.e. in main-cells)
 *  - lgfld DOFs of only the main boundary traces (no outer boundary traces)
 */
template <class PhysDim, class TopoDim>
class XField_EdgeLocal;

//----------------------------------------------------------------------------//
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType_>
class AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary :
    public AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, AlgEqSetTraits_Dense, XFieldType_>
{
public:
  typedef XFieldType_ XFieldType; // exposing for debugging

  typedef AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary
          <NDPDEClass, BCNDConvert, BCVector, AlgEqSetTraits_Dense, XFieldType> BaseType;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename BaseType::VectorSizeClass VectorSizeClass;
  typedef typename BaseType::MatrixSizeClass MatrixSizeClass;

  typedef typename BaseType::SystemMatrix SystemMatrix;
  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::BCParams BCParams;
  typedef FieldBundle_Galerkin_LiftedBoundary_Local<PhysDim,TopoDim,ArrayQ> FieldBundle_Local;

  template< class... BCArgs >
  AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary(const XFieldType& xfld,
                                                 Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                                 FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                                 Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                 const NDPDEClass& pde,
                                                 const StabilizationMatrix& stab,
                                                 const QuadratureOrder& quadratureOrder,
                                                 const ResidualNormType& resNormType,
                                                 std::vector<Real>& tol,
                                                 const std::vector<int>& CellGroups,
                                                 PyDict& BCList,
                                                 const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                                 BCArgs&... args )
  : BaseType(xfld, qfld, rfld, lgfld, pde, stab, quadratureOrder, resNormType, tol, CellGroups, BCList, BCBoundaryGroups, args...)
  {

#ifdef WHOLEPATCH
    const Patch patch = Patch::Whole;
#elif defined(INNERPATCH)
    const Patch patch = Patch::Inner;
#else
    const Patch patch = Patch::Broken;
#endif
    // Extract dofs which will be frozen by essential bcs
    extractFreeDOFs(qfld, lgfld, freeDOFs_, patch );

    // std::cout<< "nFreeDOFs = " << freeDOFs_[0].size() << std::endl;
  }

  // Dummy to mask the lack of use of interiorTraceGroups
  template< class... BCArgs >
  AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary(const XFieldType& xfld,
                                                 Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                                 FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                                 Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                 const NDPDEClass& pde,
                                                 const StabilizationMatrix& stab,
                                                 const QuadratureOrder& quadratureOrder,
                                                 const ResidualNormType& resNormType,
                                                 std::vector<Real>& tol,
                                                 const std::vector<int>& CellGroups,
                                                 const std::vector<int>& interiorTraceGroups,
                                                 PyDict& BCList,
                                                 const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                                 BCArgs&... args )
  : AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary(xfld, qfld, lgfld, pde, stab,
                                                   quadratureOrder, resNormType, tol,
                                                   CellGroups, BCList, BCBoundaryGroups, args...){}

  // Field Bundle Constructor for use in hiding the specific AlgEqSet in SolverInterface
  template< class... BCArgs>
  AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary( const XFieldType& xfld,
                                                  FieldBundle_Local& flds,
                                                  std::shared_ptr<Field_CG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                                  const NDPDEClass& pde,
                                                  const StabilizationMatrix& stab,
                                                  const QuadratureOrder& quadratureOrder,
                                                  const ResidualNormType& resNormType,
                                                  const std::vector<Real>& tol,
                                                  const std::vector<int>& CellGroups,
                                                  PyDict& BCList,
                                                  const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                                  BCArgs&&... args )
  : BaseType(xfld, flds, pLiftedQuantityfld, pde, stab, quadratureOrder, resNormType, tol, CellGroups, BCList, BCBoundaryGroups, args...)
  {

#ifdef WHOLEPATCH
    const Patch patch = Patch::Whole;
#elif defined(INNERPATCH)
    const Patch patch = Patch::Inner;
#else
    const Patch patch = Patch::Broken;
#endif

    // Extract dofs which will be frozen by essential bcs
    extractFreeDOFs(flds.qfld, flds.lgfld, freeDOFs_, patch );

    // std::cout<< "nFreeDOFs = " << freeDOFs_[0].size() << std::endl;
  }

  template< class... BCArgs>
  AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary( const XFieldType& xfld,
                                                  FieldBundle_Local& flds,
                                                  std::shared_ptr<Field_CG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                                  const NDPDEClass& pde,
                                                  const StabilizationMatrix& stab,
                                                  const QuadratureOrder& quadratureOrder,
                                                  const ResidualNormType& resNormType,
                                                  const std::vector<Real>& tol,
                                                  const std::vector<int>& CellGroups,
                                                  const std::vector<int>& interiorTraceGroups,
                                                  PyDict& BCList,
                                                  const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                                  BCArgs&&... args )
  : AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary(xfld, flds, pLiftedQuantityfld, pde, stab,
                                                   quadratureOrder, resNormType, tol,
                                                   CellGroups, BCList, BCBoundaryGroups, args...) {}

  virtual ~AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& jac        ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz ) override {}

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  // Returns the vector and matrix sizes needed for the sub-system (local solve system)
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the size of the full vector (i.e. all elements in the local mesh)
  VectorSizeClass fullVectorSize() const;

  //Translates the sub-system vector into a local solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the sub-solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  const XFieldType& paramfld() const { return xfld_; }

  using BaseType::iPDE;
  using BaseType::iBC;
  using BaseType::iq;
  using BaseType::ilg;

  const std::array<std::vector<int>,2>& getFreeDOFs( ) { return freeDOFs_ ; }

protected:
  std::array<std::vector<int>,2> freeDOFs_;

  using BaseType::xfld_;
  using BaseType::qfld_;
  using BaseType::lgfld_;
  using BaseType::quadratureOrder_;
  using BaseType::resNormType_;

};


//Computes the sub-residual for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
residual(SystemVectorView& rsd)
{
  //Compute the full residual vector of the local problem
  SystemVector rsd_full(BaseType::vectorEqSize());
  rsd_full = 0;
  BaseType::residual(rsd_full);

  //Extract the sub residual vector from the full residual, corresponding to the equations that need to be solved
  for (int i = 0; i < rsd.m(); i++)
    subVectorPlus(rsd_full[i], freeDOFs_[i], 1., rsd[i]);
}

//Computes the sub-Jacobian for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
jacobian(SystemMatrixView& jac)
{
  //Compute the full Jacobian of the local problem
  SystemMatrix jac_full(BaseType::matrixSize());
  jac_full = 0;
  BaseType::template jacobian<SystemMatrix&>(jac_full, quadratureOrder_);

  //Extract the sub Jacobian from the full Jacobian, containing only the equations that need to be solved
  for (int i = 0; i < jac_full.m(); i++)
    for (int j = 0; j < jac_full.n(); j++)
      subMatrixPlus(jac_full(i,j), freeDOFs_[i], freeDOFs_[j], 1., jac(i,j));

 // SLA::WriteMatrixMarketFile(jac(0,0), "tmp/jac_edge_local.mtx");
 // SLA::WriteMatrixMarketFile(jac(0,1), "tmp/jac_edge_01.mtx");
 // SLA::WriteMatrixMarketFile(jac(1,0), "tmp/jac_edge_10.mtx");
 // SLA::WriteMatrixMarketFile(jac(1,1), "tmp/jac_edge_11.mtx");
}

//Evaluate Residual Norm
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
std::vector<std::vector<Real>>
AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
residualNorm( const SystemVectorView& rsd ) const
{
  const int nMon = this->pde_.nMonitor();

  DLA::VectorD<Real> rsdPDEtmp(nMon);
  DLA::VectorD<Real> rsdBCtmp(nMon);

  rsdPDEtmp = 0;
  rsdBCtmp = 0;

  std::vector<std::vector<Real>> rsdNorm(this->nResidNorm(), std::vector<Real>(nMon, 0));
  std::vector<KahanSum<Real>> rsdNormKahan(nMon, 0);

  //PDE residual norm
  if (resNormType_ == ResidualNorm_L2)
  {
    for (int n = 0; n < rsd[iPDE].m(); n++)
    {
      this->pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt((Real)rsdNormKahan[j]);
  }
  else
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary::residualNorm - Unknown residual norm type!");

  //BC residual norm
  for (int n = 0; n < rsd[iBC].m(); n++)
  {
    this->pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

  return rsdNorm;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
typename AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::vectorEqSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  return { (int)freeDOFs_[iPDE].size(),
           (int)freeDOFs_[iBC].size() };
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
typename AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::vectorStateSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  return { (int)freeDOFs_[iPDE].size(),
           (int)freeDOFs_[iBC].size() };
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
typename AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");
  static_assert(iq == 0,"");
  static_assert(ilg == 1,"");

  // jacobian nonzero pattern
  //
  //           q  lg
  //   PDE     X   X
  //   BC      X   0

  const int nPDE = (int)freeDOFs_[iPDE].size();
  const int nBC = (int)freeDOFs_[iBC].size();

  return {{ { nPDE, nPDE }, { nPDE, nBC } },
          { {  nBC, nPDE }, {  nBC, nBC } }};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
typename AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
fullVectorSize() const
{
  return BaseType::vectorEqSize();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  SANS_ASSERT( (int)freeDOFs_[iPDE].size() == q[iq].m() );
  for (std::size_t k = 0; k < freeDOFs_[iPDE].size(); k++)
    qfld_.DOF(freeDOFs_[iPDE][k]) = q[iq][k];

  SANS_ASSERT( (int)freeDOFs_[iBC].size() == q[ilg].m() );
  for (std::size_t k = 0; k < freeDOFs_[iBC].size(); k++)
    lgfld_.DOF(freeDOFs_[iBC][k]) = q[ilg][k];

}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  SANS_ASSERT( (int)freeDOFs_[iPDE].size() == q[iq].m() );
  for (std::size_t k = 0; k < freeDOFs_[iPDE].size(); k++)
    q[iq][k] = qfld_.DOF(freeDOFs_[iPDE][k]);

  SANS_ASSERT( (int)freeDOFs_[iBC].size() == q[ilg].m() );
  for (std::size_t k = 0; k < freeDOFs_[iBC].size(); k++)
    q[ilg][k] = lgfld_.DOF(freeDOFs_[iBC][k]);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_LOCAL_GALERKIN_LIFTED_H
