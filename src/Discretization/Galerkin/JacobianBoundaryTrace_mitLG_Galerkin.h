// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_MITLG_GALERKIN_H
#define JACOBIANBOUNDARYTRACE_MITLG_GALERKIN_H

// jacobian boundary-trace integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/GroupElementType.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_mitLG_Galerkin_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_mitLG_Galerkin_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianBoundaryTrace_mitLG_Galerkin_impl(const IntegrandBoundaryTrace& fcn,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_lg_(mtxGlobalPDE_lg),
    mtxGlobalBC_q_(mtxGlobalBC_q), mtxGlobalBC_lg_(mtxGlobalBC_lg) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& lgfld )
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_lg_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_lg_.n() == lgfld.nDOFpossessed() + lgfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalBC_q_.m() == lgfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalBC_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalBC_lg_.m() == lgfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalBC_lg_.n() == lgfld.nDOFpossessed() + lgfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class TupleFieldType>
  void
  integrate(const int cellGroupGlobalL,
            const typename TupleFieldType                 ::template FieldCellGroupType<TopologyL>& tuplefldCellL,
            const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
            const int traceGroupGlobal,
            const typename TupleFieldType::XFieldType     ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
            const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
            int quadratureorder )
  {
    typedef typename TupleFieldType                 ::template FieldCellGroupType<TopologyL> TupleFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename TupleFieldCellGroupTypeL::template ElementType<>        ElementTupleFieldClassL;
    typedef typename QFieldCellGroupTypeL    ::template ElementType<Surreal> ElementQFieldClassL;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

    typedef typename TupleFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // element field variables
    ElementTupleFieldClassL xfldElemL( tuplefldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );
    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // DOF counts
    int nDOFL = qfldElemL.nDOF();
    int nDOFTrace = lgfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapQDOFGlobalL(nDOFL,-1);
    std::vector<int> mapDOFGlobalTrace(nDOFTrace,-1);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal> integral(quadratureorder, nDOFL, nDOFTrace);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
    std::vector<ArrayQSurreal> rsdBCTrace( nDOFTrace );

    // element jacobians
    MatrixElemClass mtxPDEElemL_qElemL(nDOFL, nDOFL);
    MatrixElemClass mtxPDEElemL_lgTrace(nDOFL, nDOFTrace);
    MatrixElemClass mtxBC_qElemL(nDOFTrace, nDOFL);
    MatrixElemClass mtxBC_lgTrace(nDOFTrace, nDOFTrace);

    // number of simultaneous derivatives per functor call
    const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobian to zero
      mtxPDEElemL_qElemL = 0;
      mtxPDEElemL_lgTrace = 0;
      mtxBC_qElemL = 0;
      mtxBC_lgTrace = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // right element, hub trace or field trace
      int elemR;
      if ( xfldTrace.getGroupRightType() == eHubTraceGroup )
        elemR = xfldTrace.getElementRight( elem );
      else
        elemR = elem;

      // copy global grid/solution DOFs to element
      tuplefldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );

      lgfldTrace.getElement( lgfldElemTrace, elemR );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(nDOFL + nDOFTrace); nchunk += nDeriv)
      {
        // associate derivative slots with solution variables

        int slot;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        for (int j = 0; j < nDOFTrace; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(lgfldElemTrace.DOF(j), n).deriv(k) = 0;

            slot = nEqn*nDOFL + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(lgfldElemTrace.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element

        // reset PDE residuals to zero
        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        for (int n = 0; n < nDOFTrace; n++)
          rsdBCTrace[n] = 0;

        integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                                 xfldElemL, qfldElemL,
                                 lgfldElemTrace),
                  get<-1>(xfldElemTrace),
                  rsdPDEElemL.data(), nDOFL,
                  rsdBCTrace.data(), nDOFTrace );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElemL_qElemL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFTrace; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxBC_qElemL(i,j), m,n) = DLA::index(rsdBCTrace[i], m).deriv(slot - nchunk);
            }
          }
        }

        for (int j = 0; j < nDOFTrace; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*nDOFL + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElemL_lgTrace(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFTrace; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxBC_lgTrace(i,j), m,n) = DLA::index(rsdBCTrace[i], m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global

      ScatterAdd<TopologyTrace, TopologyL, PhysDim, TopoDim>(
          qfldCellL, lgfldTrace,
          elemL, elemR,
          mapQDOFGlobalL.data(), nDOFL,
          mapDOFGlobalTrace.data(), nDOFTrace,
          mtxPDEElemL_qElemL,
          mtxPDEElemL_lgTrace,
          mtxBC_qElemL,
          mtxBC_lgTrace,
          mtxGlobalPDE_q_,
          mtxGlobalPDE_lg_,
          mtxGlobalBC_q_,
          mtxGlobalBC_lg_ );
    }
  }

protected:

//----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class PhysDim, class TopoDim,
            template <class> class SparseMatrixType>
  void
  ScatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int elemL, const int elemR,
      int mapDOFGlobalL[], const int nDOFL,
      int mapDOFGlobalTrace[], const int nDOFTrace,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_lgTrace,
      SANS::DLA::MatrixD<MatrixQ>& mtxBC_qElemL,
      SANS::DLA::MatrixD<MatrixQ>& mtxBC_lgTrace,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_lg,
      SparseMatrixType<MatrixQ>& mtxGlobalBC_q,
      SparseMatrixType<MatrixQ>& mtxGlobalBC_lg )
  {
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL, nDOFL );

    lgfldTrace.associativity( elemR ).getGlobalMapping( mapDOFGlobalTrace, nDOFTrace );

    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, mapDOFGlobalL, nDOFL, mapDOFGlobalL, nDOFL );
    mtxGlobalPDE_lg.scatterAdd( mtxPDEElemL_lgTrace, mapDOFGlobalL, nDOFL, mapDOFGlobalTrace, nDOFTrace );

    mtxGlobalBC_q.scatterAdd( mtxBC_qElemL, mapDOFGlobalTrace, nDOFTrace, mapDOFGlobalL, nDOFL );
    mtxGlobalBC_lg.scatterAdd( mtxBC_lgTrace, mapDOFGlobalTrace, nDOFTrace, mapDOFGlobalTrace, nDOFTrace );
  }


//----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class PhysDim, class TopoDim,
            template <class> class SparseMatrixType>
  void
  ScatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int elemL, const int elem,
      int mapDOFGlobalL[], const int nDOFL,
      int mapDOFGlobalTrace[], const int nDOFTrace,
      DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      DLA::MatrixD<MatrixQ>& mtxPDEElemL_lgTrace,
      DLA::MatrixD<MatrixQ>& mtxBC_qElemL,
      DLA::MatrixD<MatrixQ>& mtxBC_lgTrace,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_lg,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalBC_q,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalBC_lg )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
    mtxGlobalPDE_lg.scatterAdd( mtxPDEElemL_lgTrace, elemL, elem );

    mtxGlobalBC_q.scatterAdd( mtxBC_qElemL, elem, elemL );
    mtxGlobalBC_lg.scatterAdd( mtxBC_lgTrace, elem, elem );
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class MatrixQ>
JacobianBoundaryTrace_mitLG_Galerkin_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_mitLG_Galerkin( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg )
{
  return {fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalBC_q, mtxGlobalBC_lg};
}


}

#endif  // JACOBIANBOUNDARYTRACE_MITLG_GALERKIN_H
