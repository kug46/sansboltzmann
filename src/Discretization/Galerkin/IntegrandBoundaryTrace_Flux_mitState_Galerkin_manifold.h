// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_GALERKIN_MANIFOLD_H_
#define INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_GALERKIN_MANIFOLD_H_

// boundary integrand operators: specific for IBL2D

#include <ostream>
#include <vector>

#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/TraceToCellRefCoord.h"
#include "Discretization/Integrand_Type.h"

#include "Integrand_Galerkin_fwd.h"

#include "Stabilization_Nitsche.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE


template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, Galerkin_manifold> :
  public IntegrandBoundaryTraceType<IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, Galerkin_manifold>>
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Flux_mitState Category;
  typedef Galerkin_manifold DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(const PDE& pde,
                         const BCBase& bc,
                         const std::vector<int>& BoundaryGroups,
                         const StabilizationNitsche& stab) :
    pde_(pde),
    bc_(bc),
    BoundaryGroups_(BoundaryGroups),
    stab_(stab)
    {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted(const PDE& pde, const BCBase& bc,
                  const ElementXFieldTrace& xfldElemTrace,
                  const CanonicalTraceToCell& canonicalTrace,
                  const ElementParam& paramfldElem,
                  const ElementQFieldCell& qfldElem,
                  const StabilizationNitsche& stab) :
      pde_(pde),
      bc_(bc),
      xfldElemTrace_(xfldElemTrace),
      canonicalTrace_(canonicalTrace),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter in tuple
      qfldElem_(qfldElem),
      paramfldElem_(paramfldElem),
      nDOF_(qfldElem_.nDOF()),
      phi_( new Real[nDOF_] ),
      gradphi_( new VectorX[nDOF_] ),
      stab_(stab)
    {
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // element trace integrand
    template <class Ti>
    void operator()(const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrand[], int neqn) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrand, neqn);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    const StabilizationNitsche& stab_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell&                                canonicalTrace,
            const ElementParam&                                        paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology     >& qfldElem) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(
               pde_, bc_,
               xfldElemTrace, canonicalTrace,
               paramfldElem, qfldElem, stab_);
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const StabilizationNitsche& stab_;
};

template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, Galerkin_manifold>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrand[], int neqn) const
{
  SANS_ASSERT(neqn == nDOF_);

  typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes;  // manifold local axes type

  ParamT paramL;                  // Elemental parameters (such as grid coordinates and distance functions)

  VectorX N;                      // unit trace normal vector (points outward from element)
  VectorX e01L;                   // basis direction vector
  LocalAxes e0L;                  // basis direction vector
  DLA::VectorS<PhysDim::D, LocalAxes> e0L_X;    // manifold local axes tangential cartesian gradient

  ArrayQ<T> qI;                   // solution
  VectorArrayQ<T> gradqI;         // solution gradient

  QuadPointCellType sRef;         // reference-element coordinates (s,t) wrt left element

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval(canonicalTrace_, sRefTrace, sRef);

  static_assert(TopoDimCell::D == 1, "Only TopoD1 element is implemented so far."); // TODO: generalize

  // Elemental parameters (includes X)
  paramfldElem_.eval(sRef, paramL);

  // basis vectors
  xfldElem_.unitTangent(sRef, e01L);
  e0L[0] = e01L;

  xfldElem_.evalUnitTangentGradient(sRef, e0L_X);

  // unit normal: points out of domain
  traceUnitNormal(xfldElem_, sRef, xfldElemTrace_, sRefTrace, N);

  // basis value, gradient
  qfldElem_.evalBasis(sRef, phi_, nDOF_);
  xfldElem_.evalBasisGradient(sRef, qfldElem_, gradphi_, nDOF_);

  // solution value, gradient
  qfldElem_.evalFromBasis(phi_, nDOF_, qI);
  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis(gradphi_, nDOF_, gradqI);
  }
  else
  {
    gradqI = 0;
  }

  // BC data
  ArrayQ<T> qB;
  bc.state(paramL, N, qI, qB);

  for (int k = 0; k < neqn; ++k)
    integrand[k] = 0;

  // PDE residual: weak form boundary integral

  // TODO: This should call the BC for the flux
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    ArrayQ<Ti> Fn = 0;  // normal flux

    // advective flux
    if (pde_.hasFluxAdvective())
    {
      if (bc.useUpwind())
      {
        LocalAxes e0R = e0L;            // basis direction vector
        ParamT paramR = paramL;
        pde_.fluxAdvectiveUpwind(paramL, paramR, e0L, e0R, qI, qB, N, Fn);
      }
      else
      {
        VectorArrayQ<Ti> F = 0;
        pde_.fluxAdvective(paramL, e0L, qB, F);
        Fn += dot(N,F);
      }
    }

    // viscous flux
    if (pde_.hasFluxViscous())
    {
      SANS_DEVELOPER_EXCEPTION("Not implemented.");
#if 0 // not yet implemented
      VectorArrayQ<T> Fv = 0;

      //Evaluate viscous flux using boundary state qB and interior state gradient gradq
      pde_.fluxViscous(X, qB, gradqI, Fv);

      Fn += dot(N,Fv); //add normal component of viscous flux
#endif
    }

    for (int k = 0; k < neqn; ++k)
      integrand[k] += phi_[k]*Fn;
  }
}

} // namespace SANS

#endif /* INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_GALERKIN_MANIFOLD_H_ */
