// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_OUTPUT_GALERKIN_H
#define INTEGRANDBOUNDARYTRACE_OUTPUT_GALERKIN_H

// boundary output functional for DGBR2

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Discretization/Integrand_Type.h"

#include "pde/OutputCategory.h"
#include "pde/call_derived_functional.h"

#include "Integrand_Galerkin_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary output integrand

template <class PDE_, class NDOutputVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::Functional>, Galerkin> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::Functional>, Galerkin> >
{
public:
  typedef PDE_ PDE;
  typedef OutputCategory::Functional Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = typename PDE::template ArrayQ<T>;

  explicit IntegrandBoundaryTrace( const OutputBase& outputFcn,
                                   const std::vector<int>& BoundaryGroups )
    : outputFcn_(outputFcn), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,
                    class ElementParam>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>      , TopoDimCell , TopologyL    > ElementQFieldL;
    typedef Element<ArrayQ<T>      , TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const OutputBase& outputFcn,
             const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementParam& paramfldElem,
             const ElementQFieldL& qfldElem ) :
             outputFcn_(outputFcn),
             xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
             paramfldElem_(paramfldElem), xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
             qfldElem_(qfldElem)
    {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFElem() const { return qfldElem_.nDOF(); }

    // element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayJ<Ti>& integrand ) const
    {
      QuadPointCellType sRefCell;

      TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTrace_, sRefTrace, sRefCell );

      ArrayQ<T> qL = 0;
      VectorArrayQ<T> gradqL; // gradient

      VectorX X, N = 0;
      ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

      // unit normal: points out of domain
      xfldElemTrace_.unitNormal( sRefTrace, N );

      // evaluate parameters
      paramfldElem_.eval( sRefCell, param );

      // solution value, gradient
      qfldElem_.eval( sRefCell, qL );
      xfldElem_.evalGradient( sRefCell, qfldElem_, gradqL );

      // cast the outputFcn to the derived class and call it's functor
      call_derived_functional<NDOutputVector>(outputFcn_, param, N, qL, gradqL, integrand);
    }

  protected:
    const OutputBase& outputFcn_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementParam& paramfldElem_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell, class ElementParam, class BCIntegrandBoundaryTrace>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell, ElementParam >
  integrand(const BCIntegrandBoundaryTrace& fcnBC,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // XField must be the last parameter
            const Element<ArrayQ<T>       , TopoDimCell , TopologyCell >& qfldElem) const
  {
    return {outputFcn_, xfldElemTrace, canonicalTrace, paramfldElem, qfldElem};
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell, class ElementParam, class BCIntegrandBoundaryTrace>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell, ElementParam >
  integrand(const BCIntegrandBoundaryTrace& fcnBC,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // XField must be the last parameter
            const Element<ArrayQ<T>       , TopoDimCell , TopologyCell >& qfldElem,
            const Element<ArrayQ<T>       , TopoDimTrace, TopologyTrace>& lgfldElem) const
  {
    return {outputFcn_, xfldElemTrace, canonicalTrace, paramfldElem, qfldElem};
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell, class ElementParam>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell, ElementParam >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // XField must be the last parameter
            const Element<ArrayQ<T>       , TopoDimCell , TopologyCell >& qfldElem) const
  {
    return {outputFcn_, xfldElemTrace, canonicalTrace, paramfldElem, qfldElem};
  }

private:
  const OutputBase& outputFcn_;
  const std::vector<int> BoundaryGroups_;
};

}

#endif  // INTEGRANDBOUNDARYTRACE_OUTPUT_GALERKIN_H
