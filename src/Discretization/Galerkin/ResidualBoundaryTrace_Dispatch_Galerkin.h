// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_DISPATCH_GALERKIN_H
#define RESIDUALBOUNDARYTRACE_DISPATCH_GALERKIN_H

// boundary-trace integral residual functions

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "ResidualBoundaryTrace_mitLG_Galerkin.h"
#include "ResidualBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "ResidualBoundaryTrace_Galerkin.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups_HubTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Field trace variables, e.g. Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
class ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl
{
public:
  ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQR>& rsdPDEGlobal,
      Vector<ArrayQR>& rsdBCGlobal )
    : xfld_(xfld), qfld_(qfld), lgfld_(lgfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal_, rsdBCGlobal_),
        xfld_, qfld_, lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQR>& rsdPDEGlobal_;
  Vector<ArrayQR>& rsdBCGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, ArrayQR>
ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin(const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                              const int* quadratureorder, int ngroup,
                                              Vector<ArrayQR>& rsdPDEGlobal,
                                              Vector<ArrayQR>& rsdBCGlobal)
{
  static_assert( DLA::VectorSize<ArrayQ>::M == DLA::VectorSize<ArrayQR>::M, "These should be the same size.");

  return {xfld, qfld, lgfld, quadratureorder, ngroup, rsdPDEGlobal, rsdBCGlobal};
}


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Hub trace variables, e.g. IBL wake matching
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
class ResidualBoundaryTrace_HubTrace_Dispatch_Galerkin_impl
{
public:
  ResidualBoundaryTrace_HubTrace_Dispatch_Galerkin_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& hbfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQR>& rsdPDEGlobal,
      Vector<ArrayQR>& rsdHTGlobal )
    : xfld_(xfld), qfld_(qfld), hbfld_(hbfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal), rsdHTGlobal_(rsdHTGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_HubTrace<TopoDim>::integrate(
        ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal_, rsdHTGlobal_),
        xfld_, qfld_, hbfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& hbfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQR>& rsdPDEGlobal_;
  Vector<ArrayQR>& rsdHTGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
ResidualBoundaryTrace_HubTrace_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, ArrayQR>
ResidualBoundaryTrace_HubTrace_Dispatch_Galerkin(const XFieldType& xfld,
                                                 const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                 const Field<PhysDim, TopoDim, ArrayQ>& hbfld,
                                                 const int* quadratureorder, int ngroup,
                                                 Vector<ArrayQR>& rsdPDEGlobal,
                                                 Vector<ArrayQR>& rsdHTGlobal )
{
  static_assert( DLA::VectorSize<ArrayQ>::M == DLA::VectorSize<ArrayQR>::M, "These should be the same size.");

  return {xfld, qfld, hbfld, quadratureorder, ngroup, rsdPDEGlobal, rsdHTGlobal};
}


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
class ResidualBoundaryTrace_Dispatch_Galerkin_impl
{
public:
  ResidualBoundaryTrace_Dispatch_Galerkin_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQR>& rsdPDEGlobal)
    : xfld_(xfld), qfld_(qfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ResidualBoundaryTrace_Galerkin(fcn, rsdPDEGlobal_), xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQR>& rsdPDEGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
ResidualBoundaryTrace_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, ArrayQR>
ResidualBoundaryTrace_Dispatch_Galerkin(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const int* quadratureorder, int ngroup,
                                             Vector<ArrayQR>& rsdPDEGlobal)
{
  static_assert( DLA::VectorSize<ArrayQ>::M == DLA::VectorSize<ArrayQR>::M, "These should be the same size.");

  return { xfld, qfld, quadratureorder, ngroup, rsdPDEGlobal };
}



//---------------------------------------------------------------------------//
//
// Dispatch class for BC's
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
class FillResidualField_Dispatch_Galerkin_impl
{
public:
  FillResidualField_Dispatch_Galerkin_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      std::unique_ptr<Field<PhysDim,TopoDim,ArrayQ>>& up_resfld )
    : xfld_(xfld), qfld_(qfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      up_resfld_(up_resfld)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        FillResidualFieldBoundaryTrace_Galerkin(fcn, up_resfld_), xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  std::unique_ptr<Field<PhysDim,TopoDim,ArrayQ>>& up_resfld_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
FillResidualField_Dispatch_Galerkin_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, ArrayQR>
FillResidualField_Dispatch_Galerkin(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const int* quadratureorder, int ngroup,
                                             std::unique_ptr<Field<PhysDim,TopoDim,ArrayQ>>& up_resfld)
{
  static_assert( DLA::VectorSize<ArrayQ>::M == DLA::VectorSize<ArrayQR>::M, "These should be the same size.");

  return { xfld, qfld, quadratureorder, ngroup, up_resfld };
}

}

#endif //RESIDUALBOUNDARYTRACE_DISPATCH_GALERKIN_H
