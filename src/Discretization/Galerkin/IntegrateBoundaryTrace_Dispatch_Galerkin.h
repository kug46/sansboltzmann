// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATEBOUNDARYTRACE_DISPATCH_GALERKIN_H
#define INTEGRATEBOUNDARYTRACE_DISPATCH_GALERKIN_H

#include "Python/PyDict.h"

#include <map>

#include "Discretization/IntegrateBoundaryTrace_Dispatch.h"
#include "Integrand_Galerkin_fwd.h"
#include "Stabilization_Nitsche.h"

namespace SANS
{

//===========================================================================//
template<class PDEND, template<class,class> class BCNDConvert, class BCVector, class DiscTag>
struct IntegrateBoundaryTrace_Dispatch_Galerkin : public IntegrateBoundaryTrace_Dispatch<PDEND, BCNDConvert, BCVector, DiscTag>
{
  typedef IntegrateBoundaryTrace_Dispatch<PDEND, BCNDConvert, BCVector, DiscTag> BaseType;

  typedef typename BaseType::BCNDVectorsmitFTcat BCNDVectorsmitFTcat;
  typedef typename BaseType::BCNDVectorsmitHTcat BCNDVectorsmitHTcat;
  typedef typename BaseType::BCNDVectorssansFTcat BCNDVectorssansFTcat;

  typedef typename BaseType::IntegrandBoundaryTracemitFTVector IntegrandBoundaryTracemitFTVector;
  typedef typename BaseType::IntegrandBoundaryTracesansFTVector IntegrandBoundaryTracesansFTVector;

  typedef typename BaseType::def_IntegrandBoundaryTraceOp def_IntegrandBoundaryTraceOp;

//---------------------------------------------------------------------------//
  // A function for generating a std::vector of IntegrandBoundaryTrace instances
  IntegrateBoundaryTrace_Dispatch_Galerkin( const PDEND& pde,
                                            const PyDict& BCList,
                                            const std::map< std::string, std::shared_ptr<BCBase> >& BCs,
                                            const std::map< std::string, std::vector<int> >& BoundaryGroups,
                                            const StabilizationNitsche& stab)
  {
    // Extract the keys from the dictionary
    std::vector<std::string> keys = BCList.stringKeys();
    std::vector<int> BoundaryGroups_key;

    for (std::size_t i = 0; i < keys.size(); ++i)
    {
      // Use a try...catch because then we can report the problem
      try
      {
        BoundaryGroups_key = BoundaryGroups.at(keys[i]);
      }
      catch (const std::out_of_range&)
      {
        //Create a more useful error message
        this->BCKeyError(keys[i], BoundaryGroups);
      }

      // Create the next boundary trace type
      std::shared_ptr<IntegrandBoundaryTraceBase> integrand =
          detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorsmitFTcat >::type,
                                                     typename boost::mpl::end< BCNDVectorsmitFTcat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key );

      if ( integrand == NULL )
        integrand =
            detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorsmitHTcat >::type,
                                                       typename boost::mpl::end< BCNDVectorsmitHTcat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key);

      if ( integrand == NULL )
        integrand =
            detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorssansFTcat >::type,
                                                       typename boost::mpl::end< BCNDVectorssansFTcat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key, stab);

      if ( integrand == NULL )
        SANS_DEVELOPER_EXCEPTION("Should not be possible to get here.");

      integrandBoundaryTraces_.push_back(integrand);
    }

    // Just in case the vector is longer than needed
    integrandBoundaryTraces_.shrink_to_fit();
  }

//---------------------------------------------------------------------------//
  // Expose the generic dispatch with just mitFT and sansFT options
  using BaseType::dispatch;

protected:
  using BaseType::integrandBoundaryTraces_;

};

}

#endif //INTEGRATEBOUNDARYTRACE_DISPATCH_GALERKIN_H
