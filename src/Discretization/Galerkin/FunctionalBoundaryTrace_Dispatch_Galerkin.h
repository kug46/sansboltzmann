// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTINOALBOUNDARYTRACE_DISPATCH_GALERKIN_H
#define FUNCTINOALBOUNDARYTRACE_DISPATCH_GALERKIN_H

// boundary-trace integral residual functions


#include "FunctionalBoundaryTrace_FieldTrace_Galerkin.h"
#include "FunctionalBoundaryTrace_Galerkin.h"
#include "FunctionalBoundaryTrace_WeightedResidual_Galerkin.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace (e.g. with Lagrange multipliers)
//
//---------------------------------------------------------------------------//
template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class ArrayJ>
class FunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl
{
public:
  FunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      ArrayJ& functional )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), lgfld_(lgfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      functional_(functional)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        FunctionalBoundaryTrace_FieldTrace_Galerkin( fcnOutput_, fcn.cast(), functional_ ),
        xfld_, qfld_, lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  ArrayJ& functional_;
};

// Factory function

template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class ArrayJ>
FunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin_impl<FunctionalIntegrand, XFieldType, PhysDim, TopoDim, ArrayQ, ArrayJ>
FunctionalBoundaryTrace_FieldTrace_Dispatch_Galerkin(const FunctionalIntegrand& fcnOutput,
                                                     const FieldType<XFieldType>& xfld,
                                                     const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                     const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                     const int* quadratureorder, int ngroup,
                                                     ArrayJ& functional )
{
  return {fcnOutput, xfld.cast(), qfld, lgfld, quadratureorder, ngroup, functional};
}

//---------------------------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class ArrayJ>
class FunctionalBoundaryTrace_Dispatch_Galerkin_impl
{
public:
  FunctionalBoundaryTrace_Dispatch_Galerkin_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      ArrayJ& functional )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      functional_(functional)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
      FunctionalBoundaryTrace_WeightedResidual_Galerkin( fcnOutput_, fcn.cast(), functional_ ),
      xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  ArrayJ& functional_;
};

// Factory function

template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class ArrayJ>
FunctionalBoundaryTrace_Dispatch_Galerkin_impl<FunctionalIntegrand, XFieldType, PhysDim, TopoDim, ArrayQ, ArrayJ>
FunctionalBoundaryTrace_Dispatch_Galerkin(const FunctionalIntegrand& fcnOutput,
                                          const FieldType<XFieldType>& xfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                          const int* quadratureorder, int ngroup,
                                          ArrayJ& functional )
{
  return {fcnOutput, xfld.cast(), qfld, quadratureorder, ngroup, functional};
}


}

#endif //FUNCTINOALBOUNDARYTRACE_DISPATCH_GALERKIN_H
