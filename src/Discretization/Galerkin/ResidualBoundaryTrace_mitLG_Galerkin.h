// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_MITLG_GALERKIN_H
#define RESIDUALBOUNDARYTRACE_MITLG_GALERKIN_H

// boundary-trace integral residual functions

#include "Field/Field.h"
#include "Field/GroupElementType.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//
template<class IntegrandBoundaryTrace, template<class> class VectorRsdGlobal, class TR>
class ResidualBoundaryTrace_mitLG_Galerkin_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_mitLG_Galerkin_impl<IntegrandBoundaryTrace, VectorRsdGlobal, TR> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<TR> ArrayQR;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_mitLG_Galerkin_impl( const IntegrandBoundaryTrace& fcn,
                                             VectorRsdGlobal<ArrayQR>& rsdPDEGlobal,
                                             VectorRsdGlobal<ArrayQR>& rsdBCGlobal ) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& lgfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( rsdBCGlobal_.m() == lgfld.nDOFpossessed() );
  }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;
    typedef          ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceClass;

    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

    // number of integrals evaluated per element
    const int nIntegrandL  = qfldElemL.nDOF();
    const int nIntegrandBC = lgfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL, -1 );
    std::vector<int> mapDOFGlobalBC( nIntegrandBC, -1 );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQR, ArrayQR> integral(quadratureorder, nIntegrandL, nIntegrandBC);

    // element integrand/residuals
    std::vector<ArrayQR> rsdPDEElemL( nIntegrandL );
    std::vector<ArrayQR> rsdBCElem( nIntegrandBC );

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // right element, hub trace or field trace
      int elemR;
      if ( xfldTrace.getGroupRightType() == eHubTraceGroup )
        elemR = xfldTrace.getElementRight( elem );
      else
        elemR = elem;

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );

      lgfldTrace.getElement( lgfldElemTrace, elemR );

       // set the residuals to zero
      for (int n = 0; n < nIntegrandL; n++)
        rsdPDEElemL[n] = 0;

      for (int n = 0; n < nIntegrandBC; n++)
        rsdBCElem[n] = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL,
                               lgfldElemTrace),
                get<-1>(xfldElemTrace),
                rsdPDEElemL.data(), nIntegrandL,
                rsdBCElem.data(), nIntegrandBC );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );
      lgfldTrace.associativity( elemR ).getGlobalMapping( mapDOFGlobalBC.data(), nIntegrandBC );

      int nGlobal;
      for (int n = 0; n < nIntegrandL; n++)
      {
        nGlobal = mapDOFGlobalL[n];
        rsdPDEGlobal_[nGlobal] += rsdPDEElemL[n];
      }
      for (int n = 0; n < nIntegrandBC; n++)
      {
        nGlobal = mapDOFGlobalBC[n];
        rsdBCGlobal_[nGlobal] += rsdBCElem[n];
      }
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  VectorRsdGlobal<ArrayQR>& rsdPDEGlobal_;
  VectorRsdGlobal<ArrayQR>& rsdBCGlobal_;
};

// Factory function
template<class IntegrandBoundaryTrace, template<class> class VectorRsdGlobal, class ArrayQ>
ResidualBoundaryTrace_mitLG_Galerkin_impl<IntegrandBoundaryTrace, VectorRsdGlobal, typename Scalar<ArrayQ>::type>
ResidualBoundaryTrace_mitLG_Galerkin( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                      VectorRsdGlobal<ArrayQ>& rsdPDEGlobal,
                                      VectorRsdGlobal<ArrayQ>& rsdBCGlobal)
{
  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same<ArrayQ, typename IntegrandBoundaryTrace::template ArrayQ<T> >::value, "These should be the same.");
  return ResidualBoundaryTrace_mitLG_Galerkin_impl<IntegrandBoundaryTrace, VectorRsdGlobal, T>(fcn.cast(), rsdPDEGlobal, rsdBCGlobal);
}

}

#endif  // RESIDUALBOUNDARYTRACE_MITLG_GALERKIN_H
