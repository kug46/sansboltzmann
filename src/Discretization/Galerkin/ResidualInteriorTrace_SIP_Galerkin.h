// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALINTERIORTRACE_SIP_GALERKIN_H
#define RESIDUALINTERIORTRACE_SIP_GALERKIN_H

// HDG interior-trace integral residual functions

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "tools/Tuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class IntegrandInteriorTrace, template<class> class Vector>
class ResidualInteriorTrace_SIP_Galerkin_impl :
    public GroupIntegralInteriorTraceType< ResidualInteriorTrace_SIP_Galerkin_impl<IntegrandInteriorTrace, Vector> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualInteriorTrace_SIP_Galerkin_impl( const IntegrandInteriorTrace& fcn,
                                           Vector<ArrayQ>& rsdPDEGlobal)  :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // if ((cellGroupGlobalL != 0) && (cellGroupGlobalR != 0) ) return;
    // if ((cellGroupGlobalL == 0) && (cellGroupGlobalR == 0) ) return;
    SANS_ASSERT_MSG( (cellGroupGlobalL == 0) && (cellGroupGlobalR == 1), "should only be evaluated from 0 -> 1" );

    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();

    const int nIntegrandL = nDOFL;
    const int nIntegrandR = nDOFR;

    // trace element integral
    typedef ArrayQ CellIntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType>
      integralTraceL(quadratureorder, nIntegrandL);

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType>
      integralTraceR(quadratureorder, nIntegrandR);

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL, -1 );
    std::vector<int> mapDOFGlobalR( nIntegrandR, -1 );

    // element integrand/residuals
    DLA::VectorD<CellIntegrandType> rsdPDEElemL( nIntegrandL );
    DLA::VectorD<CellIntegrandType> rsdPDEElemR( nIntegrandR );

    const int nelem = xfldTrace.nElem();
    // loop over trace elements in list
    for (int elem = 0; elem < nelem; elem++)
    {

      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      xfldCellL.getElement( xfldElemL, elemL );
      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellL.getElement( qfldElemL, elemL );
      qfldCellR.getElement( qfldElemR, elemR );

      // if ((cellGroupGlobalL == 0) && (cellGroupGlobalR == 1) )
      // {
        //The fixed cell is to the left of the trace

        for (int n = 0; n < nIntegrandL; n++) rsdPDEElemL[n] = 0;

        auto integrandFunctor = fcn_.integrand(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                               xfldElemL, qfldElemL,
                                               xfldElemR, qfldElemR);

        integralTraceL( integrandFunctor, xfldElemTrace, rsdPDEElemL.data(), nIntegrandL);

        qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );

        int nGlobal;
        for (int n = 0; n < nIntegrandL; n++)
        {
          nGlobal = mapDOFGlobalL[n];
          rsdPDEGlobal_[nGlobal] += rsdPDEElemL[n];
        }
      // }
      // else if ((cellGroupGlobalL == 1) && (cellGroupGlobalR == 0) )
      // {
      //   SANS_DEVELOPER_EXCEPTION("Should never reach here, left group should always be 0");
      //   // for (int n = 0; n < nIntegrandR; n++) rsdPDEElemR[n] = 0;
      //   //
      //   // auto integrandFunctor = fcn_.integrand(xfldElemTrace, canonicalTraceR, canonicalTraceL,
      //   //                                        xfldElemR, qfldElemR,
      //   //                                        xfldElemL, qfldElemL);
      //   //
      //   // integralTraceR( integrandFunctor, xfldElemTrace, rsdPDEElemR.data(), nIntegrandR);
      //   //
      //   // qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nIntegrandR );
      //   //
      //   // int nGlobal;
      //   // for (int n = 0; n < nIntegrandR; n++)
      //   // {
      //   //   nGlobal = mapDOFGlobalR[n];
      //   //   rsdPDEGlobal_[nGlobal] += rsdPDEElemR[n];
      //   // }
      //
      // }

    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
};


// Factory function
template<class IntegrandInteriorTrace, class ArrayQ, template<class> class Vector>
ResidualInteriorTrace_SIP_Galerkin_impl<IntegrandInteriorTrace, Vector>
ResidualInteriorTrace_SIP_Galerkin( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                    Vector<ArrayQ>& rsdPDEGlobal)
{
  static_assert( std::is_same< ArrayQ, typename IntegrandInteriorTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return {fcn.cast(), rsdPDEGlobal};
}

}

#endif  // RESIDUALINTERIORTRACE_SIP_GALERKIN_H
