// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_DIRICHLET_SANSLG_GALERKIN_H
#define INTEGRANDBOUNDARYTRACE_DIRICHLET_SANSLG_GALERKIN_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Integrand_Galerkin_fwd.h"
#include "Stabilization_Nitsche.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE/BC
//
// conventional (sans Lagrange) formulation for Dirichlet BC

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin> :
  public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Dirichlet_sansLG Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  static const int N = PDE::N;

  //constructor for DG interface
  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups)
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups) {}

  //constructor for CG interface
  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const StabilizationNitsche& stab)
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimCell , Topology      > ElementXFieldCell;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template<class Ti>
    using IntegrandType = ArrayQ<Ti>;

    BasisWeighted( const PDE& pde, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem ) :
                   pde_(pde), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem),
                   paramfldElem_( paramfldElem ),
                   nDOF_(qfldElem_.nDOF()),
                   phi_( new Real[nDOF_] ),
                   gradphi_( new VectorX[nDOF_] )
    {
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType<Ti> integrand[], int neqn ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrand, neqn);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<ArrayQ<Tw>, TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<Real,       TopoDimCell, Topology> ElementEFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type IntegrandType;

    FieldWeighted( const PDE& pde, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementWFieldCell& wfldElem,
                   const ElementEFieldCell& efldElem ) :
                   pde_(pde), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem), wfldElem_(wfldElem), efldElem_(efldElem),
                   paramfldElem_( paramfldElem ),
                   nDOF_(qfldElem_.nDOF()),
                   nPhi_(efldElem_.nDOF()),
                   phi_( new Real[nPhi_] ),
                   weight_( new ArrayQ<Tw>[nPhi_] )
    {
      // Nothing
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] weight_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // element trace integrand
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType integrand[], const int nphi ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrand, nphi);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType integrand[], const int nphi ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementEFieldCell& efldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_, nPhi_;
    mutable Real *phi_;
    mutable ArrayQ<Tw> *weight_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTrace,
             paramfldElem, qfldElem };
  }

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell, class Topology, class ElementParam>
  FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<Tq>, TopoDimCell, Topology>& qfldElem,
            const Element<ArrayQ<Tw>, TopoDimCell, Topology>& wfldElem,
            const Element<Real, TopoDimCell, Topology>& efldElem) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTrace,
             paramfldElem,
             qfldElem, wfldElem, efldElem };
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
};


template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell, class Topology, class ElementParam>
template <class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType<Ti> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT param;             // Elemental parameters (such as grid coordinates and distance functions)

  VectorX Nrm;              // unit normal (points out of domain)

  ArrayQ<T> q;              // solution
  VectorArrayQ<T> gradq;    // gradient

  QuadPointCellType sRef;    // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, param );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, Nrm);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // PDE residual: weak form boundary integral

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;     // PDE flux

    // advective flux
    pde_.fluxAdvective( param, q, F );

    // viscous flux
    pde_.fluxViscous( param, q, gradq, F );

    ArrayQ<Ti> Fn = dot(Nrm,F);
    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*Fn;
  }

  // PDE residual: weighted strong-form BC

  ArrayQ<Ti> rsdBC = 0;
  MatrixQ<Ti> wghtBC = 0;
  bc.strongBC( param, Nrm, q, gradq, rsdBC );
  bc.weightBC( param, Nrm, q, gradq, wghtBC );

  ArrayQ<Ti> term = wghtBC*rsdBC;
  for (int k = 0; k < neqn; k++)
    integrand[k] -= phi_[k]*term;
}

template <class PDE, class NDBCVector>
template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class Topology, class ElementParam >
template <class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin>::
FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType integrand[], const int nphi ) const
{
  SANS_ASSERT( nphi == nPhi_ );
  SANS_DEVELOPER_EXCEPTION( "IntegrandBoundaryTrace<Dirichlet_sansLG,Galerkin>::FieldWeighted not implemented" );
#if 0

  ParamT param;             // Elemental parameters (such as grid coordinates and distance functions)

  VectorX Nrm;                // unit normal (points out of domain)

  ArrayQ<Tq> q;              // interior solution
  VectorArrayQ<Tq> gradq;    // interior gradient

  ArrayQ<Tw> w;               // weight

  QuadPointCellType sRef;    // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates (includes X)
  paramfldElem_.eval( sRef, param );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, Nrm);

  // basis value, gradient
  qfldElem_.eval( sRef, q );
  if ( needsSolutionGradient )
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );
  else
    gradq = 0;

  wfldElem_.eval( sRef, w );

  // PDE residual: weak form boundary integral
  if ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() )
  {
    VectorArrayQ<Tq> F = 0;     // PDE flux

    // advective flux
    pde_.fluxAdvective( param, q, F );

    // viscous flux
    pde_.fluxViscous( param, q, gradq, F );

    ArrayQ<Tq> Fn = dot(Nrm,F);

    integrand += dot(w,Fn);
  }

  // PDE residual: weighted strong-form BC

  ArrayQ<Tq> rsdBC = 0;
  MatrixQ<Tq> wghtBC = 0;
  bc.strongBC( param, Nrm, q, gradq, rsdBC );
  bc.weightBC( param, Nrm, q, gradq, wghtBC );

  ArrayQ<Tq> term = wghtBC*rsdBC;
  integrand -= dot(w,term);

#endif
}

}

#endif  // INTEGRANDBOUNDARYTRACE_DIRICHLET_SANSLG_GALERKIN_H
