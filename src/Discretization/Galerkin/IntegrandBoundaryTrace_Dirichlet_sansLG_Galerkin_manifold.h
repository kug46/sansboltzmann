// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_DIRICHLET_SANSLG_GALERKIN_MANIFOLD_H
#define INTEGRANDBOUNDARYTRACE_DIRICHLET_SANSLG_GALERKIN_MANIFOLD_H

// boundary integrand operators

#include "tools/call_with_derived.h"

#include "Integrand_Galerkin_fwd.h"

#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"
#include "Stabilization_Nitsche.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE/BC
//
// conventional (sans Lagrange) formulation for Dirichlet BC

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin_manifold> :
  public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin_manifold> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Dirichlet_sansLG Category;
  typedef Galerkin_manifold DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // gradient solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;           // matrices

  static const int N = PDE::N;

  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const StabilizationNitsche& stab ) :
      pde_(pde),
      bc_(bc),
      BoundaryGroups_(BoundaryGroups) {}

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;

    typedef typename ElementParam::T ParamT;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandType;

    BasisWeighted( const PDE& pde,
                   const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam&   paramfldElem,
                   const ElementQFieldCell& qfldElem ) :
                   pde_(pde), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)),
                   qfldElem_(qfldElem),
                   paramfldElem_(paramfldElem),
                   nDOF_(qfldElem_.nDOF()),
                   phi_( new Real[nDOF_]),
                   gradphi_( new VectorX[nDOF_] )
    {
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // 2D element line integrand
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType integrand[], int neqn ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrand, neqn);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType integrand[], int neqn ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementParam&      paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam&                                        paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem ) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(
               pde_, bc_,
               xfldElemTrace, canonicalTrace,
               paramfldElem, qfldElem );
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
};


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam>
template<class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, Galerkin_manifold>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  Topology, ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes;  // manifold local axes type

  ParamT paramL;             // Elemental parameters (such as grid coordinates and distance functions)

  VectorX Nrm;              // unit normal (points out of domain)
  VectorX e01L;             // basis direction vector
  LocalAxes e0L;            // basis direction vector
  DLA::VectorS<PhysDim::D, LocalAxes> e0L_X;    // manifold local axes tangential cartesian gradient

  ArrayQ<T> q;            // solution
  VectorArrayQ<T> gradq;  // gradient

  QuadPointCellType sRefL; // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRefL );

  static_assert( TopoDimCell::D == 1, "Only TopoD1 element is implemented so far." ); // TODO: generalize

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRefL, paramL );

  // physical coordinates
  xfldElem_.unitTangent( sRefL, e01L );
  e0L[0] = e01L;

  xfldElem_.evalUnitTangentGradient( sRefL, e0L_X );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRefL, xfldElemTrace_, sRefTrace, Nrm);

  // basis value, gradient
  qfldElem_.evalBasis( sRefL, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRefL, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
  }
  else
  {
    gradq = 0;
  }

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // PDE residual: weak form boundary integral

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<T> F = 0;     // PDE flux

    // advective flux
    pde_.fluxAdvective( paramL, e0L, e0L_X, q, F );

    // viscous flux
    pde_.fluxViscous( paramL, q, gradq, F );

    ArrayQ<T> Fn = dot(Nrm,F);
    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*Fn;
  }

  // PDE residual: weighted strong-form BC

  ArrayQ<T> rsdBC = 0;
  MatrixQ<T> wghtBC = 0;
  bc.strongBC( paramL, Nrm, q, gradq, rsdBC );
  bc.weightBC( paramL, e0L, Nrm, q, gradq, wghtBC );

  ArrayQ<T> term = wghtBC*rsdBC;
  for (int k = 0; k < neqn; k++)
    integrand[k] -= phi_[k]*term;
}

}

#endif // INTEGRANDBOUNDARYTRACE_DIRICHLET_SANSLG_GALERKIN_MANIFOLD_H
