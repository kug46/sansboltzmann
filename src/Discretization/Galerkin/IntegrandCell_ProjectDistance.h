// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_PROJECTDISTANCE_H
#define INTEGRANDCELL_PROJECTDISTANCE_H

// cell integrand operator: global cell L2 projection of a distance function

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "Field/Element/Element.h"
#include "Field/Tuple/ElementTuple.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template <class PhysDim_>
class IntegrandCell_ProjectDistance : public IntegrandCellType< IntegrandCell_ProjectDistance<PhysDim_> >
{
public:
  typedef PhysDim_ PhysDim;

  template< class Z = Real >
  using ArrayQ = Z;

  template< class Z = Real >
  using MatrixQ = Z;

  template <class Z = Real>
  using VectorArrayQ = DLA::VectorS<PhysDim::D,Z>; // solution gradient arrays

  IntegrandCell_ProjectDistance( const Real P, const std::vector<int>& CellGroups )
    : P_(P), cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class TopoDim, class Topology, class ElementParamType>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<Real, TopoDim, Topology> ElementFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted(  const ElementParamType& paramfldElem,
                    const ElementFieldType& distfldElem,
                    const Real P) :
                      xfldElem_(get<-1>(paramfldElem)),
                      qfldElem_(get<0>(paramfldElem)),
                      distfldElem_(distfldElem),
                      P_(P),
                      nDOF_( qfldElem_.nDOF() ),
                      phi_( new Real[nDOF_] ),
                      gradphi_( new VectorX[nDOF_] )
    {
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element residual integrand
    void operator()( const QuadPointType& sRef, ArrayQ<> integrand[], int neqn ) const;

    void operator()( const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<> >& rsdElem ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<>>& mtxMass ) const;

  protected:
    const ElementXFieldType& xfldElem_;
    const ElementFieldType& qfldElem_;
    const ElementFieldType& distfldElem_;
    const Real P_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class TopoDim, class Topology, class ElementParamType>
  BasisWeighted<TopoDim, Topology, ElementParamType>
  integrand(const ElementParamType& paramfldElem,
            const Element<Real, TopoDim, Topology>& distfldElem) const
  {
    return {paramfldElem, distfldElem, P_};
  }

protected:
  const Real P_;
  const std::vector<int> cellGroups_;
};

//
// solving \int phi*(q - f) = 0
//

template <class PhysDim>
template <class TopoDim, class Topology, class ElementParamType>
void
IntegrandCell_ProjectDistance<PhysDim>::
BasisWeighted<TopoDim,Topology,ElementParamType>::
operator()(const QuadPointType& sRef, ArrayQ<> integrand[], int neqn) const
{
  SANS_ASSERT(neqn == nDOF_);

  Real q;
  VectorX gradq;

  qfldElem_.eval( sRef, q );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  // basis value
  Real dist;
  distfldElem_.evalBasis( sRef, phi_, nDOF_ );
  distfldElem_.evalFromBasis( phi_, nDOF_, dist );

  Real absgradq = sqrt(dot(gradq, gradq));

  // Distance equation
  Real f = pow(P_/(P_-1)*q + pow(absgradq,P_), (P_-1)/P_) - pow(absgradq, P_-1);

  for (int k = 0; k < neqn; k++)
    integrand[k] = phi_[k]*(dist - f);
}

template <class PhysDim>
template <class TopoDim, class Topology, class ElementParamType>
void
IntegrandCell_ProjectDistance<PhysDim>::
BasisWeighted<TopoDim,Topology,ElementParamType>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<> >& rsdElem) const
{
  SANS_ASSERT(rsdElem.m() == nDOF_);

  Real q;
  VectorX gradq;

  qfldElem_.eval( sRef, q );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
  qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  // basis value
  Real dist;
  distfldElem_.evalBasis( sRef, phi_, nDOF_ );
  distfldElem_.evalFromBasis( phi_, nDOF_, dist );

  Real absgradq = sqrt(dot(gradq, gradq));

  // Distance equation
  Real f = pow(P_/(P_-1)*q + pow(absgradq,P_), (P_-1)/P_) - pow(absgradq, P_-1);

  for (int k = 0; k < nDOF_; k++)
    rsdElem[k] += dJ*phi_[k]*(dist - f);
}

template <class PhysDim>
template <class TopoDim, class Topology, class ElementParamType>
void
IntegrandCell_ProjectDistance<PhysDim>::
BasisWeighted<TopoDim,Topology,ElementParamType>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<Real>& mtxMass) const
{
  SANS_ASSERT(mtxMass.m() == nDOF_);
  SANS_ASSERT(mtxMass.n() == nDOF_);

  // basis value
  distfldElem_.evalBasis( sRef, phi_, nDOF_ );

  Real proj_q; // temporary storage

  // add to the mass matrix
  for (int i = 0; i < nDOF_; i++)
  {
    proj_q = dJ*phi_[i];
    for (int j = 0; j < nDOF_; j++)
      mtxMass(i,j) += proj_q*phi_[j];
  }
}

}

#endif  // INTEGRANDCELL_PROJECTDISTANCE_H
