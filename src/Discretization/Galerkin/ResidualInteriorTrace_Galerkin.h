// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALINTERIORTRACE_GALERKIN_H
#define RESIDUALINTERIORTRACE_GALERKIN_H

// interior-trace integral residual functions

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin interior-trace integral
//

template<class IntegrandInteriorTrace, template<class> class Vector>
class ResidualInteriorTrace_Galerkin_impl :
    public GroupIntegralInteriorTraceType< ResidualInteriorTrace_Galerkin_impl<IntegrandInteriorTrace, Vector> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualInteriorTrace_Galerkin_impl( const IntegrandInteriorTrace& fcn,
                                       Vector<ArrayQ>& rsdPDEGlobal) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), comm_rank_(0) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );

#ifdef SANS_MPI
    comm_rank_ = qfld.comm()->rank();

    // MPI ranks assume a DG space below
    SANS_ASSERT(qfld.spaceType() == SpaceType::Discontinuous);
#endif
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType                     ::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType                     ::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;


    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandR = qfldElemR.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL, -1 );
    std::vector<int> mapDOFGlobalR( nIntegrandR, -1 );

    // element integrand/residuals
    std::vector<ArrayQ> rsdElemL( nIntegrandL );
    std::vector<ArrayQ> rsdElemR( nIntegrandR );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR);

    // loop over elements within the trace group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; ++elem)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );

      const int rankL = qfldElemL.rank();
      const int rankR = qfldElemR.rank();

      // nothing to do if both elements are ghost elements
      if (rankL != comm_rank_ && rankR != comm_rank_) continue;

      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      auto integrandFunctor = fcn_.integrand(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                             xfldElemL, qfldElemL,
                                             xfldElemR, qfldElemR);
      // Create a functor only once for speed.  Note that the parameters are passed into the functor as const references,
      // but their values can still be modified by agents other than this functor.

      for (int n = 0; n < nIntegrandL; n++)
        rsdElemL[n] = 0;

      for (int n = 0; n < nIntegrandR; n++)
        rsdElemR[n] = 0;

      integral(integrandFunctor, xfldElemTrace,
               rsdElemL.data(), nIntegrandL,
               rsdElemR.data(), nIntegrandR );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );
      qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nIntegrandR );

      int nGlobal;
      if ( rankL == comm_rank_ )
        for (int n = 0; n < nIntegrandL; n++)
        {
          nGlobal = mapDOFGlobalL[n];
          rsdPDEGlobal_[nGlobal] += rsdElemL[n];
        }

      if ( rankR == comm_rank_ )
        for (int n = 0; n < nIntegrandR; n++)
        {
          nGlobal = mapDOFGlobalR[n];
          rsdPDEGlobal_[nGlobal] += rsdElemR[n];
        }
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandInteriorTrace, class ArrayQ, template<class> class Vector>
ResidualInteriorTrace_Galerkin_impl<IntegrandInteriorTrace, Vector>
ResidualInteriorTrace_Galerkin( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                Vector<ArrayQ>& rsdPDEGlobal)
{
  static_assert( std::is_same<ArrayQ, typename IntegrandInteriorTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return {fcn.cast(), rsdPDEGlobal};
}

} //namespace SANS

#endif  // RESIDUALINTERIORTRACE_GALERKIN_H
