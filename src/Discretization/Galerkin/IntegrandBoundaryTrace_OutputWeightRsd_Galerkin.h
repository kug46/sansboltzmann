// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_OUTPUTWEIGHTRSD_GALERKIN_H
#define INTEGRANDBOUNDARYTRACE_OUTPUTWEIGHTRSD_GALERKIN_H

// boundary output functional for Galerkin

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/call_derived_functor.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Discretization/Integrand_Type.h"

#include "pde/OutputCategory.h"

#include "Integrand_Galerkin_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary output integrand

template <class PDE_, class NDOutputVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::WeightedResidual>, Galerkin> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::WeightedResidual>, Galerkin> >
{
public:
  typedef PDE_ PDE;
  typedef OutputCategory::WeightedResidual Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = typename PDE::template ArrayQ<T>;

  explicit IntegrandBoundaryTrace( const OutputBase& outputWeights,
                                   const std::vector<int>& BoundaryGroups )
    : outputWeights_(outputWeights), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,
                    class ElementParam, class BCIntegrandBoundaryTrace>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>   , TopoDimCell , TopologyL    > ElementQFieldL;
    typedef Element<ArrayQ<T>   , TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef Element<ArrayQ<Real>, TopoDimCell , TopologyL    > ElementWFieldL;
    typedef Element<Real        , TopoDimCell , TopologyL    > ElementEFieldL;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;

    typedef typename BCIntegrandBoundaryTrace::template FieldWeighted<T,Real,TopoDimTrace,TopologyTrace,
                                                                             TopoDimCell,TopologyL,ElementParam> BCFieldWeight;

    Functor( const BCIntegrandBoundaryTrace& fcnBC,
             const OutputBase& outputWeights,
             const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementParam& paramfldElem,
             const ElementQFieldL& qfldElem ) :
               fcnBC_(fcnBC),
               outputWeights_(outputWeights),
               callWeights_(outputWeights_),
               xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
               paramfldElem_(paramfldElem), xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
               qfldElem_(qfldElem),
               wfldElem_( 0, BasisFunctionCategory_Legendre ),
               efldElem_( 0, BasisFunctionCategory_Legendre ),
               fwBC_( fcnBC.integrand(xfldElemTrace_, canonicalTrace_, paramfldElem_,
                                      qfldElem_, wfldElem_, efldElem_) )
    {}

    Functor( Functor&& f ) :
               fcnBC_(f.fcnBC_),
               outputWeights_(f.outputWeights_),
               callWeights_(outputWeights_),
               xfldElemTrace_(f.xfldElemTrace_), canonicalTrace_(f.canonicalTrace_),
               paramfldElem_(f.paramfldElem_), xfldElem_(f.xfldElem_),
               qfldElem_(f.qfldElem_),
               wfldElem_( 0, BasisFunctionCategory_Legendre ),
               efldElem_( 0, BasisFunctionCategory_Legendre ),
               fwBC_( fcnBC_.integrand(xfldElemTrace_, canonicalTrace_, paramfldElem_,
                                       qfldElem_, wfldElem_, efldElem_) )
    {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFElem() const { return qfldElem_.nDOF(); }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, ArrayJ<T>& integrand ) const
    {
      typename BCFieldWeight::IntegrandType rsdBC[1] = {0};

      VectorX X;
      xfldElemTrace_.eval(sRefTrace, X);

      // Compute the desired weight at this quadrature point
      callWeights_(X, wfldElem_.DOF(0));

      fwBC_( sRefTrace, rsdBC, 1 );

      integrand = rsdBC[0];
    }

  protected:
    const BCIntegrandBoundaryTrace& fcnBC_;
    const OutputBase& outputWeights_;
    const call_derived_functor<NDOutputVector,const OutputBase> callWeights_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementParam& paramfldElem_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;

    mutable ElementWFieldL wfldElem_;
    mutable ElementEFieldL efldElem_;

    const BCFieldWeight fwBC_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell, class ElementParam, class BCIntegrandBoundaryTrace>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell, ElementParam, BCIntegrandBoundaryTrace >
  integrand( const BCIntegrandBoundaryTrace& fcnBC,
             const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementParam& paramfldElem, // XField must be the last parameter
             const Element<ArrayQ<T>    , TopoDimCell , TopologyCell >& qfldElem) const
  {
    return {fcnBC, outputWeights_, xfldElemTrace, canonicalTrace, paramfldElem, qfldElem};
  }


  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,
                    class ElementParam, class BCIntegrandBoundaryTrace>
  class Functor_FieldTrace
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>   , TopoDimCell , TopologyL    > ElementQFieldL;
    typedef Element<ArrayQ<T>   , TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef Element<ArrayQ<Real>, TopoDimCell , TopologyL    > ElementWFieldL;
    typedef Element<ArrayQ<Real>, TopoDimTrace, TopologyTrace> ElementWFieldTrace;

    typedef Element<Real        , TopoDimCell , TopologyL    > ElementEFieldL;
    typedef Element<Real        , TopoDimTrace, TopologyTrace> ElementEFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;

    typedef typename BCIntegrandBoundaryTrace::template FieldWeighted<T,Real,TopoDimTrace,TopologyTrace,
                                                                             TopoDimCell,TopologyL,ElementParam> BCFieldWeight;

    Functor_FieldTrace( const BCIntegrandBoundaryTrace& fcnBC,
                        const OutputBase& outputWeights,
                        const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                        const ElementParam& paramfldElem,
                        const ElementQFieldL& qfldElem,
                        const ElementQFieldTrace& lgfldElem ) :
                          outputWeights_(outputWeights),
                          callWeights_(outputWeights_),
                          xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                          paramfldElem_(paramfldElem), xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                          qfldElem_(qfldElem), lgfldElem_(lgfldElem),
                          wfldElem_( 0, BasisFunctionCategory_Legendre ),
                          mufldElem_( 0, BasisFunctionCategory_Legendre ),
                          efldElem_( 0, BasisFunctionCategory_Legendre ),
                          eBfldElem_( 0, BasisFunctionCategory_Legendre ),
                          fwBC_( fcnBC.integrand(xfldElemTrace_, canonicalTrace_, paramfldElem_,
                                                 qfldElem_, wfldElem_, efldElem_,
                                                 lgfldElem_, mufldElem_, eBfldElem_) )
    {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFElem() const { return qfldElem_.nDOF(); }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, ArrayJ<T>& integrand ) const
    {
      typename BCFieldWeight::IntegrandCellType rsdPDE[1] = {0};
      typename BCFieldWeight::IntegrandTraceType rsdBC[1] = {0};

      VectorX X;
      xfldElemTrace_.eval(sRefTrace, X);

      // Compute the desired weight at this quadrature point
      callWeights_(X, wfldElem_.DOF(0));
      mufldElem_.DOF(0) = 0;

      fwBC_( sRefTrace, rsdPDE, 1, rsdBC, 1 );

      integrand = rsdPDE[0];
    }

  protected:
    const OutputBase& outputWeights_;
    const call_derived_functor<NDOutputVector,const OutputBase> callWeights_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementParam& paramfldElem_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
    const ElementQFieldTrace& lgfldElem_;

    mutable ElementWFieldL wfldElem_;
    mutable ElementWFieldTrace mufldElem_;

    mutable ElementEFieldL efldElem_;
    mutable ElementEFieldTrace eBfldElem_;

    const BCFieldWeight fwBC_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell, class ElementParam, class BCIntegrandBoundaryTrace>
  Functor_FieldTrace<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell, ElementParam, BCIntegrandBoundaryTrace >
  integrand( const BCIntegrandBoundaryTrace& fcnBC,
             const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementParam& paramfldElem, // XField must be the last parameter
             const Element<ArrayQ<T>    , TopoDimCell , TopologyCell >& qfldElem,
             const Element<ArrayQ<T>    , TopoDimTrace, TopologyTrace>& lgfldElem) const
  {
    return {fcnBC, outputWeights_, xfldElemTrace, canonicalTrace, paramfldElem, qfldElem, lgfldElem};
  }

private:
  const OutputBase& outputWeights_;
  const std::vector<int> BoundaryGroups_;
};

}

#endif  // INTEGRANDBOUNDARYTRACE_OUTPUTWEIGHTRSD_GALERKIN_H
