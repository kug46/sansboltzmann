// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_PTC_H
#define INTEGRANDCELL_GALERKIN_PTC_H

// cell integrand operators: pseudo-time continuation (PTC)

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "Field/Element/Element.h"
#include "Field/Element/ElementSequence.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: PTC
//
// integrandPDE = phi*( U_{n} - U_{n-1} )/dt
//
// where
//   phi              basis function
//   U_n(x,y)         conservative solution vector at timestep n
//   dt               is computed from a CFL number

template <class PDE>
class IntegrandCell_Galerkin_PTC : public IntegrandCellType< IntegrandCell_Galerkin_PTC<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobian

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin_PTC( const PDE& pde, const std::vector<int>& CellGroups) :
    pde_(pde), cellGroups_(CellGroups)
  {
    // PTC only applies to PDE class with conservative flux defined, otherwise there will be no time-dependent term
    SANS_ASSERT(pde_.hasFluxAdvectiveTime());
  }

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<Real, TopoDim, Topology> ElementTFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementSequence<ArrayQ<Real>, TopoDim, Topology> ElementQFieldVecType;

    typedef typename ElementXFieldType::RefCoordType RefCoordType;
    typedef typename ElementXFieldType::VectorX VectorX;

    typedef typename ElementParam::ElementTypeL ElementTypeL;
    typedef typename ElementTypeL::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    template<class Z>
    using IntegrandType = ArrayQ<Z>;

    BasisWeighted( const PDE& pde,
                   const ElementParam& paramfldElem, // assume paramfldElem is an ElementTuple like (..., fld, xfld, dtifld)
                   const ElementQFieldType& qfldElem,
                   const ElementQFieldVecType& qfldElemPast) :
      pde_(pde),
      paramfldElem_(paramfldElem.left()), // Remove the inverse time step field; remaining element = (..., fld, xfld)
      dtifldElem_(get<-1>(paramfldElem)), // dtiField must be the last parameter
      xfldElem_(get<-2>(paramfldElem)), // XField must be the 2nd to last parameter
      qfldElem_(qfldElem), qfldElemPast_(qfldElemPast),
      nDOF_(qfldElem_.nDOF() ),
      phi_( new Real[nDOF_] )
    {
      SANS_ASSERT( qfldElemPast_.nElem() == 1 ); //TODO: it's hard coded to take 1 paramfldElem here instead of a sequence
    }

    BasisWeighted( BasisWeighted&& bw ) :
      pde_(bw.pde_),
      paramfldElem_(bw.paramfldElem_),
      dtifldElem_(bw.dtifldElem_),
      xfldElem_(bw.xfldElem_),
      qfldElem_(bw.qfldElem_), qfldElemPast_(bw.qfldElemPast_),
      nDOF_(bw.nDOF_ ),
      phi_( bw.phi_ )
    {
      bw.phi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, IntegrandType<Ti> integrand[], int neqn ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

  protected:
    template<class Tq, class Ti>
    void weightedIntegrand( const ParamT& param,
                            const Real dti,
                            const ArrayQ<Tq>& q,
                            const ArrayQ<Real>& ftPast,
                            ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const ElementTypeL& paramfldElem_;
    const ElementTFieldType& dtifldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldVecType& qfldElemPast_;

    const int nDOF_;
    mutable Real *phi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const ElementSequence<ArrayQ<Real>, TopoDim, Topology>& qfldElemPast) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, qfldElemPast);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_PTC<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin_PTC<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()( const QuadPointType& sRef, IntegrandType<Ti> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  Real dti;                    // inverse time step

  ArrayQ<T> q;                 // solution
  ArrayQ<Real> qPast, ftPast;   // previous solution

  // evaluate basis fcn
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // get the inverse time step
  dtifldElem_.eval( sRef, dti );

  // evaluate solution at different timesteps
  qfldElem_.evalFromBasis( phi_, nDOF_, q);
  qfldElemPast_[0].evalFromBasis( phi_, nDOF_, qPast );

  ftPast = 0;
  pde_.fluxAdvectiveTime(param, qPast, ftPast); // TODO: IBL needs paramPast as well

  // compute the residual
  weightedIntegrand( param, dti, q, ftPast, integrand, neqn);
}


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_PTC<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxPDEElem.m() == nDOF_);
  SANS_ASSERT(mtxPDEElem.n() == nDOF_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  Real dti;                    // inverse time step

  ArrayQ<Real> q;                    // solution
  ArrayQ<SurrealClass> qSurreal = 0; // solution
  ArrayQ<Real> qPast, ftPast;   // previous solution

  MatrixQ<Real> PDE_q = 0; // temporary storage

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // evaluate solution at different timesteps
  qfldElem_.evalFromBasis( phi_, nDOF_, q);
  qfldElemPast_[0].evalFromBasis( phi_, nDOF_, qPast );
  qSurreal = q;

  // get the inverse time step
  dtifldElem_.eval( sRef, dti );

  // convert the previous time step to conservative variables
  ftPast = 0;
  pde_.fluxAdvectiveTime(param, qPast, ftPast);

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    weightedIntegrand( param, dti, qSurreal, ftPast, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;
  } // nchunk
}

// Cell integrand
//
// integrand = phi*du/dt
//
// where
//   phi                basis function
//   U_n(x,y)           conservative solution vector at timestep n
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Tq, class Ti>
void
IntegrandCell_Galerkin_PTC<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
weightedIntegrand( const ParamT& param,
                   const Real dti,
                   const ArrayQ<Tq>& q,
                   const ArrayQ<Real>& ftPast,
                   ArrayQ<Ti> integrand[], int neqn ) const
{
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  ArrayQ<Tq> ft = 0;
  pde_.fluxAdvectiveTime(param, q, ft);

  // PTC backward Euler term
  ArrayQ<Tq> dudt = (ft - ftPast)*dti;

  for (int k=0; k < neqn; k++)
    integrand[k] += phi_[k]*dudt;
}

}

#endif  // INTEGRANDCELL_GALERKIN_PTC_H
