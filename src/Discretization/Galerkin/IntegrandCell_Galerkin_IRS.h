// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_IRS_H
#define INTEGRANDCELL_GALERKIN_IRS_H

// cell integrand operators: pseudo-time continuation (IRS)

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "Field/Element/Element.h"
#include "Field/Element/ElementSequence.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: IRS
//
// integrandPDE = phi*( U_{n} - U_{n-1} )/dt
//
// where
//   phi              basis function
//   U_n(x,y)         conservative solution vector at timestep n
//   dt               is computed from a CFL number

template <class PDE>
class IntegrandCell_Galerkin_IRS : public IntegrandCellType< IntegrandCell_Galerkin_IRS<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobian

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin_IRS( const PDE& pde, const std::vector<int>& CellGroups) :
    pde_(pde), cellGroups_(CellGroups)
  {
    // IRS only applies to PDE class with conservative flux defined, otherwise there will be no time-dependent term
    SANS_ASSERT(pde_.hasFluxAdvectiveTime());
  }

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<Real, TopoDim, Topology> ElementTFieldType;
    typedef Element<Real, TopoDim, Topology> ElementHFieldType; // Not using tensor version yet
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementSequence<ArrayQ<Real>, TopoDim, Topology> ElementQFieldVecType;

    typedef typename ElementXFieldType::RefCoordType RefCoordType;
    typedef typename ElementXFieldType::VectorX VectorX;

    typedef typename ElementParam::ElementTypeL::ElementTypeL ElementTypeL;
    typedef typename ElementTypeL::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    template<class Z>
    using IntegrandType = ArrayQ<Z>;

    BasisWeighted( const PDE& pde,
                   const ElementParam& paramfldElem, // assume paramfldElem is an ElementTuple like (..., fld, xfld, hfld, dtifld)
                   const ElementQFieldType& qfldElem,
                   const ElementQFieldVecType& qfldElemPast) :
      pde_(pde),
      paramfldElem_(paramfldElem.left().left()), // Remove the inverse time step field and hfld; remaining element = (..., fld, xfld)
      dtifldElem_(get<-1>(paramfldElem)), // dtiField must be the last parameter
      hfldElem_(get<-2>(paramfldElem)), // HField must be the 2nd to last parameter
      xfldElem_(get<-3>(paramfldElem)), // XField must be the 3rd to last parameter
      qfldElem_(qfldElem), qfldElemPast_(qfldElemPast),
      nDOF_(qfldElem_.nDOF() ),
      phi_( new Real[nDOF_] ),
      gradphi_(nDOF_)
    {
      SANS_ASSERT( qfldElemPast_.nElem() == 1 ); //TODO: it's hard coded to take 1 paramfldElem here instead of a sequence
    }

    ~BasisWeighted()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, IntegrandType<Ti> integrand[], int neqn ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

  protected:
    template<class Tq, class Ti>
    void weightedIntegrand( const ParamT& param,
                            const Real dti,
                            const Real hi,
                            const VectorArrayQ<Tq>& gradu,
                            const VectorArrayQ<Real>& graduPast,
                            ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const ElementTypeL& paramfldElem_;
    const ElementTFieldType& dtifldElem_;
    const ElementHFieldType& hfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldVecType& qfldElemPast_;

    const int nDOF_;
    mutable Real *phi_;
    mutable std::vector<VectorX> gradphi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const ElementSequence<ArrayQ<Real>, TopoDim, Topology>& qfldElemPast) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, qfldElemPast);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_IRS<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin_IRS<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()( const QuadPointType& sRef, IntegrandType<Ti> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  Real dti;                    // inverse time step
  Real h;                      // element size

  ArrayQ<T> q;                           // solution
  ArrayQ<Real> qPast, uPast;             // previous solution
  VectorArrayQ<T> gradq, gradu;          // gradient
  VectorArrayQ<T> gradqPast, graduPast;  // gradient

  // evaluate basis, and gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_.data(), nDOF_ );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // get the inverse time step
  dtifldElem_.eval( sRef, dti );
  // get the hfld size
  hfldElem_.eval( sRef, h );

  // evaluate solution at different timesteps
  qfldElem_.evalFromBasis( phi_, nDOF_, q);
  qfldElem_.evalFromBasis( gradphi_.data(), nDOF_, gradq );
  qfldElemPast_[0].evalFromBasis( phi_, nDOF_, qPast );
  qfldElemPast_[0].evalFromBasis( gradphi_.data(), nDOF_, gradqPast );

  uPast = 0;
  pde_.masterState(param, qPast, uPast); // TODO: IBL needs paramPast as well

  // Get graduPast
  graduPast = 0;
  MatrixQ<T> dudq = 0;
  pde_.jacobianMasterState(param, qPast, dudq);
//  graduPast = gradqPast*dudq;
    for ( int i = 0; i < PhysDim::D; i++ )
    {
      graduPast[i] = dudq*gradqPast[i];
    }


  // Get gradu (current)
  gradu = 0;
  dudq = 0;
  pde_.jacobianMasterState(param, q, dudq);
//  gradu = gradq*dudq;
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    gradu[i] = dudq*gradq[i];
  }

  // compute the residual
  weightedIntegrand( param, dti, h, gradu, graduPast, integrand, neqn);
}


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_IRS<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxPDEElem.m() == nDOF_);
  SANS_ASSERT(mtxPDEElem.n() == nDOF_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  Real dti;                    // inverse time step
  Real h;                      // element size

  ArrayQ<Real> q;                    // solution
  ArrayQ<SurrealClass> qSurreal = 0; // solution
  ArrayQ<Real> qPast, uPast;   // previous solution
  VectorArrayQ<SurrealClass> gradqSurreal = 0, graduSurreal = 0;          // gradient
  VectorArrayQ<Real> gradq;
  VectorArrayQ<Real> gradqPast, graduPast;  // gradient

  MatrixQ<Real> PDE_gradq = 0; // temporary storage

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_.data(), nDOF_ );

  // evaluate solution at different timesteps
  qfldElem_.evalFromBasis( phi_, nDOF_, q);
  qfldElem_.evalFromBasis( gradphi_.data(), nDOF_, gradq );
  qfldElemPast_[0].evalFromBasis( phi_, nDOF_, qPast );
  qfldElemPast_[0].evalFromBasis( gradphi_.data(), nDOF_, gradqPast );

  qSurreal = q;
  gradqSurreal = gradq;

  // get the inverse time step
  dtifldElem_.eval( sRef, dti );
  // get the hfld size
  hfldElem_.eval( sRef, h );

  // convert the previous time step to conservative variables
  uPast = 0;
  pde_.masterState(param, qPast, uPast);

  // Get graduPast
  graduPast = 0;
  MatrixQ<T> dudq2 = 0;
  pde_.jacobianMasterState(param, qPast, dudq2);
//  graduPast = gradqPast*dudq2;
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    graduPast[i] = dudq2*gradqPast[i];
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

  integrandSurreal = 0;

  // get gradU
  MatrixQ<SurrealClass> dudq = 0;
  pde_.jacobianMasterState(param, qSurreal, dudq);
//  graduSurreal = gradqSurreal*dudq;
  for ( int i = 0; i < PhysDim::D; i++ )
  {
    graduSurreal[i] = dudq*gradqSurreal[i];
  }

  weightedIntegrand( param, dti, h ,graduSurreal, graduPast, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian
    slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOF_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOF_; j++)
            mtxPDEElem(i,j) += dJ*gradphi_[j][d]*PDE_gradq;
        }
      }
      slot += PDE::N;
    }
  } // nchunk
}

// Cell integrand
//
// integrand = phi*dF(u,grad u)/dt
//
// where
//   phi                basis function
//   U_n(x,y)           conservative solution vector at timestep n
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Tq, class Ti>
void
IntegrandCell_Galerkin_IRS<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
weightedIntegrand( const ParamT& param,
                   const Real dti,
                   const Real h,
                   const VectorArrayQ<Tq>& gradu,
                   const VectorArrayQ<Real>& graduPast,
                   ArrayQ<Ti> integrand[], int neqn ) const
{
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // get h-scaling
  Real C = 1.0*h*h;

  // get current IRS flux - TODO: Make this right
  VectorArrayQ<Ti> F = C*gradu;
  // get past IRS flux - TODO: Make this right
  VectorArrayQ<Ti> FPast = C*graduPast;

  // IRS backward Euler term
  VectorArrayQ<Ti> dFdt = (F - FPast)*dti;

  for (int k = 0; k < neqn; k++)
    integrand[k] -= dot(gradphi_[k], dFdt);
}

}

#endif  // INTEGRANDCELL_GALERKIN_IRS_H
