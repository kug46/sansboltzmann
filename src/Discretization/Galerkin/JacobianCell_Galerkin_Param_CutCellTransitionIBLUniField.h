// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_PARAM_CUTCELLTRANSITIONIBLUNIFIELD_H_
#define SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_PARAM_CUTCELLTRANSITIONIBLUNIFIELD_H_

#include "tools/Tuple.h"

#include "Discretization/GroupIntegral_Type.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/SurrealizedElementTuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "JacobianCell_Galerkin_Param.h"
#include "IntegrandCell_Galerkin_CutCellTransitionIBLUniField.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class Surreal, int iParam,
         class VarType, template<class, class> class PDENDConvert,
         class MatrixQP_>
class JacobianCell_Galerkin_Param_impl<
        Surreal, iParam,
        IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > >,
        MatrixQP_ > :
    public GroupIntegralCellType<
             JacobianCell_Galerkin_Param_impl<
               Surreal, iParam,
               IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > >,
               MatrixQP_ > >
{
public:
  typedef IntegrandCell_Galerkin_cutCellTransitionIBLUniField< PDENDConvert<PhysD2, PDEIBLUniField<PhysD2, VarType> > > IntegrandCellClass;

  typedef typename IntegrandCellClass::PhysDim PhysDim;

  typedef typename IntegrandCellClass::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCellClass::template ArrayQ<Surreal> ArrayQSurreal;

  typedef typename DLA::VectorD<ArrayQSurreal> ResidualElemClass;

  JacobianCell_Galerkin_Param_impl(const IntegrandCellClass& fcn,
                                   MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p ) :
    fcn_(fcn),
    mtxGlobalPDE_p_(mtxGlobalPDE_p) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim, class QBundleFieldType>
  void check( const QBundleFieldType& qfld )
  {
    SANS_ASSERT( mtxGlobalPDE_p_.m() == qfld.nDOFpossessed() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template<class Topology, class TopoDim, class TupleFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename TupleFieldType::template FieldCellGroupType<Topology>& tuplefldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
             const int quadratureorder )
  {
    typedef typename TupleFieldType::template FieldCellGroupType<Topology> TupleFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupType, Surreal, iParam>::type ElementTupleFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    typedef typename TupleType<iParam, TupleFieldCellGroupType>::type ParamCellGroupType;
    typedef typename ParamCellGroupType::template ElementType<Surreal> ElementParamFieldClass;

    typedef typename ElementParamFieldClass::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP; // jacobian type associated with each DOF

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass; // jacobian type associated with each element

    // field cell groups
    const ParamCellGroupType& paramfldCell = get<iParam>(tuplefldCell);

    // field elements
    ElementTupleFieldClass  tuplefldElem( tuplefldCell.basis() );
    ElementParamFieldClass& paramfldElem = set<iParam>(tuplefldElem);
    ElementQFieldClass      qfldElem( qfldCell.basis() );

    // DOFs per element
    const int nDOFPDE = qfldElem.nDOF();
    const int nParamDOF = paramfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFPDEGlobal(nDOFPDE,-1);
    std::vector<int> mapDOFParamGlobal(nParamDOF,-1);

    // Surrealized element integrand/residuals
    ResidualElemClass rsdElemPDE( nDOFPDE );

    // element jacobian matrix
    MatrixElemClass mtxElemPDE(nDOFPDE, nParamDOF);

    // element integral
    GalerkinWeightedIntegral_New_TransitionIBL<TopoDim, Topology, ResidualElemClass> integral(quadratureorder);

    // The integrand to be integrated
    auto integrand = fcn_.integrand( tuplefldElem, qfldElem );

    // number of simultaneous derivatives per functor call
    SANS_ASSERT_MSG(nArrayP == Surreal::N,
                    "Assumption that all the parameter derivatives are calculated simultaneously is violated!");
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = tuplefldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobian to zero
      mtxElemPDE = 0.0;

      // copy global parameter/solution DOFs to element
      tuplefldCell.getElement( tuplefldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // loop over derivative chunks (i.e. each entry of each parameter DOF in both left and right elements)
      for (int nchunk = 0; nchunk < nArrayP*nParamDOF; nchunk += nDeriv)
      {
        // associate derivative slots with parameter DOFs

        int slot = 0;
        for (int j = 0; j < nParamDOF; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(paramfldElem.DOF(j), n).deriv(k) = 0.0; // TODO: this is safe but can be slow

            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElem.DOF(j), n).deriv(slot - nchunk) = 1.0;
          }
        }

        // reset residuals
        rsdElemPDE = 0.0;

        // evaluate surrealized element residual
        integral(integrand, get<-1>(tuplefldElem), rsdElemPDE );
        // xfld should always be the last component of tuplefldElem

        // accumulate derivatives into element jacobians

        for (int j = 0; j < nParamDOF; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFPDE; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxElemPDE(i,j),m,n) = DLA::index(rsdElemPDE[i],m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global
      scatterAdd( qfldCell, paramfldCell, elem,
                  mapDOFPDEGlobal.data(), nDOFPDE,
                  mapDOFParamGlobal.data(), nParamDOF,
                  mtxElemPDE, mtxGlobalPDE_p_ );
    }
  }

protected:
  //----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, class ParamFieldCellGroupType, class MatrixQP, class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfldCell,
      const ParamFieldCellGroupType& paramfldCell,
      const int elem,
      int mapDOFEqnGlobal[], const int nDOFEqn,
      int mapParamDOFGlobal[], const int nParamDOF,
      SANS::DLA::MatrixD<MatrixQP>& mtxElemEqn_param,
      SparseMatrixType& mtxGlobalEqn_param )
  {
    qfldCell.associativity( elem ).getGlobalMapping( mapDOFEqnGlobal, nDOFEqn );
    paramfldCell.associativity( elem ).getGlobalMapping( mapParamDOFGlobal, nParamDOF );

    mtxGlobalEqn_param.scatterAdd( mtxElemEqn_param, mapDOFEqnGlobal, nDOFEqn, mapParamDOFGlobal, nParamDOF );
  }

protected:
  const IntegrandCellClass& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
};

} // namespace SANS

#endif /* SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_PARAM_CUTCELLTRANSITIONIBLUNIFIELD_H_ */
