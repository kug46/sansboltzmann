// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_PROJECTFUNCTION_H
#define INTEGRANDCELL_PROJECTFUNCTION_H

// cell integrand operator: global cell L2 projection of an analytic function

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "Field/Element/Element.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
namespace IntegrandCell_ProjFcn_detail
{
// Function type identification tag
class FcnX;
class FcnSref;

// worker object that can be partially specialized to do specific tasks
template <class FunctionNDConvert, class FunctionType>
struct worker;
} // namespace IntegrandCell_ProjFcn_detail


//----------------------------------------------------------------------------//
// Galerkin weighted cell integrand inside the integral \int_cell phi*(q - f) = 0
//   where phi is weighting function, q is solution projected onto, and f is prescribed function projected from

template <class FunctionNDConvert, class FunctionType>
class IntegrandCell_ProjectFunction : public IntegrandCellType<IntegrandCell_ProjectFunction<FunctionNDConvert, FunctionType>>
{
public:
  typedef FunctionNDConvert FunctionNDConvertType;
  typedef typename FunctionNDConvert::PhysDim PhysDim;

  template< class Z = Real >
  using ArrayQ = typename FunctionNDConvert::ArrayQ;
  template< class Z = Real >
  using MatrixQ = typename FunctionNDConvert::MatrixQ;

  IntegrandCell_ProjectFunction( const FunctionNDConvert& fcn, const std::vector<int>& CellGroups ) :
    fcn_(fcn),
    cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  // Basis-weighted integrand class: i.e. weighting function = basis function of solution q
  template<class TopoDim_, class Topology, class ElementParam>
  class BasisWeighted
  {
  private:
    friend struct IntegrandCell_ProjFcn_detail::worker<FunctionNDConvert, FunctionType>; // make a friend struct to do some labor

  public:
    typedef typename Topology::TopoDim TopoDim;

    typedef Element<ArrayQ<Real>, TopoDim, Topology> ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted(  const FunctionNDConvert& fcn,
                    const ElementParam& paramfldElem,
                    const ElementQFieldType& qfldElem ) :
      fcn_(fcn),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem_)), // XField must be the last parameter
      qfldElem_(qfldElem),
      nDOF_(qfldElem_.nDOF() ),
      phi_(nDOF_)
    {
      // phi_.resize(nDOF_); // allocate memory
    }

    ~BasisWeighted() {}

    // total DOFs
    int nDOF() const { return nDOF_; }

    // evaluate integrand at reference coordinate (e.g. of quadrature points)
    void operator()( const QuadPointType& sRef, ArrayQ<> integrand[], int neqn ) const;

    void operator()( const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<>>& rsdPDEElem) const;

    // evaluate jacobian of integrand w.r.t. elemental DOFs of solution q (i.e. mass matrix) at reference coordinate (e.g. of quadrature points)
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<>>& mtxMass ) const;

  protected:
    const FunctionNDConvert& fcn_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;

    const int nDOF_;
    mutable std::vector<Real> phi_;
  };

  //Factory function that returns the basis weighted integrand class
  template<class TopoDim, class Topology, class ElementParam>
  BasisWeighted<TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<Real>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<TopoDim, Topology, ElementParam>(fcn_, paramfldElem, qfldElem);
  }

  template<class TopoDim, class Topology, class ElementParam>
  BasisWeighted<TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<Real>, TopoDim, Topology>& qfldElem,
            const FunctionNDConvert& fcn) const
  {
    // Note that the fcn_ member of the IntegrandCell_ProjectFunction object is no longer used in
    // the BasisWeighted object that is returned in this function
    return BasisWeighted<TopoDim, Topology, ElementParam>(fcn, paramfldElem, qfldElem);
  }

  // Error-weighted integrand class: i.e. weighting function = basis function of estimate field e
  template<class TopoDim, class Topology, class ElementParam>
  class Estimate
  {
  private:
    friend struct IntegrandCell_ProjFcn_detail::worker<FunctionNDConvert, FunctionType>; // make a friend struct to do some labor

  public:
    typedef Element<ArrayQ<Real>, TopoDim, Topology>  ElementQFieldType;
    typedef Element<Real, TopoDim, Topology>          ElementEFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    Estimate( const FunctionNDConvert& fcn,
              const ElementParam& paramfldElem,
              const ElementQFieldType& qfldElem,
              const ElementEFieldType& efldElem ) :
      fcn_(fcn),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem_)), // XField must be the last parameter
      qfldElem_(qfldElem),
      efldElem_(efldElem),
      nPhi_( efldElem_.nDOF() ),
      ephi_()
    {
      ephi_.resize(nPhi_); // allocate memory
    }

    ~Estimate() {}

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }
    int nPhi() const { return efldElem_.nDOF(); }

    // evaluate integrand at reference coordinate (e.g. of quadrature points)
    void operator()( const QuadPointType& sRef, ArrayQ<> integrand[], int nphi ) const;

  protected:
    const FunctionNDConvert& fcn_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& efldElem_;

    const int nPhi_;
    mutable std::vector<Real> ephi_;
  };

  //Factory function that returns the estimate integrand class
  template<class TopoDim, class Topology, class ElementParam>
  Estimate<TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<Real>, TopoDim, Topology>& qfldElem,
            const Element<Real, TopoDim, Topology>& efldElem ) const
  {
    return Estimate<TopoDim, Topology, ElementParam>(fcn_, paramfldElem, qfldElem, efldElem);
  }

protected:
  const FunctionNDConvert& fcn_;
  const std::vector<int> cellGroups_;
};


// evaluate integrand at reference coordinate (e.g. of quadrature points)
template <class FunctionNDConvert, class FunctionType>
template <class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_ProjectFunction<FunctionNDConvert,FunctionType>::
BasisWeighted<TopoDim,Topology, ElementParam>::
operator()(const QuadPointType& sRef, ArrayQ<> integrand[], int neqn) const
{
  SANS_ASSERT(neqn == nDOF_);

  qfldElem_.evalBasis( sRef, phi_.data(), nDOF_ ); // evaluate basis functions

  ArrayQ<> q;
  qfldElem_.evalFromBasis( phi_.data(), nDOF_, q ); // evaluate solution variables

  // compute the forcing function for the projection using specialized routines depending on FunctionType
  ArrayQ<> f = IntegrandCell_ProjFcn_detail::template worker<FunctionNDConvert,FunctionType>::
                 template projectFromValue<TopoDim, Topology, ElementParam>(this, sRef);

  for (int k = 0; k < neqn; k++)
    integrand[k] = phi_[k]*(q - f);
}

template <class FunctionNDConvert, class FunctionType>
template <class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_ProjectFunction<FunctionNDConvert,FunctionType>::
BasisWeighted<TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<> >& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOF_);

  qfldElem_.evalBasis( sRef, phi_.data(), nDOF_ ); // evaluate basis functions

  ArrayQ<> q;
  qfldElem_.evalFromBasis( phi_.data(), nDOF_, q ); // evaluate solution variables

  // compute the forcing function for the projection using specialized routines depending on FunctionType
  ArrayQ<> f = IntegrandCell_ProjFcn_detail::template worker<FunctionNDConvert,FunctionType>::
                 template projectFromValue<TopoDim, Topology, ElementParam>(this, sRef);

  for (int k = 0; k < nDOF_; k++)
    rsdPDEElem[k] += dJ*phi_[k]*(q - f);
}

// evaluate jacobian of integrand w.r.t. elemental DOFs of solution q (i.e. mass matrix) at reference coordinate (e.g. of quadrature points)
template <class FunctionNDConvert, class FunctionType>
template <class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_ProjectFunction<FunctionNDConvert,FunctionType>::
BasisWeighted<TopoDim,Topology,ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<>>& mtxMass) const
{
  SANS_ASSERT(mtxMass.m() == nDOF_);
  SANS_ASSERT(mtxMass.n() == nDOF_);

  qfldElem_.evalBasis( sRef, phi_.data(), nDOF_ ); // evaluate basis functions

  MatrixQ<> I = DLA::Identity();

  MatrixQ<> proj_q; // temporary storage

  // add to the mass matrix
  for (int i = 0; i < nDOF_; i++)
  {
    proj_q = (dJ*phi_[i])*I;
    for (int j = 0; j < nDOF_; j++)
      mtxMass(i,j) += proj_q*phi_[j];
  }
}


// evaluate integrand at reference coordinate (e.g. of quadrature points)
template <class FunctionNDConvert, class FunctionType>
template <class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_ProjectFunction<FunctionNDConvert,FunctionType>::
Estimate<TopoDim,Topology, ElementParam>::
operator()(const QuadPointType& sRef, ArrayQ<> integrand[], int nphi) const
{
  SANS_ASSERT( nphi == nPhi_);

  efldElem_.evalBasis( sRef, ephi_.data(), nPhi_ ); // evaluate estimate field basis functions

  ArrayQ<> q;
  qfldElem_.eval( sRef, q ); // evaluate solution variables

  // compute the forcing function for the projection using specialized routines depending on FunctionType
  ArrayQ<> f = IntegrandCell_ProjFcn_detail::template worker<FunctionNDConvert,FunctionType>::
                 template projectFromValue<TopoDim, Topology, ElementParam>(this, sRef);

  for (int k = 0; k < nphi; k++)
    integrand[k] = ephi_[k]*pow(q - f,2.0);
}


//----------------------------------------------------------------------------//
namespace IntegrandCell_ProjFcn_detail
{
// FunctionType = FcnX: forcing function F(X) only depends on physical coordinates X
template <class FunctionNDConvert>
struct worker<FunctionNDConvert, FcnX>
{
  typedef IntegrandCell_ProjectFunction<FunctionNDConvert,FcnX> IntegrandType;
  typedef typename IntegrandType::template ArrayQ<Real> ArrayQ;

  // compute the forcing function value which is projected from
  template<class TopoDim, class Topology, class ElementParam>
  static ArrayQ
  projectFromValue(const typename IntegrandType::template BasisWeighted<TopoDim,Topology,ElementParam>* integrandFcn,
                   const typename IntegrandType::template BasisWeighted<TopoDim,Topology,ElementParam>::QuadPointType& sRef)
  {
    typename IntegrandType::template BasisWeighted<TopoDim,Topology,ElementParam>::VectorX X;

    integrandFcn->xfldElem_.eval(sRef, X);
    return integrandFcn->fcn_(X);
  }

  // compute the forcing function value which is projected from
  template<class TopoDim, class Topology, class ElementParam>
  static ArrayQ
  projectFromValue(const typename IntegrandType::template Estimate<TopoDim,Topology,ElementParam>* integrandFcn,
                   const typename IntegrandType::template Estimate<TopoDim,Topology,ElementParam>::QuadPointType& sRef)
  {
    typename IntegrandType::template Estimate<TopoDim,Topology,ElementParam>::VectorX X;

    integrandFcn->xfldElem_.eval(sRef, X);
    return integrandFcn->fcn_(X);
  }
};

// FunctionType = FcnSref: forcing function F(sRef) only depends on element reference coordinates sRef
template <class FunctionNDConvert>
struct worker<FunctionNDConvert, FcnSref>
{
  typedef IntegrandCell_ProjectFunction<FunctionNDConvert,FcnSref> IntegrandType;
  typedef typename IntegrandType::template ArrayQ<Real> ArrayQ;

  // compute the forcing function value which is projected from
  template<class TopoDim, class Topology, class ElementParam>
  static ArrayQ
  projectFromValue(const typename IntegrandType::template BasisWeighted<TopoDim,Topology,ElementParam>* integrandFcn,
                   const typename IntegrandType::template BasisWeighted<TopoDim,Topology,ElementParam>::QuadPointType& sRef)
  {
    return integrandFcn->fcn_(sRef);
  }

    // compute the forcing function value which is projected from
    template<class TopoDim, class Topology, class ElementParam>
    static ArrayQ
    projectFromValue(const typename IntegrandType::template Estimate<TopoDim,Topology,ElementParam>* integrandFcn,
                     const typename IntegrandType::template Estimate<TopoDim,Topology,ElementParam>::QuadPointType& sRef)
    {
      return integrandFcn->fcn_(sRef);
    }
};

} // namespace IntegrandCell_ProjFcn_detail

}

#endif  // INTEGRANDCELL_PROJECTFUNCTION_H
