// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_LINEARSCALAR_MITLG_GALERKIN_H
#define INTEGRANDBOUNDARYTRACE_LINEARSCALAR_MITLG_GALERKIN_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Integrand_Galerkin_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_mitLG>, Galerkin> :
  public IntegrandBoundaryTraceType<IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_mitLG>, Galerkin> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;        // matrices

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;

  static const int N = PDE::N;

  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups)
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups) {}

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;

    typedef Element<ArrayQ<T>, TopoDimCell , Topology     > ElementQFieldCell;
    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template <class Z>
    using IntegrandCellType = ArrayQ<Z>;

    template <class Z>
    using IntegrandTraceType = ArrayQ<Z>;

    BasisWeighted(  const PDE& pde, const BCBase& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem, // Xfield must be the last parameter
                    const ElementQFieldCell& qfldElem,
                    const ElementQFieldTrace& lgfldTrace ) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)),
                    qfldElem_(qfldElem),
                    lgfldTrace_(lgfldTrace),
                    paramfldElem_(paramfldElem),
                    nDOFCell_(qfldElem_.nDOF()),
                    nDOFTrace_(lgfldTrace_.nDOF()),
                    phi_( new Real[nDOFCell_] ),
                    phiT_( new Real[nDOFTrace_] ),
                    gradphi_( new VectorX[nDOFCell_] )
    {
    }

    BasisWeighted(  BasisWeighted&& bw ) :
                    pde_(bw.pde_), bc_(bw.bc_),
                    xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                    xfldElem_(bw.xfldElem_),
                    qfldElem_(bw.qfldElem_),
                    lgfldTrace_(bw.lgfldTrace_),
                    paramfldElem_(bw.paramfldElem_),
                    nDOFCell_(bw.nDOFCell_),
                    nDOFTrace_(bw.nDOFTrace_),
                    phi_( bw.phi_ ),
                    phiT_( bw.phiT_ ),
                    gradphi_( bw.gradphi_ )
    {
      bw.phi_ = nullptr; bw.phiT_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] phiT_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFCell_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType<Ti>  integrandCell[],  int neqnCell,
                                                          IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const;

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Tq, class Tg, class Ti>
    void operator()( const BC& bc,
                     const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI, const ArrayQ<Tq>& lg,
                     const ParamT& paramI, const VectorX& nL,
                     IntegrandCellType<Ti> integrandCell[], int neqnCell,
                     IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const;

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const Real A, const Real B, const Ti& bcdata,
                            const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI, const ArrayQ<Tq>& lg,
                            const ParamT& paramI, const VectorX& nL,
                            IntegrandCellType<Ti> integrandCell[], int neqnCell,
                            IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldTrace& lgfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFCell_;
    const int nDOFTrace_;
    mutable Real *phi_;
    mutable Real *phiT_;
    mutable VectorX *gradphi_;
  };


  template<class Tq, class Tw,
           class TopoDimTrace, class TopologyTrace,
           class TopoDimCell,  class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;

    typedef Element<ArrayQ<Tq>, TopoDimCell , Topology     > ElementQFieldCell;
    typedef Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<Tw>, TopoDimCell , Topology     > ElementWFieldCell;
    typedef Element<ArrayQ<Tw>, TopoDimTrace, TopologyTrace> ElementWFieldTrace;
    typedef Element<Real,       TopoDimCell , Topology     > ElementEFieldCell;
    typedef Element<Real,       TopoDimTrace, TopologyTrace> ElementEFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type Ti;

    typedef Ti IntegrandCellType;
    typedef Ti IntegrandTraceType;

    FieldWeighted(  const PDE& pde, const BCBase& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem, // Xfield must be the last parameter
                    const ElementQFieldCell& qfldElem,
                    const ElementWFieldCell& wfldElem,
                    const ElementEFieldCell& efldElem,
                    const ElementQFieldTrace& lgfldTrace,
                    const ElementWFieldTrace& mufldTrace,
                    const ElementEFieldTrace& eBfldTrace ) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)),
                    qfldElem_(qfldElem), wfldElem_(wfldElem), efldElem_(efldElem),
                    lgfldTrace_(lgfldTrace), mufldTrace_(mufldTrace), eBfldTrace_(eBfldTrace),
                    paramfldElem_(paramfldElem),
                    nDOFCell_(qfldElem_.nDOF()),
                    nDOFTrace_(lgfldTrace_.nDOF()),
                    nPhiCell_(efldElem_.nDOF()),
                    nPhiTrace_(eBfldTrace_.nDOF()),
                    phi_( new Real[nPhiCell_] ),
                    gradphi_( new VectorX[nPhiCell_] ),
                    weight_( new ArrayQ<Tw>[nPhiCell_] ),
                    gradWeight_( new VectorArrayQ<Tw>[nPhiCell_] ),
                    phiT_( new Real[nPhiTrace_] ),
                    tWeight_( new ArrayQ<Tw>[nPhiTrace_] )
    {
    }

    FieldWeighted(  FieldWeighted&& fw ) :
                    pde_(fw.pde_), bc_(fw.bc_),
                    xfldElemTrace_(fw.xfldElemTrace_), canonicalTrace_(fw.canonicalTrace_),
                    xfldElem_(fw.xfldElem_),
                    qfldElem_(fw.qfldElem_), wfldElem_(fw.wfldElem_), efldElem_(fw.efldElem_),
                    lgfldTrace_(fw.lgfldTrace_), mufldTrace_(fw.mufldTrace_), eBfldTrace_(fw.eBfldTrace_),
                    paramfldElem_(fw.paramfldElem_),
                    nDOFCell_(fw.nDOFCell_),
                    nDOFTrace_(fw.nDOFTrace_),
                    nPhiCell_(fw.nPhiCell_),
                    nPhiTrace_(fw.nPhiTrace_),
                    phi_( fw.phi_ ),
                    gradphi_( fw.gradphi_ ),
                    weight_( fw.weight_ ),
                    gradWeight_( fw.gradWeight_ ),
                    phiT_( fw.phiT_ ),
                    tWeight_( fw.tWeight_ )
    {
      fw.phi_ = nullptr;
      fw.gradphi_ = nullptr;
      fw.weight_ = nullptr;
      fw.gradWeight_ = nullptr;
      fw.phiT_ = nullptr;
      fw.tWeight_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
      delete [] weight_;
      delete [] gradWeight_;
      delete [] phiT_;
      delete [] tWeight_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return qfldElem_.nDOF(); }
    int nDOFTrace() const { return lgfldTrace_.nDOF(); }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType integrandCell[], const int nphiCell,
                                                          IntegrandTraceType integrandTrace[], const int nphiTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandCell, nphiCell, integrandTrace, nphiTrace );
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandCellType integrandCell[], const int nphiCell,
                                                                       IntegrandTraceType integrandTrace[], const int nphiTrace ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementEFieldCell& efldElem_;
    const ElementQFieldTrace& lgfldTrace_;
    const ElementWFieldTrace& mufldTrace_;
    const ElementEFieldTrace& eBfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFCell_;
    const int nDOFTrace_;

    const int nPhiCell_, nPhiTrace_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable ArrayQ<Tw> *weight_;
    mutable VectorArrayQ<Tw> *gradWeight_;
    mutable Real *phiT_;
    mutable ArrayQ<Tw> *tWeight_;

  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell , Topology     >& qfldElem,
            const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& lgfldTrace) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTrace,
             paramfldElem,
             qfldElem,
             lgfldTrace };
  }

  template<class Tq, class Tw,
           class TopoDimTrace, class TopologyTrace,
           class TopoDimCell,  class Topology, class ElementParam>
  FieldWeighted< Tq,Tw, TopoDimTrace, TopologyTrace, TopoDimCell, Topology, ElementParam >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<Tq>, TopoDimCell , Topology     >&  qfldElem,
            const Element<ArrayQ<Tw>, TopoDimCell , Topology     >&  wfldElem,
            const Element<Real,       TopoDimCell , Topology     >&  efldElem,
            const Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace>& lgfldTrace,
            const Element<ArrayQ<Tw>, TopoDimTrace, TopologyTrace>& mufldTrace,
            const Element<Real,       TopoDimTrace, TopologyTrace>& eBfldTrace) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTrace,
             paramfldElem,
             qfldElem, wfldElem, efldElem,
             lgfldTrace, mufldTrace, eBfldTrace };
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
};

template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam>
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_mitLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType<Ti> integrandCell[], int neqnCell,
                                                 IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const
{
  SANS_ASSERT( (neqnCell == nDOFCell_) && (neqnTrace == nDOFTrace_) );

  ParamT paramI;             // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                // unit normal (points out of domain)

  ArrayQ<T> qI;              // solution
  VectorArrayQ<T> gradqI;    // gradient
  ArrayQ<T> lg;              // Lagrange multiplier

  QuadPointCellType sRefCell;    // reference-element coordinates on the cell

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRefCell );

  // Elemental parameters
  paramfldElem_.eval( sRefCell, paramI );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRefCell, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElem_.evalBasis( sRefCell, phi_, nDOFCell_ );
  xfldElem_.evalBasisGradient( sRefCell, qfldElem_, gradphi_, nDOFCell_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOFCell_, qI );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradphi_, nDOFCell_, gradqI );
  else
    gradqI = 0;

  // Lagrange multiplier basis
  lgfldTrace_.evalBasis( sRefTrace, phiT_, nDOFTrace_ );

  // Lagrange multiplier
  lgfldTrace_.evalFromBasis( phiT_, nDOFTrace_, lg );

  call_with_derived<NDBCVector>(*this, bc_,
                                qI, gradqI, lg,
                                paramI, nL,
                                integrandCell, neqnCell,
                                integrandTrace, neqnTrace);
}

template <class PDE, class NDBCVector>
template < class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
template<class BC, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_mitLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc,
            const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI, const ArrayQ<Tq>& lg,
            const ParamT& paramI, const VectorX& nL,
            IntegrandCellType<Ti> integrandCell[], int neqnCell,
            IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const
{
  // BC coefficients
  Real A, B;
  bc.coefficients( paramI, nL, A, B );

  // BC data
  Ti bcdata;
  bc.data( paramI, nL, bcdata );

  // compute the integrand (note the lack of the BC template to reduce compile time)
  weightedIntegrand(A, B, bcdata,
                    qI, gradqI, lg,
                    paramI, nL,
                    integrandCell, neqnCell,
                    integrandTrace, neqnTrace);
}

template <class PDE, class NDBCVector>
template < class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
template<class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_mitLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
weightedIntegrand( const Real A, const Real B, const Ti& bcdata,
                   const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI, const ArrayQ<Tq>& lg,
                   const ParamT& paramI, const VectorX& nL,
                   IntegrandCellType<Ti> integrandCell[], int neqnCell,
                   IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const
{
  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] = 0;

  // PDE residual: weak form boundary integral

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( paramI, qI, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( paramI, qI, gradqI, F );

    ArrayQ<Ti> Fn = dot(nL,F);
    for (int k = 0; k < neqnCell; k++)
      integrandCell[k] += phi_[k]*Fn;
  }

  // PDE residual: BC Lagrange multiplier term

  VectorMatrixQ<T> a = 0;
  pde_.jacobianFluxAdvective( paramI, qI, a );
  T an = dot(nL,a);

  DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<Ti> > K = 0;
  pde_.diffusionViscous( paramI, qI, gradqI, K );

  VectorArrayQ<Ti> Kgradq = K*gradqI; // = Kn*dq/dn + Ks*dq/ds

  ArrayQ<Ti> tmp = (-B*qI + A*dot(Kgradq,nL))/(A*A + B*B) + lg;

  for (int k = 0; k < neqnCell; k++)
  {
    VectorArrayQ<Ti> Ktgradphi = Transpose(K)*gradphi_[k];
    integrandCell[k] += (phi_[k]*(A + B*an) + B*dot(Ktgradphi,nL))*tmp;
  }

  // BC residual: BC weak form

  ArrayQ<Ti> residualBC = A*qI + B*dot(Kgradq,nL) - bcdata;
  for (int k = 0; k < neqnTrace; k++)
    integrandTrace[k] = phiT_[k]*residualBC;
}


template < class PDE, class NDBCVector>
template < class Tq, class Tw,
           class TopoDimTrace, class TopologyTrace,
           class TopoDimCell,  class Topology, class ElementParam>
template < class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_mitLG>, Galerkin>::
    FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandCellType integrandCell[], const int nphiCell,
     IntegrandTraceType integrandTrace[], const int nphiTrace ) const
{
  SANS_ASSERT( nphiCell == nPhiCell_ && nphiTrace == nPhiTrace_ );
  ParamT param; // Elemental parameters (such as grid coordinates and distance functions)

  VectorX N;                                    // unit normal (points out of domain)

  ArrayQ<Tq> q;                                  // solution
  DLA::VectorS< PhysDim::D, ArrayQ<Tq> > gradq;  // solution gradient
  ArrayQ<Tq> lg;                                 // solution Lagrange multiplier

  ArrayQ<Tw> w;                                  // weight
  DLA::VectorS< PhysDim::D, ArrayQ<Tw> > gradw;  // weight gradient
  ArrayQ<Tw> mu;                                 // weight Lagrange multiplier

  QuadPointCellType sRef;                       // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, N);

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  qfldElem_.eval( sRef, q );

  // solution value, gradient
  if (needsSolutionGradient)
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );
  else
    gradq = 0;

  // Lagrange multiplier
  lgfldTrace_.eval( sRefTrace, lg );

  // Weight evaluations
  // Lagrange Multiplier
  mufldTrace_.eval( sRefTrace, mu );

  // weight value, gradient
  wfldElem_.eval( sRef, w );
  xfldElem_.evalGradient( sRef, wfldElem_, gradw );

  //estimate basis, gradient
  efldElem_.evalBasis( sRef, phi_, nPhiCell_ );
  xfldElem_.evalBasisGradient( sRef, efldElem_, gradphi_, nPhiCell_ );

  eBfldTrace_.evalBasis( sRefTrace, phiT_, nPhiTrace_ );

  for (int k = 0; k < nPhiCell_; k++)
  {
    integrandCell[k] = 0;
    weight_[k] = w* phi_[k];
    gradWeight_[k] = gradphi_[k]*w + phi_[k]*gradw;
  }

  for (int k = 0; k < nPhiTrace_; k++)
  {
    integrandTrace[k] = 0;
    tWeight_[k] = mu*phiT_[k];
  }

  // PDE residual: weak form boundary integral

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    DLA::VectorS<PhysDim::D, ArrayQ<Tq> > F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, q, gradq, F );

    ArrayQ<Tq> flux = dot(N,F);
    for (int k = 0; k < nPhiCell_; k++)
      integrandCell[k] = dot(weight_[k],flux);
  }

  // PDE residual: BC Lagrange multiplier term

  // BC coefficients
  Real A, B;
  bc.coefficients( param, N, A, B );

  DLA::VectorS< PhysDim::D, MatrixQ<Tq> > a = 0;
  pde_.jacobianFluxAdvective( param, q, a );
  Tq an = dot(N,a);

  DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<Tq> > K = 0;
  pde_.diffusionViscous( param, q, gradq, K );

  VectorArrayQ<Tq> Kgradq = K*gradq; // = Kn*dq/dn + Ks*dq/ds

  ArrayQ<Tq> tmp = (- B*q + A*dot(Kgradq,N))/(A*A + B*B) + lg;

  Tq tmp2 = (A + B*an);
  for (int k = 0; k < nPhiCell_; k++)
  {
    VectorArrayQ<Ti> Ktgradw = Transpose(K)*gradWeight_[k];
    integrandCell[k] += dot((weight_[k]*tmp2 + B*dot(Ktgradw,N)),tmp);
  }
  // BC residual: BC weak form

  // BC data
  Real bcdata;
  bc.data( param, N, bcdata );

  ArrayQ<Tq> residualBC = A*q + B*dot(Kgradq,N) - bcdata;

  for (int k = 0; k < nPhiTrace_; k++)
    integrandTrace[k] = dot(tWeight_[k],residualBC);
}

}

#endif  // INTEGRANDBOUNDARYTRACE_LINEARSCALAR_MITLG_GALERKIN_H
