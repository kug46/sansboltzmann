// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_INTEGRANDINTERIORTRACE_GALERKIN_MANIFOLD_H_
#define SRC_DISCRETIZATION_GALERKIN_INTEGRANDINTERIORTRACE_GALERKIN_MANIFOLD_H_

// interior trace integrand operator: Galerkin specialized for manifold

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Integrand_Galerkin_fwd.h"
#include "JacobianInteriorTrace_Galerkin_Element.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// interior trace integrand: Galerkin for manifolds
// specialization in avgTraceNormal formulation
//
// integrandL = + [[phiL]].{F}
// integrandR = - [[phiR]].{F}
//
// where
//   phi                basis function
//   F(x,y,e,q,qx,qy)   flux (advective, viscous)
//
template <class PDE_>
class IntegrandInteriorTrace_Galerkin_manifold<PDE_, avgTraceNormal_GalerkinManifoldInteriorTrace> :
  public IntegrandInteriorTraceType<IntegrandInteriorTrace_Galerkin_manifold<PDE_, avgTraceNormal_GalerkinManifoldInteriorTrace> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  // cppcheck-suppress noExplicitConstructor
  IntegrandInteriorTrace_Galerkin_manifold(const PDE& pde,
                                           const std::vector<int>& InteriorTraceGroups,
                                           const std::vector<int> PeriodicTraceGroups = {}) :
    pde_(pde),
    interiorTraceGroups_(InteriorTraceGroups),
    periodicTraceGroups_(PeriodicTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  std::size_t nPeriodicTraceGroups() const          { return periodicTraceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return periodicTraceGroups_[n]; }
  std::vector<int> periodicTraceGroups() const      { return periodicTraceGroups_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldTypeL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldTypeR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldTypeL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldTypeR;

    typedef typename ElementXFieldTraceType::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_Galerkin<MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes; // manifold local axes type

    BasisWeighted( const PDE& pde,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldTypeL& qfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldTypeR& qfldElemR ) :
      pde_(pde),
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), paramfldElemL_( paramfldElemL ),
      xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), paramfldElemR_( paramfldElemR ),
      nDOFL_(qfldElemL_.nDOF()),
      nDOFR_(qfldElemR_.nDOF()),
      phiL_( new Real[nDOFL_] ),
      phiR_( new Real[nDOFR_] ),
      gradphiL_( new VectorX[nDOFL_] ),
      gradphiR_( new VectorX[nDOFR_] )
    {
    }

    ~BasisWeighted()
    {
      delete [] phiL_;
      delete [] phiR_;

      delete [] gradphiL_;
      delete [] gradphiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int neqnL,
                                                     ArrayQ<Ti> integrandR[], const int neqnR ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemR ) const;

  protected:

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const QuadPointTraceType& sRefTrace,
                            const VectorX& nL,
                            const LocalAxes& e0L, const LocalAxes& e0R,
                            const ParamTL& paramL, const ParamTR& paramR,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                            const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                            ArrayQ<Ti> integrandL[], const int neqnL,
                            ArrayQ<Ti> integrandR[], const int neqnR,
                            bool gradientOnly = false ) const;

    const PDE& pde_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldTypeL& xfldElemL_;
    const ElementQFieldTypeL& qfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldTypeR& xfldElemR_;
    const ElementQFieldTypeR& qfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
    mutable VectorX *gradphiL_;
    mutable VectorX *gradphiR_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,      TopologyR,
                                  ElementParamL, ElementParamR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>    , TopoDimCell , TopologyR    >& qfldElemR) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL,     TopologyR,
                                          ElementParamL, ElementParamR>(pde_,
                                                                        xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                                                        paramfldElemL, qfldElemL,
                                                                        paramfldElemR, qfldElemR);
  }

protected:
  const PDE& pde_;
  const std::vector<int> interiorTraceGroups_;
  const std::vector<int> periodicTraceGroups_;
};


template <class PDE_>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR>
bool
IntegrandInteriorTrace_Galerkin_manifold<PDE_, avgTraceNormal_GalerkinManifoldInteriorTrace>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}

template <class PDE_>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR>
template <class Ti>
void
IntegrandInteriorTrace_Galerkin_manifold<PDE_, avgTraceNormal_GalerkinManifoldInteriorTrace>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], const int neqnL,
                                                 ArrayQ<Ti> integrandR[], const int neqnR ) const
{
  static_assert( TopoDimCell::D == 1, "Only TopoD1 element is implemented so far."); // TODO: generalize

  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );
  }

  // ------ manifold-specific treatment begins ------ //
  // trace unit normal: points to R
  VectorX nL;                      // unit trace normal vector
  traceUnitNormal(xfldElemTrace_, sRefTrace, xfldElemL_, sRefL, xfldElemR_, sRefR, nL);

  // basis vectors
  VectorX e01L, e01R;              // basis direction vector
  xfldElemL_.unitTangent(sRefL, e01L);
  xfldElemR_.unitTangent(sRefR, e01R);

  LocalAxes e0L, e0R;              // basis direction vector
  e0L[0] = e01L;
  e0R[0] = e01R;
  // ------ manifold-specific treatment ends ------ //

  // compute the integrand
  weightedIntegrand( sRefTrace,
                     nL,
                     e0L, e0R,
                     paramL, paramR,
                     qL, gradqL,
                     qR, gradqR,
                     integrandL, neqnL,
                     integrandR, neqnR );

}


//---------------------------------------------------------------------------//
template<class PDE_>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_Galerkin_manifold<PDE_, avgTraceNormal_GalerkinManifoldInteriorTrace>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  // ------ manifold-specific treatment begins ------ //
  // trace unit normal: points to R
  VectorX nL;                      // unit trace normal vector
  traceUnitNormal(xfldElemTrace_, sRefTrace, xfldElemL_, sRefL, xfldElemR_, sRefR, nL);

  // basis vectors
  VectorX e01L, e01R;              // basis direction vector
  xfldElemL_.unitTangent(sRefL, e01L);
  xfldElemR_.unitTangent(sRefR, e01R);

  LocalAxes e0L, e0R;              // basis direction vector
  e0L[0] = e01L;
  e0R[0] = e01R;
  // ------ manifold-specific treatment ends ------ //

  ArrayQ<SurrealClass> qSurrealL, qSurrealR;                // solution
  VectorArrayQ<SurrealClass> gradqSurrealL, gradqSurrealR;  // gradient

  qSurrealL = qL;
  qSurrealR = qR;

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

    gradqSurrealL = gradqL;
    gradqSurrealR = gradqR;
  }

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandRSurreal( nDOFR_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( sRefTrace,
                       nL,
                       e0L, e0R,
                       paramL, paramR,
                       qSurrealL, gradqL,
                       qSurrealR, gradqR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemR.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemL.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemR.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk


  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*2*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandLSurreal = 0;
      integrandRSurreal = 0;

      // compute the integrand
      weightedIntegrand( sRefTrace,
                         nL,
                         e0L, e0R,
                         paramL, paramR,
                         qL, gradqSurrealL,
                         qR, gradqSurrealR,
                         integrandLSurreal.data(), nDOFL_,
                         integrandRSurreal.data(), nDOFR_, true );

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDE_qL(i,j) += gradphiL_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemR.PDE_qL(i,j) += gradphiL_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }

      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemL.PDE_qR(i,j) += gradphiR_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemR.PDE_qR(i,j) += gradphiR_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient
}


template<class PDE_>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
template<class Tq, class Tg, class Ti>
void
IntegrandInteriorTrace_Galerkin_manifold<PDE_, avgTraceNormal_GalerkinManifoldInteriorTrace>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
weightedIntegrand( const QuadPointTraceType& sRefTrace,
                   const VectorX& nL,
                   const LocalAxes& e0L, const LocalAxes& e0R,
                   const ParamTL& paramL, const ParamTR& paramR,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                   const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                   ArrayQ<Ti> integrandL[], const int neqnL,
                   ArrayQ<Ti> integrandR[], const int neqnR,
                   bool gradientOnly ) const
{
  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0.0;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0.0;

  // advective flux term: +[[phi]].flux = (phi@L - phi@R) N.flux
  //                                    = +phiL N.flux; -phiR N.flux

  ArrayQ<Ti> FnL = 0.0, FnR = 0.0;     // normal flux (if they are different for left/right elements)

  if (pde_.hasFluxAdvective())
  {
    const int coNrmSignL = xfldElemTrace_.conormalSignL(sRefTrace);
    const int coNrmSignR = xfldElemTrace_.conormalSignR(sRefTrace);

    LocalAxes e0ReffL, e0LeffR;
    e0ReffL = -coNrmSignL*(coNrmSignR*e0R); // effective e0R used by left element
    e0LeffR = -coNrmSignR*(coNrmSignL*e0L); // effective e0L used by right element

    pde_.fluxAdvectiveUpwind(paramL, paramR, e0L, e0ReffL, qL, qR, nL, FnL, FnR);

    for (int k = 0; k < neqnL; ++k)
      integrandL[k] +=  phiL_[k]*FnL;

    for (int k = 0; k < neqnR; ++k)
      integrandR[k] += -phiR_[k]*FnR;
  }
}


//----------------------------------------------------------------------------//
// interior trace integrand: Galerkin for manifolds
// specialization in separate TraceNormal formulation
//
// integrandL = + [[phiL]].{F}
// integrandR = - [[phiR]].{F}
//
// where
//   phi                basis function
//   F(x,y,e,q,qx,qy)   flux (advective, viscous)
//
template <class PDE_>
class IntegrandInteriorTrace_Galerkin_manifold<PDE_, sepTraceNormal_GalerkinManifoldInteriorTrace> :
  public IntegrandInteriorTraceType<IntegrandInteriorTrace_Galerkin_manifold<PDE_, sepTraceNormal_GalerkinManifoldInteriorTrace> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  // cppcheck-suppress noExplicitConstructor
  IntegrandInteriorTrace_Galerkin_manifold(const PDE& pde,
                                           const std::vector<int>& InteriorTraceGroups,
                                           const std::vector<int> PeriodicTraceGroups = {}) :
    pde_(pde),
    interiorTraceGroups_(InteriorTraceGroups),
    periodicTraceGroups_(PeriodicTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  std::size_t nPeriodicTraceGroups() const          { return periodicTraceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return periodicTraceGroups_[n]; }
  std::vector<int> periodicTraceGroups() const      { return periodicTraceGroups_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldTypeL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldTypeR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldTypeL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldTypeR;

    typedef typename ElementXFieldTraceType::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_Galerkin<MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes; // manifold local axes type

    BasisWeighted( const PDE& pde,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldTypeL& qfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldTypeR& qfldElemR ) :
      pde_(pde),
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), paramfldElemL_( paramfldElemL ),
      xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), paramfldElemR_( paramfldElemR ),
      nDOFL_(qfldElemL_.nDOF()),
      nDOFR_(qfldElemR_.nDOF()),
      phiL_( new Real[nDOFL_] ),
      phiR_( new Real[nDOFR_] ),
      gradphiL_( new VectorX[nDOFL_] ),
      gradphiR_( new VectorX[nDOFR_] )
    {
    }

    ~BasisWeighted()
    {
      delete [] phiL_;
      delete [] phiR_;

      delete [] gradphiL_;
      delete [] gradphiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int neqnL,
                                                     ArrayQ<Ti> integrandR[], const int neqnR ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemR ) const;

  protected:

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const QuadPointTraceType& sRefTrace,
                            const VectorX& nL, const VectorX& nR,
                            const LocalAxes& e0L, const LocalAxes& e0R,
                            const ParamTL& paramL, const ParamTR& paramR,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                            const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                            ArrayQ<Ti> integrandL[], const int neqnL,
                            ArrayQ<Ti> integrandR[], const int neqnR,
                            bool gradientOnly = false ) const;

    const PDE& pde_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldTypeL& xfldElemL_;
    const ElementQFieldTypeL& qfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldTypeR& xfldElemR_;
    const ElementQFieldTypeR& qfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
    mutable VectorX *gradphiL_;
    mutable VectorX *gradphiR_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,      TopologyR,
                                  ElementParamL, ElementParamR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>    , TopoDimCell , TopologyR    >& qfldElemR) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL,     TopologyR,
                                          ElementParamL, ElementParamR>(pde_,
                                                                        xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                                                        paramfldElemL, qfldElemL,
                                                                        paramfldElemR, qfldElemR);
  }

protected:
  const PDE& pde_;
  const std::vector<int> interiorTraceGroups_;
  const std::vector<int> periodicTraceGroups_;
};


template <class PDE_>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR>
bool
IntegrandInteriorTrace_Galerkin_manifold<PDE_, sepTraceNormal_GalerkinManifoldInteriorTrace>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}

template <class PDE_>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR>
template <class Ti>
void
IntegrandInteriorTrace_Galerkin_manifold<PDE_, sepTraceNormal_GalerkinManifoldInteriorTrace>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], const int neqnL,
                                                 ArrayQ<Ti> integrandR[], const int neqnR ) const
{
  static_assert( TopoDimCell::D == 1, "Only TopoD1 element is implemented so far."); // TODO: generalize

  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );
  }

  // ------ manifold-specific treatment begins ------ //
  // trace unit normal: points to R
  VectorX nL = 0.0, nR = 0.0; // unit trace normal vectors
  traceUnitNormal(xfldElemTrace_, sRefTrace, xfldElemL_, sRefL, xfldElemR_, sRefR, nL, nR);

  // basis vectors
  VectorX e01L, e01R;              // basis direction vector
  xfldElemL_.unitTangent(sRefL, e01L);
  xfldElemR_.unitTangent(sRefR, e01R);

  LocalAxes e0L, e0R;              // basis direction vector
  e0L[0] = e01L;
  e0R[0] = e01R;
  // ------ manifold-specific treatment ends ------ //

  // compute the integrand
  weightedIntegrand( sRefTrace,
                     nL, nR,
                     e0L, e0R,
                     paramL, paramR,
                     qL, gradqL,
                     qR, gradqR,
                     integrandL, neqnL,
                     integrandR, neqnR );

}


//---------------------------------------------------------------------------//
template<class PDE_>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_Galerkin_manifold<PDE_, sepTraceNormal_GalerkinManifoldInteriorTrace>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  // ------ manifold-specific treatment begins ------ //
  // trace unit normal: points to R
  VectorX nL = 0.0, nR = 0.0; // unit trace normal vectors
  traceUnitNormal(xfldElemTrace_, sRefTrace, xfldElemL_, sRefL, xfldElemR_, sRefR, nL, nR);

  // basis vectors
  VectorX e01L, e01R;              // basis direction vector
  xfldElemL_.unitTangent(sRefL, e01L);
  xfldElemR_.unitTangent(sRefR, e01R);

  LocalAxes e0L, e0R;              // basis direction vector
  e0L[0] = e01L;
  e0R[0] = e01R;
  // ------ manifold-specific treatment ends ------ //

  ArrayQ<SurrealClass> qSurrealL, qSurrealR;                // solution
  VectorArrayQ<SurrealClass> gradqSurrealL, gradqSurrealR;  // gradient

  qSurrealL = qL;
  qSurrealR = qR;

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

    gradqSurrealL = gradqL;
    gradqSurrealR = gradqR;
  }

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandRSurreal( nDOFR_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( sRefTrace,
                       nL, nR,
                       e0L, e0R,
                       paramL, paramR,
                       qSurrealL, gradqL,
                       qSurrealR, gradqR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemR.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemL.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemR.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk


  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*2*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandLSurreal = 0;
      integrandRSurreal = 0;

      // compute the integrand
      weightedIntegrand( sRefTrace,
                         nL, nR,
                         e0L, e0R,
                         paramL, paramR,
                         qL, gradqSurrealL,
                         qR, gradqSurrealR,
                         integrandLSurreal.data(), nDOFL_,
                         integrandRSurreal.data(), nDOFR_, true );

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDE_qL(i,j) += gradphiL_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemR.PDE_qL(i,j) += gradphiL_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }

      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemL.PDE_qR(i,j) += gradphiR_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemR.PDE_qR(i,j) += gradphiR_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient
}


template<class PDE_>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
template<class Tq, class Tg, class Ti>
void
IntegrandInteriorTrace_Galerkin_manifold<PDE_, sepTraceNormal_GalerkinManifoldInteriorTrace>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
weightedIntegrand( const QuadPointTraceType& sRefTrace,
                   const VectorX& nL, const VectorX& nR,
                   const LocalAxes& e0L, const LocalAxes& e0R,
                   const ParamTL& paramL, const ParamTR& paramR,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                   const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                   ArrayQ<Ti> integrandL[], const int neqnL,
                   ArrayQ<Ti> integrandR[], const int neqnR,
                   bool gradientOnly ) const
{
  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0.0;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0.0;

  // advective flux term: +[[phi]].flux = (phi@L - phi@R) N.flux
  //                                    = +phiL N.flux; -phiR N.flux

  ArrayQ<Ti> FnL = 0.0, FnR = 0.0;     // normal flux (if they are different for left/right elements)

  if (pde_.hasFluxAdvective())
  {
    const int coNrmSignL = xfldElemTrace_.conormalSignL(sRefTrace);
    const int coNrmSignR = xfldElemTrace_.conormalSignR(sRefTrace);

    const LocalAxes e0ReffL = -coNrmSignL*(coNrmSignR*e0R); // effective e0R used by left element
    const LocalAxes e0LeffR = -coNrmSignR*(coNrmSignL*e0L); // effective e0L used by right element
    // Note that e0ReffL (e0LeffR) is e0L (e0R) rotated about the trace into the right element with the same sense of direction as e0L

    ArrayQ<Ti> FnL_dummy = 0.0, FnR_dummy = 0.0; // dummy placeholders
    pde_.fluxAdvectiveUpwind(paramL, paramR, e0L, e0ReffL, qL, qR, nL, nR, FnL, FnR_dummy);
    pde_.fluxAdvectiveUpwind(paramL, paramR, e0LeffR, e0R, qL, qR, nL, nR, FnL_dummy, FnR);

    for (int k = 0; k < neqnL; ++k)
      integrandL[k] +=  phiL_[k]*FnL;

    for (int k = 0; k < neqnR; ++k)
      integrandR[k] += -phiR_[k]*FnR;
  }
}
} // namespace SANS

#endif /* SRC_DISCRETIZATION_GALERKIN_INTEGRANDINTERIORTRACE_GALERKIN_MANIFOLD_H_ */
