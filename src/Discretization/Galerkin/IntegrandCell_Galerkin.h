// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_H
#define INTEGRANDCELL_GALERKIN_H

// cell integrand operator: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template <class PDE_>
class IntegrandCell_Galerkin : public IntegrandCellType< IntegrandCell_Galerkin<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin( const PDE& pde, const std::vector<int>& CellGroups )
    : pde_(pde), cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted(  const PDE& pde,
                    const ElementParam& paramfldElem,
                    const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem ) :
                      pde_(pde),
                      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                      qfldElem_(qfldElem),
                      paramfldElem_( paramfldElem ),
                      nDOF_( qfldElem_.nDOF() ),
                      phi_( new Real[nDOF_] ),
                      gradphi_( new VectorX[nDOF_] )
    {}

    BasisWeighted( BasisWeighted&& bw ) :
                      pde_(bw.pde_),
                      xfldElem_(bw.xfldElem_),
                      qfldElem_(bw.qfldElem_),
                      paramfldElem_( bw.paramfldElem_ ),
                      nDOF_( bw.nDOF_ ),
                      phi_( bw.phi_ ),
                      gradphi_( bw.gradphi_ )
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element residual integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn ) const;

    template<class Ti>
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

  protected:
    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const ParamT& param,
                            const ArrayQ<Tq>& q,
                            const VectorArrayQ<Tg>& gradq,
                            ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem);
  }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<Real, TopoDim, Topology> ElementEFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    FieldWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
                   const Element<Real, TopoDim, Topology>& efldElem) :
                     pde_(pde),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem),
                     wfldElem_(wfldElem),
                     efldElem_(efldElem),
                     paramfldElem_(paramfldElem),
                     nPhi_( efldElem_.nDOF() ),
                     phi_( new Real[nPhi_] ),
                     gradphi_( new VectorX[nPhi_] ),
                     weight_( new ArrayQ<T>[nPhi_] ),
                     gradWeight_( new VectorArrayQ<T>[nPhi_] )
    {
    }

    FieldWeighted( FieldWeighted&& fw ) :
                     pde_(fw.pde_),
                     xfldElem_(fw.xfldElem_),
                     qfldElem_(fw.qfldElem_),
                     wfldElem_(fw.wfldElem_),
                     efldElem_(fw.efldElem_),
                     paramfldElem_(fw.paramfldElem_),
                     nPhi_( fw.nPhi_ ),
                     phi_( fw.phi_ ),
                     gradphi_( fw.gradphi_ ),
                     weight_( fw.weight_ ),
                     gradWeight_( fw.gradWeight_ )
    {
      fw.phi_        = nullptr;
      fw.gradphi_    = nullptr;
      fw.weight_     = nullptr;
      fw.gradWeight_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
      delete [] weight_;
      delete [] gradWeight_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }

    // cell element integrand
    template <class Ti>
    void operator()( const QuadPointType& sRef, Ti integrand[], int nphi ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& wfldElem_;
    const ElementEFieldType& efldElem_;
    const ElementParam& paramfldElem_;

    const int nPhi_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable ArrayQ<T> *weight_;
    mutable VectorArrayQ<T> *gradWeight_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const Element<Real, TopoDim, Topology>& efldElem) const
  {
    return FieldWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, wfldElem, efldElem);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin<PDE>::FieldWeighted<T, TopoDim, Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn) const
{
  SANS_ASSERT(neqn == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                 // solution
  VectorArrayQ<T> gradq;       // gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  // compute the residual
  weightedIntegrand( param, q, gradq, integrand, neqn);
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti>>& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                 // solution
  VectorArrayQ<T> gradq;       // gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  // compute the residual
  DLA::VectorD<ArrayQ<Ti>> integrand(nDOF_);

  weightedIntegrand( param, q, gradq, integrand.data(), integrand.size());

  for (int k = 0; k < nDOF_; ++k)
    rsdPDEElem(k) += dJ*integrand(k);
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxPDEElem.m() == nDOF_);
  SANS_ASSERT(mtxPDEElem.n() == nDOF_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;                 // solution
  VectorArrayQ<Real> gradq;       // gradient

  ArrayQ<SurrealClass> qSurreal = 0;               // solution
  VectorArrayQ<SurrealClass> gradqSurreal = 0;     // gradient

  MatrixQ<Real> PDE_q =0 , PDE_gradq = 0; // temporary storage

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qSurreal = q;

  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    gradqSurreal = gradq;
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    weightedIntegrand( param, qSurreal, gradq, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;
  } // nchunk


  if (needsSolutionGradient)
  {
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandSurreal = 0;

      weightedIntegrand( param, q, gradqSurreal, integrandSurreal.data(), integrandSurreal.size());

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOF_; j++)
              mtxPDEElem(i,j) += dJ*gradphi_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  }
}

// Cell integrand
//
// integrand = - grad(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tg, class Ti>
void
IntegrandCell_Galerkin<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
weightedIntegrand( const ParamT& param,
                   const ArrayQ<Tq>& q,
                   const VectorArrayQ<Tg>& gradq,
                   ArrayQ<Ti> integrand[], int neqn ) const
{
  VectorArrayQ<Ti> F;           // PDE flux F(X, Q), Fv(X, Q, QX)
  ArrayQ<Ti> source;            // PDE source S(X, Q, QX)

  // using neqn here suppresses clang analyzer
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // Galerkin flux term: -gradient(phi) . F

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    F = 0;

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, q, gradq, F );

    for (int k = 0; k < neqn; k++)
      integrand[k] -= dot(gradphi_[k],F);
  }

  // Galerkin source term: +phi S

  if (pde_.hasSource())
  {
    source = 0;

    pde_.source( param, q, gradq, source );

    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < neqn; k++)
      integrand[k] -= phi_[k]*forcing;
  }
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin<PDE>::
FieldWeighted<T, TopoDim, Topology, ElementParam>::
operator()( const QuadPointType& sRef, Ti integrand[], int nphi ) const
{
  SANS_ASSERT( nphi == nPhi_ );

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;               // solution
  VectorArrayQ<T> gradq;     // solution gradient

  ArrayQ<T> w;               // weight
  VectorArrayQ<T> gradw;     // weight gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // solution value
  qfldElem_.eval( sRef, q );

  // solution gradient, hessian
  if (needsSolutionGradient)
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );

  // weight value, gradient
  wfldElem_.eval( sRef, w );
  xfldElem_.evalGradient( sRef, wfldElem_, gradw );

  // estimate basis, gradient
  efldElem_.evalBasis( sRef, phi_, nPhi_ );
  xfldElem_.evalBasisGradient( sRef, efldElem_, gradphi_, nPhi_ );

  for (int k = 0; k < nPhi_; k++)
    integrand[k] = 0;

  // for (int n = 0; n < N; n++)
  for (int k = 0; k < nPhi_; k++)
  {
    // DLA::index(weight_[k],n) = DLA::index(w,n) * phi_[k];
    weight_[k] = w * phi_[k];

    // for (int d = 0; d < PhysDim::D; d++)
    //   DLA::index(gradWeight_[k][d],n)  = DLA::index(w,n) * gradphi_[k][d] + phi_[k]*DLA::index(gradw[d],n);
    gradWeight_[k] = gradphi_[k]*w + phi_[k]*gradw;

  }

  // Galerkin flux term: -(phi grad w + w grad phi, F)
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;
    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, q, gradq, F );

    for (int d = 0; d < PhysDim::D; d++)
      for (int k = 0; k < nPhi_; k++)
      integrand[k] -= dot(gradWeight_[k][d],F[d]);
  }

  // Galerkin source term: +w S

  if (pde_.hasSource())
  {
    ArrayQ<Ti> source=0;   // PDE source S(X, D, U, UX), S(X)
    pde_.source( param, q, gradq, source );

    for (int k = 0; k < nPhi_; k++)
      integrand[k] += dot(weight_[k],source);
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < nPhi_; k++)
      integrand[k] -= dot(weight_[k],forcing);
  }
}


}

#endif  // INTEGRANDCELL_GALERKIN_H
