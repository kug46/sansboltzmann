// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALCELL_GALERKIN_H
#define RESIDUALCELL_GALERKIN_H

// Cell integral residual functions

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h" //Line 46

namespace SANS
{

// forward declaration
template<class IntegrandCell, template<class> class Vector, class TR>
class ResidualCell_Galerkin_impl;

//----------------------------------------------------------------------------//
// Field Cell group residual
//
// topology specific group residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   IntegrandCell_Galerkin            integrand class
//   XFieldType                        Grid data type, possibly a tuple with parameters

//----------------------------------------------------------------------------//
//  Galerkin cell group integral
//

template<class IntegrandCell, template<class> class Vector, class TR>
class ResidualCell_Galerkin_impl :
    public GroupIntegralCellType< ResidualCell_Galerkin_impl<IntegrandCell, Vector, TR> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template ArrayQ<TR> ArrayQR;

  typedef typename DLA::VectorD<ArrayQR> ResidualElemClass;

  // Save off the cell integrand and the residual vector
  ResidualCell_Galerkin_impl( const IntegrandCell& fcn,
                              Vector<ArrayQR>& rsdPDEGlobal) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // number of integrals evaluated
    const int nIntegrand = qfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nIntegrand, -1 );
    std::vector<int> mapDOFLocal( nIntegrand, -1 );
    std::vector<int> maprsd( nIntegrand, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = rsdPDEGlobal_.m();

    // residual array
    ResidualElemClass rsdPDEElem( nIntegrand );

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, ResidualElemClass> integral(quadratureorder);

    auto integrandFunctor = fcn_.integrand( xfldElem, qfldElem );
    // Create a functor only once for speed.  Note that the parameters are passed into the functor as const references,
    // but their values can still be modified by agents other than this functor.

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), mapDOFGlobal.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nIntegrand; n++)
      {
        if (mapDOFGlobal[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
          nDOFLocal++;
        }
      }

      // no need if all DOFs are possessed by other processors
      if (nDOFLocal == 0) continue;

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      for (int n = 0; n < nIntegrand; n++)
        rsdPDEElem[n] = 0;

      // cell integration for canonical element
      integral( integrandFunctor, get<-1>(xfldElem), rsdPDEElem );

      // scatter-add element integral to global
      for (int n = 0; n < nDOFLocal; n++)
        rsdPDEGlobal_[ mapDOFLocal[n] ] += rsdPDEElem[ maprsd[n] ];
    }
  }

protected:
  const IntegrandCell& fcn_;
  Vector<ArrayQR>& rsdPDEGlobal_;
};

// Factory function

template<class IntegrandCell, template<class> class Vector, class ArrayQ>
ResidualCell_Galerkin_impl<IntegrandCell, Vector, typename Scalar<ArrayQ>::type>
ResidualCell_Galerkin( const IntegrandCellType<IntegrandCell>& fcn,
                       Vector<ArrayQ>& rsdPDEGlobal)
{
  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same<ArrayQ, typename IntegrandCell::template ArrayQ<T> >::value, "These should be the same.");
  return ResidualCell_Galerkin_impl<IntegrandCell, Vector, T>(fcn.cast(), rsdPDEGlobal);
}


} //namespace SANS

#endif  // RESIDUALCELL_GALERKIN_H
