// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_CUTCELLTRANSITIONIBL_H_
#define SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_CUTCELLTRANSITIONIBL_H_

#include "pde/IBL/PDEIBL2D.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/SurrealizedElementTuple.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "IntegrandCell_Galerkin_CutCellTransitionIBL.h"

namespace SANS
{

// forward declaration
template<class Surreal, class IntegrandCell>
class JacobianCell_Galerkin_TransitionIBL_impl;

//----------------------------------------------------------------------------//
//  Galerkin cell integral
//
template<class Surreal, class VarType,
         template<class, class> class PDENDConvert>
class JacobianCell_Galerkin_TransitionIBL_impl<
        Surreal, IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > > > :
    public GroupIntegralCellType<
             JacobianCell_Galerkin_TransitionIBL_impl< Surreal,
               IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >
                                                     >
                                >
{
public:
  typedef IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > > IntegrandCellClass;

  typedef typename IntegrandCellClass::PhysDim PhysDim;
  typedef typename IntegrandCellClass::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCellClass::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandCellClass::template ArrayQ<Surreal> ArrayQSurreal;

  typedef typename DLA::VectorD<ArrayQSurreal> ResidualElemSurrealClass;
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  static const int iParam = 0; // TODO: hard coded index for parameter field (Qauxv) in the tuple field of IBL

  JacobianCell_Galerkin_TransitionIBL_impl(const IntegrandCellClass& fcn,
                             MatrixScatterAdd<MatrixQ>& mtxGlobalMATCH_qMatch,
                             MatrixScatterAdd<MatrixQ>& mtxGlobalMATCH_q,
                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qMatch,
                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q ) :
    fcn_(fcn),
    mtxGlobalMATCH_qMatch_(mtxGlobalMATCH_qMatch),
    mtxGlobalMATCH_q_(mtxGlobalMATCH_q),
    mtxGlobalPDE_qMatch_(mtxGlobalPDE_qMatch),
    mtxGlobalPDE_q_(mtxGlobalPDE_q) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

  //----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim, class QBundleFieldType>
  void check( const QBundleFieldType& qfld )
  {
    SANS_ASSERT( mtxGlobalMATCH_qMatch_.m() == qfld.left().nDOFpossessed() );
    SANS_ASSERT( mtxGlobalMATCH_qMatch_.n() == qfld.left().nDOF() );

    SANS_ASSERT( mtxGlobalMATCH_q_.m() == qfld.left().nDOFpossessed() );
    SANS_ASSERT( mtxGlobalMATCH_q_.n() == qfld.right().nDOF() );

    SANS_ASSERT( mtxGlobalPDE_qMatch_.m() == qfld.right().nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_qMatch_.n() == qfld.left().nDOF() );

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.right().nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.right().nDOF() );
  }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template<class Topology, class TopoDim, class TupleFieldType, class QBundleFieldCellGroupType>
  void
  integrate( const int cellGroupGlobal,
             const typename TupleFieldType::template FieldCellGroupType<Topology>& tuplefldCell,
             const QBundleFieldCellGroupType& qbundlefldCell,
             const int quadratureorder )
  {
    typedef typename TupleFieldType::template FieldCellGroupType<Topology> TupleFieldCellGroupType;
    static_assert(std::is_same<Topology, typename QBundleFieldCellGroupType::TopologyType>::value, "Wrong topology type of qbundlefldCell!");

    typedef typename QBundleFieldCellGroupType::FieldGroupTypeL QMatchFieldCellGroupType;
    typedef typename QBundleFieldCellGroupType::FieldGroupTypeR QFieldCellGroupType;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupType, Surreal, iParam>::type ElementTupleFieldClass;
    // TODO: always Surrealizing the tuple field element is easy to implement but expensive to run
    typedef typename QMatchFieldCellGroupType::template ElementType<Surreal> ElementQMatchFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldClass;

    const QMatchFieldCellGroupType& qmatchfldCell = qbundlefldCell.left();
    const QFieldCellGroupType& qfldCell = qbundlefldCell.right();

    // element field variables
    ElementTupleFieldClass tuplefldElem( tuplefldCell.basis() );
    ElementQMatchFieldClass qmatchfldElem( qmatchfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // DOFs per element
    const int nDOFMATCH = qmatchfldElem.nDOF();
    const int nDOFPDE = qfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFMATCHGlobal( nDOFMATCH, -1 );
    std::vector<int> mapDOFMATCHLocal( nDOFMATCH, -1 );
    std::vector<int> maprsdMATCH( nDOFMATCH, -1 );
    int nDOFMATCHLocal = 0;
    SANS_ASSERT_MSG(mtxGlobalMATCH_qMatch_.m() == mtxGlobalMATCH_q_.m(), "Mismatching global jacobian sizes!");
    const int nDOFMATCHpossessed = mtxGlobalMATCH_qMatch_.m();

    std::vector<int> mapDOFPDEGlobal( nDOFPDE, -1 );
    std::vector<int> mapDOFPDELocal( nDOFPDE, -1 );
    std::vector<int> maprsdPDE( nDOFPDE, -1 );
    int nDOFPDELocal = 0;
    SANS_ASSERT_MSG(mtxGlobalPDE_q_.m() == mtxGlobalPDE_qMatch_.m(), "Mismatching global jacobian sizes!");
    const int nDOFPDEpossessed = mtxGlobalPDE_q_.m();

    // Surrealized element integrand/residuals
    ResidualElemSurrealClass rsdElemMATCH( nDOFMATCH );
    ResidualElemSurrealClass rsdElemPDE( nDOFPDE );

    // element jacobian matrix
    MatrixElemClass mtxElemMATCH_qMatch(nDOFMATCH, nDOFMATCH);
    MatrixElemClass mtxElemMATCH_q(nDOFMATCH, nDOFPDE);

    MatrixElemClass mtxElemPDE_qMatch(nDOFPDE, nDOFMATCH);
    MatrixElemClass mtxElemPDE_q(nDOFPDE, nDOFPDE);

    // element integral
    GalerkinWeightedIntegral_New_TransitionIBL<TopoDim, Topology, ResidualElemSurrealClass> integral(quadratureorder);

    // The integrand to be integrated
    auto integrand = fcn_.integrand(tuplefldElem, qmatchfldElem, qfldElem);

    // just to make sure things are consistent
    SANS_ASSERT( tuplefldCell.nElem() == qbundlefldCell.nElem() );

    // variables/equations per DOF
    const int nEqn = IntegrandCellClass::N;

    // number of simultaneous derivatives (i.e. chunk) automatic differentiation functor call
    SANS_ASSERT_MSG(DLA::index(qmatchfldElem.DOF(0), 0).size() == nEqn, "Assumed AD derivative count fails!");
    SANS_ASSERT_MSG(DLA::index(qfldElem.DOF(0), 0).size() == nEqn, "Assumed AD derivative count fails!");
    const int nDeriv = DLA::index(qfldElem.DOF(0), 0).size();

    // loop over elements within group
    const int nelem = tuplefldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qmatchfldCell.associativity( elem ).getGlobalMapping( mapDOFMATCHGlobal.data(), mapDOFMATCHGlobal.size() );
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFPDEGlobal.data(), mapDOFPDEGlobal.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFMATCHLocal = 0;
      for (int n = 0; n < nDOFMATCH; n++)
      {
        if (mapDOFMATCHGlobal[n] < nDOFMATCHpossessed)
        {
          maprsdMATCH[nDOFMATCHLocal] = n;
          mapDOFMATCHLocal[nDOFMATCHLocal] = mapDOFMATCHGlobal[n];
          nDOFMATCHLocal++;
        }
      }

      nDOFPDELocal = 0;
      for (int n = 0; n < nDOFPDE; n++)
      {
        if (mapDOFPDEGlobal[n] < nDOFPDEpossessed)
        {
          maprsdPDE[nDOFPDELocal] = n;
          mapDOFPDELocal[nDOFPDELocal] = mapDOFPDEGlobal[n];
          nDOFPDELocal++;
        }
      }

      // no residuals are possessed by this processor
      if (nDOFMATCHLocal == 0 && nDOFPDELocal == 0) continue;

      // copy global grid/solution DOFs to element
      tuplefldCell.getElement( tuplefldElem, elem );
      qmatchfldCell.getElement( qmatchfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // initialize element Jacobians to zero
      mtxElemMATCH_qMatch = 0.0;
      mtxElemMATCH_q = 0.0;
      mtxElemPDE_qMatch = 0.0;
      mtxElemPDE_q = 0.0;

      // compute jacobians via automatic differentiation & loop over derivative chunks &
      for (int nchunk = 0; nchunk < nEqn*(nDOFMATCH + nDOFPDE); nchunk += nDeriv)
      {
        // associate derivative slots with solution variables
        int slot;
        for (int j = 0; j < nDOFMATCH; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qmatchfldElem.DOF(j), n).deriv(k) = 0.0; // TODO: this is safe but can be slow

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qmatchfldElem.DOF(j), n).deriv(slot - nchunk) = 1.0;
          }
        }
        for (int j = 0; j < nDOFPDE; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElem.DOF(j), n).deriv(k) = 0.0; // TODO: this is safe but can be slow

            slot = nEqn*nDOFMATCH + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElem.DOF(j), n).deriv(slot - nchunk) = 1.0;
          }
        }

        // reset residuals
        rsdElemMATCH = 0.0;
        rsdElemPDE = 0.0;

        // evaluate surrealized element residual
        integral( integrand, get<-1>(tuplefldElem), rsdElemMATCH, rsdElemPDE );
        // xfld should always be the last component of tuplefldElem

        // accumulate derivatives into element jacobians

        for (int j = 0; j < nDOFMATCH; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFMATCHLocal; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemMATCH_qMatch(i,j), m,n) = DLA::index(rsdElemMATCH[i], m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFPDELocal; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemPDE_qMatch(i,j), m,n) = DLA::index(rsdElemPDE[i], m).deriv(slot - nchunk);
            }
          }
        }

        for (int j = 0; j < nDOFPDE; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*nDOFMATCH + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFMATCHLocal; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemMATCH_q(i,j), m,n) = DLA::index(rsdElemMATCH[i], m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFPDELocal; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemPDE_q(i,j), m,n) = DLA::index(rsdElemPDE[i], m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global
      if (nDOFMATCHLocal == nDOFMATCH && nDOFPDELocal == nDOFPDE)
      {
        scatterAdd( elem, mapDOFMATCHLocal, mapDOFMATCHGlobal, mtxElemMATCH_qMatch, mtxGlobalMATCH_qMatch_ );
        scatterAdd( elem, mapDOFMATCHLocal, mapDOFPDEGlobal, mtxElemMATCH_q, mtxGlobalMATCH_q_ );
        scatterAdd( elem, mapDOFPDELocal, mapDOFMATCHGlobal, mtxElemPDE_qMatch, mtxGlobalPDE_qMatch_ );
        scatterAdd( elem, mapDOFPDELocal, mapDOFPDEGlobal, mtxElemPDE_q, mtxGlobalPDE_q_ );
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION("Not fully implemented yet!"); //TODO

//        // compress in only the DOFs possessed by this processor
//        for (int i = 0; i < nDOFPDELocal; i++)
//          for (int j = 0; j < nDOFPDE; j++)
//            mtxElemLocalPDE_q(i,j) = mtxElemPDE_q(maprsdPDE[i],j);
//
//        scatterAdd( elem, mapDOFPDELocal, mapDOFPDEGlobal, mtxElemLocalPDE_q, mtxGlobalPDE_q_ );
      }
    }
  }

protected:
  //----------------------------------------------------------------------------//
  template <class MatrixQ, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const int elem,
      std::vector<int>& mapDOFLocal,
      std::vector<int>& mapDOFGlobal,
      SANS::DLA::MatrixD<MatrixQ>& mtxElem,
      SparseMatrixType<MatrixQ>& mtxGlobal )
  {
    mtxGlobal.scatterAdd( mtxElem, mapDOFLocal.data(), mapDOFLocal.size(), mapDOFGlobal.data(), mapDOFGlobal.size() );
  }

protected:
  const IntegrandCellClass& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalMATCH_qMatch_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalMATCH_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qMatch_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
};

// Factory function

template<class Surreal, class IntegrandCell, class MatrixQ>
JacobianCell_Galerkin_TransitionIBL_impl<Surreal, IntegrandCell>
JacobianCell_Galerkin_TransitionIBL(
    const IntegrandCellType<IntegrandCell>& fcn,
    MatrixScatterAdd<MatrixQ>& mtxGlobalMATCH_qMatch,
    MatrixScatterAdd<MatrixQ>& mtxGlobalMATCH_q,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qMatch,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q )
{
  return { fcn.cast(), mtxGlobalMATCH_qMatch, mtxGlobalMATCH_q, mtxGlobalPDE_qMatch, mtxGlobalPDE_q };
}

} //namespace SANS

#endif /* SRC_DISCRETIZATION_GALERKIN_JACOBIANCELL_GALERKIN_CUTCELLTRANSITIONIBL_H_ */
