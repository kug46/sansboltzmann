// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_RESIDUALCELL_GALERKIN_H_
#define SRC_DISCRETIZATION_GALERKIN_RESIDUALCELL_GALERKIN_H_

// Cell integral residual functions customized for IBL with cut-cell transition

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "IntegrandCell_Galerkin_CutCellTransitionIBL.h"

namespace SANS
{

// forward declaration
template<class IntegrandCell, template<class> class Vector, class TR>
class ResidualCell_Galerkin_impl;

//----------------------------------------------------------------------------//
// Field Cell group residual
//
// topology specific group residual
//
// template parameters:
//   Topology                                     element topology (e.g. Triangle)
//   IntegrandCell_Galerkin_cutCellTransitionIBL  integrand class
//   XFieldType                                   Grid data type, possibly a tuple with parameters

//----------------------------------------------------------------------------//
//  Galerkin cell group integral class specialized for cut-cell transition IBL with a matching field (for transition front conservation laws)

template<class VarType,
         template<class, class> class PDENDConvert,
         template<class> class Vector, class TR>
class ResidualCell_Galerkin_impl< IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >,
                                  Vector, TR > :
    public GroupIntegralCellType<
             ResidualCell_Galerkin_impl<
               IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >,
               Vector, TR> >
{
public:
  typedef IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > > IntegrandCellClass;

  typedef typename IntegrandCellClass::PhysDim PhysDim;
  typedef typename IntegrandCellClass::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCellClass::template ArrayQ<TR> ArrayQR;

  typedef typename DLA::VectorD<ArrayQR> ResidualElemClass;

  // Save off the cell integrand and the residual vector
  ResidualCell_Galerkin_impl( const IntegrandCellClass& fcn,
                              Vector<ArrayQR>& rsdMATCHGlobal,
                              Vector<ArrayQR>& rsdPDEGlobal) :
    fcn_(fcn),
    rsdMATCHGlobal_(rsdMATCHGlobal),
    rsdPDEGlobal_(rsdPDEGlobal) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim, class QBundleFieldType>
  void check( const QBundleFieldType& qfld ) const
  {
    SANS_ASSERT( rsdMATCHGlobal_.m() == qfld.left().nDOFpossessed() );
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.right().nDOFpossessed() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType, class QBundleFieldCellGroupType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& tuplefldCell,
             const QBundleFieldCellGroupType& qbundlefldCell,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    static_assert(std::is_same<Topology, typename QBundleFieldCellGroupType::TopologyType>::value, "Wrong topology type of qbundlefldCell!");

    typedef typename QBundleFieldCellGroupType::FieldGroupTypeL QMatchFieldCellGroupType;
    typedef typename QBundleFieldCellGroupType::FieldGroupTypeR QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QMatchFieldCellGroupType::template ElementType<> ElementQMatchFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const QMatchFieldCellGroupType& qmatchfldCell = qbundlefldCell.left();
    const QFieldCellGroupType& qfldCell = qbundlefldCell.right();

    // element field variables
    ElementXFieldClass tuplefldElem( tuplefldCell.basis() );
    ElementQMatchFieldClass qmatchfldElem( qmatchfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // number of integrals evaluated
    const int nDOFMATCH = qmatchfldElem.nDOF();
    const int nDOFPDE = qfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFMATCHGlobal( nDOFMATCH, -1 );
    std::vector<int> mapDOFMATCHLocal( nDOFMATCH, -1 );
    std::vector<int> maprsdElemMATCH( nDOFMATCH, -1 );
    int nDOFMATCHLocal = 0;
    const int nDOFMATCHpossessed = rsdMATCHGlobal_.m();

    std::vector<int> mapDOFPDEGlobal( nDOFPDE, -1 );
    std::vector<int> mapDOFPDELocal( nDOFPDE, -1 );
    std::vector<int> maprsdElemPDE( nDOFPDE, -1 );
    int nDOFPDELocal = 0;
    const int nDOFPDEpossessed = rsdPDEGlobal_.m();

    // residual array
    ResidualElemClass rsdElemMATCH( nDOFMATCH );
    ResidualElemClass rsdElemPDE( nDOFPDE );

    // element integral
    GalerkinWeightedIntegral_New_TransitionIBL<TopoDim, Topology, ResidualElemClass> integral(quadratureorder);

    auto integrandFunctor = fcn_.integrand( tuplefldElem, qmatchfldElem, qfldElem );
    // Create a functor only once for speed.  Note that the parameters are passed into the functor as const references,
    // but their values can still be modified by agents other than this functor.

    // just to make sure things are consistent
    SANS_ASSERT( tuplefldCell.nElem() == qbundlefldCell.nElem() );

    // loop over elements within group
    const int nelem = tuplefldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qmatchfldCell.associativity( elem ).getGlobalMapping( mapDOFMATCHGlobal.data(), mapDOFMATCHGlobal.size() );
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFPDEGlobal.data(), mapDOFPDEGlobal.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFMATCHLocal = 0;
      for (int n = 0; n < nDOFMATCH; n++)
      {
        if (mapDOFMATCHGlobal[n] < nDOFMATCHpossessed)
        {
          maprsdElemMATCH[nDOFMATCHLocal] = n;
          mapDOFMATCHLocal[nDOFMATCHLocal] = mapDOFMATCHGlobal[n];
          nDOFMATCHLocal++;
        }
      }

      nDOFPDELocal = 0;
      for (int n = 0; n < nDOFPDE; n++)
      {
        if (mapDOFPDEGlobal[n] < nDOFPDEpossessed)
        {
          maprsdElemPDE[nDOFPDELocal] = n;
          mapDOFPDELocal[nDOFPDELocal] = mapDOFPDEGlobal[n];
          nDOFPDELocal++;
        }
      }

      // no need to compute residuals if all the equations are possessed by other processors
      if (nDOFMATCHLocal == 0 && nDOFPDELocal == 0) continue;

      // copy global grid/solution DOFs to element
      tuplefldCell.getElement( tuplefldElem, elem );
      qmatchfldCell.getElement( qmatchfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // reset residuals
      rsdElemMATCH = 0.0;
      rsdElemPDE = 0.0;

      // calculate element integral residuals
      integral( integrandFunctor, get<-1>(tuplefldElem), rsdElemMATCH, rsdElemPDE );

      // add element residuals to global
      for (int n = 0; n < nDOFMATCH; n++)
        rsdMATCHGlobal_[ mapDOFMATCHLocal[n] ] += rsdElemMATCH[ maprsdElemMATCH[n] ];

      for (int n = 0; n < nDOFPDELocal; n++)
        rsdPDEGlobal_[ mapDOFPDELocal[n] ] += rsdElemPDE[ maprsdElemPDE[n] ];
    }
  }

protected:
  const IntegrandCellClass& fcn_;
  Vector<ArrayQR>& rsdMATCHGlobal_;
  Vector<ArrayQR>& rsdPDEGlobal_;
};

// Factory function

template<class IntegrandCell, template<class> class Vector, class ArrayQ>
ResidualCell_Galerkin_impl<IntegrandCell, Vector, typename Scalar<ArrayQ>::type>
ResidualCell_Galerkin( const IntegrandCellType<IntegrandCell>& fcn,
                       Vector<ArrayQ>& rsdMATCHGlobal,
                       Vector<ArrayQ>& rsdPDEGlobal )
{
  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same<ArrayQ, typename IntegrandCell::template ArrayQ<T> >::value, "ArrayQ type should be consistent!");
  return ResidualCell_Galerkin_impl<IntegrandCell, Vector, T>(fcn.cast(), rsdMATCHGlobal, rsdPDEGlobal);
}

} //namespace SANS

#endif  // SRC_DISCRETIZATION_GALERKIN_RESIDUALCELL_GALERKIN_H_
