// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_GALERKIN_ELEMENT_H
#define JACOBIANINTERIORTRACE_GALERKIN_ELEMENT_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Discretization/JacobianElementMatrix.h"

namespace SANS
{

template<class MatrixQ>
struct JacobianElemInteriorTrace_Galerkin : JacElemMatrixType< JacobianElemInteriorTrace_Galerkin<MatrixQ> >
{
  // PDE Jacobian wrt q
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianElemInteriorTrace_Galerkin(const JacobianElemInteriorTraceSize& size)
   : nTest(size.nTest),
     nDOFL(size.nDOFL), nDOFR(size.nDOFR),
     PDE_qL(size.nTest, size.nDOFL),
     PDE_qR(size.nTest, size.nDOFR)
  {}

  const int nTest;
  const int nDOFL;
  const int nDOFR;

  // element PDE jacobian matrices wrt q
  MatrixElemClass PDE_qL;
  MatrixElemClass PDE_qR;

  inline Real operator=( const Real s )
  {
    PDE_qL = s;
    PDE_qR = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemInteriorTrace_Galerkin& operator+=(
      const JacElemMulScalar< JacobianElemInteriorTrace_Galerkin >& mul )
  {
    PDE_qL += mul.s*mul.mtx.PDE_qL;
    PDE_qR += mul.s*mul.mtx.PDE_qR;

    return *this;
  }
};

}
#endif // JACOBIANINTERIORTRACE_DGBR2_ELEMENT_H
