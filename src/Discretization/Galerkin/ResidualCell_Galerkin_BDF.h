// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALCELL_GALERKIN_BDF_H
#define RESIDUALCELL_GALERKIN_BDF_H

// Cell BDF integral residual functions

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/FieldSequence.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  BDF cell group integral
//

template<class IntegrandCell, template<class> class Vector>
class ResidualCell_Galerkin_BDF_impl :
    public GroupIntegralCellType< ResidualCell_Galerkin_BDF_impl<IntegrandCell, Vector> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualCell_Galerkin_BDF_impl( const IntegrandCell& fcn,
                                  Vector<ArrayQ>& rsdPDEGlobal ) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
                               FieldSequence<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld               = get<0>(flds);
    const FieldSequence< PhysDim, TopoDim, ArrayQ >& qfldPast = get<1>(flds);

    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( rsdPDEGlobal_.m() == qfldPast.nDOFpossessed() );
  }


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class ParamFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename ParamFieldType::template FieldCellGroupType<Topology>& pfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
                                       FieldSequence<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename ParamFieldType                           ::template FieldCellGroupType<Topology> PFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ         >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldSequence< PhysDim, TopoDim, ArrayQ >::template FieldCellGroupType<Topology> QFieldSeqCellGroupType;
    typedef typename XField< PhysDim, TopoDim                >::template FieldCellGroupType<Topology> XFieldCellGroupType;

    typedef typename PFieldCellGroupType::template ElementType<> ElementPFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename QFieldSeqCellGroupType::template ElementType<> ElementQFieldSeqClass;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    const QFieldCellGroupType&    qfldCell     = get<0>(fldsCell);
    const QFieldSeqCellGroupType& qfldCellPast = get<1>(fldsCell);

    SANS_ASSERT( pfldCell.nElem() == qfldCell.nElem() );
    // element field variables
    ElementPFieldClass pfldElem( pfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldSeqClass qfldElemPast( qfldCellPast.basis() );

    // number of integrals evaluated
    const int nDOF = qfldElem.nDOF();
    const int nIntegrand = nDOF;

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, ArrayQ > integral(quadratureorder, nIntegrand);

    // just to make sure things are consistent
    SANS_ASSERT( pfldCell.nElem() == qfldCell.nElem() );

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nIntegrand, -1 );
//    std::vector<int> mapDOFGlobalPast( nIntegrand, -1 );
    std::vector<int> mapDOFLocal( nIntegrand, -1 );
    std::vector<int> maprsd( nIntegrand, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = rsdPDEGlobal_.m();

    // residual array
    std::vector< ArrayQ > rsdElem( nIntegrand );

    // loop over elements within group
    const int nelem = pfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nIntegrand );
//      qfldCellPast[0].associativity( elem ).getGlobalMapping( mapDOFGlobalPast.data(), nIntegrand );

//      for (int n = 0; n < nIntegrand; n++)
//        SANS_ASSERT(mapDOFGlobal[n] == mapDOFGlobalPast[n]);

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nIntegrand; n++)
        if (mapDOFGlobal[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
          nDOFLocal++;
        }

      // no need if all DOFs are possessed by other processors
      if (nDOFLocal == 0) continue;

      // copy global grid/solution DOFs to element
      pfldCell.getElement( pfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qfldCellPast.getElement( qfldElemPast, elem );

      for (int n = 0; n < nIntegrand; n++)
        rsdElem[n] = 0;

      // cell integration for canonical element
      integral( fcn_.integrand(pfldElem, qfldElem, qfldElemPast),
                get<ElementXFieldClass>(pfldElem), rsdElem.data(), nIntegrand );

      // scatter-add element integral to global
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nDOF );

      // scatter-add element integral to global
      for (int n = 0; n < nDOFLocal; n++)
        rsdPDEGlobal_[ mapDOFLocal[n] ] += rsdElem[ maprsd[n] ];

    }
  }

protected:
  const IntegrandCell& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
};

// Factory function

template<class IntegrandCell, class ArrayQ, template<class> class Vector>
ResidualCell_Galerkin_BDF_impl<IntegrandCell, Vector>
ResidualCell_Galerkin_BDF( const IntegrandCellType<IntegrandCell>& fcn,
                           Vector<ArrayQ>& rsdPDEGlobal)
{
  static_assert( std::is_same< ArrayQ, typename IntegrandCell::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualCell_Galerkin_BDF_impl<IntegrandCell, Vector>(fcn.cast(), rsdPDEGlobal);
}

}

#endif  // RESIDUALCELL_GALERKIN_BDF_H
