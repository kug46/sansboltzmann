// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL_H
#define AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL_H

#include <type_traits>
#include <utility> // std::forward

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>
#include "tools/plus_std_vector.h"
#endif

#include "tools/KahanSum.h"
#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"
#include "Discretization/ResidualNormType.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin_Param.h"

#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_WakeMatch_Galerkin_IBL.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class AlgebraicEquationSet>
class AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL : public AlgebraicEquationSet
{
public:
  typedef typename AlgebraicEquationSet::NDPDEClass NDPDEClass;
  typedef typename AlgebraicEquationSet::PhysDim PhysDim;
  typedef typename AlgebraicEquationSet::TopoDim TopoDim;
  typedef typename AlgebraicEquationSet::ArrayQ ArrayQ;
  typedef typename AlgebraicEquationSet::MatrixQ MatrixQ;

  typedef typename AlgebraicEquationSet::TraitsType TraitsType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  template<class ArrayQT>
  using SystemVectorTemplate = typename TraitsType::template SystemVectorTemplate<ArrayQT>;

  typedef typename AlgebraicEquationSet::BCParams BCParams;
  typedef typename AlgebraicEquationSet::IntegrateBoundaryTrace_DispatchClass IntegrateBoundaryTrace_DispatchClass;

  typedef SurrealS<NDPDEClass::N> SurrealQClass;
  typedef SurrealS<NDPDEClass::Nparam> SurrealParamClass;

  // Indexes to order the equations and the solution vectors
  using AlgebraicEquationSet::iMATCH;
  using AlgebraicEquationSet::iPDE;
  using AlgebraicEquationSet::iBC;

  using AlgebraicEquationSet::iqMatch;
  using AlgebraicEquationSet::iq;
  using AlgebraicEquationSet::ilg;

  static const int iHT = iBC+1;
  static const int ihb = ilg+1;

  static const int nEqnSet = iHT+1;
  static const int nSolSet = ihb+1;
  static_assert(nEqnSet == nSolSet, "");


  template<class... ICArgs>
  AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL(Field<PhysDim, TopoDim, ArrayQ>& hbfld,
                                         ICArgs&&... args ) :
    AlgebraicEquationSet(std::forward<ICArgs>(args)...),
    hbfld_(hbfld)
  {
    SANS_ASSERT( tol_.size() >= nEqnSet );
  }

  AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL(const AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL&) = delete;
  AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL& operator=(const AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL&) = delete;

  virtual ~AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL() {}

  using AlgebraicEquationSet::residual;
  using AlgebraicEquationSet::jacobian;
  using AlgebraicEquationSet::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx) override
  {
    // First get the jacobian from the base class
    AlgebraicEquationSet::jacobian(mtx);

    this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_);
  }
  virtual void jacobian(SystemNonZeroPatternView& nz) override
  {
    // First get the jacobian from the base class
    AlgebraicEquationSet::jacobian(nz);

    this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_);
  }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT) override
  {
    // First get the jacobian from the base class
    AlgebraicEquationSet::jacobianTranspose(mtxT);

    jacobian(Transpose(mtxT), quadratureOrder_);
  }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override
  {
    // First get the jacobian from the base class
    AlgebraicEquationSet::jacobianTranspose(nzT);

    jacobian(Transpose(nzT), quadratureOrderMin_);
  }

  // Used to compute jacobians wrt. parameters
  template<int iParam, class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const;

  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override
  {
    AlgebraicEquationSet::residualInfo(titles, idx);
    titles.push_back("HT  : ");
    idx.push_back(iHT);
  }

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real> > residualNorm( const SystemVectorView& rsd ) const override
  {
    const int nDOFMATCH = rsd[iMATCH].m();
    const int nDOFPDEpossessed = rsd[iPDE].m();
    const int nDOFBC = rsd[iBC].m();
    const int nDOFHT = hbfld_.nDOF();
    const int nMon = pde_.nMonitor();

    DLA::VectorD<Real> rsdMATCHtmp(nMon); rsdMATCHtmp = 0.0;
    DLA::VectorD<Real> rsdPDEtmp(nMon);   rsdPDEtmp = 0.0;
    DLA::VectorD<Real> rsdBCtmp(nMon);    rsdBCtmp = 0.0;
    DLA::VectorD<Real> rsdHTtmp(nMon);    rsdHTtmp = 0;

    std::vector<std::vector<Real> > rsdNorm(nResidNorm(), std::vector<Real>(nMon, 0.0));
    std::vector<KahanSum<Real> > rsdNormKahan(nMon, 0.0);

    //Matching residual norm
    for (int n = 0; n < nDOFMATCH; n++)
    {
      pde_.interpResidVariable(rsd[iMATCH][n], rsdMATCHtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iMATCH][j] += pow(rsdMATCHtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iMATCH][j] = sqrt(rsdNorm[iMATCH][j]);

    //PDE residual norm
    if (resNormType_ == ResidualNorm_L2)
    {
      for (int n = 0; n < nDOFPDEpossessed; n++)
      {
        pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

        for (int j = 0; j < nMon; j++)
          rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
      }

      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] = rsdNormKahan[j];

  #ifdef SANS_MPI
      rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
  #endif

      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);
    }
    else
      SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Project::residualNorm - Unknown residual norm type!");

    //BC residual
    for (int n = 0; n < nDOFBC; n++)
    {
      pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

    //HT residual
    for (int n = 0; n < nDOFHT; n++)
    {
      pde_.interpResidVariable(rsd[iHT][n], rsdHTtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iHT][j] += pow(rsdHTtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iHT][j] = sqrt(rsdNorm[iHT][j]);

    return rsdNorm;
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  //Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

protected:
  template<class SparseMatrixType>
  void jacobian( SparseMatrixType mtx, const QuadratureOrder& quadratureOrder,
                 const std::vector<int>& interiorTraceCellJac = {} );

  using AlgebraicEquationSet::xfld_;
  using AlgebraicEquationSet::qfld_;
  using AlgebraicEquationSet::lgfld_;
  using AlgebraicEquationSet::dispatchBC_;
  using AlgebraicEquationSet::pde_;
  using AlgebraicEquationSet::quadratureOrder_;
  using AlgebraicEquationSet::quadratureOrderMin_;
  using AlgebraicEquationSet::resNormType_;
  using AlgebraicEquationSet::tol_;

  Field<PhysDim, TopoDim, ArrayQ>& hbfld_;
};
template<class AlgebraicEquationSet>
const int AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::iHT;

template<class AlgebraicEquationSet>
void
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::
residual(SystemVectorView& rsd)
{
  // First get the residual from the base class
  AlgebraicEquationSet::residual(rsd);

  // Then get residual associated with hubtrace DOFs
  SANS_ASSERT(rsd.m() > iHT);

  dispatchBC_.dispatch_HubTrace(
      ResidualBoundaryTrace_HubTrace_Dispatch_Galerkin( xfld_, qfld_, hbfld_,
                                                        quadratureOrder_.boundaryTraceOrders.data(),
                                                        quadratureOrder_.boundaryTraceOrders.size(),
                                                        rsd(iPDE), rsd(iHT) ) );
}


template<class AlgebraicEquationSet>
template<class SparseMatrixType>
void
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::
jacobian( SparseMatrixType jac, const QuadratureOrder& quadratureOrder,
          const std::vector<int>& interiorTraceCellJac)
{
  SANS_ASSERT(jac.m() > iHT);
  SANS_ASSERT(jac.n() > ihb);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE,iq);
  Matrix jacPDE_hb = jac(iPDE,ihb);
  Matrix jacHT_q   = jac(iHT,iq);
  Matrix jacHT_hb  = jac(iHT,ihb);

  dispatchBC_.dispatch_HubTrace(
    JacobianBoundaryTrace_mitHT_Dispatch_Galerkin<SurrealQClass>( xfld_, qfld_, hbfld_,
                                                                  quadratureOrder.boundaryTraceOrders.data(),
                                                                  quadratureOrder.boundaryTraceOrders.size(),
                                                                  jacPDE_q, jacPDE_hb, jacHT_q, jacHT_hb ) );
}


template<class AlgebraicEquationSet>
template<int iParam, class SparseMatrixType>
void
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::
jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip) const
{
  AlgebraicEquationSet::template jacobianParam<iParam>(jac, quadratureOrder, ip);

  SANS_ASSERT(jac.m() > iHT);
  SANS_ASSERT(jac.n() > ip); // TODO: in this case jac may or may not have blocks associated with lgfld DOFs

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_p = jac(iPDE, ip);
  Matrix jacHT_p  = jac(iHT, ip);

  dispatchBC_.dispatch_HubTrace(
    JacobianBoundaryTrace_mitHT_Dispatch_Galerkin_Param<SurrealParamClass, iParam>(
      xfld_, qfld_, hbfld_,
      quadratureOrder.boundaryTraceOrders.data(),
      quadratureOrder.boundaryTraceOrders.size(),
      jacPDE_p, jacHT_p ) );
}


template<class AlgebraicEquationSet>
void
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::setSolutionField(const SystemVectorView& q)
{
  // First set the solution field for the base class
  AlgebraicEquationSet::setSolutionField(q);

  SANS_ASSERT(q.m() > ihb);

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFHT = hbfld_.nDOF();
  SANS_ASSERT( nDOFHT == q[ihb].m() );
  for (int k = 0; k < nDOFHT; k++)
    hbfld_.DOF(k) = q[ihb][k];
}

template<class AlgebraicEquationSet>
void
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::fillSystemVector(SystemVectorView& q) const
{
  // First set fill the sytem vector for the base class
  AlgebraicEquationSet::fillSystemVector(q);

  SANS_ASSERT(q.m() > ihb);

  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFHT = hbfld_.nDOF();
  SANS_ASSERT( nDOFHT == q[ihb].m() );
  for (int k = 0; k < nDOFHT; k++)
    q[ihb][k] = hbfld_.DOF(k);
}


template<class AlgebraicEquationSet>
typename AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::VectorSizeClass
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::
vectorEqSize() const
{
  static_assert(iHT == iBC+1,"");

  // Get the vector size from the base class and add on the hubtrace
  VectorSizeClass vec = AlgebraicEquationSet::vectorEqSize();

  VectorSizeClass size(vec.m()+1);

  for (int i = 0; i < vec.m(); i++)
    size[i] = vec[i];

  // Create the size that represents the size of a linear algebra vector
  const int nDOFHT = hbfld_.nDOF();

  size[vec.m()] = nDOFHT;

  return size;
}

template<class AlgebraicEquationSet>
typename AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::VectorSizeClass
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::
vectorStateSize() const
{
  return vectorEqSize();
}

template<class AlgebraicEquationSet>
typename AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::MatrixSizeClass
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::matrixSize() const
{
  static_assert(iHT == iBC+1,"");
  static_assert(ihb == ilg+1,"");

  // Get the vector size from the base class and add on the hubtrace
  MatrixSizeClass mtx = AlgebraicEquationSet::matrixSize();

  MatrixSizeClass size(mtx.m()+1, mtx.n()+1);

  for (int i = 0; i < mtx.m(); i++)
    for (int j = 0; j < mtx.n(); j++)
      size(i,j) = mtx(i,j);

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFHT = hbfld_.nDOF();

  // Set the HT row
  for (int j = 0; j < mtx.n(); j++)
    size(iHT,j).resize(nDOFHT, mtx(0,j).n());

  // Set the hb column
  for (int i = 0; i < mtx.m(); i++)
    size(i,ihb).resize(mtx(i,0).m(), nDOFHT);

  // set the last on diagonal
  size(iHT,ihb).resize(nDOFHT, nDOFHT);

  return size;
}


template<class AlgebraicEquationSet>
bool
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::
isValidStateSystemVector(SystemVectorView& q)
{
  // This will also set the solution field
  bool checkBase = AlgebraicEquationSet::isValidStateSystemVector(q);
  bool checkBoundary = true;
  // Update the solution field
  setSolutionField(q);
#if 0 // TODO: don't check hb var for now
  dispatchBC_.dispatch_HubTrace(
      isValidStateBoundaryTrace_mitHT_Dispatch_Galerkin( pde_, xfld_, qfld_, hbfld_,
                                                         quadratureOrder_.boundaryTraceOrders.data(),
                                                         quadratureOrder_.boundaryTraceOrders.size(),
                                                         checkBoundary ) );
#endif
  return (checkBase && checkBoundary);
}

template<class AlgebraicEquationSet>
int
AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL<AlgebraicEquationSet>::
nResidNorm() const
{
  return AlgebraicEquationSet::nResidNorm() + 1;
}

} //namespace SANS

#endif //AlgebraicEquationSet_HubTrace_Galerkin_cutCellTrIBL_H
