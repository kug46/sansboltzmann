// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_LOCAL_PROJECT_H
#define ALGEBRAICEQUATIONSET_LOCAL_PROJECT_H

#include "tools/SANSnumerics.h"

#include "Field/FieldTypes.h"
#include "Field/XField.h"

#include "Discretization/ResidualNormType.h"
#include "Discretization/DG/JacobianDetInvResidualNorm.h"

#include "Discretization/DG/JacobianDetInvResidualNorm.h"
#include "BasisFunction/LagrangeDOFMap.h"

// #include "Field/tools/for_each_CellGroup.h"
// #include "Field/tools/for_each_BoundaryTraceGroup.h"
// #include "Field/tools/for_each_GhostBoundaryTraceGroup.h"
// #include "Field/tools/for_each_GhostBoundaryTraceGroup_Cell.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Sub.h"

#include "IntegrandInteriorTrace_JumpPenalty.h"
#include "ResidualInteriorTrace_SIP_Galerkin.h"
#include "JacobianInteriorTrace_SIP_Galerkin.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "ExtractCGLocalBoundaries.h"
#include "Field/tools/findGroups.h"
#include "AlgebraicEquationSet_Project.h"

namespace SANS
{

/* This class refers to two types of local solutions/systems, full and sub.
 * The full local solution contains all the solution DOFs in the local mesh:
 *  - qfld DOFs in both cellgroups 0 and 1
 *  - all lgfld DOFs
 *
 * The sub local solution contains only the DOFs that are solved for in the local solve:
 *  - qfld DOFs in only cellgroup 0 (i.e. in main-cells)
 *  - lgfld DOFs of only the main boundary traces (no outer boundary traces)
 */

//----------------------------------------------------------------------------//
// Inherit from the standard global L2 project AlgEqSet
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
class AlgebraicEquationSet_Local_Project : public AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits>
{
public:
  typedef AlgebraicEquationSet_Project<ParamFieldType, IntegrandCellClass, TopoDim, Traits> BaseType;
  typedef typename BaseType::PhysDim PhysDim;
  // typedef typename BaseType::ParamFieldType ParamFieldType;

  typedef typename BaseType::ArrayQ ArrayQ;
  typedef typename BaseType::MatrixQ MatrixQ;
  static const int N = DLA::VectorSize<ArrayQ>::M;

  typedef typename BaseType::TraitsType TraitsType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  template<class ArrayQT>
  using SystemVectorTemplate = typename TraitsType::template SystemVectorTemplate<ArrayQT>;

  AlgebraicEquationSet_Local_Project(const ParamFieldType& paramfld,
                                     Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                     const std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld,
                                     const IntegrandCellClass& fcnCell,
                                     const QuadratureOrder& quadratureOrder,
                                     const std::array<Real,1> tol,
                                     const ResidualNormType resNormType = ResidualNorm_L2)
  : BaseType(paramfld,qfld,fcnCell,quadratureOrder,tol,resNormType),
    up_resfld_(up_resfld),
    fcnTrace_( findInteriorTraceGroup(qfld.getXField(),0,1) )
  {
#ifdef WHOLEPATCH
    const Patch patch = Patch::Whole;
#elif defined(INNERPATCH)
    const Patch patch = Patch::Inner;
#else
    const Patch patch = Patch::Broken;
#endif

    // Extract dofs which will be frozen by essential bcs
    extractFreeDOFs(qfld, freeDOFs_, patch );

    // If you want no boundary conditions, Uncomment this, and comment above
    // for (int n = 0; n < qfld.nDOF(); n++)
    //   freeDOFs_.push_back(n);

    // If not using the whole patch or essential inner patch
    // Then the field is broken and we need to construct the map across the interface
#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))
    // This assumes that the order and basis are the same for all cell groups. It gets asserted in constructDOFMap too
    Field_CG_Cell<PhysDim,TopoDim,Real> cfld(qfld.getXField(), qfld.getCellGroupBase(0).order(), qfld.getCellGroupBase(0).basisCategory());

    group1_to_group0_ = constructDOFMap(qfld, cfld, 1, 0);

    if ( up_resfld_ != nullptr ) // if there is a local res field
    {
      SANS_ASSERT_MSG( fcnCell_.nCellGroups() == 1, "Should only be one cell group in the functional" );
      SANS_ASSERT_MSG( fcnCell_.cellGroup(0) == 0, "Functional should only be applied to cell group 0" );
      SANS_ASSERT_MSG( &up_resfld_->getXField() == &qfld_.getXField(), "Residual and solution fields must come from same grid" );
      // construct the map from resfld dofs to cell group 0 dofs
      DGgroup1_to_CGgroup0_ = constructDGtoCGDOFMap( *up_resfld_, qfld, group1_to_group0_, 1 );
    }
    else
    {
      SANS_ASSERT_MSG( fcnCell_.nCellGroups() == 2, "Should be two cell groups in the functional" );
      SANS_ASSERT_MSG( fcnCell_.cellGroup(0) == 0, "Functional should be applied to cell group 0" );
      SANS_ASSERT_MSG( fcnCell_.cellGroup(1) == 1, "Functional should be applied to cell group 1" );
    }
#endif

  }

  virtual ~AlgebraicEquationSet_Local_Project() {}

  using BaseType::residual;
  using BaseType::jacobian;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& jac        ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz ) override {}

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  // Returns the vector and matrix sizes needed for the sub-system (local solve system)
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the size of the full vector (i.e. all elements in the local mesh)
  VectorSizeClass fullVectorSize() const;

  //Translates the sub-system vector into a local solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the sub-solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  const ParamFieldType& paramfld() const { return paramfld_; }

  using BaseType::iProj;
  using BaseType::iq;

  const std::vector<int>& getFreeDOFs( ) const { return freeDOFs_ ; }

protected:
  // using BaseType::jacobian;

  std::vector<int> freeDOFs_;
  const std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld_; // pointer to the local resfld
  IntegrandInteriorTrace_JumpPenalty<PhysDim> fcnTrace_;

  std::map<int,int> group1_to_group0_; // map for dofs duplicated in the broken trace
  std::map<int,int> DGgroup1_to_CGgroup0_; // map for DG dofs in group 1 to CG dofs in group 0

  using BaseType::fcnCell_;
  using BaseType::paramfld_;
  using BaseType::qfld_;
  using BaseType::quadratureOrder_;
  using BaseType::quadratureOrderMin_;
  using BaseType::tol_;
  using BaseType::resNormType_;
};


//Computes the sub-residual for the local problem
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
void
AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::
residual(SystemVectorView& rsd)
{
  //Compute the full residual vector of the local problem
  SystemVector rsd_full(BaseType::vectorEqSize());
  rsd_full = 0;
  BaseType::residual(rsd_full);

  // If not using the whole patch or essential inner patch
  // Then the interior trace SIP term must be evaluated and the residuals transfered across the broken group
#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))

  // Add a penalty to the jumps
  IntegrateInteriorTraceGroups<TopoDim>::integrate( ResidualInteriorTrace_SIP_Galerkin(fcnTrace_, rsd_full(0)),
                                                    qfld_.getXField(), qfld_,
                                                    quadratureOrder_.interiorTraceOrders.data(),
                                                    quadratureOrder_.interiorTraceOrders.size() );

  if (up_resfld_)
  {
    SANS_ASSERT( !DGgroup1_to_CGgroup0_.empty() );
    //MOVE RESIDUAL FROM FIXED EXTERNAL PATCH TO SUBPATCH
    for (const auto& keyVal : DGgroup1_to_CGgroup0_ )
      rsd_full[0][keyVal.second] += up_resfld_->DOF(keyVal.first);
  }
  else
  {
    // Copy the residual from the above full evaluation
    for (const auto& keyVal : group1_to_group0_ )
      rsd_full[0][keyVal.second] += rsd_full[0][keyVal.first];
  }
#endif

  //Extract the sub residual vector from the full residual, corresponding to the equations that need to be solved
  subVectorPlus(rsd_full[0], freeDOFs_, 1., rsd[0]); // Only one equation!
}

//Computes the sub-Jacobian for the local problem
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
void
AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::
jacobian(SystemMatrixView& jac)
{
  //Compute the full Jacobian of the local problem
  SystemMatrix jac_full(BaseType::matrixSize());
  jac_full = 0;
  BaseType::template jacobian<SystemMatrix&>(jac_full, quadratureOrder_);


#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))
  IntegrateInteriorTraceGroups<TopoDim>::integrate(JacobianInteriorTrace_SIP_Galerkin(fcnTrace_, jac_full(0, 0)),
                                                   qfld_.getXField(), qfld_,
                                                   quadratureOrder_.interiorTraceOrders.data(),
                                                   quadratureOrder_.interiorTraceOrders.size() );
#endif


  //Extract the sub Jacobian from the full Jacobian, containing only the equations that need to be solved
  subMatrixPlus(jac_full(0,0), freeDOFs_, freeDOFs_, 1., jac(0,0));

 // SLA::WriteMatrixMarketFile(jac(0,0), "tmp/jac_local.mtx");
 // SLA::WriteMatrixMarketFile(jac_full(0,0), "tmp/jac_full.mtx");
 // SANS_DEVELOPER_EXCEPTION("throw that except boy");
}

//Evaluate Residual Norm
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
std::vector<std::vector<Real>>
AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::
residualNorm( const SystemVectorView& rsd ) const
{
  std::vector<KahanSum<Real>> rsdNormKahan(N, 0);
  std::vector<std::vector<Real>> rsdNorm(this->nResidNorm(), std::vector<Real>(N, 0));

  //PDE residual norm
  if (resNormType_ == ResidualNorm_L2)
  {
    for (int n = 0; n < rsd[iProj].m(); n++)
      for (int j = 0; j < N; j++)
        rsdNormKahan[j] += pow(rsd[iProj][n],2);

    for (int j = 0; j < N; j++)
      rsdNorm[iProj][j] = sqrt((Real)rsdNormKahan[j]);
  }
  else
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Local_Project::residualNorm - Unknown residual norm type!");

  return rsdNorm;
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
typename AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::VectorSizeClass
AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::vectorEqSize() const
{
  static_assert(iProj == 0,"");

  return { (int)freeDOFs_.size() };
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
typename AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::VectorSizeClass
AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::vectorStateSize() const
{
  static_assert(iProj == 0,"");

  return { (int)freeDOFs_.size() };
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
typename AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::MatrixSizeClass
AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::matrixSize() const
{
  static_assert(iProj == 0,"");
  static_assert(iq == 0,"");

  // jacobian nonzero pattern
  //
  //           q
  //   PDE     X

  const int nProj = (int)freeDOFs_.size();

  return {{ { nProj, nProj } }};
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
typename AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::VectorSizeClass
AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::
fullVectorSize() const
{
  return BaseType::vectorEqSize();
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
void
AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  SANS_ASSERT( (int)freeDOFs_.size() == q[iq].m() );
  for (std::size_t k = 0; k < freeDOFs_.size(); k++)
    qfld_.DOF(freeDOFs_[k]) = q[iq][k];

}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits>
void
AlgebraicEquationSet_Local_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>::fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  SANS_ASSERT( (int)freeDOFs_.size() == q[iq].m() );
  for (std::size_t k = 0; k < freeDOFs_.size(); k++)
    q[iq][k] = qfld_.DOF(freeDOFs_[k]);

}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_LOCAL_PROJECT_H
