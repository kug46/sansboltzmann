// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_WAKEMATCH_GALERKIN_IBL_H
#define INTEGRANDBOUNDARYTRACE_WAKEMATCH_GALERKIN_IBL_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Discretization/Integrand_Type.h"

#include "Integrand_Galerkin_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE

template<class PhysDim_, class VarType,
         template<class,class> class PDENDConvert,
         template<class,class> class PDE_,
         class NDBCVector>
class IntegrandBoundaryTrace<PDENDConvert<PhysDim_, PDE_<PhysDim_, VarType>>,
                             NDVectorCategory<NDBCVector, BCCategory::HubTrace>, Galerkin_manifold> :
  public IntegrandBoundaryTraceType<
      IntegrandBoundaryTrace<PDENDConvert<PhysDim_, PDE_<PhysDim_, VarType>>,
                             NDVectorCategory<NDBCVector, BCCategory::HubTrace>, Galerkin_manifold> >
{
public:
  typedef PDENDConvert<PhysDim_, PDE_<PhysDim_, VarType>> PDE;
  typedef BCCategory::HubTrace Category;
  typedef Galerkin_manifold DiscTag;

  typedef PhysDim_ PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;        // matrices

  static const int N = PDE::N;

  IntegrandBoundaryTrace(const PDE& pde,
                         const BCBase& bc,
                         const std::vector<int>& boundaryGroups) :
    pde_(pde),
    bc_(bc),
    boundaryGroups_(boundaryGroups) {}

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return boundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return boundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef typename ElementParam::T ParamT;

    typedef Element<ArrayQ<T>, TopoDimCell,  Topology> ElementQFieldCell;
    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTraceType;

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template <class Z>
    using IntegrandCellType = ArrayQ<Z>;

    template <class Z>
    using IntegrandTraceType = ArrayQ<Z>;

    BasisWeighted(const PDE& pde,
                  const BCBase& bc,
                  const ElementXFieldTrace& xfldElemTrace,
                  const CanonicalTraceToCell& canonicalTrace,
                  const ElementParam& paramfldElem, // Xfield must be the last parameter in the tuple
                  const ElementQFieldCell& qfldElem,
                  const ElementQFieldTraceType& hbfldTrace) :
      pde_(pde),
      bc_(bc),
      xfldElemTrace_(xfldElemTrace),
      canonicalTrace_(canonicalTrace),
      xfldElem_(get<-1>(paramfldElem)),
      qfldElem_(qfldElem),
      hbfldTrace_(hbfldTrace),
      paramfldElem_(paramfldElem),
      nDOFCell_(qfldElem_.nDOF()),
      nDOFTrace_(hbfldTrace_.nDOF()),
      phi_( new Real[nDOFCell_] ),
      phiT_( new Real[nDOFTrace_] ) {}

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] phiT_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFCell_; }
    int nDOFTrace() const { return nDOFTrace_; }

    template <class Ti>
    void operator()(const QuadPointTraceType& RefTrace, IntegrandCellType<Ti> integrandCell[], int neqnCell,
                                                        IntegrandTraceType<Ti> integrandTrace[], int neqnTrace) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandCell, neqnCell,
                                                          integrandTrace, neqnTrace );
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<template<class,class> class BCNDConvert,
             template<class,class> class BCIBL, class BCType, class Ti>
    void operator()( const BCNDConvert<PhysDim_,BCIBL<BCType,VarType>>& bc,
                     const QuadPointTraceType& RefTrace, IntegrandCellType<Ti> integrandCell[], int neqnCell,
                                                         IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldTraceType& hbfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFCell_;
    const int nDOFTrace_;
    mutable Real *phi_;
    mutable Real *phiT_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell , Topology     >& qfldElem,
            const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& hbfldTrace) const
  {
    return BasisWeighted< T,TopoDimTrace,TopologyTrace,
                            TopoDimCell, Topology, ElementParam>(pde_, bc_,
                                                                 xfldElemTrace, canonicalTrace,
                                                                 paramfldElem,
                                                                 qfldElem,
                                                                 hbfldTrace);
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> boundaryGroups_;
};

template<class PhysDim_, class VarType,
         template<class,class> class PDENDConvert,
         template<class,class> class PDE_,
         class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam>
template<template<class,class> class BCNDConvert,
         template<class,class> class BCIBL, class BCType, class Ti>
void
IntegrandBoundaryTrace<PDENDConvert<PhysDim_, PDE_<PhysDim_,VarType> >,
                       NDVectorCategory<NDBCVector, BCCategory::HubTrace>,
                       Galerkin_manifold>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BCNDConvert<PhysDim_, BCIBL<BCType,VarType> >& bc,
           const QuadPointTraceType& sRefTrace, IntegrandCellType<Ti>  integrandCell[],  int neqnCell,
                                                IntegrandTraceType<Ti> integrandTrace[], int neqnTrace) const
{
  static_assert(TopoDimCell::D == 1, "Only TopoD1 element is implemented so far."); // TODO: generalize
  static_assert(PhysDim_::D == 2, "Only PhysD2 is implemented so far."); // TODO: generalize

  SANS_ASSERT((neqnCell == nDOFCell_) && (neqnTrace == nDOFTrace_));

  typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes;  // manifold local axes type

  ParamT param;              // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nrm;               // unit normal (points out of domain)

  VectorX e01L;                              // basis direction vector 1
  LocalAxes e0L;                             // basis direction vector

  ArrayQ<T> q;               // solution
  ArrayQ<T> hb;              // Hub-trace variables

  QuadPointCellType sRefL;   // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval(canonicalTrace_, sRefTrace, sRefL);

  // Elemental parameters
  paramfldElem_.eval(sRefL, param);

  // physical coordinates
  xfldElem_.unitTangent(sRefL, e01L);
  e0L[0] = e01L;

  // unit normal: points out of domain
  traceUnitNormal(xfldElem_, sRefL, xfldElemTrace_, sRefTrace, nrm);

  // basis value, gradient
  qfldElem_.evalBasis(sRefL, phi_, nDOFCell_);

  // solution value, gradient
  qfldElem_.evalFromBasis(phi_, nDOFCell_, q);

  // Hub trace basis
  hbfldTrace_.evalBasis(sRefTrace, phiT_, nDOFTrace_);

  // Hub trace
  hbfldTrace_.evalFromBasis(phiT_, nDOFTrace_, hb);

  //-------  -------//
  ArrayQ<Ti> f = 0.0, sourcehub = 0.0;
  bc.fluxMatchingSourceHubtrace(param, e0L, nrm, q, hb, f, sourcehub);

  for (int k = 0; k < neqnCell; ++k)
    integrandCell[k] = phi_[k]*f; // PDE residual: weak form boundary integral

  for (int k = 0; k < neqnTrace; ++k)
    integrandTrace[k] = phiT_[k]*sourcehub; // Matching condition residual at the hubtrace
}

} // namespace SANS

#endif  // INTEGRANDBOUNDARYTRACE_WAKEMATCH_GALERKIN_IBL_H
