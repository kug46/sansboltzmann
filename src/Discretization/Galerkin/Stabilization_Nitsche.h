// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_STABILIZATION_NITSCHE_H_
#define SRC_DISCRETIZATION_GALERKIN_STABILIZATION_NITSCHE_H_

// #define EXPTTAU

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Discretization/DiscretizationObject.h"

#include "BasisFunction/BasisFunctionCategory.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// applies stabilized CG tau matrix to strong PDE residual
// strongPDE = cb*tau*strongPDE
// ArrayQ = MatrixQ*ArrayQ
// cb constant scaling, default to 1

class StabilizationNitsche : public DiscretizationObject
{
public:

  // order is used to make sure the adjoint doesn't get calculated with a Nitsche parameter
  StabilizationNitsche(const int order = 1, const Real cb = 2.0 ) :
     orderB_(order), cbB_(cb)
  {}

  void setNitscheOrder(const int order){ orderB_ = order;}
  void setNitscheConstant(const Real cbB){ cbB_ = cbB;}
  int getNitscheOrder() const { return orderB_; }

  inline Real getNitscheConstant(int dim) const
  {
    //shahbazi 2005
    return cbB_*(orderB_ + 1.0)*(orderB_ + (Real)dim)/(Real) dim;
  }

protected:
  Real orderB_;
  Real cbB_;
};


}

#endif /* SRC_DISCRETIZATION_GALERKIN_STABILIZATION_NITSCHE_H_ */
