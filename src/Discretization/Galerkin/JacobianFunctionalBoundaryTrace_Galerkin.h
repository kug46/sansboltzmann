// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALBOUNDARYTRACE_GALERKIN_H
#define JACOBIANFUNCTIONALBOUNDARYTRACE_GALERKIN_H

// jacobian boundary-trace functional jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

#include "Field/Field.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementalMassMatrix.h"

#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/Integrand_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Functional boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class FunctionalIntegrandBoundaryTrace, template<class> class Vector>
class JacobianFunctionalBoundaryTrace_Galerkin_impl :
    public GroupIntegralBoundaryTraceType<
       JacobianFunctionalBoundaryTrace_Galerkin_impl<Surreal, FunctionalIntegrandBoundaryTrace, Vector> >
{
public:
  typedef typename FunctionalIntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Real> ArrayJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template MatrixJ<Real> MatrixJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Surreal> ArrayJSurreal;

  JacobianFunctionalBoundaryTrace_Galerkin_impl( const FunctionalIntegrandBoundaryTrace& fcnJ,
                                                 Vector<MatrixJ>& jacFunctional_q ) :
    fcnJ_(fcnJ),
    jacFunctional_q_(jacFunctional_q)
  {
  }

  std::size_t nBoundaryGroups() const { return fcnJ_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnJ_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    SANS_ASSERT_MSG(qfld.nDOFpossessed() == jacFunctional_q_.m(),
                    "qfld.nDOFpossessed() = %d, jacFunctional_q_.m() = %d", qfld.nDOFpossessed(), jacFunctional_q_.m());
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType, class XFieldTraceGroupType>
  void
  integrate( const int cellGroupGloalL,
             const typename XFieldType::                      template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>:: template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const XFieldTraceGroupType& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // total number of entries in ArrayQ
    const int nVar = DLA::VectorSize<ArrayQ>::M;

    // total number of outputs in the functional
    const int nJEqn = DLA::VectorSize<ArrayJ>::M;

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFLocal( nDOFL, -1 );
    std::vector<int> maprsd( nDOFL, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = jacFunctional_q_.m();

    // trace element integral
    ElementIntegral<TopoDimTrace, TopologyTrace, ArrayJSurreal> integralJ(quadratureorder);

    // element functional jacobian vector
    DLA::VectorD<MatrixJ> jacFunctionalElem_q(nDOFL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL.data(), nDOFL );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOFL; n++)
        if (mapDOFGlobal_qL[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal_qL[n];
          nDOFLocal++;
        }

      // no need if all DOFs are possessed by other processors
      if (nDOFLocal == 0) continue;

      // reset the element jacobian
      jacFunctionalElem_q = 0;

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement(  xfldElemTrace, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*((D+1)*nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot = 0;
        int slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*nDOFL;

        // trace integration for canonical element
        ArrayJSurreal functional = 0;
        integralJ( fcnJ_.integrand(xfldElemTrace, canonicalTraceL,
                                   xfldElemL, qfldElemL),
                   get<-1>(xfldElemTrace),
                   functional );

        // accumulate derivatives into element jacobian

        //wrt qL
        slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 0; // Reset the derivative

              // Note that n,m are transposed intentionally
              for (int m = 0; m < nJEqn; m++)
                DLA::index(jacFunctionalElem_q[j],n,m) += DLA::index(functional, m).deriv(slot - nchunk);
            }
          }
        }

      }   // nchunk

      // scatter-add element jacobian to global
      for (int n = 0; n < nDOFLocal; n++)
        jacFunctional_q_[ mapDOFLocal[n] ] += jacFunctionalElem_q[ maprsd[n] ];

    } // elem
  }

protected:
  const FunctionalIntegrandBoundaryTrace& fcnJ_;
  std::vector<int> boundaryTraceGroups_;
  Vector<MatrixJ>& jacFunctional_q_;
};

// Factory function

template<class Surreal, class FunctionalIntegrandBoundaryTrace, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_Galerkin_impl<Surreal, FunctionalIntegrandBoundaryTrace, Vector>
JacobianFunctionalBoundaryTrace_Galerkin( const IntegrandBoundaryTraceType<FunctionalIntegrandBoundaryTrace>& fcnJ,
                                          Vector< MatrixJ >& func_q )
{
  typedef typename Scalar<MatrixJ>::type T;
  static_assert( std::is_same<typename FunctionalIntegrandBoundaryTrace::template MatrixJ<T>, MatrixJ>::value, "These should be the same");
  return {fcnJ.cast(), func_q};
}

}

#endif  // JACOBIANFUNCTIONALBOUNDARYTRACE_GALERKIN_H
