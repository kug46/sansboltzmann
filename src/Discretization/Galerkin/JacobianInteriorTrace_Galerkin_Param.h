// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_GALERKIN_PARAM_H
#define JACOBIANINTERIORTRACE_GALERKIN_PARAM_H

// interior-trace integral jacobian functions
// construct jacobian of PDE residuals w.r.t. parameters

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/Tuple/SurrealizedElementTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{
//----------------------------------------------------------------------------//
//  Galerkin interior-trace integral

template<class Surreal, int iParam, class IntegrandInteriorTrace, class MatrixQP_>
class JacobianInteriorTrace_Galerkin_Param_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_Galerkin_Param_impl<Surreal, iParam, IntegrandInteriorTrace, MatrixQP_> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;

  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;           // e.g. solution type
  typedef typename IntegrandInteriorTrace::template ArrayQ<Surreal> ArrayQSurreal; // e.g. integrand Surreal type

  JacobianInteriorTrace_Galerkin_Param_impl(const IntegrandInteriorTrace& fcn,
                                            MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p ) :
    fcn_(fcn),
    mtxGlobalPDE_p_(mtxGlobalPDE_p) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    SANS_ASSERT( mtxGlobalPDE_p_.m() == qfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class TupleFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename TupleFieldType                 ::template FieldCellGroupType<TopologyL>& tuplefldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename TupleFieldType                 ::template FieldCellGroupType<TopologyR>& tuplefldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XField<PhysDim, TopoDim>       ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    typedef typename TupleFieldType                 ::template FieldCellGroupType<TopologyL> TupleFieldCellGroupTypeL;
    typedef typename TupleFieldType                 ::template FieldCellGroupType<TopologyR> TupleFieldCellGroupTypeR;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef typename TupleType<iParam, TupleFieldCellGroupTypeL>::type ParamCellGroupTypeL;
    typedef typename TupleType<iParam, TupleFieldCellGroupTypeR>::type ParamCellGroupTypeR;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeL, Surreal, iParam>::type ElementTupleFieldClassL;
    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeR, Surreal, iParam>::type ElementTupleFieldClassR;

    typedef typename ParamCellGroupTypeL::template ElementType<Surreal> ElementParamFieldClassL;
    typedef typename ParamCellGroupTypeR::template ElementType<Surreal> ElementParamFieldClassR;

    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    typedef typename ElementParamFieldClassL::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP; // jacobian type associated with each DOF

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass; // jacobian type associated with each element

    // field cell groups
    const ParamCellGroupTypeL& paramfldCellL = get<iParam>(tuplefldCellL);
    const ParamCellGroupTypeR& paramfldCellR = get<iParam>(tuplefldCellR);

    // field elements
    ElementTupleFieldClassL tuplefldElemL( tuplefldCellL.basis() );
    ElementTupleFieldClassR tuplefldElemR( tuplefldCellR.basis() );

    ElementParamFieldClassL& paramfldElemL = set<iParam>(tuplefldElemL);
    ElementParamFieldClassR& paramfldElemR = set<iParam>(tuplefldElemR);

    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int paramDOFL = paramfldElemL.nDOF();
    const int paramDOFR = paramfldElemR.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapQDOFGlobalL(nDOFL);
    std::vector<int> mapQDOFGlobalR(nDOFR);
    std::vector<int> mapParamDOFGlobalL(paramDOFL);
    std::vector<int> mapParamDOFGlobalR(paramDOFR);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal> integral(quadratureorder, nDOFL, nDOFR);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
    std::vector<ArrayQSurreal> rsdPDEElemR( nDOFR );

    // element jacobians
    MatrixElemClass mtxPDEElemLL(nDOFL, paramDOFL); // rsdPDE L w.r.t param L
    MatrixElemClass mtxPDEElemLR(nDOFL, paramDOFR); // rsdPDE L w.r.t param R
    MatrixElemClass mtxPDEElemRL(nDOFR, paramDOFL); // rsdPDE R w.r.t param L
    MatrixElemClass mtxPDEElemRR(nDOFR, paramDOFR); // rsdPDE R w.r.t param R

    // loop over trace elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobians to zero
      mtxPDEElemLL = 0;
      mtxPDEElemLR = 0;
      mtxPDEElemRL = 0;
      mtxPDEElemRR = 0;

      // left/right elements
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global parameter/solution DOFs to element
      tuplefldCellL.getElement( tuplefldElemL, elemL );
      tuplefldCellR.getElement( tuplefldElemR, elemR );

      qfldCellL.getElement( qfldElemL, elemL );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );

      // number of simultaneous derivatives per integral functor call
      int nDeriv = Surreal::N;

#if 0
      SANS_ASSERT_MSG(nArrayP == nDeriv, "Parameter size should match surreal size");
#endif

      // loop over derivative chunks (i.e. each entry of each parameter DOF in both left and right elements)
      for (int nchunk = 0; nchunk < nArrayP*(paramDOFL + paramDOFR); nchunk += nDeriv)
      {
        // associate derivative slots with parameter DOFs

        int slot = 0, slotOffset = 0;
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += paramDOFL*nArrayP;

        for (int j = 0; j < paramDOFR; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemR.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }

        // reset PDE residuals to zero
        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        for (int n = 0; n < nDOFR; n++)
          rsdPDEElemR[n] = 0;

        // trace integration for canonical element
        integral(fcn_.integrand(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                tuplefldElemL, qfldElemL,
                                tuplefldElemR, qfldElemR),
                 xfldElemTrace,
                 rsdPDEElemL.data(), nDOFL,
                 rsdPDEElemR.data(), nDOFR );

        // accumulate derivatives into element jacobian

        slotOffset = 0;
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // Reset the derivative in Surreal

              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemLL(i,j),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFR; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemRL(i,j),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += paramDOFL*nArrayP;

        for (int j = 0; j < paramDOFR; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElemR.DOF(j),n).deriv(slot - nchunk) = 0; // Reset the derivative in Surreal

              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemLR(i,j),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFR; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemRR(i,j),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global
      scatterAdd( qfldCellL, qfldCellR, paramfldCellL, paramfldCellR,
                  elemL, elemR,
                  mapQDOFGlobalL.data(), mapQDOFGlobalL.size(),
                  mapQDOFGlobalR.data(), mapQDOFGlobalR.size(),
                  mapParamDOFGlobalL.data(), mapParamDOFGlobalL.size(),
                  mapParamDOFGlobalR.data(), mapParamDOFGlobalR.size(),
                  mtxPDEElemLL,
                  mtxPDEElemLR,
                  mtxPDEElemRL,
                  mtxPDEElemRR,
                  mtxGlobalPDE_p_ );
    }
  }


//----------------------------------------------------------------------------//
  template <class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            class ParamFieldCellGroupTypeL, class ParamFieldCellGroupTypeR, class MatrixQP,
            class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupTypeL& qfldL,
      const QFieldCellGroupTypeR& qfldR,
      const ParamFieldCellGroupTypeL& paramfldL,
      const ParamFieldCellGroupTypeR& paramfldR,
      const int elemL, const int elemR,
      int mapQDOFGlobalL[], const int nDOFL,
      int mapQDOFGlobalR[], const int nDOFR,
      int mapParamDOFGlobalL[], const int paramDOFL,
      int mapParamDOFGlobalR[], const int paramDOFR,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemLL,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemLR,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemRL,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemRR,
      SparseMatrixType& mtxGlobalPDE_p )
  {
    qfldL.associativity( elemL ).getGlobalMapping( mapQDOFGlobalL, nDOFL );
    qfldR.associativity( elemR ).getGlobalMapping( mapQDOFGlobalR, nDOFR );

    paramfldL.associativity( elemL ).getGlobalMapping( mapParamDOFGlobalL, paramDOFL );
    paramfldR.associativity( elemR ).getGlobalMapping( mapParamDOFGlobalR, paramDOFR );

    mtxGlobalPDE_p.scatterAdd( mtxPDEElemLL, mapQDOFGlobalL, nDOFL, mapParamDOFGlobalL, paramDOFL );
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemLR, mapQDOFGlobalL, nDOFL, mapParamDOFGlobalR, paramDOFR );
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemRL, mapQDOFGlobalR, nDOFR, mapParamDOFGlobalL, paramDOFL );
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemRR, mapQDOFGlobalR, nDOFR, mapParamDOFGlobalR, paramDOFR );
  }

#if 0
//----------------------------------------------------------------------------//
  template <class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupTypeL& qfldL,
      const QFieldCellGroupTypeR& qfldR,
      const int elemL, const int elemR,
      int mapDOFGlobalL[], const int nDOFL, const int paramDOFL,
      int mapDOFGlobalR[], const int nDOFR, const int paramDOFR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemLL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemLR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemRL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemRR,
      SparseMatrixType< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_p )
  {
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemLL, elemL, elemL );
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemLR, elemL, elemR );
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemRL, elemR, elemL );
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemRR, elemR, elemR );
  }
#endif
protected:
  const IntegrandInteriorTrace& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
};

// Factory function

template<class Surreal, int iParam, class IntegrandInteriorTrace, class MatrixQP>
JacobianInteriorTrace_Galerkin_Param_impl<Surreal, iParam, IntegrandInteriorTrace, MatrixQP>
JacobianInteriorTrace_Galerkin_Param( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                      MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p )
{
  return { fcn.cast(), mtxGlobalPDE_p };
}

} // namespace SANS

#endif  // JACOBIANINTERIORTRACE_GALERKIN_PARAM_H
