// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_HSM2D_H
#define INTEGRANDCELL_GALERKIN_HSM2D_H

// cell integrand operator: Galerkin for PDEHSM2D PDE

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/Tuple.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"

#include "pde/HSM/PDEHSM2D.h"

namespace SANS
{

// Forward declaration
template<class PDE>
class IntegrandCell_Galerkin;

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template<class VarType, template <class, class > class PDENDConvert >
class IntegrandCell_Galerkin< PDENDConvert<PhysD2,PDEHSM2D<VarType>> >
  : public IntegrandCellType< IntegrandCell_Galerkin< PDENDConvert<PhysD2,PDEHSM2D<VarType>> > >
{
public:
  typedef PDENDConvert<PhysD2,PDEHSM2D<VarType>> PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin( const PDE& pde, const std::vector<int>& CellGroups )
    : pde_(pde), cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem ) :
                   pde_(pde),
                   qfldElem_(qfldElem),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   paramfldElem_( paramfldElem ),
                   nDOF_(qfldElem_.nDOF() ),
                   phi_( new Real[nDOF_] ),
                   gradphi_( new VectorX[nDOF_] )
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                   pde_(bw.pde_),
                   qfldElem_(bw.qfldElem_),
                   xfldElem_(bw.xfldElem_),
                   paramfldElem_( bw.paramfldElem_ ),
                   nDOF_(bw.nDOF_ ),
                   phi_( bw.phi_ ),
                   gradphi_( bw.gradphi_ )
    {
      bw.phi_= nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neq ) const;

    template<class Ti>
    void operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti>>& rsdPDEElem) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem ) const;

  protected:
    template<class Tq, class Tg, class Tqs, class Ti>
    void weightedIntegrand( const ParamT& param,
                            const ArrayQ<Tq>& q,
                            const VectorArrayQ<Tg>& gradq,
                            const DLA::VectorS<TopoDim::D, ArrayQ<Tqs> >& q_sRef,
                            const DLA::VectorS<TopoDim::D, VectorX >& X_sRef,
                            ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const ElementQFieldType& qfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};


template<class VarType, template <class, class > class PDENDConvert >
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin< PDENDConvert<PhysD2,PDEHSM2D<VarType>> >::BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}


// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template<class VarType, template <class, class > class PDENDConvert >
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin< PDENDConvert<PhysD2,PDEHSM2D<VarType>> >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn) const
{
  SANS_ASSERT(neqn == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                                 // solution
  VectorArrayQ<T> gradq;                       // gradient
  DLA::VectorS<TopoDim::D, ArrayQ<T> > q_sRef; // solution derivative wrt reference coordinates

  DLA::VectorS<TopoDim::D, VectorX > X_sRef;   // X derivative wrt reference coordinates

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // derivative of solution and coordinates wrt reference coordinates
  qfldElem_.evalDerivative( sRef, q_sRef );
  xfldElem_.evalDerivative( sRef, X_sRef );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  // compute the residual
  weightedIntegrand( param, q, gradq, q_sRef, X_sRef, integrand, neqn);
}

template<class VarType, template <class, class > class PDENDConvert >
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin< PDENDConvert<PhysD2,PDEHSM2D<VarType>> >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::VectorD<ArrayQ<Ti>>& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOF_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                                 // solution
  VectorArrayQ<T> gradq;                       // gradient
  DLA::VectorS<TopoDim::D, ArrayQ<T> > q_sRef; // solution derivative wrt reference coordinates

  DLA::VectorS<TopoDim::D, VectorX > X_sRef;   // X derivative wrt reference coordinates

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // derivative of solution and coordinates wrt reference coordinates
  qfldElem_.evalDerivative( sRef, q_sRef );
  xfldElem_.evalDerivative( sRef, X_sRef );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

  // compute the residual
  DLA::VectorD<ArrayQ<Ti>> integrand(nDOF_);

  weightedIntegrand( param, q, gradq, q_sRef, X_sRef, integrand.data(), integrand.size());

  for (int k = 0; k < nDOF_; ++k)
    rsdPDEElem(k) += dJ*integrand(k);
}

template<class VarType, template <class, class > class PDENDConvert >
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin<PDENDConvert<PhysD2,PDEHSM2D<VarType>>>::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, DLA::MatrixD<MatrixQ<Real>>& mtxPDEElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxPDEElem.m() == nDOF_);
  SANS_ASSERT(mtxPDEElem.n() == nDOF_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;                 // solution
  VectorArrayQ<Real> gradq;       // gradient

  DLA::VectorS<TopoDim::D, ArrayQ<Real> > q_sRef; // solution derivative wrt reference coordinates
  DLA::VectorS<TopoDim::D, VectorX      > X_sRef; // X derivative wrt reference coordinates

  ArrayQ<SurrealClass> qSurreal = 0;               // solution
  VectorArrayQ<SurrealClass> gradqSurreal = 0;     // gradient
  DLA::VectorS<TopoDim::D, ArrayQ<SurrealClass> > q_sRefSurreal; // solution derivative wrt reference coordinates

  MatrixQ<Real> PDE_q, PDE_gradq; // temporary storage

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // derivative of solution and coordinates wrt reference coordinates
  qfldElem_.evalDerivative( sRef, q_sRef ); q_sRefSurreal = q_sRef;
  xfldElem_.evalDerivative( sRef, X_sRef );

  std::vector<Real> phis( nDOF_ );
  std::vector<Real> phit( nDOF_ );
  qfldElem_.evalBasisDerivative( sRef, phis.data(), phit.data(), nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qSurreal = q;

  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    gradqSurreal = gradq;
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    weightedIntegrand( param, qSurreal, gradq, q_sRef, X_sRef, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxPDEElem(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;
  } // nchunk


  if (needsSolutionGradient)
  {
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandSurreal = 0;

      weightedIntegrand( param, q, gradqSurreal, q_sRef, X_sRef, integrandSurreal.data(), integrandSurreal.size());

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOF_; j++)
              mtxPDEElem(i,j) += dJ*gradphi_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  }

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < TopoDim::D*PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    for (int d = 0; d < TopoDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(q_sRefSurreal[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    integrandSurreal = 0;

    weightedIntegrand( param, q, gradq, q_sRefSurreal, X_sRef, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    for (int d = 0; d < TopoDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(q_sRefSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOF_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

          if (d == 0)
            for (int j = 0; j < nDOF_; j++)
              mtxPDEElem(i,j) += dJ*phis[j]*PDE_q;
          else if ( d == 1)
            for (int j = 0; j < nDOF_; j++)
               mtxPDEElem(i,j) += dJ*phit[j]*PDE_q;
          else
            SANS_DEVELOPER_EXCEPTION("Only coded for TopoD2");
        }
      }
      slot += PDE::N;
    }
  } // nchunk
}

template<class VarType, template <class, class > class PDENDConvert >
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tg, class Tqs, class Ti>
void
IntegrandCell_Galerkin<PDENDConvert<PhysD2,PDEHSM2D<VarType>>>::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
weightedIntegrand( const ParamT& param,
                   const ArrayQ<Tq>& q,
                   const VectorArrayQ<Tg>& gradq,
                   const DLA::VectorS<TopoDim::D, ArrayQ<Tqs> >& q_sRef,
                   const DLA::VectorS<TopoDim::D, VectorX >& X_sRef,
                   ArrayQ<Ti> integrand[], int neqn ) const
{
  DLA::VectorS<PhysDim::D, ArrayQ<Ti> > F;     // PDE flux F(X, Q), Fv(X, Q, QX)
  ArrayQ<Ti> source;                           // PDE source S(X, Q, QX)

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // Galerkin flux term: -gradient(phi) . F

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    F = 0;

    // advective flux
    pde_.fluxAdvective( param, q, F );

    // viscous flux
    SANS_ASSERT( !pde_.hasFluxViscous() );
    //pde_.fluxViscous( param, q, gradq, F ); (turned off for now.... need to update arguments in NDConvert)

    for (int k = 0; k < neqn; k++)
      integrand[k] -= dot(gradphi_[k],F);
  }

  // Galerkin source term: +phi S

  if (pde_.hasSource())
  {
    source = 0;

    pde_.source( param, X_sRef, q, gradq, q_sRef, source );

    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    SANS_DEVELOPER_EXCEPTION("Forcing function for PDEHSM2D not implemented yet");
    ArrayQ<Real> forcing = 0;
    //pde_.forcingFunction( param, forcing );  (turned off for now... need to update arguments in NDConvert)

    for (int k = 0; k < neqn; k++)
      integrand[k] -= phi_[k]*forcing;
  }
}


}

#endif  // INTEGRANDCELL_GALERKIN_HSM2D_H
