// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFUNCTOR_GALERKIN_LINEARSCALAR_SANSLG_H
#define INTEGRANDBOUNDARYFUNCTOR_GALERKIN_LINEARSCALAR_SANSLG_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/TraceToCellRefCoord.h"
#include "Discretization/Integrand_Type.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "Integrand_Galerkin_fwd.h"
#include "Stabilization_Nitsche.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, Galerkin> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, Galerkin> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;

  static const int N = PDE::N;

  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const StabilizationNitsche& stab)
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups), stab_(stab) {}

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template <class Z>
    using IntegrandType = ArrayQ<Z>;

    BasisWeighted(  const PDE& pde, const BCBase& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem,
                    const ElementQFieldCell& qfldElem,
                    const StabilizationNitsche& stab) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                    qfldElem_(qfldElem),
                    paramfldElem_( paramfldElem ),
                    nDOF_(qfldElem_.nDOF()),
                    phi_( new Real[nDOF_] ),
                    gradphi_( new VectorX[nDOF_] ),
                    stab_(stab)
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                    pde_(bw.pde_), bc_(bw.bc_),
                    xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                    xfldElem_(bw.xfldElem_),
                    qfldElem_(bw.qfldElem_),
                    paramfldElem_( bw.paramfldElem_ ),
                    nDOF_(bw.nDOF_),
                    phi_( bw.phi_ ),
                    gradphi_( bw.gradphi_ ),
                    stab_(bw.stab_)
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType<Ti> integrand[], int neqn ) const;

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Tq, class Tg, class Ti>
    void operator()( const BC& bc, const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
                     const ParamT& paramI, const VectorX& nL, ArrayQ<Ti> integrand[], int neqn ) const;

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const Real A, const Real B, const Ti& bcdata,
                            const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
                            const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    const StabilizationNitsche& stab_;
  };


  template< class Tq, class Tw,
            class TopoDimTrace, class TopologyTrace,
            class TopoDimCell,  class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<ArrayQ<Tw>, TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<Real, TopoDimCell, Topology> ElementEFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type Ti;

    typedef Ti IntegrandType;

    FieldWeighted(  const PDE& pde, const BCBase& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem,
                    const ElementQFieldCell& qfldElem,
                    const ElementWFieldCell& wfldElem,
                    const ElementEFieldCell& efldElem,
                    const StabilizationNitsche& stab) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                    qfldElem_(qfldElem),
                    wfldElem_(wfldElem),
                    efldElem_(efldElem),
                    paramfldElem_( paramfldElem ),
                    nDOF_(qfldElem.nDOF()),
                    nPhi_( efldElem.nDOF()),
                    phi_( new Real[nPhi_] ),
                    gradphi_( new VectorX[nPhi_] ),
                    weight_( new ArrayQ<Tw>[nPhi_] ),
                    gradWeight_( new VectorArrayQ<Tw>[nPhi_] ),
                    stab_(stab)
    {
      // Nothing
    }

    FieldWeighted( FieldWeighted&& fw ) :
                    pde_(fw.pde_), bc_(fw.bc_),
                    xfldElemTrace_(fw.xfldElemTrace_), canonicalTrace_(fw.canonicalTrace_),
                    xfldElem_(fw.xfldElem_),
                    qfldElem_(fw.qfldElem_),
                    wfldElem_(fw.wfldElem_),
                    efldElem_(fw.efldElem_),
                    paramfldElem_( fw.paramfldElem_ ),
                    nDOF_(fw.nDOF_),
                    nPhi_(fw.nPhi_),
                    phi_(fw.phi_),
                    gradphi_(fw.gradphi_),
                    weight_(fw.weight_),
                    gradWeight_(fw.gradWeight_),
                    stab_(fw.stab_)
    {
      fw.phi_ = nullptr; fw.gradphi_ = nullptr;
      fw.weight_ = nullptr; fw.gradWeight_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
      delete [] weight_;
      delete [] gradWeight_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType integrand[], const int nphi ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrand, nphi );
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType integrand[], const int nphi ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementEFieldCell& efldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nPhi_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable ArrayQ<Tw> *weight_;
    mutable VectorArrayQ<Tw> *gradWeight_;
    const StabilizationNitsche& stab_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem) const
  {
    return BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>( pde_, bc_,
                                                                                          xfldElemTrace, canonicalTrace,
                                                                                          paramfldElem, qfldElem, stab_);
  }

  template<class Tq, class Tw,
                     class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<Tq> , TopoDimCell , Topology    >& qfldElem,
            const Element<ArrayQ<Tw> , TopoDimCell , Topology    >& wfldElem,
            const Element<Real       , TopoDimCell , Topology    >& efldElem) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTrace,
             paramfldElem,
             qfldElem, wfldElem, efldElem, stab_ };
  }
private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const StabilizationNitsche& stab_;
};

template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, IntegrandType<Ti> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT paramL;            // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;               // unit normal (points out of domain)

  ArrayQ<T> qI;             // solution
  VectorArrayQ<T> gradqI;   // gradient

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, qI );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradqI );
  else
    gradqI = 0;

  call_with_derived<NDBCVector>(*this, bc_, qI, gradqI, paramL, nL, integrand, neqn);
}

template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
            const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrand[], int neqn ) const
{
  // scalar BC coefficients
  Real A, B;
  bc.coefficients( paramL, nL, A, B );

  // scalar BC data
  Ti bcdata;
  bc.data( paramL, nL, bcdata );

  // compute the integrand (note the lack of the BC template to reduce compile time)
  weightedIntegrand(A, B, bcdata, qI, gradqI, paramL, nL, integrand, neqn);
}

template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam >
template <class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
weightedIntegrand( const Real A, const Real B, const Ti& bcdata,
                   const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
                   const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrand[], int neqn ) const
{
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // PDE residual: weak form boundary integral
  if ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() )
  {
    VectorArrayQ<Ti> F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( paramL, qI, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( paramL, qI, gradqI, F );

    ArrayQ<Ti> Fn = dot(nL,F);
    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*Fn;
  }

  // PDE residual: BC
  // NOTE: only works for scalar equations

  // inviscid term
  VectorMatrixQ<T> a = 0;
  if ( pde_.hasFluxAdvective() )
    pde_.jacobianFluxAdvective( paramL, qI, a );

  T an = dot(nL,a); //flux jacobian dotted with normal vector

  // viscous term
  DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<T> > K = 0;
  if ( pde_.hasFluxViscous() )
    pde_.diffusionViscous( paramL, qI, gradqI, K );

  VectorArrayQ<T> Kgradq = K*gradqI; // = Kn*dq/dn + Ks*dq/ds

  // BC equation
  Ti residualBC = A*qI + B*dot(Kgradq,nL) - bcdata;

  for (int k = 0; k < neqn; k++)
  {
    VectorArrayQ<T> Ktgradphi = Transpose(K)*gradphi_[k];
    integrand[k] += (phi_[k]*(B - A*an) - A*dot(Ktgradphi,nL))/(A*A + B*B)*residualBC;
  }

  //Playing with Nitsche Parameters
#if 0
  if (pde_.hasFluxViscous() )
  {
    MatrixQ<T> normK = 0.;
    Real Cb = 1e1;

    for (int i=0; i < PhysDim::D; i++)
      for (int j=0; j < PhysDim::D; j++)
        normK += nL(j)*K(i,j)*nL(i);

    Real hinv = 0;
//    for (int k = 0; k < nDOF_; k++)
//      hinv += dot(gradphi_[k],gradphi_[k]);
//    hinv = sqrt(hinv);

    hinv = xfldElemTrace_.jacobianDeterminant()/xfldElem_.jacobianDeterminant();

    MatrixQ<Ti> aAbs = 0.0;
    if (pde_.hasFluxAdvective())
      pde_.jacobianFluxAdvectiveAbsoluteValue(paramL, qI, nL, aAbs );

    for (int k = 0; k < neqn; k++)
    {
      integrand[k] += A*phi_[k]*aAbs*(residualBC)*Cb;
      integrand[k] += A*phi_[k]*hinv*normK*(residualBC)*Cb;
    }

  }
#endif


}

template <class PDE, class NDBCVector>
template<class Tq, class Tw,
         class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template <class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, Galerkin>::
FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType integrand[], const int nphi ) const
{
  SANS_ASSERT( nphi == nPhi_ );

  VectorX X;                // physical coordinates
  VectorX N;                // unit normal (points out of domain)

  ArrayQ<Tq> q;              // solution
  VectorArrayQ<Tq> gradq;    // gradient

  ArrayQ<Tw> w;              // weight
  VectorArrayQ<Tw> gradw;    // gradient

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, N);

  // solution, gradient
  qfldElem_.eval( sRef, q );

  if (needsSolutionGradient)
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );
  else
    gradq = 0;

  // weight, gradient
  wfldElem_.eval( sRef, w );
  xfldElem_.evalGradient( sRef, wfldElem_, gradw );

  // estimate basis, gradient
  efldElem_.evalBasis( sRef, phi_, nPhi_ );
  xfldElem_.evalBasisGradient( sRef, efldElem_, gradphi_, nPhi_ );

  for (int k = 0; k < nPhi_; k++)
  {
    integrand[k] = 0;
    weight_[k] = w* phi_[k];
    gradWeight_[k] = gradphi_[k]*w + phi_[k]*gradw;
  }

  // PDE residual: weak form boundary integral
  if ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() )
  {
    VectorArrayQ<Tq> F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( X, q, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( X, q, gradq, F );

    ArrayQ<Tq> Fn = dot(N,F);
    for (int k = 0; k < nPhi_; k++)
      integrand[k] += dot(weight_[k],Fn);
  }

  // PDE residual: BC
  // NOTE: only works for scalar equations

  // scalar BC coefficients
  Real A, B;
  bc.coefficients( X, N, A, B );

  // scalar BC data
  Real bcdata;
  bc.data( X, N, bcdata );

  // invisid term
  DLA::VectorS< PhysDim::D, MatrixQ<Tq> > a = 0;
  if ( pde_.hasFluxAdvective() )
    pde_.jacobianFluxAdvective( X, q, a );

  Tq an = dot(N,a); //flux jacobian dotted with normal vector

  // viscous term
  DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<Tq> > K = 0;
  if ( pde_.hasFluxViscous() )
    pde_.diffusionViscous( X, q, gradq, K );

  VectorArrayQ<Tq> Kgradq = K*gradq; // = Kn*dq/dn + Ks*dq/ds

  // BC equation
  ArrayQ<Tq> residualBC = A*q + B*dot(Kgradq,N) - bcdata;

  Tq tmp = (B - A*an);

  for (int k = 0; k < nPhi_; k++)
  {
    VectorArrayQ<Ti> Ktgradw = Transpose(K)*gradWeight_[k];
    integrand[k] += (dot(weight_[k],tmp) - A*dot(Ktgradw,N))/(A*A + B*B)*residualBC;
  }

  //Playing with Nitsche Parameters
#if 0
  if (pde_.hasFluxViscous() )
  {
    MatrixQ<T> normK = 0.;
    Real Cb = 1e1;

    for (int i=0; i < PhysDim::D; i++)
      for (int j=0; j < PhysDim::D; j++)
        normK += N(j)*K(i,j)*N(i);

    Real hinv = 0;
//    VectorX gradphi_[nDOF_]; // So that it is consistent with basis weighted
//    xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
//
//    for (int k = 0; k < nDOF_; k++)
//      hinv += dot(gradphi_[k],gradphi_[k]);
//    hinv = sqrt(hinv);

    hinv = xfldElemTrace_.jacobianDeterminant()/xfldElem_.jacobianDeterminant();

    MatrixQ<T> aAbs = 0.0;
    if (pde_.hasFluxAdvective())
      pde_.jacobianFluxAdvectiveAbsoluteValue(X, q, N, aAbs );

    integrand += A*dot(w,aAbs*(residualBC)*Cb);
    integrand += A*dot(w,hinv*normK*(residualBC)*Cb);

  }
#endif

}


}

#endif  // INTEGRANDBOUNDARYFUNCTOR_GALERKIN_LINEARSCALAR_SANSLG_H
