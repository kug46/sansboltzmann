// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_INTEGRANDCELL_GALERKIN_CUTCELLTRANSITIONIBL_H_
#define SRC_DISCRETIZATION_GALERKIN_INTEGRANDCELL_GALERKIN_CUTCELLTRANSITIONIBL_H_

// cell integrand operator: Galerkin specialized for IBL in cut-cell transition formulation

#include <ostream>
#include <vector>

#include "BasisFunction/BasisFunctionCategory.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "Discretization/Integrand_Type.h"

#include "pde/IBL/PDEIBL2D.h"

namespace SANS
{

template<class PDE_>
class IntegrandCell_Galerkin_cutCellTransitionIBL;

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template<class VarType,
         template<class, class> class PDENDConvert>
class IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > > :
    public IntegrandCellType<IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > > >
{
public:
  typedef PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::VarInterpType VarInterpType;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  IntegrandCell_Galerkin_cutCellTransitionIBL(const PDE& pde,
                                  const std::vector<int>& CellGroups) :
    pde_(pde),
    cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  // Basis-weighted integrand class: i.e. weighting function = basis function of solution q
  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > PDE;

    typedef Element<ArrayQ<T>, TopoDim, Topology>     ElementQMatchFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology>     ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;
    template <class TVectorX>
    using VectorXT = DLA::VectorS<PhysD2::D,TVectorX>;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef DLA::VectorS<TopoDim::D, VectorX> LocalAxes;         // manifold local axes type
    typedef DLA::VectorS<PhysDim::D, LocalAxes> VectorLocalAxes; // gradient of local axes type

    template<class Z>
    using IntegrandType = ArrayQ<Z>;

    BasisWeighted(const PDE& pde,
                  const ElementParam& paramfldElem,
                  const ElementQMatchFieldType& qmatchfldElem,
                  const ElementQFieldType& qfldElem) :
      pde_(pde),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem_)), // XField must be the last parameter
      qmatchfldElem_(qmatchfldElem),
      qfldElem_(qfldElem),
      nDOFMATCH_(qmatchfldElem_.nDOF()),
      nDOFPDE_(qfldElem_.nDOF()),
      phi_( new T[nDOFPDE_] ),
      gradphi_( new VectorXT<T>[nDOFPDE_] ),
      phicutcell_( new Real[nDOFPDE_] ),
      gradphicutcell_scaled_( new VectorX[nDOFPDE_] ),
      phicutcellturbtr_( new Real[nDOFPDE_] )
    {
      static_assert(TopoDim::D == 1, "Only TopoD1 is implemented so far."); // TODO: generalize
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
      delete [] phicutcell_;
      delete [] gradphicutcell_scaled_;
      delete [] phicutcellturbtr_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOF() const { return nDOFPDE_; }

    // cell element residual integrand
    template<class Ti>
    void operator()( const Real dJ_sRef, const T& sRef, const QuadPointType& sRefRef, const T& dsRef_dsRefRef,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const;

    // residuals for laminar cut-cell element
    template<class Ti>
    void lamiElem( const Real dJ_sRef, const T& sRef, const QuadPointType& sRefRef, const T& dsRef_dsRefRef,
                   DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const;

    // residuals for turbulent cut-cell element
    template<class Ti>
    void turbElem( const Real dJ_sRef, const T& sRef, const QuadPointType& sRefRef, const T& dsRef_dsRefRef,
                   DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const;

    // only TS amplification growth residual for turbulent cut-cell element
    template<class Ti>
    void turbElemSourceAmp( const T& sRefTr, const QuadPointType& sRefRefTrLami,
                            Ti& source_turbAmp) const;

    template <class Ti>
    void turbElemIntegralAmp( const Real dJ_sRef, const T& sRefTr, const T& dsRef_dsRefRef,
                              const Ti& source_turbAmp, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem ) const;

    // residuals for transition front (i.e. interface between cut-cell elements)
    template<class Ti>
    void transitionFront( const VectorX& nrm_LtoT, const T& sRefTr,
                          const QuadPointType& sRefRefTrLami, const QuadPointType& sRefRefTrTurb,
                          DLA::VectorD<ArrayQ<Ti> >& rsdMATCHElem, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const;

    // residual for dummy matching variables
    template<class Ti>
    void nonTransitionDummyMatch( DLA::VectorD<ArrayQ<Ti> >& rsdElemMATCH) const;

    template<class Tm, class Tq, class Ti>
    void weightedIntegrandTransitionFront(
        const VectorX& nrmLtoT,
        const LocalAxes& e0,
        const ParamT& param,
        const ArrayQ<Tm>& qMatch,
        const ArrayQ<Tq>& q,
        const QuadPointType& sRefRefTrLami,
        const QuadPointType& sRefRefTrTurb,
        IntegrandType<Ti> integrandMATCH[], const int neqnMATCH,
        IntegrandType<Ti> integrand[],      const int neqnPDE ) const;

    const PDE& getpde() const { return pde_; }
    const ElementQFieldType& getqfldElem() const { return qfldElem_; }

#if 1 //TODO: these need to be relocated to a more proper place
    template<class Tscalar>
    Real getScalarValuePOD(const Tscalar& x) const { return x.value(); }

    Real getScalarValuePOD(const Real& x) const { return x; }
#endif

  private:
    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const LocalAxes& e0,
                            const VectorLocalAxes& e0_X,
                            const ParamT& param,
                            const ArrayQ<Tq>& q,
                            const VectorArrayQ<Tg>& gradq,
                            const T& dsRef_dsRefRef,
                            IntegrandType<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qmatchfldElem_;
    const ElementQFieldType& qfldElem_;

    const int nDOFMATCH_;
    const int nDOFPDE_;
    mutable T *phi_; // TODO: always Surrealizing is slow
    mutable VectorXT<T> *gradphi_; // TODO: always Surrealizing is slow
    mutable Real *phicutcell_; // keep Real
    mutable VectorX *gradphicutcell_scaled_; // keep Real
    mutable Real *phicutcellturbtr_; // keep Real
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qmatchfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qmatchfldElem, qfldElem);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};


template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return (pde_.hasFluxAdvective() ||
          pde_.hasFluxViscous() ||
          pde_.hasSource() ||
          pde_.hasForcingFunction());
}


// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//

template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ_sRef, const T& sRef, const QuadPointType& sRefRef, const T& dsRef_dsRefRef,
           DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOFPDE_);

  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q, q_cutcell;                // solution
  VectorArrayQ<T> gradq = 0.0, gradq_cutcell = 0.0;      // solution gradient

  VectorX e01;                // basis direction vector
  LocalAxes e0;               // basis direction vector
  VectorLocalAxes e0_X;       // manifold local axes tangential cartesian gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  const Real sRefPOD = getScalarValuePOD(sRef); //TODO: assuming linear grid; revert to Real from Surreal

  // Basis vector and its gradients
  xfldElem_.unitTangent(sRefPOD, e01);
  e0[0] = e01;

  xfldElem_.evalUnitTangentGradient(sRefPOD, e0_X); //TODO: assuming linear grid;

  // Elemental parameters (includes X)
  paramfldElem_.left().eval(sRef, param.left()); //TODO: this assumes the entire left element is Surrealized
  paramfldElem_.right().eval(sRefPOD, param.right()); // right() = XFieldElement is not Surrealized

  // original basis value, gradient
  qfldElem_.evalBasis(sRef, phi_, nDOFPDE_);
  xfldElem_.evalBasisGradient(sRef, qfldElem_, gradphi_, nDOFPDE_);

  // cut-cell basis value, gradient
  qfldElem_.evalBasis(sRefRef, phicutcell_, nDOFPDE_);

  VectorX sRef_grad = 0.0;
  xfldElem_.evalReferenceCoordinateGradient(sRefPOD, sRef_grad); //TODO: assuming linear grid

  std::vector<Real> phisRefRef_tmp(nDOFPDE_); // d(phi)/d(sRefRef)
  qfldElem_.evalBasisDerivative(sRefRef, phisRefRef_tmp.data(), phisRefRef_tmp.size());

  for (int j = 0; j < nDOFPDE_; ++j)
    gradphicutcell_scaled_[j] = phisRefRef_tmp[j] * sRef_grad;

  // solution value, gradient
  qfldElem_.evalFromBasis(phi_, nDOFPDE_, q); // interpret DOFs with original basis function phi_
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis(gradphi_, nDOFPDE_, gradq);

  qfldElem_.evalFromBasis(phicutcell_, nDOFPDE_, q_cutcell); // interpret DOFs with cut-cell basis function phicutcell_
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis(gradphicutcell_scaled_, nDOFPDE_, gradq_cutcell);

  // re-interpret solution
#if 0 //TODO: specific to one set of primary unknowns; to be removed
  q[VarInterpType::ideltaLami] = q_cutcell[VarInterpType::ideltaLami];
  q[VarInterpType::iALami]     = q_cutcell[VarInterpType::iALami];
  q[VarInterpType::ideltaTurb] = q_cutcell[VarInterpType::ideltaTurb];
  q[VarInterpType::iATurb]     = q_cutcell[VarInterpType::iATurb];
//  q[VarInterpType::i_nt]       = q[VarInterpType::i_nt];
  q[VarInterpType::ictau]      = q_cutcell[VarInterpType::ictau];

  for (int d = 0; d < D; ++d)
  {
    gradq[d][VarInterpType::ideltaLami] = gradq_cutcell[d][VarInterpType::ideltaLami];
    gradq[d][VarInterpType::iALami]     = gradq_cutcell[d][VarInterpType::iALami];
    gradq[d][VarInterpType::ideltaTurb] = gradq_cutcell[d][VarInterpType::ideltaTurb];
    gradq[d][VarInterpType::iATurb]     = gradq_cutcell[d][VarInterpType::iATurb];
//    gradq[d][VarInterpType::i_nt]       = gradq[d][VarInterpType::i_nt];
    gradq[d][VarInterpType::ictau]      = gradq_cutcell[d][VarInterpType::ictau];
  }
#else
  for (int j = 0; j < N; ++j)
  {
    if (j != VarInterpType::i_nt) // only re-interpret DOFs other than the nt variable
    {
      q[j] = q_cutcell[j];

      for (int d = 0; d < D; ++d)
        gradq[d][j] = gradq_cutcell[d][j];
    }
  }
#endif

  // compute the residual
  DLA::VectorD<ArrayQ<Ti>> integrand(nDOFPDE_);

  weightedIntegrand( e0, e0_X, param, q, gradq, dsRef_dsRefRef, integrand.data(), integrand.size());

  for (int k = 0; k < nDOFPDE_; ++k)
  {
    rsdPDEElem(k)[PDE::ir_momLami] += integrand(k)[PDE::ir_momLami]*dJ_sRef;
    rsdPDEElem(k)[PDE::ir_keLami]  += integrand(k)[PDE::ir_keLami] *dJ_sRef;
    rsdPDEElem(k)[PDE::ir_momTurb] += integrand(k)[PDE::ir_momTurb]*dJ_sRef;
    rsdPDEElem(k)[PDE::ir_keTurb]  += integrand(k)[PDE::ir_keTurb] *dJ_sRef;
    rsdPDEElem(k)[PDE::ir_amp]     += integrand(k)[PDE::ir_amp]    *dJ_sRef*dsRef_dsRefRef;
    rsdPDEElem(k)[PDE::ir_lag]     += integrand(k)[PDE::ir_lag]    *dJ_sRef;
  }
}

template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tg, class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
weightedIntegrand( const LocalAxes& e0,
                   const VectorLocalAxes& e0_X,
                   const ParamT& param,
                   const ArrayQ<Tq>& q,
                   const VectorArrayQ<Tg>& gradq,
                   const T& dsRef_dsRefRef,
                   IntegrandType<Ti> integrand[], int neqn ) const
{
  // PDE integrand
  for (int k = 0; k < neqn; ++k)
    integrand[k] = 0;

  // Galerkin flux term: -gradient(phi) . F
  VectorArrayQ<Ti> F = 0;          // PDE flux F(X, U), Fv(X, U, UX)

  // advective flux
  if (pde_.hasFluxAdvective())
    pde_.fluxAdvective(param, e0, q, F);

  ArrayQ<Ti> integrand_tmp_phi = 0.0, integrand_tmp_phiwt = 0.0;
  for (int k = 0; k < neqn; k++)
  {
    integrand_tmp_phi = dot(gradphi_[k],F);
    integrand_tmp_phiwt = dot(gradphicutcell_scaled_[k],F);

    integrand[k][PDE::ir_momLami] -= integrand_tmp_phiwt[PDE::ir_momLami];
    integrand[k][PDE::ir_keLami]  -= integrand_tmp_phiwt[PDE::ir_keLami];
    integrand[k][PDE::ir_momTurb] -= integrand_tmp_phiwt[PDE::ir_momTurb];
    integrand[k][PDE::ir_keTurb]  -= integrand_tmp_phiwt[PDE::ir_keTurb];
    integrand[k][PDE::ir_amp]     -= integrand_tmp_phi[PDE::ir_amp];
    integrand[k][PDE::ir_lag]     -= integrand_tmp_phiwt[PDE::ir_lag];
  }


  // Galerkin source term: +phi S
  if (pde_.hasSource())
  {
    ArrayQ<Ti> source = 0;                            // PDE source S(X, Q, QX)

    pde_.source(param, e0, e0_X, q, gradq, source);

    for (int k = 0; k < neqn; k++)
    {
      integrand_tmp_phi = phi_[k]*source;
      integrand_tmp_phiwt = phicutcell_[k]*dsRef_dsRefRef*source;

      integrand[k][PDE::ir_momLami] += integrand_tmp_phiwt[PDE::ir_momLami];
      integrand[k][PDE::ir_keLami]  += integrand_tmp_phiwt[PDE::ir_keLami];
      integrand[k][PDE::ir_momTurb] += integrand_tmp_phiwt[PDE::ir_momTurb];
      integrand[k][PDE::ir_keTurb]  += integrand_tmp_phiwt[PDE::ir_keTurb];
      integrand[k][PDE::ir_amp]     += integrand_tmp_phi[PDE::ir_amp];
      integrand[k][PDE::ir_lag]     += integrand_tmp_phiwt[PDE::ir_lag];
    }
  }
}


template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
lamiElem(const Real dJ_sRef, const T& sRef, const QuadPointType& sRefRef, const T& dsRef_dsRefRef,
         DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOFPDE_);

  DLA::VectorD<ArrayQ<Ti> > rsdPDEElem_copy(nDOFPDE_);
  rsdPDEElem_copy = 0.0;

  (*this)(dJ_sRef, sRef, sRefRef, dsRef_dsRefRef, rsdPDEElem_copy);

  for (int k = 0; k < nDOFPDE_; ++k)
  {
    // reuse only appropriate parts of the regular cell integrand calculation
    rsdPDEElem(k)[PDE::ir_momLami] += rsdPDEElem_copy(k)[PDE::ir_momLami];
    rsdPDEElem(k)[PDE::ir_keLami] += rsdPDEElem_copy(k)[PDE::ir_keLami];
    rsdPDEElem(k)[PDE::ir_amp] += rsdPDEElem_copy(k)[PDE::ir_amp];
  }
}

template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
turbElem(const Real dJ_sRef, const T& sRef, const QuadPointType& sRefRef, const T& dsRef_dsRefRef,
         DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOFPDE_);

  DLA::VectorD<ArrayQ<Ti> > rsdPDEElem_copy(nDOFPDE_);
  rsdPDEElem_copy = 0.0;

  (*this)(dJ_sRef, sRef, sRefRef, dsRef_dsRefRef, rsdPDEElem_copy);

  for (int k = 0; k < nDOFPDE_; ++k)
  {
    rsdPDEElem(k)[PDE::ir_momTurb] += rsdPDEElem_copy(k)[PDE::ir_momTurb];
    rsdPDEElem(k)[PDE::ir_keTurb] += rsdPDEElem_copy(k)[PDE::ir_keTurb];
    rsdPDEElem(k)[PDE::ir_amp] += rsdPDEElem_copy(k)[PDE::ir_amp];
    rsdPDEElem(k)[PDE::ir_lag] += rsdPDEElem_copy(k)[PDE::ir_lag];
  }
}

template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
turbElemSourceAmp(const T& sRefTr, const QuadPointType& sRefRefTrLami,
                  Ti& source_turbAmp) const
{
  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q, q_cutcell;     // solution

  VectorX e01;                // basis direction vector
  LocalAxes e0;               // basis direction vector

  const Real sRefPOD = getScalarValuePOD(sRefTr); //TODO: assuming linear grid; revert to Real from Surreal

  // Basis vector and its gradients
  xfldElem_.unitTangent(sRefPOD, e01);
  e0[0] = e01;

  // Elemental parameters (includes X)
  paramfldElem_.left().eval(sRefTr, param.left()); //TODO: this assumes the entire left element is Surrealized
  paramfldElem_.right().eval(sRefPOD, param.right()); // right() = XFieldElement is not Surrealized

  // original basis value, gradient
  qfldElem_.evalBasis(sRefTr, phi_, nDOFPDE_);

  // cut-cell basis value, gradient
  qfldElem_.evalBasis(sRefRefTrLami, phicutcell_, nDOFPDE_);

  // solution value, gradient
  qfldElem_.evalFromBasis(phi_, nDOFPDE_, q); // interpret DOFs with original basis function phi_
  qfldElem_.evalFromBasis(phicutcell_, nDOFPDE_, q_cutcell); // interpret DOFs with cut-cell basis function phicutcell_

  // re-interpret solution
#if 0 //TODO: specific to one set of primary unknowns; to be removed
  q[VarInterpType::ideltaLami] = q_cutcell[VarInterpType::ideltaLami];
  q[VarInterpType::iALami]     = q_cutcell[VarInterpType::iALami];
//  q[VarInterpType::ideltaTurb] = q_cutcell_turb[VarInterpType::ideltaTurb]; // TODO: turbulent delta and A should not be used
//  q[VarInterpType::iATurb]     = q_cutcell_turb[VarInterpType::iATurb];
//  q[VarInterpType::i_nt]       = q[VarInterpType::i_nt];
  q[VarInterpType::ictau]      = q_cutcell[VarInterpType::ictau];
#else // TODO: also very hacky
  q[0] = q_cutcell[0];
  q[1] = q_cutcell[1];
//  q[VarInterpType::ideltaTurb] = q_cutcell_turb[VarInterpType::ideltaTurb]; // TODO: turbulent delta and A should not be used
//  q[VarInterpType::iATurb]     = q_cutcell_turb[VarInterpType::iATurb];
//  q[VarInterpType::i_nt]       = q[VarInterpType::i_nt];
  q[5] = q_cutcell[5];
#endif

  // compute source term for TS amplification
  source_turbAmp = 0.0;

  pde_.sourceTurbAmp(param, e0, q, source_turbAmp);
}

template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
turbElemIntegralAmp(const Real dJ_sRef, const T& sRef, const T& dsRef_dsRefRef,
                    const Ti& source_turbAmp, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOFPDE_);

  // original basis value, gradient
  qfldElem_.evalBasis(sRef, phi_, nDOFPDE_);

  // add residual integral to TS amplification equation
  for (int k = 0; k < nDOFPDE_; ++k)
    rsdPDEElem(k)[PDE::ir_amp] += source_turbAmp*phi_[k]*dJ_sRef*dsRef_dsRefRef;
}

template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology, ElementParam>::
transitionFront(const VectorX& nrm_LtoT, const T& sRefTr,
                const QuadPointType& sRefRefTrLami, const QuadPointType& sRefRefTrTurb,
                DLA::VectorD<ArrayQ<Ti> >& rsdMATCHElem, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElem) const
{
  SANS_ASSERT(rsdPDEElem.m() == nDOFPDE_);

  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> qMatch;
  ArrayQ<T> q, q_cutcellLami, q_cutcellTurb;                 // solution

  VectorX e01;                // basis direction vector
  LocalAxes e0;               // basis direction vector

  const Real sRefTrPOD = getScalarValuePOD(sRefTr); //TODO: assuming linear grid

  // Basis vector and its gradients
  xfldElem_.unitTangent(sRefTrPOD, e01);
  e0[0] = e01;

  // Elemental parameters (includes X)
  paramfldElem_.left().eval(sRefTr, param.left()); //TODO: this assumes the entire left element is Surrealized
  paramfldElem_.right().eval(sRefTrPOD, param.right()); // right() = XFieldElement is not Surrealized

  // basis value, gradient
  qmatchfldElem_.eval(Topology::centerRef, qMatch);

  qfldElem_.evalBasis(sRefTr, phi_, nDOFPDE_);
  qfldElem_.evalBasis(sRefRefTrLami, phicutcell_, nDOFPDE_);
  qfldElem_.evalBasis(sRefRefTrTurb, phicutcellturbtr_, nDOFPDE_);

  // solution value
  qfldElem_.evalFromBasis(phi_, nDOFPDE_, q);
  qfldElem_.evalFromBasis(phicutcell_, nDOFPDE_, q_cutcellLami);
  qfldElem_.evalFromBasis(phicutcellturbtr_, nDOFPDE_, q_cutcellTurb);

  // re-interpret solution
#if 0 //TODO: specific to one set of primary unknowns; to be removed
  q[VarInterpType::ideltaLami] = q_cutcellLami[VarInterpType::ideltaLami];
  q[VarInterpType::iALami]     = q_cutcellLami[VarInterpType::iALami];
  q[VarInterpType::ideltaTurb] = q_cutcellTurb[VarInterpType::ideltaTurb];
  q[VarInterpType::iATurb]     = q_cutcellTurb[VarInterpType::iATurb];
//  q[VarInterpType::i_nt]       = q[VarInterpType::i_nt];
  q[VarInterpType::ictau]      = q_cutcellTurb[VarInterpType::ictau];
#else // TODO: also very hacky
  q[0] = q_cutcellLami[0];
  q[1] = q_cutcellLami[1];
  q[2] = q_cutcellTurb[2];
  q[3] = q_cutcellTurb[3];
//  q[VarInterpType::i_nt]       = q[VarInterpType::i_nt];
  q[5] = q_cutcellTurb[5];
#endif

  // add to element residual
  weightedIntegrandTransitionFront( nrm_LtoT, e0, param, qMatch, q, sRefRefTrLami, sRefRefTrTurb,
                                    rsdMATCHElem.data(), rsdMATCHElem.size(),
                                    rsdPDEElem.data(), rsdPDEElem.size() );
}

template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Tm, class Tq, class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
weightedIntegrandTransitionFront(
    const VectorX& nrmLtoT,
    const LocalAxes& e0,
    const ParamT& param,
    const ArrayQ<Tm>& qMatch, const ArrayQ<Tq>& q,
    const QuadPointType& sRefRefTrLami, const QuadPointType& sRefRefTrTurb,
    IntegrandType<Ti> integrandMATCH[], const int neqnMATCH,
    IntegrandType<Ti> integrand[],      const int neqnPDE ) const
{
  ArrayQ<Ti> Fn = 0.0;
  ArrayQ<Ti> sourceMatching = 0.0;
  pde_.cutCellTransitionFluxAndSource(param, e0, nrmLtoT, qMatch, q, Fn, sourceMatching);

  // laminar part
  for (int k = 0; k < neqnPDE; ++k)
  {
    integrand[k][PDE::ir_momLami] += phicutcell_[k]*Fn[PDE::ir_momLami];
    integrand[k][PDE::ir_keLami] += phicutcell_[k]*Fn[PDE::ir_keLami];
//    integrand[k][PDE::ir_amp] += 0.0;
  }

  // turbulent part
  for (int k = 0; k < neqnPDE; ++k)
  {
    integrand[k][PDE::ir_momTurb] += phicutcellturbtr_[k]*(-Fn[PDE::ir_momTurb]);
    integrand[k][PDE::ir_keTurb] += phicutcellturbtr_[k]*(-Fn[PDE::ir_keTurb]);
    integrand[k][PDE::ir_lag] += phicutcellturbtr_[k]*(-Fn[PDE::ir_lag]);
  }

  // matching conditions
  for (int k = 0; k < neqnMATCH; ++k)
    integrandMATCH[k] += sourceMatching;
}

// residual for dummy matching variables
template<class VarType,
         template<class, class> class PDENDConvert>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_Galerkin_cutCellTransitionIBL< PDENDConvert<PhysD2, PDEIBL<PhysD2, VarType> > >::
BasisWeighted<T,TopoDim,Topology,ElementParam>::
nonTransitionDummyMatch( DLA::VectorD<ArrayQ<Ti> >& rsdElemMATCH) const
{
  SANS_ASSERT(rsdElemMATCH.m() == nDOFMATCH_);

  ArrayQ<T> qMatch = qmatchfldElem_.eval(Topology::centerRef);

  ArrayQ<Ti> sourceMatch = 0.0;
  pde_.sourceMatchNonTransitionDummy(qMatch, sourceMatch);

  for (int j = 0; j < nDOFMATCH_; ++j)
    rsdElemMATCH(j) += sourceMatch;
}

} // namespace SANS

#endif /* SRC_DISCRETIZATION_GALERKIN_INTEGRANDCELL_GALERKIN_CUTCELLTRANSITIONIBL_H_ */
