// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_SIP_GALERKIN_H
#define JACOBIANINTERIORTRACE_SIP_GALERKIN_H

// interior-trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral without Lagrange multipliers
//

template<class IntegrandInteriorTrace>
class JacobianInteriorTrace_SIP_Galerkin_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_SIP_Galerkin_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianInteriorTrace_SIP_Galerkin_impl(const IntegrandInteriorTrace& fcn,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q)
  {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    if ((cellGroupGlobalL != 0) && (cellGroupGlobalR != 0) ) return;
    if ((cellGroupGlobalL == 0) && (cellGroupGlobalR == 0) ) return;

    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;


    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL, -1);
    std::vector<int> mapDOFGlobalR(nDOFR, -1);

    // trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, MatrixElemClass> integral(quadratureorder);

    // element PDE jacobian matrices wrt q
    MatrixElemClass mtxElemL(nDOFL, nDOFL);
    MatrixElemClass mtxElemR(nDOFR, nDOFR);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      //The cell that called this function is to the left of the trace
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // left/right elements
      int elemL = xfldTrace.getElementLeft( elem );
      int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      xfldCellL.getElement( xfldElemL, elemL );
      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellL.getElement( qfldElemL, elemL );
      qfldCellR.getElement( qfldElemR, elemR );

      if ((cellGroupGlobalL == 0) && (cellGroupGlobalR == 1) )
      {
        auto integrandFunctor = fcn_.integrand(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                               xfldElemL, qfldElemL,
                                               xfldElemR, qfldElemR);

        // PDE trace integration for canonical element
        mtxElemL = 0;
        integral( integrandFunctor, xfldElemTrace, mtxElemL );

        qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );

        mtxGlobalPDE_q_.scatterAdd( mtxElemL, mapDOFGlobalL.data(), mapDOFGlobalL.size(),
                                              mapDOFGlobalL.data(), mapDOFGlobalL.size()  );
      }
      else if ((cellGroupGlobalL == 1) && (cellGroupGlobalR == 0) )
      {
        auto integrandFunctor = fcn_.integrand(xfldElemTrace, canonicalTraceR, canonicalTraceL,
                                               xfldElemR, qfldElemR,
                                               xfldElemL, qfldElemL);

        // PDE trace integration for canonical element
        mtxElemR = 0;
        integral( integrandFunctor, xfldElemTrace, mtxElemR );

        qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nDOFR );

        mtxGlobalPDE_q_.scatterAdd( mtxElemR, mapDOFGlobalR.data(), mapDOFGlobalR.size(),
                                              mapDOFGlobalR.data(), mapDOFGlobalR.size() );
      }

    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
};

// Factory function

template<class IntegrandInteriorTrace, class MatrixQ>
JacobianInteriorTrace_SIP_Galerkin_impl<IntegrandInteriorTrace>
JacobianInteriorTrace_SIP_Galerkin( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q )
{
  return { fcn.cast(), mtxGlobalPDE_q };
}

} // namespace SANS

#endif  // JACOBIANINTERIORTRACE_GALERKIN_H
