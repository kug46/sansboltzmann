// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_GALERKIN_STABILIZATION_GALERKIN_H_
#define SRC_DISCRETIZATION_GALERKIN_STABILIZATION_GALERKIN_H_

// #define EXPTTAU

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/InverseLUP.h"
#include "LinearAlgebra/DenseLinAlg/InverseQR.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Trace.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/Identity.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"
#include "LinearAlgebra/Transpose.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "Stabilization_Nitsche.h"

#ifdef __INTEL_COMPILER
#include "LinearAlgebra/DenseLinAlg/tools/MatrixSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#endif

namespace SANS
{

enum class StabilizationType
{
  SUPG,
  GLS,
  Adjoint,
  AGLSPrimal,
  AGLSAdjoint,
  Unstabilized,
  nStabilizationType // must be last for testing
};

enum class TauType
{
  Constant,
  Glasby,
#ifdef EXPTTAU
  Experimental,
#endif
  nTauType // must be last for testing
};

//----------------------------------------------------------------------------//
// applies stabilized CG tau matrix to strong PDE residual
// strongPDE = cb*tau*strongPDE
// ArrayQ = MatrixQ*ArrayQ
// cb constant scaling, default to 1

class StabilizationMatrix : public StabilizationNitsche
{
public:

  typedef StabilizationNitsche BaseType;

  // order is used to make sure the adjoint doesn't get calculated with a different tau
  StabilizationMatrix(const StabilizationType stab, const TauType tauType = TauType::Glasby,
                      const int order = 1, const Real cbS = 1.0,
                      const int orderB = 1, const Real cbB = 2.0 ) :
    BaseType(orderB, cbB), stabType_(stab), tauType_(tauType), orderS_(order), cbS_(cbS) {}

  static bool checkStabTau(const StabilizationType stabType, const TauType tauType)
  {
    return true;
  }

  StabilizationType getStabType() const {return stabType_;}
  TauType getTauType() const {return tauType_;}

  void setStabOrder(const int order){ orderS_ = order;}
  int getStabOrder() const { return orderS_; }

  template<int N, class Tpq, class Tt>
  struct TauInverse
  {
    TauInverse(const DLA::MatrixS<N,N,Tpq>& numerator1, const DLA::MatrixS<N,N,Tpq>& numerator2, const DLA::MatrixS<N,N,Tt>& tauinv) :
      numerator1(numerator1), numerator2(numerator2), inv(tauinv) {}

    DLA::MatrixS<N,N,Tpq> numerator1;
    DLA::MatrixS<N,N,Tpq> numerator2;
    DLA::Fixed::MatrixFactorized< DLA::MatrixSLUPSolver, N, N, Tt > inv;
  };

  template<class Tpq, class Tt>
  struct TauInverse<1, Tpq, Tt>
  {
    TauInverse(const Tpq& numerator1, const Tpq& numerator2, const Tt& tauinv) :
      numerator1(numerator1), numerator2(numerator2), inv(tauinv) {}

    const Tpq numerator1;
    const Tpq numerator2;
    const Tt inv;
  };


  template<class Tq, class Tk, class Ti, class PDE, class ParamT>
  void solve(const PDE& pde, const ParamT& param,
             const typename PDE::template ArrayQ<Tq>& q,
             const typename PDE::template TensorMatrixQ<Tk>& K,
             const typename PDE::template MatrixQ<Tk>& dsdu,
             const Real *phi,
             const typename DLA::VectorS<PDE::D,Real> *gradphi,
             const int& order,  const int& nNodes, const int& nDOF,
             typename PDE::template ArrayQ<Ti>& strongPDE) const;

  template<class Tpq, class Tq, class Tk, class Tt, class PDE, class ParamT>
  TauInverse<PDE::N, Tpq, Tt>
  factorize(const PDE& pde, const ParamT& param,
            const typename PDE::template ArrayQ<Tq>& q,
            const typename PDE::template TensorMatrixQ<Tk>& K,
            const typename PDE::template MatrixQ<Tk>& dsdu,
            const Real *phi,
            const typename DLA::VectorS<PDE::D,Real> *gradphi,
            const int& order,  const int& nNodes, const int& nDOF) const;

  template<class Ti, int N, class Tpq, class Tt>
  void
  backsolve( const TauInverse<N,  Tpq, Tt>& tau, DLA::VectorS<N, Ti>& strongPDE ) const;

  template<class Ti, class Tpq, class Tt>
  void
  backsolve( const TauInverse<1, Tpq, Tt>& tau, Ti& strongPDE ) const;

  template<class Tq, class Tg, class Th, class Ti, class PDE, class ParamT >
  void
  constructStrongPDE(const PDE& pde, const ParamT& param,
            const typename PDE::template ArrayQ<Tq>& q,
            const typename PDE::template VectorArrayQ<Tg>& gradq,
            const typename PDE::template TensorSymArrayQ<Th>& hessq,
            const typename PDE::template ArrayQ<Ti>& source,
            const typename PDE::template ArrayQ<Real>& forcing,
            typename PDE::template ArrayQ<Ti>& strongPDE) const;


  template<class Tq, class Tg, class Th, class Ti, class PDE, class ParamT>
  void
  constructAdjointPDE(const PDE& pde, const ParamT& param,
            const typename PDE::template ArrayQ<Tq>& q,
            const typename PDE::template ArrayQ<Real>& qR,
            const typename PDE::template VectorArrayQ<Tg>& gradq,
            const typename PDE::template VectorArrayQ<Real>& gradqR,
            const typename PDE::template TensorSymArrayQ<Th>& hessq,
            const typename PDE::template TensorSymArrayQ<Real>& hessqR,
            const typename PDE::template TensorMatrixQ<Real>& K,
            const int& order,
            typename PDE::template ArrayQ<Ti>& strongPDE) const;

  template<class Tq, class Tg,class Ti, class PDE, class ParamT>
  void
  operatorSUPG(const PDE& pde, const ParamT& param,
            const typename PDE::template ArrayQ<Tq>& q,
            const typename PDE::template VectorArrayQ<Tg>& gradq,
            const typename DLA::VectorS<PDE::D,Real> *gradphi,
            const typename PDE::template ArrayQ<Ti>& strongPDE,
            const int& neqn,
            typename PDE::template ArrayQ<Ti> integrand[]) const;


  template<class T, class Ti, class PDE, class ParamT>
  void
  operatorSUPGFW(const PDE& pde, const ParamT& param,
            const typename PDE::template ArrayQ<T>& q,
            const typename PDE::template VectorArrayQ<T>& gradq,
            const typename PDE::template VectorArrayQ<T>& gradw,
            const typename PDE::template ArrayQ<T>& strongPDE,
            Ti& stabintegrand) const;


  template<class Tq, class Tg, class Th, class Tk, class Ti, class PDE, class ParamT>
  void
  constructHelperGLS(const PDE& pde, const ParamT& param,
                const typename PDE::template ArrayQ<Tq>& q,
                const typename PDE::template VectorArrayQ<Tg>& gradq,
                const typename PDE::template TensorSymArrayQ<Th>& hessq,
                const typename PDE::template TensorMatrixQ<Tk>& K,
                const typename PDE::template ArrayQ<Ti>& strongPDE,
                const int& order,
                typename PDE::template VectorArrayQ<Ti>& strongF,
                typename PDE::template VectorArrayQ<Ti>& strongGradK,
                typename PDE::template TensorArrayQ<Ti>& strongK,
                typename PDE::template ArrayQ<Ti>& strongS,
                typename PDE::template VectorArrayQ<Ti>& strongSgrad,
                typename PDE::template ArrayQ<Ti>& strongSgrad2) const;


  template<class Tq, class Tg, class Th, class Tk, class Ti, class PDE, class ParamT>
  void
  operatorGLS(const PDE& pde, const ParamT& param,
            const typename PDE::template ArrayQ<Tq>& q,
            const typename PDE::template VectorArrayQ<Tg>& gradq,
            const typename PDE::template TensorSymArrayQ<Th>& hessq,
            const typename PDE::template TensorMatrixQ<Tk>& K,
            const Real *phi,
            const typename DLA::VectorS<PDE::D,Real> *gradphi,
            const typename DLA::MatrixSymS<PDE::D, Real> *hessphi,
            const typename PDE::template ArrayQ<Ti>& strongPDE,
            const int& neqn,
            const int& order,
            typename PDE::template ArrayQ<Ti> integrand[]) const;


  template<class T, class Ti, class PDE, class ParamT>
  void
  operatorGLSFW(const PDE& pde, const ParamT& param,
            const typename PDE::template ArrayQ<T>& q,
            const typename PDE::template VectorArrayQ<T>& gradq,
            const typename PDE::template TensorSymArrayQ<T>& hessq,
            const typename PDE::template TensorMatrixQ<T>& K,
            const typename PDE::template ArrayQ<T>& w,
            const typename PDE::template VectorArrayQ<T>& gradw,
            const typename PDE::template TensorSymArrayQ<T>& hessw,
            const typename PDE::template ArrayQ<T>& strongPDE,
            const int& order,
            Ti& stabIntegrand) const;



  template<class Tq, class Tg, class Th, class Tk, class Ti, class PDE, class ParamT, class GradParamT>
  void
  constructHelperVMS(const PDE& pde, const ParamT& param, const GradParamT& gradparam,
                const typename PDE::template ArrayQ<Tq>& q,
                const typename PDE::template VectorArrayQ<Tg>& gradq,
                const typename PDE::template TensorSymArrayQ<Th>& hessq,
                const typename PDE::template TensorMatrixQ<Tk>& K,
                const typename PDE::template ArrayQ<Ti>& strongPDE,
                const int& order,
                typename PDE::template VectorArrayQ<Ti>& strongF,
                typename PDE::template VectorArrayQ<Ti>& strongGradK,
                typename PDE::template TensorArrayQ<Ti>& strongK,
                typename PDE::template ArrayQ<Ti>& strongS,
                typename PDE::template VectorArrayQ<Ti>& strongSgrad,
                typename PDE::template ArrayQ<Ti>& strongSgrad2) const;

  template<class Tq, class Tg, class Th, class Tk, class Ti, class PDE, class ParamT, class GradParamT>
  void
  operatorAdjoint(const PDE& pde, const ParamT& param, const GradParamT& gradparam,
            const typename PDE::template ArrayQ<Tq>& q,
            const typename PDE::template VectorArrayQ<Tg>& gradq,
            const typename PDE::template TensorSymArrayQ<Th>& hessq,
            const typename PDE::template TensorMatrixQ<Tk>& K,
            const Real *phi,
            const typename DLA::VectorS<PDE::D,Real> *gradphi,
            const typename DLA::MatrixSymS<PDE::D, Real> *hessphi,
            const typename PDE::template ArrayQ<Ti>& strongPDE,
            const int& neqn,
            const int& order,
            typename PDE::template ArrayQ<Ti> integrand[]) const;


  template<class T, class Ti, class PDE, class ParamT, class GradParamT>
  void
  operatorAdjointFW(const PDE& pde, const ParamT& param, const GradParamT& gradparam,
            const typename PDE::template ArrayQ<T>& q,
            const typename PDE::template VectorArrayQ<T>& gradq,
            const typename PDE::template TensorSymArrayQ<T>& hessq,
            const typename PDE::template TensorMatrixQ<T>& K,
            const typename PDE::template ArrayQ<T>& w,
            const typename PDE::template VectorArrayQ<T>& gradw,
            const typename PDE::template TensorSymArrayQ<T>& hessw,
            const typename PDE::template ArrayQ<T>& strongPDE,
            const int& order,
            Ti& stabIntegrand) const;

private:
  const StabilizationType stabType_;
  const TauType tauType_;
  int orderS_;
  const Real cbS_;
};

template<class Tq, class Tk, class Ti, class PDE, class ParamT>
void
StabilizationMatrix::solve(const PDE& pde, const ParamT& param,
           const typename PDE::template ArrayQ<Tq>& q,
           const typename PDE::template TensorMatrixQ<Tk>& K,
           const typename PDE::template MatrixQ<Tk>& dsdu,
           const Real *phi,
           const typename DLA::VectorS<PDE::D,Real> *gradphi,
           const int& order,  const int& nNodes, const int& nDOF,
           typename PDE::template ArrayQ<Ti>& strongPDE) const
{
  typedef typename Scalar<typename promote_Surreal<ParamT, Tq, Tk>::type>::type Tt;
  typedef typename Scalar<typename promote_Surreal<ParamT, Tq>::type>::type Tpq;

  // compute tau-inverse
  TauInverse<PDE::N, Tpq, Tt> tau = this->template factorize<Tpq, Tq, Tk, Tt>(pde, param, q, K, dsdu, phi, gradphi, order, nNodes, nDOF);

  // apply tau to strongPDE
  backsolve(tau, strongPDE);
}

template<class Tpq, class Tq, class Tk, class Tt, class PDE, class ParamT>
StabilizationMatrix::TauInverse<PDE::N, Tpq, Tt>
StabilizationMatrix::factorize(const PDE& pde, const ParamT& param,
          const typename PDE::template ArrayQ<Tq>& q,
          const typename PDE::template TensorMatrixQ<Tk>& K,
          const typename PDE::template MatrixQ<Tk>& dsdu,
          const Real *phi,
          const typename DLA::VectorS<PDE::D,Real> *gradphi,
          const int& order,  const int& nNodes, const int& nDOF) const
{
  typedef TauInverse<PDE::N, Tpq, Tt> TauInverseType;

  typename PDE::template MatrixQ<Tt> tauinv = 0;

  if (stabType_ != StabilizationType::Unstabilized)
  {
    // std::cout<< "Stabilizing" << std::endl;
    if (tauType_ == TauType::Constant)
    {
      tauinv = DLA::Identity();
      return TauInverseType(0, 0, tauinv);
    }
    else if (tauType_ == TauType::Glasby)
    {

      if (pde.hasFluxAdvective())
        for (int k = 0; k < nDOF; k++)
          pde.jacobianFluxAdvectiveAbsoluteValue(param, q, gradphi[k], tauinv);

      if (pde.hasFluxViscous())
      {
        // Below is a faster version of
        //for ( int k = 0; k < nDOF; k++ )
        //  tauinv += gradphi[k][i]*K(i,j)*gradphi[k][j];

        DLA::MatrixS<PDE::D,PDE::D,Real> gradphisum = 0;

        for ( int k = 0; k < nDOF; k++ )
          for ( int i = 0; i < PDE::D; i++ )
            for ( int j = 0; j < PDE::D; j++ )
              gradphisum(i,j) += gradphi[k][i]*gradphi[k][j];

        for ( int i = 0; i < PDE::D; i++ )
          for ( int j = 0; j < PDE::D; j++ )
            tauinv += gradphisum(i,j)*K(i,j);
      }

#if 0
      if (pde.hasSource())
        for (int k = 0; k < nDOF; k++)
        {
          tauinv += dsdu*phi[k];
        }
#else

      if (pde.hasSource())
        tauinv += dsdu;
#endif

      return TauInverseType(0, 0, tauinv);
    }
#ifdef EXPTTAU
    else if (tauType_ == TauType::Experimental)
    {
//        SANS_DEVELOPER_EXCEPTION("EXPERIMENTAL TAU NOT WORKING..");
////
#if 0
      //HUGHES 1990 TAU FOR 1D ADVECTION-DIFFUSION

      typename PDE::template MatrixQ<Tt> adv = 0;
      int nNodes = PDE::D + 1;
      if (pde.hasFluxAdvective())
        for (int k = 0; k < nNodes; k++)
          pde.jacobianFluxAdvectiveAbsoluteValue(param, q, gradphi[k], adv);

      typename PDE::template MatrixQ<Tt> visc = 0;
      if (pde.hasFluxViscous())
      {
        // Below is a faster version of
        //for ( int k = 0; k < nDOF; k++ )
        //  tauinv += gradphi[k][i]*K(i,j)*gradphi[k][j];

        DLA::MatrixS<PDE::D,PDE::D,Real> gradphisum = 0;

        for ( int k = 0; k < nNodes; k++ )
          for ( int i = 0; i < PDE::D; i++ )
            for ( int j = 0; j < PDE::D; j++ )
              gradphisum(i,j) += gradphi[k][i]*gradphi[k][j];

        for ( int i = 0; i < PDE::D; i++ )
          for ( int j = 0; j < PDE::D; j++ )
            visc += gradphisum(i,j)*K(i,j);
      }


      Tt advtr = DLA::tr(adv);
      Tt visctr = DLA::tr(visc);

      Real mk;
      if (order == 1) mk = 1./6.;
      if (order == 2) mk = 1./12.;
      if (order == 3) mk = 1./50.;
      if (order == 4) mk = 1./138.;

      Tt Pe = mk*advtr/visctr;

      Tt one = 1;
      Tt xi = min(Pe, one);

      tauinv = 2.*adv / xi;

//        Tt Pe = advtr / visctr;
//        if (Pe <= 2.0)
//          tauinv = 1.e16;
//        else
//          tauinv = adv/(0.5 - 1/Pe);
      return TauInverseType(0, 0, tauinv);
#else
      int nNodes = PDE::D + 1;
      //KAMENETSKII TAU
      Tq Cn1 = 0, Cn2 = 0;
      for (int k=0; k<nNodes; k++)
      {
        pde.stabMatrix(param, q, K, gradphi[k], tauinv);
        pde.getCN(param, q, gradphi[k],Cn1, Cn2);
      }

      typename PDE::template MatrixQ<Tq> dudv = 0;
      pde.adjointVariableJacobian(param, q, dudv);

      typename PDE::template MatrixQ<Tpq> taunum = dudv;

      typename PDE::template MatrixQ<Tpq> taunum2 = 0;
      pde.stabMatrix2(param, q, Cn1, Cn2, taunum2);

      return TauInverseType(taunum, taunum2, tauinv);

#endif
//


    }
#endif
    else
    {
      SANS_DEVELOPER_EXCEPTION("INVALID TAUTYPE");
    }

  }

  tauinv = DLA::Identity();
  return TauInverseType(0, 0, tauinv);
}

template<class Ti, int N, class Tpq, class Tt>
void
StabilizationMatrix::backsolve( const TauInverse<N,  Tpq, Tt>& tau, DLA::VectorS<N, Ti>& strongPDE ) const
{
  if (stabType_ != StabilizationType::Unstabilized)
  {
    if (tauType_ == TauType::Constant)
    {
      strongPDE *= cbS_;
    }
    else if (tauType_ == TauType::Glasby)
    {
      strongPDE = cbS_*tau.inv.backsolve(strongPDE);
    }
#ifdef EXPTTAU
    else if (tauType_ == TauType::Experimental)
    {
      DLA::VectorS<N,Ti> strongPDEcopy = strongPDE;

      DLA::VectorS<N, Ti> tmp = tau.inv.backsolve(strongPDE);
      tmp += tau.numerator2*strongPDEcopy;
      strongPDE = cb2*tau.numerator1*tmp;
    }
#endif
    else
    {
      SANS_DEVELOPER_EXCEPTION("INVALID TAUTYPE");
    }

  }
}

template<class Ti, class Tpq, class Tt>
void
StabilizationMatrix::backsolve( const TauInverse<1, Tpq, Tt>& tau, Ti& strongPDE ) const
{
  if (stabType_ != StabilizationType::Unstabilized)
  {
    if (tauType_ == TauType::Constant)
    {
      strongPDE *= cbS_;
    }
    else if (tauType_ == TauType::Glasby)
    {
      strongPDE = cbS_*strongPDE/tau.inv;
    }
#ifdef EXPTTAU
    else if (tauType_ == TauType::Experimental)
    {
      strongPDE = cb2*tau.numerator1*strongPDE/tau.inv;
    }
#endif
    else
    {
      SANS_DEVELOPER_EXCEPTION("INVALID TAUTYPE");
    }

  }
}


template<class Tq, class Tg, class Th, class Ti, class PDE, class ParamT >
void
StabilizationMatrix::constructStrongPDE(const PDE& pde, const ParamT& param,
          const typename PDE::template ArrayQ<Tq>& q,
          const typename PDE::template VectorArrayQ<Tg>& gradq,
          const typename PDE::template TensorSymArrayQ<Th>& hessq,
          const typename PDE::template ArrayQ<Ti>& source,
          const typename PDE::template ArrayQ<Real>& forcing,
          typename PDE::template ArrayQ<Ti>& strongPDE) const
{
  if (stabType_ == StabilizationType::Unstabilized)
  {
    SANS_DEVELOPER_EXCEPTION("NO NEED TO CONSTRUCT RHS FOR UNSTABILIZED");
    strongPDE = 0;
  }
  else if (stabType_ == StabilizationType::AGLSAdjoint)
  {
    SANS_DEVELOPER_EXCEPTION("SHOULD NOT BE CALLING STRONG PDE FOR AGLS");
  }
  else
  {
    if (pde.hasFluxAdvective())
      pde.strongFluxAdvective( param, q, gradq, strongPDE );

    if (pde.hasFluxViscous())
      pde.strongFluxViscous( param, q, gradq, hessq, strongPDE );

    if (pde.hasSource())
      strongPDE += source;

    if (pde.hasForcingFunction())
      strongPDE -= forcing;
  }

}




template<class Tq, class Tg, class Th, class Ti, class PDE, class ParamT>
void
StabilizationMatrix::constructAdjointPDE(const PDE& pde, const ParamT& param,
          const typename PDE::template ArrayQ<Tq>& q,
          const typename PDE::template ArrayQ<Real>& qR,
          const typename PDE::template VectorArrayQ<Tg>& gradq,
          const typename PDE::template VectorArrayQ<Real>& gradqR,
          const typename PDE::template TensorSymArrayQ<Th>& hessq,
          const typename PDE::template TensorSymArrayQ<Real>& hessqR,
          const typename PDE::template TensorMatrixQ<Real>& K,
          const int& order,
          typename PDE::template ArrayQ<Ti>& strongPDE) const
{

    //ASSEMBLE LINEARIZED ADJOINT PDE//CONSTRUCTS APPROPRIATE JACOBIANS FOR WEIGHT
    typename PDE::template VectorMatrixQ<Real> dFdu = 0;

    if (pde.hasFluxAdvective())
      pde.jacobianFluxAdvective( param, qR, dFdu );

    if ( pde.hasFluxViscous() )
      pde.jacobianFluxViscous( param, qR, gradqR, dFdu );

    typename PDE::template VectorTensorMatrixQ<Real> GradK = 0;
    //dViscous du term
    if ( pde.hasFluxViscous() )
      pde.diffusionViscousGradient( param, qR, gradqR, hessqR, GradK );

    //source term
    typename PDE::template MatrixQ<Real> dsdu = 0;
    if (pde.hasSource())
      pde.jacobianSourceHACK( param, qR, gradqR, dsdu );

    //gradient dependent source
    typename PDE::template VectorMatrixQ<Real> dsdgradu = 0;
    typename PDE::template MatrixQ<Real> grad_dsdgradu = 0;
    if (pde.needsSolutionGradientforSource())
    {
      pde.jacobianGradientSourceHACK( param, qR, gradqR, dsdgradu );
      pde.jacobianGradientSourceGradientHACK(param, qR, gradqR, hessqR, grad_dsdgradu);
    }

    typename PDE::template ArrayQ<Tq> u = 0;
    typename PDE::template VectorArrayQ<Ti> gradu = 0;
    typename PDE::template TensorSymArrayQ<Ti> hessu = 0;

    pde.masterState(param, q, u);
    pde.masterStateGradient(param, q, gradq, gradu);

    if (pde.hasFluxViscous() && order > 1 )
      pde.masterStateHessian(param, q, gradq, hessq, hessu);

    // ASSEMBLE TO INTEGRAND //
    //ADVECTIVE FLUX
    for (int i=0; i< PDE::D; i++)
      strongPDE += dFdu[i]*gradu[i];

    //VISCOUS
    if (pde.hasFluxViscous() && order > 1)
    {
      for (int i = 0; i < PDE::D; i++)
        for (int j = 0; j< PDE::D; j++)
          strongPDE +=  K(i,j)*hessu(i,j);

      for (int i = 0; i < PDE::D; i++)
      {
        typename PDE::template MatrixQ<Real> tmp = 0;
        for (int j = 0; j< PDE::D; j++)
          tmp += GradK[j](i,j);
        //
        strongPDE +=  tmp*gradu[i];
      }
    }

    //SOURCE
    if (pde.hasSource())
    {

      //necessary entropy variable stuff
      strongPDE -= Transpose(dsdu)*u;

      if (pde.needsSolutionGradientforSource() )
      {
        for (int i=0; i< PDE::D; i++)
          strongPDE += Transpose(dsdgradu[i])*gradu[i];

        strongPDE += Transpose(grad_dsdgradu)*u;
      }
    }

}



template<class Tq, class Tg,class Ti, class PDE, class ParamT>
void
StabilizationMatrix::operatorSUPG(const PDE& pde, const ParamT& param,
          const typename PDE::template ArrayQ<Tq>& q,
          const typename PDE::template VectorArrayQ<Tg>& gradq,
          const typename DLA::VectorS<PDE::D,Real> *gradphi,
          const typename PDE::template ArrayQ<Ti>& strongPDE,
          const int& neqn,
          typename PDE::template ArrayQ<Ti> integrand[]) const
{

  if (pde.hasFluxAdvective())
  {
    typename PDE::template VectorMatrixQ<Ti> dFdu = 0;

    pde.jacobianFluxAdvective( param, q, dFdu );

    typename PDE::template VectorArrayQ<Ti> strongF = 0;
    for (int d = 0; d < PDE::D; d++)
      strongF[d] = dFdu[d] * strongPDE;

    if (pde.hasFluxAdvective())
      for (int k = 0; k < neqn; k++)
        integrand[k] += dot( gradphi[k], strongF );
  }

}

template<class T, class Ti, class PDE, class ParamT>
void
StabilizationMatrix::operatorSUPGFW(const PDE& pde, const ParamT& param,
          const typename PDE::template ArrayQ<T>& q,
          const typename PDE::template VectorArrayQ<T>& gradq,
          const typename PDE::template VectorArrayQ<T>& gradw,
          const typename PDE::template ArrayQ<T>& strongPDE,
          Ti& stabintegrand) const
{

  if (pde.hasFluxAdvective())
  {
    typename PDE::template VectorMatrixQ<T> dFdu = 0;
    pde.jacobianFluxAdvective( param, q, dFdu );

    typename PDE::template VectorArrayQ<T> strongF = 0;
    for (int d = 0; d < PDE::D; d++)
      strongF[d] = dFdu[d] * strongPDE;

    if (pde.hasFluxAdvective())
      stabintegrand += dot( gradw, strongF );
  }

}

template<class Tq, class Tg, class Th, class Tk, class Ti, class PDE, class ParamT>
void
StabilizationMatrix::constructHelperGLS(const PDE& pde, const ParamT& param,
              const typename PDE::template ArrayQ<Tq>& q,
              const typename PDE::template VectorArrayQ<Tg>& gradq,
              const typename PDE::template TensorSymArrayQ<Th>& hessq,
              const typename PDE::template TensorMatrixQ<Tk>& K,
              const typename PDE::template ArrayQ<Ti>& strongPDE,
              const int& order,
              typename PDE::template VectorArrayQ<Ti>& strongF,
              typename PDE::template VectorArrayQ<Ti>& strongGradK,
              typename PDE::template TensorArrayQ<Ti>& strongK,
              typename PDE::template ArrayQ<Ti>& strongS,
              typename PDE::template VectorArrayQ<Ti>& strongSgrad,
              typename PDE::template ArrayQ<Ti>& strongSgrad2) const
{
  typedef typename promote_Surreal<Tq,Tg>::type Tqg;
  //CONSTRUCTS APPROPRIATE JACOBIANS FOR WEIGHT
  typename PDE::template VectorMatrixQ<Tq> dFdu = 0;
#ifdef __INTEL_COMPILER
  typedef typename PDE::template MatrixQ<Ti> MatrixQ;
  // Intel optimization does not properly initiaize dFdu = 0, sigh...
  for (int i = 0; i < PDE::D; i++)
    for (int m = 0; m < DLA::MatrixSize<MatrixQ>::M; m++)
      for (int n = 0; n < DLA::MatrixSize<MatrixQ>::N; n++)
        DLA::index(dFdu[i],m,n) = 0;
#endif

  if (pde.hasFluxAdvective())
    pde.jacobianFluxAdvective( param, q, dFdu );

//  if ( pde.hasFluxViscous() )
//    pde.jacobianFluxViscous( param, q, gradq, dFdu );

  if (pde.hasFluxAdvective() || pde.hasFluxViscous())
    for (int d = 0; d < PDE::D; d++)
      strongF[d] = dFdu[d] * strongPDE;

  typename PDE::template VectorTensorMatrixQ<Ti> GradK = 0;
#ifdef __INTEL_COMPILER
  // Intel optimization does not properly initiaize GradK = 0, sigh...
  for (int i = 0; i < PDE::D; i++)
    for (int j = 0; j < PDE::D; j++)
      for (int k = 0; k < PDE::D; k++)
        for (int m = 0; m < DLA::MatrixSize<MatrixQ>::M; m++)
          for (int n = 0; n < DLA::MatrixSize<MatrixQ>::N; n++)
            DLA::index(GradK[i](j,k),m,n) = 0;
#endif

  //dViscous du term
  if (pde.hasFluxViscous() && order > 1)
  {
//    pde.diffusionViscousGradient( param, q, gradq, hessq, GradK );
//
//    for (int i = 0; i < PDE::D; i++)
//      for (int j = 0; j< PDE::D; j++)
//        strongGradK[i] += GradK[i](i,j) * strongPDE;

    for (int i = 0; i < PDE::D; i++)
      for (int j = 0; j < PDE::D; j++)
        strongK(i,j) = K(i,j) * strongPDE;

  }

  //    //source term
  typename PDE::template MatrixQ<Tqg> dsdu = 0;

  if (pde.hasSource())
  {
    //    pde.jacobianSourceHACK( param, q, gradq, dsdu );
    //    strongS = Transpose(dsdu)*strongPDE;

    pde.jacobianSource( param, q, gradq, dsdu );
    strongS = dsdu*strongPDE;

    //gradient dependent source
    typename PDE::template VectorMatrixQ<Tqg> dsdgradu = 0;
    //    typename PDE::template MatrixQ<Ti> grad_dsdgradu = 0;
    if (pde.needsSolutionGradientforSource())
    {
      //    pde.jacobianGradientSourceHACK( param, q, gradq, dsdgradu );
      pde.jacobianGradientSource( param, q, gradq, dsdgradu );

      for (int d = 0; d < PDE::D; d++)
      {
        strongSgrad[d] = dsdgradu[d]*strongPDE;
        //      strongSgrad[d] = Transpose(dsdgradu[d])*strongPDE;
      }
    }

  }

}


template<class Tq, class Tg, class Th, class Tk, class Ti, class PDE, class ParamT>
void
StabilizationMatrix::operatorGLS(const PDE& pde, const ParamT& param,
          const typename PDE::template ArrayQ<Tq>& q,
          const typename PDE::template VectorArrayQ<Tg>& gradq,
          const typename PDE::template TensorSymArrayQ<Th>& hessq,
          const typename PDE::template TensorMatrixQ<Tk>& K,
          const Real *phi,
          const typename DLA::VectorS<PDE::D,Real> *gradphi,
          const typename DLA::MatrixSymS<PDE::D, Real> *hessphi,
          const typename PDE::template ArrayQ<Ti>& strongPDE,
          const int& neqn,
          const int& order,
          typename PDE::template ArrayQ<Ti> integrand[]) const
{
  //ASSEMBLE LINEARIZED STRONG FORM PDE
  typename PDE::template VectorArrayQ<Ti> strongF = 0;
  typename PDE::template VectorArrayQ<Ti> strongGradK = 0;
  typename PDE::template TensorArrayQ<Ti> strongK = 0;
  typename PDE::template ArrayQ<Ti> strongS = 0;
  typename PDE::template VectorArrayQ<Ti> strongSgrad = 0;
  typename PDE::template ArrayQ<Ti> strongSgrad2 = 0;

  constructHelperGLS<Tq, Tg, Th, Tk, Ti>(pde, param, q, gradq, hessq, K, strongPDE, order,
                                      strongF, strongGradK, strongK, strongS, strongSgrad, strongSgrad2);

    // ASSEMBLE TO INTEGRAND
    for (int k = 0; k < neqn; k++)
    {
      //ADVECTIVE FLUX
      integrand[k] += dot( gradphi[k], strongF );

      //VISCOUS
      if (pde.hasFluxViscous() && order > 1)
      {
        for (int i = 0; i < PDE::D; i++)
          for (int j = 0; j< PDE::D; j++)
            integrand[k] -= hessphi[k](i,j)*strongK(i,j);

//        integrand[k] -= dot( gradphi[k], strongGradK );

      }

      //SOURCE
      if (pde.hasSource())
        integrand[k] += phi[k]*strongS;

      if (pde.needsSolutionGradientforSource() )
        integrand[k] += dot( gradphi[k], strongSgrad );
    }

}

template<class T, class Ti, class PDE, class ParamT>
void
StabilizationMatrix::operatorGLSFW(const PDE& pde, const ParamT& param,
          const typename PDE::template ArrayQ<T>& q,
          const typename PDE::template VectorArrayQ<T>& gradq,
          const typename PDE::template TensorSymArrayQ<T>& hessq,
          const typename PDE::template TensorMatrixQ<T>& K,
          const typename PDE::template ArrayQ<T>& w,
          const typename PDE::template VectorArrayQ<T>& gradw,
          const typename PDE::template TensorSymArrayQ<T>& hessw,
          const typename PDE::template ArrayQ<T>& strongPDE,
          const int& order,
          Ti& stabIntegrand) const
{
  //ASSEMBLE LINEARIZED STRONG FORM PDE
  typename PDE::template VectorArrayQ<Ti> strongF = 0;
  typename PDE::template VectorArrayQ<Ti> strongGradK = 0;
  typename PDE::template TensorArrayQ<Ti> strongK = 0;
  typename PDE::template ArrayQ<Ti> strongS = 0;
  typename PDE::template VectorArrayQ<Ti> strongSgrad = 0;
  typename PDE::template ArrayQ<Ti> strongSgrad2 = 0;

  constructHelperGLS<T, T, T, T, T>(pde, param, q, gradq, hessq, K, strongPDE, order,
                                      strongF, strongGradK, strongK, strongS, strongSgrad, strongSgrad2);

    // ASSEMBLE TO INTEGRAND
      //ADVECTIVE FLUX
    stabIntegrand += dot( gradw, strongF );

    //VISCOUS
    if (pde.hasFluxViscous() && order > 1)
    {
      for (int i = 0; i < PDE::D; i++)
        for (int j = 0; j< PDE::D; j++)
          stabIntegrand -= dot(hessw(i,j),strongK(i,j));

//      stabIntegrand -= dot( gradw, strongGradK );
    }

    //SOURCE
    if (pde.hasSource())
      stabIntegrand += dot(w,strongS);

    if (pde.needsSolutionGradientforSource() )
      stabIntegrand += dot( gradw, strongSgrad );


}


template<class Tq, class Tg, class Th, class Tk, class Ti, class PDE, class ParamT, class GradParamT>
void
StabilizationMatrix::constructHelperVMS(const PDE& pde, const ParamT& param, const GradParamT& gradparam,
                                        const typename PDE::template ArrayQ<Tq>& q,
                                        const typename PDE::template VectorArrayQ<Tg>& gradq,
                                        const typename PDE::template TensorSymArrayQ<Th>& hessq,
                                        const typename PDE::template TensorMatrixQ<Tk>& K,
                                        const typename PDE::template ArrayQ<Ti>& strongPDE,
                                        const int& order,
                                        typename PDE::template VectorArrayQ<Ti>& strongF,
                                        typename PDE::template VectorArrayQ<Ti>& strongGradK,
                                        typename PDE::template TensorArrayQ<Ti>& strongK,
                                        typename PDE::template ArrayQ<Ti>& strongS,
                                        typename PDE::template VectorArrayQ<Ti>& strongSgrad,
                                        typename PDE::template ArrayQ<Ti>& strongSgrad2) const
{
  //CONSTRUCTS APPROPRIATE JACOBIANS FOR WEIGHT
  typename PDE::template VectorMatrixQ<Ti> dFdu = 0;

#ifdef __INTEL_COMPILER
  typedef typename PDE::template MatrixQ<Ti> MatrixQ;
  // Intel optimization does not properly initiaize dFdu = 0, sigh...
  for (int i = 0; i < PDE::D; i++)
    for (int m = 0; m < DLA::MatrixSize<MatrixQ>::M; m++)
      for (int n = 0; n < DLA::MatrixSize<MatrixQ>::N; n++)
        DLA::index(dFdu[i],m,n) = 0;
#endif

  if (pde.hasFluxAdvective())
    pde.jacobianFluxAdvective( param, q, dFdu );

  if (pde.hasFluxViscous())
    pde.jacobianFluxViscous( param, q, gradq, dFdu );

  if (pde.hasFluxAdvective() || pde.hasFluxViscous())
    for (int d = 0; d < PDE::D; d++)
      strongF[d] = dFdu[d] * strongPDE;

  typename PDE::template VectorTensorMatrixQ<Ti> GradK = 0;
  if ( pde.hasFluxViscous() )
  {
    pde.diffusionViscousGradient( param, q, gradq, hessq, GradK );

    for (int i = 0; i < PDE::D; i++)
      for (int j = 0; j< PDE::D; j++)
        strongGradK[i] += GradK[j](i,j) * strongPDE; //transpose on grad dot K^T

    // Hessian Term
    if (order > 1)
      for (int i = 0; i < PDE::D; i++)
        for (int j = 0; j < PDE::D; j++)
          strongK(i,j) = K(i,j) * strongPDE;
  }

  //source term
  typename PDE::template MatrixQ<Ti> dsdu = 0;
  if (pde.hasSource())
  {
    pde.jacobianSource( param, q, gradq, dsdu );
    strongS = dsdu * strongPDE;
  }

  //gradient dependent source
  typename PDE::template VectorMatrixQ<Ti> dsdgradu = 0;
  typename PDE::template MatrixQ<Ti> grad_dsdgradu = 0;
  if (pde.needsSolutionGradientforSource())
  {
    pde.jacobianGradientSource( param, q, gradq, dsdgradu );

    for (int d = 0; d < PDE::D; d++)
      strongSgrad[d] = dsdgradu[d] * strongPDE;

//    pde.jacobianGradientSourceGradient(param, gradparam, q, gradq, hessq, grad_dsdgradu);
    pde.jacobianGradientSourceGradient(param, q, gradq, hessq, grad_dsdgradu);
    strongSgrad2 = grad_dsdgradu*strongPDE;
  }

}


template<class Tq, class Tg, class Th, class Tk, class Ti, class PDE, class ParamT, class GradParamT>
void
StabilizationMatrix::operatorAdjoint(const PDE& pde, const ParamT& param, const GradParamT& gradparam,
          const typename PDE::template ArrayQ<Tq>& q,
          const typename PDE::template VectorArrayQ<Tg>& gradq,
          const typename PDE::template TensorSymArrayQ<Th>& hessq,
          const typename PDE::template TensorMatrixQ<Tk>& K,
          const Real *phi,
          const typename DLA::VectorS<PDE::D,Real> *gradphi,
          const typename DLA::MatrixSymS<PDE::D, Real> *hessphi,
          const typename PDE::template ArrayQ<Ti>& strongPDE,
          const int& neqn,
          const int& order,
          typename PDE::template ArrayQ<Ti> integrand[]) const
{
  //ASSEMBLE LINEARIZED ADJOINT PDE

  typename PDE::template VectorArrayQ<Ti> strongF = 0;
  typename PDE::template VectorMatrixQ<Ti> dFdu = 0;
  typename PDE::template VectorArrayQ<Ti> strongGradK = 0;
  typename PDE::template TensorArrayQ<Ti> strongK = 0;
  typename PDE::template ArrayQ<Ti> strongS = 0;
  typename PDE::template VectorArrayQ<Ti> strongSgrad = 0;
  typename PDE::template ArrayQ<Ti> strongSgrad2 = 0;

  constructHelperVMS<Tq, Tg, Th, Tk, Ti>(pde, param, gradparam, q, gradq, hessq, K, strongPDE, order,
                                      strongF, strongGradK, strongK, strongS, strongSgrad, strongSgrad2);

  // ASSEMBLE TO INTEGRAND
  for (int k = 0; k < neqn; k++)
  {
    //ADVECTIVE FLUX
    integrand[k] += dot( gradphi[k], strongF );

    //VISCOUS
    if (pde.hasFluxViscous() && order > 1)
    {
      for (int i = 0; i < PDE::D; i++)
        for (int j = 0; j< PDE::D; j++)
          integrand[k] += hessphi[k](i,j)*strongK(i,j);

      integrand[k] += dot( gradphi[k], strongGradK );
    }

    //SOURCE
    if (pde.hasSource())
      integrand[k] -= phi[k]*strongS;

    if (pde.needsSolutionGradientforSource() )
    {
      integrand[k] += dot( gradphi[k], strongSgrad );
      integrand[k] += phi[k]*strongSgrad2;
    }

  }

}


template<class T, class Ti, class PDE, class ParamT, class GradParamT>
void
StabilizationMatrix::operatorAdjointFW(const PDE& pde, const ParamT& param, const GradParamT& gradparam,
                                       const typename PDE::template ArrayQ<T>& q,
                                       const typename PDE::template VectorArrayQ<T>& gradq,
                                       const typename PDE::template TensorSymArrayQ<T>& hessq,
                                       const typename PDE::template TensorMatrixQ<T>& K,
                                       const typename PDE::template ArrayQ<T>& w,
                                       const typename PDE::template VectorArrayQ<T>& gradw,
                                       const typename PDE::template TensorSymArrayQ<T>& hessw,
                                       const typename PDE::template ArrayQ<T>& strongPDE,
                                       const int& order,
                                       Ti& stabIntegrand) const
{
  //ASSEMBLE LINEARIZED ADJOINT PDE

  typename PDE::template VectorArrayQ<Ti> strongF = 0;
  typename PDE::template VectorMatrixQ<Ti> dFdu = 0;
  typename PDE::template VectorArrayQ<Ti> strongGradK = 0;
  typename PDE::template TensorArrayQ<Ti> strongK = 0;
  typename PDE::template ArrayQ<Ti> strongS = 0;
  typename PDE::template VectorArrayQ<Ti> strongSgrad = 0;
  typename PDE::template ArrayQ<Ti> strongSgrad2 = 0;

  constructHelperVMS<T, T, T, T, T>(pde, param, gradparam, q, gradq, hessq, K, strongPDE, order,
                                    strongF, strongGradK, strongK, strongS, strongSgrad, strongSgrad2);

  // ASSEMBLE TO INTEGRAND
  //ADVECTIVE FLUX
  stabIntegrand += dot( gradw, strongF );

  //VISCOUS
  if (pde.hasFluxViscous() && order > 1)
  {
    for (int i = 0; i < PDE::D; i++)
      for (int j = 0; j< PDE::D; j++)
        stabIntegrand += dot(hessw(i,j),strongK(i,j));

    stabIntegrand += dot( gradw, strongGradK );
  }

  //SOURCE
  if (pde.hasSource())
    stabIntegrand -= dot(w,strongS);

  if (pde.needsSolutionGradientforSource() )
  {
    stabIntegrand += dot( gradw, strongSgrad );
    stabIntegrand += dot(w,strongSgrad2);
  }

}



}

#endif /* SRC_DISCRETIZATION_GALERKIN_STABILIZATION_GALERKIN_H_ */
