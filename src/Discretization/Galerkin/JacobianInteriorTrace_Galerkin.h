// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_GALERKIN_H
#define JACOBIANINTERIORTRACE_GALERKIN_H

// interior-trace integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "JacobianInteriorTrace_Galerkin_Element.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral without Lagrange multipliers
//

template<class IntegrandInteriorTrace>
class JacobianInteriorTrace_Galerkin_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_Galerkin_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template MatrixQ<Real> MatrixQ;

  typedef JacobianElemInteriorTrace_Galerkin<MatrixQ> JacobianElemInteriorTraceType;

  JacobianInteriorTrace_Galerkin_impl(const IntegrandInteriorTrace& fcn,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), comm_rank_(0) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

#ifdef SANS_MPI
    comm_rank_ = qfld.comm()->rank();

    // MPI ranks assume a DG space below
    SANS_ASSERT(qfld.spaceType() == SpaceType::Discontinuous);
#endif
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;


    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;


    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL);
    std::vector<int> mapDOFGlobalR(nDOFR);

    JacobianElemInteriorTraceSize sizeL(nDOFL, nDOFL, nDOFR);
    JacobianElemInteriorTraceSize sizeR(nDOFR, nDOFL, nDOFR);

    // trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType, JacobianElemInteriorTraceType>
      integral(quadratureorder);

    // element PDE jacobian matrices wrt q
    JacobianElemInteriorTraceType mtxElemL(sizeL);
    JacobianElemInteriorTraceType mtxElemR(sizeR);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // left/right elements
      int elemL = xfldTrace.getElementLeft( elem );
      int elemR = xfldTrace.getElementRight( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );

      const int rankL = qfldElemL.rank();
      const int rankR = qfldElemR.rank();

      // nothing to do if both elements are ghost elements
      if (rankL != comm_rank_ && rankR != comm_rank_) continue;

      // trace integration for canonical element
      integral(
           fcn_.integrand( xfldElemTrace, canonicalTraceL, canonicalTraceR,
                           xfldElemL, qfldElemL,
                           xfldElemR, qfldElemR),
           xfldElemTrace, mtxElemL, mtxElemR );

      // scatter-add element jacobian to global

      scatterAdd(
          qfldCellL, qfldCellR,
          rankL, rankR,
          elemL, elemR,
          mapDOFGlobalL.data(), mapDOFGlobalL.size(),
          mapDOFGlobalR.data(), mapDOFGlobalR.size(),
          mtxElemL.PDE_qL,
          mtxElemL.PDE_qR,
          mtxElemR.PDE_qL,
          mtxElemR.PDE_qR,
          mtxGlobalPDE_q_ );
    }
  }


//----------------------------------------------------------------------------//
  template <class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupTypeL& qfldL,
      const QFieldCellGroupTypeR& qfldR,
      const int rankL, const int rankR,
      const int elemL, const int elemR,
      int mapDOFGlobalL[], const int nDOFL,
      int mapDOFGlobalR[], const int nDOFR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qR,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
  {
    qfldL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL, nDOFL );
    qfldR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR, nDOFR );

    // jacobian wrt qL
    if (rankL == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qL, mapDOFGlobalL, nDOFL, mapDOFGlobalL, nDOFL );
    if (rankR == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qL, mapDOFGlobalR, nDOFR, mapDOFGlobalL, nDOFL );

    // jacobian qrt qR
    if (rankL == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qR, mapDOFGlobalL, nDOFL, mapDOFGlobalR, nDOFR );
    if (rankR == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qR, mapDOFGlobalR, nDOFR, mapDOFGlobalR, nDOFR );
  }

#if 0
//----------------------------------------------------------------------------//
  template <class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupTypeL& qfldL,
      const QFieldCellGroupTypeR& qfldR,
      const int rankL, const int rankR,
      const int elemL, const int elemR,
      int mapDOFGlobalL[], const int nDOFL,
      int mapDOFGlobalR[], const int nDOFR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemLL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemLR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemRL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemRR,
      SparseMatrixType< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemLL, elemL, elemL );
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemLR, elemL, elemR );
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemRL, elemR, elemL );
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemRR, elemR, elemR );
  }
#endif

protected:
  const IntegrandInteriorTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandInteriorTrace, class MatrixQ>
JacobianInteriorTrace_Galerkin_impl<IntegrandInteriorTrace>
JacobianInteriorTrace_Galerkin( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q )
{
  return { fcn.cast(), mtxGlobalPDE_q };
}

} // namespace SANS

#endif  // JACOBIANINTERIORTRACE_GALERKIN_H
