// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php


#ifndef EXTRACTCGLOCALBOUNDARIES_H
#define EXTRACTCGLOCALBOUNDARIES_H

#include <vector>
#include <array>

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"
#include "BasisFunction/LagrangeDOFMap.h"
#include "tools/safe_at.h"


#if defined(WHOLEPATCH) && defined(INNERPATCH)
#error  // Can't compile with both WHOLEPATCH and INNERPATCH
#endif


namespace SANS
{
  // The
  enum class Patch { Broken, Whole, Inner };

// We really need a better version of this switch ultimately, but only one of these should win.

  // Function for extracting fixed dofs off of an existing Field. Used in CG local patch
  template< class PhysDim, class TopoDim, class ArrayQ>
  void extractFreeDOFs( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                        const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                        std::array<std::vector<int>,2>& freeDOFs,
                        const Patch patch = Patch::Broken );

  // Function for extracting fixed dofs off of an existing Field. Used in CG local patch
  template< class PhysDim, class TopoDim, class ArrayQ>
  void extractFreeDOFs( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                        std::vector<int>& freeDOFs,
                        const Patch patch = Patch::Broken );

  // Function for creating maps between dofs of a given cell group to another in a broken group
  // For instance, mapping from cell group 1 to cell group 0, for those dofs in the interface
  template <class PhysDim, class TopoDim, class ArrayQ, class ArrayT>
  std::map<int,int> constructDOFMap( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                     const Field<PhysDim, TopoDim, ArrayT>& cfld,
                                     const int cellGroup_From, const int cellGroup_To );

  // Function for mapping from a DG field to a CG field, whose dofs are keyed into using the brokenDOFMap
  template< class PhysDim, class TopoDim, class ArrayQ>
  std::map<int,int> constructDGtoCGDOFMap( const Field<PhysDim, TopoDim, ArrayQ>& dfld, // discontinuous field going from
                                          const Field<PhysDim, TopoDim, ArrayQ>& qfld, // broken field that discontinuous field maps to
                                          const std::map<int,int>& brokenDOFMap, // map between broken dofs across trace
                                          const int cellGroup );

  // class for marking the xfield support dofs and freeing group 0 dofs
  template <class PhysDim, class ArrayQ >
  class MarkFreeCellGroup : public GroupFunctorCellType<MarkFreeCellGroup<PhysDim,ArrayQ>>
  {
  public:
    MarkFreeCellGroup( std::map<int,bool>& xfld_free_dofmap,
                std::map<int,bool>& qfld_free_dofmap, const std::vector<int> cellGroups  ) :
                xfld_free_dofmap_(xfld_free_dofmap), qfld_free_dofmap_(qfld_free_dofmap),
                cellGroups_(cellGroups){}

    std::size_t nCellGroups() const          { return cellGroups_.size(); }
    std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

    //----------------------------------------------------------------------------//
    template< class Topology >
    void
    apply(const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>,
                                    Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
          template FieldCellGroupType<Topology>& fldsCell,
          const int cellGroupGlobal);

  protected:
    std::map<int,bool>& xfld_free_dofmap_; // map of which xnodes are going to be freed, need for later
    std::map<int,bool>& qfld_free_dofmap_; // map of which qfld dofs are going to be freed
    const std::vector<int> cellGroups_;
  };

  // Unneeded in the end
  // class for marking xfield dofs that are in boundary traces, these should all be free to apply their boundary conditions
  template< class PhysDim, class ArrayQ >
  class MarkFreeBTraceGroup : public GroupFunctorBoundaryTraceType<MarkFreeBTraceGroup<PhysDim,ArrayQ>>
  {
  public:
    MarkFreeBTraceGroup( std::map<int,bool>& xfld_free_dofmap,
                      std::map<int,bool>& qfld_free_dofmap,
                      const std::vector<int>& boundaryTraceGroups ) :
                      xfld_free_dofmap_(xfld_free_dofmap),
                      qfld_free_dofmap_(qfld_free_dofmap),
                      boundaryTraceGroups_(boundaryTraceGroups) {}

    std::size_t nBoundaryTraceGroups() const          { return boundaryTraceGroups_.size(); }
    std::size_t boundaryTraceGroup(const int n) const { return boundaryTraceGroups_[n];     }

    //----------------------------------------------------------------------------//
    template< class TopologyTrace >
    void
    apply(const typename FieldTuple<XField<PhysDim, typename TopologyTrace::CellTopoDim>,
                                    Field<PhysDim,typename TopologyTrace::CellTopoDim,ArrayQ>,TupleClass<>>::
          template FieldTraceGroupType<TopologyTrace>& fldsTrace,
         const int traceGroupGlobal);

  protected:
    std::map<int,bool>& xfld_free_dofmap_; // map of which xnodes are going to be freed, need for later
    std::map<int,bool>& qfld_free_dofmap_; // map of which xnodes are going to be freed, need for later
    const std::vector<int>& boundaryTraceGroups_;
  };

  // class for marking and freezing all dofs in ghost boundaries
  template <class PhysDim, class ArrayQ >
  class FreezeGhostBoundaryTrace : public GroupFunctorBoundaryCellType<FreezeGhostBoundaryTrace<PhysDim,ArrayQ>>
  {
  public:
    FreezeGhostBoundaryTrace( std::map<int,bool>& xfld_free_dofmap,
                              std::map<int,bool>& qfld_free_dofmap,
                              const std::vector<int>& boundaryTraceGroups  ) :
                              xfld_free_dofmap_(xfld_free_dofmap),
                              qfld_free_dofmap_(qfld_free_dofmap),
                              boundaryTraceGroups_(boundaryTraceGroups) {}

    std::size_t nBoundaryGroups() const          { return boundaryTraceGroups_.size(); }
    std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n];     }

    //----------------------------------------------------------------------------//
    template< class TopologyTrace, class TopologyL >
    void
    apply( const typename Field<PhysDim, typename TopologyL::TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCell,
           const int cellGroupGlobalL,
           const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
           const int traceGroupGlobal);

  protected:
    std::map<int,bool>& xfld_free_dofmap_; // map of which xnodes are going to be frozen
    std::map<int,bool>& qfld_free_dofmap_; // map of which qfld dofs are to be frozen
    const std::vector<int>& boundaryTraceGroups_;
  };

  // class for marking the group 1 dofs using the marked xfield support
  template <class PhysDim, class ArrayQ>
  class ApplyMarkCellGroup : public GroupFunctorCellType<ApplyMarkCellGroup<PhysDim,ArrayQ>>
  {
  public:
    ApplyMarkCellGroup( std::map<int,bool>& qfld_free_dofmap, const std::vector<int>& cellGroups,
                        const bool cellGroupMark ) :
                        qfld_free_dofmap_(qfld_free_dofmap), cellGroups_(cellGroups), cellGroupMark_(cellGroupMark) {}

    std::size_t nCellGroups() const          { return cellGroups_.size(); }
    std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

    //----------------------------------------------------------------------------//
    template< class Topology >
    void
    apply(const typename Field<PhysDim, typename Topology::TopoDim, ArrayQ>::
          template FieldCellGroupType<Topology>& qfldCell,
          const int cellGroupGlobal)
    {
      std::vector<int> qfldCellGlobalMap( qfldCell.nBasis() );

      for (int elem = 0; elem < qfldCell.nElem(); elem++ )
      {
        qfldCell.associativity(elem).getGlobalMapping( qfldCellGlobalMap.data(), qfldCellGlobalMap.size() );

        // mark the qfield dofs as free
        for (int i = 0; i < qfldCell.nBasis(); i++)
          qfld_free_dofmap_.at( qfldCellGlobalMap[i] ) = cellGroupMark_; // apply the mark from the constructor
      }
    }
  protected:
    std::map<int,bool>& qfld_free_dofmap_; // map of which qfld dofs are going to be freed
    const std::vector<int>& cellGroups_;
    const bool cellGroupMark_; // how the cell group is going to be marked
  };


  // class for marking the lagrange dofs using the marked xfield support
  template < class PhysDim, class ArrayQ>
  class FreeLGBTraceGroups : public GroupFunctorBoundaryTraceType<FreeLGBTraceGroups<PhysDim,ArrayQ>>
  {
  public:
    FreeLGBTraceGroups( const std::map<int,bool>& xfld_free_dofmap,
                        std::map<int,bool>& lgfld_free_dofmap, const std::vector<int>& boundaryTraceGroups  ) :
                    xfld_free_dofmap_(xfld_free_dofmap), lgfld_free_dofmap_(lgfld_free_dofmap),
                    boundaryTraceGroups_(boundaryTraceGroups) {}

    std::size_t nBoundaryTraceGroups() const          { return boundaryTraceGroups_.size(); }
    std::size_t boundaryTraceGroup(const int n) const { return boundaryTraceGroups_[n];     }

    //----------------------------------------------------------------------------//
    template< class TopologyTrace >
    void
    apply(const typename FieldTuple<XField<PhysDim, typename TopologyTrace::CellTopoDim>,
                                     Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>, TupleClass<>>::
          template FieldTraceGroupType<TopologyTrace>& fldsTrace,
          const int cellGroupGlobal);
  protected:
    const std::map<int,bool>& xfld_free_dofmap_; // map of which xnodes are going to be freed, need for later
    std::map<int,bool>& lgfld_free_dofmap_; // map of which lgfld dofs are going to be freed
    const std::vector<int>& boundaryTraceGroups_;
  };


  // class for marking the continuous field DOFs
  template <class PhysDim, class ArrayT, class ArrayQ >
  class MarkContinuousFieldDOFs : public GroupFunctorCellType<MarkContinuousFieldDOFs<PhysDim,ArrayT,ArrayQ>>
  {
  public:
    MarkContinuousFieldDOFs(std::vector<int>& continuousDOFVector, const std::vector<int> cellGroups  ) :
                            continuousDOFVector_(continuousDOFVector), cellGroups_(cellGroups) {}

    std::size_t nCellGroups() const          { return cellGroups_.size(); }
    std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

    //----------------------------------------------------------------------------//
    template< class Topology >
    void
    apply(const typename FieldTuple<Field<PhysDim, typename Topology::TopoDim, ArrayT>,
                                    Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
          template FieldCellGroupType<Topology>& fldsCell,
          const int cellGroupGlobal);

  protected:
    std::vector<int>& continuousDOFVector_; // vector of broken field dofs keyed by the continuous field dofs
    const std::vector<int> cellGroups_;
  };


  // class for marking the continuous field DOFs
  template <class PhysDim, class ArrayT, class ArrayQ >
  class LabelBrokenFieldDOFs : public GroupFunctorCellType<LabelBrokenFieldDOFs<PhysDim,ArrayT,ArrayQ>>
  {
  public:
    LabelBrokenFieldDOFs( const std::vector<int>& continuousDOFVector,
                          std::map<int,int>& brokenDOFMap,
                          const std::vector<int> cellGroups  ) :
                          continuousDOFVector_(continuousDOFVector), brokenDOFMap_(brokenDOFMap),
                          cellGroups_(cellGroups) {}

    std::size_t nCellGroups() const          { return cellGroups_.size(); }
    std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

    //----------------------------------------------------------------------------//
    template< class Topology >
    void
    apply(const typename FieldTuple<Field<PhysDim, typename Topology::TopoDim, ArrayT>,
                                    Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
          template FieldCellGroupType<Topology>& fldsCell,
          const int cellGroupGlobal);

  protected:
    const std::vector<int>& continuousDOFVector_; // vector of broken field dofs keyed by the continuous field dofs
    std::map<int,int>& brokenDOFMap_; // map from broken dof to broken dof (matching across any shared interface
    const std::vector<int> cellGroups_;
  };



  // class for mapping from DG dofs to CG dofs
  template <class PhysDim, class ArrayQ >
  class PairFieldDOFs : public GroupFunctorCellType<PairFieldDOFs<PhysDim,ArrayQ>>
  {
  public:
    PairFieldDOFs(std::map<int,int>& DG_to_CG_DOFMap,
                  const std::vector<int> cellGroups  ) :
                  DG_to_CG_DOFMap_(DG_to_CG_DOFMap),
                  cellGroups_(cellGroups) {}

    std::size_t nCellGroups() const          { return cellGroups_.size(); }
    std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

    //----------------------------------------------------------------------------//
    template< class Topology >
    void
    apply(const typename FieldTuple<Field<PhysDim, typename Topology::TopoDim, ArrayQ>,
                                    Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
          template FieldCellGroupType<Topology>& fldsCell,
          const int cellGroupGlobal)
    {
      // Cell Group Types
      typedef typename Field< PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

      const QFieldCellGroupType& dfldCell = get<0>( fldsCell );
      const QFieldCellGroupType& cfldCell = get<1>( fldsCell );

      std::vector<int> dfldCellGlobalMap( dfldCell.nBasis() ), cfldCellGlobalMap( cfldCell.nBasis() );
      SANS_ASSERT_MSG( dfldCell.nBasis()        == cfldCell.nBasis(),        "must use basis with same number of DOFs" );
      SANS_ASSERT_MSG( dfldCell.basisCategory() == cfldCell.basisCategory(), "must use the same basis function category" );

      for (int elem = 0; elem < cfldCell.nElem(); elem++ )
      {
        dfldCell.associativity(elem).getGlobalMapping( dfldCellGlobalMap.data(), dfldCellGlobalMap.size() );
        cfldCell.associativity(elem).getGlobalMapping( cfldCellGlobalMap.data(), cfldCellGlobalMap.size() );

        for (std::size_t i = 0; i < cfldCellGlobalMap.size(); i++)
          DG_to_CG_DOFMap_[dfldCellGlobalMap[i]] = cfldCellGlobalMap[i];
      }
    }

  protected:
    std::map<int,int>& DG_to_CG_DOFMap_; // map from broken dof to broken dof (matching across any shared interface
    const std::vector<int> cellGroups_;
  };


  // class for mapping from DG dofs to CG dofs -- not in the impl because it won't get instantiated otherwise
  template <class PhysDim, class ArrayQ >
  class ZeroField : public GroupFunctorCellType<ZeroField<PhysDim,ArrayQ>>
  {
  public:
    ZeroField(const std::vector<int> cellGroups  ) :
                  cellGroups_(cellGroups) {}

    std::size_t nCellGroups() const          { return cellGroups_.size(); }
    std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

    //----------------------------------------------------------------------------//
    template< class Topology >
    void
    apply(const typename Field<PhysDim, typename Topology::TopoDim, ArrayQ>::
          template FieldCellGroupType<Topology>& fldCell,
          const int cellGroupGlobal)
    {
      // Cell Group Types
      typedef typename Field< PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;
      typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

      QFieldCellGroupType& qfldCell = const_cast<QFieldCellGroupType&>( fldCell ); // cast away the constness

      ElementQFieldClass qfldElem( qfldCell.basis() );

      const int nDOF = qfldElem.nDOF();

      for (int elem = 0; elem < qfldCell.nElem(); elem++ )
      {
        qfldCell.getElement( qfldElem, elem );

        for (int dof = 0; dof < nDOF; dof++)
          qfldElem.DOF(dof) = 0;

        qfldCell.setElement( qfldElem, elem );
      }
    }

  protected:
    const std::vector<int> cellGroups_;
  };



} //namespace SANS

#endif //AlgebraicEquationSet_Project_H
