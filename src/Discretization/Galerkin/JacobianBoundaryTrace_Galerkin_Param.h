// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_GALERKIN_PARAM_H
#define JACOBIANBOUNDARYTRACE_GALERKIN_PARAM_H

// jacobian boundary-trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/Tuple/SurrealizedElementTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral without Lagrange multipliers
//

template<class Surreal, int iParam, class IntegrandBoundaryTrace, class MatrixQP_>
class JacobianBoundaryTrace_Galerkin_Param_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_Galerkin_Param_impl<Surreal, iParam, IntegrandBoundaryTrace, MatrixQP_> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;

  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;

  JacobianBoundaryTrace_Galerkin_Param_impl(const IntegrandBoundaryTrace& fcn,
                                            MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p ) :
    fcn_(fcn),
    mtxGlobalPDE_p_(mtxGlobalPDE_p) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class TupleFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename TupleFieldType                 ::template FieldCellGroupType<TopologyL>& tuplefldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XField<PhysDim, TopoDim>       ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename TupleFieldType                 ::template FieldCellGroupType<TopologyL> TupleFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename TupleType<iParam, TupleFieldCellGroupTypeL>::type ParamCellGroupTypeL;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeL, Surreal, iParam>::type ElementTupleFieldClassL;

    typedef typename ParamCellGroupTypeL ::template ElementType<Surreal> ElementParamFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementQFieldClassL;

    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef typename ElementParamFieldClassL::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP;

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass;

    // field groups
    const ParamCellGroupTypeL& paramfldCellL = get<iParam>(tuplefldCellL);

    // field elements
    ElementTupleFieldClassL  tuplefldElemL( tuplefldCellL.basis() );
    ElementQFieldClassL      qfldElemL( qfldCellL.basis() );
    ElementParamFieldClassL& paramfldElemL = set<iParam>(tuplefldElemL);

    ElementXFieldTraceClass  xfldElemTrace( xfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int paramDOFL = paramfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapQDOFGlobalL(nDOFL,-1);
    std::vector<int> mapParamDOFGlobalL(paramDOFL,-1);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal> integral(quadratureorder, nDOFL);

    // element integrand/residual
    std::vector<ArrayQSurreal> rsdPDEElemL(nDOFL);

    // element jacobian matrix
    MatrixElemClass mtxPDEElemLL(nDOFL, paramDOFL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobian to zero
      mtxPDEElemLL = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global parameter/solution DOFs to element
      tuplefldCellL.getElement( tuplefldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement(  xfldElemTrace, elem );

      // number of simultaneous derivatives per functor call
#if 0
      const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();
#else
      const int nDeriv = Surreal::N;
#endif

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nArrayP*paramDOFL; nchunk += nDeriv)
      {
        // associate derivative slots with solution variables

        int slot = 0;
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element

        // reset PDE residuals to zero
        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        integral(fcn_.integrand(xfldElemTrace, canonicalTraceL,
                                tuplefldElemL, qfldElemL),
                 xfldElemTrace,
                 rsdPDEElemL.data(), nDOFL );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElemL.DOF(j), n).deriv(slot - nchunk) = 0; // Reset the derivative

              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemLL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global

      scatterAdd( qfldCellL, paramfldCellL,
                  elemL,
                  mapQDOFGlobalL.data(), mapQDOFGlobalL.size(),
                  mapParamDOFGlobalL.data(), mapParamDOFGlobalL.size(),
                  mtxPDEElemLL,
                  mtxGlobalPDE_p_ );
    }
  }

protected:

//----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, class ParamFieldCellGroupType, class MatrixQP,
            class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfldCellL,
      const ParamFieldCellGroupType& paramfldCellL,
      const int elemL,
      int mapQDOFGlobalL[], const int nDOFL,
      int mapParamDOFGlobalL[], const int paramDOFL,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemLL,
      SparseMatrixType& mtxGlobalPDE_p )
  {
    qfldCellL.associativity( elemL ).getGlobalMapping( mapQDOFGlobalL, nDOFL );

    paramfldCellL.associativity( elemL ).getGlobalMapping( mapParamDOFGlobalL, paramDOFL );

    mtxGlobalPDE_p.scatterAdd( mtxPDEElemLL, mapQDOFGlobalL, nDOFL, mapParamDOFGlobalL, paramDOFL );
  }

#if 0
//----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class TopoDim, class MatrixQP,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int elemL,
      int mapDOFGlobalL[], const int nDOFL,
      DLA::MatrixD<MatrixQP>& mtxPDEElemLL,
      SparseMatrixType< DLA::MatrixD<MatrixQP> >& mtxGlobalPDE_p )
  {
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemLL, elemL, elemL );
  }
#endif

protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
};

// Factory function

template<class Surreal, int iParam, class IntegrandBoundaryTrace, class MatrixQP>
JacobianBoundaryTrace_Galerkin_Param_impl<Surreal, iParam, IntegrandBoundaryTrace, MatrixQP>
JacobianBoundaryTrace_Galerkin_Param( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                      MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p )
{
  return { fcn.cast(), mtxGlobalPDE_p };
}


}

#endif  // JACOBIANBOUNDARYTRACE_GALERKIN_PARAM_H
