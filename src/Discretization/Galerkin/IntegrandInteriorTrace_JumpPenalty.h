// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDINTERIORTRACE_JUMPPENALTY_H
#define INTEGRANDINTERIORTRACE_JUMPPENALTY_H

// interior trace integrand operator: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

// #include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// One-sided interior trace SIP integrand: Galerkin local solves
//
// integrandL = + [[phiL]].{N(u_L - u_R)}  (assuming u_R is fixed, u_L is allowed to vary)
//        N ~ hL is a source style nitsche parameter
//
// where
//   phi              basis function

template <class PhysDim_>
class IntegrandInteriorTrace_JumpPenalty
: public IntegrandInteriorTraceType<IntegrandInteriorTrace_JumpPenalty<PhysDim_>>
{
public:

  template <class Z = Real >
  using ArrayQ = Z;

  template <class Z = Real>
  using VectorArrayQ = Real;

  template <class Z = Real >
  using MatrixQ = Z;

  typedef PhysDim_ PhysDim;

  // cppcheck-suppress noExplicitConstructor
  IntegrandInteriorTrace_JumpPenalty( const std::vector<int>& InteriorTraceGroups ) :
    interiorTraceGroups_(InteriorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  // Should be possible to remove all these TopoDim. The topology classes have all the details
  template< class TopologyTrace, class TopologyL,     class TopologyR,
                                 class ElementParamL, class ElementParamR>
  class BasisWeighted
  {
  public:
    typedef typename TopologyTrace::TopoDim TopoDimTrace;
    typedef typename TopologyL::TopoDim TopoDimCell;

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef typename ElementXFieldTraceType::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldL& qfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldR& qfldElemR ) :
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), paramfldElemL_( paramfldElemL ),
      xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), paramfldElemR_( paramfldElemR ),
      nDOFL_(qfldElemL_.nDOF()),
      phiL_(nDOFL_),
      gradphiL_(nDOFL_),
      // Cb_(1.0/(pow(qfldElemL.order(),2)))
      Cb_(1)
      {}

    ~BasisWeighted() { }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; };

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }

    // trace element residual integrand
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<> integrandL[], const int neqnL ) const;

    // trace element residual integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace, DLA::VectorD<ArrayQ<>>& rsdPDEElemL ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace, DLA::MatrixD<MatrixQ<>>& mtxElemL ) const;

  protected:
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    mutable std::vector<Real> phiL_;
    mutable std::vector<VectorX> gradphiL_;
    const Real Cb_;
  };

  //Factory function that returns the basis weighted integrand class
  template< class TopologyTrace, class TopologyL,     class TopologyR,
                                 class ElementParamL, class ElementParamR >
  BasisWeighted<TopologyTrace, TopologyL,      TopologyR,
                               ElementParamL, ElementParamR>
  integrand(const ElementXField<PhysDim, typename TopologyTrace::TopoDim, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<>, typename TopologyL::TopoDim, TopologyL>& qfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<>, typename TopologyR::TopoDim, TopologyR>& qfldElemR) const
  {
    return BasisWeighted<TopologyTrace, TopologyL, TopologyR,
                                        ElementParamL, ElementParamR>(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                                                      paramfldElemL, qfldElemL,
                                                                      paramfldElemR, qfldElemR);
  }

  // Should be possible to remove all these TopoDim. The topology classes have all the details
  template< class TopologyTrace, class TopologyL,     class TopologyR,
                                 class ElementParamL, class ElementParamR>
  class Estimate
  {
  public:
    typedef typename TopologyTrace::TopoDim TopoDimTrace;
    typedef typename TopologyL::TopoDim TopoDimCell;

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef Element<Real, TopoDimCell, TopologyL> ElementEFieldL;
    typedef Element<Real, TopoDimCell, TopologyR> ElementEFieldR;

    typedef typename ElementXFieldTraceType::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Estimate( const ElementXFieldTraceType& xfldElemTrace,
                 const CanonicalTraceToCell& canonicalTraceL,
                 const CanonicalTraceToCell& canonicalTraceR,
                 const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                 const ElementQFieldL& qfldElemL,
                 const ElementEFieldL& efldElemL,
                 const ElementParamR& paramfldElemR,     // XField must be the last parameter of tuple
                 const ElementQFieldR& qfldElemR,
                 const ElementEFieldR& efldElemR ) :
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
      xfldElemL_(get<-1>(paramfldElemL)),
      qfldElemL_(qfldElemL), paramfldElemL_( paramfldElemL ), efldElemL_(efldElemL),
      xfldElemR_(get<-1>(paramfldElemR)),
      qfldElemR_(qfldElemR), paramfldElemR_( paramfldElemR ), efldElemR_(efldElemR),
      nPhi_( efldElemL_.nDOF() ),
      ephi_(nPhi_),
      // Cb_(1.0/(pow(qfldElemL.order(),2)))
      Cb_(1)
      {}

    ~Estimate() { }

    // total DOFs
    int nDOF() const { return qfldElemL_.nDOF(); }
    int nPhi() const { return efldElemL_.nDOF(); }

    // trace element residual integrand
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<> integrandL[], const int nphi ) const;

  protected:
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;

    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementEFieldL& efldElemL_;

    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementParamR& paramfldElemR_;
    const ElementEFieldR& efldElemR_;

    const int nPhi_;
    mutable std::vector<Real> ephi_;
    const Real Cb_;
  };


  //Factory function that returns the basis weighted integrand class
  template< class TopologyTrace, class TopologyL,     class TopologyR,
                                 class ElementParamL, class ElementParamR >
  Estimate<TopologyTrace, TopologyL,     TopologyR,
                          ElementParamL, ElementParamR>
  integrand(const ElementXField<PhysDim, typename TopologyTrace::TopoDim, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<>, typename TopologyL::TopoDim, TopologyL>& qfldElemL,
            const Element<Real, typename TopologyL::TopoDim, TopologyL>& efldElemL,
            const ElementParamR& paramfldElemR,     // XField must be the last parameter
            const Element<ArrayQ<>, typename TopologyR::TopoDim, TopologyR>& qfldElemR,
            const Element<Real, typename TopologyR::TopoDim, TopologyR>& efldElemR) const
  {
    return Estimate<TopologyTrace, TopologyL, TopologyR,
                                   ElementParamL, ElementParamR>( xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                                                  paramfldElemL, qfldElemL, efldElemL,
                                                                  paramfldElemR, qfldElemR, efldElemR );
  }


protected:
  const std::vector<int> interiorTraceGroups_;
};

template <class PhysDim_>
template<class TopologyTrace, class TopologyL,     class TopologyR,
                              class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_JumpPenalty<PhysDim_>::
BasisWeighted<TopologyTrace, TopologyL,     TopologyR,
                             ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<> integrandL[], const int neqnL ) const
{
  SANS_ASSERT(neqnL == nDOFL_);

  ArrayQ<> qL, qR;                // solutions

  QuadPointCellType sRefL, sRefR; // reference-element coordinates (s,t)

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // basis value
  qfldElemL_.evalBasis( sRefL, phiL_.data(), phiL_.size() );
  // xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_.data(), gradphiL_.size() );
  qfldElemL_.evalFromBasis( phiL_.data(), phiL_.size(), qL );

  qfldElemR_.eval( sRefR, qR );

  Real hL = xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  // Real hR = xfldElemR_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  // Real h = max(hL,hR);
  // Real hL = 1.0;

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = Cb_*hL*phiL_[k]*(qL - qR);

  // // unit normal: points to R
  // VectorX nL; // unit normal for left element (points to right element)
  // traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);
  //
  // for (int k = 0; k < neqnL; k++)
  //   integrandL[k] -= pow(Cb_*hL,2.0)*dot(nL,gradphiL_[k])*(qL - qR);

}

template <class PhysDim_>
template<class TopologyTrace, class TopologyL,     class TopologyR,
                              class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_JumpPenalty<PhysDim_>::
BasisWeighted<TopologyTrace, TopologyL,     TopologyR,
                             ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace, DLA::VectorD<ArrayQ<>>& rsdPDEElemL ) const
{
  SANS_ASSERT(rsdPDEElemL.m() == nDOFL_);

  ArrayQ<> qL, qR;                // solutions

  QuadPointCellType sRefL, sRefR; // reference-element coordinates (s,t)

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // basis value
  qfldElemL_.evalBasis( sRefL, phiL_.data(), phiL_.size() );
  // xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_.data(), gradphiL_.size() );
  qfldElemL_.evalFromBasis( phiL_.data(), phiL_.size(), qL );

  qfldElemR_.eval( sRefR, qR );

  Real hL = xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();

  for (int k = 0; k < nDOFL_; k++)
    rsdPDEElemL[k] += dJ*Cb_*hL*phiL_[k]*(qL - qR);

  // // unit normal: points to R
  // VectorX nL; // unit normal for left element (points to right element)
  // traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);
  //
  // for (int k = 0; k < nDOFL_; k++)
  //   rsdPDEElemL[k] -= dJ*pow(Cb_*hL,2.0)*dot(nL,gradphiL_[k])*(qL - qR);
}

template <class PhysDim_>
template<class TopologyTrace, class TopologyL,     class TopologyR,
                              class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_JumpPenalty<PhysDim_>::
BasisWeighted<TopologyTrace, TopologyL,     TopologyR,
                             ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace, DLA::MatrixD<MatrixQ<>>& mtxElemL ) const
{
  SANS_ASSERT(mtxElemL.m() == nDOFL_);
  SANS_ASSERT(mtxElemL.n() == nDOFL_);

  QuadPointCellType sRefL, sRefR; // reference-element coordinates (s,t)

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // basis value
  qfldElemL_.evalBasis( sRefL, phiL_.data(), phiL_.size() );
  // xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_.data(), gradphiL_.size() );

  MatrixQ<> I = DLA::Identity();
  MatrixQ<> proj_q; // temporary storage

  Real hL = xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  // Real hR = xfldElemR_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  // Real h = max(hL,hR);

  // // unit normal: points to R
  // VectorX nL; // unit normal for left element (points to right element)
  // traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  for (int i = 0; i < nDOFL_; i++)
  {
    proj_q = dJ*Cb_*hL*phiL_[i]*I;
    // proj_q = dJ*(hL*phiL_[i] - pow(Cb_*hL,2.0)*dot(nL,gradphiL_[i]) )*I;
    for (int j = 0; j < nDOFL_; j++)
      mtxElemL(i,j) += proj_q*phiL_[j];
  }
}


template <class PhysDim_>
template<class TopologyTrace, class TopologyL,     class TopologyR,
                              class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_JumpPenalty<PhysDim_>::
Estimate<TopologyTrace, TopologyL,     TopologyR,
                             ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<> integrandL[], const int nphi ) const
{
  SANS_ASSERT(nphi == nPhi_);

  ArrayQ<> qL, qR;                // solutions

  QuadPointCellType sRefL, sRefR; // reference-element coordinates (s,t)

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // basis value
  efldElemL_.evalBasis( sRefL, ephi_.data(), nPhi_ );

  qfldElemL_.eval( sRefL, qL );
  qfldElemR_.eval( sRefR, qR );

  // VectorX X = 0;
  // xfldElemTrace_.eval(sRefTrace, X);

  // HACK
  // Real r = norm(X,2.0);
  // Real theta = atan2(X(1),X(0));
  // Real theta0 = PI/2.0;
  // Real alpha = 2.0/3.0;
  // Real q = pow(r,alpha)*sin(alpha*(theta + theta0));

  // HACK


  Real hL = xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  // Real hR = xfldElemR_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  // Real h = max(hL,hR);

  // The true adjoint weighting is (u-u_h) -> need the true function evaluated here!
  for (int k = 0; k < nphi; k++)
    integrandL[k] = Cb_*hL*ephi_[k]*(qL-qR)*(qL - qR);
    // integrandL[k] = 0.0*hL;


}



} // namespace SANS
#endif //INTEGRANDINTERIORTRACE_JUMPPENALTY_H
