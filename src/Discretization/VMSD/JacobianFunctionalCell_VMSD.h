// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALCELL_VMSD_H
#define JACOBIANFUNCTIONALCELL_VMSD_H

// Cell integral functional jacobian functions for Galerkin

#include "tools/Tuple.h"

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/tools/Surrealize.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group functional jacobian
//

template<class IntegrandCell, template<class> class Vector>
class JacobianFunctionalCell_VMSD_impl :
    public GroupIntegralCellType< JacobianFunctionalCell_VMSD_impl<IntegrandCell, Vector> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;

  typedef typename IntegrandCell::template ArrayJ<Real> ArrayJ;
  typedef typename IntegrandCell::template MatrixJ<Real> MatrixJ;

  typedef DLA::VectorD<MatrixJ> MatrixElemClass;

  // Save off the cell integrand and the residual vector
  JacobianFunctionalCell_VMSD_impl( const IntegrandCell& fcn,
                                        Vector<MatrixJ>& jacFunctional_qp,
                                        Vector<MatrixJ>& jacFunctional_q) :
                                        fcn_(fcn),
                                        jacFunctional_qp_(jacFunctional_qp),
                                        jacFunctional_q_(jacFunctional_q) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
                               Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);

    SANS_ASSERT_MSG(qfld.nDOFpossessed() == jacFunctional_q_.m(),
                    "qfld.nDOFpossessed() = %d, jacFunctional_q_.m() = %d", qfld.nDOFpossessed(), jacFunctional_q_.m());

    SANS_ASSERT_MSG(qpfld.nDOFpossessed() + qpfld.nDOFghost() == jacFunctional_qp_.m(),
                    "qpfld.nDOFpossessed() + qpfld.nDOFghost() = %d, jacFunctional_qp_.m() = %d", qpfld.nDOFpossessed(), jacFunctional_qp_.m());
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field    < PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qpfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );

    // DOFs per element
    const int nDOF = qfldElem.nDOF();
    const int nDOFp = qpfldElem.nDOF();

    // element functional jacobian vector
    DLA::VectorD<MatrixJ> jacFunctionalElem(nDOF);
    DLA::VectorD<MatrixJ> jacFunctionalElemp(nDOFp);

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal(nDOF, -1);
    std::vector<int> mapDOFLocal( nDOF, -1 );
    std::vector<int> maprsd( nDOF, -1 );
    int nDOFLocal = 0;

    std::vector<int> mapDOFpGlobal(nDOFp, -1);

    const int nDOFpossessed = jacFunctional_q_.m();
    const int nDOFppossessed = jacFunctional_qp_.m();

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, MatrixElemClass, MatrixElemClass> integral(quadratureorder);

    // The integrand to be integrated
    auto integrand = fcn_.integrand( xfldElem, qfldElem, qpfldElem );

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == qpfldCell.nElem() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qpfldCell.getElement( qpfldElem, elem );

      // Get the local-to-global mapping to scatter the jacobian
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nDOF );
      qpfldCell.associativity( elem ).getGlobalMapping( mapDOFpGlobal.data(), nDOFp );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOF; n++)
        if (mapDOFGlobal[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
          nDOFLocal++;
        }

      if (nDOFLocal == 0 && mapDOFpGlobal[0] >= nDOFppossessed ) continue;

      jacFunctionalElem = 0; jacFunctionalElemp = 0;

      integral( integrand, get<-1>(xfldElem), jacFunctionalElem, jacFunctionalElemp);

      // HACK TO SET BUBBLE RESIDUALS TO ZERO; REMOVES OVERLAP IN BASIS
      const int nBubble = qfldCell.associativity( elem ).nBubble();
      if (nBubble > 0 &&  (qpfldElem.order() > PhysDim::D) )
      {
        SANS_ASSERT( qpfldElem.basis()->category() == BasisFunctionCategory_Lagrange );
        for (int ind=(nDOFp - nBubble); ind<nDOFp; ind++) //CELL DOFS WILL BE LAST?
        {
          jacFunctionalElemp[ind] = 0;
        }
      }

      // scatter-add element jacobian to global
      for (int n = 0; n < nDOFLocal; n++)
        jacFunctional_q_[ mapDOFLocal[n] ] += jacFunctionalElem[ maprsd[n] ];

      if ( mapDOFpGlobal[0] < nDOFppossessed )
        for (int n = 0; n< nDOFp; n++)
          jacFunctional_qp_[ mapDOFpGlobal[n] ] += jacFunctionalElemp[n];

    }
  }

protected:
  const IntegrandCell& fcn_;
  Vector<MatrixJ>& jacFunctional_qp_;
  Vector<MatrixJ>& jacFunctional_q_;
};

// Factory function

template<class IntegrandCell, template<class> class Vector, class MatrixJ>
JacobianFunctionalCell_VMSD_impl<IntegrandCell, Vector>
JacobianFunctionalCell_VMSD( const IntegrandCellType<IntegrandCell>& fcn,
                                 Vector< MatrixJ >& jacFunctional_qp,
                                 Vector< MatrixJ >& jacFunctional_q )
{
  typedef typename Scalar<MatrixJ>::type T;
  static_assert( std::is_same<typename IntegrandCell::template MatrixJ<T>, MatrixJ>::value, "These should be the same");
  return JacobianFunctionalCell_VMSD_impl<IntegrandCell, Vector>(fcn.cast(), jacFunctional_qp, jacFunctional_q);
}

} // namespace SANS

#endif  // JACOBIANFUNCTIONALCELL_VMSD_H
