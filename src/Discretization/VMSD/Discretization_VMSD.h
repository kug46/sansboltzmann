// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_VMSD_DISCRETIZATION_VMSD_H_
#define SRC_DISCRETIZATION_VMSD_DISCRETIZATION_VMSD_H_

// #define EXPTTAU

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Discretization/DiscretizationObject.h"

#include "BasisFunction/BasisFunctionCategory.h"
#include "Discretization/Galerkin/Stabilization_Nitsche.h"

namespace SANS
{


//----------------------------------------------------------------------------//
// applies stabilized CG tau matrix to strong PDE residual
// strongPDE = cb*tau*strongPDE
// ArrayQ = MatrixQ*ArrayQ
// cb constant scaling, default to 1


enum VMSDType
{
  VMSDp = 0,
  VMSDpminus1 = 1,
  VMSDfixedp = 2,
  VMSD0 = 3,
};

class DiscretizationVMSD : public StabilizationNitsche
{
public:

  typedef StabilizationNitsche BaseType;

  // order is used to make sure the adjoint doesn't get calculated with a higher Nitsche parameter
  DiscretizationVMSD(const VMSDType vmsdType = VMSDp, const bool isStaticCondensed = true,
                     const int orderB = 1, const Real cbB = 1.0, const int fixedpOrder = -1 ) :
    BaseType(orderB, cbB),
    vmsdType_(vmsdType),
    isStaticCondensed_(isStaticCondensed),
    fixedpOrder_(fixedpOrder)
  {}

  bool isStaticCondensed() const { return isStaticCondensed_; }

  VMSDType getVMSDType() const { return vmsdType_; }

  int pOrder( const int order) const
  {
    int p = order;

    if (vmsdType_ == VMSD0)
    {
      p=0;
    }
    else if (vmsdType_ == VMSDpminus1)
    {
      p = order - 1;
    }
    else if (vmsdType_ == VMSDfixedp)
    {
      SANS_ASSERT(fixedpOrder_ >= 0);
      p = fixedpOrder_;
    }

    return p;
  }


protected:
  const VMSDType vmsdType_;
  const bool isStaticCondensed_;
  const int fixedpOrder_;

};

}

#endif /* SRC_DISCRETIZATION_VMSD_DISCRETIZATION_VMSD_H_ */
