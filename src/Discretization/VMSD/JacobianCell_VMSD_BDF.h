// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_VMSD_BDF_H
#define JACOBIANCELL_VMSD_BDF_H

// HDG cell integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "JacobianCell_VMSD_Element.h"

#include "Field/Field.h"
#include "Field/FieldSequence.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/FieldData/FieldDataMatrixD_Cell.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral jacobian
//

template<class IntegrandCell>
class JacobianCell_VMSD_BDF_impl :
    public GroupIntegralCellType< JacobianCell_VMSD_BDF_impl<IntegrandCell> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template MatrixQ<Real> MatrixQ;

  typedef JacobianElemCell_VMSD<PhysDim,MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianCell_VMSD_BDF_impl( const IntegrandCell& fcn,
                                    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                    FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_q,
                                    FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_qp ) :
    fcn_(fcn), mtxGlobalPDE_q_(mtxGlobalPDE_q), jac_pdep_q_(jac_pdep_q), jac_pdep_qp_(jac_pdep_qp) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                       FieldSequence<PhysDim, TopoDim, ArrayQ>,
                                       Field<PhysDim,TopoDim,ArrayQ>,
                                       FieldSequence<PhysDim, TopoDim, ArrayQ>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld               = get<0>(flds);
    const FieldSequence< PhysDim, TopoDim, ArrayQ >& qfldPast = get<1>(flds);

    const int qm = qfld.nDOFpossessed();
    const int qn = qfld.nDOFpossessed() + qfld.nDOFghost();

    const int qpastm = qfldPast.nDOFpossessed();
    const int qpastn = qfldPast.nDOFpossessed() + qfldPast.nDOFghost();

    SANS_ASSERT_MSG(qfld.nDOFpossessed()  == qfldPast.nDOFpossessed()                       , "%d != %d", qm, qpastm);
    SANS_ASSERT_MSG(qfld.nDOFpossessed() + qfld.nDOFghost() == qfldPast.nDOFpossessed() + qfldPast.nDOFghost(),
                     "%d != %d",qn, qpastn);
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class ParamFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename ParamFieldType::template FieldCellGroupType<Topology>& pfldCell,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                               FieldSequence<PhysDim, TopoDim, ArrayQ>,
                                                               Field<PhysDim,TopoDim,ArrayQ>,
                                                               FieldSequence<PhysDim, TopoDim, ArrayQ>>::type::
                                                               template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {

    typedef typename ParamFieldType                           ::template FieldCellGroupType<Topology> PFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ         >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldSequence< PhysDim, TopoDim, ArrayQ >::template FieldCellGroupType<Topology> QFieldSeqCellGroupType;
    typedef typename XField< PhysDim, TopoDim                >::template FieldCellGroupType<Topology> XFieldCellGroupType;

    typedef typename PFieldCellGroupType::template ElementType<> ElementPFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename QFieldSeqCellGroupType::template ElementType<> ElementQFieldSeqClass;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    const QFieldCellGroupType&    qfldCell     = get<0>(fldsCell);
    const QFieldSeqCellGroupType& qfldCellPast = get<1>(fldsCell);
    const QFieldCellGroupType&    qpfldCell     = get<2>(fldsCell);
    const QFieldSeqCellGroupType& qpfldCellPast = get<3>(fldsCell);

    // element field variables
    ElementPFieldClass pfldElem( pfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldSeqClass qfldElemPast( qfldCellPast.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );
    ElementQFieldSeqClass qpfldElemPast( qpfldCellPast.basis() );

    // DOFs per element
    const int nDOF = qfldElem.nDOF();
    const int nDOFp = qpfldElem.nDOF();

    // element-to-global DOF mapping
    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nDOF, -1 );
    std::vector<int> mapDOFGlobal_qp( nDOF, -1 );
    std::vector<int> mapDOFLocal( nDOF, -1 );
    std::vector<int> maprsd( nDOF, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = mtxGlobalPDE_q_.m();

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, MatrixElemClass >
      integral(quadratureorder);

    // element jacobian matrices
    MatrixElemClass mtxElem(nDOF, nDOFp);
    MatrixElemClass mtxElemLocal(nDOF, nDOFp);

    // loop over elements within group
    const int nelem = pfldCell.nElem();

    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), mapDOFGlobal.size() );
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal_qp.data(), mapDOFGlobal_qp.size() );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOF; n++)
        if (mapDOFGlobal[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
          nDOFLocal++;
        }


      // zero element Jacobians
      mtxElem = 0;
      mtxElemLocal = 0;

      // copy global grid/solution DOFs to element
      pfldCell.getElement( pfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qfldCellPast.getElement( qfldElemPast, elem );
      qpfldCell.getElement( qpfldElem, elem );
      qpfldCellPast.getElement( qpfldElemPast, elem );

      // cell integration for canonical element
      integral( fcn_.integrand( pfldElem, qfldElem, qfldElemPast, qpfldElem, qpfldElemPast ),
                get<ElementXFieldClass>(pfldElem), mtxElem );

      jac_pdep_q_.getCellGroupGlobal(cellGroupGlobal)[elem] = mtxElem.PDEp_q;
      jac_pdep_qp_.getCellGroupGlobal(cellGroupGlobal)[elem] = mtxElem.PDEp_qp;

      // no CG residuals are possessed by this processor:
      if (nDOFLocal == 0) continue;

      // scatter-add element jacobian to global
      if (nDOFLocal == nDOF)
        scatterAdd( elem, mapDOFGlobal, nDOFLocal, mapDOFGlobal, mtxElem.PDE_q, mtxGlobalPDE_q_ );
      else
      {
        // compress in only the DOFs possessed by this processor
        for (int i = 0; i < nDOFLocal; i++)
          for (int j = 0; j < nDOF; j++)
            mtxElemLocal.PDE_q(i,j) = mtxElem.PDE_q(maprsd[i],j);

        scatterAdd( elem, mapDOFLocal, nDOFLocal, mapDOFGlobal, mtxElemLocal.PDE_q, mtxGlobalPDE_q_ );
      }
    } // elem
  }

  //----------------------------------------------------------------------------//
    template <template <class> class SparseMatrixType>
    void
    scatterAdd(
        const int elem,
        std::vector<int>& mapDOFLocal,
        const int nDOFLocal,
        std::vector<int>& mapDOFGlobal,
        const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDE_q,
        SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
    {
      // jacobian wrt q
      mtxGlobalPDE_q.scatterAdd( mtxElemPDE_q, mapDOFLocal.data(), nDOFLocal, mapDOFGlobal.data(), mapDOFGlobal.size() );
    }

protected:
  const IntegrandCell& fcn_;

  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_q_;
  FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_qp_;
};



// Factory function

template<class IntegrandCell, class MatrixQ>
JacobianCell_VMSD_BDF_impl<IntegrandCell>
JacobianCell_VMSD_BDF( const IntegrandCellType<IntegrandCell>& fcn,
                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                             FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_q,
                             FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_qp)
{
  return {fcn.cast(), mtxGlobalPDE_q, jac_pdep_q, jac_pdep_qp};
}

}


#endif  // JACOBIANCELL_VMSD_BDF_H
