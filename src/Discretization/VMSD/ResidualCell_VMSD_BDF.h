// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALCELL_VMSD_BDF_H
#define RESIDUALCELL_VMSD_BDF_H

// Cell BDF integral residual functions

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/FieldSequence.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  BDF cell group integral
//

template<class IntegrandCell, template<class> class Vector>
class ResidualCell_VMSD_BDF_impl :
    public GroupIntegralCellType< ResidualCell_VMSD_BDF_impl<IntegrandCell, Vector> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualCell_VMSD_BDF_impl( const IntegrandCell& fcn,
                                  Vector<ArrayQ>& rsdPDEGlobal,
                                  Vector<ArrayQ>& rsdPDEpGlobal ) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), rsdPDEpGlobal_(rsdPDEpGlobal) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                          FieldSequence<PhysDim, TopoDim, ArrayQ>,
                                          Field<PhysDim,TopoDim,ArrayQ>,
                                          FieldSequence<PhysDim, TopoDim, ArrayQ>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld               = get<0>(flds);
    const FieldSequence< PhysDim, TopoDim, ArrayQ >& qfldPast = get<1>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qpfld               = get<2>(flds);
    const FieldSequence< PhysDim, TopoDim, ArrayQ >& qpfldPast = get<3>(flds);

    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( rsdPDEGlobal_.m() == qfldPast.nDOFpossessed() );

    SANS_ASSERT( rsdPDEpGlobal_.m() == qpfld.nDOFpossessed()
                 ||  rsdPDEpGlobal_.m() == qpfld.nDOFpossessed()  +  qpfld.nDOFghost() );
    SANS_ASSERT( rsdPDEpGlobal_.m() == qpfldPast.nDOFpossessed()
                 ||  rsdPDEpGlobal_.m() == qpfldPast.nDOFpossessed()  +  qpfldPast.nDOFghost() );
  }


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class ParamFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename ParamFieldType::template FieldCellGroupType<Topology>& pfldCell,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,
                                                  FieldSequence<PhysDim, TopoDim, ArrayQ>,
                                                  Field<PhysDim,TopoDim,ArrayQ>,
                                                  FieldSequence<PhysDim, TopoDim, ArrayQ>>::type::
                                                  template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename ParamFieldType                           ::template FieldCellGroupType<Topology> PFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ         >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldSequence< PhysDim, TopoDim, ArrayQ >::template FieldCellGroupType<Topology> QFieldSeqCellGroupType;
    typedef typename XField< PhysDim, TopoDim                >::template FieldCellGroupType<Topology> XFieldCellGroupType;

    typedef typename PFieldCellGroupType::template ElementType<> ElementPFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename QFieldSeqCellGroupType::template ElementType<> ElementQFieldSeqClass;
    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    const QFieldCellGroupType&    qfldCell     = get<0>(fldsCell);
    const QFieldSeqCellGroupType& qfldCellPast = get<1>(fldsCell);
    const QFieldCellGroupType&    qpfldCell     = get<2>(fldsCell);
    const QFieldSeqCellGroupType& qpfldCellPast = get<3>(fldsCell);

    SANS_ASSERT( pfldCell.nElem() == qfldCell.nElem() );
    // element field variables
    ElementPFieldClass pfldElem( pfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldSeqClass qfldElemPast( qfldCellPast.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );
    ElementQFieldSeqClass qpfldElemPast( qpfldCellPast.basis() );

    // number of integrals evaluated
    const int nDOF = qfldElem.nDOF();
    const int nIntegrand = nDOF;

    const int nDOFp = qpfldElem.nDOF();
    const int nIntegrandp = nDOFp;

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, ArrayQ, ArrayQ > integral(quadratureorder, nIntegrand, nIntegrandp);

    // just to make sure things are consistent
    SANS_ASSERT( pfldCell.nElem() == qfldCell.nElem() );

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nIntegrand, -1 );
    std::vector<int> mapDOFGlobal_qp( nIntegrandp, -1 );
//    std::vector<int> mapDOFGlobalPast( nIntegrand, -1 );
    std::vector<int> mapDOFLocal( nIntegrand, -1 );
    std::vector<int> maprsd( nIntegrand, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = rsdPDEGlobal_.m();
    const int nDOFppossessed = rsdPDEpGlobal_.m();

    // residual array
    std::vector< ArrayQ > rsdElem( nIntegrand );
    std::vector< ArrayQ > rsdElemp( nIntegrandp );

    // loop over elements within group
    const int nelem = pfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the mapping for the DOFs
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nIntegrand );
      qpfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal_qp.data(), nIntegrandp );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nIntegrand; n++)
        if (mapDOFGlobal[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
          nDOFLocal++;
        }

      // no need if nothing is possessed
      if (nDOFLocal == 0 && mapDOFGlobal_qp[0] >= nDOFppossessed ) continue;

      // copy global grid/solution DOFs to element
      pfldCell.getElement( pfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qfldCellPast.getElement( qfldElemPast, elem );
      qpfldCell.getElement( qpfldElem, elem );
      qpfldCellPast.getElement( qpfldElemPast, elem );

      for (int n = 0; n < nIntegrand; n++)
        rsdElem[n] = 0;

      for (int n = 0; n < nIntegrandp; n++)
        rsdElemp[n] = 0;

      // cell integration for canonical element
      integral( fcn_.integrand(pfldElem, qfldElem, qfldElemPast, qpfldElem, qpfldElemPast),
                get<ElementXFieldClass>(pfldElem), rsdElem.data(), nIntegrand, rsdElemp.data(), nIntegrandp  );

      // scatter-add element integral to global
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nDOF );

      // HACK TO SET BUBBLE RESIDUALS TO ZERO; REMOVES OVERLAP IN BASIS
      const int nBubble = qfldCell.associativity( elem ).nBubble();
      if (nBubble > 0 &&  (qpfldElem.order() > PhysDim::D) )
      {
        SANS_ASSERT( qpfldElem.basis()->category() == BasisFunctionCategory_Lagrange );
        for (int ind=(nDOFp - nBubble); ind<nDOFp; ind++) //CELL DOFS WILL BE LAST?
        {
          rsdElemp[ind] = 0;
        }
      }

      // scatter-add element integral to global
      for (int n = 0; n < nDOFLocal; n++)
        rsdPDEGlobal_[ mapDOFLocal[n] ] += rsdElem[ maprsd[n] ];

      if ( mapDOFGlobal_qp[0] < nDOFppossessed )
      {
        for (int n = 0; n < nDOFp; n++)
          rsdPDEpGlobal_[mapDOFGlobal_qp[n]] += rsdElemp[n];

      }

    }
  }

protected:
  const IntegrandCell& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdPDEpGlobal_;
};

// Factory function

template<class IntegrandCell, class ArrayQ, template<class> class Vector>
ResidualCell_VMSD_BDF_impl<IntegrandCell, Vector>
ResidualCell_VMSD_BDF( const IntegrandCellType<IntegrandCell>& fcn,
                           Vector<ArrayQ>& rsdPDEGlobal,
                           Vector<ArrayQ>& rsdPDEpGlobal)
{
  static_assert( std::is_same< ArrayQ, typename IntegrandCell::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualCell_VMSD_BDF_impl<IntegrandCell, Vector>(fcn.cast(), rsdPDEGlobal, rsdPDEpGlobal);
}

}

#endif  // RESIDUALCELL_GALERKIN_BDF_H
