// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_VMSD_H
#define ALGEBRAICEQUATIONSET_VMSD_H

#include <type_traits>

#include "FieldBundle_VMSD.h"
#include "IntegrandCell_VMSD.h"
#include "IntegrandCell_VMSD_NL.h"
#include "IntegrandTrace_VMSD.h"
#include "IntegrandTrace_VMSD_NL.h"
#include "JacobianCell_VMSD.h"
#include "JacobianCell_VMSD_SC.h"
#include "JacobianCell_VMSD_SC2.h"
#include "ResidualCell_VMSD.h"
#include "SolutionData_VMSD.h"
#include "CompleteUpdate_VMSD.h"
#include "Discretization_VMSD.h"
#include "ResidualBoundaryTrace_Dispatch_VMSD.h"
#include "JacobianBoundaryTrace_Dispatch_VMSD.h"
#include "IntegrateBoundaryTrace_Dispatch_VMSD.h"
#include "ComputePerimeter.h"

#include "IntegrandBoundaryTrace_Flux_mitState_VMSD.h"
#include "IntegrandBoundaryTrace_Dirichlet_mitLG_VMSD.h"
#include "IntegrandBoundaryTrace_Dirichlet_sansLG_VMSD.h"
#include "IntegrandBoundaryTrace_LinearScalar_sansLG_VMSD.h"
#include "IntegrandBoundaryTrace_LinearScalar_mitLG_VMSD.h"
#include "IntegrandBoundaryTrace_None_VMSD.h"
#include "tools/SANSnumerics.h"
#include "tools/KahanSum.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/output_Tecplot.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/QuadratureOrder.h"
#include "Discretization/ResidualNormType.h"

#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/isValidState/isValidStateCell_VMSD.h"
#include "Discretization/isValidState/isValidStateInteriorTrace_VMSD.h"
#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch.h"

//#include "ErrorEstimate/Galerkin/ErrorEstimate_StrongForm_Galerkin.h"
#include "ErrorEstimate/ErrorEstimate_fwd.h"
//#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>
#include "tools/plus_std_vector.h"
#endif

#define VMSDNL

namespace SANS
{

template<> struct DiscBCTag<BCCategory::Dirichlet_mitLG, VMSD>     { typedef VMSD type; };
template<> struct DiscBCTag<BCCategory::Dirichlet_sansLG, VMSD>    { typedef VMSD type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_mitLG, VMSD>  { typedef VMSD type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG, VMSD> { typedef VMSD type; };
template<> struct DiscBCTag<BCCategory::Flux_mitState, VMSD>       { typedef VMSD type; };
template<> struct DiscBCTag<BCCategory::None, VMSD>                { typedef VMSD type; };

// Forward declare
template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;

template <class PhysDim, class TopoDim, class T>
class Field_EG_Cell;


//----------------------------------------------------------------------------//
template< class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
          class Traits, class XFieldType>
class AlgebraicEquationSet_VMSD : public AlgebraicEquationSet_Debug<NDPDEClass_, Traits>
{
public:
  typedef NDPDEClass_ NDPDEClass;
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  template<class ArrayQT>
  using SystemVectorTemplate = typename TraitsType::template SystemVectorTemplate<ArrayQT>;

  typedef BCParameters<BCVector> BCParams;

  typedef IntegrandCell_VMSD_NL<NDPDEClass> IntegrandCellClass;
  typedef IntegrandTrace_VMSD_NL<NDPDEClass> IntegrandTraceClass;

  //TODO: disc tag to be updated
//  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_DispatchClass;
  typedef IntegrateBoundaryTrace_Dispatch_VMSD<NDPDEClass, BCNDConvert, BCVector, VMSD> IntegrateBoundaryTrace_DispatchClass;

  // Base must be used in constructor so that Local can use the BaseType Constructor
  // The global bundle must also be visible, so that SolverInterface can find it
  typedef FieldBundleBase_VMSD<PhysDim,TopoDim,ArrayQ> FieldBundleBase;
  typedef FieldBundle_VMSD<PhysDim,TopoDim,ArrayQ> FieldBundle;

  template<class ParamBuilderType>
  using SolutionDataClass = SolutionData_VMSD<PhysDim, TopoDim, NDPDEClass, ParamBuilderType>;

  typedef ErrorEstimate_VMSD<NDPDEClass, BCNDConvert, BCVector, XFieldType> ErrorEstimateClass;

  template< class... BCArgs >
  AlgebraicEquationSet_VMSD(const XFieldType& xfld,
                                Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                Field_EG_Cell<PhysDim, TopoDim, ArrayQ>& qpfld,
                                Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                const NDPDEClass& pde,
                                const DiscretizationVMSD& stab,
                                const QuadratureOrder& quadratureOrder,
                                const ResidualNormType& resNormType,
                                const std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                const std::vector<int>& interiorTraceGroups,
                                PyDict& BCList,
                                const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                BCArgs&&... args ) :
    DebugBaseType(pde, tol),
    fcnCell_(pde, CellGroups),
    fcnITrace_(pde, stab, interiorTraceGroups ),
    boundaryTraceGroups_(0),
    fcnBTrace_(pde, stab, boundaryTraceGroups_ ),
    BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, std::forward<BCArgs>(args)...)),
    dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab),
    xfld_(xfld),
    xfldCellToTrace_(xfld_.getXField()),
    qfld_(qfld),
    qpfld_(qpfld),
    lgfld_(lgfld),
    pde_(pde),
    quadratureOrder_(get<-1>(xfld), quadratureOrder.cellOrders[0]),
    quadratureOrderMin_(get<-1>(xfld), 0),
    resNormType_(resNormType), tol_(tol),
    mtxCompleteResidual_(qpfld, qfld),
    boundJacPDE_qp_(qfld, qpfld)
  {
    SANS_ASSERT( tol_.size() == 3 );
    for (auto it = CellGroups.begin(); it != CellGroups.end(); ++it) // loop over groups
      for (auto itt = std::next(it,1); itt != CellGroups.end(); ++itt) // loop over remaining groups
        SANS_ASSERT_MSG( *it != *itt, "A cell group can not be specified twice" );

    for (auto mit = BCBoundaryGroups.begin(); mit != BCBoundaryGroups.end(); ++mit)
      for (auto it = mit->second.begin(); it != mit->second.end(); ++it) // loop over groups
        for (auto itt = std::next(it,1); itt != mit->second.end(); ++itt) // loop over remaining groups
          SANS_ASSERT_MSG( *it != *itt, "A boundary group can not be specified twice in one boundary condition" );

    for (int i =0; i<xfld_.getXField().nBoundaryTraceGroups(); i++)
      boundaryTraceGroups_.push_back(i);

    //set static condensation flag
    isStaticCondensed_ = stab.isStaticCondensed();

    //no parallel if it's not static condensed
    if (!isStaticCondensed_) SANS_ASSERT(xfld_.comm()->size() == 1);

  }

  // Solution Data Constructor for using in hiding the specific AlgEqSet in SolverInterface
  template< class... BCArgs>
  AlgebraicEquationSet_VMSD( const XFieldType& xfld,
                                 FieldBundleBase& flds,
                                 const NDPDEClass& pde,
                                 const DiscretizationVMSD& stab,
                                 const QuadratureOrder& quadratureOrder,
                                 const ResidualNormType& resNormType,
                                 const std::vector<Real>& tol,
                                 const std::vector<int>& CellGroups,
                                 const std::vector<int>& interiorTraceGroups,
                                 PyDict& BCList,
                                 const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                 BCArgs&&... args ) :
                                 AlgebraicEquationSet_VMSD(xfld, flds.qfld, flds.qpfld, flds.lgfld, pde, stab,
                                                          quadratureOrder, resNormType, tol, CellGroups, interiorTraceGroups,
                                                          BCList, BCBoundaryGroups, args...) {}


  // Solution Data Constructor for using in hiding the specific AlgEqSet in SolverInterface
  // with dummy discretization object
  template< class... BCArgs>
  AlgebraicEquationSet_VMSD( const XFieldType& xfld,
                            FieldBundleBase& flds,
                            std::shared_ptr<Field_CG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                            const NDPDEClass& pde,
                            const DiscretizationVMSD& stab,
                            const QuadratureOrder& quadratureOrder,
                            const ResidualNormType& resNormType,
                            const std::vector<Real>& tol,
                            const std::vector<int>& CellGroups,
                            const std::vector<int>& interiorTraceGroups,
                            PyDict& BCList,
                            const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                            BCArgs&&... args  ) :
                            AlgebraicEquationSet_VMSD(xfld, flds.qfld, flds.qpfld, flds.lgfld, pde, stab,
                                                     quadratureOrder, resNormType, tol, CellGroups, interiorTraceGroups,
                                                     BCList, BCBoundaryGroups, args...) {}


  virtual ~AlgebraicEquationSet_VMSD() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;


  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override { this->template jacobian<        SystemMatrixView&>(mtx, quadratureOrder_ );    }
  virtual void jacobian(SystemNonZeroPatternView& nz) override { this->template jacobian<SystemNonZeroPatternView&>( nz, quadratureOrderMin_ ); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override { jacobian(Transpose(mtxT), quadratureOrder_ );    }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override { jacobian(Transpose( nzT), quadratureOrderMin_ ); }

  void jacobian(SystemVectorView& b, SystemNonZeroPatternView& nz, bool transposed) override
  {
    this->template jacobian<SystemNonZeroPatternView&>( b, nz, transposed, quadratureOrderMin_ );
  }

  void jacobian(SystemVectorView& b, SystemMatrixView& mtx, bool transposed) override
  {
    this->template jacobian<SystemMatrixView&>( b, mtx, transposed, quadratureOrder_ );
  }

  void jacobian(SystemVectorView& b, SystemNonZeroPatternView& nz,
                 FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_q, FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_qp, bool transposed)
  {
    this->template jacobian<SystemNonZeroPatternView&>( b, nz, transposed, jac_pdep_q, jac_pdep_qp, quadratureOrderMin_ );
  }

  void jacobian(SystemVectorView& b, SystemMatrixView& mtx,
                FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_q, FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_qp, bool transposed)
  {
    this->template jacobian<SystemMatrixView&>( b, mtx, transposed, jac_pdep_q, jac_pdep_qp, quadratureOrder_ );
  }

  // Used to compute jacobians wrt. parameters
  template<int iParam, class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const;

  void completeUpdate(const SystemVectorView& rsd, SystemVectorView& xcondensed, SystemVectorView& x) const override;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override
  {
    const int nDOFPDE = qfld_.nDOFpossessed();
    const int nDOFPDEP = qpfld_.nDOFpossessed();
    const int nDOFBC = lgfld_.nDOFpossessed();
    const int nMon = pde_.nMonitor();

    DLA::VectorD<Real> rsdPDEtmp(nMon);
    DLA::VectorD<Real> rsdPDEptmp(nMon);
    DLA::VectorD<Real> rsdBCtmp(nMon);

    rsdPDEtmp = 0;
    rsdPDEptmp = 0;
    rsdBCtmp = 0;

    std::vector<std::vector<Real>> rsdNorm(3, std::vector<Real>(nMon, 0));
    std::vector<KahanSum<Real>> rsdNormKahanCoarse(nMon, 0);
    std::vector<KahanSum<Real>> rsdNormKahanFine(nMon, 0);

    //compute residual norm
    //HACKED Simple L2 norm
    //TODO: Allow for non-L2 norms
    if (resNormType_ == ResidualNorm_L2 || resNormType_ == ResidualNorm_L2_DOFWeighted)
    {
      for (int n = 0; n < nDOFPDE; n++)
      {
        pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

        for (int j = 0; j < nMon; j++)
          rsdNormKahanCoarse[j] += pow(rsdPDEtmp[j],2);
      }

      for (int n = 0; n < nDOFPDEP; n++)
      {
        pde_.interpResidVariable(rsd[iPDEp][n], rsdPDEptmp);

        for (int j = 0; j < nMon; j++)
          rsdNormKahanFine[j] += pow(rsdPDEptmp[j],2);
      }


      for (int j = 0; j < nMon; j++)
      {
        rsdNorm[iPDE][j] = rsdNormKahanCoarse[j];
        rsdNorm[iPDEp][j] = rsdNormKahanFine[j];
      }

#ifdef SANS_MPI
      rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
      rsdNorm[iPDEp] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDEp], std::plus<std::vector<Real>>());
#endif

      if (resNormType_ == ResidualNorm_L2)
      {
        for (int j = 0; j < nMon; j++)
        {
//          Real tmp = sqrt( rsdNorm[iPDE][j] + rsdNorm[iPDEp][j] );
          rsdNorm[iPDE][j] = sqrt( rsdNorm[iPDE][j] );
          rsdNorm[iPDEp][j] = sqrt( rsdNorm[iPDEp][j] );
        }
      }
      else
      {  //DOF WEIGHTING

        int qDOFnative = qfld_.nDOFnative();
        int qpDOFnative = qpfld_.nDOFnative();

        for (int j = 0; j < nMon; j++)
        {
          //        rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);
          //        rsdNorm[iPDEp][j] = sqrt(rsdNorm[iPDEp][j]);
//          Real tmp = sqrt( rsdNorm[iPDE][j]/qDOFnative + rsdNorm[iPDEp][j]/qpDOFnative );
          rsdNorm[iPDE][j] = sqrt( rsdNorm[iPDE][j]/qDOFnative );
          rsdNorm[iPDEp][j] = sqrt(rsdNorm[iPDEp][j]/qpDOFnative );
        }

      }
    }
    else if (resNormType_ == ResidualNorm_L2_DOFAvg)
    {
      int nDOFPDETotal = nDOFPDE;
      int nDOFPDEPTotal = nDOFPDEP;
      for (int n = 0; n < nDOFPDE; n++)
      {
        pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

        for (int j = 0; j < nMon; j++)
          rsdNormKahanCoarse[j] += pow(rsdPDEtmp[j],2);
      }

      for (int n = 0; n < nDOFPDEP; n++)
      {
        pde_.interpResidVariable(rsd[iPDEp][n], rsdPDEptmp);

        for (int j = 0; j < nMon; j++)
          rsdNormKahanFine[j] += pow(rsdPDEptmp[j],2);
      }


      for (int j = 0; j < nMon; j++)
      {
        rsdNorm[iPDE][j] = rsdNormKahanCoarse[j];
        rsdNorm[iPDEp][j] = rsdNormKahanFine[j];
      }

#ifdef SANS_MPI
      nDOFPDETotal = boost::mpi::all_reduce(*qfld_.comm(), nDOFPDETotal, std::plus<Real>());
      nDOFPDEPTotal = boost::mpi::all_reduce(*qfld_.comm(), nDOFPDEPTotal, std::plus<Real>());
      rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
      rsdNorm[iPDEp] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDEp], std::plus<std::vector<Real>>());
#endif
      int nDOFTotal = nDOFPDETotal + nDOFPDEPTotal;
      for (int j = 0; j < nMon; j++)
      {
//        rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);
//        rsdNorm[iPDEp][j] = sqrt(rsdNorm[iPDEp][j]);
        Real tmp = sqrt( nDOFTotal*(rsdNorm[iPDE][j]/nDOFPDETotal + rsdNorm[iPDEp][j]/nDOFPDEPTotal) );
        rsdNorm[iPDE][j] = tmp;
        rsdNorm[iPDEp][j] = tmp;
      }
    }
    else
      SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_VMSD::residualNorm - Unknown residual norm type!");

    //BC residual
    for (int n = 0; n < nDOFBC; n++)
    {
      pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

    return rsdNorm;

  }

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override
  {
    titles = {"PDEp : ",
              "PDE : ",
              "BC  : "};
    idx = {iPDEp, iPDE, iBC};
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;
  void setAdjointField(const SystemVectorView& adj,
                        Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& wfld,
                        Field_EG_Cell<PhysDim, TopoDim, ArrayQ>& wpfld,
                        Field<PhysDim, TopoDim, ArrayQ>& mufld);

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override { fillSystemVector(qfld_, qpfld_, lgfld_, q); }
  void fillSystemVector( const Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                         const Field_EG_Cell<PhysDim, TopoDim, ArrayQ>& qpfld,
                         const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                         SystemVectorView& q) const;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // Gives the PDE and solution indices in the system
  virtual int indexPDESC() const override { return iPDESC; }
  virtual int indexQSC() const override { return iqSC; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;


  virtual void dumpSolution(const std::string& filenamebase) const override
  {
    std::string filenameq = filenamebase + "_qfld.plt";
    output_Tecplot(qfld_, filenameq);
    std::string filenameqp = filenamebase + "_qpfld.plt";
    output_Tecplot(qpfld_, filenameqp);
  }

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {

    if (isStaticCondensed_ )
    {
      return {qfld_.continuousGlobalMap(0, lgfld_.nDOFpossessed()),
              lgfld_.continuousGlobalMap(qfld_.nDOFpossessed(), 0)};
    }
    else
    {
      static_assert(iqp == 0,"");
      static_assert(iq == 1,"");
      static_assert(ilg == 2,"");

      const int nDOFPDEppos = qpfld_.nDOFpossessed();
      const int nDOFPDEpos = qfld_.nDOFpossessed();
      const int nDOFBCpos  = lgfld_.nDOFpossessed();

      return {qpfld_.continuousGlobalMap(0, nDOFPDEpos + nDOFBCpos),
        qfld_.continuousGlobalMap(nDOFPDEppos, nDOFBCpos),
        lgfld_.continuousGlobalMap(nDOFPDEpos + nDOFPDEppos, 0)};
    }
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return qfld_.comm(); }

  virtual void syncDOFs_MPI() override
  {
    qpfld_.syncDOFs_MPI_Cached();
    qfld_.syncDOFs_MPI_Cached();
    lgfld_.syncDOFs_MPI_Cached();
  }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  // Indexes to order the equations and the solution vectors
  // primes go first in Jacobian so that top left block is block diagonal
  static const int iPDEp = 0;
  static const int iPDE = 1;
  static const int iBC = 2;
  static const int iqp = 0;
  static const int iq = 1;
  static const int ilg = 2;

  //static condensation indexing
  static const int iPDESC = 0;
  static const int iBCSC = 1;
  static const int iqSC = 0;
  static const int ilgSC = 1;


  const std::map< std::string, std::shared_ptr<BCBase> >& BCs() const { return BCs_; }
  const IntegrateBoundaryTrace_DispatchClass& dispatchBC() const { return dispatchBC_; }

  void fillResidualField( const Field_DG_Cell<PhysDim,TopoDim,ArrayQ>& qfld_dg,
                                std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld ) const;

protected:
  template<class SparseMatrixType>
  void jacobian( SparseMatrixType mtx, const QuadratureOrder& quadratureOrder );

  template<class SparseMatrixType>
  void jacobian(SystemVectorView& b, SparseMatrixType mtx, bool transposed, const QuadratureOrder& quadratureOrder );

  template<class SparseMatrixType>
  void jacobian(SystemVectorView& b, SparseMatrixType mtx, bool transposed,
                FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_q, FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_qp, const QuadratureOrder& quadratureOrder );


  std::vector<int> cellGroups_;
  IntegrandCellClass fcnCell_;
  std::vector<int> interiorTraceGroups_;
  IntegrandTraceClass fcnITrace_;
  std::vector<int> boundaryTraceGroups_;
  IntegrandTraceClass fcnBTrace_;
  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_DispatchClass dispatchBC_;

  const XFieldType& xfld_;
  XField_CellToTrace<PhysDim, TopoDim> xfldCellToTrace_;
  Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  Field_EG_Cell<PhysDim, TopoDim, ArrayQ>& qpfld_;
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const NDPDEClass& pde_;
  const QuadratureOrder quadratureOrder_;
  const QuadratureOrder quadratureOrderMin_;
  const ResidualNormType resNormType_;
  const std::vector<Real>& tol_;

//  bool computed_dRsd_;
  FieldDataMatrixD_Cell<MatrixQ> mtxCompleteResidual_;
  FieldDataMatrixD_BoundaryCell<MatrixQ> boundJacPDE_qp_; //auxiliary jacobian for boundary cells

  using BaseType::isStaticCondensed_;

};

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_VMSD(fcnCell_, fcnITrace_, fcnBTrace_, xfldCellToTrace_,
                                                            xfld_, qfld_, qpfld_, quadratureOrder_, rsd(iPDE), rsd(iPDEp) ),
                                           xfld_, (qfld_, qpfld_),  quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatch_VMSD(
      ResidualBoundaryTrace_Dispatch_VMSD( xfld_, qfld_, qpfld_,
                                           quadratureOrder_.boundaryTraceOrders.data(),
                                           quadratureOrder_.boundaryTraceOrders.size(), rsd(iPDE) ) );

}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder)
{
  SANS_ASSERT(jac.m() == 3);
  SANS_ASSERT(jac.n() == 3);
  SANS_ASSERT( !isStaticCondensed_ );

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE,iq);
  Matrix jacPDE_qp  = jac(iPDE,iqp);

//  Matrix jacPDE_lg = jac(iPDE,ilg);

  Matrix jacPDEp_q  = jac(iPDEp,iq);
  Matrix jacPDEp_qp  = jac(iPDEp,iqp);
//  Matrix jacPDEp_lg = jac(iPDEp,ilg);

//  Matrix jacBC_q   = jac(iBC,iq);
//  Matrix jacBC_qp   = jac(iBC,iqp);
//  Matrix jacBC_lg  = jac(iBC,ilg);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate( JacobianCell_VMSD<SurrealClass>(fcnCell_, fcnITrace_, fcnBTrace_, xfldCellToTrace_,
                                                                          xfld_, qfld_, qpfld_, quadratureOrder_,
                                                            jacPDE_q, jacPDE_qp, jacPDEp_q, jacPDEp_qp),
                                           xfld_, (qfld_, qpfld_),  quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size()  );


  dispatchBC_.dispatch_VMSD(
      JacobianBoundaryTrace_Dispatch_VMSD<SurrealClass>( xfld_, qfld_, qpfld_,
                                           quadratureOrder_.boundaryTraceOrders.data(),
                                           quadratureOrder_.boundaryTraceOrders.size(),
                                           jacPDE_q, jacPDE_qp  ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SystemVectorView& rsd, SparseMatrixType jac, bool transpose, const QuadratureOrder& quadratureOrder)
{
  SANS_ASSERT(jac.m() == 2);
  SANS_ASSERT(jac.n() == 2);
  SANS_ASSERT( isStaticCondensed_ );

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDESC,iqSC);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  boundJacPDE_qp_ = 0;

  dispatchBC_.dispatch_VMSD(
      JacobianBoundaryTrace_Dispatch_VMSD_SC<SurrealClass>( xfld_, qfld_, qpfld_,
                                           quadratureOrder_.boundaryTraceOrders.data(),
                                           quadratureOrder_.boundaryTraceOrders.size(),
                                           jacPDE_q, boundJacPDE_qp_  ) );

  FieldDataMatrixD_Cell<MatrixQ> dummyjac;

    IntegrateCellGroups<TopoDim>::integrate( JacobianCell_VMSD_SC2<SurrealClass>(fcnCell_, fcnITrace_, fcnBTrace_, xfldCellToTrace_,
                                                                            xfld_, qfld_, qpfld_, quadratureOrder_, transpose,
                                                                            rsd(iPDE), rsd(iPDEp), jacPDE_q,
                                                                            dummyjac, dummyjac,
                                                                            mtxCompleteResidual_, boundJacPDE_qp_),
                                             xfld_, (qfld_, qpfld_),  quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size()  );


}



template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SystemVectorView& rsd, SparseMatrixType jac, bool transpose,
         FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_q,  FieldDataMatrixD_Cell<MatrixQ>& jac_pdep_qp,  const QuadratureOrder& quadratureOrder)
{
  SANS_ASSERT(jac.m() == 2);
  SANS_ASSERT(jac.n() == 2);
  SANS_ASSERT( isStaticCondensed_ );

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDESC,iqSC);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  dispatchBC_.dispatch_VMSD(
      JacobianBoundaryTrace_Dispatch_VMSD_SC<SurrealClass>( xfld_, qfld_, qpfld_,
                                           quadratureOrder_.boundaryTraceOrders.data(),
                                           quadratureOrder_.boundaryTraceOrders.size(),
                                           jacPDE_q, boundJacPDE_qp_  ) );

    IntegrateCellGroups<TopoDim>::integrate( JacobianCell_VMSD_SC2<SurrealClass>(fcnCell_, fcnITrace_, fcnBTrace_, xfldCellToTrace_,
                                                                            xfld_, qfld_, qpfld_, quadratureOrder_, transpose,
                                                                            rsd(iPDE), rsd(iPDEp), jacPDE_q,
                                                                            jac_pdep_q, jac_pdep_qp,
                                                                            mtxCompleteResidual_, boundJacPDE_qp_),
                                             xfld_, (qfld_, qpfld_),  quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size()  );


}




template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<int iParam, class SparseMatrixType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const
{
  SANS_DEVELOPER_EXCEPTION("VMSD JACOBIANPARAMS AREN'T IMPLEMENTED");
#if 0
  SANS_ASSERT(jac.m() >= 2);
  SANS_ASSERT(jac.n() > ip);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_p  = jac(iPDE,ip);
  Matrix jacBC_p = jac(iBC,ip);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_Galerkin_Param<SurrealClass, iParam>(fcnCell_, jacPDE_p),
      xfld_, qfld_,
      quadratureOrder_.cellOrders.data(),
      quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatch(
    JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_Param<SurrealClass, iParam>( xfld_, qfld_, lgfld_,
                                                                               quadratureOrder_.boundaryTraceOrders.data(),
                                                                               quadratureOrder_.boundaryTraceOrders.size(),
                                                                               jacPDE_p, jacBC_p ),
    JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_Param<SurrealClass, iParam>( xfld_, qfld_,
                                                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                                                quadratureOrder_.boundaryTraceOrders.size(),
                                                                                jacPDE_p ) );
#endif
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
completeUpdate(const SystemVectorView& rsd, SystemVectorView& xcondensed, SystemVectorView& x) const
{
  SANS_ASSERT( isStaticCondensed_ == true);
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == x[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
  {
    x[iq][k] = xcondensed[iqSC][k];
  }

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == x[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
  {
    x[ilg][k] = xcondensed[ilgSC][k];
  }

  //solve for qp field using the matrix we saved off...
  IntegrateCellGroups<TopoDim>::integrate( CompleteUpdate_VMSD( fcnCell_, mtxCompleteResidual_, rsd(iPDEp), x(iq), x(iqp) ),
                                           xfld_, (qfld_, qpfld_),  quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

}



template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDEp = qpfld_.nDOFpossessed() + qpfld_.nDOFghost();
  SANS_ASSERT( nDOFPDEp == q[iqp].m() );
  for (int k = 0; k < nDOFPDEp; k++)
    qpfld_.DOF(k) = q[iqp][k];

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    lgfld_.DOF(k) = q[ilg][k];
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>
::setAdjointField( const SystemVectorView& adj,
                    Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& wfld,
                    Field_EG_Cell<PhysDim, TopoDim, ArrayQ>& wpfld,
                    Field<PhysDim, TopoDim, ArrayQ>& mufld )
{
  // Used to assign Adjoint field bundle

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = wfld.nDOFpossessed() + wfld.nDOFghost();
  SANS_ASSERT( nDOFPDE == adj[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
  {
    wfld.DOF(k) = adj[iq][k];
  }

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDEP = wpfld.nDOFpossessed() + wpfld.nDOFghost();
  SANS_ASSERT( nDOFPDEP == adj[iqp].m() );
  for (int k = 0; k < nDOFPDEP; k++)
  {
    wpfld.DOF(k) = adj[iqp][k];
  }

  const int nDOFBC = mufld.nDOFpossessed() + mufld.nDOFghost();
  SANS_ASSERT( nDOFBC == adj[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    mufld.DOF(k) = adj[ilg][k];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
fillSystemVector(const Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                 const Field_EG_Cell<PhysDim, TopoDim, ArrayQ>& qpfld,
                 const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                 SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld.nDOFpossessed() + qfld.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld.DOF(k);

  const int nDOFPDEp = qpfld.nDOFpossessed() + qpfld.nDOFghost();
  SANS_ASSERT( nDOFPDEp == q[iqp].m() );
  for (int k = 0; k < nDOFPDEp; k++)
    q[iqp][k] = qpfld.DOF(k);

  const int nDOFBC = lgfld.nDOFpossessed() + lgfld.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld.DOF(k);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDEp == 0,"");
  static_assert(iPDE == 1,"");
  static_assert(iBC == 2,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();

  int nDOFPDEppos = 0;
  if ( isStaticCondensed_ )
    nDOFPDEppos = qpfld_.nDOFpossessed() + qpfld_.nDOFghost();
  else
    nDOFPDEppos = qpfld_.nDOFpossessed();

//  const int nDOFPDEppos = qpfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();


  return {nDOFPDEppos,
          nDOFPDEpos,
          nDOFBCpos};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorStateSize() const
{
  static_assert(iqp == 0,"");
  static_assert(iq == 1,"");
  static_assert(ilg == 2,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFPDEp = qpfld_.nDOFpossessed() + qpfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {nDOFPDEp,
          nDOFPDE,
          nDOFBC};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::matrixSize() const
{
  static_assert(iPDEp == 0,"");
  static_assert(iPDE == 1,"");
  static_assert(iBC == 2,"");
  static_assert(iqp == 0,"");
  static_assert(iq == 1,"");
  static_assert(ilg == 2,"");

  if ( isStaticCondensed_ == true )
  {  // Create the size that represents the size of a sparse linear algebra matrix
    const int nDOFPDEpos = qfld_.nDOFpossessed();
    const int nDOFBCpos  = lgfld_.nDOFpossessed();

    const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
    const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

    return {{ {nDOFPDEpos, nDOFPDE}, {nDOFPDEpos, nDOFBC} },
            { {nDOFBCpos , nDOFPDE}, {nDOFBCpos , nDOFBC} }};

  }
  else
  {

    // Create the size that represents the size of a sparse linear algebra matrix
    const int nDOFPDEpos = qfld_.nDOFpossessed();
    const int nDOFPDEppos = qpfld_.nDOFpossessed();
    const int nDOFBCpos  = lgfld_.nDOFpossessed();

    const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
    const int nDOFPDEp = qpfld_.nDOFpossessed() + qpfld_.nDOFghost();
    const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

    return {{ {nDOFPDEppos, nDOFPDEp}, {nDOFPDEppos, nDOFPDE}, {nDOFPDEppos, nDOFBC} },
      { {nDOFPDEpos, nDOFPDEp}, {nDOFPDEpos, nDOFPDE}, {nDOFPDEpos, nDOFBC} },
      { {nDOFBCpos , nDOFPDEp}, {nDOFBCpos , nDOFPDE}, {nDOFBCpos , nDOFBC} }};
  }
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
bool
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  bool isValidState = true;

  // Update the solution field
  setSolutionField(q);

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell_VMSD(pde_, fcnCell_.cellGroups(), isValidState),
                                             xfld_, (qfld_, qpfld_),  quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  IntegrateInteriorTraceGroups<TopoDim>::integrate( isValidStateInteriorTrace_VMSD(pde_, fcnITrace_.interiorTraceGroups(), isValidState),
                                                    xfld_, (qfld_, qpfld_),
                                                    quadratureOrder_.interiorTraceOrders.data(),
                                                    quadratureOrder_.interiorTraceOrders.size() );

  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                quadratureOrder_.boundaryTraceOrders.size(),
                                                isValidState ),
      isValidStateBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size(),
                                                 isValidState )
    );

#ifdef SANS_MPI
  int validstate = isValidState ? 1 : 0;
  isValidState = (boost::mpi::all_reduce(*qfld_.comm(), validstate, std::plus<int>()) == qfld_.comm()->size());
#endif

  return isValidState;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
inline int
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
nResidNorm() const
{
  return 3;
}



template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
fillResidualField(const Field_DG_Cell<PhysDim,TopoDim,ArrayQ>& qfld_dg,
                  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld) const
{
  DLA::VectorD<ArrayQ> rsd_dg(qfld_dg.nDOFpossessed());
  DLA::VectorD<ArrayQ> rsdp_dg(qpfld_.nDOFpossessed() + qpfld_.nDOFghost() );
  rsd_dg = 0;

  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_VMSD(fcnCell_, fcnITrace_, fcnBTrace_, xfldCellToTrace_,
                                                            xfld_, qfld_dg, qpfld_, quadratureOrder_,
                                                            rsd_dg, rsdp_dg ),
                                           xfld_, (qfld_dg, qpfld_),  quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatch_VMSD(
      ResidualBoundaryTrace_Dispatch_VMSD( xfld_, qfld_dg, qpfld_,
                                               quadratureOrder_.boundaryTraceOrders.data(),
                                               quadratureOrder_.boundaryTraceOrders.size(),
                                               rsd_dg ) );

  for (int i=0; i<qfld_dg.nDOFpossessed(); i++)
    up_resfld->DOF(i) = rsd_dg[i];

}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);

//  std::string filename_iBC = filenamebase + "_iBC_iter" + std::to_string(nonlinear_iter) + ".plt";
//  this->dumpLinesearchField(lgfld_, *pStepData, iBC, filename_iBC);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_VMSD_H
