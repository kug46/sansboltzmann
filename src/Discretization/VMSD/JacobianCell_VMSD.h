// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_VMSD_H
#define JACOBIANCELL_VMSD_H

// HDG cell integral jacobian functions

#include "../VMSD/JacobianCell_VMSD_Element.h"
#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/Identity.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/XField_CellToTrace.h"

#include "Discretization/QuadratureOrder.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateGhostBoundaryTraceGroups.h"
#include "Jacobian_InnerBoundaryTrace_VMSD.h"
#include "JacobianInteriorTrace_VMSD.h"


namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral jacobian
//

template<class Surreal, class IntegrandCell, class IntegrandITrace, class XFieldType_, class TopoDim_>
class JacobianCell_VMSD_impl :
    public GroupIntegralCellType< JacobianCell_VMSD_impl<Surreal, IntegrandCell, IntegrandITrace, XFieldType_, TopoDim_> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::PDE PDE;

  static_assert( std::is_same< PhysDim, typename IntegrandITrace::PhysDim >::value, "PhysDim should be the same.");

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename IntegrandCell::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandCell::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;


  // Save off the boundary trace integrand and the residual vectors
  JacobianCell_VMSD_impl( const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace, const IntegrandITrace& fcnBTrace,
                         const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace,
                         const XFieldType_& xfld,
                         const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                         const Field<PhysDim, TopoDim_, ArrayQ>& qpfld,
                         const QuadratureOrder& quadOrder,
                         MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                         MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qp,
                         MatrixScatterAdd<MatrixQ>& mtxGlobalPDEp_q,
                         MatrixScatterAdd<MatrixQ>& mtxGlobalPDEp_qp) :
                           fcnCell_(fcnCell), fcnITrace_(fcnITrace), fcnBTrace_(fcnBTrace),
                           xfldCellToTrace_(xfldCellToTrace),
                           xfld_(xfld), qfld_(qfld), qpfld_(qpfld),
                           quadOrder_(quadOrder),
                           nITraceGroup_( xfld.getXField().nInteriorTraceGroups() ),
                           nBTraceGroup_( xfld.getXField().nBoundaryTraceGroups()),
                           nGTraceGroup_( xfld.getXField().nGhostBoundaryTraceGroups() ),
                           mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_qp_(mtxGlobalPDE_qp),
                           mtxGlobalPDEp_q_(mtxGlobalPDEp_q), mtxGlobalPDEp_qp_(mtxGlobalPDEp_qp),
                           comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_qp_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDEp_q_.m() == qpfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDEp_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDEp_qp_.m() == qpfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDEp_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                          ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    typedef JacobianElemCell_VMSD<PhysDim,MatrixQ> JacobianElemCellType;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qpfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass        qfldElem       ( qfldCell.basis() );
    ElementQFieldClass        qpfldElem       ( qpfldCell.basis() );

    // DOFs per element
    const int qDOF = qfldElem.nDOF();
    const int qDOFp = qpfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_q(qDOF, -1);
    std::vector<int> mapDOFLocal_q(qDOF, -1);
    std::vector<int> maprsd(qDOF, -1);
    int nDOFLocal = 0;
    int nDOFpossessed =  mtxGlobalPDE_q_.m();

    std::vector<int> mapDOFGlobal_qp(qDOFp, -1);
//   int nDOFppossessed =  mtxGlobalPDEp_qp_.m();

    // element integrals
    GalerkinWeightedIntegral_New<TopoDim, Topology, JacobianElemCellType > integralPDE(quadratureorder);

    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == qpfldCell.nElem() );

    // element jacobian matrices
    JacobianElemCellType mtxElem(qDOF, qDOFp);
    JacobianElemCellType mtxElemTmp(qDOF, qDOFp);

    // loop over elements within group and solve for the auxiliary variables first
    for (int elem = 0; elem < nelem; elem++)
    {
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal_q.data(), mapDOFGlobal_q.size() );
      qpfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal_qp.data(), mapDOFGlobal_qp.size() );

      nDOFLocal = 0;
      for (int n=0; n<qDOF; n++)
      {
        if (mapDOFGlobal_q[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal_q[nDOFLocal] = mapDOFGlobal_q[n];
          nDOFLocal++;
        }
      }

      if (nDOFLocal == 0 && qpfldCell.associativity( elem ).rank() != comm_rank_) continue;

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qpfldCell.getElement( qpfldElem, elem );

      std::map<int,std::vector<int>> cellITraceGroups;
      std::map<int,std::vector<int>> cellBTraceGroups;
      std::map<int,std::vector<int>> cellGTraceGroups;

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, elem, trace);

        if (traceinfo.type == TraceInfo::Interior)
        {
          //add this traceGroup and traceElem to the set of interior traces attached to this cell
          cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::Boundary)
        {
          cellBTraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::GhostBoundary)
        {
          cellGTraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
      }

      //-----------------------------------------------------------------------------------------

      //try to save some computation by only computing q' jacobians for cell group zero
      //because we only allow q' to change outside of cellgroup zero

      mtxElem = 0; mtxElemTmp = 0;
      // cell integration for canonical element
      integralPDE( fcnCell_.integrand(xfldElem, qfldElem, qpfldElem), get<-1>(xfldElem), mtxElemTmp );
      mtxElem += mtxElemTmp;

      //Collect the contributions to the PDE residual jacobian wrt q from the traces
      //Other jacobians (i.e. PDE_qI, INT_q, and INT_qI) will be scatter-added inside JacobianInteriorTrace_HDG


      if (nITraceGroup_ > 0)
      {
        std::vector<int> quadOrderITrace;

        for (int i=0; i< nITraceGroup_; i++)
          quadOrderITrace.push_back(quadOrder_.interiorTraceOrders[0]);

        IntegrateInteriorTraceGroups<TopoDim>::integrate(
            JacobianInteriorTrace_VMSD<Surreal>(fcnITrace_, cellITraceGroups,
                                               cellGroupGlobal, elem,
                                               mtxElem.PDE_q, mtxElem.PDE_qp,
                                               mtxElem.PDEp_q, mtxElem.PDEp_qp),
                                               xfld_, (qfld_, qpfld_),
                                               quadOrderITrace.data(), quadOrderITrace.size());
      }

      if (nBTraceGroup_ > 0)
      {
        std::vector<int> quadOrderBTrace;

        for (int i=0; i< nBTraceGroup_; i++)
          quadOrderBTrace.push_back(quadOrder_.interiorTraceOrders[0]);

        IntegrateBoundaryTraceGroups<TopoDim>::integrate(
            Jacobian_InnerBoundaryTrace_VMSD<Surreal>(fcnBTrace_, cellBTraceGroups,
                                               cellGroupGlobal, elem,
                                               mtxElem.PDE_q, mtxElem.PDE_qp,
                                               mtxElem.PDEp_q, mtxElem.PDEp_qp),
                                               xfld_, (qfld_, qpfld_),
                                               quadOrderBTrace.data(), quadOrderBTrace.size());
      }

//      SANS_ASSERT(nGTraceGroup_ == 0);
      if (nGTraceGroup_ > 0)
      {
        std::vector<int> quadOrderGTrace;

        for (int i=0; i< nGTraceGroup_; i++)
          quadOrderGTrace.push_back(quadOrder_.interiorTraceOrders[0]);

        IntegrateGhostBoundaryTraceGroups<TopoDim>::integrate(
            Jacobian_InnerBoundaryTrace_VMSD<Surreal>(fcnBTrace_, cellGTraceGroups,
                                               cellGroupGlobal, elem,
                                               mtxElem.PDE_q, mtxElem.PDE_qp,
                                               mtxElem.PDEp_q, mtxElem.PDEp_qp),
                                               xfld_, (qfld_, qpfld_),
                                               quadOrderGTrace.data(), quadOrderGTrace.size() );
      }



      // HACK TO SET CELL DOFS TO ZERO DIRECTLY
      // REMOVES OVERLAP IN BASIS
      const int nBubble = qfldCell.associativity( elem ).nBubble();
      if (nBubble > 0 &&  (qpfldElem.order() > PhysDim::D) )
      {
        std::vector<int> mapDOFpCell( nBubble, -1 );
        SANS_ASSERT( qpfldElem.basis()->category() == BasisFunctionCategory_Lagrange );

        for (int ind=(qDOFp - nBubble); ind<qDOFp; ind++) //CELL DOFS WILL BE LAST?
        {
          for (int j=0; j<qDOFp; j++)
          {
            mtxElem.PDEp_qp(ind, j) = 0;
            mtxElem.PDEp_qp(j, ind) = 0;
          }

          for (int j=0; j<qDOF; j++)
          {
            mtxElem.PDEp_q(ind, j) = 0;
            mtxElem.PDE_qp(j, ind) = 0;
          }

          mtxElem.PDEp_qp(ind,ind) = DLA::Identity();
        }
      }

      // coarse eqns
      if (nDOFLocal == qDOF)
      {
        scatterAdd(mapDOFGlobal_q, nDOFLocal, mapDOFGlobal_q, mtxElem.PDE_q, mtxGlobalPDE_q_ ); //w.r.t q
        scatterAdd(mapDOFGlobal_q, nDOFLocal, mapDOFGlobal_qp, mtxElem.PDE_qp, mtxGlobalPDE_qp_ ); //w.r.t qp
      }
      else
      {
        mtxElemTmp = 0;
        for (int i = 0; i < nDOFLocal; i++)
        {
          for (int j = 0; j < qDOF; j++)
            mtxElemTmp.PDE_q(i,j) = mtxElem.PDE_q(maprsd[i], j);

          for (int j = 0; j < qDOFp; j++)
            mtxElemTmp.PDE_qp(i,j) = mtxElem.PDE_qp(maprsd[i], j);
        }

        scatterAdd(mapDOFLocal_q, nDOFLocal, mapDOFGlobal_q, mtxElemTmp.PDE_q, mtxGlobalPDE_q_);
        scatterAdd(mapDOFLocal_q, nDOFLocal, mapDOFGlobal_qp, mtxElemTmp.PDE_qp, mtxGlobalPDE_qp_);
      }

      if ( qpfldCell.associativity( elem ).rank() == comm_rank_ )
      {
        scatterAdd(mapDOFGlobal_qp, qDOFp, mapDOFGlobal_q, mtxElem.PDEp_q, mtxGlobalPDEp_q_ ); //w.r.t q
        scatterAdd(mapDOFGlobal_qp, qDOFp, mapDOFGlobal_qp, mtxElem.PDEp_qp, mtxGlobalPDEp_qp_ ); //w.r.t qp
      }
    } //elem

  }

//----------------------------------------------------------------------------//
  template <template <class> class SparseMatrixType>
  void
  scatterAdd( std::vector<int>& mapDOFLocal, const int nDOFLocal,
              std::vector<int>& mapDOFGlobal,
              const SANS::DLA::MatrixD<MatrixQ>& mtxElem,
              SparseMatrixType<MatrixQ>& mtxGlobal )
  {
    // jacobian wrt q

    mtxGlobal.scatterAdd( mtxElem, mapDOFLocal.data(), nDOFLocal, mapDOFGlobal.data(), mapDOFGlobal.size() );

  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const IntegrandITrace& fcnBTrace_;

  const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace_;
  const XFieldType_& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qpfld_;

  const QuadratureOrder& quadOrder_;
  const int nITraceGroup_;
  const int nBTraceGroup_;
  const int nGTraceGroup_;

  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qp_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDEp_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDEp_qp_;

  mutable int comm_rank_;
};

// Factory function

template<class Surreal, class IntegrandCell, class IntegrandITrace,
         class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
JacobianCell_VMSD_impl<Surreal, IntegrandCell, IntegrandITrace, XFieldType, TopoDim>
JacobianCell_VMSD( const IntegrandCellType<IntegrandCell>& fcnCell,
                  const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                  const IntegrandInteriorTraceType<IntegrandITrace>& fcnBTrace,
                  const XField_CellToTrace<PhysDim, TopoDim>& xfldCellToTrace,
                  const XFieldType& xfld,
                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                  const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                  const QuadratureOrder& quadOrder,
                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qp,
                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDEp_q,
                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDEp_qp)
{
  return { fcnCell.cast(), fcnITrace.cast(), fcnBTrace.cast(),
           xfldCellToTrace, xfld, qfld, qpfld, quadOrder,
           mtxGlobalPDE_q, mtxGlobalPDE_qp,
           mtxGlobalPDEp_q, mtxGlobalPDEp_qp};
}

}

#endif  // JACOBIANCELL_VMSD_H
