// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_VMSD_H
#define JACOBIANINTERIORTRACE_VMSD_H

// HDG trace integral jacobian functions

#include "../VMSD/JacobianCell_VMSD_Element.h"
#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Field/FieldData/FieldDataReal_Cell.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class Surreal, class IntegrandInteriorTrace>
class JacobianInteriorTrace_VMSD_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_VMSD_impl<Surreal, IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianInteriorTrace_VMSD_impl( const IntegrandInteriorTrace& fcn,
                                  const std::map<int,std::vector<int>>& cellITraceGroups,
                                  const int& cellgroup, const int& cellelem,
                                  MatrixElemClass& mtxElemPDE_q,
                                  MatrixElemClass& mtxElemPDE_qp,
                                  MatrixElemClass& mtxElemPDEp_q,
                                  MatrixElemClass& mtxElemPDEp_qp) :
    fcn_(fcn), cellITraceGroups_(cellITraceGroups),
    cellgroup_(cellgroup), cellelem_(cellelem),
    mtxElemPDE_q_(mtxElemPDE_q), mtxElemPDE_qp_(mtxElemPDE_qp),
    mtxElemPDEp_q_(mtxElemPDEp_q), mtxElemPDEp_qp_(mtxElemPDEp_qp)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds) const
  {
    //CHECKS NOT APPLICABLE
//    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
//    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
//
//    SANS_ASSERT( mtxElemPDE_q_.m() == qfld.nDOFpossessed() );
//    SANS_ASSERT( mtxElemPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDE_qp_.m() == qfld.nDOFpossessed() );
//    SANS_ASSERT( mtxElemPDE_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDEp_q_.m() == qpfld.nDOFpossessed() );
//    SANS_ASSERT( mtxElemPDEp_q_.n() == qfld.nDOFpossessed() + qpfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDEp_qp_.m() == qpfld.nDOFpossessed() );
//    SANS_ASSERT( mtxElemPDEp_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef JacobianElemCell_VMSD<PhysDim, MatrixQ> JacobianElemInteriorTraceType;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const QFieldCellGroupTypeR& qpfldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementQFieldClassR qpfldElemR( qpfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFpL = qpfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFpR = qpfldElemR.nDOF();

    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType>
      integralPDEL(quadratureorder);

    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType>
      integralPDER(quadratureorder);

    // element jacobian matrices
    JacobianElemInteriorTraceType mtxElemL(nDOFL, nDOFpL);
    JacobianElemInteriorTraceType mtxElemR(nDOFR, nDOFpR);


    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        //The cell that called this function is to the left of the trace
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        qpfldCellL.getElement( qpfldElemL, elemL );

        // PDE trace integration for canonical element
        integralPDEL( fcn_.integrand(xfldElemTrace, canonicalTraceL, +1, xfldElemL, qfldElemL, qpfldElemL),
                          xfldElemTrace, mtxElemL );

        mtxElemPDE_q_ += mtxElemL.PDE_q;
        mtxElemPDE_qp_ += mtxElemL.PDE_qp;
        mtxElemPDEp_q_ += mtxElemL.PDEp_q;
        mtxElemPDEp_qp_ += mtxElemL.PDEp_qp;

      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        //The cell that called this function is to the right of the trace
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        qpfldCellR.getElement( qpfldElemR, elemR );

        // PDE trace integration for canonical element
        integralPDER( fcn_.integrand(xfldElemTrace, canonicalTraceR, -1, xfldElemR, qfldElemR, qpfldElemR),
                          xfldElemTrace, mtxElemR );

        mtxElemPDE_q_ += mtxElemR.PDE_q;
        mtxElemPDE_qp_ += mtxElemR.PDE_qp;
        mtxElemPDEp_q_ += mtxElemR.PDEp_q;
        mtxElemPDEp_qp_ += mtxElemR.PDEp_qp;

      }
      else
        SANS_DEVELOPER_EXCEPTION("JacobianInteriorTrace_VMSD_impl::integrate - Invalid configuration!");

    } //elem loop
  }


protected:
  const IntegrandInteriorTrace& fcn_;
  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;

  MatrixElemClass& mtxElemPDE_q_; //jacobian of PDE residual wrt q for the central cell
  MatrixElemClass& mtxElemPDE_qp_; //jacobian of PDE residual wrt q for the central cell
  MatrixElemClass& mtxElemPDEp_q_; //jacobian of PDE residual wrt q for the central cell
  MatrixElemClass& mtxElemPDEp_qp_; //jacobian of PDE residual wrt q for the central cell

  std::vector<int> traceGroupIndices_;
};


// Factory function

template<class Surreal, class IntegrandInteriorTrace, class MatrixQ>
JacobianInteriorTrace_VMSD_impl<Surreal, IntegrandInteriorTrace>
JacobianInteriorTrace_VMSD( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                           const std::map<int,std::vector<int>>& cellITraceGroups,
                           const int& cellgroup, const int& cellelem,
                           DLA::MatrixD<MatrixQ>& mtxElemPDE_q,
                           DLA::MatrixD<MatrixQ>& mtxElemPDE_qp,
                           DLA::MatrixD<MatrixQ>& mtxElemPDEp_q,
                           DLA::MatrixD<MatrixQ>& mtxElemPDEp_qp)
{
  return { fcn.cast(), cellITraceGroups, cellgroup, cellelem,
           mtxElemPDE_q, mtxElemPDE_qp,
           mtxElemPDEp_q, mtxElemPDEp_qp };
}

}


#endif  // JACOBIANINTERIORTRACE_VMSD_H
