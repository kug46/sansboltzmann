// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDTRACE_VMSD_H
#define INTEGRANDTRACE_VMSD_H

// interior trace integrand operator: VMSD
//#define VMSDBOUNDARYSOURCE

#include <ostream>
#include <vector>

#include "../VMSD/JacobianCell_VMSD_Element.h"
#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/Galerkin/Stabilization_Nitsche.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// interior trace integrand: VMSD - upwind and viscous stabilization between DG and CG fields
//
// integrandL = + [[phiL]].{F}
// integrandR =   [[phiR]].{F}
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective, viscous)
//
template <class PDE_>
class IntegrandTrace_VMSD : public IntegrandInteriorTraceType<IntegrandTrace_VMSD<PDE_> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  // cppcheck-suppress noExplicitConstructor
  IntegrandTrace_VMSD(const PDE& pde, const StabilizationNitsche& stab,
                      const std::vector<int>& interiorTraceGroups,
                      const bool isVMSD0 = false,
                      const std::vector<int> periodicTraceGroups = {} ) :
    pde_(pde),
    stab_(stab),
    interiorTraceGroups_(interiorTraceGroups),
    periodicTraceGroups_(periodicTraceGroups),
    isVMSD0_(isVMSD0) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  std::size_t nPeriodicTraceGroups() const          { return periodicTraceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return periodicTraceGroups_[n]; }
  std::vector<int> periodicTraceGroups() const      { return periodicTraceGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL, class ElementParamL>
  class BasisWeighted
  {
  public:

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;

    typedef typename ElementXFieldTraceType::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;

    typedef JacobianElemCell_VMSD<PhysDim,MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde,
                   const StabilizationNitsche& stab,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldL& qfldElemL,
                   const ElementQFieldL& qpfldElemL) :
      pde_(pde), stab_(stab),
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), qpfldElemL_(qpfldElemL), paramfldElemL_( paramfldElemL ),
      nDOFL_(qfldElemL_.nDOF()),
      nDOFpL_(qpfldElemL_.nDOF()),
      phiL_( new Real[nDOFL_] ),
      phipL_( new Real[nDOFpL_] ),
      gradphiL_( new VectorX[nDOFL_] ),
      gradphipL_( new VectorX[nDOFpL_] ),
      Cb_(stab_.getNitscheConstant(D)),
      invL_(xfldElemTrace_.jacobianDeterminant() / xfldElemL_.jacobianDeterminant()),
      isVMSD0_(qpfldElemL_.order() == 0) {}

    BasisWeighted( BasisWeighted&& bw ) :
      pde_(bw.pde_), stab_(bw.stab_),
      xfldElemTrace_(bw.xfldElemTrace_), canonicalTraceL_(bw.canonicalTraceL_), normalSign_(bw.normalSign_),
      xfldElemL_(bw.xfldElemL_), qfldElemL_(bw.qfldElemL_), qpfldElemL_(bw.qpfldElemL_), paramfldElemL_( bw.paramfldElemL_ ),
      nDOFL_(bw.nDOFL_),
      nDOFpL_(bw.nDOFpL_),
      phiL_( bw.phiL_ ),
      phipL_( bw.phipL_ ),
      gradphiL_( bw.gradphiL_ ),
      gradphipL_( bw.gradphipL_ ),
      Cb_(bw.Cb_),
      invL_(bw.invL_),
      isVMSD0_(bw.isVMSD0_)
    {
      bw.phiL_ = nullptr; bw.gradphiL_ = nullptr;
      bw.phipL_ = nullptr; bw.gradphipL_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phiL_;
      delete [] phipL_;

      delete [] gradphiL_;
      delete [] gradphipL_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFpLeft() const { return nDOFpL_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int neqnL,
                                                     ArrayQ<Ti> integrandPL[], const int neqnLP ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL ) const;

  protected:
    template<class Tq, class Tqp, class Tg, class Tgp, class Ti>
    void weightedIntegrand( const VectorX& nL,
        const ParamTL& paramL,
        const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& qpL,
        const VectorArrayQ<Tg>& gradqL, const VectorArrayQ<Tgp>& gradqpL,
        ArrayQ<Ti> integrandL[], const int neqnL,
        ArrayQ<Ti> integrandPL[], const int neqnPL,
        bool gradientOnly = false ) const;

    const PDE& pde_;
    const StabilizationNitsche& stab_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementQFieldL& qpfldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFpL_;
    mutable Real *phiL_;
    mutable Real *phipL_;
    mutable VectorX *gradphiL_;
    mutable VectorX *gradphipL_;

    const Real Cb_;
    const Real invL_;

    const bool isVMSD0_;

  };


  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL, class ElementParamL>
  class FieldWeighted
  {
  public:

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL   > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;

    typedef Element<Real, TopoDimCell, TopologyL> ElementEFieldL;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef typename ElementParamL::T ParamTL;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    FieldWeighted( const PDE& pde,
                   const StabilizationNitsche& stab,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter
                   const ElementQFieldL& qfldElemL, const ElementQFieldL& qpfldElemL,
                   const ElementQFieldL& wfldElemL, const ElementQFieldL& wpfldElemL,
                   const ElementEFieldL& efldElemL) :
                   pde_(pde), stab_(stab),
                   xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
                   xfldElemL_(get<-1>(paramfldElemL)),
                   qfldElemL_(qfldElemL), qpfldElemL_(qpfldElemL),
                   wfldElemL_(wfldElemL), wpfldElemL_(wpfldElemL),
                   efldElemL_(efldElemL),
                   paramfldElemL_( paramfldElemL ),
                   nDOFL_(qfldElemL_.nDOF()), nDOFpL_(qpfldElemL_.nDOF()),
                   nPhiL_(efldElemL_.nDOF()),
                   phiL_( new Real[nPhiL_] ),
                   gradphiL_( new VectorX[nPhiL_] ),
                   weightL_( new ArrayQ<T>[nPhiL_] ),
                   gradWeightL_( new VectorArrayQ<T>[nPhiL_] ),
                   weightpL_( new ArrayQ<T>[nPhiL_] ),
                   gradWeightpL_( new VectorArrayQ<T>[nPhiL_] ),
                   Cb_(stab_.getNitscheConstant(D)),
                   invL_(xfldElemTrace_.jacobianDeterminant() / xfldElemL_.jacobianDeterminant()),
                   isVMSD0_(qpfldElemL_.order() == 0)
    {
    }

    FieldWeighted( FieldWeighted&& fw ) :
                   pde_(fw.pde_), stab_(fw.stab_),
                   xfldElemTrace_(fw.xfldElemTrace_), canonicalTraceL_(fw.canonicalTraceL_), normalSign_(fw.normalSign_),
                   xfldElemL_(fw.xfldElemL_),
                   qfldElemL_(fw.qfldElemL_), qpfldElemL_(fw.qpfldElemL_),
                   wfldElemL_(fw.wfldElemL_), wpfldElemL_(fw.wpfldElemL_),
                   efldElemL_(fw.efldElemL_),
                   paramfldElemL_( fw.paramfldElemL_ ),
                   nDOFL_(fw.nDOFL_), nDOFpL_(fw.nDOFpL_),
                   nPhiL_(fw.nPhiL_),
                   phiL_( fw.phiL_ ),
                   gradphiL_( fw.gradphiL_ ),
                   weightL_( fw.weightL_ ),
                   gradWeightL_( fw.gradWeightL_ ),
                   weightpL_( fw.weightpL_ ),
                   gradWeightpL_( fw.gradWeightpL_ ),
                   Cb_(fw.Cb_),
                   invL_(fw.invL_),
                   isVMSD0_(fw.isVMSD0_)
    {
      fw.phiL_ = nullptr; fw.gradphiL_ = nullptr;
      fw.weightL_ = nullptr; fw.gradWeightL_ = nullptr;
      fw.weightpL_ = nullptr; fw.gradWeightpL_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phiL_;
      delete [] gradphiL_;
      delete [] weightL_;
      delete [] gradWeightL_;
      delete [] weightpL_;
      delete [] gradWeightpL_;
    }


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFpLeft() const { return nDOFpL_; }

    // element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, Ti integrandL[], const int nphiL ) const;

  protected:
    const PDE& pde_;
    const StabilizationNitsche& stab_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementQFieldL& qpfldElemL_;
    const ElementQFieldL& wfldElemL_;
    const ElementQFieldL& wpfldElemL_;
    const ElementEFieldL& efldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFpL_;
    const int nPhiL_;
    mutable Real *phiL_;
    mutable VectorX *gradphiL_;
    mutable ArrayQ<T> *weightL_;
    mutable VectorArrayQ<T> *gradWeightL_;
    mutable ArrayQ<T> *weightpL_;
    mutable VectorArrayQ<T> *gradWeightpL_;

    const Real Cb_;
    const Real invL_;
    const bool isVMSD0_;
  };

  //Factory function that returns the basis weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL, class ElementParamL >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL, ElementParamL>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qpfldElemL) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL, ElementParamL>(pde_, stab_, xfldElemTrace, canonicalTraceL, normalSign,
                                                                    paramfldElemL, qfldElemL, qpfldElemL);
  }

  //Factory function that returns the field weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL, class ElementParamL >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell, TopologyL, ElementParamL  >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qpfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& wfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& wpfldElemL,
            const Element<Real, TopoDimCell, TopologyL>& efldElemL) const
  {
    return FieldWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL, ElementParamL>(pde_, stab_, xfldElemTrace, canonicalTraceL, normalSign,
                                                                    paramfldElemL, qfldElemL, qpfldElemL,
                                                                    wfldElemL, wpfldElemL, efldElemL);
  }


protected:
  const PDE& pde_;
  const StabilizationNitsche& stab_;
  std::vector<int> interiorTraceGroups_;
  std::vector<int> periodicTraceGroups_;
  const bool isVMSD0_;
};


template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class ElementParamL >
bool
IntegrandTrace_VMSD<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}

template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class ElementParamL>
bool
IntegrandTrace_VMSD<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}


template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class ElementParamL>
template <class Ti>
void
IntegrandTrace_VMSD<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], const int neqnL,
                                                 ArrayQ<Ti> integrandPL[], const int neqnPL ) const
{
  SANS_ASSERT(neqnL == nDOFL_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> qL, qpL;                // solutions
  VectorArrayQ<T> gradqL, gradqpL;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)

  const bool needsSolutionGradient =
      (pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qpfldElemL_.evalBasis( sRefL, phipL_, nDOFpL_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qpfldElemL_.evalFromBasis( phipL_, nDOFpL_, qpL );
  if (needsSolutionGradient)
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );

  if (!isVMSD0_)
  {
    xfldElemL_.evalBasisGradient( sRefL, qpfldElemL_, gradphipL_, nDOFpL_ );

    if (needsSolutionGradient)
      qpfldElemL_.evalFromBasis( gradphipL_, nDOFpL_, gradqpL );
  }
  else
  {
    gradqpL = 0;
  }

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  nL *= normalSign_;

  // compute the integrand
  weightedIntegrand( nL, paramL,
                     qL, qpL,
                     gradqL, gradqpL,
                     integrandL, neqnL,
                     integrandPL, neqnPL );
}


//---------------------------------------------------------------------------//
template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class ElementParamL>
void
IntegrandTrace_VMSD<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace, JacobianElemInteriorTraceType& mtxElemL ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nDOF == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFp == nDOFpL_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> qL, qpL;                // solutions
  VectorArrayQ<T> gradqL, gradqpL;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)

  const bool needsSolutionGradient =
      (pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qpfldElemL_.evalBasis( sRefL, phipL_, nDOFpL_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qpfldElemL_.evalFromBasis( phipL_, nDOFpL_, qpL );

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  nL *= normalSign_;

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL, qpSurrealL;                // solution
  VectorArrayQ<SurrealClass> gradqSurrealL, gradqpSurrealL;  // gradient

  qSurrealL = qL;
  qpSurrealL = qpL;

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );

    gradqSurrealL = gradqL;
  }

  if (!isVMSD0_)
  {
    xfldElemL_.evalBasisGradient( sRefL, qpfldElemL_, gradphipL_, nDOFpL_ );
    qpfldElemL_.evalFromBasis( gradphipL_, nDOFpL_, gradqpL );
    gradqpSurrealL = gradqpL;
  }
  else
  {
    gradqpL = 0;
  }

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0; // temporary storage
  MatrixQ<Real> PDE_qp = 0, PDE_gradqp = 0; // temporary storage
  MatrixQ<Real> PDEp_q = 0, PDEp_gradq = 0; // temporary storage
  MatrixQ<Real> PDEp_qp = 0, PDEp_gradqp = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurrealP( nDOFpL_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

//    integrandLSurreal = 0; //this happens inside
//    integrandLSurrealP = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL,
                       qSurrealL, qpL, gradqL, gradqpL,
                       integrandLSurreal.data(), nDOFL_,
                       integrandLSurrealP.data(), nDOFpL_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_q(i,j) += dJ*phiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFpL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDEp_q,m,n) = DLA::index(integrandLSurrealP[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDEp_q(i,j) += dJ*phiL_[j]*PDEp_q;
      }

    } // if slot
    slot += PDE::N;

  } // nchunk

  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

//      integrandLSurreal = 0; //this happens inside
//      integrandLSurrealP = 0;

      // compute the integrand
      weightedIntegrand( nL, paramL,
                         qL, qpL, gradqSurrealL, gradqpL,
                         integrandLSurreal.data(), nDOFL_,
                         integrandLSurrealP.data(), nDOFpL_ );

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDE_q(i,j) += dJ*gradphiL_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFpL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDEp_gradq,m,n) = DLA::index(integrandLSurrealP[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDEp_q(i,j) += dJ*gradphiL_[j][d]*PDEp_gradq;
          }

        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient

  //derivatives w.r.t to q'
  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qpSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

//    integrandLSurreal = 0; //this happens inside
//    integrandLSurrealP = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL,
                       qL, qpSurrealL, gradqL, gradqpL,
                       integrandLSurreal.data(), nDOFL_,
                       integrandLSurrealP.data(), nDOFpL_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qpSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_qp,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFpL_; j++)
          mtxElemL.PDE_qp(i,j) += dJ*phipL_[j]*PDE_qp;
      }


      for (int i = 0; i < nDOFpL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDEp_qp,m,n) = DLA::index(integrandLSurrealP[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFpL_; j++)
          mtxElemL.PDEp_qp(i,j) += dJ*phipL_[j]*PDEp_qp;
      }

    } // if slot
    slot += PDE::N;

  } // nchunk


  // loop over derivative chunks to computes derivatives w.r.t gradqp
  if (needsSolutionGradient && (!isVMSD0_))
  {
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqpSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

//      integrandLSurreal = 0; // this happens inside
//      integrandLSurrealP = 0;

      // compute the integrand
      weightedIntegrand( nL, paramL,
                         qL, qpL, gradqL, gradqpSurrealL,
                         integrandLSurreal.data(), nDOFL_,
                         integrandLSurrealP.data(), nDOFpL_, true );

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqpSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradqp,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFpL_; j++)
              mtxElemL.PDE_qp(i,j) += dJ*gradphipL_[j][d]*PDE_gradqp;
          }

          for (int i = 0; i < nDOFpL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDEp_gradqp,m,n) = DLA::index(integrandLSurrealP[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFpL_; j++)
              mtxElemL.PDEp_qp(i,j) += dJ*gradphipL_[j][d]*PDEp_gradqp;
          }

        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient

}

template<class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class ElementParamL>
template<class Tq, class Tqp, class Tg, class Tgp, class Ti>
void
IntegrandTrace_VMSD<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
weightedIntegrand( const VectorX& nL, const ParamTL& paramL,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& qpL,
                   const VectorArrayQ<Tg>& gradqL, const VectorArrayQ<Tgp>& gradqpL,
                   ArrayQ<Ti> integrandL[], const int neqnL,
                   ArrayQ<Ti> integrandPL[], const int neqnPL,
                   bool gradientOnly ) const
{
  typedef typename promote_Surreal<Tq,Tqp>::type TqS;
  typedef typename promote_Surreal<Tq,Tg>::type TqgS;

  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnPL == nDOFpL_);

  //zero integrands
  for (int k = 0; k < neqnL; k++) integrandL[k] = 0;
  for (int k = 0; k < neqnPL; k++) integrandPL[k] = 0;

  //NO R1 viscous flux

  // R2 fluxes: DUAL CONSISTENCY TERM

  //convert from qpL, gradqpL  to du, gradu
  ArrayQ<TqS>  du = 0;
  pde_.perturbedMasterState( paramL, qL, qpL, du );

  DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<TqgS>> K = 0;  // diffusion matrix
  pde_.diffusionViscous( paramL, qL, gradqL, K ); // boundary state, interior gradient

  VectorArrayQ<TqS> duN = 0;
  for (int i=0; i<PhysDim::D; i++)
    duN[i] -= du*nL[i];  //negate because perturbedGradFluxViscous returns -K*duN

  typedef typename promote_Surreal<TqgS,TqS>::type T3;
  VectorArrayQ<T3> Kndu = 0;
  pde_.perturbedGradFluxViscous(paramL, K, duN, Kndu);

  // viscous flux dual consistency term
  for (int k = 0; k < neqnL; k++)
    integrandL[k] -= dot(gradphiL_[k],Kndu);

  // R3 term: <v', -K.grad u>
  ArrayQ<Ti> FnL  = 0; // normal advective flux

  VectorArrayQ<Tq> FA  = 0; // normal advective flux
  pde_.fluxAdvective( paramL, qL, FA );
  FnL += dot(nL,FA);

  MatrixQ<Tq> dFdu = 0;
  pde_.jacobianFluxAdvectiveAbsoluteValue(paramL, qL, nL, dFdu);
  FnL += dFdu*du;

  VectorArrayQ<TqgS> FL = 0;
  pde_.fluxViscous( paramL, qL, gradqL, FL );
  FnL += dot(nL,FL);

  //R4 TERMS:

  //UPWINDING

  //VISCOUS FLUX

  if (!isVMSD0_)
  {
    typedef typename promote_Surreal<Tq,Tgp>::type T4;
    VectorArrayQ<T4> dgradu = 0;
    for (int d=0; d<PDE::D; d++)
      pde_.perturbedMasterState(paramL, qL, gradqpL[d], dgradu[d]);

    typedef typename promote_Surreal<TqgS,T4>::type T5;
    VectorArrayQ<T5> dFv = 0;
    pde_.perturbedGradFluxViscous(paramL, K, dgradu, dFv);
    FnL += dot(nL,dFv);
  }

  //SIP STABILIZATION
  ArrayQ<T3> nKndu = dot(nL, Kndu);

  if (!isVMSD0_)
    FnL += Cb_*invL_*nKndu;
  else
    FnL += 2.*invL_*nKndu;


  for (int k = 0; k < neqnPL; k++)
    integrandPL[k] +=  phipL_[k]*FnL;

  if (!isVMSD0_)
  {
    //DUAL CONSISTENCY
    for (int k = 0; k < neqnPL; k++)
      integrandPL[k] -= dot(gradphipL_[k],Kndu);
  }

}


template<class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class ElementParamL>
template <class Ti>
void
IntegrandTrace_VMSD<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, ElementParamL>::
operator()(const QuadPointTraceType& sRefTrace, Ti integrandL[], const int nphiL ) const
{
  SANS_ASSERT( nphiL == nPhiL_ );

  ParamTL paramL;   // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;       // unit normal for left element (points to right element)

  ArrayQ<T> qL = 0, qpL = 0;                // solution
  VectorArrayQ<T> gradqL = 0, gradqpL = 0;  // solution gradients

  ArrayQ<T> wL = 0, wpL = 0;                // weight
  VectorArrayQ<T> gradwL = 0, gradwpL = 0;  // weight gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  nL *= normalSign_;

  // solution value, gradient
  qfldElemL_.eval( sRefL, qL );
  qpfldElemL_.eval( sRefL, qpL );

  if (pde_.hasFluxViscous() || pde_.hasSourceTrace())
  {
    xfldElemL_.evalGradient( sRefL, qfldElemL_, gradqL );
    xfldElemL_.evalGradient( sRefL, qpfldElemL_, gradqpL );
  }

  // weight value,
  wfldElemL_.eval( sRefL, wL );
  wpfldElemL_.eval( sRefL, wpL );

  xfldElemL_.evalGradient( sRefL, wfldElemL_, gradwL );

  // estimate basis, gradient
  efldElemL_.evalBasis( sRefL, phiL_, nPhiL_);
  xfldElemL_.evalBasisGradient( sRefL, efldElemL_, gradphiL_, nPhiL_ );

  if (!isVMSD0_)
    xfldElemL_.evalGradient( sRefL, wpfldElemL_, gradwpL );

  // Grad (w phi) needed
  for (int k = 0; k < nPhiL_; k++)
  {
    integrandL[k] = 0;
    gradWeightL_[k] = gradphiL_[k]*wL + phiL_[k]*gradwL;

    weightpL_[k] = phiL_[k]*wpL;
    gradWeightpL_[k] = gradphiL_[k]*wpL + phiL_[k]*gradwpL;
  }



  //ASSEMBLE FLUXES

  ArrayQ<T> du = 0;
  pde_.perturbedMasterState( paramL, qL, qpL, du );

  DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<T>> K = 0;  // diffusion matrix
  pde_.diffusionViscous( paramL, qL, gradqL, K ); // boundary state, interior gradient

  //TMP FUNCTIONS FOR DUAL CONSISTENCY.. WILL USE FOR BOTH INTEGRANDS
  VectorArrayQ<T> duN = 0;
  for (int i=0; i<PhysDim::D; i++)
    duN[i] -= du*nL[i]; //negate because perturbedGradFluxViscous returns -K*duN

  VectorArrayQ<T> Kndu = 0;
  pde_.perturbedGradFluxViscous(paramL, K, duN, Kndu);

  // R1 TERMS: EMPTY

  // R2 TERMS: DUAL CONSISTENCY TERM
  // viscous flux dual consistency term
  for (int k = 0; k < nPhiL_; k++)
    integrandL[k] -= dot(gradWeightL_[k],Kndu);

  //R3 TERMS: coarse flux weighted with fine basis functions

  // R3 term: <v', -K.grad u>
  ArrayQ<T> FnL  = 0; // normal advective flux

  VectorArrayQ<T> FA  = 0; // normal advective flux
  pde_.fluxAdvective( paramL, qL, FA );
  FnL += dot(nL,FA);

  MatrixQ<T> dFdu = 0;
  pde_.jacobianFluxAdvectiveAbsoluteValue(paramL, qL, nL, dFdu);
  FnL += dFdu*du;

  VectorArrayQ<Ti> FL = 0;
  pde_.fluxViscous( paramL, qL, gradqL, FL );
  FnL += dot(nL,FL);

  for (int k = 0; k < nPhiL_; k++)
    integrandL[k] += dot(weightpL_[k],FnL);

  ArrayQ<Ti> dF = 0;
  //VISCOUS FLUX

  if (!isVMSD0_)
  {
    VectorArrayQ<Ti> dgradu = 0;
    for (int d=0; d<PDE::D; d++)
      pde_.perturbedMasterState(paramL, qL, gradqpL[d], dgradu[d]);

    VectorArrayQ<Ti> dFv = 0;
    pde_.perturbedGradFluxViscous(paramL, K, dgradu, dFv);
    dF += dot(nL, dFv); //dF = -K*dgradu
  }

  //SIP STABILIZATION
  ArrayQ<Ti> nKndu = dot(nL, Kndu);

  if (!isVMSD0_)
    dF += Cb_*invL_*nKndu;
  else
    dF += 2.*invL_*nKndu;


  for (int k = 0; k < nPhiL_; k++)
    integrandL[k] +=  dot(weightpL_[k],dF);

  if (!isVMSD0_)
  {
    //DUAL CONSISTENCY
    for (int k = 0; k < nPhiL_; k++)
      integrandL[k] -= dot(gradWeightpL_[k],Kndu);
  }

}

} // namespace SANS
#endif //INTEGRANDTRACE_VMSD_H
