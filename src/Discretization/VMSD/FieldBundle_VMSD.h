// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDBUNDLE_VMSD_H_
#define FIELDBUNDLE_VMSD_H_

#include "Field/Field.h"
#include "Field/FieldTypes.h"
#include "Field/Local/Field_Local.h"
#include "Field/EmbeddedCGType.h"
#include "Topology/Dimension.h"

#include "tools/make_unique.h"

#include "tools/split_cat_std_vector.h" // SANS::cat in derived constructors

namespace SANS
{

//-----------------------------------------------//
// Stabilized Galerkin
//-----------------------------------------------//

template< class PhysD, class TopoD, class ArrayQ_>
struct FieldBundleBase_VMSD
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;

  FieldBundleBase_VMSD( const XField<PhysDim, TopoDim>& xfld,
                            Field_CG_Cell< PhysDim, TopoDim, ArrayQ >& qfld,
                            Field_EG_Cell< PhysDim, TopoDim, ArrayQ >& qpfld,
                            Field_CG_BoundaryTrace< PhysDim, TopoDim, ArrayQ >& lgfld,
                            std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld,
                            const int order, const int orderp,
                            const BasisFunctionCategory basis_cgcell,
                            const BasisFunctionCategory basis_dgcell,
                            const BasisFunctionCategory basis_trace ) :
  xfld(xfld), qfld(qfld), qpfld(qpfld), lgfld(lgfld), up_resfld(up_resfld), order(order), orderp(orderp),
  basis_cgcell(basis_cgcell), basis_dgcell(basis_dgcell), basis_trace(basis_trace) {}

  void projectTo(FieldBundleBase_VMSD& bundleTo)
  {
    qfld.projectTo(bundleTo.qfld);
    qpfld.projectTo(bundleTo.qpfld);
    lgfld.projectTo(bundleTo.lgfld);
  }

  const XField<PhysDim,TopoDim>& xfld;

  Field_CG_Cell<PhysDim,TopoDim,ArrayQ>& qfld;
  Field_EG_Cell<PhysDim,TopoDim,ArrayQ>& qpfld;
  Field_CG_BoundaryTrace<PhysDim, TopoDim, ArrayQ>& lgfld;

  // a pointer to the residual field used for estimation and local solves later
  // This is a reference because the pointer is owned by the derived class (below)
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld;

  const int order;
  const int orderp;

  const BasisFunctionCategory basis_cgcell;
  const BasisFunctionCategory basis_dgcell;
  const BasisFunctionCategory basis_trace;

  static constexpr SpaceType spaceType = SpaceType::Continuous;

//  SpaceType spaceType() { return SpaceType::Continuous; }

  void dump() { qfld.dump(); }
};

// Forward declare so input args for local work
template< class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_VMSD;

// public of GlobalBundle is so that AlgebraicEquationSet Basetype works with it
template<class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_VMSD_Local :
    public FieldBundleBase_VMSD<PhysD, TopoD, ArrayQ_>
{
  typedef FieldBundleBase_VMSD<PhysD, TopoD, ArrayQ_> BaseType;

  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;

  typedef Field_CG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef Field_EG_Cell<PhysDim, TopoDim, ArrayQ> QPFieldType;
  typedef Field_CG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef Field_Local<QFieldType> QFieldType_Local;
  typedef Field_Local<QPFieldType> QPFieldType_Local;
  typedef Field_Local<LGFieldType> LGFieldType_Local;

  // If the local patch is WHOLEPATCH or INNERPATCH, then do not use broken field!
#if defined(WHOLEPATCH) || defined(INNERPATCH)

  // active_local_BGroup_list is a vec of vec so that boundaries that came from the same original global trace
  // continue to have shared dofs in the local field.
  FieldBundle_VMSD_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_VMSD<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<std::vector<int>>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_,qpfld_, lgfld_, up_resfld_, order, globalfields.orderp,
    globalfields.basis_cgcell, globalfields.basis_dgcell, globalfields.basis_trace),
    qfld_( xfld_local, globalfields.qfld, order, globalfields.basis_cgcell ),
    qpfld_( qfld_, globalfields.qpfld, globalfields.orderp, globalfields.basis_dgcell ),
    lgfld_( active_local_BGroup_list, xfld_local, globalfields.lgfld, order, globalfields.basis_trace ) // Broken Groups
  { }

  // Breaks up the groups in-situ
  FieldBundle_VMSD_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_VMSD<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<int>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_, qpfld_, lgfld_, up_resfld_, order, globalfields.orderp,
    globalfields.basis_cgcell, globalfields.basis_dgcell, globalfields.basis_trace),
    qfld_( xfld_local, globalfields.qfld, order, globalfields.basis_cgcell ),
    qpfld_( qfld_, globalfields.qpfld, globalfields.orderp, globalfields.basis_dgcell ),
    lgfld_( xfld_local, globalfields.lgfld, order, globalfields.basis_trace, active_local_BGroup_list ) // Unbroken Groups
  { }
#else
  // active_local_BGroup_list is a vec of vec so that boundaries that came from the same original global trace
  // continue to have shared dofs in the local field.
  FieldBundle_VMSD_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_VMSD<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<std::vector<int>>& active_local_BGroup_list  )
  : BaseType(xfld_local,qfld_,qpfld_, lgfld_, up_resfld_,order, globalfields.orderp,
    globalfields.basis_cgcell, globalfields.basis_dgcell, globalfields.basis_trace),
    qfld_(  SANS::split(xfld_local.getReSolveCellGroups()), xfld_local, globalfields.qfld, order, globalfields.basis_cgcell ), // broken groups
    qpfld_( qfld_, globalfields.qpfld, globalfields.orderp, globalfields.basis_dgcell ),
    lgfld_( active_local_BGroup_list, xfld_local, globalfields.lgfld, order, globalfields.basis_trace ) // Broken Groups
  {
    SANS_ASSERT_MSG( active_local_BGroup_list.empty(), "Local Patches with Lagrange multipliers not supported");

    if (globalfields.up_resfld) // if there is a residual field in the global field bundle
    {
      // create a new local residual field and transfer the global residual field onto it
      up_resfld_ = SANS::make_unique<Field_Local<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>>
        ( xfld_local, *globalfields.up_resfld, order, globalfields.basis_cgcell );
    }
  }

  // Breaks up the groups in-situ
  FieldBundle_VMSD_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                             const FieldBundle_VMSD<PhysDim, TopoDim, ArrayQ>& globalfields,
                             const int order, const std::vector<int>& active_local_BGroup_list  )
  : BaseType(xfld_local, qfld_, qpfld_, lgfld_, up_resfld_, order, globalfields.orderp,
    globalfields.basis_cgcell, globalfields.basis_dgcell, globalfields.basis_trace),
    qfld_(  SANS::split(xfld_local.getReSolveCellGroups()), xfld_local, globalfields.qfld, order, globalfields.basis_cgcell ), // broken groups
    qpfld_( qfld_, globalfields.qpfld, globalfields.orderp, globalfields.basis_dgcell ),
    lgfld_( xfld_local, globalfields.lgfld, order, globalfields.basis_trace, active_local_BGroup_list ) // Unbroken Groups
  {
    SANS_ASSERT_MSG( active_local_BGroup_list.empty(), "Local Patches with Lagrange multipliers not supported");

    if (globalfields.up_resfld) // if there is a residual field in the global field bundle
    {
      // transfer the residual field
      up_resfld_ = SANS::make_unique<Field_Local<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>>
        ( xfld_local, *globalfields.up_resfld, order, globalfields.basis_cgcell );
    }
  }
#endif
  using BaseType::order;
  using BaseType::orderp;

  using BaseType::basis_cgcell;
  using BaseType::basis_dgcell;
  using BaseType::basis_trace;

protected:
  QFieldType_Local qfld_;
  QPFieldType_Local qpfld_;
  LGFieldType_Local lgfld_;
  // pointer to a resfld, this is an actual field rather than a local. It is declared then with a local version above
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>> up_resfld_;
};

template<class PhysD, class TopoD, class ArrayQ_>
struct FieldBundle_VMSD :
    public FieldBundleBase_VMSD<PhysD, TopoD, ArrayQ_>
{
  typedef FieldBundleBase_VMSD<PhysD, TopoD, ArrayQ_> BaseType;

  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;

  typedef Field_CG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef Field_EG_Cell<PhysDim, TopoDim, ArrayQ> QPFieldType;
  typedef Field_CG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef FieldBundle_VMSD_Local<PhysD,TopoD,ArrayQ> FieldBundle_Local;

  FieldBundle_VMSD( const XField<PhysDim, TopoDim>& xfld, const int order, const int orderp,
                        const BasisFunctionCategory basis_cgcell, const BasisFunctionCategory basis_dgcell, const BasisFunctionCategory basis_trace,
                        const std::vector<int>& active_BGroup_list )
  : BaseType(xfld, qfld_, qpfld_, lgfld_, up_resfld_, order, orderp,
    basis_cgcell, basis_dgcell, basis_trace),
    qfld_( xfld, order, basis_cgcell, EmbeddedCGField ),
    qpfld_( qfld_, orderp, basis_dgcell ),
    lgfld_( xfld, order, basis_trace, active_BGroup_list ) // Unbroken groups
  {
    // the up_resfld_ isn't declared here, it will be filled in the SolverInterface class.
  }

  // A pseudo copy constructor
  FieldBundle_VMSD( const XField<PhysDim,TopoDim>&xfld, const int order,
                   const FieldBundleBase_VMSD<PhysDim,TopoDim,ArrayQ>& flds,
                   const std::vector<int>& active_BGroup_list )
  : BaseType(xfld, qfld_, qpfld_, lgfld_, up_resfld_, order, order,
    flds.basis_cgcell, flds.basis_dgcell, flds.basis_trace),
    qfld_( xfld, order, flds.basis_cgcell, EmbeddedCGField),
    qpfld_( qfld_, order, flds.basis_dgcell ),
    lgfld_( xfld, order, flds.basis_trace, active_BGroup_list)
  {
  }

  // A pseudo copy constructor
  FieldBundle_VMSD( const XField<PhysDim,TopoDim>&xfld, const int order, const int orderp,
                   const BaseType& flds,
                   const std::vector<int>& active_BGroup_list )
  : BaseType(xfld, qfld_, qpfld_, lgfld_, up_resfld_, order, orderp,
    flds.basis_cgcell, flds.basis_dgcell, flds.basis_trace),
    qfld_( xfld, order, flds.basis_cgcell, EmbeddedCGField),
    qpfld_( qfld_, orderp, flds.basis_dgcell ),
    lgfld_( xfld, order, flds.basis_trace, active_BGroup_list)
  {
  }

  using BaseType::order;
  using BaseType::orderp;

  using BaseType::basis_cgcell;
  using BaseType::basis_dgcell;
  using BaseType::basis_trace;

protected:
  QFieldType qfld_;
  QPFieldType qpfld_;
  LGFieldType lgfld_;

  // pointer to the Residual field
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>> up_resfld_;
};



}

#endif /* FIELDBUNDLE_VMSD_H_ */
