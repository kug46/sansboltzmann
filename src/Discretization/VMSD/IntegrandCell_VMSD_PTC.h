// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_VMSD_PTC_H
#define INTEGRANDCELL_VMSD_PTC_H

// cell integrand operators: pseudo-time continuation (PTC)

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "Field/Element/Element.h"
#include "Field/Element/ElementSequence.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"
#include "JacobianCell_VMSD_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: PTC
//
// integrandPDE = phi*( U_{n} - U_{n-1} )/dt
//
// where
//   phi              basis function
//   U_n(x,y)         conservative solution vector at timestep n
//   dt               is computed from a CFL number

template <class PDE>
class IntegrandCell_VMSD_PTC : public IntegrandCellType< IntegrandCell_VMSD_PTC<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobian

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_VMSD_PTC( const PDE& pde, const std::vector<int>& CellGroups) :
    pde_(pde), cellGroups_(CellGroups)
  {
    // PTC only applies to PDE class with conservative flux defined, otherwise there will be no time-dependent term
    SANS_ASSERT(pde_.hasFluxAdvectiveTime());
  }

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<Real, TopoDim, Topology> ElementTFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementSequence<ArrayQ<Real>, TopoDim, Topology> ElementQFieldVecType;

    typedef typename ElementXFieldType::RefCoordType RefCoordType;
    typedef typename ElementXFieldType::VectorX VectorX;

    typedef typename ElementParam::ElementTypeL ElementTypeL;
    typedef typename ElementTypeL::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef JacobianElemCell_VMSD<PhysDim,MatrixQ<Real>> JacobianElemCellType;

    template<class Z>
    using IntegrandType = ArrayQ<Z>;

    BasisWeighted( const PDE& pde,
                   const ElementParam& paramfldElem, // assume paramfldElem is an ElementTuple like (..., fld, xfld, dtifld)
                   const ElementQFieldType& qfldElem,
                   const ElementQFieldVecType& qfldElemPast,
                   const ElementQFieldType& qpfldElem,
                   const ElementQFieldVecType& qpfldElemPast) :
      pde_(pde),
      paramfldElem_(paramfldElem.left()), // Remove the inverse time step field; remaining element = (..., fld, xfld)
      dtifldElem_(get<-1>(paramfldElem)), // dtiField must be the last parameter
      xfldElem_(get<-2>(paramfldElem)), // XField must be the 2nd to last parameter
      qfldElem_(qfldElem), qfldElemPast_(qfldElemPast),
      qpfldElem_(qpfldElem), qpfldElemPast_(qpfldElemPast),
      nDOF_(qfldElem_.nDOF() ),
      phi_( new Real[nDOF_] ),
      nDOFp_(qpfldElem_.nDOF() ),
      phip_( new Real[nDOFp_] ),
      basisEqual_((qfldElem_.basis()->category() == qpfldElem_.basis()->category()) &&
                  (qfldElem_.order() == qpfldElem_.order() ))
    {
      SANS_ASSERT( qfldElemPast_.nElem() == 1 ); //TODO: it's hard coded to take 1 paramfldElem here instead of a sequence
    }

    BasisWeighted( BasisWeighted&& bw ) :
      pde_(bw.pde_),
      paramfldElem_(bw.paramfldElem_),
      dtifldElem_(bw.dtifldElem_),
      xfldElem_(bw.xfldElem_),
      qfldElem_(bw.qfldElem_), qfldElemPast_(bw.qfldElemPast_),
      qpfldElem_(bw.qpfldElem_), qpfldElemPast_(bw.qpfldElemPast_),
      nDOF_(bw.nDOF_ ),
      phi_( bw.phi_ ),
      nDOFp_(bw.nDOFp_ ),
      phip_( bw.phip_ ),
      basisEqual_(bw.basisEqual_)
    {
      bw.phi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] phip_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, IntegrandType<Ti> integrand[], int neqn,
                                                IntegrandType<Ti> integrandp[], int neqnp) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxPDEElem ) const;

  protected:
    template<class Tq, class Ti>
    void weightedIntegrand( const ParamT& param,
                            const Real dti,
                            const ArrayQ<Tq>& qTot,
                            const ArrayQ<Real>& ftPastTot,
                            ArrayQ<Ti> integrand[], int neqn,
                            ArrayQ<Ti> integrandp[], int neqnp ) const;

    const PDE& pde_;
    const ElementTypeL& paramfldElem_;
    const ElementTFieldType& dtifldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldVecType& qfldElemPast_;
    const ElementQFieldType& qpfldElem_;
    const ElementQFieldVecType& qpfldElemPast_;

    const int nDOF_;
    mutable Real *phi_;

    const int nDOFp_;
    mutable Real *phip_;

    const bool basisEqual_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const ElementSequence<ArrayQ<Real>, TopoDim, Topology>& qfldElemPast,
            const Element<ArrayQ<T>, TopoDim, Topology>& qpfldElem,
            const ElementSequence<ArrayQ<Real>, TopoDim, Topology>& qpfldElemPast) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem,
                                                             qfldElem, qfldElemPast, qpfldElem, qpfldElemPast);
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_VMSD_PTC<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_VMSD_PTC<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()( const QuadPointType& sRef, IntegrandType<Ti> integrand[], int neqn, IntegrandType<Ti> integrandp[], int neqnp ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  Real dti = 0;                    // inverse time step

  ArrayQ<T> q = 0;                 // solution
  ArrayQ<Real> qPast = 0;

  ArrayQ<T> qp = 0;
  ArrayQ<T> qTot = 0;    // solution
  ArrayQ<Real> qpPast = 0;
  ArrayQ<Real> qPastTot = 0;
  ArrayQ<Real> ftPastTot = 0;   // previous solution

  // evaluate basis fcn
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  if (basisEqual_)
  {
    for (int k=0; k<nDOF_; k++)
      phip_[k] = phi_[k];
  }
  else
  {
    qpfldElem_.evalBasis( sRef, phip_, nDOFp_ );
  }

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // get the inverse time step
  dtifldElem_.eval( sRef, dti );

  // evaluate solution at different timesteps
  qfldElem_.evalFromBasis( phi_, nDOF_, q);
  qfldElemPast_[0].evalFromBasis( phi_, nDOF_, qPast );

  qpfldElem_.evalFromBasis( phip_, nDOFp_, qp);
  qpfldElemPast_[0].evalFromBasis( phip_, nDOFp_, qpPast );

  qTot = q + qp;
  qPastTot = qPast + qpPast;

  pde_.fluxAdvectiveTime(param, qPastTot, ftPastTot); // TODO: IBL needs paramPast as well

  // compute the residual
  weightedIntegrand( param, dti, qTot, ftPastTot, integrand, neqn, integrandp, neqnp);
}


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_VMSD_PTC<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxElem.nDOF == nDOF_);
  SANS_ASSERT(mtxElem.nDOFp == nDOFp_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  Real dti;                    // inverse time step

  ArrayQ<Real> q, qp, qTot;                    // solution
  ArrayQ<Real> qPast, qpPast, qtotPast;   // previous solution
  ArrayQ<Real> ftPastTot = 0;   // previous solution

  ArrayQ<SurrealClass> qTotSurreal = 0; // solution

  MatrixQ<Real> PDE_q = 0; // temporary storage
  MatrixQ<Real> PDEp_q = 0; // temporary storage

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  if (basisEqual_)
  {
    for (int k=0; k<nDOF_; k++)
      phip_[k] = phi_[k];
  }
  else
  {
    qpfldElem_.evalBasis( sRef, phip_, nDOFp_ );
  }

  // evaluate solution at different timesteps
  qfldElem_.evalFromBasis( phi_, nDOF_, q);
  qfldElemPast_[0].evalFromBasis( phi_, nDOF_, qPast );

  // evaluate solution at different timesteps
  qpfldElem_.evalFromBasis( phip_, nDOFp_, qp);
  qpfldElemPast_[0].evalFromBasis( phip_, nDOFp_, qpPast );

  qTot = q + qp;
  qtotPast = qPast + qpPast;

  qTotSurreal = qTot;

  // get the inverse time step
  dtifldElem_.eval( sRef, dti );

  // convert the previous time step to conservative variables
  pde_.fluxAdvectiveTime(param, qtotPast, ftPastTot);

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandpSurreal( nDOFp_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qTotSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;
    integrandpSurreal = 0;

    weightedIntegrand( param, dti, qTotSurreal, ftPastTot, integrandSurreal.data(), integrandSurreal.size(),
                                                           integrandpSurreal.data(), integrandpSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qTotSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxElem.PDE_q(i,j) += dJ*phi_[j]*PDE_q;

        for (int j = 0; j < nDOFp_; j++)
          mtxElem.PDE_qp(i,j) += dJ*phip_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFp_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDEp_q,m,n) = DLA::index(integrandpSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxElem.PDEp_q(i,j) += dJ*phi_[j]*PDEp_q;

        for (int j = 0; j < nDOFp_; j++)
          mtxElem.PDEp_qp(i,j) += dJ*phip_[j]*PDEp_q;
      }
    }
    slot += PDE::N;
  } // nchunk

}

// Cell integrand
//
// integrand = phi*du/dt
//
// where
//   phi                basis function
//   U_n(x,y)           conservative solution vector at timestep n
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Tq, class Ti>
void
IntegrandCell_VMSD_PTC<PDE>::BasisWeighted<T,TopoDim,Topology,ElementParam>::
weightedIntegrand( const ParamT& param,
                   const Real dti,
                   const ArrayQ<Tq>& qTot,
                   const ArrayQ<Real>& ftPastTot,
                   ArrayQ<Ti> integrand[], int neqn,
                   ArrayQ<Ti> integrandp[], int neqnp  ) const
{
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  for (int k = 0; k < neqnp; k++)
    integrandp[k] = 0;

  ArrayQ<Tq> ftTot = 0;
  pde_.fluxAdvectiveTime(param, qTot, ftTot);

  // PTC backward Euler term
  ArrayQ<Tq> dudt = (ftTot - ftPastTot)*dti;

  for (int k=0; k < neqn; k++)
    integrand[k] += phi_[k]*dudt;

  for (int k=0; k < neqnp; k++)
    integrandp[k] += phip_[k]*dudt;
}

}

#endif  // INTEGRANDCELL_VMSD_PTC_H
