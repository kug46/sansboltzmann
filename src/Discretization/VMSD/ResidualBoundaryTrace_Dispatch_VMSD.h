// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_DISPATCH_VMSD_H
#define RESIDUALBOUNDARYTRACE_DISPATCH_VMSD_H

// boundary-trace integral residual functions

#include "ComputePerimeter.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "ResidualBoundaryTrace_VMSD.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
class ResidualBoundaryTrace_Dispatch_VMSD_impl
{
public:
  ResidualBoundaryTrace_Dispatch_VMSD_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQR>& rsdPDEGlobal)
    : xfld_(xfld), qfld_(qfld), qpfld_(qpfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ResidualBoundaryTrace_VMSD(fcn, rsdPDEGlobal_), xfld_, (qfld_, qpfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQR>& rsdPDEGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
ResidualBoundaryTrace_Dispatch_VMSD_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, ArrayQR>
ResidualBoundaryTrace_Dispatch_VMSD(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                             const int* quadratureorder, int ngroup,
                                             Vector<ArrayQR>& rsdPDEGlobal)
{
  static_assert( DLA::VectorSize<ArrayQ>::M == DLA::VectorSize<ArrayQR>::M, "These should be the same size.");

  return { xfld, qfld, qpfld, quadratureorder, ngroup, rsdPDEGlobal };
}

}

#endif //RESIDUALBOUNDARYTRACE_DISPATCH_VMSD_H
