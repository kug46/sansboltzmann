// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTINOALBOUNDARYTRACE_DISPATCH_VMSD_H
#define FUNCTINOALBOUNDARYTRACE_DISPATCH_VMSD_H

// boundary-trace integral residual functions


#include "FunctionalBoundaryTrace_WeightedResidual_VMSD.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class ArrayJ>
class FunctionalBoundaryTrace_Dispatch_VMSD_impl
{
public:
  FunctionalBoundaryTrace_Dispatch_VMSD_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const int* quadratureorder, int ngroup,
      ArrayJ& functional )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), qpfld_(qpfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      functional_(functional)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
      FunctionalBoundaryTrace_WeightedResidual_VMSD( fcnOutput_, fcn.cast(), functional_ ),
      xfld_, (qfld_, qpfld_), quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const int* quadratureorder_;
  const int ngroup_;
  ArrayJ& functional_;
};

// Factory function

template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class ArrayJ>
FunctionalBoundaryTrace_Dispatch_VMSD_impl<FunctionalIntegrand, XFieldType, PhysDim, TopoDim, ArrayQ, ArrayJ>
FunctionalBoundaryTrace_Dispatch_VMSD(const FunctionalIntegrand& fcnOutput,
                                          const FieldType<XFieldType>& xfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                          const int* quadratureorder, int ngroup,
                                          ArrayJ& functional )
{
  return {fcnOutput, xfld.cast(), qfld, qpfld, quadratureorder, ngroup, functional};
}


}

#endif //FUNCTINOALBOUNDARYTRACE_DISPATCH_VMSD_H
