// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALINTERIORTRACE_VMSD_H
#define RESIDUALINTERIORTRACE_VMSD_H

// HDG interior-trace integral residual functions

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Field/FieldData/FieldDataReal_Cell.h"
#include "tools/Tuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class IntegrandInteriorTrace>
class ResidualInteriorTrace_VMSD_impl :
    public GroupIntegralInteriorTraceType< ResidualInteriorTrace_VMSD_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualInteriorTrace_VMSD_impl( const IntegrandInteriorTrace& fcn,
                                  const std::map<int,std::vector<int>>& cellITraceGroups,
                                  const int& cellgroup, const int& cellelem,
                                  DLA::VectorD<ArrayQ>& rsdPDEElemCell,
                                  DLA::VectorD<ArrayQ>& rsdPDEpElemCell) :
    fcn_(fcn), cellITraceGroups_(cellITraceGroups),
    cellgroup_(cellgroup), cellelem_(cellelem),
    rsdPDEElemCell_(rsdPDEElemCell), rsdPDEpElemCell_(rsdPDEpElemCell)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {

    //CHECKS NOT APPLICABLE HERE

//    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
//    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
//    SANS_ASSERT( rsdPDEElemCell_.m() == qfld.nDOFpossessed() );
//    SANS_ASSERT( rsdPDEpElemCell_.m() == qpfld.nDOFpossessed() );

  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const QFieldCellGroupTypeR& qpfldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementQFieldClassR qpfldElemR( qpfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFpL = qpfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFpR = qpfldElemR.nDOF();

    const int nIntegrandL = nDOFL;
    const int nIntegrandPL = nDOFpL;
    const int nIntegrandR = nDOFR;
    const int nIntegrandPR = nDOFpR;

    // trace element integral
    typedef ArrayQ CellIntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, CellIntegrandType>
      integralTraceL(quadratureorder, nIntegrandL, nIntegrandPL);

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, CellIntegrandType>
      integralTraceR(quadratureorder, nIntegrandR, nIntegrandPR);

    // element integrand/residuals
    DLA::VectorD<CellIntegrandType> rsdPDEElemL( nIntegrandL );
    DLA::VectorD<CellIntegrandType> rsdPDEElemR( nIntegrandR );
    DLA::VectorD<CellIntegrandType> rsdPDEpElemL( nIntegrandPL );
    DLA::VectorD<CellIntegrandType> rsdPDEpElemR( nIntegrandPR );

    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {

        //The cell that called this function is to the left of the trace
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        qpfldCellL.getElement( qpfldElemL, elemL );

        for (int n = 0; n < nIntegrandL; n++) rsdPDEElemL[n] = 0;
        for (int n = 0; n < nIntegrandPL; n++) rsdPDEpElemL[n] = 0;

        integralTraceL( fcn_.integrand(xfldElemTrace,  canonicalTraceL, +1,
                                       xfldElemL, qfldElemL, qpfldElemL),
                        xfldElemTrace,
                        rsdPDEElemL.data(), nIntegrandL,
                        rsdPDEpElemL.data(), nIntegrandPL);

        rsdPDEElemCell_ += rsdPDEElemL;
        rsdPDEpElemCell_ += rsdPDEpElemL;
      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        //The cell that called this function is to the right of the trace
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        qpfldCellR.getElement( qpfldElemR, elemR );

        for (int n = 0; n < nIntegrandR; n++) rsdPDEElemR[n] = 0;
        for (int n = 0; n < nIntegrandPR; n++) rsdPDEpElemR[n] = 0;

        integralTraceR( fcn_.integrand(xfldElemTrace, canonicalTraceR, -1,
                                       xfldElemR, qfldElemR, qpfldElemR),
                        xfldElemTrace, rsdPDEElemR.data(), nIntegrandR, rsdPDEpElemR.data(), nIntegrandPR);

        rsdPDEElemCell_ += rsdPDEElemR;
        rsdPDEpElemCell_ += rsdPDEpElemR;
      }
      else
        SANS_DEVELOPER_EXCEPTION("ResidualInteriorTrace_VMSD_impl::integrate - Invalid configuration!");

    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;

  DLA::VectorD<ArrayQ>& rsdPDEElemCell_;
  DLA::VectorD<ArrayQ>& rsdPDEpElemCell_;

  std::vector<int> traceGroupIndices_;

};


// Factory function
template<class IntegrandInteriorTrace, class ArrayQ>
ResidualInteriorTrace_VMSD_impl<IntegrandInteriorTrace>
ResidualInteriorTrace_VMSD( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                           const std::map<int,std::vector<int>>& cellITraceGroups,
                           const int& cellgroup, const int& cellelem,
                           DLA::VectorD<ArrayQ>& rsdPDEElemCell,
                           DLA::VectorD<ArrayQ>& rsdPDEpElemCell)
{
  static_assert( std::is_same< ArrayQ, typename IntegrandInteriorTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualInteriorTrace_VMSD_impl<IntegrandInteriorTrace>(fcn.cast(), cellITraceGroups, cellgroup, cellelem,
                                                                 rsdPDEElemCell, rsdPDEpElemCell);
}

}

#endif  // RESIDUALINTERIORTRACE_VMSD_H
