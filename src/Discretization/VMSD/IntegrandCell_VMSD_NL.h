// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_VMSD_NL_H
#define INTEGRANDCELL_VMSD_NL_H

// cell integrand operator: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"

#include "Discretization/Integrand_Type.h"
#include "JacobianCell_VMSD_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template <class PDE_>
class IntegrandCell_VMSD_NL : public IntegrandCellType< IntegrandCell_VMSD_NL<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays
  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_VMSD_NL( const PDE& pde, const std::vector<int>& CellGroups )
    : pde_(pde), cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  //weight functions are coarse scale basis functions
  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef JacobianElemCell_VMSD<PhysDim,MatrixQ<Real>> JacobianElemCellType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    BasisWeighted(  const PDE& pde,
                    const ElementParam& paramfldElem,
                    const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
                    const Element<ArrayQ<T>, TopoDim, Topology>& qpfldElem,
                    const bool computeQP = true) :
                      pde_(pde),
                      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                      qfldElem_(qfldElem),
                      qpfldElem_(qpfldElem),
                      paramfldElem_( paramfldElem ),
                      nDOF_( qfldElem_.nDOF() ),
                      nDOFp_( qpfldElem_.nDOF() ),
                      phi_( new Real[nDOF_] ),
                      phip_( new Real[nDOFp_] ),
                      gradphi_( new VectorX[nDOF_] ),
                      gradphip_( new VectorX[nDOFp_] ),
                      basisEqual_((qfldElem_.basis()->category() == qpfldElem_.basis()->category()) &&
                                  (qfldElem_.order() == qpfldElem_.order() ))
    {
    }

    BasisWeighted( BasisWeighted&& bw ) :
                      pde_(bw.pde_),
                      xfldElem_(bw.xfldElem_),
                      qfldElem_(bw.qfldElem_),
                      qpfldElem_(bw.qpfldElem_),
                      paramfldElem_( bw.paramfldElem_ ),
                      nDOF_( bw.nDOF_ ),
                      nDOFp_( bw.nDOFp_ ),
                      phi_( bw.phi_ ),
                      phip_( bw.phip_ ),
                      gradphi_( bw.gradphi_ ),
                      gradphip_( bw.gradphip_ ),
                      basisEqual_(bw.basisEqual_)
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
      bw.phip_ = nullptr; bw.gradphip_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] phip_;
      delete [] gradphi_;
      delete [] gradphip_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }
    int nDOFp() const { return nDOFp_; }

    // cell element residual integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                                                ArrayQ<Ti> integrandFine[], int neqnFine) const;

    template<class Ti>
    void operator()( const Real dJ, const QuadPointType& sRef,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemCoarse, DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemFine ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxPDEElem ) const;

  protected:
    template<class Tq, class Tg,class Ti>
    void weightedIntegrand( const ParamT& param,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                            ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                            ArrayQ<Ti> integrandFine[], int neqnFine ) const;

    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& qpfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nDOFp_;
    mutable Real *phi_;
    mutable Real *phip_;
    mutable VectorX *gradphi_;
    mutable VectorX *gradphip_;

    const bool basisEqual_;

  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qpfldElem,
            const bool computeQP = true) const
  {
    return BasisWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, qpfldElem, computeQP);
  }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<Real, TopoDim, Topology> ElementEFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    FieldWeighted( const PDE& pde,
                   const ElementParam& paramfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& qpfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
                   const Element<ArrayQ<T>, TopoDim, Topology>& wpfldElem,
                   const Element<Real, TopoDim, Topology>& efldElem) :
                     pde_(pde),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem),
                     qpfldElem_(qpfldElem),
                     wfldElem_(wfldElem),
                     wpfldElem_(wpfldElem),
                     efldElem_(efldElem),
                     paramfldElem_(paramfldElem),
                     nPhi_( efldElem_.nDOF() ),
                     phi_( new Real[nPhi_] ),
                     gradphi_( new VectorX[nPhi_] ),
                     weight_( new ArrayQ<T>[nPhi_] ),
                     gradWeight_( new VectorArrayQ<T>[nPhi_] ),
                     basisEqual_((qfldElem_.basis()->category() == qpfldElem_.basis()->category()) &&
                                 (qfldElem_.order() == qpfldElem_.order() ))
    {}

    FieldWeighted( FieldWeighted&& fw ) :
                     pde_(fw.pde_),
                     xfldElem_(fw.xfldElem_),
                     qfldElem_(fw.qfldElem_),
                     qpfldElem_(fw.qpfldElem_),
                     wfldElem_(fw.wfldElem_),
                     wpfldElem_(fw.wpfldElem_),
                     efldElem_(fw.efldElem_),
                     paramfldElem_(fw.paramfldElem_),
                     nPhi_( fw.nPhi_ ),
                     phi_( fw.phi_ ),
                     gradphi_( fw.gradphi_ ),
                     weight_( fw.weight_ ),
                     gradWeight_( fw.gradWeight_ ),
                     basisEqual_(fw.basisEqual_)
    {
      fw.phi_ = nullptr; fw.gradphi_ = nullptr;
      fw.weight_ = nullptr; fw.gradWeight_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] gradphi_;
      delete [] weight_;
      delete [] gradWeight_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }
    int nDOFp() const { return qpfldElem_.nDOF(); }

    // cell element integrand
    template <class Ti>
    void operator()( const QuadPointType& sRef, Ti integrand[], int nphi ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& qpfldElem_;
    const ElementQFieldType& wfldElem_;
    const ElementQFieldType& wpfldElem_;
    const ElementEFieldType& efldElem_;
    const ElementParam& paramfldElem_;

    const int nPhi_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    mutable ArrayQ<T> *weight_;
    mutable VectorArrayQ<T> *gradWeight_;

    const bool basisEqual_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qpfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& wpfldElem,
            const Element<Real, TopoDim, Topology>& efldElem) const
  {
    return FieldWeighted<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem, qpfldElem, wfldElem, wpfldElem, efldElem);
  }



protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_VMSD_NL<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_VMSD_NL<PDE>::FieldWeighted<T, TopoDim, Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_VMSD_NL<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const QuadPointType& sRef, ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                                      ArrayQ<Ti> integrandFine[], int neqnFine) const
{
  SANS_ASSERT(neqnCoarse == nDOF_);
  SANS_ASSERT(neqnFine == nDOFp_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                 // solution
  ArrayQ<T> qp;                // solution
  VectorArrayQ<T> gradq = 0;   // gradient
  VectorArrayQ<T> gradqp = 0;  // gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // if q and q' share the same basis, copy over instead of recomputing
  if (basisEqual_)
  {
    for (int k=0; k<nDOF_; k++)
    {
      phip_[k] = phi_[k];
      gradphip_[k] = gradphi_[k];
    }
  }
  else
  {
    qpfldElem_.evalBasis( sRef, phip_, nDOFp_ );
    xfldElem_.evalBasisGradient( sRef, qpfldElem_, gradphip_, nDOFp_ );
  }

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qpfldElem_.evalFromBasis( phip_, nDOFp_, qp );
  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    qpfldElem_.evalFromBasis( gradphip_, nDOFp_, gradqp );
  }


  q += qp;
  gradq += gradqp;

  // compute the residual
  weightedIntegrand( param, q, gradq,
                     integrandCoarse, neqnCoarse,
                     integrandFine, neqnFine);
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_VMSD_NL<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef,
           DLA::VectorD<ArrayQ<Ti>>& rsdPDEElemCoarse,
           DLA::VectorD<ArrayQ<Ti>>& rsdPDEElemFine) const
{
  SANS_ASSERT(rsdPDEElemCoarse.m() == nDOF_);
  SANS_ASSERT(rsdPDEElemFine.m() == nDOFp_);

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                 // solution
  ArrayQ<T> qp;                // solution
  VectorArrayQ<T> gradq = 0;   // gradient
  VectorArrayQ<T> gradqp = 0;  // gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // if q and q' share the same basis, copy over instead of recomputing
  if (basisEqual_)
  {
    for (int k=0; k<nDOF_; k++)
    {
      phip_[k] = phi_[k];
      gradphip_[k] = gradphi_[k];
    }
  }
  else
  {
    qpfldElem_.evalBasis( sRef, phip_, nDOFp_ );
    xfldElem_.evalBasisGradient( sRef, qpfldElem_, gradphip_, nDOFp_ );
  }

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qpfldElem_.evalFromBasis( phip_, nDOFp_, qp );
  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    qpfldElem_.evalFromBasis( gradphip_, nDOFp_, gradqp );
  }

  q += qp;
  gradq += gradqp;

  // compute the residual
  DLA::VectorD<ArrayQ<Ti>> integrandCoarse(nDOF_);
  DLA::VectorD<ArrayQ<Ti>> integrandFine(nDOFp_);

  weightedIntegrand( param, q, gradq,
                     integrandCoarse.data(), integrandCoarse.size(),
                     integrandFine.data(), integrandFine.size());

  for (int k = 0; k < nDOF_; ++k)
    rsdPDEElemCoarse(k) += dJ*integrandCoarse(k);

  for (int k = 0; k < nDOF_; ++k)
    rsdPDEElemFine(k) += dJ*integrandFine(k);
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_VMSD_NL<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxElem.nDOF == nDOF_);
  SANS_ASSERT(mtxElem.nDOFp == nDOFp_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;                 // solution
  ArrayQ<Real> qp;                // solution
  VectorArrayQ<Real> gradq = 0;   // gradient
  VectorArrayQ<Real> gradqp = 0;  // gradient

  ArrayQ<SurrealClass> qSurreal = 0;               // solution
  ArrayQ<SurrealClass> qpSurreal = 0;               // solution
  VectorArrayQ<SurrealClass> gradqSurreal = 0;     // gradient
  VectorArrayQ<SurrealClass> gradqpSurreal = 0;     // gradient

  MatrixQ<Real> PDE_q =0 , PDE_gradq = 0; // temporary storage
//  MatrixQ<Real> PDE_qp =0; // temporary storage
  MatrixQ<Real> PDEp_q =0 , PDEp_gradq = 0; // temporary storage
//  MatrixQ<Real> PDEp_qp =0 , PDEp_gradqp = 0; // temporary storage

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // if q and q' share the same basis, copy over instead of recomputing
  if (basisEqual_)
  {
    for (int k=0; k<nDOF_; k++)
    {
      phip_[k] = phi_[k];
      gradphip_[k] = gradphi_[k];
    }
  }
  else
  {
    qpfldElem_.evalBasis( sRef, phip_, nDOFp_ );
    xfldElem_.evalBasisGradient( sRef, qpfldElem_, gradphip_, nDOFp_ );
  }

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
//  qSurreal = q;

  qpfldElem_.evalFromBasis( phip_, nDOFp_, qp );
//  qpSurreal = qp;

  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
//    gradqSurreal = gradq;

    qpfldElem_.evalFromBasis( gradphip_, nDOFp_, gradqp );
//    gradqpSurreal = gradqp;
  }

  q += qp;
  gradq += gradqp;

  qSurreal = q;
  gradqSurreal = gradq;

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurrealp( nDOFp_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;
    integrandSurrealp = 0;

    weightedIntegrand( param, qSurreal, gradq,
                       integrandSurreal.data(), integrandSurreal.size(),
                       integrandSurrealp.data(), integrandSurrealp.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxElem.PDE_q(i,j) += dJ*phi_[j]*PDE_q;

        for (int j = 0; j < nDOFp_; j++)
          mtxElem.PDE_qp(i,j) += dJ*phip_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFp_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDEp_q,m,n) = DLA::index(integrandSurrealp[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxElem.PDEp_q(i,j) += dJ*phi_[j]*PDEp_q;

        for (int j = 0; j < nDOFp_; j++)
          mtxElem.PDEp_qp(i,j) += dJ*phip_[j]*PDEp_q;
      }
    }
    slot += PDE::N;
  } // nchunk

  if (needsSolutionGradient)
  {
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandSurreal = 0;
      integrandSurrealp = 0;

      weightedIntegrand( param, q, gradqSurreal,
                         integrandSurreal.data(), integrandSurreal.size(),
                         integrandSurrealp.data(), integrandSurrealp.size());

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOF_; j++)
              mtxElem.PDE_q(i,j) += dJ*gradphi_[j][d]*PDE_gradq;

            for (int j = 0; j < nDOFp_; j++)
              mtxElem.PDE_qp(i,j) += dJ*gradphip_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFp_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDEp_gradq,m,n) = DLA::index(integrandSurrealp[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOF_; j++)
              mtxElem.PDEp_q(i,j) += dJ*gradphi_[j][d]*PDEp_gradq;

            for (int j = 0; j < nDOFp_; j++)
              mtxElem.PDEp_qp(i,j) += dJ*gradphip_[j][d]*PDEp_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  }

}

// Cell integrand
//
// integrand = - grad(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tg, class Ti>
void
IntegrandCell_VMSD_NL<PDE>::BasisWeighted<T,TopoDim,Topology, ElementParam>::
weightedIntegrand( const ParamT& param,
                   const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                   ArrayQ<Ti> integrandCoarse[], int neqnCoarse,
                   ArrayQ<Ti> integrandFine[], int neqnFine ) const
{

  //R1 term: Galerkin coarse scale

  // using neqn here suppresses clang analyzer
  for (int k = 0; k < neqnCoarse; k++)
    integrandCoarse[k] = 0;

  for (int k = 0; k < neqnFine; k++)
    integrandFine[k] = 0;

  // Galerkin flux term: -gradient(phi) . F

  // PDE flux F(X, Q), Fv(X, Q, QX)
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;

//    VectorArrayQ<Tq> FA = 0;           // PDE flux F(X, Q), Fv(X, Q, QX)
    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

//    F += FA;

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, q, gradq, F );

    for (int k = 0; k < neqnCoarse; k++)
      integrandCoarse[k] -= dot(gradphi_[k],F);

    for (int k = 0; k < neqnFine; k++)
      integrandFine[k] -= dot(gradphip_[k],F);

  }

  // Galerkin source term: +phi S
  if (pde_.hasSource())
  {
    ArrayQ<Ti> source = 0;            // PDE source S(X, Q, QX)

    pde_.source( param, q, gradq, source );

    for (int k = 0; k < neqnCoarse; k++)
      integrandCoarse[k] += phi_[k]*source;

    for (int k = 0; k < neqnFine; k++)
      integrandFine[k] += phip_[k]*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS
  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < neqnCoarse; k++)
      integrandCoarse[k] -= phi_[k]*forcing;

    for (int k = 0; k < neqnFine; k++)
      integrandFine[k] -= phip_[k]*forcing;
  }

}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_VMSD_NL<PDE>::
FieldWeighted<T, TopoDim, Topology, ElementParam>::
operator()( const QuadPointType& sRef, Ti integrand[], int nphi ) const
{
  SANS_ASSERT( nphi == nPhi_ );

  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                // Coarse scale solution
  ArrayQ<T> qp;               // Fine scale solution
  VectorArrayQ<T> gradq = 0;  // Coarse solution gradient
  VectorArrayQ<T> gradqp = 0; // Fine solution gradient

  ArrayQ<T> w;               // weight
  ArrayQ<T> wp;               // weight
  VectorArrayQ<T> gradw;     // weight gradient
  VectorArrayQ<T> gradwp;     // weight gradient

  const bool needsSolutionGradient =
      ( pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // solution value
  qfldElem_.eval( sRef, q );
  qpfldElem_.eval( sRef, qp );

  // solution gradient, hessian
  if (needsSolutionGradient)
  {
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );
    xfldElem_.evalGradient( sRef, qpfldElem_, gradqp );
  }

  q += qp;
  gradq += gradqp;

  // weight value, gradient
  wfldElem_.eval( sRef, w );
  wpfldElem_.eval( sRef, wp );
  xfldElem_.evalGradient( sRef, wfldElem_, gradw );
  xfldElem_.evalGradient( sRef, wpfldElem_, gradwp );

  // estimate basis, gradient
  efldElem_.evalBasis( sRef, phi_, nPhi_ );
  xfldElem_.evalBasisGradient( sRef, efldElem_, gradphi_, nPhi_ );

  for (int k = 0; k < nPhi_; k++)
    integrand[k] = 0;

  //ASSUMING THAT efldElem_ AND epfldElem_ HAVE THE SAME BASIS!
  // for (int n = 0; n < N; n++)
  for (int k = 0; k < nPhi_; k++)
  {
    //CAN COMBINE WEIGHT FUNCTIONS BECAUSE VOLUME TERMS ARE ALL THE SAME
    // DLA::index(weight_[k],n) = DLA::index(w,n) * phi_[k];
    weight_[k] = (w + wp) * phi_[k];

    for (int d = 0; d < PhysDim::D; d++)
      gradWeight_[k][d]= (w + wp) * gradphi_[k][d] + phi_[k]*(gradw[d] + gradwp[d]);
    // gradWeight_[k] = gradphi_[k]*(w +wp) + phi_[k]*(gradw + gradwp);
  }

  // weighting the coarse-scale fluxes:
  // Galerkin flux term: -(phi grad w + w grad phi, F)
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;
    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, q, gradq, F );

    for (int d = 0; d < PhysDim::D; d++)
      for (int k = 0; k < nPhi_; k++)
        integrand[k] -= dot(gradWeight_[k][d],F[d]);
  }

  // Galerkin source term: +w S

  if (pde_.hasSource())
  {
    ArrayQ<Ti> source=0;   // PDE source S(X, D, U, UX), S(X)
    pde_.source( param, q, gradq, source );

    for (int k = 0; k < nPhi_; k++)
      integrand[k] += dot(weight_[k],source);
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < nPhi_; k++)
      integrand[k] -= dot(weight_[k],forcing);
  }

}


}

#endif  // INTEGRANDCELL_VMSD_NL_H
