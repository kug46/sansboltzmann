// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_LOCAL_VMSD_LINEAR_H
#define ALGEBRAICEQUATIONSET_LOCAL_VMSD_LINEAR_H

#include "AlgebraicEquationSet_VMSD_Linear.h"
#include "tools/SANSnumerics.h"

#include "Field/FieldTypes.h"
#include "Field/XField.h"

#include "Discretization/ResidualNormType.h"
#include "Discretization/DG/JacobianDetInvResidualNorm.h"
#include "BasisFunction/LagrangeDOFMap.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/tools/for_each_GhostBoundaryTraceGroup.h"
#include "Field/tools/for_each_GhostBoundaryTraceGroup_Cell.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Sub.h"

#include "Discretization/Galerkin/ExtractCGLocalBoundaries.h"
#include "Discretization/Galerkin/Stabilization_Nitsche.h"

#include "Discretization/Galerkin/IntegrandInteriorTrace_SIP_Galerkin.h"
#include "Discretization/Galerkin/ResidualInteriorTrace_SIP_Galerkin.h"
#include "Discretization/Galerkin/JacobianInteriorTrace_SIP_Galerkin.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

namespace SANS
{

/* This class refers to two types of local solutions/systems, full and sub.
 * The full local solution contains all the solution DOFs in the local mesh:
 *  - qfld DOFs in both cellgroups 0 and 1
 *  - all lgfld DOFs
 *
 * The sub local solution contains only the DOFs that are solved for in the local solve:
 *  - qfld DOFs in only cellgroup 0 (i.e. in main-cells)
 *  - lgfld DOFs of only the main boundary traces (no outer boundary traces)
 */
template <class PhysDim, class TopoDim>
class XField_EdgeLocal;

//----------------------------------------------------------------------------//
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType_>
class AlgebraicEquationSet_Local_VMSD_Linear :
    public AlgebraicEquationSet_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, AlgEqSetTraits_Dense, XFieldType_>
{
public:
  typedef XFieldType_ XFieldType; // exposing for debugging

  typedef AlgebraicEquationSet_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, AlgEqSetTraits_Dense, XFieldType> BaseType;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename BaseType::VectorSizeClass VectorSizeClass;
  typedef typename BaseType::MatrixSizeClass MatrixSizeClass;

  typedef typename BaseType::SystemMatrix SystemMatrix;
  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::BCParams BCParams;
  typedef FieldBundle_VMSD_Local<PhysDim,TopoDim,ArrayQ> FieldBundle_Local;

  typedef IntegrandInteriorTrace_SIP_Galerkin<NDPDEClass> IntegrandTraceSIPClass;

  template< class... BCArgs >
  AlgebraicEquationSet_Local_VMSD_Linear(const XFieldType& xfld,
                                                 Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                                 Field_EG_Cell<PhysDim, TopoDim, ArrayQ>& qpfld,
                                                 Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                 std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld,
                                                 const NDPDEClass& pde,
                                                 const DiscretizationVMSD& stab,
                                                 const QuadratureOrder& quadratureOrder,
                                                 const ResidualNormType& resNormType,
                                                 std::vector<Real>& tol,
                                                 const std::vector<int>& CellGroups,
                                                 const std::vector<int>& interiorTraceGroups,
                                                 PyDict& BCList,
                                                 const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                                 BCArgs&... args )
  : BaseType(xfld, qfld, qpfld, lgfld, pde, stab, quadratureOrder, resNormType, tol,
             CellGroups, interiorTraceGroups, BCList, BCBoundaryGroups, args...),
    interiorTraceGroups0to1_(findInteriorTraceGroup(xfld_.getXField(),0,1)),
    fcnSIP_(pde, stab, interiorTraceGroups0to1_),
    up_resfld_(up_resfld)
  {
    // If not using the whole patch or essential inner patch
    // Then the field is broken and we need to construct the map across the interface

#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))
    Field_CG_Cell<PhysDim,TopoDim,Real> cfld(xfld_.getXField(), qfld.getCellGroupBase(0).order(), qfld.getCellGroupBase(0).basisCategory());
    group1_to_group0_ = constructDOFMap( qfld, cfld, 1, 0);

    if (up_resfld_) // if there is a local res field
    {
      SANS_ASSERT_MSG( &up_resfld_->getXField() == &qfld_.getXField(), "Residual and solution fields must come from same grid" );
      // construct the map from resfld dofs to cell group 0 dofs
      DGgroup1_to_CGgroup0_ = constructDGtoCGDOFMap( *up_resfld_, qfld, group1_to_group0_, 1 );
    }
#endif

    if (fcnCell_.nCellGroups() == 1)
    {
      // figure out which qpfld_ dofs should be free by falsing all and then freeing the desired cell groups
      std::map<int,bool> qpfld_map;
      for (int i = 0; i < qpfld_.nDOF(); i++)
        qpfld_map[i] = false;

      // free the qpfld_ DOFS in the cellgroups to be solved
      for_each_CellGroup<TopoDim>::apply(
        ApplyMarkCellGroup<PhysDim,ArrayQ>(qpfld_map,fcnCell_.cellGroups(),true), qpfld_ );

      // loop over map and add those dofs that are free to the list
      for (auto const& keyVal: qpfld_map )
        if (keyVal.second == true )
          freeDOFs_[iPDEp].push_back(keyVal.first);
    }
    else
    {
      // All the cell groups should be free!
      freeDOFs_[iPDEp].reserve(qpfld_.nDOF());
      for (int dof = 0; dof < qpfld_.nDOF(); dof++)
        freeDOFs_[iPDEp].push_back(dof);
    }

#ifdef WHOLEPATCH
    const Patch patch = Patch::Whole;
#elif defined(INNERPATCH)
    const Patch patch = Patch::Inner;
#else
    const Patch patch = Patch::Broken;
#endif


    std::array<std::vector<int>,2> freeDOFs2;
    // Extract dofs which will be frozen by essential bcs
    extractFreeDOFs(qfld, lgfld, freeDOFs2, patch );  // using CG version of freeDOFS_

    for ( const int it : freeDOFs2[0] )  // copy over qfld
      freeDOFs_[iPDE].push_back( it );

    for ( const int it : freeDOFs2[1])  // copy over lgfld
      freeDOFs_[iBC].push_back(  it );

  }

  // Field Bundle Constructor for use in hiding the specific AlgEqSet in SolverInterface
  template< class... BCArgs>
  AlgebraicEquationSet_Local_VMSD_Linear( const XFieldType& xfld,
                                                  FieldBundle_Local& flds,
                                                  std::shared_ptr<Field_CG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                                  const NDPDEClass& pde,
                                                  const DiscretizationVMSD& stab,
                                                  const QuadratureOrder& quadratureOrder,
                                                  const ResidualNormType& resNormType,
                                                  const std::vector<Real>& tol,
                                                  const std::vector<int>& CellGroups,
                                                  const std::vector<int>& interiorTraceGroups,
                                                  PyDict& BCList,
                                                  const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                                  BCArgs&&... args )
  : BaseType(xfld, flds, pLiftedQuantityfld, pde, stab, quadratureOrder, resNormType, tol,
            CellGroups, interiorTraceGroups, BCList, BCBoundaryGroups, args...),
            interiorTraceGroups0to1_(findInteriorTraceGroup(xfld_.getXField(),0,1)),
            fcnSIP_(pde, stab, interiorTraceGroups0to1_),
            up_resfld_(flds.up_resfld)
  {
    // If not using the whole patch or essential inner patch
    // Then the field is broken and we need to construct the map across the interface
#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))
    Field_CG_Cell<PhysDim,TopoDim,Real> cfld(xfld_.getXField(), flds.order, flds.basis_cgcell);
    group1_to_group0_ = constructDOFMap( flds.qfld, cfld, 1, 0);

    if (up_resfld_) // if there is a local res field
    {
      SANS_ASSERT_MSG( &up_resfld_->getXField() == &qfld_.getXField(), "Residual and solution fields must come from same grid" );
      // construct the map from resfld dofs to cell group 0 dofs
      DGgroup1_to_CGgroup0_ = constructDGtoCGDOFMap( *up_resfld_, qfld_, group1_to_group0_, 1 );
    }
#endif

    if (fcnCell_.nCellGroups() == 1) // the p dofs in the outer region are being left alone
    {
      // figure out which qpfld_ dofs should be free by falsing all and then freeing the desired cell groups
      std::map<int,bool> qpfld_map;
      for (int i = 0; i < qpfld_.nDOF(); i++)
        qpfld_map[i] = false;

      // free the qpfld_ DOFS in the cellgroups to be solved
      for_each_CellGroup<TopoDim>::apply(
        ApplyMarkCellGroup<PhysDim,ArrayQ>(qpfld_map,fcnCell_.cellGroups(),true), qpfld_ );

      // loop over map and add those dofs that are free to the list
      for (auto const& keyVal: qpfld_map )
        if (keyVal.second == true )
          freeDOFs_[iPDEp].push_back(keyVal.first);
    }
    else
    {
      // All the cell groups should be free!
      freeDOFs_[iPDEp].reserve(qpfld_.nDOF());
      for (int dof = 0; dof < qpfld_.nDOF(); dof++)
        freeDOFs_[iPDEp].push_back(dof);
    }

#ifdef WHOLEPATCH
    const Patch patch = Patch::Whole;
#elif defined(INNERPATCH)
    const Patch patch = Patch::Inner;
#else
    const Patch patch = Patch::Broken;
#endif

    std::array<std::vector<int>,2> freeDOFs2;
    // Extract dofs which will be frozen by essential bcs
    extractFreeDOFs(flds.qfld, flds.lgfld, freeDOFs2, patch ); // using CG version of freeDOFS_

    for ( const int it : freeDOFs2[0] )  // copy over qfld
      freeDOFs_[iPDE].push_back( it );

    for ( const int it : freeDOFs2[1])  // copy over lgfld
      freeDOFs_[iBC].push_back( it );

  }


  virtual ~AlgebraicEquationSet_Local_VMSD_Linear() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& jac        ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz ) override {}

  void jacobian( SystemVectorView& rsd, SystemMatrixView& jac, bool transposed = false ) override;

  void completeUpdate(const SystemVectorView& rsd, SystemVectorView& xcondensed, SystemVectorView& x) const override;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  // Returns the vector and matrix sizes needed for the sub-system (local solve system)
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the size of the full vector (i.e. all elements in the local mesh)
  VectorSizeClass fullVectorSize() const;

  //Translates the sub-system vector into a local solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the sub-solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  const XFieldType& paramfld() const { return xfld_; }

  using BaseType::iPDE;
  using BaseType::iPDEp;
  using BaseType::iBC;
  using BaseType::iq;
  using BaseType::iqp;
  using BaseType::ilg;

  using BaseType::iPDESC;
  using BaseType::iBCSC;
  using BaseType::iqSC;
  using BaseType::ilgSC;

  const std::array<std::vector<int>,3>& getFreeDOFs( ) { return freeDOFs_ ; }

protected:
  std::array<std::vector<int>,3> freeDOFs_;

  std::map<int,int> group1_to_group0_;     // map for dofs duplicated in the broken trace
  std::map<int,int> DGgroup1_to_CGgroup0_; // map for DG dofs in group 1 to CG dofs in group 0

  using BaseType::xfld_;
  using BaseType::qfld_;
  using BaseType::qpfld_;
  using BaseType::lgfld_;
  using BaseType::quadratureOrder_;
  using BaseType::resNormType_;

  using BaseType::isStaticCondensed_;

  using BaseType::fcnCell_;
  using BaseType::fcnITrace_;
  using BaseType::fcnBTrace_;
  using BaseType::xfldCellToTrace_;
  using BaseType::dispatchBC_;

  std::vector<int> interiorTraceGroups0to1_;
  IntegrandTraceSIPClass fcnSIP_;
  std::unique_ptr<Field_DG_Cell<PhysDim,TopoDim,ArrayQ>>& up_resfld_; // pointer to the local resfld
};


//Computes the sub-residual for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
residual(SystemVectorView& rsd)
{

  SANS_ASSERT(rsd.m() == 3);
  //Compute the full residual vector of the local problem
  SystemVector rsd_full(BaseType::vectorEqSize());
  rsd_full = 0;
  BaseType::residual(rsd_full);

// If not WHOLEPATCH Or INNERPATCH, need to compute the SIP residual
#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))
  //add SIP type stabilization
  IntegrateInteriorTraceGroups<TopoDim>::integrate(ResidualInteriorTrace_SIP_Galerkin(fcnSIP_, rsd_full(iPDE)),
                                                   xfld_, qfld_,
                                                   quadratureOrder_.interiorTraceOrders.data(),
                                                   quadratureOrder_.interiorTraceOrders.size() );

   if (up_resfld_)
   {
     // If there is more than one cell group, you're gonna need a map
     SANS_ASSERT_MSG( (!DGgroup1_to_CGgroup0_.empty()
                     || qfld_.nCellGroups() == 1
                     || qfld_.getCellGroupBase(1).nElem()==0),
       "Must have constructed a map if there are elements to map to" );
     //MOVE RESIDUAL FROM FIXED EXTERNAL PATCH TO SUBPATCH
     for (const auto& keyVal : DGgroup1_to_CGgroup0_ )
       rsd_full[iPDE][keyVal.second] += up_resfld_->DOF(keyVal.first);
   }
   else
   {
     SANS_ASSERT_MSG( this->fcnCell_.nCellGroups() > 1 || qfld_.nCellGroups() == 1,
       "Must have evaluated the residual in the wider patch if its there" );
     // Copy the residual from the above full evaluation
     for (const auto& keyVal : group1_to_group0_ )
     {
       rsd_full[iPDE][keyVal.second] += rsd_full[iPDE][keyVal.first];
       rsd_full[iPDE][keyVal.first] = 0; // it has been transfered, so it is now zeroed
     }
   }
#endif

  //Extract the sub residual vector from the full residual, corresponding to the equations that need to be solved
  for (int i = 0; i < rsd.m(); i++)
  {
    SANS_ASSERT( rsd[i].m() == static_cast<int>(freeDOFs_[i].size())  );
    subVectorPlus(rsd_full[i], freeDOFs_[i], 1., rsd[i]);
  }

}

//Computes the sub-Jacobian for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
jacobian(SystemMatrixView& jac)
{
  SANS_ASSERT( !isStaticCondensed_ );

  SANS_ASSERT(jac.m() == 3);
  SANS_ASSERT(jac.n() == 3);
  //Compute the full Jacobian of the local problem
  SystemMatrix jac_full(BaseType::matrixSize());
  jac_full = 0;
  BaseType::template jacobian<SystemMatrix&>(jac_full, quadratureOrder_); //try to save some computation

  //Extract the sub Jacobian from the full Jacobian, containing only the equations that need to be solved
  for (int i = 0; i < jac_full.m(); i++)
    for (int j = 0; j < jac_full.n(); j++)
    {
      SANS_ASSERT( jac(i,j).m() == (short)freeDOFs_[i].size()  );
      SANS_ASSERT( jac(i,j).n() == (short)freeDOFs_[j].size()   );
      subMatrixPlus(jac_full(i,j), freeDOFs_[i], freeDOFs_[j], 1., jac(i,j));
    }

 // SLA::WriteMatrixMarketFile(jac(0,0), "tmp/jac_edge_local.mtx");
 // SLA::WriteMatrixMarketFile(jac(0,1), "tmp/jac_edge_01.mtx");
 // SLA::WriteMatrixMarketFile(jac(1,0), "tmp/jac_edge_10.mtx");
 // SLA::WriteMatrixMarketFile(jac(1,1), "tmp/jac_edge_11.mtx");
}

//Computes the sub-Jacobian for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
jacobian(SystemVectorView& rsd, SystemMatrixView& jac, bool transpose)
{
  SANS_ASSERT( isStaticCondensed_ );
  SANS_ASSERT(rsd.m() == 3);
  SANS_ASSERT(jac.m() == 2);
  SANS_ASSERT(jac.n() == 2);
  //Compute the full Jacobian of the local problem
  SystemVector rsd_full(BaseType::vectorEqSize());
  SystemMatrix jac_full(BaseType::matrixSize());

  rsd_full = 0;
  // Copy the solution from the linear algebra vector to the field variables
  for (std::size_t k = 0; k < freeDOFs_[iPDEp].size(); k++)
    rsd_full[iPDEp][freeDOFs_[iPDEp][k]] = rsd[iPDEp][k];

  for (std::size_t k = 0; k < freeDOFs_[iPDE].size(); k++)
    rsd_full[iPDE][freeDOFs_[iPDE][k]] = rsd[iPDE][k];

  for (std::size_t k = 0; k < freeDOFs_[iBC].size(); k++)
    rsd_full[iBC][freeDOFs_[iBC][k]] = rsd[iBC][k];

  jac_full = 0;
  BaseType::template jacobian<SystemMatrix&>(rsd_full, jac_full, transpose, quadratureOrder_);

#if (!defined(WHOLEPATCH)) && (!defined(INNERPATCH))
  IntegrateInteriorTraceGroups<TopoDim>::integrate(JacobianInteriorTrace_SIP_Galerkin(fcnSIP_, jac_full(iPDESC, iPDESC)),
                                                   xfld_, qfld_,
                                                   quadratureOrder_.interiorTraceOrders.data(),
                                                   quadratureOrder_.interiorTraceOrders.size() );
#endif

  SANS_ASSERT( jac(iPDESC,iqSC).m() == (short)freeDOFs_[iPDE].size()  );
  SANS_ASSERT( jac(iPDESC,iqSC).n() == (short)freeDOFs_[iPDE].size()   );
  subMatrixPlus(jac_full(iPDESC,iqSC), freeDOFs_[iPDE], freeDOFs_[iPDE], 1., jac(0,0));

  //Extract the sub residual vector from the full residual, corresponding to the equations that need to be solved
  for (int i = 0; i < rsd.m(); i++)
  {
    SANS_ASSERT( rsd[i].m() == (short)freeDOFs_[i].size()  );
    subVectorValue(rsd_full[i], freeDOFs_[i], 1., rsd[i]);
  }
 // SLA::WriteMatrixMarketFile(jac(0,0), "tmp/jac_edge_local.mtx");
 // SLA::WriteMatrixMarketFile(jac(0,1), "tmp/jac_edge_01.mtx");
 // SLA::WriteMatrixMarketFile(jac(1,0), "tmp/jac_edge_10.mtx");
 // SLA::WriteMatrixMarketFile(jac(1,1), "tmp/jac_edge_11.mtx");
}

//Computes the sub-Jacobian for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
completeUpdate(const SystemVectorView& rsd, SystemVectorView& xcondensed, SystemVectorView& x) const
{
  SANS_ASSERT(rsd.m() == 3);
  SystemVector rsd_full(BaseType::vectorEqSize());
  rsd_full = 0;

  // Copy the solution from the linear algebra vector to the field variables
  for (std::size_t k = 0; k < freeDOFs_[iPDEp].size(); k++)
    rsd_full[iPDEp][freeDOFs_[iPDEp][k]] = rsd[iPDEp][k];

  for (std::size_t k = 0; k < freeDOFs_[iPDE].size(); k++)
    rsd_full[iPDE][freeDOFs_[iPDE][k]] = rsd[iPDE][k];

  for (std::size_t k = 0; k < freeDOFs_[iBC].size(); k++)
    rsd_full[iBC][freeDOFs_[iBC][k]] = rsd[iBC][k];

  SystemVector x_full(BaseType::vectorEqSize());
  x_full = 0;

  SystemVector xcondensed_full = x_full.sub(1,2);

  // Copy the solution from the linear algebra vector to the field variables

  for (std::size_t k = 0; k < freeDOFs_[iPDEp].size(); k++)
    x_full[iPDEp][freeDOFs_[iPDEp][k]] = x[iqp][k];

  for (std::size_t k = 0; k < freeDOFs_[iPDE].size(); k++)
  {
    xcondensed_full[iPDESC][freeDOFs_[iPDE][k]] = xcondensed[iqSC][k];
    x_full[iPDE][freeDOFs_[iPDE][k]] = xcondensed[iqSC][k];
  }

  for (std::size_t k = 0; k < freeDOFs_[iBC].size(); k++)
  {
    xcondensed_full[iBCSC][freeDOFs_[iBC][k]] = xcondensed[ilgSC][k];
    x_full[iBC][freeDOFs_[iBC][k]] = xcondensed[ilgSC][k];
  }

  BaseType::completeUpdate(rsd_full, xcondensed_full, x_full);

  //Extract the sub residual vector from the full residual, corresponding to the equations that need to be solved
  for (int i = 0; i < x.m(); i++)
  {
    SANS_ASSERT( x[i].m() == (short)freeDOFs_[i].size()  );
    subVectorValue(x_full[i], freeDOFs_[i], 1., x[i]);
  }

}


//Evaluate Residual Norm
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
std::vector<std::vector<Real>>
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
residualNorm( const SystemVectorView& rsd ) const
{
  const int nMon = this->pde_.nMonitor();

  DLA::VectorD<Real> rsdPDEtmp(nMon);
  DLA::VectorD<Real> rsdPDEptmp(nMon);
  DLA::VectorD<Real> rsdBCtmp(nMon);

  rsdPDEtmp = 0;
  rsdPDEptmp = 0;
  rsdBCtmp = 0;

  std::vector<std::vector<Real>> rsdNorm(this->nResidNorm(), std::vector<Real>(nMon, 0));
  std::vector<KahanSum<Real>> rsdNormKahanPDE(nMon, 0);
  std::vector<KahanSum<Real>> rsdNormKahanPDEp(nMon, 0);


  for (int j = 0; j < nMon; j++)
  {
    rsdNormKahanPDE[j] = 0;
    rsdNormKahanPDEp[j] = 0;
  }

  //PDE residual norm
  if (resNormType_ == ResidualNorm_L2 || resNormType_ == ResidualNorm_L2_DOFWeighted)
  {
    for (int n = 0; n < rsd[iPDE].m(); n++)
    {
      this->pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNormKahanPDE[j] += pow(rsdPDEtmp[j],2);
    }

    for (int n = 0; n < rsd[iPDEp].m(); n++)
    {
      this->pde_.interpResidVariable(rsd[iPDEp][n], rsdPDEptmp);

      for (int j = 0; j < nMon; j++)
        rsdNormKahanPDEp[j] += pow(rsdPDEptmp[j],2);
    }

    if (resNormType_ == ResidualNorm_L2)
    {
      for (int j = 0; j < nMon; j++)
      {
        Real tmp = sqrt((Real)rsdNormKahanPDE[j] + (Real)rsdNormKahanPDEp[j]);
        rsdNorm[iPDE][j] = tmp;
        rsdNorm[iPDEp][j] = tmp;
      }
    }
    else
    {
      int qdof = qfld_.nDOFnative();
      int qpdof = qpfld_.nDOFnative();

      for (int j = 0; j < nMon; j++)
      {
        Real tmp = sqrt((Real)rsdNormKahanPDE[j]/qdof + (Real)rsdNormKahanPDEp[j]/qpdof);
        rsdNorm[iPDE][j] = tmp;
        rsdNorm[iPDEp][j] = tmp;
      }
    }


  }
  else
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Local_VMSD_Linear::residualNorm - Unknown residual norm type!");

  //BC residual norm
  for (int n = 0; n < rsd[iBC].m(); n++)
  {
    this->pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

  return rsdNorm;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
typename AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::vectorEqSize() const
{
  static_assert(iPDEp == 0,"");
  static_assert(iPDE == 1,"");
  static_assert(iBC == 2,"");

  return { (int)freeDOFs_[iPDEp].size(),
           (int)freeDOFs_[iPDE].size(),
           (int)freeDOFs_[iBC].size() };
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
typename AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::vectorStateSize() const
{
  static_assert(iPDEp == 0,"");
  static_assert(iPDE == 1,"");
  static_assert(iBC == 2,"");

  return { (int)freeDOFs_[iPDEp].size(),
           (int)freeDOFs_[iPDE].size(),
           (int)freeDOFs_[iBC].size() };
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
typename AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::matrixSize() const
{
  static_assert(iPDEp == 0,"");
  static_assert(iPDE == 1,"");
  static_assert(iBC == 2,"");
  static_assert(iqp == 0,"");
  static_assert(iq == 1,"");
  static_assert(ilg == 2,"");

  // jacobian nonzero pattern
  //
  //           q  lg
  //   PDE     X   X
  //   BC      X   0

  if (isStaticCondensed_)
  {
    //  const int nPDEp = (int)freeDOFs_[iPDEp].size();
    const int nPDE = (int)freeDOFs_[iPDE].size();
    const int nBC = (int)freeDOFs_[iBC].size();

    return {  { { nPDE, nPDE }, { nPDE, nBC} },
              { {  nBC, nPDE },  {  nBC, nBC} } };
  }
  else
  {
    const int nPDEp = (int)freeDOFs_[iPDEp].size();
    const int nPDE = (int)freeDOFs_[iPDE].size();
    const int nBC = (int)freeDOFs_[iBC].size();

    return {  { {nPDEp, nPDEp}, {nPDEp, nPDE}, {nPDEp, nBC} },
              { {nPDE, nPDEp}, { nPDE, nPDE }, { nPDE, nBC} },
              { {nBC, nPDEp}, {  nBC, nPDE },  {  nBC, nBC} } };

  }
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
typename AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::
fullVectorSize() const
{
  return BaseType::vectorEqSize();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  SANS_ASSERT( (int)freeDOFs_[iPDEp].size() == q[iqp].m() );
  for (std::size_t k = 0; k < freeDOFs_[iPDEp].size(); k++)
    qpfld_.DOF(freeDOFs_[iPDEp][k]) = q[iqp][k];

  // Copy the solution from the linear algebra vector to the field variables
  SANS_ASSERT( (int)freeDOFs_[iPDE].size() == q[iq].m() );
  for (std::size_t k = 0; k < freeDOFs_[iPDE].size(); k++)
    qfld_.DOF(freeDOFs_[iPDE][k]) = q[iq][k];

  SANS_ASSERT( (int)freeDOFs_[iBC].size() == q[ilg].m() );
  for (std::size_t k = 0; k < freeDOFs_[iBC].size(); k++)
    lgfld_.DOF(freeDOFs_[iBC][k]) = q[ilg][k];

}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType>
void
AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType>::fillSystemVector(SystemVectorView& q) const
{

  // Copy the solution from the field variables to the linear algebra vector
  SANS_ASSERT( (int)freeDOFs_[iPDEp].size() == q[iqp].m() );
  for (std::size_t k = 0; k < freeDOFs_[iPDEp].size(); k++)
    q[iqp][k] = qpfld_.DOF(freeDOFs_[iPDEp][k]);

  // Copy the solution from the field variables to the linear algebra vector
  SANS_ASSERT( (int)freeDOFs_[iPDE].size() == q[iq].m() );
  for (std::size_t k = 0; k < freeDOFs_[iPDE].size(); k++)
    q[iq][k] = qfld_.DOF(freeDOFs_[iPDE][k]);


  SANS_ASSERT( (int)freeDOFs_[iBC].size() == q[ilg].m() );
  for (std::size_t k = 0; k < freeDOFs_[iBC].size(); k++)
    q[ilg][k] = lgfld_.DOF(freeDOFs_[iBC][k]);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_LOCAL_VMSD_LINEAR_H
