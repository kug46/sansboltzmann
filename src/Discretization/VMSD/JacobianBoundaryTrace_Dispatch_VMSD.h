// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DISPATCH_VMSD_H
#define JACOBIANBOUNDARYTRACE_DISPATCH_VMSD_H

// boundary-trace integral jacobian functions

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "JacobianBoundaryTrace_VMSD.h"
#include "JacobianBoundaryTrace_VMSD_SC.h"

namespace SANS
{


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
class JacobianBoundaryTrace_Dispatch_VMSD_SC_impl
{
public:
  JacobianBoundaryTrace_Dispatch_VMSD_SC_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const int* quadratureorder, int ngroup,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
      FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDE_qp )
    : xfld_(xfld), qfld_(qfld), qpfld_(qpfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_q_(mtxGlobalPDE_q),
      boundJacPDE_qp_(boundJacPDE_qp)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        JacobianBoundaryTrace_VMSD_SC<Surreal>(fcn, mtxGlobalPDE_q_, boundJacPDE_qp_),
        xfld_, (qfld_, qpfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDE_qp_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
JacobianBoundaryTrace_Dispatch_VMSD_SC_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, MatrixQ>
JacobianBoundaryTrace_Dispatch_VMSD_SC(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                             const int* quadratureorder, int ngroup,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                             FieldDataMatrixD_BoundaryCell<MatrixQ>& boundJacPDE_qp)
{
  return { xfld, qfld, qpfld, quadratureorder, ngroup, mtxGlobalPDE_q, boundJacPDE_qp };
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
class JacobianBoundaryTrace_Dispatch_VMSD_impl
{
public:
  JacobianBoundaryTrace_Dispatch_VMSD_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const int* quadratureorder, int ngroup,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qp)
    : xfld_(xfld), qfld_(qfld), qpfld_(qpfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_q_(mtxGlobalPDE_q),
      mtxGlobalPDE_qp_(mtxGlobalPDE_qp)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        JacobianBoundaryTrace_VMSD<Surreal>(fcn, mtxGlobalPDE_q_, mtxGlobalPDE_qp_),
        xfld_, (qfld_, qpfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qp_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
JacobianBoundaryTrace_Dispatch_VMSD_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, MatrixQ>
JacobianBoundaryTrace_Dispatch_VMSD(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                             const int* quadratureorder, int ngroup,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qp)
{
  return { xfld, qfld, qpfld, quadratureorder, ngroup, mtxGlobalPDE_q, mtxGlobalPDE_qp };
}

}

#endif //JACOBIANBOUNDARYTRACE_DISPATCH_VMSD_H
