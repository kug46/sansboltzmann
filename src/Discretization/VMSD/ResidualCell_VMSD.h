// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALCELL_VMSD_H
#define RESIDUALCELL_VMSD_H

// Cell integral residual functions

#include <map>

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/CellToTraceAssociativity.h"

#include "Discretization/QuadratureOrder.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateGhostBoundaryTraceGroups.h"
#include "Residual_InnerBoundaryTrace_VMSD.h"
#include "ResidualInteriorTrace_VMSD.h"


namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral
//

template<class IntegrandCell, class IntegrandTrace, class XFieldCellToTrace,
         class XFieldClass, class QFieldType, class QPFieldType, template<class> class Vector>
class ResidualCell_VMSD_impl :
    public GroupIntegralCellType< ResidualCell_VMSD_impl<IntegrandCell, IntegrandTrace, XFieldCellToTrace,
                                                        XFieldClass, QFieldType, QPFieldType, Vector> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;

  static_assert( std::is_same< PhysDim, typename IntegrandTrace::PhysDim >::value, "PhysDim should be the same.");

  // Save off the boundary trace integrand and the residual vectors
  ResidualCell_VMSD_impl( const IntegrandCell& fcnCell, const IntegrandTrace& fcnITrace,  const IntegrandTrace& fcnBTrace,
                         const XFieldCellToTrace& xfldCellToTrace,
                         const XFieldClass& xfld, const QFieldType& qfld,  const QPFieldType& qpfld,
                         const QuadratureOrder& quadOrder,
                         Vector<ArrayQ>& rsdPDEGlobal, Vector<ArrayQ>& rsdPDEpGlobal) :
                           fcnCell_(fcnCell), fcnITrace_(fcnITrace),  fcnBTrace_(fcnBTrace),
                           xfldCellToTrace_(xfldCellToTrace),
                           xfld_(xfld), qfld_(qfld), qpfld_(qpfld), quadOrder_(quadOrder),
                           nITraceGroup_( xfld.getXField().nInteriorTraceGroups() ),
                           nBTraceGroup_( xfld.getXField().nBoundaryTraceGroups() ),
                           nGTraceGroup_( xfld.getXField().nGhostBoundaryTraceGroups() ),
                           rsdPDEGlobal_(rsdPDEGlobal), rsdPDEpGlobal_(rsdPDEpGlobal),
                           comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( rsdPDEpGlobal_.m() == qpfld.nDOFpossessed() ||
                 rsdPDEpGlobal_.m() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                            ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qpfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );

    // number of integrals evaluated
    const int qDOF = qfldElem.nDOF();
    const int qDOFp = qpfldElem.nDOF();

    const int nDOFpossessed = rsdPDEGlobal_.m();
    const int nDOFppossessed = rsdPDEpGlobal_.m(); //compute ghost residuals

    // element integral
    typedef ArrayQ PDEIntegrandType;

    typedef GalerkinWeightedIntegral<TopoDim, Topology, PDEIntegrandType, PDEIntegrandType> IntegralType;
    IntegralType integralPDE(quadratureorder, qDOF, qDOFp);

    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == qpfldCell.nElem() );

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_q( qDOF, -1 );
    std::vector<int> mapDOFLocal(qDOF, -1);
    std::vector<int> maprsd(qDOF, -1);
    int nDOFLocal = 0;

    std::vector<int> mapDOFGlobal_qp( qDOFp, -1 );

    // residual array
    DLA::VectorD<PDEIntegrandType> rsdPDEElem( qDOF );
    DLA::VectorD<PDEIntegrandType> rsdPDEpElem( qDOFp );

    // loop over elements within group and solve for the auxiliary variables first
    for (int elem = 0; elem < nelem; elem++)
    {

      // scatter-add element integral to global
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal_q.data(), qDOF );
      qpfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal_qp.data(), qDOFp );

      nDOFLocal = 0;
      for (int n =0; n<qDOF; n++)
      {
        if (mapDOFGlobal_q[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal_q[n];
          nDOFLocal++;
        }
      }

      if (nDOFLocal == 0 && mapDOFGlobal_qp[0] >= nDOFppossessed ) continue;

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qpfldCell.getElement( qpfldElem, elem );

      //-----------------------------------------------------------------------------------------

      for (int n = 0; n < qDOF; n++) rsdPDEElem[n] = 0;
      for (int n = 0; n < qDOFp; n++) rsdPDEpElem[n] = 0;

      // cell integration for canonical element

      integralPDE( fcnCell_.integrand(xfldElem, qfldElem, qpfldElem), get<-1>(xfldElem),
                   rsdPDEElem.data(), qDOF,
                   rsdPDEpElem.data(), qDOFp );


      std::map<int,std::vector<int>> cellITraceGroups;
      std::map<int,std::vector<int>> cellBTraceGroups;
      std::map<int,std::vector<int>> cellGTraceGroups;

      //ELEMENT-WISE TRACE INTEGRAND
      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
//        std::cout << "elem: " << elem << ", trace: " << trace << "\n";
        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, elem, trace);

        if (traceinfo.type == TraceInfo::Interior)
        {
          cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::Boundary) //boundary trace
        {
          cellBTraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::GhostBoundary) //boundary trace
        {
          cellGTraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
      }

      //Collect the contributions to the PDE residual from the traces
      if (nITraceGroup_>0)
      {
        std::vector<int> quadOrderITrace;

        for (int i=0; i< nITraceGroup_; i++)
        {
          quadOrderITrace.push_back(quadOrder_.interiorTraceOrders[0]);
        }

        IntegrateInteriorTraceGroups<TopoDim>::integrate(ResidualInteriorTrace_VMSD(fcnITrace_, cellITraceGroups,
                                                                                   cellGroupGlobal, elem,
                                                                                   rsdPDEElem, rsdPDEpElem),
                                                         xfld_, (qfld_, qpfld_),
                                                         quadOrderITrace.data(), quadOrderITrace.size() );
      }

      if (nBTraceGroup_ > 0)
      {
        std::vector<int> quadOrderBTrace;

        for (int i=0; i< nBTraceGroup_; i++)
          quadOrderBTrace.push_back(quadOrder_.interiorTraceOrders[0]);


        IntegrateBoundaryTraceGroups<TopoDim>::integrate(Residual_InnerBoundaryTrace_VMSD(fcnBTrace_, cellBTraceGroups,
                                                                                    cellGroupGlobal, elem,
                                                                                    rsdPDEElem, rsdPDEpElem),
                                                         xfld_, (qfld_, qpfld_),
                                                         quadOrderBTrace.data(), quadOrderBTrace.size() );
      }



      //WE NEED TO INTEGRATE ON GHOST TRACES FOR VMSD
      if (nGTraceGroup_ > 0)
      {
        std::vector<int> quadOrderGTrace;

        for (int i=0; i< nGTraceGroup_; i++)
          quadOrderGTrace.push_back(quadOrder_.interiorTraceOrders[0]);


        IntegrateGhostBoundaryTraceGroups<TopoDim>::integrate(Residual_InnerBoundaryTrace_VMSD(fcnBTrace_, cellGTraceGroups,
                                                                                         cellGroupGlobal, elem,
                                                                                         rsdPDEElem, rsdPDEpElem),
                                                              xfld_, (qfld_, qpfld_),
                                                              quadOrderGTrace.data(), quadOrderGTrace.size() );
      }


      // HACK TO SET BUBBLE RESIDUALS TO ZERO; REMOVES OVERLAP IN BASIS
      const int nBubble = qfldCell.associativity( elem ).nBubble();
      if (nBubble > 0 &&  (qpfldElem.order() > PhysDim::D) )
      {
        SANS_ASSERT( qpfldElem.basis()->category() == BasisFunctionCategory_Lagrange );
        for (int ind=(qDOFp - nBubble); ind<qDOFp; ind++) //CELL DOFS WILL BE LAST?
        {
          rsdPDEpElem[ind] = 0;
        }
      }

      for (int n = 0; n < nDOFLocal; n++)
        rsdPDEGlobal_[mapDOFLocal[n]] += rsdPDEElem[ maprsd[n] ];

      if ( mapDOFGlobal_qp[0] < nDOFppossessed )
      {
        for (int n = 0; n < qDOFp; n++)
          rsdPDEpGlobal_[mapDOFGlobal_qp[n]] += rsdPDEpElem[n];

      }

    } //elem loop

  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandTrace& fcnITrace_;
  const IntegrandTrace& fcnBTrace_;
  const XFieldCellToTrace& xfldCellToTrace_;
  const XFieldClass& xfld_;
  const QFieldType& qfld_;
  const QPFieldType& qpfld_;

  const QuadratureOrder& quadOrder_;
  const int nITraceGroup_;
  const int nBTraceGroup_;
  const int nGTraceGroup_;

  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdPDEpGlobal_;

  mutable int comm_rank_;

};

// Factory function

template<class IntegrandCell, class IntegrandTrace, class XFieldCellToTrace,
         class XFieldType, class QFieldType, class QPFieldType, template<class> class Vector, class ArrayQ>
ResidualCell_VMSD_impl<IntegrandCell, IntegrandTrace, XFieldCellToTrace,
                      XFieldType, QFieldType, QPFieldType, Vector>
ResidualCell_VMSD( const IntegrandCellType<IntegrandCell>& fcnCell,
                  const IntegrandInteriorTraceType<IntegrandTrace>& fcnITrace,
                  const IntegrandInteriorTraceType<IntegrandTrace>& fcnBTrace,
                  const XFieldCellToTrace& xfldCellToTrace,
                  const XFieldType& xfld, const QFieldType& qfld, const QPFieldType& qpfld,
                  const QuadratureOrder& quadOrder,
                  Vector<ArrayQ>& rsdPDEGlobal, Vector<ArrayQ>& rsdPDEpGlobal)
{
  static_assert( std::is_same< ArrayQ, typename IntegrandCell::template ArrayQ<Real> >::value, "These should be the same.");

  return ResidualCell_VMSD_impl<IntegrandCell, IntegrandTrace, XFieldCellToTrace,
                               XFieldType, QFieldType, QPFieldType, Vector>(
      fcnCell.cast(), fcnITrace.cast(), fcnBTrace.cast(),
      xfldCellToTrace, xfld, qfld, qpfld, quadOrder,
      rsdPDEGlobal, rsdPDEpGlobal );
}

}

#endif  // RESIDUALCELL_VMSD_H
