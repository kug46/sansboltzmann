// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALBOUNDARYTRACE_WEIGHTEDRESIDUAL_VMSD_H
#define JACOBIANFUNCTIONALBOUNDARYTRACE_WEIGHTEDRESIDUAL_VMSD_H

// jacobian boundary-trace functional jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

#include "Field/FieldData/FieldDataReal_Cell.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementalMassMatrix.h"

#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/Integrand_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Functional boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, template<class> class Vector>
class JacobianFunctionalBoundaryTrace_WeightedResidual_VMSD_impl :
    public GroupIntegralBoundaryTraceType<
       JacobianFunctionalBoundaryTrace_WeightedResidual_VMSD_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector> >
{
public:
  typedef typename FunctionalIntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Real> ArrayJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template MatrixJ<Real> MatrixJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Surreal> ArrayJSurreal;

  JacobianFunctionalBoundaryTrace_WeightedResidual_VMSD_impl( const FunctionalIntegrandBoundaryTrace& fcnJ,
                                                                  const BCIntegrandBoundaryTrace& fcnBC,
                                                                  Vector<MatrixJ>& jacFunctional_q,
                                                                  Vector<MatrixJ>& jacFunctional_qp ) :
    fcnJ_(fcnJ),
    fcnBC_(fcnBC),
    jacFunctional_q_(jacFunctional_q),
    jacFunctional_qp_(jacFunctional_qp),
    comm_rank_(0)
  {
    // Find all groups that are common between the BC and functional
    for (std::size_t i = 0; i < fcnJ_.nBoundaryGroups(); i++)
    {
      for (std::size_t j = 0; j < fcnBC_.nBoundaryGroups(); j++)
      {
        std::size_t iBoundaryGroupJ = fcnJ_.boundaryGroup(i);
        if (iBoundaryGroupJ == fcnBC_.boundaryGroup(j))
          boundaryTraceGroups_.push_back(iBoundaryGroupJ);
      }
    }
  }

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    SANS_ASSERT_MSG( qfld.nDOFpossessed() == jacFunctional_q_.m(),
                    "qfld.nDOFpossessed() = %d, jacFunctional_q_.m() = %d", qfld.nDOFpossessed(), jacFunctional_q_.m());

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType, class XFieldTraceGroupType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::                      template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const XFieldTraceGroupType& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL qpfldElemL( qpfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // total number of entries in ArrayQ
    const int nVar = DLA::VectorSize<ArrayQ>::M;

    // total number of outputs in the functional
    const int nJEqn = DLA::VectorSize<ArrayJ>::M;

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();
    int nDOFpL = qpfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFLocal( nDOFL, -1 );
    std::vector<int> maprsd( nDOFL, -1 );
    int nDOFLocal = 0;
    const int nDOFpossessed = jacFunctional_q_.m();

    std::vector<int> mapDOFGlobal_qpL(nDOFpL, -1);

    // trace element integral
    ElementIntegral<TopoDimTrace, TopologyTrace, ArrayJSurreal> integralJ(quadratureorder);

    // element functional jacobian vector
    DLA::VectorD<MatrixJ> jacFunctionalElem_q(nDOFL);
    DLA::VectorD<MatrixJ> jacFunctionalElem_qp(nDOFpL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL.data(), nDOFL );
      qpfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qpL.data(), nDOFpL );

      // copy over the map for each DOF that is possessed by this processor
      nDOFLocal = 0;
      for (int n = 0; n < nDOFL; n++)
        if (mapDOFGlobal_qL[n] < nDOFpossessed)
        {
          maprsd[nDOFLocal] = n;
          mapDOFLocal[nDOFLocal] = mapDOFGlobal_qL[n];
          nDOFLocal++;
        }

      // no need if all DOFs are possessed by other processors
      if (nDOFLocal == 0 && qpfldCellL.associativity( elem ).rank() != comm_rank_) continue;

      // reset the element jacobian
      jacFunctionalElem_q = 0;
      jacFunctionalElem_qp = 0;

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      qpfldCellL.getElement( qpfldElemL, elemL );

      xfldTrace.getElement(  xfldElemTrace, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*((D+1)*nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot = 0;
        int slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*nDOFL;

        // trace integration for canonical element
        ArrayJSurreal functional = 0;
        integralJ( fcnJ_.integrand(fcnBC_,
                                   xfldElemTrace, canonicalTraceL,
                                   xfldElemL, qfldElemL, qpfldElemL),
                   get<-1>(xfldElemTrace),
                   functional );

        // accumulate derivatives into element jacobian

        //wrt qL
        slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 0; // Reset the derivative

              // Note that n,m are transposed intentionally
              for (int m = 0; m < nJEqn; m++)
                DLA::index(jacFunctionalElem_q[j],n,m) += DLA::index(functional, m).deriv(slot - nchunk);
            }
          }
        }

      }   // nchunk

      // scatter-add element jacobian to global
      for (int n = 0; n < nDOFLocal; n++)
        jacFunctional_q_[ mapDOFLocal[n] ] += jacFunctionalElem_q[ maprsd[n] ];

      // JACOBIAN WRT TO Q'
      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*((D+1)*nDOFpL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot = 0;
        int slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qpfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*nDOFL;

        // trace integration for canonical element
        ArrayJSurreal functional = 0;
        integralJ( fcnJ_.integrand(fcnBC_,
                                   xfldElemTrace, canonicalTraceL,
                                   xfldElemL, qfldElemL, qpfldElemL),
                   get<-1>(xfldElemTrace),
                   functional );

        // accumulate derivatives into element jacobian

        //wrt qL
        slotOffset = 0;
        for (int j = 0; j < nDOFpL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qpfldElemL.DOF(j), n).deriv(slot - nchunk) = 0; // Reset the derivative

              // Note that n,m are transposed intentionally
              for (int m = 0; m < nJEqn; m++)
                DLA::index(jacFunctionalElem_qp[j],n,m) += DLA::index(functional, m).deriv(slot - nchunk);
            }
          }
        }

      }   // nchunk


      // HACK TO SET BUBBLE RESIDUALS TO ZERO; REMOVES OVERLAP IN BASIS
      const int nBubble = qfldCellL.associativity( elemL ).nBubble();
      if (nBubble > 0 &&  (qpfldElemL.order() > PhysDim::D) )
      {
        SANS_ASSERT( qpfldElemL.basis()->category() == BasisFunctionCategory_Lagrange );
        for (int ind=(nDOFpL - nBubble); ind<nDOFpL; ind++) //CELL DOFS WILL BE LAST?
        {
          jacFunctionalElem_qp[ind] = 0;
        }
      }


      // scatter-add element jacobian to global
      for (int n = 0; n < nDOFpL; n++)
        jacFunctional_qp_[ n ] += jacFunctionalElem_qp[ n ];
    } // elem
  }

protected:
  const FunctionalIntegrandBoundaryTrace& fcnJ_;
  const BCIntegrandBoundaryTrace& fcnBC_;
  std::vector<int> boundaryTraceGroups_;
  Vector<MatrixJ>& jacFunctional_q_;
  Vector<MatrixJ>& jacFunctional_qp_;
  mutable int comm_rank_;
};

// Factory function

template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_WeightedResidual_VMSD_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, Vector>
JacobianFunctionalBoundaryTrace_WeightedResidual_VMSD( const IntegrandBoundaryTraceType<FunctionalIntegrandBoundaryTrace>& fcnJ,
                                                           const IntegrandBoundaryTraceType<BCIntegrandBoundaryTrace>& fcnBC,
                                                           Vector< MatrixJ >& func_q, Vector< MatrixJ >& func_qp )
{
  typedef typename Scalar<MatrixJ>::type T;
  static_assert( std::is_same<typename FunctionalIntegrandBoundaryTrace::template MatrixJ<T>, MatrixJ>::value, "These should be the same");
  return {fcnJ.cast(), fcnBC.cast(), func_q, func_qp };
}

}

#endif  // JACOBIANFUNCTIONALBOUNDARYTRACE_WEIGHTEDRESIDUAL_VMSD_H
