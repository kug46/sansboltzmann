// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_VMSD_H
#define JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_VMSD_H

// boundary-trace integral residual functions


//#include "JacobianFunctionalBoundaryTrace_FieldTrace_Galerkin.h"
//#include "JacobianFunctionalBoundaryTrace_Galerkin.h"
#include "JacobianFunctionalBoundaryTrace_WeightedResidual_VMSD.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, template<class> class Vector, class MatrixJ>
class JacobianFunctionalBoundaryTrace_Dispatch_VMSD_impl
{
public:
  JacobianFunctionalBoundaryTrace_Dispatch_VMSD_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
      const int* quadratureorder, int ngroup,
      Vector< MatrixJ >& func_q,
      Vector< MatrixJ >& func_qp )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), qpfld_(qpfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      func_q_(func_q), func_qp_(func_qp)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
      JacobianFunctionalBoundaryTrace_WeightedResidual_VMSD<Surreal>( fcnOutput_, fcn.cast(), func_q_, func_qp_ ),
                            xfld_, (qfld_, qpfld_), quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qpfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector< MatrixJ >& func_q_;
  Vector< MatrixJ >& func_qp_;
};

// Factory function

template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_VMSD_impl<Surreal, FunctionalIntegrand, XFieldType, PhysDim, TopoDim,
                                                       ArrayQ, Vector, MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_VMSD(const FunctionalIntegrand& fcnOutput,
                                                  const FieldType<XFieldType>& xfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& qpfld,
                                                  const int* quadratureorder, int ngroup,
                                                  Vector< MatrixJ >& func_q,
                                                  Vector< MatrixJ >& func_qp )
{
  return {fcnOutput, xfld.cast(), qfld, qpfld, quadratureorder, ngroup, func_q, func_qp};
}


}

#endif //JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_VMSD_H
