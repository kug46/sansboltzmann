// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONDATA_VMSD_STABILIZED_H_
#define SOLUTIONDATA_VMSD_STABILIZED_H_

#include "Adaptation/MOESS/ParamFieldBuilder.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/ProjectSoln/ProjectConstTrace.h"
#include "Field/ProjectSoln/ProjectGlobalField.h"
#include "Field/ProjectSoln/InterpolateFunctionCell_Continuous.h"
#include "Discretization/VMSD/Discretization_VMSD.h"
#include "FieldBundle_VMSD.h"
#include "Field/output_Tecplot.h"

namespace SANS
{

template<class PhysDim, class TopoDim, class NDPDEClass, class ParamBuilderType>
struct SolutionData_VMSD
{
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef FieldBundle_VMSD<PhysDim, TopoDim, ArrayQ> FieldBundle;

  typedef ParamFieldBuilder<ParamBuilderType, PhysDim, TopoDim,
                            typename FieldBundle::QFieldType> ParamFieldBuilderType;
  typedef ParamFieldBuilder_Local<ParamBuilderType, PhysDim, TopoDim,
                                  typename FieldBundle::QFieldType> ParamFieldBuilderLocalType;

  typedef typename ParamFieldBuilderType::FieldType ParamFieldType;
  typedef Field_CG_Cell<PhysDim, TopoDim, Real> LiftedQuantityFieldType;

  template<class XFieldType>
  SolutionData_VMSD(const XFieldType& xfld_, const NDPDEClass& pde, const DiscretizationVMSD& disc,
                     const int primal_order, const int primal_porder, const int adjoint_order,  const int adjoint_porder,
                     const BasisFunctionCategory basis_cell,  const BasisFunctionCategory basis_dgcell, const BasisFunctionCategory basis_trace,
                     const std::vector<int>& mitlg_boundarygroups)
  : SolutionData_VMSD(xfld_, pde, disc, primal_order, primal_porder, adjoint_order, adjoint_porder,
                       basis_cell, basis_dgcell, basis_trace, mitlg_boundarygroups, PyDict()) {}

  template<class XFieldType>
  SolutionData_VMSD(const XFieldType& xfld_, const NDPDEClass& pde, const DiscretizationVMSD& disc,
                   const int primal_order, const int primal_porder, const int adjoint_order,  const int adjoint_porder,
                     const BasisFunctionCategory basis_cell, const BasisFunctionCategory basis_dgcell, const BasisFunctionCategory basis_trace,
                     const std::vector<int>& mitlg_boundarygroups, const PyDict& parambuilderDict)
  : xfld(get<-1>(xfld_)),
    pde(pde),
    primal(xfld, primal_order , disc.pOrder(primal_porder), basis_cell, basis_dgcell, basis_trace, mitlg_boundarygroups),
    adjoint(xfld, adjoint_order , disc.pOrder(adjoint_porder), basis_cell, basis_dgcell, basis_trace, mitlg_boundarygroups),
    mitlg_boundarygroups(mitlg_boundarygroups),
    parambuilder(xfld_, primal.qfld, parambuilderDict),
    paramfld(parambuilder.fld),
    disc(disc)
  {
  }

  void setSolution(const typename FieldBundle::ArrayQ& q)
  {
    primal.qfld = q;
    primal.qpfld = 0;
    primal.lgfld = 0;
  }

  template <class SolutionFunctionType>
  void setSolution(const SolutionFunctionType& fcn, const std::vector<int>& cellGroups)
  {
    for_each_CellGroup<TopoDim>::apply( InterpolateFunctionCell_Continuous(fcn, cellGroups), (xfld, primal.qfld) );
    primal.qpfld = 0;
  }

  void setSolution(const SolutionData_VMSD& solFrom, const bool& isP1 = false)
  {
    if (&xfld == &solFrom.xfld)
    {
      solFrom.primal.qfld.projectTo(primal.qfld);
      solFrom.primal.qpfld.projectTo(primal.qpfld);
    }
    else //need to project between meshes
    {
      if (isP1)  //P1 solution projection is more robust
      {
        Field_CG_Cell<PhysDim, TopoDim, ArrayQ> qfldP1( xfld, 1, solFrom.primal.basis_cgcell, EmbeddedCGField );
        ProjectGlobalField(solFrom.primal.qfld, qfldP1);
        qfldP1.projectTo(primal.qfld);
      }
      else
      {
        ProjectGlobalField(solFrom.primal.qfld, primal.qfld);
      }
      primal.qpfld = 0;
    }
  }

  void setSolution(const typename FieldBundle::QFieldType& qfld )
  {
    primal.qpfld = 0;
    if (&xfld == &qfld.getXField())
    {
      qfld.projectTo(primal.qfld);
    }
    else //need to project between meshes
    {
      ProjectGlobalField(qfld, primal.qfld);
    }
  }

  // set solution on boundaryGroup - Needs to be called before essential bcs get applied
  void setBoundarySolution( const typename FieldBundle::ArrayQ& q, const std::vector<int>& boundaryGroups )
  {
    // Assign the value to the trace
    for_each_BoundaryTraceGroup<TopoDim>::apply( ProjectConstTrace<PhysDim>(q, boundaryGroups), *this );
  }

  template <class SolutionFunctionType>
  void setBoundarySolution( const SolutionFunctionType& fcn, const std::vector<int>& boundaryGroups )
  {
    // Assign the value to the trace
    for_each_BoundaryTraceGroup<TopoDim>::apply( InterpolateFunctionTrace_Continuous(fcn,boundaryGroups), (xfld, primal.qfld) );
  }

  void createLiftedQuantityField(const int order, const BasisFunctionCategory& category)
  {
    orderLiftedQuantity = order;
    basisCategoryLiftedQuantity = category;
    pliftedQuantityfld = std::make_shared<LiftedQuantityFieldType>(xfld, orderLiftedQuantity, basisCategoryLiftedQuantity);
  }

  void dumpPrimalSolution( const std::string filebase, const std::string tag) const
  {
    output_Tecplot( primal.qfld, filebase + "qfld" + tag );
    output_Tecplot( primal.qpfld, filebase + "qpfld" + tag );
  }

  const XField<PhysDim, TopoDim>& xfld;

  static constexpr SpaceType spaceType = SpaceType::Continuous;

  const NDPDEClass& pde;

  // solution fields
  FieldBundle primal;

  // adjoint fields in richer space (P+1)
  FieldBundle adjoint;

  //lifted quantity field (i.e. shock sensor field)
  std::shared_ptr<LiftedQuantityFieldType> pliftedQuantityfld;
  int orderLiftedQuantity = 1;
  BasisFunctionCategory basisCategoryLiftedQuantity = BasisFunctionCategory_Hierarchical;

  std::vector<int> mitlg_boundarygroups;

  ParamFieldBuilderType parambuilder;
  const ParamFieldType& paramfld; //parameter fields

  const DiscretizationVMSD& disc;

};

}

#endif /* SOLUTIONDATA_VMSD_STABILIZED_H_ */
