// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef COMPUTEPERIMETER_H
#define COMPUTEPERIMETER_H

// boundary-trace integral residual functions


#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Field/FieldData/FieldDataReal_Cell.h"

#include "tools/Tuple.h"

namespace SANS
{

template <class PhysDim, class ArrayQ >
class ComputePerimeter_BoundaryTrace : public GroupIntegralBoundaryTraceType<ComputePerimeter_BoundaryTrace<PhysDim, ArrayQ>>
{
public:
  ComputePerimeter_BoundaryTrace( FieldDataReal_Cell& cellPerimeter,
                                  const std::vector<int>& boundaryTraceGroups   ) :
    cellPerimeter_(cellPerimeter),
    boundaryTraceGroups_(boundaryTraceGroups) {}

  std::size_t nBoundaryGroups() const          { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n];     }

  //----------------------------------------------------------------------------//
    // A function for checking the correct size of the residual vectors
    template <class TopoDim>
    void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
    {}

  //----------------------------------------------------------------------------//
    // Integration function that integrates each element in the boundary trace group
    template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
    void
    integrate( const int cellGroupGlobalL,
               const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
               const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
               const int traceGroupGlobal,
               const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
               int quadratureorder )
    {
      typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;

      typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;

      typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
      typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

      // element field variables
      ElementXFieldClassL xfldElemL( xfldCellL.basis() );
      ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

      // loop over elements within group
      int nelem = xfldTrace.nElem();
      for (int elem = 0; elem < nelem; elem++)
      {
        const int elemL = xfldTrace.getElementLeft( elem );

        xfldTrace.getElement( xfldElemTrace, elem );
        xfldCellL.getElement( xfldElemL, elemL );

        cellPerimeter_.getCellGroupGlobal(cellGroupGlobalL)[elem] += xfldElemTrace.jacobianDeterminant()
                                                                      /get<-1>(xfldElemL).jacobianDeterminant();

      }
    }

protected:
  FieldDataReal_Cell& cellPerimeter_;
  const std::vector<int>& boundaryTraceGroups_;
};


template <class PhysDim, class ArrayQ >
class ComputePerimeter_InteriorTrace : public GroupIntegralInteriorTraceType<ComputePerimeter_InteriorTrace<PhysDim, ArrayQ>>
{
public:
  ComputePerimeter_InteriorTrace( FieldDataReal_Cell& cellPerimeter,
                                  const std::vector<int>& interiorTraceGroups   ) :
                                    cellPerimeter_(cellPerimeter),
                                    interiorTraceGroups_(interiorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const          { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n];     }

  std::size_t nPeriodicTraceGroups() const { SANS_DEVELOPER_EXCEPTION("PERIODIC TRACES NOT IMPLEMENTED"); return 0; }

  std::size_t periodicTraceGroup(const int n) const
  {
     SANS_DEVELOPER_EXCEPTION("PERIODIC TRACES NOT IMPLEMENTED");
     return interiorTraceGroups_[n];
  }

  //----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {}

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      xfldCellL.getElement( xfldElemL, elemL );
      xfldCellR.getElement( xfldElemR, elemR );

      cellPerimeter_.getCellGroupGlobal(cellGroupGlobalL)[elemL] += xfldElemTrace.jacobianDeterminant()
                                                                          /get<-1>(xfldElemL).jacobianDeterminant();


      cellPerimeter_.getCellGroupGlobal(cellGroupGlobalR)[elemR] += xfldElemTrace.jacobianDeterminant()
                                                                          /get<-1>(xfldElemR).jacobianDeterminant();

    }
  }

protected:
  FieldDataReal_Cell& cellPerimeter_;
  const std::vector<int>& interiorTraceGroups_;
};


}

#endif  // COMPUTEPERIMETER_H
