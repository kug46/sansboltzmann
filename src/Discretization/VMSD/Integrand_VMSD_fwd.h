// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRAND_VMSD_FWD_H
#define INTEGRAND_VMSD_FWD_H

namespace SANS
{

//----------------------------------------------------------------------------//
// Tag to indicate VMSD type integrands

class VMSD {};

//----------------------------------------------------------------------------//
// Cell integrand: VMSD

template <class PDE>
class IntegrandCell_VMSD;

template <class PDE>
class IntegrandCell_VMSD_NL;
//----------------------------------------------------------------------------//
// Trace integrand: VMSD

template <class PDE>
class IntegrandTrace_VMSD;

template <class PDE>
class IntegrandTrace_VMSD_NL;


}

#endif // INTEGRAND_VMSD_FWD_H
