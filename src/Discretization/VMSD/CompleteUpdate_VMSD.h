// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef COMPLETEUPDATE_VMSD_H
#define COMPLETEUPDATE_VMSD_H

// Cell integral residual functions

#include <map>

#include "ComputePerimeter.h"
#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/CellToTraceAssociativity.h"
#include "Field/FieldData/FieldDataMatrixD_Cell.h"

#include "Discretization/QuadratureOrder.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateGhostBoundaryTraceGroups.h"
#include "ResidualInteriorTrace_VMSD.h"


namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral
//

template<class IntegrandCell, template<class> class Vector>
class CompleteUpdate_VMSD_impl :
    public GroupIntegralCellType< CompleteUpdate_VMSD_impl<IntegrandCell, Vector> >
{
public:

  typedef typename IntegrandCell::PDE PDE;
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;

  CompleteUpdate_VMSD_impl( const IntegrandCell& fcnCell,
                            const FieldDataMatrixD_Cell<MatrixQ>& mtxCompleteResidual,
                            const Vector<ArrayQ>& rsdPDEpGlobal, Vector<ArrayQ>& dq, Vector<ArrayQ>& dqp) :
                           fcnCell_(fcnCell),
                           mtxCompleteResidual_(mtxCompleteResidual), rsdPDEpGlobal_(rsdPDEpGlobal), dq_(dq), dqp_(dqp),
                           comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
    SANS_ASSERT( rsdPDEpGlobal_.m() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );

    SANS_ASSERT( dqp_.m() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
    SANS_ASSERT( dq_.m() == qfld.nDOFpossessed() + qfld.nDOFghost()  );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                            ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qpfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );

    // number of integrals evaluated
    const int qDOF = qfldElem.nDOF();
    const int qDOFp = qpfldElem.nDOF();

//    const int nDOFpossessed = dq_.m();
    const int nDOFppossessed = dqp_.m();

    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == qpfldCell.nElem() );

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( qDOF, -1 );
    std::vector<int> mapDOFpGlobal( qDOFp, -1 );

    // residual array
    DLA::VectorD<ArrayQ> rsdPDEpElem( qDOFp );
    DLA::VectorD<ArrayQ> dqElem( qDOF );
    DLA::VectorD<ArrayQ> dqpElem( qDOFp );

    // loop over elements within group and solve for the auxiliary variables first
    for (int elem = 0; elem < nelem; elem++)
    {

      // scatter-add element integral to global
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), qDOF );
      qpfldCell.associativity( elem ).getGlobalMapping( mapDOFpGlobal.data(), qDOFp );

      if ( mapDOFpGlobal[0] >= nDOFppossessed) continue;

      // copy global grid/solution DOFs to element
//      xfldCell.getElement( xfldElem, elem );
//      qfldCell.getElement( qfldElem, elem );
//      qpfldCell.getElement( qpfldElem, elem );

      //-----------------------------------------------------------------------------------------

      for (int n = 0; n < qDOFp; n++)
      {
        rsdPDEpElem[n] = rsdPDEpGlobal_[mapDOFpGlobal[n]];
      }

      for (int n = 0; n < qDOF; n++)
      {
        dqElem[n] = dq_[mapDOFGlobal[n]];
      }

      dqpElem = rsdPDEpElem - mtxCompleteResidual_.getCellGroupGlobal(cellGroupGlobal)[elem]*dqElem;

      for (int n = 0; n < qDOFp; n++)
      {
        dqp_[mapDOFpGlobal[n]] = dqpElem[n];
      }

    } //elem loop

  }

protected:
  const IntegrandCell& fcnCell_;
  const FieldDataMatrixD_Cell<MatrixQ>& mtxCompleteResidual_;

  const Vector<ArrayQ>& rsdPDEpGlobal_;
  Vector<ArrayQ>& dq_;
  Vector<ArrayQ>& dqp_;

  mutable int comm_rank_;
};

// Factory function

template<class IntegrandCell, template<class> class Vector, class ArrayQ, class MatrixQ>
CompleteUpdate_VMSD_impl<IntegrandCell, Vector>
CompleteUpdate_VMSD( const IntegrandCellType<IntegrandCell>& fcnCell, const FieldDataMatrixD_Cell<MatrixQ>& mtxCompleteResidual,
                     const Vector<ArrayQ>& rsdPDEpGlobal, Vector<ArrayQ>& dq, Vector<ArrayQ>& dqp )
{
  return { fcnCell.cast(), mtxCompleteResidual, rsdPDEpGlobal, dq, dqp };
}

}

#endif  // COMPLETEUPDATE_VMSD_H
