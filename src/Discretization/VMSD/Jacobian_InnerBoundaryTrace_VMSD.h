// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIAN_INNERBOUNDARYTRACE_VMSD_H
#define JACOBIAN_INNERBOUNDARYTRACE_VMSD_H

// HDG trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/CellToTraceAssociativity.h"

#include "Field/FieldData/FieldDataReal_Cell.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateCellGroups.h"
#include "JacobianCell_VMSD_Element.h"



namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary trace group integral without lagrange multipliers
//

template<class Surreal, class IntegrandITrace>
class Jacobian_InnerBoundaryTrace_VMSD_impl :
    public GroupIntegralBoundaryTraceType< Jacobian_InnerBoundaryTrace_VMSD_impl<Surreal, IntegrandITrace> >
{
public:
  typedef typename IntegrandITrace::PhysDim PhysDim;
  typedef typename IntegrandITrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  Jacobian_InnerBoundaryTrace_VMSD_impl(const IntegrandITrace& fcnITrace,
                                  const std::map<int,std::vector<int>>& cellBTraceGroups,
                                  const int& cellgroup, const int& cellelem,
                                  DLA::MatrixD<MatrixQ>& mtxElemPDE_q,
                                  DLA::MatrixD<MatrixQ>& mtxElemPDE_qp,
                                  DLA::MatrixD<MatrixQ>& mtxElemPDEp_q,
                                  DLA::MatrixD<MatrixQ>& mtxElemPDEp_qp ) :
    fcnITrace_(fcnITrace), cellBTraceGroups_(cellBTraceGroups),
    cellgroup_(cellgroup), cellelem_(cellelem),
    mtxElemPDE_q_(mtxElemPDE_q), mtxElemPDE_qp_(mtxElemPDE_qp),
    mtxElemPDEp_q_(mtxElemPDEp_q), mtxElemPDEp_qp_(mtxElemPDEp_qp)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellBTraceGroups_.begin(); it != cellBTraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nBoundaryGroups() const { return traceGroupIndices_.size(); }
  std::size_t boundaryGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the matrices
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    //CHECKS NOT APPLICABLE

//    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
//    const Field<PhysDim, TopoDim, ArrayQ>& qpfld = get<1>(flds);
//
//    SANS_ASSERT( mtxElemPDE_q_.m() == qfld.nDOF() );
//    SANS_ASSERT( mtxElemPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDE_qp_.m() == qfld.nDOF() );
//    SANS_ASSERT( mtxElemPDE_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDEp_q_.m() == qpfld.nDOF() );
//    SANS_ASSERT( mtxElemPDEp_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
//
//    SANS_ASSERT( mtxElemPDEp_qp_.m() == qpfld.nDOF() );
//    SANS_ASSERT( mtxElemPDEp_qp_.n() == qpfld.nDOFpossessed() + qpfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<>        ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;
    typedef JacobianElemCell_VMSD<PhysDim, MatrixQ> JacobianElemInteriorTraceType;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& qpfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldSurrealClassL qpfldElemL( qpfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFpL = qpfldElemL.nDOF();

//    const int nIntegrandL = nDOFL;
//    const int nIntegrandPL = nDOFpL;

    // element integral
//    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType> integralCoarse(quadratureorder, nDOFL);
//    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType> integralFine(quadratureorder, nDOFpL);
//
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType > integral(quadratureorder);

    // element integrand/residual
    JacobianElemInteriorTraceType mtxElemL(nDOFL, nDOFpL);

    const std::vector<int>& traceElemList = cellBTraceGroups_.at(traceGroupGlobal);
    // loop over elements within group
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];
      const int elemL = xfldTrace.getElementLeft( elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        qpfldCellL.getElement( qpfldElemL, elemL );

        xfldTrace.getElement( xfldElemTrace, elem );

        // PDE trace integration for canonical element
        integral( fcnITrace_.integrand(xfldElemTrace, canonicalTraceL, +1, xfldElemL, qfldElemL, qpfldElemL),
                          xfldElemTrace, mtxElemL );

        mtxElemPDE_q_ += mtxElemL.PDE_q;
        mtxElemPDE_qp_ += mtxElemL.PDE_qp;
        mtxElemPDEp_q_ += mtxElemL.PDEp_q;
        mtxElemPDEp_qp_ += mtxElemL.PDEp_qp;
      }
    }

  }



protected:
  const IntegrandITrace& fcnITrace_;
  const std::map<int,std::vector<int>>& cellBTraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;

  MatrixElemClass& mtxElemPDE_q_; //jacobian of PDE residual wrt q for the central cell
  MatrixElemClass& mtxElemPDE_qp_; //jacobian of PDE residual wrt q for the central cell
  MatrixElemClass& mtxElemPDEp_q_; //jacobian of PDE residual wrt q for the central cell
  MatrixElemClass& mtxElemPDEp_qp_; //jacobian of PDE residual wrt q for the central cell

  std::vector<int> traceGroupIndices_;
};

// Factory function

template<class Surreal, class IntegrandITrace, class MatrixQ>
Jacobian_InnerBoundaryTrace_VMSD_impl<Surreal, IntegrandITrace>
Jacobian_InnerBoundaryTrace_VMSD( const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                           const std::map<int,std::vector<int>>& cellBTraceGroups,
                           const int& cellgroup, const int& cellelem,
                           DLA::MatrixD<MatrixQ>& mtxElemPDE_q,
                           DLA::MatrixD<MatrixQ>& mtxElemPDE_qp,
                           DLA::MatrixD<MatrixQ>& mtxElemPDEp_q,
                           DLA::MatrixD<MatrixQ>& mtxElemPDEp_qp)
{
  return { fcnITrace.cast(), cellBTraceGroups, cellgroup, cellelem,
           mtxElemPDE_q, mtxElemPDE_qp,
           mtxElemPDEp_q, mtxElemPDEp_qp };
}

}

#endif  // JACOBIAN_INNERBOUNDARYTRACE_VMSD_H
