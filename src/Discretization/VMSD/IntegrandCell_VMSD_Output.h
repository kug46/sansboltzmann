// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_VMSD_OUTPUT
#define INTEGRANDCELL_VMSD_OUTPUT

#include <vector>

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"

#define VMSDNLOUTPUT

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand for generic output functional


template <class OutputFunctional, class PDE>
class IntegrandCell_VMSD_Output :
    public IntegrandCellType< IntegrandCell_VMSD_Output<OutputFunctional, PDE> >
{
public:
  typedef typename OutputFunctional::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = typename OutputFunctional::template ArrayJ<T>;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = typename OutputFunctional::template MatrixJ<T>;

  // cppcheck-suppress noExplicitConstructor
  IntegrandCell_VMSD_Output(const PDE& pde, const OutputFunctional& outputFcn, const std::vector<int>& cellGroups,
                           const bool sum = false ) :
    pde_(pde), outputFcn_(outputFcn), cellGroups_(cellGroups), sum_(sum)
  {
    // Nothing
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  const OutputFunctional& getOutputFcn() const {return outputFcn_;}

  template<class T, class TopoDim, class Topology, class ElementParam>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim  , TopoDim, Topology> ElementXFieldType;
    typedef Element      <ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element      <Real     , TopoDim, Topology> ElementEFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;
    typedef typename ElementParam::gradT ParamGradT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;
    static const int N = DLA::VectorSize<ArrayQ<T>>::M;
    static const int nOut = DLA::VectorSize<ArrayJ<T>>::M;

    Functor( const PDE& pde,
             const OutputFunctional& outputFcn,
             const bool sum,
             const ElementParam& paramfldElem,
             const ElementQFieldType& qfldElem,
             const ElementQFieldType& qpfldElem) :
      pde_(pde),
      outputFcn_(outputFcn),
      sum_(sum),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
      qfldElem_(qfldElem),
      qpfldElem_(qpfldElem),
      nDOF_(qfldElem.nDOF()),
      nDOFp_(qpfldElem.nDOF()),
      phi_( new Real[nDOF_] ),
      phip_( new Real[nDOFp_] ),
      gradphi_( new VectorX[nDOF_] ),
      gradphip_( new VectorX[nDOFp_] )
    {
    }

    Functor( Functor&& f ) :
      pde_(f.pde_),
      outputFcn_(f.outputFcn_),
      sum_(f.sum_),
      paramfldElem_(f.paramfldElem_),
      xfldElem_(f.xfldElem_),
      qfldElem_(f.qfldElem_),
      qpfldElem_(f.qpfldElem_),
      nDOF_(f.nDOF_),
      nDOFp_(f.nDOFp_),
      phi_( f.phi_ ),
      phip_( f.phip_ ),
      gradphi_( f.gradphi_ ),
      gradphip_( f.gradphip_ )
    {
      f.phi_  = nullptr; f.gradphi_  = nullptr;
      f.phip_ = nullptr; f.gradphip_ = nullptr;
    }

    ~Functor()
    {
      delete [] phi_;
      delete [] phip_;
      delete [] gradphi_;
      delete [] gradphip_;
    }

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // element output integrand
    void operator()( const QuadPointType& sRef, ArrayJ<T>& integrand ) const;

    // element output integrand
    void operator()(const Real dJ, const QuadPointType& sRef,
                    DLA::VectorD<MatrixJ<Real>>& mtxPDEElem, DLA::VectorD<MatrixJ<Real>>& mtxPDEElemp ) const;

  protected:
    template <class Tq, class Tqp, class Tg, class Tgp, class Ti>
    void evalIntegrand( const ParamT& param,
                            const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
                            const ArrayQ<Tqp>& qp, const VectorArrayQ<Tgp>& gradqp,
                            ArrayJ<Ti>& integrand ) const;

    const PDE& pde_;
    const OutputFunctional& outputFcn_;
    const bool sum_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementQFieldType& qpfldElem_;
    const int nDOF_;
    const int nDOFp_;
    mutable Real *phi_;
    mutable Real *phip_;
    mutable VectorX *gradphi_;
    mutable VectorX *gradphip_;

  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  Functor<T,TopoDim,Topology,ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qpfldElem) const
  {
    return Functor<T,TopoDim,Topology,ElementParam>(pde_, outputFcn_, sum_, paramfldElem, qfldElem, qpfldElem);
  }

private:
  const PDE& pde_;
  const OutputFunctional& outputFcn_;
  const std::vector<int> cellGroups_;
  const bool sum_;
};



template<class OutputFunctional, class PDE>
template<class T, class TopoDim, class Topology, class ElementParam>
template <class Tq, class Tqp, class Tg, class Tgp, class Ti>
void
IntegrandCell_VMSD_Output<OutputFunctional, PDE>::
Functor<T, TopoDim, Topology, ElementParam>::
evalIntegrand( const ParamT& param,
               const ArrayQ<Tq>& q, const VectorArrayQ<Tg>& gradq,
               const ArrayQ<Tqp>& qp, const VectorArrayQ<Tgp>& gradqp,
               ArrayJ<Ti>& integrand ) const
{
#ifdef VMSDNLOUTPUT //if it's non-linear just add it up
  ArrayQ<Ti> qtot = q + qp;
  VectorArrayQ<Ti> gradqtot = gradq + gradqp;
  outputFcn_(param, qtot, gradqtot, integrand);
#else
  if (sum_) //only appropriate for things like L2..
  {
    //shouldn't really be doing this for gradqp;
    ArrayQ<Ti> qtot = q + qp;
    VectorArrayQ<Ti> gradqtot = gradq + gradqp;
    outputFcn_(param, qtot, gradqtot, integrand);
  }
  else
  {
    //shouldn't really be doing this for gradqp;
    VectorArrayQ<Ti> gradqtot = gradq + gradqp;
    outputFcn_(param, q, gradqtot, integrand);

    MatrixJ<Ti> dJdu = 0;
    outputFcn_.outputJacobian(param, q, gradq, dJdu);

    ArrayQ<Ti> du = 0;
    pde_.perturbedMasterState(param, q, qp, du);

    integrand += dot(dJdu,du);
  }
#endif

}


// element output integrand
template<class OutputFunctional, class PDE>
template<class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_VMSD_Output<OutputFunctional, PDE>::Functor<T,TopoDim,Topology,ElementParam>::
operator()( const QuadPointType& sRef, ArrayJ<T>& integrand ) const
{
  VectorX X;       // physical coordinates
  ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q = 0;               // solution
  ArrayQ<T> qp = 0;               // solution
  VectorArrayQ<T> gradq = 0;     // gradient
  VectorArrayQ<T> gradqp = 0;     // gradient

  const bool needsSolutionGradient = outputFcn_.needsSolutionGradient();

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // solution value, gradient
  qfldElem_.eval( sRef, q );
  qpfldElem_.eval( sRef, qp );

  if (needsSolutionGradient)
  {
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );
    xfldElem_.evalGradient( sRef, qpfldElem_, gradqp );
  }

  evalIntegrand(param, q, gradq, qp, gradqp, integrand);

}

// element output integrand
template<class OutputFunctional, class PDE>
template<class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_VMSD_Output<OutputFunctional, PDE>::Functor<T,TopoDim,Topology,ElementParam>::
operator()(const Real dJ, const QuadPointType& sRef,
           DLA::VectorD<MatrixJ<Real>>& mtxPDEElem,
           DLA::VectorD<MatrixJ<Real>>& mtxPDEElemp ) const
{
  typedef SurrealS<N> SurrealClass;
  static const int nDeriv = SurrealClass::N;

  VectorX X;       // physical coordinates
  ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q = 0;              // solution
  VectorArrayQ<Real> gradq = 0;    // gradient
  ArrayQ<Real> qp = 0;
  VectorArrayQ<Real> gradqp = 0;    // gradient

  ArrayQ<SurrealClass> qSurreal;              // solution
  VectorArrayQ<SurrealClass> gradqSurreal;    // gradient
  ArrayQ<SurrealClass> qpSurreal;              // solution
  VectorArrayQ<SurrealClass> gradqpSurreal;    // gradient

  const bool needsSolutionGradient = outputFcn_.needsSolutionGradient();

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  qpfldElem_.evalBasis( sRef, phip_, nDOFp_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qpfldElem_.evalFromBasis( phip_, nDOFp_, qp );

  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qpfldElem_, gradphip_, nDOFp_ );
  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    qpfldElem_.evalFromBasis( gradphip_, nDOFp_, gradqp );
  }

  //surrealize
  qSurreal = q;
  qpSurreal = qp;
  gradqSurreal = gradq;
  gradqpSurreal = gradqp;

  // element integrand/residual
  ArrayJ<SurrealClass> integrandSurreal;
  MatrixJ<Real> J_q =0, J_gradq = 0; // temporary storage
  MatrixJ<Real> J_qp =0, J_gradqp = 0; // temporary storage

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables
    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += N;

    integrandSurreal = 0;
    evalIntegrand(param, qSurreal, gradq, qp, gradqp, integrandSurreal);

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < N; n++)
      {
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative
      }

      // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
      for (int m = 0; m < nOut; m++)
        for (int n = 0; n < N; n++)
        {
          // Note that n,m are transposed intentionally
          DLA::index(J_q,n,m) = DLA::index(integrandSurreal, m).deriv(slot + n - nchunk);
        }

      for (int j = 0; j < nDOF_; j++)
        mtxPDEElem[j] += dJ*phi_[j]*J_q;

    }
    slot += N;
  } // nchunk


  if (needsSolutionGradient)
  {
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < N; n++)
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 1;
        slot += N;
      }

      integrandSurreal = 0;

      evalIntegrand(param, q, gradqSurreal, qp, gradqp, integrandSurreal);


      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < N; n++)
          {
            DLA::index(gradqSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative
          }

          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < nOut; m++)
            for (int n = 0; n < N; n++)
            {
              // Note that n,m are transposed intentionally
              DLA::index(J_gradq,n,m) = DLA::index(integrandSurreal, m).deriv(slot + n - nchunk);
            }

          for (int j = 0; j < nDOF_; j++)
            mtxPDEElem[j] += dJ*gradphi_[j][d]*J_gradq;

        }
        slot += N;
      }
    } // nchunk
  }

  // WRT qp, gradqp

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables
    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < N; n++)
        DLA::index(qpSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += N;

    integrandSurreal = 0;
    evalIntegrand(param, q, gradq, qpSurreal, gradqp, integrandSurreal);

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < N; n++)
      {
        DLA::index(qpSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative
      }

      // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
      for (int m = 0; m < nOut; m++)
        for (int n = 0; n < N; n++)
        {
          // Note that n,m are transposed intentionally
          DLA::index(J_qp,n,m) = DLA::index(integrandSurreal, m).deriv(slot + n - nchunk);
        }

      for (int j = 0; j < nDOFp_; j++)
        mtxPDEElemp[j] += dJ*phip_[j]*J_qp;

    }
    slot += N;
  } // nchunk


  if (needsSolutionGradient)
  {
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < N; n++)
            DLA::index(gradqpSurreal[d], n).deriv(slot + n - nchunk) = 1;
        slot += N;
      }

      integrandSurreal = 0;
      evalIntegrand(param, q, gradq, qp, gradqpSurreal, integrandSurreal);

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < N; n++)
          {
            DLA::index(gradqpSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative
          }

          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < nOut; m++)
            for (int n = 0; n < N; n++)
            {
              // Note that n,m are transposed intentionally
              DLA::index(J_gradqp,n,m) = DLA::index(integrandSurreal, m).deriv(slot + n - nchunk);
            }

          for (int j = 0; j < nDOFp_; j++)
            mtxPDEElemp[j] += dJ*gradphip_[j][d]*J_gradqp;

        }
        slot += N;
      }
    } // nchunk
  }

}


}

#endif //INTEGRANDCELL_VMSD_OUTPUT
