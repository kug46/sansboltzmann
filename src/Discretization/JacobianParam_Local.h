// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANPARAM_LOCAL_H_
#define JACOBIANPARAM_LOCAL_H_

#include <map>

#include <boost/mpl/begin_end.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/next.hpp>

#include "tools/SANSnumerics.h"

#include "JacobianParam.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class iParams, class AESClass_Local>
class JacobianParam_Local
  : public JacobianParam<iParams, typename AESClass_Local::BaseType > //Base type should be the globa AES class
{
public:
  typedef JacobianParam<iParams, typename AESClass_Local::BaseType > BaseType;

  typedef typename BaseType::TraitsType TraitsType;

  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;
  typedef typename TraitsType::VectorSizeClass VectorSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  // Constructor
  //cppcheck-suppress noExplicitConstructor
  JacobianParam_Local( AESClass_Local& AES, const std::map<int,int>& columnMap ) :
    BaseType(AES, columnMap), AES_(AES)
  {}

  virtual ~JacobianParam_Local();

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& jac_sub       ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz_sub) override {};

protected:
  MatrixSizeClass fullMatrixSize(const SystemMatrix& jac_sub) const;

  void subJacobian(const SystemMatrix& jac_full, SystemMatrixView& jac_sub);

  AESClass_Local& AES_;
  using BaseType::columnMap_;
};


// Destructor
template<class iParams, class AESClass_Local>
JacobianParam_Local<iParams, AESClass_Local >::
~JacobianParam_Local() {}

//Fills jacobian or the non-zero pattern of a jacobian
template<class iParams, class AESClass_Local>
void
JacobianParam_Local<iParams, AESClass_Local >::
jacobian(SystemMatrixView& jac_sub)
{
  //Jacobian 'nonzero' pattern for dense matrix
  SystemNonZeroPattern nz_full(fullMatrixSize(jac_sub));

  //Compute the full Jacobian of the local problem
  SystemMatrix jac_full(nz_full);
  jac_full = 0;
  BaseType::jacobian(jac_full);

  //Extract the sub Jacobian from the full Jacobian, containing only the equations that need to be solved
  subJacobian(jac_full, jac_sub);

//  if (AESParam_ != nullptr) AESParam_->jacobian(mtx);
//  jacobian<SystemMatrix&>(mtx, quadratureOrder_ );
}

template<class iParams, class AESClass_Local>
void
JacobianParam_Local<iParams, AESClass_Local>::
subJacobian(const SystemMatrix& jac_full, SystemMatrixView& jac_sub)
{
  //Extract the sub-matrix from the full matrix

  // Loop over equations, i.e. iPDE, iBC
  for ( int iEq = 0; iEq < jac_sub.m(); iEq++ )
  {
    // Loop over variables, i.e. iq, ilg
    for ( int iVar = 0; iVar < jac_sub.n(); iVar++)
    {
      const int nRsd = jac_sub(iEq,iVar).m();
      const int nDOF = jac_sub(iEq,iVar).n();

      // Loop over residual equations and DOFs
      // The sub matrix is always stores as the first set residuals and DOFs
      for (int i = 0; i < nRsd; i++)
        for (int j = 0; j < nDOF; j++)
          jac_sub(iEq,iVar)(i,j) = jac_full(iEq,iVar)(i,j);
    }
  }
}

//===========================================================================//
namespace detail
{

// Looping class: Get the current BC type check if it matches the BC parameter
template < class iter, class end >
struct LocalFullJacobianParamSizeIterator
{
  template<class ParamFieldType, class VectorSizeClass, class MatrixSizeClass>
  static void set( const ParamFieldType& paramfld,
                   const VectorSizeClass& rowSize,
                   const std::map<int,int>& columnMap,
                   MatrixSizeClass& size )
  {
    typedef typename boost::mpl::deref<iter>::type iParam;

    // Get the total DOFs for the paramete field
    const int nDOFParam = get<iParam::value>(paramfld).nDOF();

    // This will only fill in sizes that exist in the column map, but we don't need anything more
    // for the local solve.
    const int ip = columnMap.at(iParam::value);

    // Set the matrix size for the current column
    for (int i = 0; i < rowSize.m(); i++)
      size(i,ip) = {rowSize[i].m(), nDOFParam};

    // Iterate recursively to the next option
    typedef typename boost::mpl::next<iter>::type nextiter;
    LocalFullJacobianParamSizeIterator< nextiter, end>::set(paramfld, rowSize, columnMap, size);
  }
};

// Recursive loop terminates when 'iter' is the same as 'end'
template < class end >
struct LocalFullJacobianParamSizeIterator< end, end >
{
  template<class ParamFieldType, class VectorSizeClass, class MatrixSizeClass>
  static void set( const ParamFieldType& paramfld,
                   const VectorSizeClass& rowSize,
                   const std::map<int,int>& columnMap,
                   MatrixSizeClass& size )
  {
  }
};

} // namespace detail

template<class iParams, class AESClass_Local>
typename JacobianParam_Local<iParams, AESClass_Local>::MatrixSizeClass
JacobianParam_Local<iParams, AESClass_Local >::
fullMatrixSize(const SystemMatrix& jac_sub) const
{
  // Create the size that represents the full linear algebra system of the local mesh (i.e. all elements)

  MatrixSizeClass size( jac_sub.m(), jac_sub.n() );

  VectorSizeClass rowSize( AES_.fullVectorSize() );

  detail::LocalFullJacobianParamSizeIterator< typename boost::mpl::begin< iParams >::type,
                                              typename boost::mpl::end< iParams >::type
                                            >::set( AES_.paramfld(), rowSize, columnMap_, size );

  return size;
}

} //namespace SANS


#endif // JACOBIANPARAM_LOCAL_H_
