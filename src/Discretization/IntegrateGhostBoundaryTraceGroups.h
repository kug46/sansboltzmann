// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATEGHOSTBOUNDARYTRACEGROUPS_H
#define INTEGRATEGHOSTBOUNDARYTRACEGROUPS_H

// boundary-trace integral residual functions

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "GroupIntegral_Type.h"

namespace SANS
{


template <class TopologyTrace, class TopologyL, class TopoDim,
          class IntegralBoundaryTraceGroup,
          class XFieldType, class QFieldType>
void
IntegrateGhostBoundaryTraceGroups_Integral(
    IntegralBoundaryTraceGroup& integral,
    const XFieldType& xfld,
    const QFieldType& qfld,
    const int traceGroupGlobal,
    const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const int quadratureorder )
{
  const int cellGroupGlobalL = xfldTrace.getGroupLeft();

  // Integrate over the trace group
  integral.template integrate<TopologyTrace, TopologyL, TopoDim, XFieldType>(
      cellGroupGlobalL,
      xfld.template getCellGroup<TopologyL>(cellGroupGlobalL),
      qfld.template getCellGroupGlobal<TopologyL>(cellGroupGlobalL),
      traceGroupGlobal,
      xfldTrace,
      quadratureorder );
}


template<class TopDim>
class IntegrateGhostBoundaryTraceGroups;


template<>
class IntegrateGhostBoundaryTraceGroups<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  template <class TopologyTrace, class IntegralBoundaryTraceGroup,
            class XFieldType, class QFieldType>
  static void
  LeftTopology(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // determine topology for R
      IntegrateGhostBoundaryTraceGroups_Integral<TopologyTrace, Line, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template <class IntegralBoundaryTraceGroup, class XFieldType, class QFieldType>
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<QFieldType>& qfldType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT( ngroup == xfld.getXField().nGhostBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);

      if ( xfld.getXField().getGhostBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node>( integral,
                            xfld, qfld,
                            boundaryGroup,
                            xfld.getXField().template getGhostBoundaryTraceGroup<Node>(boundaryGroup),
                            quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class IntegrateGhostBoundaryTraceGroups<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class TopologyTrace, class IntegralBoundaryTraceGroup,
            class XFieldType, class QFieldType>
  static void
  LeftTopology(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      IntegrateGhostBoundaryTraceGroups_Integral<TopologyTrace, Triangle, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
    {
      // determine topology for R
      IntegrateGhostBoundaryTraceGroups_Integral<TopologyTrace, Quad, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template <class IntegralBoundaryTraceGroup, class XFieldType, class QFieldType>
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<QFieldType>& qfldType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT( ngroup == xfld.getXField().nGhostBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);
      if ( xfld.getXField().getGhostBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line>( integral,
                            xfld, qfld,
                            boundaryGroup,
                            xfld.getXField().template getGhostBoundaryTraceGroup<Line>(boundaryGroup),
                            quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};



template<>
class IntegrateGhostBoundaryTraceGroups<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

  template <class IntegralBoundaryTraceGroup, class XFieldType, class QFieldType>
  static void
  LeftTopology_Triangle(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      IntegrateGhostBoundaryTraceGroups_Integral<Triangle, Tet, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template <class IntegralBoundaryTraceGroup, class XFieldType, class QFieldType>
  static void
  LeftTopology_Quad(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      IntegrateGhostBoundaryTraceGroups_Integral<Quad, Hex, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template <class IntegralBoundaryTraceGroup, class XFieldType, class QFieldType>
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<QFieldType>& qfldType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT( ngroup == xfld.getXField().nGhostBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);

      if ( xfld.getXField().getGhostBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle( integral,
                               xfld, qfld,
                               boundaryGroup,
                               xfld.getXField().template getGhostBoundaryTraceGroup<Triangle>(boundaryGroup),
                               quadratureorder[boundaryGroup] );
      }
      else if ( xfld.getXField().getGhostBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad( integral,
                           xfld, qfld,
                           boundaryGroup,
                           xfld.getXField().template getGhostBoundaryTraceGroup<Quad>(boundaryGroup),
                           quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class IntegrateGhostBoundaryTraceGroups<TopoD4>
{
public:
  typedef TopoD4 TopoDim;

  template <class IntegralBoundaryTraceGroup, class XFieldType, class QFieldType>
  static void
  LeftTopology_Tet(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Pentatope) )
    {
      // determine topology for R
      IntegrateGhostBoundaryTraceGroups_Integral<Tet, Pentatope, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template <class IntegralBoundaryTraceGroup, class XFieldType, class QFieldType>
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<QFieldType>& qfldType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT( ngroup == xfld.getXField().nGhostBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);

      if ( xfld.getXField().getGhostBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Tet) )
      {
        LeftTopology_Tet( integral,
                          xfld, qfld,
                          boundaryGroup,
                          xfld.getXField().template getGhostBoundaryTraceGroup<Tet>(boundaryGroup),
                          quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

}

#endif  // INTEGRATEGHOSTBOUNDARYTRACEGROUPS_H
