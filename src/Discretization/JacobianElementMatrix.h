// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANELEMENTMATRIX_H
#define JACOBIANELEMENTMATRIX_H

#include "tools/SANSnumerics.h" // Real

// Simple lazy expression building framework for elemental Jacobian matrices

namespace SANS
{

template<class JacElemMatrix>
struct JacElemMatrixType
{
  const JacElemMatrix& cast() const { return static_cast<const JacElemMatrix&>(*this); }
};

template<class JacElemMatrix>
struct JacElemMulScalar
{
  JacElemMulScalar(
      const JacElemMatrix& mtx, const Real s) : mtx(mtx), s(s) {}

  const JacElemMatrix& mtx;
  const Real s;
};

template<class JacElemMatrix>
inline JacElemMulScalar<JacElemMatrix>
operator*(const Real& s, const JacElemMatrixType<JacElemMatrix>& mtx)
{
  return { mtx.cast(), s };
}

struct JacobianElemInteriorTraceSize
{
  JacobianElemInteriorTraceSize(const int nTest, const int nDOFL, const int nDOFR)
    : nTest(nTest), nDOFL(nDOFL), nDOFR(nDOFR) {}

  const int nTest;
  const int nDOFL;
  const int nDOFR;
};

struct JacobianElemTraceSizeVMSD
{
  JacobianElemTraceSizeVMSD(const int nDOFL, const int nDOFp)
    : nDOFL(nDOFL), nDOFp(nDOFp) {}

  const int nDOFL;
  const int nDOFp;
};

}

#endif // JACOBIANELEMENTMATRIX_H
