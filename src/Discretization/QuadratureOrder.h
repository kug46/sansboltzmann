// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QUADRATUREORDER_H
#define QUADRATUREORDER_H

#include <vector>

#include "Field/XField.h"

namespace SANS
{

struct QuadratureOrder
{

  QuadratureOrder( const QuadratureOrder& quadOrder, const std::vector<int>& order_inc ) :
    QuadratureOrder(quadOrder)
  {
    for (auto it = cellOrders.begin(); it != cellOrders.end(); ++it)
      if (*it != 0 )
        *it += order_inc[0];

    for (auto it = interiorTraceOrders.begin(); it != interiorTraceOrders.end(); ++it)
      if (*it != 0 )
        *it += order_inc[1];

    for (auto it = boundaryTraceOrders.begin(); it != boundaryTraceOrders.end(); ++it)
      if (*it != 0 )
        *it += order_inc[2];
  }

  template<class PhysDim, class TopoDim>
  QuadratureOrder( const XField<PhysDim, TopoDim>& xfld, const int cell_order, const int interior_order, const int boundary_order ) :
    cellOrders(xfld.nCellGroups(), cell_order),
    interiorTraceOrders(xfld.nInteriorTraceGroups(), interior_order),
    boundaryTraceOrders(xfld.nBoundaryTraceGroups(), boundary_order)
  {}

  template<class PhysDim, class TopoDim>
  QuadratureOrder( const XField<PhysDim, TopoDim>& xfld, const int order ) :
    QuadratureOrder(xfld, order, order, order)
  {}

  template<class PhysDim>
  QuadratureOrder( const XField<PhysDim, TopoD1>& xfld, const int order ) :
    cellOrders(xfld.nCellGroups(), order),
    interiorTraceOrders(xfld.nInteriorTraceGroups(), 0),
    boundaryTraceOrders(xfld.nBoundaryTraceGroups(), 0)
  {}

  std::vector<int> cellOrders;
  std::vector<int> interiorTraceOrders;
  std::vector<int> boundaryTraceOrders;
};

}

#endif //QUADRATUREORDER_H
