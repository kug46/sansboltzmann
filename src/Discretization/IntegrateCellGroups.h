// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATECELLGROUPS_H
#define INTEGRATECELLGROUPS_H

// Cell integral residual functions

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"

#include "GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class TopDim>
class IntegrateCellGroups;

// base class interface
template<>
class IntegrateCellGroups<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  template <class IntegralCell, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralCellType<IntegralCell>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], const int ngroup )
  {
    IntegralCell& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT_MSG( ngroup == xfld.nCellGroups(), "ngroup = %d, xfld.nCellGroups() = %d", ngroup, xfld.nCellGroups() );

    // loop over element groups
    for (std::size_t group = 0; group < integral.nCellGroups(); group++)
    {
      const int cellGroupGlobal = integral.cellGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Line) )
      {
        integral.template integrate<Line, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Line>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Line>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] ); // TODO: redundant TopoDim template?
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};


template<>
class IntegrateCellGroups<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class IntegralCell, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralCellType<IntegralCell>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], const int ngroup )
  {
    IntegralCell& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT_MSG( ngroup == xfld.nCellGroups(), "ngroup = %d, xfld.nCellGroups() = %d", ngroup, xfld.nCellGroups() );

    // loop over element groups
    for (std::size_t group = 0; group < integral.nCellGroups(); group++)
    {
      const int cellGroupGlobal = integral.cellGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        integral.template integrate<Triangle, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Triangle>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Triangle>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] );
      }
      else if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        integral.template integrate<Quad, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Quad>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Quad>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};



template<>
class IntegrateCellGroups<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

  template <class IntegralCell, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralCellType<IntegralCell>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], const int ngroup )
  {
    IntegralCell& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT_MSG( ngroup == xfld.nCellGroups(), "ngroup = %d, xfld.nCellGroups() = %d", ngroup, xfld.nCellGroups() );

    // loop over element groups
    for (std::size_t group = 0; group < integral.nCellGroups(); group++)
    {
      const int cellGroupGlobal = integral.cellGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        integral.template integrate<Tet, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Tet>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Tet>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] );
      }
      else if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Hex) )
      {
        integral.template integrate<Hex, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Hex>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Hex>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};

template<>
class IntegrateCellGroups<TopoD4>
{
public:
  typedef TopoD4 TopoDim;

  template <class IntegralCell, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralCellType<IntegralCell>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], const int ngroup )
  {
    IntegralCell& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT_MSG( ngroup == xfld.nCellGroups(), "ngroup = %d, xfld.nCellGroups() = %d", ngroup, xfld.nCellGroups() );

    // loop over element groups
    for (std::size_t group = 0; group < integral.nCellGroups(); group++)
    {
      const int cellGroupGlobal = integral.cellGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Pentatope) )
      {
        integral.template integrate<Pentatope, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Pentatope>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Pentatope>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};

}

#endif  // INTEGRATECELLGROUPS_H
