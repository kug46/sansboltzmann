// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_TYPE_H
#define ALGEBRAICEQUATIONSET_TYPE_H

namespace SANS
{

// Forward declarations

class DGBR2;

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
class AlgebraicEquationSet_DGBR2;

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
class AlgebraicEquationSet_DGAdvective;

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
class AlgebraicEquationSet_Local_DG;


template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_Galerkin_Stabilized;

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType_>
class AlgebraicEquationSet_Local_Galerkin_Stabilized;


template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary;

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType_>
class AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary;

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_VMSD;

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_VMSD_Linear;

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_VMSD_BR2;

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType_>
class AlgebraicEquationSet_Local_VMSD;

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType_>
class AlgebraicEquationSet_Local_VMSD_Linear;


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class XFieldType_>
class AlgebraicEquationSet_Local_VMSD_BR2;

// A meta class to deduce what local equation set a global equation set is associated with

template < class EquationSet >
struct LocalEquationSet;

template <class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
          class Traits, class XFieldType>
struct LocalEquationSet<AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DGBR2, XFieldType>>
{
  typedef AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DGBR2, XFieldType, AlgebraicEquationSet_DGBR2> type;
};

template <class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
          class Traits, class DiscTag, class XFieldType>
struct LocalEquationSet<AlgebraicEquationSet_DGAdvective<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>>
{
  typedef AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSet_DGAdvective> type;
};

template <class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
          class Traits, class XFieldType>
struct LocalEquationSet<AlgebraicEquationSet_Galerkin_Stabilized<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>>
{
  typedef AlgebraicEquationSet_Local_Galerkin_Stabilized<NDPDEClass, BCNDConvert, BCVector, XFieldType> type;
};

template <class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
          class Traits, class XFieldType>
struct LocalEquationSet<AlgebraicEquationSet_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>>
{
  typedef AlgebraicEquationSet_Local_Galerkin_Stabilized_LiftedBoundary<NDPDEClass, BCNDConvert, BCVector, XFieldType> type;
};

template <class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
          class Traits, class XFieldType>
struct LocalEquationSet<AlgebraicEquationSet_VMSD<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>>
{
  typedef AlgebraicEquationSet_Local_VMSD<NDPDEClass, BCNDConvert, BCVector, XFieldType> type;
};

template <class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
          class Traits, class XFieldType>
struct LocalEquationSet<AlgebraicEquationSet_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>>
{
  typedef AlgebraicEquationSet_Local_VMSD_Linear<NDPDEClass, BCNDConvert, BCVector, XFieldType> type;
};


template <class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
          class Traits, class XFieldType>
struct LocalEquationSet<AlgebraicEquationSet_VMSD_BR2<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>>
{
  typedef AlgebraicEquationSet_Local_VMSD_BR2<NDPDEClass, BCNDConvert, BCVector, XFieldType> type;
};


}

#endif //INTEGRAND_TYPES_H
