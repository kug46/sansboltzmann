// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANPARAMBASE_H
#define JACOBIANPARAMBASE_H

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/MatrixSizeType.h"
#include "LinearAlgebra/MatrixViewType.h"
#include "LinearAlgebra/NonZeroPatternType.h"
#include "LinearAlgebra/TransposeTraits.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// An abstract base class for Jacobians wrt parameters
//

template<class SystemMatrix>
class JacobianParamBase
{
public:
  typedef typename NonZeroPatternType<SystemMatrix>::type SystemNonZeroPattern;

  typedef typename MatrixViewType<SystemMatrix>::type SystemMatrixView;
  typedef typename NonZeroPatternType<SystemMatrix>::Viewtype SystemNonZeroPatternView;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;

  typedef typename TransposeTraits<SystemNonZeroPattern>::type SystemNonZeroPatternTransposeType;
  typedef typename TransposeTraits<SystemMatrix>::type SystemMatrixTransposeType;

  typedef typename TransposeViewTraits<SystemNonZeroPatternTransposeType>::type SystemNonZeroPatternTranspose;
  typedef typename TransposeViewTraits<SystemMatrixTransposeType>::type SystemMatrixTranspose;

  virtual ~JacobianParamBase() {}

  //Computes the jacobian or fills the non-zero pattern of a jacobian
  virtual void jacobian( SystemMatrixView& mtx) = 0;
  virtual void jacobian( SystemNonZeroPatternView& nz) = 0;

  virtual void jacobian( SystemMatrixTranspose mtx) = 0;
  virtual void jacobian( SystemNonZeroPatternTranspose nz) = 0;
};

} //namespace SANS

#endif //JACOBIANPARAMBASE_H
