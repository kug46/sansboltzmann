// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DISCRETIZATIONOBJECT_H
#define DISCRETIZATIONOBJECT_H

namespace SANS
{

// The generic discretization object tag
class DiscretizationObject {};

}

#endif //DISCRETIZATIONOBJECT_H
