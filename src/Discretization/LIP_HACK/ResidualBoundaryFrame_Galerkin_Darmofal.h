// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYFRAME_GALERKIN_DARMOFAL_H
#define RESIDUALBOUNDARYFRAME_GALERKIN_DARMOFAL_H

// boundary-frame area integral functional (yeah, a 1D topological integral in topological 2D...)

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField3D_Wake.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/Element.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "IntegrandCell_Galerkin_Darmofal.h"

namespace SANS
{


template <class TopologyCell,
          class XFieldType,
          class ArrayQ,
          class PDE>
void
ResidualBoundaryFrame_Galerkin_Darmofal_Group_Integral(
    const IntegrandCell_Galerkin_Darmofal<PDE>& fcn,
    const std::map<int,NodeToBCElemMap>& nodeElemMap,
    const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCell>& xfldCell,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCell>& qfldCell,
    const int sgn,
    int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  // number of integrals evaluated per element
  int nIntegrand = qfldElem.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobal(nIntegrand);

  // frame element integral
  GalerkinWeightedIntegral<TopoD3, TopologyCell, ArrayQ> integral(quadratureorder, nIntegrand);

  // element integrand/residuals
  std::unique_ptr<ArrayQ[]> rsdPDEElem( new ArrayQ[nIntegrand] );

  // loop over elements within group
  for (auto map = nodeElemMap.begin(); map != nodeElemMap.end(); map++)
  {
    SANS_ASSERT( map->second.KuttaPoint >= fcn.dupPointOffset);
    int nTraceElemPoint = map->first;
    int nelem = map->second.elems.size();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemCell = map->second.elems[ elem ];

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elemCell );
      qfldCell.getElement( qfldElem, elemCell );

      for (int n = 0; n < nIntegrand; n++)
        rsdPDEElem[n] = 0;

      integral(
          fcn.integrand(xfldElem, qfldElem),
                        xfldElem,
                        rsdPDEElem.get(), nIntegrand );

      // scatter-add element residuals to global
      qfldCell.associativity( elemCell ).getGlobalMapping( mapDOFGlobal.data(), nIntegrand );

      for (int n = 0; n < nIntegrand; n++)
      {
        if ( mapDOFGlobal[n] != nTraceElemPoint ) continue;
        //if (nGlobal < fcn.dupPointOffset) continue;
        //SANS_ASSERT ( std::find(fcn.KuttaPoints.begin(), fcn.KuttaPoints.end(), nGlobal) != fcn.KuttaPoints.end() );
        rsdPDEGlobal[map->second.KuttaPoint] += sgn*rsdPDEElem[n];
      }
    }
  }
}

//----------------------------------------------------------------------------//
// Residual Boundary Frame
//
template<class TopDim>
class ResidualBoundaryFrame_Galerkin_Darmofal;

template<>
class ResidualBoundaryFrame_Galerkin_Darmofal<TopoD3>
{
protected:
  typedef TopoD3 TopoDim;

public:
  template <class XFieldType, class PDE,
            class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandCell_Galerkin_Darmofal<PDE>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );

    for (auto it = xfld.KuttaCellElemLeft_.begin(); it != xfld.KuttaCellElemLeft_.end(); it++)
    {
      int groupCell = it->first;

      if ( xfld.getCellGroupBase(groupCell).topoTypeID() == typeid(Tet) )
      {
        ResidualBoundaryFrame_Galerkin_Darmofal_Group_Integral<Tet, XFieldType, ArrayQ>(
            fcn, it->second,
            xfld.template getCellGroup<Tet>(groupCell),
            qfld.template getCellGroup<Tet>(groupCell),
            1,
            quadratureorder[groupCell],
            rsdPDEGlobal );
      }
      else if ( xfld.getCellGroupBase(groupCell).topoTypeID() == typeid(Hex) )
      {
        ResidualBoundaryFrame_Galerkin_Darmofal_Group_Integral<Hex, XFieldType, ArrayQ>(
            fcn, it->second,
            xfld.template getCellGroup<Hex>(groupCell),
            qfld.template getCellGroup<Hex>(groupCell),
            1,
            quadratureorder[groupCell],
            rsdPDEGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
    }


    for (auto it = xfld.KuttaCellElemRight_.begin(); it != xfld.KuttaCellElemRight_.end(); it++)
    {
      int groupCell = it->first;

      if ( xfld.getCellGroupBase(groupCell).topoTypeID() == typeid(Tet) )
      {
        ResidualBoundaryFrame_Galerkin_Darmofal_Group_Integral<Tet, XFieldType, ArrayQ>(
            fcn, it->second,
            xfld.template getCellGroup<Tet>(groupCell),
            qfld.template getCellGroup<Tet>(groupCell),
            -1,
            quadratureorder[groupCell],
            rsdPDEGlobal );
      }
      else if ( xfld.getCellGroupBase(groupCell).topoTypeID() == typeid(Hex) )
      {
        ResidualBoundaryFrame_Galerkin_Darmofal_Group_Integral<Hex, XFieldType, ArrayQ>(
            fcn, it->second,
            xfld.template getCellGroup<Hex>(groupCell),
            qfld.template getCellGroup<Hex>(groupCell),
            -1,
            quadratureorder[groupCell],
            rsdPDEGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
    }

  }
};

}

#endif  // RESIDUALBOUNDARYFRAME_GALERKIN_DARMOFAL_H
