// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_GALERKIN_FARFIELD_IPTF_H
#define INTEGRANDBOUNDARYTRACE_GALERKIN_FARFIELD_IPTF_H

// interior/periodic-trace integrand operators: Galerkin sansLG for 2D wake-cut

// 2-D Incompressible Potential PDE class
// based on weighted-norm functional

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"
#include "pde/FullPotential/PDEIncompressiblePotentialTwoField2D.h"
#include "pde/FullPotential/BCIncompressiblePotentialTwoField2D_sansLG.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"
#include "Discretization/LIP_HACK/JacobianBoundaryTrace_Galerkin_Element_IPTF.h"

namespace SANS
{

#if 0
//----------------------------------------------------------------------------//
// wake-cut BC

struct BCInt_WakeCut_IPTF_Params : noncopyable
{
  const ParameterNumeric<Real> circ{"circ", NO_DEFAULT, NO_RANGE, "Vortex circulation"};

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.circ));
    d.checkUnknownInputs(allParams);
  }

  static constexpr const char* BCName{"Int_WakeCut_IPTF"};
  struct Option
  {
    const DictOption Int_WakeCut_IPTF{BCInt_WakeCut_IPTF_Params::BCName, BCInt_WakeCut_IPTF_Params::checkInputs};
  };

  static BCInt_WakeCut_IPTF_Params params;
};


class BCInt_WakeCut_IPTF
  : public BCType< BCInt_WakeCut_IPTF >
{
public:
  typedef BCCategory::IPTF_sansLG Category;
  typedef BCInt_WakeCut_IPTF_Params ParamsType;

  typedef PDEIncompressiblePotentialTwoField2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 2;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  explicit BCInt_WakeCut_IPTF( PyDict& d ) : circ_(d.get(ParamsType::params.circ)) {}
  explicit BCInt_WakeCut_IPTF( const Real& circ ) : circ_(circ) {}

  BCInt_WakeCut_IPTF( const BCInt_WakeCut_IPTF& ) = delete;
  BCInt_WakeCut_IPTF& operator=( const BCInt_WakeCut_IPTF& ) = delete;

  Real getCirculation() const { return circ_; }

private:
  Real circ_;         // vortex strength/circulation
};
#endif


//----------------------------------------------------------------------------//
// boundary-trace integrand: Galerkin sansLG for IPTF

template <class PDE, class NDBCVectorCategory, class Disc, class Tglb>
class IntegrandBoundaryTrace_Farfield_IPTF;

template <class PDE_, class NDBCVector, class Tglb>
class IntegrandBoundaryTrace_Farfield_IPTF<PDE_, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb > :
    public IntegrandBoundaryTraceType<
             IntegrandBoundaryTrace_Farfield_IPTF<PDE_, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb> >
{
public:
  typedef PDE_ PDE; // NDPDEClass

  typedef BCCategory::IPTF_sansLG Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  //typedef typename NDBCVector::Tglb Tglb;
  typedef BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Tglb> BCTry;
  //typedef BCNDConvertSpace< PhysD2, BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Tglb> > BCTry;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  IntegrandBoundaryTrace_Farfield_IPTF( const PDE& pde, const BCTry& bc, const std::vector<int>& boundaryGroups)
    : pde_(pde), bc_(bc), boundaryGroups_(boundaryGroups) {}

  virtual ~IntegrandBoundaryTrace_Farfield_IPTF() {}

  std::size_t nBoundaryGroups() const { return boundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return boundaryGroups_; }

  //const BCBase& getBC() const { return bc_; }

  // access to global variables in BC
  template <class T>
  void getGlobal( ArrayQ<T>& glb ) const { glb(0) = bc_.getCirculation(); }

  template <class T>
  void setGlobal( const ArrayQ<T>& glb ) { bc_.setCirculation( glb(0) ); }


  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell , Topology     > ElementQFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef JacobianElemBoundaryTrace_Galerkin_IPTF<MatrixQ<Real>> JacobianElemBoundaryTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template <class Z>
    using IntegrandType = ArrayQ<Z>;

    BasisWeighted( const PDE& pde, const BCTry& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem ) :
                   pde_(pde), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem),
                   paramfldElem_( paramfldElem ),
                   nDOF_(qfldElem_.nDOF()),
                   psi_( new Real[nDOF_] ),
                   gradpsi_( new VectorX[nDOF_] )
    {
    }

    ~BasisWeighted()
    {
      delete [] psi_;
      delete [] gradpsi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
#if 1
    template <class Ti>
    void operator()(
        const QuadPointTraceType& RefTrace,
        IntegrandType<Ti> integrandPDEL[], int nPDEL,
        IntegrandType<Ti> integrandAux[], int nAux ) const;
#else
    template <class Ti>
    void operator()(
        const QuadPointTraceType& RefTrace,
        IntegrandType<Ti> integrandPDEL[], int nPDEL,
        IntegrandType<Ti> integrandAux[], int nAux ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandPDEL, nPDEL, integrandAux, nAux);
    }
#endif

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemBoundaryTraceType& mtxElemL,
                     JacobianElemBoundaryTraceType& mtxAux ) const;

    FRIEND_CALL_WITH_DERIVED
  protected:
#if 1
    template<class BC, class Tq, class Tg, class Ti>
    void operator()( const BC& bc, const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
                     const ParamT& paramI, const VectorX& nL,
                     ArrayQ<Ti> integrandPDEL[], int nPDEL,
                     ArrayQ<Ti> integrandAux[], int nAux ) const;

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const MatrixQ<Real>& A, const MatrixQ<Real>& B, const ArrayQ<Ti>& bcdata, const Real& phiVortex,
                            const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
                            const ParamT& paramL, const VectorX& nL,
                            ArrayQ<Ti> integrandPDEL[], int nPDEL,
                            ArrayQ<Ti> integrandAux[], int nAux ) const;
#else
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace,
                     IntegrandType<Ti> integrandPDEL[], int nPDEL,
                     IntegrandType<Ti> integrandAux[], int nAux ) const;
#endif

    const PDE& pde_;
    const BCTry& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *psi_;
    mutable VectorX *gradpsi_;
  };


#if 0   // not implemented in baseline Galerkin_sansLG
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef T IntegrandType;

    FieldWeighted(  const PDE& pde, const BCTry& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem,
                    const ElementQFieldCell& qfldElem,
                    const ElementQFieldCell& wfldElem ) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                    qfldElem_(qfldElem),
                    wfldElem_(wfldElem),
                    paramfldElem_( paramfldElem ),
                    nDOF_(qfldElem.nDOF())
    {
      // Nothing
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
#if 0
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType& integrandPDEL ) const;
#else
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType& integrandPDEL ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandPDEL);
    }
#endif

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType& integrandPDEL ) const;

    const PDE& pde_;
    const BCTry& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldCell& wfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
  };
#endif  // not implemented in baseline Galerkin_sansLG

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem) const
  {
#if 0     // original
    return BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>( pde_, bc_,
                                                                                          xfldElemTrace, canonicalTrace,
                                                                                          paramfldElem, qfldElem);
#else     // equivalent but simpler
    return { pde_, bc_,
             xfldElemTrace, canonicalTrace,
             paramfldElem, qfldElem };
#endif
  }

#if 0   // not implemented in baseline Galerkin_sansLG
  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& wfldElem) const
  {
    return FieldWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>( pde_, bc_,
                                                                                          xfldElemTrace, canonicalTrace,
                                                                                          paramfldElem, qfldElem, wfldElem);
  }
#endif  // not implemented in baseline Galerkin_sansLG

private:
  const PDE& pde_;
  const BCTry& bc_;
  const std::vector<int> boundaryGroups_;
};


template <class PDE, class NDBCVector, class Tglb>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace_Farfield_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace,
            IntegrandType<Ti> integrandPDEL[], int nPDEL,
            IntegrandType<Ti> integrandAux[], int nAux ) const
{
  SANS_ASSERT( nPDEL == nDOF_ );

  ParamT paramL;            // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;               // unit normal (points out of domain)

  ArrayQ<T> qL;             // solution
  VectorArrayQ<T> gradqL;   // gradient

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, psi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradpsi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( psi_, nDOF_, qL );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradpsi_, nDOF_, gradqL );
  else
    gradqL = 0;

  //call_with_derived<NDBCVector>(*this, bc_, qL, gradqL, paramL, nL, integrandPDEL, nPDEL, integrandAux, nAux);
  operator()(bc_, qL, gradqL, paramL, nL, integrandPDEL, nPDEL, integrandAux, nAux);
}


//---------------------------------------------------------------------------//
template <class PDE, class NDBCVector, class Tglb>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
void
IntegrandBoundaryTrace_Farfield_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemBoundaryTraceType& mtxElemL,
            JacobianElemBoundaryTraceType& mtxAux ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOF_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOF_);

  SANS_ASSERT(mtxAux.nTest == mtxAux.nAux);
  SANS_ASSERT(mtxAux.nDOFL == nDOF_);

  const int nAux = mtxAux.nAux;     // total auxiliary/global equations/variables

  ParamT paramL;                  // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> qL;                     // solutions
  VectorArrayQ<T> gradqL;           // solution gradients

  QuadPointCellType sRefL;          // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // left reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRefL );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRefL, paramL );

  // basis value, gradient
  qfldElem_.evalBasis( sRefL, psi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRefL, qfldElem_, gradpsi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( psi_, nDOF_, qL );

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElem_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL;               // solution
  VectorArrayQ<SurrealClass> gradqSurrealL;     // gradient

  qSurrealL = qL;

  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradpsi_, nDOF_, gradqL );
    gradqSurrealL = gradqL;
  }

  // BC class copy
  // NOTE: this is needed since operator() declared const (and integrand classes are not copyable, etc)
  BCTry bc(bc_);

  // surreal global variable (circulation)
  ArrayQ<SurrealClass> glbSurreal;
  glbSurreal = 0;
  bc.getCirculation( glbSurreal(0) );

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandPDELSurreal( nDOF_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandAuxSurreal( nAux );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(glbSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    //std::cout << "operatorFFD 0: qSurrealL = " << qSurrealL(0) << "  " << qSurrealL(1);
    //std::cout << "  glbSurreal = " << glbSurreal(0) << "  " <<  glbSurreal(1) << std::endl;

    integrandPDELSurreal = 0;
    integrandAuxSurreal  = 0;

    // compute the integrand
    bc.setCirculation( glbSurreal(0) );
    operator()(bc, qSurrealL, gradqL, paramL, nL, integrandPDELSurreal.data(), nDOF_, integrandAuxSurreal.data(), nAux);

    // accumulate derivatives into element jacobians: d/d(qL)
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandPDELSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxElemL.rsd_qL(i,j) += dJ*psi_[j]*PDE_q;
      }

      for (int i = 0; i < nAux; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandAuxSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxAux.rsd_qL(i,j) += dJ*psi_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

    // accumulate derivatives into element jacobians: d/d(glb)
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(glbSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative
      bc.setCirculation( glbSurreal(0) );

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandPDELSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nAux; j++)
          mtxElemL.rsd_glb(i,j) += dJ*PDE_q;
      }

      for (int i = 0; i < nAux; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandAuxSurreal[i], m).deriv(slot + n - nchunk);
        //std::cout << "operatorFFD: PDE_q = " << PDE_q(0,0) << " " << PDE_q(0,1) << " " << PDE_q(1,0) << " " << PDE_q(1,1) << std::endl;

        for (int j = 0; j < nAux; j++)
          mtxAux.rsd_glb(i,j) += dJ*PDE_q;
      }
    } // if slot
    slot += PDE::N;

    //std::cout << "operatorFFD 1: qSurrealL = " << qSurrealL(0) << "  " << qSurrealL(1);
    //std::cout << "  glbSurreal = " << glbSurreal(0) << "  " <<  glbSurreal(1) << std::endl;

  } // nchunk

  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandPDELSurreal = 0;
      integrandAuxSurreal  = 0;

      // compute the integrand
      operator()(bc, qL, gradqSurrealL, paramL, nL, integrandPDELSurreal.data(), nDOF_, integrandAuxSurreal.data(), nAux);

      // accumulate derivatives into element jacobians: d/d(gradqL)
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandPDELSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOF_; j++)
              mtxElemL.rsd_qL(i,j) += dJ*gradpsi_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nAux; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandAuxSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOF_; j++)
              mtxAux.rsd_qL(i,j) += dJ*gradpsi_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient

  // dummy global equation
  mtxAux.rsd_glb(0,0)(1,1) = 1;
  //std::cout << "integrandFFD: size mtxAux.rsd_glb = " << mtxAux.rsd_glb.m() << " x " << mtxAux.rsd_glb.n() << std::endl;
  //std::cout << "integrandFFD: size mtxAux.rsd_glb(0,0) = " << mtxAux.rsd_glb(0,0).m() << " x " << mtxAux.rsd_glb(0,0).n() << std::endl;
}


template <class PDE, class NDBCVector, class Tglb>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell , class Topology     , class ElementParam>
template<class BC, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace_Farfield_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
            const ParamT& paramL, const VectorX& nL,
            ArrayQ<Ti> integrandPDEL[], int nPDEL,
            ArrayQ<Ti> integrandAux[], int nAux ) const
{
  SANS_ASSERT( nPDEL == nDOF_ );

#if 0   // not needed
  ParamT param;             // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;               // unit normal (points out of domain)

  ArrayQ<T> q;              // solution
  VectorArrayQ<T> gradq;    // gradient

  QuadPointCellType sRef;    // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, param );

  // unit normal: points to R
  traceUnitNormal( xfldElem_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, psi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradpsi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( psi_, nDOF_, q );
  qfldElem_.evalFromBasis( gradpsi_, nDOF_, gradq );
#endif

  // BC coefficients, data
#if 0
  MatrixQ<Real> A, B;
  ArrayQ<Ti> bcdata;
  bc.coefficients( paramL, nL, A, B );
  bc.data( paramL, nL, bcdata );

  const VectorX& X = paramL;    // HACK ??
  Real phiVortex = bc.unitVortexPotential( X[0], X[1] );
#else
  const VectorX& X = paramL;    // HACK ??

  MatrixQ<Real> A, B;
  ArrayQ<Ti> bcdata;
  bc.coefficients( X[0], X[1], 0., nL[0], nL[1], A, B );
  bc.data( X[0], X[1], 0., nL[0], nL[1], bcdata );
  Real phiVortex = bc.unitVortexPotential( X[0], X[1] );
#endif

  // compute the integrand
  weightedIntegrand( A, B, bcdata, phiVortex, qL, gradqL, paramL, nL, integrandPDEL, nPDEL, integrandAux, nAux );
}


template <class PDE, class NDBCVector, class Tglb>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam >
template <class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace_Farfield_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
weightedIntegrand( const MatrixQ<Real>& A, const MatrixQ<Real>& B, const ArrayQ<Ti>& bcdata, const Real& phiVortex,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                   const ParamT& paramL, const VectorX& nL,
                   ArrayQ<Ti> integrandPDEL[], int nPDEL,
                   ArrayQ<Ti> integrandAux[], int nAux ) const
{
  const VectorX& X = paramL;    // HACK ??
  //const VectorX& X = get<-1>(paramL);
  //const VectorX& X = paramL.right();        // cartesian coordinates of evaluation point

  // tau basis functions
  Real *tau = new Real[nDOF_];              // basis (L/R)
  VectorX *gradtau = new VectorX[nDOF_];    // basis gradient (L/R)
  for (int k = 0; k < nDOF_; k++)
  {
    tau[k] = psi_[k];
    gradtau[k] = gradpsi_[k];
  }

  for (int k = 0; k < nPDEL; k++)
    integrandPDEL[k] = 0;

  // PDE residual: weak form boundary integral

#ifdef USE_IPTF_NORM
  ArrayQ<Tg> gradqSq = dot(gradqL, gradqL);
  Tg   wghtD = pde_.gaussianD( X[0], X[1] ) * pow( gradqSq(0), pde_.normD()/2 - 1);
#else
  Real wghtD = pde_.gaussianD( X[0], X[1] );
#endif

  ArrayQ<Tg> qnL = dot(nL, gradqL);

#undef USE_OLDFORM
#ifdef USE_OLDFORM
  for (int k = 0; k < nPDEL; k++)
  {
    integrandPDEL[k](0) += -dot(nL, gradpsi_[k])*qL(1);
    integrandPDEL[k](1) += -tau[k]*qnL(0);
  }
#else   // integrand now consistent with PDE class
  for (int k = 0; k < nPDEL; k++)
  {
    integrandPDEL[k](0) += -psi_[k]*(qnL(1) + wghtD*qnL(0));
    integrandPDEL[k](1) += -tau[k]*qnL(0);
  }
#endif    // USE_OLDFORM

  // PDE residual: sans-Lagrange BC terms

  Ti rsd1 = qL(0) - bcdata(0);        // farfield BC
  Ti rsd2 = qnL(1) + wghtD*qnL(0);
  //std::cout << "integrandFFD: circ = " << bcdata(0) << "  rsd1 = " << rsd1 << "  rsd2 = " << rsd2 << std::endl;
  //std::cout << "integrandFFD: qL = " << qL(0) << "  " << qL(1);
  //std::cout << "  gradqL[0] = " << gradqL[0](0) << "  " <<  gradqL[0](1) << "  gradqL[1] = " << gradqL[1](0) << "  " <<  gradqL[1](1);
  //std::cout << "  bcdata = " << bcdata(0) << "  " <<  bcdata(1) << std::endl;

#ifdef USE_OLDFORM
  for (int k = 0; k < nPDEL; k++)
  {
    integrandPDEL[k](0) += -dot(nL, gradpsi_[k])*wghtD*rsd1 - psi_[k]*rsd2;
    integrandPDEL[k](1) += -dot(nL, gradtau[k] )      *rsd1;
  }
#else     // properly shows strong-form BCs for sigma
  Ti rsd3 = qL(1);

  for (int k = 0; k < nPDEL; k++)
  {
    integrandPDEL[k](0) += -dot(nL, gradpsi_[k])*wghtD*rsd1 - dot(nL, gradpsi_[k])*rsd3;
    integrandPDEL[k](1) += -dot(nL, gradtau[k] )      *rsd1;
  }
#endif    // USE_OLDFORM

  // Aux/global residual:

#ifdef USE_FIXCIRC
  integrandAux[0](0) = 0;
  //integrandAux[0](1) = bcdata(1);
#else
  integrandAux[0](0) = phiVortex * rsd2;
  //integrandAux[0](1) = bcdata(1);
#endif    // USE_FIXCIRC
  //std::cout << "integrandFFD: Aux = " << integrandAux[0](0) << " " << integrandAux[0](1) << std::endl;

#define USE_NITSCHE
#ifdef USE_NITSCHE
  // Nitsche stabilization terms

  // normal grid scale
  Real h = 0;
  for (int k = 0; k < nPDEL; k++)
    h += std::abs(dot(nL, gradpsi_[k]));
  h = 1./h;

#if 0     // old formulation (ref: ??)
  Real beta1    = 10;   // Nitsche parameter
  Real alpha2B2 = 10;
  for (int k = 0; k < nPDEL; k++)
  {
    integrandPDEL[k](0) += (alpha2B2/h)*psi_[k]*qL(1);
    integrandPDEL[k](1) += (beta1/h)*tau[k]*rsd1;
  }
#else     // adjoint consistent (ref: lipw, Sec 3.5 & 3.6; lagrangeBC_LIPw_ffd_28feb18)
  Real a = 2/h;   // Nitsche parameters
  Real b = 2*a;

  for (int k = 0; k < nPDEL; k++)
  {
    integrandPDEL[k](0) += psi_[k]*(2*a*rsd1 + b*rsd3);
    integrandPDEL[k](1) += tau[k] *(  b*rsd1);
  }

#ifdef USE_FIXCIRC
#else
  integrandAux[0](0) += -phiVortex*(2*a*rsd1 + b*rsd3);
#endif    // USE_FIXCIRC
#endif
#endif    // USE_NITSCHE

  delete [] tau;
  delete [] gradtau;
}


#if 0   // not implemented in baseline Galerkin_sansLG
template <class PDE, class NDBCVector, class Tglb>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace_Farfield_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
FieldWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace,
            IntegrandType& integrandPDEL,
            IntegrandType& integrandAux ) const
{
  ParamTL paramL;   // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  VectorX nL;       // unit normal for left element (points to right element)
  VectorX nR;       //                              (points to left element)

  ArrayQ<T> qL, qR;                // solution
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients
  VectorArrayQ<T> dqn;             // jump in q.n

  ArrayQ<T> wL, wR;                // weight
  VectorArrayQ<T> gradwL, gradwR;  // weight gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: nL points to R; nR points to L
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);
  nR = -nL;

  // solution value, gradient
  qfldElemL_.eval( sRefL, qL );
  qfldElemR_.eval( sRefR, qR );
  if (pde_.hasFluxViscous() || pde_.hasSourceTrace())
  {
    xfldElemL_.evalGradient( sRefL, qfldElemL_, gradqL );
    xfldElemR_.evalGradient( sRefR, qfldElemR_, gradqR );
  }

  // weight value,
  wfldElemL_.eval( sRefL, wL );
  wfldElemR_.eval( sRefR, wR );
  if (pde_.hasFluxViscous())
  {
    xfldElemL_.evalGradient( sRefL, wfldElemL_, gradwL );
    xfldElemR_.evalGradient( sRefR, wfldElemR_, gradwR );
  }

  integrandL = 0;
  integrandR = 0;

  // PDE residual: weak form boundary integral

  ArrayQ<Ti> qnL = dot(nL, gradqL);
  ArrayQ<Ti> qnR = dot(nR, gradqR);

  ArrayQ<Ti> qnSum = 0.5*(qnR + qnL);
  ArrayQ<Ti> qnDif = 0.5*(qnR - qnL);

  integrandL += -wL*(qnSum - qnDif);
  integrandR += -wR*(qnSum + qnDif);

  // PDE residual: sans-Lagrange BC terms

  Real circ = bc_.getCirculation();

  ArrayQ<Ti> rsdMass = 0.5*(qnR + qnL);          // mass flux BC
  ArrayQ<Ti> rsdCirc = 0.5*((qR - qL) + circ);   // potential jump BC

  integrandL += wL*rsdMass - dot(nL, gradwL)*(-rsdCirc);
  integrandR += wR*rsdMass - dot(nR, gradwR)*(+rsdCirc);

#ifdef USE_NITSCHE
  // edge length
  Real ds = xfldElemTrace_.jacobianDeterminant();

  // adjacent element area
  Real areaL = xfldElemL_.jacobianDeterminant();
  Real areaR = xfldElemR_.jacobianDeterminant();

  // normal grid scale
  Real h = 0.5*(areaL + areaR)/ds;

  Real beta1  = 10;   // Nitsche parameters
  Real gamma2 = 10;

  integrandL += (beta1/(4*h))*(-wL)*rsdCirc + (gamma2*h)*dot(nL, gradwL)*rsdMass;
  integrandR += (beta1/(4*h))*(+wR)*rsdCirc + (gamma2*h)*dot(nR, gradwR)*rsdMass;
#endif
}
#endif  // not implemented in baseline Galerkin_sansLG

}   // namespace SANS

#endif  // INTEGRANDBOUNDARYTRACE_GALERKIN_FARFIELD_IPTF_H
