// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_GALERKIN_WAKE_LIP_H
#define RESIDUALBOUNDARYTRACE_GALERKIN_WAKE_LIP_H

// boundary-trace integral residual functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Field/XFieldVolume.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "IntegrandFunctor_Galerkin_WakeCut_LIP.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Linearized Incompressible Potential Flow Galerkin Wake boundary-trace integral
//
//  functions dispatched based on left (L) element topologies
//
//  process:
//  (a) loop over groups; dispatch to L (ResidualBoundaryTrace_LeftTopology)
//  (b) dispatch to R (ResidualBoundaryTrace_RightTopology)
//  (c) call base class routine with specific L and R (ResidualBoundaryTrace_Group w/ Base& params)
//  (d) cast to specific L and R and call L/R specific topology routine (ResidualBoundaryTrace_Group_Integral)
//  (e) loop over traces and integrate

template <class TopologyTrace, class TopologyL, class TopologyR,
          class PhysDim, class TopoDim, class ArrayQ,
          class PDE>
void
ResidualBoundaryTrace_Galerkin_WakeCut_LIP_Group_Integral(
    const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR>& xfldCellR,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
    int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
    SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
  typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;


  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  ElementXFieldClassL xfldElemR( xfldCellR.basis() );
  ElementQFieldClassR qfldElemR( qfldCellR.basis() );

  ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

  ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

  // number of integrals evaluated per element
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  int nIntegrandWake = lgfldElemTrace.nDOF();

  // element-to-global DOF mapping
  std::unique_ptr<int[]> mapDOFGlobalL( new int[nIntegrandL] );
  std::unique_ptr<int[]> mapDOFGlobalR( new int[nIntegrandR] );
  std::unique_ptr<int[]> mapDOFGlobalWake( new int[nIntegrandWake] );

  // trace element integral
  GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQ, ArrayQ, ArrayQ>
    integral(quadratureorder, nIntegrandL, nIntegrandR, nIntegrandWake);

  // element integrand/residuals
  std::unique_ptr<ArrayQ[]> rsdPDEElemL( new ArrayQ[nIntegrandL] );
  std::unique_ptr<ArrayQ[]> rsdPDEElemR( new ArrayQ[nIntegrandR] );
  std::unique_ptr<ArrayQ[]> rsdWakeElem( new ArrayQ[nIntegrandWake] );

  // loop over elements within group
  int nelem = xfldTrace.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int elemL = xfldTrace.getElementLeft( elem );
    const int elemR = xfldTrace.getElementRight( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldCellR.getElement( xfldElemR, elemR );
    qfldCellR.getElement( qfldElemR, elemR );

    lgfldTrace.getElement( lgfldElemTrace, elem );

    xfldTrace.getElement( xfldElemTrace, elem );

    for (int n = 0; n < nIntegrandL; n++)
      rsdPDEElemL[n] = 0;

    for (int n = 0; n < nIntegrandR; n++)
      rsdPDEElemR[n] = 0;

    for (int n = 0; n < nIntegrandWake; n++)
      rsdWakeElem[n] = 0;

    integral(
        fcn.integrand(xfldElemTrace, lgfldElemTrace,
                      canonicalTraceL, canonicalTraceR,
                      xfldElemL, qfldElemL,
                      xfldElemR, qfldElemR ),
        xfldElemTrace,
        rsdPDEElemL.get(), nIntegrandL,
        rsdPDEElemR.get(), nIntegrandR,
        rsdWakeElem.get(), nIntegrandWake );

    // scatter-add element residuals to global
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.get(), nIntegrandL );
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.get(), nIntegrandR );
    lgfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalWake.get(), nIntegrandWake );

    int nGlobal;
    for (int n = 0; n < nIntegrandL; n++)
    {
      nGlobal = mapDOFGlobalL[n];
      rsdPDEGlobal[nGlobal] += rsdPDEElemL[n];
    }
    for (int n = 0; n < nIntegrandR; n++)
    {
      nGlobal = mapDOFGlobalR[n];
      rsdPDEGlobal[nGlobal] += rsdPDEElemR[n];
    }
    for (int n = 0; n < nIntegrandWake; n++)
    {
      nGlobal = mapDOFGlobalWake[n];
      rsdWakeGlobal[nGlobal] += rsdWakeElem[n];
    }
  }
}


template <class TopologyTrace, class TopologyL, class TopologyR,
          class PDE,
          class PhysDim, class TopoDim, class ArrayQ>
void
ResidualBoundaryTrace_Galerkin_WakeCut_LIP_Group(
    const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
    SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
{
  const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

  const int groupL = xfldTrace.getGroupLeft();
  const int groupR = xfldTrace.getGroupRight();

  // Integrate over the trace group
  ResidualBoundaryTrace_Galerkin_WakeCut_LIP_Group_Integral<TopologyTrace, TopologyL, TopologyR, PhysDim, TopoDim, ArrayQ>(
      fcn, xfldTrace, lgfldTrace,
      xfld.template getCellGroup<TopologyL>(groupL), qfld.template getCellGroup<TopologyL>(groupL),
      xfld.template getCellGroup<TopologyR>(groupR), qfld.template getCellGroup<TopologyR>(groupR),
      quadratureorder,
      rsdPDEGlobal, rsdWakeGlobal );

}


template<class TopDim>
class ResidualBoundaryTrace_Galerkin_WakeCut_LIP;


template<>
class ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class TopologyTrace, class PDE,
            class PhysDim, class ArrayQ>
  static void
  LeftTopology(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      ResidualBoundaryTrace_Galerkin_WakeCut_LIP_Group<TopologyTrace, Triangle>(
          fcn, xfldTrace, lgfldTrace, qfld, quadratureorder, rsdPDEGlobal, rsdWakeGlobal );
    }
    else
    {
      char msg[] = "Error in ResidualBoundaryTrace_Galerkin_LG<TopoD2>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class PDE,
  class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {
    SANS_ASSERT( rsdPDEGlobal.m() == qfld.nDOF() );
    SANS_ASSERT( rsdWakeGlobal.m() == lgfld.nDOF() );
    SANS_ASSERT( &qfld.getXField() == &lgfld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);

      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line>( fcn,
            xfld.template getBoundaryTraceGroup<Line>(boundaryGroup),
            lgfld.template getBoundaryTraceGroupGlobal<Line>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup], rsdPDEGlobal, rsdWakeGlobal );
      }
      else
      {
        char msg[] = "Error in ResidualBoundaryTrace_Galerkin_LG<TopoD2>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};



template<>
class ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

  template <class TopologyTrace, class TopologyL,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  RightTopology(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      ResidualBoundaryTrace_Galerkin_WakeCut_LIP_Group<TopologyTrace, TopologyL, Tet>(
          fcn, xfldTrace, lgfldTrace, qfld, quadratureorder, rsdPDEGlobal, rsdWakeGlobal );
    }
    else
    {
      char msg[] = "Error in ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  template <class TopologyTrace, class PDE,
            class PhysDim, class ArrayQ>
  static void
  LeftTopology(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Tet>( fcn, xfldTrace, lgfldTrace, qfld, quadratureorder, rsdPDEGlobal, rsdWakeGlobal );
    }
    else
    {
      char msg[] = "Error in ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class PDE,
  class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {
    SANS_ASSERT( rsdPDEGlobal.m() == qfld.nDOF() );
    SANS_ASSERT( rsdWakeGlobal.m() == lgfld.nDOF() );
    SANS_ASSERT( &qfld.getXField() == &lgfld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);

      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology<Triangle>( fcn,
            xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroup),
            lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup], rsdPDEGlobal, rsdWakeGlobal );
      }
      else
      {
        char msg[] = "Error in ResidualBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};

}

#endif  // RESIDUALBOUNDARYTRACE_GALERKIN_WAKE_LIP_H
