// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_GALERKIN_WALL_IPTF_H
#define INTEGRANDBOUNDARYTRACE_GALERKIN_WALL_IPTF_H

// interior/periodic-trace integrand operators: Galerkin sansLG for 2D wake-cut

// 2-D Incompressible Potential PDE class
// based on weighted-norm functional

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"
#include "pde/FullPotential/PDEIncompressiblePotentialTwoField2D.h"
#include "pde/FullPotential/BCIncompressiblePotentialTwoField2D_sansLG.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"
//#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin_Element.h"

namespace SANS
{

#if 0
//----------------------------------------------------------------------------//
// wake-cut BC

struct BCInt_WakeCut_IPTF_Params : noncopyable
{
  const ParameterNumeric<Real> circ{"circ", NO_DEFAULT, NO_RANGE, "Vortex circulation"};

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.circ));
    d.checkUnknownInputs(allParams);
  }

  static constexpr const char* BCName{"Int_WakeCut_IPTF"};
  struct Option
  {
    const DictOption Int_WakeCut_IPTF{BCInt_WakeCut_IPTF_Params::BCName, BCInt_WakeCut_IPTF_Params::checkInputs};
  };

  static BCInt_WakeCut_IPTF_Params params;
};


class BCInt_WakeCut_IPTF
  : public BCType< BCInt_WakeCut_IPTF >
{
public:
  typedef BCCategory::IPTF_sansLG Category;
  typedef BCInt_WakeCut_IPTF_Params ParamsType;

  typedef PDEIncompressiblePotentialTwoField2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 2;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  explicit BCInt_WakeCut_IPTF( PyDict& d ) : circ_(d.get(ParamsType::params.circ)) {}
  explicit BCInt_WakeCut_IPTF( const Real& circ ) : circ_(circ) {}

  BCInt_WakeCut_IPTF( const BCInt_WakeCut_IPTF& ) = delete;
  BCInt_WakeCut_IPTF& operator=( const BCInt_WakeCut_IPTF& ) = delete;

  Real getCirculation() const { return circ_; }

private:
  Real circ_;         // vortex strength/circulation
};
#endif


//----------------------------------------------------------------------------//
// boundary-trace integrand: Galerkin sansLG for IPTF

template <class PDE, class NDBCVectorCategory, class Disc>
class IntegrandBoundaryTrace_Wall_IPTF;

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace_Wall_IPTF<PDE_, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace_Wall_IPTF<PDE_, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  //typedef BCIncompressiblePotentialTwoField2D<BCTypeWall, Real> BCClass;
  //typedef BCNDConvertSpace<PhysD2, BCClass> NDBCClass;

  typedef BCCategory::IPTF_sansLG Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  IntegrandBoundaryTrace_Wall_IPTF( const PDE& pde, const BCBase& bc, const std::vector<int>& boundaryGroups)
    : pde_(pde), bc_(bc), boundaryGroups_(boundaryGroups) {}

  virtual ~IntegrandBoundaryTrace_Wall_IPTF() {}

  std::size_t nBoundaryGroups() const { return boundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return boundaryGroups_; }

  //const BCBase& getBC() const { return bc_; }


  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell , Topology     > ElementQFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template <class Z>
    using IntegrandType = ArrayQ<Z>;

    BasisWeighted( const PDE& pde, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem ) :
                   pde_(pde), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem),
                   paramfldElem_( paramfldElem ),
                   nDOF_(qfldElem_.nDOF()),
                   psi_( new Real[nDOF_] ),
                   gradpsi_( new VectorX[nDOF_] )
    {
    }

    ~BasisWeighted()
    {
      delete [] psi_;
      delete [] gradpsi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
#if 1
    template <class Ti>
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType<Ti> integrandPDE[], int nPDE ) const;
#else
    template <class Ti>
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType<Ti> integrandPDE[], int nPDE ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandPDE, nPDE);
    }
#endif

    FRIEND_CALL_WITH_DERIVED
  protected:
#if 1
    template<class BC, class Tq, class Tg, class Ti>
    void operator()( const BC& bc, const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
                     const ParamT& paramI, const VectorX& nL, ArrayQ<Ti> integrandPDE[], int nPDE ) const;

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const MatrixQ<Real>& A, const MatrixQ<Real>& B, const ArrayQ<Ti>& bcdata,
                            const ArrayQ<Tq>& qI, const VectorArrayQ<Tg>& gradqI,
                            const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrandPDE[], int nPDE ) const;
#else
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType<Ti> integrandPDE[], int nPDE ) const;
#endif

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *psi_;
    mutable VectorX *gradpsi_;
  };


#if 0   // not implemented in baseline Galerkin_sansLG
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef T IntegrandType;

    FieldWeighted(  const PDE& pde, const BCBase& bc,
                    const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                    const ElementParam& paramfldElem,
                    const ElementQFieldCell& qfldElem,
                    const ElementQFieldCell& wfldElem ) :
                    pde_(pde), bc_(bc),
                    xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                    xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                    qfldElem_(qfldElem),
                    wfldElem_(wfldElem),
                    paramfldElem_( paramfldElem ),
                    nDOF_(qfldElem.nDOF())
    {
      // Nothing
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
#if 0
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType& integrandPDE ) const;
#else
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType& integrandPDE ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandPDE);
    }
#endif

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType& integrandPDE ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldCell& wfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
  };
#endif  // not implemented in baseline Galerkin_sansLG

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem) const
  {
    return BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>( pde_, bc_,
                                                                                          xfldElemTrace, canonicalTrace,
                                                                                          paramfldElem, qfldElem);
  }

#if 0   // not implemented in baseline Galerkin_sansLG
  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& qfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , Topology    >& wfldElem) const
  {
    return FieldWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>( pde_, bc_,
                                                                                          xfldElemTrace, canonicalTrace,
                                                                                          paramfldElem, qfldElem, wfldElem);
  }
#endif  // not implemented in baseline Galerkin_sansLG

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> boundaryGroups_;
};


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace_Wall_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, IntegrandType<Ti> integrandPDE[], int nPDE ) const
{
  SANS_ASSERT( nPDE == nDOF_ );

  ParamT paramL;            // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;               // unit normal (points out of domain)

  ArrayQ<T> qL;             // solution
  VectorArrayQ<T> gradqL;   // gradient

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, psi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradpsi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( psi_, nDOF_, qL );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradpsi_, nDOF_, gradqL );
  else
    gradqL = 0;

  call_with_derived<NDBCVector>(*this, bc_, qL, gradqL, paramL, nL, integrandPDE, nPDE);
}


template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell , class Topology     , class ElementParam>
template<class BC, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace_Wall_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
            const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrandPDE[], int nPDE ) const
{
  SANS_ASSERT( nPDE == nDOF_ );

#if 0   // not needed
  ParamT param;             // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;               // unit normal (points out of domain)

  ArrayQ<T> q;              // solution
  VectorArrayQ<T> gradq;    // gradient

  QuadPointCellType sRef;    // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, param );

  // unit normal: points to R
  traceUnitNormal( xfldElem_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, psi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradpsi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( psi_, nDOF_, q );
  qfldElem_.evalFromBasis( gradpsi_, nDOF_, gradq );
#endif

  // BC coefficients, data
  MatrixQ<Real> A, B;
  ArrayQ<Ti> bcdata;
  bc.coefficients( paramL, nL, A, B );
  bc.data( paramL, nL, bcdata );

  // compute the integrand
  weightedIntegrand( A, B, bcdata, qL, gradqL, paramL, nL, integrandPDE, nPDE );
}


#if 0   // need to do Surreal for circ
//---------------------------------------------------------------------------//
template <class PDE, class NDBCVector>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
void
IntegrandBoundaryTrace_Wall_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin>::
BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell,
                 TopologyL,     TopologyR,
                 ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, psiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, psiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradpsiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradpsiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( psiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( psiR_, nDOFR_, qR );

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL, qSurrealR;                // solution
  VectorArrayQ<SurrealClass> gradqSurrealL, gradqSurrealR;  // gradient

  qSurrealL = qL;
  qSurrealR = qR;

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradpsiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradpsiR_, nDOFR_, gradqR );

    gradqSurrealL = gradqL;
    gradqSurrealR = gradqR;
  }

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandRSurreal( nDOFR_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL, paramR,
                       qSurrealL, gradqL,
                       qSurrealR, gradqR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += dJ*psiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemR.PDE_qL(i,j) += dJ*psiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemL.PDE_qR(i,j) += dJ*psiR_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemR.PDE_qR(i,j) += dJ*psiR_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk

  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*2*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandLSurreal = 0;
      integrandRSurreal = 0;

      // compute the integrand
      weightedIntegrand( nL,
                         paramL, paramR,
                         qL, gradqSurrealL,
                         qR, gradqSurrealR,
                         integrandLSurreal.data(), nDOFL_,
                         integrandRSurreal.data(), nDOFR_, true );

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDE_qL(i,j) += dJ*gradpsiL_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemR.PDE_qL(i,j) += dJ*gradpsiL_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }

      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemL.PDE_qR(i,j) += dJ*gradpsiR_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemR.PDE_qR(i,j) += dJ*gradpsiR_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient
}
#endif


template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam >
template <class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace_Wall_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
weightedIntegrand( const MatrixQ<Real>& A, const MatrixQ<Real>& B, const ArrayQ<Ti>& bcdata,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                   const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrandPDE[], int nPDE ) const
{
  const VectorX& X = paramL;    // HACK ??
  //const VectorX& X = get<-1>(paramL);
  //const VectorX& X = paramL.right();        // cartesian coordinates of evaluation point

  // tau basis functions
  Real *tau = new Real[nDOF_];              // basis (L/R)
  //VectorX *gradtau = new VectorX[nDOF_];    // basis gradient (L/R)
  for (int k = 0; k < nDOF_; k++)
  {
    tau[k] = psi_[k];
    //gradtau[k] = gradpsi_[k];
  }

  for (int k = 0; k < nPDE; k++)
    integrandPDE[k] = 0;

  // PDE residual: weak form boundary integral

  ArrayQ<Tg> qnL = dot(nL, gradqL);

#undef USE_OLDFORM
#ifdef USE_OLDFORM
  for (int k = 0; k < nPDE; k++)
  {
    integrandPDE[k](0) += -dot(nL, gradpsi_[k])*qL(1);
    integrandPDE[k](1) += -tau[k]*qnL(0);
  }
#else   // integrand now consistent with PDE class
#ifdef USE_IPTF_NORM
  ArrayQ<Tg> gradqSq = dot(gradqL, gradqL);
  Tg   wghtD = pde_.gaussianD( X[0], X[1] ) * pow( gradqSq(0), pde_.normD()/2 - 1);
#else
  Real wghtD = pde_.gaussianD( X[0], X[1] );
#endif    // USE_IPTF_NORM

  for (int k = 0; k < nPDE; k++)
  {
    integrandPDE[k](0) += -psi_[k]*(qnL(1) + wghtD*qnL(0));
    integrandPDE[k](1) += -tau[k]*qnL(0);
  }
#endif    // USE_OLDFORM

  // PDE residual: sans-Lagrange BC terms

  ArrayQ<Tg> qsL = nL(1)*gradqL(0) - nL(0)*gradqL(1);           // d(q)/ds

#ifdef USE_IPTF_NORM
  Tg   wghtA = pde_.gaussianA( X[0], X[1] ) * pow( qsL(0)*qsL(0), pde_.normA()/2 - 1);
#else
  Real wghtA = pde_.gaussianA( X[0], X[1] );
#endif

#ifdef USE_OLDFORM
  Ti rsd1 = qnL(0) - bcdata(0);    // wall BC
  Ti rsd2 = qL(1);

  Ti fcn = wghtA * qsL(0);

  for (int k = 0; k < nPDE; k++)
  {
    Real psis = nL(1)*gradpsi_[k](0) - nL(0)*gradpsi_[k](1);    // d(psi)/ds
    integrandPDE[k](0) += dot(nL, gradpsi_[k])*rsd2 + psis*fcn;
    integrandPDE[k](1) += tau[k]*rsd1;
  }
#else     // properly shows strong-form BCs for sigma
  Ti rsd1 = qnL(0) - bcdata(0);    // wall BC
  Ti rsd2 = qnL(1) + wghtD*qnL(0);

  Ti fcn = wghtA * qsL(0);

  for (int k = 0; k < nPDE; k++)
  {
    Real psis = nL(1)*gradpsi_[k](0) - nL(0)*gradpsi_[k](1);    // d(psi)/ds
    integrandPDE[k](0) += psi_[k]*rsd2 + psis*fcn;
    integrandPDE[k](1) += tau[k]*rsd1;
  }
#endif

#if 0   // no infrastructure yet
  // Aux/global residual:

#ifdef USE_FIXCIRC
  integrandAux[0] = 0;
#else
  integrandAux[0] = phiVortex * tmp2;
#endif
#endif

  delete [] tau;
  //delete [] gradtau;
}


#if 0   // not implemented in baseline Galerkin_sansLG
template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace_Wall_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin>::
FieldWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType& integrandPDE ) const
{
  ParamTL paramL;   // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  VectorX nL;       // unit normal for left element (points to right element)
  VectorX nR;       //                              (points to left element)

  ArrayQ<T> qL, qR;                // solution
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients
  VectorArrayQ<T> dqn;             // jump in q.n

  ArrayQ<T> wL, wR;                // weight
  VectorArrayQ<T> gradwL, gradwR;  // weight gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: nL points to R; nR points to L
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);
  nR = -nL;

  // solution value, gradient
  qfldElemL_.eval( sRefL, qL );
  qfldElemR_.eval( sRefR, qR );
  if (pde_.hasFluxViscous() || pde_.hasSourceTrace())
  {
    xfldElemL_.evalGradient( sRefL, qfldElemL_, gradqL );
    xfldElemR_.evalGradient( sRefR, qfldElemR_, gradqR );
  }

  // weight value,
  wfldElemL_.eval( sRefL, wL );
  wfldElemR_.eval( sRefR, wR );
  if (pde_.hasFluxViscous())
  {
    xfldElemL_.evalGradient( sRefL, wfldElemL_, gradwL );
    xfldElemR_.evalGradient( sRefR, wfldElemR_, gradwR );
  }

  integrandL = 0;
  integrandR = 0;

  // PDE residual: weak form boundary integral

  ArrayQ<Ti> qnL = dot(nL, gradqL);
  ArrayQ<Ti> qnR = dot(nR, gradqR);

  ArrayQ<Ti> qnSum = 0.5*(qnR + qnL);
  ArrayQ<Ti> qnDif = 0.5*(qnR - qnL);

  integrandL += -wL*(qnSum - qnDif);
  integrandR += -wR*(qnSum + qnDif);

  // PDE residual: sans-Lagrange BC terms

  Real circ = bc_.getCirculation();

  ArrayQ<Ti> rsdMass = 0.5*(qnR + qnL);          // mass flux BC
  ArrayQ<Ti> rsdCirc = 0.5*((qR - qL) + circ);   // potential jump BC

  integrandL += wL*rsdMass - dot(nL, gradwL)*(-rsdCirc);
  integrandR += wR*rsdMass - dot(nR, gradwR)*(+rsdCirc);

#ifdef USE_NITSCHE
  // edge length
  Real ds = xfldElemTrace_.jacobianDeterminant();

  // adjacent element area
  Real areaL = xfldElemL_.jacobianDeterminant();
  Real areaR = xfldElemR_.jacobianDeterminant();

  // normal grid scale
  Real h = 0.5*(areaL + areaR)/ds;

  Real beta1  = 10;   // Nitsche parameters
  Real gamma2 = 10;

  integrandL += (beta1/(4*h))*(-wL)*rsdCirc + (gamma2*h)*dot(nL, gradwL)*rsdMass;
  integrandR += (beta1/(4*h))*(+wR)*rsdCirc + (gamma2*h)*dot(nR, gradwR)*rsdMass;
#endif
}
#endif  // not implemented in baseline Galerkin_sansLG

}   // namespace SANS

#endif  // INTEGRANDBOUNDARYTRACE_GALERKIN_WALL_IPTF_H
