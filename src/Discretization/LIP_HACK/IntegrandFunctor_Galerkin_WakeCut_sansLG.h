// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDFUNCTOR_GALERKIN_WAKECUT_SANSLG_H
#define INTEGRANDFUNCTOR_GALERKIN_WAKECUT_SANSLG_H

// cell/trace integrand operators: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#if 0
#include "pde/FullPotential/SolutionFunction3D_LIP.h"
#endif

namespace SANS
{

//----------------------------------------------------------------------------//
// Trace integrand: Galerkin

template <class PDE>
class IntegrandTrace_Galerkin_WakeCut_sansLG
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandTrace_Galerkin_WakeCut_sansLG(const PDE& pde, const std::vector<int>& BoundaryGroups,
                                       const int dupPointOffset, const std::vector<int>& KuttaPoints)
    : dupPointOffset(dupPointOffset), KuttaPoints(KuttaPoints), pde_(pde), BoundaryGroups_(BoundaryGroups) {}

  const int dupPointOffset;
  const std::vector<int> KuttaPoints;

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const PDE& pde,
             const ElementXFieldTrace& xfldElemTrace,
             const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldL& xfldElemL,
             const ElementQFieldL& qfldElemL,
             const ElementXFieldR& xfldElemR,
             const ElementQFieldR& qfldElemR ) :
             pde_(pde),
             xfldElemTrace_(xfldElemTrace),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR) {}


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRef, ArrayQ<T> integrandL[], const int nrsdL,
                                                     ArrayQ<T> integrandR[], const int nrsdR ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL, TopologyR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell , TopologyL    >& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElemL,
            const ElementXField<PhysDim, TopoDimCell , TopologyR    >& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyR    >& qfldElemR) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>(pde_,
                                                                                      xfldElemTrace,
                                                                                      canonicalTraceL, canonicalTraceR,
                                                                                      xfldElemL, qfldElemL,
                                                                                      xfldElemR, qfldElemR);
  }

protected:
  const PDE& pde_;
  const std::vector<int> BoundaryGroups_;
};


template <class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
bool
IntegrandTrace_Galerkin_WakeCut_sansLG<PDE>::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::needsEvaluation() const
{
  return ( pde_.hasFluxViscous() ||
           pde_.hasSource() );
}


template <class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
void
IntegrandTrace_Galerkin_WakeCut_sansLG<PDE>::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::operator()(
    const QuadPointTraceType& RefTrace, ArrayQ<T> integrandL[], const int nrsdL,
                                        ArrayQ<T> integrandR[], const int nrsdR ) const
{
  typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

  const int nDOFL = qfldElemL_.nDOF();             // total solution DOFs in left element
  const int nDOFR = qfldElemR_.nDOF();             // total solution DOFs in right element

  SANS_ASSERT(nrsdL == nDOFL);
  SANS_ASSERT(nrsdR == nDOFR);

  VectorX X = 0;
  VectorX nL, nR;           // unit normal for left element (points to right element)

  Real phiL[nDOFL];                                                // basis (left)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiL(nDOFL);  // basis gradient (left)
  Real phiR[nDOFR];                                                // basis (right)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiR(nDOFR);  // basis gradient (right)

  ArrayQ<T> qL, qR;           // solution
  VectorArrayQ gradqL, gradqR;  // gradient

  DLA::VectorS< PhysDim::D, Real > U; // Freestream Velocity

  QuadPointCellType sRefL;            // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, RefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, RefTrace, sRefR );

  // physical coordinates
  xfldElemTrace_.coordinates( RefTrace, X );

  // unit normal: L points to R
  xfldElemTrace_.unitNormal( RefTrace, nL );
  nR = -nL;

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL, nDOFL );
  qfldElemR_.evalBasis( sRefR, phiR, nDOFR );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL.data(), nDOFL );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR.data(), nDOFR );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL, nDOFL, qL );
  qfldElemR_.evalFromBasis( phiR, nDOFR, qR );
  qfldElemL_.evalFromBasis( gradphiL.data(), nDOFL, gradqL );
  qfldElemR_.evalFromBasis( gradphiR.data(), nDOFR, gradqR );

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] = 0;

#if 1
  // viscous flux
  DLA::VectorS<PhysDim::D, ArrayQ<T> > FL = 0, FR = 0;     // PDE flux

  pde_.fluxViscous( X, qL, gradqL, FL );
  pde_.fluxViscous( X, qR, gradqR, FR );

  ArrayQ<T> fluxL = dot(nL,FL);
  ArrayQ<T> fluxR = dot(nR,FR);

  // PDE flux terms

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += phiL[k]*fluxL;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += phiR[k]*fluxR;
#endif

  // mass conservation Exact BC (The PDE flux should be turned off for this one)
#if 0
  EllipticPotential exact;

  exact( {X[0], X[1], X[2]-nL[2]*1e-12}, gradqL);
  exact( {X[0], X[1], X[2]+nL[2]*1e-12}, gradqR);

  ArrayQ<T> qnL = dot(nL,gradqL);
  ArrayQ<T> qnR = dot(nR,gradqR);

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] -= phiL[k]*qnL;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] -= phiR[k]*qnR;
#endif

  // mass conservation Numerical BC
#if 1
  ArrayQ<T> qnL = dot(nL,gradqL);
  ArrayQ<T> qnR = dot(nR,gradqR);

  ArrayQ<T> qnsum = qnR + qnL;

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += 0.5*phiL[k]*qnsum;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += 0.5*phiR[k]*qnsum;
#endif

  // zero pressure jump ala David Eller
#if 0
  pde_.freestream(U);

  VectorArrayQ gradqjump = gradqR - gradqL;
  ArrayQ<T> Ugradqjump = dot(U,gradqjump);

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += -0.5*dot(U,gradphiL[k])*Ugradqjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] +=  0.5*dot(U,gradphiR[k])*Ugradqjump;
#endif

  // zero pressure jump normal wieghted
#if 0
  pde_.freestream(U);

  VectorArrayQ gradqjump = gradqR - gradqL;
  ArrayQ<T> Ugradqjump = dot(U,gradqjump);

//  nL += U;
//  nR += U;
  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += -0.5*dot(nL,gradphiL[k])*Ugradqjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] +=  0.5*dot(nR,gradphiR[k])*Ugradqjump;
#endif

// Zero jump in the tangential gradient
#if 1
  //pde_.freestream(U);
  U = pde_.Up(X);

  // Compute the surface tangent gradient
  gradqL -= dot(nL,gradqL)*nL;
  gradqR -= dot(nR,gradqR)*nR;

  Real Un = dot(nL,U); // Normal velocity

  DLA::VectorS< PhysDim::D, Real > Us = U - Un*nL; // Tangential velocity
  //Real V = sqrt( dot(Us, Us) );

  ArrayQ<T> Ugradqjump = dot(Us,gradqR) - dot(Us,gradqL);

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += -0.5*dot(nL,gradphiL[k])*Ugradqjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] +=  0.5*dot(nR,gradphiR[k])*Ugradqjump;

#endif

// Zero jump
#if 0
  ArrayQ<T> qjump = qR - qL;

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] +=  0.5*dot(nL,gradphiL[k])*qjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += -0.5*dot(nR,gradphiR[k])*qjump;
#endif

// jump( phi ) * jump( dq/dn )
#if 0
//  ArrayQ<T> qnL = dot(nL,gradqL);
//  ArrayQ<T> qnR = dot(nR,gradqR);

  ArrayQ<T> qnjump = qnR - qnL;

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += -0.5*phiL[k]*qnjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] +=  0.5*phiR[k]*qnjump;
#endif

  //Exact potential jump and mass flux
#if 0
  EllipticPotential exact;
/*
  DLA::VectorS< PhysDim::D, Real> gradgL, gradgR;  // exact gradient

  exact( {X[0], X[1], X[2]-nL[2]*1e-11}, gradgL);
  exact( {X[0], X[1], X[2]+nL[2]*1e-11}, gradgR);

  Real gnL = dot(nL,gradgL);
  Real gnR = dot(nR,gradgR);

  Real gnsum = gnR + gnL;
*/
  Real gL = exact( {X[0], X[1], X[2]-nL[2]*1e-11});
  Real gR = exact( {X[0], X[1], X[2]+nL[2]*1e-11});

  ArrayQ<T> G = (qR - qL) - (gR - gL);

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] +=  0.5*dot(nL,gradphiL[k])*G;// + 0.5*phiL[k]*gnsum;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += -0.5*dot(nR,gradphiR[k])*G;// + 0.5*phiR[k]*gnsum;
#endif

}

}

#endif  // INTEGRANDFUNCTOR_GALERKIN_WAKECUT_SANSLG_H
