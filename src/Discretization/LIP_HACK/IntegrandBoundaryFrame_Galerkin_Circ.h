// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFRAME_GALERKIN_CIRC_H
#define INTEGRANDBOUNDARYFRAME_GALERKIN_CIRC_H

// boundary-frame integral functional
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/TraceToCellRefCoord.h"

#include "LinearAlgebra/DenseLinAlg/tools/cross.h"


namespace SANS
{

//----------------------------------------------------------------------------//
template <class PDE>
class IntegrandBoundaryFrame_Galerkin_Circ
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryFrame_Galerkin_Circ( const PDE& pde, const std::vector<int>& BoundaryFrameGroups, const int dupPointOffset )
    : dupPointOffset(dupPointOffset), pde_(pde), BoundaryFrameGroups_(BoundaryFrameGroups)
  {}

  const int dupPointOffset;

  std::size_t nBoundaryFrameGroups() const { return BoundaryFrameGroups_.size(); }
  std::size_t boundaryFrameGroup(const int n) const { return BoundaryFrameGroups_[n]; }

  template<class T, class TopoDimFrame, class TopologyFrame,
                    class TopoDimTrace, class TopologyTraceL, class TopologyTraceR,
                    class TopoDimCell, class TopologyCellL, class TopologyCellR>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimFrame, TopologyFrame > ElementXFieldFrame;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTraceL > ElementXFieldTraceL;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTraceR > ElementXFieldTraceR;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyCellL > ElementXFieldCellL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyCellR > ElementXFieldCellR;

    typedef Element<ArrayQ<T>, TopoDimFrame, TopologyFrame > ElementGFieldFrame;
    typedef Element<ArrayQ<T>, TopoDimTrace , TopologyTraceL> ElementQFieldTraceL;
    typedef Element<ArrayQ<T>, TopoDimTrace , TopologyTraceR> ElementQFieldTraceR;
    typedef Element<ArrayQ<T>, TopoDimCell  , TopologyCellL> ElementQFieldCellL;
    typedef Element<ArrayQ<T>, TopoDimCell  , TopologyCellR> ElementQFieldCellR;

    typedef typename ElementXFieldFrame::RefCoordType RefCoordFrameType;
    typedef typename ElementXFieldTraceL::RefCoordType RefCoordTraceType;
    typedef typename ElementXFieldCellL::RefCoordType RefCoordCellTypeL;
    typedef typename ElementXFieldCellR::RefCoordType RefCoordCellTypeR;
    typedef typename ElementXFieldTraceL::VectorX VectorX;

    typedef QuadraturePoint<typename TopologyFrame::TopoDim> QuadPointFrameType;
    //typedef QuadraturePoint<typename TopologyTrace::TopoDim> QuadPointTraceType;
    //typedef QuadratureCellTracePoint<typename TopologyCellL::TopoDim> QuadPointCellLType;

    Functor( const PDE& pde,
             const ElementXFieldFrame& xfldElemFrame,
             const ElementGFieldFrame& GfldElemFrame,
             const ElementXFieldTraceL& xfldElemTraceL,
             const ElementXFieldTraceR& xfldElemTraceR,
             const CanonicalTraceToCell& canonicalFrameL,
             const CanonicalTraceToCell& canonicalFrameR,
             const CanonicalTraceToCell& canonicalTraceL,
             const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldCellL& xfldElemL,
             const ElementQFieldCellL& qfldElemL,
             const ElementXFieldCellR& xfldElemR,
             const ElementQFieldCellR& qfldElemR ) :
             pde_(pde), xfldElemFrame_(xfldElemFrame), GfldElemFrame_(GfldElemFrame),
             xfldElemTraceL_(xfldElemTraceL), xfldElemTraceR_(xfldElemTraceR),
             canonicalFrameL_(canonicalFrameL), canonicalFrameR_(canonicalFrameR),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR)
             {}

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // trace integrand
    void operator()( const QuadPointFrameType& sRefFrame,
                     ArrayQ<T> integrandPDEL[], int nrsdPDEL,
                     ArrayQ<T> integrandPDER[], int nrsdPDER,
                     ArrayQ<T> integrandKutta[], int nrsdKutta ) const
    {
      typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

      const int nDOFF = GfldElemFrame_.nDOF();         // total solution DOFs in frame element
      const int nDOFL = qfldElemL_.nDOF();             // total solution DOFs in left element
      const int nDOFR = qfldElemR_.nDOF();             // total solution DOFs in left element

      SANS_ASSERT(nrsdPDEL == nDOFL);
      SANS_ASSERT(nrsdPDER == nDOFR);
      SANS_ASSERT(nrsdKutta == nDOFF);

      VectorX xL, xR;
      VectorX nL, nR, t;           // unit normal for left element (points to right element)

      std::vector<Real> phiF(nDOFF);                                   // basis on frame

      std::vector<Real> phiL(nDOFL);                                   // basis (left)
      std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiL(nDOFL);  // basis gradient (left)
      std::vector<Real> phiR(nDOFR);                                   // basis (right)
      std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiR(nDOFR);  // basis gradient (right)

      ArrayQ<T> qL, qR, Gamma;
      VectorArrayQ gradqL, gradqR;  // gradients

      DLA::VectorS< PhysDim::D, Real > U; // Freestream Velocity

      RefCoordTraceType sRefTraceL;
      RefCoordTraceType sRefTraceR;
      RefCoordCellTypeL sRefCellL;
      RefCoordCellTypeR sRefCellR;

      TraceToCellRefCoord<TopologyFrame, TopoDimTrace, TopologyTraceL>::eval( canonicalFrameL_, sRefFrame.ref, sRefTraceL );
      TraceToCellRefCoord<TopologyFrame, TopoDimTrace, TopologyTraceR>::eval( canonicalFrameR_, sRefFrame.ref, sRefTraceR );
      TraceToCellRefCoord<TopologyTraceL, TopoDimCell, TopologyCellL>::eval( canonicalTraceL_, sRefTraceL, sRefCellL );
      TraceToCellRefCoord<TopologyTraceR, TopoDimCell, TopologyCellR>::eval( canonicalTraceR_, sRefTraceR, sRefCellR );

      // unit tangent on the frame element
      xfldElemFrame_.unitTangent( sRefFrame, t );

      // unit normal: L points to R
      xfldElemTraceL_.unitNormal( sRefTraceL, nL );
      xfldElemTraceR_.unitNormal( sRefTraceR, nR );

      // basis value, gradient
      GfldElemFrame_.evalBasis( sRefFrame, phiF.data(), nDOFF );
      qfldElemL_.evalBasis( sRefCellL, phiL.data(), nDOFL );
      qfldElemR_.evalBasis( sRefCellR, phiR.data(), nDOFR );
      xfldElemL_.evalBasisGradient( sRefCellL, qfldElemL_, gradphiL.data(), nDOFL );
      xfldElemR_.evalBasisGradient( sRefCellR, qfldElemR_, gradphiR.data(), nDOFR );

      xfldElemL_.eval( sRefCellL, xL );
      xfldElemR_.eval( sRefCellR, xR );

      // solution value, gradient
      GfldElemFrame_.evalFromBasis( phiF.data(), nDOFF, Gamma );
      qfldElemL_.evalFromBasis( phiL.data(), nDOFL, qL );
      qfldElemR_.evalFromBasis( phiR.data(), nDOFR, qR );
      qfldElemL_.evalFromBasis( gradphiL.data(), nDOFL, gradqL );
      qfldElemR_.evalFromBasis( gradphiR.data(), nDOFR, gradqR );

      for (int n = 0; n < PhysDim::D; n++)
        SANS_ASSERT( fabs(xL[n] - xR[n]) < 1e-12 );

      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] = 0;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] = 0;

      for (int k = 0; k < nrsdKutta; k++)
        integrandKutta[k] = 0;

      //pde_.freestream(U);
      U = pde_.Up(xL);

      //VectorX NL = cross(t,nL);
      //VectorX NR = cross(t,nR);
      //Real Un = dot(U,NR) - dot(U,NL); //Velocity approximately in the direction of the wake

      ArrayQ<T> qjump = qR - qL;

      Real UnL = dot(nL,U); // Normal velocity
      DLA::VectorS< PhysDim::D, Real > UsL = U - UnL*nL; // Tangential velocity

      Real UnR = dot(nR,U); // Normal velocity
      DLA::VectorS< PhysDim::D, Real > UsR = U - UnR*nR; // Tangential velocity

      // Compute the surface tangent gradient
      gradqL -= dot(nL,gradqL)*nL;
      gradqR -= dot(nR,gradqR)*nR;

      ArrayQ<T> Ugradqjump = dot(UsR, gradqR) - dot(UsL, gradqL);
//      ArrayQ<T> Ugradqjump = dot(U, gradqR) - dot(U, gradqL);
      //Ugradqjump += 0.5*(dot(gradqR,gradqR) - dot(gradqL,gradqL));

//      for (int k = 0; k < nDOFL; k++)
//        integrandPDEL[k] += -0.5*dot(nL,gradphiL[k])*(qjump + Gamma);
//
//      for (int k = 0; k < nDOFR; k++)
//        integrandPDER[k] +=  0.5*dot(nR,gradphiR[k])*(qjump + Gamma);

//      for (int k = 0; k < nDOFL; k++)
//        integrandPDEL[k] += -0.5*phiL[k]*(qjump + Gamma);
//
//      for (int k = 0; k < nDOFR; k++)
//        integrandPDER[k] +=  0.5*phiR[k]*(qjump + Gamma);

//      for (int k = 0; k < nDOFR; k++)
//        integrandPDER[k] += phiR[k]*(qjump + Gamma);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += dot(nR,gradphiR[k])*(qjump + Gamma);

//      for (int k = 0; k < nDOFR; k++)
//        integrandPDER[k] += phiR[k]*Ugradqjump; //Ugradqjump; //

#if 1

//      for (int k = 0; k < nDOFF; k++)
//        integrandKutta[k] += phiF[k]*(qjump + Gamma);

//      for (int k = 0; k < nDOFF; k++)
//        integrandKutta[k] += phiF[k]*Gamma;

      for (int k = 0; k < nDOFF; k++)
        integrandKutta[k] += phiF[k]*Ugradqjump;

//      for (int k = 0; k < nDOFL; k++)
//        integrandPDEL[k] += -0.5*dot(nL,gradphiL[k])*Ugradqjump;
//
//      for (int k = 0; k < nDOFR; k++)
//        integrandPDER[k] +=  0.5*dot(nR,gradphiR[k])*Ugradqjump;
//

#endif

    }

  protected:
    const PDE& pde_;
    const ElementXFieldFrame& xfldElemFrame_;
    const ElementGFieldFrame& GfldElemFrame_;
    const ElementXFieldTraceL& xfldElemTraceL_;
    const ElementXFieldTraceR& xfldElemTraceR_;
    const CanonicalTraceToCell canonicalFrameL_;
    const CanonicalTraceToCell canonicalFrameR_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldCellL& xfldElemL_;
    const ElementQFieldCellL& qfldElemL_;
    const ElementXFieldCellR& xfldElemR_;
    const ElementQFieldCellR& qfldElemR_;
  };

  template<class T, class TopoDimFrame, class TopologyFrame,
                    class TopoDimTrace, class TopologyTraceL, class TopologyTraceR,
                    class TopoDimCell, class TopologyCellL, class TopologyCellR>
  Functor<T, TopoDimFrame, TopologyFrame,
             TopoDimTrace, TopologyTraceL, TopologyTraceR,
             TopoDimCell, TopologyCellL, TopologyCellR>
  integrand(const ElementXField<PhysDim, TopoDimFrame, TopologyFrame>& xfldElemFrame,
            const Element<ArrayQ<T>    , TopoDimFrame, TopologyFrame>& GfldElemFrame,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTraceL>& xfldElemTraceL,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTraceR>& xfldElemTraceR,
            const CanonicalTraceToCell& canonicalFrameL,
            const CanonicalTraceToCell& canonicalFrameR,
            const CanonicalTraceToCell& canonicalTraceL,
            const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell , TopologyCellL >& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyCellL >& qfldElemL,
            const ElementXField<PhysDim, TopoDimCell , TopologyCellR >& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyCellR >& qfldElemR ) const
  {
    return Functor<T, TopoDimFrame, TopologyFrame,
                      TopoDimTrace, TopologyTraceL, TopologyTraceR,
                      TopoDimCell, TopologyCellL, TopologyCellR>(pde_, xfldElemFrame, GfldElemFrame,
                                                                 xfldElemTraceL, xfldElemTraceR,
                                                                 canonicalFrameL, canonicalFrameR,
                                                                 canonicalTraceL, canonicalTraceR,
                                                                 xfldElemL, qfldElemL,
                                                                 xfldElemR, qfldElemR );
  }

protected:
  const PDE& pde_;
  const std::vector<int> BoundaryFrameGroups_;
};

} //namespace SANS

#endif // INTEGRANDBOUNDARYFRAME_GALERKIN_CIRC_H
