// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_GALERKIN_WAKECUT_LIP_H
#define JACOBIANBOUNDARYTRACE_GALERKIN_WAKECUT_LIP_H

// jacobian boundary-trace integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "IntegrandFunctor_Galerkin_WakeCut_LIP.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class QFieldCellGroupType, class QFieldTraceGroupType,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryTrace_Galerkin_WakeCut_LIP_ScatterAdd(
    const QFieldTraceGroupType& lgfldTrace,
    const QFieldCellGroupType& qfldCellL,
    const QFieldCellGroupType& qfldCellR,
    const int elemL, const int elemR, const int elem,
    int mapDOFGlobalL[], const int nDOFL,
    int mapDOFGlobalR[], const int nDOFR,
    int mapDOFGlobalWake[], const int nDOFWake,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemR,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_lgTrace,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qElemR,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_lgTrace,
    SANS::DLA::MatrixD<MatrixQ>& mtxWake_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxWake_qElemR,
    SANS::DLA::MatrixD<MatrixQ>& mtxWake_lgTrace,
    SparseMatrix<MatrixQ>& mtxGlobalPDE_q,
    SparseMatrix<MatrixQ>& mtxGlobalPDE_lg,
    SparseMatrix<MatrixQ>& mtxGlobalWake_q,
    SparseMatrix<MatrixQ>& mtxGlobalWake_lg )
{
  lgfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalWake, nDOFWake );

  qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL, nDOFL );
  qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR, nDOFR );

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, mapDOFGlobalL, nDOFL, mapDOFGlobalL, nDOFL );
  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemR, mapDOFGlobalL, nDOFL, mapDOFGlobalR, nDOFR );
  mtxGlobalPDE_lg.scatterAdd( mtxPDEElemL_lgTrace, mapDOFGlobalL, nDOFL, mapDOFGlobalWake, nDOFWake );

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qElemL, mapDOFGlobalR, nDOFR, mapDOFGlobalL, nDOFL );
  mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qElemR, mapDOFGlobalR, nDOFR, mapDOFGlobalR, nDOFR );
  mtxGlobalPDE_lg.scatterAdd( mtxPDEElemR_lgTrace, mapDOFGlobalR, nDOFR, mapDOFGlobalWake, nDOFWake );

  mtxGlobalWake_q.scatterAdd( mtxWake_qElemL, mapDOFGlobalWake, nDOFWake, mapDOFGlobalL, nDOFL );
  mtxGlobalWake_q.scatterAdd( mtxWake_qElemR, mapDOFGlobalWake, nDOFWake, mapDOFGlobalR, nDOFR );
  mtxGlobalWake_lg.scatterAdd( mtxWake_lgTrace, mapDOFGlobalWake, nDOFWake, mapDOFGlobalWake, nDOFWake );
}


//----------------------------------------------------------------------------//
template <class QFieldCellGroupType, class QFieldTraceGroupType,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryTrace_Galerkin_WakeCut_LIP_ScatterAdd(
    const QFieldTraceGroupType& lgfldTrace,
    const QFieldCellGroupType& qfldCellL,
    const int elemL, const int elem,
    int mapDOFGlobalL[], const int nDOFL,
    int mapDOFGlobalWake[], const int nDOFWake,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_lgTrace,
    SANS::DLA::MatrixD<MatrixQ>& mtxWake_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxWake_lgTrace,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_lg,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalWake_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalWake_lg )
{

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryWake_ScatterAdd: elemL, elem = " << elemL << ," " << elem << std::endl;
  std::cout << "JacobianPDE_Group_2DBoundaryWake_ScatterAdd (before): mtxPDEGlobal = ";
  mtxPDEGlobal.dump(2);
#endif

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryWake_ScatterAdd: mtxGlobalPDE_q = ";  mtxGlobalPDE_q.dump(2);
#endif

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
  mtxGlobalPDE_lg.scatterAdd( mtxPDEElemL_lgTrace, elemL, elem );

  mtxGlobalWake_q.scatterAdd( mtxWake_qElemL, elem, elemL );
  mtxGlobalWake_lg.scatterAdd( mtxWake_lgTrace, elem, elem );
}


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//
//  functions dispatched based on left (L) element topology
//
//  process:
//  (a) loop over groups; dispatch to L (JacobianPDE_LeftTopology_Integral2DBoundaryWake)
//  (b) call base class routine with specific L (JacobianPDE_Group_Integral2DBoundaryWake w/ Base& params)
//  (c) cast to specific L and call L-specific topology routine (JacobianPDE_Group_Integral2DBoundaryWake)
//  (d) loop over Wakes and integrate


template <class Surreal, class TopologyTrace, class TopologyL, class TopologyR,
          class PhysDim, class TopoDim, class ArrayQ,
          class PDE,
          class SparseMatrix>
void
JacobianBoundaryTrace_Galerkin_WakeCut_LIP_Group_Integral(
    const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR>& xfldCellR,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q,
    SparseMatrix& mtxGlobalPDE_lg,
    SparseMatrix& mtxGlobalWake_q,
    SparseMatrix& mtxGlobalWake_lg )
{
  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
  typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  ElementXFieldClassL xfldElemR( xfldCellR.basis() );
  ElementQFieldClassL qfldElemR( qfldCellR.basis() );

  ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );
  ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

  // variables/equations per DOF
  const int nEqn = PDE::N;

  // DOFs per element
  int nDOFL = qfldElemL.nDOF();
  int nDOFR = qfldElemR.nDOF();

  // DOFs per trace
  int nDOFTrace = lgfldElemTrace.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobalL(nDOFL);
  std::vector<int> mapDOFGlobalR(nDOFR);
  std::vector<int> mapDOFGlobalTrace(nDOFTrace);

  // trace element integral
  GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal, ArrayQSurreal>
    integral(quadratureorder, nDOFL, nDOFR, nDOFTrace);

  // element integrand/residuals
  std::unique_ptr<ArrayQSurreal[]> rsdPDEElemL( new ArrayQSurreal[nDOFL] );
  std::unique_ptr<ArrayQSurreal[]> rsdPDEElemR( new ArrayQSurreal[nDOFR] );
  std::unique_ptr<ArrayQSurreal[]> rsdWakeTrace( new ArrayQSurreal[nDOFTrace] );

  // element jacobians
  MatrixElemClass mtxPDEElemL_qElemL(nDOFL, nDOFL);
  MatrixElemClass mtxPDEElemL_qElemR(nDOFL, nDOFR);
  MatrixElemClass mtxPDEElemL_lgTrace(nDOFL, nDOFTrace);

  MatrixElemClass mtxPDEElemR_qElemL(nDOFR, nDOFL);
  MatrixElemClass mtxPDEElemR_qElemR(nDOFR, nDOFR);
  MatrixElemClass mtxPDEElemR_lgTrace(nDOFR, nDOFTrace);

  MatrixElemClass mtxWake_qElemL(nDOFTrace, nDOFL);
  MatrixElemClass mtxWake_qElemR(nDOFTrace, nDOFR);
  MatrixElemClass mtxWake_lgTrace(nDOFTrace, nDOFTrace);

  // loop over elements within group
  int nelem = xfldTrace.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // zero element Jacobians
    mtxPDEElemL_qElemL = 0;
    mtxPDEElemL_qElemR = 0;
    mtxPDEElemL_lgTrace = 0;

    mtxPDEElemR_qElemL = 0;
    mtxPDEElemR_qElemR = 0;
    mtxPDEElemR_lgTrace = 0;

    mtxWake_qElemL = 0;
    mtxWake_qElemR = 0;
    mtxWake_lgTrace = 0;

    // left element
    const int elemL = xfldTrace.getElementLeft( elem );
    const int elemR = xfldTrace.getElementRight( elem );
    const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldCellR.getElement( xfldElemR, elemR );
    qfldCellR.getElement( qfldElemR, elemR );

    xfldTrace.getElement(  xfldElemTrace, elem );
    lgfldTrace.getElement( lgfldElemTrace, elem );

    // number of simultaneous derivatives per functor call
    const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nEqn*(nDOFL + nDOFR + nDOFTrace); nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot;
      for (int j = 0; j < nDOFL; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
        }
      }
      for (int j = 0; j < nDOFR; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(qfldElemR.DOF(j), n).deriv(k) = 0;

          slot = nEqn*(nDOFL + j) + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemR.DOF(j), n).deriv(slot - nchunk) = 1;
        }
      }
      for (int j = 0; j < nDOFTrace; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(lgfldElemTrace.DOF(j), n).deriv(k) = 0;

          slot = nEqn*(nDOFL + nDOFR + j) + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(lgfldElemTrace.DOF(j), n).deriv(slot - nchunk) = 1;
        }
      }

      // line integration for canonical element

      for (int n = 0; n < nDOFL; n++)
        rsdPDEElemL[n] = 0;

      for (int n = 0; n < nDOFR; n++)
        rsdPDEElemR[n] = 0;

      for (int n = 0; n < nDOFTrace; n++)
        rsdWakeTrace[n] = 0;

      integral(
          fcn.integrand(xfldElemTrace, lgfldElemTrace,
                        canonicalTraceL, canonicalTraceR,
                        xfldElemL, qfldElemL,
                        xfldElemR, qfldElemR),
          xfldElemTrace,
          rsdPDEElemL.get(), nDOFL,
          rsdPDEElemR.get(), nDOFR,
          rsdWakeTrace.get(), nDOFTrace );

      // accumulate derivatives into element jacobian

      for (int j = 0; j < nDOFL; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int i = 0; i < nDOFL; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemL_qElemL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

            for (int i = 0; i < nDOFR; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemR_qElemL(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot - nchunk);

            for (int i = 0; i < nDOFTrace; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxWake_qElemL(i,j), m,n) = DLA::index(rsdWakeTrace[i], m).deriv(slot - nchunk);
          }
        }
      }

      for (int j = 0; j < nDOFR; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*(nDOFL + j) + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int i = 0; i < nDOFL; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemL_qElemR(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

            for (int i = 0; i < nDOFR; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemR_qElemR(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot - nchunk);

            for (int i = 0; i < nDOFTrace; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxWake_qElemR(i,j), m,n) = DLA::index(rsdWakeTrace[i], m).deriv(slot - nchunk);
          }
        }
      }

      for (int j = 0; j < nDOFTrace; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*(nDOFL + nDOFR + j) + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int i = 0; i < nDOFL; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemL_lgTrace(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

            for (int i = 0; i < nDOFR; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemR_lgTrace(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot - nchunk);

            for (int i = 0; i < nDOFTrace; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxWake_lgTrace(i,j), m,n) = DLA::index(rsdWakeTrace[i], m).deriv(slot - nchunk);
          }
        }
      }
    }   // nchunk

    // scatter-add element jacobian to global

    JacobianBoundaryTrace_Galerkin_WakeCut_LIP_ScatterAdd(
        lgfldTrace, qfldCellL, qfldCellR,
        elemL, elemR, elem,
        mapDOFGlobalL.data(), nDOFL,
        mapDOFGlobalR.data(), nDOFR,
        mapDOFGlobalTrace.data(), nDOFTrace,
        mtxPDEElemL_qElemL, mtxPDEElemL_qElemR, mtxPDEElemL_lgTrace,
        mtxPDEElemR_qElemL, mtxPDEElemR_qElemR, mtxPDEElemR_lgTrace,
        mtxWake_qElemL    , mtxWake_qElemR    , mtxWake_lgTrace,
        mtxGlobalPDE_q,
        mtxGlobalPDE_lg,
        mtxGlobalWake_q,
        mtxGlobalWake_lg );

  }
}


//----------------------------------------------------------------------------//
template <class Surreal, class TopologyTrace, class TopologyL, class TopologyR,
          class PDE,
          class PhysDim, class TopoDim, class ArrayQ,
          class SparseMatrix>
void
JacobianBoundaryTrace_Galerkin_WakeCut_LIP_Group(
    const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q,
    SparseMatrix& mtxGlobalPDE_lg,
    SparseMatrix& mtxGlobalWake_q,
    SparseMatrix& mtxGlobalWake_lg )
{
  const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

  const int groupL = xfldTrace.getGroupLeft();
  const int groupR = xfldTrace.getGroupRight();

  // Integrate over the trace group
  JacobianBoundaryTrace_Galerkin_WakeCut_LIP_Group_Integral<Surreal,TopologyTrace,TopologyL,TopologyR,PhysDim,TopoDim,ArrayQ>(
      fcn, xfldTrace, lgfldTrace,
      xfld.template getCellGroup<TopologyL>(groupL), qfld.template getCellGroup<TopologyL>(groupL),
      xfld.template getCellGroup<TopologyR>(groupR), qfld.template getCellGroup<TopologyR>(groupR),
      quadratureorder,
      mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalWake_q, mtxGlobalWake_lg );
}

template<class Surreal, class TopoDim>
class JacobianBoundaryTrace_Galerkin_WakeCut_LIP;

#if 0
template<class Surreal>
class JacobianBoundaryTrace_Galerkin_WakeCut_LIP<Surreal,TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  //----------------------------------------------------------------------------//
  template <class TopologyTrace,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  LeftTopology(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q,
      SparseMatrix& mtxGlobalPDE_lg,
      SparseMatrix& mtxGlobalWake_q,
      SparseMatrix& mtxGlobalWake_lg )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // integrate over the boundary
      JacobianBoundaryTrace_Galerkin_WakeCut_LIP_Group<Surreal, TopologyTrace, Triangle>(
          fcn, xfldTrace, qfld, lgfldTrace,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalWake_q, mtxGlobalWake_lg );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryTrace_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q,
      SparseMatrix& mtxGlobalPDE_lg,
      SparseMatrix& mtxGlobalWake_q,
      SparseMatrix& mtxGlobalWake_lg )
  {
    SANS_ASSERT( mtxGlobalPDE_q.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalPDE_lg.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_lg.n() == lgfld.nDOF() );

    SANS_ASSERT( mtxGlobalWake_q.m() == lgfld.nDOF() );
    SANS_ASSERT( mtxGlobalWake_q.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalWake_lg.m() == lgfld.nDOF() );
    SANS_ASSERT( mtxGlobalWake_lg.n() == lgfld.nDOF() );

    SANS_ASSERT( &qfld.getXField() == &lgfld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over line element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        // dispatch to determine left topology
        LeftTopology<Line>(
            fcn,
            xfld.template getBoundaryTraceGroup<Line>(boundaryGroup),
            qfld,
            lgfld.template getBoundaryTraceGroup<Line>(boundaryGroup),
            quadratureorder[boundaryGroup],
            mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalWake_q, mtxGlobalWake_lg );
      }
      else
      {
        char msg[] = "Error in JacobianBoundaryTrace_Galerkin<TopoD2>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};
#endif


template<class Surreal>
class JacobianBoundaryTrace_Galerkin_WakeCut_LIP<Surreal,TopoD3>
{
public:
  typedef TopoD3 TopoDim;
  //----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  RightTopology(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q,
      SparseMatrix& mtxGlobalPDE_lg,
      SparseMatrix& mtxGlobalWake_q,
      SparseMatrix& mtxGlobalWake_lg )
  {
    // determine topology for R
    const int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      JacobianBoundaryTrace_Galerkin_WakeCut_LIP_Group<Surreal, TopologyTrace, TopologyL, Tet>(
          fcn, xfldTrace, lgfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalWake_q, mtxGlobalWake_lg );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::RightTopology: unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class TopologyTrace,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  LeftTopology(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q,
      SparseMatrix& mtxGlobalPDE_lg,
      SparseMatrix& mtxGlobalWake_q,
      SparseMatrix& mtxGlobalWake_lg )
  {

    // determine topology for L
    const int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      RightTopology<TopologyTrace, Tet>(
          fcn, xfldTrace, lgfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalWake_q, mtxGlobalWake_lg );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandTrace_Galerkin_WakeCut_LIP<PDE>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q,
      SparseMatrix& mtxGlobalPDE_lg,
      SparseMatrix& mtxGlobalWake_q,
      SparseMatrix& mtxGlobalWake_lg )
  {
    SANS_ASSERT( mtxGlobalPDE_q.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalPDE_lg.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_lg.n() == lgfld.nDOF() );

    SANS_ASSERT( mtxGlobalWake_q.m() == lgfld.nDOF() );
    SANS_ASSERT( mtxGlobalWake_q.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalWake_lg.m() == lgfld.nDOF() );
    SANS_ASSERT( mtxGlobalWake_lg.n() == lgfld.nDOF() );

    SANS_ASSERT( &qfld.getXField() == &lgfld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over line element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Triangle) )
      {
        // dispatch to determine left topology
        LeftTopology<Triangle>(
            fcn,
            xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroup),
            lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup],
            mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalWake_q, mtxGlobalWake_lg );
      }
      else
      {
        char msg[] = "Error in JacobianBoundaryTrace_Galerkin_WakeCut_LIP<TopoD3>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};


}

#endif  // JACOBIANBOUNDARYTRACE_GALERKIN_WAKECUT_LIP_H
