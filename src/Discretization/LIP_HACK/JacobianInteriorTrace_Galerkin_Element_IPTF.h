// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_GALERKIN_ELEMENT_IPTF_H
#define JACOBIANINTERIORTRACE_GALERKIN_ELEMENT_IPTF_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Discretization/JacobianElementMatrix.h"

namespace SANS
{

struct JacobianElemInteriorTraceSize_IPTF
{
  JacobianElemInteriorTraceSize_IPTF( const int nTest, const int nDOFL, const int nDOFR, const int nAux )
    : nTest(nTest), nDOFL(nDOFL), nDOFR(nDOFR), nAux(nAux) {}

  const int nTest;
  const int nDOFL;
  const int nDOFR;
  const int nAux;
};


template<class MatrixQ>
struct JacobianElemInteriorTrace_Galerkin_IPTF : JacElemMatrixType< JacobianElemInteriorTrace_Galerkin_IPTF<MatrixQ> >
{
  // PDE Jacobian wrt q, glb
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianElemInteriorTrace_Galerkin_IPTF(const JacobianElemInteriorTraceSize_IPTF& size)
   : nTest(size.nTest), nDOFL(size.nDOFL), nDOFR(size.nDOFR), nAux(size.nAux),
     rsd_qL(size.nTest, size.nDOFL),
     rsd_qR(size.nTest, size.nDOFR),
     rsd_glb(size.nTest, size.nAux)
  {}

  const int nTest;
  const int nDOFL;
  const int nDOFR;
  const int nAux;

  // element PDE jacobian matrices wrt q
  MatrixElemClass rsd_qL;
  MatrixElemClass rsd_qR;
  MatrixElemClass rsd_glb;

  inline Real operator=( const Real s )
  {
    rsd_qL  = s;
    rsd_qR  = s;
    rsd_glb = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemInteriorTrace_Galerkin_IPTF& operator+=(
      const JacElemMulScalar< JacobianElemInteriorTrace_Galerkin_IPTF >& mul )
  {
    rsd_qL  += mul.s*mul.mtx.rsd_qL;
    rsd_qR  += mul.s*mul.mtx.rsd_qR;
    rsd_glb += mul.s*mul.mtx.rsd_glb;

    return *this;
  }
};

}   // namespace SANS

#endif // JACOBIANINTERIORTRACE_GALERKIN_ELEMENT_IPTF_H
