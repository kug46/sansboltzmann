// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_GALERKIN_KUTTA_H
#define JACOBIANCELL_GALERKIN_KUTTA_H

// cell integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Field_DG_BoundaryFrame.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "IntegrandFunctorCell_Galerkin_Kutta_LIP.h"

namespace SANS
{
#if 0
//----------------------------------------------------------------------------//
template <class QFieldCellGroupType, class MatrixQ, template <class> class SparseMatrix>
void
JacobianCell_Galerkin_Group_ScatterAdd(
    const QFieldCellGroupType& qfld,
    const int elem,
    int mapDOFGlobal[], const int nDOF,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElem,
    SparseMatrix<MatrixQ>& mtxPDEGlobal )
{
  qfld.associativity( elem ).getGlobalMapping( mapDOFGlobal, nDOF );

#if 0
  std::cout << "JacobianPDE_Group_ScatterAdd<Real>: mapDOFGlobal = ";
  for (int i = 0; i < nDOF; i++)
    std::cout << mapDOFGlobal[i] << " ";
  std::cout << std::endl;
#endif

#if 0
    std::cout << "JacobianPDE_Group_ScatterAdd<Real>: mtxPDEElem = " << std::endl;
    for (int i = 0; i < nDOF; i++)
    {
      std::cout << " ";
      for (int j = 0; j < nDOF; j++)
        std::cout << " " << mtxPDEElem(i,j)(0,0);
      std::cout << std::endl;
    }
#endif

#if 0
    std::cout << "JacobianPDE_Group_ScatterAdd<Real>: mtxPDEGlobal = " << std::endl;
    int ig, jg;
    for (int i = 0; i < nDOF; i++)
    {
      ig = mapDOFGlobal[i];
      std::cout << " ";
      for (int j = 0; j < nDOF; j++)
      {
        jg = mapDOFGlobal[j];
        std::cout << " " << mtxPDEGlobal(ig,jg);
      }
      std::cout << std::endl;
    }
#endif

  mtxPDEGlobal.scatterAdd( mtxPDEElem, mapDOFGlobal, nDOF );

#if 0
  WriteMatrixMarketFile( mtxPDEGlobal, std::cout );
#endif
}


//----------------------------------------------------------------------------//
template <class QFieldCellGroupType, class MatrixQ, template <class> class SparseMatrix>
void
JacobianCell_Galerkin_Group_ScatterAdd(
    const QFieldCellGroupType& qfld,
    const int elem,
    int mapDOFGlobal[], const int nDOF,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElem,
    SparseMatrix< SANS::DLA::MatrixD< MatrixQ > >& mtxPDEGlobal )
{

#if 0
  std::cout << "JacobianPDE_Group_ScatterAdd< MatrixD<Real> >: row, col = " << elem << elem << std::endl;
#endif

  mtxPDEGlobal.scatterAdd( mtxPDEElem, elem, elem );
}
#endif

//----------------------------------------------------------------------------//
// 2D Galerkin area integral
//
// topology specific group integral
//
// template parameters:
//   Topology                               element topology (e.g. Triangle)
//   PDE                                    PDE class; used as PDE and PDE
//   Surreal                                auto-differentiation class (e.g. SurrealS)
//   Integrand2DAreaFunctor<Topology,PDE>   integrand functor
//   SparseMatrix                           sparse matrix class with PDE::MatrixQ elements

template <class Surreal, class Topology, class PhysDim, class TopoDim, class ArrayQ,
          class PDE,
          class SparseMatrix>
void
JacobianCell_Galerkin_Kutta_Group_Integral(
    const IntegrandCell_Galerkin_Kutta_LIP<PDE>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldGroup,
    const int quadratureorder,
    const std::map<int,int>& mapKuttaGlobal,
    SparseMatrix& mtxPDEGlobal, int sgn )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;

  typedef SANS::DLA::MatrixD<MatrixQ> MatrixElemClass;

  //SANS_ASSERT( mtxPDEGlobal.m() == qfldGroup.nDOF() );
  //SANS_ASSERT( mtxPDEGlobal.n() == qfldGroup.nDOF() );

  // element field variables
  ElementXFieldClass xfldElem( xfldGroup.basis() );
  ElementQFieldSurrealClass qfldElemSurreal( qfldGroup.basis() );

  // DOFs per element
  const int nDOFX = xfldElem.nDOF();
  const int nDOF = qfldElemSurreal.nDOF();

  // variables/equations per DOF
  const int nEqn = PDE::N;

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobalX(nDOFX);
  std::vector<int> mapDOFGlobalCell(nDOF);

  SANS_ASSERT(nDOFX == nDOF);

  // element integral
  GalerkinWeightedIntegral<TopoDim, Topology, ArrayQSurreal> integral(quadratureorder, nDOF);

  // element integrand/residual
  std::vector<ArrayQSurreal> rsdPDEElemSurreal( nDOF );

  std::vector<int> basisMap(nDOF, 0);

  // number of simultaneous derivatives per functor call
  const int nDeriv = DLA::index(qfldElemSurreal.DOF(0),0).size();

  // loop over elements within group
  const int nelem = xfldGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {

    // copy global grid/solution DOFs to element
    xfldGroup.getElement( xfldElem, elem );
    qfldGroup.getElement( qfldElemSurreal, elem );

    xfldGroup.associativity( elem ).getGlobalMapping( mapDOFGlobalX.data(), nDOFX );

    int nFrameDOF = 0;
    for (int n = 0; n < nDOFX; n++)
    {
      basisMap[n] = 0;
      try
      {
        mapKuttaGlobal.at(mapDOFGlobalX[n]);
        nFrameDOF++;
      }
      catch ( const std::exception& ) {}
    }

    if ( nFrameDOF == 0 ) continue;

    // element jacobian matrix
    MatrixElemClass mtxPDEElem(nFrameDOF, nDOF);

    // zero element Jacobian
    mtxPDEElem = 0;

    // Get the frame global DOF mapping
    std::vector<int> mapDOFGlobalFrame(nFrameDOF);
    int iF = 0;
    for (int n = 0; n < nDOFX; n++)
    {
      try
      {
        int nGlobal = mapKuttaGlobal.at(mapDOFGlobalX[n]);
        mapDOFGlobalFrame[iF] = nGlobal;
        iF++;
        basisMap[n] = 1;
      }
      catch ( const std::exception& ) {}
    }

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nEqn*nDOF; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot;
      for (int j = 0; j < nDOF; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(qfldElemSurreal.DOF(j), n).deriv(k) = 0;

          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemSurreal.DOF(j), n).deriv(slot - nchunk) = 1;
        }
      }

      for (int n = 0; n < nDOF; n++)
        rsdPDEElemSurreal[n] = 0;

      // cell integration for canonical element
      integral( fcn.integrand(xfldElem, qfldElemSurreal), xfldElem, rsdPDEElemSurreal.data(), nDOF );

#if 0
      std::cout << "JacobianPDE_Group_Integral2DArea: nchunk = " << nchunk << std::endl;
      for (int n = 0; n < nDOF; n++)
      {
        std::cout << "  rsdPDEElemSurreal[" << n << "] =" << std::endl;
        rsdPDEElemSurreal[n].dump(4);
      }
#endif

      // accumulate derivatives into element jacobian
      for (int j = 0; j < nDOF; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            int ii = 0;
            for (int i = 0; i < nDOF; i++)
            {
              if ( basisMap[i] == 0 ) continue; // Skip any basis functions that are not 1 on the TE
              for (int m = 0; m < nEqn; m++)
              {
                SANS_ASSERT( ii < nFrameDOF && j < nDOF );
                DLA::index(mtxPDEElem(ii,j), m,n) = sgn*DLA::index(rsdPDEElemSurreal[i], m).deriv(slot - nchunk);
              }
              ii++;
            }
          }
        }
      }
    }   // nchunk

#if 0
    std::cout << "JacobianPDE_Group_Integral2DArea: mtxPDEElem = " << std::endl;
    for (int i = 0; i < nDOF; i++)
    {
      std::cout << " ";
      for (int j = 0; j < nDOF; j++)
        std::cout << " " << mtxPDEElem(i,j)(0,0);
      std::cout << std::endl;
    }
#endif

    // scatter-add element jacobian to global
    //JacobianCell_Galerkin_Group_ScatterAdd( qfld, elem, mapDOFGlobal, nDOF, mtxPDEElem, mtxPDEGlobal );

    qfldGroup.associativity( elem ).getGlobalMapping( mapDOFGlobalCell.data(), nDOF );

    mtxPDEGlobal.scatterAdd( mtxPDEElem, mapDOFGlobalFrame.data(), nFrameDOF, mapDOFGlobalCell.data(), nDOF );

  }
}


//----------------------------------------------------------------------------//
template<class Surreal, class TopDim>
class JacobianCell_Galerkin_Kutta;

// base class interface
#if 0
template<class Surreal>
class JacobianCell_Galerkin<Surreal, TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class PDE,
            class PhysDim, class ArrayQ, class SparseMatrix>
  static void
  integrate( const IntegrandCell_Galerkin<PDE>& fcn,
                const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                int quadratureorder[], int ngroup,
                SparseMatrix& mtxGlobal )
  {
    SANS_ASSERT( ngroup == qfld.nCellGroups() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        JacobianCell_Galerkin_Group_Integral<Surreal, Triangle, PhysDim, TopoDim,ArrayQ>( fcn,
                                      xfld.template getCellGroup<Triangle>(group),
                                      qfld.template getCellGroup<Triangle>(group),
                                      quadratureorder[group], mtxGlobal );
      }
      else if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        JacobianCell_Galerkin_Group_Integral<Surreal, Quad, PhysDim, TopoDim,ArrayQ>( fcn,
                                      xfld.template getCellGroup<Quad>(group),
                                      qfld.template getCellGroup<Quad>(group),
                                      quadratureorder[group], mtxGlobal );
      }
      else
      {
        const char msg[] = "Error in JacobianCell_Galerkin<TopoD2>: unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};
#endif

template<class Surreal>
class JacobianCell_Galerkin_Kutta<Surreal, TopoD3>
{
public:
  typedef TopoD3 TopoDim;

  template <class PDE, class XFieldType,
            class PhysDim, class ArrayQ, class SparseMatrix>
  static void
  integrate( const IntegrandCell_Galerkin_Kutta_LIP<PDE>& fcn,
             const XFieldType& xfld,
             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             const Field_DG_BoundaryFrame<PhysDim, TopoD3, ArrayQ>& Gfld,
             int quadratureorder[], int ngroup,
             SparseMatrix& mtxGlobal )
  {
    SANS_ASSERT( ngroup == qfld.nCellGroups() );
    //SANS_ASSERT( mtxGlobal.m() == Gfld.nDOF() );

    // loop over element groups
    for (std::size_t group = 0; group < fcn.nBoundaryFrameGroups(); group++)
    {
      const int boundaryFrameGroup = fcn.boundaryFrameGroup(group);

      const typename XFieldType::FieldFrameGroupType& xfldFrame = xfld.getBoundaryFrameGroup(boundaryFrameGroup);
      const typename Field_DG_BoundaryFrame<PhysDim, TopoD3, ArrayQ>::FieldFrameGroupType&
        GfldFrame = Gfld.getBoundaryFrameGroupGlobal(boundaryFrameGroup);
      std::map<int,int> mapKuttaGlobalLeft, mapKuttaGlobalRight;

      int nDOFX = xfldFrame.basis()->nBasis();
      int nIntegrandK = GfldFrame.basis()->nBasis();
      std::vector<int> mapDOFGlobalX( nDOFX );
      std::vector<int> mapDOFGlobalK( nIntegrandK );
      SANS_ASSERT( nIntegrandK == 2 ); //TODO: HACK hard coded for P1 at the moment...
      SANS_ASSERT( nDOFX == nIntegrandK ); //TODO: HACK hard coded for grid and solution order the same

      int nelem = xfldFrame.nElem();
      for (int elem = 0; elem < nelem; elem++)
      {
        xfldFrame.associativity( elem ).getGlobalMapping( mapDOFGlobalX.data(), nDOFX );
        GfldFrame.associativity( elem ).getGlobalMapping( mapDOFGlobalK.data(), nIntegrandK );

        for (int n = 0; n < nDOFX; n++)
        {
          if ( mapDOFGlobalX[n] < xfld.dupPointOffset_ ) continue;

          // The X nodes are associated with the duplicate points
          mapKuttaGlobalRight[mapDOFGlobalX[n]] = mapDOFGlobalK[n];

          // Convert duplicate points on the right to points on the left
          if ( mapDOFGlobalX[n] >= xfld.dupPointOffset_ )
            mapDOFGlobalX[n] = xfld.invPointMap_[ mapDOFGlobalX[n] - xfld.dupPointOffset_ ];

          mapKuttaGlobalLeft[mapDOFGlobalX[n]] = mapDOFGlobalK[n];
        }
      }


      const int cellGroupLeft = fcn.cellGroupLeft(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupLeft).topoTypeID() == typeid(Tet) )
      {
        JacobianCell_Galerkin_Kutta_Group_Integral<Surreal, Tet, PhysDim, TopoDim,ArrayQ>( fcn,
                                      xfld.template getCellGroup<Tet>(cellGroupLeft),
                                      qfld.template getCellGroup<Tet>(cellGroupLeft),
                                      quadratureorder[cellGroupLeft], mapKuttaGlobalLeft, mtxGlobal, -1 );
      }
      else if ( xfld.getCellGroupBase(cellGroupLeft).topoTypeID() == typeid(Hex) )
      {
        JacobianCell_Galerkin_Kutta_Group_Integral<Surreal, Hex, PhysDim, TopoDim,ArrayQ>( fcn,
                                      xfld.template getCellGroup<Hex>(cellGroupLeft),
                                      qfld.template getCellGroup<Hex>(cellGroupLeft),
                                      quadratureorder[cellGroupLeft], mapKuttaGlobalLeft, mtxGlobal, -1 );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Error in JacobianCell_Galerkin_Kutta<TopoD3>: unknown topology\n" );


      const int cellGroupRight = fcn.cellGroupRight(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupRight).topoTypeID() == typeid(Tet) )
      {
        JacobianCell_Galerkin_Kutta_Group_Integral<Surreal, Tet, PhysDim, TopoDim,ArrayQ>( fcn,
                                      xfld.template getCellGroup<Tet>(cellGroupRight),
                                      qfld.template getCellGroup<Tet>(cellGroupRight),
                                      quadratureorder[cellGroupRight], mapKuttaGlobalRight, mtxGlobal, 1 );
      }
      else if ( xfld.getCellGroupBase(cellGroupRight).topoTypeID() == typeid(Hex) )
      {
        JacobianCell_Galerkin_Kutta_Group_Integral<Surreal, Hex, PhysDim, TopoDim,ArrayQ>( fcn,
                                      xfld.template getCellGroup<Hex>(cellGroupRight),
                                      qfld.template getCellGroup<Hex>(cellGroupRight),
                                      quadratureorder[cellGroupRight], mapKuttaGlobalRight, mtxGlobal, 1 );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Error in JacobianCell_Galerkin_Kutta<TopoD3>: unknown topology\n" );

    }
  }
};

}

#endif  // JACOBIANCELL_GALERKIN_KUTTA_H
