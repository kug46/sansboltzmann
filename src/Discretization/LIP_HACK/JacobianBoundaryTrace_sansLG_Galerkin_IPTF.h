// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_SANSLG_GALERKIN_H
#define JACOBIANBOUNDARYTRACE_SANSLG_GALERKIN_H

// jacobian boundary-trace sans-Lagrange integral jacobian functions
// IPTF: includes both PDE and auxiliary jacobians

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#define USE_NEW
#ifdef USE_NEW
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "JacobianBoundaryTrace_Galerkin_Element_IPTF.h"
#else
#include "Field/Element/GalerkinWeightedIntegral.h"
#endif

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_sansLG_Galerkin_IPTF_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_sansLG_Galerkin_IPTF_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianBoundaryTrace_sansLG_Galerkin_IPTF_impl( const IntegrandBoundaryTrace& fcn,
                                                   MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                                   MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_glb,
                                                   MatrixScatterAdd<MatrixQ>& mtxGlobalAux_q,
                                                   MatrixScatterAdd<MatrixQ>& mtxGlobalAux_glb ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_glb_(mtxGlobalPDE_glb),
    mtxGlobalAux_q_(mtxGlobalAux_q), mtxGlobalAux_glb_(mtxGlobalAux_glb) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

  const int nAux = 1;     // size of auxiliary/global blocks

  //----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    #if 0
    std::cout << "check: mtxGlobalPDE_q: " << mtxGlobalPDE_q_.m() << ", " << mtxGlobalPDE_q_.n() << std::endl;
    std::cout << "check: mtxGlobalPDE_glb: " << mtxGlobalPDE_glb_.m() << ", " << mtxGlobalPDE_glb_.n() << std::endl;
    std::cout << "check: mtxGlobalAux_q: " << mtxGlobalAux_q_.m() << ", " << mtxGlobalAux_q_.n() << std::endl;
    std::cout << "check: mtxGlobalAux_glb: " << mtxGlobalAux_glb_.m() << ", " << mtxGlobalAux_glb_.n() << std::endl;
    #endif

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_glb_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_glb_.n() == nAux );

    SANS_ASSERT( mtxGlobalAux_q_.m() == nAux );
    SANS_ASSERT( mtxGlobalAux_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalAux_glb_.m() == nAux );
    SANS_ASSERT( mtxGlobalAux_glb_.n() == nAux );
  }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class TupleFieldType>
  void
  integrate(const int cellGroupGlobalL,
            const typename TupleFieldType                 ::template FieldCellGroupType<TopologyL>& tuplefldCellL,
            const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
            const int traceGroupGlobal,
            const typename TupleFieldType::XFieldType     ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
            int quadratureorder )
  {
    typedef typename TupleFieldType                 ::template FieldCellGroupType<TopologyL> TupleFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename TupleFieldCellGroupTypeL::template ElementType<>        ElementTupleFieldClassL;
    typedef typename QFieldCellGroupTypeL    ::template ElementType<Surreal> ElementQFieldClassL;

    typedef typename TupleFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // element field variables
    ElementTupleFieldClassL  xfldElemL( tuplefldCellL.basis() );
    ElementQFieldClassL      qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );

#ifdef USE_NEW
#else
    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;
#endif

    // DOF counts
    int nDOFL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL, -1);
    std::vector<int> mapAux(nAux, 0);

#ifdef USE_NEW
    // trace element integral
    typedef JacobianElemBoundaryTrace_Galerkin_IPTF<MatrixQ> JacobianElemBoundaryTraceType;
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemBoundaryTraceType,
                                                              JacobianElemBoundaryTraceType>
      integral(quadratureorder);

    // element jacobian matrices: PDE, auxiliary wrt qL, qR, glb
    JacobianElemBoundaryTraceSize_IPTF sizeL(nDOFL,  nDOFL, nAux);
    JacobianElemBoundaryTraceSize_IPTF sizeAux(nAux, nDOFL, nAux);
    JacobianElemBoundaryTraceType mtxPDEElemL(sizeL);
    JacobianElemBoundaryTraceType mtxAux(sizeAux);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // left element
      int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      int elemR = -1;

      // copy global grid/solution DOFs to element
      tuplefldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );

      // trace integration for canonical element
      integral(
          fcn_.integrand( xfldElemTrace, canonicalTraceL,
                          xfldElemL, qfldElemL ),
          xfldElemTrace, mtxPDEElemL, mtxAux );

      // scatter-add element jacobian to global

      scatterAdd<TopologyTrace, TopologyL, PhysDim, TopoDim>(
          qfldCellL,
          elemL, elemR,
          mapDOFGlobalL.data(), nDOFL,
          mapAux.data(), nAux,
          mtxPDEElemL.rsd_qL, mtxPDEElemL.rsd_glb,
          mtxAux.rsd_qL     , mtxAux.rsd_glb     ,
          mtxGlobalPDE_q_, mtxGlobalPDE_glb_,
          mtxGlobalAux_q_, mtxGlobalAux_glb_ );
    }
#else     // original: consistent with JacobianBoundaryTrace_Galerkin
    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal> integral(quadratureorder, nDOFL, nAux);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
    std::vector<ArrayQSurreal> rsdAux( nAux );

    // element jacobians
    MatrixElemClass mtxPDEElemL_qElemL(nDOFL, nDOFL);
    MatrixElemClass mtxPDEElemL_glb(nDOFL, nAux);
    MatrixElemClass mtxAux_qElemL(nAux, nDOFL);
    MatrixElemClass mtxAux_glb(nAux, nAux);

    // global variables
    ArrayQSurreal glb;
    fcn_.getGlobal( glb );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobian to zero
      mtxPDEElemL_qElemL = 0;
      mtxPDEElemL_glb = 0;
      mtxAux_qElemL = 0;
      mtxAux_glb = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // right element, hub trace or field trace
      int elemR;
      if ( xfldTrace.getGroupRightType() == eHubTraceGroup )
        elemR = xfldTrace.getElementRight( elem );
      else
        elemR = elem;

      // copy global grid/solution DOFs to element
      tuplefldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );

      // number of simultaneous derivatives per functor call
      const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(nDOFL + nAux); nchunk += nDeriv)
      {
        // associate derivative slots with solution variables

        int slot;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        for (int j = 0; j < nAux; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              glb(j).deriv(k) = 0;

            slot = nEqn*nDOFL + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              glb(j).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element

        // reset PDE residuals to zero
        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;
        for (int n = 0; n < nAux; n++)
          rsdAux[n] = 0;

        integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                                 xfldElemL, qfldElemL),
                  get<-1>(xfldElemTrace),
                  rsdPDEElemL.data(), nDOFL,
                  rsdAux.data(), nAux );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElemL_qElemL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

              for (int i = 0; i < nAux; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxAux_qElemL(i,j), m,n) = DLA::index(rsdAux[i], m).deriv(slot - nchunk);
            }
          }
        }

        for (int j = 0; j < nAux; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*nDOFL + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElemL_glb(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

              for (int i = 0; i < nAux; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxAux_glb(i,j), m,n) = DLA::index(rsdAux[i], m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global

      scatterAdd<TopologyTrace, TopologyL, PhysDim, TopoDim>(
          qfldCellL,
          elemL, elemR,
          mapDOFGlobalL.data(), nDOFL,
          mapAux.data(), nAux,
          mtxPDEElemL_qElemL,
          mtxPDEElemL_glb,
          mtxAux_qElemL,
          mtxAux_glb,
          mtxGlobalPDE_q_,
          mtxGlobalPDE_glb_,
          mtxGlobalAux_q_,
          mtxGlobalAux_glb_ );
    }
#endif    // USE_NEW
  }

protected:

  //----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class PhysDim, class TopoDim,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int elemL, const int elemR,
      int mapDOFGlobalL[], const int nDOFL,
      int mapAux[], const int nAux,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_glb,
      SANS::DLA::MatrixD<MatrixQ>& mtxAux_qElemL,
      SANS::DLA::MatrixD<MatrixQ>& mtxAux_glb,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_glb,
      SparseMatrixType<MatrixQ>& mtxGlobalAux_q,
      SparseMatrixType<MatrixQ>& mtxGlobalAux_glb )
  {
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL, nDOFL );

    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, mapDOFGlobalL, nDOFL, mapDOFGlobalL, nDOFL );
    mtxGlobalPDE_glb.scatterAdd( mtxPDEElemL_glb, mapDOFGlobalL, nDOFL, mapAux, nAux );

    mtxGlobalAux_q.scatterAdd( mtxAux_qElemL, mapAux, nAux, mapDOFGlobalL, nDOFL );
    mtxGlobalAux_glb.scatterAdd( mtxAux_glb, mapAux, nAux, mapAux, nAux );
  }


#if 0   // not sure if this is ever used; if-def'ed out in baseline sansLG_Galerkin class
  //----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class PhysDim, class TopoDim,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int elemL, const int elem,
      int mapDOFGlobalL[], const int nDOFL,
      int mapAux[], const int nAux,
      DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      DLA::MatrixD<MatrixQ>& mtxPDEElemL_glb,
      DLA::MatrixD<MatrixQ>& mtxAux_qElemL,
      DLA::MatrixD<MatrixQ>& mtxAux_glb,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_glb,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalAux_q,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalAux_glb )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
    mtxGlobalPDE_glb.scatterAdd( mtxPDEElemL_glb, elemL, elem );

    mtxGlobalAux_q.scatterAdd( mtxAux_qElemL, elem, elemL );
    mtxGlobalAux_glb.scatterAdd( mtxAux_glb, elem, elem );
  }
#endif

protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_glb_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAux_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAux_glb_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class MatrixQ>
JacobianBoundaryTrace_sansLG_Galerkin_IPTF_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_sansLG_Galerkin_IPTF( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_glb,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalAux_q,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalAux_glb )
{
  return { fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_glb, mtxGlobalAux_q, mtxGlobalAux_glb };
}


}   // namespace SANS

#endif  // JACOBIANBOUNDARYTRACE_SANSLG_GALERKIN_H
