// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYFRAME_GALERKIN_DARMOFAL_H
#define JACOBIANBOUNDARYFRAME_GALERKIN_DARMOFAL_H

// jacobian boundary-frame integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/XField3D_Wake.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/Element.h"

#include "IntegrandCell_Galerkin_Darmofal.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class QFieldCellGroupType,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryFrame_Galerkin_Darmofal_ScatterAdd(
    const QFieldCellGroupType& qfldCell, const int elemCell,
    int mapDOFGlobal[], int mapDOFGlobalKutta[], const int nDOF,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElem_qElem,
    SparseMatrix<MatrixQ>& mtxGlobalPDE_q )
{
  mtxGlobalPDE_q.scatterAdd( mtxPDEElem_qElem, mapDOFGlobalKutta, nDOF, mapDOFGlobal, nDOF );
}

#if 0
//----------------------------------------------------------------------------//
template <class QFieldCellGroupType,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryFrame_Galerkin_Darmofal_ScatterAdd(
    const QFieldTraceGroupType& lgfldTrace,
    const QFieldCellGroupType& qfldCellL,
    const int elemL, const int elem,
    int mapDOFGlobalL[], const int nDOFL,
    int mapDOFGlobalEdge[], const int nDOFEdge,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_lgEdge,
    SANS::DLA::MatrixD<MatrixQ>& mtxBC_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxBC_lgEdge,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_lg,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalBC_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalBC_lg )
{

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryEdge_ScatterAdd: elemL, elem = " << elemL << ," " << elem << std::endl;
  std::cout << "JacobianPDE_Group_2DBoundaryEdge_ScatterAdd (before): mtxPDEGlobal = ";
  mtxPDEGlobal.dump(2);
#endif

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryEdge_ScatterAdd: mtxGlobalPDE_q = ";  mtxGlobalPDE_q.dump(2);
#endif

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
  mtxGlobalPDE_lg.scatterAdd( mtxPDEElemL_lgEdge, elemL, elem );

  mtxGlobalBC_q.scatterAdd( mtxBC_qElemL, elem, elemL );
  mtxGlobalBC_lg.scatterAdd( mtxBC_lgEdge, elem, elem );
}
#endif

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//
//  functions dispatched based on left (L) element topology
//
//  process:
//  (a) loop over groups; dispatch to L (JacobianPDE_LeftTopology_Integral2DBoundaryEdge)
//  (b) call base class routine with specific L (JacobianPDE_Group_Integral2DBoundaryEdge w/ Base& params)
//  (c) cast to specific L and call L-specific topology routine (JacobianPDE_Group_Integral2DBoundaryEdge)
//  (d) loop over edges and integrate


template <class Surreal,
          class TopologyCell,
          class XFieldType,
          class PhysDim, class TopoDim, class ArrayQ,
          class PDE,
          class SparseMatrix>
void
JacobianBoundaryFrame_Galerkin_Darmofal_Group_Integral(
    const IntegrandCell_Galerkin_Darmofal<PDE>& fcn,
    const std::map<int,NodeToBCElemMap>& nodeElemMap,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCell>& xfldCell,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCell>& qfldCell,
    const int sgn,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q )
{
  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );

  ElementQFieldClass qfldElem( qfldCell.basis() );

  // variables/equations per DOF
  const int nEqn = PDE::N;

  // DOFs per element
  int nDOF = qfldElem.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobal(nDOF);
  std::vector<int> mapDOFGlobalKutta(nDOF);

  // trace element integral
  GalerkinWeightedIntegral<TopoD3, TopologyCell, ArrayQSurreal> integral(quadratureorder, nDOF);

  // element integrand/residuals
  std::vector<ArrayQSurreal> rsdPDEElem( nDOF );

  // element jacobians
  MatrixElemClass mtxPDEElem_qElem(nDOF, nDOF);

  // loop over elements within group
  for (auto map = nodeElemMap.begin(); map != nodeElemMap.end(); map++)
  {
    int nTraceElemPoint = map->first;
    int nelem = map->second.elems.size();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxPDEElem_qElem = 0;

      const int elemCell = map->second.elems[ elem ];

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elemCell );
      qfldCell.getElement( qfldElem, elemCell );

      qfldCell.associativity( elemCell ).getGlobalMapping( mapDOFGlobal.data(), nDOF );

      // number of simultaneous derivatives per functor call
      const int nDeriv = DLA::index(qfldElem.DOF(0), 0).size();

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*nDOF; nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot;
        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElem.DOF(j), n).deriv(k) = 0;

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElem.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element
        for (int n = 0; n < nDOF; n++)
          rsdPDEElem[n] = 0;

        integral(
            fcn.integrand(xfldElem, qfldElem),
                          xfldElem,
                          rsdPDEElem.data(), nDOF );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOF; i++)
              {
                if ( mapDOFGlobal[i] != nTraceElemPoint ) continue;

                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElem_qElem(i,j), m,n) = sgn*DLA::index(rsdPDEElem[i], m).deriv(slot - nchunk);
              }
            }
          }
        }
      }   // nchunk

      // Create the map for the Kutta equations
      for (int n = 0; n < nDOF; n++)
      {
        mapDOFGlobalKutta[n] = 0;
        if ( mapDOFGlobal[n] == nTraceElemPoint )
          mapDOFGlobalKutta[n] = map->second.KuttaPoint;
      }

      // scatter-add element jacobian to global

      JacobianBoundaryFrame_Galerkin_Darmofal_ScatterAdd(
          qfldCell, elemCell,
          mapDOFGlobal.data(), mapDOFGlobalKutta.data(), nDOF,
          mtxPDEElem_qElem,
          mtxGlobalPDE_q );

    } // elem
  }
}


//----------------------------------------------------------------------------//
template<class Surreal, class TopoDim>
class JacobianBoundaryFrame_Galerkin_Darmofal;

template<class Surreal>
class JacobianBoundaryFrame_Galerkin_Darmofal<Surreal,TopoD3>
{
  typedef TopoD3 TopoDim;
public:
  template <class XFieldType, class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandCell_Galerkin_Darmofal<PDE>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q )
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );

    for (auto it = xfld.KuttaCellElemLeft_.begin(); it != xfld.KuttaCellElemLeft_.end(); it++)
    {
      int groupCell = it->first;

      if ( xfld.getCellGroupBase(groupCell).topoTypeID() == typeid(Tet) )
      {
        JacobianBoundaryFrame_Galerkin_Darmofal_Group_Integral<Surreal, Tet, XFieldType, PhysDim, TopoDim, ArrayQ>(
            fcn, it->second,
            xfld.template getCellGroup<Tet>(groupCell),
            qfld.template getCellGroup<Tet>(groupCell),
            1,
            quadratureorder[groupCell],
            mtxGlobalPDE_q );
      }
      else if ( xfld.getCellGroupBase(groupCell).topoTypeID() == typeid(Hex) )
      {
        JacobianBoundaryFrame_Galerkin_Darmofal_Group_Integral<Surreal, Hex, XFieldType, PhysDim, TopoDim, ArrayQ>(
            fcn, it->second,
            xfld.template getCellGroup<Hex>(groupCell),
            qfld.template getCellGroup<Hex>(groupCell),
            1,
            quadratureorder[groupCell],
            mtxGlobalPDE_q );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
    }


    for (auto it = xfld.KuttaCellElemRight_.begin(); it != xfld.KuttaCellElemRight_.end(); it++)
    {
      int groupCell = it->first;

      if ( xfld.getCellGroupBase(groupCell).topoTypeID() == typeid(Tet) )
      {
        JacobianBoundaryFrame_Galerkin_Darmofal_Group_Integral<Surreal, Tet, XFieldType, PhysDim, TopoDim, ArrayQ>(
            fcn, it->second,
            xfld.template getCellGroup<Tet>(groupCell),
            qfld.template getCellGroup<Tet>(groupCell),
            -1,
            quadratureorder[groupCell],
            mtxGlobalPDE_q );
      }
      else if ( xfld.getCellGroupBase(groupCell).topoTypeID() == typeid(Hex) )
      {
        JacobianBoundaryFrame_Galerkin_Darmofal_Group_Integral<Surreal, Hex, XFieldType, PhysDim, TopoDim, ArrayQ>(
            fcn, it->second,
            xfld.template getCellGroup<Hex>(groupCell),
            qfld.template getCellGroup<Hex>(groupCell),
            -1,
            quadratureorder[groupCell],
            mtxGlobalPDE_q );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
    }

  }
};


}

#endif  // JACOBIANBOUNDARYFRAME_GALERKIN_DARMOFAL_H
