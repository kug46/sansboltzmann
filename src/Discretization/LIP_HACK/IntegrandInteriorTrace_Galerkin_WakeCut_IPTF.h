// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDINTERIORTRACE_GALERKIN_WAKECUT_IPTF_H
#define INTEGRANDINTERIORTRACE_GALERKIN_WAKECUT_IPTF_H

// interior/periodic-trace integrand operators: Galerkin sansLG for 2D wake-cut

// 2-D Incompressible Potential PDE class
// based on weighted-norm functional

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"
#include "pde/FullPotential/PDEIncompressiblePotentialTwoField2D.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"
#include "Discretization/LIP_HACK/JacobianInteriorTrace_Galerkin_Element_IPTF.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// wake-cut BC

struct BCWakeCut_IPTF_Params : noncopyable
{
  const ParameterNumeric<Real> circ{"circ", NO_DEFAULT, NO_RANGE, "Vortex circulation"};

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.circ));
    d.checkUnknownInputs(allParams);
  }

  static constexpr const char* BCName{"Int_WakeCut_IPTF"};
  struct Option
  {
    const DictOption Int_WakeCut_IPTF{BCWakeCut_IPTF_Params::BCName, BCWakeCut_IPTF_Params::checkInputs};
  };

  static BCWakeCut_IPTF_Params params;
};


template <class Tglb>
class BCWakeCut_IPTF
  : public BCType< BCWakeCut_IPTF<Tglb> >
{
public:
  typedef BCCategory::IPTF_sansLG Category;
  typedef BCWakeCut_IPTF_Params ParamsType;

  typedef PDEIncompressiblePotentialTwoField2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 2;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  explicit BCWakeCut_IPTF( PyDict& d ) : circ_(d.get(ParamsType::params.circ)) {}
  explicit BCWakeCut_IPTF( const Real& circ ) : circ_(circ) {}

#if 0
  BCWakeCut_IPTF( const BCWakeCut_IPTF& ) = delete;
  BCWakeCut_IPTF& operator=( const BCWakeCut_IPTF& ) = delete;
#else
  BCWakeCut_IPTF( const BCWakeCut_IPTF& bc ) : circ_(bc.circ_) {}
  BCWakeCut_IPTF& operator=( const BCWakeCut_IPTF& bc ) { circ_ = bc.circ_; }
#endif

  Tglb getCirculation() const { return circ_; }
  void getCirculation(       Tglb& circ ) const { circ  = circ_; }
  void setCirculation( const Tglb& circ )       { circ_ = circ; }

private:
  Tglb circ_;         // vortex strength/circulation
};


//----------------------------------------------------------------------------//
// interior/periodic-trace integrand: Galerkin sansLG for LIP

template <class PDE, class NDBCVectorCategory, class Disc, class Tglb>
class IntegrandBoundaryTrace_WakeCut_IPTF;

template <class PDE_, class NDBCVector, class Tglb>
class IntegrandBoundaryTrace_WakeCut_IPTF< PDE_, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb > :
  public IntegrandInteriorTraceType<IntegrandBoundaryTrace_WakeCut_IPTF<PDE_, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  //typedef typename NDBCVector::Tglb Tglb;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  // cppcheck-suppress noExplicitConstructor
  IntegrandBoundaryTrace_WakeCut_IPTF( const PDE& pde, const BCWakeCut_IPTF<Tglb>& bc, const std::vector<int>& interiorTraceGroups )
  //IntegrandBoundaryTrace_WakeCut_IPTF( const PDE& pde, const BCBase& bc, const std::vector<int>& interiorTraceGroups )
    : pde_(pde), bc_(bc), interiorTraceGroups_(interiorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return 0; }
  std::size_t interiorTraceGroup(const int n) const { return -1; }
  const std::vector<int>& interiorTraceGroups() const { return dummy_; }

  std::size_t nPeriodicTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  std::vector<int> getPeriodicTraceGroups() const { return interiorTraceGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                                        class ElementParamL, class ElementParamR>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldTypeL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldTypeR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldTypeL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldTypeR;

    typedef typename ElementXFieldTraceType::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_Galerkin_IPTF<MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde,
                   const BCWakeCut_IPTF<Tglb>& bc,
//                   const BCBase& bc,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldTypeL& qfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldTypeR& qfldElemR ) :
      pde_(pde), bc_(bc),
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), paramfldElemL_( paramfldElemL ),
      xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), paramfldElemR_( paramfldElemR ),
      nDOFL_(qfldElemL_.nDOF()),
      nDOFR_(qfldElemR_.nDOF()),
      psiL_( new Real[nDOFL_] ),
      psiR_( new Real[nDOFR_] ),
      gradpsiL_( new VectorX[nDOFL_] ),
      gradpsiR_( new VectorX[nDOFR_] ) {}
    ~BasisWeighted()
    {
      delete [] psiL_;
      delete [] psiR_;

      delete [] gradpsiL_;
      delete [] gradpsiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef,
                     ArrayQ<Ti> integrandPDEL[], const int nPDEL,
                     ArrayQ<Ti> integrandPDER[], const int nPDER,
                     ArrayQ<Ti> integrandAux[], const int nAux ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemR,
                     JacobianElemInteriorTraceType& mtxAux ) const;

  protected:
    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const BCWakeCut_IPTF<Tglb>& bc,
                            const VectorX& nL,
                            const ParamTL& paramL, const ParamTR& paramR,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                            const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                            ArrayQ<Ti> integrandPDEL[], const int nPDEL,
                            ArrayQ<Ti> integrandPDER[], const int nPDER,
                            ArrayQ<Ti> integrandAux[], const int nAux,
                            bool gradientOnly = false ) const;

    const PDE& pde_;
    //BCWakeCut_IPTF<Tglb> bc_;    // not const, since manipulating surreal derivatives
    const BCWakeCut_IPTF<Tglb>& bc_;    // not const, since manipulating surreal derivatives
    //const BCBase& bc_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldTypeL& xfldElemL_;
    const ElementQFieldTypeL& qfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldTypeR& xfldElemR_;
    const ElementQFieldTypeR& qfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *psiL_;          // basis (L/R)
    mutable Real *psiR_;
    mutable VectorX *gradpsiL_;   // basis gradient (L/R)
    mutable VectorX *gradpsiR_;
  };


  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL   > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR   > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldTypeR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    FieldWeighted( const PDE& pde,
                   const BCWakeCut_IPTF<Tglb>& bc,
//                   const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter
                   const ElementQFieldL& qfldElemL,
                   const ElementQFieldL& wfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldTypeR& qfldElemR,
                   const ElementQFieldTypeR& wfldElemR ) :
                   pde_(pde), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                   xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), wfldElemL_(wfldElemL), paramfldElemL_( paramfldElemL ),
                   xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), wfldElemR_(wfldElemR), paramfldElemR_( paramfldElemR ),
                   nDOFL_(qfldElemL_.nDOF()), nDOFR_(qfldElemR_.nDOF()) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, Ti& integrandPDEL, Ti& integrandPDER, Ti& integrandAux ) const;

  protected:
    const PDE& pde_;
    //BCWakeCut_IPTF<Tglb> bc_;    // not const, since manipulating surreal derivatives
    const BCWakeCut_IPTF<Tglb>& bc_;
    //const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementQFieldL& wfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldTypeR& qfldElemR_;
    const ElementQFieldTypeR& wfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
  };


  //Factory function that returns the basis weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,      TopologyR,
                                  ElementParamL, ElementParamR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& qfldElemR) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTraceL, canonicalTraceR,
             paramfldElemL, qfldElemL,
             paramfldElemR, qfldElemR };
  }

  //Factory function that returns the field weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell, TopologyL,     TopologyR,
                                ElementParamL, ElementParamR  >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& wfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& qfldElemR,
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& wfldElemR) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTraceL, canonicalTraceR,
             paramfldElemL, qfldElemL, wfldElemL,
             paramfldElemR, qfldElemR, wfldElemR };
  }

protected:
  const PDE& pde_;
  //BCWakeCut_IPTF<Tglb> bc_;    // not const, since manipulating surreal derivatives
  const BCWakeCut_IPTF<Tglb>& bc_;
  //const BCBase& bc_;
  const std::vector<int> interiorTraceGroups_;
  const std::vector<int> dummy_;
};


template <class PDE, class NDBCVector, class Tglb>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
bool
IntegrandBoundaryTrace_WakeCut_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, TopologyR,
                 ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxViscous() );
}

template <class PDE, class NDBCVector, class Tglb>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
bool
IntegrandBoundaryTrace_WakeCut_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, TopologyR,
                 ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxViscous() );
}


template <class PDE, class NDBCVector, class Tglb>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
template <class Ti>
void
IntegrandBoundaryTrace_WakeCut_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandPDEL[], const int nPDEL,
                                                 ArrayQ<Ti> integrandPDER[], const int nPDER,
                                                 ArrayQ<Ti> integrandAux[], const int nAux ) const
{
  SANS_ASSERT(nPDEL == nDOFL_);
  SANS_ASSERT(nPDER == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, psiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, psiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradpsiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradpsiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( psiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( psiR_, nDOFR_, qR );
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradpsiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradpsiR_, nDOFR_, gradqR );
  }

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // compute the integrand
  weightedIntegrand( bc_,
                     nL,
                     paramL, paramR,
                     qL, gradqL,
                     qR, gradqR,
                     integrandPDEL, nPDEL,
                     integrandPDER, nPDER,
                     integrandAux, nAux );
}


//---------------------------------------------------------------------------//
template <class PDE, class NDBCVector, class Tglb>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
void
IntegrandBoundaryTrace_WakeCut_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell,
                 TopologyL,     TopologyR,
                 ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR,
            JacobianElemInteriorTraceType& mtxAux ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  SANS_ASSERT(mtxAux.nTest == mtxAux.nAux);
  SANS_ASSERT(mtxAux.nDOFL == nDOFL_);
  SANS_ASSERT(mtxAux.nDOFR == nDOFR_);

  const int nAux = mtxAux.nAux;     // total auxiliary/global equations/variables

  ParamTL paramL;                   // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                 // solutions
  VectorArrayQ<T> gradqL, gradqR;   // solution gradients

  QuadPointCellType sRefL;          // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, psiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, psiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradpsiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradpsiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( psiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( psiR_, nDOFR_, qR );

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL, qSurrealR;                // solution
  VectorArrayQ<SurrealClass> gradqSurrealL, gradqSurrealR;  // gradient

  qSurrealL = qL;
  qSurrealR = qR;

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradpsiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradpsiR_, nDOFR_, gradqR );

    gradqSurrealL = gradqL;
    gradqSurrealR = gradqR;
  }

  // BC class copy
  // NOTE: this is needed since operator() declared const (and integrand classes are not copyable, etc)
  BCWakeCut_IPTF<Tglb> bc(bc_);

  // surreal global variable (circulation)
  ArrayQ<SurrealClass> glbSurreal;
  glbSurreal = 0;
  bc.getCirculation( glbSurreal(0) );

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandPDELSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandPDERSurreal( nDOFR_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandAuxSurreal( nAux );

  // loop over derivative chunks to computes derivatives w.r.t q, glb
  for (int nchunk = 0; nchunk < 3*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(glbSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandPDELSurreal = 0;
    integrandPDERSurreal = 0;
    integrandAuxSurreal  = 0;

    // compute the integrand
    bc.setCirculation( glbSurreal(0) );
    weightedIntegrand( bc,
                       nL,
                       paramL, paramR,
                       qSurrealL, gradqL,
                       qSurrealR, gradqR,
                       integrandPDELSurreal.data(), nDOFL_,
                       integrandPDERSurreal.data(), nDOFR_ ,
                       integrandAuxSurreal.data(), nAux );

    // accumulate derivatives into element jacobians: d/d(qL)
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandPDELSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.rsd_qL(i,j) += dJ*psiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandPDERSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemR.rsd_qL(i,j) += dJ*psiL_[j]*PDE_q;
      }

      for (int i = 0; i < nAux; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandAuxSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxAux.rsd_qL(i,j) += dJ*psiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

    // accumulate derivatives into element jacobians: d/d(qR)
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandPDELSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemL.rsd_qR(i,j) += dJ*psiR_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandPDERSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemR.rsd_qR(i,j) += dJ*psiR_[j]*PDE_q;
      }

      for (int i = 0; i < nAux; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandAuxSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxAux.rsd_qR(i,j) += dJ*psiR_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

    // accumulate derivatives into element jacobians: d/d(glb)
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(glbSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative
      bc.setCirculation( glbSurreal(0) );

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandPDELSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nAux; j++)
          mtxElemL.rsd_glb(i,j) += dJ*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandPDERSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nAux; j++)
          mtxElemR.rsd_glb(i,j) += dJ*PDE_q;
      }

      for (int i = 0; i < nAux; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandAuxSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nAux; j++)
          mtxAux.rsd_glb(i,j) += dJ*PDE_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk

  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*2*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandPDELSurreal = 0;
      integrandPDERSurreal = 0;
      integrandAuxSurreal  = 0;

      // compute the integrand
      weightedIntegrand( bc,
                         nL,
                         paramL, paramR,
                         qL, gradqSurrealL,
                         qR, gradqSurrealR,
                         integrandPDELSurreal.data(), nDOFL_,
                         integrandPDERSurreal.data(), nDOFR_,
                         integrandAuxSurreal.data(), nAux, true );

      // accumulate derivatives into element jacobians: d/d(gradqL)
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandPDELSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.rsd_qL(i,j) += dJ*gradpsiL_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandPDERSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemR.rsd_qL(i,j) += dJ*gradpsiL_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nAux; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandAuxSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxAux.rsd_qL(i,j) += dJ*gradpsiL_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }

      // accumulate derivatives into element jacobians: d/d(gradqR)
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandPDELSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemL.rsd_qR(i,j) += dJ*gradpsiR_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandPDERSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemR.rsd_qR(i,j) += dJ*gradpsiR_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nAux; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandAuxSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxAux.rsd_qR(i,j) += dJ*gradpsiR_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient
}


template <class PDE, class NDBCVector, class Tglb>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
template<class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace_WakeCut_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell,
                 TopologyL,     TopologyR,
                 ElementParamL, ElementParamR>::
weightedIntegrand( const BCWakeCut_IPTF<Tglb>& bc,
                   const VectorX& nL,
                   const ParamTL& paramL, const ParamTR& paramR,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                   const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                   ArrayQ<Ti> integrandPDEL[], const int nPDEL,
                   ArrayQ<Ti> integrandPDER[], const int nPDER,
                   ArrayQ<Ti> integrandAux[], int nAux,
                   bool gradientOnly ) const
{
  const VectorX nR = -nL;                     // right unit-normal (points to left)

  const VectorX& X = paramL;    // HACK ??
  //const VectorX& X = paramL.right();          // cartesian coordinates of evaluation point

  // tau basis functions
  Real *tauL = new Real[nDOFL_];              // basis (L/R)
  Real *tauR = new Real[nDOFR_];
  VectorX *gradtauL = new VectorX[nDOFL_];    // basis gradient (L/R)
  VectorX *gradtauR = new VectorX[nDOFR_];
  for (int k = 0; k < nDOFL_; k++)
  {
    tauL[k] = psiL_[k];
    gradtauL[k] = gradpsiL_[k];
  }
  for (int k = 0; k < nDOFR_; k++)
  {
    tauR[k] = psiR_[k];
    gradtauR[k] = gradpsiR_[k];
  }

  // Gaussian weight
  Real wghtD = pde_.gaussianD( X[0], X[1] );
#ifdef USE_IPTF_NORM
  ArrayQ<Tg> gradqLSq = dot(gradqL, gradqL);
  ArrayQ<Tg> gradqRSq = dot(gradqR, gradqR);
  Tg   wghtDL = wghtD * pow( gradqLSq(0), pde_.normD()/2 - 1);
  Tg   wghtDR = wghtD * pow( gradqRSq(0), pde_.normD()/2 - 1);
#else
  Real wghtDL = wghtD;
  Real wghtDR = wghtD;
#endif

  for (int k = 0; k < nPDEL; k++)
    integrandPDEL[k] = 0;

  for (int k = 0; k < nPDER; k++)
    integrandPDER[k] = 0;

  // PDE residual: weak form boundary integral

  ArrayQ<Tg> qnL = dot(nL, gradqL);
  ArrayQ<Tg> qnR = dot(nR, gradqR);

  ArrayQ<Tg> qnSum = 0.5*(qnR + qnL);
  ArrayQ<Tg> qnDif = 0.5*(qnR - qnL);

#undef USE_OLDFORM
#ifdef USE_OLDFORM
  ArrayQ<Tq> qSum = 0.5*(qR + qL);
  ArrayQ<Tq> qDif = 0.5*(qR - qL);

  for (int k = 0; k < nDOFL_; k++)
  {
    integrandPDEL[k](0) += -dot(nL, gradpsiL_[k])*(qSum(1) - qDif(1));
    integrandPDEL[k](1) += -tauL[k]*(qnSum(0) - qnDif(0));
  }

  for (int k = 0; k < nDOFR_; k++)
  {
    integrandPDER[k](0) += -dot(nR, gradpsiR_[k])*(qSum(1) + qDif(1));
    integrandPDER[k](1) += -tauR[k]*(qnSum(0) + qnDif(0));
  }
#else   // integrand now consistent with PDE class
  ArrayQ<Tg> wghtDqnSum = 0.5*(wghtDR*qnR + wghtDL*qnL);
  ArrayQ<Tg> wghtDqnDif = 0.5*(wghtDR*qnR - wghtDL*qnL);

  for (int k = 0; k < nDOFL_; k++)
  {
    integrandPDEL[k](0) += -psiL_[k]*(qnSum(1) - qnDif(1) + wghtDqnSum(0) - wghtDqnDif(0));
    integrandPDEL[k](1) += -tauL[k]*(qnSum(0) - qnDif(0));
  }

  for (int k = 0; k < nDOFR_; k++)
  {
    integrandPDER[k](0) += -psiR_[k]*(qnSum(1) + qnDif(1) + wghtDqnSum(0) + wghtDqnDif(0));
    integrandPDER[k](1) += -tauR[k]*(qnSum(0) + qnDif(0));
  }
#endif

  // PDE residual: sans-Lagrange BC terms

  Ti circ = bc.getCirculation();

  Tg rsdMass = 0.5*(qnR(0) + qnL(0));          // mass flux BC
  Ti rsdCirc = 0.5*((qR(0) - qL(0)) + circ);   // potential jump BC
#ifdef USE_OLDFORM
  Tg rsdAux1 = 0.5*((qnR(1) - qnL(1)) + wghtD*(qnR(0) - qnL(0)));
  Tq rsdAux2 = 0.5*(qR(1) + qL(1));
  //std::cout << "integrandWake: circ = " << circ << "  mass = " << rsdMass << "  jump = " << rsdCirc << std::endl;
  //std::cout << "integrandWake: qL = " << qL(0) << " " << qL(1) << "  qR = " << qR(0) << " " << qR(1);
  //std::cout << "  qnL = " << qnL(0) << " " << qnL(1) << "  qnR = " << qnR(0) << " " << qnR(1) << std::endl;

  for (int k = 0; k < nDOFL_; k++)
  {
    integrandPDEL[k](0) += - psiL_[k]*(-rsdAux1) + dot(nL, gradpsiL_[k])*(rsdAux2 + wghtD*rsdCirc);
    integrandPDEL[k](1) += tauL[k]*rsdMass - dot(nL, gradtauL[k])*(-rsdCirc);
  }

  for (int k = 0; k < nDOFR_; k++)
  {
    integrandPDER[k](0) += - psiR_[k]*(+rsdAux1) + dot(nR, gradpsiR_[k])*(rsdAux2 - wghtD*rsdCirc);
    integrandPDER[k](1) += tauR[k]*rsdMass - dot(nR, gradtauR[k])*(+rsdCirc);
  }
#else     // properly shows strong-form BCs for sigma
  Tg rsdAux1 = 0.5*((qnR(1) + qnL(1)) + (wghtDR*qnR(0) + wghtDL*qnL(0)));
  Tq rsdAux2 = 0.5*(qR(1) - qL(1));

  for (int k = 0; k < nDOFL_; k++)
  {
    integrandPDEL[k](0) += psiL_[k]*rsdAux1 + dot(nL, gradpsiL_[k])*(rsdAux2 + wghtDR*rsdCirc);
    integrandPDEL[k](1) += tauL[k]*rsdMass + dot(nL, gradtauL[k])*rsdCirc;
  }

  for (int k = 0; k < nDOFR_; k++)
  {
    integrandPDER[k](0) += psiR_[k]*rsdAux1 - dot(nR, gradpsiR_[k])*(rsdAux2 + wghtDL*rsdCirc);
    integrandPDER[k](1) += tauR[k]*rsdMass - dot(nR, gradtauR[k])*rsdCirc;
  }
#endif

  // auxiliary/global residual:

#ifdef USE_FIXCIRC
  integrandAux[0](0) = 0;
  integrandAux[0](1) = 0;
#else
#ifdef USE_OLDFORM
  integrandAux[0](0) = - rsdAux1;
  integrandAux[0](1) = 0;
#else
  Tg termAux = 0.5*((qnR(1) - qnL(1)) + (wghtDR*qnR(0) - wghtDL*qnL(0)));
  integrandAux[0](0) = - termAux;
  integrandAux[0](1) = 0;
#endif
#endif
  //std::cout << "integrandWake: Aux = " << integrandAux[0](0) << " " << integrandAux[0](1) << std::endl;
  #if 0
  std::cout << "integrandWake: Aux = " << integrandAux[0](0);
  std::cout << "  nL(" << nL << ")";
  std::cout << "  nR(" << nR << ")";
  std::cout << "  qL(" << qL << ")  qR(" << qR << ")";
  std::cout << "  gradqL(" << gradqL << ")";
  std::cout << "  gradqR(" << gradqR << ")";
  std::cout << std::endl;
  #endif

#define USE_NITSCHE
#ifdef USE_NITSCHE
  // Nitsche stabilization terms

#if 0
  // edge length
  Real ds = xfldElemTrace_.jacobianDeterminant();

  // adjacent element area
  Real areaL = xfldElemL_.jacobianDeterminant();
  Real areaR = xfldElemR_.jacobianDeterminant();

  // normal grid scale
  Real h = 0.5*(areaL + areaR)/ds;
#else
  // normal grid scale
  Real h = 0;
  for (int k = 0; k < nDOFL_; k++)
    h += std::abs(dot(nL, gradpsiL_[k]));
  for (int k = 0; k < nDOFR_; k++)
    h += std::abs(dot(nR, gradpsiR_[k]));
  h = 2./h;
#endif

  Real dgamma = 10/h;       // Nitsche parameter

  for (int k = 0; k < nDOFL_; k++)
  {
    integrandPDEL[k](0) += dgamma*(-psiL_[k])*rsdAux2;
    integrandPDEL[k](1) += dgamma*(-tauL[k])*rsdCirc;
  }

  for (int k = 0; k < nDOFR_; k++)
  {
    integrandPDER[k](0) += dgamma*(+psiR_[k])*rsdAux2;
    integrandPDER[k](1) += dgamma*(+tauR[k])*rsdCirc;
  }

#ifdef USE_FIXCIRC
#else
  integrandAux[0](0) += dgamma*rsdAux2;
#endif    // USE_FIXCIRC
#endif    // USE_NITSCHE

  delete [] tauL;
  delete [] tauR;
  delete [] gradtauL;
  delete [] gradtauR;
}


template <class PDE, class NDBCVector, class Tglb>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
template <class Ti>
void
IntegrandBoundaryTrace_WakeCut_IPTF<PDE, NDVectorCategory<NDBCVector, BCCategory::IPTF_sansLG>, Galerkin, Tglb>::
FieldWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell,
                 TopologyL,     TopologyR,
                 ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, Ti& integrandPDEL, Ti& integrandPDER, Ti& integrandAux ) const
{
  ParamTL paramL;   // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  VectorX nL;       // unit normal for left element (points to right element)
  VectorX nR;       //                              (points to left element)

  ArrayQ<T> qL, qR;                // solution
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients
  VectorArrayQ<T> dqn;             // jump in q.n

  ArrayQ<T> wL, wR;                // weight
  VectorArrayQ<T> gradwL, gradwR;  // weight gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: nL points to R; nR points to L
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);
  nR = -nL;

  // solution value, gradient
  qfldElemL_.eval( sRefL, qL );
  qfldElemR_.eval( sRefR, qR );
  if (pde_.hasFluxViscous() || pde_.hasSourceTrace())
  {
    xfldElemL_.evalGradient( sRefL, qfldElemL_, gradqL );
    xfldElemR_.evalGradient( sRefR, qfldElemR_, gradqR );
  }

  // weight value,
  wfldElemL_.eval( sRefL, wL );
  wfldElemR_.eval( sRefR, wR );
  if (pde_.hasFluxViscous())
  {
    xfldElemL_.evalGradient( sRefL, wfldElemL_, gradwL );
    xfldElemR_.evalGradient( sRefR, wfldElemR_, gradwR );
  }

  integrandPDEL = 0;
  integrandPDER = 0;

#if 0   // not finished
  // PDE residual: weak form boundary integral

  ArrayQ<Ti> qnL = dot(nL, gradqL);
  ArrayQ<Ti> qnR = dot(nR, gradqR);

  ArrayQ<Ti> qnSum = 0.5*(qnR + qnL);
  ArrayQ<Ti> qnDif = 0.5*(qnR - qnL);

  integrandPDEL += -wL*(qnSum - qnDif);
  integrandPDER += -wR*(qnSum + qnDif);

  // PDE residual: sans-Lagrange BC terms

  Real circ = bc_.getCirculation();

  ArrayQ<Ti> rsdMass = 0.5*(qnR + qnL);          // mass flux BC
  ArrayQ<Ti> rsdCirc = 0.5*((qR - qL) + circ);   // potential jump BC

  integrandPDEL += wL*rsdMass - dot(nL, gradwL)*(-rsdCirc);
  integrandPDER += wR*rsdMass - dot(nR, gradwR)*(+rsdCirc);

#ifdef USE_NITSCHE
  // edge length
  Real ds = xfldElemTrace_.jacobianDeterminant();

  // adjacent element area
  Real areaL = xfldElemL_.jacobianDeterminant();
  Real areaR = xfldElemR_.jacobianDeterminant();

  // normal grid scale
  Real h = 0.5*(areaL + areaR)/ds;

  Real beta1  = 10;   // Nitsche parameters
  Real gamma2 = 10;

  integrandPDEL += (beta1/(4*h))*(-wL)*rsdCirc + (gamma2*h)*dot(nL, gradwL)*rsdMass;
  integrandPDER += (beta1/(4*h))*(+wR)*rsdCirc + (gamma2*h)*dot(nR, gradwR)*rsdMass;
#endif

  // auxiliary/global residual:

#ifdef USE_FIXCIRC
  integrandAux(0) = 0;
  integrandAux(1) = 0;
#else
  integrandAux(0) = - rsdAux1;
  integrandAux(1) = 0;
#endif
#else
  SANS_DEVELOPER_EXCEPTION( "IPTF: FieldWeighted wake-cut not finished" );
#endif
}

}   // namespace SANS

#endif  // INTEGRANDINTERIORTRACE_GALERKIN_WAKECUT_IPTF_H
