// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDFUNCTOR_GALERKIN_WAKECUT_LIP_H
#define INTEGRANDFUNCTOR_GALERKIN_WAKECUT_LIP_H

// cell/trace integrand operators: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Trace integrand: Galerkin

template <class PDE>
class IntegrandTrace_Galerkin_WakeCut_LIP
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandTrace_Galerkin_WakeCut_LIP(const PDE& pde, const std::vector<int>& BoundaryGroups)
    : pde_(pde), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;
    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const PDE& pde,
             const ElementXFieldTrace& xfldElemTrace,
             const ElementQFieldTrace& lgfldElemTraceMass,
             const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldL& xfldElemL,
             const ElementQFieldL& qfldElemL,
             const ElementXFieldR& xfldElemR,
             const ElementQFieldR& qfldElemR ) :
             pde_(pde),
             xfldElemTrace_(xfldElemTrace), lgfldElemTraceMass_(lgfldElemTraceMass),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR) {}


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRef, ArrayQ<T> integrandL[], const int nrsdL,
                                                     ArrayQ<T> integrandR[], const int nrsdR,
                                                     ArrayQ<T> integrandWake[], const int nrsdW ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementQFieldTrace& lgfldElemTraceMass_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL, TopologyR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const Element<ArrayQ<T>    , TopoDimTrace, TopologyTrace>& lgfldElemTraceMass,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell , TopologyL    >& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElemL,
            const ElementXField<PhysDim, TopoDimCell , TopologyR    >& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyR    >& qfldElemR) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>(pde_,
                                                                                      xfldElemTrace, lgfldElemTraceMass,
                                                                                      canonicalTraceL, canonicalTraceR,
                                                                                      xfldElemL, qfldElemL,
                                                                                      xfldElemR, qfldElemR);
  }

protected:
  const PDE& pde_;
  const std::vector<int> BoundaryGroups_;
};


template <class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
bool
IntegrandTrace_Galerkin_WakeCut_LIP<PDE>::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::needsEvaluation() const
{
  return ( pde_.hasFluxViscous() ||
           pde_.hasSource() );
}


template <class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
void
IntegrandTrace_Galerkin_WakeCut_LIP<PDE>::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::operator()(
    const QuadPointTraceType& RefTrace, ArrayQ<T> integrandL[], const int nrsdL,
                                        ArrayQ<T> integrandR[], const int nrsdR,
                                        ArrayQ<T> integrandWake[], const int nrsdW ) const
{
  typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

  const int nDOFL = qfldElemL_.nDOF();             // total solution DOFs in left element
  const int nDOFR = qfldElemR_.nDOF();             // total solution DOFs in right element
  const int nDOFMass = lgfldElemTraceMass_.nDOF(); // total Lagrange multiplier DOFs in edge-element

  SANS_ASSERT(nrsdL == nDOFL);
  SANS_ASSERT(nrsdR == nDOFR);
  SANS_ASSERT(nrsdW == nDOFMass);

  VectorX X = 0;
  VectorX nL, nR;           // unit normal for left element (points to right element)

  Real phiL[nDOFL];                                                // basis (left)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiL(nDOFL);  // basis gradient (left)
  Real phiR[nDOFR];                                                // basis (right)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiR(nDOFR);  // basis gradient (right)
  Real muW[nDOFMass];                                              // Lagrange multiplier bases

  ArrayQ<T> qL, qR;           // solution
  VectorArrayQ gradqL, gradqR;  // gradient
  ArrayQ<T> lgW;              // Lagrange multipliers

  DLA::VectorS< PhysDim::D, Real > U; // Freestream Velocity

  QuadPointCellType sRefL;              // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, RefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, RefTrace, sRefR );

  // physical coordinates
  xfldElemTrace_.coordinates( RefTrace, X );

  // unit normal: L points to R
  xfldElemTrace_.unitNormal( RefTrace, nL );
  nR = -nL;

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL, nDOFL );
  qfldElemR_.evalBasis( sRefR, phiR, nDOFR );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL.data(), nDOFL );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR.data(), nDOFR );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL, nDOFL, qL );
  qfldElemR_.evalFromBasis( phiR, nDOFR, qR );
  qfldElemL_.evalFromBasis( gradphiL.data(), nDOFL, gradqL );
  qfldElemR_.evalFromBasis( gradphiR.data(), nDOFR, gradqR );

  // Lagrange multiplier basis
  lgfldElemTraceMass_.evalBasis( RefTrace, muW, nDOFMass );

  // Lagrange multiplier
  lgfldElemTraceMass_.evalFromBasis( muW, nDOFMass, lgW );

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] = 0;

  for (int k = 0; k < nrsdW; k++)
    integrandWake[k] = 0;

#if 1
  // viscous flux
  DLA::VectorS<PhysDim::D, ArrayQ<T> > FL = 0, FR = 0;     // PDE flux

  pde_.fluxViscous( X, qL, gradqL, FL );
  pde_.fluxViscous( X, qR, gradqR, FR );

  ArrayQ<T> fluxL = dot(nL,FL);
  ArrayQ<T> fluxR = dot(nR,FR);

  // PDE flux terms

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += phiL[k]*fluxL;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += phiR[k]*fluxR;
#endif

  // mass conservation Numerical BC
#if 0
  ArrayQ<T> qnL = dot(nL,gradqL);
  ArrayQ<T> qnR = dot(nR,gradqR);

  ArrayQ<T> qnsum = qnR + qnL;

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += 0.5*phiL[k]*qnsum;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += 0.5*phiR[k]*qnsum;
#endif


  // zero jump
#if 0
  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += -phiL[k]*lgW;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] +=  phiR[k]*lgW;

  // wake mass-flux constraint
  ArrayQ<T> qjump = qR - qL;

  for (int k = 0; k < nDOFMass; k++)
    integrandWake[k] += muW[k]*qjump;
#endif

  // mass conservation
#if 1
  ArrayQ<T> qnL = dot(nL,gradqL);
  ArrayQ<T> qnR = dot(nR,gradqR);

  ArrayQ<T> qsum = qL + qR;
  ArrayQ<T> qnsum = qnR + qnL;

  ArrayQ<T> tmp = lgW - 0.5*qsum;

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += dot(nL,gradphiL[k])*tmp - 0.5*phiL[k]*qnsum;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += dot(nR,gradphiR[k])*tmp - 0.5*phiR[k]*qnsum;

  // wake mass-flux constraint

  for (int k = 0; k < nDOFMass; k++)
    integrandWake[k] += muW[k]*qnsum;
#endif

// Zero jump
#if 1
  ArrayQ<T> qjump = qR - qL;

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] +=  0.5*dot(nL,gradphiL[k])*qjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += -0.5*dot(nR,gradphiR[k])*qjump;
#endif

  // zero pressure jump ala David Eller
#if 0
  pde_.freestream(U);

  VectorArrayQ gradqjump = gradqR - gradqL;
  ArrayQ<T> Ugradqjump = dot(U,gradqjump);

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += -dot(U,gradphiL[k])*Ugradqjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += dot(U,gradphiR[k])*Ugradqjump;
#endif

  // zero pressure jump
#if 0
  pde_.freestream(U);

  VectorArrayQ gradqjump = gradqR - gradqL;
  ArrayQ<T> Ugradqjump = dot(U,gradqjump);

  nL += U;
  nR += U;
  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += -0.5*dot(nL,gradphiL[k])*Ugradqjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] +=  0.5*dot(nR,gradphiR[k])*Ugradqjump;
#endif
}

}

#endif  // INTEGRANDFUNCTOR_GALERKIN_WAKECUT_LIP_H
