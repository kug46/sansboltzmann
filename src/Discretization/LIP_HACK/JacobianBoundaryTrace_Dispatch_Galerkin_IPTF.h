// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DISPATCH_GALERKIN_IPTF_H
#define JACOBIANBOUNDARYTRACE_DISPATCH_GALERKIN_IPTF_H

// boundary-trace integral jacobian functions
// IPTF: includes both PDE and auxiliary jacobians

#include "JacobianBoundaryTrace_sansLG_Galerkin_IPTF.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

#if 0       // mitLG not implemented
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
class JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_impl
{
public:
  JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg )
    : xfld_(xfld), qfld_(qfld), lgfld_(lgfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_lg_(mtxGlobalPDE_lg), mtxGlobalBC_q_(mtxGlobalBC_q), mtxGlobalBC_lg_(mtxGlobalBC_lg)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_mitLG_Galerkin<Surreal>(fcn, mtxGlobalPDE_q_, mtxGlobalPDE_lg_, mtxGlobalBC_q_, mtxGlobalBC_lg_),
        xfld_, qfld_, lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, MatrixQ>
JacobianBoundaryTrace_mitLG_Dispatch_Galerkin( const XFieldType& xfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                               const int* quadratureorder, int ngroup,
                                               MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                               MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
                                               MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
                                               MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg )
{
  return { xfld, qfld, lgfld, quadratureorder, ngroup, mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalBC_q, mtxGlobalBC_lg };
}
#endif      // mitLG not implemented


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
class JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_IPTF_impl
{
public:
  JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_IPTF_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_glb,
      MatrixScatterAdd<MatrixQ>& mtxGlobalAux_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalAux_glb )
    : xfld_(xfld), qfld_(qfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_glb_(mtxGlobalPDE_glb),
      mtxGlobalAux_q_(mtxGlobalAux_q), mtxGlobalAux_glb_(mtxGlobalAux_glb)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        JacobianBoundaryTrace_sansLG_Galerkin_IPTF<Surreal>(fcn, mtxGlobalPDE_q_, mtxGlobalPDE_glb_, mtxGlobalAux_q_, mtxGlobalAux_glb_),
        xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_glb_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAux_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAux_glb_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_IPTF_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, MatrixQ>
JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_IPTF( const XFieldType& xfld,
                                                     const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                     const int* quadratureorder, int ngroup,
                                                     MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                                     MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_glb,
                                                     MatrixScatterAdd<MatrixQ>& mtxGlobalAux_q,
                                                     MatrixScatterAdd<MatrixQ>& mtxGlobalAux_glb )
{
  return { xfld, qfld, quadratureorder, ngroup, mtxGlobalPDE_q, mtxGlobalPDE_glb, mtxGlobalAux_q, mtxGlobalAux_glb };
}


}   // namespace SANS

#endif //JACOBIANBOUNDARYTRACE_DISPATCH_GALERKIN_IPTF_H
