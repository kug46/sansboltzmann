// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYFRAME_GALERKIN_CIRC_H
#define JACOBIANBOUNDARYFRAME_GALERKIN_CIRC_H

// jacobian boundary-frame integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Field_DG_BoundaryFrame.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/Element.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "IntegrandBoundaryFrame_Galerkin_Circ.h"


namespace SANS
{

//----------------------------------------------------------------------------//
template <class GFieldFrameGroupType, class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryFrame_Galerkin_Circ_ScatterAdd(
    const GFieldFrameGroupType& GfldFrame, const int elemF,
    const QFieldCellGroupTypeL& qfldCellL, const int elemL,
    const QFieldCellGroupTypeR& qfldCellR, const int elemR,
    int mapDOFGlobalF[], const int nDOFF,
    int mapDOFGlobalL[], const int nDOFL,
    int mapDOFGlobalR[], const int nDOFR,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemR,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_G,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qElemR,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_G,
    SANS::DLA::MatrixD<MatrixQ>& mtxKutta_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxKutta_qElemR,
    SANS::DLA::MatrixD<MatrixQ>& mtxKutta_G,
    SparseMatrix<MatrixQ>& mtxGlobalPDE_q, SparseMatrix<MatrixQ>& mtxGlobalPDE_G,
    SparseMatrix<MatrixQ>& mtxGlobalKutta_q, SparseMatrix<MatrixQ>& mtxGlobalKutta_G )
{
  GfldFrame.associativity( elemF ).getGlobalMapping( mapDOFGlobalF, nDOFF );
  qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL, nDOFL );
  qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR, nDOFR );

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, mapDOFGlobalL, nDOFL, mapDOFGlobalL, nDOFL );
  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemR, mapDOFGlobalL, nDOFL, mapDOFGlobalR, nDOFR );
  mtxGlobalPDE_G.scatterAdd( mtxPDEElemL_G, mapDOFGlobalL, nDOFL, mapDOFGlobalF, nDOFF );

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qElemL, mapDOFGlobalR, nDOFR, mapDOFGlobalL, nDOFL );
  mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qElemR, mapDOFGlobalR, nDOFR, mapDOFGlobalR, nDOFR );
  mtxGlobalPDE_G.scatterAdd( mtxPDEElemR_G, mapDOFGlobalR, nDOFR, mapDOFGlobalF, nDOFF );

  mtxGlobalKutta_q.scatterAdd( mtxKutta_qElemL, mapDOFGlobalF, nDOFF, mapDOFGlobalL, nDOFL );
  mtxGlobalKutta_q.scatterAdd( mtxKutta_qElemR, mapDOFGlobalF, nDOFF, mapDOFGlobalR, nDOFR );
  mtxGlobalKutta_G.scatterAdd( mtxKutta_G, mapDOFGlobalF, nDOFF, mapDOFGlobalF, nDOFF );
}

#if 0
//----------------------------------------------------------------------------//
template <class QFieldCellGroupType,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryFrame_Galerkin_ScatterAdd(
    const QFieldTraceGroupType& lgfldTrace,
    const QFieldCellGroupType& qfldCellL,
    const int elemL, const int elem,
    int mapDOFGlobalL[], const int nDOFL,
    int mapDOFGlobalEdge[], const int nDOFEdge,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_lgEdge,
    SANS::DLA::MatrixD<MatrixQ>& mtxBC_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxBC_lgEdge,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_lg,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalBC_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalBC_lg )
{

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryEdge_ScatterAdd: elemL, elem = " << elemL << ," " << elem << std::endl;
  std::cout << "JacobianPDE_Group_2DBoundaryEdge_ScatterAdd (before): mtxPDEGlobal = ";
  mtxPDEGlobal.dump(2);
#endif

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryEdge_ScatterAdd: mtxGlobalPDE_q = ";  mtxGlobalPDE_q.dump(2);
#endif

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
  mtxGlobalPDE_lg.scatterAdd( mtxPDEElemL_lgEdge, elemL, elem );

  mtxGlobalBC_q.scatterAdd( mtxBC_qElemL, elem, elemL );
  mtxGlobalBC_lg.scatterAdd( mtxBC_lgEdge, elem, elem );
}
#endif

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//
//  functions dispatched based on left (L) element topology
//
//  process:
//  (a) loop over groups; dispatch to L (JacobianPDE_LeftTopology_Integral2DBoundaryEdge)
//  (b) call base class routine with specific L (JacobianPDE_Group_Integral2DBoundaryEdge w/ Base& params)
//  (c) cast to specific L and call L-specific topology routine (JacobianPDE_Group_Integral2DBoundaryEdge)
//  (d) loop over edges and integrate


template <class Surreal,
          class TopologyFrame,
          class TopologyTraceL, class TopologyCellL,
          class TopologyTraceR, class TopologyCellR,
          class XFieldType, class FieldFrameType,
          class PhysDim, class TopoDim, class ArrayQ,
          class PDE,
          class SparseMatrix>
void
JacobianBoundaryFrame_Galerkin_Circ_Group_Integral(
    const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCellL>& xfldCellL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCellL>& qfldCellL,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceR>& xfldTraceR,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCellR>& xfldCellR,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCellR>& qfldCellR,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_G,
    SparseMatrix& mtxGlobalKutta_q, SparseMatrix& mtxGlobalKutta_G )
{
  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCellL> XFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCellL> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCellR> XFieldCellGroupTypeR;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCellR> QFieldCellGroupTypeR;

  typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
  typedef typename QFieldCellGroupTypeR::template ElementType<Surreal> ElementQFieldClassR;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL> XFieldTraceGroupTypeL;
  typedef typename XFieldTraceGroupTypeL::template ElementType<> ElementXFieldTraceClassL;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceR> XFieldTraceGroupTypeR;
  typedef typename XFieldTraceGroupTypeR::template ElementType<> ElementXFieldTraceClassR;

  typedef typename XFieldType::FieldFrameGroupType XFieldFrameGroupType;
  typedef typename Field_DG_BoundaryFrame<PhysDim, TopoDim, ArrayQ>::FieldFrameGroupType GFieldFrameGroupType;
  typedef typename XFieldFrameGroupType::template ElementType<> ElementXFieldFrameClass;
  typedef typename GFieldFrameGroupType::template ElementType<Surreal> ElementGFieldFrameClass;
  typedef typename ElementXFieldFrameClass::TopoDim TopoDimFrame;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementXFieldClassR xfldElemR( xfldCellR.basis() );

  ElementQFieldClassL qfldElemL( qfldCellL.basis() );
  ElementQFieldClassR qfldElemR( qfldCellR.basis() );

  ElementXFieldTraceClassL xfldElemTraceL( xfldTraceL.basis() );
  ElementXFieldTraceClassR xfldElemTraceR( xfldTraceR.basis() );

  ElementXFieldFrameClass xfldElemFrame( xfldFrame.basis() );
  ElementGFieldFrameClass GfldElemFrame( GfldFrame.basis() );

  // variables/equations per DOF
  const int nEqn = PDE::N;

  // DOFs per element
  int nDOFL = qfldElemL.nDOF();
  int nDOFR = qfldElemR.nDOF();
  int nDOFK = GfldElemFrame.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobalL(nDOFL);
  std::vector<int> mapDOFGlobalR(nDOFR);
  std::vector<int> mapDOFGlobalF(nDOFK);

  // trace element integral
  GalerkinWeightedIntegral<TopoDimFrame, TopologyFrame, ArrayQSurreal, ArrayQSurreal, ArrayQSurreal> integral(quadratureorder, nDOFL, nDOFR, nDOFK);

  // element integrand/residuals
  std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
  std::vector<ArrayQSurreal> rsdPDEElemR( nDOFR );
  std::vector<ArrayQSurreal> rsdPDEElemK( nDOFK );

  // element jacobians
  MatrixElemClass mtxPDEElemL_qElemL(nDOFL, nDOFL);
  MatrixElemClass mtxPDEElemL_qElemR(nDOFL, nDOFR);
  MatrixElemClass mtxPDEElemL_G(nDOFL, nDOFK);

  MatrixElemClass mtxPDEElemR_qElemL(nDOFR, nDOFL);
  MatrixElemClass mtxPDEElemR_qElemR(nDOFR, nDOFR);
  MatrixElemClass mtxPDEElemR_G(nDOFR, nDOFK);

  MatrixElemClass mtxKutta_qElemL(nDOFK, nDOFL);
  MatrixElemClass mtxKutta_qElemR(nDOFK, nDOFR);
  MatrixElemClass mtxKutta_G(nDOFK, nDOFK);

  // loop over elements within group
  int nelem = xfldFrame.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // zero element Jacobians
    mtxPDEElemL_qElemL = 0;
    mtxPDEElemL_qElemR = 0;
    mtxPDEElemL_G = 0;
    mtxPDEElemR_qElemL = 0;
    mtxPDEElemR_qElemR = 0;
    mtxPDEElemR_G = 0;
    mtxKutta_qElemL = 0;
    mtxKutta_qElemR = 0;
    mtxKutta_G = 0;

    xfldFrame.getElement(xfldElemFrame, elem);
    GfldFrame.getElement(GfldElemFrame, elem);

    const int elemTraceL = xfldFrame.getElementLeft( elem );
    const int elemTraceR = xfldFrame.getElementRight( elem );
    CanonicalTraceToCell& canonicalFrameL  = xfldFrame.getCanonicalTraceLeft( elem );
    CanonicalTraceToCell& canonicalFrameR  = xfldFrame.getCanonicalTraceRight( elem );

    xfldTraceL.getElement( xfldElemTraceL, elemTraceL );
    xfldTraceR.getElement( xfldElemTraceR, elemTraceR );

    CanonicalTraceToCell& canonicalTraceL = xfldTraceL.getCanonicalTraceLeft( elemTraceL );
    CanonicalTraceToCell& canonicalTraceR = xfldTraceR.getCanonicalTraceLeft( elemTraceR ); //This is not a typo

    const int elemL = xfldTraceL.getElementLeft( elemTraceL );
    const int elemR = xfldTraceR.getElementLeft( elemTraceR ); // This is not a typo

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldCellR.getElement( xfldElemR, elemR );
    qfldCellR.getElement( qfldElemR, elemR );

    GfldFrame.associativity( elem  ).getGlobalMapping( mapDOFGlobalF.data(), nDOFK );
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nDOFR );

    // number of simultaneous derivatives per functor call
    const int nDeriv = DLA::index(qfldElemR.DOF(0), 0).size();

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nEqn*(nDOFL+nDOFR+nDOFK); nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot;
      for (int j = 0; j < nDOFL; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
        }
      }
      for (int j = 0; j < nDOFR; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(qfldElemR.DOF(j), n).deriv(k) = 0;

          slot = nEqn*nDOFL + nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemR.DOF(j), n).deriv(slot - nchunk) = 1;
        }
      }
      for (int j = 0; j < nDOFK; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(GfldElemFrame.DOF(j), n).deriv(k) = 0;

          slot = nEqn*(nDOFL+nDOFR) + nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(GfldElemFrame.DOF(j), n).deriv(slot - nchunk) = 1;
        }
      }

      // line integration for canonical element
      for (int n = 0; n < nDOFL; n++)
        rsdPDEElemL[n] = 0;

      for (int n = 0; n < nDOFR; n++)
        rsdPDEElemR[n] = 0;

      for (int n = 0; n < nDOFK; n++)
        rsdPDEElemK[n] = 0;


      integral(
          fcn.integrand(xfldElemFrame, GfldElemFrame,
                        xfldElemTraceL, xfldElemTraceR,
                        canonicalFrameL, canonicalFrameR,
                        canonicalTraceL, canonicalTraceR,
                        xfldElemL, qfldElemL,
                        xfldElemR, qfldElemR ),
          xfldElemFrame,
          rsdPDEElemL.data(), nDOFL,
          rsdPDEElemR.data(), nDOFR,
          rsdPDEElemK.data(), nDOFK );

      // accumulate derivatives into element jacobian

      for (int j = 0; j < nDOFL; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int i = 0; i < nDOFL; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemL_qElemL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

            for (int i = 0; i < nDOFR; i++)
            {
              if (mapDOFGlobalR[i] < fcn.dupPointOffset) continue;
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemR_qElemL(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot - nchunk);
            }

            for (int i = 0; i < nDOFK; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxKutta_qElemL(i,j), m,n) = DLA::index(rsdPDEElemK[i], m).deriv(slot - nchunk);
          }
        }
      }

      for (int j = 0; j < nDOFR; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*nDOFL + nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int i = 0; i < nDOFL; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemL_qElemR(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

            for (int i = 0; i < nDOFR; i++)
            {
              if (mapDOFGlobalR[i] < fcn.dupPointOffset) continue;
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemR_qElemR(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot - nchunk);
            }

            for (int i = 0; i < nDOFK; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxKutta_qElemR(i,j), m,n) = DLA::index(rsdPDEElemK[i], m).deriv(slot - nchunk);
          }
        }

        for (int j = 0; j < nDOFK; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*(nDOFL + nDOFR) + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElemL_G(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFR; i++)
              {
                if (mapDOFGlobalR[i] < fcn.dupPointOffset) continue;
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElemR_G(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot - nchunk);
              }

              for (int i = 0; i < nDOFK; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxKutta_G(i,j), m,n) = DLA::index(rsdPDEElemK[i], m).deriv(slot - nchunk);
            }
          }
        }

      }
    }   // nchunk

    // scatter-add element jacobian to global

    JacobianBoundaryFrame_Galerkin_Circ_ScatterAdd(
        GfldFrame, elem,
        qfldCellL, elemL,
        qfldCellR, elemR,
        mapDOFGlobalF.data(), nDOFK,
        mapDOFGlobalL.data(), nDOFL,
        mapDOFGlobalR.data(), nDOFR,
        mtxPDEElemL_qElemL,
        mtxPDEElemL_qElemR,
        mtxPDEElemL_G,
        mtxPDEElemR_qElemL,
        mtxPDEElemR_qElemR,
        mtxPDEElemR_G,
        mtxKutta_qElemL,
        mtxKutta_qElemR,
        mtxKutta_G,
        mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
  }
}


//----------------------------------------------------------------------------//
template <class Surreal, class TopologyFrame,
          class TopologyTraceL, class TopologyCellL,
          class TopologyTraceR, class TopologyCellR,
          class XFieldType, class FieldFrameType,
          class PDE,
          class PhysDim, class TopoDim, class ArrayQ,
          class SparseMatrix>
void
JacobianBoundaryFrame_Galerkin_Circ_Group(
    const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceR>& xfldTraceR,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_G,
    SparseMatrix& mtxGlobalKutta_q, SparseMatrix& mtxGlobalKutta_G )
{
  const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

  int groupL = xfldTraceL.getGroupLeft();
  int groupR = xfldTraceR.getGroupLeft(); //This is not a typo.

  // Integrate over the trace group
  JacobianBoundaryFrame_Galerkin_Circ_Group_Integral<Surreal,
                                                TopologyFrame,
                                                TopologyTraceL,TopologyCellL,
                                                TopologyTraceR,TopologyCellR,
                                                XFieldType,FieldFrameType,PhysDim,TopoDim,ArrayQ>(
      fcn, xfldFrame, GfldFrame,
      xfldTraceL, xfld.template getCellGroup<TopologyCellL>(groupL), qfld.template getCellGroup<TopologyCellL>(groupL),
      xfldTraceR, xfld.template getCellGroup<TopologyCellR>(groupR), qfld.template getCellGroup<TopologyCellR>(groupR),
      quadratureorder,
      mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
}

template<class Surreal, class TopoDim>
class JacobianBoundaryFrame_Galerkin_Circ;
#if 0
template<class Surreal>
class JacobianBoundaryFrame_Galerkin<Surreal,TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  //----------------------------------------------------------------------------//
  template <class TopologyTrace,
            class PDE, class BC,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  LeftTopology(
      const IntegrandBoundary_Galerkin_PDE<PDE, BC>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_Galerkin_Group<Surreal, TopologyTrace, Line>(
          fcn, xfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD1>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class PDE, class BC,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandBoundary_Galerkin_PDE<PDE, BC>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q )
  {
    SANS_ASSERT( mtxGlobalPDE_q.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q.n() == qfld.nDOF() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over line element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Node) )
      {
        // dispatch to determine left topology
        LeftTopology<Node>(
            fcn,
            xfld.template getBoundaryTraceGroup<Node>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup],
            mtxGlobalPDE_q );
      }
      else
      {
        char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD1>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};


template<class Surreal>
class JacobianBoundaryFrame_Galerkin<Surreal,TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  //----------------------------------------------------------------------------//
  template <class TopologyTrace,
            class PDE, class BC,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  LeftTopology(
      const IntegrandBoundary_Galerkin_PDE<PDE, BC>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_Galerkin_Group<Surreal, TopologyTrace, Triangle>(
          fcn, xfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class PDE, class BC,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandBoundary_Galerkin_PDE<PDE, BC>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q )
  {
    SANS_ASSERT( mtxGlobalPDE_q.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q.n() == qfld.nDOF() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over line element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        // dispatch to determine left topology
        LeftTopology<Line>(
            fcn,
            xfld.template getBoundaryTraceGroup<Line>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup],
            mtxGlobalPDE_q );
      }
      else
      {
        char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};
#endif


template<class Surreal>
class JacobianBoundaryFrame_Galerkin_Circ<Surreal,TopoD3>
{
  typedef TopoD3 TopoDim;
protected:

  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyTraceL, class TopologyCellL,
            class XFieldType, class FieldFrameType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellRight_Triangle(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTraceR,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_G,
      SparseMatrix& mtxGlobalKutta_q, SparseMatrix& mtxGlobalKutta_G )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_Galerkin_Circ_Group<Surreal, TopologyFrame, TopologyTraceL, TopologyCellL, Triangle, Tet, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame, xfldTraceL, xfldTraceR, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyTraceL, class TopologyCellL,
            class XFieldType, class FieldFrameType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellRight_Quad(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTraceR,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_G,
      SparseMatrix& mtxGlobalKutta_q, SparseMatrix& mtxGlobalKutta_G )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_Galerkin_Circ_Group<Surreal, TopologyFrame, TopologyTraceL, TopologyCellL, Quad, Hex, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame, xfldTraceL, xfldTraceR, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyTraceL, class TopologyCellL,
            class XFieldType, class FieldFrameType, class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TraceTopologyRight(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_G,
      SparseMatrix& mtxGlobalKutta_q, SparseMatrix& mtxGlobalKutta_G )
  {
    const int boundaryGroupR = xfldFrame.getGroupRight();

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // get the boundary group
    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Triangle) )
    {
      // dispatch to determine left topology
      TopologyCellRight_Triangle<TopologyFrame, TopologyTraceL, TopologyCellL, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame,
          xfldTraceL,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupR),
          qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Quad) )
    {
      // dispatch to determine left topology
      TopologyCellRight_Quad<TopologyFrame, TopologyTraceL, TopologyCellL, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame,
          xfldTraceL,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupR),
          qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType, class FieldFrameType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellLeft_Triangle(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTraceL,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_G,
      SparseMatrix& mtxGlobalKutta_q, SparseMatrix& mtxGlobalKutta_G )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      TraceTopologyRight<TopologyFrame, Triangle, Tet, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame, xfldTraceL, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType, class FieldFrameType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellLeft_Quad(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename Field_DG_BoundaryFrame<PhysDim, TopoD3, ArrayQ>::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTraceL,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_G,
      SparseMatrix& mtxGlobalKutta_q, SparseMatrix& mtxGlobalKutta_G )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      TraceTopologyRight<TopologyFrame, Quad, Hex, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame, xfldTraceL, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class XFieldType, class FieldFrameType, class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TraceTopologyLeft(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_G,
      SparseMatrix& mtxGlobalKutta_q, SparseMatrix& mtxGlobalKutta_G )
  {
    const int boundaryGroupL = xfldFrame.getGroupLeft();

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // get the boundary group
    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Triangle) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Triangle<TopologyFrame, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupL),
          qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Quad) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Quad<TopologyFrame, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupL),
          qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class XFieldType, class FieldFrameType, class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldFrameType& Gfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_G,
      SparseMatrix& mtxGlobalKutta_q, SparseMatrix& mtxGlobalKutta_G )
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );

    SANS_ASSERT( ngroup == xfld.nBoundaryFrameGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryFrameGroups(); group++)
    {
      const int boundaryFrameGroup = fcn.boundaryFrameGroup(group);

      TraceTopologyLeft<Line, XFieldType, FieldFrameType>( fcn,
          xfld.getBoundaryFrameGroup(boundaryFrameGroup),
          Gfld.getBoundaryFrameGroupGlobal(boundaryFrameGroup),
          qfld,
          quadratureorder[boundaryFrameGroup], mtxGlobalPDE_q, mtxGlobalPDE_G, mtxGlobalKutta_q, mtxGlobalKutta_G );

    }
  }
};


}

#endif  // JACOBIANBOUNDARYFRAME_GALERKIN_CIRC_H
