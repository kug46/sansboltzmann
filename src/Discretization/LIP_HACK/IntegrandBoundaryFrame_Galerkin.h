// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFRAME_GALERKIN_H
#define INTEGRANDBOUNDARYFRAME_GALERKIN_H

// boundary-frame integral functional
#include "tools/SANSnumerics.h"
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PDE>
class IntegrandBoundaryFrame_Galerkin
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryFrame_Galerkin( const PDE& pde, const std::vector<int>& BoundaryFrameGroups,
                                   const int dupPointOffset, const std::vector<int>& KuttaPoints )
    : dupPointOffset(dupPointOffset), KuttaPoints(KuttaPoints), pde_(pde), BoundaryFrameGroups_(BoundaryFrameGroups)
  {}

  const int dupPointOffset;
  const std::vector<int> KuttaPoints;

  std::size_t nBoundaryFrameGroups() const { return BoundaryFrameGroups_.size(); }
  std::size_t boundaryFrameGroup(const int n) const { return BoundaryFrameGroups_[n]; }

  template<class T, class TopoDimFrame, class TopologyFrame,
                    class TopoDimTrace, class TopologyTraceL, class TopologyTraceR,
                    class TopoDimCell, class TopologyCellL, class TopologyCellR>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimFrame, TopologyFrame > ElementXFieldFrame;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTraceL > ElementXFieldTraceL;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTraceR > ElementXFieldTraceR;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyCellL > ElementXFieldCellL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyCellR > ElementXFieldCellR;

    typedef Element<ArrayQ<T>, TopoDimTrace , TopologyTraceL> ElementQFieldTraceL;
    typedef Element<ArrayQ<T>, TopoDimTrace , TopologyTraceR> ElementQFieldTraceR;
    typedef Element<ArrayQ<T>, TopoDimCell  , TopologyCellL> ElementQFieldCellL;
    typedef Element<ArrayQ<T>, TopoDimCell  , TopologyCellR> ElementQFieldCellR;

    typedef typename ElementXFieldFrame::RefCoordType RefCoordFrameType;
    typedef typename ElementXFieldTraceL::RefCoordType RefCoordTraceType;
    typedef typename ElementXFieldCellL::RefCoordType RefCoordCellTypeL;
    typedef typename ElementXFieldCellR::RefCoordType RefCoordCellTypeR;
    typedef typename ElementXFieldTraceL::VectorX VectorX;

    typedef QuadraturePoint<typename TopologyFrame::TopoDim> QuadPointFrameType;
    //typedef QuadraturePoint<typename TopologyTrace::TopoDim> QuadPointTraceType;
    //typedef QuadratureCellTracePoint<typename TopologyCellL::TopoDim> QuadPointCellLType;

    Functor( const PDE& pde,
             const ElementXFieldFrame& xfldElemFrame,
             const ElementXFieldTraceL& xfldElemTraceL,
             const ElementXFieldTraceR& xfldElemTraceR,
             const CanonicalTraceToCell& canonicalFrameL,
             const CanonicalTraceToCell& canonicalFrameR,
             const CanonicalTraceToCell& canonicalTraceL,
             const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldCellL& xfldElemL,
             const ElementQFieldCellL& qfldElemL,
             const ElementXFieldCellR& xfldElemR,
             const ElementQFieldCellR& qfldElemR ) :
             pde_(pde), xfldElemFrame_(xfldElemFrame), xfldElemTraceL_(xfldElemTraceL), xfldElemTraceR_(xfldElemTraceR),
             canonicalFrameL_(canonicalFrameL), canonicalFrameR_(canonicalFrameR),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR)
             {}

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // trace integrand
    void operator()( const QuadPointFrameType& sRefFrame,
                     ArrayQ<T> integrandPDEL[], int nrsdPDEL,
                     ArrayQ<T> integrandPDER[], int nrsdPDER ) const
    {
      typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

      const int nDOFL = qfldElemL_.nDOF();             // total solution DOFs in left element
      const int nDOFR = qfldElemR_.nDOF();             // total solution DOFs in right element

      SANS_ASSERT(nrsdPDEL == nDOFL);
      SANS_ASSERT(nrsdPDER == nDOFR);
      SANS_ASSERT( nDOFL == nDOFR );

      VectorX xL, xR;
      VectorX nL, nR;           // unit normal for left element (points to right element)

      std::vector<Real> phiL(nDOFL);                                   // basis (left)
      std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiL(nDOFL);  // basis gradient (left)
      std::vector<Real> phiR(nDOFR);                                   // basis (right)
      std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiR(nDOFR);  // basis gradient (right)

      ArrayQ<T> qL, qR;
      VectorArrayQ gradqL, gradqR;  // gradients

      DLA::VectorS< PhysDim::D, Real > U; // Freestream Velocity

      RefCoordTraceType sRefTraceL;
      RefCoordTraceType sRefTraceR;
      RefCoordCellTypeL sRefCellL;
      RefCoordCellTypeR sRefCellR;

      TraceToCellRefCoord<TopologyFrame, TopoDimTrace, TopologyTraceL>::eval( canonicalFrameL_, sRefFrame.ref, sRefTraceL );
      TraceToCellRefCoord<TopologyFrame, TopoDimTrace, TopologyTraceR>::eval( canonicalFrameR_, sRefFrame.ref, sRefTraceR );
      TraceToCellRefCoord<TopologyTraceL, TopoDimCell, TopologyCellL>::eval( canonicalTraceL_, sRefTraceL, sRefCellL );
      TraceToCellRefCoord<TopologyTraceR, TopoDimCell, TopologyCellR>::eval( canonicalTraceR_, sRefTraceR, sRefCellR );

      // unit normals
      xfldElemTraceL_.unitNormal( sRefTraceL, nL );
      xfldElemTraceR_.unitNormal( sRefTraceR, nR );

      // basis value, gradient
      qfldElemL_.evalBasis( sRefCellL, phiL.data(), nDOFL );
      qfldElemR_.evalBasis( sRefCellR, phiR.data(), nDOFR );
      xfldElemL_.evalBasisGradient( sRefCellL, qfldElemL_, gradphiL.data(), nDOFL );
      xfldElemR_.evalBasisGradient( sRefCellR, qfldElemR_, gradphiR.data(), nDOFR );

      xfldElemL_.eval( sRefCellL, xL );
      xfldElemR_.eval( sRefCellR, xR );

      // solution value, gradient
      qfldElemL_.eval( sRefCellL, qL );
      qfldElemR_.eval( sRefCellR, qR );
      qfldElemL_.evalFromBasis( gradphiL.data(), nDOFL, gradqL );
      qfldElemR_.evalFromBasis( gradphiR.data(), nDOFR, gradqR );

      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] = 0;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] = 0;

      //pde_.freestream(U);
      U = pde_.Up(xL);

      for (int n = 0; n < PhysDim::D; n++)
        SANS_ASSERT( fabs(xL[n] - xR[n]) < 1e-12 );

      //std::cout << "nL = " << nL << "  nR = " << nR << std::endl;

      //VectorArrayQ gradqjump = gradqR - gradqL;
      //ArrayQ<T> Ugradqjump = dot(U,gradqjump);

      //Ugradqjump += 0.5*(dot(gradqR,gradqR) - dot(gradqL,gradqL));
#if 1
      //Real hL = pow( xfldElemTraceL_.jacobianDeterminant(), 1./TopoDimTrace::D );
      Real UnL = dot(nL,U); // Normal velocity
      DLA::VectorS< PhysDim::D, Real > UsL = U - UnL*nL; // Tangential velocity
      //Real VL = sqrt( dot(UsL, UsL) );
#endif
#if 1
      //Real hR = pow( xfldElemTraceR_.jacobianDeterminant(), 1./TopoDimTrace::D );
      Real UnR = dot(nR,U); // Normal velocity
      DLA::VectorS< PhysDim::D, Real > UsR = U - UnR*nR; // Tangential velocity
      //Real VR = sqrt( dot(UsR, UsR) );
#endif

      // tangential BC
#if 0
      // Compute the surface tangent gradient
      gradqL -= dot(nL,gradqL)*nL;
      gradqR -= dot(nR,gradqR)*nR;

      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += phiL[k]*dot(UsL,gradqL);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += phiR[k]*dot(UsR,gradqR);
#endif

#if 0
      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += -(phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*Ugradqjump;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] +=  (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*Ugradqjump;
#endif

      //Tangential pressure jump
#if 0
      // Compute the surface tangent gradient
      gradqL -= dot(nL,gradqL)*nL;
      gradqR -= dot(nR,gradqR)*nR;

      ArrayQ<T> Ugradqjump = dot(U,gradqR) - dot(U,gradqL);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += phiR[k]*Ugradqjump;
#endif

      //Pressure jump
#if 0
      ArrayQ<T> Ugradqjump = dot(U,gradqR) - dot(U,gradqL);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += phiR[k]*Ugradqjump;
#endif

      //Pressure jump
#if 0
      DLA::VectorS< PhysDim::D, T > VR = gradqR + U;
      DLA::VectorS< PhysDim::D, T > VL = gradqL + U;

      ArrayQ<T> Pjump = dot(VR,VR) - dot(VL,VL);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += phiR[k]*Pjump;
#endif

      //Pressure jump
#if 0
      for (int k = 0; k < nDOFR; k++)
      {
        Real wR = dot(nR,gradphiR[k]);
        Real wL = dot(nL,gradphiL[k]);
        integrandPDER[k] += wR*dot(U,gradqR) + wL*dot(U,gradqL);
      }
#endif

      //Pressure jump
#if 0
      DLA::VectorS< PhysDim::D, T > VR = gradqR + U;
      DLA::VectorS< PhysDim::D, T > VL = gradqL + U;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += dot(nR,gradphiR[k])*dot(VR,VR) + dot(nL,gradphiL[k])*dot(VL,VL);
#endif

      //Pressure jump
#if 0
      // Compute the surface tangent gradient
      gradqL -= dot(nL,gradqL)*nL;
      gradqR -= dot(nR,gradqR)*nR;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*dot(UsR,gradqR)
                          - (phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*dot(UsL,gradqL);
#endif

      //Pressure jump
#if 1
      // Compute the surface tangent gradient
      gradqL -= dot(nL,gradqL)*nL;
      gradqR -= dot(nR,gradqR)*nR;

      ArrayQ<T> UgradqR = dot(UsR,gradqR) - UnR*UnR;
      ArrayQ<T> UgradqL = dot(UsL,gradqL) - UnL*UnL;
      ArrayQ<T> Ugradqjump = UgradqR - UgradqL;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += phiR[k]*Ugradqjump;
#endif


#if 0
      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += (phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*Ugradqjump;
#endif

#if 0
      ArrayQ<T> Ugradqjump = dot(U,gradqR) - dot(U,gradqL);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] +=  (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*Ugradqjump;
#endif

#if 0
      VectorArrayQ gradqsum = gradqR + gradqL;
      ArrayQ<T> Ugradqsum = dot(U,gradqsum);

      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += (phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*Ugradqsum;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*Ugradqsum;
#endif

#if 0
      VectorArrayQ gradqsum = gradqR + gradqL;
      ArrayQ<T> Ugradqsum = dot(U,gradqsum);

      for (int k = 0; k < nDOFL; k++)
      {
        Real PhiL = phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k]));
        integrandPDEL[k] += 0.5*PhiL*Ugradqsum - 0.5*PhiL*Ugradqjump;
      }

      for (int k = 0; k < nDOFR; k++)
      {
        Real PhiR = phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k]));
        integrandPDER[k] += 0.5*PhiR*Ugradqsum + 0.5*PhiR*Ugradqjump;
      }
#endif

#if 0
      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += -(phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*(0.5*dot(gradqL,gradqL) + dot(U,gradqL));

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] +=  (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*(0.5*dot(gradqR,gradqR) + dot(U,gradqR));
#endif

#if 0
      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += 0.5*phiL[k]*(qR - qL);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += 0.5*phiR[k]*(qR - qL);
#endif
    }

  protected:
    const PDE& pde_;
    const ElementXFieldFrame& xfldElemFrame_;
    const ElementXFieldTraceL& xfldElemTraceL_;
    const ElementXFieldTraceR& xfldElemTraceR_;
    const CanonicalTraceToCell canonicalFrameL_;
    const CanonicalTraceToCell canonicalFrameR_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldCellL& xfldElemL_;
    const ElementQFieldCellL& qfldElemL_;
    const ElementXFieldCellR& xfldElemR_;
    const ElementQFieldCellR& qfldElemR_;
  };

  template<class T, class TopoDimFrame, class TopologyFrame,
                    class TopoDimTrace, class TopologyTraceL, class TopologyTraceR,
                    class TopoDimCell, class TopologyCellL, class TopologyCellR>
  Functor<T, TopoDimFrame, TopologyFrame,
             TopoDimTrace, TopologyTraceL, TopologyTraceR,
             TopoDimCell, TopologyCellL, TopologyCellR>
  integrand(const ElementXField<PhysDim, TopoDimFrame, TopologyFrame>& xfldElemFrame,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTraceL>& xfldElemTraceL,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTraceR>& xfldElemTraceR,
            const CanonicalTraceToCell& canonicalFrameL,
            const CanonicalTraceToCell& canonicalFrameR,
            const CanonicalTraceToCell& canonicalTraceL,
            const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell , TopologyCellL >& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyCellL >& qfldElemL,
            const ElementXField<PhysDim, TopoDimCell , TopologyCellR >& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyCellR >& qfldElemR ) const
  {
    return Functor<T, TopoDimFrame, TopologyFrame,
                      TopoDimTrace, TopologyTraceL, TopologyTraceR,
                      TopoDimCell, TopologyCellL, TopologyCellR>(pde_, xfldElemFrame,
                                                                 xfldElemTraceL, xfldElemTraceR,
                                                                 canonicalFrameL, canonicalFrameR,
                                                                 canonicalTraceL, canonicalTraceR,
                                                                 xfldElemL, qfldElemL,
                                                                 xfldElemR, qfldElemR );
  }

protected:
  const PDE& pde_;
  const std::vector<int> BoundaryFrameGroups_;
};

} //namespace SANS

#endif // INTEGRANDBOUNDARYFRAME_GALERKIN_H
