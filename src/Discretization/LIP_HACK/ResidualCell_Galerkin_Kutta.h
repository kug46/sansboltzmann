// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALCELL_GALERKIN_KUTTA_H
#define RESIDUALCELL_GALERKIN_KUTTA_H

// Cell integral residual functions

#include <memory> //unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Field_DG_BoundaryFrame.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "IntegrandFunctorCell_Galerkin_Kutta_LIP.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group residual
//
// topology specific group residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   PDE              integrand functor

template <class Topology, class PhysDim, class TopoDim,
          class PDE,
          class ArrayQ>
void
ResidualCell_Galerkin_Kutta_Group_Integral(
    const IntegrandCell_Galerkin_Kutta_LIP<PDE>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldGroup,
    const int quadratureorder,
    const std::map<int,int>& mapKuttaGlobal,
    SLA::SparseVector<ArrayQ>& rsdKuttaGlobal, int sgn )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldGroup.basis() );
  ElementQFieldClass qfldElem( qfldGroup.basis() );

  // number of integrals evaluated
  const int nDOFX = qfldElem.nDOF();
  const int nIntegrandK = qfldElem.nDOF();

  // element integral
  GalerkinWeightedIntegral<TopoDim, Topology, ArrayQ> integral(quadratureorder, nIntegrandK);

  // just to make sure things are consistent
  SANS_ASSERT( xfldGroup.nElem() == qfldGroup.nElem() );

  // element-to-global DOF mapping
  std::unique_ptr<int[]> mapDOFGlobalX( new int[nDOFX] );
  //std::unique_ptr<int[]> mapDOFGlobalK( new int[nIntegrandK] );

  for (int n = 0; n < nDOFX; n++)       mapDOFGlobalX[n] = -1; // to remove clang analyzer warnings
  //for (int n = 0; n < nIntegrandK; n++) mapDOFGlobalK[n] = -1; // to remove clang analyzer warnings

  // residual array
  std::unique_ptr<ArrayQ[]> rsdPDEElem( new ArrayQ[nIntegrandK] );

  // loop over elements within group
  const int nelem = xfldGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // copy global grid/solution DOFs to element
    xfldGroup.getElement( xfldElem, elem );
    qfldGroup.getElement( qfldElem, elem );

    for (int n = 0; n < nIntegrandK; n++)
      rsdPDEElem[n] = 0;

    // cell integration for canonical element
    integral( fcn.integrand(xfldElem, qfldElem), xfldElem, rsdPDEElem.get(), nIntegrandK );

    // scatter-add element integral to global
    xfldGroup.associativity( elem ).getGlobalMapping( mapDOFGlobalX.get(), nDOFX );
    //qfldGroup.associativity( elem ).getGlobalMapping( mapDOFGlobalK.get(), nIntegrandK );

    for (int n = 0; n < nDOFX; n++)
    {
      try
      {
        int nGlobal = mapKuttaGlobal.at(mapDOFGlobalX[n]);
        rsdKuttaGlobal[nGlobal] += sgn*rsdPDEElem[n];
      }
      catch ( const std::exception& ) {}
    }
  }
}

//----------------------------------------------------------------------------//
template<class TopDim>
class ResidualCell_Galerkin_Kutta;

// base class interface
#if 0
template<>
class ResidualCell_Galerkin_Kutta<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class PDE,
            class PhysDim, class ArrayQ>
  static void
  integrate( const IntegrandCell_Galerkin<PDE>& fcn,
             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             int quadratureorder[], int ngroup,
             SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
  {
    SANS_ASSERT( ngroup == qfld.nCellGroups() );
    SANS_ASSERT( rsdKuttaGlobal.m() == qfld.nDOF() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // loop over element groups
    for (std::size_t group = 0; group < fcn.nCellGroups(); group++)
    {
      const int cellGroup = fcn.cellGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        ResidualCell_Galerkin_Group_Integral<Triangle, PhysDim, TopoDim, PDE, ArrayQ>( fcn,
                                      xfld.template getCellGroup<Triangle>(cellGroup),
                                      qfld.template getCellGroupGlobal<Triangle>(cellGroup),
                                      quadratureorder[cellGroup], rsdKuttaGlobal );
      }
      else if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Quad) )
      {
        ResidualCell_Galerkin_Group_Integral<Quad, PhysDim, TopoDim, PDE, ArrayQ>( fcn,
                                      xfld.template getCellGroup<Quad>(cellGroup),
                                      qfld.template getCellGroupGlobal<Quad>(cellGroup),
                                      quadratureorder[cellGroup], rsdKuttaGlobal );
      }
      else
      {
        const char msg[] = "Error in ResidualCell_Galerkin<TopoD2>: unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};
#endif


template<>
class ResidualCell_Galerkin_Kutta<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

  template <class PDE, class XFieldType,
            class PhysDim, class ArrayQ>
  static void
  integrate( const IntegrandCell_Galerkin_Kutta_LIP<PDE>& fcn,
             const XFieldType& xfld,
             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             const Field_DG_BoundaryFrame<PhysDim, TopoD3, ArrayQ>& Gfld,
             int quadratureorder[], int ngroup,
             SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
  {
    SANS_ASSERT( ngroup == qfld.nCellGroups() );
    //SANS_ASSERT( rsdKuttaGlobal.m() == Gfld.nDOF() );

    // loop over element groups
    for (std::size_t group = 0; group < fcn.nBoundaryFrameGroups(); group++)
    {
      const int boundaryFrameGroup = fcn.boundaryFrameGroup(group);

      const typename XFieldType::FieldFrameGroupType& xfldFrame = xfld.getBoundaryFrameGroup(boundaryFrameGroup);
      const typename Field_DG_BoundaryFrame<PhysDim, TopoD3, ArrayQ>::FieldFrameGroupType&
        GfldFrame = Gfld.getBoundaryFrameGroupGlobal(boundaryFrameGroup);
      std::map<int,int> mapKuttaGlobalLeft, mapKuttaGlobalRight;

      int nDOFX = xfldFrame.basis()->nBasis();
      int nIntegrandK = GfldFrame.basis()->nBasis();
      std::vector<int> mapDOFGlobalX( nDOFX );
      std::vector<int> mapDOFGlobalK( nIntegrandK );
      SANS_ASSERT( nIntegrandK == 2 ); //TODO: HACK hard coded for P1 at the moment...
      SANS_ASSERT( nDOFX == nIntegrandK ); //TODO: HACK hard coded for grid and solution order the same

      int nelem = xfldFrame.nElem();
      for (int elem = 0; elem < nelem; elem++)
      {
        xfldFrame.associativity( elem ).getGlobalMapping( &mapDOFGlobalX[0], nDOFX );
        GfldFrame.associativity( elem ).getGlobalMapping( &mapDOFGlobalK[0], nIntegrandK );

        for (int n = 0; n < nDOFX; n++)
        {
          if ( mapDOFGlobalX[n] < xfld.dupPointOffset_ ) continue;

          // The X nodes are associated with the duplicate points
          mapKuttaGlobalRight[mapDOFGlobalX[n]] = mapDOFGlobalK[n];

          // Convert duplicate points on the right to points on the left
          if ( mapDOFGlobalX[n] >= xfld.dupPointOffset_ )
            mapDOFGlobalX[n] = xfld.invPointMap_[ mapDOFGlobalX[n] - xfld.dupPointOffset_ ];

          mapKuttaGlobalLeft[mapDOFGlobalX[n]] = mapDOFGlobalK[n];
        }
      }


      const int cellGroupLeft = fcn.cellGroupLeft(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupLeft).topoTypeID() == typeid(Tet) )
      {
        ResidualCell_Galerkin_Kutta_Group_Integral<Tet, PhysDim, TopoDim, PDE, ArrayQ>( fcn,
                                      xfld.template getCellGroup<Tet>(cellGroupLeft),
                                      qfld.template getCellGroupGlobal<Tet>(cellGroupLeft),
                                      quadratureorder[cellGroupLeft], mapKuttaGlobalLeft, rsdKuttaGlobal, -1 );
      }
      else if ( xfld.getCellGroupBase(cellGroupLeft).topoTypeID() == typeid(Hex) )
      {
        ResidualCell_Galerkin_Kutta_Group_Integral<Hex, PhysDim, TopoDim, PDE, ArrayQ>( fcn,
                                      xfld.template getCellGroup<Hex>(cellGroupLeft),
                                      qfld.template getCellGroupGlobal<Hex>(cellGroupLeft),
                                      quadratureorder[cellGroupLeft], mapKuttaGlobalLeft, rsdKuttaGlobal, -1 );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Error in ResidualCell_Galerkin_Kutta<TopoD3>: unknown topology\n" );


      const int cellGroupRight = fcn.cellGroupRight(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupRight).topoTypeID() == typeid(Tet) )
      {
        ResidualCell_Galerkin_Kutta_Group_Integral<Tet, PhysDim, TopoDim, PDE, ArrayQ>( fcn,
                                      xfld.template getCellGroup<Tet>(cellGroupRight),
                                      qfld.template getCellGroupGlobal<Tet>(cellGroupRight),
                                      quadratureorder[cellGroupRight], mapKuttaGlobalRight, rsdKuttaGlobal, 1 );
      }
      else if ( xfld.getCellGroupBase(cellGroupRight).topoTypeID() == typeid(Hex) )
      {
        ResidualCell_Galerkin_Kutta_Group_Integral<Hex, PhysDim, TopoDim, PDE, ArrayQ>( fcn,
                                      xfld.template getCellGroup<Hex>(cellGroupRight),
                                      qfld.template getCellGroupGlobal<Hex>(cellGroupRight),
                                      quadratureorder[cellGroupRight], mapKuttaGlobalRight, rsdKuttaGlobal, 1 );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Error in ResidualCell_Galerkin_Kutta<TopoD3>: unknown topology\n" );

    }
  }
};

}

#endif  // RESIDUALCELL_GALERKIN_KUTTA_H
