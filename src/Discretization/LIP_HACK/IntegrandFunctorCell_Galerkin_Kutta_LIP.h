// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDFUNCTORCELL_GALERKIN_KUTTA_LIP_H
#define INTEGRANDFUNCTORCELL_GALERKIN_KUTTA_LIP_H

// cell/trace integrand operators: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template <class PDE>
class IntegrandCell_Galerkin_Kutta_LIP
{
public:
  typedef typename PDE::PhysDim PhysDim;

  typedef typename PDE::VectorX VectorX;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin_Kutta_LIP( const PDE& pde,
                                    const std::vector<int>& BoundaryFrameGroups,
                                    const std::vector<int>& CellGroupsLeft,
                                    const std::vector<int>& CellGroupsRight )
    : pde_(pde), BoundaryFrameGroups_(BoundaryFrameGroups),
      CellGroupsLeft_(CellGroupsLeft), CellGroupsRight_(CellGroupsRight)
  {
    SANS_ASSERT(BoundaryFrameGroups_.size() == CellGroupsLeft_.size());
    SANS_ASSERT(BoundaryFrameGroups_.size() == CellGroupsRight_.size());
  }

  std::size_t nBoundaryFrameGroups() const { return BoundaryFrameGroups_.size(); }
  std::size_t boundaryFrameGroup(const int n) const { return BoundaryFrameGroups_[n]; }

  std::size_t cellGroupLeft(const int n) const { return CellGroupsLeft_[n]; }
  std::size_t cellGroupRight(const int n) const { return CellGroupsRight_[n]; }

  template<class T, class TopoDim, class Topology>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    Functor( const PDE& pde,
             const ElementXFieldType& xfldElem,
             const ElementQFieldType& qfldElem ) :
             pde_(pde), xfldElem_(xfldElem), qfldElem_(qfldElem) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }

    // 2D element area integrand
    void operator()( const QuadPointType& sRef, ArrayQ<T> integrand[], int neq ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
  };

  template<class T, class TopoDim, class Topology>
  Functor<T, TopoDim, Topology> integrand(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                                          const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return Functor<T, TopoDim, Topology>(pde_, xfldElem, qfldElem);
  }
protected:
  const PDE& pde_;
  const std::vector<int> BoundaryFrameGroups_;
  const std::vector<int> CellGroupsLeft_;
  const std::vector<int> CellGroupsRight_;
};


template <class PDE>
template <class T, class TopoDim, class Topology>
bool
IntegrandCell_Galerkin_Kutta_LIP<PDE>::Functor<T,TopoDim,Topology>::needsEvaluation() const
{
  return true;
}


// Cell integrand
//
template <class PDE>
template <class T, class TopoDim, class Topology>
void
IntegrandCell_Galerkin_Kutta_LIP<PDE>::Functor<T,TopoDim,Topology>::operator()(
    const QuadPointType& sRef, ArrayQ<T> integrand[], int neqn ) const
{
  const int nDOF = qfldElem_.nDOF();         // total solution DOFs in element

  SANS_ASSERT(neqn == nDOF);

  VectorX X;

  std::vector<Real> phi(nDOF);             // basis
  std::vector< DLA::VectorS<PhysDim::D, Real> > gradphi(nDOF);            // basis gradient

  ArrayQ<T> q;                // solution
  DLA::VectorS<PhysDim::D, ArrayQ<T> > gradq;           // gradient

  DLA::VectorS< PhysDim::D, Real > U; // Freestream Velocity

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi.data(), nDOF );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi.data(), nDOF );

  xfldElem_.eval( sRef, X );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi.data(), nDOF, q );
  qfldElem_.evalFromBasis( gradphi.data(), nDOF, gradq );

  //pde_.freestream(U);
  U = pde_.Up(X);

  for (int k = 0; k < nDOF; k++)
    integrand[k] = 0;

  // Linearized Pressure

  ArrayQ<T> Ugradq = dot(U,gradq);

#if 0
  for (int k = 0; k < nDOF; k++)
    integrand[k] += phi[k]*Ugradq;
#endif

#if 1
  for (int k = 0; k < nDOF; k++)
    integrand[k] += dot(U,gradphi[k])*Ugradq;
#endif

}


}

#endif  // INTEGRANDFUNCTORCELL_GALERKIN_KUTTA_H
