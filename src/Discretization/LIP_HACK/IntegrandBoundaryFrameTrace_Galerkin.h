// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFRAMETRACE_GALERKIN_H
#define INTEGRANDBOUNDARYFRAMETRACE_GALERKIN_H

// boundary-frame integral functional
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PDE>
class IntegrandBoundaryFrameTrace_Galerkin
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryFrameTrace_Galerkin( const PDE& pde, const std::vector<int>& BoundaryFrameGroups, const int dupPointOffset )
    : dupPointOffset(dupPointOffset), pde_(pde), BoundaryFrameGroups_(BoundaryFrameGroups)
  {}

  const int dupPointOffset;

  std::size_t nBoundaryFrameGroups() const { return BoundaryFrameGroups_.size(); }
  std::size_t boundaryFrameGroup(const int n) const { return BoundaryFrameGroups_[n]; }

  template<class T, class TopoDimFrame, class TopologyFrame,
                    class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimFrame, TopologyFrame > ElementXFieldFrame;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace > ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyCell > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace , TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell  , TopologyCell> ElementQFieldCell;

    typedef typename ElementXFieldFrame::RefCoordType RefCoordFrameType;
    typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
    typedef typename ElementXFieldCell::RefCoordType RefCoordCellType;
    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<typename TopologyTrace::TopoDim> QuadPointTraceType;
    typedef QuadratureCellTracePoint<typename TopologyCell::TopoDim> QuadPointCellType;

    Functor( const PDE& pde,
             const ElementXFieldFrame& xfldElemFrame,
             const ElementXFieldTrace& xfldElemTrace,
             const CanonicalTraceToCell& canonicalFrame,
             const CanonicalTraceToCell& canonicalTrace,
             const ElementXFieldCell& xfldElem,
             const ElementQFieldCell& qfldElem ) :
             pde_(pde), xfldElemFrame_(xfldElemFrame), xfldElemTrace_(xfldElemTrace),
             canonicalFrame_(canonicalFrame),
             canonicalTrace_(canonicalTrace),
             xfldElem_(xfldElem), qfldElem_(qfldElem)
             {}

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // trace integrand
    void operator()( const QuadPointTraceType& sRefTrace,
                     ArrayQ<T> integrandPDE[], int nrsdPDE ) const
    {
      typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

      const int nDOF = qfldElem_.nDOF();             // total solution DOFs in left element

      SANS_ASSERT(nrsdPDE == nDOF);

      VectorX X;
      VectorX nL, nR;           // unit normal for left element (points to right element)

      Real phi[nDOF];                                               // basis
      std::vector< DLA::VectorS< PhysDim::D, Real> > gradphi(nDOF); // basis gradient

      ArrayQ<T> q;
      VectorArrayQ gradq;  // gradients

      DLA::VectorS< PhysDim::D, Real > U; // Freestream Velocity

      QuadPointCellType sRefCell;

      TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyCell>::eval( canonicalTrace_, sRefTrace, sRefCell );

      // unit normals
      xfldElemTrace_.unitNormal( sRefTrace, nL );

      // basis value, gradient
      qfldElem_.evalBasis( sRefCell, phi, nDOF );
      xfldElem_.evalBasisGradient( sRefCell, qfldElem_, gradphi.data(), nDOF );

      xfldElem_.eval( sRefCell, X );

      // solution value, gradient
      qfldElem_.eval( sRefCell, q );
      qfldElem_.evalFromBasis( gradphi.data(), nDOF, gradq );

      for (int k = 0; k < nDOF; k++)
        integrandPDE[k] = 0;


      //pde_.freestream(U);
      U = pde_.Up(X);

      DLA::VectorS< PhysDim::D, Real > Us = U - dot(nL,U)*nL; // Tangential velocity


      // Compute the surface tangent gradient
      gradq -= dot(nL,gradq)*nL;

      for (int k = 0; k < nDOF; k++)
        integrandPDE[k] += phi[k]*dot(Us,gradq);
    }

  protected:
    const PDE& pde_;
    const ElementXFieldFrame& xfldElemFrame_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalFrame_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
  };

  template<class T, class TopoDimFrame, class TopologyFrame,
                    class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell>
  Functor<T, TopoDimFrame, TopologyFrame,
             TopoDimTrace, TopologyTrace,
             TopoDimCell, TopologyCell>
  integrand(const ElementXField<PhysDim, TopoDimFrame, TopologyFrame>& xfldElemFrame,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalFrame,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysDim, TopoDimCell , TopologyCell >& xfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyCell >& qfldElem ) const
  {
    return Functor<T, TopoDimFrame, TopologyFrame,
                      TopoDimTrace, TopologyTrace,
                      TopoDimCell, TopologyCell>(pde_, xfldElemFrame,
                                                 xfldElemTrace,
                                                 canonicalFrame,
                                                 canonicalTrace,
                                                 xfldElem, qfldElem );
  }

protected:
  const PDE& pde_;
  const std::vector<int> BoundaryFrameGroups_;
};

} //namespace SANS

#endif // INTEGRANDBOUNDARYFRAMETRACE_GALERKIN_H
