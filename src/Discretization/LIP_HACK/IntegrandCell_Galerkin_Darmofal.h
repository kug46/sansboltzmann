// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_GALERKIN_DARMOFAL_H
#define INTEGRANDCELL_GALERKIN_DARMOFAL_H

// cell integrand operator: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/Tuple.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: Galerkin

template <class PDE>
class IntegrandCell_Galerkin_Darmofal : public IntegrandCellType< IntegrandCell_Galerkin_Darmofal<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_Galerkin_Darmofal( const PDE& pde,
                                   const int dupPointOffset, const std::vector<int>& KuttaPoints )
    : dupPointOffset(dupPointOffset), KuttaPoints(KuttaPoints), pde_(pde) {}

  const int dupPointOffset;
  const std::vector<int> KuttaPoints;

  template<class T, class TopoDim, class Topology, class ElementParam>
  class Functor
  {
  public:
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    Functor( const PDE& pde,
             const ElementParam& paramfldElem,
             const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem ) :
             pde_(pde),
             qfldElem_(qfldElem), xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
             paramfldElem_( paramfldElem ) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }

    // cell element integrand
    void operator()( const QuadPointType& sRef, ArrayQ<T> integrand[], int neq ) const;

  protected:
    const PDE& pde_;
    const ElementQFieldType& qfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementParam& paramfldElem_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  Functor<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return Functor<T, TopoDim, Topology, ElementParam>(pde_, paramfldElem, qfldElem);
  }

protected:
  const PDE& pde_;
};


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_Galerkin_Darmofal<PDE>::Functor<T,TopoDim,Topology, ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}


// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,q)         solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_Galerkin_Darmofal<PDE>::Functor<T,TopoDim,Topology, ElementParam>::operator()(
    const QuadPointType& sRef, ArrayQ<T> integrand[], int neqn ) const
{
  const int nDOF = qfldElem_.nDOF();         // total solution DOFs in element

  SANS_ASSERT(neqn == nDOF);
  SANS_ASSERT(nDOF > 0); //Suppress clang analyzer warning

  std::vector<Real> phi(nDOF);             // basis
  std::vector< DLA::VectorS<PhysDim::D, Real> > gradphi(nDOF);            // basis gradient

  DLA::VectorS<PhysDim::D, ArrayQ<T> > gradq; // gradient

  DLA::VectorS< PhysDim::D, Real > U; // Freestream Velocity

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi.data(), nDOF );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi.data(), nDOF );

  // solution gradient
  qfldElem_.evalFromBasis( gradphi.data(), nDOF, gradq );

  pde_.freestream(U);

  ArrayQ<T> Ugradq = dot(U,gradq);

  for (int k = 0; k < nDOF; k++)
    integrand[k] = phi[k]*Ugradq;
}


}

#endif  // INTEGRANDCELL_GALERKIN_DARMOFAL_H
