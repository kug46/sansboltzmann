// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYFRAMETRACE_GALERKIN_H
#define RESIDUALBOUNDARYFRAMETRACE_GALERKIN_H

// boundary-frame integral functional

#include <memory>     // std::unique_ptr
#include <algorithm>  // std::find

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/Element.h"

#include "Field/XFieldVolume.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "IntegrandBoundaryFrameTrace_Galerkin.h"

namespace SANS
{


template <class TopologyFrame,
          class TopologyTraceL, class TopologyCellL,
          class TopologyTraceR, class TopologyCellR,
          class XFieldType,
          class ArrayQ,
          class PDE>
void
ResidualBoundaryFrameTrace_Galerkin_Group_Integral(
    const IntegrandBoundaryFrameTrace_Galerkin<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
    const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL>& xfldCellL,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL>& qfldCellL,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceR>& xfldTraceR,
    const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR>& xfldCellR,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR>& qfldCellR,
    int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL> XFieldCellGroupTypeL;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR> XFieldCellGroupTypeR;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR> QFieldCellGroupTypeR;

  typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
  typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

  typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceL> XFieldTraceGroupTypeL;
  typedef typename XFieldTraceGroupTypeL::template ElementType<> ElementXFieldTraceClassL;

  typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceR> XFieldTraceGroupTypeR;
  typedef typename XFieldTraceGroupTypeR::template ElementType<> ElementXFieldTraceClassR;

  typedef typename XFieldType::FieldFrameGroupType XFieldFrameGroupType;
  typedef typename XFieldFrameGroupType::template ElementType<> ElementXFieldFrameClass;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  ElementXFieldClassR xfldElemR( xfldCellR.basis() );
  ElementQFieldClassR qfldElemR( qfldCellR.basis() );

  ElementXFieldTraceClassL xfldElemTraceL( xfldTraceL.basis() );
  ElementXFieldTraceClassR xfldElemTraceR( xfldTraceR.basis() );

  ElementXFieldFrameClass xfldElemFrame( xfldFrame.basis() );

  SANS_ASSERT( qfldCellL.basis()->order() == 1 && qfldCellR.basis()->order() == 1 );

  // number of integrals evaluated per element
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  int nDOFF = xfldElemFrame.nDOF();

  // element-to-global DOF mapping
  std::unique_ptr<int[]> mapDOFGlobalL( new int[nIntegrandL] );
  std::unique_ptr<int[]> mapDOFGlobalR( new int[nIntegrandR] );

  std::vector<int> mapDOFGlobalF( nDOFF );

  // trace element integrals
  GalerkinWeightedIntegral<TopoD2, TopologyTraceL, ArrayQ> integralL(quadratureorder, nIntegrandL);
  GalerkinWeightedIntegral<TopoD2, TopologyTraceR, ArrayQ> integralR(quadratureorder, nIntegrandR);

  // element integrand/residuals
  std::unique_ptr<ArrayQ[]> rsdPDEElemL( new ArrayQ[nIntegrandL] );
  std::unique_ptr<ArrayQ[]> rsdPDEElemR( new ArrayQ[nIntegrandR] );

  // loop over elements within group
  int nelem = xfldFrame.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    xfldFrame.getElement(xfldElemFrame, elem);

    const int elemTraceL = xfldFrame.getElementLeft( elem );
    const int elemTraceR = xfldFrame.getElementRight( elem );
    CanonicalTraceToCell& canonicalFrameL  = xfldFrame.getCanonicalTraceLeft( elem );
    CanonicalTraceToCell& canonicalFrameR  = xfldFrame.getCanonicalTraceRight( elem );

    xfldTraceL.getElement( xfldElemTraceL, elemTraceL );
    xfldTraceR.getElement( xfldElemTraceR, elemTraceR );

    CanonicalTraceToCell& canonicalTraceL = xfldTraceL.getCanonicalTraceLeft( elemTraceL );
    CanonicalTraceToCell& canonicalTraceR = xfldTraceR.getCanonicalTraceLeft( elemTraceR ); //Not a typo

    const int elemL = xfldTraceL.getElementLeft( elemTraceL );
    const int elemR = xfldTraceR.getElementLeft( elemTraceR ); //Not a typo

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldCellR.getElement( xfldElemR, elemR );
    qfldCellR.getElement( qfldElemR, elemR );

    for (int n = 0; n < nIntegrandL; n++)
      rsdPDEElemL[n] = 0;

    for (int n = 0; n < nIntegrandR; n++)
      rsdPDEElemR[n] = 0;

    integralL(
        fcn.integrand(xfldElemFrame, xfldElemTraceL,
                      canonicalFrameL,
                      canonicalTraceL,
                      xfldElemL, qfldElemL ),
        xfldElemTraceL,
        rsdPDEElemL.get(), nIntegrandL );

    integralR(
        fcn.integrand(xfldElemFrame, xfldElemTraceR,
                      canonicalFrameR,
                      canonicalTraceR,
                      xfldElemR, qfldElemR ),
        xfldElemTraceR,
        rsdPDEElemR.get(), nIntegrandR );

    // scatter-add element residuals to global
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.get(), nIntegrandL );
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.get(), nIntegrandR );

    xfldFrame.associativity( elem ).getGlobalMapping( &mapDOFGlobalF[0], nDOFF );

    int nGlobal;
    for (int n = 0; n < nIntegrandL; n++)
    {
      nGlobal = mapDOFGlobalL[n];
      if (std::find(mapDOFGlobalF.begin(), mapDOFGlobalF.end(), nGlobal) == mapDOFGlobalF.end()) continue;
      rsdPDEGlobal[nGlobal] += rsdPDEElemL[n];
    }

    for (int n = 0; n < nIntegrandR; n++)
    {
      nGlobal = mapDOFGlobalR[n];
      if (std::find(mapDOFGlobalF.begin(), mapDOFGlobalF.end(), nGlobal) == mapDOFGlobalF.end()) continue;
      rsdPDEGlobal[nGlobal] += rsdPDEElemR[n];
    }

  }
}



template <class TopologyFrame,
          class TopologyTraceL, class TopologyCellL,
          class TopologyTraceR, class TopologyCellR,
          class XFieldType,
          class PDE,
          class ArrayQ>
static void
ResidualBoundaryFrameTrace_Galerkin_CellGroups(
    const IntegrandBoundaryFrameTrace_Galerkin<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceR>& xfldTraceR,
    const Field<PhysD3, TopoD3, ArrayQ>& qfld,
    const int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
{
  const XField<PhysD3, TopoD3>& xfld = qfld.getXField();

  int groupCellL = xfldTraceL.getGroupLeft();
  int groupCellR = xfldTraceR.getGroupLeft(); // This is not a typo as xfldTraceR is a BC field

  // Integrate over the trace group
  ResidualBoundaryFrameTrace_Galerkin_Group_Integral<TopologyFrame,
                                                TopologyTraceL, TopologyCellL,
                                                TopologyTraceR, TopologyCellR, XFieldType, ArrayQ>( fcn, xfldFrame,
                    xfldTraceL, xfld.template getCellGroup<TopologyCellL>(groupCellL), qfld.template getCellGroup<TopologyCellL>(groupCellL),
                    xfldTraceR, xfld.template getCellGroup<TopologyCellR>(groupCellR), qfld.template getCellGroup<TopologyCellR>(groupCellR),
                    quadratureorder,
                    rsdPDEGlobal );
}


//----------------------------------------------------------------------------//
// Residual Boundary Frame
//
template<class TopDim>
class ResidualBoundaryFrameTrace_Galerkin;

template<>
class ResidualBoundaryFrameTrace_Galerkin<TopoD3>
{
protected:
  typedef TopoD3 TopoDim;

  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyTraceL, class TopologyCellL,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellRight_Triangle(
      const IntegrandBoundaryFrameTrace_Galerkin<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTraceR,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      ResidualBoundaryFrameTrace_Galerkin_CellGroups<TopologyFrame, TopologyTraceL, TopologyCellL, Triangle, Tet, XFieldType>(
          fcn, xfldFrame, xfldTraceL, xfldTraceR, qfld,
          quadratureorder,
          rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyTraceL, class TopologyCellL,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellRight_Quad(
      const IntegrandBoundaryFrameTrace_Galerkin<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTraceR,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      ResidualBoundaryFrameTrace_Galerkin_CellGroups<TopologyFrame, TopologyTraceL, TopologyCellL, Quad, Hex, XFieldType>(
          fcn, xfldFrame, xfldTraceL, xfldTraceR, qfld,
          quadratureorder,
          rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyTraceL, class TopologyCellL,
            class XFieldType, class PDE,
            class PhysDim, class ArrayQ>
  static void
  TraceTopologyRight(
      const IntegrandBoundaryFrameTrace_Galerkin<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
  {
    const int boundaryGroupR = xfldFrame.getGroupRight();

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // get the boundary group
    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Triangle) )
    {
      // dispatch to determine left topology
      TopologyCellRight_Triangle<TopologyFrame, TopologyTraceL, TopologyCellL, XFieldType>(
          fcn, xfldFrame,
          xfldTraceL,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupR),
          qfld,
          quadratureorder,
          rsdPDEGlobal );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Quad) )
    {
      // dispatch to determine left topology
      TopologyCellRight_Quad<TopologyFrame, TopologyTraceL, TopologyCellL, XFieldType>(
          fcn, xfldFrame,
          xfldTraceL,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupR),
          qfld,
          quadratureorder,
          rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellLeft_Triangle(
      const IntegrandBoundaryFrameTrace_Galerkin<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTraceL,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      TraceTopologyRight<TopologyFrame, Triangle, Tet, XFieldType>(
          fcn, xfldFrame, xfldTraceL, qfld,
          quadratureorder,
          rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellLeft_Quad(
      const IntegrandBoundaryFrameTrace_Galerkin<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTraceL,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      TraceTopologyRight<TopologyFrame, Quad, Hex, XFieldType>(
          fcn, xfldFrame, xfldTraceL, qfld,
          quadratureorder,
          rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class XFieldType, class PDE,
            class PhysDim, class ArrayQ>
  static void
  TraceTopologyLeft(
      const IntegrandBoundaryFrameTrace_Galerkin<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
  {
    const int boundaryGroupL = xfldFrame.getGroupLeft();

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // get the boundary group
    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Triangle) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Triangle<TopologyFrame, XFieldType>(
          fcn, xfldFrame,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupL),
          qfld,
          quadratureorder,
          rsdPDEGlobal );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Quad) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Quad<TopologyFrame, XFieldType>(
          fcn, xfldFrame,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupL),
          qfld,
          quadratureorder,
          rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class XFieldType, class PDE,
            class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandBoundaryFrameTrace_Galerkin<PDE>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );

    SANS_ASSERT( ngroup == xfld.nBoundaryFrameGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryFrameGroups(); group++)
    {
      const int boundaryFrameGroup = fcn.boundaryFrameGroup(group);

      TraceTopologyLeft<Line, XFieldType>( fcn,
          xfld.getBoundaryFrameGroup(boundaryFrameGroup),
          qfld,
          quadratureorder[boundaryFrameGroup], rsdPDEGlobal );

    }
  }
};

}

#endif  // RESIDUALBOUNDARYFRAMETRACE_GALERKIN_H
