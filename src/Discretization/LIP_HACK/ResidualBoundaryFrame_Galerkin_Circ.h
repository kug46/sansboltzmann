// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYFRAME_GALERKIN_CIRC_H
#define RESIDUALBOUNDARYFRAME_GALERKIN_CIRC_H

// boundary-frame integral functional

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/Element.h"

#include "Field/XFieldVolume.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "IntegrandBoundaryFrame_Galerkin_Circ.h"

#include "Field/Field_DG_BoundaryFrame.h"

namespace SANS
{


template <class TopologyFrame,
          class TopologyTraceL, class TopologyCellL,
          class TopologyTraceR, class TopologyCellR,
          class XFieldType, class FieldFrameType,
          class ArrayQ,
          class PDE>
void
ResidualBoundaryFrame_Galerkin_Circ_Group_Integral(
    const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
    const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL>& xfldCellL,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL>& qfldCellL,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceR>& xfldTraceR,
    const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR>& xfldCellR,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR>& qfldCellR,
    int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal, SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL> XFieldCellGroupTypeL;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR> XFieldCellGroupTypeR;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR> QFieldCellGroupTypeR;

  typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
  typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

  typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceL> XFieldTraceGroupTypeL;
  typedef typename XFieldTraceGroupTypeL::template ElementType<> ElementXFieldTraceClassL;

  typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceR> XFieldTraceGroupTypeR;
  typedef typename XFieldTraceGroupTypeR::template ElementType<> ElementXFieldTraceClassR;

  typedef typename XFieldType::FieldFrameGroupType XFieldFrameGroupType;
  typedef typename Field_DG_BoundaryFrame<PhysD3, TopoD3, ArrayQ>::FieldFrameGroupType GFieldFrameGroupType;
  typedef typename XFieldFrameGroupType::template ElementType<> ElementXFieldFrameClass;
  typedef typename GFieldFrameGroupType::template ElementType<> ElementGFieldFrameClass;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  ElementXFieldClassR xfldElemR( xfldCellR.basis() );
  ElementQFieldClassR qfldElemR( qfldCellR.basis() );

  ElementXFieldTraceClassL xfldElemTraceL( xfldTraceL.basis() );
  ElementXFieldTraceClassR xfldElemTraceR( xfldTraceR.basis() );

  ElementXFieldFrameClass xfldElemFrame( xfldFrame.basis() );
  ElementGFieldFrameClass GfldElemFrame( GfldFrame.basis() );

  // number of integrals evaluated per element
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  int nIntegrandK = GfldElemFrame.nDOF();

  // element-to-global DOF mapping
  std::unique_ptr<int[]> mapDOFGlobalL( new int[nIntegrandL] );
  std::unique_ptr<int[]> mapDOFGlobalR( new int[nIntegrandR] );
  std::unique_ptr<int[]> mapDOFGlobalK( new int[nIntegrandK] );

  // frame element integral
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR, nIntegrandK);

  // element integrand/residuals
  std::unique_ptr<ArrayQ[]> rsdPDEElemL( new ArrayQ[nIntegrandL] );
  std::unique_ptr<ArrayQ[]> rsdPDEElemR( new ArrayQ[nIntegrandR] );
  std::unique_ptr<ArrayQ[]> rsdPDEElemK( new ArrayQ[nIntegrandK] );

  // loop over elements within group
  int nelem = xfldFrame.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    xfldFrame.getElement(xfldElemFrame, elem);
    GfldFrame.getElement(GfldElemFrame, elem);

    const int elemTraceL = xfldFrame.getElementLeft( elem );
    const int elemTraceR = xfldFrame.getElementRight( elem );
    CanonicalTraceToCell& canonicalFrameL  = xfldFrame.getCanonicalTraceLeft( elem );
    CanonicalTraceToCell& canonicalFrameR  = xfldFrame.getCanonicalTraceRight( elem );

    xfldTraceL.getElement( xfldElemTraceL, elemTraceL );
    xfldTraceR.getElement( xfldElemTraceR, elemTraceR );

    CanonicalTraceToCell& canonicalTraceL = xfldTraceL.getCanonicalTraceLeft( elemTraceL );
    CanonicalTraceToCell& canonicalTraceR = xfldTraceR.getCanonicalTraceLeft( elemTraceR ); //Not a typo

    const int elemL = xfldTraceL.getElementLeft( elemTraceL );
    const int elemR = xfldTraceR.getElementLeft( elemTraceR ); //Not a typo

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldCellR.getElement( xfldElemR, elemR );
    qfldCellR.getElement( qfldElemR, elemR );

    for (int n = 0; n < nIntegrandL; n++)
      rsdPDEElemL[n] = 0;

    for (int n = 0; n < nIntegrandR; n++)
      rsdPDEElemR[n] = 0;

    for (int n = 0; n < nIntegrandK; n++)
      rsdPDEElemK[n] = 0;

    integral(
        fcn.integrand(xfldElemFrame, GfldElemFrame, xfldElemTraceL, xfldElemTraceR,
                      canonicalFrameL, canonicalFrameR,
                      canonicalTraceL, canonicalTraceR,
                      xfldElemL, qfldElemL,
                      xfldElemR, qfldElemR ),
        xfldElemFrame,
        rsdPDEElemL.get(), nIntegrandL,
        rsdPDEElemR.get(), nIntegrandR,
        rsdPDEElemK.get(), nIntegrandK );

    // scatter-add element residuals to global
    GfldFrame.associativity( elem ).getGlobalMapping( mapDOFGlobalK.get(), nIntegrandK );
    qfldCellR.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.get(), nIntegrandL );
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.get(), nIntegrandR );

    int nGlobal;
    for (int n = 0; n < nIntegrandL; n++)
    {
      nGlobal = mapDOFGlobalL[n];
      SANS_ASSERT( nGlobal < fcn.dupPointOffset );
      rsdPDEGlobal[nGlobal] += rsdPDEElemL[n];
    }

    for (int n = 0; n < nIntegrandR; n++)
    {
      nGlobal = mapDOFGlobalR[n];
      if (nGlobal < fcn.dupPointOffset) continue;
      rsdPDEGlobal[nGlobal] += rsdPDEElemR[n];
    }

    for (int n = 0; n < nIntegrandK; n++)
    {
      nGlobal = mapDOFGlobalK[n];
      rsdKuttaGlobal[nGlobal] += rsdPDEElemK[n];
    }

  }
}



template <class TopologyFrame,
          class TopologyTraceL, class TopologyCellL,
          class TopologyTraceR, class TopologyCellR,
          class XFieldType, class FieldFrameType,
          class PDE,
          class ArrayQ>
static void
ResidualBoundaryFrame_Galerkin_Circ_CellGroups(
    const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTraceR>& xfldTraceR,
    const Field<PhysD3, TopoD3, ArrayQ>& qfld,
    const int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal, SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
{
  const XField<PhysD3, TopoD3>& xfld = qfld.getXField();

  int groupCellL = xfldTraceL.getGroupLeft();
  int groupCellR = xfldTraceR.getGroupLeft(); // This is not a typo as xfldTraceR is a BC field

  // Integrate over the trace group
  ResidualBoundaryFrame_Galerkin_Circ_Group_Integral<TopologyFrame,
                                                TopologyTraceL, TopologyCellL,
                                                TopologyTraceR, TopologyCellR, XFieldType, FieldFrameType, ArrayQ>( fcn, xfldFrame, GfldFrame,
                    xfldTraceL, xfld.template getCellGroup<TopologyCellL>(groupCellL), qfld.template getCellGroup<TopologyCellL>(groupCellL),
                    xfldTraceR, xfld.template getCellGroup<TopologyCellR>(groupCellR), qfld.template getCellGroup<TopologyCellR>(groupCellR),
                    quadratureorder,
                    rsdPDEGlobal, rsdKuttaGlobal );
}


//----------------------------------------------------------------------------//
// Residual Boundary Frame
//
template<class TopDim>
class ResidualBoundaryFrame_Galerkin_Circ;

template<>
class ResidualBoundaryFrame_Galerkin_Circ<TopoD3>
{
protected:
  typedef TopoD3 TopoDim;

  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyTraceL, class TopologyCellL,
            class XFieldType, class FieldFrameType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellRight_Triangle(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTraceR,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal, SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      ResidualBoundaryFrame_Galerkin_Circ_CellGroups<TopologyFrame, TopologyTraceL, TopologyCellL, Triangle, Tet, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame, xfldTraceL, xfldTraceR, qfld,
          quadratureorder,
          rsdPDEGlobal, rsdKuttaGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyTraceL, class TopologyCellL,
            class XFieldType, class FieldFrameType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellRight_Quad(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTraceR,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal, SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      ResidualBoundaryFrame_Galerkin_Circ_CellGroups<TopologyFrame, TopologyTraceL, TopologyCellL, Quad, Hex, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame, xfldTraceL, xfldTraceR, qfld,
          quadratureorder,
          rsdPDEGlobal, rsdKuttaGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyTraceL, class TopologyCellL,
            class XFieldType, class FieldFrameType, class PDE,
            class PhysDim, class ArrayQ>
  static void
  TraceTopologyRight(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTraceL>& xfldTraceL,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal, SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
  {
    const int boundaryGroupR = xfldFrame.getGroupRight();

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // get the boundary group
    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Triangle) )
    {
      // dispatch to determine left topology
      TopologyCellRight_Triangle<TopologyFrame, TopologyTraceL, TopologyCellL, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame,
          xfldTraceL,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupR),
          qfld,
          quadratureorder,
          rsdPDEGlobal, rsdKuttaGlobal );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Quad) )
    {
      // dispatch to determine left topology
      TopologyCellRight_Quad<TopologyFrame, TopologyTraceL, TopologyCellL, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame,
          xfldTraceL,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupR),
          qfld,
          quadratureorder,
          rsdPDEGlobal, rsdKuttaGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType, class FieldFrameType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellLeft_Triangle(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTraceL,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal, SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      TraceTopologyRight<TopologyFrame, Triangle, Tet, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame, xfldTraceL, qfld,
          quadratureorder,
          rsdPDEGlobal, rsdKuttaGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType, class FieldFrameType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellLeft_Quad(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTraceL,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal, SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
  {

    // determine topology for L
    int groupL = xfldTraceL.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      TraceTopologyRight<TopologyFrame, Quad, Hex, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame, xfldTraceL, qfld,
          quadratureorder,
          rsdPDEGlobal, rsdKuttaGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class XFieldType, class FieldFrameType, class PDE,
            class PhysDim, class ArrayQ>
  static void
  TraceTopologyLeft(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename FieldFrameType::FieldFrameGroupType& GfldFrame,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal, SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
  {
    const int boundaryGroupL = xfldFrame.getGroupLeft();

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // get the boundary group
    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Triangle) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Triangle<TopologyFrame, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupL),
          qfld,
          quadratureorder,
          rsdPDEGlobal, rsdKuttaGlobal );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Quad) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Quad<TopologyFrame, XFieldType, FieldFrameType>(
          fcn, xfldFrame, GfldFrame,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupL),
          qfld,
          quadratureorder,
          rsdPDEGlobal, rsdKuttaGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_Galerkin<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class XFieldType, class FieldFrameType, class PDE,
            class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandBoundaryFrame_Galerkin_Circ<PDE>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldFrameType& Gfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal, SLA::SparseVector<ArrayQ>& rsdKuttaGlobal )
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );

    SANS_ASSERT( ngroup == xfld.nBoundaryFrameGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryFrameGroups(); group++)
    {
      const int boundaryFrameGroup = fcn.boundaryFrameGroup(group);

      TraceTopologyLeft<Line, XFieldType, FieldFrameType>( fcn,
          xfld.getBoundaryFrameGroup(boundaryFrameGroup),
          Gfld.getBoundaryFrameGroupGlobal(boundaryFrameGroup),
          qfld,
          quadratureorder[boundaryFrameGroup], rsdPDEGlobal, rsdKuttaGlobal );

    }
  }
};

}

#endif  // RESIDUALBOUNDARYFRAME_GALERKIN_CIRC_H
