// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_GALERKIN_WAKECUT_LIP_SRA_H
#define INTEGRANDBOUNDARYTRACE_GALERKIN_WAKECUT_LIP_SRA_H

// trace integrand operators: Galerkin sansLG for 2D wake-cut

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Trace integrand: Galerkin sansLG

struct BC_WakeCut_LIP_SRA_Params : noncopyable
{
  const ParameterNumeric<Real> circ{"circ", NO_DEFAULT, NO_RANGE, "Vortex circulation"};

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.circ));
    d.checkUnknownInputs(allParams);
  }

  static constexpr const char* BCName{"WakeCut_LIP_SRA"};
  struct Option
  {
    const DictOption WakeCut_LIP_SRA{BC_WakeCut_LIP_SRA_Params::BCName, BC_WakeCut_LIP_SRA_Params::checkInputs};
  };

  static BC_WakeCut_LIP_SRA_Params params;
};


class BC_WakeCut_LIP_SRA
  : public BCType< BC_WakeCut_LIP_SRA >
{
public:
  typedef BCCategory::WakeCut_Potential_Drela Category;
  typedef BC_WakeCut_LIP_SRA_Params ParamsType;

  typedef PhysD2 PhysDim;   // HARDCODED TO 2D FOR NOW
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 2;                   // total BCs

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using MatrixQ = T;    // matrices

  explicit BC_WakeCut_LIP_SRA( PyDict& d ) : circ_(d.get(ParamsType::params.circ)) {}
  explicit BC_WakeCut_LIP_SRA( const Real& circ ) : circ_(circ) {}

  BC_WakeCut_LIP_SRA( const BC_WakeCut_LIP_SRA& ) = delete;
  BC_WakeCut_LIP_SRA& operator=( const BC_WakeCut_LIP_SRA& ) = delete;

  Real getCirculation() const { return circ_; }

private:
  Real circ_;         // vortex strength/circulation
};


template <class PDE, class NDBCVector>
class IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin > :
  public IntegrandInteriorTraceType<IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin> >
{
public:
  typedef BC_WakeCut_LIP_SRA BC;
  typedef BCCategory::WakeCut_Potential_Drela Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace( const PDE& pde, const BC_WakeCut_LIP_SRA& bc, const std::vector<int>& BoundaryGroups )
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryTraceGroups() const { return 0; }
  std::size_t boundaryTraceGroup(const int n) const { return -1; }
  std::vector<int> getBoundaryTraceGroups() const { return dummy_; }

  std::size_t nPeriodicTraceGroups() const { return BoundaryGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getPeriodicTraceGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const PDE& pde,
             const BC_WakeCut_LIP_SRA& bc,
             const ElementXFieldTrace& xfldElemTrace,
             const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldL& xfldElemL,
             const ElementQFieldL& qfldElemL,
             const ElementXFieldR& xfldElemR,
             const ElementQFieldR& qfldElemR ) :
             pde_(pde), bc_(bc),
             xfldElemTrace_(xfldElemTrace),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR) {}


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int nrsdL,
                                                     ArrayQ<Ti> integrandR[], const int nrsdR ) const;

  protected:
    const PDE& pde_;
    const BC_WakeCut_LIP_SRA& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL, TopologyR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell , TopologyL    >& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElemL,
            const ElementXField<PhysDim, TopoDimCell , TopologyR    >& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyR    >& qfldElemR) const
  {
    return {pde_, bc_,
            xfldElemTrace,
            canonicalTraceL, canonicalTraceR,
            xfldElemL, qfldElemL,
            xfldElemR, qfldElemR};
  }

protected:
  const PDE& pde_;
  const BC_WakeCut_LIP_SRA& bc_;
  const std::vector<int> BoundaryGroups_;
  const std::vector<int> dummy_;
};


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
bool
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin>::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::needsEvaluation() const
{
  return pde_.hasFluxViscous();
}


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin>::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::operator()(
    const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandL[], const int nrsdL,
                                        ArrayQ<Ti> integrandR[], const int nrsdR ) const
{
  typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

  const int nDOFL = qfldElemL_.nDOF();             // total solution DOFs in left element
  const int nDOFR = qfldElemR_.nDOF();             // total solution DOFs in right element

  SANS_ASSERT(nrsdL == nDOFL);
  SANS_ASSERT(nrsdR == nDOFR);

  VectorX X = 0;
  VectorX nL, nR;           // unit normal for left element (points to right element)

  Real psiL[nDOFL];                                                // basis (left)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradpsiL(nDOFL);  // basis gradient (left)
  Real psiR[nDOFR];                                                // basis (right)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradpsiR(nDOFR);  // basis gradient (right)

  ArrayQ<T> qL, qR;             // solution
  VectorArrayQ gradqL, gradqR;  // gradient

  QuadPointCellType sRefL;      // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, RefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, RefTrace, sRefR );

  // physical coordinates
  xfldElemTrace_.coordinates( RefTrace, X );

  // unit normal: L points to R
  xfldElemTrace_.unitNormal( RefTrace, nL );
  nR = -nL;

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, psiL, nDOFL );
  qfldElemR_.evalBasis( sRefR, psiR, nDOFR );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradpsiL.data(), nDOFL );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradpsiR.data(), nDOFR );

  // solution value, gradient
  qfldElemL_.evalFromBasis( psiL, nDOFL, qL );
  qfldElemR_.evalFromBasis( psiR, nDOFR, qR );
  qfldElemL_.evalFromBasis( gradpsiL.data(), nDOFL, gradqL );
  qfldElemR_.evalFromBasis( gradpsiR.data(), nDOFR, gradqR );

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] = 0;

  // PDE residual: weak form boundary integral

  ArrayQ<T> qnL = dot(nL, gradqL);
  ArrayQ<T> qnR = dot(nR, gradqR);

  ArrayQ<T> qnSum = 0.5*(qnR + qnL);
  ArrayQ<T> qnDif = 0.5*(qnR - qnL);

  for (int k = 0; k < nDOFL; k++)
  {
    integrandL[k] += -psiL[k]*(qnSum - qnDif);
  }

  for (int k = 0; k < nDOFR; k++)
  {
    integrandR[k] += -psiR[k]*(qnSum + qnDif);
  }

  // PDE residual: sans-Lagrange BC terms

  Real circ = bc_.getCirculation();

  ArrayQ<T> rsdMass = 0.5*(qnR + qnL);          // mass flux BC
  ArrayQ<T> rsdCirc = 0.5*((qR - qL) + circ);   // potential jump BC

  for (int k = 0; k < nDOFL; k++)
  {
    integrandL[k] += psiL[k]*rsdMass - dot(nL, gradpsiL[k])*(-rsdCirc);
  }

  for (int k = 0; k < nDOFR; k++)
  {
    integrandR[k] += psiR[k]*rsdMass - dot(nR, gradpsiR[k])*(+rsdCirc);
  }

#undef USE_NITSCHE
#ifdef USE_NITSCHE
  // edge length
  Real ds = xfldElemTrace_.jacobianDeterminant();

  // adjacent element area
  Real areaL = xfldElemL_.jacobianDeterminant();
  Real areaR = xfldElemR_.jacobianDeterminant();

  // normal grid scale
  Real h = 0.5*(areaL + areaR)/ds;

  Real beta1  = 10;   // Nitsche parameters
  Real gamma2 = 10;

  for (int k = 0; k < nDOFL; k++)
  {
    integrandL[k] += (beta1/(4*h))*(-psiL[k])*rsdCirc
                   + (gamma2*h)*dot(nL, gradpsiL[k])*rsdMass;
  }

  for (int k = 0; k < nDOFR; k++)
  {
    integrandR[k] += (beta1/(4*h))*(+psiR[k])*rsdCirc
                   + (gamma2*h)*dot(nR, gradpsiR[k])*rsdMass;
  }
#endif

}

}   // namespace SANS

#endif  // INTEGRANDBOUNDARYTRACE_GALERKIN_WAKECUT_LIP_SRA_H
