// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFUNCTOR_LINEARSCALAR_SANSLG_FP_HACK_H
#define INTEGRANDBOUNDARYFUNCTOR_LINEARSCALAR_SANSLG_FP_HACK_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"
#include "pde/FullPotential/PDEFullPotential3D.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "Discretization/Integrand_Type.h"

#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"
#include "Discretization/Galerkin/Stabilization_Nitsche.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE

template <template<class,class> class NDConvert, class Tg, class NDBCVector>
class IntegrandBoundaryTrace<NDConvert<PhysD3,PDEFullPotential3D<Tg>>,
                             NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG_FP_HACK>, Galerkin> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<NDConvert<PhysD3,PDEFullPotential3D<Tg>>,
                                                              NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG_FP_HACK>, Galerkin> >
{
public:
  typedef NDConvert<PhysD3,PDEFullPotential3D<Tg>> PDE;
  typedef BCCategory::LinearScalar_sansLG_FP_HACK Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  static const int D = PhysDim::D;
  static const int N = PDE::N;

  IntegrandBoundaryTrace( const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups,
                          const StabilizationNitsche& stab)
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldL& qfldElem ) :
                     pde_(pde), bc_(bc),
                     xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem),
                     paramfldElem_( paramfldElem )
    {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFElem() const { return qfldElem_.nDOF(); }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandPDE[], int neqn ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandPDE, neqn);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandPDE[], int neqn ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
    const ElementParam& paramfldElem_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParam>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElem) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParam>(pde_, bc_,
                                                                                               xfldElemTrace, canonicalTrace,
                                                                                               paramfldElem, qfldElem);
  }
private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
};


template <template<class,class> class NDConvert, class Tg, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class ElementParam>
template <class BC, class Ti>
void
IntegrandBoundaryTrace<NDConvert<PhysD3,PDEFullPotential3D<Tg>>, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG_FP_HACK>, Galerkin>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,TopologyL,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandPDE[], int nrsdPDE ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;

  typedef DLA::VectorS< PhysDim::D, Ts > VectorT;

  const int nDOFElem  = qfldElem_.nDOF();    // total solution DOFs in area-element

  SANS_ASSERT( (nrsdPDE == nDOFElem) );

  VectorX X;                // physical coordinates
  VectorX N;                // unit normal (points out of domain)

  std::vector<Real> phi(nDOFElem);                                   // solution basis
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradphi(nDOFElem);  // basis gradient (left)

  ArrayQ<T> q;              // solution
  VectorArrayQ<T> gradq;    // gradient
  DLA::VectorS<D, T> gradPhi;

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  ParamT param;             // Elemental parameters (such as grid coordinates and distance functions)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, param );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, N );
  //traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, N);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi.data(), nDOFElem );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi.data(), nDOFElem );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi.data(), nDOFElem, q );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradphi.data(), nDOFElem, gradq );
  else
    gradq = 0;

  for (int d = 0; d < PhysDim::D; d++)
    gradPhi[d] = gradq[d][0];

  for (int k = 0; k < nDOFElem; k++)
    integrandPDE[k] = 0;

  // PDE residual: weak form boundary integral

  if ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() )
  {
    VectorArrayQ<Ti> F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( param, q, gradq, F );

    ArrayQ<Ti> Fn = dot(N,F);
    for (int k = 0; k < nDOFElem; k++)
      integrandPDE[k] += phi[k]*Fn;
  }

  // PDE residual: BC
  // NOTE: only works for FP equations

  // scalar BC coefficients
  MatrixQ<Real> Am, Bm;
  bc.coefficients( get<-1>(param), N, Am, Bm );
  Real A = Am(0,0);
  Real B = Bm(0,0);

  // scalar BC data
  ArrayQ<Tg> bcdatam;
  bc.data( get<-1>(param), N, bcdatam );
  Tg bcdata = bcdatam[0];

  DLA::VectorS< PhysDim::D, Tg > U = pde_.freestream();

  //const T& rho = q[1];
  Ts rho = pde_.density(q);

  // Dirichlet inflow
  if (A == 1)
  {
    VectorT V = gradPhi + U;
    Ts Vn = dot(N,V);

    Ts rho_sh = pde_.density_sh(gradq);
    Ts h_sh = pde_.enthalpy_sh(gradq);

    Ts tmp_sh = rho_sh/h_sh*Vn;
    for (int k = 0; k < nDOFElem; k++)
    {
      integrandPDE[k][0] -= dot(gradphi[k],N)*rho*q[0];
      integrandPDE[k][1] -= phi[k]*tmp_sh*q[0];
    }
  }
  // Inflow and Wall
  else if (B == 1)
  {
    T Phin = dot(N,gradPhi);

    for (int k = 0; k < nDOFElem; k++)
      integrandPDE[k][0] += phi[k]*rho*(Phin - bcdata);

  }
  else
    SANS_DEVELOPER_EXCEPTION("A or B must be 1!");
}

}

#endif  // INTEGRANDBOUNDARYFUNCTOR_LINEARSCALAR_SANSLG_FP_HACK_H
