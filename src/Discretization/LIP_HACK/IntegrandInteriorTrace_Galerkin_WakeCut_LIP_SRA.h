// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDINTERIORTRACE_GALERKIN_WAKECUT_LIP_SRA_H
#define INTEGRANDINTERIORTRACE_GALERKIN_WAKECUT_LIP_SRA_H

// interior/periodic-trace integrand operators: Galerkin sansLG for 2D wake-cut

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"
#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// wake-cut BC

struct BCInt_WakeCut_LIP_SRA_Params : noncopyable
{
  const ParameterNumeric<Real> circ{"circ", NO_DEFAULT, NO_RANGE, "Vortex circulation"};

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.circ));
    d.checkUnknownInputs(allParams);
  }

  static constexpr const char* BCName{"Int_WakeCut_LIP_SRA"};
  struct Option
  {
    const DictOption Int_WakeCut_LIP_SRA{BCInt_WakeCut_LIP_SRA_Params::BCName, BCInt_WakeCut_LIP_SRA_Params::checkInputs};
  };

  static BCInt_WakeCut_LIP_SRA_Params params;
};


class BCInt_WakeCut_LIP_SRA
  : public BCType< BCInt_WakeCut_LIP_SRA >
{
public:
  typedef BCCategory::WakeCut_Potential_Drela Category;
  typedef BCInt_WakeCut_LIP_SRA_Params ParamsType;

  typedef PhysD2 PhysDim;   // HARDCODED TO 2D FOR NOW
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 2;                   // total BCs

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using MatrixQ = T;    // matrices

  explicit BCInt_WakeCut_LIP_SRA( PyDict& d ) : circ_(d.get(ParamsType::params.circ)) {}
  explicit BCInt_WakeCut_LIP_SRA( const Real& circ ) : circ_(circ) {}

  BCInt_WakeCut_LIP_SRA( const BCInt_WakeCut_LIP_SRA& ) = delete;
  BCInt_WakeCut_LIP_SRA& operator=( const BCInt_WakeCut_LIP_SRA& ) = delete;

  Real getCirculation() const { return circ_; }

private:
  Real circ_;         // vortex strength/circulation
};


//----------------------------------------------------------------------------//
// interior/periodic-trace integrand: Galerkin sansLG for LIP

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin > :
  public IntegrandInteriorTraceType<IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  // cppcheck-suppress noExplicitConstructor
  IntegrandBoundaryTrace( const PDE& pde, const BCInt_WakeCut_LIP_SRA& bc, const std::vector<int>& interiorTraceGroups )
    : pde_(pde), bc_(bc), interiorTraceGroups_(interiorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return 0; }
  std::size_t interiorTraceGroup(const int n) const { return -1; }
  const std::vector<int>& interiorTraceGroups() const { return dummy_; }

  std::size_t nPeriodicTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  std::vector<int> getPeriodicTraceGroups() const { return interiorTraceGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                                        class ElementParamL, class ElementParamR>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldTypeL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldTypeR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldTypeL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldTypeR;

    typedef typename ElementXFieldTraceType::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_Galerkin<MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde,
                   const BCInt_WakeCut_LIP_SRA& bc,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter of tuple
                   const ElementQFieldTypeL& qfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldTypeR& qfldElemR ) :
      pde_(pde), bc_(bc),
      xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
      xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), paramfldElemL_( paramfldElemL ),
      xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), paramfldElemR_( paramfldElemR ),
      nDOFL_(qfldElemL_.nDOF()),
      nDOFR_(qfldElemR_.nDOF()),
      psiL_( new Real[nDOFL_] ),
      psiR_( new Real[nDOFR_] ),
      gradpsiL_( new VectorX[nDOFL_] ),
      gradpsiR_( new VectorX[nDOFR_] ) {}
    ~BasisWeighted()
    {
      delete [] psiL_;
      delete [] psiR_;

      delete [] gradpsiL_;
      delete [] gradpsiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int neqnL,
                                                     ArrayQ<Ti> integrandR[], const int neqnR ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemR ) const;

  protected:
    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const VectorX& nL,
                            const ParamTL& paramL, const ParamTR& paramR,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                            const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                            ArrayQ<Ti> integrandL[], const int neqnL,
                            ArrayQ<Ti> integrandR[], const int neqnR,
                            bool gradientOnly = false ) const;

    const PDE& pde_;
    const BCInt_WakeCut_LIP_SRA& bc_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldTypeL& xfldElemL_;
    const ElementQFieldTypeL& qfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldTypeR& xfldElemR_;
    const ElementQFieldTypeR& qfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *psiL_;          // basis (L/R)
    mutable Real *psiR_;
    mutable VectorX *gradpsiL_;   // basis gradient (L/R)
    mutable VectorX *gradpsiR_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL   > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR   > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldTypeR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    FieldWeighted( const PDE& pde,
                   const BCInt_WakeCut_LIP_SRA& bc,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL,     // XField must be the last parameter
                   const ElementQFieldL& qfldElemL,
                   const ElementQFieldL& wfldElemL,
                   const ElementParamR& paramfldElemR,
                   const ElementQFieldTypeR& qfldElemR,
                   const ElementQFieldTypeR& wfldElemR ) :
                   pde_(pde), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                   xfldElemL_(get<-1>(paramfldElemL)), qfldElemL_(qfldElemL), wfldElemL_(wfldElemL), paramfldElemL_( paramfldElemL ),
                   xfldElemR_(get<-1>(paramfldElemR)), qfldElemR_(qfldElemR), wfldElemR_(wfldElemR), paramfldElemR_( paramfldElemR ),
                   nDOFL_(qfldElemL_.nDOF()), nDOFR_(qfldElemR_.nDOF()) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRef, Ti& integrandL, Ti& integrandR ) const;

  protected:
    const PDE& pde_;
    const BCInt_WakeCut_LIP_SRA& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementQFieldL& wfldElemL_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldTypeR& qfldElemR_;
    const ElementQFieldTypeR& wfldElemR_;
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
  };


  //Factory function that returns the basis weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,      TopologyR,
                                  ElementParamL, ElementParamR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& qfldElemR) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTraceL, canonicalTraceR,
             paramfldElemL, qfldElemL,
             paramfldElemR, qfldElemR };
  }

  //Factory function that returns the field weighted integrand class
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell, TopologyL,     TopologyR,
                                ElementParamL, ElementParamR  >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL,
            const Element<ArrayQ<T>, TopoDimCell, TopologyL>& wfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& qfldElemR,
            const Element<ArrayQ<T>, TopoDimCell, TopologyR>& wfldElemR) const
  {
    return { pde_, bc_,
             xfldElemTrace, canonicalTraceL, canonicalTraceR,
             paramfldElemL, qfldElemL, wfldElemL,
             paramfldElemR, qfldElemR, wfldElemR };
  }

protected:
  const PDE& pde_;
  const BCInt_WakeCut_LIP_SRA& bc_;
  const std::vector<int> interiorTraceGroups_;
  const std::vector<int> dummy_;
};


template <class PDE, class NDBCVector>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
bool
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, TopologyR,
                 ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxViscous() );
}

template <class PDE, class NDBCVector>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
bool
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL, TopologyR,
                 ElementParamL, ElementParamR>::needsEvaluation() const
{
  return ( pde_.hasFluxViscous() );
}


template <class PDE, class NDBCVector>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], const int neqnL,
                                                 ArrayQ<Ti> integrandR[], const int neqnR ) const
{
  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, psiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, psiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradpsiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradpsiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( psiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( psiR_, nDOFR_, qR );
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradpsiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradpsiR_, nDOFR_, gradqR );
  }

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // compute the integrand
  weightedIntegrand( nL,
                     paramL, paramR,
                     qL, gradqL,
                     qR, gradqR,
                     integrandL, neqnL,
                     integrandR, neqnR );
}


//---------------------------------------------------------------------------//
template <class PDE, class NDBCVector>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin>::
BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell,
                 TopologyL,     TopologyR,
                 ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  ArrayQ<T> qL, qR;                // solutions
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, psiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, psiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradpsiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradpsiR_, nDOFR_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( psiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( psiR_, nDOFR_, qR );

  // unit normal: points to R
  VectorX nL;                      // unit normal for left element (points to right element)
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL, qSurrealR;                // solution
  VectorArrayQ<SurrealClass> gradqSurrealL, gradqSurrealR;  // gradient

  qSurrealL = qL;
  qSurrealR = qR;

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradpsiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradpsiR_, nDOFR_, gradqR );

    gradqSurrealL = gradqL;
    gradqSurrealR = gradqR;
  }

  MatrixQ<Real> PDE_q = 0, PDE_gradq = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandRSurreal( nDOFR_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL, paramR,
                       qSurrealL, gradqL,
                       qSurrealR, gradqR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += dJ*psiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemR.PDE_qL(i,j) += dJ*psiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemL.PDE_qR(i,j) += dJ*psiR_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemR.PDE_qR(i,j) += dJ*psiR_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk

  // loop over derivative chunks to computes derivatives w.r.t gradq
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*2*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandLSurreal = 0;
      integrandRSurreal = 0;

      // compute the integrand
      weightedIntegrand( nL,
                         paramL, paramR,
                         qL, gradqSurrealL,
                         qR, gradqSurrealR,
                         integrandLSurreal.data(), nDOFL_,
                         integrandRSurreal.data(), nDOFR_, true );

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDE_qL(i,j) += dJ*gradpsiL_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemR.PDE_qL(i,j) += dJ*gradpsiL_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }

      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(gradqSurrealR[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemL.PDE_qR(i,j) += dJ*gradpsiR_[j][d]*PDE_gradq;
          }

          for (int i = 0; i < nDOFR_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFR_; j++)
              mtxElemR.PDE_qR(i,j) += dJ*gradpsiR_[j][d]*PDE_gradq;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient
}


template <class PDE, class NDBCVector>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
template<class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin>::
BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell,
                 TopologyL,     TopologyR,
                 ElementParamL, ElementParamR>::
weightedIntegrand( const VectorX& nL,
                   const ParamTL& paramL, const ParamTR& paramR,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqL,
                   const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqR,
                   ArrayQ<Ti> integrandL[], const int neqnL,
                   ArrayQ<Ti> integrandR[], const int neqnR,
                   bool gradientOnly ) const
{
  VectorX nR = -nL;

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0;

  // PDE residual: weak form boundary integral

  ArrayQ<Tg> qnL = dot(nL, gradqL);
  ArrayQ<Tg> qnR = dot(nR, gradqR);

  ArrayQ<Tg> qnSum = 0.5*(qnR + qnL);
  ArrayQ<Tg> qnDif = 0.5*(qnR - qnL);

  for (int k = 0; k < nDOFL_; k++)
  {
    integrandL[k] += -psiL_[k]*(qnSum - qnDif);
  }

  for (int k = 0; k < nDOFR_; k++)
  {
    integrandR[k] += -psiR_[k]*(qnSum + qnDif);
  }

  // PDE residual: sans-Lagrange BC terms

  Real circ = bc_.getCirculation();

  ArrayQ<Tg> rsdMass = 0.5*(qnR + qnL);          // mass flux BC
  ArrayQ<Tq> rsdCirc = 0.5*((qR - qL) + circ);   // potential jump BC

  for (int k = 0; k < nDOFL_; k++)
  {
    integrandL[k] += psiL_[k]*rsdMass - dot(nL, gradpsiL_[k])*(-rsdCirc);
  }

  for (int k = 0; k < nDOFR_; k++)
  {
    integrandR[k] += psiR_[k]*rsdMass - dot(nR, gradpsiR_[k])*(+rsdCirc);
  }

#define USE_NITSCHE
#ifdef USE_NITSCHE
  // edge length
  Real ds = xfldElemTrace_.jacobianDeterminant();

  // adjacent element area
  Real areaL = xfldElemL_.jacobianDeterminant();
  Real areaR = xfldElemR_.jacobianDeterminant();

  // normal grid scale
  Real h = 0.5*(areaL + areaR)/ds;

  Real beta1  = 10;   // Nitsche parameters
  Real gamma2 = 10;

  for (int k = 0; k < nDOFL_; k++)
  {
    integrandL[k] += (beta1/(4*h))*(-psiL_[k])*rsdCirc + (gamma2*h)*dot(nL, gradpsiL_[k])*rsdMass;
  }

  for (int k = 0; k < nDOFR_; k++)
  {
    integrandR[k] += (beta1/(4*h))*(+psiR_[k])*rsdCirc + (gamma2*h)*dot(nR, gradpsiR_[k])*rsdMass;
  }
#endif
}


template <class PDE, class NDBCVector>
template< class T, class TopoDimTrace, class TopologyTrace, class TopoDimCell,
                   class TopologyL,     class TopologyR,
                   class ElementParamL, class ElementParamR >
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin>::
FieldWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell,
                 TopologyL,     TopologyR,
                 ElementParamL, ElementParamR>::
operator()(const QuadPointTraceType& sRefTrace, Ti& integrandL, Ti& integrandR ) const
{
  ParamTL paramL;   // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;

  VectorX nL;       // unit normal for left element (points to right element)
  VectorX nR;       //                              (points to left element)

  ArrayQ<T> qL, qR;                // solution
  VectorArrayQ<T> gradqL, gradqR;  // solution gradients
  VectorArrayQ<T> dqn;             // jump in q.n

  ArrayQ<T> wL, wR;                // weight
  VectorArrayQ<T> gradwL, gradwR;  // weight gradients

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: nL points to R; nR points to L
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);
  nR = -nL;

  // solution value, gradient
  qfldElemL_.eval( sRefL, qL );
  qfldElemR_.eval( sRefR, qR );
  if (pde_.hasFluxViscous() || pde_.hasSourceTrace())
  {
    xfldElemL_.evalGradient( sRefL, qfldElemL_, gradqL );
    xfldElemR_.evalGradient( sRefR, qfldElemR_, gradqR );
  }

  // weight value,
  wfldElemL_.eval( sRefL, wL );
  wfldElemR_.eval( sRefR, wR );
  if (pde_.hasFluxViscous())
  {
    xfldElemL_.evalGradient( sRefL, wfldElemL_, gradwL );
    xfldElemR_.evalGradient( sRefR, wfldElemR_, gradwR );
  }

  integrandL = 0;
  integrandR = 0;

  // PDE residual: weak form boundary integral

  ArrayQ<Ti> qnL = dot(nL, gradqL);
  ArrayQ<Ti> qnR = dot(nR, gradqR);

  ArrayQ<Ti> qnSum = 0.5*(qnR + qnL);
  ArrayQ<Ti> qnDif = 0.5*(qnR - qnL);

  integrandL += -wL*(qnSum - qnDif);
  integrandR += -wR*(qnSum + qnDif);

  // PDE residual: sans-Lagrange BC terms

  Real circ = bc_.getCirculation();

  ArrayQ<Ti> rsdMass = 0.5*(qnR + qnL);          // mass flux BC
  ArrayQ<Ti> rsdCirc = 0.5*((qR - qL) + circ);   // potential jump BC

  integrandL += wL*rsdMass - dot(nL, gradwL)*(-rsdCirc);
  integrandR += wR*rsdMass - dot(nR, gradwR)*(+rsdCirc);

#ifdef USE_NITSCHE
  // edge length
  Real ds = xfldElemTrace_.jacobianDeterminant();

  // adjacent element area
  Real areaL = xfldElemL_.jacobianDeterminant();
  Real areaR = xfldElemR_.jacobianDeterminant();

  // normal grid scale
  Real h = 0.5*(areaL + areaR)/ds;

  Real beta1  = 10;   // Nitsche parameters
  Real gamma2 = 10;

  integrandL += (beta1/(4*h))*(-wL)*rsdCirc + (gamma2*h)*dot(nL, gradwL)*rsdMass;
  integrandR += (beta1/(4*h))*(+wR)*rsdCirc + (gamma2*h)*dot(nR, gradwR)*rsdMass;
#endif
}

}   // namespace SANS

#endif  // INTEGRANDINTERIORTRACE_GALERKIN_WAKECUT_LIP_SRA_H
