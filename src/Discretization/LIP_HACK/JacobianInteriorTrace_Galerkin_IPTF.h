// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_GALERKIN_IPTF_H
#define JACOBIANINTERIORTRACE_GALERKIN_IPTF_H

// interior-trace integral jacobian functions
// IPTF: includes both PDE and auxiliary jacobians

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "JacobianInteriorTrace_Galerkin_Element_IPTF.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral without Lagrange multipliers
//

template<class IntegrandInteriorTrace>
class JacobianInteriorTrace_Galerkin_IPTF_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_Galerkin_IPTF_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianInteriorTrace_Galerkin_IPTF_impl( const IntegrandInteriorTrace& fcn,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_glb,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalAux_q,
                                            MatrixScatterAdd<MatrixQ>& mtxGlobalAux_glb ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_glb_(mtxGlobalPDE_glb),
    mtxGlobalAux_q_(mtxGlobalAux_q), mtxGlobalAux_glb_(mtxGlobalAux_glb),
    comm_rank_(0) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

  const int nAux = 1;     // size of auxiliary/global blocks

  //----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_glb_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_glb_.n() == nAux );

    SANS_ASSERT( mtxGlobalAux_q_.m() == nAux );
    SANS_ASSERT( mtxGlobalAux_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalAux_glb_.m() == nAux );
    SANS_ASSERT( mtxGlobalAux_glb_.n() == nAux );

    comm_rank_ = qfld.comm()->rank();

#ifdef SANS_MPI
    // MPI ranks assume a DG space below
    SANS_ASSERT(qfld.spaceType() == SpaceType::Discontinuous);
#endif
  }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;


    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;


    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef JacobianElemInteriorTrace_Galerkin_IPTF<MatrixQ> JacobianElemInteriorTraceType;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL, -1);
    std::vector<int> mapDOFGlobalR(nDOFR, -1);
    std::vector<int> mapAux(nAux, 0);

    JacobianElemInteriorTraceSize_IPTF sizeL(nDOFL,  nDOFL, nDOFR, nAux);
    JacobianElemInteriorTraceSize_IPTF sizeR(nDOFR,  nDOFL, nDOFR, nAux);
    JacobianElemInteriorTraceSize_IPTF sizeAux(nAux, nDOFL, nDOFR, nAux);

    // trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType,
                                                              JacobianElemInteriorTraceType,
                                                              JacobianElemInteriorTraceType>
      integral(quadratureorder);

    // element jacobian matrices: PDE, auxiliary wrt qL, qR, glb
    JacobianElemInteriorTraceType mtxPDEElemL(sizeL);
    JacobianElemInteriorTraceType mtxPDEElemR(sizeR);
    JacobianElemInteriorTraceType mtxAux(sizeAux);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // left/right elements
      int elemL = xfldTrace.getElementLeft( elem );
      int elemR = xfldTrace.getElementRight( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );

      const int rankL = qfldElemL.rank();
      const int rankR = qfldElemR.rank();

      // nothing to do if both elements are ghost elements
      if (rankL != comm_rank_ && rankR != comm_rank_) continue;

      // trace integration for canonical element
      integral(
          fcn_.integrand( xfldElemTrace, canonicalTraceL, canonicalTraceR,
                          xfldElemL, qfldElemL,
                          xfldElemR, qfldElemR ),
          xfldElemTrace, mtxPDEElemL, mtxPDEElemR, mtxAux );

      // scatter-add element jacobian to global

      scatterAdd(
          qfldCellL, qfldCellR,
          rankL, rankR,
          elemL, elemR,
          mapDOFGlobalL.data(), nDOFL,
          mapDOFGlobalR.data(), nDOFR,
          mapAux.data(), nAux,
          mtxPDEElemL.rsd_qL, mtxPDEElemL.rsd_qR, mtxPDEElemL.rsd_glb,
          mtxPDEElemR.rsd_qL, mtxPDEElemR.rsd_qR, mtxPDEElemR.rsd_glb,
          mtxAux.rsd_qL     , mtxAux.rsd_qR     , mtxAux.rsd_glb     ,
          mtxGlobalPDE_q_, mtxGlobalPDE_glb_,
          mtxGlobalAux_q_, mtxGlobalAux_glb_ );
    }
  }


  //----------------------------------------------------------------------------//
  template <class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupTypeL& qfldL,
      const QFieldCellGroupTypeR& qfldR,
      const int rankL, const int rankR,
      const int elemL, const int elemR,
      int mapDOFGlobalL[], const int nDOFL,
      int mapDOFGlobalR[], const int nDOFR,
      int mapAux[], const int nAux,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_glb,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_glb,
      SANS::DLA::MatrixD<MatrixQ>& mtxAux_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxAux_qR,
      SANS::DLA::MatrixD<MatrixQ>& mtxAux_glb,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_glb,
      SparseMatrixType<MatrixQ>& mtxGlobalAux_q,
      SparseMatrixType<MatrixQ>& mtxGlobalAux_glb  )
  {
    qfldL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL, nDOFL );
    qfldR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR, nDOFR );

    // jacobian wrt qL
    if (rankL == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qL, mapDOFGlobalL, nDOFL, mapDOFGlobalL, nDOFL );
    if (rankR == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qL, mapDOFGlobalR, nDOFR, mapDOFGlobalL, nDOFL );
    mtxGlobalAux_q.scatterAdd( mtxAux_qL, mapAux, nAux, mapDOFGlobalL, nDOFL );

    // jacobian wrt qR
    if (rankL == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qR, mapDOFGlobalL, nDOFL, mapDOFGlobalR, nDOFR );
    if (rankR == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qR, mapDOFGlobalR, nDOFR, mapDOFGlobalR, nDOFR );
    mtxGlobalAux_q.scatterAdd( mtxAux_qR, mapAux, nAux, mapDOFGlobalR, nDOFR );

    // jacobian wrt glb
    mtxGlobalPDE_glb.scatterAdd( mtxPDEElemL_glb, mapDOFGlobalL, nDOFL, mapAux, nAux );
    mtxGlobalPDE_glb.scatterAdd( mtxPDEElemR_glb, mapDOFGlobalR, nDOFR, mapAux, nAux );
    mtxGlobalAux_q.scatterAdd( mtxAux_glb, mapAux, nAux, mapAux, nAux );
  }

#if 0
  //----------------------------------------------------------------------------//
  template <class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupTypeL& qfldL,
      const QFieldCellGroupTypeR& qfldR,
      const int rankL, const int rankR,
      const int elemL, const int elemR,
      int mapDOFGlobalL[], const int nDOFL,
      int mapDOFGlobalR[], const int nDOFR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemLL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemLR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemRL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemRR,
      SparseMatrixType< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemLL, elemL, elemL );
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemLR, elemL, elemR );
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemRL, elemR, elemL );
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemRR, elemR, elemR );
  }
#endif

protected:
  const IntegrandInteriorTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_glb_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAux_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalAux_glb_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandInteriorTrace, class MatrixQ>
JacobianInteriorTrace_Galerkin_IPTF_impl<IntegrandInteriorTrace>
JacobianInteriorTrace_Galerkin_IPTF( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                     MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                     MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_glb,
                                     MatrixScatterAdd<MatrixQ>& mtxGlobalAux_q,
                                     MatrixScatterAdd<MatrixQ>& mtxGlobalAux_glb )
{
  return { fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_glb, mtxGlobalAux_q, mtxGlobalAux_glb };
}

} // namespace SANS

#endif  // JACOBIANINTERIORTRACE_GALERKIN_IPTF_H
