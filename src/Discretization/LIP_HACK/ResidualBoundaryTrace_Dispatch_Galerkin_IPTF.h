// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_DISPATCH_GALERKIN_IPTF_H
#define RESIDUALBOUNDARYTRACE_DISPATCH_GALERKIN_IPTF_H

// boundary-trace integral residual functions
// IPTF: includes both PDE and auxiliary residuals

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "ResidualBoundaryTrace_sansLG_Galerkin_IPTF.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

#if 0       // mitLG not implemented
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Field trace variables, e.g. Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
class ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_IPTF_impl
{
public:
  ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_IPTF_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQR>& rsdPDEGlobal,
      Vector<ArrayQR>& rsdBCGlobal )
    : xfld_(xfld), qfld_(qfld), lgfld_(lgfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal_, rsdBCGlobal_),
        xfld_, qfld_, lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQR>& rsdPDEGlobal_;
  Vector<ArrayQR>& rsdBCGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_IPTF_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, ArrayQR>
ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_IPTF(const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                              const int* quadratureorder, int ngroup,
                                              Vector<ArrayQR>& rsdPDEGlobal,
                                              Vector<ArrayQR>& rsdBCGlobal)
{
  static_assert( DLA::VectorSize<ArrayQ>::M == DLA::VectorSize<ArrayQR>::M, "These should be the same size.");

  return ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_IPTF_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, ArrayQR>(
      xfld, qfld, lgfld, quadratureorder, ngroup, rsdPDEGlobal, rsdBCGlobal);
}
#endif      // mitLG not implemented


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
class ResidualBoundaryTrace_Dispatch_Galerkin_IPTF_impl
{
public:
  ResidualBoundaryTrace_Dispatch_Galerkin_IPTF_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQR>& rsdPDEGlobal,
      Vector<ArrayQR>& rsdAuxGlobal )
    : xfld_(xfld), qfld_(qfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal), rsdAuxGlobal_(rsdAuxGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        ResidualBoundaryTrace_sansLG_Galerkin_IPTF(fcn, rsdPDEGlobal_, rsdAuxGlobal_),
        xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQR>& rsdPDEGlobal_;       // PDE residuals
  Vector<ArrayQR>& rsdAuxGlobal_;       // auxiliary residuals
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class ArrayQR>
ResidualBoundaryTrace_Dispatch_Galerkin_IPTF_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, ArrayQR>
ResidualBoundaryTrace_Dispatch_Galerkin_IPTF(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const int* quadratureorder, int ngroup,
                                             Vector<ArrayQR>& rsdPDEGlobal, Vector<ArrayQR>& rsdAuxGlobal)
{
  static_assert( DLA::VectorSize<ArrayQ>::M == DLA::VectorSize<ArrayQR>::M, "These should be the same size.");

  return { xfld, qfld, quadratureorder, ngroup, rsdPDEGlobal, rsdAuxGlobal };
}


}   // namespace SANS

#endif //RESIDUALBOUNDARYTRACE_DISPATCH_GALERKIN_IPTF_H
