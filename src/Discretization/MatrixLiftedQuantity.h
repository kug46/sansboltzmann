// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MATRIXLIFTEDQUANTITY_H
#define MATRIXLIFTEDQUANTITY_H

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"

namespace SANS
{

//A template metafunction for getting the type of a PDE jacobian wrt a lifted quantity

template<class ArrayQ>
struct MatrixLiftedQuantity;

//Specializations
template <>
struct MatrixLiftedQuantity<Real>
{
  typedef Real type;
  typedef Real transpose_type;
};

template<int N, class T>
struct MatrixLiftedQuantity<DLA::MatrixS<N,N,T>>
{
  typedef DLA::MatrixS<N,1,T> type;
  typedef DLA::MatrixS<1,N,T> transpose_type;
};

}

#endif //MATRIXLIFTEDQUANTITY_H
