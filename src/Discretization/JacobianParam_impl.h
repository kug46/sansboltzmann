// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(JACOBIANPARAM_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <boost/mpl/begin_end.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/next.hpp>

#include "Discretization/JacobianParam.h"

namespace SANS
{

//===========================================================================//
namespace detail_JP
{

// Looping class: Get the current BC type check if it matches the BC parameter
template < class iter, class end >
struct JacobianParamIterator
{
  template<class AESClass, class SparseMatrixType>
  static void jacobian( const AESClass& AES, SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, const std::map<int,int>& columnMap )
  {
    typedef typename boost::mpl::deref<iter>::type iParam;

    AES.template jacobianParam<iParam::value>(jac, quadratureOrder, columnMap.at(iParam::value));

    // Iterate recursively to the next option
    typedef typename boost::mpl::next<iter>::type nextiter;
    JacobianParamIterator< nextiter, end>::jacobian(AES, jac, quadratureOrder, columnMap);
  }
};

// Recursive loop terminates when 'iter' is the same as 'end'
template < class end >
struct JacobianParamIterator< end, end >
{
  template<class AESClass, class SparseMatrixType>
  static void jacobian( AESClass& AES, SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, const std::map<int,int>& columnMap )
  {
    // Do nothing
  }
};

} // namespace detail

//===========================================================================//
//Fills jacobian or the non-zero pattern of a jacobian
template<class iParams, class AESClass>
void
JacobianParam<iParams, AESClass>::
jacobian(SystemMatrixView& mtx)
{
  if (JacParamChained_ != nullptr) JacParamChained_->jacobian(mtx);
  jacobian<SystemMatrixView&>(mtx, quadratureOrder_);
}

template<class iParams, class AESClass>
void
JacobianParam<iParams, AESClass>::
jacobian(SystemNonZeroPatternView& nz)
{
  if (JacParamChained_ != nullptr) JacParamChained_->jacobian(nz);
  jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_);
}

//Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
template<class iParams, class AESClass>
void
JacobianParam<iParams, AESClass>::
jacobian(SystemMatrixTranspose mtxT)
{
  jacobian(mtxT, quadratureOrder_);
}

template<class iParams, class AESClass>
void
JacobianParam<iParams, AESClass>::
jacobian(SystemNonZeroPatternTranspose nzT)
{
  jacobian(nzT, quadratureOrderMin_);
}

template<class iParams, class AESClass>
template<class SparseMatrixType>
void
JacobianParam<iParams, AESClass>::
jacobian(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder)
{
  detail_JP::JacobianParamIterator< typename boost::mpl::begin< iParams >::type,
                                    typename boost::mpl::end< iParams >::type >::
             jacobian(AES_, jac, quadratureOrder, columnMap_);
}

} //namespace SANS
