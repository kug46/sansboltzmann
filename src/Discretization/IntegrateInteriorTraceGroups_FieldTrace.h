// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATEINTERIORTRACEGROUPS_FIELDTRACE_H
#define INTEGRATEINTERIORTRACEGROUPS_FIELDTRACE_H

// interior-trace group integral functions with mortar elements //TODO: define mortar element?

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "GroupIntegral_Type.h"

namespace SANS
{

template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim,
          class IntegralInteriorTrace,
          class XFieldType, class QFieldType, class QIFieldType >
void
IntegrateInteriorTraceGroups_FieldTrace_Integral(
    IntegralInteriorTrace& integral,
    const XFieldType& xfld,
    const QFieldType& qfld,
    const int traceGroupGlobal,
    const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const QIFieldType& qIfldTrace,
    const int quadratureorder )
{
  int cellGroupGlobalL = xfldTrace.getGroupLeft();
  int cellGroupGlobalR = xfldTrace.getGroupRight();

  // Integrate over the trace group
  integral.template integrate<TopologyTrace, TopologyL, TopologyR, TopoDim, XFieldType>(
      cellGroupGlobalL,
      xfld.template getCellGroup<TopologyL>(cellGroupGlobalL),
      qfld.template getCellGroupGlobal<TopologyL>(cellGroupGlobalL),
      cellGroupGlobalR,
      xfld.template getCellGroup<TopologyR>(cellGroupGlobalR),
      qfld.template getCellGroupGlobal<TopologyR>(cellGroupGlobalR),
      traceGroupGlobal,
      xfldTrace,
      qIfldTrace,
      quadratureorder );
}


template<class TopDim>
class IntegrateInteriorTraceGroups_FieldTrace;


template<>
class IntegrateInteriorTraceGroups_FieldTrace<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

protected:
  template <class TopologyTrace, class TopologyL, class QIFieldType, class IntegralInteriorTrace,
            class XFieldType, class QFieldType >
  static void
  RightTopology(IntegralInteriorTrace& integral,
                const XFieldType& xfld,
                const QFieldType& qfld,
                const int traceGroupGlobal,
                const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const typename QIFieldType::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
                const int quadratureorder )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Line) )
    {
      IntegrateInteriorTraceGroups_FieldTrace_Integral<TopologyTrace, TopologyL, Line, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, qIfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }


  template <class TopologyTrace, class QIFieldType, class IntegralInteriorTrace,
            class XFieldType, class QFieldType >
  static void
  LeftTopology( IntegralInteriorTrace& integral,
                const XFieldType& xfld,
                const QFieldType& qfld,
                const int traceGroupGlobal,
                const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const typename QIFieldType::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
                const int quadratureorder )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Line, QIFieldType>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, qIfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );

  }

public:
  template <class IntegralInteriorTrace,
            class XFieldType, class QFieldType, class QIFieldType >
  static void
  integrate( GroupIntegralInteriorTraceType<IntegralInteriorTrace>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const FieldType<QIFieldType>& qIfldType,
             const int quadratureorder[], int ngroup )
  {
    IntegralInteriorTrace& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();
    const QIFieldType& qIfld = qIfldType.cast();

    integral.template check<TopoDim>( qfld, qIfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT( &xfld.getXField() == &qIfld.getXField() );
    SANS_ASSERT( ngroup == xfld.getXField().nInteriorTraceGroups() );

    // loop over line element groups
    for (std::size_t group = 0; group < integral.nInteriorTraceGroups(); group++)
    {
      const int traceGroupGlobal = integral.interiorTraceGroup(group);

      if ( xfld.getXField().getInteriorTraceGroupBase(traceGroupGlobal).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node, QIFieldType>( integral,
                                         xfld, qfld,
                                         traceGroupGlobal,
                                         xfld.getXField().template getInteriorTraceGroup<Node>(traceGroupGlobal),
                                         qIfld.template getInteriorTraceGroupGlobal<Node>(traceGroupGlobal),
                                         quadratureorder[traceGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION("unknown trace topology" );

    }
  }
};


template<>
class IntegrateInteriorTraceGroups_FieldTrace<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

protected:
  template <class TopologyTrace, class TopologyL, class QIFieldType, class IntegralInteriorTrace,
            class XFieldType, class QFieldType >
  static void
  RightTopology(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename QIFieldType::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      const int quadratureorder )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Triangle) )
    {
      IntegrateInteriorTraceGroups_FieldTrace_Integral<TopologyTrace, TopologyL, Triangle, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, qIfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }


  template <class TopologyTrace, class QIFieldType, class IntegralInteriorTrace,
            class XFieldType, class QFieldType >
  static void
  LeftTopology(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename QIFieldType::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      const int quadratureorder )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Triangle, QIFieldType>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, qIfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class IntegralInteriorTrace,
            class XFieldType, class QFieldType, class QIFieldType >
  static void
  integrate( GroupIntegralInteriorTraceType<IntegralInteriorTrace>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const FieldType<QIFieldType>& qIfldType,
             const int quadratureorder[], int ngroup )
  {
    IntegralInteriorTrace& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();
    const QIFieldType& qIfld = qIfldType.cast();

    integral.template check<TopoDim>( qfld, qIfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT( &xfld.getXField() == &qIfld.getXField() );
    SANS_ASSERT( ngroup == xfld.getXField().nInteriorTraceGroups() );

    // loop over line element groups
    for (std::size_t group = 0; group < integral.nInteriorTraceGroups(); group++)
    {
      const int traceGroupGlobal = integral.interiorTraceGroup(group);

      if ( xfld.getXField().getInteriorTraceGroupBase(traceGroupGlobal).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line, QIFieldType>( integral,
                                         xfld, qfld,
                                         traceGroupGlobal,
                                         xfld.getXField().template getInteriorTraceGroup<Line>(traceGroupGlobal),
                                         qIfld.template getInteriorTraceGroupGlobal<Line>(traceGroupGlobal),
                                         quadratureorder[traceGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

template<>
class IntegrateInteriorTraceGroups_FieldTrace<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

protected:
  template <class TopologyL, class QIFieldType, class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  RightTopology_Triangle(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
      const typename QIFieldType::template FieldTraceGroupType<Triangle>& qIfldTrace,
      const int quadratureorder )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      IntegrateInteriorTraceGroups_FieldTrace_Integral<Triangle, TopologyL, Tet, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, qIfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }

  template <class TopologyL, class QIFieldType, class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  RightTopology_Quad(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
      const typename QIFieldType::template FieldTraceGroupType<Quad>& qIfldTrace,
      const int quadratureorder )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      IntegrateInteriorTraceGroups_FieldTrace_Integral<Quad, TopologyL, Hex, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, qIfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }


  template <class QIFieldType, class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  LeftTopology_Triangle(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
      const typename QIFieldType::template FieldTraceGroupType<Triangle>& qIfldTrace,
      const int quadratureorder )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      RightTopology_Triangle<Tet, QIFieldType>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, qIfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }

  template <class QIFieldType, class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  LeftTopology_Quad(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
      const typename QIFieldType::template FieldTraceGroupType<Quad>& qIfldTrace,
      const int quadratureorder )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      RightTopology_Quad<Hex, QIFieldType>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, qIfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }

public:
  template <class IntegralInteriorTrace,
            class XFieldType, class QFieldType, class QIFieldType>
  static void
  integrate( GroupIntegralInteriorTraceType<IntegralInteriorTrace>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const FieldType<QIFieldType>& qIfldType,
             const int quadratureorder[], int ngroup )
  {
    IntegralInteriorTrace& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();
    const QIFieldType& qIfld = qIfldType.cast();

    integral.template check<TopoDim>( qfld, qIfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT( &xfld.getXField() == &qIfld.getXField() );
    SANS_ASSERT( ngroup == xfld.getXField().nInteriorTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nInteriorTraceGroups(); group++)
    {
      const int traceGroupGlobal = integral.interiorTraceGroup(group);

      if ( xfld.getXField().getInteriorTraceGroupBase(traceGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle<QIFieldType>(
            integral, xfld, qfld, traceGroupGlobal,
            xfld.getXField().template getInteriorTraceGroup<Triangle>(traceGroupGlobal),
            qIfld.template getInteriorTraceGroupGlobal<Triangle>(traceGroupGlobal),
            quadratureorder[traceGroupGlobal] );
      }
      else if ( xfld.getXField().getInteriorTraceGroupBase(traceGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad<QIFieldType>(
            integral, xfld, qfld, traceGroupGlobal,
            xfld.getXField().template getInteriorTraceGroup<Quad>(traceGroupGlobal),
            qIfld.template getInteriorTraceGroupGlobal<Quad>(traceGroupGlobal),
            quadratureorder[traceGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown trace topology." );
    }
  }
};

}

#endif  // INTEGRATEINTERIORTRACEGROUPS_FIELDTRACE_H
