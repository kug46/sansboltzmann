// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRAND_TYPES_H
#define INTEGRAND_TYPES_H

#include <typeinfo>

#include "tools/SANSException.h"
#include "pde/BCCategory.h"
#include "DiscretizationBCTag.h"

namespace SANS
{

class IntegrandBoundaryTraceBase
{
public:
  virtual ~IntegrandBoundaryTraceBase() {}

  virtual const std::type_info& derivedTypeID() const = 0;
  virtual const std::type_info& discTagID() const = 0;
  virtual bool needsFieldTrace() const = 0;
  virtual bool needsHubTrace() const = 0;

  // Safe methods for casting the BC type
  template<class IntegrandBoundaryTrace>
  IntegrandBoundaryTrace& cast()
  {
    SANS_ASSERT( typeid(IntegrandBoundaryTrace) == derivedTypeID() );
    return static_cast<IntegrandBoundaryTrace&>(*this);
  }

  template<class IntegrandBoundaryTrace>
  const IntegrandBoundaryTrace& cast() const
  {
    SANS_ASSERT( typeid(IntegrandBoundaryTrace) == derivedTypeID() );
    return static_cast<const IntegrandBoundaryTrace&>(*this);
  }
};


// A set of base classes to indicate a specific class is an implementation of an integrand functor

template<class Derived>
struct IntegrandCellType
{
  IntegrandCellType() {}
  IntegrandCellType(const IntegrandCellType&) = delete;
  IntegrandCellType operator=(const IntegrandCellType&) = delete;

  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

template<class Derived>
struct IntegrandInteriorTraceType
{
  IntegrandInteriorTraceType() {}
  IntegrandInteriorTraceType(const IntegrandInteriorTraceType&) = delete;
  IntegrandInteriorTraceType operator=(const IntegrandInteriorTraceType&) = delete;

  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

template<class Derived>
struct IntegrandBoundaryTraceType : public IntegrandBoundaryTraceBase
{
  IntegrandBoundaryTraceType() {}
  IntegrandBoundaryTraceType(const IntegrandBoundaryTraceType&) = delete;
  IntegrandBoundaryTraceType operator=(const IntegrandBoundaryTraceType&) = delete;

  virtual const std::type_info& derivedTypeID() const override
  {
    static_assert( std::is_base_of<IntegrandBoundaryTraceType<Derived>, Derived>::value, "This is usually a copy/paste mistake" );

    return typeid(Derived);
  }

  virtual const std::type_info& discTagID() const override
  {
    return typeid(typename Derived::DiscTag);
  }

  virtual bool needsFieldTrace() const override
  {
    return CategoryNeedsFieldTrace<typename Derived::Category>::value;
  }

  virtual bool needsHubTrace() const override
  {
    return CategoryNeedsHubTrace<typename Derived::Category>::value;
  }

  virtual ~IntegrandBoundaryTraceType() {}

  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};


//----------------------------------------------------------------------------//
// Trace boundary integrand
template <class PDE, class NDBCVecCategory, class DiscTag>
class IntegrandBoundaryTrace;

}

#endif //INTEGRAND_TYPES_H
