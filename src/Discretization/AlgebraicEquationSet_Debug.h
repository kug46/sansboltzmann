// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_DEBUG_H
#define ALGEBRAICEQUATIONSET_DEBUG_H

#include "tools/SANSnumerics.h"

#include <vector>
#include <iostream>
#include <iomanip> // std::setprecision
#include <limits>  // std::numeric_limits

#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Field/FieldLinesearch/FieldLinesearch_Line.h"
#include "Field/FieldLinesearch/FieldLinesearch_Area.h"
#include "Field/FieldLinesearch/FieldLinesearch_Volume.h"
#include "Field/FieldLinesearch/FieldLinesearch_SpaceTime.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/output_Tecplot.h"
#include "Field/tools/for_each_CellGroup.h"

#include "tools/output_std_vector.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// An abstract base class for line-search debugging operations

template<class PhysDim, class T, class LinesearchVector>
class AlgebraicEquationSet_Debug_Linesearch_impl;

template<class NDPDEClass, class Traits>
class AlgebraicEquationSet_Debug : public AlgebraicEquationSetTraits<typename NDPDEClass::template MatrixQ<Real>,
                                                                     typename NDPDEClass::template ArrayQ<Real>,
                                                                     Traits>::AlgebraicEquationSetBaseClass
{
public:
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;
  typedef typename LinesearchData::element_type LinesearchVector;

  AlgebraicEquationSet_Debug( const NDPDEClass& pde, const std::vector<Real>& tol ) :
    pde_(pde), tol_(tol)
  {}

  virtual ~AlgebraicEquationSet_Debug() {}

  virtual void dumpSolution(const std::string& filename) const override { SANS_ASSERT(false); }

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override
  {

    SANS_ASSERT( tol_.size() == rsdNorm.size() );

    //converged?
    bool converged = true;
    // This checks for nans also, because nan fails all comparisons
    // ( a != a ) = true, iff a = nan
    for (std::size_t iEq = 0; iEq < rsdNorm.size() && converged; iEq++)
      for (std::size_t iMon = 0; iMon < rsdNorm[iEq].size() && converged; iMon++)
       if ( rsdNorm[iEq][iMon] > tol_[iEq] || std::isnan(rsdNorm[iEq][iMon]) )
         converged = false;

    return converged;
  }

  //Convergence check of the residual - single index only
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override
  {
    SANS_ASSERT( tol_.size() == rsdNorm.size() );

    bool converged = true;

    if ( rsdNorm[iEq][iMon] > tol_[iEq] )
         converged = false;

    // if you're a nan, definitely not converged
    if (std::isnan(rsdNorm[iEq][iMon]) == true)
      converged = false;

    return converged;
  }

  // is the residual norm decreased?
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override
  {
    //Compute Residual Norms

    SANS_ASSERT( tol_.size() == rsdNormNew.size() );

    //is the residual actually decreased or converged?
    bool decreased = true;
    for (std::size_t iEq = 0; iEq < rsdNormNew.size() && decreased; iEq++)
      for (std::size_t iMon = 0; iMon < rsdNormNew[iEq].size() && decreased; iMon++)
      {
        // This checks for nans also, because nan fails all comparisons
        // ( a != a ) = true, iff a = nan
        if ( rsdNormOld[iEq][iMon] < rsdNormNew[iEq][iMon] && rsdNormOld[iEq][iMon] > tol_[iEq] )
          decreased = false;

        if ( std::isnan( rsdNormNew[iEq][iMon] ) ) decreased = false;
      }

    return decreased;
  }

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real> >& rsdNorm, std::ostream& os = std::cout) const override
  {
    std::vector<std::string> titles;
    std::vector<int> idx;

    residualInfo(titles, idx);

    SANS_ASSERT( titles.size() == idx.size() );
    SANS_ASSERT( titles.size() == tol_.size() );

    os << std::scientific;
    for ( std::size_t ir = 0; ir < idx.size(); ir++)
    {
      os << titles[ir];
      for (std::size_t i = 0; i < rsdNorm[idx[ir]].size(); i++)
        os << std::setprecision(12) << rsdNorm[idx[ir]][i] << " ";
      os << "> " << std::setprecision(3) << tol_[idx[ir]];
      os << std::endl;
    }
  }

  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const = 0;

  //Update the localized linesearch data
  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override final
  {
    const int nResid = rsd.m();
    const int nMon = pde_.nMonitor();
    bool initialize_flag = false;
    bool isDataComplete = true;
    Real s_init = 0.0;

    SANS_ASSERT( (int) tol_.size() == nResid );

    //Initialize data if needed
    if (pResData == nullptr)
    {
      initialize_flag = true;
      s_init = s;
      pResData  = std::make_shared<LinesearchVector>(nResid);
      pStepData = std::make_shared<LinesearchVector>(nResid);

      for (int i = 0; i < nResid; i++)
      {
        (*pResData )[i].resize(rsd[i].m(), nMon);
        (*pStepData)[i].resize(rsd[i].m(), nMon);
      }
    }

    DLA::VectorD<Real> rsdtmp(nMon);

    for (int i = 0; i < nResid; i++)
    {
      for (int j = 0; j < rsd[i].m(); j++)
      {
        if ( i == this->indexPDE() )
        {
          pde_.interpResidVariable(rsd[i][j], rsdtmp); //PDE residuals
        }
        else if ( i == this->indexBC() )
        {
          pde_.interpResidBC(rsd[i][j], rsdtmp); //BC residuals
        }
        else if ( i == this->indexAUX() )
        {
          pde_.interpResidGradient(rsd[i][j], rsdtmp); //Auxiliary equation residuals
        }
        else if ( i == this->indexINT() )
        {
          pde_.interpResidVariable(rsd[i][j], rsdtmp); //Interior residuals
        }

        for (int k = 0; k < nMon; k++)
        {
          rsdtmp[k] = fabs(rsdtmp[k]);

          if ( initialize_flag )
          {
            //Save the residual monitor value in the initial run, and set step-size to zero
            (*pResData )[i][j][k] = rsdtmp[k];
            (*pStepData)[i][j][k] = s_init;
          }
          else if ( (*pResData)[i][j][k] > tol_[i] )
          {
            if ( rsdtmp[k] < (*pResData)[i][j][k] )
            {
              //Store the largest stepsize that gives a reduction in the current residual monitor
              (*pStepData)[i][j][k] = max(s, (*pStepData)[i][j][k]);
            }
          }
          else
          {
            //Initial residual norm is already below tolerance, so set local step-size to 1.
            (*pStepData)[i][j][k] = 1.0;
          }

          // If the step-size for this residual monitor is still zero, the data is incomplete
          // and we have to re-evaluate with a smaller stepsize
          if ( (*pStepData)[i][j][k] == s_init ) isDataComplete = false;

        } //loop over monitors
      } //loop over DOFs
    }

    return (isDataComplete || initialize_flag);
  }

  template <class PhysDim, class TopoDim, class T>
  void dumpLinesearchField(const Field<PhysDim, TopoDim, T>& fld, const LinesearchVector& stepData,
                           const int indexRes, const std::string& filename) const
  {
    const int nDOF = stepData[indexRes].m();
    SANS_ASSERT(nDOF == fld.nDOF());

    if (nDOF>0)
    {
      FieldLinesearch<PhysDim, TopoDim, T> stepsize_fld(fld);

      for_each_CellGroup<TopoDim>::apply(
          AlgebraicEquationSet_Debug_Linesearch_impl<PhysDim, T, LinesearchVector>(fld.getGlobalCellGroups(), stepData, indexRes),
          (fld, stepsize_fld) );

      output_Tecplot( stepsize_fld, filename );
    }
  }

  // Are we having machine precision issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override
  {
    //  We perturb the input vector by (1 + eps) * q(i)
    SystemVector qtilde(this->vectorStateSize());
    qtilde = q;
    qtilde *= (1.0 + std::numeric_limits<Real>::epsilon());
    // If this isn't valid, don't do the check
    if (this->isValidStateSystemVector(qtilde))
    {
      // 3. Test at the the new q
      SystemVector rsdtilde(this->vectorEqSize());
      this->residual(qtilde, rsdtilde);
      // 4. Is the perturbed residual near the old one
      std::vector<std::vector<Real>> R2norm( this->residualNorm(rsdtilde) );
      std::vector<std::vector<Real>> R3norm( R0norm );
      for (std::size_t i = 0; i < R0norm.size(); i++)
        for (std::size_t j = 0; j < R0norm[i].size(); j++)
          R3norm[i][j] = std::abs(R0norm[i][j] - R2norm[i][j]);
      // 5. OK, so this is a bit of a hack
      //  If the deltaR is larger than all our residuals, then we're looking for a
      //  change in q that is less than eps (in some sense), so we're going to have
      //  a bad time...
//      std::cout << "R0norm: " << R0norm << std::endl;
//      std::cout << "R3norm: " << R3norm << std::endl;
      bool isEps = false;
      for (std::size_t i = 0; i < R0norm.size(); i++)
        for (std::size_t j = 0; j < R0norm[i].size(); j++)
          if (   (R0norm[i][j] <  R3norm[i][j])  // Is the residual smaller than our 'perturbation'?
              && (R0norm[i][j] >  tol_[i])       // Is the residual converged?
              && (R3norm[i][j] != 0.0))          // Is the perturbation exactly zero?
            isEps = true;                        // Then we have a possible issue with machine precision

      if (!isEps)
      {
        return true;
      }
    }

    return false;
  }

private:
  const NDPDEClass& pde_;
  const std::vector<Real> tol_;
};


template<class PhysDim, class T, class LinesearchVector>
class AlgebraicEquationSet_Debug_Linesearch_impl :
    public GroupFunctorCellType<AlgebraicEquationSet_Debug_Linesearch_impl<PhysDim, T, LinesearchVector>>
{
public:

  AlgebraicEquationSet_Debug_Linesearch_impl( const std::vector<int>& cellgroup_list,
                                              const LinesearchVector& stepData,
                                              const int& indexRes ) :
    cellgroup_list_(cellgroup_list), stepData_(stepData), indexRes_(indexRes) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology>
  void
  apply(const typename FieldTuple< Field<PhysDim, typename Topology::TopoDim, T>,
                                   FieldLinesearch<PhysDim, typename Topology::TopoDim, T>,TupleClass<> >::
        template FieldCellGroupType<Topology>& fldsCellGroup,
        const int cellGroupGlobal)
  {
    // Cell Group Types
    typedef typename Field<PhysDim,typename Topology::TopoDim,T   >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLinesearch<PhysDim,typename Topology::TopoDim,T>::template FieldCellGroupType<Topology> SFieldCellGroupType;

    typedef typename QFieldCellGroupType::template ElementType<>  ElementQFieldClass;
    typedef typename SFieldCellGroupType::template ElementType<>  ElementSFieldClass;

    const QFieldCellGroupType& qfldCellGroup = get<0>(fldsCellGroup);
    SFieldCellGroupType& sfldCellGroup = const_cast<SFieldCellGroupType&>(get<1>(fldsCellGroup));

    ElementQFieldClass qfldElem(qfldCellGroup.basis());
    ElementSFieldClass sfldElem(sfldCellGroup.basis());

    const int nDOF = qfldElem.nDOF();

    int nElem = qfldCellGroup.nElem(); //Number of elements in cell group

    std::vector<int> DOFMap(nDOF);
    for (int elem = 0; elem < nElem; elem++ )
    {
      qfldCellGroup.associativity(elem).getGlobalMapping(DOFMap.data(), DOFMap.size());

      // initialize all values in ArrayQ to -1, some might not be used
      sfldElem.DOF(0) = -1;

      //Find the minimum step-size taken for all DOFs and residual monitors on this element
      for (int k = 0; k < stepData_[indexRes_][DOFMap[0]].m(); k++)
      {
        Real min_step = std::numeric_limits<Real>::max();
        for (int n = 0; n < nDOF; n++)
          min_step = min(stepData_[indexRes_][DOFMap[n]][k], min_step);

        DLA::index(sfldElem.DOF(0),k) = min_step;
      }
      sfldCellGroup.setElement(sfldElem,elem);
    }
  }

protected:
  const std::vector<int> cellgroup_list_;
  const LinesearchVector& stepData_;
  const int& indexRes_;
};


} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_DEBUG_H
