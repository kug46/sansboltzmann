// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRAND_DGBR2_FWD_H
#define INTEGRAND_DGBR2_FWD_H

namespace SANS
{

//----------------------------------------------------------------------------//
// Tag to indicate a DGBR2 type integrands

class DGBR2 {};
class DGBR2_manifold {};

//----------------------------------------------------------------------------//
// Cell integrand: DGBR2

template <class PDE>
class IntegrandCell_DGBR2;

template <class PDE>
class IntegrandCell_DGBR2_manifold;

//----------------------------------------------------------------------------//
// Trace integrand: DGBR2

template <class PDE>
class IntegrandInteriorTrace_DGBR2;

template <class PDE>
class IntegrandInteriorTrace_DGBR2_manifold;

}

#endif // INTEGRAND_DGBR2_FWD_H
