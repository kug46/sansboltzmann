// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "AlgebraicEquationSet_DGBR2.h"

#include <type_traits>
#include <memory>
//#include <boost/mpl/assert.hpp>

#include "Surreal/SurrealS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Field/XField.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/output_Tecplot.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegratePeriodicTraceGroups.h"

//Boundary trace integrands - Galerkin
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin_manifold.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin_manifold.h"

//Boundary trace integrands - DG
#include "Discretization/DG/IntegrandBoundaryTrace_LinearScalar_sansLG_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2.h"
#include "Discretization/DG/IntegrandBoundaryTrace_Flux_mitState_DGBR2_manifold.h"

#include "Discretization/DG/ResidualCell_DGBR2.h"
#include "Discretization/DG/ResidualInteriorTrace_DGBR2.h"
#include "Discretization/DG/ResidualBoundaryTrace_Dispatch_DGBR2.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/DG/JacobianCell_DGBR2_LiftingOperator.h"
#include "Discretization/DG/JacobianCell_DGBR2.h"
#include "Discretization/DG/JacobianInteriorTrace_DGBR2.h"
#include "Discretization/DG/JacobianInteriorTrace_DGBR2_LiftedQuantity.h"
#include "Discretization/DG/JacobianBoundaryTrace_Dispatch_DGBR2.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin.h"

//Parameter Jacobian
#include "Discretization/DG/JacobianCell_DGBR2_Param.h"
#include "Discretization/DG/JacobianInteriorTrace_DGBR2_Param.h"
#include "Discretization/DG/JacobianBoundaryTrace_Dispatch_DGBR2_Param.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin_Param.h"

//Compute lifting operator field
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftingOperator.h"
#include "Discretization/DG/SetFieldBoundaryTrace_DGBR2_LiftingOperator.h"
#include "Discretization/DG/SetFieldBoundaryTrace_Dispatch_LiftingOperator.h"

#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_AdjointLO.h"
#include "Discretization/DG/SetFieldBoundaryTrace_DGBR2_AdjointLO.h"
#include "Discretization/DG/SetFieldBoundaryTrace_Dispatch_AdjointLO.h"

//Compute lifted quantity field
#include "Discretization/DG/SetFieldInteriorTrace_DGBR2_LiftedQuantity.h"

#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/isValidState/isValidStateInteriorTrace.h"
#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch.h"

#include "Discretization/UpdateFraction/UpdateFractionCell.h"
#include "Discretization/UpdateFraction/UpdateFractionInteriorTrace.h"
#include "Discretization/UpdateFraction/UpdateFractionBoundaryTrace_Dispatch.h"

#include "Discretization/DG/MassMatInvResidualNorm.h"
#include "Discretization/DG/JacobianDetInvResidualNorm.h"

//#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>
#include "tools/plus_std_vector.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#include "tools/output_std_vector.h"

namespace SANS
{

// This templated constructor must be explicitly instantiated as well
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
template< class... BCArgs >
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
AlgebraicEquationSet_DGBR2( const XFieldType& xfld,
                            Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                            FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld,
                            Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                            std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                            const NDPDEClass& pde,
                            const DiscretizationDGBR2& disc,
                            const QuadratureOrder& quadratureOrder,
                            const ResidualNormType& resNormType,
                            const std::vector<Real>& tol,
                            const std::vector<int>& CellGroups,
                            const std::vector<int>& InteriorTraceGroups,
                            PyDict& BCList,
                            const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                            BCArgs&... args )
  :  DebugBaseType(pde, tol),
     fcnCell_(pde, disc, CellGroups),
     fcnTrace_( pde, disc, InteriorTraceGroups, xfld.getXField().getPeriodicTraceGroupsGlobal(CellGroups) ),
     BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
     dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, disc),
     xfld_(xfld), qfld_(qfld), rfld_(rfld), lgfld_(lgfld), mmfld_(qfld),
     pde_(pde), quadratureOrder_(quadratureOrder), quadratureOrderMin_(get<-1>(xfld), 0),
     resNormType_(resNormType), tol_(tol),
     AES_(nullptr), pLiftedQuantityfld_(pLiftedQuantityfld)
{
  SANS_ASSERT(tol_.size() == 2);

  for (auto it = CellGroups.begin(); it != CellGroups.end(); ++it) // loop over groups
    for (auto itt = std::next(it,1); itt != CellGroups.end(); ++itt) // loop over remaining groups
      SANS_ASSERT_MSG( *it != *itt, "A cell group can not be specified twice" );

  for (auto it = InteriorTraceGroups.begin(); it != InteriorTraceGroups.end(); ++it) // loop over groups
    for (auto itt = std::next(it,1); itt != InteriorTraceGroups.end(); ++itt) // loop over remaining groups
      SANS_ASSERT_MSG( *it != *itt, "An interior trace group can not be specified twice" );

  for (auto mit = BCBoundaryGroups.begin(); mit != BCBoundaryGroups.end(); ++mit)
    for (auto it = mit->second.begin(); it != mit->second.end(); ++it) // loop over groups
      for (auto itt = std::next(it,1); itt != mit->second.end(); ++itt) // loop over remaining groups
        SANS_ASSERT_MSG( *it != *itt, "A boundary group can not be specified twice in one boundary condition" );


  if ( pde_.hasSourceLiftedQuantity() )
  {
    SANS_ASSERT_MSG(pLiftedQuantityfld_, "AlgebraicEquationSet_DGBR2 - The lifted quantity field has not been initialized!");
    pmmfldLiftedQuantity_ = std::make_shared<FieldDataInvMassMatrix_Cell>(*pLiftedQuantityfld_);
  }

  // Do not compute lifting operators here.
  // Computing the adjoint for error estimation for example must be done with prolongated lifting operators
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
template< class... BCArgs >
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
AlgebraicEquationSet_DGBR2( const XFieldType& xfld,
                            Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                            FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld,
                            Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                            const NDPDEClass& pde,
                            const DiscretizationDGBR2& disc,
                            const QuadratureOrder& quadratureOrder,
                            const ResidualNormType& resNormType,
                            const std::vector<Real>& tol,
                            const std::vector<int>& CellGroups,
                            const std::vector<int>& InteriorTraceGroups,
                            PyDict& BCList,
                            const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                            BCArgs&... args )
   : AlgebraicEquationSet_DGBR2( xfld, qfld, rfld, lgfld, {}, pde, disc, quadratureOrder, resNormType, tol,
                                 CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...) {}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
template< class... BCArgs >
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
AlgebraicEquationSet_DGBR2( const XFieldType& xfld,
                            FieldBundleBase& flds,
                            std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                            const NDPDEClass& pde,
                            const DiscretizationDGBR2& disc,
                            const QuadratureOrder& quadratureOrder,
                            const ResidualNormType& resNormType,
                            const std::vector<Real>& tol,
                            const std::vector<int>& CellGroups,
                            const std::vector<int>& InteriorTraceGroups,
                            PyDict& BCList,
                            const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                            BCArgs&... args ):
  AlgebraicEquationSet_DGBR2(xfld, flds.qfld, flds.rfld, flds.lgfld, pLiftedQuantityfld, pde, disc, quadratureOrder,
                             resNormType, tol, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args...) {}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::~AlgebraicEquationSet_DGBR2() {}

//Fills jacobian or the non-zero pattern of a jacobian
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobian(SystemMatrixView& mtx)
{
  if (AES_ != nullptr) AES_->jacobian(mtx);
  this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );

//  SLA::WriteMatrixMarketFile(mtx(0,0), "tmp/jac_full_00.mtx");
//  SLA::WriteMatrixMarketFile(mtx(0,1), "tmp/jac_full_01.mtx");
//  SLA::WriteMatrixMarketFile(mtx(1,0), "tmp/jac_full_10.mtx");
//  SLA::WriteMatrixMarketFile(mtx(1,1), "tmp/jac_full_11.mtx");
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobian(SystemNonZeroPatternView& nz)
{
  if (AES_ != nullptr) AES_->jacobian(nz);
  this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
}

//Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobianTranspose(SystemMatrixView& mtxT)
{
  if (AES_ != nullptr) AES_->jacobianTranspose(mtxT);
  jacobian(Transpose(mtxT), quadratureOrder_ );
}
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobianTranspose(SystemNonZeroPatternView& nzT)
{
  if (AES_ != nullptr) AES_->jacobianTranspose(nzT);
  jacobian(Transpose(nzT), quadratureOrderMin_ );
}


//Evaluate Residual Norm
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
std::vector<std::vector<Real>>
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
residualNorm( const SystemVectorView& rsd ) const
{
  const int nDOFPDEpossessed = rsd[iPDE].m();
  const int nDOFBC = rsd[iBC].m();
  const int nMon = pde_.nMonitor();

  DLA::VectorD<Real> rsdPDEtmp(nMon);
  DLA::VectorD<Real> rsdBCtmp(nMon);

  rsdPDEtmp = 0;
  rsdBCtmp = 0;

  std::vector<std::vector<Real>> rsdNorm(nResidNorm(), std::vector<Real>(nMon, 0));
  std::vector<KahanSum<Real>> rsdNormKahan(nMon, 0);

  //PDE residual norm
  if (resNormType_ == ResidualNorm_L2 || resNormType_ == ResidualNorm_L2_DOFWeighted)
  {
    for (int n = 0; n < nDOFPDEpossessed; n++)
    {
      pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = rsdNormKahan[j];

#ifdef SANS_MPI
    rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
#endif

    if (resNormType_ == ResidualNorm_L2)
    {
      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);
    }
    else
    {
      int qDOFnative = qfld_.nDOFnative();

      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]/qDOFnative);
    }
  }
  else if (resNormType_ == ResidualNorm_L2_DOFAvg)
  {
    int nDOFTotal = nDOFPDEpossessed;
    for (int n = 0; n < nDOFPDEpossessed; n++)
    {
      pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = rsdNormKahan[j];

#ifdef SANS_MPI
    nDOFTotal = boost::mpi::all_reduce( *qfld_.comm(), nDOFTotal, std::plus<Real>() );
    rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
#endif

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]/nDOFTotal);
  }
  else if (resNormType_ == ResidualNorm_InvJac_DOF_Weighted)
  {
    // compute residual norms weighted by the elemental jacobian determinant
    Real invJ = 0;
    int nDOFPDE = nDOFPDEpossessed;
    for_each_CellGroup<TopoDim>::apply(
        JacobianDetInvResidualNorm(qfld_.comm()->rank(), fcnCell_.cellGroups(), pde_, rsd[iPDE], rsdNormKahan, invJ),
        (get<-1>(xfld_), qfld_) );

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = rsdNormKahan[j];

#ifdef SANS_MPI
    rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
    nDOFPDE = boost::mpi::all_reduce(*qfld_.comm(), nDOFPDE, std::plus<int>());
#endif

    //Normalize by number of DOF
    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j] / nDOFPDE);
  }
  else if (resNormType_ == ResidualNorm_InvJac_RelWeighted)
  {
    // compute residual norms weighted by the elemental jacobian determinant
    Real invJ = 0;
    for_each_CellGroup<TopoDim>::apply(
        JacobianDetInvResidualNorm(qfld_.comm()->rank(), fcnCell_.cellGroups(), pde_, rsd[iPDE], rsdNormKahan, invJ),
        (get<-1>(xfld_), qfld_) );

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = rsdNormKahan[j];

#ifdef SANS_MPI
    rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
    invJ = boost::mpi::all_reduce(*qfld_.comm(), invJ, std::plus<Real>());
#endif

    //Normalize by sum of inverse jacobian determinants
    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]) / invJ;
  }
  else
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_DGBR2::residualNorm - Unknown residual norm type!");

  //BC residual norm
  for (int n = 0; n < nDOFBC; n++)
  {
    pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

  return rsdNorm;
}

// prints a residual decrease failure
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const
{
  titles = {"PDE : ",
            "BC  : "};
  idx = {iPDE, iBC};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
residual(SystemVectorView& rsd)
{
  if (AES_ != nullptr) AES_->residual(rsd);

  if (!pde_.hasSourceLiftedQuantity())
  {
    IntegrateCellGroups<TopoDim>::integrate( ResidualCell_DGBR2(fcnCell_, rsd(iq)),
                                             xfld_, (qfld_, rfld_),
                                             quadratureOrder_.cellOrders.data(),
                                             quadratureOrder_.cellOrders.size() );
  }
  else
  {
    IntegrateCellGroups<TopoDim>::integrate( ResidualCell_DGBR2(fcnCell_, rsd(iq)),
                                             xfld_, (qfld_, rfld_, *pLiftedQuantityfld_),
                                             quadratureOrder_.cellOrders.data(),
                                             quadratureOrder_.cellOrders.size() );
  }

  IntegrateInteriorTraceGroups<TopoDim>::integrate(ResidualInteriorTrace_DGBR2(fcnTrace_, rsd(iq)),
                                                   xfld_, (qfld_, rfld_),
                                                   quadratureOrder_.interiorTraceOrders.data(),
                                                   quadratureOrder_.interiorTraceOrders.size() );

  IntegratePeriodicTraceGroups<TopoDim>::integrate(ResidualInteriorTrace_DGBR2(fcnTrace_, rsd(iq)),
                                                       xfld_, (qfld_, rfld_),
                                                       quadratureOrder_.boundaryTraceOrders.data(),
                                                       quadratureOrder_.boundaryTraceOrders.size() );

  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin( xfld_, qfld_, lgfld_,
                                                          quadratureOrder_.boundaryTraceOrders.data(),
                                                          quadratureOrder_.boundaryTraceOrders.size(),
                                                          rsd(iq), rsd(ilg) ),
      ResidualBoundaryTrace_Dispatch_DGBR2( xfld_, qfld_, rfld_,
                                            quadratureOrder_.boundaryTraceOrders.data(),
                                            quadratureOrder_.boundaryTraceOrders.size(),
                                            rsd(iq) ),
      ResidualBoundaryTrace_Dispatch_Galerkin( xfld_, qfld_,
                                               quadratureOrder_.boundaryTraceOrders.data(),
                                               quadratureOrder_.boundaryTraceOrders.size(),
                                               rsd(iq) ) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobian( SparseMatrixType jac, const QuadratureOrder& quadratureOrder,
          const std::vector<int>& interiorTraceCellJac )
{
  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE,iq);
  Matrix jacPDE_lg = jac(iPDE,ilg);

  Matrix jacBC_q   = jac(iBC,iq);
  Matrix jacBC_lg  = jac(iBC,ilg);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld_); // Lifting operator jacobian from cell integral

  if ( pde_.hasSourceLiftedQuantity() )
  {
    SANS_ASSERT_MSG(pLiftedQuantityfld_ != nullptr,
                    "AlgebraicEquationSet_DGBR2::jacobian - The lifted quantity field has not been initialized!");

    SANS_ASSERT_MSG(pmmfldLiftedQuantity_ != nullptr,
                    "AlgebraicEquationSet_DGBR2::jacobian - The lifted quantity mass-matrix field has not been initialized!");

    typedef typename MatrixLiftedQuantity<MatrixQ>::type MatrixT;
    std::shared_ptr<FieldDataMatrixD_Cell<MatrixT>> pjacPDE_liftedQuantity; //PDE jacobian wrt lifted quantity

    pjacPDE_liftedQuantity = std::make_shared<FieldDataMatrixD_Cell<MatrixT>>(qfld_, *pLiftedQuantityfld_);

    IntegrateCellGroups<TopoDim>::integrate(
        JacobianCell_DGBR2(fcnCell_, jacPDE_q, jacPDE_R, pjacPDE_liftedQuantity),
        xfld_, (qfld_, rfld_, *pLiftedQuantityfld_),
        quadratureOrder.cellOrders.data(),
        quadratureOrder.cellOrders.size() );

    IntegrateInteriorTraceGroups<TopoDim>::integrate(
        JacobianInteriorTrace_DGBR2_LiftedQuantity(fcnTrace_, *pmmfldLiftedQuantity_, *pjacPDE_liftedQuantity,
                                                   jacPDE_q, interiorTraceCellJac),
        xfld_, (qfld_, *pLiftedQuantityfld_),
        quadratureOrder.interiorTraceOrders.data(),
        quadratureOrder.interiorTraceOrders.size() );
  }
  else
  {
    IntegrateCellGroups<TopoDim>::integrate(
        JacobianCell_DGBR2(fcnCell_, jacPDE_q, jacPDE_R),
        xfld_, (qfld_, rfld_),
        quadratureOrder.cellOrders.data(),
        quadratureOrder.cellOrders.size() );
  }

  IntegrateInteriorTraceGroups<TopoDim>::integrate(
      JacobianInteriorTrace_DGBR2(fcnTrace_, mmfld_, jacPDE_R, jacPDE_q, interiorTraceCellJac),
      xfld_, (qfld_, rfld_),
      quadratureOrder.interiorTraceOrders.data(),
      quadratureOrder.interiorTraceOrders.size() );

  IntegratePeriodicTraceGroups<TopoDim>::integrate(
      JacobianInteriorTrace_DGBR2(fcnTrace_, mmfld_, jacPDE_R, jacPDE_q, interiorTraceCellJac),
      xfld_, (qfld_, rfld_),
      quadratureOrder.boundaryTraceOrders.data(),
      quadratureOrder.boundaryTraceOrders.size() );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_mitLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_, lgfld_,
                                                                   quadratureOrder.boundaryTraceOrders.data(),
                                                                   quadratureOrder.boundaryTraceOrders.size(),
                                                                   jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg ),
      JacobianBoundaryTrace_Dispatch_DGBR2<SurrealClass>( xfld_, qfld_, rfld_, jacPDE_R,
                                                          quadratureOrder.boundaryTraceOrders.data(),
                                                          quadratureOrder.boundaryTraceOrders.size(),
                                                          jacPDE_q ),
      JacobianBoundaryTrace_sansLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_,
                                                                    quadratureOrder.boundaryTraceOrders.data(),
                                                                    quadratureOrder.boundaryTraceOrders.size(),
                                                                    jacPDE_q ) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
template<int iParam, class SparseMatrixType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const
{
  SANS_ASSERT(jac.m() >= 2);
  SANS_ASSERT(jac.n() >  ip);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_p = jac(iPDE,ip);
  Matrix jacBC_p  = jac(iBC,ip);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_DGBR2_Param<SurrealClass, iParam>(fcnCell_, jacPDE_p),
      xfld_, (qfld_, rfld_), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  IntegrateInteriorTraceGroups<TopoDim>::integrate(
      JacobianInteriorTrace_DGBR2_Param<SurrealClass, iParam>(fcnTrace_, jacPDE_p),
      xfld_, (qfld_, rfld_), quadratureOrder.interiorTraceOrders.data(), quadratureOrder.interiorTraceOrders.size());

  IntegratePeriodicTraceGroups<TopoDim>::integrate(
      JacobianInteriorTrace_DGBR2_Param<SurrealClass, iParam>(fcnTrace_, jacPDE_p),
      xfld_, (qfld_, rfld_), quadratureOrder.boundaryTraceOrders.data(), quadratureOrder.boundaryTraceOrders.size());

  dispatchBC_.dispatch(
    JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_Param<SurrealClass, iParam>( xfld_, qfld_, lgfld_,
                                                                               quadratureOrder.boundaryTraceOrders.data(),
                                                                               quadratureOrder.boundaryTraceOrders.size(),
                                                                               jacPDE_p, jacBC_p ),
    JacobianBoundaryTrace_Dispatch_DGBR2_Param<SurrealClass, iParam>( xfld_, qfld_, rfld_,
                                                                      quadratureOrder.boundaryTraceOrders.data(),
                                                                      quadratureOrder.boundaryTraceOrders.size(),
                                                                      jacPDE_p ),
    JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_Param<SurrealClass, iParam>( xfld_, qfld_,
                                                                                quadratureOrder.boundaryTraceOrders.data(),
                                                                                quadratureOrder.boundaryTraceOrders.size(),
                                                                                jacPDE_p ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
setSolutionField(const SystemVectorView& q)
{
  if (AES_ != nullptr) AES_->setSolutionField(q);

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    lgfld_.DOF(k) = q[ilg][k];

  computeLiftingOperators();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
computeLiftingOperators()
{
  // Compute the lifting operator field
  IntegrateInteriorTraceGroups<TopoDim>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcnTrace_, mmfld_),
                                                   xfld_, (qfld_, rfld_),
                                                   quadratureOrder_.interiorTraceOrders.data(),
                                                   quadratureOrder_.interiorTraceOrders.size() );

  IntegratePeriodicTraceGroups<TopoDim>::integrate(SetFieldInteriorTrace_DGBR2_LiftingOperator(fcnTrace_, mmfld_),
                                                   xfld_, (qfld_, rfld_),
                                                   quadratureOrder_.boundaryTraceOrders.data(),
                                                   quadratureOrder_.boundaryTraceOrders.size() );

  dispatchBC_.dispatch_DGBR2(
      SetFieldBoundaryTrace_Dispatch_LiftingOperator( xfld_, qfld_, rfld_, mmfld_,
                                                      quadratureOrder_.boundaryTraceOrders.data(),
                                                      quadratureOrder_.boundaryTraceOrders.size() ) );

  // Compute the lifted quantity field if needed by source terms
  if ( pde_.hasSourceLiftedQuantity() )
  {
    SANS_ASSERT_MSG(pLiftedQuantityfld_ != nullptr,
                    "AlgebraicEquationSet_DGBR2::computeLiftingOperators - The lifted quantity field has not been initialized!");

    SANS_ASSERT_MSG(pmmfldLiftedQuantity_ != nullptr,
                    "AlgebraicEquationSet_DGBR2::computeLiftingOperators - The lifted quantity mass-matrix field has not been initialized!");

    *pLiftedQuantityfld_ = 0.0; //This is important! The lifted quantity is calculated by accumulation.
    IntegrateInteriorTraceGroups<TopoDim>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcnTrace_, *pmmfldLiftedQuantity_),
                                                     xfld_, (qfld_, *pLiftedQuantityfld_),
                                                     quadratureOrder_.interiorTraceOrders.data(),
                                                     quadratureOrder_.interiorTraceOrders.size() );

    IntegratePeriodicTraceGroups<TopoDim>::integrate(SetFieldInteriorTrace_DGBR2_LiftedQuantity(fcnTrace_, *pmmfldLiftedQuantity_),
                                                     xfld_, (qfld_, *pLiftedQuantityfld_),
                                                     quadratureOrder_.boundaryTraceOrders.data(),
                                                     quadratureOrder_.boundaryTraceOrders.size() );

  }
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
setAdjointField(const SystemVectorView& adj,
                Field_DG_Cell<PhysDim, TopoDim, ArrayQ>&           wfld,
                FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& sfld,
                Field<PhysDim, TopoDim, ArrayQ>&                   mufld )
{
  if (AES_ != nullptr) SANS_DEVELOPER_EXCEPTION("Don't know how to chain the adjoint yet....");

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = wfld.nDOFpossessed() + wfld.nDOFghost();
  SANS_ASSERT( nDOFPDE == adj[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    wfld.DOF(k) = adj[iq][k];

  const int nDOFBC = mufld.nDOFpossessed() + mufld.nDOFghost();
  SANS_ASSERT( nDOFBC == adj[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    mufld.DOF(k) = adj[ilg][k];

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  FieldDataMatrixD_CellLift<RowMatrixQ> jacPDE_R(rfld_); // Lifting operator jacobian from cell integral

  // PDE jacobian wrt R (this should be saved off somehow to improve efficiency)
  if ( pde_.needsSolutionGradientforSource() )
  {
    if ( pde_.hasSourceLiftedQuantity() )
    {
      SANS_ASSERT_MSG(pLiftedQuantityfld_ != nullptr,
                      "AlgebraicEquationSet_DGBR2::setAdjointField - The lifted quantity field has not been initialized!");

      IntegrateCellGroups<TopoDim>::integrate( JacobianCell_DGBR2_LiftingOperator(fcnCell_, jacPDE_R),
                                               xfld_, (qfld_, rfld_, *pLiftedQuantityfld_),
                                               quadratureOrder_.cellOrders.data(),
                                               quadratureOrder_.cellOrders.size() );
    }
    else
    {
      IntegrateCellGroups<TopoDim>::integrate( JacobianCell_DGBR2_LiftingOperator(fcnCell_, jacPDE_R),
                                               xfld_, (qfld_, rfld_),
                                               quadratureOrder_.cellOrders.data(),
                                               quadratureOrder_.cellOrders.size() );
    }
  }
  else
    jacPDE_R = 0;

  // Compute the lifting operator field
  IntegrateInteriorTraceGroups<TopoDim>::integrate(
      SetFieldInteriorTrace_DGBR2_AdjointLO(fcnTrace_, mmfld_, jacPDE_R),
      xfld_, (qfld_, rfld_, wfld, sfld),
      quadratureOrder_.interiorTraceOrders.data(),
      quadratureOrder_.interiorTraceOrders.size() );

  dispatchBC_.dispatch_DGBR2(
      SetFieldBoundaryTrace_Dispatch_AdjointLO<SurrealClass>( xfld_, qfld_, rfld_, wfld, sfld, jacPDE_R,
                                                              quadratureOrder_.boundaryTraceOrders.data(),
                                                              quadratureOrder_.boundaryTraceOrders.size() ) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
fillSystemVector(SystemVectorView& q) const
{
  if (AES_ != nullptr) AES_->fillSystemVector(q);
  fillSystemVector(q, qfld_, lgfld_);
}
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
fillSystemVector(SystemVectorView& q,
                 const Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                 const Field<PhysDim, TopoDim, ArrayQ>& lgfld) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld.nDOFpossessed() + qfld.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld.DOF(k);

  const int nDOFBC = lgfld.nDOFpossessed() + lgfld.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld.DOF(k);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
typename AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::VectorSizeClass
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  return {nDOFPDEpos,
          nDOFBCpos};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
typename AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::VectorSizeClass
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
vectorStateSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {nDOFPDE,
          nDOFBC};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
typename AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");
  static_assert(iq == 0,"");
  static_assert(ilg == 1,"");

  // jacobian nonzero pattern
  //
  //           q  lg
  //   PDE     X   X
  //   BC      X   0

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {{ {nDOFPDEpos, nDOFPDE}, {nDOFPDEpos, nDOFBC} },
          { {nDOFBCpos , nDOFPDE}, {nDOFBCpos , nDOFBC} }};
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
Real
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const
{
  Field<PhysDim, TopoDim, ArrayQ> dqfld(qfld_, FieldCopy());
  Field<PhysDim, TopoDim, ArrayQ> dlgfld(lgfld_, FieldCopy());

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  SANS_ASSERT( nDOFPDE == dq[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
  {
    qfld_.DOF(k) = q[iq][k];
    dqfld.DOF(k) = dq[iq][k];
  }

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  SANS_ASSERT( nDOFBC == dq[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
  {
    lgfld_.DOF(k) = q[ilg][k];
    dlgfld.DOF(k) = dq[ilg][k];
  }

  Real updateFraction = 1;

  IntegrateCellGroups<TopoDim>::integrate( UpdateFractionCell(pde_, fcnCell_.cellGroups(), maxChangeFraction, updateFraction),
                                           xfld_, (qfld_, dqfld),
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );

  IntegrateInteriorTraceGroups<TopoDim>::integrate( UpdateFractionInteriorTrace(pde_, fcnTrace_.interiorTraceGroups(),
                                                                                maxChangeFraction, updateFraction),
                                                    xfld_, (qfld_, dqfld),
                                                    quadratureOrder_.interiorTraceOrders.data(),
                                                    quadratureOrder_.interiorTraceOrders.size() );

  IntegratePeriodicTraceGroups<TopoDim>::integrate( UpdateFractionInteriorTrace(pde_, fcnTrace_.periodicTraceGroups(),
                                                                                maxChangeFraction, updateFraction),
                                                    xfld_, (qfld_, dqfld),
                                                    quadratureOrder_.boundaryTraceOrders.data(),
                                                    quadratureOrder_.boundaryTraceOrders.size() );

  dispatchBC_.dispatch(
      UpdateFractionBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, dqfld, lgfld_, dlgfld,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size(), maxChangeFraction, updateFraction ),
      UpdateFractionBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_, dqfld,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size(), maxChangeFraction, updateFraction )
    );

  if (AES_ != nullptr)
    updateFraction = MIN(updateFraction, AES_->updateFraction(q, dq, maxChangeFraction) );

#ifdef SANS_MPI
  return boost::mpi::all_reduce(*qfld_.comm(), updateFraction, boost::mpi::minimum<Real>());
#else
  return updateFraction;
#endif
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
bool
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  // Update the solution field
  setSolutionField(q);

  bool isValidState = true;

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                           xfld_, qfld_,
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );

  IntegrateInteriorTraceGroups<TopoDim>::integrate( isValidStateInteriorTrace(pde_, fcnTrace_.interiorTraceGroups(), isValidState),
                                                    xfld_, qfld_,
                                                    quadratureOrder_.interiorTraceOrders.data(),
                                                    quadratureOrder_.interiorTraceOrders.size() );

  IntegratePeriodicTraceGroups<TopoDim>::integrate( isValidStateInteriorTrace(pde_, fcnTrace_.periodicTraceGroups(), isValidState),
                                                    xfld_, qfld_,
                                                    quadratureOrder_.boundaryTraceOrders.data(),
                                                    quadratureOrder_.boundaryTraceOrders.size() );

  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                quadratureOrder_.boundaryTraceOrders.size(), isValidState ),
      isValidStateBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size(), isValidState )
    );

#ifdef SANS_MPI
  int validstate = isValidState ? 1 : 0;
  isValidState = (boost::mpi::all_reduce(*qfld_.comm(), validstate, std::plus<int>()) == qfld_.comm()->size());
#endif

  bool checkAES_daisychain = true;
  if (AES_ != nullptr) checkAES_daisychain = AES_->isValidStateSystemVector(q);

  return (isValidState && checkAES_daisychain);
}

// Add an AES to this one to daisy-chain
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
addAlgebraicEquationSet(std::shared_ptr<BaseType> AES)
{
  AES_ = AES;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
std::vector<GlobalContinuousMap>
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
continuousGlobalMap() const
{
  return {qfld_.continuousGlobalMap(0, lgfld_.nDOFpossessed()),
          lgfld_.continuousGlobalMap(qfld_.nDOFpossessed(), 0)};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
std::shared_ptr<mpi::communicator>
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
comm() const
{
  return qfld_.comm();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
syncDOFs_MPI()
{
  // can't sync rfld as it lacks syncDOF for the time being
  qfld_.syncDOFs_MPI_Cached();
  lgfld_.syncDOFs_MPI_Cached();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
int
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
nResidNorm() const
{
  return 2;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_DGBR2<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);

//  std::string filename_iBC = filenamebase + "_iBC_iter" + std::to_string(nonlinear_iter) + ".plt";
//  this->dumpLinesearchField(lgfld_, *pStepData, iBC, filename_iBC);
}

} //namespace SANS


#include <boost/preprocessor/cat.hpp>

// Helper macros to reduce the amount of work needed to instantiate AlgebraicEquationSet_DGBR2
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_TRAITS(PDEND, BCNDCONVERT, BCVECTOR, TOPODIM, TRAITS, DISCTAG, PARAMFIELDTUPLE ) \
template class AlgebraicEquationSet_DGBR2< PDEND, BCNDCONVERT, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE >; \
\
template AlgebraicEquationSet_DGBR2< PDEND, BCNDCONVERT, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE >:: \
         AlgebraicEquationSet_DGBR2(const PARAMFIELDTUPLE& xfld, \
                                    Field_DG_Cell<PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real> >& qfld, \
                                    FieldLift_DG_Cell<PDEND::PhysDim, TOPODIM, PDEND::VectorArrayQ<Real> >& rfld, \
                                    Field<PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real> >& lgfld, \
                                    std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld, \
                                    const PDEND& pde, \
                                    const DiscretizationDGBR2& disc, \
                                    const QuadratureOrder& quadratureOrder, \
                                    const ResidualNormType& resNormType, \
                                    const std::vector<Real>& tol, \
                                    const std::vector<int>& CellGroups, \
                                    const std::vector<int>& InteriorTraceGroups, \
                                    PyDict& BCList, \
                                    const std::map< std::string, std::vector<int> >& BCBoundaryGroups ); \
\
template AlgebraicEquationSet_DGBR2< PDEND, BCNDCONVERT, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE >:: \
         AlgebraicEquationSet_DGBR2(const PARAMFIELDTUPLE& xfld, \
                                    Field_DG_Cell<PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real> >& qfld, \
                                    FieldLift_DG_Cell<PDEND::PhysDim, TOPODIM, PDEND::VectorArrayQ<Real> >& rfld, \
                                    Field<PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real> >& lgfld, \
                                    const PDEND& pde, \
                                    const DiscretizationDGBR2& disc, \
                                    const QuadratureOrder& quadratureOrder, \
                                    const ResidualNormType& resNormType, \
                                    const std::vector<Real>& tol, \
                                    const std::vector<int>& CellGroups, \
                                    const std::vector<int>& InteriorTraceGroups, \
                                    PyDict& BCList, \
                                    const std::map< std::string, std::vector<int> >& BCBoundaryGroups ); \
\
template AlgebraicEquationSet_DGBR2< PDEND, BCNDCONVERT, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE >:: \
         AlgebraicEquationSet_DGBR2(const PARAMFIELDTUPLE& xfld, \
                                    FieldBundleBase& flds, \
                                    std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld, \
                                    const PDEND& pde, \
                                    const DiscretizationDGBR2& disc, \
                                    const QuadratureOrder& quadratureOrder, \
                                    const ResidualNormType& resNormType, \
                                    const std::vector<Real>& tol, \
                                    const std::vector<int>& CellGroups, \
                                    const std::vector<int>& InteriorTraceGroups, \
                                    PyDict& BCList, \
                                    const std::map< std::string, std::vector<int> >& BCBoundaryGroups );


#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_UNSTEADY_TRAITS(PDEND, BCVECTOR, TOPODIM, TRAITS, DISCTAG, PARAMFIELDTUPLE ) \
\
template AlgebraicEquationSet_DGBR2< PDEND, BCNDConvertSpace, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE >:: \
         AlgebraicEquationSet_DGBR2(const PARAMFIELDTUPLE& xfld, \
                                    Field_DG_Cell<    PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& qfld, \
                                    FieldLift_DG_Cell<PDEND::PhysDim, TOPODIM, PDEND::VectorArrayQ<Real> >& rfld, \
                                    Field<            PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& lgfld, \
                                    std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld, \
                                    const PDEND& pde, \
                                    const DiscretizationDGBR2& disc, \
                                    const QuadratureOrder& quadratureOrder, \
                                    const ResidualNormType& resNormType, \
                                    const std::vector<Real>& tol, \
                                    const std::vector<int>& CellGroups, \
                                    const std::vector<int>& InteriorTraceGroups, \
                                    PyDict& BCList, \
                                    const std::map< std::string, std::vector<int> >& BCBoundaryGroups, GlobalTime& ); \
\
template AlgebraicEquationSet_DGBR2< PDEND, BCNDConvertSpace, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE >:: \
         AlgebraicEquationSet_DGBR2(const PARAMFIELDTUPLE& xfld, \
                                    Field_DG_Cell<    PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& qfld, \
                                    FieldLift_DG_Cell<PDEND::PhysDim, TOPODIM, PDEND::VectorArrayQ<Real> >& rfld, \
                                    Field<            PDEND::PhysDim, TOPODIM, PDEND::ArrayQ<Real>       >& lgfld, \
                                    const PDEND& pde, \
                                    const DiscretizationDGBR2& disc, \
                                    const QuadratureOrder& quadratureOrder, \
                                    const ResidualNormType& resNormType, \
                                    const std::vector<Real>& tol, \
                                    const std::vector<int>& CellGroups, \
                                    const std::vector<int>& InteriorTraceGroups, \
                                    PyDict& BCList, \
                                    const std::map< std::string, std::vector<int> >& BCBoundaryGroups, GlobalTime& ); \
\
template AlgebraicEquationSet_DGBR2< PDEND, BCNDConvertSpace, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE >:: \
         AlgebraicEquationSet_DGBR2(const PARAMFIELDTUPLE& xfld, \
                                    FieldBundleBase& flds, \
                                    std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld, \
                                    const PDEND& pde, \
                                    const DiscretizationDGBR2& disc, \
                                    const QuadratureOrder& quadratureOrder, \
                                    const ResidualNormType& resNormType, \
                                    const std::vector<Real>& tol, \
                                    const std::vector<int>& CellGroups, \
                                    const std::vector<int>& InteriorTraceGroups, \
                                    PyDict& BCList, \
                                    const std::map< std::string, std::vector<int> >& BCBoundaryGroups, GlobalTime& );

//---------------------------------------------------------------------------//
// This is the main macro used for steady instantiation
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDE, BCVECTOR, TOPODIM, TRAITS, DISCTAG, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpace<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpace); \
\
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_TRAITS( BOOST_PP_CAT(PDE, NDSpace), BCNDConvertSpace, BCVECTOR, TOPODIM, TRAITS, DISCTAG, PARAMFIELDTUPLE ) \
\
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_UNSTEADY_TRAITS( BOOST_PP_CAT(PDE, NDSpace), BCVECTOR, TOPODIM, TRAITS, DISCTAG, PARAMFIELDTUPLE )

//---------------------------------------------------------------------------//
// This is the main macro used for space-time instantiation
#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDE, BCVECTOR, TOPODIM, TRAITS, DISCTAG, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpaceTime<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpaceTime); \
\
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_TRAITS( BOOST_PP_CAT(PDE,NDSpaceTime),BCNDConvertSpaceTime,BCVECTOR,TOPODIM,TRAITS,DISCTAG,PARAMFIELDTUPLE )


//===========================================================================//
// The JacobianParam macros assume that the AlgebraicEquationSet_DGBR2 is already instantiated

//---------------------------------------------------------------------------//
// Steady parameter jacobians
#define JACOBIANPARAM_DGBR2_INSTANTIATE_SPACE( IPARAM, PDE, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE ) \
\
template class JacobianParam<IPARAM, \
                             AlgebraicEquationSet_DGBR2< BOOST_PP_CAT(PDE, NDSpace), BCNDConvertSpace, BCVECTOR, \
                             TRAITS, DISCTAG, PARAMFIELDTUPLE >>;

//---------------------------------------------------------------------------//
// Space-time parameter jacobians
#define JACOBIANPARAM_DGBR2_INSTANTIATE_SPACETIME( IPARAM, PDE, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE ) \
\
template class JacobianParam<IPARAM, \
                             AlgebraicEquationSet_DGBR2< BOOST_PP_CAT(PDE, NDSpaceTime), BCNDConvertSpaceTime, BCVECTOR, \
                             TRAITS, DISCTAG, PARAMFIELDTUPLE >>;
