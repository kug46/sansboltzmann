// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_LOCAL_DG_H
#define ALGEBRAICEQUATIONSET_LOCAL_DG_H

#include "tools/SANSnumerics.h"

#include "Python/PyDict.h"

#include "Field/FieldTypes.h"
#include "Field/XField.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

#include "Discretization/QuadratureOrder.h"
#include "Discretization/ResidualNormType.h"

#include "DiscretizationDGBR2.h"

namespace SANS
{

/* This class refers to two types of local solutions/systems, full and sub.
 * The full local solution contains all the solution DOFs in the local mesh:
 *  - qfld DOFs in both cellgroups 0 and 1
 *  - all lgfld DOFs
 *
 * The sub local solution contains only the DOFs that are solved for in the local solve:
 *  - qfld DOFs in only cellgroup 0 (i.e. in main-cells)
 *  - lgfld DOFs of only the main boundary traces (no outer boundary traces)
 */
//

//----------------------------------------------------------------------------//
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
class AlgebraicEquationSet_Local_DG :
    public AlgebraicEquationSetBaseType<NDPDEClass_, BCNDConvert, BCVector, AlgEqSetTraits_Dense, DiscTag, XFieldType>
{
public:
  typedef NDPDEClass_ NDPDEClass;

  typedef AlgebraicEquationSetBaseType<NDPDEClass, BCNDConvert, BCVector, AlgEqSetTraits_Dense, DiscTag, XFieldType> BaseType;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename BaseType::VectorSizeClass VectorSizeClass;
  typedef typename BaseType::MatrixSizeClass MatrixSizeClass;

  typedef typename BaseType::SystemMatrix SystemMatrix;
  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::BCParams BCParams;
  typedef typename BaseType::FieldBundle_Local FieldBundle_Local;

  // DGBR2 specific constructor with lifted quantity field
  template< class... BCArgs >
  AlgebraicEquationSet_Local_DG(const XFieldType& xfld,
                                Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                const NDPDEClass& pde,
                                const DiscretizationDGBR2& disc,
                                const QuadratureOrder& quadratureOrder,
                                const ResidualNormType& resNormType,
                                std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                const std::vector<int>& InteriorTraceGroups,
                                PyDict& BCList,
                                const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                BCArgs&... args )
   : BaseType(xfld, qfld, rfld, lgfld, pLiftedQuantityfld, pde, disc, quadratureOrder, resNormType, tol,
              CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args... )
  {
    init();
  }

  // DGBR2 specific constructor without lifted quantity field
  template< class... BCArgs >
  AlgebraicEquationSet_Local_DG(const XFieldType& xfld,
                                Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                const NDPDEClass& pde,
                                const DiscretizationDGBR2& disc,
                                const QuadratureOrder& quadratureOrder,
                                const ResidualNormType& resNormType,
                                std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                const std::vector<int>& InteriorTraceGroups,
                                PyDict& BCList,
                                const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                BCArgs&... args )
   : AlgebraicEquationSet_Local_DG(xfld, qfld, rfld, lgfld, {}, pde, disc, quadratureOrder, resNormType, tol,
                                   CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args... ) {}

  // DGAdvective specific constructor
  template< class... BCArgs >
  AlgebraicEquationSet_Local_DG(const XFieldType& xfld,
                                Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                const NDPDEClass& pde,
                                const QuadratureOrder& quadratureOrder,
                                const ResidualNormType& resNormType,
                                std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                const std::vector<int>& InteriorTraceGroups,
                                PyDict& BCList,
                                const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                BCArgs&... args )
   : BaseType(xfld, qfld, lgfld, pde, quadratureOrder, resNormType, tol,
              CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args... )
  {
    init();
  }

  // General constructor with FieldBundles
  template< class... BCArgs >
  AlgebraicEquationSet_Local_DG(const XFieldType& xfld,
                                FieldBundle_Local& flds,
                                std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                const NDPDEClass& pde,
                                const DiscretizationDGBR2& disc,
                                const QuadratureOrder& quadratureOrder,
                                const ResidualNormType& resNormType,
                                std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                const std::vector<int>& InteriorTraceGroups,
                                PyDict& BCList,
                                const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                BCArgs&... args )
    // Implictly casts the Local Fieldbundle to the Base type that AlgebraicEqSet takes
    : BaseType(xfld, flds, pLiftedQuantityfld, pde, disc, quadratureOrder, resNormType, tol,
               CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args... )
  {
    init();
  }

  // General constructor with FieldBundles for DGAdvective
  template< class... BCArgs >
  AlgebraicEquationSet_Local_DG(const XFieldType& xfld,
                                FieldBundle_Local& flds,
                                std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                                const NDPDEClass& pde,
                                const DiscretizationObject& disc,
                                const QuadratureOrder& quadratureOrder,
                                const ResidualNormType& resNormType,
                                std::vector<Real>& tol,
                                const std::vector<int>& CellGroups,
                                const std::vector<int>& InteriorTraceGroups,
                                PyDict& BCList,
                                const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                                BCArgs&... args )
    // Implictly casts the Local Fieldbundle to the Base type that AlgebraicEqSet takes
    : BaseType(xfld, flds, pLiftedQuantityfld, pde, disc, quadratureOrder, resNormType, tol,
               CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args... )
  {
    init();
  }

  virtual ~AlgebraicEquationSet_Local_DG() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& jac        ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz ) override {}

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  // Returns the vector and matrix sizes needed for the sub-system (local solve system)
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the size of the full vector (i.e. all elements in the local mesh)
  VectorSizeClass fullVectorSize() const;

  //Translates the sub-system vector into a local solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the sub-solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  const XFieldType& paramfld() const { return xfld_; }

  using BaseType::iPDE;
  using BaseType::iBC;
  using BaseType::iq;
  using BaseType::ilg;

protected:
  void init();

  int sub_qfld_nDOF_, sub_lgfld_nDOF_; //DOF counts for sub-system unknowns

  void subResidual(const SystemVectorView& rsd_full, SystemVectorView& rsd_sub);

  template<class SparseMatrixType>
  void subJacobian(const SparseMatrixType& jac_full, SparseMatrixType& jac_sub);

  using BaseType::xfld_;
  using BaseType::qfld_;
  using BaseType::lgfld_;
  using BaseType::quadratureOrder_;
  using BaseType::resNormType_;
};

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_LOCAL_DG_H
