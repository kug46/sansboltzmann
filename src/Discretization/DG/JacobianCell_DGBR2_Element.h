// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_DGBR2_ELEMENT_H
#define JACOBIANCELL_DGBR2_ELEMENT_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Discretization/JacobianElementMatrix.h"
#include "Discretization/MatrixLiftedQuantity.h"

namespace SANS
{


template<class PhysDim, class MatrixQ>
struct JacobianElemCell_DGBR2 : JacElemMatrixType< JacobianElemCell_DGBR2<PhysDim,MatrixQ> >
{
  // PDE Jacobian wrt q
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // PDE Jacobian wrt r
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  // PDE Jacobian wrt lifted quantity
  typedef typename MatrixLiftedQuantity<MatrixQ>::type MatrixT;
  typedef DLA::MatrixD<MatrixT> MatrixElemLiftedScalarClass;

  JacobianElemCell_DGBR2(const int qDOF, const int sDOF = 0)
   : qDOF(qDOF),
     sDOF(sDOF),
     PDE_q(qDOF, qDOF),
     PDE_R(qDOF, qDOF),
     PDE_s(qDOF, sDOF)
  {}

  const int qDOF, sDOF;

  // element PDE jacobian matrices wrt q
  MatrixElemClass PDE_q;

  // element PDE jacobian matrices wrt r (lifting operator)
  MatrixRElemClass PDE_R;

  // element PDE jacobian matrices wrt lifted quantity
  MatrixElemLiftedScalarClass PDE_s;

  inline Real operator=( const Real s )
  {
    PDE_q = s;
    PDE_R = s;
    PDE_s = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemCell_DGBR2& operator+=(
      const JacElemMulScalar< JacobianElemCell_DGBR2 >& mul )
  {
    PDE_q += mul.s*mul.mtx.PDE_q;
    PDE_R += mul.s*mul.mtx.PDE_R;
    PDE_s += mul.s*mul.mtx.PDE_s;

    return *this;
  }
};

}
#endif // JACOBIANCELL_DGBR2_ELEMENT_H
