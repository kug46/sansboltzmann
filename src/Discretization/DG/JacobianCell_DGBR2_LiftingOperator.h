// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_DGBR2_LIFTINGOPERATOR_H
#define JACOBIANCELL_DGBR2_LIFTINGOPERATOR_H

// DG BR2 cell integral jacobian wrt the lifting operator

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/FieldData/FieldDataMatrixD_CellLift.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "JacobianCell_DGBR2_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class IntegrandCell>
class JacobianCell_DGBR2_LiftingOperator_impl :
    public GroupIntegralCellType< JacobianCell_DGBR2_LiftingOperator_impl<IntegrandCell> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  typedef Real LiftedT; //data-type for lifted quantity

  JacobianCell_DGBR2_LiftingOperator_impl(const IntegrandCell& fcn,
                          FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R ) :
    fcn_(fcn), jacPDE_R_(jacPDE_R) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {}

  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple,
                                       Field<PhysDim, TopoDim, ArrayQ>,
                                       FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                       Field<PhysDim, TopoDim, LiftedT>>::type& flds ) const
  {}


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobal,
      const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<Topology>& fldsCell,
      const int quadratureorder )
  {
    typedef typename XFieldType                              ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ        >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementrFieldClass;
    typedef Element<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;

    typedef JacobianElemCell_DGBR2<PhysDim,MatrixQ> JacobianElemCellType;

    // Jacobian of PDE cell integral wrt lifting operator R
    DLA::MatrixDView_Array<RowMatrixQ>& PDE_R = jacPDE_R_.getCellGroupGlobal(cellGroupGlobal);

    // only need this jacobian if the source is a function of the gradient (i.e. the lifting operator)
    if ( !fcn_.needsSolutionGradientforSource() )
    {
      PDE_R = 0;
      return;
    }

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementrFieldClass rfldElems( rfldCell.basis() );
    ElementRFieldClass RfldElem( rfldCell.basis() );

    // DOFs per element
    const int nDOF = qfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal(nDOF, -1);

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, JacobianElemCellType > integral(quadratureorder);

    // element jacobian matrices
    JacobianElemCellType mtxElem(nDOF);

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      rfldCell.getElement( rfldElems, elem );

      // compute the sum of lifting operator on the cell
      RfldElem.vectorViewDOF() = 0;
      for (int trace = 0; trace < Topology::NTrace; trace++)
        RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

      // cell integration for canonical element
      integral( fcn_.integrand_PDE(xfldElem, qfldElem, RfldElem, true),
                get<-1>(xfldElem), mtxElem );

      // save off the jacobian
      PDE_R[ elem ] = mtxElem.PDE_R;

    } // elem

  }


  // For case with lifted quantity field for source terms
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobal,
      const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
      const typename MakeTuple<FieldTuple,
                               Field<PhysDim, TopoDim, ArrayQ>,
                               FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                               Field<PhysDim, TopoDim, LiftedT>>::type
                               ::template FieldCellGroupType<Topology>& fldsCell,
      const int quadratureorder )
  {
    typedef typename XFieldType                                ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ          >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, LiftedT         >::template FieldCellGroupType<Topology> SFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementrFieldClass;
    typedef Element<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;
    typedef typename SFieldCellGroupType::template ElementType<> ElementSFieldClass;

    typedef JacobianElemCell_DGBR2<PhysDim,MatrixQ> JacobianElemCellType;

    // Jacobian of PDE cell integral wrt lifting operator R
    DLA::MatrixDView_Array<RowMatrixQ>& PDE_R = jacPDE_R_.getCellGroupGlobal(cellGroupGlobal);

    // only need this jacobian if the source is a function of the gradient (i.e. the lifting operator)
    if ( !fcn_.needsSolutionGradientforSource() )
    {
      PDE_R = 0;
      return;
    }

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);
    const SFieldCellGroupType& sfldCell = get<2>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementrFieldClass rfldElems( rfldCell.basis() );
    ElementRFieldClass RfldElem( rfldCell.basis() );
    std::shared_ptr<ElementSFieldClass> psfldElem = std::make_shared<ElementSFieldClass>( sfldCell.basis() );

    // DOFs per element
    const int nDOF = qfldElem.nDOF();
    const int sDOF = psfldElem->nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal(nDOF, -1);

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, JacobianElemCellType > integral(quadratureorder);

    // element jacobian matrices
    JacobianElemCellType mtxElem(nDOF, sDOF);

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      rfldCell.getElement( rfldElems, elem );
      sfldCell.getElement( *psfldElem, elem );

      // compute the sum of lifting operator on the cell
      RfldElem.vectorViewDOF() = 0;
      for (int trace = 0; trace < Topology::NTrace; trace++)
        RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

      // cell integration for canonical element
      integral( fcn_.integrand_PDE(xfldElem, qfldElem, RfldElem, psfldElem, true),
                get<-1>(xfldElem), mtxElem );

      // save off the jacobian
      PDE_R[ elem ] = mtxElem.PDE_R;

    } // elem

  }

protected:
  const IntegrandCell& fcn_;
  FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R_;
};


// Factory function

template<class IntegrandCell, class RowMatrixQ>
JacobianCell_DGBR2_LiftingOperator_impl<IntegrandCell>
JacobianCell_DGBR2_LiftingOperator( const IntegrandCellType<IntegrandCell>& fcnPDE,
                                    FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R )
{
  return { fcnPDE.cast(), jacPDE_R };
}

} //namespace SANS

#endif  // JACOBIANCELL_DGBR2_LIFTINGOPERATOR_H
