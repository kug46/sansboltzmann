// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/PDENavierStokes3D.h"
#include "pde/NS/BCNavierStokes3D.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

namespace SANS
{

typedef QTypePrimitiveRhoPressure QType;
typedef ViscosityModel_Sutherland ViscosityModelType;
typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
typedef PDENavierStokes3D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
typedef BCNavierStokes3DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass>::type BCVector;

typedef XField<PhysD3, TopoD3> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, DGBR2, ParamFieldTupleType )

}
