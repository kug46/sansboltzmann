// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#include <boost/mpl/vector_c.hpp>
#define JACOBIANPARAM_INSTANTIATE
#include "Discretization/JacobianParam_impl.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEulermitAVDiffusion2D.h"
#include "pde/NS/BCEuler2D.h"

#include "pde/Sensor/SensorParameter_Traits.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"


namespace SANS
{

typedef QTypePrimitiveRhoPressure QType;
typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
typedef PDEEulermitAVDiffusion2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                       Field<PhysD2, TopoD2, SensorParameterTraits<PhysD3>::ArrayQ<Real>>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )

// Compute Jacobian wrt parameter 1, i.e. the sensor parameter
typedef boost::mpl::vector1_c<int,1> iParam;

JACOBIANPARAM_DGBR2_INSTANTIATE_SPACE( iParam, PDEClass, BCVector, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
JACOBIANPARAM_DGBR2_INSTANTIATE_SPACE( iParam, PDEClass, BCVector, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )
#if 0
typedef typename DLA::MatrixSymS<PhysD3::D,Real> HTypeSpaceTime;

typedef typename MakeTuple<FieldTuple, Field<PhysD3, TopoD3, HTypeSpaceTime>,
                                       Field<PhysD3, TopoD3, SensorParameterTraits<PhysD3>::ArrayQ<Real>>,
                                       XField<PhysD3, TopoD3>>::type ParamFieldTupleSpaceTime;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpaceTime )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleSpaceTime )

JACOBIANPARAM_DGBR2_INSTANTIATE_SPACETIME( iParam, PDEClass, BCVector, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpaceTime )
JACOBIANPARAM_DGBR2_INSTANTIATE_SPACETIME( iParam, PDEClass, BCVector, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleSpaceTime )
#endif
}
