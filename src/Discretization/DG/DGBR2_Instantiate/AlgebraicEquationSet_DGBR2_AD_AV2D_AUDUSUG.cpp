// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include <boost/mpl/vector_c.hpp>
#define JACOBIANPARAM_INSTANTIATE
#include "Discretization/JacobianParam_impl.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion_ArtificialViscosity2D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion_ArtificialViscosity2D.h"

#include "pde/Sensor/SensorParameter_Traits.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{

typedef AdvectiveFlux2D_Uniform AdvectiveFluxType;
typedef ViscousFlux2D_Uniform ViscousFluxType;
typedef Source2D_UniformGrad SourceType;

typedef PDEAdvectionDiffusion_ArtificialViscosity<PhysD2, AdvectiveFluxType, ViscousFluxType, SourceType> PDEClass;

typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType>,
                                       Field<PhysD2, TopoD2, SensorParameterTraits<PhysD2>::ArrayQ<Real>>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTupleType;

typedef BCAdvectionDiffusion_ArtificialViscosity2DVector<AdvectiveFluxType, ViscousFluxType> BCVector;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, DGBR2, ParamFieldTupleType )

// Compute Jacobian wrt parameter 1, i.e. the sensor parameter
typedef boost::mpl::vector1_c<int,1> iParam;

JACOBIANPARAM_DGBR2_INSTANTIATE_SPACE( iParam, PDEClass, BCVector, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
JACOBIANPARAM_DGBR2_INSTANTIATE_SPACE( iParam, PDEClass, BCVector, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )

}
