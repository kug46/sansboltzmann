// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include "pde/NS/TraitsRANSSA.h"
#include "pde/NS/PDERANSSA_Source2D.h"
#include "pde/NS/BCRANSSA2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/QRANSSA2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{

typedef QTypePrimitiveRhoPressure QType;
typedef ViscosityModel_Const ViscosityModelType;
typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
typedef PDERANSSA_Source2D<TraitsSizeRANSSA, TraitsModelRANSSAClass> PDEClass;
typedef BCRANSSA2DVector<TraitsSizeRANSSA, TraitsModelRANSSAClass>::type BCVector;

typedef FieldTuple<Field<PhysD2, TopoD2, Real>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, DGBR2, ParamFieldTupleType )

}
