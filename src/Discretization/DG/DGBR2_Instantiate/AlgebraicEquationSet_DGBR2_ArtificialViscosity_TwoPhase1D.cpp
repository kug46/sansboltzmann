// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include "pde/PorousMedia/Q1DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/Sensor_TwoPhase.h"

#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity1D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity1D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

//#include "pde/NDConvert/PDENDConvertSpace1D.h"
//#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

//#include "Field/XFieldLine.h"
//#include "Field/FieldLine_DG_Cell.h"
//#include "Field/FieldLine_DG_BoundaryTrace.h"
//#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{

typedef DensityModel_Comp DensityModel;
typedef PorosityModel_Comp PorosityModel;
typedef RelPermModel_PowerLaw RelPermModel;
typedef ViscosityModel_Constant ViscModel;
typedef Real RockPermModel;
typedef CapillaryModel_Linear CapillaryModel;
typedef QTypePrimitive_pnSw QType;

typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel,
                            RelPermModel, RelPermModel, ViscModel, ViscModel,
                            RockPermModel, CapillaryModel> TraitsModelTwoPhaseClass;

typedef PDETwoPhase_ArtificialViscosity1D<TraitsSizeTwoPhaseArtificialViscosity,
                                          TraitsModelTwoPhaseClass> PDEBaseClass;

typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
typedef AVSensor_Source1D_TwoPhase<PDEBaseClass> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
typedef PDEmitAVSensor1D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV> PDEClass;

typedef BCTwoPhaseArtificialViscosity1DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                              TraitsModelAV> BCVector;

//typedef typename DLA::MatrixSymS<PhysD1::D,Real> HType;
//
//typedef FieldTuple<Field<PhysD1, TopoD1, HType>, XField<PhysD1, TopoD1>, TupleClass<>> ParamFieldTupleType;
//
//ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
//ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )
//
//ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, DGBR2, ParamFieldTupleType )


typedef typename DLA::MatrixSymS<PhysD2::D,Real> HTypeSpaceTime;

typedef FieldTuple<Field<PhysD2, TopoD2, HTypeSpaceTime>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleSpaceTime;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpaceTime )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleSpaceTime )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, DGBR2, ParamFieldTupleSpaceTime )

}
