// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include "pde/Lorenz/PDELorenz.h"
#include "pde/Lorenz/BCLorenz.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

namespace SANS
{

typedef PDELorenz PDEClass;
typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

typedef BCLorenzVector BCVector;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE(PDEClass, BCVector, TopoD1, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType)

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE(PDEClass, BCVector, DGBR2, ParamFieldTupleType)

}
