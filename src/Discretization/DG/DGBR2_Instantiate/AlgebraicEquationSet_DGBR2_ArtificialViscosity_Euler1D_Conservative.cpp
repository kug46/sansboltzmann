// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#include <boost/mpl/vector_c.hpp>
#define JACOBIANPARAM_INSTANTIATE
#include "Discretization/JacobianParam_impl.h"

#include "pde/NS/Fluids1D_Sensor.h"
#include "pde/NS/TraitsEulerArtificialViscosity.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/PDEEulermitAVDiffusion1D.h"
#include "pde/NS/BCEulermitAVSensor1D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"


namespace SANS
{

typedef QTypeConservative QType;
typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity, TraitsModelEulerClass> PDEBaseClass;

typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;
typedef Fluids_Sensor<PhysD1, PDEBaseClass> Sensor;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Sensor> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> TraitsModelAV;
typedef PDEmitAVSensor1D<TraitsSizeEulerArtificialViscosity, TraitsModelAV> PDEClass;


typedef BCEulermitAVSensor1DVector<TraitsSizeEulerArtificialViscosity, TraitsModelAV> BCVector;

typedef typename DLA::MatrixSymS<PhysD1::D,Real> HType;

typedef FieldTuple<Field<PhysD1, TopoD1, HType>, XField<PhysD1, TopoD1>, TupleClass<>> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )


#if 0
typedef typename DLA::MatrixSymS<PhysD2::D,Real> HTypeSpaceTime;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HTypeSpaceTime>,
                                       Field<PhysD2, TopoD2, SensorParameterTraits<PhysD2>::ArrayQ<Real>>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTupleSpaceTime;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpaceTime )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleSpaceTime )

JACOBIANPARAM_DGBR2_INSTANTIATE_SPACETIME( iParam, PDEClass, BCVector, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpaceTime )
JACOBIANPARAM_DGBR2_INSTANTIATE_SPACETIME( iParam, PDEClass, BCVector, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleSpaceTime )
#endif
}
