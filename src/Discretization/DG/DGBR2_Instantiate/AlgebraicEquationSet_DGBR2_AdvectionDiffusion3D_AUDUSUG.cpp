// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"

#include "pde/NDConvert/PDENDConvertSpaceTime3D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime3D.h"

#include "Field/XFieldSpacetime.h"
#include "Field/FieldSpacetime_DG_Cell.h"
#include "Field/FieldSpacetime_DG_BoundaryTrace.h"
#include "Field/FieldLiftSpaceTime_DG_Cell.h"

namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_UniformGrad > PDEClass;
typedef XField<PhysD3, TopoD3> ParamFieldTupleType;

// Add the solution BC to the BCVector
typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, DGBR2, ParamFieldTupleType )

typedef XField<PhysD4, TopoD4> ParamFieldTupleSpaceTime;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME(PDEClass, BCVector, TopoD4, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpaceTime)
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME(PDEClass, BCVector, TopoD4, AlgEqSetTraits_Dense, DGBR2, ParamFieldTupleSpaceTime)

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACETIME(PDEClass, BCVector, DGBR2, ParamFieldTupleSpaceTime)

// philip
typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Radial,
                              ViscousFlux3D_Uniform,
                              Source3D_UniformGrad > PDEClassR;
typedef XField<PhysD3, TopoD3> ParamFieldTupleType;

// Add the solution BC to the BCVector
typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Radial, ViscousFlux3D_Uniform> BCVectorR;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClassR, BCVectorR, TopoD3, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClassR, BCVectorR, TopoD3, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDEClassR, BCVectorR, DGBR2, ParamFieldTupleType )

typedef XField<PhysD4, TopoD4> ParamFieldTupleSpaceTimeR;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME(PDEClassR, BCVectorR, TopoD4, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpaceTimeR)
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME(PDEClassR, BCVectorR, TopoD4, AlgEqSetTraits_Dense, DGBR2, ParamFieldTupleSpaceTimeR)

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACETIME(PDEClassR, BCVectorR, DGBR2, ParamFieldTupleSpaceTimeR)

}
