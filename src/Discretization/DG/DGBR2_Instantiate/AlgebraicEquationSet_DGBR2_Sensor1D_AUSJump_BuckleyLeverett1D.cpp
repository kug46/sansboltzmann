// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include <boost/mpl/vector_c.hpp>
#define JACOBIANPARAM_INSTANTIATE
#include "Discretization/JacobianParam_impl.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/Sensor_BuckleyLeverett.h"

#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/BCSensorParameter1D.h"

//#include "pde/Sensor/PDESensorParameter2D.h"
//#include "pde/Sensor/BCSensorParameter2D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

//#include "pde/NDConvert/PDENDConvertSpace2D.h"
//#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{

typedef QTypePrimitive_Sw QType;
typedef TraitsModelBuckleyLeverett<QType, RelPermModel_PowerLaw, CapillaryModel_Linear> TraitsModelClass;
typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;

typedef Sensor_AdvectiveFlux1D_Uniform Sensor_Advection;
typedef Sensor_ViscousFlux1D_GenHScale Sensor_Diffusion;
typedef Sensor_BuckleyLeverett<PDEClass> Sensor;
typedef Source1D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor;

typedef PDESensorParameter<PhysD1,
                           SensorParameterTraits<PhysD1>,
                           Sensor_Advection,
                           Sensor_Diffusion,
                           Source_JumpSensor> PDEClass_Sensor;

typedef typename DLA::MatrixSymS<PhysD1::D,Real> HType;
typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType_ST;

typedef typename MakeTuple<FieldTuple, Field<PhysD1, TopoD1, HType>,
                                       Field<PhysD1, TopoD1, PDEClass::ArrayQ<Real>>,
                                       XField<PhysD1, TopoD1>>::type ParamFieldTupleType;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType_ST>,
                                       Field<PhysD2, TopoD2, PDEClass::ArrayQ<Real>>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTupleType_ST;

typedef BCSensorParameter1DVector<Sensor_Advection, Sensor_Diffusion> BCVector;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor, BCVector, TopoD1, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor, BCVector, TopoD1, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass_Sensor, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType_ST )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass_Sensor, BCVector, TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType_ST )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACETIME( PDEClass_Sensor, BCVector, DGBR2, ParamFieldTupleType_ST )


// Compute Jacobian wrt parameter 1, i.e. the PDE solution parameter
typedef boost::mpl::vector1_c<int,1> iParam;

JACOBIANPARAM_DGBR2_INSTANTIATE_SPACETIME( iParam, PDEClass_Sensor, BCVector, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType_ST )
JACOBIANPARAM_DGBR2_INSTANTIATE_SPACETIME( iParam, PDEClass_Sensor, BCVector, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType_ST )

#if 0
// 2D spatial sensor

typedef Sensor_AdvectiveFlux2D_Uniform Sensor_Advection2D;
typedef Sensor_ViscousFlux2D_GenHScale Sensor_Diffusion2D;
typedef Source2D_JumpSensor_BuckleyLeverett<Sensor> Source_JumpSensor2D;

typedef PDESensorParameter<PhysD2,
                           SensorParameterTraits<PhysD2>,
                           Sensor_Advection2D,
                           Sensor_Diffusion2D,
                           Source_JumpSensor2D> PDEClass_Sensor2D;

typedef BCSensorParameter2DVector<Sensor_Advection2D, Sensor_Diffusion2D> BCVector_Sensor2D;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor2D, BCVector_Sensor2D, TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType_ST )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor2D, BCVector_Sensor2D, TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType_ST )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor2D, BCVector_Sensor2D, DGBR2, ParamFieldTupleType_ST )

JACOBIANPARAM_DGBR2_INSTANTIATE_SPACE( iParam, PDEClass_Sensor2D, BCVector_Sensor2D, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType_ST )
JACOBIANPARAM_DGBR2_INSTANTIATE_SPACE( iParam, PDEClass_Sensor2D, BCVector_Sensor2D, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType_ST )
#endif
}
