// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include "pde/PorousMedia/Q2DPrimitive_pnSw.h"
#include "pde/PorousMedia/DensityModel.h"
#include "pde/PorousMedia/PorosityModel.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/ViscosityModel_Constant.h"
#include "pde/PorousMedia/PermeabilityModel2D.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/Sensor_TwoPhase.h"

#include "pde/PorousMedia/TraitsTwoPhaseArtificialViscosity.h"
#include "pde/PorousMedia/PDETwoPhase_ArtificialViscosity2D.h"
#include "pde/PorousMedia/BCTwoPhaseArtificialViscosity2D.h"

#include "pde/ArtificialViscosity/AVVariable.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"
#include "Field/FieldLiftVolume_DG_Cell.h"


namespace SANS
{

// compressiblity / Quadblock

typedef DensityModel_Comp DensityModel;
typedef RelPermModel_PowerLaw RelPermModel;
typedef ViscosityModel_Constant ViscModel;
typedef CapillaryModel_Linear CapillaryModel;
typedef QTypePrimitive_pnSw QType;

typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel_Comp,
                            RelPermModel, RelPermModel, ViscModel, ViscModel,
                            PermeabilityModel2D_QuadBlock, CapillaryModel> TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                          TraitsModel_RelPower_ViscConst_PermQuadBlock_CapLinear>
        PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeTwoPhaseArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeTwoPhaseArtificialViscosity> SensorViscousFlux;
typedef AVSensor_Source2D_TwoPhase<PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear> SensorSource_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef TraitsModelArtificialViscosity<PDE_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear, SensorAdvectiveFlux, SensorViscousFlux,
                                       SensorSource_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear>
        TraitsModelAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;
typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear>
        PDEmitAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                              TraitsModelAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear>
        BCVector_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear;

typedef typename DLA::MatrixSymS<PhysD2::D,Real> HTypeSpace;

typedef FieldTuple<Field<PhysD2, TopoD2, HTypeSpace>, XField<PhysD2, TopoD2>, TupleClass<>> ParamFieldTupleSpace;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEmitAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                              BCVector_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                              TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpace )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEmitAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                              BCVector_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                              TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleSpace )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDEmitAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                                    BCVector_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                                    DGBR2, ParamFieldTupleSpace )


typedef typename DLA::MatrixSymS<PhysD3::D,Real> HTypeSpaceTime;

typedef FieldTuple<Field<PhysD3, TopoD3, HTypeSpaceTime>, XField<PhysD3, TopoD3>, TupleClass<>> ParamFieldTupleSpaceTime;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEmitAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                                  BCVector_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                                  TopoD3, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpaceTime )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEmitAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                                  BCVector_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                                  TopoD3, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleSpaceTime )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACETIME( PDEmitAV_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                                        BCVector_Comp_RelPower_ViscConst_PermQuadBlock_CapLinear,
                                                        DGBR2, ParamFieldTupleSpaceTime )

// Cartesian look-up Tables

typedef TraitsModelTwoPhase<QType, DensityModel, DensityModel, PorosityModel_CartTable,
                            RelPermModel, RelPermModel, ViscModel, ViscModel,
                            PermeabilityModel2D_CartTable, CapillaryModel> TraitsModel_RelPower_ViscConst_CartTable_CapLinear;

typedef PDETwoPhase_ArtificialViscosity2D<TraitsSizeTwoPhaseArtificialViscosity,
                                          TraitsModel_RelPower_ViscConst_CartTable_CapLinear>
        PDE_Comp_RelPower_ViscConst_CartTable_CapLinear;

typedef AVSensor_Source2D_TwoPhase<PDE_Comp_RelPower_ViscConst_CartTable_CapLinear> SensorSource_Comp_RelPower_ViscConst_CartTable_CapLinear;

typedef TraitsModelArtificialViscosity<PDE_Comp_RelPower_ViscConst_CartTable_CapLinear, SensorAdvectiveFlux, SensorViscousFlux,
                                       SensorSource_Comp_RelPower_ViscConst_CartTable_CapLinear>
        TraitsModelAV_Comp_RelPower_ViscConst_CartTable_CapLinear;
typedef PDEmitAVSensor2D<TraitsSizeTwoPhaseArtificialViscosity, TraitsModelAV_Comp_RelPower_ViscConst_CartTable_CapLinear>
        PDEmitAV_Comp_RelPower_ViscConst_CartTable_CapLinear;

typedef BCTwoPhaseArtificialViscosity2DVector<TraitsSizeTwoPhaseArtificialViscosity,
                                              TraitsModelAV_Comp_RelPower_ViscConst_CartTable_CapLinear>
        BCVector_Comp_RelPower_ViscConst_CartTable_CapLinear;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEmitAV_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                              BCVector_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                              TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpace )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEmitAV_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                              BCVector_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                              TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleSpace )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDEmitAV_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                                    BCVector_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                                    DGBR2, ParamFieldTupleSpace )

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEmitAV_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                                  BCVector_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                                  TopoD3, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleSpaceTime )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEmitAV_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                                  BCVector_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                                  TopoD3, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleSpaceTime )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACETIME( PDEmitAV_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                                        BCVector_Comp_RelPower_ViscConst_CartTable_CapLinear,
                                                        DGBR2, ParamFieldTupleSpaceTime )
}
