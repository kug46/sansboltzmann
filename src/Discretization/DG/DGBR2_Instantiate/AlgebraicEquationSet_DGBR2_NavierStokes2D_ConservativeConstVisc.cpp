// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{

typedef QTypeConservative QType;
typedef ViscosityModel_Const ViscosityModelType;
typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass>::type BCVector;

typedef XField<PhysD2, TopoD2> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )

}
