// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include <boost/mpl/vector_c.hpp>
#define JACOBIANPARAM_INSTANTIATE
#include "Discretization/JacobianParam_impl.h"


#include "pde/AdvectionDiffusion/AdvectionDiffusion_Sensor.h"

#include "pde/Sensor/Source1D_JumpSensor.h"
#include "pde/Sensor/PDESensorParameter1D.h"
#include "pde/Sensor/BCSensorParameter1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

namespace SANS
{

typedef Sensor_AdvectiveFlux1D_Uniform Sensor_Advection;
typedef Sensor_ViscousFlux1D_GenHScale Sensor_Diffusion;
typedef AdvectionDiffusion_Sensor Sensor;
typedef Source1D_JumpSensor<Sensor> Source_JumpSensor;

typedef PDESensorParameter<PhysD1,
                           SensorParameterTraits<PhysD1>,
                           Sensor_Advection,
                           Sensor_Diffusion,
                           Source_JumpSensor > PDEClass_Sensor;

typedef typename DLA::MatrixSymS<PhysD1::D,Real> HType;
typedef typename DLA::MatrixSymS<PhysD2::D,Real> HType_ST;

typedef typename MakeTuple<FieldTuple, Field<PhysD1, TopoD1, HType>,
                                       Field<PhysD1, TopoD1, Real>,
                                       XField<PhysD1, TopoD1>>::type ParamFieldTupleType;

typedef typename MakeTuple<FieldTuple, Field<PhysD2, TopoD2, HType_ST>,
                                       Field<PhysD2, TopoD2, Real>,
                                       XField<PhysD2, TopoD2>>::type ParamFieldTupleType_ST;

typedef BCSensorParameter1DVector<Sensor_Advection, Sensor_Diffusion> BCVector;
// Compute Jacobian wrt parameter 1, i.e. the sensor parameter
typedef boost::mpl::vector1_c<int,1> iParam;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor, BCVector, TopoD1, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass_Sensor, BCVector, TopoD1, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )

JACOBIANPARAM_DGBR2_INSTANTIATE_SPACE( iParam, PDEClass_Sensor, BCVector, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )
JACOBIANPARAM_DGBR2_INSTANTIATE_SPACE( iParam, PDEClass_Sensor, BCVector, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType )


ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass_Sensor, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType_ST )
ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACETIME( PDEClass_Sensor, BCVector, TopoD2, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType_ST )

ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACETIME( PDEClass_Sensor, BCVector, DGBR2, ParamFieldTupleType_ST )

JACOBIANPARAM_DGBR2_INSTANTIATE_SPACETIME( iParam, PDEClass_Sensor, BCVector, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType_ST )
JACOBIANPARAM_DGBR2_INSTANTIATE_SPACETIME( iParam, PDEClass_Sensor, BCVector, AlgEqSetTraits_Dense , DGBR2, ParamFieldTupleType_ST )

}
