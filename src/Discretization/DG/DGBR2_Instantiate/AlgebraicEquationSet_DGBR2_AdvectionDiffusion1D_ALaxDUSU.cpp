// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGBR2_impl.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"
#include "Field/FieldLiftLine_DG_Cell.h"

namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD1,
                              LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>,
                              ViscousFlux1D_Uniform,
                              Source1D_Uniform > PDEClass;
typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

typedef BCAdvectionDiffusion1DVector<LocalLaxFriedrichs1D<AdvectiveFlux1D_Burgers>, ViscousFlux1D_Uniform> BCVector;

ALGEBRAICEQUATIONSET_DGBR2_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Sparse, DGBR2, ParamFieldTupleType )

}
