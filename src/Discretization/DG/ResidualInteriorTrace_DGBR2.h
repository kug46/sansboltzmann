// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALINTERIORTRACE_DGBR2_H
#define RESIDUALINTERIORTRACE_DGBR2_H

// DG interior-trace integral residual functions

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG BR2 interior-trace integral
//

template<class IntegrandInteriorTrace, template<class> class Vector>
class ResidualInteriorTrace_DGBR2_impl :
    public GroupIntegralInteriorTraceType< ResidualInteriorTrace_DGBR2_impl<IntegrandInteriorTrace, Vector> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandInteriorTrace::PDE PDE;

  // Save off the boundary trace integrand and the residual vectors
  ResidualInteriorTrace_DGBR2_impl( const IntegrandInteriorTrace& fcn,
                                    Vector<ArrayQ>& rsdPDEGlobal ) :
     fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), comm_rank_(0) {}


  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                               template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                               template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename RFieldCellGroupTypeR::template ElementType<> ElementRFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const RFieldCellGroupTypeR& rfldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementRFieldClassR rfldElemR( rfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nIntegrandL = nDOFL;
    const int nIntegrandR = nDOFR;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nDOFL );
    std::vector<int> mapDOFGlobalR( nDOFR );

    // PDE trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQ, ArrayQ>
      integral(quadratureorder, nIntegrandL, nIntegrandR);

    // PDE cell residuals
    std::vector< ArrayQ > rsdPDEElemL( nIntegrandL );
    std::vector< ArrayQ > rsdPDEElemR( nIntegrandR );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      rfldCellR.getElement( rfldElemR, elemR, canonicalTraceR.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      const int rankL = qfldElemL.rank();
      const int rankR = qfldElemR.rank();

      // nothing to do if both elements are ghost elements
      if (rankL != comm_rank_ && rankR != comm_rank_) continue;

      for (int n = 0; n < nIntegrandL; n++) rsdPDEElemL[n] = 0;
      for (int n = 0; n < nIntegrandR; n++) rsdPDEElemR[n] = 0;

      // Compute the PDE residual given the lifting operators
      integral( fcn_.integrand_PDE(xfldElemTrace,
                                   canonicalTraceL, canonicalTraceR,
                                   xfldElemL, qfldElemL, rfldElemL,
                                   xfldElemR, qfldElemR, rfldElemR),
                xfldElemTrace, rsdPDEElemL.data(), nIntegrandL, rsdPDEElemR.data(), nIntegrandR );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );
      qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nDOFR );

      int nGlobal;
      if ( rankL == comm_rank_ )
        for (int n = 0; n < nDOFL; n++)
        {
          nGlobal = mapDOFGlobalL[n];
          rsdPDEGlobal_[nGlobal] += rsdPDEElemL[n];
        }

      if ( rankR == comm_rank_ )
        for (int n = 0; n < nDOFR; n++)
        {
          nGlobal = mapDOFGlobalR[n];
          rsdPDEGlobal_[nGlobal] += rsdPDEElemR[n];
        }
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  mutable int comm_rank_;
};


// Factory function

template<class IntegrandInteriorTrace, template<class> class Vector, class ArrayQ>
ResidualInteriorTrace_DGBR2_impl<IntegrandInteriorTrace, Vector>
ResidualInteriorTrace_DGBR2( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                             Vector<ArrayQ>& rsdPDEGlobal )
{
  static_assert( std::is_same< ArrayQ, typename IntegrandInteriorTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualInteriorTrace_DGBR2_impl<IntegrandInteriorTrace, Vector>(fcn.cast(), rsdPDEGlobal);
}

}

#endif  // RESIDUALINTERIORTRACE_DGBR2_H
