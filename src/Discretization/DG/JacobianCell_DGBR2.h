// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_DGBR2_H
#define JACOBIANCELL_DGBR2_H

// DG BR2 cell integral jacobian functions

#include <memory>

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/FieldData/FieldDataMatrixD_Cell.h"
#include "Field/FieldData/FieldDataMatrixD_CellLift.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "JacobianCell_DGBR2_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG BR2 cell integral residual
//

template<class IntegrandCell>
class JacobianCell_DGBR2_impl :
    public GroupIntegralCellType< JacobianCell_DGBR2_impl<IntegrandCell> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  typedef Real LiftedT; //data-type for lifted quantity
  typedef typename MatrixLiftedQuantity<MatrixQ>::type MatrixT;

  JacobianCell_DGBR2_impl(const IntegrandCell& fcn,
                          MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                          FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R ) :
                            fcn_(fcn),
                            mtxGlobalPDE_q_(mtxGlobalPDE_q),
                            jacPDE_R_(jacPDE_R),
                            comm_rank_(0) {}

  JacobianCell_DGBR2_impl(const IntegrandCell& fcn,
                          MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                          FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
                          std::shared_ptr<FieldDataMatrixD_Cell<MatrixT>> pjacPDE_s) :
                            fcn_(fcn),
                            mtxGlobalPDE_q_(mtxGlobalPDE_q),
                            jacPDE_R_(jacPDE_R),
                            pjacPDE_s_(pjacPDE_s),
                            comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the jacobian matrix
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    comm_rank_ = qfld.comm()->rank();
  }

  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple,
                                       Field<PhysDim, TopoDim, ArrayQ>,
                                       FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                       Field<PhysDim, TopoDim, LiftedT>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT_MSG(pjacPDE_s_, "Storage for jacobian wrt lifted quantity is not initialized!");

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobal,
      const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<Topology>& fldsCell,
      const int quadratureorder )
  {
    typedef typename XFieldType                              ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ        >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementrFieldClass;
    typedef Element<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;

    typedef JacobianElemCell_DGBR2<PhysDim,MatrixQ> JacobianElemCellType;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);

    // Jacobian of PDE cell integral wrt lifting operator R
    DLA::MatrixDView_Array<RowMatrixQ>& PDE_R = jacPDE_R_.getCellGroupGlobal(cellGroupGlobal);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementrFieldClass rfldElems( rfldCell.basis() );
    ElementRFieldClass RfldElem( rfldCell.basis() );

    // DOFs per element
    const int nDOF = qfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal(nDOF, -1);

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, JacobianElemCellType > integral(quadratureorder);

    // element jacobian matrices
    JacobianElemCellType mtxElem(nDOF);

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      qfldCell.getElement( qfldElem, elem );

      // only need to perform the volume integral on processors that possess the element
      if ( qfldElem.rank() != comm_rank_ ) continue;

      xfldCell.getElement( xfldElem, elem );
      rfldCell.getElement( rfldElems, elem );

      RfldElem.vectorViewDOF() = 0;
      for (int trace = 0; trace < Topology::NTrace; trace++)
        RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

      // cell integration for canonical element
      integral( fcn_.integrand_PDE(xfldElem, qfldElem, RfldElem),
                get<-1>(xfldElem), mtxElem );

      if ( fcn_.needsSolutionGradientforSource() )
        PDE_R[ elem ] = mtxElem.PDE_R;

      // scatter-add element jacobian to global
      scatterAdd(
          qfldCell, elem, nDOF,
          mapDOFGlobal.data(),
          mtxElem.PDE_q,
          mtxGlobalPDE_q_ );

    } // elem
  }

  //For case with lifted quantity field for source terms
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobal,
      const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
      const typename MakeTuple<FieldTuple,
                               Field<PhysDim, TopoDim, ArrayQ>,
                               FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                               Field<PhysDim, TopoDim, LiftedT>>::type
                               ::template FieldCellGroupType<Topology>& fldsCell,
      const int quadratureorder )
  {
    typedef typename XFieldType                                ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ          >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, LiftedT         >::template FieldCellGroupType<Topology> SFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementrFieldClass;
    typedef Element<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;
    typedef typename SFieldCellGroupType::template ElementType<> ElementSFieldClass;

    typedef JacobianElemCell_DGBR2<PhysDim,MatrixQ> JacobianElemCellType;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);
    const SFieldCellGroupType& sfldCell = get<2>(fldsCell);

    // Jacobian of PDE cell integral wrt lifting operator R
    DLA::MatrixDView_Array<RowMatrixQ>& PDE_R = jacPDE_R_.getCellGroupGlobal(cellGroupGlobal);

    // Jacobian of PDE cell integral wrt to lifted quantity
    DLA::MatrixDView_Array<MatrixT>& PDE_s = pjacPDE_s_->getCellGroupGlobal(cellGroupGlobal);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementrFieldClass rfldElems( rfldCell.basis() );
    ElementRFieldClass RfldElem( rfldCell.basis() );
    std::shared_ptr<ElementSFieldClass> psfldElem = std::make_shared<ElementSFieldClass>( sfldCell.basis() );

    // DOFs per element
    const int qDOF = qfldElem.nDOF();
    const int sDOF = psfldElem->nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal(qDOF, -1);

    // element integral
    GalerkinWeightedIntegral_New<TopoDim, Topology, JacobianElemCellType > integral(quadratureorder);

    // element jacobian matrices
    JacobianElemCellType mtxElem(qDOF, sDOF);

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      qfldCell.getElement( qfldElem, elem );

      // only need to perform the volume integral on processors that possess the element
      if ( qfldElem.rank() != comm_rank_ ) continue;

      xfldCell.getElement( xfldElem, elem );
      rfldCell.getElement( rfldElems, elem );
      sfldCell.getElement( *psfldElem, elem );

      RfldElem.vectorViewDOF() = 0;
      for (int trace = 0; trace < Topology::NTrace; trace++)
        RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

      // cell integration for canonical element
      integral( fcn_.integrand_PDE(xfldElem, qfldElem, RfldElem, psfldElem),
                get<-1>(xfldElem), mtxElem );

      if ( fcn_.needsSolutionGradientforSource() )
        PDE_R[elem] = mtxElem.PDE_R;

      PDE_s[elem] = mtxElem.PDE_s;

      // scatter-add element jacobian to global
      scatterAdd(
          qfldCell, elem, qDOF,
          mapDOFGlobal.data(),
          mtxElem.PDE_q,
          mtxGlobalPDE_q_ );

    } // elem
  }

//----------------------------------------------------------------------------//
  template <class QFieldCellGroupType,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfld,
      const int elem, const int nDOF,
      int mapDOFGlobal_q[],
      const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
  {
    // jacobian wrt q
    qfld.associativity( elem ).getGlobalMapping( mapDOFGlobal_q, nDOF );

    mtxGlobalPDE_q.scatterAdd( mtxElemPDE_q, mapDOFGlobal_q, nDOF );
  }

protected:
  const IntegrandCell& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R_;
  std::shared_ptr<FieldDataMatrixD_Cell<MatrixT>> pjacPDE_s_;
  mutable int comm_rank_;
};


// Factory function

template<class IntegrandCell, class MatrixQ, class RowMatrixQ>
JacobianCell_DGBR2_impl<IntegrandCell>
JacobianCell_DGBR2( const IntegrandCellType<IntegrandCell>& fcnPDE,
                    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                    FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R )
{
  return {fcnPDE.cast(), mtxGlobalPDE_q, jacPDE_R};
}

template<class IntegrandCell, class MatrixQ, class RowMatrixQ, class MatrixT>
JacobianCell_DGBR2_impl<IntegrandCell>
JacobianCell_DGBR2( const IntegrandCellType<IntegrandCell>& fcnPDE,
                    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                    FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
                    std::shared_ptr<FieldDataMatrixD_Cell<MatrixT>> pjacPDE_s )
{
  return {fcnPDE.cast(), mtxGlobalPDE_q, jacPDE_R, pjacPDE_s};
}

} //namespace SANS

#endif  // JACOBIANCELL_DGBR2_H
