// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_DGBR2_OUTPUT
#define INTEGRANDCELL_DGBR2_OUTPUT

#include <vector>

#include "BasisFunction/Quadrature_Cache.h"

#include "Field/Element/Element.h"

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand for generic output functional

template <class OutputFunctional>
class IntegrandCell_DGBR2_Output :
    public IntegrandCellType< IntegrandCell_DGBR2_Output<OutputFunctional> >
{
public:
  typedef typename OutputFunctional::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename OutputFunctional::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename OutputFunctional::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = typename OutputFunctional::template ArrayJ<T>;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = typename OutputFunctional::template MatrixJ<T>;

  // cppcheck-suppress noExplicitConstructor
  IntegrandCell_DGBR2_Output( const OutputFunctional& outputFcn, const std::vector<int>& cellGroups ) :
    outputFcn_(outputFcn), cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim        , TopoDim, Topology> ElementXFieldType;
    typedef Element      <ArrayQ<T>      , TopoDim, Topology> ElementQFieldType;
    typedef ElementLift  <VectorArrayQ<T>, TopoDim, Topology> ElementRFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;

    Functor( const OutputFunctional& outputFcn,
             const ElementParam& paramfldElem,
             const ElementQFieldType& qfldElem,
             const ElementRFieldType& rfldElems) :
      outputFcn_(outputFcn),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
      qfldElem_(qfldElem),
      rfldElems_(rfldElems),
      nDOF_( qfldElem_.nDOF() ),
      phi_( new Real[nDOF_] ),
      gradphi_( new VectorX[nDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElems_.basis() );
    }

    Functor( Functor&& f ) :
      outputFcn_(f.outputFcn_),
      paramfldElem_(f.paramfldElem_),
      xfldElem_(f.xfldElem_),
      qfldElem_(f.qfldElem_),
      rfldElems_(f.rfldElems_),
      nDOF_( f.nDOF_ ),
      phi_( f.phi_ ),
      gradphi_( f.gradphi_ )
    {
      f.phi_ = nullptr; f.gradphi_ = nullptr;
    }

    ~Functor()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // element output integrand
    void operator()( const QuadPointType& sRef, ArrayJ<T>& integrand ) const
    {
      VectorX X;       // physical coordinates
      ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

      ArrayQ<T> q;                    // solution
      VectorArrayQ<T> gradqlifted;    // lifted-gradient

      const bool needsSolutionGradient = outputFcn_.needsSolutionGradient();

      // Elemental parameters
      paramfldElem_.eval( sRef, param );

      // basis value, gradient
      qfldElem_.evalBasis( sRef, phi_, nDOF_ );
      xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

      // solution value, gradient
      qfldElem_.evalFromBasis( phi_, nDOF_, q );

      if (needsSolutionGradient)
      {
        VectorArrayQ<T> gradq;
        VectorArrayQ<T> rlift;     // Lifting operators

        qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );

        gradqlifted = gradq;

        // lifted gradient: gradient summed with all trace lifting operators
        for (int n = 0; n < nTrace; n++)
        {
          rfldElems_[n].evalFromBasis( phi_, nDOF_, rlift );
          gradqlifted += rlift;
        }
      }
      else
        gradqlifted = 0;

      outputFcn_(param, q, gradqlifted, integrand);
    }

  protected:
    const OutputFunctional& outputFcn_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementRFieldType& rfldElems_;  // lifting operators, one per edge;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  Functor<T,TopoDim,Topology,ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& rfldElems) const
  {
    return {outputFcn_,
            paramfldElem,
            qfldElem,
            rfldElems};
  }

private:
  const OutputFunctional& outputFcn_;
  const std::vector<int> cellGroups_;
};

}

#endif //INTEGRANDCELL_DGBR2_OUTPUT
