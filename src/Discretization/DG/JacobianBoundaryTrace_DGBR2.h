// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DGBR2_H
#define JACOBIANBOUNDARYTRACE_DGBR2_H

// jacobian boundary-trace integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataMatrixD_CellLift.h"
#include "Field/Element/ElementalMassMatrix.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  DGBR2 boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_DGBR2_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_DGBR2_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianBoundaryTrace_DGBR2_impl(const IntegrandBoundaryTrace& fcn,
                                          const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
                                          MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q ) :
    fcn_(fcn), jacPDE_R_(jacPDE_R), mtxGlobalPDE_q_(mtxGlobalPDE_q) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<Surreal> ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL,-1);

    // Cell lifting operator jacobian
    const DLA::MatrixDView_Array<RowMatrixQ>& PDE_rL = jacPDE_R_.getCellGroupGlobal(cellGroupGlobalL);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal> integralPDE(quadratureorder, nDOFL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralLO(quadratureorder, nDOFL);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
    std::vector<VectorArrayQSurreal> rsdLOElemL( nDOFL );

    // element jacobians
    MatrixElemClass mtxPDEElemL_qL(nDOFL, nDOFL);
    MatrixRElemClass mtxPDEElemL_rL(nDOFL, nDOFL);

    MatrixLOElemClass mtxLOElemL_qL(nDOFL, nDOFL);
    DLA::MatrixD< DLA::MatrixS<D,D,Real> > mtxLOElemL_rL(nDOFL, nDOFL);

    // lifting operator jacobian wrt q
    MatrixLOElemClass rL_qL(nDOFL, nDOFL);

    // computes the elemental mass matrix
    ElementalMassMatrix<TopoDim, TopologyL> massMtx(get<-1>(xfldElemL), qfldElemL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxPDEElemL_qL = 0; mtxPDEElemL_rL = 0;
      mtxLOElemL_qL  = 0; mtxLOElemL_rL  = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      qfldCellL.getElement( qfldElemL, elemL );

      // only need to perform the integral on processors that possess the element
      if ( qfldElemL.rank() != comm_rank_ ) continue;

      xfldCellL.getElement( xfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      // Compute the elemental mass matrix and then add the lifting operator term from the BC
      massMtx(get<-1>(xfldElemL), mtxLOElemL_rL);

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*((D+1)*nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot, slotOffset = 0;

        //wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int k = 0; k < nDeriv; k++)
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(k) = 0;

              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }

        // line integration for canonical element

        for (int n = 0; n < nDOFL; n++)
          rsdLOElemL[n] = 0;

        integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                      xfldElemL, qfldElemL, rfldElemL),
                    get<-1>(xfldElemTrace),
                    rsdLOElemL.data(), nDOFL );

        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        integralPDE(
            fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL, rfldElemL),
            get<-1>(xfldElemTrace),
            rsdPDEElemL.data(), nDOFL );

        // accumulate derivatives into element jacobian
        slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxPDEElemL_qL(i,j), m,n) += DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

                  for (int d = 0; d < D; d++)
                    DLA::index(mtxLOElemL_qL(i,j)[d],m,n) += DLA::index(rsdLOElemL[i][d],m).deriv(slot - nchunk);
                }
            }
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d1 = 0; d1 < D; d1++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d1) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxPDEElemL_rL(i,j)(0,d1),m,n) += DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);

                    for (int d0 = 0; d0 < D; d0++)
                      DLA::index(mtxLOElemL_rL(i,j)(d0,d1),m,n) += DLA::index(rsdLOElemL[i][d0],m).deriv(slot - nchunk);
                  }
              }
            } //n loop
          } //d1 loop
        } //j loop

      }   // nchunk

      // Compute the lifting operator jacobian
      rL_qL = -DLA::InverseLU::Solve(mtxLOElemL_rL, mtxLOElemL_qL);

      // Add jacobian contributions from the cell integral
      if (fcn_.needsSolutionGradientforSource())
        mtxPDEElemL_rL += PDE_rL[ elemL ];

      // Add the chain rule from the lifting operators to the PDE jacobian
      mtxPDEElemL_qL += mtxPDEElemL_rL*rL_qL;

      // scatter-add element jacobian to global

      scatterAdd(
          xfldTrace, qfldCellL,
          elemL,
          mapDOFGlobal_qL.data(), nDOFL,
          mtxPDEElemL_qL,
          mtxGlobalPDE_q_ );
    }
  }

protected:

//----------------------------------------------------------------------------//
  template <class XFieldTraceGroupType, class QFieldCellGroupTypeL,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const XFieldTraceGroupType& xfldTrace,
      const QFieldCellGroupTypeL& qfldCellL,
      const int elemL,
      int mapDOFGlobal_qL[],const int nDOFL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qL,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
  {
    // global mapping for qL
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qL, mapDOFGlobal_qL, nDOFL);
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  mutable int comm_rank_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class RowMatrixQ, class MatrixQ>
JacobianBoundaryTrace_DGBR2_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_DGBR2( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                             const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q )
{
  return {fcn.cast(), jacPDE_R, mtxGlobalPDE_q};
}


}

#endif  // JACOBIANBOUNDARYTRACE_DGBR2_H
