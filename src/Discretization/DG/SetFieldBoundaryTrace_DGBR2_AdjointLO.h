// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDBOUNDARYTRACE_DGBR2_ADJOINTLO_H
#define SETFIELDBOUNDARYTRACE_DGBR2_ADJOINTLO_H

// jacobian boundary-trace integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataMatrixD_CellLift.h"
#include "Field/Element/ElementalMassMatrix.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  DGBR2 boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class IntegrandBoundaryTrace>
class SetFieldBoundaryTrace_DGBR2_AdjointLO_impl :
    public GroupIntegralBoundaryTraceType< SetFieldBoundaryTrace_DGBR2_AdjointLO_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,1,ArrayQ> RowArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  SetFieldBoundaryTrace_DGBR2_AdjointLO_impl(const IntegrandBoundaryTrace& fcn,
                                             const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R ) :
    fcn_(fcn), jacPDE_R_(jacPDE_R), comm_rank_(0) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                   FieldLift<PhysDim,TopoDim,VectorArrayQ>, // r
                                                   Field<PhysDim,TopoDim,ArrayQ>, // w
                                                   FieldLift<PhysDim,TopoDim,VectorArrayQ>>::type // s
                                                   & flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>,           // q
                                                  FieldLift<PhysDim,TopoDim,VectorArrayQ>, // r
                                                  Field<PhysDim,TopoDim,ArrayQ>,           // w
                                                  FieldLift<PhysDim,TopoDim,VectorArrayQ>  // s
                                     >::type::template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<Surreal> ElementRFieldSurrealClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<>        ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<2>(fldsCellL);
          RFieldCellGroupTypeL& sfldCellL = const_cast<RFieldCellGroupTypeL&>(get<3>(fldsCellL));

    // element field variables
    ElementXFieldClassL        xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldSurrealClassL rfldElemL( rfldCellL.basis() );
    ElementQFieldClassL        wfldElemL( wfldCellL.basis() );
    ElementRFieldClassL        sfldElemL( sfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL,-1);

    // Cell lifting operator jacobian
    const DLA::MatrixDView_Array<RowMatrixQ>& PDE_rL = jacPDE_R_.getCellGroupGlobal(cellGroupGlobalL);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal> integralPDE(quadratureorder, nDOFL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralLO(quadratureorder, nDOFL);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
    std::vector<VectorArrayQSurreal> rsdLOElemL( nDOFL );

    // temporary storage for matrix-vector multiplication
    DLA::VectorD<VectorArrayQ> tmpL( nDOFL );

    // element jacobians
    MatrixLOElemClass mtxTPDEElemL_rL(nDOFL, nDOFL);

    DLA::MatrixD< DLA::MatrixS<D,D,Real> > mtxTLOElemL_rL(nDOFL, nDOFL);

    // computes the elemental mass matrix
    ElementalMassMatrix<TopoDim, TopologyL> massMtx(get<-1>(xfldElemL), qfldElemL);

    // Provide a vector of the primal adjoint
    DLA::VectorD<RowArrayQ> wL( nDOFL );

    // Provide a vector view of the adjoint lifting operator DOFs
    DLA::VectorDView<VectorArrayQ> sL( sfldElemL.vectorViewDOF() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxTPDEElemL_rL = 0;
      mtxTLOElemL_rL  = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );
      wfldCellL.getElement( wfldElemL, elemL );
      sfldCellL.getElement( sfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      // only need to perform the integral on processors that possess the element
      if ( qfldElemL.rank() != comm_rank_ ) continue;

      // Compute the elemental mass matrix and then add the lifting operator term from the BC
      // Mass matrix is symmetric, so no need for transpose
      massMtx(get<-1>(xfldElemL), mtxTLOElemL_rL);

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(D*nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }

        // line integration for canonical element

        for (int n = 0; n < nDOFL; n++)
          rsdLOElemL[n] = 0;

        integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                      xfldElemL, qfldElemL, rfldElemL),
                    get<-1>(xfldElemTrace),
                    rsdLOElemL.data(), nDOFL );

        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        integralPDE( fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL,
                                        xfldElemL, qfldElemL, rfldElemL),
                     get<-1>(xfldElemTrace),
                     rsdPDEElemL.data(), nDOFL );

        // accumulate derivatives into element jacobian

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d1 = 0; d1 < D; d1++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = nEqn*(D*j + d1) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(rfldElemL.DOF(j)[d1],n).deriv(slot - nchunk) = 0;

                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    // Fill in as transposed (j,i)[d1](n,m)
                    DLA::index(mtxTPDEElemL_rL(j,i)[d1],n,m) += DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);

                    // Fill in as transposed (j,i)(d1,d0)(n,m)
                    for (int d0 = 0; d0 < D; d0++)
                      DLA::index(mtxTLOElemL_rL(j,i)(d1,d0),n,m) += DLA::index(rsdLOElemL[i][d0],m).deriv(slot - nchunk);
                  }
              }
            } //n loop
          } //d1 loop
        } //j loop

      }   // nchunk

      // Copy over adjoints for matrix-vector multiplication
      for (int i = 0; i < nDOFL; i++) wL[i] = wfldElemL.DOF(i);

      // Add jacobian contributions from the cell integral
      mtxTPDEElemL_rL += Transpose(PDE_rL[ elemL ]);

      tmpL = mtxTPDEElemL_rL*wL;

      // Accumulate the second contribution to the lifting operator adjoint
      // sL = [dLO/dr]^-T * ([dJ/dr]^T - [dPDE/dr]^T * w)
      tmpL = -DLA::InverseLU::Solve(mtxTLOElemL_rL, tmpL);

      // Extract the sfld element for accumulation
      sfldCellL.getElement( sfldElemL, elemL, canonicalTraceL.trace );

      sL += tmpL;

      // Save off the second component of lifting operator adjoint
      sfldCellL.setElement( sfldElemL, elemL, canonicalTraceL.trace );
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R_;
  mutable int comm_rank_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class RowMatrixQ>
SetFieldBoundaryTrace_DGBR2_AdjointLO_impl<Surreal, IntegrandBoundaryTrace>
SetFieldBoundaryTrace_DGBR2_AdjointLO( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                       const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R )
{
  return {fcn.cast(), jacPDE_R};
}


}

#endif  // SETFIELDBOUNDARYTRACE_DGBR2_ADJOINTLO_H
