// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONALCELL_DGBR2_H
#define FUNCTIONALCELL_DGBR2_H

// Cell integral functional

#include "tools/Tuple.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/FieldLift.h"

#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementLift.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group functional
//

template<class IntegrandCell, class T>
class FunctionalCell_DGBR2_impl :
    public GroupIntegralCellType< FunctionalCell_DGBR2_impl<IntegrandCell, T> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<T> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename IntegrandCell::template ArrayJ<T> ArrayJ;

  // Save off the cell integrand and the residual vector
  FunctionalCell_DGBR2_impl( const IntegrandCell& fcn,
                             ArrayJ& functional ) :
                             fcn_(fcn), functional_(functional), comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                      template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                                ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ          >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementRFieldClass rfldElems( rfldCell.basis() );

    // element integral
    ElementIntegral<TopoDim, Topology, ArrayJ> integral(quadratureorder);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == rfldCell.nElem() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      qfldCell.getElement( qfldElem, elem );

      // only need to perform the volume integral on processors that possess the element
      if ( qfldElem.rank() != comm_rank_ ) continue;

      xfldCell.getElement( xfldElem, elem );
      rfldCell.getElement( rfldElems, elem );

      ArrayJ result = 0;

      // cell integration for canonical element
      integral( fcn_.integrand(xfldElem, qfldElem, rfldElems), get<-1>(xfldElem), result );

      // sum up the functional
      functional_ += result;

    }
  }

protected:
  const IntegrandCell& fcn_;
  ArrayJ& functional_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandCell, class ArrayJ>
FunctionalCell_DGBR2_impl<IntegrandCell,typename Scalar<ArrayJ>::type>
FunctionalCell_DGBR2( const IntegrandCellType<IntegrandCell>& fcn,
                      ArrayJ& functional )
{
  typedef typename Scalar<ArrayJ>::type T;
  static_assert( std::is_same<typename IntegrandCell::template ArrayJ<T>, ArrayJ>::value, "These should be the same");
  return FunctionalCell_DGBR2_impl<IntegrandCell,T>(fcn.cast(), functional);
}

} // namespace SANS

#endif  // FUNCTIONALCELL_DGBR2_H
