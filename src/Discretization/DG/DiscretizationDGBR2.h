// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DISCRETIZATIONDGBR2_H
#define DISCRETIZATIONDGBR2_H

// DG discretization: BR2

#include <iostream>

#include "tools/SANSnumerics.h"     // Real

#include "Field/Element/Element.h" // ElementXField
#include "Discretization/DiscretizationObject.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// DG BR2: IntegrandType templated on number of Lifting Operators required
template<class ArrayQ, class VectorArrayQ, class Topology = std::nullptr_t >
class IntegrandDGBR2;

//----------------------------------------------------------------------------//
// DG BR2: Integrands for lumped integrands - Single Lifting Operator (trace Integrand)
// Doesn't take Topology because it is only ever one Trace
template<class ArrayQ, class VectorArrayQ>
class IntegrandDGBR2<ArrayQ, VectorArrayQ, std::nullptr_t>
{
public:
  ArrayQ PDE;
  VectorArrayQ Lift;

  IntegrandDGBR2() {}
  // This constructor allows for the syntax
  // IntegrandDGBR2<ArrayQ,VectorArrayQ> integrand = 0;
  // cppcheck-suppress noExplicitConstructor
  IntegrandDGBR2( const int& v ) : PDE(v), Lift(v) {}

  // Having explicit constructor avoids the mistake
  // IntegrandDGBR2<ArrayQ,VectorArrayQ> integrand += Real(a);
  explicit IntegrandDGBR2( const Real& v ) : PDE(v), Lift(v) {}

  IntegrandDGBR2& operator=( const Real& v )
  {
    PDE = v;
    Lift = v;
    return *this;
  }

  IntegrandDGBR2& operator+=( const IntegrandDGBR2& v )
  {
    PDE += v.PDE;
    Lift += v.Lift;
    return *this;
  }
};

template<class ArrayQ, class VectorArrayQ>
IntegrandDGBR2<ArrayQ,VectorArrayQ, std::nullptr_t> operator*( const Real a,
                                                const IntegrandDGBR2<ArrayQ,VectorArrayQ, std::nullptr_t>& v )
{
  IntegrandDGBR2<ArrayQ,VectorArrayQ, std::nullptr_t> w;
  w.PDE = a*v.PDE;
  w.Lift = a*v.Lift;
  return w;
}

//----------------------------------------------------------------------------//
// DG BR2: Integrands for trace integrals - Topology numbered (cell Integrand)
// Takes in Cell topology because it needs to know NTrace
template<class ArrayQ, class VectorArrayQ, class Topology>
class IntegrandDGBR2
{
public:
  ArrayQ PDE;
  VectorArrayQ Lift[Topology::NTrace];

  IntegrandDGBR2() {}

  // This constructor allows for the syntax
  // IntegrandDGBR2<ArrayQ,VectorArrayQ> integrand = 0;
  // cppcheck-suppress noExplicitConstructor
  IntegrandDGBR2( const int& v ) : PDE(v)
  {
    for ( int i = 0; i < Topology::NTrace; i++ )
      Lift[i] = v;
  }
  // Having explicit constructor avoids the mistake
  // IntegrandDGBR2<ArrayQ,VectorArrayQ> integrand += Real(a);
  explicit IntegrandDGBR2( const Real& v ) : PDE(v)
  {
    for ( int i = 0; i < Topology::NTrace; i++ )
      Lift[i] = v;
  }

  IntegrandDGBR2& operator=( const Real& v )
  {
    PDE = v;
    for ( int i = 0; i < Topology::NTrace; i++ )
      Lift[i] = v;
    return *this;
  }

  IntegrandDGBR2& operator+=( const IntegrandDGBR2& v )
  {
    PDE += v.PDE;
    for ( int i = 0; i < Topology::NTrace; i++ )
      Lift[i] += v.Lift[i];
    return *this;
  }
};

template<class ArrayQ, class VectorArrayQ, class Topology>
IntegrandDGBR2<ArrayQ,VectorArrayQ, Topology> operator*( const Real a,
                                                const IntegrandDGBR2<ArrayQ,VectorArrayQ, Topology>& v )
{
  IntegrandDGBR2<ArrayQ,VectorArrayQ, Topology> w;
  w.PDE = a*v.PDE;
  for ( int i = 0; i < Topology::NTrace; i++ )
    w.Lift[i] = a*v.Lift[i];
  return w;
}

//----------------------------------------------------------------------------//
// DG BR2: controls viscous-eta determination

class DiscretizationDGBR2 : public DiscretizationObject
{
public:

  DiscretizationDGBR2(
      int viscousEtaType, Real viscousEtaParameter = 0,
      bool verbose = false, std::ostream& out = std::cout ) :
      viscousEtaType_(viscousEtaType), viscousEtaParameter_(viscousEtaParameter),
      verbose_(verbose), out_(out)
      { init(); }

  int viscousEtaType() const { return viscousEtaType_; }
  Real viscousEtaParameter() const { return viscousEtaParameter_; }

  template <class PhysDim, class TopoDimTrace, class TopologyTrace, class TopoDimCell, class TopologyL, class TopologyR>
  Real viscousEta(
      const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xedge,
      const ElementXField<PhysDim, TopoDimCell, TopologyL>& xelemL,
      const ElementXField<PhysDim, TopoDimCell, TopologyR>& xelemR,
      const int traceL, const int traceR ) const;

private:
  void init();

private:
  int viscousEtaType_;
  Real viscousEtaParameter_;
  bool verbose_;
  std::ostream& out_;
};

}

#endif  // DISCRETIZATIONDGBR2_H
