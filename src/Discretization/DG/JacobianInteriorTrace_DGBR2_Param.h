// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_DGBR2_PARAM_H
#define JACOBIANINTERIORTRACE_DGBR2_PARAM_H

// construct jacobian of PDE residuals w.r.t. parameters

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Tuple/SurrealizedElementTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// DG BR2 interior trace integral Jacobian
//

template<class Surreal, int iParam, class IntegrandInteriorTrace, class MatrixQP_>
class JacobianInteriorTrace_DGBR2_Param_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_DGBR2_Param_impl<Surreal, iParam, IntegrandInteriorTrace, MatrixQP_> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename IntegrandInteriorTrace::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianInteriorTrace_DGBR2_Param_impl(const IntegrandInteriorTrace& fcn,
                                         MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p ) :
    fcn_(fcn), mtxGlobalPDE_p_(mtxGlobalPDE_p) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
//    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

//    SANS_ASSERT( mtxGlobalPDE_p_.m() == qfld.nDOF() );
//    SANS_ASSERT( mtxGlobalPDE_p_.n() == qfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class TupleFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename TupleFieldType::template FieldCellGroupType<TopologyL>& tuplefldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename TupleFieldType::template FieldCellGroupType<TopologyR>& tuplefldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XField<PhysDim, TopoDim>       ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {

    // Left types
    typedef typename TupleFieldType                            ::template FieldCellGroupType<TopologyL> TupleFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>           ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;
    typedef typename TupleType<iParam, TupleFieldCellGroupTypeL>::type ParamCellGroupTypeL;

    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldSurrealClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldSurrealClassL;
    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeL, Surreal, iParam>::type ElementTupleFieldClassL;
    typedef typename ParamCellGroupTypeL::template ElementType<Surreal> ElementParamFieldClassL;

    // Right types
    typedef typename TupleFieldType                            ::template FieldCellGroupType<TopologyR> TupleFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>           ::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;
    typedef typename TupleType<iParam, TupleFieldCellGroupTypeR>::type ParamCellGroupTypeR;

    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldSurrealClassR;
    typedef typename RFieldCellGroupTypeR::template ElementType<> ElementRFieldSurrealClassR;
    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeR, Surreal, iParam>::type ElementTupleFieldClassR;
    typedef typename ParamCellGroupTypeR::template ElementType<Surreal> ElementParamFieldClassR;

    // Trace types
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef typename ElementParamFieldClassL::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP;  // jacobian type associated with each DOF

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass; // jacobian type associated with each element

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);
    const ParamCellGroupTypeL& paramfldCellL = get<iParam>(tuplefldCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const RFieldCellGroupTypeR& rfldCellR = get<1>(fldsCellR);
    const ParamCellGroupTypeR& paramfldCellR = get<iParam>(tuplefldCellR);

    // element field variables
    ElementTupleFieldClassL tuplefldElemL( tuplefldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldSurrealClassL rfldElemL( rfldCellL.basis() );
    ElementParamFieldClassL& paramfldElemL = set<iParam>(tuplefldElemL);

    ElementTupleFieldClassR tuplefldElemR( tuplefldCellR.basis() );
    ElementQFieldSurrealClassR qfldElemR( qfldCellR.basis() );
    ElementRFieldSurrealClassR rfldElemR( rfldCellR.basis() );
    ElementParamFieldClassR& paramfldElemR = set<iParam>(tuplefldElemR);

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );


    // DOFs per element / number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int paramDOFL = paramfldElemL.nDOF();
    const int paramDOFR = paramfldElemR.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapQDOFGlobal_pL(nDOFL);
    std::vector<int> mapQDOFGlobal_pR(nDOFR);
    std::vector<int> mapParamDOFGlobalL(paramDOFL);
    std::vector<int> mapParamDOFGlobalR(paramDOFR);

    // PDE trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal>
      integralPDE(quadratureorder, nDOFL, nDOFR);

    // PDE cell residuals
    std::vector< ArrayQSurreal > rsdPDEElemL( nDOFL );
    std::vector< ArrayQSurreal > rsdPDEElemR( nDOFR );


    // element PDE jacobian matrices wrt q
    MatrixElemClass mtxPDEElemL_pL(nDOFL, paramDOFL);
    MatrixElemClass mtxPDEElemR_pL(nDOFR, paramDOFL);

    MatrixElemClass mtxPDEElemL_pR(nDOFL, paramDOFR);
    MatrixElemClass mtxPDEElemR_pR(nDOFR, paramDOFR);

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero PDE element Jacobians
      mtxPDEElemL_pL = 0; mtxPDEElemR_pL = 0;
      mtxPDEElemL_pR = 0; mtxPDEElemR_pR = 0;

      // left/right elements
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      tuplefldCellL.getElement( tuplefldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      tuplefldCellR.getElement( tuplefldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      rfldCellR.getElement( rfldElemR, elemR, canonicalTraceR.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      // number of simultaneous derivatives per functor call
      const int nDeriv = Surreal::N;


      // loop over derivative chunks for derivatives wrt qL and qR
      for (int nchunk = 0; nchunk < nArrayP*(paramDOFL + paramDOFR); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        // wrt qL
        int slot = 0, slotOffset = 0;
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += paramDOFL*nArrayP;

        // wrt qR
        for (int j = 0; j < paramDOFR; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemR.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += paramDOFR*nArrayP;

        for (int n = 0; n < nDOFL; n++) rsdPDEElemL[n] = 0;
        for (int n = 0; n < nDOFR; n++) rsdPDEElemR[n] = 0;

        // PDE trace integration for canonical element
        integralPDE( fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                        tuplefldElemL, qfldElemL, rfldElemL,
                                        tuplefldElemR, qfldElemR, rfldElemR),
                     xfldElemTrace, rsdPDEElemL.data(), nDOFL, rsdPDEElemR.data(), nDOFR);

        // accumulate derivatives into element jacobians

        slotOffset = 0;
        // wrt qL
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              //L_pL
              for (int i = 0; i < nDOFL; i++)
              {
                for (int m = 0; m < nArrayQ; m++)
                {
                  DLA::index(mtxPDEElemL_pL(i,j),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);
                }
              }

              //R_pL
              for (int i = 0; i < nDOFR; i++)
              {
                for (int m = 0; m < nArrayQ; m++)
                {
                  DLA::index(mtxPDEElemR_pL(i,j),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);
                }
              }

            }
          } //n loop
        } //j loop
        slotOffset += paramDOFL*nArrayP;

        // wrt qR
        for (int j = 0; j < paramDOFR; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElemR.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              //L_pR
              for (int i = 0; i < nDOFL; i++)
              {
                for (int m = 0; m < nArrayQ; m++)
                {
                  DLA::index(mtxPDEElemL_pR(i,j),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);
                }
              }

              //R_pR
              for (int i = 0; i < nDOFR; i++)
              {
                for (int m = 0; m < nArrayQ; m++)
                {
                  DLA::index(mtxPDEElemR_pR(i,j),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);
                }
              }

            }
          } //n loop
        } //j loop
      }   // nchunk loop

      // scatter-add element jacobian to global
      scatterAdd(
          xfldTrace,
          qfldCellL, qfldCellR,
          paramfldCellL, paramfldCellR,
          elemL, elemR,
          mapQDOFGlobal_pL.data(), nDOFL,
          mapQDOFGlobal_pR.data(), nDOFR,
          mapParamDOFGlobalL.data(), paramDOFL,
          mapParamDOFGlobalR.data(), paramDOFR,
          mtxPDEElemL_pL,
          mtxPDEElemL_pR,
          mtxPDEElemR_pL,
          mtxPDEElemR_pR,
          mtxGlobalPDE_p_);

    } //loop over elements

  }

//----------------------------------------------------------------------------//
  template <class XFieldTraceGroupType,
            class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            class ParamFieldCellGroupTypeL, class ParamFieldCellGroupTypeR, class MatrixQP,
            class SparseMatrixType>
  void
  scatterAdd(
      const XFieldTraceGroupType& xfldTrace,
      const QFieldCellGroupTypeL& qfldCellL,
      const QFieldCellGroupTypeR& qfldCellR,
      const ParamFieldCellGroupTypeL& paramfldL,
      const ParamFieldCellGroupTypeR& paramfldR,
      const int elemL, const int elemR,
      int mapQDOFGlobal_pL[], const int nDOFL,
      int mapQDOFGlobal_pR[], const int nDOFR,

      int mapParamDOFGlobalL[], const int paramDOFL,
      int mapParamDOFGlobalR[], const int paramDOFR,

      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemL_pL,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemL_pR,

      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemR_pL,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemR_pR,

      SparseMatrixType& mtxGlobalPDE_p)
  {
    // global mapping for L
    qfldCellL.associativity( elemL ).getGlobalMapping( mapQDOFGlobal_pL, nDOFL );
    paramfldL.associativity( elemL ).getGlobalMapping( mapParamDOFGlobalL, paramDOFL );

    // global mapping for R
    qfldCellR.associativity( elemR ).getGlobalMapping( mapQDOFGlobal_pR, nDOFR );
    paramfldR.associativity( elemR ).getGlobalMapping( mapParamDOFGlobalR, paramDOFR );


    mtxGlobalPDE_p.scatterAdd( mtxPDEElemL_pL, mapQDOFGlobal_pL, nDOFL, mapParamDOFGlobalL, paramDOFL );
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemL_pR, mapQDOFGlobal_pL, nDOFL, mapParamDOFGlobalR, paramDOFR );

    mtxGlobalPDE_p.scatterAdd( mtxPDEElemR_pL, mapQDOFGlobal_pR, nDOFR, mapParamDOFGlobalL, paramDOFL );
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemR_pR, mapQDOFGlobal_pR, nDOFR, mapParamDOFGlobalR, paramDOFR );
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
};


// Factory function

template<class Surreal, int iParam, class IntegrandInteriorTrace, class MatrixQP>
JacobianInteriorTrace_DGBR2_Param_impl<Surreal, iParam, IntegrandInteriorTrace, MatrixQP>
JacobianInteriorTrace_DGBR2_Param( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                   MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p )
{
  return { fcn.cast(), mtxGlobalPDE_p };
}

}

#endif  // JACOBIANINTERIORTRACE_DGBR2_PARAM_H
