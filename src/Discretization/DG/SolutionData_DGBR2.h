// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONDATA_DGBR2_H_
#define SOLUTIONDATA_DGBR2_H_

#include "Discretization/DG/FieldBundle_DGBR2.h"
#include "Adaptation/MOESS/ParamFieldBuilder.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_BoundaryTraceGroup.h"
#include "Field/ProjectSoln/ProjectConstTrace.h"
#include "Field/ProjectSoln/ProjectGlobalField.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"

#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Field/output_Tecplot.h"

namespace SANS
{

template<class PhysDim, class TopoDim, class NDPDEClass, class ParamBuilderType>
struct SolutionData_DGBR2
{
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  typedef FieldBundle_DGBR2<PhysDim, TopoDim, ArrayQ> FieldBundle;

  typedef ParamFieldBuilder<ParamBuilderType, PhysDim, TopoDim,
                            typename FieldBundle::QFieldType> ParamFieldBuilderType;
  typedef ParamFieldBuilder_Local<ParamBuilderType, PhysDim, TopoDim,
                                  typename FieldBundle::QFieldType> ParamFieldBuilderLocalType;

  typedef typename ParamFieldBuilderType::FieldType ParamFieldType;
  typedef Field_DG_Cell<PhysDim, TopoDim, Real> LiftedQuantityFieldType;

  template<class XFieldType>
  SolutionData_DGBR2(const XFieldType& xfld_, const NDPDEClass& pde,
                     const int primal_order, const int adjoint_order,
                     const BasisFunctionCategory basis_cell, const BasisFunctionCategory basis_trace,
                     const std::vector<int>& mitlg_boundarygroups,
                     const DiscretizationDGBR2& disc)
  : SolutionData_DGBR2(xfld_, pde, primal_order, adjoint_order,
                       basis_cell, basis_trace, mitlg_boundarygroups, PyDict(), disc) {}

  template<class XFieldType>
  SolutionData_DGBR2(const XFieldType& xfld_, const NDPDEClass& pde,
                     const int primal_order, const int adjoint_order,
                     const BasisFunctionCategory basis_cell, const BasisFunctionCategory basis_trace,
                     const std::vector<int>& mitlg_boundarygroups, const PyDict& parambuilderDict,
                     const DiscretizationDGBR2& disc)
  : xfld(get<-1>(xfld_)),
    pde(pde),
    primal (xfld, primal_order , basis_cell, basis_trace, mitlg_boundarygroups),
    adjoint(xfld, adjoint_order, basis_cell, basis_trace, mitlg_boundarygroups),
    mitlg_boundarygroups(mitlg_boundarygroups),
    parambuilder(xfld_, primal.qfld, parambuilderDict),
    paramfld(parambuilder.fld),
    disc(disc) {}

  void setSolution(const typename FieldBundle::ArrayQ& q)
  {
    primal.qfld = q;
    primal.lgfld = q;
  }

  template <class SolutionFunctionType>
  void setSolution(const SolutionFunctionType& fcn, const std::vector<int>& cellGroups)
  {
    for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_Discontinuous(fcn, cellGroups), (xfld, primal.qfld) );
  }

  void setSolution(const SolutionData_DGBR2& solFrom, const bool isP1 = false)
  {
    if (&xfld == &solFrom.xfld)
    {
      solFrom.primal.qfld.projectTo(primal.qfld);
    }
    else //need to project between meshes
    {
      if (isP1) //P1 solution projection is more robust
      {
        Field_DG_Cell<PhysDim, TopoDim, ArrayQ> qfldP1( xfld, 1, solFrom.primal.basis_cell);
        ProjectGlobalField(solFrom.primal.qfld, qfldP1);
        qfldP1.projectTo(primal.qfld);
      }
      else
        ProjectGlobalField(solFrom.primal.qfld, primal.qfld);
    }
  }

  // set solution on boundaryGroup - Needs to be called before essential bcs get applied
  void setBoundarySolution( const typename FieldBundle::ArrayQ& q, const std::vector<int>& boundaryGroups )
  {
    // Assign the value to the trace
    for_each_BoundaryTraceGroup<TopoDim>::apply( ProjectConstTrace<PhysDim>(q, boundaryGroups), *this );
  }

  template <class SolutionFunctionType>
  void setBoundarySolution( const SolutionFunctionType& fcn, const std::vector<int>& boundaryGroups )
  {
    // Assign the value to the trace
    for_each_BoundaryTraceGroup<TopoDim>::apply( InterpolateFunctionTrace_Continuous(fcn,boundaryGroups), (xfld, primal.qfld) );
  }

  void createLiftedQuantityField(const int order, const BasisFunctionCategory& category)
  {
    orderLiftedQuantity = order;
    basisCategoryLiftedQuantity = category;
    pliftedQuantityfld = std::make_shared<LiftedQuantityFieldType>(xfld, orderLiftedQuantity, basisCategoryLiftedQuantity);
  }

  void dumpPrimalSolution( const std::string filebase, const std::string tag) const
  {
    output_Tecplot( primal.qfld, filebase + "qfld" + tag );
  }

  const XField<PhysDim, TopoDim>& xfld;

  static constexpr SpaceType spaceType = SpaceType::Discontinuous;

  const NDPDEClass& pde;

  // solution fields
  FieldBundle primal;

  // adjoint fields in richer space (P+1)
  FieldBundle adjoint;

  //lifted quantity field (i.e. shock sensor field)
  std::shared_ptr<LiftedQuantityFieldType> pliftedQuantityfld;
  int orderLiftedQuantity = 0;
  BasisFunctionCategory basisCategoryLiftedQuantity = BasisFunctionCategory_Legendre;

  std::vector<int> mitlg_boundarygroups;

  ParamFieldBuilderType parambuilder;
  const ParamFieldType& paramfld; //parameter fields
  const DiscretizationDGBR2& disc;
};

}

#endif /* SOLUTIONDATA_DGBR2_H_ */
