// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DISPATCH_DGBR2_PARAM_H
#define JACOBIANBOUNDARYTRACE_DISPATCH_DGBR2_PARAM_H

// boundary-trace integral jacobian functions

//#include "JacobianBoundaryTrace_mitLG_DGBR2.h"
#include "Discretization/DG/JacobianBoundaryTrace_DGBR2_Param.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers and with Lifting operators
//
//---------------------------------------------------------------------------//
template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class MatrixQP>
class JacobianBoundaryTrace_Dispatch_DGBR2_Param_impl
{
public:
  JacobianBoundaryTrace_Dispatch_DGBR2_Param_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const int* quadratureorder, int ngroup,
      MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_p_(mtxGlobalPDE_p)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        JacobianBoundaryTrace_DGBR2_Param<Surreal,iParam>(fcn, mtxGlobalPDE_p_),
        xfld_, (qfld_, rfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p_;
};

// Factory function

template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class MatrixQP>
JacobianBoundaryTrace_Dispatch_DGBR2_Param_impl<Surreal, iParam, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, MatrixQP>
JacobianBoundaryTrace_Dispatch_DGBR2_Param(const XFieldType& xfld,
                                           const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                           const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                           const int* quadratureorder, int ngroup,
                                           MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p )
{
  return { xfld, qfld, rfld, quadratureorder, ngroup, mtxGlobalPDE_p };
}

}

#endif //JACOBIANBOUNDARYTRACE_DISPATCH_DGBR2_PARAM_H
