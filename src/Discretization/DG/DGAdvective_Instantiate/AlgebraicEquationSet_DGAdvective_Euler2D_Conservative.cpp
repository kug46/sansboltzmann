// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q2DConservative.h"
#include "pde/NS/PDEEuler2D.h"
#include "pde/NS/BCEuler2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

namespace SANS
{
//Conservative Variable Instantiation

typedef QTypeConservative QType;
typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
typedef PDEEuler2D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
typedef BCEuler2DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

typedef XField<PhysD2, TopoD2> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleType )
}
