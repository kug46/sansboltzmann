// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "pde/NS/TraitsBoltzmannD2Q13.h"
#include "pde/NS/QD2Q13PrimitiveDistributionFunctions.h"
#include "pde/NS/PDEBoltzmannD2Q13.h"
#include "pde/NS/BCBoltzmannD2Q13.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

//#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
//#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

//#include "Field/XFieldVolume.h"
//#include "Field/FieldVolume_DG_Cell.h"
//#include "Field/FieldVolume_DG_BoundaryTrace.h"

namespace SANS
{

typedef QTypePrimitiveDistributionFunctions QType;
typedef TraitsModelBoltzmannD2Q13<QType, GasModel> TraitsModelBoltzmannD2Q13Class;
typedef PDEBoltzmannD2Q13<TraitsSizeBoltzmannD2Q13, TraitsModelBoltzmannD2Q13Class> PDEClass;
typedef BCBoltzmannD2Q13Vector<TraitsSizeBoltzmannD2Q13, TraitsModelBoltzmannD2Q13Class> BCVector;

typedef XField<PhysD2, TopoD2> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleType )

//typedef XField<PhysD2, TopoD3> ParamFieldTupleSpaceTime;

//ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleSpaceTime )

}
