// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q3DPrimitiveRhoPressure.h"
#include "pde/NS/PDEEuler3D.h"
#include "pde/NS/BCEuler3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/XFieldVolume.h"
#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

namespace SANS
{
//Primitive Density Pressure Variable Instantiation

typedef QTypePrimitiveRhoPressure QType;
typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
typedef PDEEuler3D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
typedef BCEuler3DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;

typedef XField<PhysD3, TopoD3> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Dense , DGAdv, ParamFieldTupleType )

ALGEBRAICEQUATIONSET_LOCAL_DGADVECTIVE_INSTANTIATE_SPACE( PDEClass, BCVector, DGAdv, ParamFieldTupleType )
}
