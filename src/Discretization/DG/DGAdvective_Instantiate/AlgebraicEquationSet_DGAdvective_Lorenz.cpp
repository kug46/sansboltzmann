// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#define ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_Local_DG_impl.h"

#include "pde/Lorenz/PDELorenz.h"
#include "pde/Lorenz/BCLorenz.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

namespace SANS
{
typedef PDELorenz PDEClass;
typedef BCLorenzVector BCVector;

typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACE(PDEClass, BCVector, TopoD1, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleType)
ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACE(PDEClass, BCVector, TopoD1, AlgEqSetTraits_Dense , DGAdv, ParamFieldTupleType)

ALGEBRAICEQUATIONSET_LOCAL_DGADVECTIVE_INSTANTIATE_SPACE(PDEClass, BCVector, DGAdv, ParamFieldTupleType)
}
