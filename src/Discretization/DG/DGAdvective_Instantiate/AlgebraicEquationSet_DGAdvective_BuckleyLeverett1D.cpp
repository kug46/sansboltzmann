// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "pde/PorousMedia/Q1DPrimitive_Sw.h"
#include "pde/PorousMedia/RelPermModel_PowerLaw.h"
#include "pde/PorousMedia/CapillaryModel.h"
#include "pde/PorousMedia/TraitsBuckleyLeverett.h"
#include "pde/PorousMedia/PDEBuckleyLeverett1D.h"
#include "pde/PorousMedia/BCBuckleyLeverett1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

namespace SANS
{
typedef TraitsModelBuckleyLeverett<QTypePrimitive_Sw, RelPermModel_PowerLaw, CapillaryModel_None> TraitsModelClass;
typedef PDEBuckleyLeverett1D<TraitsSizeBuckleyLeverett, TraitsModelClass> PDEClass;
typedef BCBuckleyLeverett1DVector<PDEClass> BCBuckleyLeverettVector;

typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACE( PDEClass, BCBuckleyLeverettVector,
                                                    TopoD1, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleType )

typedef XField<PhysD2, TopoD2> ParamFieldTupleSpaceTime;

ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACETIME( PDEClass, BCBuckleyLeverettVector,
                                                        TopoD2, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleSpaceTime )
ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACETIME( PDEClass, BCBuckleyLeverettVector,
                                                        TopoD2, AlgEqSetTraits_Dense , DGAdv, ParamFieldTupleSpaceTime )

}
