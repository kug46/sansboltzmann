// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "pde/NS/TraitsBoltzmann2D.h"
#include "pde/NS/Q2DPrimitiveDistributionFunctions.h"
#include "pde/NS/PDEBoltzmann2D.h"
#include "pde/NS/BCBoltzmann2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

//#include "pde/NDConvert/PDENDConvertSpaceTime2D.h"
//#include "pde/NDConvert/BCNDConvertSpaceTime2D.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

//#include "Field/XFieldVolume.h"
//#include "Field/FieldVolume_DG_Cell.h"
//#include "Field/FieldVolume_DG_BoundaryTrace.h"

namespace SANS
{

typedef QTypePrimitiveDistributionFunctions QType;
typedef TraitsModelBoltzmann2D<QType, GasModel> TraitsModelBoltzmann2DClass;
typedef PDEBoltzmann2D<TraitsSizeBoltzmann2D, TraitsModelBoltzmann2DClass> PDEClass;
typedef BCBoltzmann2DVector<TraitsSizeBoltzmann2D, TraitsModelBoltzmann2DClass> BCVector;

typedef XField<PhysD2, TopoD2> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleType )

//typedef XField<PhysD2, TopoD3> ParamFieldTupleSpaceTime;

//ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleSpaceTime )

}
