// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "pde/ShallowWater/PDEShallowWater2D.h"
#include "pde/ShallowWater/BCShallowWater2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

namespace SANS
{

typedef ShallowWaterSolutionFunction2D_GeometricSeriesTheta SolutionClass;

typedef PDEShallowWater<PhysD2,VarTypeHVelocity2D,SolutionClass> PDEClass;
typedef PDEClass::ArrayQ<Real> ArrayQ;

//---------------------------------------------------------------------------//
// Steady instantiation
typedef PDENDConvertSpace<PhysD2, PDEClass> PDENDClass;

typedef BCShallowWater2DVector<VarTypeHVelocity2D,SolutionClass> BCVector;

// Sparse system
template class AlgebraicEquationSet_DGAdvective< PDENDClass, BCNDConvertSpace, BCVector,
                                                 AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD1> >;

template AlgebraicEquationSet_DGAdvective< PDENDClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD2, TopoD1> >::
         AlgebraicEquationSet_DGAdvective(const XField<PhysD2, TopoD1>& xfld,
                                          Field_DG_Cell<PhysD2, TopoD1, ArrayQ>& qfld,
                                          Field<PhysD2, TopoD1, ArrayQ>& lgfld,
                                          const PDENDClass& pde,
                                          const QuadratureOrder& quadratureOrder,
                                          const ResidualNormType& resNormType,
                                          const std::vector<Real>& tol,
                                          const std::vector<int>& CellGroups,
                                          const std::vector<int>& InteriorTraceGroups,
                                          PyDict& BCList,
                                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups);

// Dense system
template class AlgebraicEquationSet_DGAdvective< PDENDClass, BCNDConvertSpace, BCVector,
                                                 AlgEqSetTraits_Dense, DGAdv, XField<PhysD2, TopoD1> >;

template AlgebraicEquationSet_DGAdvective< PDENDClass, BCNDConvertSpace, BCVector,
                                           AlgEqSetTraits_Dense, DGAdv, XField<PhysD2, TopoD1> >::
         AlgebraicEquationSet_DGAdvective(const XField<PhysD2, TopoD1>& xfld,
                                          Field_DG_Cell<PhysD2, TopoD1, ArrayQ>& qfld,
                                          Field<PhysD2, TopoD1, ArrayQ>& lgfld,
                                          const PDENDClass& pde,
                                          const QuadratureOrder& quadratureOrder,
                                          const ResidualNormType& resNormType,
                                          const std::vector<Real>& tol,
                                          const std::vector<int>& CellGroups,
                                          const std::vector<int>& InteriorTraceGroups,
                                          PyDict& BCList,
                                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups);

} // namespace SANS
