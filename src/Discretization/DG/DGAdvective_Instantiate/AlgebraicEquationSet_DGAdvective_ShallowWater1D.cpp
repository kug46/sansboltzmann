// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "pde/ShallowWater/PDEShallowWater1D.h"
#include "pde/ShallowWater/BCShallowWater1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

namespace SANS
{

typedef ShallowWaterSolutionFunction1D_GeometricSeriesx_Forcing SolutionClass;

typedef PDEShallowWater<PhysD1,VarTypeHVelocity1D,SolutionClass> PDEShallowWater1DGeometricSeries;
typedef PDEShallowWater1DGeometricSeries::ArrayQ<Real> ArrayQ;

//---------------------------------------------------------------------------//
// Steady instantiation
typedef PDENDConvertSpace<PhysD1, PDEShallowWater1DGeometricSeries> PDENDSteadyShallowWater1DGeometricSeries;

typedef BCShallowWater1DVector<VarTypeHVelocity1D,SolutionClass> BCShallowWater1DGeometricSeriesVector;

// Sparse system
template class AlgebraicEquationSet_DGAdvective< PDENDSteadyShallowWater1DGeometricSeries,
                                                 BCNDConvertSpace, BCShallowWater1DGeometricSeriesVector,
                                                 AlgEqSetTraits_Sparse, DGAdv, XField<PhysD1, TopoD1> >;

template AlgebraicEquationSet_DGAdvective< PDENDSteadyShallowWater1DGeometricSeries,
                                           BCNDConvertSpace, BCShallowWater1DGeometricSeriesVector,
                                           AlgEqSetTraits_Sparse, DGAdv, XField<PhysD1, TopoD1> >::
         AlgebraicEquationSet_DGAdvective(const XField<PhysD1, TopoD1>& xfld,
                                          Field_DG_Cell<PhysD1, TopoD1, ArrayQ>& qfld,
                                          Field<PhysD1, TopoD1, ArrayQ>& lgfld,
                                          const PDENDSteadyShallowWater1DGeometricSeries& pde,
                                          const QuadratureOrder& quadratureOrder,
                                          const ResidualNormType& resNormType,
                                          const std::vector<Real>& tol,
                                          const std::vector<int>& CellGroups,
                                          const std::vector<int>& InteriorTraceGroups,
                                          PyDict& BCList,
                                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups);

// Dense system
template class AlgebraicEquationSet_DGAdvective< PDENDSteadyShallowWater1DGeometricSeries,
                                                 BCNDConvertSpace, BCShallowWater1DGeometricSeriesVector,
                                                 AlgEqSetTraits_Dense, DGAdv, XField<PhysD1, TopoD1> >;

template AlgebraicEquationSet_DGAdvective< PDENDSteadyShallowWater1DGeometricSeries,
                                           BCNDConvertSpace, BCShallowWater1DGeometricSeriesVector,
                                           AlgEqSetTraits_Dense, DGAdv, XField<PhysD1, TopoD1> >::
         AlgebraicEquationSet_DGAdvective(const XField<PhysD1, TopoD1>& xfld,
                                          Field_DG_Cell<PhysD1, TopoD1, ArrayQ>& qfld,
                                          Field<PhysD1, TopoD1, ArrayQ>& lgfld,
                                          const PDENDSteadyShallowWater1DGeometricSeries& pde,
                                          const QuadratureOrder& quadratureOrder,
                                          const ResidualNormType& resNormType,
                                          const std::vector<Real>& tol,
                                          const std::vector<int>& CellGroups,
                                          const std::vector<int>& InteriorTraceGroups,
                                          PyDict& BCList,
                                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups);

} // namespace SANS
