// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE
#include "Discretization/DG/AlgebraicEquationSet_DGAdvective_impl.h"

#include "pde/NS/TraitsBoltzmannD1Q3.h"
#include "pde/NS/QD1Q3PrimitiveDistributionFunctions.h"
#include "pde/NS/PDEBoltzmannD1Q3.h"
#include "pde/NS/BCBoltzmannD1Q3.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "Field/XFieldLine.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "Field/XFieldArea.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

namespace SANS
{

typedef QTypePrimitiveDistributionFunctions QType;
typedef TraitsModelBoltzmannD1Q3<QType, GasModel> TraitsModelBoltzmannD1Q3Class;
typedef PDEBoltzmannD1Q3<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> PDEClass;
typedef BCBoltzmannD1Q3Vector<TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3Class> BCVector;

typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleType )

typedef XField<PhysD2, TopoD2> ParamFieldTupleSpaceTime;

ALGEBRAICEQUATIONSET_DGADVECTIVE_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, DGAdv, ParamFieldTupleSpaceTime )

}
