// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef MASSMATINVRESIDUALNORM_H
#define MASSMATINVRESIDUALNORM_H

#include "tools/SANSnumerics.h" // Real

#include <vector>

#include "tools/KahanSum.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

//----------------------------------------------------------------------------//
//
// Computes a residual norm weighted by the inverse mass matrix
//
//----------------------------------------------------------------------------//


namespace SANS
{

template<class NDPDEClass, template<class> class Vector >
class MassMatInvResidualNorm_impl : public GroupFunctorCellType< MassMatInvResidualNorm_impl<NDPDEClass,Vector> >
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  MassMatInvResidualNorm_impl( const std::vector<int>& cellgroup_list,
                               const NDPDEClass& pde,
                               const FieldDataInvMassMatrix_Cell& mmfld,
                               const Vector<ArrayQ>& rsd,
                               std::vector<KahanSum<Real>>& rsdNorm ) :
    cellgroup_list_(cellgroup_list), pde_(pde), mmfld_(mmfld), rsd_(rsd), rsdNorm_(rsdNorm) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename Field<PhysDim,typename Topology::TopoDim,ArrayQ>::template FieldCellGroupType<Topology>& fldCellGroup,
        const int cellGroupGlobal)
  {
    // number of DOFs per element
    const int nDOF = fldCellGroup.nBasis();

    DLA::VectorD<ArrayQ> rsdElem(nDOF);
    DLA::VectorD<ArrayQ> rsdWeightElem(nDOF);
    std::vector<int> mapDOFGlobal(nDOF);

    const int nElem = fldCellGroup.nElem(); //Number of elements in cell group

    const DLA::MatrixDView_Array<Real>& mmfldCell = mmfld_.getCellGroupGlobal(cellGroupGlobal);

    const int nMon = pde_.nMonitor();
    DLA::VectorD<Real> rsdtmp(nMon);

    //loop over cells in local mesh
    for (int elem = 0; elem < nElem; elem++ )
    {
      // get the global mapping for the element DOFs
      fldCellGroup.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), mapDOFGlobal.size() );

      // copy gobal DOFs from the residual vector
      for (int n = 0; n < nDOF; n++)
        rsdElem[n] = rsd_[mapDOFGlobal[n]];

      // weight the elemental residual by the inverse mass matrix
      rsdWeightElem = mmfldCell[elem]*rsdElem;

      // add the squared residual norm
      for (int n = 0; n < nDOF; n++)
      {
        pde_.interpResidVariable(rsdWeightElem[n], rsdtmp);

        for (int j = 0; j < nMon; j++)
          rsdNorm_[j] += pow(rsdtmp[j],2);
      }

    } //loop over elements
  }

protected:
  const std::vector<int> cellgroup_list_;
  const NDPDEClass& pde_;
  const FieldDataInvMassMatrix_Cell& mmfld_; // Inverse mass matrix field
  const Vector<ArrayQ>& rsd_;
  std::vector<KahanSum<Real>>& rsdNorm_;
};

// Factory function
template<class NDPDEClass, template<class> class Vector, class ArrayQ >
MassMatInvResidualNorm_impl<NDPDEClass, Vector>
MassMatInvResidualNorm( const std::vector<int>& cellgroup_list,
                        const NDPDEClass& pde,
                        const FieldDataInvMassMatrix_Cell& mmfld,
                        const Vector<ArrayQ>& rsd,
                        std::vector<KahanSum<Real>>& rsdNorm )
{
  return {cellgroup_list, pde, mmfld, rsd, rsdNorm};
}

} // namespace SANS


#endif //MASSMATINVRESIDUALNORM_H
