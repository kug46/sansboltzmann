// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDINTERIORTRACE_DGBR2_ADJOINTLO_H
#define SETFIELDINTERIORTRACE_DGBR2_ADJOINTLO_H

// interior-trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/FieldData/FieldDataMatrixD_CellLift.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "JacobianInteriorTrace_DGBR2_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// DG BR2 interior trace lifting operator adjoint
//

template<class IntegrandInteriorTrace>
class SetFieldInteriorTrace_DGBR2_AdjointLO_impl :
    public GroupIntegralInteriorTraceType< SetFieldInteriorTrace_DGBR2_AdjointLO_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,1,ArrayQ> RowArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  SetFieldInteriorTrace_DGBR2_AdjointLO_impl(const IntegrandInteriorTrace& fcn,
                                   const FieldDataInvMassMatrix_Cell& mmfld,
                                   const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R ) :
    fcn_(fcn), mmfld_(mmfld), jacPDE_R_(jacPDE_R) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                                   FieldLift<PhysDim,TopoDim,VectorArrayQ>, // r
                                                   Field<PhysDim,TopoDim,ArrayQ>, // w
                                                   FieldLift<PhysDim,TopoDim,VectorArrayQ>>::type // s
                                                   & flds ) const
  {
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                           FieldLift<PhysDim,TopoDim,VectorArrayQ>, // r
                                           Field<PhysDim,TopoDim,ArrayQ>, // w
                                           FieldLift<PhysDim,TopoDim,VectorArrayQ>>::type // s
                     ::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename MakeTuple<FieldTuple, Field<PhysDim,TopoDim,ArrayQ>, // q
                                           FieldLift<PhysDim,TopoDim,VectorArrayQ>, // r
                                           Field<PhysDim,TopoDim,ArrayQ>, // w
                                           FieldLift<PhysDim,TopoDim,VectorArrayQ>>::type // s
                     ::template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {

    // Left types
    typedef typename XFieldType                                ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>           ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    // Right types
    typedef typename XFieldType                                ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>           ::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename RFieldCellGroupTypeR::template ElementType<> ElementRFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename RFieldCellGroupTypeR::template ElementType<> ElementRFieldClassR;

    // Trace types
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef JacobianElemInteriorTrace_DGBR2_PDE_RT<PhysDim, MatrixQ> JacobianElemInteriorTracePDERTType;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<2>(fldsCellL);
          RFieldCellGroupTypeL& sfldCellL = const_cast<RFieldCellGroupTypeL&>(get<3>(fldsCellL));

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const RFieldCellGroupTypeR& rfldCellR = get<1>(fldsCellR);
    const QFieldCellGroupTypeR& wfldCellR = get<2>(fldsCellR);
          RFieldCellGroupTypeR& sfldCellR = const_cast<RFieldCellGroupTypeR&>(get<3>(fldsCellR));

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );
    ElementQFieldClassL wfldElemL( wfldCellL.basis() );
    ElementRFieldClassL sfldElemL( sfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementRFieldClassR rfldElemR( rfldCellR.basis() );
    ElementQFieldClassR wfldElemR( wfldCellR.basis() );
    ElementRFieldClassR sfldElemR( sfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element / number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();

    // Inverse mass matrix used to compute lifing operators
    const DLA::MatrixDView_Array<Real>& mmfldCellL = mmfld_.getCellGroupGlobal(cellGroupGlobalL);
    const DLA::MatrixDView_Array<Real>& mmfldCellR = mmfld_.getCellGroupGlobal(cellGroupGlobalR);

    const DLA::MatrixDView_Array<RowMatrixQ>& PDE_rL = jacPDE_R_.getCellGroupGlobal(cellGroupGlobalL);
    const DLA::MatrixDView_Array<RowMatrixQ>& PDE_rR = jacPDE_R_.getCellGroupGlobal(cellGroupGlobalR);

    // PDE trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTracePDERTType, JacobianElemInteriorTracePDERTType>
      integralPDE(quadratureorder);

    JacobianElemInteriorTraceSize sizeL(nDOFL, nDOFL, nDOFR);
    JacobianElemInteriorTraceSize sizeR(nDOFR, nDOFL, nDOFR);

    // temporary storage for matrix-vector multiplication
    DLA::VectorD<VectorArrayQ> tmpL( nDOFL );
    DLA::VectorD<VectorArrayQ> tmpR( nDOFL );

    // transposed element PDE jacobian matrices wrt r (lifting operator)
    JacobianElemInteriorTracePDERTType mtxElemL(sizeL);
    JacobianElemInteriorTracePDERTType mtxElemR(sizeR);

    // Provide a vector of the primal adjoint DOFs
    DLA::VectorD<RowArrayQ> wL( nDOFL );
    DLA::VectorD<RowArrayQ> wR( nDOFR );

    // Provide a vector view of the lifting operator DOFs
    DLA::VectorDView<VectorArrayQ> sL( sfldElemL.vectorViewDOF() );
    DLA::VectorDView<VectorArrayQ> sR( sfldElemR.vectorViewDOF() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // left/right elements
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );
      wfldCellL.getElement( wfldElemL, elemL );
      sfldCellL.getElement( sfldElemL, elemL, canonicalTraceL.trace );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      rfldCellR.getElement( rfldElemR, elemR, canonicalTraceR.trace );
      wfldCellR.getElement( wfldElemR, elemR );
      sfldCellR.getElement( sfldElemR, elemR, canonicalTraceR.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      // trace integration for canonical element
      integralPDE( fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                      xfldElemL, qfldElemL, rfldElemL,
                                      xfldElemR, qfldElemR, rfldElemR),
                   xfldElemTrace, mtxElemL, mtxElemR);

      // Copy over adjoints for matrix-vector multiplication
      for (int i = 0; i < nDOFL; i++) wL[i] = wfldElemL.DOF(i);
      for (int i = 0; i < nDOFR; i++) wR[i] = wfldElemR.DOF(i);

      // Add jacobian contributions from the cell integral
      if (fcn_.needsSolutionGradientforSource())
      {
        mtxElemL.PDET_rL += Transpose(PDE_rL[ elemL ]);
        mtxElemR.PDET_rR += Transpose(PDE_rR[ elemR ]);
      }

      tmpL = mtxElemL.PDET_rL*wL + mtxElemR.PDET_rL*wR;
      tmpR = mtxElemL.PDET_rR*wL + mtxElemR.PDET_rR*wR;

      // Compute the adjoint lifting operator
      sL = -mmfldCellL[elemL]*tmpL;
      sR = -mmfldCellR[elemR]*tmpR;

      // Set the adjoint lifting operators
      sfldCellL.setElement( sfldElemL, elemL, canonicalTraceL.trace );
      sfldCellR.setElement( sfldElemR, elemR, canonicalTraceR.trace );

    } //loop over elements

  }

protected:
  const IntegrandInteriorTrace& fcn_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R_;
};


// Factory function

template<class IntegrandInteriorTrace, class RowMatrixQ>
SetFieldInteriorTrace_DGBR2_AdjointLO_impl<IntegrandInteriorTrace>
SetFieldInteriorTrace_DGBR2_AdjointLO( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                       const FieldDataInvMassMatrix_Cell& mmfld,
                                       const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R )
{
  return {fcn.cast(), mmfld, jacPDE_R};
}

}

#endif  // SETFIELDINTERIORTRACE_DGBR2_ADJOINTLO_H
