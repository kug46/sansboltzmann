// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONDATA_DGBR2_BLOCK2_H_
#define SOLUTIONDATA_DGBR2_BLOCK2_H_

#include "Discretization/DG/FieldBundle_DGBR2.h"
#include "Adaptation/MOESS/ParamFieldBuilder_Block2.h"

#include "Field/tools/for_each_CellGroup.h"
#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectGlobalField.h"

namespace SANS
{

template<class PhysDim, class TopoDim, class NDPDEClass0, class NDPDEClass1, class ParamBuilderType>
struct SolutionData_DGBR2_Block2
{
  typedef typename NDPDEClass0::template ArrayQ<Real> ArrayQ0;
  typedef typename NDPDEClass1::template ArrayQ<Real> ArrayQ1;

  typedef FieldBundle_DGBR2<PhysDim, TopoDim, ArrayQ0> FieldBundleType0;
  typedef FieldBundle_DGBR2<PhysDim, TopoDim, ArrayQ1> FieldBundleType1;

  typedef ParamFieldBuilder_Block2<ParamBuilderType, PhysDim, TopoDim,
                                   typename FieldBundleType0::QFieldType,
                                   typename FieldBundleType1::QFieldType> ParamFieldBuilder;

  typedef typename ParamFieldBuilder::FieldType0 ParamFieldType0;
  typedef typename ParamFieldBuilder::FieldType1 ParamFieldType1;

  SolutionData_DGBR2_Block2(const XField<PhysDim, TopoDim>& xfld_, const NDPDEClass0& pde0, const NDPDEClass1& pde1,
                            const int primal_order0, const int primal_order1,
                            const int adjoint_order0, const int adjoint_order1,
                            const BasisFunctionCategory basis_cell0, const BasisFunctionCategory basis_cell1,
                            const BasisFunctionCategory basis_trace0, const BasisFunctionCategory basis_trace1,
                            const std::vector<int>& mitlg_boundarygroups0, const std::vector<int>& mitlg_boundarygroups1,
                            const DiscretizationDGBR2& disc0, const DiscretizationDGBR2& disc1)
  : xfld(xfld_),
    pde0(pde0),
    pde1(pde1),
    primal0 (xfld, primal_order0 , basis_cell0, basis_trace0, mitlg_boundarygroups0),
    primal1 (xfld, primal_order1 , basis_cell1, basis_trace1, mitlg_boundarygroups1),
    adjoint0(xfld, adjoint_order0, basis_cell0, basis_trace0, mitlg_boundarygroups0),
    adjoint1(xfld, adjoint_order1, basis_cell1, basis_trace1, mitlg_boundarygroups1),
    mitlg_boundarygroups0(mitlg_boundarygroups0),
    mitlg_boundarygroups1(mitlg_boundarygroups1),
    parambuilder(xfld, primal0.qfld, primal1.qfld),
    paramfld0(parambuilder.fld0),
    paramfld1(parambuilder.fld1),
    disc0(disc0),
    disc1(disc1) {}

  void setSolution(const typename FieldBundleType0::ArrayQ& q0, const typename FieldBundleType1::ArrayQ& q1)
  {
    primal0.qfld = q0;
    primal1.qfld = q1;
  }

  template <class SolutionFunctionType>
  void setSolution0(const SolutionFunctionType& fcn, const std::vector<int>& cellGroups)
  {
    for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_Discontinuous(fcn, cellGroups), (xfld, primal0.qfld) );
  }

  template <class SolutionFunctionType>
  void setSolution1(const SolutionFunctionType& fcn, const std::vector<int>& cellGroups)
  {
    for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_Discontinuous(fcn, cellGroups), (xfld, primal1.qfld) );
  }

  void setSolution(const SolutionData_DGBR2_Block2& solFrom)
  {
    if (&xfld == &solFrom.xfld)
    {
      solFrom.primal0.qfld.projectTo(primal0.qfld);
      solFrom.primal1.qfld.projectTo(primal1.qfld);
    }
    else //need to project between meshes
    {
      ProjectGlobalField(solFrom.primal0.qfld, primal0.qfld);
      ProjectGlobalField(solFrom.primal1.qfld, primal1.qfld);
    }
  }

  const XField<PhysDim, TopoDim>& xfld;

  static constexpr SpaceType spaceType = SpaceType::Discontinuous;

  const NDPDEClass0& pde0;
  const NDPDEClass1& pde1;

  // solution fields
  FieldBundleType0 primal0;
  FieldBundleType1 primal1;

  // adjoint fields in richer space (P+1)
  FieldBundleType0 adjoint0;
  FieldBundleType1 adjoint1;

  std::vector<int> mitlg_boundarygroups0;
  std::vector<int> mitlg_boundarygroups1;

  ParamFieldBuilder parambuilder;

  const ParamFieldType0& paramfld0; //parameter fields for first equation
  const ParamFieldType1& paramfld1; //parameter fields for second equation

  const DiscretizationDGBR2& disc0;
  const DiscretizationDGBR2& disc1;
};

}

#endif /* SOLUTIONDATA_DGBR2_BLOCK2_H_ */
