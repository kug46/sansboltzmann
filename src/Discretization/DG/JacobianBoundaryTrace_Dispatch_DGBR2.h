// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DISPATCH_DGBR2_H
#define JACOBIANBOUNDARYTRACE_DISPATCH_DGBR2_H

// boundary-trace integral jacobian functions

#include "JacobianBoundaryTrace_mitLG_DGBR2.h"
#include "Discretization/DG/JacobianBoundaryTrace_DGBR2.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
#if 0
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class MatrixQ>
class JacobianBoundaryTrace_mitLG_Dispatch_DGBR2_impl
{
public:
  JacobianBoundaryTrace_mitLG_Dispatch_DGBR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_r,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
      MatrixScatterAdd<MatrixQ>& mtxGlobalLO_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalLO_r,
      MatrixScatterAdd<MatrixQ>& mtxGlobalLO_lg,
      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_r,
      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), lgfld_(lgfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_r_(mtxGlobalPDE_r), mtxGlobalPDE_lg_(mtxGlobalPDE_lg),
      mtxGlobalLO_q_ (mtxGlobalLO_q) , mtxGlobalLO_r_ (mtxGlobalLO_r) , mtxGlobalLO_lg_ (mtxGlobalLO_lg),
      mtxGlobalBC_q_ (mtxGlobalBC_q) , mtxGlobalBC_r_ (mtxGlobalBC_r) , mtxGlobalBC_lg_ (mtxGlobalBC_lg)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_mitLG_DGBR2<Surreal>(fcn,
                                                   mtxGlobalPDE_q_, mtxGlobalPDE_r_, mtxGlobalPDE_lg_,
                                                   mtxGlobalLO_q_ , mtxGlobalLO_r_ , mtxGlobalLO_lg_ ,
                                                   mtxGlobalBC_q_ , mtxGlobalBC_r_ , mtxGlobalBC_lg_),
        xfld_, (qfld_, rfld_), lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_r_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalLO_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalLO_r_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalLO_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_r_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class MatrixQ>
JacobianBoundaryTrace_mitLG_Dispatch_DGBR2_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, MatrixQ>
JacobianBoundaryTrace_mitLG_Dispatch_DGBR2(
    const XFieldType& xfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
    const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
    const int* quadratureorder, int ngroup,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_r,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
    MatrixScatterAdd<MatrixQ>& mtxGlobalLO_q,
    MatrixScatterAdd<MatrixQ>& mtxGlobalLO_r,
    MatrixScatterAdd<MatrixQ>& mtxGlobalLO_lg,
    MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
    MatrixScatterAdd<MatrixQ>& mtxGlobalBC_r,
    MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg )
{
  return { xfld, qfld, rfld, lgfld, quadratureorder, ngroup,
           mtxGlobalPDE_q, mtxGlobalPDE_r, mtxGlobalPDE_lg,
           mtxGlobalLO_q , mtxGlobalLO_r , mtxGlobalLO_lg,
           mtxGlobalBC_q , mtxGlobalBC_r , mtxGlobalBC_lg };
}
#endif
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers and with Lifting operators
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class RowMatrixQ, class MatrixQ>
class JacobianBoundaryTrace_Dispatch_DGBR2_impl
{
public:
  JacobianBoundaryTrace_Dispatch_DGBR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
      const int* quadratureorder, int ngroup,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), jacPDE_R_(jacPDE_R),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_q_(mtxGlobalPDE_q)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        JacobianBoundaryTrace_DGBR2<Surreal>(fcn, jacPDE_R_, mtxGlobalPDE_q_),
        xfld_, (qfld_, rfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class RowMatrixQ, class MatrixQ>
JacobianBoundaryTrace_Dispatch_DGBR2_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, RowMatrixQ, MatrixQ>
JacobianBoundaryTrace_Dispatch_DGBR2(const XFieldType& xfld,
                                     const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                     const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                     const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
                                     const int* quadratureorder, int ngroup,
                                     MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q )
{
  return { xfld, qfld, rfld, jacPDE_R, quadratureorder, ngroup, mtxGlobalPDE_q };
}

}

#endif //JACOBIANBOUNDARYTRACE_DISPATCH_DGBR2_H
