// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_DGBR2_H
#define RESIDUALBOUNDARYTRACE_DGBR2_H

// boundary-trace integral residual functions

#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DGBR2 boundary-trace integral
//

template<class IntegrandBoundaryTrace, template<class> class Vector>
class ResidualBoundaryTrace_DGBR2_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_DGBR2_impl<IntegrandBoundaryTrace, Vector> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_DGBR2_impl( const IntegrandBoundaryTrace& fcn,
                                           Vector<ArrayQ>& rsdPDEGlobal ) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType                               ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>          ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    int nIntegrandL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQ> integral(quadratureorder, nIntegrandL);

    // element integrand/residuals
    std::vector<ArrayQ> rsdPDEElemL( nIntegrandL );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      qfldCellL.getElement( qfldElemL, elemL );

      // only need to perform the volume integral on processors that possess the element
      if ( qfldElemL.rank() != comm_rank_ ) continue;

      xfldCellL.getElement( xfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      for (int n = 0; n < nIntegrandL; n++)
        rsdPDEElemL[n] = 0;

      integral( fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL,
                                   xfldElemL, qfldElemL, rfldElemL),
                get<-1>(xfldElemTrace),
                rsdPDEElemL.data(), nIntegrandL );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );

      int nGlobal;
      for (int n = 0; n < nIntegrandL; n++)
      {
        nGlobal = mapDOFGlobalL[n];
        rsdPDEGlobal_[nGlobal] += rsdPDEElemL[n];
      }
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandBoundaryTrace, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_DGBR2_impl<IntegrandBoundaryTrace, Vector>
ResidualBoundaryTrace_DGBR2( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                    Vector<ArrayQ>& rsdPDEGlobal)
{
  static_assert( std::is_same<ArrayQ, typename IntegrandBoundaryTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualBoundaryTrace_DGBR2_impl<IntegrandBoundaryTrace, Vector>(fcn.cast(), rsdPDEGlobal);
}

}

#endif  // RESIDUALBOUNDARYTRACE_DGBR2_H
