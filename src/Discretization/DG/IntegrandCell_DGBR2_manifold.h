// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_DGBR2_MANIFOLD_H_
#define INTEGRANDCELL_DGBR2_MANIFOLD_H_

// cell integrand operators: DG BR2 (Little-r formulation) for manifolds
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/DG/DiscretizationDGBR2.h"
#include "JacobianCell_DGBR2_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// cell element integrand: DG BR2 (Little-r formulation) for manifolds
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective, viscous and BR2 lifting operator)
//   S(x,y,d,q)       solution-dependent source
//   RHS(x,y)         right-hand side forcing function

template <class PDE_>
class IntegrandCell_DGBR2_manifold : public IntegrandCellType< IntegrandCell_DGBR2_manifold<PDE_> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;             // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;           // diffusion matrices

  IntegrandCell_DGBR2_manifold( const PDE& pde,
                                const DiscretizationDGBR2& disc,
                                const std::vector<int>& cellGroups ) :
    pde_(pde),
    disc_(disc),
    cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  // used to know if the source needs the gradient (i.e. lifting operator)
  bool needsSolutionGradientforSource() const
  {
    return pde_.needsSolutionGradientforSource();
  }

  // functor: computes integrands
  template<class T, class Tr, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted_PDE
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology>    ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology>        ElementQFieldType;
    typedef Element<VectorArrayQ<Tr>, TopoDim, Topology> ElementRFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef JacobianElemCell_DGBR2<PhysDim,MatrixQ<Real>> JacobianElemCellType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    template<class Z>
    using IntegrandType = ArrayQ<Z>;

    static const int nTrace = Topology::NTrace;        // total traces in element

    typedef DLA::VectorS<TopoDim::D, VectorX> LocalAxes;         // manifold local axes type
    typedef DLA::VectorS<PhysDim::D, LocalAxes> VectorLocalAxes; // gradient of local axes type

    BasisWeighted_PDE( const PDE& pde,
                       const DiscretizationDGBR2& disc,
                       const ElementParam&      paramfldElem,
                       const ElementQFieldType& qfldElem,
                       const ElementRFieldType& RfldElem,
                       const bool liftedOnly ) :
      pde_(pde),
      disc_(disc),
      xfldElem_(get<-1>(paramfldElem)),
      qfldElem_(qfldElem),
      RfldElem_(RfldElem),
      paramfldElem_(paramfldElem),
      nDOF_(qfldElem_.nDOF()),
      phi_( new Real[nDOF_] ),
      gradphi_( new VectorX[nDOF_] ),
      liftedOnly_(liftedOnly)
    {
      SANS_ASSERT( qfldElem_.basis() == RfldElem_.basis() );
    }

    ~BasisWeighted_PDE()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element residual integrand
    template<class Ti>
    void operator()( const QuadPointType& s, IntegrandType<Ti> integrand[], int neqn ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem ) const;

  private:
    template<class Tq, class Tg, class Ti>
    void fluxIntegrand( const LocalAxes& e0,
                        const VectorLocalAxes& e0_X,
                        const ParamT& param,
                        const ArrayQ<Tq>& q,
                        const VectorArrayQ<Tg>& gradq,
                        IntegrandType<Ti> integrandPDE[], int neqn ) const;

    template<class Tq, class Tg, class Ti>
    void sourceIntegrand( const LocalAxes& e0,
                          const VectorLocalAxes& e0_X,
                          const ParamT& param,
                          const ArrayQ<Tq>& q,
                          const VectorArrayQ<Tg>& gradqlifted,
                          IntegrandType<Ti> integrandPDE[], int neqn ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementRFieldType& RfldElem_;  // lifting operators, sum over all traces
    const ElementParam&      paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
    const bool liftedOnly_;
  };


  template<class T, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted_LO
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology>       ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology>           ElementQFieldType;
    typedef ElementLift<VectorArrayQ<T>, TopoDim, Topology> ElementRFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;        // total traces in element

    typedef DLA::VectorS<nTrace, VectorArrayQ<T>> IntegrandType;

    BasisWeighted_LO( const PDE& pde,
                      const ElementParam& paramfldElem,
                      const ElementRFieldType& rfldElems ) :
        pde_(pde),
        xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
        rfldElems_(rfldElems),
        paramfldElem_(paramfldElem),
        nDOF_( rfldElems_.nDOFElem() ),
        phi_( new Real[nDOF_] )
    {}

    ~BasisWeighted_LO()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return ( pde_.hasFluxViscous() ||
               (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );
    }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    void operator()( const QuadPointType& s, IntegrandType integrandLO[], int neqn ) const;

  private:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementRFieldType& rfldElems_;  // lifting operators, one per trace
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
  };


  // return integrand functor
  template<class Tq, class Tr, class TopoDim, class Topology, class ElementParam>
  BasisWeighted_PDE<Tq, Tr, TopoDim, Topology, ElementParam>
  integrand_PDE(const ElementParam&                                 paramfldElem,
                const Element<ArrayQ<Tq>, TopoDim, Topology>&       qfldElem,
                const Element<VectorArrayQ<Tr>, TopoDim, Topology>& RfldElem,
                const bool liftedOnly = false ) const
  {
    return {pde_, disc_,
            paramfldElem,
            qfldElem, RfldElem, liftedOnly};
  }


  template<class T, class TopoDim, class Topology, class ElementParam>
  BasisWeighted_LO<T,TopoDim,Topology,ElementParam>
  integrand_LO(const ElementParam& paramfldElem,
               const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& rfldElems) const
  {
    return {pde_, paramfldElem, rfldElems};
  }

private:
  const PDE& pde_;
  const DiscretizationDGBR2& disc_;
  const std::vector<int> cellGroups_;
};


template <class PDE_>
template <class Tq, class Tr, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_DGBR2_manifold<PDE_>::
BasisWeighted_PDE<Tq,Tr,TopoDim,Topology,ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}


template <class PDE_>
template <class Tq, class Tr, class TopoDim, class Topology, class ElementParam>
template<class Ti>
void
IntegrandCell_DGBR2_manifold<PDE_>::
BasisWeighted_PDE<Tq,Tr,TopoDim,Topology,ElementParam>::
operator()( const QuadPointType& sRef, IntegrandType<Ti> integrandPDE[], int neqn ) const
{
  static_assert(TopoDim::D == 1, "Only TopoD1 is implemented so far."); // TODO: generalize

  SANS_ASSERT(neqn == nDOF_);


  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Tq> q;                // solution
  VectorArrayQ<Tq> gradq;      // solution gradient

  VectorX e01;                // basis direction vector
  LocalAxes e0;               // basis direction vector
  VectorLocalAxes e0_X;       // manifold local axes tangential cartesian gradient

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // basis vector and its gradients
  xfldElem_.unitTangent( sRef, e01 );
  e0[0] = e01;

  xfldElem_.evalUnitTangentGradient( sRef, e0_X );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis(gradphi_, nDOF_, gradq);
  else
    gradq = 0;

  typedef typename promote_Surreal<Tq, Tr>::type Tg;

  VectorArrayQ<Tg> gradqlifted;

  if (pde_.hasSource() && pde_.needsSolutionGradientforSource())
  {
    VectorArrayQ<Tr> Rlift;     // Sum of lifting operators

    // lifted gradient: gradient summed with all trace lifting operators
    RfldElem_.evalFromBasis( phi_, nDOF_, Rlift );

    gradqlifted = gradq + Rlift;
  }

  // compute the residual
  fluxIntegrand( e0, e0_X, param, q, gradq, integrandPDE, neqn);

  if (pde_.hasSource())
    sourceIntegrand( e0, e0_X, param, q, gradqlifted, integrandPDE, neqn);
}

template <class PDE>
template <class Tq, class Tr, class TopoDim, class Topology, class ElementParam >
void
IntegrandCell_DGBR2_manifold<PDE>::
BasisWeighted_PDE<Tq,Tr,TopoDim,Topology,ElementParam>::
operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxElem.qDOF == nDOF_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;                 // solution
  VectorArrayQ<Real> gradq;       // gradient
  VectorArrayQ<Real> gradqlifted;

  ArrayQ<SurrealClass> qSurreal = 0;               // solution
  VectorArrayQ<SurrealClass> gradqSurreal = 0;     // gradient
  VectorArrayQ<SurrealClass> gradqliftedSurreal = 0;

  VectorX e01;                // basis direction vector
  LocalAxes e0;               // basis direction vector
  VectorLocalAxes e0_X;       // manifold local axes tangential cartesian gradient

  MatrixQ<Real> PDE_q, PDE_gradq;   // temporary Jacobian storage

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // basis vector and its gradients
  xfldElem_.unitTangent( sRef, e01 );
  e0[0] = e01;

  xfldElem_.evalUnitTangentGradient( sRef, e0_X );

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  qSurreal = q;

  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradq );
    gradqSurreal = gradq;
  }

  if (pde_.hasSource() && pde_.needsSolutionGradientforSource())
  {
    VectorArrayQ<Real> Rlift; // Sum of lifting operators
    // lifted gradient: gradient summed with all trace lifting operators
    RfldElem_.evalFromBasis( phi_, nDOF_, Rlift );

    gradqliftedSurreal = gradqlifted = gradq + Rlift;
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {

    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    fluxIntegrand(e0, e0_X,param, qSurreal, gradq, integrandSurreal.data(), integrandSurreal.size());

    if (pde_.hasSource())
      sourceIntegrand(e0, e0_X,param, qSurreal, gradqlifted, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxElem.PDE_q(i,j) += phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;

  } // nchunk


  if (needsSolutionGradient)
  {
    DLA::VectorD<ArrayQ<SurrealClass>> sourceSurreal( nDOF_ );

    MatrixQ<Real> S_gradq;
    MatrixQ<Real> PDE_gradq;

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
          {
            DLA::index(gradqSurreal[d],       n).deriv(slot + n - nchunk) = 1;
            DLA::index(gradqliftedSurreal[d], n).deriv(slot + n - nchunk) = 1;
          }
        slot += PDE::N;
      }

      integrandSurreal = 0;
      sourceSurreal = 0;

      fluxIntegrand(e0, e0_X, param, q, gradqSurreal, integrandSurreal.data(), integrandSurreal.size());

      if (pde_.hasSource() && pde_.needsSolutionGradientforSource())
        sourceIntegrand(e0, e0_X, param, q, gradqliftedSurreal, sourceSurreal.data(), sourceSurreal.size());

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
          {
            DLA::index(gradqSurreal[d],       n).deriv(slot + n - nchunk) = 0; // Reset the derivative
            DLA::index(gradqliftedSurreal[d], n).deriv(slot + n - nchunk) = 0;
          }

          for (int i = 0; i < nDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
              {
                DLA::index(S_gradq,m,n)   = DLA::index(sourceSurreal[i], m).deriv(slot + n - nchunk);
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk)
                                          + DLA::index(S_gradq,m,n);
              }

            for (int j = 0; j < nDOF_; j++)
            {
              mtxElem.PDE_q(i,j)      += dJ*gradphi_[j][d]*PDE_gradq;
              mtxElem.PDE_R(i,j)(0,d) += dJ*phi_[j]*S_gradq;
            }
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  }
}

template <class PDE_>
template <class T0, class T1, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tg, class Ti>
void
IntegrandCell_DGBR2_manifold<PDE_>::
BasisWeighted_PDE<T0,T1,TopoDim,Topology,ElementParam>::
fluxIntegrand( const LocalAxes& e0,
               const VectorLocalAxes& e0_X,
               const ParamT& param,
               const ArrayQ<Tq>& q,
               const VectorArrayQ<Tg>& gradq,
               IntegrandType<Ti> integrandPDE[], int neqn ) const
{
  // PDE integrand
  for (int k = 0; k < neqn; k++)
    integrandPDE[k] = 0;

  if ( !liftedOnly_ )
  {
    // flux term: -gradient(phi) . F
    VectorArrayQ<Ti> F = 0;          // PDE flux F(X, U), Fv(X, U, UX)

    // advective flux
    if (pde_.hasFluxAdvective())
      pde_.fluxAdvective( param, e0, e0_X, q, F );

    // viscous flux
    if (pde_.hasFluxViscous())
      pde_.fluxViscous( param, q, gradq, F );

    for (int k = 0; k < neqn; k++)
      integrandPDE[k] -= dot(gradphi_[k],F);
  }

  // right-hand-side forcing function: -phi RHS

  if (!liftedOnly_ && pde_.hasForcingFunction())
  {
    SANS_DEVELOPER_EXCEPTION("Not used/implemented yet");

    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < neqn; k++)
      integrandPDE[k] -= phi_[k]*forcing;
  }
}

template <class PDE_>
template <class T0, class T1, class TopoDim, class Topology, class ElementParam>
template<class Tq, class Tg, class Ti>
void
IntegrandCell_DGBR2_manifold<PDE_>::
BasisWeighted_PDE<T0,T1,TopoDim,Topology,ElementParam>::
sourceIntegrand( const LocalAxes& e0,
                 const VectorLocalAxes& e0_X,
                 const ParamT& param,
                 const ArrayQ<Tq>& q,
                 const VectorArrayQ<Tg>& gradqlifted,
                 IntegrandType<Ti> integrandPDE[], int neqn ) const
{
  // source term: +phi S
  // solution gradient augmented by lifting operator (asymptotically dual consistent)

  ArrayQ<Ti> source = 0;           // PDE source S(U, UX)
  pde_.source( param, e0, e0_X, q, gradqlifted, source );

  for (int k = 0; k < neqn; k++)
    integrandPDE[k] += phi_[k]*source;
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam >
void
IntegrandCell_DGBR2_manifold<PDE>::
BasisWeighted_LO<T,TopoDim,Topology,ElementParam>::operator()(
    const QuadPointType& sRef, IntegrandType integrandLO[], int neqn ) const
{
  SANS_ASSERT(neqn == nDOF_);

  VectorArrayQ<T> rlift;     // Lifting operators

  // basis value, gradient
  rfldElems_.evalBasis( sRef, phi_, nDOF_ );

  // Lifting Operator Integrand
  for (int trace = 0; trace < nTrace; trace++)
  {
    // lifting operators
    rfldElems_[trace].evalFromBasis( phi_, nDOF_, rlift );

    // volume term: phi*rlift
    for (int k = 0; k < neqn; k++)
      integrandLO[k][trace] = phi_[k]*rlift;
  }
}

} //namespace SANS

#endif /* INTEGRANDCELL_DGBR2_MANIFOLD_H_ */
