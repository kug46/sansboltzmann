// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_DGBR2_MANIFOLD_H_
#define INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_DGBR2_MANIFOLD_H_

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/DG/DiscretizationDGBR2.h"

#include "Integrand_DGBR2_fwd.h"

namespace SANS
{

// Discretization tag
class DGBR2_manifold;

//----------------------------------------------------------------------------//
// element boundary integrand: PDE

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, DGBR2_manifold> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, DGBR2_manifold> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Flux_mitState Category;
  typedef DGBR2_manifold DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal; // Lifting Operator integrand

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;

  template <class T>
  using TensorMatrixQ = typename PDE::template TensorMatrixQ<T>;    // diffusion matrix

  static const int D = PhysDim::D;          // Physical dimensions
  static const int N = PDE::N;

  IntegrandBoundaryTrace(
          const PDE& pde,
          const BCBase& bc,
          const std::vector<int>& BoundaryGroups,
          const DiscretizationDGBR2& disc) :
      pde_(pde),
      bc_(bc),
      BoundaryGroups_(BoundaryGroups),
      disc_(disc) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  // used to know if the source needs the gradient (i.e. lifting operator)
  bool needsSolutionGradientforSource() const
  {
    return pde_.needsSolutionGradientforSource();
  }

  // does the lifting operator equation depend linearly on the lifting operator
  bool needsLiftingOperatorforLO() const { return false; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam>
  class BasisWeighted_PDE
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    >  ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology>       ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementRFieldCell;

    typedef typename ElementParam::T ParamT;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandType;

    BasisWeighted_PDE( const PDE& pde,
                       const DiscretizationDGBR2& disc,
                       const BCBase& bc,
                       const ElementXFieldTrace& xfldElemTrace,
                       const CanonicalTraceToCell& canonicalTrace,
                       const ElementParam& paramfldElem, // Xfield must be the last parameter
                       const ElementQFieldCell& qfldElem,
                       const ElementRFieldCell& rfldElem ) :
            pde_(pde), disc_(disc), bc_(bc),
            xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
            xfldElem_(get<-1>(paramfldElem)),
            qfldElem_(qfldElem),
            rfldElem_(rfldElem),
            paramfldElem_(paramfldElem),
            nDOF_(qfldElem_.nDOF()),
            phi_( new Real[nDOF_] ),
            gradphi_( new VectorX[nDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElem_.basis() );
    }

    ~BasisWeighted_PDE()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType integrandPDE[], int neqn ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandPDE, neqn);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandType integrandPDE[], int neqn ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class Tq, class Tr,
           class TopoDimTrace, class TopologyTrace,
           class TopoDimCell, class Topology, class ElementParam>
  class BasisWeighted_LO
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>, TopoDimCell, Topology>       ElementQFieldCell;
    typedef Element<VectorArrayQ<Tr>, TopoDimCell, Topology> ElementRFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef DLA::MatrixD<VectorMatrixQ<Real>> MatrixLOElemClass;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef VectorArrayQ<Tr> IntegrandType;

    BasisWeighted_LO( const PDE& pde,
                      const DiscretizationDGBR2& disc,
                      const BCBase& bc,
                      const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                      const ElementParam& paramfldElem, // Xfield must be the last parameter
                      const ElementQFieldCell& qfldElem,
                      const ElementRFieldCell& rfldElem ) :
        pde_(pde), disc_(disc), bc_(bc),
        xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
        xfldElem_(get<-1>(paramfldElem)),
        qfldElem_(qfldElem),
        rfldElem_(rfldElem),
        paramfldElem_(paramfldElem),
        nDOF_(qfldElem_.nDOF()),
        phi_( new Real[nDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElem_.basis() );
    }

    ~BasisWeighted_LO()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& RefTrace, IntegrandType integrandLO[], int neqn ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandLO, neqn);
    }

    // boundary element Jacobian integrand wrt r
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace, MatrixLOElemClass& mtxElemL_rL ) const {}

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, VectorArrayQ<Ti> integrandLO[], int neqn ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
  };

  template < class T, class TopoDimTrace, class TopologyTrace,
                      class TopoDimCell,  class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementRFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef IntegrandDGBR2< Real, VecReal > IntegrandType;

    typedef typename ElementParam::T ParamT;

    FieldWeighted( const PDE& pde,
                   const DiscretizationDGBR2& disc,
                   const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem, // Xfield must be the last parameter
                   const ElementQFieldCell& qfldElem,
                   const ElementRFieldCell& rfldElem,
                   const ElementQFieldCell& wfldElem,
                   const ElementRFieldCell& sfldElem ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)),
                   qfldElem_(qfldElem), rfldElem_(rfldElem),
                   wfldElem_(wfldElem), sfldElem_(sfldElem),
                   paramfldElem_(paramfldElem),
                   nDOF_(qfldElem_.nDOF()),
                   nwDOF_(wfldElem_.nDOF()),
                   phi_( new Real[nDOF_] ),
                   wphi_( new Real[nwDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == sfldElem_.basis() );
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] wphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandType& integrand ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrand);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType& integrand ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementQFieldCell& wfldElem_;
    const ElementRFieldCell& sfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nwDOF_;
    mutable Real *phi_;
    mutable Real *wphi_;
  };

  template < class T, class TopoDimTrace, class TopologyTrace,
                      class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted_PDE< T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam >
  integrand_PDE(const ElementXField<PhysDim  , TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                const ElementParam& paramfldElem, // Xfield must be the last parameter
                const Element<ArrayQ<T>      , TopoDimCell , Topology    >& qfldElem,
                const Element<VectorArrayQ<T>, TopoDimCell , Topology    >& rfldElem ) const
  {
    return BasisWeighted_PDE<T, TopoDimTrace, TopologyTrace,
                                TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                      xfldElemTrace, canonicalTrace,
                                                                      paramfldElem,
                                                                      qfldElem, rfldElem);
  }

  template < class Tq, class Tr,
             class TopoDimTrace, class TopologyTrace,
             class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted_LO< Tq, Tr, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam >
  integrand_LO(const ElementXField<PhysDim  , TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
               const ElementParam& paramfldElem, // Xfield must be the last parameter
               const Element<ArrayQ<Tq>      , TopoDimCell , Topology    >& qfldElem,
               const Element<VectorArrayQ<Tr>, TopoDimCell , Topology    >& rfldElem ) const
  {
    return BasisWeighted_LO<Tq, Tr,
                            TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                  xfldElemTrace, canonicalTrace,
                                                                  paramfldElem,
                                                                  qfldElem, rfldElem);
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, Topology, ElementParam >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<T>,TopoDimCell,Topology      >& qfldElem,
            const Element<VectorArrayQ<T>,TopoDimCell,Topology>& rfldElem,
            const Element<ArrayQ<T>,TopoDimCell,Topology      >& wfldElem,
            const Element<VectorArrayQ<T>,TopoDimCell,Topology>& sfldElem) const
  {
    return FieldWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam >(pde_, disc_, bc_,
                                                                     xfldElemTrace, canonicalTrace,
                                                                     paramfldElem,
                                                                     qfldElem, rfldElem,
                                                                     wfldElem, sfldElem);
  }
private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const DiscretizationDGBR2& disc_;
};

template <class PDE_, class NDBCVector>
template < class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
template <class BC>
void
IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, DGBR2_manifold>::
BasisWeighted_PDE<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType integrandPDE[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes;  // manifold local axes type

  ParamT paramL;             // Elemental parameters (such as grid coordinates and distance functions)
  ParamT paramR;             // Elemental parameters (such as grid coordinates and distance functions)

  VectorX Nrm;              // unit normal (points out of domain)
  VectorX e01L;             // basis direction vector
  LocalAxes e0L;            // basis direction vector
  DLA::VectorS<PhysDim::D, LocalAxes> e0L_X;    // manifold local axes tangential cartesian gradient

  ArrayQ<T> qI, qB;          // interior/BC solution
  VectorArrayQ<T> gradqI;    // interior gradient
  VectorArrayQ<T> dqn;       // jump in q*n

  VectorArrayQ<T> rlift;    // lifting operators
  Real eta = 0;             // BR2 viscous eta

  QuadPointCellType sRefL;  // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRefL );

  // Elemental parameters (includes X)
  paramfldElem_.eval( sRefL, paramL );

  // physical coordinates
  xfldElem_.unitTangent( sRefL, e01L );
  e0L[0] = e01L;

  xfldElem_.evalUnitTangentGradient( sRefL, e0L_X );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRefL, xfldElemTrace_, sRefTrace, Nrm);

  // basis value, gradient
  qfldElem_.evalBasis( sRefL, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRefL, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElem_.evalFromBasis( phi_, nDOF_, qI );
  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradqI );

    rfldElem_.evalFromBasis( phi_, nDOF_, rlift );
    eta = disc_.viscousEta( xfldElemTrace_, xfldElem_, xfldElem_, canonicalTrace_.trace, canonicalTrace_.trace );
  }
  else
  {
    gradqI = 0;
    rlift = 0;
  }

  for (int k = 0; k < neqn; k++)
    integrandPDE[k] = 0;

  // BC data
  bc.state( paramL, Nrm, qI, qB );

  // PDE residual: weak form boundary integral

  ArrayQ<T> Fn = 0;  // normal flux

  //TODO: This should call the BC for the flux
  // advective flux
  if ( pde_.hasFluxAdvective() )
  {
    if ( bc.useUpwind() )
    {
      LocalAxes e0R = e0L;            // basis direction vector
      paramR = paramL;
      pde_.fluxAdvectiveUpwind( paramL, paramR, e0L, e0R, qI, qB, Nrm, Fn );
    }
    else
    {
      VectorArrayQ<T> F = 0;
      pde_.fluxAdvective( paramL, e0L, e0L_X, qB, F );
      Fn += dot(Nrm,F);
    }
  }

  // viscous flux
  if ( pde_.hasFluxViscous() )
  {
    VectorArrayQ<T> Fv = 0;
    VectorArrayQ<T> gradqlifted = gradqI + eta*rlift;
    SANS_DEVELOPER_EXCEPTION("Not implemented");
#if 0
    //Evaluate viscous flux using boundary state qB and interior state gradient gradq
    pde_.fluxViscous( paramL, qB, gradqlifted, Fv );

    Fn += dot(Nrm,Fv); //add normal component of viscous flux

    // Dual consistency term

    MatrixQ<T> uB_qB = 0; // conservation variable jacobians dU(Q)/dQ
    pde_.jacobianMasterState( qB, uB_qB );

    DLA::MatrixS< D, D, MatrixQ<T> > KB  = 0;  // diffusion matrix

    pde_.diffusionViscous( X, qB, KB );

    VectorArrayQ<T> dunB;
    for ( int i = 0; i < D; i++ )
      dunB[i] = uB_qB*dqn[i];

    VectorArrayQ<T> tmpB = KB*dunB;

    for (int k = 0; k < neqn; k++)
      integrand[k].PDE -= dot(gradphi_[k],tmpB);
#endif

  }

  // add normal flux
  for (int k = 0; k < neqn; k++)
    integrandPDE[k] += phi_[k]*Fn;

}

template <class PDE_, class NDBCVector>
template < class Tq, class Tr,
           class TopoDimTrace, class TopologyTrace,
           class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Ti>
void
IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, DGBR2_manifold>::
BasisWeighted_LO<Tq,Tr,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandLO[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT paramI;            // Elemental parameters (such as grid coordinates and distance functions)

  VectorX N;                // unit normal (points out of domain)

  ArrayQ<Tq> qI, qB;          // interior/BC solution
  VectorArrayQ<Tq> dqn;       // jump in q*n

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramI );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, N);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElem_.evalFromBasis( phi_, nDOF_, qI );

  // BC data
  bc.state( paramI, N, qI, qB );

  // Jump in primitive variable [ nL*( qI - qB ) ]
  for (int d = 0; d < D; d++)
    dqn[d] = N[d]*(qI - qB);

  //Lifting operator residuals:
  for (int k = 0; k < neqn; k++)
    integrandLO[k] = phi_[k]*dqn;
}

}


#endif /* INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_DGBR2_MANIFOLD_H_ */
