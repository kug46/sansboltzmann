// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDINTERIORTRACE_DGBR2_H
#define INTEGRANDINTERIORTRACE_DGBR2_H

// interior trace integrand operators: DG BR2 (Little-r formulation)

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/SANSTraitsScalar.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/MatrixLiftedQuantity.h"

#include "DiscretizationDGBR2.h"
#include "JacobianInteriorTrace_DGBR2_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// interior element trace integrand: DG BR2 (Little-r formulation)
//
// integrandL = + [[phiL]].{F} - [[phiL]].(eta*{rL + rR})
// integrandR = + [[phiR]].{F} - [[phiR]].(eta*{rL + rR})
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective, viscous)

template <class PDE_>
class IntegrandInteriorTrace_DGBR2 :
    public IntegrandInteriorTraceType< IntegrandInteriorTrace_DGBR2<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal; // Lifting Operator integrand

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  template <class T>
  using TensorMatrixQ = typename PDE::template TensorMatrixQ<T>;    // diffusion matrix

  template <class Z>
  using MatrixTT = typename MatrixLiftedQuantity<MatrixQ<Z>>::transpose_type; //lifted quantity jacobian wrt q

  static const int D = PhysDim::D;          // Physical dimensions
  static const int N = PDE::N;


  IntegrandInteriorTrace_DGBR2( const PDE& pde,
                                const DiscretizationDGBR2& disc,
                                const std::vector<int>& InteriorTraceGroups,
                                const std::vector<int> PeriodicTraceGroups = {}) :
      pde_(pde),
      disc_(disc),
      interiorTraceGroups_(InteriorTraceGroups),
      periodicTraceGroups_(PeriodicTraceGroups) {}

  std::size_t nInteriorTraceGroups() const            { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const   { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  std::size_t nPeriodicTraceGroups() const          { return periodicTraceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return periodicTraceGroups_[n]; }
  std::vector<int> periodicTraceGroups() const      { return periodicTraceGroups_; }

  // used to know if the source needs the gradient (i.e. lifting operator)
  bool needsSolutionGradientforSource() const
  {
    return pde_.needsSolutionGradientforSource();
  }

  template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                              class TopoDimCell,  class TopologyL,     class TopologyR,
                                                  class ElementParamL, class ElementParamR >
  class BasisWeighted_PDE
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef Element<VectorArrayQ<Tr>, TopoDimCell, TopologyL> ElementRFieldL;
    typedef Element<VectorArrayQ<Tr>, TopoDimCell, TopologyR> ElementRFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_DGBR2_PDE<PhysDim,MatrixQ<Real>> JacobianElemInteriorTraceType;
    typedef JacobianElemInteriorTrace_DGBR2_PDE_RT<PhysDim,MatrixQ<Real>> JacobianRTElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted_PDE( const PDE& pde,
                       const DiscretizationDGBR2& disc,
                       const ElementXFieldTrace& xfldElemTrace,
                       const CanonicalTraceToCell& canonicalTraceL,
                       const CanonicalTraceToCell& canonicalTraceR,
                       const ElementParamL&  paramfldElemL,
                       const ElementQFieldL& qfldElemL,
                       const ElementRFieldL& rfldElemL,
                       const ElementParamR&  paramfldElemR,
                       const ElementQFieldR& qfldElemR,
                       const ElementRFieldR& rfldElemR,
                       const bool liftedOnly = false ) :
                         pde_(pde), disc_(disc),
                         xfldElemTrace_(xfldElemTrace),
                         canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                         xfldElemL_(get<-1>(paramfldElemL)),
                         qfldElemL_(qfldElemL), rfldElemL_(rfldElemL),
                         paramfldElemL_(paramfldElemL),
                         xfldElemR_(get<-1>(paramfldElemR)),
                         qfldElemR_(qfldElemR), rfldElemR_(rfldElemR),
                         paramfldElemR_(paramfldElemR),
                         nDOFL_( qfldElemL_.nDOF() ),
                         nDOFR_( qfldElemR_.nDOF() ),
                         phiL_( new Real[nDOFL_] ),
                         phiR_( new Real[nDOFR_] ),
                         gradphiL_( new VectorX[nDOFL_] ),
                         gradphiR_( new VectorX[nDOFR_] ),
                         liftedOnly_(liftedOnly)
    {
      SANS_ASSERT( qfldElemL_.basis() == rfldElemL_.basis() );
      SANS_ASSERT( qfldElemR_.basis() == rfldElemR_.basis() );
    }

    BasisWeighted_PDE( BasisWeighted_PDE&& bw ) :
                         pde_(bw.pde_), disc_(bw.disc_),
                         xfldElemTrace_(bw.xfldElemTrace_),
                         canonicalTraceL_(bw.canonicalTraceL_), canonicalTraceR_(bw.canonicalTraceR_),
                         xfldElemL_(bw.xfldElemL_),
                         qfldElemL_(bw.qfldElemL_), rfldElemL_(bw.rfldElemL_),
                         paramfldElemL_(bw.paramfldElemL_),
                         xfldElemR_(bw.xfldElemR_),
                         qfldElemR_(bw.qfldElemR_), rfldElemR_(bw.rfldElemR_),
                         paramfldElemR_(bw.paramfldElemR_),
                         nDOFL_( bw.nDOFL_ ),
                         nDOFR_( bw.nDOFR_ ),
                         phiL_( bw.phiL_ ),
                         phiR_( bw.phiR_ ),
                         gradphiL_( bw.gradphiL_ ),
                         gradphiR_( bw.gradphiR_ ),
                         liftedOnly_(bw.liftedOnly_)
    {
      bw.phiL_     = nullptr; bw.phiR_     = nullptr;
      bw.gradphiL_ = nullptr; bw.gradphiR_ = nullptr;
    }

    ~BasisWeighted_PDE()
    {
      delete [] phiL_;
      delete [] phiR_;

      delete [] gradphiL_;
      delete [] gradphiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return pde_.hasFluxAdvective() || pde_.hasFluxViscous(); }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // trace element residual integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], const int neqnL,
                                                          ArrayQ<Ti> integrandR[], const int neqnR ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemR ) const;

    // trace element transposed Jacobian wrt r
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianRTElemInteriorTraceType& mtxElemL,
                     JacobianRTElemInteriorTraceType& mtxElemR ) const;
  private:

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const VectorX& nL,
                            const ParamTL& paramL, const ParamTR& paramR,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                            const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqliftedR,
                            ArrayQ<Ti> integrandL[], const int neqnL,
                            ArrayQ<Ti> integrandR[], const int neqnR,
                            bool gradientOnly = false ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementRFieldL& rfldElemL_;    // lifting operator (L)
    const ElementParamL&  paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementRFieldR& rfldElemR_;    // lifting operator (R)
    const ElementParamR&  paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
    mutable VectorX *gradphiL_;
    mutable VectorX *gradphiR_;

    const bool liftedOnly_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                                        class ElementParamL, class ElementParamR >
  class BasisWeighted_LO
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementRFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyR> ElementRFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_DGBR2_LO<PhysDim> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef VectorArrayQ<T> IntegrandType;

    BasisWeighted_LO( const PDE& pde,
                      const ElementXFieldTrace& xfldElemTrace,
                      const CanonicalTraceToCell& canonicalTraceL,
                      const CanonicalTraceToCell& canonicalTraceR,
                      const ElementParamL&  paramfldElemL,
                      const ElementQFieldL& qfldElemL,
                      const ElementParamR&  paramfldElemR,
                      const ElementQFieldR& qfldElemR ) :
                        pde_(pde),
                        xfldElemTrace_(xfldElemTrace),
                        canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                        xfldElemL_(get<-1>(paramfldElemL)),
                        qfldElemL_(qfldElemL),
                        paramfldElemL_(paramfldElemL),
                        xfldElemR_(get<-1>(paramfldElemR)),
                        qfldElemR_(qfldElemR),
                        paramfldElemR_(paramfldElemR),
                        nDOFL_( qfldElemL_.nDOF() ),
                        nDOFR_( qfldElemR_.nDOF() ),
                        phiL_( new Real[nDOFL_] ),
                        phiR_( new Real[nDOFR_] )
    {}

    BasisWeighted_LO( BasisWeighted_LO&& bw ) :
                        pde_(bw.pde_),
                        xfldElemTrace_(bw.xfldElemTrace_),
                        canonicalTraceL_(bw.canonicalTraceL_), canonicalTraceR_(bw.canonicalTraceR_),
                        xfldElemL_(bw.xfldElemL_),
                        qfldElemL_(bw.qfldElemL_),
                        paramfldElemL_(bw.paramfldElemL_),
                        xfldElemR_(bw.xfldElemR_),
                        qfldElemR_(bw.qfldElemR_),
                        paramfldElemR_(bw.paramfldElemR_),
                        nDOFL_( bw.nDOFL_ ),
                        nDOFR_( bw.nDOFR_ ),
                        phiL_( bw.phiL_ ),
                        phiR_( bw.phiR_ )
    {
      bw.phiL_ = nullptr; bw.phiR_ = nullptr;
    }

    ~BasisWeighted_LO()
    {
      delete [] phiL_;
      delete [] phiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return pde_.hasFluxViscous() ||
            (pde_.hasSource() && pde_.needsSolutionGradientforSource());
    }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], int neqnL,
                                                          VectorArrayQ<Ti> integrandR[], int neqnR ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemLOL,
                     JacobianElemInteriorTraceType& mtxElemLOR ) const;

  private:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementParamL&  paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementParamR&  paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                                        class ElementParamL, class ElementParamR >
  class BasisWeighted_LiftedScalar
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef Element<T, TopoDimCell, TopologyL> ElementSFieldL;
    typedef Element<T, TopoDimCell, TopologyR> ElementSFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_DGBR2_PDE<PhysDim,MatrixTT<Real>> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef T IntegrandType;

    BasisWeighted_LiftedScalar( const PDE& pde,
                                const ElementXFieldTrace& xfldElemTrace,
                                const CanonicalTraceToCell& canonicalTraceL,
                                const CanonicalTraceToCell& canonicalTraceR,
                                const ElementParamL&  paramfldElemL,
                                const ElementQFieldL& qfldElemL,
                                const ElementSFieldL& sfldElemL,
                                const ElementParamR&  paramfldElemR,
                                const ElementQFieldR& qfldElemR,
                                const ElementSFieldR& sfldElemR ) :
                                  pde_(pde),
                                  xfldElemTrace_(xfldElemTrace),
                                  canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                                  xfldElemL_(get<-1>(paramfldElemL)),
                                  qfldElemL_(qfldElemL),
                                  sfldElemL_(sfldElemL),
                                  paramfldElemL_(paramfldElemL),
                                  xfldElemR_(get<-1>(paramfldElemR)),
                                  qfldElemR_(qfldElemR),
                                  sfldElemR_(sfldElemR),
                                  paramfldElemR_(paramfldElemR),
                                  qDOFL_( qfldElemL_.nDOF() ),
                                  qDOFR_( qfldElemR_.nDOF() ),
                                  phiqL_( new Real[qDOFL_] ),
                                  phiqR_( new Real[qDOFR_] ),
                                  sDOFL_( sfldElemL_.nDOF() ),
                                  sDOFR_( sfldElemR_.nDOF() ),
                                  phisL_( new Real[sDOFL_] ),
                                  phisR_( new Real[sDOFR_] )
    {}

    BasisWeighted_LiftedScalar( BasisWeighted_LiftedScalar&& bw ) :
                                  pde_(bw.pde_),
                                  xfldElemTrace_(bw.xfldElemTrace_),
                                  canonicalTraceL_(bw.canonicalTraceL_), canonicalTraceR_(bw.canonicalTraceR_),
                                  xfldElemL_(bw.xfldElemL_),
                                  qfldElemL_(bw.qfldElemL_),
                                  sfldElemL_(bw.sfldElemL_),
                                  paramfldElemL_(bw.paramfldElemL_),
                                  xfldElemR_(bw.xfldElemR_),
                                  qfldElemR_(bw.qfldElemR_),
                                  sfldElemR_(bw.sfldElemR_),
                                  paramfldElemR_(bw.paramfldElemR_),
                                  qDOFL_( bw.nDOFL_ ),
                                  qDOFR_( bw.nDOFR_ ),
                                  phiqL_( bw.phiqL_ ),
                                  phiqR_( bw.phiqR_ ),
                                  sDOFL_( bw.sDOFL_ ),
                                  sDOFR_( bw.sDOFR_ ),
                                  phisL_( bw.phisL_ ),
                                  phisR_( bw.phisR_ )
    {
      bw.phiqL_ = nullptr; bw.phiqR_ = nullptr;
      bw.phisL_ = nullptr; bw.phisR_ = nullptr;
    }

    ~BasisWeighted_LiftedScalar()
    {
      delete [] phiqL_;
      delete [] phiqR_;
      delete [] phisL_;
      delete [] phisR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return pde_.hasSource();
    }

    // total PDE equations
    int nEqn() const { return 1; }

    // total DOFs
    int nDOFLeft() const { return sDOFL_; }
    int nDOFRight() const { return sDOFR_; }

    // trace element residual integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, Ti integrandL[], int neqnL,
                                                          Ti integrandR[], int neqnR ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemR ) const;

  private:

    template<class Tq, class Ti>
    void weightedIntegrand( const ParamTL& paramL, const ParamTR& paramR,
                            const Real& xfldTraceJacDet, const Real& xfldCellJacDetL, const Real& xfldCellJacDetR,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            Ti integrandL[], const int neqnL,
                            Ti integrandR[], const int neqnR ) const;

    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementSFieldL& sfldElemL_;
    const ElementParamL&  paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementSFieldR& sfldElemR_;
    const ElementParamR&  paramfldElemR_;

    //basis functions of qfld
    const int qDOFL_;
    const int qDOFR_;
    mutable Real *phiqL_;
    mutable Real *phiqR_;

    //basis functions of lifted quantity field (can be different to qfld)
    const int sDOFL_;
    const int sDOFR_;
    mutable Real *phisL_;
    mutable Real *phisR_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                                         class ElementParamL, class ElementParamR >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementRFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyR> ElementRFieldR;

    typedef typename ElementXFieldL::RefCoordType RefCoordCellType;
    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef IntegrandDGBR2< Real, Real > IntegrandType;

    FieldWeighted( const PDE& pde,
                   const DiscretizationDGBR2& disc,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
                   const ElementParamL& paramfldElemL, // XField must be the last parameter
                   const ElementQFieldL& qfldElemL,
                   const ElementRFieldL& rfldElemL,
                   const ElementQFieldL& wfldElemL,
                   const ElementRFieldL& sfldElemL,
                   const ElementParamR& paramfldElemR, // XField must be the last parameter
                   const ElementQFieldR& qfldElemR,
                   const ElementRFieldR& rfldElemR,
                   const ElementQFieldR& wfldElemR,
                   const ElementRFieldR& sfldElemR ) :
                   pde_(pde), disc_(disc),
                   xfldElemTrace_(xfldElemTrace),
                   canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                   xfldElemL_(get<-1>(paramfldElemL)),
                   qfldElemL_(qfldElemL), rfldElemL_(rfldElemL),
                   wfldElemL_(wfldElemL), sfldElemL_(sfldElemL),
                   paramfldElemL_( paramfldElemL ),
                   xfldElemR_(get<-1>(paramfldElemR)),
                   qfldElemR_(qfldElemR), rfldElemR_(rfldElemR),
                   wfldElemR_(wfldElemR), sfldElemR_(sfldElemR),
                   paramfldElemR_( paramfldElemR ),
                   nDOFL_( qfldElemL_.nDOF() ),
                   nDOFR_( qfldElemR_.nDOF() ),
                   nwDOFL_( wfldElemL_.nDOF() ),
                   nwDOFR_( wfldElemR_.nDOF() ),
                   phiL_( new Real[nDOFL_] ),
                   phiR_( new Real[nDOFR_] ),
                   gradphiL_( new VectorX[nDOFL_] ),
                   gradphiR_( new VectorX[nDOFR_] ),
                   wphiL_( new Real[nwDOFL_] ),
                   wphiR_( new Real[nwDOFR_] ),
                   wgradphiL_( new VectorX[nwDOFL_] ),
                   wgradphiR_( new VectorX[nwDOFR_] )
    {
      SANS_ASSERT( qfldElemL_.basis() == rfldElemL_.basis() );
      SANS_ASSERT( wfldElemL_.basis() == sfldElemL_.basis() );
      SANS_ASSERT( qfldElemR_.basis() == rfldElemR_.basis() );
      SANS_ASSERT( wfldElemR_.basis() == sfldElemR_.basis() );
    }

    FieldWeighted( FieldWeighted&& fw ) :
                   pde_(fw.pde_), disc_(fw.disc_),
                   xfldElemTrace_(fw.xfldElemTrace_),
                   canonicalTraceL_(fw.canonicalTraceL_), canonicalTraceR_(fw.canonicalTraceR_),
                   xfldElemL_(fw.xfldElemL_),
                   qfldElemL_(fw.qfldElemL_), rfldElemL_(fw.rfldElemL_),
                   wfldElemL_(fw.wfldElemL_), sfldElemL_(fw.sfldElemL_),
                   paramfldElemL_( fw.paramfldElemL_ ),
                   xfldElemR_(fw.xfldElemR_),
                   qfldElemR_(fw.qfldElemR_), rfldElemR_(fw.rfldElemR_),
                   wfldElemR_(fw.wfldElemR_), sfldElemR_(fw.sfldElemR_),
                   paramfldElemR_( fw.paramfldElemR_ ),
                   nDOFL_( fw.nDOFL_ ),
                   nDOFR_( fw.nDOFR_ ),
                   nwDOFL_( fw.nDOFL_ ),
                   nwDOFR_( fw.nDOFR_ ),
                   phiL_( fw.phiL_ ),
                   phiR_( fw.phiR_ ),
                   gradphiL_( fw.gradphiL_ ),
                   gradphiR_( fw.gradphiR_ ),
                   wphiL_( fw.wphiL_ ),
                   wphiR_( fw.wphiR_ ),
                   wgradphiL_( fw.wgradphiL_ ),
                   wgradphiR_( fw.wgradphiR_ )
    {
      fw.phiL_      = nullptr; fw.phiR_      = nullptr;
      fw.gradphiL_  = nullptr; fw.gradphiR_  = nullptr;
      fw.wphiL_     = nullptr; fw.wphiR_     = nullptr;
      fw.wgradphiL_ = nullptr; fw.wgradphiR_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phiL_;
      delete [] phiR_;

      delete [] gradphiL_;
      delete [] gradphiR_;

      delete [] wphiL_;
      delete [] wphiR_;

      delete [] wgradphiL_;
      delete [] wgradphiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return pde_.hasFluxAdvective() || pde_.hasFluxViscous(); }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // trace element integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandType& integrandL,
                                                          IntegrandType& integrandR ) const;

  private:
    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementRFieldL& rfldElemL_;    // lifting operator (L)
    const ElementQFieldL& wfldElemL_;
    const ElementRFieldL& sfldElemL_;    // lifting operator (L)
    const ElementParamL& paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementRFieldR& rfldElemR_;    // lifting operator (R)
    const ElementQFieldR& wfldElemR_;
    const ElementRFieldR& sfldElemR_;    // lifting operator (R)
    const ElementParamR& paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    const int nwDOFL_;
    const int nwDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
    mutable VectorX *gradphiL_;
    mutable VectorX *gradphiR_;
    mutable Real *wphiL_;
    mutable Real *wphiR_;
    mutable VectorX *wgradphiL_;
    mutable VectorX *wgradphiR_;
  };

  template<class Tq, class Tr, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class TopologyL,     class TopologyR,
                                                   class ElementParamL, class ElementParamR>
  BasisWeighted_PDE<Tq, Tr, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL,     TopologyR,
                                          ElementParamL, ElementParamR>
  integrand_PDE(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
                const CanonicalTraceToCell& canonicalTraceL,
                const CanonicalTraceToCell& canonicalTraceR,
                const ElementParamL&                                     paramfldElemL,
                const Element<ArrayQ<Tq>      , TopoDimCell, TopologyL>&   qfldElemL,
                const Element<VectorArrayQ<Tr>, TopoDimCell, TopologyL>& rfldElemL,
                const ElementParamR&                                     paramfldElemR,
                const Element<ArrayQ<Tq>      , TopoDimCell, TopologyR>&   qfldElemR,
                const Element<VectorArrayQ<Tr>, TopoDimCell, TopologyR>& rfldElemR,
                const bool liftedOnly = false) const
  {
    return {pde_, disc_, xfldElemTrace,
            canonicalTraceL, canonicalTraceR,
            paramfldElemL, qfldElemL, rfldElemL,
            paramfldElemR, qfldElemR, rfldElemR,
            liftedOnly};
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                    class ElementParamL, class ElementParamR>
  BasisWeighted_LO<T, TopoDimTrace, TopologyTrace,
                      TopoDimCell, TopologyL, TopologyR,
                      ElementParamL, ElementParamR>
  integrand_LO(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
                 const CanonicalTraceToCell& canonicalTraceL,
                 const CanonicalTraceToCell& canonicalTraceR,
                 const ElementParamL&                                    paramfldElemL,
                 const Element<ArrayQ<T>    , TopoDimCell, TopologyL>&   qfldElemL,
                 const ElementParamR&                                    paramfldElemR,
                 const Element<ArrayQ<T>    , TopoDimCell, TopologyR>&   qfldElemR) const
  {
    return {pde_, xfldElemTrace,
            canonicalTraceL, canonicalTraceR,
            paramfldElemL, qfldElemL,
            paramfldElemR, qfldElemR};
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                    class ElementParamL, class ElementParamR>
  BasisWeighted_LiftedScalar<T, TopoDimTrace, TopologyTrace,
                             TopoDimCell, TopologyL, TopologyR,
                             ElementParamL, ElementParamR>
  integrand_LiftedScalar(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
                         const CanonicalTraceToCell& canonicalTraceL,
                         const CanonicalTraceToCell& canonicalTraceR,
                         const ElementParamL&                               paramfldElemL,
                         const Element<ArrayQ<T>, TopoDimCell, TopologyL>&  qfldElemL,
                         const Element<T        , TopoDimCell, TopologyL>&  sfldElemL,
                         const ElementParamR&                               paramfldElemR,
                         const Element<ArrayQ<T>, TopoDimCell, TopologyR>&  qfldElemR,
                         const Element<T        , TopoDimCell, TopologyR>&  sfldElemR) const
  {
    return {pde_, xfldElemTrace,
            canonicalTraceL, canonicalTraceR,
            paramfldElemL, qfldElemL, sfldElemL,
            paramfldElemR, qfldElemR, sfldElemR};
  }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class TopologyR,
                     class ElementParamL, class ElementParamR >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL, TopologyR,
                   ElementParamL, ElementParamR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>    , TopoDimCell, TopologyL>&   qfldElemL,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& rfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell, TopologyL>&   wfldElemL,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& sfldElemL,
            const ElementParamR& paramfldElemR,    // XField must be the last parameter
            const Element<ArrayQ<T>    , TopoDimCell, TopologyR>&   qfldElemR,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyR>& rfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell, TopologyR>&   wfldElemR,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyR>& sfldElemR) const
  {
    return {pde_, disc_,
            xfldElemTrace,
            canonicalTraceL, canonicalTraceR,
            paramfldElemL,
            qfldElemL, rfldElemL,
            wfldElemL, sfldElemL,
            paramfldElemR,
            qfldElemR, rfldElemR,
            wfldElemR, sfldElemR};
  }

private:
  const PDE& pde_;
  const DiscretizationDGBR2& disc_;
  const std::vector<int> interiorTraceGroups_;
  const std::vector<int> periodicTraceGroups_;
};

//---------------------------------------------------------------------------//
template <class PDE>
template <class Tq, class Tr, class TopoDimTrace, class TopologyTrace,
                              class TopoDimCell,  class TopologyL, class TopologyR,
                                                  class ElementParamL, class ElementParamR>
template <class Ti>
void
IntegrandInteriorTrace_DGBR2<PDE>::
BasisWeighted_PDE<Tq, Tr, TopoDimTrace, TopologyTrace,
                          TopoDimCell,  TopologyL,     TopologyR,
                                        ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], const int neqnL,
                                                 ArrayQ<Ti> integrandR[], const int neqnR ) const
{
  typedef typename promote_Surreal<Tq, Tr>::type Tg;

  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  ParamTL paramL;                  // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;                  // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                      // unit normal for left element (points to right element)

  ArrayQ<Tq> qL, qR;               // solution
  VectorArrayQ<Tq> gradqL, gradqR; // gradient

  VectorArrayQ<Tr> rliftL, rliftR; // lifting operators; x- and y-components
  Real eta = 0;                    // BR2 viscous eta

  QuadPointCellType sRefL;         // reference-element coordinates of the cell
  QuadPointCellType sRefR;

  const bool needsSolutionGradient = pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) || pde_.hasSourceTrace();

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  VectorArrayQ<Tg> gradqliftedL;
  VectorArrayQ<Tg> gradqliftedR;

  // PDE
  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

    rfldElemL_.evalFromBasis( phiL_, nDOFL_, rliftL );
    rfldElemR_.evalFromBasis( phiR_, nDOFR_, rliftR );

    eta = disc_.viscousEta( xfldElemTrace_, xfldElemL_, xfldElemR_, canonicalTraceL_.trace, canonicalTraceR_.trace );

    gradqliftedL = gradqL + eta*rliftL;
    gradqliftedR = gradqR + eta*rliftR;
  }

  // compute the integrand
  weightedIntegrand( nL,
                     paramL, paramR,
                     qL, gradqliftedL,
                     qR, gradqliftedR,
                     integrandL, neqnL,
                     integrandR, neqnR );
}


//---------------------------------------------------------------------------//
template <class PDE>
template <class Tq, class Tr, class TopoDimTrace, class TopologyTrace,
                              class TopoDimCell,  class TopologyL, class TopologyR,
                                                  class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_DGBR2<PDE>::
BasisWeighted_PDE<Tq, Tr, TopoDimTrace, TopologyTrace,
                          TopoDimCell,  TopologyL,     TopologyR,
                                        ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR ) const
{
  // Must be a multiple of PDE::N
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  ParamTL paramL;                    // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;                    // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                        // unit normal for left element (points to right element)

  ArrayQ<Real> qL, qR;               // solution
  VectorArrayQ<Real> gradqL, gradqR; // gradient

  VectorArrayQ<Real> rliftL, rliftR; // lifting operators; x- and y-components
  Real eta = 0;                      // BR2 viscous eta

  ArrayQ<SurrealClass> qSurrealL, qSurrealR; // solution

  QuadPointCellType sRefL;         // reference-element coordinates of the cell
  QuadPointCellType sRefR;

  const bool needsSolutionGradient = pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) || pde_.hasSourceTrace();

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  VectorArrayQ<Real> gradqliftedL;
  VectorArrayQ<Real> gradqliftedR;

  VectorArrayQ<SurrealClass> gradqliftedSurrealL;
  VectorArrayQ<SurrealClass> gradqliftedSurrealR;

  // PDE
  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  qSurrealL = qL;
  qSurrealR = qR;

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

    rfldElemL_.evalFromBasis( phiL_, nDOFL_, rliftL );
    rfldElemR_.evalFromBasis( phiR_, nDOFR_, rliftR );

    eta = disc_.viscousEta( xfldElemTrace_, xfldElemL_, xfldElemR_, canonicalTraceL_.trace, canonicalTraceR_.trace );

    gradqliftedSurrealL = gradqliftedL = gradqL + eta*rliftL;
    gradqliftedSurrealR = gradqliftedR = gradqR + eta*rliftR;
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandRSurreal( nDOFR_ );

  MatrixQ<Real> PDE_q = 0;

  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL, paramR,
                       qSurrealL, gradqliftedL,
                       qSurrealR, gradqliftedR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_ );

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemR.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemL.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemR.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk


  DLA::MatrixDView<Real> phiL(phiL_, 1, nDOFL_);
  DLA::MatrixDView<Real> phiR(phiR_, 1, nDOFR_);

  // compute the lifting operator jacobians at the quadrature point
  DLA::MatrixD<VectorX> rL_qL = eta*(phiL*mtxElemL.r_qL);
  DLA::MatrixD<VectorX> rL_qR = eta*(phiL*mtxElemL.r_qR);

  DLA::MatrixD<VectorX> rR_qL = eta*(phiR*mtxElemR.r_qL);
  DLA::MatrixD<VectorX> rR_qR = eta*(phiR*mtxElemR.r_qR);

  MatrixQ<Real> PDEL_gradqL = 0;
  MatrixQ<Real> PDEL_gradqR = 0;
  MatrixQ<Real> PDER_gradqL = 0;
  MatrixQ<Real> PDER_gradqR = 0;

  // loop over derivative chunks
  // this simultaneously computes derivatives w.r.t gradq, rlift,
  // and the lifting operator quation wrt q
  for (int nchunk = 0; nchunk < PhysDim::D*2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealL[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealR[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

   integrandLSurreal = 0;
   integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL, paramR,
                       qL, gradqliftedSurrealL,
                       qR, gradqliftedSurrealR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_, true );

    // accumulate derivatives into element jacobian

    slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOFL_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDEL_gradqL,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

          const MatrixQ<Real>& PDEL_rL = PDEL_gradqL; // alias for clarity

          // Documentation to explain the simplification below
          // PDEL_qL = gradphiL_[j][d]*PDEL_gradqL;
          // PDEL_rL = PDEL_gradqL;
          // mtxElemL.PDE_qL(i,j) += dJ*(gradphiL_[j][d]*PDEL_gradqL + PDEL_rL*rL_qL(0,j)[d]);
          for (int j = 0; j < nDOFL_; j++)
            mtxElemL.PDE_qL(i,j) += dJ*(gradphiL_[j][d] + rL_qL(0,j)[d])*PDEL_gradqL;

          for (int j = 0; j < nDOFR_; j++)
            mtxElemL.PDE_qR(i,j) += dJ*PDEL_rL*rL_qR(0,j)[d];

        } // for i

        for (int i = 0; i < nDOFR_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDER_gradqL,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

          const MatrixQ<Real>& PDER_rL = PDER_gradqL; // alias for clarity

          // Documentation to explain the simplification below
          // PDER_qL = gradphiL_[j][d]*PDEL_gradqL;
          // PDER_rL = PDER_gradqL;
          // mtxElemR.PDE_qL(i,j) += dJ*(gradphiL_[j][d]*PDER_gradqL + PDER_rL*rL_qL(0,j)[d]);
          for (int j = 0; j < nDOFL_; j++)
            mtxElemR.PDE_qL(i,j) += dJ*(gradphiL_[j][d] + rL_qL(0,j)[d])*PDER_gradqL;

          for (int j = 0; j < nDOFR_; j++)
            mtxElemR.PDE_qR(i,j) += dJ*PDER_rL*rL_qR(0,j)[d];

        } // for i
      } // if slot
      slot += PDE::N;
    }

    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealR[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOFL_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDEL_gradqR,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

          const MatrixQ<Real>& PDEL_rR = PDEL_gradqR; // alias for clarity

          // Documentation to explain the simplification below
          // PDEL_qR = gradphiR_[j][d]*PDEL_gradqR;
          // PDEL_rR = PDEL_gradqR;
          // mtxElemL.PDE_qR(i,j) += dJ*(gradphiR_[j][d]*PDEL_gradqR + PDEL_rR*rR_qR(0,j)[d]);
          for (int j = 0; j < nDOFR_; j++)
            mtxElemL.PDE_qR(i,j) += dJ*(gradphiR_[j][d] + rR_qR(0,j)[d])*PDEL_gradqR;

          for (int j = 0; j < nDOFL_; j++)
            mtxElemL.PDE_qL(i,j) += dJ*PDEL_rR*rR_qL(0,j)[d];
        }

        for (int i = 0; i < nDOFR_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDER_gradqR,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

          const MatrixQ<Real>& PDER_rR = PDER_gradqR; // alias for clarity

          // Documentation to explain the simplification below
          // PDER_qR = gradphiR_[j][d]*PDEL_gradqR;
          // PDER_rR = PDER_gradqR;
          // mtxElemR.PDE_qR(i,j) += dJ*(gradphiR_[j][d]*PDER_gradqR + PDER_rR*rR_qR(0,j)[d]);
          for (int j = 0; j < nDOFR_; j++)
            mtxElemR.PDE_qR(i,j) += dJ*(gradphiR_[j][d] + rR_qR(0,j)[d])*PDER_gradqR;

          for (int j = 0; j < nDOFL_; j++)
            mtxElemR.PDE_qL(i,j) += dJ*PDER_rR*rR_qL(0,j)[d];
        }
      } // if slot
      slot += PDE::N;
    }
  } // nchunk

}


//---------------------------------------------------------------------------//
template <class PDE>
template <class Tq, class Tr, class TopoDimTrace, class TopologyTrace,
                              class TopoDimCell,  class TopologyL, class TopologyR,
                                                  class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_DGBR2<PDE>::
BasisWeighted_PDE<Tq, Tr, TopoDimTrace, TopologyTrace,
                          TopoDimCell,  TopologyL,     TopologyR,
                                        ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianRTElemInteriorTraceType& mtxElemL,
            JacobianRTElemInteriorTraceType& mtxElemR ) const
{
  // Must be a multiple of PDE::N
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  ParamTL paramL;                    // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;                    // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                        // unit normal for left element (points to right element)

  ArrayQ<Real> qL, qR;               // solution
  VectorArrayQ<Real> gradqL, gradqR; // gradient

  VectorArrayQ<Real> rliftL, rliftR; // lifting operators; x- and y-components
  Real eta = 0;                      // BR2 viscous eta

  QuadPointCellType sRefL;         // reference-element coordinates of the cell
  QuadPointCellType sRefR;

  const bool needsSolutionGradient = pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) || pde_.hasSourceTrace();

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  VectorArrayQ<SurrealClass> gradqliftedSurrealL;
  VectorArrayQ<SurrealClass> gradqliftedSurrealR;

  // PDE
  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

    rfldElemL_.evalFromBasis( phiL_, nDOFL_, rliftL );
    rfldElemR_.evalFromBasis( phiR_, nDOFR_, rliftR );

    eta = disc_.viscousEta( xfldElemTrace_, xfldElemL_, xfldElemR_, canonicalTraceL_.trace, canonicalTraceR_.trace );

    gradqliftedSurrealL = gradqL + eta*rliftL;
    gradqliftedSurrealR = gradqR + eta*rliftR;
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandRSurreal( nDOFR_ );

  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  MatrixQ<Real> PDELT_rL = 0;
  MatrixQ<Real> PDELT_rR = 0;
  MatrixQ<Real> PDERT_rL = 0;
  MatrixQ<Real> PDERT_rR = 0;

  // loop over derivative chunks
  // this computes derivatives w.r.t rlift and stores them transposed
  for (int nchunk = 0; nchunk < PhysDim::D*2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealL[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealR[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

   integrandLSurreal = 0;
   integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL, paramR,
                       qL, gradqliftedSurrealL,
                       qR, gradqliftedSurrealR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_, true );

    // accumulate derivatives into element jacobian

    slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOFL_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDELT_rL,n,m) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOFL_; j++)
            mtxElemL.PDET_rL(j,i)[d] += dJ*eta*phiL_[j]*PDELT_rL;
        } // for i

        for (int i = 0; i < nDOFR_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDERT_rL,n,m) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOFL_; j++)
            mtxElemR.PDET_rL(j,i)[d] += dJ*eta*phiL_[j]*PDERT_rL;

        } // for i
      } // if slot
      slot += PDE::N;
    }

    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealR[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOFL_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDELT_rR,n,m) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOFR_; j++)
            mtxElemL.PDET_rR(j,i)[d] += dJ*eta*phiR_[j]*PDELT_rR;
        }

        for (int i = 0; i < nDOFR_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDERT_rR,n,m) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOFR_; j++)
            mtxElemR.PDET_rR(j,i)[d] += dJ*eta*phiR_[j]*PDERT_rR;
        }
      } // if slot
      slot += PDE::N;
    }
  } // nchunk
}

//---------------------------------------------------------------------------//
template <class PDE>
template <class T0, class T1, class TopoDimTrace, class TopologyTrace,
                              class TopoDimCell,  class TopologyL, class TopologyR,
                                                  class ElementParamL, class ElementParamR>
template <class Tq, class Tg, class Ti>
void
IntegrandInteriorTrace_DGBR2<PDE>::
BasisWeighted_PDE<T0, T1, TopoDimTrace, TopologyTrace,
                          TopoDimCell,  TopologyL,     TopologyR,
                                        ElementParamL, ElementParamR>::
weightedIntegrand( const VectorX& nL,
                   const ParamTL& paramL, const ParamTR& paramR,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                   const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqliftedR,
                   ArrayQ<Ti> integrandL[], const int neqnL,
                   ArrayQ<Ti> integrandR[], const int neqnR,
                   bool gradientOnly ) const
{
  ArrayQ<Ti> Fn = 0;               // normal flux

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0;

  //-----------------------------------------------------------------//
  // advective flux term: +[[phi]].flux = (phi@L - phi@R) nL.flux
  //                                    = +phiL nL.flux; -phiR nL.flux
  //-----------------------------------------------------------------//

  if (pde_.hasFluxAdvective() && !liftedOnly_ && !gradientOnly)
    pde_.fluxAdvectiveUpwind( paramL, qL, qR, nL, Fn );

  //-----------------------------------------------------------------//
  // viscous flux term: +[[phi]].flux -[[phi]].(eta*{KL*rL + KL*rR})
  //                    = (phi@L - phi@R) nL.flux -(phi@L - phi@R) nL.(eta*{KL*rL + KR*rR})
  //                    = +phiL nL.flux; -phiR nL.flux; phiL nL.(-eta*{KL*rL + KR*rR}); -phiR nL.(-eta*{KL*rL + KR*rR})
  //-----------------------------------------------------------------//

  if (pde_.hasFluxViscous())
  {
    pde_.fluxViscous( paramL, paramR, qL, gradqliftedL, qR, gradqliftedR, nL, Fn );
  }

  // add normal flux
  for (int k = 0; k < neqnL; k++)
    integrandL[k] +=  phiL_[k]*Fn;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] += -phiR_[k]*Fn;

  //-----------------------------------------------------------------//
  // dual consistency term: -{gradphi^t (dgradU/dgradQ K) }.[[q]]
  //                      = - 0.5*( gradphi@L^t (K*dgradUR/dgradQR) + gradphi@R^t (KR*dgradUR/dgradQR) ). nL*( qL - qR )
  //-----------------------------------------------------------------//

  if (pde_.hasFluxViscous())
  {
    VectorArrayQ<Tq> dqn;            // jump in q.n

    // Jump in primitive variable [ nL*( qL - qR ) ]
    for (int d = 0; d < D; d++)
      dqn[d] = nL[d]*(qL - qR);

    if (pde_.fluxViscousLinearInGradient())
    {
      if (!liftedOnly_ && !gradientOnly)
      {
        // K*dun can be a very expensive operation, this is a shortcut for
        // viscous fluxes of the form K(q).gradu

        VectorArrayQ<Ti> FL = 0;
        VectorArrayQ<Ti> FR = 0;
        pde_.fluxViscous( paramL, qL, dqn, FL );
        pde_.fluxViscous( paramR, qR, dqn, FR );

        for (int k = 0; k < neqnL; k++)
          integrandL[k] += 0.5*dot(gradphiL_[k],FL);

        for (int k = 0; k < neqnR; k++)
          integrandR[k] += 0.5*dot(gradphiR_[k],FR);
      }
    }
    else
    {
      // K*dun can be a very expensive operation, but this works for
      // viscous fluxes of the form K(q,gradq).gradu

      MatrixQ<Tq> uL_qL = 0; // conservation solution jacobians dU(Q)/dQ
      MatrixQ<Tq> uR_qR = 0;
      pde_.jacobianMasterState( paramL, qL, uL_qL );
      pde_.jacobianMasterState( paramR, qR, uR_qR );

      VectorArrayQ<Tq> dunL, dunR;
      for ( int d = 0; d < D; d++ )
      {
        dunL[d] = uL_qL*dqn[d];
        dunR[d] = uR_qR*dqn[d];
      }

      TensorMatrixQ<Ti> KL = 0, KR = 0;   // diffusion matrix

      pde_.diffusionViscous( paramL, qL, gradqliftedL, KL );
      pde_.diffusionViscous( paramR, qR, gradqliftedR, KR );

      VectorArrayQ<Ti> tmpL = KL*dunL;
      VectorArrayQ<Ti> tmpR = KR*dunR;

      for (int k = 0; k < neqnL; k++)
        integrandL[k] -= 0.5*dot(gradphiL_[k],tmpL);

      for (int k = 0; k < neqnR; k++)
        integrandR[k] -= 0.5*dot(gradphiR_[k],tmpR);
    }
  }

  //-----------------------------------------------------------------//
  // trace source term: +phi S
  //-----------------------------------------------------------------//

  if (pde_.hasSourceTrace())
  {
    ArrayQ<Ti> sourceL=0, sourceR=0;      // PDE source S(X, D, U, UX), S(X)

    // We need the number of traces for the left and right elements
    int nTraceL = TopologyL::NTrace;
    int nTraceR = TopologyR::NTrace;

    pde_.sourceTrace( paramL, paramR, qL, gradqliftedL, qR, gradqliftedR, sourceL, sourceR );

    for (int k = 0; k < neqnL; k++)
      integrandL[k] += phiL_[k]*sourceL * xfldElemL_.jacobianDeterminant() / xfldElemTrace_.jacobianDeterminant() / Real(nTraceL);

    for (int k = 0; k < neqnR; k++)
      integrandR[k] += phiR_[k]*sourceR * xfldElemR_.jacobianDeterminant() / xfldElemTrace_.jacobianDeterminant() / Real(nTraceR);
  }
}

//---------------------------------------------------------------------------//
template <class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class TopologyR,
                  class ElementParamL, class ElementParamR>
template <class Ti>
void
IntegrandInteriorTrace_DGBR2<PDE>::
BasisWeighted_LO<T, TopoDimTrace, TopologyTrace,
                    TopoDimCell,  TopologyL,     TopologyR,
                    ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL,
                                                 VectorArrayQ<Ti> integrandR[], const int neqnR ) const
{
  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  VectorX nL;                     // unit normal for left element (points to right element)

  ArrayQ<T> qL, qR;               // solution
  VectorArrayQ<T> dqn;            // jump in q.n

  QuadPointCellType sRefL;         // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );

  // solution value
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < D; d++)
    dqn[d] = nL[d]*(qL - qR);

  // Lifting Operator residual
  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0.5*phiL_[k]*dqn;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0.5*phiR_[k]*dqn;

}

//---------------------------------------------------------------------------//
template <class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class TopologyR,
                  class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_DGBR2<PDE>::
BasisWeighted_LO<T, TopoDimTrace, TopologyTrace,
                    TopoDimCell,  TopologyL,     TopologyR,
                                  ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemLOL,
            JacobianElemInteriorTraceType& mtxElemLOR ) const
{
  SANS_ASSERT(mtxElemLOL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemLOL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemLOL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemLOR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemLOR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemLOR.nDOFR == nDOFR_);

  ParamTL paramL;                    // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;                    // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                        // unit normal for left element (points to right element)

  QuadPointCellType sRefL;         // reference-element coordinates of the cell
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );

  // computes derivatives of the lifting operator equation wrt q
  // the equation is linear, so no Surreal
  // Note that the Jacobian is diagonal MatrixQ with all identical entries.
  // This is reduced to a scalar matrix for computational efficiency

  for (int i = 0; i < nDOFL_; i++)
    for (int j = 0; j < nDOFL_; j++)
      mtxElemLOL._qL(i,j) +=  dJ*0.5*phiL_[i]*phiL_[j]*nL;

  for (int i = 0; i < nDOFR_; i++)
    for (int j = 0; j < nDOFL_; j++)
      mtxElemLOR._qL(i,j) +=  dJ*0.5*phiR_[i]*phiL_[j]*nL;

  for (int i = 0; i < nDOFL_; i++)
    for (int j = 0; j < nDOFR_; j++)
      mtxElemLOL._qR(i,j) += -dJ*0.5*phiL_[i]*phiR_[j]*nL;

  for (int i = 0; i < nDOFR_; i++)
    for (int j = 0; j < nDOFR_; j++)
      mtxElemLOR._qR(i,j) += -dJ*0.5*phiR_[i]*phiR_[j]*nL;
}

//---------------------------------------------------------------------------//
template <class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class TopologyR,
                  class ElementParamL, class ElementParamR>
template <class Ti>
void
IntegrandInteriorTrace_DGBR2<PDE>::
BasisWeighted_LiftedScalar<T, TopoDimTrace, TopologyTrace,
                           TopoDimCell,  TopologyL,     TopologyR,
                           ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace, Ti integrandL[], const int neqnL,
                                                 Ti integrandR[], const int neqnR ) const
{
  SANS_ASSERT(neqnL == sDOFL_);
  SANS_ASSERT(neqnR == sDOFR_);

  ParamTL paramL;                 // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;                 // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> qL, qR;               // solution

  QuadPointCellType sRefL;        // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiqL_, qDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiqR_, qDOFR_ );

  sfldElemL_.evalBasis( sRefL, phisL_, sDOFL_ );
  sfldElemR_.evalBasis( sRefR, phisR_, sDOFR_ );

  // solution value
  qfldElemL_.evalFromBasis( phiqL_, qDOFL_, qL );
  qfldElemR_.evalFromBasis( phiqR_, qDOFR_, qR );

  Real xfldTraceJacDet = xfldElemTrace_.jacobianDeterminant(sRefTrace); //volume of trace
  Real xfldCellJacDetL = xfldElemL_.jacobianDeterminant(); //volume of left element (eval at centerRef to ensure complete cancellation)
  Real xfldCellJacDetR = xfldElemR_.jacobianDeterminant(); //volume of right element (eval at centerRef to ensure complete cancellation)

  // compute the integrand
  weightedIntegrand( paramL, paramR,
                     xfldTraceJacDet, xfldCellJacDetL, xfldCellJacDetR,
                     qL, qR,
                     integrandL, neqnL,
                     integrandR, neqnR );
}

//---------------------------------------------------------------------------//
template <class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class TopologyR,
                  class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_DGBR2<PDE>::
BasisWeighted_LiftedScalar<T, TopoDimTrace, TopologyTrace,
                           TopoDimCell,  TopologyL,     TopologyR,
                           ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR ) const
{
  // Must be a multiple of PDE::N
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxElemL.nTest == sDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == qDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == qDOFR_);

  SANS_ASSERT(mtxElemR.nTest == sDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == qDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == qDOFR_);

  ParamTL paramL;                    // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;                    // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                        // unit normal for left element (points to right element)

  ArrayQ<Real> qL, qR;               // solution
  ArrayQ<SurrealClass> qSurrealL, qSurrealR; // solution

  QuadPointCellType sRefL;         // reference-element coordinates of the cell
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiqL_, qDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiqR_, qDOFR_ );

  sfldElemL_.evalBasis( sRefL, phisL_, sDOFL_ );
  sfldElemR_.evalBasis( sRefR, phisR_, sDOFR_ );

  // solution value
  qfldElemL_.evalFromBasis( phiqL_, qDOFL_, qL );
  qfldElemR_.evalFromBasis( phiqR_, qDOFR_, qR );

  qSurrealL = qL;
  qSurrealR = qR;

  Real xfldTraceJacDet = xfldElemTrace_.jacobianDeterminant(sRefTrace); //volume of trace
  Real xfldCellJacDetL = xfldElemL_.jacobianDeterminant(); //volume of left element (eval at centerRef to ensure complete cancellation)
  Real xfldCellJacDetR = xfldElemR_.jacobianDeterminant(); //volume of right element (eval at centerRef to ensure complete cancellation)

  // element integrand/residual
  DLA::VectorD<SurrealClass> integrandLSurreal( sDOFL_ );
  DLA::VectorD<SurrealClass> integrandRSurreal( sDOFR_ );

  MatrixTT<Real> liftedQuantity_q = 0;

  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( paramL, paramR,
                       xfldTraceJacDet, xfldCellJacDetL, xfldCellJacDetR,
                       qSurrealL, qSurrealR,
                       integrandLSurreal.data(), sDOFL_,
                       integrandRSurreal.data(), sDOFR_ );

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < sDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < 1; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(liftedQuantity_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < qDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += dJ*phiqL_[j]*liftedQuantity_q;
      }

      for (int i = 0; i < sDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < 1; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(liftedQuantity_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < qDOFL_; j++)
          mtxElemR.PDE_qL(i,j) += dJ*phiqL_[j]*liftedQuantity_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < sDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < 1; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(liftedQuantity_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < qDOFR_; j++)
          mtxElemL.PDE_qR(i,j) += dJ*phiqR_[j]*liftedQuantity_q;
      }

      for (int i = 0; i < sDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < 1; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(liftedQuantity_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < qDOFR_; j++)
          mtxElemR.PDE_qR(i,j) += dJ*phiqR_[j]*liftedQuantity_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk
}

//---------------------------------------------------------------------------//
template <class PDE>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL, class TopologyR,
                   class ElementParamL, class ElementParamR>
template <class Tq, class Ti>
void
IntegrandInteriorTrace_DGBR2<PDE>::
BasisWeighted_LiftedScalar<T, TopoDimTrace, TopologyTrace,
                           TopoDimCell,  TopologyL,     TopologyR,
                           ElementParamL, ElementParamR>::
weightedIntegrand( const ParamTL& paramL, const ParamTR& paramR,
                   const Real& xfldTraceJacDet, const Real& xfldCellJacDetL, const Real& xfldCellJacDetR,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                   Ti integrandL[], const int neqnL,
                   Ti integrandR[], const int neqnR ) const
{
  SANS_ASSERT( pde_.hasSourceLiftedQuantity() );

  Ti s = 0; //scalar quantity on trace that will be lifted to volume
  pde_.sourceLiftedQuantity( paramL, paramR, qL, qR, s );

  // We need the number of traces for the left and right elements
  Real nTraceL = TopologyL::NTrace;
  Real nTraceR = TopologyR::NTrace;

  //Normalize by trace volume
  s = s / xfldTraceJacDet;

  // residuals
  for (int k = 0; k < neqnL; k++)
    integrandL[k] = -phisL_[k] * s * xfldCellJacDetL / nTraceL; //additional factors cancel out the volume of each cell

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = -phisR_[k] * s * xfldCellJacDetR / nTraceR; //additional factors cancel out the volume of each cell
}

//---------------------------------------------------------------------------//
template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class TopologyR,
                                       class ElementParamL, class ElementParamR >
void
IntegrandInteriorTrace_DGBR2<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     TopologyR,
                               ElementParamL, ElementParamR>::operator()(
    const QuadPointTraceType& sRefTrace, IntegrandType& integrandL,
                                         IntegrandType& integrandR ) const
{
  ParamTL paramL;                 // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;                 // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                     // unit normal for left element (points to right element)

  ArrayQ<T> qL, qR;               // solution
  VectorArrayQ<T> gradqL, gradqR; // gradient
  VectorArrayQ<T> dqn;            // jump in q.n

  ArrayQ<T> wL, wR;               // weight
  VectorArrayQ<T> gradwL, gradwR; // gradient

  VectorArrayQ<T> rliftL, rliftR; // lifting operators; x- and y-components
  VectorArrayQ<T> sliftL, sliftR; // lifting operators; x- and y-components
  Real eta = 0;                   // BR2 viscous eta

  QuadPointCellType sRefL;        // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient = pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) || pde_.hasSourceTrace();

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // unit normal: points to R
  traceUnitNormal( xfldElemL_, sRefL, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  // weight basis value
  wfldElemL_.evalBasis( sRefL, wphiL_, nwDOFL_ );
  wfldElemR_.evalBasis( sRefR, wphiR_, nwDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, wfldElemL_, wgradphiL_, nwDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, wfldElemR_, wgradphiR_, nwDOFR_ );

  // weight value
  wfldElemL_.evalFromBasis( wphiL_, nwDOFL_, wL );
  wfldElemR_.evalFromBasis( wphiR_, nwDOFR_, wR );
  wfldElemL_.evalFromBasis( wgradphiL_, nwDOFL_, gradwL );
  wfldElemR_.evalFromBasis( wgradphiR_, nwDOFR_, gradwR );

  // PDE
  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

    rfldElemL_.evalFromBasis( phiL_, nDOFL_, rliftL );
    rfldElemR_.evalFromBasis( phiR_, nDOFR_, rliftR );
    sfldElemL_.evalFromBasis( wphiL_, nwDOFL_, sliftL );
    sfldElemR_.evalFromBasis( wphiR_, nwDOFR_, sliftR );

    eta = disc_.viscousEta( xfldElemTrace_, xfldElemL_, xfldElemR_, canonicalTraceL_.trace, canonicalTraceR_.trace );
  }
  else
  {
    gradqL = 0;
    gradqR = 0;
    rliftL = 0;
    rliftR = 0;
    sliftL = 0;
    sliftR = 0;
  }

  integrandL = 0;
  integrandR = 0;

  // advective flux term: +[[phi]].flux = (phi@L - phi@R) nL.flux
  //                                    = +phiL nL.flux; -phiR nL.flux

  ArrayQ<T> Fn = 0;               // normal flux

  if (pde_.hasFluxAdvective())
    pde_.fluxAdvectiveUpwind( paramL, qL, qR, nL, Fn );

  // viscous flux term: +[[phi]].flux -[[phi]].(eta*{KL*rL + KL*rR})
  //                    = (phi@L - phi@R) nL.flux -(phi@L - phi@R) nL.(eta*{KL*rL + KR*rR})
  //                    = +phiL nL.flux; -phiR nL.flux; phiL nL.(-eta*{KL*rL + KR*rR}); -phiR nL.(-eta*{KL*rL + KR*rR})

  // dual consistency term: -{gradphi^t (dgradU/dgradQ K) }.[[q]]
  //                      = - 0.5*( gradphi@L^t (K*dgradUR/dgradQR) + gradphi@R^t (KR*dgradUR/dgradQR) ). nL*( qL - qR )


  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < D; d++)
    dqn[d] = nL[d]*(qL - qR);


  if (pde_.hasFluxViscous())
  {
    VectorArrayQ<T> gradqliftedL = gradqL + eta*rliftL;
    VectorArrayQ<T> gradqliftedR = gradqR + eta*rliftR;

    pde_.fluxViscous( paramL, paramR, qL, gradqliftedL, qR, gradqliftedR, nL, Fn );

    // Dual consistency term

    if (pde_.fluxViscousLinearInGradient())
    {
      // K*dun can be a very expensive operation, this is a shortcut for
      // viscous fluxes of the form K(q).gradu

      VectorArrayQ<T> FL = 0;
      VectorArrayQ<T> FR = 0;
      pde_.fluxViscous( paramL, qL, dqn, FL );
      pde_.fluxViscous( paramR, qR, dqn, FR );

      for (int d = 0; d < PhysDim::D; d++)
      {
        integrandL.PDE += 0.5*dot(gradwL[d],FL[d]);
        integrandR.PDE += 0.5*dot(gradwR[d],FR[d]);
      }
    }
    else
    {
      // K*dun can be a very expensive operation, but this works for
      // viscous fluxes of the form K(q,gradq).gradu

      MatrixQ<T> uL_qL = 0; // conservation solution jacobians dU(Q)/dQ
      MatrixQ<T> uR_qR = 0;
      pde_.jacobianMasterState( paramL, qL, uL_qL );
      pde_.jacobianMasterState( paramR, qR, uR_qR );

      VectorArrayQ<T> dunL, dunR;
      for ( int i = 0; i < D; i++ )
      {
        dunL[i] = uL_qL*dqn[i];
        dunR[i] = uR_qR*dqn[i];
      }

      TensorMatrixQ<T> KL = 0, KR = 0;   // diffusion matrix

      pde_.diffusionViscous( paramL, qL, gradqliftedL, KL );
      pde_.diffusionViscous( paramR, qR, gradqliftedR, KR );

      VectorArrayQ<T> tmpL = KL*dunL;
      VectorArrayQ<T> tmpR = KR*dunR;

      for (int d = 0; d < PhysDim::D; d++)
      {
        integrandL.PDE -= 0.5*dot(gradwL[d],tmpL[d]);
        integrandR.PDE -= 0.5*dot(gradwR[d],tmpR[d]);
      }
    }
  }

  // add normal flux
  integrandL.PDE +=  dot(wL,Fn);
  integrandR.PDE += -dot(wR,Fn);

  integrandL.Lift = 0.5*dot(sliftL,dqn);
  integrandR.Lift = 0.5*dot(sliftR,dqn);

  // traces source term: +w S
  if (pde_.hasSourceTrace())
  {
    ArrayQ<T> sourceL=0, sourceR=0;      // PDE source S(X, Q, QX)
    pde_.sourceTrace( paramL, paramR, qL, gradqL, qR, gradqR, sourceL, sourceR );

    // We need the number of traces for the left and right elements
    int nTraceL = TopologyL::NTrace;
    int nTraceR = TopologyR::NTrace;

    integrandL.PDE += dot(wL,sourceL) * xfldElemL_.jacobianDeterminant() / xfldElemTrace_.jacobianDeterminant() / Real(nTraceL);
    integrandR.PDE += dot(wR,sourceR) * xfldElemR_.jacobianDeterminant() / xfldElemTrace_.jacobianDeterminant() / Real(nTraceR);
  }
}

}

#endif  // INTEGRANDINTERIORTRACE_DGBR2_H
