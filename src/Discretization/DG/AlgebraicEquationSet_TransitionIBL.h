// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_DG_AlgebraicEquationSet_TransitionIBL_H_
#define SRC_DISCRETIZATION_DG_AlgebraicEquationSet_TransitionIBL_H_

#include <vector>
#include <map>
#include <string>

#include "tools/SANSnumerics.h"

#include "pde/BCParameters.h"

#include "Field/FieldTypes.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "FieldBundle_DGAdvective.h"

#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin_CutCellTransitionIBL.h"

#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_manifold.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/QuadratureOrder.h"
#include "Discretization/ResidualNormType.h"
#include "Discretization/DiscretizationObject.h"

#include <type_traits>

#include "Surreal/SurrealS.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"

#include "Discretization/UpdateFraction/UpdateFractionCell.h"
#include "Discretization/UpdateFraction/UpdateFractionInteriorTrace.h"
#include "Discretization/UpdateFraction/UpdateFractionBoundaryTrace_Dispatch.h"

#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/isValidState/isValidStateInteriorTrace.h"
#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch.h"

#include "Discretization/DG/JacobianDetInvResidualNorm.h"

#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"
#include "Discretization/Galerkin/IntegrandInteriorTrace_Galerkin_manifold.h"

#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

//Boundary trace integrands - Galerkin
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin_manifold.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Galerkin_manifold.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin_CutCellTransitionIBL.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/ResidualInteriorTrace_Galerkin.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin_CutCellTransitionIBL.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin.h"

//Parameter Jacobians
#include "Discretization/Galerkin/JacobianCell_Galerkin_Param_CutCellTransitionIBL.h"
#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin_Param.h"
#include "Discretization/Galerkin/JacobianBoundaryTrace_Dispatch_Galerkin_Param.h"

#include "Discretization/Galerkin/Stabilization_Nitsche.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>
#include "tools/plus_std_vector.h"
#endif

namespace SANS
{

class DGAdv_manifold;

template<class DiscTag>
struct DGAdvIntegrands;

template<>
struct DGAdvIntegrands<DGAdv_manifold>
{
  template <class PDE>
  using IntegrandCell = IntegrandCell_Galerkin_manifold<PDE>;

  template <class PDE>
  using IntegrandInteriorTrace = IntegrandInteriorTrace_Galerkin_manifold<PDE>;
};

// refers to src/Discretization/DiscretizationBCTag.h
template<> struct DiscBCTag<BCCategory::Flux_mitState, DGAdv_manifold>       { typedef Galerkin_manifold type; };
template<> struct DiscBCTag<BCCategory::None, DGAdv_manifold>                { typedef Galerkin_manifold type; };
template<> struct DiscBCTag<BCCategory::HubTrace, DGAdv_manifold>            { typedef Galerkin_manifold type; };

//----------------------------------------------------------------------------//
// Mostly imitating the class AlgebraicEquationSet_DGAdvective.  Customized for IBL transition.
//
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
class AlgebraicEquationSet_TransitionIBL : public AlgebraicEquationSet_Debug<NDPDEClass_, Traits>
{
public:
  typedef NDPDEClass_ NDPDEClass;
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
//  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef SurrealS<NDPDEClass::N> SurrealClass;

  typedef Traits TraitsTag;
  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef BCParameters<BCVector> BCParams;

  typedef IntegrandCell_Galerkin_cutCellTransitionIBL<NDPDEClass> IntegrandCellClass;
  typedef typename DGAdvIntegrands<DiscTag>::template IntegrandInteriorTrace<NDPDEClass> IntegrandInteriorTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, DiscTag> IntegrateBoundaryTrace_DispatchClass;

  // Indexes to order the equations and the solution vectors
  static const int nEqnSet = 3;
  static const int nSolSet = 3;
  static_assert(nEqnSet == nSolSet, "");

  static const int iMATCH = 0;
  static const int iPDE = 1;
  static const int iBC = 2;

  static const int iqMatch = 0;
  static const int iq = 1;
  static const int ilg = 2;

  // Constructor
  template< class... BCArgs >
  AlgebraicEquationSet_TransitionIBL(
      const XFieldType& xfld,
      Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& matchfld,
      Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
      Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const NDPDEClass& pde,
      const QuadratureOrder& quadratureOrder,
      const ResidualNormType& resNormType,
      const std::vector<Real>& tol,
      const std::vector<int>& CellGroups,
      const std::vector<int>& InteriorTraceGroups,
      PyDict& BCList,
      const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
      BCArgs&... args ) :
    DebugBaseType(pde, tol),
    stab_(1),
    fcnCell_(pde, CellGroups),
    fcnTrace_(pde, InteriorTraceGroups),
    BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
    dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab_),
    xfld_(xfld),
    matchfld_(matchfld),
    qfld_(qfld),
    lgfld_(lgfld),
    pde_(pde),
    quadratureOrder_(quadratureOrder),
    quadratureOrderMin_(get<-1>(xfld), 0),
    resNormType_(resNormType), tol_(tol),
    AES_(nullptr)
  {
    SANS_ASSERT_MSG( !pde.hasFluxViscous(), "DGAdvection only works for advective PDEs" );

    SANS_ASSERT_MSG(0 == (matchfld_.template getCellGroup<Line>(0)).order(), "matchfld_ should be a p=0 DG field!");

    SANS_ASSERT_MSG( tol_.size() >= nEqnSet, "Should have at least %d sets of equations", nEqnSet );
  }

  virtual ~AlgebraicEquationSet_TransitionIBL() {}

  AlgebraicEquationSet_TransitionIBL(const AlgebraicEquationSet_TransitionIBL&) = delete;
  AlgebraicEquationSet_TransitionIBL& operator=(const AlgebraicEquationSet_TransitionIBL&) = delete;

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz) override;

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override;
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override;

  // Used to compute jacobians wrt. parameters
  template<int iParam, class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override;

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override { setSolutionField(q, matchfld_, qfld_, lgfld_); }
  void setSolutionField(const SystemVectorView& q,
                        Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& matchfld,
                        Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                        Field<PhysDim, TopoDim, ArrayQ>& lgfld);

  //Sets the primal adjoint field and computes the lifting operator adjoint
  virtual void setAdjointField(const SystemVectorView& adj,
                       Field_DG_Cell<PhysDim, TopoDim, ArrayQ>&           wfld,
                       Field<PhysDim, TopoDim, ArrayQ>&                   mufld );

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector and matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // update fraction needed for physically valid state
  virtual Real updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const override;

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Add an AES to this one to daisy-chain
  virtual void addAlgebraicEquationSet(std::shared_ptr<BaseType> AES);

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override;

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override;

  virtual void syncDOFs_MPI() override {}

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  virtual void dumpSolution(const std::string& filename) const override
  {
    output_Tecplot(qfld_, filename);
  }

  const IntegrandCellClass& fcnCell() const { return fcnCell_; }
  const IntegrandInteriorTraceClass& fcnTrace() const { return fcnTrace_; }
  const std::map< std::string, std::shared_ptr<BCBase> >& BCs() const { return BCs_; }
  const IntegrateBoundaryTrace_DispatchClass& dispatchBC() const { return dispatchBC_; }

  const XFieldType& xfld() const { return xfld_; }
  const Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld() const { return qfld_; }
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld() const { return lgfld_; }
  const NDPDEClass& pde() const { return pde_; }

  const QuadratureOrder& quadratureOrder() const { return quadratureOrder_; }
  const QuadratureOrder& quadratureOrderMin() const { return quadratureOrderMin_; }

protected:
  template<class SparseMatrixType>
  void jacobian( SparseMatrixType mtx, const QuadratureOrder& quadratureOrder,
                 const std::vector<int>& interiorTraceCellJac = {} );

  StabilizationNitsche stab_;
  IntegrandCellClass fcnCell_;
  IntegrandInteriorTraceClass fcnTrace_;
  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_DispatchClass dispatchBC_;

  const XFieldType& xfld_;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& matchfld_;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const NDPDEClass& pde_;
  QuadratureOrder quadratureOrder_;
  QuadratureOrder quadratureOrderMin_;
  const ResidualNormType resNormType_;
  const std::vector<Real> tol_;
  std::shared_ptr<BaseType> AES_;
};


//Fills jacobian or the non-zero pattern of a jacobian
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobian(SystemMatrixView& mtx)
{
  if (AES_ != nullptr) AES_->jacobian(mtx);
  this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobian(SystemNonZeroPatternView& nz)
{
  if (AES_ != nullptr) AES_->jacobian(nz);
  this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
}

//Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobianTranspose(SystemMatrixView& mtxT)
{
  if (AES_ != nullptr) AES_->jacobianTranspose(mtxT);
  jacobian(Transpose(mtxT), quadratureOrder_ );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobianTranspose(SystemNonZeroPatternView& nzT)
{
  if (AES_ != nullptr) AES_->jacobianTranspose(nzT);
  jacobian(Transpose(nzT), quadratureOrderMin_ );
}

//Evaluate Residual Norm
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
std::vector<std::vector<Real> >
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
residualNorm( const SystemVectorView& rsd ) const
{
  const int nDOFMATCH = rsd[iMATCH].m();
  const int nDOFPDEpossessed = rsd[iPDE].m();
  const int nDOFBC = rsd[iBC].m();
  const int nMon = pde_.nMonitor();

  DLA::VectorD<Real> rsdMATCHtmp(nMon); rsdMATCHtmp = 0.0;
  DLA::VectorD<Real> rsdPDEtmp(nMon);   rsdPDEtmp = 0.0;
  DLA::VectorD<Real> rsdBCtmp(nMon);    rsdBCtmp = 0.0;

  std::vector<std::vector<Real> > rsdNorm(nResidNorm(), std::vector<Real>(nMon, 0.0));

  //Matching residual norm
#if 1 // TODO: to be cleaned up
  for (int n = 0; n < nDOFMATCH; n++)
  {
    pde_.interpResidVariable(rsd[iMATCH][n], rsdMATCHtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iMATCH][j] += pow(rsdMATCHtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iMATCH][j] = sqrt(rsdNorm[iMATCH][j]);
#else // ignore matching residuals
  for (int j = 0; j < nMon; j++)
    rsdNorm[iMATCH][j] = 0.0;
#endif

  //PDE residual norm
  std::vector<KahanSum<Real> > rsdNormKahan(nMon, 0.0);

  if (resNormType_ == ResidualNorm_L2)
  {
    for (int n = 0; n < nDOFPDEpossessed; n++)
    {
      pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = rsdNormKahan[j];

#ifdef SANS_MPI
    rsdNorm[iPDE] = boost::mpi::all_reduce(*qfld_.comm(), rsdNorm[iPDE], std::plus<std::vector<Real>>());
#endif

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);
  }
  else
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Project::residualNorm - Unknown residual norm type!");

  //BC residual norm
  for (int n = 0; n < nDOFBC; n++)
  {
    pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

  return rsdNorm;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const
{
  titles = {"MATCH: ",
            "PDE  : ",
            "BC   : "};
  idx = {iMATCH, iPDE, iBC};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
residual(SystemVectorView& rsd)
{
  SANS_ASSERT(rsd.m() >= nEqnSet);

  if (AES_ != nullptr) AES_->residual(rsd);

  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd(iMATCH), rsd(iPDE)),
                                           xfld_, (matchfld_, qfld_),
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );

  IntegrateInteriorTraceGroups<TopoDim>::integrate( ResidualInteriorTrace_Galerkin(fcnTrace_, rsd(iPDE)),
                                                    xfld_, qfld_,
                                                    quadratureOrder_.interiorTraceOrders.data(),
                                                    quadratureOrder_.interiorTraceOrders.size() );

  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin( xfld_, qfld_, lgfld_,
                                                          quadratureOrder_.boundaryTraceOrders.data(),
                                                          quadratureOrder_.boundaryTraceOrders.size(),
                                                          rsd(iPDE), rsd(ilg) ),
      ResidualBoundaryTrace_Dispatch_Galerkin( xfld_, qfld_,
                                               quadratureOrder_.boundaryTraceOrders.data(),
                                               quadratureOrder_.boundaryTraceOrders.size(),
                                               rsd(iPDE) ) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder,
         const std::vector<int>& interiorTraceCellJac)
{
  SANS_ASSERT(jac.m() >= nEqnSet);
  SANS_ASSERT(jac.n() >= nSolSet);

  // Get the matrix type of components of SparseMatrixType (aka jac). This could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacMATCH_qMatch = jac(iMATCH,iqMatch);
  Matrix jacMATCH_q = jac(iMATCH,iq);

  Matrix jacPDE_qMatch = jac(iPDE,iqMatch);
  Matrix jacPDE_q      = jac(iPDE,iq);
  Matrix jacPDE_lg     = jac(iPDE,ilg);

  Matrix jacBC_q  = jac(iBC,iq);
  Matrix jacBC_lg = jac(iBC,ilg);

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_Galerkin_TransitionIBL<SurrealClass>(fcnCell_, jacMATCH_qMatch, jacMATCH_q, jacPDE_qMatch, jacPDE_q),
      xfld_, (matchfld_, qfld_), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );
  IntegrateInteriorTraceGroups<TopoDim>::integrate(
      JacobianInteriorTrace_Galerkin(fcnTrace_, jacPDE_q),
      xfld_, qfld_, quadratureOrder.interiorTraceOrders.data(), quadratureOrder.interiorTraceOrders.size() );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_mitLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_, lgfld_,
                                                                   quadratureOrder.boundaryTraceOrders.data(),
                                                                   quadratureOrder.boundaryTraceOrders.size(),
                                                                   jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg ),
      JacobianBoundaryTrace_sansLG_Dispatch_Galerkin<SurrealClass>( xfld_, qfld_,
                                                                    quadratureOrder.boundaryTraceOrders.data(),
                                                                    quadratureOrder.boundaryTraceOrders.size(),
                                                                    jacPDE_q ) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
template<int iParam, class SparseMatrixType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const
{
  SANS_ASSERT(jac.m() >= nEqnSet);
  SANS_ASSERT(jac.n() > ip);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacMATCH_p = jac(iMATCH, ip);
  Matrix jacPDE_p = jac(iPDE, ip);
  Matrix jacBC_p = jac(iBC, ip);

  typedef SurrealS<NDPDEClass::Nparam> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_Galerkin_Param<SurrealClass,iParam>(fcnCell_, jacMATCH_p, jacPDE_p),
                                                       xfld_, (matchfld_, qfld_),
                                                       quadratureOrder_.cellOrders.data(),
                                                       quadratureOrder_.cellOrders.size() );

  IntegrateInteriorTraceGroups<TopoDim>::integrate(
      JacobianInteriorTrace_Galerkin_Param<SurrealClass,iParam>(fcnTrace_, jacPDE_p),
      xfld_, qfld_,
      quadratureOrder_.interiorTraceOrders.data(),
      quadratureOrder_.interiorTraceOrders.size() );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_Param<SurrealClass, iParam>(
          xfld_, qfld_, lgfld_,
          quadratureOrder_.boundaryTraceOrders.data(),
          quadratureOrder_.boundaryTraceOrders.size(),
          jacPDE_p, jacBC_p ),
      JacobianBoundaryTrace_sansLG_Dispatch_Galerkin_Param<SurrealClass, iParam>(
          xfld_, qfld_,
          quadratureOrder_.boundaryTraceOrders.data(),
          quadratureOrder_.boundaryTraceOrders.size(),
          jacPDE_p ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
setSolutionField(const SystemVectorView& q,
                 Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& matchfld,
                 Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                 Field<PhysDim, TopoDim, ArrayQ>& lgfld)
{
  if (AES_ != nullptr) AES_->setSolutionField(q);

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFMATCH = matchfld_.nDOFpossessed() + matchfld_.nDOFghost();
  SANS_ASSERT( nDOFMATCH == q[iqMatch].m() );
  for (int k = 0; k < nDOFMATCH; k++)
    matchfld.DOF(k) = q[iqMatch][k];

  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld.DOF(k) = q[iq][k];

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    lgfld.DOF(k) = q[ilg][k];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
setAdjointField(const SystemVectorView& adj,
                Field_DG_Cell<PhysDim, TopoDim, ArrayQ>&           wfld,
                Field<PhysDim, TopoDim, ArrayQ>&                   mufld )
{
  SANS_DEVELOPER_EXCEPTION("Not implemented yet."); //TODO

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = wfld.nDOFpossessed() + wfld.nDOFghost();
  SANS_ASSERT( nDOFPDE == adj[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    wfld.DOF(k) = adj[iq][k];

  const int nDOFBC = mufld.nDOFpossessed() + mufld.nDOFghost();
  SANS_ASSERT( nDOFBC == adj[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    mufld.DOF(k) = adj[ilg][k];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
fillSystemVector(SystemVectorView& q) const
{
  if (AES_ != nullptr) AES_->fillSystemVector(q);

  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFMATCH = matchfld_.nDOFpossessed() + matchfld_.nDOFghost();
  SANS_ASSERT( nDOFMATCH == q[iqMatch].m() );
  for (int k = 0; k < nDOFMATCH; k++)
    q[iqMatch][k] = matchfld_.DOF(k);

  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld_.DOF(k);

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld_.DOF(k);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
typename AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::VectorSizeClass
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
vectorEqSize() const
{
  static_assert(iMATCH == 0, "Assumed ordering fails!");
  static_assert(iPDE == 1, "Assumed ordering fails!");
  static_assert(iBC == 2, "Assumed ordering fails!");

  // Create the size that represents the equations linear algebra vector
  const int nDOFMATCHpos = matchfld_.nDOFpossessed();
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos = lgfld_.nDOFpossessed();

  std::initializer_list<int> lst = {nDOFMATCHpos, nDOFPDEpos, nDOFBCpos};
  SANS_ASSERT_MSG(lst.size() == nEqnSet, "");

  return lst;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
typename AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::VectorSizeClass
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
vectorStateSize() const
{
  static_assert(iqMatch == 0, "Assumed ordering fails!");
  static_assert(iq == 1, "Assumed ordering fails!");
  static_assert(ilg == 2, "Assumed ordering fails!");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFMATCH = matchfld_.nDOFpossessed() + matchfld_.nDOFghost();
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  std::initializer_list<int> lst = {nDOFMATCH, nDOFPDE, nDOFBC};
  SANS_ASSERT_MSG(lst.size() == nSolSet, "");

  return lst;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
typename AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
matrixSize() const
{
  static_assert(iMATCH == 0, "Assumed ordering fails!");
  static_assert(iPDE == 1, "Assumed ordering fails!");
  static_assert(iBC == 2, "Assumed ordering fails!");

  static_assert(iqMatch == 0, "Assumed ordering fails!");
  static_assert(iq == 1, "Assumed ordering fails!");
  static_assert(ilg == 2, "Assumed ordering fails!");

  // jacobian nonzero pattern
  //
  //           qMatch q  lg
  //   MATCH   0      X  0
  //   PDE     X      X  X
  //   BC      0      X  0

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFMATCHpos = matchfld_.nDOFpossessed();
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos = lgfld_.nDOFpossessed();

  const int nDOFMATCH = matchfld_.nDOFpossessed() + matchfld_.nDOFghost();
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return { { {nDOFMATCHpos, nDOFMATCH}, {nDOFMATCHpos, nDOFPDE}, {nDOFMATCHpos, nDOFBC} },
           { {nDOFPDEpos,   nDOFMATCH}, {nDOFPDEpos,   nDOFPDE}, {nDOFPDEpos,   nDOFBC} },
           { {nDOFBCpos ,   nDOFMATCH}, {nDOFBCpos ,   nDOFPDE}, {nDOFBCpos,    nDOFBC} } };
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class DiscTag, class XFieldType>
Real
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const
{
  // Copy the solution from the linear algebra vector to the field variables
#if 0 // Currently, updates in matching field variables are not limited since the limiting does not seem necessary
  const int nDOFMATCH = matchfld_.nDOF();
  SANS_ASSERT( nDOFMATCH == q[iqMatch].m() );
  SANS_ASSERT( nDOFMATCH == dq[iqMatch].m() );
  Field<PhysDim, TopoDim, ArrayQ> dmatchfld(matchfld_, FieldCopy());
  for (int k = 0; k < nDOFMATCH; k++)
  {
    matchfld_.DOF(k) = q[iqMatch][k];
    dmatchfld.DOF(k) = dq[iqMatch][k];
  }
#endif

  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  SANS_ASSERT( nDOFPDE == dq[iq].m() );
  Field<PhysDim, TopoDim, ArrayQ> dqfld(qfld_, FieldCopy());
  for (int k = 0; k < nDOFPDE; k++)
  {
    qfld_.DOF(k) = q[iq][k];
    dqfld.DOF(k) = dq[iq][k];
  }

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  SANS_ASSERT( nDOFBC == dq[ilg].m() );
  Field<PhysDim, TopoDim, ArrayQ> dlgfld(lgfld_, FieldCopy());
  for (int k = 0; k < nDOFBC; k++)
  {
    lgfld_.DOF(k) = q[ilg][k];
    dlgfld.DOF(k) = dq[ilg][k];
  }

  Real updateFraction = 1.0;

  IntegrateCellGroups<TopoDim>::integrate( UpdateFractionCell(pde_, fcnCell_.cellGroups(), maxChangeFraction, updateFraction),
                                           xfld_, (qfld_, dqfld),
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );
  IntegrateInteriorTraceGroups<TopoDim>::integrate( UpdateFractionInteriorTrace(pde_, fcnTrace_.interiorTraceGroups(),
                                                                                maxChangeFraction, updateFraction),
                                                    xfld_, (qfld_, dqfld),
                                                    quadratureOrder_.interiorTraceOrders.data(),
                                                    quadratureOrder_.interiorTraceOrders.size() );
  dispatchBC_.dispatch(
      UpdateFractionBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, dqfld, lgfld_, dlgfld,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size(), maxChangeFraction, updateFraction ),
      UpdateFractionBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_, dqfld,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size(), maxChangeFraction, updateFraction )
    );

  if (AES_ != nullptr)
    updateFraction = MIN(updateFraction, AES_->updateFraction(q, dq, maxChangeFraction) );

#ifdef SANS_MPI
  return boost::mpi::all_reduce(*qfld_.comm(), updateFraction, boost::mpi::minimum<Real>());
#else
  return updateFraction;
#endif
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
bool
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  bool isValidState = true;

  // Update the solution field
  setSolutionField(q);

  //TODO: check matchfld_ validity

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                             xfld_, qfld_,
                                             quadratureOrder_.cellOrders.data(),
                                             quadratureOrder_.cellOrders.size() );
  IntegrateInteriorTraceGroups<TopoDim>::integrate( isValidStateInteriorTrace(pde_, fcnTrace_.interiorTraceGroups(), isValidState),
                                                    xfld_, qfld_,
                                                    quadratureOrder_.interiorTraceOrders.data(),
                                                    quadratureOrder_.interiorTraceOrders.size() );
  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                quadratureOrder_.boundaryTraceOrders.size(), isValidState ),
      isValidStateBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size(), isValidState )
    );

#ifdef SANS_MPI
  int validstate = isValidState ? 1 : 0;
  isValidState = (boost::mpi::all_reduce(*qfld_.comm(), validstate, std::plus<int>()) == qfld_.comm()->size());
#endif

  bool checkAES_daisychain = true;
  if (AES_ != nullptr) checkAES_daisychain = AES_->isValidStateSystemVector(q);

  return (isValidState && checkAES_daisychain);
}

// Add an AES to this one to daisy-chain
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
addAlgebraicEquationSet(std::shared_ptr<BaseType> AES)
{
  AES_ = AES;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
int
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
nResidNorm() const
{
  return nEqnSet;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
std::vector<GlobalContinuousMap>
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
continuousGlobalMap() const
{
  return {qfld_.continuousGlobalMap(0, lgfld_.nDOFpossessed()),
          lgfld_.continuousGlobalMap(qfld_.nDOFpossessed(), 0)};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
std::shared_ptr<mpi::communicator>
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
comm() const
{
  return qfld_.comm();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_TransitionIBL<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);

//  std::string filename_iBC = filenamebase + "_iBC_iter" + std::to_string(nonlinear_iter) + ".plt";
//  this->dumpLinesearchField(lgfld_, *pStepData, iBC, filename_iBC);
}

} //namespace SANS

#endif /* SRC_DISCRETIZATION_DG_AlgebraicEquationSet_TransitionIBL_H_ */
