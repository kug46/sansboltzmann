// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_OUTPUT_DGBR2_H
#define INTEGRANDBOUNDARYTRACE_OUTPUT_DGBR2_H

// boundary output functional for DGBR2

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/Element/Element.h"

#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Output_Galerkin.h"

#include "pde/OutputCategory.h"
#include "pde/call_derived_functional.h"

#include "Integrand_DGBR2_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary output integrand

template <class PDE_, class NDOutputVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::Functional>, DGBR2> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::Functional>, DGBR2> >
{
public:
  typedef PDE_ PDE;
  typedef OutputCategory::Functional Category;
  typedef DGBR2 DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the transpose Jacobian of this functional
  template<class T>
  using MatrixJ = typename PDE::template ArrayQ<T>;

  explicit IntegrandBoundaryTrace( const DiscretizationDGBR2& disc,
                                   const OutputBase& outputFcn,
                                   const std::vector<int>& BoundaryGroups )
    : disc_(disc), outputFcn_(outputFcn), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,
                    class ElementParam>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>      , TopoDimCell , TopologyL    > ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell , TopologyL    > ElementRFieldL;
    typedef Element<ArrayQ<T>      , TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const DiscretizationDGBR2& disc,
             const OutputBase& outputFcn,
             const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementParam& paramfldElem,
             const ElementQFieldL& qfldElem,
             const ElementRFieldL& rfldElem  ) :
             disc_(disc), outputFcn_(outputFcn),
             xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
             paramfldElem_(paramfldElem), xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
             qfldElem_(qfldElem), rfldElem_(rfldElem),
             nDOF_(qfldElem_.nDOF()),
             phi_( new Real[nDOF_] ),
             gradphi_( new VectorX[nDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElem_.basis() );
    }

    ~Functor()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFElem() const { return qfldElem_.nDOF(); }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, ArrayJ<T>& integrand ) const
    {

      QuadPointCellType sRefCell;

      TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTrace_, sRefTrace, sRefCell );

      ArrayQ<T> qL = 0;
      VectorArrayQ<T> gradqL_lifted; // lifted-gradient
      VectorArrayQ<T> rlift;         // lifting operator

      VectorX X, N = 0;
      ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

      // unit normal: points out of domain
      xfldElemTrace_.unitNormal( sRefTrace, N );

      // evaluate parameters
      paramfldElem_.eval( sRefCell, param );

      // basis value, gradient
      qfldElem_.evalBasis( sRefCell, phi_, nDOF_ );

      // solution value, gradient, lifting operators, viscous eta parameter
      qfldElem_.evalFromBasis( phi_, nDOF_, qL );

      xfldElem_.evalBasisGradient( sRefCell, qfldElem_, gradphi_, nDOF_ );
      qfldElem_.evalFromBasis( gradphi_, nDOF_, gradqL_lifted );

      rfldElem_.evalFromBasis( phi_, nDOF_, rlift );
      Real eta = disc_.viscousEta( xfldElemTrace_, xfldElem_, xfldElem_, canonicalTrace_.trace, canonicalTrace_.trace );
      gradqL_lifted += eta*rlift;

      call_derived_functional<NDOutputVector>(outputFcn_, param, N, qL, gradqL_lifted, integrand);
    }

  protected:
    const DiscretizationDGBR2& disc_;
    const OutputBase& outputFcn_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementParam& paramfldElem_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
    const ElementRFieldL& rfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell, class ElementParam, class BCIntegrandBoundaryTrace>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell, ElementParam >
  integrand(const BCIntegrandBoundaryTrace& fcnBC,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // XField must be the last parameter
            const Element<ArrayQ<T>       , TopoDimCell , TopologyCell >& qfldElem,
            const Element<VectorArrayQ<T> , TopoDimCell , TopologyCell >& rfldElem) const
  {
    return {disc_, outputFcn_, xfldElemTrace, canonicalTrace, paramfldElem, qfldElem, rfldElem};
  }


  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell, class ElementParam, class BCIntegrandBoundaryTrace>
  typename
  IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::Functional>, Galerkin>::template
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell, ElementParam >
  integrand(const BCIntegrandBoundaryTrace& fcnBC,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // XField must be the last parameter
            const Element<ArrayQ<T>       , TopoDimCell , TopologyCell >& qfldElem) const
  {
    return {outputFcn_, xfldElemTrace, canonicalTrace, paramfldElem, qfldElem};
  }

private:
  const DiscretizationDGBR2& disc_;
  const OutputBase& outputFcn_;
  const std::vector<int> BoundaryGroups_;
};

}

#endif  // INTEGRANDBOUNDARYTRACE_OUTPUT_DGBR2_H
