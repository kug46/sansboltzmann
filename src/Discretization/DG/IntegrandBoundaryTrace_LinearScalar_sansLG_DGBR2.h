// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFUNCTOR_LINEARSCALAR_SANSLG_DGBR2_H
#define INTEGRANDBOUNDARYFUNCTOR_LINEARSCALAR_SANSLG_DGBR2_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/Tuple.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/DG/DiscretizationDGBR2.h"

#include "Integrand_DGBR2_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary integrand: PDE

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef DGBR2 DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;      // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal; // Lifting Operator integrand

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;    // solution/residual jacobians

  template <class T>
  using VectorMatrixQ = typename PDE::template VectorMatrixQ<T>;

  template <class T>
  using TensorMatrixQ = typename PDE::template TensorMatrixQ<T>;    // diffusion matrix

  static const int N = PDE::N;

  IntegrandBoundaryTrace(
      const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const DiscretizationDGBR2& disc)
    : pde_(pde), bc_(bc), BoundaryGroups_(BoundaryGroups), disc_(disc) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  // used to know if the source needs the gradient (i.e. lifting operator)
  bool needsSolutionGradientforSource() const
  {
    return pde_.needsSolutionGradientforSource();
  }

  // does the lifting operator equation depend linearly on the lifting operator
  bool needsLiftingOperatorforLO() const
  {
    return pde_.hasFluxViscous();
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam>
  class BasisWeighted_PDE
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementRFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandType;

    BasisWeighted_PDE( const PDE& pde,
                       const DiscretizationDGBR2& disc,
                       const BCBase& bc,
                       const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                       const ElementParam& paramfldElem, // Xfield must be the last parameter
                       const ElementQFieldCell& qfldElem,
                       const ElementRFieldCell& rfldElem ) :
                         pde_(pde), disc_(disc), bc_(bc),
                         xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                         xfldElem_(get<-1>(paramfldElem)),
                         qfldElem_(qfldElem), rfldElem_(rfldElem),
                         paramfldElem_(paramfldElem),
                         nDOF_(qfldElem_.nDOF()),
                         phi_( new Real[nDOF_] ),
                         gradphi_( new VectorX[nDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElem_.basis() );
    }

    BasisWeighted_PDE( BasisWeighted_PDE&& bw ) :
                         pde_(bw.pde_), disc_(bw.disc_), bc_(bw.bc_),
                         xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                         xfldElem_(bw.xfldElem_),
                         qfldElem_(bw.qfldElem_), rfldElem_(bw.rfldElem_),
                         paramfldElem_(bw.paramfldElem_),
                         nDOF_(bw.nDOF_),
                         phi_( bw.phi_ ),
                         gradphi_( bw.gradphi_ )
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted_PDE()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandPDE[], int neqn ) const;

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Tq, class Tg, class Ti>
    void operator()( const BC& bc, const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                     const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrandPDE[], int neqn ) const;

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const Real A, const Real B, const Ti& bcdata,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                            const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrandPDE[], int neqn ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };


  template<class T, class Tr,
           class TopoDimTrace, class TopologyTrace,
           class TopoDimCell, class Topology, class ElementParam>
  class BasisWeighted_LO
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<T>       , TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Tr>, TopoDimCell, Topology> ElementRFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef DLA::MatrixD<VectorMatrixQ<Real>> MatrixLOElemClass;

    typedef typename promote_Surreal<T, Tr>::type Ts;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef VectorArrayQ<Ts> IntegrandType;

    BasisWeighted_LO( const PDE& pde,
                      const DiscretizationDGBR2& disc,
                      const BCBase& bc,
                      const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                      const ElementParam& paramfldElem, // Xfield must be the last parameter
                      const ElementQFieldCell& qfldElem,
                      const ElementRFieldCell& rfldElem ) :
                        pde_(pde), disc_(disc), bc_(bc),
                        xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                        xfldElem_(get<-1>(paramfldElem)),
                        qfldElem_(qfldElem), rfldElem_(rfldElem),
                        paramfldElem_(paramfldElem),
                        nDOF_(qfldElem_.nDOF()),
                        phi_( new Real[nDOF_] ),
                        gradphi_( new VectorX[nDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElem_.basis() );
    }

    BasisWeighted_LO( BasisWeighted_LO&& bw ) :
                        pde_(bw.pde_), disc_(bw.disc_), bc_(bw.bc_),
                        xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                        xfldElem_(bw.xfldElem_),
                        qfldElem_(bw.qfldElem_), rfldElem_(bw.rfldElem_),
                        paramfldElem_(bw.paramfldElem_),
                        nDOF_(bw.nDOF_),
                        phi_( bw.phi_ ),
                        gradphi_( bw.gradphi_ )
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted_LO()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& RefTrace, VectorArrayQ<Ti> integrandLO[], int neqn ) const;

    // boundary element Jacobian integrand wrt r
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace, MatrixLOElemClass& mtxElemL_rL ) const;

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Tq, class Tg, class Ti>
    void operator()( const BC& bc, const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                     const ParamT& paramL, const VectorX& nL, VectorArrayQ<Ti> integrandLO[], int neqn ) const;

    template<class BC, class Tq, class Tg>
    void operator()( const BC& bc, const Real dJ, const Real eta,
                     const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                     const ParamT& paramL, const VectorX& nL, MatrixLOElemClass& mtxElemL_rL ) const;

    template<class Tbc, class Tq, class Tg, class Ti>
    void weightedIntegrand( const Real A, const Real B, const Tbc& bcdata,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                            const ParamT& paramL, const VectorX& nL, VectorArrayQ<Ti> integrandLO[], int neqn ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };

  template < class Tq, class Tw,
             class TopoDimTrace, class TopologyTrace,
             class TopoDimCell,  class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>      , TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Tq>, TopoDimCell, Topology> ElementRFieldCell;

    typedef Element<ArrayQ<Tw>      , TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<VectorArrayQ<Tw>, TopoDimCell, Topology> ElementSFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type Ti;

    typedef IntegrandDGBR2< Ti, Ti > IntegrandType;

    FieldWeighted( const PDE& pde,
                   const DiscretizationDGBR2& disc,
                   const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem, // Xfield must be the last parameter
                   const ElementQFieldCell& qfldElem,
                   const ElementRFieldCell& rfldElem,
                   const ElementWFieldCell& wfldElem,
                   const ElementSFieldCell& sfldElem ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)),
                   qfldElem_(qfldElem), rfldElem_(rfldElem),
                   wfldElem_(wfldElem), sfldElem_(sfldElem),
                   paramfldElem_(paramfldElem),
                   nDOF_(qfldElem_.nDOF()),
                   nwDOF_(wfldElem_.nDOF()),
                   phi_( new Real[nDOF_] ),
                   wphi_( new Real[nwDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == sfldElem_.basis() );
    }

    FieldWeighted( FieldWeighted&& fw ) :
                   pde_(fw.pde_), disc_(fw.disc_), bc_(fw.bc_),
                   xfldElemTrace_(fw.xfldElemTrace_), canonicalTrace_(fw.canonicalTrace_),
                   xfldElem_(fw.xfldElem_),
                   qfldElem_(fw.qfldElem_), rfldElem_(fw.rfldElem_),
                   wfldElem_(fw.wfldElem_), sfldElem_(fw.sfldElem_),
                   paramfldElem_(fw.paramfldElem_),
                   nDOF_(fw.nDOF_),
                   nwDOF_(fw.nwDOF_),
                   phi_( fw.phi_ ),
                   wphi_( fw.wphi_ )
    {
      fw.phi_ = nullptr; fw.wphi_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] wphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandType& integrand ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrand);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType& integrand ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementSFieldCell& sfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nwDOF_;
    mutable Real *phi_;
    mutable Real *wphi_;
  };


  template < class Tq, class Tw,
             class TopoDimTrace, class TopologyTrace,
             class TopoDimCell,  class Topology, class ElementParam >
  class FieldWeighted_Nodal
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>      , TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Tq>, TopoDimCell, Topology> ElementRFieldCell;

    typedef Element<ArrayQ<Tw>      , TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<VectorArrayQ<Tw>, TopoDimCell, Topology> ElementSFieldCell;
    typedef Element<Real, TopoDimCell, Topology> ElementEFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type Ti;

//    typedef IntegrandDGBR2< Ti, Ti > IntegrandType;
    typedef Ti IntegrandType;

    FieldWeighted_Nodal( const PDE& pde,
                   const DiscretizationDGBR2& disc,
                   const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem, // Xfield must be the last parameter
                   const ElementQFieldCell& qfldElem,
                   const ElementRFieldCell& rfldElem,
                   const ElementWFieldCell& wfldElem,
                   const ElementSFieldCell& sfldElem,
                   const ElementEFieldCell& efldElem ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)),
                   qfldElem_(qfldElem), rfldElem_(rfldElem),
                   wfldElem_(wfldElem), sfldElem_(sfldElem),
                   efldElem_(efldElem),
                   paramfldElem_(paramfldElem),
                   nDOF_(qfldElem_.nDOF()),
                   nwDOF_(wfldElem_.nDOF()),
                   phi_( new Real[nDOF_] ),
                   wphi_( new Real[nwDOF_] ),
                   nPhi_(efldElem_.nDOF()),
                   ephi_( new Real[nPhi_] ),
                   gradephi_( new VectorX[nPhi_] ),
                   weight_( new ArrayQ<Tw>[nPhi_] ),
                   gradWeight_( new VectorArrayQ<Tw>[nPhi_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == sfldElem_.basis() );
    }

    FieldWeighted_Nodal( FieldWeighted_Nodal&& fw ) :
                   pde_(fw.pde_), disc_(fw.disc_), bc_(fw.bc_),
                   xfldElemTrace_(fw.xfldElemTrace_), canonicalTrace_(fw.canonicalTrace_),
                   xfldElem_(fw.xfldElem_),
                   qfldElem_(fw.qfldElem_), rfldElem_(fw.rfldElem_),
                   wfldElem_(fw.wfldElem_), sfldElem_(fw.sfldElem_),
                   efldElem_(fw.efldElem_),
                   paramfldElem_(fw.paramfldElem_),
                   nDOF_(fw.nDOF_),
                   nwDOF_(fw.nwDOF_),
                   phi_( fw.phi_ ),
                   wphi_( fw.wphi_ ),
                   nPhi_( fw.nPhi_ ),
                   ephi_( fw.ephi_ ),
                   gradephi_( fw.gradephi_ ),
                   weight_( fw.weight_ ),
                   gradWeight_( fw.gradWeight_ )
    {
      fw.phi_ = nullptr; fw.wphi_ = nullptr;

      fw.ephi_       = nullptr;
      fw.gradephi_   = nullptr;
      fw.weight_     = nullptr;
      fw.gradWeight_ = nullptr;
    }

    ~FieldWeighted_Nodal()
    {
      delete [] phi_;
      delete [] wphi_;


      delete [] ephi_;
      delete [] gradephi_;
      delete [] weight_;
      delete [] gradWeight_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandType integrand[], const int nphi ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrand, nphi);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType integrand[], const int nphi ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementRFieldCell& rfldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementSFieldCell& sfldElem_;
    const ElementEFieldCell& efldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nwDOF_;
    mutable Real *phi_;
    mutable Real *wphi_;
    const int nPhi_;
    mutable Real *ephi_;
    mutable VectorX *gradephi_;
    mutable ArrayQ<Tw> *weight_;
    mutable VectorArrayQ<Tw> *gradWeight_;
  };

  template < class T, class TopoDimTrace, class TopologyTrace,
                      class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted_PDE< T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam >
  integrand_PDE(const ElementXField<PhysDim  , TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                const ElementParam& paramfldElem, // Xfield must be the last parameter
                const Element<ArrayQ<T>      , TopoDimCell , Topology    >& qfldElem,
                const Element<VectorArrayQ<T>, TopoDimCell , Topology    >& rfldElem ) const
  {
    return {pde_, disc_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem,
            qfldElem, rfldElem};
  }

  template < class Tq, class Tr,
             class TopoDimTrace, class TopologyTrace,
             class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted_LO< Tq, Tr, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam >
  integrand_LO(const ElementXField<PhysDim  , TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
               const ElementParam& paramfldElem, // Xfield must be the last parameter
               const Element<ArrayQ<Tq>      , TopoDimCell , Topology    >& qfldElem,
               const Element<VectorArrayQ<Tr>, TopoDimCell , Topology    >& rfldElem ) const
  {
    return {pde_, disc_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem,
            qfldElem, rfldElem};
  }

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace, TopoDimCell, Topology, ElementParam >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<Tq>      ,TopoDimCell,Topology>& qfldElem,
            const Element<VectorArrayQ<Tq>,TopoDimCell,Topology>& rfldElem,
            const Element<ArrayQ<Tw>      ,TopoDimCell,Topology>& wfldElem,
            const Element<VectorArrayQ<Tw>,TopoDimCell,Topology>& sfldElem) const
  {
    return {pde_, disc_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem,
            qfldElem, rfldElem,
            wfldElem, sfldElem};
  }


  //HACKY INTERFACE FOR GALERKIN ERR EST TO USE BR2 BCS
  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted_Nodal<Tq, Tw, TopoDimTrace, TopologyTrace, TopoDimCell, Topology, ElementParam >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<Tq>      ,TopoDimCell,Topology>& qfldElem,
            const Element<VectorArrayQ<Tq>,TopoDimCell,Topology>& rfldElem,
            const Element<ArrayQ<Tw>      ,TopoDimCell,Topology>& wfldElem,
            const Element<VectorArrayQ<Tw>,TopoDimCell,Topology>& sfldElem,
            const Element<Real, TopoDimCell, Topology>& efldElem) const
  {
    return {pde_, disc_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem,
            qfldElem, rfldElem,
            wfldElem, sfldElem, efldElem};
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const DiscretizationDGBR2& disc_;
};

template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
BasisWeighted_PDE<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandPDE[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT paramL;              // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                 // unit normal (points out of domain)

  ArrayQ<T> qL;                 // interior solution
  VectorArrayQ<T> gradqliftedL; // interior gradient lifted

  Real eta = 0;               // BR2 viscous eta

  QuadPointCellType sRef;     // reference-element coordinates on the cell

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent cell-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElem_.evalFromBasis( phi_, nDOF_, qL );
  if (needsSolutionGradient)
  {
    VectorArrayQ<T> gradqL;       // interior gradient
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradqL );

    VectorArrayQ<T> rlift;   // lifting operators
    rfldElem_.evalFromBasis( phi_, nDOF_, rlift );
    eta = disc_.viscousEta( xfldElemTrace_, xfldElem_, xfldElem_, canonicalTrace_.trace, canonicalTrace_.trace );

    // augment the gradient with the lifting operator
    gradqliftedL = gradqL + eta*rlift;
  }
  else
    gradqliftedL = 0;

  call_with_derived<NDBCVector>(*this, bc_, qL, gradqliftedL, paramL, nL, integrandPDE, neqn);
}

template <class PDE, class NDBCVector>
template <class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
BasisWeighted_PDE<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
            const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrandPDE[], int neqn ) const
{
  // scalar BC coefficients
  Real A, B;
  bc.coefficients( paramL, nL, A, B );

  // scalar BC data
  Ti bcdata;
  bc.data( paramL, nL, bcdata );

  // compute the integrand (note the lack of the BC template to reduce compile time)
  weightedIntegrand(A, B, bcdata, qL, gradqliftedL, paramL, nL, integrandPDE, neqn);
}

template <class PDE, class NDBCVector>
template < class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
template <class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
BasisWeighted_PDE<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
weightedIntegrand( const Real A, const Real B, const Ti& bcdata,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                   const ParamT& paramL, const VectorX& nL, ArrayQ<Ti> integrandPDE[], int neqn ) const
{
  for (int k = 0; k < neqn; k++)
    integrandPDE[k] = 0;

  // PDE residual: weak form boundary integral
  if ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() )
  {
    VectorArrayQ<Ti> F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( paramL, qL, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( paramL, qL, gradqliftedL, F );

    ArrayQ<Ti> Fn = dot(nL,F);
    for (int k = 0; k < neqn; k++)
      integrandPDE[k] += phi_[k]*Fn;
  }

  // PDE residual: BC
  // NOTE: only works for scalar equations

  // inviscid term
  DLA::VectorS< PhysDim::D, MatrixQ<T> > a = 0;
  if ( pde_.hasFluxAdvective() )
    pde_.jacobianFluxAdvective( paramL, qL, a );

  Ti an = dot(nL,a); //flux jacobian dotted with normal vector

  // viscous term
  TensorMatrixQ<Ti> K = 0;
  if ( pde_.hasFluxViscous() )
    pde_.diffusionViscous( paramL, qL, gradqliftedL, K );

  VectorArrayQ<Ti> KgradqLlift = K*gradqliftedL;

  // BC equation
  Ti residualBC = (A*qL + B*dot(KgradqLlift,nL) - bcdata)/(A*A + B*B);

  for (int k = 0; k < neqn; k++)
  {
    VectorArrayQ<Ti> Ktgradphi = Transpose(K)*gradphi_[k];
    integrandPDE[k] += (phi_[k]*(B - A*an) - A*dot(Ktgradphi,nL))*residualBC;
  }
}

template <class PDE, class NDBCVector>
template <class T, class Tr,
          class TopoDimTrace, class TopologyTrace,
          class TopoDimCell,  class Topology, class ElementParam >
template <class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
BasisWeighted_LO<T,Tr,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandLO[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT paramL;                 // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                    // unit normal (points out of domain)

  ArrayQ<T> qL;                  // interior solution
  VectorArrayQ<Ts> gradqliftedL; // interior gradient lifted

  Real eta = 0;                  // BR2 viscous eta

  QuadPointCellType sRef;        // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coordinates
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElem_.evalFromBasis( phi_, nDOF_, qL );
  if (needsSolutionGradient)
  {
    VectorArrayQ<T> gradqL;     // interior gradient
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradqL );

    VectorArrayQ<Tr> rlift;     // lifting operator
    rfldElem_.evalFromBasis( phi_, nDOF_, rlift );

    eta = disc_.viscousEta( xfldElemTrace_, xfldElem_, xfldElem_, canonicalTrace_.trace, canonicalTrace_.trace );

    // augment the gradient with the lifting operator
    gradqliftedL = gradqL + eta*rlift;
  }
  else
  {
    gradqliftedL = 0;
  }


  call_with_derived<NDBCVector>(*this, bc_, qL, gradqliftedL, paramL, nL, integrandLO, neqn);
}

template <class PDE, class NDBCVector>
template <class T, class Tr,
          class TopoDimTrace, class TopologyTrace,
          class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
BasisWeighted_LO<T,Tr,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
            const ParamT& paramL, const VectorX& nL, VectorArrayQ<Ti> integrandLO[], int neqn ) const
{
  // Boundary data
  // NOTE: only works for scalar equations

  // scalar BC coefficients
  Real A, B;
  bc.coefficients( paramL, nL, A, B );

  // scalar BC data
  Ti bcdata;
  bc.data( paramL, nL, bcdata );

  // compute the integrand (note the lack of the BC template to reduce compile time)
  weightedIntegrand(A, B, bcdata, qL, gradqliftedL, paramL, nL, integrandLO, neqn);
}


template <class PDE, class NDBCVector>
template <class T, class Tr,
          class TopoDimTrace, class TopologyTrace,
          class TopoDimCell,  class Topology, class ElementParam >
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
BasisWeighted_LO<T,Tr,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace, MatrixLOElemClass& mtxElemL_rL ) const
{
  SANS_ASSERT( mtxElemL_rL.m() == nDOF_ );
  SANS_ASSERT( mtxElemL_rL.n() == nDOF_ );

  ParamT paramL;                 // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                    // unit normal (points out of domain)

  ArrayQ<T> qL;                  // interior solution
  VectorArrayQ<Ts> gradqliftedL; // interior gradient lifted

  Real eta = 0;                  // BR2 viscous eta

  QuadPointCellType sRef;        // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coordinates
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElem_.evalFromBasis( phi_, nDOF_, qL );
  if (needsSolutionGradient)
  {
    VectorArrayQ<T> gradqL;     // interior gradient
    qfldElem_.evalFromBasis( gradphi_, nDOF_, gradqL );

    VectorArrayQ<Tr> rlift;     // lifting operator
    rfldElem_.evalFromBasis( phi_, nDOF_, rlift );

    eta = disc_.viscousEta( xfldElemTrace_, xfldElem_, xfldElem_, canonicalTrace_.trace, canonicalTrace_.trace );

    // augment the gradient with the lifting operator
    gradqliftedL = gradqL + eta*rlift;
  }
  else
  {
    gradqliftedL = 0;
  }

  call_with_derived<NDBCVector>(*this, bc_, dJ, eta, qL, gradqliftedL, paramL, nL, mtxElemL_rL);
}

template <class PDE, class NDBCVector>
template <class T, class Tr,
          class TopoDimTrace, class TopologyTrace,
          class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Tq, class Tg>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
BasisWeighted_LO<T,Tr,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const Real dJ, const Real eta,
            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
            const ParamT& paramL, const VectorX& nL, MatrixLOElemClass& mtxElemL_rL ) const
{
  // Must be a multiple of PDE::N
  typedef SurrealS<PDE::N> SurrealClass;

  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  // PDE residual: BC
  // NOTE: only works for scalar equations

  // scalar BC coefficients
  Real A, B;
  bc.coefficients( paramL, nL, A, B );

  // scalar BC data
  Real bcdata;
  bc.data( paramL, nL, bcdata );

  MatrixQ<Real> PDE_rL = 0;                                      // temporary storage
  VectorArrayQ<SurrealClass> gradqliftedSurrealL = gradqliftedL; // interior gradient lifted

  DLA::VectorD<VectorArrayQ<SurrealClass>> integrandLOSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealL[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    // compute the integrand (note the lack of the BC template to reduce compile time)
    weightedIntegrand(A, B, bcdata, qL, gradqliftedSurrealL, paramL, nL, integrandLOSurreal.data(), integrandLOSurreal.size());

    slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOF_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDE_rL,m,n) = DLA::index(integrandLOSurreal[i][d], m).deriv(slot + n - nchunk);

          for (int j = 0; j < nDOF_; j++)
            mtxElemL_rL(i,j)[d] += dJ*phi_[j]*eta*PDE_rL;

        } // for i
      } // if slot
      slot += PDE::N;
    }

  }
}

template <class PDE, class NDBCVector>
template <class T, class Tr,
          class TopoDimTrace, class TopologyTrace,
          class TopoDimCell,  class Topology, class ElementParam >
template <class Tbc, class Tq, class Tg, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
BasisWeighted_LO<T,Tr,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
weightedIntegrand( const Real A, const Real B, const Tbc& bcdata,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                   const ParamT& paramL, const VectorX& nL, VectorArrayQ<Ti> integrandLO[], int neqn ) const
{
  // viscous term
  TensorMatrixQ<Ti> K = 0;
  if ( pde_.hasFluxViscous() )
    pde_.diffusionViscous( paramL, qL, gradqliftedL, K );

  VectorArrayQ<Ti> Fvlift = K*gradqliftedL;

  // BC equation
  Ti residualBC = (A*qL + B*dot(Fvlift,nL) - bcdata)/(A*A + B*B);

  //Lifting operator residuals:
  VectorArrayQ<Ti> residualLOn;
  for (int d = 0; d < PhysDim::D; d++)
    residualLOn[d] = nL[d]*A*residualBC;

  for (int k = 0; k < neqn; k++)
    integrandLO[k] = phi_[k]*residualLOn;
}

template <class PDE, class NDBCVector>
template <class Tq, class Tw,
          class TopoDimTrace, class TopologyTrace,
          class TopoDimCell,  class Topology, class ElementParam >
template <class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType& integrand ) const
{
  ParamT paramL;                 // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                    // unit normal (points out of domain)

  ArrayQ<Tq> qL;                 // interior solution
  VectorArrayQ<Tq> gradqL;       // interior gradient
  VectorArrayQ<Tq> gradqliftedL; // interior gradient lifted

  ArrayQ<Tw> w;                  // weight
  VectorArrayQ<Tw> gradw;        // weight gradient

  VectorArrayQ<Tq> rlift;        // lifting operators
  VectorArrayQ<Tw> slift;        // lifting operators
  Real eta = 0;                  // BR2 viscous eta

  QuadPointCellType sRef;        // reference-element coordinates

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElem_.evalFromBasis( phi_, nDOF_, qL );
  if (needsSolutionGradient)
  {
    xfldElem_.evalGradient( sRef, qfldElem_, gradqL );

    rfldElem_.evalFromBasis( phi_, nDOF_, rlift );
    eta = disc_.viscousEta( xfldElemTrace_, xfldElem_, xfldElem_, canonicalTrace_.trace, canonicalTrace_.trace );
    // augment the gradient with the lifting operator
    gradqliftedL = gradqL + eta*rlift;
  }
  else
  {
    gradqL = 0;
    gradqliftedL = 0;
    rlift = 0;
  }

  // weight value
  wfldElem_.evalBasis( sRef, wphi_, nwDOF_ );
  wfldElem_.evalFromBasis( wphi_, nwDOF_, w );
  sfldElem_.evalFromBasis( wphi_, nwDOF_, slift );

  xfldElem_.evalGradient( sRef, wfldElem_, gradw );

  integrand = 0;

  // PDE residual: weak form boundary integral
  if ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() )
  {
    VectorArrayQ<Tq> F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( paramL, qL, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( paramL, qL, gradqliftedL, F );

    ArrayQ<Tq> Fn = dot(nL,F);
    integrand.PDE += dot(w,Fn);
  }

  // PDE residual: BC
  // NOTE: only works for scalar equations

  // scalar BC coefficients
  Real A, B;
  bc.coefficients( paramL, nL, A, B );

  // scalar BC data
  Real bcdata;
  bc.data( paramL, nL, bcdata );

  // invisid term
  DLA::VectorS< PhysDim::D, MatrixQ<Tq> > a = 0;
  if ( pde_.hasFluxAdvective() )
    pde_.jacobianFluxAdvective( paramL, qL, a );

  Tq an = dot(nL,a); //flux jacobian dotted with normal vector

  // viscous term
  DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<Tq> > K = 0;
  if ( pde_.hasFluxViscous() )
    pde_.diffusionViscous( paramL, qL, gradqliftedL, K );

  VectorArrayQ<Tq> KgradqLlift = K*gradqliftedL; // = Kn*dq/dn + Ks*dq/ds

  // BC equation
  ArrayQ<Tq> residualBC = (A*qL + B*dot(KgradqLlift,nL) - bcdata)/(A*A + B*B);

  VectorArrayQ<Ti> Ktgradw = Transpose(K)*gradw;
  Tq tmp = (B - A*an);
  integrand.PDE += (dot(w,tmp) - A*dot(Ktgradw,nL))*residualBC;

  //Lifting operator residuals:
  VectorArrayQ<Ti> residualLOn;
  for (int d = 0; d < PhysDim::D; d++)
    residualLOn[d] = nL[d]*A*residualBC;

  integrand.Lift = dot(slift,residualLOn);
}

template <class PDE, class NDBCVector>
template <class Tq, class Tw,
          class TopoDimTrace, class TopologyTrace,
          class TopoDimCell,  class Topology, class ElementParam >
template <class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, DGBR2>::
FieldWeighted_Nodal<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()( const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandType integrand[], const int nphi ) const
{
  ParamT paramL;                 // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                    // unit normal (points out of domain)

  ArrayQ<Tq> qL;                 // interior solution
  VectorArrayQ<Tq> gradqL;       // interior gradient
  VectorArrayQ<Tq> gradqliftedL; // interior gradient lifted

  ArrayQ<Tw> w;                  // weight
  VectorArrayQ<Tw> gradw;        // weight gradient

  VectorArrayQ<Tq> rlift;        // lifting operators
  VectorArrayQ<Tw> slift;        // lifting operators
  Real eta = 0;                  // BR2 viscous eta

  QuadPointCellType sRef;        // reference-element coordinates

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // Elemental parameters
  paramfldElem_.eval( sRef, paramL );

  // unit normal: points out of domain
  traceUnitNormal( xfldElem_, sRef, xfldElemTrace_, sRefTrace, nL);

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );

  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElem_.evalFromBasis( phi_, nDOF_, qL );
  if (needsSolutionGradient)
  {
    xfldElem_.evalGradient( sRef, qfldElem_, gradqL );

    rfldElem_.evalFromBasis( phi_, nDOF_, rlift );
    eta = disc_.viscousEta( xfldElemTrace_, xfldElem_, xfldElem_, canonicalTrace_.trace, canonicalTrace_.trace );
    // augment the gradient with the lifting operator
    gradqliftedL = gradqL + eta*rlift;
  }
  else
  {
    gradqL = 0;
    gradqliftedL = 0;
    rlift = 0;
  }

  // weight value
  wfldElem_.evalBasis( sRef, wphi_, nwDOF_ );
  wfldElem_.evalFromBasis( wphi_, nwDOF_, w );
  sfldElem_.evalFromBasis( wphi_, nwDOF_, slift );

  xfldElem_.evalGradient( sRef, wfldElem_, gradw );

  // estimate basis
  efldElem_.evalBasis( sRef, ephi_, nPhi_ );
  xfldElem_.evalBasisGradient( sRef, efldElem_, gradephi_, nPhi_ );

  for (int k = 0; k < nPhi_; k++)
  {
    weight_[k] = ephi_[k]*w;
    gradWeight_[k] = gradephi_[k]*w + ephi_[k]*gradw;
    integrand[k] = 0;
  }


  // PDE residual: weak form boundary integral
  if ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() )
  {
    VectorArrayQ<Tq> F = 0;     // PDE flux

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( paramL, qL, F );

    // viscous flux
    if ( pde_.hasFluxViscous() )
      pde_.fluxViscous( paramL, qL, gradqliftedL, F );

    ArrayQ<Tq> Fn = dot(nL,F);

    for (int k = 0; k < nPhi_; k++)
      integrand[k] += dot(weight_[k], Fn);
  }

  // PDE residual: BC
  // NOTE: only works for scalar equations

  // scalar BC coefficients
  Real A, B;
  bc.coefficients( paramL, nL, A, B );

  // scalar BC data
  Real bcdata;
  bc.data( paramL, nL, bcdata );

  // invisid term
  DLA::VectorS< PhysDim::D, MatrixQ<Tq> > a = 0;
  if ( pde_.hasFluxAdvective() )
    pde_.jacobianFluxAdvective( paramL, qL, a );

  Tq an = dot(nL,a); //flux jacobian dotted with normal vector

  // viscous term
  DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<Tq> > K = 0;
  if ( pde_.hasFluxViscous() )
    pde_.diffusionViscous( paramL, qL, gradqliftedL, K );

  VectorArrayQ<Tq> KgradqLlift = K*gradqliftedL; // = Kn*dq/dn + Ks*dq/ds

  // BC equation
  ArrayQ<Tq> residualBC = (A*qL + B*dot(KgradqLlift,nL) - bcdata)/(A*A + B*B);



  Tq tmp = (B - A*an);
  for (int k = 0; k < nPhi_; k++)
  {
    integrand[k] += dot(weight_[k], tmp);

    VectorArrayQ<Ti> Ktgradw = Transpose(K)*gradWeight_[k];

    integrand[k] += (A*dot(Ktgradw,nL))*residualBC;
  }

  //Lifting operator residuals:
  VectorArrayQ<Ti> residualLOn;
  for (int d = 0; d < PhysDim::D; d++)
    residualLOn[d] = nL[d]*A*residualBC;

  for (int k = 0; k < nPhi_; k++)
    integrand[k] += dot(slift,residualLOn)*ephi_[k];

}

}

#endif  // INTEGRANDBOUNDARYFUNCTOR_LINEARSCALAR_SANSLG_DGBR2_H
