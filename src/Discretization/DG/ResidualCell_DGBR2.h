// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALCELL_DGBR2_H
#define RESIDUALCELL_DGBR2_H

// Cell integral residual functions

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/FieldLift.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/ElementLift.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG BR2 cell integral
//

template<class IntegrandCell, template<class> class Vector>
class ResidualCell_DGBR2_impl :
    public GroupIntegralCellType< ResidualCell_DGBR2_impl<IntegrandCell, Vector> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;

  typedef Real LiftedT; //data-type for lifted quantity

  // Save off the boundary trace integrand and the residual vectors
  ResidualCell_DGBR2_impl( const IntegrandCell& fcn, Vector<ArrayQ>& rsdPDEGlobal ) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
                               FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );

    comm_rank_ = qfld.comm()->rank();
  }

  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple,
                                       Field<PhysDim, TopoDim, ArrayQ>,
                                       FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                                       Field<PhysDim, TopoDim, LiftedT>>::type& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobal,
      const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<Topology>& fldsCell,
      const int quadratureorder )
  {
    typedef typename XFieldType                                ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ          >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementrFieldClass;
    typedef Element<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementrFieldClass rfldElems( rfldCell.basis() );
    ElementRFieldClass RfldElem( rfldCell.basis() );

    // number of integrals evaluated
    const int nDOF = qfldElem.nDOF();
    const int nIntegrand = nDOF;

    // element integral
    typedef ArrayQ IntegrandType;
    GalerkinWeightedIntegral<TopoDim, Topology, IntegrandType> integral(quadratureorder, nIntegrand);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == rfldCell.nElem() );

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nDOF, -1 );

    // residual array
    std::vector< IntegrandType > rsdPDEElem( nIntegrand );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      qfldCell.getElement( qfldElem, elem );

      // only need to perform the volume integral on processors that possess the element
      if ( qfldElem.rank() != comm_rank_ ) continue;

      xfldCell.getElement( xfldElem, elem );
      rfldCell.getElement( rfldElems, elem );

      // accumulate the lifting operator DOFs into a single element
      RfldElem.vectorViewDOF() = 0;
      for (int trace = 0; trace < Topology::NTrace; trace++)
        RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

      for (int n = 0; n < nIntegrand; n++)
        rsdPDEElem[n] = 0;

      // cell integration for canonical element
      integral( fcn_.integrand_PDE(xfldElem, qfldElem, RfldElem),
                get<-1>(xfldElem), rsdPDEElem.data(), nIntegrand );

      // scatter-add element integral to global
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nDOF );

      int nGlobal;
      for (int n = 0; n < nDOF; n++)
      {
        nGlobal = mapDOFGlobal[n];
        rsdPDEGlobal_[nGlobal] += rsdPDEElem[n];
      }
    }
  }

  //For case with lifted quantity field for source terms
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobal,
      const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
      const typename MakeTuple<FieldTuple,
                               Field<PhysDim, TopoDim, ArrayQ>,
                               FieldLift<PhysDim, TopoDim, VectorArrayQ>,
                               Field<PhysDim, TopoDim, LiftedT>>::type
                               ::template FieldCellGroupType<Topology>& fldsCell,
      const int quadratureorder )
  {
    typedef typename XFieldType                               ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ          >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, LiftedT         >::template FieldCellGroupType<Topology> SFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementrFieldClass;
    typedef Element<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;
    typedef typename SFieldCellGroupType::template ElementType<> ElementSFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);
    const SFieldCellGroupType& sfldCell = get<2>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementrFieldClass rfldElems( rfldCell.basis() );
    ElementRFieldClass RfldElem( rfldCell.basis() );
    std::shared_ptr<ElementSFieldClass> psfldElem = std::make_shared<ElementSFieldClass>( sfldCell.basis() );

    // number of integrals evaluated
    const int nDOF = qfldElem.nDOF();
    const int nIntegrand = nDOF;

    // element integral
    typedef ArrayQ IntegrandType;
    GalerkinWeightedIntegral<TopoDim, Topology, IntegrandType> integral(quadratureorder, nIntegrand);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == rfldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == sfldCell.nElem() );

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nDOF, -1 );

    // residual array
    std::vector< IntegrandType > rsdPDEElem( nIntegrand );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      qfldCell.getElement( qfldElem, elem );

      // only need to perform the volume integral on processors that possess the element
      if ( qfldElem.rank() != comm_rank_ ) continue;

      xfldCell.getElement( xfldElem, elem );
      rfldCell.getElement( rfldElems, elem );
      sfldCell.getElement( *psfldElem, elem );

      // accumulate the lifting operator DOFs into a single element
      RfldElem.vectorViewDOF() = 0;
      for (int trace = 0; trace < Topology::NTrace; trace++)
        RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

      for (int n = 0; n < nIntegrand; n++)
          rsdPDEElem[n] = 0;

      // cell integration for canonical element
      integral( fcn_.integrand_PDE(xfldElem, qfldElem, RfldElem, psfldElem),
                get<-1>(xfldElem), rsdPDEElem.data(), nIntegrand );

      // scatter-add element integral to global
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nDOF );

      int nGlobal;
      for (int n = 0; n < nDOF; n++)
      {
        nGlobal = mapDOFGlobal[n];
        rsdPDEGlobal_[nGlobal] += rsdPDEElem[n];
      }
    }
  }

protected:
  const IntegrandCell& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  mutable int comm_rank_;
};


// Factory function

template<class IntegrandCell, template<class> class Vector, class ArrayQ>
ResidualCell_DGBR2_impl<IntegrandCell, Vector>
ResidualCell_DGBR2( const IntegrandCellType<IntegrandCell>& fcnPDE,
                    Vector<ArrayQ>& rsdPDEGlobal )
{
  static_assert( std::is_same< ArrayQ, typename IntegrandCell::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualCell_DGBR2_impl<IntegrandCell, Vector>(fcnPDE.cast(), rsdPDEGlobal);
}

} //namespace SANS

#endif  // RESIDUALCELL_DGBR2_H
