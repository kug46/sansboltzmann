// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_DGBR2_H
#define JACOBIANINTERIORTRACE_DGBR2_H

// interior-trace integral jacobian functions

#include <algorithm> // find

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/FieldData/FieldDataMatrixD_CellLift.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "JacobianInteriorTrace_DGBR2_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// DG BR2 interior trace integral Jacobian
//

template<class IntegrandInteriorTrace>
class JacobianInteriorTrace_DGBR2_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_DGBR2_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<DLA::VectorS<PhysDim::D,Real>> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianInteriorTrace_DGBR2_impl(const IntegrandInteriorTrace& fcn,
                                   const FieldDataInvMassMatrix_Cell& mmfld,
                                   const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
                                   MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                   const std::vector<int>& interiorTraceCellJac) :
    fcn_(fcn), mmfld_(mmfld), jacPDE_R_(jacPDE_R), mtxGlobalPDE_q_(mtxGlobalPDE_q),
    interiorTraceCellJac_(interiorTraceCellJac), comm_rank_(0) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {

    // Left types
    typedef typename XFieldType                                ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>           ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<>        ElementRFieldClassL;

    // Right types
    typedef typename XFieldType                                ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>           ::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> RFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<>        ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<>        ElementQFieldClassR;
    typedef typename RFieldCellGroupTypeR::template ElementType<>        ElementRFieldClassR;

    // Trace types
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef JacobianElemInteriorTrace_DGBR2_PDE<PhysDim, MatrixQ> JacobianElemInteriorTracePDEType;
    typedef JacobianElemInteriorTrace_DGBR2_LO <PhysDim> JacobianElemInteriorTraceLOType;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const RFieldCellGroupTypeR& rfldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementRFieldClassR rfldElemR( rfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element / number of integrals evaluated per element
    int nDOFL = qfldElemL.nDOF();
    int nDOFR = qfldElemR.nDOF();
    const int nIntegrandL = nDOFL;
    const int nIntegrandR = nDOFR;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL);
    std::vector<int> mapDOFGlobal_qR(nDOFR);

    // Inverse mass matrix used to compute lifing operators
    const DLA::MatrixDView_Array<Real>& mmfldCellL = mmfld_.getCellGroupGlobal(cellGroupGlobalL);
    const DLA::MatrixDView_Array<Real>& mmfldCellR = mmfld_.getCellGroupGlobal(cellGroupGlobalR);

    const DLA::MatrixDView_Array<RowMatrixQ>& PDE_rL = jacPDE_R_.getCellGroupGlobal(cellGroupGlobalL);
    const DLA::MatrixDView_Array<RowMatrixQ>& PDE_rR = jacPDE_R_.getCellGroupGlobal(cellGroupGlobalR);

    JacobianElemInteriorTraceSize sizeL(nDOFL, nDOFL, nDOFR);
    JacobianElemInteriorTraceSize sizeR(nDOFR, nDOFL, nDOFR);

    // PDE trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTracePDEType, JacobianElemInteriorTracePDEType>
      integralPDE(quadratureorder);

    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceLOType, JacobianElemInteriorTraceLOType>
      integralLO(quadratureorder);

    // element PDE jacobian matrices wrt q
    JacobianElemInteriorTracePDEType mtxElemL(sizeL);
    JacobianElemInteriorTracePDEType mtxElemR(sizeR);

    JacobianElemInteriorTraceLOType mtxElemLOL(sizeL);
    JacobianElemInteriorTraceLOType mtxElemLOR(sizeR);

    // complete lifting operator jacobian wrt q after mass matrix multiplication
    MatrixLOElemClass rL_qL(nDOFL, nDOFL);
    MatrixLOElemClass rR_qL(nDOFR, nDOFL);

    MatrixLOElemClass rL_qR(nDOFL, nDOFR);
    MatrixLOElemClass rR_qR(nDOFR, nDOFR);

    if (interiorTraceCellJac_.size() > 0)
    {
      // Determine if we need jacobians wrt the left cell
      if ( std::find(interiorTraceCellJac_.begin(), interiorTraceCellJac_.end(), cellGroupGlobalL) == interiorTraceCellJac_.end() )
        nDOFL = 0;

      // Determine if we need jacobians wrt the right cell
      if ( std::find(interiorTraceCellJac_.begin(), interiorTraceCellJac_.end(), cellGroupGlobalR) == interiorTraceCellJac_.end() )
        nDOFR = 0;
    }

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // left/right elements
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      rfldCellR.getElement( rfldElemR, elemR, canonicalTraceR.trace );

      xfldTrace.getElement( xfldElemTrace, elem );

      const int rankL = qfldElemL.rank();
      const int rankR = qfldElemR.rank();

      // nothing to do if both elements are ghost elements
      if (rankL != comm_rank_ && rankR != comm_rank_) continue;

      // lifting operator trace integration for canonical element
      integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                    xfldElemL, qfldElemL,
                                    xfldElemR, qfldElemR),
                   xfldElemTrace, mtxElemLOL, mtxElemLOR);

      // Finish up the lifting operator jacobians
      if ( nDOFL > 0 ) mtxElemL.r_qL = -mmfldCellL[elemL]*mtxElemLOL._qL;
      if ( nDOFR > 0 ) mtxElemL.r_qR = -mmfldCellL[elemL]*mtxElemLOL._qR;

      if ( nDOFL > 0 ) mtxElemR.r_qL = -mmfldCellR[elemR]*mtxElemLOR._qL;
      if ( nDOFR > 0 ) mtxElemR.r_qR = -mmfldCellR[elemR]*mtxElemLOR._qR;

      // PDE trace integration for canonical element
      integralPDE( fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                      xfldElemL, qfldElemL, rfldElemL,
                                      xfldElemR, qfldElemR, rfldElemR),
                   xfldElemTrace, mtxElemL, mtxElemR);

      // Add the chain rule from the lifting operators to the PDE jacobian
      // TODO: This is a very expensive operation (yeah surprising).
      //       The computational cost can be significantly reduced by moving this to JacobianCell,
      //       but that makes JacobianCell a non-local operation (sigh...)
      if (fcn_.needsSolutionGradientforSource())
      {
        if ( nDOFL > 0 ) mtxElemL.PDE_qL += PDE_rL[ elemL ]*mtxElemL.r_qL;
        if ( nDOFR > 0 ) mtxElemL.PDE_qR += PDE_rL[ elemL ]*mtxElemL.r_qR;

        if ( nDOFL > 0 ) mtxElemR.PDE_qL += PDE_rR[ elemR ]*mtxElemR.r_qL;
        if ( nDOFR > 0 ) mtxElemR.PDE_qR += PDE_rR[ elemR ]*mtxElemR.r_qR;
      }

      // scatter-add element jacobian to global
      scatterAdd(
          xfldTrace,
          qfldCellL, qfldCellR,
          rankL, rankR,
          elemL, elemR,
          mapDOFGlobal_qL.data(), nIntegrandL,
          mapDOFGlobal_qR.data(), nIntegrandR,
          mtxElemL.PDE_qL,
          mtxElemL.PDE_qR,
          mtxElemR.PDE_qL,
          mtxElemR.PDE_qR,
          mtxGlobalPDE_q_);

    } //loop over elements

  }

//----------------------------------------------------------------------------//
  template <class XFieldTraceGroupType,
            class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const XFieldTraceGroupType& xfldTrace,
      const QFieldCellGroupTypeL& qfldCellL,
      const QFieldCellGroupTypeR& qfldCellR,
      const int rankL, const int rankR,
      const int elemL, const int elemR,
      int mapDOFGlobal_qL[], const int nDOFL,
      int mapDOFGlobal_qR[], const int nDOFR,

      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qR,

      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qR,

      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q)
  {
    // global mapping for qL
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    // global mapping for qR
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobal_qR, nDOFR );

    // jacobian wrt qL
    if (rankL == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qL, mapDOFGlobal_qL, nDOFL );
    if (rankR == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qL, mapDOFGlobal_qR, nDOFR, mapDOFGlobal_qL, nDOFL );

    // jacobian qrt qR
    if (rankL == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qR, mapDOFGlobal_qL, nDOFL, mapDOFGlobal_qR, nDOFR );
    if (rankR == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qR, mapDOFGlobal_qR, nDOFR );
  }

  #if 0
  //----------------------------------------------------------------------------//
  template <class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            class MatrixQ,
            template <class> class SparseMatrix>
  void
  JacobianInteriorTrace_DGBR2_ScatterAdd(
      const QFieldCellGroupTypeL& qfldL,
      const QFieldCellGroupTypeR& qfldR,
      const int elemL, const int elemR,
      int mapDOFGlobalL[], const int nDOFL,
      int mapDOFGlobalR[], const int nDOFR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemLL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemLR,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemRL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemRR,
      SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxPDEGlobal )
  {

  #if 0
    std::cout << "JacobianPDE_Group_2DInteriorEdge_ScatterAdd: elemL, elemR = " << elemL << " " << elemR << std:endl;
    std::cout << "JacobianPDE_Group_2DInteriorEdge_ScatterAdd (before): mtxPDEGlobal = ";
    mtxPDEGlobal.dump(2);
  #endif

    mtxPDEGlobal.scatterAdd( mtxPDEElemLL, elemL, elemL );
    mtxPDEGlobal.scatterAdd( mtxPDEElemLR, elemL, elemR );
    mtxPDEGlobal.scatterAdd( mtxPDEElemRL, elemR, elemL );
    mtxPDEGlobal.scatterAdd( mtxPDEElemRR, elemR, elemR );
  }
  #endif


protected:
  const IntegrandInteriorTrace& fcn_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  const std::vector<int> interiorTraceCellJac_;
  mutable int comm_rank_;
};


// Factory function

template<class IntegrandInteriorTrace, class RowMatrixQ, class MatrixQ>
JacobianInteriorTrace_DGBR2_impl<IntegrandInteriorTrace>
JacobianInteriorTrace_DGBR2( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                             const FieldDataInvMassMatrix_Cell& mmfld,
                             const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                             std::vector<int> interiorTraceCellJac = {})
{
  return {fcn.cast(), mmfld, jacPDE_R, mtxGlobalPDE_q, interiorTraceCellJac};
}

}

#endif  // JACOBIANINTERIORTRACE_DGBR2_H
