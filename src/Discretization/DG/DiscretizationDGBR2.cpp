// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//

#include <ostream>
#include <cmath> // sqrt

#include "Topology/ElementTopology.h"
#include "Discretization/DG/DiscretizationDGBR2.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// distance between straight line and point
static
Real DistancePointLine( Real x0, Real y0, Real x1, Real y1, Real xp, Real yp )
{
  Real s;         // parameter along line: s=0 @ (x0,y0); s=1 @ (x1,y1)
  Real dx, dy;
  Real xl, yl;    // perpendicular intersection point
  Real dist;

  dx = x1 - x0;
  dy = y1 - y0;

  s = (dx*(xp - x0) + dy*(yp - y0)) / (dx*dx + dy*dy);
  xl = x0 + s*dx;
  yl = y0 + s*dy;

  dx   = xp - xl;
  dy   = yp - yl;
  dist = sqrt(dx*dx + dy*dy);

  return dist;
}


void
DiscretizationDGBR2::init()
{

  // dump viscous-eta parameter/multiplier

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    if (verbose_)
      out_ << "viscous-eta parameter from element topology; multiplier = " << viscousEtaParameter_ << std::endl;
    break;

  // element geometry based
  case -2:
    if (verbose_)
      out_ << "viscous-eta parameter from element geometry; multiplier = " << viscousEtaParameter_ << std::endl;
    break;

  // use input viscous-eta parameter
  default:
    if (verbose_)
      out_ << "viscous-eta parameter from input = " << viscousEtaParameter_ << std::endl;
    break;
  }
}


template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD1, TopoD0, Node>& xedge,
    const ElementXField<PhysD1, TopoD1, Line>& xelemL,
    const ElementXField<PhysD1, TopoD1, Line>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 2;
    eta *= viscousEtaParameter_;
    break;

  //TODO: element geometry based - INCOMPLETE - just setting to 2 for now.
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based eta not implemented");
    eta = 2;
    eta *= viscousEtaParameter_;
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD2, TopoD0, Node>& xedge,
    const ElementXField<PhysD2, TopoD1, Line>& xelemL,
    const ElementXField<PhysD2, TopoD1, Line>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 2;
    eta *= viscousEtaParameter_;
    break;

  //TODO: element geometry based - INCOMPLETE - just setting to 2 for now.
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based eta not implemented");
    eta = 2;
    eta *= viscousEtaParameter_;
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD2, TopoD1, Line>& xedge,
    const ElementXField<PhysD2, TopoD2, Triangle>& xelemL,
    const ElementXField<PhysD2, TopoD2, Triangle>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 3;
    eta *= viscousEtaParameter_;
    break;

  // element geometry based
  case -2:
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD2, TopoD1, Line>& xedge,
    const ElementXField<PhysD2, TopoD2, Quad>& xelemL,
    const ElementXField<PhysD2, TopoD2, Triangle>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 4;
    eta *= viscousEtaParameter_;
    break;
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based eta not implemented");
    break;
#if 0
  // element geometry based
  case -2:
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
    break;
#endif
  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD2, TopoD1, Line>& xedge,
    const ElementXField<PhysD2, TopoD2, Triangle>& xelemL,
    const ElementXField<PhysD2, TopoD2, Quad>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 4;
    eta *= viscousEtaParameter_;
    break;
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based eta not implemented");
    break;
#if 0
  // element geometry based
  case -2:
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
    break;
#endif
  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD2, TopoD1, Line>& xedge,
    const ElementXField<PhysD2, TopoD2, Quad>& xelemL,
    const ElementXField<PhysD2, TopoD2, Quad>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 4;
    eta *= viscousEtaParameter_;
    break;
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based eta not implemented");
    break;
#if 0
  // element geometry based
  case -2:
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
    break;
#endif
  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD3, TopoD1, Line>& xedge,
    const ElementXField<PhysD3, TopoD2, Triangle>& xelemL,
    const ElementXField<PhysD3, TopoD2, Triangle>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 3;
    eta *= viscousEtaParameter_;
    break;

  //TODO: element geometry based
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based viscousEta not implemented for PhysD3 TopoD2");
#if 0
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
#endif
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD3, TopoD1, Line>& xedge,
    const ElementXField<PhysD3, TopoD2, Quad>& xelemL,
    const ElementXField<PhysD3, TopoD2, Triangle>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 4;
    eta *= viscousEtaParameter_;
    break;

  //TODO: element geometry based
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based viscousEta not implemented for PhysD3 TopoD2");
#if 0
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
#endif
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD3, TopoD1, Line>& xedge,
    const ElementXField<PhysD3, TopoD2, Triangle>& xelemL,
    const ElementXField<PhysD3, TopoD2, Quad>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 4;
    eta *= viscousEtaParameter_;
    break;

  //TODO: element geometry based
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based viscousEta not implemented for PhysD3 TopoD2");
#if 0
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
#endif
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD3, TopoD1, Line>& xedge,
    const ElementXField<PhysD3, TopoD2, Quad>& xelemL,
    const ElementXField<PhysD3, TopoD2, Quad>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 4;
    eta *= viscousEtaParameter_;
    break;

  //TODO: element geometry based
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based viscousEta not implemented for PhysD3 TopoD2");
#if 0
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
#endif
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD3, TopoD2, Triangle>& xedge,
    const ElementXField<PhysD3, TopoD3, Tet>& xelemL,
    const ElementXField<PhysD3, TopoD3, Tet>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 4;
    eta *= viscousEtaParameter_;
    break;

  //TODO: element geometry based
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based viscousEta not implemented for PhysD3 TopoD3");
#if 0
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
#endif
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD3, TopoD2, Quad>& xedge,
    const ElementXField<PhysD3, TopoD3, Hex>& xelemL,
    const ElementXField<PhysD3, TopoD3, Hex>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = 6;
    eta *= viscousEtaParameter_;
    break;

  //TODO: element geometry based
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based viscousEta not implemented for PhysD3 TopoD3");
#if 0
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
#endif
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

#if 0
template <class TopologyL, class TopologyR>
Real
DiscretizationDGBR2::viscousEtaParameter(
      const ElementXField2DLine& xedge,
      const ElementXField2DArea<TopologyL>& xelemL, const ElementXField2DArea<TopologyR>& xelemR,
      int edgeL, int edgeR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = max( TopologyL::NEdge, TopologyR::NEdge );
    eta *= viscousEtaParameter_;
    break;

  // element geometry based
  case -2:
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.xDOF(0);  y0 = xedge.yDOF(0);
    x1 = xedge.xDOF(1);  y1 = xedge.yDOF(1);

    xp = xelemL.xDOF(edgeL);     // NOTE: only works for triangles; what happens for quads?
    yp = xelemL.yDOF(edgeL);
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.xDOF(edgeL);
    yp = xelemR.yDOF(edgeL);
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}
#endif

template <>
Real DiscretizationDGBR2::viscousEta(
    const ElementXField<PhysD4, TopoD3, Tet>& xtrace,
    const ElementXField<PhysD4, TopoD4, Pentatope>& xelemL,
    const ElementXField<PhysD4, TopoD4, Pentatope>& xelemR,
    const int traceL, const int traceR ) const
{
  Real eta;

  switch (viscousEtaType_)
  {

  // topology based
  case -1:
    eta = Pentatope::NTrace;
    eta *= viscousEtaParameter_;
    break;

  //TODO: element geometry based
  case -2:
    SANS_DEVELOPER_EXCEPTION("Geometry based viscousEta not implemented for PhysD4 TopoD4");
#if 0
    Real x0, y0, x1, y1, xp, yp;
    Real hL, hR;

    x0 = xedge.DOF(0)[0];  y0 = xedge.DOF(0)[1];
    x1 = xedge.DOF(1)[0];  y1 = xedge.DOF(1)[1];

    xp = xelemL.DOF(traceL)[0];
    yp = xelemL.DOF(traceL)[1];
    hL = DistancePointLine( x0, y0, x1, y1, xp, yp );

    xp = xelemR.DOF(traceR)[0];
    yp = xelemR.DOF(traceR)[1];
    hR = DistancePointLine( x0, y0, x1, y1, xp, yp );
    //std::cout << "hL = " << hL << "  hR = " << hR << std::endl;
    //std::cout << "0: = " << x0 << " " << y0 << "  1: = "  << x1 << " " << y1 << "  1: = " << xp << " " << yp << std::endl;

    eta = 3./( 1 + 0.5*(hR/hL + hL/hR) );
    eta *= viscousEtaParameter_;
#endif
    break;

  // use input viscous-eta parameter
  default:
    eta = viscousEtaParameter_;
    break;
  }

  return eta;
}

}
