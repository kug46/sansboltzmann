// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_DGBR2_H
#define JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_DGBR2_H

// boundary-trace integral residual functions


//#include "Discretization/DG/JacobianFunctionalBoundaryTrace_FieldTrace_DGBR2.h"
#include "Discretization/DG/JacobianFunctionalBoundaryTrace_DGBR2.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace (e.g. with Lagrange multipliers)
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ>
class JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl
{
public:
  JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQ>& rsdPDEGlobal,
      Vector<ArrayQ>& rsdBCGlobal )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), sfld_(sfld), lgfld_(lgfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Functionals with field trace not implemented yet.");
#if 0
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianFunctionalBoundaryTrace_FieldTrace_DGBR2(fcn, rsdPDEGlobal_, rsdBCGlobal_),
        xfld_, (qfld_, rfld_), lgfld_, quadratureorder_, ngroup_ );
#endif
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdBCGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ>
JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, VectorArrayQ>
JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_DGBR2(const XFieldType& xfld,
                                           const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                           const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                           const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld,
                                           const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                           const int* quadratureorder, int ngroup,
                                           Vector<ArrayQ>& rsdPDEGlobal,
                                           Vector<ArrayQ>& rsdBCGlobal)
{
  return JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, VectorArrayQ>(
      xfld, qfld, rfld, sfld, lgfld, quadratureorder, ngroup, rsdPDEGlobal, rsdBCGlobal);
}

//---------------------------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, template<class> class Vector, class MatrixJ>
class JacobianFunctionalBoundaryTrace_Dispatch_DGBR2_impl
{
public:
  JacobianFunctionalBoundaryTrace_Dispatch_DGBR2_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld,
      const int* quadratureorder, int ngroup,
      Vector< MatrixJ >& func_q )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), rfld_(rfld), sfld_(sfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      func_q_(func_q)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
      JacobianFunctionalBoundaryTrace_DGBR2<Surreal>( fcnOutput_, fcn.cast(), func_q_ ),
                            xfld_, (qfld_, rfld_, sfld_), quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector< MatrixJ >& func_q_;
};

// Factory function

template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_DGBR2_impl<Surreal, FunctionalIntegrand, XFieldType, PhysDim, TopoDim,
                                                    ArrayQ, VectorArrayQ, Vector, MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_DGBR2(const FunctionalIntegrand& fcnOutput,
                                               const XFieldType& xfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                               const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                               const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld,
                                               const int* quadratureorder, int ngroup,
                                               Vector< MatrixJ >& func_q )
{
  return JacobianFunctionalBoundaryTrace_Dispatch_DGBR2_impl<Surreal, FunctionalIntegrand, XFieldType, PhysDim, TopoDim,
                                                             ArrayQ, VectorArrayQ, Vector, MatrixJ>(
      fcnOutput, xfld, qfld, rfld, sfld, quadratureorder, ngroup, func_q);
}


}

#endif //JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_DGBR2_H
