// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_DGBR2_H
#define INTEGRANDCELL_DGBR2_H

// cell integrand operators: DG BR2 (Little-r formulation)

#include <vector>
#include <memory>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Field/Element/Element.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/MatrixLiftedQuantity.h"
#include "DiscretizationDGBR2.h"
#include "JacobianCell_DGBR2_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// cell element integrand: DG BR2 (Little-r formulation)
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective, viscous and BR2 lifting operator)
//   S(x,y,d,q)       solution-dependent source
//   RHS(x,y)         right-hand side forcing function

template <class PDE_>
class IntegrandCell_DGBR2 : public IntegrandCellType< IntegrandCell_DGBR2<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal; // Lifting Operator integrand

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // diffusion matrices

  template <class Z>
  using MatrixT = typename MatrixLiftedQuantity<MatrixQ<Z>>::type; //PDE jacobian wrt lifted quantity

  static const int D = PhysDim::D;          // Physical dimensions
  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_DGBR2(
    const PDE& pde,
    const DiscretizationDGBR2& disc, const std::vector<int>& CellGroups ) :
    pde_(pde), disc_(disc), cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  // used to know if the source needs the gradient (i.e. lifting operator)
  bool needsSolutionGradientforSource() const
  {
    return pde_.needsSolutionGradientforSource();
  }

  template<class T, class Tr, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted_PDE
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<VectorArrayQ<Tr>, TopoDim, Topology> ElementRFieldType;
    typedef Element<T, TopoDim, Topology> ElementSFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef JacobianElemCell_DGBR2<PhysDim,MatrixQ<Real>> JacobianElemCellType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;

    BasisWeighted_PDE( const PDE& pde,
                       const DiscretizationDGBR2& disc,
                       const ElementParam& paramfldElem,
                       const ElementQFieldType& qfldElem,
                       const ElementRFieldType& RfldElem,
                       const bool liftedOnly ) :
                         pde_(pde), disc_(disc),
                         xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                         qfldElem_(qfldElem), RfldElem_(RfldElem),
                         paramfldElem_(paramfldElem),
                         qDOF_( qfldElem_.nDOF() ),
                         sDOF_(0),
                         phi_(qDOF_),
                         gradphi_(qDOF_),
                         phis_(sDOF_),
                         liftedOnly_(liftedOnly)
    {
      SANS_ASSERT( qfldElem_.basis() == RfldElem_.basis() );
    }

    //With lifted-quantity element
    BasisWeighted_PDE( const PDE& pde,
                       const DiscretizationDGBR2& disc,
                       const ElementParam& paramfldElem,
                       const ElementQFieldType& qfldElem,
                       const ElementRFieldType& RfldElem,
                       const std::shared_ptr<ElementSFieldType> psfldElem,
                       const bool liftedOnly ) :
                         pde_(pde), disc_(disc),
                         xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                         qfldElem_(qfldElem), RfldElem_(RfldElem),
                         psfldElem_(psfldElem),
                         paramfldElem_(paramfldElem),
                         qDOF_( qfldElem_.nDOF() ),
                         sDOF_( psfldElem_->nDOF() ),
                         phi_(qDOF_),
                         gradphi_(qDOF_),
                         phis_(sDOF_),
                         liftedOnly_(liftedOnly)
    {
      SANS_ASSERT( qfldElem_.basis() == RfldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return ( pde_.hasFluxAdvective() ||
               pde_.hasFluxViscous() ||
               pde_.hasSource() ||
               pde_.hasForcingFunction() );
    }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qDOF_; }

    // cell element residual integrand
    template<class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem ) const;

  private:
    template<class Tq, class Tg, class Ti>
    void fluxIntegrand( const ParamT& param,
                        const ArrayQ<Tq>& q,
                        const VectorArrayQ<Tg>& gradq,
                        ArrayQ<Ti> integrandPDE[], int neqn ) const;

    template<class Tq, class Tg, class Ti>
    void sourceIntegrand( const ParamT& param,
                          const ArrayQ<Tq>& q,
                          const VectorArrayQ<Tg>& gradqlifted,
                          ArrayQ<Ti> integrandPDE[], int neqn ) const;

    //with lifted-quantity (i.e. for shock-capturing)
    template<class Tq, class Tg, class Ts, class Ti>
    void sourceIntegrand( const ParamT& param,
                          const Ts& lifted_quantity,
                          const ArrayQ<Tq>& q,
                          const VectorArrayQ<Tg>& gradqlifted,
                          ArrayQ<Ti> integrandPDE[], int neqn ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementRFieldType& RfldElem_;  // lifting operators, sum over traces
    const std::shared_ptr<ElementSFieldType> psfldElem_; //pointer to lifted quantity element (may not always exist)
    const ElementParam& paramfldElem_;

    const int qDOF_;
    mutable int sDOF_;
    mutable std::vector<Real> phi_;
    mutable std::vector<VectorX> gradphi_;
    mutable std::vector<Real> phis_;
    const bool liftedOnly_;
  };


  template<class T, class TopoDim, class Topology>
  class BasisWeighted_LO
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementLift<VectorArrayQ<T>, TopoDim, Topology> ElementRFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;

    typedef DLA::VectorS<nTrace, VectorArrayQ<T>> IntegrandType;

    BasisWeighted_LO( const PDE& pde,
                      const ElementXFieldType& xfldElem_,
                      const ElementRFieldType& rfldElems ) :
                        pde_(pde),
                        xfldElem_(xfldElem_),
                        rfldElems_(rfldElems),
                        nDOF_( rfldElems_.nDOFElem() ),
                        phi_( new Real[nDOF_] )
    {}

    ~BasisWeighted_LO()
    {
      delete [] phi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return ( pde_.hasFluxViscous() ||
               (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );
    }


    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // cell element integrand
    void operator()( const QuadPointType& s, IntegrandType integrandLO[], int neqn ) const;
  private:
    const PDE& pde_;
    const ElementXFieldType& xfldElem_;
    const ElementRFieldType& rfldElems_;  // lifting operators, one per trace

    const int nDOF_;
    mutable Real *phi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted
  {
  public:

    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementLift<VectorArrayQ<T>, TopoDim, Topology> ElementRFieldType;
    typedef Element<T, TopoDim, Topology> ElementSFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef IntegrandDGBR2<Real, Real, Topology> IntegrandType;

    static const int nTrace = Topology::NTrace;

    FieldWeighted(  const PDE& pde,
                    const DiscretizationDGBR2& disc,
                    const ElementParam& paramfldElem,
                    const ElementQFieldType& qfldElem,
                    const ElementRFieldType& rfldElems,
                    const ElementQFieldType& wfldElem,
                    const ElementRFieldType& sfldElems ) :
                    pde_(pde), disc_(disc),
                    xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                    qfldElem_(qfldElem), rfldElems_(rfldElems),
                    wfldElem_(wfldElem), sfldElems_(sfldElems),
                    paramfldElem_(paramfldElem),
                    qDOF_( qfldElem_.nDOF() ),
                    sDOF_(0),
                    wDOF_( wfldElem_.nDOF() ),
                    phi_(qDOF_),
                    wphi_(wDOF_),
                    phis_(sDOF_)
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElems_.basis() );
      SANS_ASSERT( wfldElem_.basis() == sfldElems_.basis() );
    }

    //With lifted-quantity element
    FieldWeighted(  const PDE& pde,
                    const DiscretizationDGBR2& disc,
                    const ElementParam& paramfldElem,
                    const ElementQFieldType& qfldElem,
                    const ElementRFieldType& rfldElems,
                    const ElementQFieldType& wfldElem,
                    const ElementRFieldType& sfldElems,
                    const std::shared_ptr<ElementSFieldType> psfldElem ) :
                    pde_(pde), disc_(disc),
                    xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                    qfldElem_(qfldElem), rfldElems_(rfldElems),
                    wfldElem_(wfldElem), sfldElems_(sfldElems),
                    psfldElem_(psfldElem),
                    paramfldElem_(paramfldElem),
                    qDOF_( qfldElem_.nDOF() ),
                    sDOF_( psfldElem_->nDOF() ),
                    wDOF_( wfldElem_.nDOF() ),
                    phi_(qDOF_),
                    wphi_(wDOF_),
                    phis_(sDOF_)
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElems_.basis() );
      SANS_ASSERT( wfldElem_.basis() == sfldElems_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return ( pde_.hasFluxAdvective() ||
               pde_.hasFluxViscous() ||
               pde_.hasSource() ||
               pde_.hasForcingFunction() );
    }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qDOF_; }

    // cell element integrand
    void operator()( const QuadPointType& s, IntegrandType& integrand ) const;

  private:
    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementRFieldType& rfldElems_;  // lifting operators, one per trace
    const ElementQFieldType& wfldElem_;
    const ElementRFieldType& sfldElems_;  // lifting operators, one per trace
    const std::shared_ptr<ElementSFieldType> psfldElem_; //pointer to lifted quantity element (may not always exist)
    const ElementParam& paramfldElem_;

    const int qDOF_;
    mutable int sDOF_;
    const int wDOF_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> wphi_;
    mutable std::vector<Real> phis_;
  };


  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted_LO_Nodal
  {
  public:

    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef ElementLift<VectorArrayQ<T>, TopoDim, Topology> ElementRFieldType;
    typedef Element<T, TopoDim, Topology> ElementSFieldType;

    typedef Element<Real, TopoDim, Topology> ElementEFieldCell;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

//    typedef IntegrandDGBR2<Real, Real, Topology> IntegrandType;
    typedef Real IntegrandType;

    static const int nTrace = Topology::NTrace;

    FieldWeighted_LO_Nodal(  const PDE& pde,
                    const DiscretizationDGBR2& disc,
                    const ElementParam& paramfldElem,
                    const ElementQFieldType& qfldElem,
                    const ElementRFieldType& rfldElems,
                    const ElementQFieldType& wfldElem,
                    const ElementRFieldType& sfldElems,
                    const ElementEFieldCell& efldElem ) :
                    pde_(pde), disc_(disc),
                    xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                    qfldElem_(qfldElem), rfldElems_(rfldElems),
                    wfldElem_(wfldElem), sfldElems_(sfldElems),
                    efldElem_(efldElem),
                    paramfldElem_(paramfldElem),
                    qDOF_( qfldElem_.nDOF() ),
                    sDOF_(0),
                    wDOF_( wfldElem_.nDOF() ),
                    phi_(qDOF_),
                    wphi_(wDOF_),
                    phis_(sDOF_),
                    nPhi_(efldElem_.nDOF()),
                    ephi_( new Real[nPhi_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElems_.basis() );
      SANS_ASSERT( wfldElem_.basis() == sfldElems_.basis() );
    }

    //With lifted-quantity element
    FieldWeighted_LO_Nodal(  const PDE& pde,
                    const DiscretizationDGBR2& disc,
                    const ElementParam& paramfldElem,
                    const ElementQFieldType& qfldElem,
                    const ElementRFieldType& rfldElems,
                    const ElementQFieldType& wfldElem,
                    const ElementRFieldType& sfldElems,
                    const std::shared_ptr<ElementSFieldType> psfldElem,
                    const ElementEFieldCell& efldElem ) :
                    pde_(pde), disc_(disc),
                    xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                    qfldElem_(qfldElem), rfldElems_(rfldElems),
                    wfldElem_(wfldElem), sfldElems_(sfldElems),
                    psfldElem_(psfldElem),
                    paramfldElem_(paramfldElem),
                    efldElem_(efldElem),
                    qDOF_( qfldElem_.nDOF() ),
                    sDOF_( psfldElem_->nDOF() ),
                    wDOF_( wfldElem_.nDOF() ),
                    phi_(qDOF_),
                    wphi_(wDOF_),
                    phis_(sDOF_),
                    nPhi_(efldElem_.nDOF()),
                    ephi_( new Real[nPhi_] )
    {
      SANS_ASSERT( qfldElem_.basis() == rfldElems_.basis() );
      SANS_ASSERT( wfldElem_.basis() == sfldElems_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return ( pde_.hasFluxAdvective() ||
               pde_.hasFluxViscous() ||
               pde_.hasSource() ||
               pde_.hasForcingFunction() );
    }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qDOF_; }

    // cell element integrand
    void operator()( const QuadPointType& sRefTrace, IntegrandType integrand[], const int nphi ) const;

  private:
    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementRFieldType& rfldElems_;  // lifting operators, one per trace
    const ElementQFieldType& wfldElem_;
    const ElementRFieldType& sfldElems_;  // lifting operators, one per trace
    const ElementEFieldCell& efldElem_;
    const std::shared_ptr<ElementSFieldType> psfldElem_; //pointer to lifted quantity element (may not always exist)
    const ElementParam& paramfldElem_;

    const int qDOF_;
    mutable int sDOF_;
    const int wDOF_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> wphi_;
    mutable std::vector<Real> phis_;
    const int nPhi_;
    mutable Real *ephi_;
  };

  template<class Tq, class Tr, class TopoDim, class Topology, class ElementParam>
  BasisWeighted_PDE<Tq,Tr,TopoDim,Topology,ElementParam>
  integrand_PDE(const ElementParam& paramfldElem,
                const Element<ArrayQ<Tq>,       TopoDim, Topology>& qfldElem,
                const Element<VectorArrayQ<Tr>, TopoDim, Topology>& RfldElem,
                const bool liftedOnly = false) const
  {
    return {pde_, disc_, paramfldElem,
            qfldElem, RfldElem, liftedOnly};
  }

  template<class Tq, class Tr, class TopoDim, class Topology, class ElementParam>
  BasisWeighted_PDE<Tq,Tr,TopoDim,Topology,ElementParam>
  integrand_PDE(const ElementParam& paramfldElem,
                const Element<ArrayQ<Tq>,       TopoDim, Topology>& qfldElem,
                const Element<VectorArrayQ<Tr>, TopoDim, Topology>& RfldElem,
                const std::shared_ptr<Element<Tq, TopoDim, Topology>> psfldElem,
                const bool liftedOnly = false) const
  {
    return {pde_, disc_, paramfldElem,
            qfldElem, RfldElem, psfldElem, liftedOnly};
  }

  template<class T, class TopoDim, class Topology>
  BasisWeighted_LO<T,TopoDim,Topology>
  integrand_LO(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
               const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& rfldElems) const
  {
    return {pde_, xfldElem, rfldElems};
  }

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T,TopoDim,Topology,ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& rfldElems,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& sfldElems) const
  {
    return {pde_, disc_,
            paramfldElem,
            qfldElem, rfldElems,
            wfldElem, sfldElems};
  }

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T,TopoDim,Topology,ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& rfldElems,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& sfldElems,
            const std::shared_ptr<Element<T, TopoDim, Topology>> psfldElem) const
  {
    return {pde_, disc_,
            paramfldElem,
            qfldElem, rfldElems,
            wfldElem, sfldElems, psfldElem};
  }


  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted_LO_Nodal<T,TopoDim,Topology,ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& rfldElems,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const ElementLift<VectorArrayQ<T>, TopoDim, Topology>& sfldElems,
            const Element<Real, TopoDim, Topology>& efldElem) const
  {
    return {pde_, disc_,
            paramfldElem,
            qfldElem, rfldElems,
            wfldElem, sfldElems, efldElem};
  }

private:
  const PDE& pde_;
  const DiscretizationDGBR2& disc_;
  const std::vector<int> cellGroups_;
};

template <class PDE>
template <class Tq, class Tr, class TopoDim, class Topology, class ElementParam >
template <class Ti>
void
IntegrandCell_DGBR2<PDE>::
BasisWeighted_PDE<Tq,Tr,TopoDim,Topology,ElementParam>::
operator()( const QuadPointType& sRef, ArrayQ<Ti> integrandPDE[], int neqn ) const
{
  typedef typename promote_Surreal<Tq, Tr>::type Tg;

  SANS_ASSERT(neqn == qDOF_);

  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Tq> q;               // solution
  VectorArrayQ<Tq> gradq;     // gradient

  const bool needsSolutionGradient = pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), qDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_.data(), qDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), qDOF_, q );
  if (needsSolutionGradient)
    qfldElem_.evalFromBasis( gradphi_.data(), qDOF_, gradq );
  else
    gradq = 0;

  Tq liftedQuantity;
  if (pde_.hasSourceLiftedQuantity())
  {
    SANS_ASSERT_MSG(psfldElem_, "IntegrandCell_DGBR2::BasisWeighted_PDE - Lifted quantity field has not been initialized!");

    psfldElem_->evalBasis( sRef, phis_.data(), sDOF_ );
    psfldElem_->evalFromBasis( phis_.data(), sDOF_, liftedQuantity );
  }

  VectorArrayQ<Tg> gradqlifted;

  if (pde_.hasSource() && pde_.needsSolutionGradientforSource())
  {
    VectorArrayQ<Tr> Rlift;     // Sum of lifting operators

    // lifted gradient: gradient summed with all trace lifting operators
    RfldElem_.evalFromBasis( phi_.data(), qDOF_, Rlift );

    gradqlifted = gradq + Rlift;
  }

  // compute the residual
  fluxIntegrand(param, q, gradq, integrandPDE, neqn);

  if (pde_.hasSource())
  {
    if (pde_.hasSourceLiftedQuantity())
      sourceIntegrand(param, liftedQuantity, q, gradqlifted, integrandPDE, neqn);
    else
      sourceIntegrand(param, q, gradqlifted, integrandPDE, neqn);
  }
}

template <class PDE>
template <class Tq, class Tr, class TopoDim, class Topology, class ElementParam >
void
IntegrandCell_DGBR2<PDE>::
BasisWeighted_PDE<Tq,Tr,TopoDim,Topology,ElementParam>::
operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem ) const
{
  typedef SurrealS<PDE::N> SurrealClassQ;
  typedef SurrealS<1> SurrealClassS;

  SANS_ASSERT(mtxElem.qDOF == qDOF_);
  SANS_ASSERT(mtxElem.sDOF == sDOF_);

  // number of simultaneous derivatives per functor call
  static const int nDerivq = SurrealClassQ::N;
  static const int nDerivs = SurrealClassS::N;
  static_assert(nDerivq % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;                 // solution
  VectorArrayQ<Real> gradq;       // gradient
  VectorArrayQ<Real> gradqlifted;

  ArrayQ<SurrealClassQ> qSurreal = 0;               // solution
  VectorArrayQ<SurrealClassQ> gradqSurreal = 0;     // gradient
  VectorArrayQ<SurrealClassQ> gradqliftedSurreal = 0;

  Real liftedQuantity;
  SurrealClassS liftedQuantitySurreal = 0;

  MatrixQ<Real> PDE_q = 0;   // temporary Jacobian storage
  MatrixT<Real> PDE_s = 0; //temporary storage for PDE jacobian wrt lifted quantity

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), qDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_.data(), qDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), qDOF_, q );
  qSurreal = q;

  if (needsSolutionGradient)
  {
    qfldElem_.evalFromBasis( gradphi_.data(), qDOF_, gradq );
    gradqSurreal = gradq;
  }

  if (pde_.hasSource() && pde_.needsSolutionGradientforSource())
  {
    VectorArrayQ<Real> Rlift; // Sum of lifting operators
    // lifted gradient: gradient summed with all trace lifting operators
    RfldElem_.evalFromBasis( phi_.data(), qDOF_, Rlift );

    gradqliftedSurreal = gradqlifted = gradq + Rlift;
  }

  if (pde_.hasSourceLiftedQuantity())
  {
    SANS_ASSERT_MSG(psfldElem_, "IntegrandCell_DGBR2::BasisWeighted_PDE - Lifted quantity field has not been initialized!");

    psfldElem_->evalBasis( sRef, phis_.data(), sDOF_ );
    psfldElem_->evalFromBasis( phis_.data(), sDOF_, liftedQuantity );
    liftedQuantitySurreal = liftedQuantity;
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClassQ>> integrandSurreal( qDOF_ );
  DLA::VectorD<ArrayQ<SurrealClassS>> integrandSurrealS( qDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N && !liftedOnly_; nchunk += nDerivq)
  {
    // associate derivative slots with solution variables
    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDerivq))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    fluxIntegrand(param, qSurreal, gradq, integrandSurreal.data(), integrandSurreal.size());

    if (pde_.hasSource())
    {
      if (pde_.hasSourceLiftedQuantity())
        sourceIntegrand(param, liftedQuantity, qSurreal, gradqlifted, integrandSurreal.data(), integrandSurreal.size());
      else
        sourceIntegrand(param, qSurreal, gradqlifted, integrandSurreal.data(), integrandSurreal.size());
    }

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDerivq))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < qDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < qDOF_; j++)
          mtxElem.PDE_q(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;
  } // nchunk


  if (needsSolutionGradient)
  {
    DLA::VectorD<ArrayQ<SurrealClassQ>> sourceSurreal( qDOF_ );

    MatrixQ<Real> S_gradq = 0;
    MatrixQ<Real> PDE_gradq = 0;

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDerivq)
    {
      // associate derivative slots with solution variables
      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDerivq))
          for (int n = 0; n < PDE::N; n++)
          {
            DLA::index(gradqSurreal[d],       n).deriv(slot + n - nchunk) = 1;
            DLA::index(gradqliftedSurreal[d], n).deriv(slot + n - nchunk) = 1;
          }
        slot += PDE::N;
      }

      integrandSurreal = 0;
      sourceSurreal = 0;

      if (!liftedOnly_)
        fluxIntegrand(param, q, gradqSurreal, integrandSurreal.data(), integrandSurreal.size());

      if (pde_.hasSource() && pde_.needsSolutionGradientforSource())
      {
        if (pde_.hasSourceLiftedQuantity())
          sourceIntegrand(param, liftedQuantity, q, gradqliftedSurreal, sourceSurreal.data(), sourceSurreal.size());
        else
          sourceIntegrand(param, q, gradqliftedSurreal, sourceSurreal.data(), sourceSurreal.size());
      }

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDerivq))
        {
          for (int n = 0; n < PDE::N; n++)
          {
            DLA::index(gradqSurreal[d],       n).deriv(slot + n - nchunk) = 0; // Reset the derivative
            DLA::index(gradqliftedSurreal[d], n).deriv(slot + n - nchunk) = 0;
          }

          for (int i = 0; i < qDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
              {
                DLA::index(S_gradq,m,n)   = DLA::index(sourceSurreal[i], m).deriv(slot + n - nchunk);
                DLA::index(PDE_gradq,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk)
                                          + DLA::index(S_gradq,m,n);
              }

            for (int j = 0; j < qDOF_; j++)
            {
              mtxElem.PDE_q(i,j)      += dJ*gradphi_[j][d]*PDE_gradq;
              mtxElem.PDE_R(i,j)(0,d) += dJ*phi_[j]*S_gradq;
            }
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  }

  //Compute derivative wrt lifted quantity
  if (pde_.hasSource() && pde_.hasSourceLiftedQuantity())
  {
    // loop over derivative chunks
    for (int nchunk = 0; nchunk < 1 && !liftedOnly_; nchunk += nDerivs)
    {
      // associate derivative slots with solution variables
      int slot = 0;
      if ((slot >= nchunk) && (slot < nchunk + nDerivs))
        for (int n = 0; n < 1; n++)
          DLA::index(liftedQuantitySurreal, n).deriv(slot + n - nchunk) = 1;
      slot += 1;

      integrandSurrealS = 0;

      sourceIntegrand(param, liftedQuantitySurreal, q, gradqlifted, integrandSurrealS.data(), integrandSurrealS.size());

      // accumulate derivatives into element jacobian
      slot = 0;
      if ((slot >= nchunk) && (slot < nchunk + nDerivs))
      {
        for (int n = 0; n < 1; n++)
          DLA::index(liftedQuantitySurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < qDOF_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < 1; n++)
              DLA::index(PDE_s,m,n) = DLA::index(integrandSurrealS[i], m).deriv(slot + n - nchunk);

          for (int j = 0; j < sDOF_; j++)
            mtxElem.PDE_s(i,j) += dJ*phis_[j]*PDE_s;
        }
      }
      slot += 1;
    } // nchunk
  }
}

template <class PDE>
template <class T, class Tr, class TopoDim, class Topology, class ElementParam >
template <class Tq, class Tg, class Ti>
void
IntegrandCell_DGBR2<PDE>::
BasisWeighted_PDE<T,Tr,TopoDim,Topology,ElementParam>::
fluxIntegrand( const ParamT& param,
               const ArrayQ<Tq>& q,
               const VectorArrayQ<Tg>& gradq,
               ArrayQ<Ti> integrandPDE[], int neqn ) const
{

  for (int k = 0; k < neqn; k++)
    integrandPDE[k] = 0;

  // PDE integrand
  // flux term: -gradient(phi) . F
  VectorArrayQ<Ti> F=0;         // PDE flux F(X, U), Fv(X, U, UX)

  // advective flux
  if (pde_.hasFluxAdvective())
    pde_.fluxAdvective( param, q, F );

  // viscous flux
  if (pde_.hasFluxViscous())
    pde_.fluxViscous( param, q, gradq, F );

  for (int k = 0; k < neqn; k++)
    integrandPDE[k] -= dot(gradphi_[k],F);

  // right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < neqn; k++)
      integrandPDE[k] -= phi_[k]*forcing;
  }
}

template <class PDE>
template <class T0, class T1, class TopoDim, class Topology, class ElementParam >
template <class Tq, class Tg, class Ti>
void
IntegrandCell_DGBR2<PDE>::
BasisWeighted_PDE<T0,T1,TopoDim,Topology,ElementParam>::
sourceIntegrand( const ParamT& param,
                 const ArrayQ<Tq>& q,
                 const VectorArrayQ<Tg>& gradqlifted,
                 ArrayQ<Ti> integrandPDE[], int neqn ) const
{
  // source term: +phi S
  // solution gradient augmented by lifting operator (asymptotically dual consistent)

  ArrayQ<Ti> source = 0;
  pde_.source( param, q, gradqlifted, source );

  for (int k = 0; k < neqn; k++)
    integrandPDE[k] += phi_[k]*source;
}

template <class PDE>
template <class T0, class T1, class TopoDim, class Topology, class ElementParam >
template <class Tq, class Tg, class Ts, class Ti>
void
IntegrandCell_DGBR2<PDE>::
BasisWeighted_PDE<T0,T1,TopoDim,Topology,ElementParam>::
sourceIntegrand( const ParamT& param,
                 const Ts& lifted_quantity,
                 const ArrayQ<Tq>& q,
                 const VectorArrayQ<Tg>& gradqlifted,
                 ArrayQ<Ti> integrandPDE[], int neqn ) const
{
  // source term: +phi S
  // solution gradient augmented by lifting operator (asymptotically dual consistent)

  ArrayQ<Ti> source = 0;
  pde_.source( param, lifted_quantity, q, gradqlifted, source );

  for (int k = 0; k < neqn; k++)
    integrandPDE[k] += phi_[k]*source;
}

template <class PDE>
template <class T, class TopoDim, class Topology>
void
IntegrandCell_DGBR2<PDE>::BasisWeighted_LO<T,TopoDim,Topology>::operator()(
    const QuadPointType& sRef, IntegrandType integrandLO[], int neqn ) const
{
  SANS_ASSERT(neqn == nDOF_);

  VectorArrayQ<T> rlift;     // Lifting operators

  // basis value, gradient
  rfldElems_.evalBasis( sRef, phi_, nDOF_ );

  // Lifting Operator Integrand
  for (int trace = 0; trace < nTrace; trace++)
  {
    // lifting operators
    rfldElems_[trace].evalFromBasis( phi_, nDOF_, rlift );

    // volume term: phi*rlift
    for (int k = 0; k < neqn; k++)
      integrandLO[k][trace] = phi_[k]*rlift;
  }
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_DGBR2<PDE>::FieldWeighted<T,TopoDim,Topology,ElementParam>::operator()(
    const QuadPointType& sRef, IntegrandType& integrand ) const
{
  ParamT param;                   // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                    // solution
  VectorArrayQ<T> gradq;          // gradient
  VectorArrayQ<T> rlift;          // Lifting operators
  VectorArrayQ<T> Rlift;          // Big-R summed lifting operator

  ArrayQ<T> w;                    // weight
  VectorArrayQ<T> gradw;            // gradient of weight

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), qDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), qDOF_, q );
  if (needsSolutionGradient)
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );

  // basis value, gradient
  wfldElem_.evalBasis( sRef, wphi_.data(), wDOF_ );

  // weight value, gradient
  wfldElem_.evalFromBasis( wphi_.data(), wDOF_, w );
  if (pde_.hasFluxViscous() || pde_.hasFluxAdvective())
    xfldElem_.evalGradient( sRef, wfldElem_, gradw );
  else
    gradw=0;

  T liftedQuantity;
  if (pde_.hasSourceLiftedQuantity())
  {
    SANS_ASSERT_MSG(psfldElem_, "IntegrandCell_DGBR2::FieldWeighted - Lifted quantity field has not been initialized!");

    psfldElem_->evalBasis( sRef, phis_.data(), sDOF_ );
    psfldElem_->evalFromBasis( phis_.data(), sDOF_, liftedQuantity );
  }

  // zero out the integrand
  integrand = 0;

  // lifting operator: sum of edge lifting operators
  if (pde_.hasFluxViscous() || pde_.hasSource() )
  {
    VectorArrayQ<T> tmp1,tmp2;

    Rlift = 0;
    for (int trace = 0; trace < nTrace; trace++)
    {
      // Lifting Operator
      // eval lifting operators from basis
      rfldElems_[trace].evalFromBasis(  phi_.data(), qDOF_, tmp1 );
      sfldElems_[trace].evalFromBasis( wphi_.data(), wDOF_, tmp2 );

      // set integrand for trace
      integrand.Lift[trace] = dot(tmp1,tmp2); // dotting up the vector and system of eq

      Rlift += tmp1;
    }
  }

  // PDE
  // flux term: -gradient(w) . F
  VectorArrayQ<T> F = 0;

  // advective flux
  if (pde_.hasFluxAdvective())
    pde_.fluxAdvective( param, q, F );

  // viscous flux
  if (pde_.hasFluxViscous())
    pde_.fluxViscous( param, q, gradq, F );

  for (int d = 0; d < PhysDim::D; d++)
    integrand.PDE -= dot(gradw[d], F[d]);

  // source term: +w S
  // solution gradient augmented by lifting operator (asymptotically dual consistent)

  if (pde_.hasSource())
  {
    VectorArrayQ<T> gradqlifted = 0;

    if (pde_.needsSolutionGradientforSource())
      gradqlifted = gradq + Rlift;

    ArrayQ<T> source = 0;
    if (pde_.hasSourceLiftedQuantity())
      pde_.source( param, liftedQuantity, q, gradqlifted, source );
    else
      pde_.source( param, q, gradqlifted, source );

    integrand.PDE += dot(w,source);
  }

  // right-hand-side forcing function: -w RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    integrand.PDE -= dot(w,forcing);
  }

}


template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_DGBR2<PDE>::FieldWeighted_LO_Nodal<T,TopoDim,Topology,ElementParam>::operator()(
    const QuadPointType& sRef, IntegrandType integrand[], const int nphi ) const
{
  ParamT param;                   // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                    // solution
  VectorArrayQ<T> gradq;          // gradient
  VectorArrayQ<T> rlift;          // Lifting operators
  VectorArrayQ<T> Rlift;          // Big-R summed lifting operator

  ArrayQ<T> w;                    // weight
  VectorArrayQ<T> gradw;            // gradient of weight

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), qDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), qDOF_, q );
  if (needsSolutionGradient)
    xfldElem_.evalGradient( sRef, qfldElem_, gradq );

  // basis value, gradient
  wfldElem_.evalBasis( sRef, wphi_.data(), wDOF_ );

  // weight value, gradient
  wfldElem_.evalFromBasis( wphi_.data(), wDOF_, w );
  if (pde_.hasFluxViscous() || pde_.hasFluxAdvective())
    xfldElem_.evalGradient( sRef, wfldElem_, gradw );
  else
    gradw=0;

  T liftedQuantity;
  if (pde_.hasSourceLiftedQuantity())
  {
    SANS_ASSERT_MSG(psfldElem_, "IntegrandCell_DGBR2::FieldWeighted - Lifted quantity field has not been initialized!");

    psfldElem_->evalBasis( sRef, phis_.data(), sDOF_ );
    psfldElem_->evalFromBasis( phis_.data(), sDOF_, liftedQuantity );
  }


  // estimate basis
  efldElem_.evalBasis( sRef, ephi_, nPhi_ );

  // zero out the integrand
  for (int k=0; k<nPhi_; k++)
    integrand[k] = 0;

  // lifting operator: sum of edge lifting operators
  if (pde_.hasFluxViscous() || pde_.hasSource() )
  {
    VectorArrayQ<T> tmp1,tmp2;

    Rlift = 0;
    for (int trace = 0; trace < nTrace; trace++)
    {
      // Lifting Operator
      // eval lifting operators from basis
      rfldElems_[trace].evalFromBasis(  phi_.data(), qDOF_, tmp1 );
      sfldElems_[trace].evalFromBasis( wphi_.data(), wDOF_, tmp2 );

      // set integrand for trace
//      integrand.Lift[trace] = dot(tmp1,tmp2); // dotting up the vector and system of eq

//      Rlift += tmp1;

      for (int k=0; k<nPhi_; k++)
        integrand[k] += dot(tmp1, tmp2)*ephi_[k];
    }
  }
//
//  // PDE
//  // flux term: -gradient(w) . F
//  VectorArrayQ<T> F = 0;
//
//  // advective flux
//  if (pde_.hasFluxAdvective())
//    pde_.fluxAdvective( param, q, F );
//
//  // viscous flux
//  if (pde_.hasFluxViscous())
//    pde_.fluxViscous( param, q, gradq, F );
//
//  for (int d = 0; d < PhysDim::D; d++)
//    integrand.PDE -= dot(gradw[d], F[d]);
//
//  // source term: +w S
//  // solution gradient augmented by lifting operator (asymptotically dual consistent)
//
//  if (pde_.hasSource())
//  {
//    VectorArrayQ<T> gradqlifted = 0;
//
//    if (pde_.needsSolutionGradientforSource())
//      gradqlifted = gradq + Rlift;
//
//    ArrayQ<T> source = 0;
//    if (pde_.hasSourceLiftedQuantity())
//      pde_.source( param, liftedQuantity, q, gradqlifted, source );
//    else
//      pde_.source( param, q, gradqlifted, source );
//
//    integrand.PDE += dot(w,source);
//  }
//
//  // right-hand-side forcing function: -w RHS
//
//  if (pde_.hasForcingFunction())
//  {
//    ArrayQ<Real> forcing = 0;
//    pde_.forcingFunction( param, forcing );
//
//    integrand.PDE -= dot(w,forcing);
//  }

}

} //namespace SANS

#endif  // INTEGRANDCELL_DGBR2_H
