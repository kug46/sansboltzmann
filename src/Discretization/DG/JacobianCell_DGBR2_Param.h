// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_DGBR2_PARAM_H
#define JACOBIANCELL_DGBR2_PARAM_H

// DG BR2 cell integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Tuple/SurrealizedElementTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG BR2 cell integral residual
//

template<class Surreal, int iParam, class IntegrandCell, class MatrixQP_>
class JacobianCell_DGBR2_Param_impl :
    public GroupIntegralCellType< JacobianCell_DGBR2_Param_impl<Surreal, iParam, IntegrandCell, MatrixQP_> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::PDE PDE;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename IntegrandCell::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianCell_DGBR2_Param_impl(const IntegrandCell& fcn,
                                MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p ) :
    fcn_(fcn),
    mtxGlobalPDE_p_(mtxGlobalPDE_p)
  {
    // Nothing
  }

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
//    const Field<PhysDim, TopoDim, ArrayQ>&     qfld = get<0>(flds);

//    SANS_ASSERT( mtxGlobalPDE_p_.m() == qfld.nDOF() );
//    SANS_ASSERT( mtxGlobalPDE_p_.n() == qfld.nDOF() );
  }


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class TupleFieldType>
  void
  integrate(
      const int cellGroupGlobal,
      const typename TupleFieldType::template FieldCellGroupType<Topology>& tuplefldCell,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                     template FieldCellGroupType<Topology>& fldsCell,
      const int quadratureorder )
  {
    typedef typename TupleFieldType                            ::template FieldCellGroupType<Topology> TupleFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ          >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

    typedef typename TupleType<iParam, TupleFieldCellGroupType>::type ParamCellGroupType;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupType, Surreal, iParam>::type ElementTupleFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename ParamCellGroupType::template ElementType<Surreal> ElementParamFieldClass;
    typedef ElementLift<VectorArrayQ, TopoDim, Topology> ElementrFieldClass;
    typedef Element<VectorArrayQ, TopoDim, Topology> ElementRFieldClass;

    typedef typename ElementParamFieldClass::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP;  // jacobian type associated with each DOF

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass; // jacobian type associated with each element

    // field cell groups
    const ParamCellGroupType& paramfldCell = get<iParam>(tuplefldCell);

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);

    // element field variables
    ElementTupleFieldClass tuplefldElem( tuplefldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementrFieldClass rfldElems( rfldCell.basis() );
    ElementRFieldClass RfldElem( rfldCell.basis() );
    ElementParamFieldClass& paramfldElem = set<iParam>(tuplefldElem);

    // DOFs per element
    const int nDOF = qfldElem.nDOF();
    const int paramDOF = paramfldElem.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapQDOFGlobal(nDOF, -1);
    std::vector<int> mapParamDOFGlobal(paramDOF, -1);

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, ArrayQSurreal > integral(quadratureorder, nDOF);

    // element integrand/residual
    std::vector< ArrayQSurreal > rsdPDEElem( nDOF );

    // element jacobian matrices
    MatrixElemClass mtxElemPDE_q(nDOF, paramDOF);

    MatrixRElemClass mtxElemPDE_r(nDOF, nDOF);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = tuplefldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobian
      mtxElemPDE_q = 0;

      // copy global grid/solution DOFs to element
      tuplefldCell.getElement( tuplefldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      rfldCell.getElement( rfldElems, elem );

      RfldElem.vectorViewDOF() = 0;
      for (int trace = 0; trace < Topology::NTrace; trace++)
        RfldElem.vectorViewDOF() += rfldElems[trace].vectorViewDOF();

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nArrayP*paramDOF; nchunk += nDeriv)
      {

        // associate derivative slots with solution

        int slot = 0, slotOffset = 0;
        for (int j = 0; j < paramDOF; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElem.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += paramDOF*nArrayP;

        for (int n = 0; n < nDOF; n++)
          rsdPDEElem[n] = 0;

        // cell integration for canonical element
        integral( fcn_.integrand_PDE(tuplefldElem, qfldElem, RfldElem),
                  get<-1>(tuplefldElem), rsdPDEElem.data(), nDOF );

        // accumulate derivatives into element jacobians
        slotOffset = 0;
        for (int j = 0; j < paramDOF; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElem.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              for (int i = 0; i < nDOF; i++)
              {
                for (int m = 0; m < nArrayQ; m++)
                {
                  DLA::index(mtxElemPDE_q(i,j),m,n) = DLA::index(rsdPDEElem[i],m).deriv(slot - nchunk);
                }
              }
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global
      scatterAdd(
          qfldCell, paramfldCell, elem, nDOF, paramDOF,
          mapQDOFGlobal.data(),
          mapParamDOFGlobal.data(),
          mtxElemPDE_q,
          mtxGlobalPDE_p_ );
    } // elem

  }


//----------------------------------------------------------------------------//
  template <class QFieldCellGroupType,
            class ParamFieldCellGroupType, class MatrixQP,
            class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfld,
      const ParamFieldCellGroupType& paramfld,
      const int elem, const int nDOF, const int paramDOF,
      int mapQDOFGlobal_q[],
      int mapParamDOFGlobal[],
      DLA::MatrixD<MatrixQP>& mtxElemPDE_q,
      SparseMatrixType& mtxGlobalPDE_p )
  {
    qfld.associativity( elem ).getGlobalMapping( mapQDOFGlobal_q, nDOF );

    paramfld.associativity( elem ).getGlobalMapping( mapParamDOFGlobal, paramDOF );

    mtxGlobalPDE_p.scatterAdd( mtxElemPDE_q, mapQDOFGlobal_q, nDOF, mapParamDOFGlobal, paramDOF );
  }

protected:
  const IntegrandCell& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
};


// Factory function

template<class Surreal, int iParam, class IntegrandCell, class MatrixQP_>
JacobianCell_DGBR2_Param_impl<Surreal, iParam, IntegrandCell, MatrixQP_>
JacobianCell_DGBR2_Param( const IntegrandCellType<IntegrandCell>& fcnPDE,
                          MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p )
{
  return { fcnPDE.cast(), mtxGlobalPDE_p };
}

} //namespace SANS

#endif  // JACOBIANCELL_DGBR2_PARAM_H
