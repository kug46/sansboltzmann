// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DGBR2_PARAM_H
#define JACOBIANBOUNDARYTRACE_DGBR2_PARAM_H

// jacobian boundary-trace integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Tuple/SurrealizedElementTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  DGBR2 boundary-trace integral without Lagrange multipliers
//

template<class Surreal, int iParam, class IntegrandBoundaryTrace, class MatrixQP_>
class JacobianBoundaryTrace_DGBR2_Param_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_DGBR2_Param_impl<Surreal, iParam, IntegrandBoundaryTrace, MatrixQP_> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  JacobianBoundaryTrace_DGBR2_Param_impl(const IntegrandBoundaryTrace& fcn,
                                         MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p ) :
    fcn_(fcn), mtxGlobalPDE_p_(mtxGlobalPDE_p) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class TupleFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename TupleFieldType::template FieldCellGroupType<TopologyL>& tuplefldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename TupleFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename TupleFieldType                            ::template FieldCellGroupType<TopologyL> TupleFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>           ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename TupleType<iParam, TupleFieldCellGroupTypeL>::type ParamCellGroupTypeL;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeL, Surreal, iParam>::type ElementTupleFieldClassL;

    typedef typename ParamCellGroupTypeL ::template ElementType<Surreal> ElementParamFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<>        ElementRFieldClassL;

    typedef typename TupleFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef typename ElementParamFieldClassL::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP;

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass;

    // field groups
    const ParamCellGroupTypeL& paramfldCellL = get<iParam>(tuplefldCellL);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

//    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementTupleFieldClassL tuplefldElemL( tuplefldCellL.basis() );
    ElementParamFieldClassL& paramfldElemL = set<iParam>(tuplefldElemL);
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

//    // variables/equations per DOF
//    const int nEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int paramDOFL = paramfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL,-1);
    std::vector<int> mapDOFGlobal_pL(paramDOFL,-1);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal> integralPDE(quadratureorder, nDOFL);
//    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQSurreal> integralLO(quadratureorder, nDOFL);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
//    std::vector<VectorArrayQSurreal> rsdLOElemL( nDOFL );

    // element jacobians
    MatrixElemClass mtxPDEElemL_pL(nDOFL, paramDOFL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxPDEElemL_pL = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      tuplefldCellL.getElement( tuplefldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement( xfldElemTrace, elem );


      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nArrayP*paramDOFL; nchunk += nDeriv)
      {
        // associate derivative slots with solution variables

        int slot, slotOffset = 0;

        //wrt paramL
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(paramfldElemL.DOF(j), n).deriv(k) = 0;

            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nArrayP*paramDOFL;

        // line integration for canonical element

#if 0   // Lifting operators are not functions of parameters
        for (int n = 0; n < nDOFL; n++) rsdLOElemL[n] = 0;

        integralLO( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                      tuplefldElemL, qfldElemL, rfldElemL),
                    get<-1>(xfldElemTrace), rsdLOElemL.data(), nDOFL );
#endif

        for (int n = 0; n < nDOFL; n++) rsdPDEElemL[n] = 0;

        integralPDE(
            fcn_.integrand_PDE(xfldElemTrace, canonicalTraceL,
                               tuplefldElemL, qfldElemL, rfldElemL),
            get<-1>(xfldElemTrace), rsdPDEElemL.data(), nDOFL );

        // accumulate derivatives into element jacobian
        slotOffset = 0;

        // wrt paramL
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemL_pL(i,j), m,n) += DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nArrayP*paramDOFL;

      }   // nchunk

      // scatter-add element jacobian to global

      scatterAdd(
          xfldTrace, qfldCellL, paramfldCellL,
          elemL,
          mapDOFGlobal_qL.data(), nDOFL,
          mapDOFGlobal_pL.data(), paramDOFL,
          mtxPDEElemL_pL,
          mtxGlobalPDE_p_ );
    }
  }

protected:

//----------------------------------------------------------------------------//
  template <class XFieldTraceGroupType, class QFieldCellGroupTypeL, class ParamFieldCellGroupType,
            class MatrixQP, class SparseMatrixType>
  void
  scatterAdd(
      const XFieldTraceGroupType& xfldTrace,
      const QFieldCellGroupTypeL& qfldCellL,
      const ParamFieldCellGroupType& paramfldCellL,
      const int elemL,
      int mapDOFGlobal_qL[],const int nDOFL,
      int mapDOFGlobal_pL[], const int paramDOFL,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemL_pL,
      SparseMatrixType& mtxGlobalPDE_p )
  {
    // global mapping for qL
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    // global mapping for pL
    paramfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_pL, paramDOFL );

    mtxGlobalPDE_p.scatterAdd( mtxPDEElemL_pL, mapDOFGlobal_qL, nDOFL, mapDOFGlobal_pL, paramDOFL);
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
};

// Factory function

template<class Surreal, int iParam, class IntegrandBoundaryTrace, class MatrixQP>
JacobianBoundaryTrace_DGBR2_Param_impl<Surreal, iParam, IntegrandBoundaryTrace, MatrixQP>
JacobianBoundaryTrace_DGBR2_Param( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                   MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p )
{
  return { fcn.cast(), mtxGlobalPDE_p };
}


}

#endif  // JACOBIANBOUNDARYTRACE_DGBR2_PARAM_H
