// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_DISPATCH_DGBR2_H
#define RESIDUALBOUNDARYTRACE_DISPATCH_DGBR2_H

// boundary-trace integral residual functions


#include "Discretization/DG/ResidualBoundaryTrace_FieldTrace_DGBR2.h"
#include "ResidualBoundaryTrace_DGBR2.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace (e.g. with Lagrange multipliers)
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ>
class ResidualBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl
{
public:
  ResidualBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQ>& rsdPDEGlobal,
      Vector<ArrayQ>& rsdBCGlobal )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), lgfld_(lgfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        ResidualBoundaryTrace_FieldTrace_DGBR2(fcn, rsdPDEGlobal_, rsdBCGlobal_),
        xfld_, (qfld_, rfld_), lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdBCGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ>
ResidualBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, VectorArrayQ>
ResidualBoundaryTrace_FieldTrace_Dispatch_DGBR2(const XFieldType& xfld,
                                           const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                           const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                           const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                           const int* quadratureorder, int ngroup,
                                           Vector<ArrayQ>& rsdPDEGlobal,
                                           Vector<ArrayQ>& rsdBCGlobal)
{
  return ResidualBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, VectorArrayQ>(
      xfld, qfld, rfld, lgfld, quadratureorder, ngroup, rsdPDEGlobal, rsdBCGlobal);
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without field trace
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ>
class ResidualBoundaryTrace_Dispatch_DGBR2_impl
{
public:
  ResidualBoundaryTrace_Dispatch_DGBR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQ>& rsdPDEGlobal )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate( ResidualBoundaryTrace_DGBR2(fcn, rsdPDEGlobal_),
                                                      xfld_, (qfld_, rfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQ>& rsdPDEGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ>
ResidualBoundaryTrace_Dispatch_DGBR2_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, VectorArrayQ>
ResidualBoundaryTrace_Dispatch_DGBR2(const XFieldType& xfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                            const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                            const int* quadratureorder, int ngroup,
                                            Vector<ArrayQ>& rsdPDEGlobal)
{
  return ResidualBoundaryTrace_Dispatch_DGBR2_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, VectorArrayQ>(
      xfld, qfld, rfld, quadratureorder, ngroup, rsdPDEGlobal);
}


}

#endif //RESIDUALBOUNDARYTRACE_DISPATCH_DGBR2_H
