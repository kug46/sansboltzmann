// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_MITLG_DGBR2_H
#define JACOBIANBOUNDARYTRACE_MITLG_DGBR2_H

// jacobian boundary-trace integral jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  DGBR2 boundary-trace integral
//

template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_mitLG_DGBR2_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_mitLG_DGBR2_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianBoundaryTrace_mitLG_DGBR2_impl(const IntegrandBoundaryTrace& fcn,
                                         MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                         MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
                                         MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
                                         MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_lg_(mtxGlobalPDE_lg),
    mtxGlobalBC_q_ (mtxGlobalBC_q) , mtxGlobalBC_lg_ (mtxGlobalBC_lg){}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& lgfld ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&         qfld = get<0>(flds);

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_lg_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_lg_.n() == lgfld.nDOFpossessed() + lgfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalBC_q_.m() == lgfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalBC_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalBC_lg_.m() == lgfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalBC_lg_.n() == lgfld.nDOFpossessed() + lgfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
             int quadratureorder )
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented correctly yet, nor tested.");
#if 0
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<Surreal> ElementRFieldClassL;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

    typedef typename XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );
    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();

    // DOFs per edge
    int nDOFTrace = lgfldElemTrace.nDOF();

    // element-to-global DOF mapping
    int mapDOFGlobal_qL[nDOFL];
    int mapDOFGlobal_rL[nDOFL*D];
    for (int k = 0; k < nDOFL; k++)   { mapDOFGlobal_qL[k] = -1; }
    for (int k = 0; k < nDOFL*D; k++) { mapDOFGlobal_rL[k] = -1; }

    int mapDOFGlobal_lg[nDOFTrace];
    for ( int i = 0; i < nDOFTrace; i++ ) mapDOFGlobal_lg[i] = -1;

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace,
                             IntegrandDGBR2<ArrayQSurreal,VectorArrayQSurreal>, ArrayQSurreal>
                    integral(quadratureorder, nDOFL, nDOFTrace);

    // element integrand/residuals
    std::unique_ptr<IntegrandDGBR2<ArrayQSurreal,VectorArrayQSurreal>[]> rsdPDEElemL( new IntegrandDGBR2<ArrayQSurreal,VectorArrayQSurreal>[nDOFL] );
    std::unique_ptr<ArrayQSurreal[]> rsdBCTrace( new ArrayQSurreal[nDOFTrace] );

    // element jacobians
    MatrixElemClass mtxPDEElemL_qL (nDOFL, nDOFL);
    MatrixElemClass mtxPDEElemL_rL (nDOFL, D*nDOFL);
    MatrixElemClass mtxPDEElemL_lgTrace(nDOFL, nDOFTrace);

    MatrixElemClass mtxLOElemL_qL (D*nDOFL, nDOFL);
    DLA::MatrixD< DLA::MatrixS<D,D,Real> > mtxLOElemL_rL (nDOFL, nDOFL);
    MatrixElemClass mtxLOElemL_lgTrace(D*nDOFL, nDOFTrace);

    MatrixElemClass mtxBC_qL(nDOFTrace, nDOFL);
    MatrixElemClass mtxBC_rL(nDOFTrace, D*nDOFL);
    MatrixElemClass mtxBC_lgTrace(nDOFTrace, nDOFTrace);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxPDEElemL_qL = 0; mtxPDEElemL_rL = 0; mtxPDEElemL_lgTrace = 0;
      mtxLOElemL_qL  = 0; mtxLOElemL_rL  = 0; mtxLOElemL_lgTrace  = 0;
      mtxBC_qL       = 0; mtxBC_rL       = 0; mtxBC_lgTrace       = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement(  xfldElemTrace, elem );
      lgfldTrace.getElement( lgfldElemTrace, elem );

      // only need to perform the integral on processors that possess the element
      if ( qfldElemL.rank() != comm_rank_ ) continue;

      // number of simultaneous derivatives per functor call
      const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*((D+1)*nDOFL + nDOFTrace); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot, slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int k = 0; k < nDeriv; k++)
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(k) = 0;

              slot = slotOffset + nEqn*D*j + nEqn*d + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(rfldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }
        slotOffset += nEqn*D*nDOFL;

        // wrt lg
        for (int j = 0; j < nDOFTrace; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(lgfldElemTrace.DOF(j), n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(lgfldElemTrace.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element

        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        for (int n = 0; n < nDOFTrace; n++)
          rsdBCTrace[n] = 0;

        integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                                 xfldElemL, qfldElemL, rfldElemL, lgfldElemTrace),
                  get<-1>(xfldElemTrace),
                  rsdPDEElemL.get(), nDOFL,
                  rsdBCTrace.get(), nDOFTrace );

        // accumulate derivatives into element jacobian

        slotOffset = 0;
        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxPDEElemL_qL(i,j),m,n) = DLA::index(rsdPDEElemL[i].PDE, m).deriv(slot - nchunk);

                  for (int d = 0; d < D; d++)
                    DLA::index(mtxLOElemL_qL(D*i+d,j),m,n) = DLA::index(rsdPDEElemL[i].Lift[d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFTrace; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxBC_qL(i,j),m,n) = DLA::index(rsdBCTrace[i], m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nEqn*nDOFL;


        // wrt rL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d1 = 0; d1 < D; d1++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*D*j + nEqn*d1 + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxPDEElemL_rL(i,D*j+d1),m,n) = DLA::index(rsdPDEElemL[i].PDE,m).deriv(slot - nchunk);

                    for (int d0 = 0; d0 < D; d0++)
                      DLA::index(mtxLOElemL_rL(D*i+d0,D*j+d1),m,n) = DLA::index(rsdPDEElemL[i].Lift[d0],m).deriv(slot - nchunk);
                  }

                for (int i = 0; i < nDOFTrace; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxBC_rL(i,D*j+d1),m,n) = DLA::index(rsdBCTrace[i], m).deriv(slot - nchunk);
              }
            } //n loop
          } //d1 loop
        } //j loop
        slotOffset += nEqn*D*nDOFL;

        // wrt lg
        for (int j = 0; j < nDOFTrace; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxPDEElemL_lgTrace(i,j), m,n) = DLA::index(rsdPDEElemL[i].PDE, m).deriv(slot - nchunk);

                  for (int d = 0; d < D; d++)
                    DLA::index(mtxLOElemL_lgTrace(D*i+d,j),m,n) = DLA::index(rsdPDEElemL[i].Lift[d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFTrace; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxBC_lgTrace(i,j), m,n) = DLA::index(rsdBCTrace[i], m).deriv(slot - nchunk);
            }
          }
        }

      }   // nchunk

      // scatter-add element jacobian to global

      scatterAdd(
          xfldTrace, qfldCellL, rfldCellL, lgfldTrace,
          elem, elemL,
          mapDOFGlobal_qL, mapDOFGlobal_rL, nDOFL,
          mapDOFGlobal_lg, nDOFTrace,
          mtxPDEElemL_qL, mtxPDEElemL_rL, mtxPDEElemL_lgTrace,
          mtxLOElemL_qL , mtxLOElemL_rL , mtxLOElemL_lgTrace,
          mtxBC_qL      , mtxBC_rL      , mtxBC_lgTrace,
          mtxGlobalPDE_q_, mtxGlobalPDE_r_, mtxGlobalPDE_lg_,
          mtxGlobalLO_q_ , mtxGlobalLO_r_ , mtxGlobalLO_lg_ ,
          mtxGlobalBC_q_ , mtxGlobalBC_r_ , mtxGlobalBC_lg_ );
    }
#endif
  }

protected:

//----------------------------------------------------------------------------//
  template <class XFieldTraceGroupType, class QFieldCellGroupTypeL, class RFieldCellGroupTypeL,
            class QFieldTraceGroupType, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const XFieldTraceGroupType& xfldTrace,
      const QFieldCellGroupTypeL& qfldCellL,
      const RFieldCellGroupTypeL& rfldCellL,
      const QFieldTraceGroupType& lgfldTrace,
      const int elem, const int elemL,
      int mapDOFGlobal_qL[], int mapDOFGlobal_rL[], const int nDOFL,
      int mapDOFGlobal_lg[], const int nDOFTrace,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_lgTrace,
      SANS::DLA::MatrixD<MatrixQ>& mtxBC_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxBC_lgTrace,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_lg,
      SparseMatrixType<MatrixQ>& mtxGlobalBC_q,
      SparseMatrixType<MatrixQ>& mtxGlobalBC_lg )
  {
#if 0
    const CanonicalTraceToCell canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    rfldCellL.associativity( elemL, canonicalTraceL.trace ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    // global mapping for rL
    for (int n = 0; n < nDOFL; n++)
      for (int d = 0; d < PhysDim::D; d++)
        mapDOFGlobal_rL[PhysDim::D*n+d] = PhysDim::D*mapDOFGlobal_qL[n] + d;

    // global mapping for qL
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    // global mapping for lg
    lgfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_lg, nDOFTrace );

    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qL, mapDOFGlobal_qL, nDOFL);
    mtxGlobalPDE_lg.scatterAdd( mtxPDEElemL_lgTrace, mapDOFGlobal_qL, nDOFL, mapDOFGlobal_lg, nDOFTrace );

    mtxGlobalBC_q.scatterAdd( mtxBC_qL, mapDOFGlobal_lg, nDOFTrace, mapDOFGlobal_qL, nDOFL );
    mtxGlobalBC_lg.scatterAdd( mtxBC_lgTrace, mapDOFGlobal_lg, nDOFTrace );
#endif
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class MatrixQ>
JacobianBoundaryTrace_mitLG_DGBR2_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_mitLG_DGBR2( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                   MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                   MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
                                   MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
                                   MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg )
{
  return { fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_lg,
                       mtxGlobalBC_q , mtxGlobalBC_lg  };
}


}

#endif  // JACOBIANBOUNDARYTRACE_MITLG_DGBR2_H
