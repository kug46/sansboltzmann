// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATEBOUNDARYTRACE_DISPATCH_DGBR2_H
#define INTEGRATEBOUNDARYTRACE_DISPATCH_DGBR2_H

#include "Python/PyDict.h"

#include <map>
#include <stdexcept>
#include <vector>
#include <string>

#include <boost/mpl/partition.hpp>
#include <boost/mpl/back_inserter.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/transform.hpp>

#include "tools/add_decorator.h"

#include "Discretization/IntegrateBoundaryTrace_Dispatch.h"
#include "DiscretizationDGBR2.h"
#include "Integrand_DGBR2_fwd.h"

namespace SANS
{

template<class DiscTag, class BC>
struct is_DGBR2 :
  public boost::mpl::bool_< std::is_same< typename DiscBCTag<typename BC::Category, DiscTag >::type, DGBR2         >::value ||
                            std::is_same< typename DiscBCTag<typename BC::Category, DiscTag >::type, DGBR2_manifold>::value    > {};

//===========================================================================//
template<class PDEND, template<class,class> class BCNDConvert, class BCVector, class DiscTag>
struct IntegrateBoundaryTrace_Dispatch_DGBR2 : public IntegrateBoundaryTrace_Dispatch<PDEND, BCNDConvert, BCVector, DiscTag>
{
  typedef IntegrateBoundaryTrace_Dispatch<PDEND, BCNDConvert, BCVector, DiscTag> BaseType;

  typedef typename BaseType::BCNDVectorsmitFTcat BCNDVectorsmitFTcat;
  typedef typename BaseType::BCNDVectorsmitHTcat BCNDVectorsmitHTcat;

  typedef typename BaseType::def_IntegrandBoundaryTraceOp def_IntegrandBoundaryTraceOp;
  typedef typename BaseType::IntegrandBoundaryTracemitFTVector IntegrandBoundaryTracemitFTVector;

  // First separate the BCVectorsansFT into a pair of vectors depending on if they need Lifting operators or not
  typedef typename boost::mpl::partition< typename BaseType::BCVectorsansFT,
                                          is_DGBR2<DiscTag, boost::mpl::_1 >,
                                          boost::mpl::back_inserter< boost::mpl::vector<> >,
                                          boost::mpl::back_inserter< boost::mpl::vector<> >
                                        >::type PairBCVectorsansFT;

  // Define the two extra BCVectors
  typedef typename PairBCVectorsansFT::first  BCVectorDGBR2;
  typedef typename PairBCVectorsansFT::second BCVectorsansFT;

  // Decorate the BCVector withs the NDConvert class
  typedef typename boost::mpl::transform< BCVectorDGBR2 , add_ND_decorator<BCNDConvert, boost::mpl::_1> >::type BCNDVectorDGBR2;
  typedef typename boost::mpl::transform< BCVectorsansFT, add_ND_decorator<BCNDConvert, boost::mpl::_1> >::type BCNDVectorsansFT;

  // Group the BCVectors by category
  typedef typename GroupBCVectorByCategory<BCNDVectorDGBR2>::type BCNDVectorsDGBR2cat;
  typedef typename GroupBCVectorByCategory<BCNDVectorsansFT>::type BCNDVectorssansFTcat;

  // Create two integrand boundary trace vectors
  typedef typename boost::mpl::transform< BCNDVectorsDGBR2cat, def_IntegrandBoundaryTraceOp >::type IntegrandBoundaryTraceDGBR2Vector;
  typedef typename boost::mpl::transform< BCNDVectorssansFTcat, def_IntegrandBoundaryTraceOp >::type IntegrandBoundaryTracesansFTVector;

//---------------------------------------------------------------------------//
  // A function for generating a std::vector of IntegrandBoundaryTrace instances
  template<class... Args>
  IntegrateBoundaryTrace_Dispatch_DGBR2( const PDEND& pde,
                                         PyDict& BCList,
                                         const std::map< std::string, std::shared_ptr<BCBase> >& BCs,
                                         const std::map< std::string, std::vector<int> >& BoundaryGroups,
                                         const DiscretizationDGBR2& disc,
                                         Args&&... args)
  {
    // Extract the keys from the dictionary
    std::vector<std::string> keys = BCList.stringKeys();
    std::vector<int> BoundaryGroups_key;

    for (std::size_t i = 0; i < keys.size(); ++i)
    {
      // Use a try...catch because then we can report the problem
      try
      {
        BoundaryGroups_key = BoundaryGroups.at(keys[i]);
      }
      catch (const std::out_of_range&)
      {
        //Create a more useful error message
        this->BCKeyError(keys[i], BoundaryGroups);
      }

      // Create the next boundary trace type
      std::shared_ptr<IntegrandBoundaryTraceBase> integrand =
          detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorsmitFTcat >::type,
                                                     typename boost::mpl::end< BCNDVectorsmitFTcat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key, std::forward<Args>(args)... );

      if ( integrand == NULL )
        integrand =
            detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorsmitHTcat >::type,
                                                       typename boost::mpl::end< BCNDVectorsmitHTcat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key, std::forward<Args>(args)...);

      if ( integrand == NULL )
        integrand =
            detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorssansFTcat >::type,
                                                       typename boost::mpl::end< BCNDVectorssansFTcat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key, std::forward<Args>(args)... );

      if ( integrand == NULL )
        integrand =
            detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorsDGBR2cat >::type,
                                                       typename boost::mpl::end< BCNDVectorsDGBR2cat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key, disc, std::forward<Args>(args)... );

      if ( integrand == NULL )
        SANS_DEVELOPER_EXCEPTION("Should not be possible to get here.");

      integrandBoundaryTraces_.push_back(integrand);
    }

    // Just in case the vector is longer than needed
    integrandBoundaryTraces_.shrink_to_fit();
  }

//---------------------------------------------------------------------------//
  template<class DispatchmitFTType, class DispatchDGBR2Type, class DispatchsansFTType>
  void dispatch( DispatchmitFTType&& dispatchmitFT,
                 DispatchDGBR2Type&& dispatchDGBR2,
                 DispatchsansFTType&& dispatchsansFT) const
  {
    // Loop over all integrand boundary traces and dispatch the call
    // with the IntegrandBoundaryTraceBase cast to the derived IntegrandBoundaryTrace
    for (std::size_t n = 0; n < integrandBoundaryTraces_.size(); n++ )
    {
      // Hub trace is handled separately
      if ( integrandBoundaryTraces_[n]->needsHubTrace() ) continue;

      if ( integrandBoundaryTraces_[n]->needsFieldTrace() )
        call_with_derived<IntegrandBoundaryTracemitFTVector, DispatchmitFTType&>( dispatchmitFT, *integrandBoundaryTraces_[n] );
      else
      {
        if ( integrandBoundaryTraces_[n]->discTagID() == typeid(DGBR2) ||
             integrandBoundaryTraces_[n]->discTagID() == typeid(DGBR2_manifold) )
        {
          call_with_derived<IntegrandBoundaryTraceDGBR2Vector, DispatchDGBR2Type&>( dispatchDGBR2, *integrandBoundaryTraces_[n] );
        }
        else
        {
          call_with_derived<IntegrandBoundaryTracesansFTVector, DispatchsansFTType&>( dispatchsansFT, *integrandBoundaryTraces_[n] );
        }
      }
    }
  }

//---------------------------------------------------------------------------//
  template<class DispatchDGBR2Type, class DispatchsansFTType>
  void dispatch_DGBR2( DispatchDGBR2Type&& dispatchDGBR2,
                       DispatchsansFTType&& dispatchsansFT) const
  {
    // Loop over all integrand boundary traces and dispatch the call
    // with the IntegrandBoundaryTraceBase cast to the derived IntegrandBoundaryTrace
    for (std::size_t n = 0; n < integrandBoundaryTraces_.size(); n++ )
    {
      // Hub trace is handled separately
      if ( integrandBoundaryTraces_[n]->needsHubTrace() ) continue;

      if ( integrandBoundaryTraces_[n]->discTagID() == typeid(DGBR2) ||
          integrandBoundaryTraces_[n]->discTagID() == typeid(DGBR2_manifold) )
      {
        call_with_derived<IntegrandBoundaryTraceDGBR2Vector, DispatchDGBR2Type&>( dispatchDGBR2, *integrandBoundaryTraces_[n] );
      }
      else
      {
        call_with_derived<IntegrandBoundaryTracesansFTVector, DispatchsansFTType&>( dispatchsansFT, *integrandBoundaryTraces_[n] );
      }
    }
  }

//---------------------------------------------------------------------------//
  template<class DispatchDGBR2Type>
  void dispatch_DGBR2( DispatchDGBR2Type&& dispatchDGBR2) const
  {
    // Loop over all integrand boundary traces and dispatch the call
    // with the IntegrandBoundaryTraceBase cast to the derived IntegrandBoundaryTrace
    // This function only dispatches to DGBR2 type integrands
    for (std::size_t n = 0; n < integrandBoundaryTraces_.size(); n++ )
    {
      if ( integrandBoundaryTraces_[n]->discTagID() == typeid(DGBR2) ||
           integrandBoundaryTraces_[n]->discTagID() == typeid(DGBR2_manifold) )
        call_with_derived<IntegrandBoundaryTraceDGBR2Vector, DispatchDGBR2Type&>( dispatchDGBR2, *integrandBoundaryTraces_[n] );
    }
  }

  // Expose the generic dispatch with just mitFT and sansFT options
  using BaseType::dispatch;

protected:
  using BaseType::integrandBoundaryTraces_;
};

}

#endif //INTEGRATEBOUNDARYTRACE_DISPATCH_DGBR2_H
