// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANDETINVRESIDUALNORM_H
#define JACOBIANDETINVRESIDUALNORM_H

#include "tools/SANSnumerics.h" // Real

#include <vector>

#include "tools/KahanSum.h"

#include "Quadrature/Quadrature.h"
#include "BasisFunction/Quadrature_Cache.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/XField.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

//----------------------------------------------------------------------------//
//
// Computes a residual norm weighted by the inverse mass matrix
//
//----------------------------------------------------------------------------//


namespace SANS
{

template<class NDPDEClass, template<class> class Vector >
class JacobianDetInvResidualNorm_impl : public GroupFunctorCellType< JacobianDetInvResidualNorm_impl<NDPDEClass,Vector> >
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;

  JacobianDetInvResidualNorm_impl(
      const int comm_rank,
      const std::vector<int>& cellgroup_list,
      const NDPDEClass& pde,
      const Vector<ArrayQ>& rsd,
      std::vector<KahanSum<Real>>& rsdNorm,
      Real& invJ) :
    comm_rank_(comm_rank), cellgroup_list_(cellgroup_list),
    pde_(pde), rsd_(rsd), rsdNorm_(rsdNorm), invJ_(invJ) {}

  std::size_t nCellGroups() const          { return cellgroup_list_.size(); }
  std::size_t cellGroup(const int n) const { return cellgroup_list_[n];     }

  //----------------------------------------------------------------------------//
  // Function that is applied to each cell group
  template< class Topology >
  void
  apply(const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>,
                                   Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>::
                          template FieldCellGroupType<Topology>& fldsCellGroup,
        const int cellGroupGlobal)
  {
    typedef typename XField< PhysDim, typename Topology::TopoDim        >::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename  Field< PhysDim, typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;

    typedef QuadraturePoint<typename Topology::TopoDim> QuadPointType;


    const XFieldCellGroupType& xfldCell = get<0>(fldsCellGroup);
    const QFieldCellGroupType& qfldCell = get<1>(fldsCellGroup);

    ElementXFieldClass xfldElem( xfldCell.basis() );

    // number of DOFs per element
    const int nDOF = qfldCell.nBasis();

    DLA::VectorD<ArrayQ> rsdElem(nDOF);
    std::vector<int> mapDOFGlobal(nDOF);

    const int nElem = qfldCell.nElem(); //Number of elements in cell group

    const int nMon = pde_.nMonitor();
    DLA::VectorD<Real> rsdtmp(nMon);

    // Not high enough order to integrate the size exactly, but does not need to be exact
    Quadrature<typename Topology::TopoDim, Topology> quadrature( xfldCell.basis()->order() );
    const int nquad = quadrature.nQuadrature();
    Real weight, J;
    Real invJ;

    //loop over cells in local mesh
    for (int elem = 0; elem < nElem; elem++ )
    {
      // get the grid element
      xfldCell.getElement( xfldElem, elem );

      // don't consider residuals possessed by other processors
      if (xfldElem.rank() != comm_rank_) continue;

      // get the global mapping for the element DOFs
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), mapDOFGlobal.size() );

      // copy global DOFs from the residual vector
      for (int n = 0; n < nDOF; n++)
        rsdElem[n] = rsd_[mapDOFGlobal[n]];

      // compute the size of the cell element
      J = 0;
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadrature.weight( iquad, weight );
        QuadPointType sRef = quadrature.coordinates_cache( iquad );

        J += weight * xfldElem.jacobianDeterminant( sRef );
      }
      invJ = 1./J;

      // Accumulate the total inverse Jacobian
      invJ_ += invJ;

      // add the squared residual norm
      for (int n = 0; n < nDOF; n++)
      {
        pde_.interpResidVariable(rsdElem[n], rsdtmp);

        // weight the elemental residual by the inverse Jacobian determinant
        for (int j = 0; j < nMon; j++)
          rsdNorm_[j] += pow(rsdtmp[j]*invJ, 2);
      }

    } //loop over elements
  }

protected:
  const int comm_rank_;
  const std::vector<int> cellgroup_list_;
  const NDPDEClass& pde_;
  const Vector<ArrayQ>& rsd_;
  std::vector<KahanSum<Real>>& rsdNorm_;
  Real& invJ_;
};

// Factory function
template<class NDPDEClass, template<class> class Vector, class ArrayQ >
JacobianDetInvResidualNorm_impl<NDPDEClass, Vector>
JacobianDetInvResidualNorm( const int comm_rank,
                            const std::vector<int>& cellgroup_list,
                            const NDPDEClass& pde,
                            const Vector<ArrayQ>& rsd,
                            std::vector<KahanSum<Real>>& rsdNorm, Real& invJ )
{
  return {comm_rank, cellgroup_list, pde, rsd, rsdNorm, invJ};
}

} // namespace SANS


#endif //JACOBIANDETINVRESIDUALNORM_H
