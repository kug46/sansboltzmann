// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDINTERIORTRACE_DGBR2_MANIFOLD_H_
#define INTEGRANDINTERIORTRACE_DGBR2_MANIFOLD_H_

// interior trace integrand operators: DG BR2 (Little-r formulation) for manifolds

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/SurrealS.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "DiscretizationDGBR2.h"
#include "JacobianInteriorTrace_DGBR2_Element.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// interior element trace integrand: DG BR2 (Little-r formulation) for manifolds
//
// integrandL = + [[phiL]].{F} - [[phiL]].(eta*{rL + rR})
// integrandR = + [[phiR]].{F} - [[phiR]].(eta*{rL + rR})
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective, viscous)

template <class PDE_>
class IntegrandInteriorTrace_DGBR2_manifold :
    public IntegrandInteriorTraceType< IntegrandInteriorTrace_DGBR2_manifold<PDE_> >
{
public:
  typedef PDE_ PDE; // NDPDEClass
  typedef typename PDE::PhysDim PhysDim;

  static const int D = PhysDim::D;   // Physical dimensions
  static const int N = PDE::N;       // total PDE equations

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;             // solution/residual arrays

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;     // Lifting Operator integrand

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;           // solution/residual jacobians

  IntegrandInteriorTrace_DGBR2_manifold( const PDE& pde,
                                         const DiscretizationDGBR2& disc,
                                         const std::vector<int>& InteriorTraceGroups) :
      pde_(pde),
      disc_(disc),
      interiorTraceGroups_(InteriorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  // used to know if the source needs the gradient (i.e. lifting operator)
  bool needsSolutionGradientforSource() const
  {
    return pde_.needsSolutionGradientforSource();
  }

  // functor: computes integrands
  template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                              class TopoDimCell,  class TopologyL,     class TopologyR,
                                                  class ElementParamL, class ElementParamR>
  class BasisWeighted_PDE
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef Element<VectorArrayQ<Tr>, TopoDimCell, TopologyL> ElementRFieldL;
    typedef Element<VectorArrayQ<Tr>, TopoDimCell, TopologyR> ElementRFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef JacobianElemInteriorTrace_DGBR2_PDE<PhysDim,MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes; // manifold local axes type

    BasisWeighted_PDE( const PDE& pde,
                       const DiscretizationDGBR2& disc,
                       const ElementXFieldTrace& xfldElemTrace,
                       const CanonicalTraceToCell& canonicalTraceL,
                       const CanonicalTraceToCell& canonicalTraceR,
                       const ElementParamL&  paramfldElemL,
                       const ElementQFieldL& qfldElemL,
                       const ElementRFieldL& rfldElemL,
                       const ElementParamR&  paramfldElemR,
                       const ElementQFieldR& qfldElemR,
                       const ElementRFieldR& rfldElemR,
                       const bool liftedOnly = false ) :
        pde_(pde),
        disc_(disc),
        xfldElemTrace_(xfldElemTrace),
        canonicalTraceL_(canonicalTraceL),
        canonicalTraceR_(canonicalTraceR),
        xfldElemL_(get<-1>(paramfldElemL)),
        qfldElemL_(qfldElemL),
        rfldElemL_(rfldElemL),
        paramfldElemL_(paramfldElemL),
        xfldElemR_(get<-1>(paramfldElemR)),
        qfldElemR_(qfldElemR),
        rfldElemR_(rfldElemR),
        paramfldElemR_(paramfldElemR),
        nDOFL_(qfldElemL_.nDOF()),
        nDOFR_(qfldElemR_.nDOF()),
        phiL_( new Real[nDOFL_] ),
        phiR_( new Real[nDOFR_] ),
        gradphiL_( new VectorX[nDOFL_] ),
        gradphiR_( new VectorX[nDOFR_] ),
        liftedOnly_(liftedOnly)
    {
      SANS_ASSERT( qfldElemL_.basis() == rfldElemL_.basis() );
      SANS_ASSERT( qfldElemR_.basis() == rfldElemR_.basis() );
    }

    ~BasisWeighted_PDE()
    {
      delete [] phiL_;
      delete [] phiR_;

      delete [] gradphiL_;
      delete [] gradphiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return nDOFL_; }
    int nDOFRight() const { return nDOFR_; }

    // trace element integrand
    template <class Ti>
    void operator()(const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandL[], int neqnL,
                                                         ArrayQ<Ti> integrandR[], int neqnR) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemPDEL,
                     JacobianElemInteriorTraceType& mtxElemPDER ) const;
  private:

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const QuadPointTraceType& sRefTrace,
                            const VectorX& nL,
                            const LocalAxes& e0L, const LocalAxes& e0R,
                            const ParamTL& paramL, const ParamTR& paramR,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                            const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqliftedR,
                            ArrayQ<Ti> integrandL[], const int neqnL,
                            ArrayQ<Ti> integrandR[], const int neqnR,
                            bool gradientOnly = false ) const;

    const PDE& pde_;
    const DiscretizationDGBR2& disc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementRFieldL& rfldElemL_;    // lifting operator (L)
    const ElementParamL&  paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementRFieldR& rfldElemR_;    // lifting operator (R)
    const ElementParamR&  paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
    mutable VectorX *gradphiL_;
    mutable VectorX *gradphiR_;

    const bool liftedOnly_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                                        class ElementParamL, class ElementParamR >
  class BasisWeighted_LO
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementRFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyR> ElementRFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;
    typedef typename ElementParamR::T ParamTR;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef VectorArrayQ<T> IntegrandType;

    BasisWeighted_LO( const PDE& pde,
                      const ElementXFieldTrace& xfldElemTrace,
                      const CanonicalTraceToCell& canonicalTraceL,
                      const CanonicalTraceToCell& canonicalTraceR,
                      const ElementParamL&  paramfldElemL,
                      const ElementQFieldL& qfldElemL,
                      const ElementParamR&  paramfldElemR,
                      const ElementQFieldR& qfldElemR ) :
        pde_(pde),
        xfldElemTrace_(xfldElemTrace),
        canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
        xfldElemL_(get<-1>(paramfldElemL)),
        qfldElemL_(qfldElemL),
        paramfldElemL_(paramfldElemL),
        xfldElemR_(get<-1>(paramfldElemR)),
        qfldElemR_(qfldElemR),
        paramfldElemR_(paramfldElemR),
        nDOFL_(qfldElemL_.nDOF()),
        nDOFR_(qfldElemR_.nDOF()),
        phiL_( new Real[nDOFL_] ),
        phiR_( new Real[nDOFR_] )
    {}

    ~BasisWeighted_LO()
    {
      delete [] phiL_;
      delete [] phiR_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return ( pde_.hasFluxViscous() ||
              (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );
    }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // trace element integrand
    void operator()( const QuadPointTraceType& sRef, IntegrandType integrandL[], int neqnL,
                                                     IntegrandType integrandR[], int neqnR ) const;

  private:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementParamL&  paramfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementParamR&  paramfldElemR_;

    const int nDOFL_;
    const int nDOFR_;
    mutable Real *phiL_;
    mutable Real *phiR_;
  };

  // return integrand functor
  template<class Tq, class Tr, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class TopologyL,     class TopologyR,
                                                   class ElementParamL, class ElementParamR>
  BasisWeighted_PDE<Tq, Tr, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  TopologyL, TopologyR,
                                          ElementParamL, ElementParamR>
  integrand_PDE(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
                const CanonicalTraceToCell& canonicalTraceL,
                const CanonicalTraceToCell& canonicalTraceR,
                const ElementParamL&                                     paramfldElemL,
                const Element<ArrayQ<Tq>      , TopoDimCell, TopologyL>& qfldElemL,
                const Element<VectorArrayQ<Tr>, TopoDimCell, TopologyL>& rfldElemL,
                const ElementParamR&                                     paramfldElemR,
                const Element<ArrayQ<Tq>      , TopoDimCell, TopologyR>& qfldElemR,
                const Element<VectorArrayQ<Tr>, TopoDimCell, TopologyR>& rfldElemR,
                const bool liftedOnly = false) const
  {
    return {pde_, disc_, xfldElemTrace,
            canonicalTraceL, canonicalTraceR,
            paramfldElemL, qfldElemL, rfldElemL,
            paramfldElemR, qfldElemR, rfldElemR, liftedOnly};
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,     class TopologyR,
                    class ElementParamL, class ElementParamR>
  BasisWeighted_LO<T, TopoDimTrace, TopologyTrace,
                      TopoDimCell, TopologyL, TopologyR,
                      ElementParamL, ElementParamR>
  integrand_LO(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
                 const CanonicalTraceToCell& canonicalTraceL,
                 const CanonicalTraceToCell& canonicalTraceR,
                 const ElementParamL&                                    paramfldElemL,
                 const Element<ArrayQ<T>    , TopoDimCell, TopologyL>&   qfldElemL,
                 const ElementParamR&                                    paramfldElemR,
                 const Element<ArrayQ<T>    , TopoDimCell, TopologyR>&   qfldElemR) const
  {
    return {pde_, xfldElemTrace,
            canonicalTraceL, canonicalTraceR,
            paramfldElemL, qfldElemL,
            paramfldElemR, qfldElemR};
  }

private:
  const PDE& pde_;
  const DiscretizationDGBR2& disc_;
  const std::vector<int> interiorTraceGroups_;
};


template <class PDE_>
template<class Tq, class Tr, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class TopologyL,     class TopologyR,
                                                 class ElementParamL, class ElementParamR>
bool
IntegrandInteriorTrace_DGBR2_manifold<PDE_>::
BasisWeighted_PDE<Tq, Tr, TopoDimTrace, TopologyTrace,
                          TopoDimCell, TopologyL, TopologyR,
                          ElementParamL, ElementParamR>::
needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() );
}


template <class PDE_>
template<class Tq, class Tr, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
template <class Ti>
void
IntegrandInteriorTrace_DGBR2_manifold<PDE_>::
BasisWeighted_PDE<Tq, Tr, TopoDimTrace, TopologyTrace,
                          TopoDimCell, TopologyL, TopologyR,
                          ElementParamL, ElementParamR>::
operator()( const QuadPointTraceType& sRefTrace,
            ArrayQ<Ti> integrandL[], const int neqnL,
            ArrayQ<Ti> integrandR[], const int neqnR ) const
{
  static_assert( TopoDimCell::D == 1, "Only TopoD1 element is implemented so far." ); // TODO: generalize

  typedef typename promote_Surreal<Tq, Tr>::type Tg;

  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes;  // manifold local axes type

  ParamTL paramL;
  ParamTR paramR;                 // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                     // unit trace normal vector (points to right element)
  VectorX e01L, e01R;             // basis direction vector
  LocalAxes e0L, e0R;             // basis direction vector

  ArrayQ<Tq> qL, qR;               // solutions
  ArrayQ<Tq> uL, uR;               // conservation solutions
  VectorArrayQ<Tq> gradqL, gradqR; // solution gradients

  VectorArrayQ<Tr> rliftL, rliftR; // lifting operators; x- and y-components
  Real eta = 0;                    // BR2 viscous eta

  VectorArrayQ<Tg> gradqliftedL;
  VectorArrayQ<Tg> gradqliftedR;

  QuadPointCellType sRefL;        // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      ( pde_.hasSource() && pde_.needsSolutionGradientforSource() );

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters (includes X)
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // surface basis vectors
  xfldElemL_.unitTangent( sRefL, e01L );
  xfldElemR_.unitTangent( sRefR, e01R );
  e0L[0] = e01L;
  e0R[0] = e01R;

  // unit normal: points to R
  traceUnitNormal( xfldElemTrace_, sRefTrace, xfldElemL_, sRefL, xfldElemR_, sRefR, nL);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  // PDE
  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );
  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

    rfldElemL_.evalFromBasis( phiL_, nDOFL_, rliftL );
    rfldElemR_.evalFromBasis( phiR_, nDOFR_, rliftR );

    eta = disc_.viscousEta( xfldElemTrace_, xfldElemL_, xfldElemR_, canonicalTraceL_.trace, canonicalTraceR_.trace );

    gradqliftedL = gradqL + eta*rliftL;
    gradqliftedR = gradqR + eta*rliftR;
  }
  else
  {
    gradqL = 0;
    gradqR = 0;
    rliftL = 0;
    rliftR = 0;
  }

  // compute the integrand
  weightedIntegrand( sRefTrace,
                     nL,
                     e0L, e0R,
                     paramL, paramR,
                     qL, gradqliftedL,
                     qR, gradqliftedR,
                     integrandL, neqnL,
                     integrandR, neqnR );

}

//---------------------------------------------------------------------------//
template <class PDE>
template<class Tq, class Tr, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class TopologyL, class TopologyR,
                             class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_DGBR2_manifold<PDE>::
BasisWeighted_PDE<Tq, Tr, TopoDimTrace, TopologyTrace,
                          TopoDimCell,  TopologyL,     TopologyR,
                                        ElementParamL, ElementParamR>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemR ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFR_);

  SANS_ASSERT(mtxElemR.nTest == nDOFR_);
  SANS_ASSERT(mtxElemR.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemR.nDOFR == nDOFR_);

  ParamTL paramL;                    // Elemental parameters (such as grid coordinates and distance functions)
  ParamTR paramR;                    // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                        // unit normal for left element (points to right element)
  VectorX e01L, e01R;                // basis direction vector
  LocalAxes e0L, e0R;                // basis direction vector

  ArrayQ<Real> qL, qR;               // solution
  VectorArrayQ<Real> gradqL, gradqR; // gradient

  VectorArrayQ<Real> rliftL, rliftR; // lifting operators; x- and y-components
  Real eta = 0;                      // BR2 viscous eta

  ArrayQ<SurrealClass> qSurrealL, qSurrealR; // solution

  QuadPointCellType sRefL;         // reference-element coordinates of the cell
  QuadPointCellType sRefR;

  const bool needsSolutionGradient = pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) || pde_.hasSourceTrace();

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // Elemental parameters
  paramfldElemL_.eval( sRefL, paramL );
  paramfldElemR_.eval( sRefR, paramR );

  // surface basis vectors
  xfldElemL_.unitTangent( sRefL, e01L );
  xfldElemR_.unitTangent( sRefR, e01R );
  e0L[0] = e01L;
  e0R[0] = e01R;

  // unit normal: points to R
  traceUnitNormal(xfldElemTrace_, sRefTrace, xfldElemL_, sRefL, xfldElemR_, sRefR, nL);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR_, nDOFR_ );

  VectorArrayQ<Real> gradqliftedL;
  VectorArrayQ<Real> gradqliftedR;

  VectorArrayQ<SurrealClass> gradqliftedSurrealL;
  VectorArrayQ<SurrealClass> gradqliftedSurrealR;

  // PDE
  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  qSurrealL = qL;
  qSurrealR = qR;

  if (needsSolutionGradient)
  {
    qfldElemL_.evalFromBasis( gradphiL_, nDOFL_, gradqL );
    qfldElemR_.evalFromBasis( gradphiR_, nDOFR_, gradqR );

    rfldElemL_.evalFromBasis( phiL_, nDOFL_, rliftL );
    rfldElemR_.evalFromBasis( phiR_, nDOFR_, rliftR );

    eta = disc_.viscousEta( xfldElemTrace_, xfldElemL_, xfldElemR_, canonicalTraceL_.trace, canonicalTraceR_.trace );

    gradqliftedSurrealL = gradqliftedL = gradqL + eta*rliftL;
    gradqliftedSurrealR = gradqliftedR = gradqR + eta*rliftR;
  }

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandRSurreal( nDOFR_ );

  MatrixQ<Real> PDE_q;

  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( sRefTrace,
                       nL,
                       e0L, e0R,
                       paramL, paramR,
                       qSurrealL, gradqliftedL,
                       qSurrealR, gradqliftedR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_ );

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemR.PDE_qL(i,j) += dJ*phiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealR, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemL.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFR_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFR_; j++)
          mtxElemR.PDE_qR(i,j) += dJ*phiR_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;
  } // nchunk

  DLA::MatrixDView<Real> phiL(phiL_, 1, nDOFL_);
  DLA::MatrixDView<Real> phiR(phiR_, 1, nDOFR_);

  // compute the lifting operator jacobians at the quadrature point
  DLA::MatrixD<VectorX> rL_qL = eta*(phiL*mtxElemL.r_qL);
  DLA::MatrixD<VectorX> rL_qR = eta*(phiL*mtxElemL.r_qR);

  DLA::MatrixD<VectorX> rR_qL = eta*(phiR*mtxElemR.r_qL);
  DLA::MatrixD<VectorX> rR_qR = eta*(phiR*mtxElemR.r_qR);

  MatrixQ<Real> PDEL_gradqL;
  MatrixQ<Real> PDEL_gradqR;
  MatrixQ<Real> PDER_gradqL;
  MatrixQ<Real> PDER_gradqR;

  // loop over derivative chunks
  // this simultaneously computes derivatives w.r.t gradq, rlift,
  // and the lifting operator quation wrt q
  for (int nchunk = 0; nchunk < PhysDim::D*2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealL[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealR[d], n).deriv(slot + n - nchunk) = 1;
      slot += PDE::N;
    }

    integrandLSurreal = 0;
    integrandRSurreal = 0;

    // compute the integrand
    weightedIntegrand( sRefTrace,
                       nL,
                       e0L, e0R,
                       paramL, paramR,
                       qL, gradqliftedSurrealL,
                       qR, gradqliftedSurrealR,
                       integrandLSurreal.data(), nDOFL_,
                       integrandRSurreal.data(), nDOFR_, true );

    // accumulate derivatives into element jacobian

    slot = 0;
    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOFL_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDEL_gradqL,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

          const MatrixQ<Real>& PDEL_rL = PDEL_gradqL; // alias for clarity

          // Documentation to explain the simplification below
          // PDEL_qL = gradphiL_[j][d]*PDEL_gradqL;
          // PDEL_rL = PDEL_gradqL;
          // mtxElemL.PDE_qL(i,j) += dJ*(gradphiL_[j][d]*PDEL_gradqL + PDEL_rL*rL_qL(0,j)[d]);
          for (int j = 0; j < nDOFL_; j++)
            mtxElemL.PDE_qL(i,j) += dJ*(gradphiL_[j][d] + rL_qL(0,j)[d])*PDEL_gradqL;

          for (int j = 0; j < nDOFR_; j++)
            mtxElemL.PDE_qR(i,j) += dJ*PDEL_rL*rL_qR(0,j)[d];

        } // for i

        for (int i = 0; i < nDOFR_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDER_gradqL,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

          const MatrixQ<Real>& PDER_rL = PDER_gradqL; // alias for clarity

          // Documentation to explain the simplification below
          // PDER_qL = gradphiL_[j][d]*PDEL_gradqL;
          // PDER_rL = PDER_gradqL;
          // mtxElemR.PDE_qL(i,j) += dJ*(gradphiL_[j][d]*PDER_gradqL + PDER_rL*rL_qL(0,j)[d]);
          for (int j = 0; j < nDOFL_; j++)
            mtxElemR.PDE_qL(i,j) += dJ*(gradphiL_[j][d] + rL_qL(0,j)[d])*PDER_gradqL;

          for (int j = 0; j < nDOFR_; j++)
            mtxElemR.PDE_qR(i,j) += dJ*PDER_rL*rL_qR(0,j)[d];

        } // for i
      } // if slot
      slot += PDE::N;
    }

    for (int d = 0; d < PhysDim::D; d++)
    {
      if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      {
        for (int n = 0; n < PDE::N; n++)
          DLA::index(gradqliftedSurrealR[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

        for (int i = 0; i < nDOFL_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDEL_gradqR,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

          const MatrixQ<Real>& PDEL_rR = PDEL_gradqR; // alias for clarity

          // Documentation to explain the simplification below
          // PDEL_qR = gradphiR_[j][d]*PDEL_gradqR;
          // PDEL_rR = PDEL_gradqR;
          // mtxElemL.PDE_qR(i,j) += dJ*(gradphiR_[j][d]*PDEL_gradqR + PDEL_rR*rR_qR(0,j)[d]);
          for (int j = 0; j < nDOFR_; j++)
            mtxElemL.PDE_qR(i,j) += dJ*(gradphiR_[j][d] + rR_qR(0,j)[d])*PDEL_gradqR;

          for (int j = 0; j < nDOFL_; j++)
            mtxElemL.PDE_qL(i,j) += dJ*PDEL_rR*rR_qL(0,j)[d];
        }

        for (int i = 0; i < nDOFR_; i++)
        {
          // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
          for (int m = 0; m < PDE::N; m++)
            for (int n = 0; n < PDE::N; n++)
              DLA::index(PDER_gradqR,m,n) = DLA::index(integrandRSurreal[i], m).deriv(slot + n - nchunk);

          const MatrixQ<Real>& PDER_rR = PDER_gradqR; // alias for clarity

          // Documentation to explain the simplification below
          // PDER_qR = gradphiR_[j][d]*PDEL_gradqR;
          // PDER_rR = PDER_gradqR;
          // mtxElemR.PDE_qR(i,j) += dJ*(gradphiR_[j][d]*PDER_gradqR + PDER_rR*rR_qR(0,j)[d]);
          for (int j = 0; j < nDOFR_; j++)
            mtxElemR.PDE_qR(i,j) += dJ*(gradphiR_[j][d] + rR_qR(0,j)[d])*PDER_gradqR;

          for (int j = 0; j < nDOFL_; j++)
            mtxElemR.PDE_qL(i,j) += dJ*PDER_rR*rR_qL(0,j)[d];
        }
      } // if slot
      slot += PDE::N;
    }
  } // nchunk

}


template <class PDE_>
template<class T, class Tr, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL,     class TopologyR,
                                      class ElementParamL, class ElementParamR>
template<class Tq, class Tg, class Ti>
void
IntegrandInteriorTrace_DGBR2_manifold<PDE_>::
BasisWeighted_PDE<T, Tr, TopoDimTrace, TopologyTrace,
                         TopoDimCell, TopologyL, TopologyR,
                         ElementParamL, ElementParamR>::
weightedIntegrand( const QuadPointTraceType& sRefTrace,
                   const VectorX& nL,
                   const LocalAxes& e0L, const LocalAxes& e0R,
                   const ParamTL& paramL, const ParamTR& paramR,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& gradqliftedL,
                   const ArrayQ<Tq>& qR, const VectorArrayQ<Tg>& gradqliftedR,
                   ArrayQ<Ti> integrandL[], const int neqnL,
                   ArrayQ<Ti> integrandR[], const int neqnR,
                   bool gradientOnly ) const
{

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0;

  // advective flux term: +[[phi]].flux = (phi@L - phi@R) N.flux
  //                                    = +phiL N.flux; -phiR N.flux

  ArrayQ<Ti> FnL = 0, FnR = 0;     // normal flux (if different for left/right elements)

  if (pde_.hasFluxAdvective() && !liftedOnly_)
  {
    const int coNrmSignL = xfldElemTrace_.conormalSignL(sRefTrace);
    const int coNrmSignR = xfldElemTrace_.conormalSignR(sRefTrace);

    LocalAxes e0ReffL, e0LeffR;
    e0ReffL = -coNrmSignL*(coNrmSignR*e0R); // effective e0R used by left element
    e0LeffR = -coNrmSignR*(coNrmSignL*e0L); // effective e0L used by right element

    pde_.fluxAdvectiveUpwind( paramL, paramR, e0L, e0ReffL, qL, qR, nL, FnL );
    pde_.fluxAdvectiveUpwind( paramL, paramR, e0LeffR, e0R, qL, qR, nL, FnR );

    for (int k = 0; k < neqnL; k++)
      integrandL[k] +=  phiL_[k]*FnL;

    for (int k = 0; k < neqnR; k++)
      integrandR[k] += -phiR_[k]*FnR;
  }

  VectorArrayQ<Tq> dqn;            // dummy variable for jump in q

  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < D; d++)
    dqn[d] = nL[d]*(qL - qR);

  // viscous flux term: +[[phi]].flux -[[phi]].(eta*{KL*rL + KL*rR})
  //                    = (phi@L - phi@R) N.flux -(phi@L - phi@R) N.(eta*{KL*rL + KR*rR})
  //                    = +phiL N.flux; -phiR N.flux; phiL N.(-eta*{KL*rL + KR*rR}); -phiR N.(-eta*{KL*rL + KR*rR})

  // dual consistency term: -{gradphi^t (dgradU/dgradQ K) }.[[q]]
    //                      = - 0.5*( gradphi@L^t (K*dgradUR/dgradQR) + gradphi@R^t (KR*dgradUR/dgradQR) ). nL*( qL - qR )

  if (pde_.hasFluxViscous())
  {
    // viscous flux term: +[[phi]].flux -[[phi]].(eta*{KL*rL + KL*rR})
    //                    = (phi@L - phi@R) N.flux -(phi@L - phi@R) N.(eta*{KL*rL + KR*rR})
    //                    = +phiL N.flux; -phiR N.flux; phiL N.(-eta*{KL*rL + KR*rR}); -phiR N.(-eta*{KL*rL + KR*rR})

    // dual consistency term: -{gradphi^t (dgradU/dgradQ K) }.[[q]]
      //                      = - 0.5*( gradphi@L^t (K*dgradUR/dgradQR) + gradphi@R^t (KR*dgradUR/dgradQR) ). nL*( qL - qR )

    SANS_DEVELOPER_EXCEPTION("FluxViscous has not yet been implemented on manifolds."); // TODO: to be developed
#if 0
    VectorArrayQ<T> gradqliftedL = gradqL + eta*rliftL;
    VectorArrayQ<T> gradqliftedR = gradqR + eta*rliftR;

    VectorArrayQ<T> FL = 0;
    VectorArrayQ<T> FR = 0;
    pde_.fluxViscous( paramL, qL, gradqliftedL, FL );
    pde_.fluxViscous( paramR, qR, gradqliftedR, FR );

    VectorArrayQ<T> Favg = 0.5*(FL + FR);
    Fn += dot(N,Favg);  // TODO: should this be added onto fluxAdvective

    // Dual consistency term

    MatrixQ<T> uL_qL = 0; // conservation solution jacobians dU(Q)/dQ
    MatrixQ<T> uR_qR = 0;
    pde_.jacobianMasterState( qL, uL_qL );
    pde_.jacobianMasterState( qR, uR_qR );

    VectorArrayQ<T> dunL, dunR;
    for ( int i = 0; i < D; i++ )
    {
      dunL[i] = uL_qL*dqn[i];
      dunR[i] = uR_qR*dqn[i];
    }

    DLA::MatrixS< D, D, MatrixQ<T> > KL = 0, KR = 0;   // diffusion matrix

    pde_.diffusionViscous( paramL, qL, KL );
    pde_.diffusionViscous( paramR, qR, KR );

    VectorArrayQ<T> tmpL = KL*dunL;
    VectorArrayQ<T> tmpR = KR*dunR;

    for (int k = 0; k < neqnL; k++)
      integrandL[k].PDE -= 0.5*dot(gradphiL_[k],tmpL);

    for (int k = 0; k < neqnR; k++)
      integrandR[k].PDE -= 0.5*dot(gradphiR_[k],tmpR);
#endif
  }

}

//---------------------------------------------------------------------------//
template <class PDE>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class TopologyL, class TopologyR,
                  class ElementParamL, class ElementParamR>
void
IntegrandInteriorTrace_DGBR2_manifold<PDE>::
BasisWeighted_LO<T, TopoDimTrace, TopologyTrace,
                    TopoDimCell,  TopologyL,     TopologyR,
                    ElementParamL, ElementParamR>::
operator()(const QuadPointTraceType& sRefTrace,
           IntegrandType integrandL[], const int neqnL,
           IntegrandType integrandR[], const int neqnR ) const
{
  SANS_ASSERT(neqnL == nDOFL_);
  SANS_ASSERT(neqnR == nDOFR_);

  VectorX N;                     // unit normal for left element (points to right element)

  ArrayQ<T> qL, qR;               // solution
  VectorArrayQ<T> dqn;            // jump in q.n

  QuadPointCellType sRefL;        // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // unit normal: points to R
  traceUnitNormal( xfldElemTrace_, sRefTrace, xfldElemL_, sRefL, xfldElemR_, sRefR, N);

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemR_.evalBasis( sRefR, phiR_, nDOFR_ );

  // PDE
  // solution value, gradient, lifting operators, viscous eta parameter
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemR_.evalFromBasis( phiR_, nDOFR_, qR );

  // Jump in primitive variable [ nL*( qL - qR ) ]
  for (int d = 0; d < D; d++)
    dqn[d] = N[d]*(qL - qR);

  // Lifting Operator
  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0.5*phiL_[k]*dqn;

  for (int k = 0; k < neqnR; k++)
    integrandR[k] = 0.5*phiR_[k]*dqn;
}

} // namespace SANS

#endif /* INTEGRANDINTERIORTRACE_DGBR2_MANIFOLD_H_ */
