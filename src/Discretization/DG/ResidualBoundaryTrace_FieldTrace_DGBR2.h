// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_FIELDTRACE_DGBR2_H
#define RESIDUALBOUNDARYTRACE_FIELDTRACE_DGBR2_H

// boundary-trace integral residual functions

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DGBR2 boundary-trace integral
//

template<class IntegrandBoundaryTrace, template<class> class Vector>
class ResidualBoundaryTrace_FieldTrace_DGBR2_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_FieldTrace_DGBR2_impl<IntegrandBoundaryTrace, Vector> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_FieldTrace_DGBR2_impl( const IntegrandBoundaryTrace& fcn,
                                          Vector<ArrayQ>& rsdPDEGlobal,
                                          Vector<ArrayQ>& rsdBCGlobal) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& lgfld ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&           qfld = get<0>(flds);

    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOF() );
    SANS_ASSERT( rsdBCGlobal_.m()  == lgfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
             int quadratureorder )
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented or tested.");
#if 0
    typedef typename XFieldType                             ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>        ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const RFieldCellGroupTypeL& rfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

    // number of integrals evaluated per element
    int nIntegrandL = qfldElemL.nDOF();
    int nIntegrandBC = lgfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL );
    std::vector<int> mapDOFGlobalBC( nIntegrandBC );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQ, ArrayQ>
      integral(quadratureorder, nIntegrandL, nIntegrandBC);

    // element integrand/residuals
    std::vector<ArrayQ> rsdPDEElemL( nIntegrandL );
    std::vector<ArrayQ> rsdBCElem( nIntegrandBC );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      rfldCellL.getElement( rfldElemL, elemL, canonicalTraceL.trace );

      xfldTrace.getElement( xfldElemTrace, elem );
      lgfldTrace.getElement( lgfldElemTrace, elem );

      // only need to perform the integral on processors that possess the element
      if ( qfldElemL.rank() != comm_rank_ ) continue;

      for (int n = 0; n < nIntegrandL; n++)
        rsdPDEElemL[n] = 0;

      for (int n = 0; n < nIntegrandBC; n++)
        rsdBCElem[n] = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL, rfldElemL,
                               lgfldElemTrace),
                get<-1>(xfldElemTrace),
                rsdPDEElemL.data(), nIntegrandL,
                rsdBCElem.data(), nIntegrandBC );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );
      lgfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalBC.data(), nIntegrandBC );

      int nGlobal;
      for (int n = 0; n < nIntegrandL; n++)
      {
        nGlobal = mapDOFGlobalL[n];
        rsdPDEGlobal_[nGlobal] += rsdPDEElemL[n];
      }

      for (int n = 0; n < nIntegrandBC; n++)
      {
        nGlobal = mapDOFGlobalBC[n];
        rsdBCGlobal_[nGlobal] += rsdBCElem[n];
      }
    }
#endif
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdBCGlobal_;
};

// Factory function

template<class IntegrandBoundaryTrace, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_FieldTrace_DGBR2_impl<IntegrandBoundaryTrace, Vector>
ResidualBoundaryTrace_FieldTrace_DGBR2( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                   Vector<ArrayQ>& rsdPDEGlobal,
                                   Vector<ArrayQ>& rsdBCGlobal)
{
  static_assert( std::is_same<ArrayQ, typename IntegrandBoundaryTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualBoundaryTrace_FieldTrace_DGBR2_impl<IntegrandBoundaryTrace, Vector>(fcn.cast(), rsdPDEGlobal, rsdBCGlobal);
}

}

#endif  // RESIDUALBOUNDARYTRACE_FIELDTRACE_DGBR2_H
