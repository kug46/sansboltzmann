// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDINTERIORTRACE_DGBR2_LIFTEDQUANTITY_H
#define SETFIELDINTERIORTRACE_DGBR2_LIFTEDQUANTITY_H

// Computes lifted quantity variables (i.e. sensor for shock capturing)

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "IntegrandInteriorTrace_DGBR2.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DG BR2 interior-trace integral
//

template<class IntegrandInteriorTrace>
class SetFieldInteriorTrace_DGBR2_LiftedQuantity_impl :
    public GroupIntegralInteriorTraceType< SetFieldInteriorTrace_DGBR2_LiftedQuantity_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PDE PDE;
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;

  typedef Real LiftedT; //data-type for lifted quantity

  // Save off the interor trace integrand and inverse mass matrix field
  SetFieldInteriorTrace_DGBR2_LiftedQuantity_impl( const IntegrandInteriorTrace& fcn,
                                                   const FieldDataInvMassMatrix_Cell& mmfld ) :
                                                     fcn_(fcn), mmfld_(mmfld) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // Nothing to check
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, LiftedT>, TupleClass<>>& flds ) const
  {
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
                                Field<PhysDim, TopoDim, LiftedT>, TupleClass<>>::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
                                Field<PhysDim, TopoDim, LiftedT>, TupleClass<>>::template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, LiftedT     >::template FieldCellGroupType<TopologyL> SFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename SFieldCellGroupTypeL::template ElementType<> ElementSFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, LiftedT     >::template FieldCellGroupType<TopologyR> SFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename SFieldCellGroupTypeR::template ElementType<> ElementSFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
          SFieldCellGroupTypeL& sfldCellL = const_cast<SFieldCellGroupTypeL&>(get<1>(fldsCellL));

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
          SFieldCellGroupTypeR& sfldCellR = const_cast<SFieldCellGroupTypeR&>(get<1>(fldsCellR));


    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementSFieldClassL sfldElemL( sfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementSFieldClassR sfldElemR( sfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    const int sDOFL = sfldElemL.nDOF();
    const int sDOFR = sfldElemR.nDOF();
    const int nIntegrandL = sDOFL;
    const int nIntegrandR = sDOFR;

    const DLA::MatrixDView_Array<Real>& mmfldCellL = mmfld_.getCellGroupGlobal(cellGroupGlobalL);
    const DLA::MatrixDView_Array<Real>& mmfldCellR = mmfld_.getCellGroupGlobal(cellGroupGlobalR);

    // trace element integral
    typedef LiftedT IntegrandType;
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, IntegrandType, IntegrandType>
      integral(quadratureorder, nIntegrandL, nIntegrandR);

    // element integrand/residuals
    DLA::VectorD<IntegrandType> rsdElemL( nIntegrandL );
    DLA::VectorD<IntegrandType> rsdElemR( nIntegrandR );

    // Provide a vector view of the lifting operator DOFs
    DLA::VectorDView<LiftedT> sL( sfldElemL.vectorViewDOF() );
    DLA::VectorDView<LiftedT> sR( sfldElemR.vectorViewDOF() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      sfldCellL.getElement( sfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      sfldCellR.getElement( sfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );

      for (int n = 0; n < nIntegrandL; n++) rsdElemL[n] = 0;
      for (int n = 0; n < nIntegrandR; n++) rsdElemR[n] = 0;

      integral( fcn_.integrand_LiftedScalar(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                            xfldElemL, qfldElemL, sfldElemL,
                                            xfldElemR, qfldElemR, sfldElemR),
                xfldElemTrace, rsdElemL.data(), nIntegrandL, rsdElemR.data(), nIntegrandR );

      // Accumulate the lifted quantity DOFs
      sL -= mmfldCellL[elemL]*rsdElemL;
      sR -= mmfldCellR[elemR]*rsdElemR;

      // Set the lifted quantities
      sfldCellL.setElement( sfldElemL, elemL );
      sfldCellR.setElement( sfldElemR, elemR );
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
};


// Factory function

template<class IntegrandInteriorTrace>
SetFieldInteriorTrace_DGBR2_LiftedQuantity_impl<IntegrandInteriorTrace>
SetFieldInteriorTrace_DGBR2_LiftedQuantity( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                            const FieldDataInvMassMatrix_Cell& mmfld)
{
  return SetFieldInteriorTrace_DGBR2_LiftedQuantity_impl<IntegrandInteriorTrace>(fcn.cast(), mmfld);
}

}

#endif  // SETFIELDINTERIORTRACE_DGBR2_LIFTEDQUANTITY_H
