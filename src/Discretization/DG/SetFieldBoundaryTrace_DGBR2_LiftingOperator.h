// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDBOUNDARYTRACE_DGBR2_H
#define SETFIELDBOUNDARYTRACE_DGBR2_H

// boundary-trace integral residual functions

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/Element/ElementalMassMatrix.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  DGBR2 boundary-trace integral
//

template<class IntegrandBoundaryTrace>
class SetFieldBoundaryTrace_DGBR2_LiftingOperator_impl :
    public GroupIntegralBoundaryTraceType< SetFieldBoundaryTrace_DGBR2_LiftingOperator_impl<IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;

  // Save off the boundary trace integrand and the residual vectors
  explicit SetFieldBoundaryTrace_DGBR2_LiftingOperator_impl( const IntegrandBoundaryTrace& fcn,
                                                             const FieldDataInvMassMatrix_Cell& mmfld ) :
         fcn_(fcn), mmfld_(mmfld), comm_rank_(0) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                        template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType                               ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>          ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename FieldLift<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> RFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename RFieldCellGroupTypeL::template ElementType<> ElementRFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef DLA::MatrixD<MatrixQ> MatrixLOClass;
    typedef DLA::MatrixD<VectorMatrixQ> MatrixLOElemClass;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
          RFieldCellGroupTypeL& rfldCellL = const_cast<RFieldCellGroupTypeL&>(get<1>(fldsCellL));

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementRFieldClassL rfldElemL( rfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    int nIntegrandL = qfldElemL.nDOF();
    int nDOFL = nIntegrandL;

    const DLA::MatrixDView_Array<Real>& mmfldCellL = mmfld_.getCellGroupGlobal(cellGroupGlobalL);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, VectorArrayQ> integralLO_rsd(quadratureorder, nIntegrandL);
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, MatrixLOElemClass> integralLO_r(quadratureorder);

    // element integrand/residuals
    DLA::VectorD<VectorArrayQ> rsdLOElemL( nIntegrandL );

    // residual for the local solve
    DLA::VectorD<ArrayQ> rsd( nIntegrandL ), sln( nIntegrandL );

    MatrixLOClass mm( nDOFL, nDOFL );
    MatrixLOClass M( nDOFL, nDOFL );
    MatrixLOElemClass mtxLOElemL_rL( nDOFL, nDOFL );

    ElementalMassMatrix<TopoDim, TopologyL> massMtx(get<-1>(xfldElemL), qfldElemL);

    // Provide a vector view of the lifting operator DOFs
    DLA::VectorDView<VectorArrayQ> rL( rfldElemL.vectorViewDOF() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      // only need to perform the integral on processors that possess the element
      if ( qfldElemL.rank() != comm_rank_ ) continue;

      // Set the lifting operator to zero, this assumes the BC term can be linearly dependent on rL
      rfldElemL.vectorViewDOF() = 0;

      xfldTrace.getElement( xfldElemTrace, elem );

      // compute the lifting operator residual
      integralLO_rsd( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                        xfldElemL, qfldElemL, rfldElemL),
                      get<-1>(xfldElemTrace),
                      rsdLOElemL.data(), nIntegrandL );

      if (fcn_.needsLiftingOperatorforLO())
      {
        // Compute the elemental mass matrix and then add the lifting operator term from the BC
        massMtx(get<-1>(xfldElemL), mm);

        // add the lifting operator Jacobain wrt r
        integralLO_r( fcn_.integrand_LO(xfldElemTrace, canonicalTraceL,
                                        xfldElemL, qfldElemL, rfldElemL),
                      get<-1>(xfldElemTrace),
                      mtxLOElemL_rL );

        // solve the lifting operator one dimension at a time
        for (int d = 0; d < PhysDim::D; d++)
        {
          // Copy over the negated residuals and complete the mass matrix
          for (int i = 0; i < nDOFL; i++)
          {
            rsd[i] = -rsdLOElemL[i][d];
            for (int j = 0; j < nDOFL; j++)
              M(i,j) = mm(i,j) + mtxLOElemL_rL(i,j)[d];
          }

          // Compute the lifting operator DOFs
          sln = DLA::InverseLU::Solve(M, rsd);

          // copy over the solution
          for (int i = 0; i < nDOFL; i++)
            rL[i][d] = sln[i];
        }
      }
      else
      {
        // lifting operator equation does not depend on the lifting operator
        // so it can be computed consistent with an interior trace
        rL = -mmfldCellL[elemL]*rsdLOElemL;
      }

      // Set the lifting operators
      rfldCellL.setElement( rfldElemL, elemL, canonicalTraceL.trace );
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandBoundaryTrace>
SetFieldBoundaryTrace_DGBR2_LiftingOperator_impl<IntegrandBoundaryTrace>
SetFieldBoundaryTrace_DGBR2_LiftingOperator( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                             const FieldDataInvMassMatrix_Cell& mmfld )
{
  return SetFieldBoundaryTrace_DGBR2_LiftingOperator_impl<IntegrandBoundaryTrace>(fcn.cast(), mmfld);
}

}

#endif  // SETFIELDBOUNDARYTRACE_DGBR2_H
