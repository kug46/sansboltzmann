// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDOUNDARYTRACE_DISPATCH_ADJOINTLO_H
#define SETFIELDOUNDARYTRACE_DISPATCH_ADJOINTLO_H

// boundary-trace integral adjoint lifting operator functions

#include "SetFieldBoundaryTrace_DGBR2_AdjointLO.h"

#include "Field/FieldData/FieldDataMatrixD_CellLift.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{
#if 0
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace (e.g. with Lagrange multipliers)
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
class SetFieldBoundaryTrace_FieldTrace_Dispatch_AdjointLO_impl
{
public:
  SetFieldBoundaryTrace_FieldTrace_Dispatch_AdjointLO_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), lgfld_(lgfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Can't compute lifting operators with mitLG BC's");
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
SetFieldBoundaryTrace_FieldTrace_Dispatch_AdjointLO_impl<XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>
SetFieldBoundaryTrace_FieldTrace_Dispatch_AdjointLO(const XFieldType& xfld,
                                                          const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                          const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                          const int* quadratureorder, int ngroup )
{
  return SetFieldBoundaryTrace_FieldTrace_Dispatch_AdjointLO_impl<XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
      xfld, qfld, rfld, lgfld, quadratureorder, ngroup);
}
#endif
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without field trace
//
//---------------------------------------------------------------------------//
template<class SurrealClass, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class RowMatrixQ>
class SetFieldBoundaryTrace_Dispatch_AdjointLO_impl
{
public:
  SetFieldBoundaryTrace_Dispatch_AdjointLO_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld,
      const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
      const int* quadratureorder, int ngroup )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), wfld_(wfld), sfld_(sfld), jacPDE_R_(jacPDE_R),
      quadratureorder_(quadratureorder), ngroup_(ngroup)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        SetFieldBoundaryTrace_DGBR2_AdjointLO<SurrealClass>(fcn, jacPDE_R_),
        xfld_, (qfld_, rfld_, wfld_, sfld_), quadratureorder_, ngroup_);
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld_;
  const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R_;
  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template<class SurrealClass, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class RowMatrixQ>
SetFieldBoundaryTrace_Dispatch_AdjointLO_impl<SurrealClass, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, RowMatrixQ>
SetFieldBoundaryTrace_Dispatch_AdjointLO(const XFieldType& xfld,
                                         const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                         const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                         const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                         const FieldLift<PhysDim, TopoDim, VectorArrayQ>& sfld,
                                         const FieldDataMatrixD_CellLift<RowMatrixQ>& jacPDE_R,
                                         const int* quadratureorder, int ngroup )
{
  return SetFieldBoundaryTrace_Dispatch_AdjointLO_impl<SurrealClass, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, RowMatrixQ>(
      xfld, qfld, rfld, wfld, sfld, jacPDE_R, quadratureorder, ngroup);
}

}

#endif //SETFIELDOUNDARYTRACE_DISPATCH_ADJOINTLO_H
