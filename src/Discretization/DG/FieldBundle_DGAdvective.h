// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDBUNDLE_DGADVECTIVE_H_
#define FIELDBUNDLE_DGADVECTIVE_H_

#include "Field/Field.h"
#include "Field/FieldTypes.h"

#include "Field/Local/Field_Local.h"

namespace SANS
{

//---------------------------------------------------------------------------//
// DGAdvective Field Bundles
//---------------------------------------------------------------------------//

template< class PhysD, class TopoD, class ArrayQ_>
struct FieldBundleBase_DGAdvective
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;

  FieldBundleBase_DGAdvective( const XField<PhysDim, TopoDim>& xfld,
                               Field_DG_Cell< PhysDim, TopoDim, ArrayQ >& qfld,
                               Field_DG_BoundaryTrace< PhysDim, TopoDim, ArrayQ >& lgfld,
                               const int order,
                               const BasisFunctionCategory basis_cell,
                               const BasisFunctionCategory basis_trace ) :
  xfld(xfld), qfld(qfld), lgfld(lgfld), order(order), basis_cell(basis_cell), basis_trace(basis_trace) {}

  void projectTo(FieldBundleBase_DGAdvective& bundleTo)
  {
    qfld.projectTo(bundleTo.qfld);
    lgfld.projectTo(bundleTo.lgfld);
  }

  const XField<PhysDim,TopoDim>& xfld;

  Field_DG_Cell<PhysDim,TopoDim,ArrayQ>& qfld;
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ>& lgfld;

  const int order;

  const BasisFunctionCategory basis_cell;
  const BasisFunctionCategory basis_trace;

  static constexpr SpaceType spaceType = SpaceType::Discontinuous;
};

//---------------------------------------------------------------------------//
// Forward declare so input args for local work
template< class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_DGAdvective;

// public of GlobalBundle is so that AlgebraicEquationSet Basetype works with it
template<class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_DGAdvective_Local :
    public FieldBundleBase_DGAdvective<PhysD, TopoD, ArrayQ_>
{
  typedef FieldBundleBase_DGAdvective<PhysD, TopoD, ArrayQ_> BaseType;

  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;

  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef Field_Local<QFieldType> QFieldType_Local;
  typedef Field_Local<LGFieldType> LGFieldType_Local;

  // active_local_BGroup_list is a vec of vec so that boundaries that came from the same original global trace
  // continue to have shared dofs in the local field. This is necessary in CG
  FieldBundle_DGAdvective_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                                const FieldBundle_DGAdvective<PhysDim, TopoDim, ArrayQ>& globalfields,
                                const int order, const std::vector<std::vector<int>>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_,lgfld_,order, globalfields.basis_cell, globalfields.basis_trace),
    qfld_ ( xfld_local, globalfields.qfld , order  , globalfields.basis_cell ),
    lgfld_( active_local_BGroup_list, xfld_local, globalfields.lgfld, std::max(order-1,0), globalfields.basis_trace )
  {
  }

  using BaseType::order;
  using BaseType::basis_cell;
  using BaseType::basis_trace;

protected:
  QFieldType_Local qfld_;
  LGFieldType_Local lgfld_;
};

//---------------------------------------------------------------------------//
template<class PhysD, class TopoD, class ArrayQ_>
struct FieldBundle_DGAdvective :
    public FieldBundleBase_DGAdvective<PhysD, TopoD, ArrayQ_>
{
  typedef FieldBundleBase_DGAdvective<PhysD, TopoD, ArrayQ_> BaseType;

  typedef PhysD PhysDim;
  typedef TopoD TopoDim;
  typedef ArrayQ_ ArrayQ;

  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef FieldBundle_DGAdvective_Local<PhysD,TopoD,ArrayQ> FieldBundle_Local;

  FieldBundle_DGAdvective( const XField<PhysDim, TopoDim>& xfld, const int order,
                           const BasisFunctionCategory basis_cell, const BasisFunctionCategory basis_trace,
                           const std::vector<int>& active_BGroup_list )
  : BaseType(xfld,qfld_,lgfld_,order, basis_cell,basis_trace),
    qfld_ ( xfld, order  , basis_cell ),
    lgfld_( xfld, std::max(order-1,0), basis_trace, active_BGroup_list )
  {
  }


  // A pseudo copy constructor, hides the need for passing discretization specific extra like stab
  FieldBundle_DGAdvective( const XField<PhysDim,TopoDim>&xfld, const int order,
                           const BaseType& flds,
                           const std::vector<int>& active_BGroup_list )
  : BaseType( xfld, qfld_, lgfld_, order, flds.basis_cell, flds.basis_trace ),
    qfld_( xfld, order, basis_cell),
    lgfld_( xfld, order-1, flds.basis_trace, active_BGroup_list )
  {
  }

  using BaseType::order;
  using BaseType::basis_cell;
  using BaseType::basis_trace;

protected:
  QFieldType qfld_;
  LGFieldType lgfld_;
};

} // namespace SANS

#endif /* FIELDBUNDLE_DGADVECTIVE_H_ */
