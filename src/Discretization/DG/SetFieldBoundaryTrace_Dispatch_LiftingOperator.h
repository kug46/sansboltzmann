// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDOUNDARYTRACE_DISPATCH_LIFTINGOPERATOR_H
#define SETFIELDOUNDARYTRACE_DISPATCH_LIFTINGOPERATOR_H

// boundary-trace integral residual functions

#include "SetFieldBoundaryTrace_DGBR2_LiftingOperator.h"

#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{
#if 0
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace (e.g. with Lagrange multipliers)
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
class SetFieldBoundaryTrace_FieldTrace_Dispatch_LiftingOperator_impl
{
public:
  SetFieldBoundaryTrace_FieldTrace_Dispatch_LiftingOperator_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), lgfld_(lgfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Can't compute lifting operators with mitLG BC's");
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
SetFieldBoundaryTrace_FieldTrace_Dispatch_LiftingOperator_impl<XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>
SetFieldBoundaryTrace_FieldTrace_Dispatch_LiftingOperator(const XFieldType& xfld,
                                                          const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                          const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                          const int* quadratureorder, int ngroup )
{
  return SetFieldBoundaryTrace_FieldTrace_Dispatch_LiftingOperator_impl<XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
      xfld, qfld, rfld, lgfld, quadratureorder, ngroup);
}
#endif
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without field trace
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
class SetFieldBoundaryTrace_Dispatch_LiftingOperator_impl
{
public:
  SetFieldBoundaryTrace_Dispatch_LiftingOperator_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const FieldDataInvMassMatrix_Cell& mmfld,
      const int* quadratureorder, int ngroup )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), mmfld_(mmfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        SetFieldBoundaryTrace_DGBR2_LiftingOperator(fcn, mmfld_),
        xfld_, (qfld_, rfld_), quadratureorder_, ngroup_);
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
SetFieldBoundaryTrace_Dispatch_LiftingOperator_impl<XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>
SetFieldBoundaryTrace_Dispatch_LiftingOperator(const XFieldType& xfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                               const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                               const FieldDataInvMassMatrix_Cell& mmfld,
                                               const int* quadratureorder, int ngroup )
{
  return {xfld, qfld, rfld, mmfld, quadratureorder, ngroup};
}

}

#endif //SETFIELDOUNDARYTRACE_DISPATCH_LIFTINGOPERATOR_H
