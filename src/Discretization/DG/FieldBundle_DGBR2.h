// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FIELDBUNDLE_DGBR2_H_
#define FIELDBUNDLE_DGBR2_H_

#include "Field/Field.h"
#include "Field/FieldTypes.h"
#include "Field/FieldLift.h"

#include "Field/Local/Field_Local.h"
#include "Field/Local/FieldLift_Local.h"

#include "Discretization/DG/DiscretizationDGBR2.h"

namespace SANS
{

template< class PhysD, class TopoD, class ArrayQ_>
struct FieldBundleBase_DGBR2
{
  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef ArrayQ_ ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  FieldBundleBase_DGBR2( const XField<PhysDim, TopoDim>& xfld,
                         Field_DG_Cell< PhysDim, TopoDim, ArrayQ >& qfld,
                         FieldLift_DG_Cell< PhysDim, TopoDim, VectorArrayQ >& rfld,
                         Field_DG_BoundaryTrace< PhysDim, TopoDim, ArrayQ >& lgfld,
                         const int order,
                         const BasisFunctionCategory basis_cell,
                         const BasisFunctionCategory basis_trace) :
  xfld(xfld), qfld(qfld), rfld(rfld), lgfld(lgfld), order(order), basis_cell(basis_cell), basis_trace(basis_trace) {}

  void projectTo(FieldBundleBase_DGBR2& bundleTo)
  {
    qfld.projectTo(bundleTo.qfld);
    rfld.projectTo(bundleTo.rfld);
    lgfld.projectTo(bundleTo.lgfld);
  }

  const XField<PhysDim,TopoDim>& xfld;

  Field_DG_Cell<PhysDim,TopoDim,ArrayQ>& qfld;
  FieldLift_DG_Cell<PhysDim,TopoDim,VectorArrayQ>& rfld;
  Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ>& lgfld;

  const int order;

  const BasisFunctionCategory basis_cell;
  const BasisFunctionCategory basis_trace;

  static constexpr SpaceType spaceType = SpaceType::Discontinuous;

};

// Forward declare so input args for local work
template< class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_DGBR2;

// public of GlobalBundle is so that AlgebraicEquationSet Basetype works with it
template<class PhysD, class TopoD, class ArrayQ_ >
struct FieldBundle_DGBR2_Local : public FieldBundleBase_DGBR2<PhysD, TopoD, ArrayQ_>
{
  typedef FieldBundleBase_DGBR2<PhysD, TopoD, ArrayQ_> BaseType;

  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef ArrayQ_ ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ> AuxFieldType;
  typedef Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef Field_Local<QFieldType> QFieldType_Local;
  typedef FieldLift_Local<AuxFieldType> AuxFieldType_Local;
  typedef Field_Local<LGFieldType> LGFieldType_Local;

  // active_local_BGroup_list is a vec of vec so that boundaries that came from the same original global trace
  // continue to have shared dofs in the local field. This is necessary in CG
  FieldBundle_DGBR2_Local(const XField_Local_Base<PhysDim, TopoDim>& xfld_local,
                          const FieldBundle_DGBR2<PhysDim, TopoDim, ArrayQ>& globalfields,
                          const int order, const std::vector<std::vector<int>>& active_local_BGroup_list )
  : BaseType(xfld_local,qfld_,rfld_,lgfld_,order, globalfields.basis_cell, globalfields.basis_trace),
    qfld_ ( xfld_local, globalfields.qfld , order  , globalfields.basis_cell ),
    rfld_ ( xfld_local, globalfields.rfld , order  , globalfields.basis_cell ),
    lgfld_( active_local_BGroup_list, xfld_local, globalfields.lgfld, order-1, globalfields.basis_trace )
  {
  }

  using BaseType::order;
  using BaseType::basis_cell;
  using BaseType::basis_trace;

protected:
  QFieldType_Local qfld_;
  AuxFieldType_Local rfld_;
  LGFieldType_Local lgfld_;
};

template<class PhysD, class TopoD, class ArrayQ_>
struct FieldBundle_DGBR2 : public FieldBundleBase_DGBR2<PhysD, TopoD, ArrayQ_>
{
  typedef FieldBundleBase_DGBR2<PhysD, TopoD, ArrayQ_> BaseType;

  typedef PhysD PhysDim;
  typedef TopoD TopoDim;

  typedef ArrayQ_ ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;

  typedef Field_DG_Cell<PhysDim, TopoDim, ArrayQ> QFieldType;
  typedef FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ> AuxFieldType;
  typedef Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> LGFieldType;

  typedef FieldBundle_DGBR2_Local<PhysD,TopoD,ArrayQ_> FieldBundle_Local;

  FieldBundle_DGBR2(const XField<PhysDim, TopoDim>& xfld, const int order,
                    const BasisFunctionCategory basis_cell, const BasisFunctionCategory basis_trace,
                    const std::vector<int>& active_BGroup_list)
  : BaseType(xfld,qfld_,rfld_,lgfld_,order,basis_cell,basis_trace),
    qfld_( xfld, order, basis_cell ),
    rfld_( xfld, order, basis_cell ),
    lgfld_( xfld, order-1, basis_trace, active_BGroup_list )
  {
  }

  // A pseudo copy constructor, hides the need for passing discretization specific extras like stab
  FieldBundle_DGBR2( const XField<PhysDim,TopoDim>&xfld, const int order,
                     const BaseType& flds,
                     const std::vector<int>& active_BGroup_list )
  : BaseType(xfld, qfld_, rfld_, lgfld_, order,flds.basis_cell, flds.basis_trace ),
    qfld_( xfld, order, flds.basis_cell),
    rfld_( xfld, order, flds.basis_cell ),
    lgfld_( xfld, order-1, flds.basis_trace, active_BGroup_list )
  {
  }

  using BaseType::order;
  using BaseType::basis_cell;
  using BaseType::basis_trace;

protected:
  QFieldType qfld_;
  AuxFieldType rfld_;
  LGFieldType lgfld_;
};


}

#endif /* FIELDBUNDLE_DGBR2_H_ */
