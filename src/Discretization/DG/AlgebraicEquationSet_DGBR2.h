// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_DGBR2_H
#define ALGEBRAICEQUATIONSET_DGBR2_H

#include <vector>
#include <map>
#include <string>

#include "tools/SANSnumerics.h"

#include "pde/BCParameters.h"

#include "Field/FieldTypes.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "Discretization/QuadratureOrder.h"
#include "Discretization/ResidualNormType.h"

#include "Discretization/DG/Integrand_DGBR2_fwd.h"
#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"

#include "Discretization/DG/FieldBundle_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2.h"
#include "Discretization/DG/IntegrandCell_DGBR2_manifold.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2.h"
#include "Discretization/DG/IntegrandInteriorTrace_DGBR2_manifold.h"
#include "Discretization/DG/IntegrateBoundaryTrace_Dispatch_DGBR2.h"
#include "Discretization/DG/SolutionData_DGBR2.h"

//#include "ErrorEstimate/DG/ErrorEstimate_DGBR2.h"
#include "ErrorEstimate/ErrorEstimate_fwd.h"

namespace SANS
{

// Discretization (specifically, integrands) identification tag
class DGBR2;
class DGBR2_manifold;

template<class DiscTag>
struct DGBR2Integrands;

template<>
struct DGBR2Integrands<DGBR2>
{
  template <class PDE>
  using IntegrandCell = IntegrandCell_DGBR2<PDE>;

  template <class PDE>
  using IntegrandInteriorTrace = IntegrandInteriorTrace_DGBR2<PDE>;
};

template<>
struct DGBR2Integrands<DGBR2_manifold>
{
  template <class PDE>
  using IntegrandCell = IntegrandCell_DGBR2_manifold<PDE>;

  template <class PDE>
  using IntegrandInteriorTrace = IntegrandInteriorTrace_DGBR2_manifold<PDE>;
};

// refers to src/Discretization/DiscretizationBCTag.h
template<> struct DiscBCTag<BCCategory::Dirichlet_mitLG, DGBR2>     { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::Dirichlet_sansLG, DGBR2>    { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_mitLG, DGBR2>  { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG, DGBR2> { typedef DGBR2 type; };
template<> struct DiscBCTag<BCCategory::Flux_mitState, DGBR2>       { typedef DGBR2 type; };
template<> struct DiscBCTag<BCCategory::None, DGBR2>                { typedef Galerkin type; };

#if 1 //TODO: not implemented yet but are here for compilation to succeed
template<> struct DiscBCTag<BCCategory::Dirichlet_sansLG, DGBR2_manifold>    { typedef Galerkin_manifold type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_mitLG, DGBR2_manifold>  { typedef Galerkin_manifold type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG, DGBR2_manifold> { typedef Galerkin_manifold type; };
#endif
template<> struct DiscBCTag<BCCategory::Flux_mitState, DGBR2_manifold>       { typedef DGBR2_manifold type; };
template<> struct DiscBCTag<BCCategory::None, DGBR2_manifold>                { typedef Galerkin_manifold type; };

//----------------------------------------------------------------------------//
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
class AlgebraicEquationSet_DGBR2 : public AlgebraicEquationSet_Debug<NDPDEClass_, Traits>
{
public:
  typedef NDPDEClass_ NDPDEClass;
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef Traits TraitsTag;
  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef BCParameters<BCVector> BCParams;

  typedef typename DGBR2Integrands<DiscTag>::template IntegrandCell<NDPDEClass> IntegrandCellClass;
  typedef typename DGBR2Integrands<DiscTag>::template IntegrandInteriorTrace<NDPDEClass> IntegrandTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_DGBR2<NDPDEClass, BCNDConvert, BCVector, DiscTag> IntegrateBoundaryTrace_DispatchClass;

  // Base must be used in constructor so that Local can use the BaseType Constructor
  // The global bundle must also be visible, so that SolverInterface can find it
  typedef FieldBundleBase_DGBR2<PhysDim,TopoDim,ArrayQ> FieldBundleBase;
  typedef FieldBundle_DGBR2<PhysDim,TopoDim,ArrayQ> FieldBundle;
  typedef FieldBundle_DGBR2_Local<PhysDim,TopoDim,ArrayQ> FieldBundle_Local;

  typedef ErrorEstimate_DGBR2<NDPDEClass, BCNDConvert, BCVector, XFieldType> ErrorEstimateClass;

  template<class ParamBuilderType>
  using SolutionDataClass = SolutionData_DGBR2<PhysDim, TopoDim, NDPDEClass, ParamBuilderType>;

  template< class... BCArgs >
  AlgebraicEquationSet_DGBR2(const XFieldType&                                  xfld,
                             Field_DG_Cell<PhysDim, TopoDim, ArrayQ>&           qfld,
                             FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld,
                             Field<PhysDim, TopoDim, ArrayQ>&                   lgfld,
                             const NDPDEClass& pde,
                             const DiscretizationDGBR2& disc,
                             const QuadratureOrder& quadratureOrder,
                             const ResidualNormType& resNormType,
                             const std::vector<Real>& tol,
                             const std::vector<int>& CellGroups,
                             const std::vector<int>& InteriorTraceGroups,
                             PyDict& BCList,
                             const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                             BCArgs&... args);

  //Constructor for passing in a lifted quantity field (i.e. for shock-capturing)
  template< class... BCArgs >
  AlgebraicEquationSet_DGBR2( const XFieldType& xfld,
                              Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                              FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld,
                              Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                              std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                              const NDPDEClass& pde,
                              const DiscretizationDGBR2& disc,
                              const QuadratureOrder& quadratureOrder,
                              const ResidualNormType& resNormType,
                              const std::vector<Real>& tol,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& InteriorTraceGroups,
                              PyDict& BCList,
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                              BCArgs&... args );

  // Field Bundle constructor for use in hiding the specific AlgEqSet in SolverInterface
  template< class... BCArgs >
  AlgebraicEquationSet_DGBR2( const XFieldType& xfld,
                              FieldBundleBase& flds,
                              std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld,
                              const NDPDEClass& pde,
                              const DiscretizationDGBR2& disc,
                              const QuadratureOrder& quadratureOrder,
                              const ResidualNormType& resNormType,
                              const std::vector<Real>& tol,
                              const std::vector<int>& CellGroups,
                              const std::vector<int>& InteriorTraceGroups,
                              PyDict& BCList,
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                              BCArgs&... args );

  virtual ~AlgebraicEquationSet_DGBR2();

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz) override;

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override;
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override;

  // Used to compute jacobians wrt. parameters
  template<int iParam, class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override;

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Sets the primal adjoint field and computes the lifting operator adjoint
  void setAdjointField(const SystemVectorView& adj,
                       Field_DG_Cell<PhysDim, TopoDim, ArrayQ>&           wfld,
                       FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& sfld,
                       Field<PhysDim, TopoDim, ArrayQ>&                   mufld );

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;
  void fillSystemVector( SystemVectorView& q,
                         const Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                         const Field<PhysDim, TopoDim, ArrayQ>& lgfld) const;

  // Returns the vector and matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // update fraction needed for physically valid state
  virtual Real updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const override;

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Add an AES to this one to daisy-chain
  virtual void addAlgebraicEquationSet(std::shared_ptr<BaseType> AES);

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override;

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override;

  virtual void syncDOFs_MPI() override;

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  virtual void dumpSolution(const std::string& filename) const override
  {
    output_Tecplot(qfld_, filename);
  }

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iBC = 1;
  static const int iq = 0;
  static const int ilg = 1;

  const IntegrateBoundaryTrace_DispatchClass& dispatchBC() const { return dispatchBC_; }

  const QuadratureOrder& quadratureOrder() const { return quadratureOrder_; }
  const QuadratureOrder& quadratureOrderMin() const { return quadratureOrderMin_; }

protected:
  template<class SparseMatrixType>
  void jacobian( SparseMatrixType mtx, const QuadratureOrder& quadratureOrder,
                 const std::vector<int>& interiorTraceCellJac = {} );

  void computeLiftingOperators();

  // Alias to be consistent with DGAdvective
  void computeSecondaryFields() { computeLiftingOperators(); }

  IntegrandCellClass fcnCell_;
  IntegrandTraceClass fcnTrace_;
  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_DispatchClass dispatchBC_;

  const XFieldType& xfld_;
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  FieldLift_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const FieldDataInvMassMatrix_Cell mmfld_; // Inverse mass matrix field
  const NDPDEClass& pde_;
  QuadratureOrder quadratureOrder_;
  QuadratureOrder quadratureOrderMin_;
  const ResidualNormType resNormType_;
  const std::vector<Real> tol_;
  std::shared_ptr<BaseType> AES_;

  std::shared_ptr<Field_DG_Cell<PhysDim, TopoDim, Real>> pLiftedQuantityfld_; //used to store the sensor field in shock-capturing
  std::shared_ptr<FieldDataInvMassMatrix_Cell> pmmfldLiftedQuantity_; //mass matrix of the above lifted quantity field
};


} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_DGBR2_H
