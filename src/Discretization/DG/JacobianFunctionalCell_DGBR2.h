// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALCELL_DGBR2_H
#define JACOBIANFUNCTIONALCELL_DGBR2_H

// Cell integral functional jacobian functions for DGBR2

#include "tools/Tuple.h"

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/XField.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/tools/Surrealize.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group functional jacobian
//

template<class Surreal, class IntegrandCell, template<class> class Vector>
class JacobianFunctionalCell_DGBR2_impl :
    public GroupIntegralCellType< JacobianFunctionalCell_DGBR2_impl<Surreal, IntegrandCell, Vector> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename IntegrandCell::template ArrayJ<Real> ArrayJ;
  typedef typename IntegrandCell::template MatrixJ<Real> MatrixJ;
  typedef typename IntegrandCell::template ArrayJ<Surreal> ArrayJSurreal;

  typedef typename IntegrandCell::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  // Save off the cell integrand and the residual vector
  JacobianFunctionalCell_DGBR2_impl( const IntegrandCell& fcn,
                                     Vector<MatrixJ>& jacFunctional) :
    fcn_(fcn), jacFunctional_(jacFunctional), comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    SANS_ASSERT(qfld.nDOFpossessed() == jacFunctional_.m());

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, FieldLift<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field    < PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename FieldLift< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> RFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;
    typedef ElementLift<VectorArrayQSurreal, TopoDim, Topology> ElementRFieldSurrealClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const RFieldCellGroupType& rfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldSurrealClass qfldElemSurreal( qfldCell.basis() );
    ElementRFieldSurrealClass rfldElemsSurreal( rfldCell.basis() );

    // DOFs per element
    const int nDOF = qfldElemSurreal.nDOF();

    // total number of entries in ArrayQ
    const int nVar = DLA::VectorSize<ArrayQ>::M;

    // total number of outputs in the functional
    const int nOut = DLA::VectorSize<ArrayJ>::M;

    // element functional jacobian vector
    DLA::VectorD<MatrixJ> jacFunctionalElem(nDOF);

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal(nDOF, -1);

    // element integral
    ElementIntegral<TopoDim, Topology, ArrayJSurreal> integral(quadratureorder);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N; //DLA::index(qfldElemSurreal.DOF(0),0).size();

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == rfldCell.nElem() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      qfldCell.getElement( qfldElemSurreal, elem );

      // only need to perform the volume integral on processors that possess the element
      if ( qfldElemSurreal.rank() != comm_rank_ ) continue;

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      rfldCell.getElement( rfldElemsSurreal, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*nDOF; nchunk += nDeriv)
      {
        // associate derivative slots with solution variables
        int slot;
        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemSurreal.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // cell integration for canonical element
        ArrayJSurreal functional = 0;
        integral( fcn_.integrand(xfldElem, qfldElemSurreal, rfldElemsSurreal), get<-1>(xfldElem), functional );

        // accumulate derivatives into element jacobian
        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemSurreal.DOF(j), n).deriv(slot - nchunk) = 0; // reset the derivative

              // Note that n,m are transposed intentionally
              for (int m = 0; m < nOut; m++)
                DLA::index(jacFunctionalElem[j],n,m) = DLA::index(functional,m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // Get the local-to-global mapping to scatter the jacobian
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nDOF );

      // scatter-add element jacobian to global
      for (int i = 0; i < nDOF; i++)
        jacFunctional_[mapDOFGlobal[i]] += jacFunctionalElem[i];
    }
  }

protected:
  const IntegrandCell& fcn_;
  Vector<MatrixJ>& jacFunctional_;
  mutable int comm_rank_;
};

// Factory function

template<class Surreal, class IntegrandCell, template<class> class Vector, class MatrixJ>
JacobianFunctionalCell_DGBR2_impl<Surreal, IntegrandCell, Vector>
JacobianFunctionalCell_DGBR2( const IntegrandCellType<IntegrandCell>& fcn,
                              Vector< MatrixJ >& functional )
{
  typedef typename Scalar<MatrixJ>::type T;
  static_assert( std::is_same<typename IntegrandCell::template MatrixJ<T>, MatrixJ>::value, "These should be the same");
  return {fcn.cast(), functional};
}

} // namespace SANS

#endif  // JACOBIANFUNCTIONALCELL_DGBR2_H
