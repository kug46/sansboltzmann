// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#if !defined(ALGEBRAICEQUATIONSET_LOCAL_DG_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "Discretization/DG/AlgebraicEquationSet_Local_DG.h"

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Field/FieldTypes.h"
#include "Field/XField.h"

#include "Field/tools/for_each_CellGroup.h"

#include "Discretization/DG/JacobianDetInvResidualNorm.h"

//#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"
namespace SANS
{

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
void
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::
init()
{
  //DOF counts for sub-system
  const int main_group = 0;
  sub_qfld_nDOF_ = qfld_.nDOFCellGroup(main_group); //Re-solving for only main-cell q DOFs in local mesh

  sub_lgfld_nDOF_ = 0; //Re-solving for only btrace DOFs of main-cells in the local mesh
  for (int i = 0; i < lgfld_.nBoundaryTraceGroups(); i++)
    sub_lgfld_nDOF_ += lgfld_.nDOFBoundaryTraceGroup(i);
}

//Computes the sub-residual for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
void
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::
residual(SystemVectorView& rsd)
{
  //Compute the full residual vector of the local problem
  SystemVector rsd_full(BaseType::vectorEqSize());
  rsd_full = 0;
  BaseType::residual(rsd_full);

  //Extract the sub residual vector from the full residual, corresponding to the equations that need to be solved
  subResidual(rsd_full, rsd);
}

//Computes the sub-Jacobian for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
void
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::
jacobian(SystemMatrixView& jac)
{
  //Compute the full Jacobian of the local problem
  SystemMatrix jac_full(BaseType::matrixSize());
  jac_full = 0;
  BaseType::template jacobian<SystemMatrix&>(jac_full, quadratureOrder_, {0});

  //Extract the sub Jacobian from the full Jacobian, containing only the equations that need to be solved
  subJacobian<SystemMatrixView>(jac_full, jac);

//  SLA::WriteMatrixMarketFile(jac(0,0), "tmp/jac_00.mtx");
//  SLA::WriteMatrixMarketFile(jac(0,1), "tmp/jac_01.mtx");
//  SLA::WriteMatrixMarketFile(jac(1,0), "tmp/jac_10.mtx");
//  SLA::WriteMatrixMarketFile(jac(1,1), "tmp/jac_11.mtx");
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
void
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::
subResidual(const SystemVectorView& rsd_full, SystemVectorView& rsd_sub)
{
  //Dimension-checks
  SANS_ASSERT( rsd_sub.m() == 2 );
  SANS_ASSERT( sub_qfld_nDOF_  == rsd_sub[iPDE].m() );
  SANS_ASSERT( sub_lgfld_nDOF_ == rsd_sub[iBC].m() );

  for (int i=0; i<sub_qfld_nDOF_; i++)
    rsd_sub[iPDE][i] = rsd_full[iPDE][i];

  for (int i=0; i<sub_lgfld_nDOF_; i++)
    rsd_sub[iBC][i] = rsd_full[iBC][i];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::
subJacobian(const SparseMatrixType& jac_full, SparseMatrixType& jac_sub)
{
  //Dimension-checks
  SANS_ASSERT( jac_sub.m() == 2 );
  SANS_ASSERT( jac_sub.n() == 2 );

  for (int i = 0; i < 2; i++)
  {
    SANS_ASSERT( sub_qfld_nDOF_  == jac_sub(iPDE,i).m() );
    SANS_ASSERT( sub_lgfld_nDOF_ == jac_sub(iBC,i).m());

    SANS_ASSERT( sub_qfld_nDOF_  == jac_sub(i,iq).n() );
    SANS_ASSERT( sub_lgfld_nDOF_ == jac_sub(i,ilg).n());
  }

  for (int i=0; i<sub_qfld_nDOF_; i++)
  {
    for (int j=0; j<sub_qfld_nDOF_; j++)
      jac_sub(iPDE,iq)(i,j) = jac_full(iPDE,iq)(i,j);

    for (int j=0; j<sub_lgfld_nDOF_; j++)
      jac_sub(iPDE,ilg)(i,j) = jac_full(iPDE,ilg)(i,j);
  }

  for (int i=0; i<sub_lgfld_nDOF_; i++)
  {
    for (int j=0; j<sub_qfld_nDOF_; j++)
      jac_sub(iBC,iq)(i,j) = jac_full(iBC,iq)(i,j);

    for (int j=0; j<sub_lgfld_nDOF_; j++)
      jac_sub(iBC,ilg)(i,j) = jac_full(iBC,ilg)(i,j);
  }
}

//Evaluate Residual Norm
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
std::vector<std::vector<Real>>
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::
residualNorm( const SystemVectorView& rsd ) const
{
  const int nMon = this->pde_.nMonitor();

  DLA::VectorD<Real> rsdPDEtmp(nMon);
  DLA::VectorD<Real> rsdBCtmp(nMon);

  rsdPDEtmp = 0;
  rsdBCtmp = 0;

  std::vector<std::vector<Real>> rsdNorm(this->nResidNorm(), std::vector<Real>(nMon, 0));
  std::vector<KahanSum<Real>> rsdNormKahan(nMon, 0);

  //PDE residual norm
  if (resNormType_ == ResidualNorm_L2 || resNormType_ == ResidualNorm_L2_DOFWeighted)
  {
    for (int n = 0; n < sub_qfld_nDOF_; n++)
    {
      this->pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNormKahan[j] += pow(rsdPDEtmp[j],2);
    }

    if (resNormType_ == ResidualNorm_L2)
    {
      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] = sqrt(rsdNormKahan[j]);
    }
    else
    {
      int qDOF = qfld_.nDOFnative();
      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] = sqrt(rsdNormKahan[j]/qDOF);
    }
  }
  else if (resNormType_ == ResidualNorm_InvJac_DOF_Weighted)
  {
    // compute residual norms weighted by the elemental jacobian determinant
    std::vector<int> cellgroups = {0};
    Real invJ = 0;
    int nDOF = sub_qfld_nDOF_;
    for_each_CellGroup<TopoDim>::apply(
        JacobianDetInvResidualNorm(qfld_.comm()->rank(), cellgroups, this->pde_, rsd[iPDE], rsdNormKahan, invJ),
        (get<-1>(xfld_), qfld_) );

    //Normalize by number of DOF
    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNormKahan[j] / nDOF);
  }
  else if (resNormType_ == ResidualNorm_InvJac_RelWeighted)
  {
    // compute residual norms weighted by the elemental jacobian determinant
    std::vector<int> cellgroups = {0};
    Real invJ = 0;
    for_each_CellGroup<TopoDim>::apply(
        JacobianDetInvResidualNorm(qfld_.comm()->rank(), cellgroups, this->pde_, rsd[iPDE], rsdNormKahan, invJ),
        (get<-1>(xfld_), qfld_) );

    //Normalize by sum of inverse jacobian determinants
    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNormKahan[j]) / invJ;
  }
  else
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Local_DG::residualNorm - Unknown residual norm type!");

  //BC residual norm
  for (int n = 0; n < sub_lgfld_nDOF_; n++)
  {
    this->pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

  return rsdNorm;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
typename AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::VectorSizeClass
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::vectorEqSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  return { sub_qfld_nDOF_,
           sub_lgfld_nDOF_ };
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
typename AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::VectorSizeClass
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::vectorStateSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  return { sub_qfld_nDOF_,
           sub_lgfld_nDOF_ };
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
typename AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::MatrixSizeClass
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");
  static_assert(iq == 0,"");
  static_assert(ilg == 1,"");

  // jacobian nonzero pattern
  //
  //           q  lg
  //   PDE     X   X
  //   BC      X   0

  return {{ { sub_qfld_nDOF_ , sub_qfld_nDOF_}, { sub_qfld_nDOF_ , sub_lgfld_nDOF_} },
          { { sub_lgfld_nDOF_, sub_qfld_nDOF_}, { sub_lgfld_nDOF_, sub_lgfld_nDOF_} }};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
typename AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::VectorSizeClass
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::
fullVectorSize() const
{
  return BaseType::vectorEqSize();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
void
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType,AlgebraicEquationSetBaseType>::
setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  SANS_ASSERT( sub_qfld_nDOF_ == q[iq].m() );
  for (int k = 0; k < sub_qfld_nDOF_; k++)
    qfld_.DOF(k) = q[iq][k];

  SANS_ASSERT( sub_lgfld_nDOF_ == q[ilg].m() );
  for (int k = 0; k < sub_lgfld_nDOF_; k++)
    lgfld_.DOF(k) = q[ilg][k];

  //Update any secondary field, e.g. lifting operator field for DGBR2
  this->computeSecondaryFields();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class DiscTag, class XFieldType,
         template<class, template<class,class> class, class, class, class, class> class AlgebraicEquationSetBaseType>
void
AlgebraicEquationSet_Local_DG<NDPDEClass, BCNDConvert, BCVector, DiscTag, XFieldType, AlgebraicEquationSetBaseType>::
fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  SANS_ASSERT( sub_qfld_nDOF_ == q[iq].m() );
  for (int k = 0; k < sub_qfld_nDOF_; k++)
    q[iq][k] = qfld_.DOF(k);

  SANS_ASSERT( sub_lgfld_nDOF_ == q[ilg].m() );
  for (int k = 0; k < sub_lgfld_nDOF_; k++)
    q[ilg][k] = lgfld_.DOF(k);
}

} //namespace SANS

#include <boost/preprocessor/cat.hpp>

//---------------------------------------------------------------------------//
// Helper macros to reduce the amount of work needed to instantiate AlgebraicEquationSet_Local_DGBR2
//---------------------------------------------------------------------------//
// This is the main macro used for steady instantiation
#define ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACE( PDE, BCVECTOR, DISCTAG, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpace<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpace); \
\
template class AlgebraicEquationSet_Local_DG< BOOST_PP_CAT(PDE, NDSpace), BCNDConvertSpace, \
                                              BCVECTOR, DISCTAG, PARAMFIELDTUPLE, AlgebraicEquationSet_DGBR2 >;

//---------------------------------------------------------------------------//
// This is the main macro used for space-time instantiation
#define ALGEBRAICEQUATIONSET_LOCAL_DGBR2_INSTANTIATE_SPACETIME( PDE, BCVECTOR, DISCTAG, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpaceTime<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpaceTime); \
\
template class AlgebraicEquationSet_Local_DG< BOOST_PP_CAT(PDE, NDSpaceTime), BCNDConvertSpaceTime, \
                                              BCVECTOR, DISCTAG, PARAMFIELDTUPLE, AlgebraicEquationSet_DGBR2 >;



//---------------------------------------------------------------------------//
// Helper macros to reduce the amount of work needed to instantiate AlgebraicEquationSet_Local_DGAdvective
//---------------------------------------------------------------------------//
// This is the main macro used for steady instantiation
#define ALGEBRAICEQUATIONSET_LOCAL_DGADVECTIVE_INSTANTIATE_SPACE( PDE, BCVECTOR, DISCTAG, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpace<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpace); \
\
template class AlgebraicEquationSet_Local_DG< BOOST_PP_CAT(PDE, NDSpace), BCNDConvertSpace, \
                                              BCVECTOR, DISCTAG, PARAMFIELDTUPLE, AlgebraicEquationSet_DGAdvective >;

//---------------------------------------------------------------------------//
// This is the main macro used for space-time instantiation
#define ALGEBRAICEQUATIONSET_LOCAL_DGADVECTIVE_INSTANTIATE_SPACETIME( PDE, BCVECTOR, DISCTAG, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpaceTime<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpaceTime); \
\
template class AlgebraicEquationSet_Local_DG< BOOST_PP_CAT(PDE, NDSpaceTime), BCNDConvertSpaceTime, \
                                              BCVECTOR, DISCTAG, PARAMFIELDTUPLE, AlgebraicEquationSet_DGAdvective >;
