// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_DGBR2_ELEMENT_H
#define JACOBIANINTERIORTRACE_DGBR2_ELEMENT_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Discretization/JacobianElementMatrix.h"

namespace SANS
{


template<class PhysDim, class MatrixQ>
struct JacobianElemInteriorTrace_DGBR2_PDE : JacElemMatrixType< JacobianElemInteriorTrace_DGBR2_PDE<PhysDim,MatrixQ> >
{
  // PDE Jacobian wrt q
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Lifting Operator (r) wrt q
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;
  typedef DLA::MatrixD<VectorX> MatrixLOElemClass;

  JacobianElemInteriorTrace_DGBR2_PDE(const JacobianElemInteriorTraceSize& size)
   : nTest(size.nTest),
     nDOFL(size.nDOFL), nDOFR(size.nDOFR),
     PDE_qL(size.nTest, size.nDOFL),
     PDE_qR(size.nTest, size.nDOFR),
     r_qL(size.nTest, size.nDOFL),
     r_qR(size.nTest, size.nDOFR)
  {}

  const int nTest;
  const int nDOFL;
  const int nDOFR;

  // element PDE jacobian matrices wrt q
  MatrixElemClass PDE_qL;
  MatrixElemClass PDE_qR;

  // element Lifting Operator (r) jacobian matrices wrt q
  MatrixLOElemClass r_qL;
  MatrixLOElemClass r_qR;

  inline Real operator=( const Real s )
  {
    PDE_qL = s;
    PDE_qR = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemInteriorTrace_DGBR2_PDE& operator+=(
      const JacElemMulScalar< JacobianElemInteriorTrace_DGBR2_PDE >& mul )
  {
    PDE_qL += mul.s*mul.mtx.PDE_qL;
    PDE_qR += mul.s*mul.mtx.PDE_qR;

    return *this;
  }
};

template<class PhysDim>
struct JacobianElemInteriorTrace_DGBR2_LO : JacElemMatrixType< JacobianElemInteriorTrace_DGBR2_LO<PhysDim> >
{
  // Lifting Operator Equation (LO) wrt q
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;
  typedef DLA::MatrixD<VectorX> MatrixLOElemClass;

  JacobianElemInteriorTrace_DGBR2_LO(const JacobianElemInteriorTraceSize& size)
   : nTest(size.nTest),
     nDOFL(size.nDOFL), nDOFR(size.nDOFR),
     _qL(size.nTest, size.nDOFL),
     _qR(size.nTest, size.nDOFR)
  {}

  const int nTest;
  const int nDOFL;
  const int nDOFR;

  // element LO jacobian matrices wrt q
  MatrixLOElemClass _qL;
  MatrixLOElemClass _qR;

  inline Real operator=( const Real s )
  {
    _qL = s;
    _qR = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemInteriorTrace_DGBR2_LO& operator+=(
      const JacElemMulScalar< JacobianElemInteriorTrace_DGBR2_LO >& mul )
  {
    _qL += mul.s*mul.mtx._qL;
    _qR += mul.s*mul.mtx._qR;

    return *this;
  }
};

template<class PhysDim, class MatrixQ>
struct JacobianElemInteriorTrace_DGBR2_PDE_RT : JacElemMatrixType< JacobianElemInteriorTrace_DGBR2_PDE_RT<PhysDim,MatrixQ> >
{
  // Transposed PDE jacobian wrt r
  typedef DLA::VectorS<PhysDim::D,MatrixQ> VectorMatrixQ;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixTLOElemClass;

  JacobianElemInteriorTrace_DGBR2_PDE_RT(const JacobianElemInteriorTraceSize& size)
   : nTest(size.nTest),
     nDOFL(size.nDOFL), nDOFR(size.nDOFR),
     PDET_rL(size.nDOFL, size.nTest), // NOT: transposed
     PDET_rR(size.nDOFR, size.nTest)
  {}

  const int nTest;
  const int nDOFL;
  const int nDOFR;

  // element transposed PDE jacobian matrices wrt r
  MatrixTLOElemClass PDET_rL;
  MatrixTLOElemClass PDET_rR;

  inline Real operator=( const Real s )
  {
    PDET_rL = s;
    PDET_rR = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemInteriorTrace_DGBR2_PDE_RT& operator+=(
      const JacElemMulScalar< JacobianElemInteriorTrace_DGBR2_PDE_RT >& mul )
  {
    PDET_rL += mul.s*mul.mtx.PDET_rL;
    PDET_rR += mul.s*mul.mtx.PDET_rR;

    return *this;
  }
};

}
#endif // JACOBIANINTERIORTRACE_DGBR2_ELEMENT_H
