// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_DGBR2_LIFTEDQUANTITY_H
#define JACOBIANINTERIORTRACE_DGBR2_LIFTEDQUANTITY_H

// interior-trace integral jacobian functions

#include <algorithm> // find

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "DiscretizationDGBR2.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/MatrixLiftedQuantity.h"
#include "JacobianInteriorTrace_DGBR2_Element.h"

namespace SANS
{

#if 1
//----------------------------------------------------------------------------//
// DG BR2 interior trace integral Jacobian
//

template<class IntegrandInteriorTrace>
class JacobianInteriorTrace_DGBR2_LiftedQuantity_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_DGBR2_LiftedQuantity_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;

  typedef Real LiftedT; //data-type for lifted quantity
  typedef typename MatrixLiftedQuantity<MatrixQ>::type MatrixT; //for PDE jacobian wrt lifted quantity
  typedef typename MatrixLiftedQuantity<MatrixQ>::transpose_type MatrixTT; //for lifted quantity jacobian wrt q

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianInteriorTrace_DGBR2_LiftedQuantity_impl(const IntegrandInteriorTrace& fcn,
                                                  const FieldDataInvMassMatrix_Cell& mmfld,
                                                  const FieldDataMatrixD_Cell<MatrixT>& jacPDE_liftedQuantity,
                                                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                                  const std::vector<int>& interiorTraceCellJac) :
                                                    fcn_(fcn), mmfld_(mmfld),
                                                    jacPDE_liftedQuantity_(jacPDE_liftedQuantity),
                                                    mtxGlobalPDE_q_(mtxGlobalPDE_q),
                                                    interiorTraceCellJac_(interiorTraceCellJac), comm_rank_(0) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, LiftedT>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, LiftedT>, TupleClass<>>::
                     template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, LiftedT>, TupleClass<>>::
                     template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType                      ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ >::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, LiftedT>::template FieldCellGroupType<TopologyL> SFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementQFieldClassL;
    typedef typename SFieldCellGroupTypeL::template ElementType<>        ElementSFieldClassL;

    // Right types
    typedef typename XFieldType                      ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ >::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, LiftedT>::template FieldCellGroupType<TopologyR> SFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<>        ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<>        ElementQFieldClassR;
    typedef typename SFieldCellGroupTypeR::template ElementType<>        ElementSFieldClassR;

    // Trace types
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef JacobianElemInteriorTrace_DGBR2_PDE<PhysDim, MatrixTT> JacobianElemInteriorTraceType;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const SFieldCellGroupTypeL& sfldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const SFieldCellGroupTypeR& sfldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementSFieldClassL sfldElemL( sfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementSFieldClassR sfldElemR( sfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // DOFs per element / number of integrals evaluated per element
    int qDOFL = qfldElemL.nDOF();
    int qDOFR = qfldElemR.nDOF();
    int sDOFL = sfldElemL.nDOF();
    int sDOFR = sfldElemR.nDOF();
    const int nIntegrandL = qDOFL;
    const int nIntegrandR = qDOFR;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(qDOFL);
    std::vector<int> mapDOFGlobal_qR(qDOFR);

    // Inverse mass matrix used to compute lifing operators
    const DLA::MatrixDView_Array<Real>& mmfldCellL = mmfld_.getCellGroupGlobal(cellGroupGlobalL);
    const DLA::MatrixDView_Array<Real>& mmfldCellR = mmfld_.getCellGroupGlobal(cellGroupGlobalR);

    const DLA::MatrixDView_Array<MatrixT>& PDE_sL = jacPDE_liftedQuantity_.getCellGroupGlobal(cellGroupGlobalL);
    const DLA::MatrixDView_Array<MatrixT>& PDE_sR = jacPDE_liftedQuantity_.getCellGroupGlobal(cellGroupGlobalR);

    JacobianElemInteriorTraceSize sizeL(sDOFL, qDOFL, qDOFR);
    JacobianElemInteriorTraceSize sizeR(sDOFR, qDOFL, qDOFR);

    // PDE trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType, JacobianElemInteriorTraceType>
      integral(quadratureorder);

    // element lifted quantity jacobian matrices wrt q
    JacobianElemInteriorTraceType mtxElemL(sizeL);
    JacobianElemInteriorTraceType mtxElemR(sizeR);

    // complete lifted quantity jacobian wrt q after mass matrix multiplication
    typename JacobianElemInteriorTraceType::MatrixElemClass sL_qL(sDOFL, qDOFL);
    typename JacobianElemInteriorTraceType::MatrixElemClass sR_qL(sDOFR, qDOFL);

    typename JacobianElemInteriorTraceType::MatrixElemClass sL_qR(sDOFL, qDOFR);
    typename JacobianElemInteriorTraceType::MatrixElemClass sR_qR(sDOFR, qDOFR);

    // completed PDE jacobian wrt q (from chain rule)
    MatrixElemClass PDEL_qL(qDOFL, qDOFL);
    MatrixElemClass PDER_qL(qDOFR, qDOFL);
    MatrixElemClass PDEL_qR(qDOFL, qDOFR);
    MatrixElemClass PDER_qR(qDOFR, qDOFR);

    if (interiorTraceCellJac_.size() > 0)
    {
      // Determine if we need jacobians wrt the left cell
      if ( std::find(interiorTraceCellJac_.begin(), interiorTraceCellJac_.end(), cellGroupGlobalL) == interiorTraceCellJac_.end() )
        qDOFL = 0;

      // Determine if we need jacobians wrt the right cell
      if ( std::find(interiorTraceCellJac_.begin(), interiorTraceCellJac_.end(), cellGroupGlobalR) == interiorTraceCellJac_.end() )
        qDOFR = 0;
    }

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // left/right elements
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      sfldCellL.getElement( sfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      sfldCellR.getElement( sfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );

      const int rankL = qfldElemL.rank();
      const int rankR = qfldElemR.rank();

      // nothing to do if both elements are ghost elements
      if (rankL != comm_rank_ && rankR != comm_rank_) continue;

      // lifted quantity trace integration for canonical element
      integral( fcn_.integrand_LiftedScalar(xfldElemTrace, canonicalTraceL, canonicalTraceR,
                                            xfldElemL, qfldElemL, sfldElemL,
                                            xfldElemR, qfldElemR, sfldElemR),
                xfldElemTrace, mtxElemL, mtxElemR);

      // Finish up the lifted quantity jacobians
      if ( qDOFL > 0 ) sL_qL = -mmfldCellL[elemL]*mtxElemL.PDE_qL;
      if ( qDOFR > 0 ) sL_qR = -mmfldCellL[elemL]*mtxElemL.PDE_qR;

      if ( qDOFL > 0 ) sR_qL = -mmfldCellR[elemR]*mtxElemR.PDE_qL;
      if ( qDOFR > 0 ) sR_qR = -mmfldCellR[elemR]*mtxElemR.PDE_qR;

      PDEL_qL = 0;
      PDEL_qR = 0;
      PDER_qL = 0;
      PDER_qR = 0;

//      std::cout << "elemL: " << elemL << ", elemR: " << elemR << std::endl;
//      std::cout << "PDE_sL[ elemL ]: " << PDE_sL[ elemL ] << std::endl;
//      std::cout << "PDE_sR[ elemR ]: " << PDE_sR[ elemR ] << std::endl;
//      std::cout << "sL_qL: " << sL_qL << std::endl;
//      std::cout << "sL_qR: " << sL_qR << std::endl;
//      std::cout << "sR_qL: " << sR_qL << std::endl;
//      std::cout << "sR_qR: " << sR_qR << std::endl;
//      std::cout << std::endl;

      //Complete the chain rule
      if ( qDOFL > 0 ) PDEL_qL = PDE_sL[ elemL ]*sL_qL;
      if ( qDOFR > 0 ) PDEL_qR = PDE_sL[ elemL ]*sL_qR;

      if ( qDOFL > 0 ) PDER_qL = PDE_sR[ elemR ]*sR_qL;
      if ( qDOFR > 0 ) PDER_qR = PDE_sR[ elemR ]*sR_qR;

      // scatter-add element jacobian to global
      scatterAdd(
          xfldTrace,
          qfldCellL, qfldCellR,
          rankL, rankR,
          elemL, elemR,
          mapDOFGlobal_qL.data(), nIntegrandL,
          mapDOFGlobal_qR.data(), nIntegrandR,
          PDEL_qL,
          PDEL_qR,
          PDER_qL,
          PDER_qR,
          mtxGlobalPDE_q_);

    } //loop over elements

  }

//----------------------------------------------------------------------------//
  template <class XFieldTraceGroupType,
            class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const XFieldTraceGroupType& xfldTrace,
      const QFieldCellGroupTypeL& qfldCellL,
      const QFieldCellGroupTypeR& qfldCellR,
      const int rankL, const int rankR,
      const int elemL, const int elemR,
      int mapDOFGlobal_qL[], const int nDOFL,
      int mapDOFGlobal_qR[], const int nDOFR,

      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qR,

      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qR,

      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q)
  {
    // global mapping for qL
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    // global mapping for qR
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobal_qR, nDOFR );

    // jacobian wrt qL
    if (rankL == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qL, mapDOFGlobal_qL, nDOFL );
    if (rankR == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qL, mapDOFGlobal_qR, nDOFR, mapDOFGlobal_qL, nDOFL );

    // jacobian qrt qR
    if (rankL == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qR, mapDOFGlobal_qL, nDOFL, mapDOFGlobal_qR, nDOFR );
    if (rankR == comm_rank_) mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qR, mapDOFGlobal_qR, nDOFR );
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  const FieldDataMatrixD_Cell<MatrixT>& jacPDE_liftedQuantity_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  const std::vector<int> interiorTraceCellJac_;
  mutable int comm_rank_;
};


// Factory function

template<class IntegrandInteriorTrace, class MatrixT, class MatrixQ>
JacobianInteriorTrace_DGBR2_LiftedQuantity_impl<IntegrandInteriorTrace>
JacobianInteriorTrace_DGBR2_LiftedQuantity(
    const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
    const FieldDataInvMassMatrix_Cell& mmfld,
    const FieldDataMatrixD_Cell<MatrixT>& jacPDE_liftedQuantity,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
    std::vector<int> interiorTraceCellJac = {})
{
  return {fcn.cast(), mmfld, jacPDE_liftedQuantity, mtxGlobalPDE_q, interiorTraceCellJac};
}

#endif
}

#endif  // JACOBIANINTERIORTRACE_DGBR2_LIFTEDQUANTITY_H
