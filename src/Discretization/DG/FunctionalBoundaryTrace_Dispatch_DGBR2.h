// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTINOALBOUNDARYTRACE_DISPATCH_DGBR2_H
#define FUNCTINOALBOUNDARYTRACE_DISPATCH_DGBR2_H

// boundary-trace integral residual functions


//#include "Discretization/DG/FunctionalBoundaryTrace_FieldTrace_DGBR2.h"
#include "Discretization/DG/FunctionalBoundaryTrace_DGBR2.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace (e.g. with Lagrange multipliers)
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ>
class FunctionalBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl
{
public:
  FunctionalBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      Vector<ArrayQ>& rsdPDEGlobal,
      Vector<ArrayQ>& rsdBCGlobal )
    : xfld_(xfld), qfld_(qfld), rfld_(rfld), lgfld_(lgfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Functionals with field trace not implemented yet.");
#if 0
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        FunctionalBoundaryTrace_FieldTrace_DGBR2(fcn, rsdPDEGlobal_, rsdBCGlobal_),
        xfld_, (qfld_, rfld_), lgfld_, quadratureorder_, ngroup_ );
#endif
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdBCGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ, class VectorArrayQ>
FunctionalBoundaryTrace_FieldTrace_Dispatch_DGBR2_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ, VectorArrayQ>
FunctionalBoundaryTrace_FieldTrace_Dispatch_DGBR2(const FieldType<XFieldType>& xfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                  const int* quadratureorder, int ngroup,
                                                  Vector<ArrayQ>& rsdPDEGlobal,
                                                  Vector<ArrayQ>& rsdBCGlobal)
{
  return {xfld, qfld, rfld, lgfld, quadratureorder, ngroup, rsdPDEGlobal, rsdBCGlobal};
}

//---------------------------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class ArrayJ>
class FunctionalBoundaryTrace_Dispatch_DGBR2_impl
{
public:
  FunctionalBoundaryTrace_Dispatch_DGBR2_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
      const int* quadratureorder, int ngroup,
      ArrayJ& functional )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), rfld_(rfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      functional_(functional)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
      FunctionalBoundaryTrace_DGBR2( fcnOutput_, fcn.cast(), functional_ ), xfld_, (qfld_, rfld_), quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld_;
  const int* quadratureorder_;
  const int ngroup_;
  ArrayJ& functional_;
};

// Factory function

template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class ArrayJ>
FunctionalBoundaryTrace_Dispatch_DGBR2_impl<FunctionalIntegrand, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, ArrayJ>
FunctionalBoundaryTrace_Dispatch_DGBR2(const FunctionalIntegrand& fcnOutput,
                                       const FieldType<XFieldType>& xfld,
                                       const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                       const FieldLift<PhysDim, TopoDim, VectorArrayQ>& rfld,
                                       const int* quadratureorder, int ngroup,
                                       ArrayJ& functional )
{
  return {fcnOutput, xfld.cast(), qfld, rfld, quadratureorder, ngroup, functional};
}


}

#endif //FUNCTINOALBOUNDARYTRACE_DISPATCH_DGBR2_H
