// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALNORMTYPE_H
#define RESIDUALNORMTYPE_H

namespace SANS
{

// residual norm categories
enum ResidualNormType
{
  ResidualNorm_L2,
  ResidualNorm_L2_DOFAvg,
  ResidualNorm_InvJac_DOF_Weighted,
  ResidualNorm_InvJac_RelWeighted,
  ResidualNorm_L2_DOFWeighted,
  ResidualNorm_Default = ResidualNorm_InvJac_DOF_Weighted
};

}

#endif //RESIDUALNORMTYPE_H
