// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UPDATEFRACTIONECELL_H
#define UPDATEFRACTIONECELL_H

// Cell physicality check functions

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"

#include "Quadrature/Quadrature.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "BasisFunction/BasisFunctionLine_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope_Lagrange.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group physicality check
//

template<class PDE>
class UpdateFractionCell_impl :
    public GroupIntegralCellType< UpdateFractionCell_impl<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;

  // Save off the cell integrand, the pde, and the reference to our final result
  UpdateFractionCell_impl( const PDE& pde,
                           const std::vector<int>& cellGroups,
                           const Real maxChangeFraction, Real& updateFraction ) :
    pde_(pde),
    cellGroups_(cellGroups),
    maxChangeFraction_(maxChangeFraction),
    updateFraction_(updateFraction)
  {
    // Nothing
  }

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

//----------------------------------------------------------------------------//
  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    // Nothing
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>
                   ::template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    typedef typename ElementXFieldClass::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    ArrayQ q, dq;
    ParamT param;
    Real updateFraction = updateFraction_;

    // uses same quadrature rule as residual evaluations
    Quadrature<TopoDim, Topology> quadrature( quadratureorder );

    // also check the corners nodes of the element
    std::vector<DLA::VectorS<TopoDim::D,Real>> sRefVertices;
    LagrangeNodes<Topology>::get(1, sRefVertices); // TODO: currently assume that element corners/vertices are p=1 Lagrange interpolation nodes

    const QFieldCellGroupType& qfldCell  = get<0>(fldsCell);
    const QFieldCellGroupType& dqfldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass dqfldElem( dqfldCell.basis() );

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == dqfldCell.nElem() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      dqfldCell.getElement( dqfldElem, elem );

      // loop over quadrature points
      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        QuadPointType sRef = quadrature.coordinates_cache( iquad );

        // solution value and increment
        xfldElem.eval( sRef, param );
        qfldElem.eval( sRef, q );
        dqfldElem.eval( sRef, dq );

        pde_.updateFraction(param, q, dq, maxChangeFraction_, updateFraction);
        updateFraction_ = MIN(updateFraction_, updateFraction);
      }

      for (std::size_t i = 0; i < sRefVertices.size(); i++)
      {
        // solution value and increment
        xfldElem.eval( sRefVertices[i], param );
        qfldElem.eval( sRefVertices[i], q );
        dqfldElem.eval( sRefVertices[i], dq );

        pde_.updateFraction(param, q, dq, maxChangeFraction_, updateFraction);
        updateFraction_ = MIN(updateFraction_, updateFraction);
      }
    }
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
  const Real maxChangeFraction_;
  Real& updateFraction_;
};

// Factory function

template<class PDE>
UpdateFractionCell_impl<PDE>
UpdateFractionCell( const PDE& pde,
                    const std::vector<int>& cellGroups,
                    const Real maxChangeFraction, Real& updateFraction )
{
  return UpdateFractionCell_impl<PDE>(pde, cellGroups, maxChangeFraction, updateFraction);
}


} //namespace SANS

#endif  // UPDATEFRACTIONECELL_H
