// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UPDATEFRACTIONINTERIORTRACE_H
#define UPDATEFRACTIONINTERIORTRACE_H

// interior-trace integral UpdateFraction functions

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"

#include "Quadrature/Quadrature.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  interior-trace update fraction
//

template<class PDE>
class UpdateFractionInteriorTrace_impl :
    public GroupIntegralInteriorTraceType< UpdateFractionInteriorTrace_impl<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the UpdateFraction vectors
  UpdateFractionInteriorTrace_impl( const PDE& pde,
                                    const std::vector<int>& interiorTraceGroups,
                                    const Real maxChangeFraction, Real& updateFraction ) :
    pde_(pde),
    interiorTraceGroups_(interiorTraceGroups),
    maxChangeFraction_(maxChangeFraction),
    updateFraction_(updateFraction)
  {
  }

  std::size_t nInteriorTraceGroups() const            { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const   { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  std::size_t nPeriodicTraceGroups() const            { return interiorTraceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const   { return interiorTraceGroups_[n]; }
  const std::vector<int>& periodicTraceGroups() const { return interiorTraceGroups_; }

//----------------------------------------------------------------------------//
  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    // Nothing
  }


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType                     ::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>
            ::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType                     ::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>
            ::template FieldCellGroupType<TopologyR>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType         ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const int quadratureorder )
  {
    typedef typename XFieldType                     ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename XFieldType                     ::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    typedef typename ElementXFieldClassL::T ParamTL;
    typedef typename ElementXFieldClassR::T ParamTR;

    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDim> QuadPointCellType;

    ArrayQ q, dq;
    QuadPointCellType sRefL;      // reference-element coordinates left
    QuadPointCellType sRefR;      // reference-element coordinates right

    ParamTL paramL;
    ParamTR paramR;

    Real updateFraction = updateFraction_;

    // use quadrature rule that matches the polynomial order
    Quadrature<TopoDimTrace, TopologyTrace> quadrature( quadratureorder );

    const QFieldCellGroupTypeL& qfldCellL  = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& dqfldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR  = get<0>(fldsCellR);
    const QFieldCellGroupTypeR& dqfldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL ( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL ( qfldCellL.basis() );
    ElementQFieldClassL dqfldElemL( dqfldCellL.basis() );

    ElementXFieldClassR xfldElemR ( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR ( qfldCellR.basis() );
    ElementQFieldClassR dqfldElemR( dqfldCellR.basis() );

    const int nquad = quadrature.nQuadrature();

    // Left
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // copy global solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      dqfldCellL.getElement( dqfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );
      dqfldCellR.getElement( dqfldElemR, elemR );

      // loop over quadrature points
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        // reference-trace coordinates
        QuadPointTraceType sRefTrace = quadrature.coordinates_cache( iquad );

        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalTraceL, sRefTrace, sRefL );
        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::eval( canonicalTraceR, sRefTrace, sRefR );

        xfldElemL.eval( sRefL, paramL );
        qfldElemL.eval( sRefL, q );
        dqfldElemL.eval( sRefL, dq );

        pde_.updateFraction(paramL, q, dq, maxChangeFraction_, updateFraction);
        updateFraction_ = MIN(updateFraction_, updateFraction);

        xfldElemR.eval( sRefR, paramR );
        qfldElemR.eval( sRefR, q );
        dqfldElemR.eval( sRefR, dq );

        pde_.updateFraction(paramL, q, dq, maxChangeFraction_, updateFraction);
        updateFraction_ = MIN(updateFraction_, updateFraction);
      }
    }
  }

protected:
  const PDE& pde_;
  const std::vector<int> interiorTraceGroups_;
  const Real maxChangeFraction_;
  Real& updateFraction_;
};

// Factory function

template<class PDE>
UpdateFractionInteriorTrace_impl<PDE>
UpdateFractionInteriorTrace( const PDE& pde,
                             const std::vector<int>& interiorTraceGroups,
                             const Real maxChangeFraction, Real& updateFraction )
{
  return UpdateFractionInteriorTrace_impl<PDE>(pde, interiorTraceGroups, maxChangeFraction, updateFraction);
}

} //namespace SANS

#endif  // UPDATEFRACTIONINTERIORTRACE_H
