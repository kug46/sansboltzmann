// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UPDATEFRACTIONBOUNDARYTRACE_SANSLG_H
#define UPDATEFRACTIONBOUNDARYTRACE_SANSLG_H

// boundary-trace integral residual functions

#include "Field/Field.h"
#include "Field/Element/TraceUnitNormal.h"
#include "Field/Tuple/FieldTuple.h"

#include "Quadrature/Quadrature.h"

//#include "call_derived_UpdateFraction.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class PDE, class NDBCVector>
class UpdateFractionBoundaryTrace_sansLG_impl :
    public GroupIntegralBoundaryTraceType< UpdateFractionBoundaryTrace_sansLG_impl<PDE, NDBCVector> >
{
public:
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  UpdateFractionBoundaryTrace_sansLG_impl( const PDE& pde,
                                           const BCBase& bc,
                                           const std::vector<int>& BoundaryGroups,
                                           const Real maxChangeFraction, Real& updateFraction) :
    pde_(pde),
    bc_(bc),
    BoundaryGroups_(BoundaryGroups),
    maxChangeFraction_(maxChangeFraction),
    updateFraction_(updateFraction)
   {
   }

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

//----------------------------------------------------------------------------//
  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    // Nothing
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>
                   ::template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    typedef typename ElementXFieldClassL::T ParamTL;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTrace;
    typedef QuadratureCellTracePoint<TopoDim> QuadPointCell;

    //typedef typename ElementXFieldTraceClass::VectorX VectorX;

    ArrayQ q, dq;
    //VectorX nL;                  // unit normal (points out of domain)
    QuadPointCell sRefL;         // reference-element coordinates left

    ParamTL paramL;

    Real updateFraction = updateFraction_;

    // use quadrature rule that matches the polynomial order
    Quadrature<TopoDimTrace, TopologyTrace> quadrature( quadratureorder );

    const QFieldCellGroupTypeL& qfldCellL  = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& dqfldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL dqfldElemL( dqfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      dqfldCellL.getElement( dqfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );

      // loop over quadrature points
      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        QuadPointTrace sRefTrace = quadrature.coordinates_cache( iquad );

        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalTraceL, sRefTrace, sRefL );

        xfldElemL.eval( sRefL, paramL );
        qfldElemL.eval( sRefL, q );
        dqfldElemL.eval( sRefL, dq );

        pde_.updateFraction(paramL, q, dq, maxChangeFraction_, updateFraction);
        updateFraction_ = MIN(updateFraction_, updateFraction);

        // unit normal: points out of domain
        //traceUnitNormal( get<-1>(xfldElemL), sRefL, get<-1>(xfldElemTrace), sRefTrace, nL);
        //
        //call_derived_UpdateFraction<NDBCVector>(bc_, nL, q, UpdateFraction_);
      }
    }
  }

protected:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const Real maxChangeFraction_;
  Real& updateFraction_;
};

// Factory function

template<class NDBCVector, class PDE>
UpdateFractionBoundaryTrace_sansLG_impl<PDE, NDBCVector>
UpdateFractionBoundaryTrace_sansLG( const PDE& pde,
                                    const BCBase& bc,
                                    const std::vector<int>& BoundaryGroups,
                                    const Real maxChangeFraction, Real& updateFraction )
{
  return UpdateFractionBoundaryTrace_sansLG_impl<PDE, NDBCVector>(pde, bc, BoundaryGroups, maxChangeFraction, updateFraction);
}


}

#endif  // UPDATEFRACTIONBOUNDARYTRACE_SANSLG_H
