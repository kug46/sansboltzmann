// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UPDATEFRACTIONBOUNDARYTRACE_DISPATCH_H
#define UPDATEFRACTIONBOUNDARYTRACE_DISPATCH_H

// boundary-trace integral residual functions

#include <Discretization/UpdateFraction/UpdateFractionBoundaryTrace_mitLG.h>
#include <Discretization/UpdateFraction/UpdateFractionBoundaryTrace_sansLG.h>

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups_HubTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class UpdateFractionBoundaryTrace_mitLG_Dispatch_impl
{
public:
  UpdateFractionBoundaryTrace_mitLG_Dispatch_impl(
      PDE& pde,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& dqfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const Field<PhysDim, TopoDim, ArrayQ>& dlgfld,
      const int* quadratureorder, int ngroup,
      const Real maxChangeFraction, Real& updateFraction ) :
    xfld_(xfld),
    qfld_(qfld),
    dqfld_(dqfld),
    lgfld_(lgfld),
    dlgfld_(dlgfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    pde_(pde),
    maxChangeFraction_(maxChangeFraction),
    updateFraction_(updateFraction)
  {
    // Nothing
  }

  template<class NDPDE, class NDBCVector, class Category, class DiscTag>
  void operator()(const IntegrandBoundaryTrace<NDPDE, NDVectorCategory<NDBCVector, Category>, DiscTag>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        UpdateFractionBoundaryTrace_mitLG<NDBCVector>(pde_, fcn.getBC(), fcn.getBoundaryGroups(), maxChangeFraction_, updateFraction_),
        xfld_, (qfld_, dqfld_), (lgfld_, dlgfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& dqfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& dlgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  PDE& pde_;
  const Real maxChangeFraction_;
  Real& updateFraction_;
};

// Factory function

template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
UpdateFractionBoundaryTrace_mitLG_Dispatch_impl<PDE, XFieldType, PhysDim, TopoDim, ArrayQ>
UpdateFractionBoundaryTrace_mitLG_Dispatch( PDE& pde,
                                            const XFieldType& xfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& dqfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& dlgfld,
                                            const int* quadratureorder, int ngroup,
                                            const Real maxChangeFraction, Real& updateFraction )
{
  return { pde, xfld, qfld, dqfld, lgfld, dlgfld, quadratureorder, ngroup, maxChangeFraction, updateFraction };
}


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Hub Trace
//
//---------------------------------------------------------------------------//
template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class UpdateFractionBoundaryTrace_mitHT_Dispatch_impl
{
public:
  UpdateFractionBoundaryTrace_mitHT_Dispatch_impl(
      PDE& pde,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& dqfld,
      const Field<PhysDim, TopoDim, ArrayQ>& hbfld,
      const int* quadratureorder, int ngroup,
      const Real maxChangeFraction, Real& updateFraction ) :
    xfld_(xfld),
    qfld_(qfld),
    dqfld_(dqfld),
    hbfld_(hbfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    pde_(pde),
    maxChangeFraction_(maxChangeFraction),
    updateFraction_(updateFraction)
  {
    // Nothing
  }

  template<class NDPDE, class NDBCVector, class Category, class DiscTag>
  void operator()(const IntegrandBoundaryTrace<NDPDE, NDVectorCategory<NDBCVector, Category>, DiscTag>& fcn)
  {
    IntegrateBoundaryTraceGroups_HubTrace<TopoDim>::integrate(
        UpdateFractionBoundaryTrace_mitLG<NDBCVector>(pde_, fcn.getBC(), fcn.getBoundaryGroups(), maxChangeFraction_, updateFraction_),
        xfld_, (qfld_, dqfld_), hbfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& dqfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& hbfld_;
  const int* quadratureorder_;
  const int ngroup_;
  PDE& pde_;
  const Real maxChangeFraction_;
  Real& updateFraction_;
};

// Factory function

template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
UpdateFractionBoundaryTrace_mitHT_Dispatch_impl<PDE, XFieldType, PhysDim, TopoDim, ArrayQ>
UpdateFractionBoundaryTrace_mitHT_Dispatch( PDE& pde,
                                            const XFieldType& xfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& dqfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& hbfld,
                                            const int* quadratureorder, int ngroup,
                                            const Real maxChangeFraction, Real& updateFraction )
{
  return { pde, xfld, qfld, dqfld, hbfld, quadratureorder, ngroup, maxChangeFraction, updateFraction };
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class UpdateFractionBoundaryTrace_sansLG_Dispatch_impl
{
public:
  UpdateFractionBoundaryTrace_sansLG_Dispatch_impl(
      PDE& pde,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& dqfld,
      const int* quadratureorder, int ngroup,
      const Real maxChangeFraction, Real& updateFraction ):
    xfld_(xfld),
    qfld_(qfld),
    dqfld_(dqfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    pde_(pde),
    maxChangeFraction_(maxChangeFraction),
    updateFraction_(updateFraction)
  {
    // Nothing
  }

  template<class NDPDE, class NDBCVector, class Category, class DiscTag>
  void operator()(const IntegrandBoundaryTrace<NDPDE, NDVectorCategory<NDBCVector, Category>, DiscTag>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        UpdateFractionBoundaryTrace_sansLG<NDBCVector>(pde_, fcn.getBC(), fcn.getBoundaryGroups(), maxChangeFraction_, updateFraction_),
        xfld_, (qfld_, dqfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& dqfld_;
  const int* quadratureorder_;
  const int ngroup_;
  PDE& pde_;
  const Real maxChangeFraction_;
  Real& updateFraction_;
};

// Factory function
template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
UpdateFractionBoundaryTrace_sansLG_Dispatch_impl<PDE, XFieldType, PhysDim, TopoDim, ArrayQ>
UpdateFractionBoundaryTrace_sansLG_Dispatch( PDE& pde,
                                             const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& dqfld,
                                             const int* quadratureorder, int ngroup,
                                             const Real maxChangeFraction, Real& updateFraction )
{
  return {pde, xfld, qfld, dqfld, quadratureorder, ngroup, maxChangeFraction, updateFraction};
}


}

#endif // UPDATEFRACTIONBOUNDARYTRACE_DISPATCH_H
