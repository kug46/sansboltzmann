// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef UPDATEFRACTIONBOUNDARYTRACE_MITLG_H
#define UPDATEFRACTIONBOUNDARYTRACE_MITLG_H

// boundary-trace integral residual functions

#include "Field/GroupElementType.h"
#include "Field/Field.h"
#include "Field/Element/TraceUnitNormal.h"
#include "Field/Tuple/FieldTuple.h"

#include "Quadrature/Quadrature.h"

//#include "call_derived_UpdateFraction.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class PDE, class NDBCVector>
class UpdateFractionBoundaryTrace_mitLG_impl :
    public GroupIntegralBoundaryTraceType< UpdateFractionBoundaryTrace_mitLG_impl<PDE, NDBCVector> >
{
public:
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  UpdateFractionBoundaryTrace_mitLG_impl( const PDE& pde,
                                          const BCBase& bc,
                                          const std::vector<int>& BoundaryGroups,
                                          const Real maxChangeFraction, Real& updateFraction ) :
   pde_(pde),
   bc_(bc),
   BoundaryGroups_(BoundaryGroups),
   maxChangeFraction_(maxChangeFraction),
   updateFraction_(updateFraction)
  {
  }

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

//----------------------------------------------------------------------------//
  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& qflds,
              const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& lgflds ) const
  {
    // Nothing
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>
            ::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>
            ::template FieldTraceGroupType<TopologyTrace>& fldsTrace,
      const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldClassL::T ParamTL;

    typedef typename TopologyTrace::TopoDim TopoDimTrace;
    typedef QuadraturePoint<TopoDimTrace> QuadPointTrace;
    typedef QuadratureCellTracePoint<TopoDim> QuadPointCell;

    typedef DLA::VectorS<PhysDim::D,Real> VectorX;

    ArrayQ q, dq, lg, dlg;
    VectorX nL;                  // unit normal (points out of domain)
    QuadPointCell sRefL;         // reference-element coordinates left

    ParamTL paramL;

    Real updateFraction;

    // use quadrature rule that matches the polynomial order
    Quadrature<TopoDimTrace, TopologyTrace> quadrature( quadratureorder );

    const QFieldCellGroupTypeL& qfldCellL  = get<0>(fldsCellL);
    const QFieldCellGroupTypeL& dqfldCellL = get<1>(fldsCellL);

    const QFieldTraceGroupType& lgfldTrace  = get<0>(fldsTrace);
    const QFieldTraceGroupType& dlgfldTrace = get<1>(fldsTrace);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassL dqfldElemL( dqfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );
    ElementQFieldTraceClass dlgfldElemTrace( dlgfldTrace.basis() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; ++elem)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      dqfldCellL.getElement( dqfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      lgfldTrace.getElement( lgfldElemTrace, elem );
      dlgfldTrace.getElement( dlgfldElemTrace, elem );

      // loop over quadrature points
      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        QuadPointTrace sRefTrace = quadrature.coordinates_cache( iquad );

        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalTraceL, sRefTrace, sRefL );

        // basis value, gradient
        xfldElemL.eval( sRefL, paramL );
        qfldElemL.eval( sRefL, q );
        dqfldElemL.eval( sRefL, dq );

        // unit normal: points out of domain
        traceUnitNormal( get<-1>(xfldElemL), sRefL, get<-1>(xfldElemTrace), sRefTrace, nL);

        pde_.updateFraction(paramL, q, dq, maxChangeFraction_, updateFraction);
        updateFraction_ = MIN(updateFraction_, updateFraction);

        lgfldElemTrace.eval( sRefTrace, lg );
        dlgfldElemTrace.eval( sRefTrace, dlg );

        pde_.updateFraction(paramL, lg, dlg, maxChangeFraction_, updateFraction);
        updateFraction_ = MIN(updateFraction_, updateFraction);

        //if ( UpdateFraction_ )
        //  call_derived_UpdateFraction<NDBCVector>(bc_, nL, q, UpdateFraction_);
      }
    }
  }

protected:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const Real maxChangeFraction_;
  Real& updateFraction_;
};

// Factory function

template<class NDBCVector, class PDE>
UpdateFractionBoundaryTrace_mitLG_impl<PDE, NDBCVector>
UpdateFractionBoundaryTrace_mitLG( const PDE& pde,
                                   const BCBase& bc,
                                   const std::vector<int>& BoundaryGroups,
                                   const Real maxChangeFraction, Real& updateFraction )
{
  return {pde, bc, BoundaryGroups, maxChangeFraction, updateFraction};
}


}

#endif  // UPDATEFRACTIONBOUNDARYTRACE_MITLG_H
