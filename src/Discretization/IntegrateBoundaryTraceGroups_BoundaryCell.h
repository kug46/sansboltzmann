// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATEBOUNDARYTRACEGROUPS_BOUNDARYCELL_H
#define INTEGRATEBOUNDARYTRACEGROUPS_BOUNDARYCELL_H

// boundary-trace integral residual functions

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "GroupIntegral_Type.h"

namespace SANS
{


template <class TopologyTrace, class TopologyL, class TopoDim, class IntegralBoundaryTraceGroup,
class XFieldType, class FieldCellType, class FieldTraceType >
void
IntegrateBoundaryTraceGroups_BoundaryCell_Integral(
    IntegralBoundaryTraceGroup& integral,
    const XFieldType& xfld,
    const FieldCellType& fldCell,
    const int traceGroupGlobal,
    const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const FieldTraceType& fldTrace,
    const int quadratureorder )
{
  const int cellGroupGlobalL = xfldTrace.getGroupLeft();
  // std::cout<< "cellGroupGlobalL = " << cellGroupGlobalL << ", traceGroupGlobal = " << traceGroupGlobal << std::endl;
  // Integrate over the trace group
  integral.template integrate<TopologyTrace, TopologyL, TopoDim, XFieldType>(
      cellGroupGlobalL,
      xfld.template getCellGroup<TopologyL>(cellGroupGlobalL),
      fldCell.template getCellGroupGlobal<TopologyL>(cellGroupGlobalL),
      traceGroupGlobal,
      xfldTrace, fldTrace.template getCellGroupGlobal<TopologyL>(traceGroupGlobal),
      quadratureorder );
}


template<class TopDim>
class IntegrateBoundaryTraceGroups_BoundaryCell;


template<>
class IntegrateBoundaryTraceGroups_BoundaryCell<TopoD1>
{
public:
  typedef TopoD1 TopoDim;
private:
  template <class TopologyTrace, class FieldTraceType, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType >
  static void
  LeftTopology(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const FieldCellType& fldCell,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const FieldTraceType& fldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // determine topology for R
      IntegrateBoundaryTraceGroups_BoundaryCell_Integral<TopologyTrace, Line, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal, xfldTrace, fldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }
public:
  template <class FieldTraceType, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType>
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<FieldCellType>& fldCellType,
      const FieldType<FieldTraceType>& fldTraceType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const FieldCellType& fldCell = fldCellType.cast();
    const FieldTraceType& fldTrace = fldTraceType.cast();

    integral.template check<TopoDim>( fldCell, fldTrace );

    SANS_ASSERT( &xfld.getXField() == &fldCell.getXField() );
    SANS_ASSERT( &fldCell.getXField() == &fldTrace.getXField() );

    SANS_ASSERT_MSG( ngroup == xfld.getXField().nBoundaryTraceGroups(),
                     "ngroup = %d, xfld.getXField().nBoundaryTraceGroups() = %d", ngroup, xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);
      if ( xfld.getXField().getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node,FieldTraceType>(  integral,
                                            xfld, fldCell,
                                            boundaryGroup,
                                            xfld.getXField().template getBoundaryTraceGroup<Node>(boundaryGroup),
                                            fldTrace,
                                            quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class IntegrateBoundaryTraceGroups_BoundaryCell<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

private:
  template <class TopologyTrace, class FieldTraceType, class IntegralBoundaryTraceGroup,
            class XFieldType, class FieldCellType>
  static void
  LeftTopology(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const FieldCellType& fldCell,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const FieldTraceType& fldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();
    // std::cout<< "groupL = " << groupL << std::endl;
    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // std::cout<< "dispatching with triangle left" << std::endl;
      IntegrateBoundaryTraceGroups_BoundaryCell_Integral<TopologyTrace, Triangle, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal, xfldTrace, fldTrace, quadratureorder );
    }
    else if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
    {
      // std::cout<< "dispatching with quad left" << std::endl;
      // determine topology for R
      IntegrateBoundaryTraceGroups_BoundaryCell_Integral<TopologyTrace, Quad, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal, xfldTrace, fldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }
public:
  template <class FieldTraceType, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType >
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<FieldCellType>& fldCellType,
      const FieldType<FieldTraceType>& fldTraceType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const FieldCellType& fldCell = fldCellType.cast();
    const FieldTraceType& fldTrace = fldTraceType.cast();

    integral.template check<TopoDim>( fldCell, fldTrace );

    SANS_ASSERT( &xfld.getXField() == &fldCell.getXField() );
    SANS_ASSERT( &xfld.getXField() == &fldTrace.getXField() );

    SANS_ASSERT_MSG( ngroup == xfld.getXField().nBoundaryTraceGroups(),
                     "ngroup = %d, xfld.getXField().nBoundaryTraceGroups() = %d", ngroup, xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);
      // std::cout<< "group = " << group << ", boundaryGroup = " << boundaryGroup << std::endl;
      if ( xfld.getXField().getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line,FieldTraceType>(integral,
                                          xfld, fldCell,
                                          boundaryGroup,
                                          xfld.getXField().template getBoundaryTraceGroup<Line>(boundaryGroup),
                                          fldTrace,
                                          quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class IntegrateBoundaryTraceGroups_BoundaryCell<TopoD3>
{
public:
  typedef TopoD3 TopoDim;
private:
  template <class FieldTraceType, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType>
  static void
  LeftTopology_Triangle(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const FieldCellType& fldCell,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
      const FieldTraceType& fldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      IntegrateBoundaryTraceGroups_BoundaryCell_Integral<Triangle, Tet, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal, xfldTrace, fldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

  template <class FieldTraceType, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType>
  static void
  LeftTopology_Quad(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const FieldCellType& fldCell,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
      const FieldTraceType& fldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      IntegrateBoundaryTraceGroups_BoundaryCell_Integral<Quad, Hex, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal, xfldTrace, fldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }

public:
  template <class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType, class FieldTraceType>
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<FieldCellType>& fldCellType,
      const FieldType<FieldTraceType>& fldTraceType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const FieldCellType& fldCell = fldCellType.cast();
    const FieldTraceType& fldTrace = fldTraceType.cast();

    integral.template check<TopoDim>( fldCell, fldTrace );

    SANS_ASSERT( &xfld.getXField() == &fldCell.getXField() );
    SANS_ASSERT( &fldCell.getXField() == &fldTrace.getXField() );

    SANS_ASSERT_MSG( ngroup == xfld.getXField().nBoundaryTraceGroups(),
                     "ngroup = %d, xfld.getXField().nBoundaryTraceGroups() = %d", ngroup, xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);

      if ( xfld.getXField().getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle<FieldTraceType>( integral,
                                               xfld, fldCell,
                                               boundaryGroup,
                                               xfld.getXField().template getBoundaryTraceGroup<Triangle>(boundaryGroup),
                                               fldTrace,
                                               quadratureorder[boundaryGroup] );
      }
      else if ( xfld.getXField().getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad<FieldTraceType>( integral,
                                           xfld, fldCell,
                                           boundaryGroup,
                                           xfld.getXField().template getBoundaryTraceGroup<Quad>(boundaryGroup),
                                           fldTrace,
                                           quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};


template<>
class IntegrateBoundaryTraceGroups_BoundaryCell<TopoD4>
{
public:
  typedef TopoD4 TopoDim;
private:
  template <class FieldTraceType, class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType>
  static void
  LeftTopology_Tet(
      IntegralBoundaryTraceGroup& integral,
      const XFieldType& xfld,
      const FieldCellType& fldCell,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
      const FieldTraceType& fldTrace,
      const int quadratureorder )
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( fldCell.getCellGroupBase(groupL).topoTypeID() == typeid(Pentatope) )
    {
      // determine topology for R
      IntegrateBoundaryTraceGroups_BoundaryCell_Integral<Tet, Pentatope, TopoDim>(
          integral, xfld, fldCell, traceGroupGlobal, xfldTrace, fldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unknown cell topology" );
  }
public:

  template <class IntegralBoundaryTraceGroup, class XFieldType, class FieldCellType, class FieldTraceType>
  static void
  integrate(
      GroupIntegralBoundaryTraceType<IntegralBoundaryTraceGroup>&& integralType,
      const FieldType<XFieldType>& xfldType,
      const FieldType<FieldCellType>& fldCellType,
      const FieldType<FieldTraceType>& fldTraceType,
      const int quadratureorder[], int ngroup )
  {
    IntegralBoundaryTraceGroup& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const FieldCellType& fldCell = fldCellType.cast();
    const FieldTraceType& fldTrace = fldTraceType.cast();

    integral.template check<TopoDim>( fldCell, fldTrace );

    SANS_ASSERT( &xfld.getXField() == &fldCell.getXField() );
    SANS_ASSERT( &fldCell.getXField() == &fldTrace.getXField() );

    SANS_ASSERT_MSG( ngroup == xfld.getXField().nBoundaryTraceGroups(),
                     "ngroup = %d, xfld.getXField().nBoundaryTraceGroups() = %d", ngroup, xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = integral.boundaryGroup(group);

      if ( xfld.getXField().getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Tet) )
      {
        LeftTopology_Tet<FieldTraceType>( integral,
                                          xfld, fldCell,
                                          boundaryGroup,
                                          xfld.getXField().template getBoundaryTraceGroup<Tet>(boundaryGroup),
                                          fldTrace,
                                          quadratureorder[boundaryGroup] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
  }
};

}

#endif  // INTEGRATEBOUNDARYTRACEGROUPS_BOUNDARYCELL_H
