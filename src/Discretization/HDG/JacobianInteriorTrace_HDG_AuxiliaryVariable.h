// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_HDG_AUXILIARYVARIABLE_H
#define JACOBIANINTERIORTRACE_HDG_AUXILIARYVARIABLE_H

// HDG trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class Surreal, class IntegrandInteriorTrace>
class JacobianInteriorTrace_HDG_AuxiliaryVariable_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_HDG_AuxiliaryVariable_impl<Surreal, IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianInteriorTrace_HDG_AuxiliaryVariable_impl( const IntegrandInteriorTrace& fcn,
                                                    const std::map<int,std::vector<int>>& cellITraceGroups,
                                                    const int& cellgroup, const int& cellelem,
                                                    MatrixAUXElemClass& mtxAUX_q,
                                                    std::vector<MatrixAUXElemClass>& mtxAUX_qI,
                                                    std::vector<std::vector<int>>& mapDOFGlobal_qI ) :
                                                      fcn_(fcn), cellITraceGroups_(cellITraceGroups),
                                                      cellgroup_(cellgroup), cellelem_(cellelem),
                                                      mtxAUX_q_(mtxAUX_q), mtxAUX_qI_(mtxAUX_qI),
                                                      mapDOFGlobal_qI_(mapDOFGlobal_qI)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const {}

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<Surreal> ElementQFieldSurrealClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldSurrealTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldSurrealClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldSurrealTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    const int nIntegrandL = nDOFL;
    const int nIntegrandR = nDOFR;

    // variables/equations per DOF
    const int nEqn = IntegrandInteriorTrace::N;

    //Check if sizes are consistent
    if (xfldTrace.getGroupLeft() == cellgroup_)
    {
      SANS_ASSERT( mtxAUX_qI_.size() == TopologyL::NTrace );
      SANS_ASSERT( mapDOFGlobal_qI_.size() == TopologyL::NTrace );
    }
    else if (xfldTrace.getGroupRight() == cellgroup_)
    {
      SANS_ASSERT( mtxAUX_qI_.size() == TopologyR::NTrace );
      SANS_ASSERT( mapDOFGlobal_qI_.size() == TopologyR::NTrace );
    }
    else
      SANS_DEVELOPER_EXCEPTION("JacobianInteriorTrace_HDG_AuxiliaryVariable_impl::integrate - Invalid configuration!");

    // element integral
    typedef VectorArrayQSurreal AUXIntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, AUXIntegrandType> integralAUXL(quadratureorder, nDOFL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, AUXIntegrandType> integralAUXR(quadratureorder, nDOFR);

    // auxiliary variable residuals
    DLA::VectorD< AUXIntegrandType > rsdElemAUXL( nIntegrandL );
    DLA::VectorD< AUXIntegrandType > rsdElemAUXR( nIntegrandR );

    // element auxiliary variable equation jacobian matrices (Array length D of nDOFxnDOF matrices)
    MatrixAUXElemClass mtxElemAUXL_qL(nDOFL, nDOFL);
    MatrixAUXElemClass mtxElemAUXR_qR(nDOFR, nDOFR);
    MatrixAUXElemClass mtxElemAUXL_qI(nDOFL, nDOFI);
    MatrixAUXElemClass mtxElemAUXR_qI(nDOFR, nDOFI);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        //The cell that called this function is to the left of the trace
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );

        // zero AUX element Jacobians
        mtxElemAUXL_qL = 0.0;
        mtxElemAUXL_qI = 0.0;

        //Resize the auxiliary jacobian for this trace, since we know the nDOF counts now
        mtxAUX_qI_[canonicalTraceL.trace].resize(nDOFL, nDOFI);
        mtxAUX_qI_[canonicalTraceL.trace] = 0.0;

        // loop over derivative chunks for derivatives wrt qL and qI
        for (int nchunk = 0; nchunk < nEqn*(nDOFL + nDOFI); nchunk += nDeriv)
        {
          // associate derivative slots with solution variables
          int slot, slotOffset = 0;

          // wrt qL
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nEqn*nDOFL;


          // wrt qI
          for (int j = 0; j < nDOFI; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nEqn*nDOFI;

          //------------------------------------------------------------------------------------------------

          for (int n = 0; n < nIntegrandL; n++) rsdElemAUXL[n] = 0;

          // PDE trace integration for canonical element
          integralAUXL( fcn_.integrand_AUX(xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                           xfldElemL, qfldElemL),
                        xfldElemTrace, rsdElemAUXL.data(), nIntegrandL);

          //------------------------------------------------------------------------------------------------

          // accumulate derivatives into element jacobians
          slotOffset = 0;

          // wrt qL
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

                //AUXL_qL
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                    for (int d = 0; d < D; d++)
                      DLA::index(mtxElemAUXL_qL(i,j)[d],m,n) = DLA::index(rsdElemAUXL[i][d],m).deriv(slot - nchunk);
              }
            } //n loop
          } //j loop
          slotOffset += nEqn*nDOFL;


          // wrt qI
          for (int j = 0; j < nDOFI; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

                //PDEL_qI
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                    for (int d = 0; d < D; d++)
                      DLA::index(mtxElemAUXL_qI(i,j)[d],m,n) = DLA::index(rsdElemAUXL[i][d],m).deriv(slot - nchunk);
              }
            } //n loop
          } //j loop
          slotOffset += nEqn*nDOFI;

        } // nchunk loop

        //Accumulate
        mtxAUX_q_                         += mtxElemAUXL_qL;
        mtxAUX_qI_[canonicalTraceL.trace] += mtxElemAUXL_qI;

        // get the global DOF mapping for qI on this trace and save for later use
        mapDOFGlobal_qI_[canonicalTraceL.trace].resize(nDOFI);
        qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI_[canonicalTraceL.trace].data(), nDOFI );

      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        //The cell that called this function is to the right of the trace
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );

        // zero AUX element Jacobians
        mtxElemAUXR_qR = 0;
        mtxElemAUXR_qI = 0;

        //Resize the auxiliary jacobian for this trace, since we know the nDOF counts now
        mtxAUX_qI_[canonicalTraceR.trace].resize(nDOFR, nDOFI);
        mtxAUX_qI_[canonicalTraceR.trace] = 0.0;

        // loop over derivative chunks for derivatives wrt qR, aR and qI
        for (int nchunk = 0; nchunk < nEqn*(nDOFR + nDOFI); nchunk += nDeriv)
        {
          // associate derivative slots with solution variables
          int slot, slotOffset = 0;

          // wrt qR
          for (int j = 0; j < nDOFR; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(qfldElemR.DOF(j),n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nEqn*nDOFR;

          // wrt qI
          for (int j = 0; j < nDOFI; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nEqn*nDOFI;

          //------------------------------------------------------------------------------------------------

          for (int n = 0; n < nIntegrandR; n++) rsdElemAUXR[n] = 0;

          // PDE trace integration for canonical element
          integralAUXR( fcn_.integrand_AUX(xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                           xfldElemR, qfldElemR),
                        xfldElemTrace, rsdElemAUXR.data(), nIntegrandR);

          //------------------------------------------------------------------------------------------------

          // accumulate derivatives into element jacobians
          slotOffset = 0;

          // wrt qR
          for (int j = 0; j < nDOFR; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(qfldElemR.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

                //AUXR_qR
                for (int i = 0; i < nDOFR; i++)
                  for (int m = 0; m < nEqn; m++)
                    for (int d = 0; d < D; d++)
                      DLA::index(mtxElemAUXR_qR(i,j)[d],m,n) = DLA::index(rsdElemAUXR[i][d],m).deriv(slot - nchunk);
              }
            } //n loop
          } //j loop
          slotOffset += nEqn*nDOFR;


          // wrt qI
          for (int j = 0; j < nDOFI; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

                //AUXR_qI
                for (int i = 0; i < nDOFR; i++)
                  for (int m = 0; m < nEqn; m++)
                    for (int d = 0; d < D; d++)
                      DLA::index(mtxElemAUXR_qI(i,j)[d],m,n) = DLA::index(rsdElemAUXR[i][d],m).deriv(slot - nchunk);

              }
            } //n loop
          } //j loop
          slotOffset += nEqn*nDOFI;

        } // nchunk loop

        //Accumulate
        mtxAUX_q_                         += mtxElemAUXR_qR;
        mtxAUX_qI_[canonicalTraceR.trace] += mtxElemAUXR_qI;

        // get the global DOF mapping for qI on this trace and save for later use
        mapDOFGlobal_qI_[canonicalTraceR.trace].resize(nDOFI);
        qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI_[canonicalTraceR.trace].data(), nDOFI );

      }
      else
        SANS_DEVELOPER_EXCEPTION("JacobianInteriorTrace_HDG_AuxiliaryVariable_impl::integrate - Invalid configuration!");

    } //elem loop
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;
  MatrixAUXElemClass& mtxAUX_q_; //jacobian of auxiliary residual wrt q of the "central" cell
  std::vector<MatrixAUXElemClass>& mtxAUX_qI_; //jacobians of auxiliary residual wrt qI of all traces around "central" cell
  std::vector<std::vector<int>>& mapDOFGlobal_qI_; //global DOF maps for qI DOFs on all traces around "central" cell

  std::vector<int> traceGroupIndices_;
};


// Factory function

template<class Surreal, class IntegrandInteriorTrace, class VectorMatrixQ>
JacobianInteriorTrace_HDG_AuxiliaryVariable_impl<Surreal, IntegrandInteriorTrace>
JacobianInteriorTrace_HDG_AuxiliaryVariable( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                             const std::map<int,std::vector<int>>& cellITraceGroups,
                                             const int& cellgroup, const int& cellelem,
                                             DLA::MatrixD<VectorMatrixQ>& mtxAUX_q,
                                             std::vector<DLA::MatrixD<VectorMatrixQ>>& mtxAUX_qI,
                                             std::vector<std::vector<int>>& mapDOFGlobal_qI )
{
  return { fcn.cast(), cellITraceGroups, cellgroup, cellelem,
           mtxAUX_q, mtxAUX_qI, mapDOFGlobal_qI };
}

}

#endif  // JACOBIANINTERIORTRACE_HDG_AUXILIARYVARIABLE_H
