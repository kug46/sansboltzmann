// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_GLOBALMAP_H
#define JACOBIANBOUNDARYTRACE_GLOBALMAP_H

// HDG trace integral jacobian functions

#include <vector>

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryTrace.h"
#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_GlobalMap_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_GlobalMap_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianBoundaryTrace_GlobalMap_impl( const IntegrandBoundaryTrace& fcnBTrace,
                                                    std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI ) :
                                                      fcnBTrace_(fcnBTrace),
                                                      mapDOFGlobal_qI_(mapDOFGlobal_qI) {}

  std::size_t nBoundaryGroups() const { return fcnBTrace_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnBTrace_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    SANS_ASSERT( (int) mapDOFGlobal_qI_.size() == qIfld.getXField().nBoundaryTraceGroups() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::
                       template FieldCellGroupType<TopologyL>& qfldCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
//    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
//    typedef typename Field<PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

//    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
//    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;

    //Trace types
//    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

//    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

//    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

//    const int D = PhysDim::D;

    // element field variables
//    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
//    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );

//    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
//    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

//    const int nIntegrandL = nDOFL;

    // variables/equations per DOF
//    const int nEqn = IntegrandBoundaryTrace::N;

    // number of simultaneous derivatives per functor call
//    const int nDeriv = Surreal::N;

    // element integral
//    typedef VectorArrayQSurreal IntegrandType;

    const int nelem = xfldTrace.nElem();

    mapDOFGlobal_qI_[traceGroupGlobal].resize(nelem, std::vector<int>(nDOFI, -1));

    // loop over trace elements
    for (int elem = 0; elem < nelem; elem++)
    {
//      const int elemL = xfldTrace.getElementLeft( elem );
//      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

//      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // get the global DOF mapping for qI on this trace and save for later use
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI_[traceGroupGlobal][elem].data(), nDOFI );

    } //elem loop
  }

protected:
  const IntegrandBoundaryTrace& fcnBTrace_;
  std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI_; //index: [tracegroup][traceelem][dof]
};


// Factory function

template<class Surreal, class IntegrandBoundaryTrace>
JacobianBoundaryTrace_GlobalMap_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_GlobalMap( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcnBTrace,
                                 std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI)
{
  return { fcnBTrace.cast(), mapDOFGlobal_qI };
}

}

#endif  // JACOBIANBOUNDARYTRACE_GLOBALMAP_H
