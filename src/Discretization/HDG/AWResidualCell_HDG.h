// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AWRESIDUALCELL_HDG_H
#define AWRESIDUALCELL_HDG_H

// Cell integral residual functions

#include <memory> //unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/AWIntegrandFunctor_HDG.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Field Cell group residual
//
// topology specific group residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   IntegrandCellFunctor              integrand functor
//   WeightFunction                    analytic expression to weight with

template <class Topology, class PhysDim, class TopoDim,
          class ArrayQ, class VectorArrayQ,
          class PDE, class WeightFunction>
void
AWResidualCell_HDG_Group_Integral(
    const AWIntegrandCell_HDG<PDE, WeightFunction>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfldGroup,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldGroup,
    const typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology>& afldGroup,
    const int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
    SLA::SparseVector<ArrayQ>& rsdAuGlobal,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& efldGroup)
{
  typedef typename XField<PhysDim, TopoDim            >::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;
  typedef typename Field< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
  typedef typename AFieldCellGroupType::template ElementType<> ElementAFieldClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldGroup.basis() );
  ElementQFieldClass qfldElem( qfldGroup.basis() );
  ElementAFieldClass afldElem( afldGroup.basis() );
  ElementQFieldClass efldElem( efldGroup.basis() );

  // number of integrals evaluated
  const int nIntegrand = 1;

  // element integral
  GalerkinWeightedIntegral<TopoDim, Topology, IntegrandHDG<ArrayQ,VectorArrayQ> > integral(quadratureorder, nIntegrand);

  // just to make sure things are consistent
  SANS_ASSERT( xfldGroup.nElem() == qfldGroup.nElem() );
  SANS_ASSERT( xfldGroup.nElem() == afldGroup.nElem() );
  SANS_ASSERT( efldGroup.basis()->order() == 0 ); // checking p0 field

  // residual array
  std::unique_ptr< IntegrandHDG<ArrayQ,VectorArrayQ>[] > rsdElem( new IntegrandHDG<ArrayQ,VectorArrayQ>[nIntegrand] );

  // element-to-global DOF mapping
  std::unique_ptr<int[]> mapDOFGlobal( new int[1] ); // hardcoded 1 given efld is the output

  mapDOFGlobal[0] = -1;

  // loop over elements within group
  const int nelem = xfldGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    //std::cout << "elem:" << elem << std::endl;

    // copy global grid/solution DOFs to element
    xfldGroup.getElement( xfldElem, elem );
    qfldGroup.getElement( qfldElem, elem );
    afldGroup.getElement( afldElem, elem );

    rsdElem[0] = 0; // zero elemental residual

    // cell integration for canonical element
    integral( fcn.integrand(xfldElem, qfldElem, afldElem), xfldElem, rsdElem.get(), nIntegrand );

    // scatter-add element integral to global
    efldGroup.associativity( elem ).getGlobalMapping( mapDOFGlobal.get(), 1 );

    rsdPDEGlobal[mapDOFGlobal[0]] += rsdElem[0].PDE; // map to global rsd PDE vector

    for (int d = 0; d < PhysDim::D; d++)
     rsdAuGlobal[PhysDim::D*mapDOFGlobal[0]+d] += rsdElem[0].Au[d]; // map to global rsdAu vector

  }
}

//----------------------------------------------------------------------------//
template<class TopDim>
class AWResidualCell_HDG;

// base class interface
template<>
class AWResidualCell_HDG<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  template <class PDE, class WeightFunction,
            class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  integrate( const AWIntegrandCell_HDG<PDE,WeightFunction>& fcn,
             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
             int quadratureorder[], int ngroup,
             SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
             SLA::SparseVector<ArrayQ>& rsdAuGlobal )
  {
    SANS_ASSERT( ngroup == afld.nCellGroups() );
    SANS_ASSERT( &qfld.getXField() == &afld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = afld.getXField();

    // Error Est Field - for associativity maps
    Field_DG_Cell<PhysDim, TopoDim, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);
    efld = 0;
    SANS_ASSERT(efld.nDOF() == rsdPDEGlobal.m() ); // checking the rsdFields are P0
    SANS_ASSERT(efld.nDOF()*PhysDim::D == rsdAuGlobal.m() );

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Line) )
      {
        AWResidualCell_HDG_Group_Integral<Line, PhysDim, TopoDim, ArrayQ, VectorArrayQ>( fcn,
                                      xfld.template getCellGroup<Line>(group),
                                      qfld.template getCellGroup<Line>(group),
                                      afld.template getCellGroup<Line>(group),
                                      quadratureorder[group], rsdPDEGlobal, rsdAuGlobal,
                                      efld.template getCellGroup<Line>(group) );
      }
      else
      {
        const char msg[] = "Error in AWResidualCell_HDG<TopoD1>: unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};


template<>
class AWResidualCell_HDG<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class PDE, class WeightFunction,
            class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  integrate( const AWIntegrandCell_HDG<PDE,WeightFunction>& fcn,
             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
             int quadratureorder[], int ngroup,
             SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
             SLA::SparseVector<ArrayQ>& rsdAuGlobal)
  {
    SANS_ASSERT( ngroup == afld.nCellGroups() );
    SANS_ASSERT( &qfld.getXField() == &afld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = afld.getXField();

    // Error Est Field - for associativity maps
    Field_DG_Cell<PhysDim, TopoDim, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);
    efld = 0;
    SANS_ASSERT(efld.nDOF() == rsdPDEGlobal.m() ); // checking the rsdFields are P0
    //std::cout<< "rsdAUGlobal.m():" << rsdAuGlobal.m() << std::endl;
    //std::cout<< "nDOF()*D:" << efld.nDOF()*PhysDim::D << std::endl;
    SANS_ASSERT(efld.nDOF()*PhysDim::D == rsdAuGlobal.m() );

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        AWResidualCell_HDG_Group_Integral<Triangle, PhysDim, TopoDim, ArrayQ, VectorArrayQ>( fcn,
                                      xfld.template getCellGroup<Triangle>(group),
                                      qfld.template getCellGroup<Triangle>(group),
                                      afld.template getCellGroup<Triangle>(group),
                                      quadratureorder[group], rsdPDEGlobal, rsdAuGlobal,
                                      efld.template getCellGroup<Triangle>(group) );
      }
      else
      {
        const char msg[] = "Error in AWResidualCell_HDG<TopoD2>: unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }

  }
};

template<>
class AWResidualCell_HDG<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

  template <class PDE, class WeightFunction,
            class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  integrate( const AWIntegrandCell_HDG<PDE,WeightFunction>& fcn,
             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
             int quadratureorder[], int ngroup,
             SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
             SLA::SparseVector<ArrayQ>& rsdAuGlobal)
  {
    SANS_ASSERT( ngroup == afld.nCellGroups() );
    SANS_ASSERT( &qfld.getXField() == &afld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = afld.getXField();

    // Error Est Field - for associativity maps
    Field_DG_Cell<PhysDim, TopoDim, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);
    efld = 0;
    SANS_ASSERT(efld.nDOF() == rsdPDEGlobal.m() ); // checking the rsdFields are P0
    //std::cout<< "rsdAUGlobal.m():" << rsdAuGlobal.m() << std::endl;
    //std::cout<< "nDOF()*D:" << efld.nDOF()*PhysDim::D << std::endl;

    SANS_ASSERT(efld.nDOF()*PhysDim::D == rsdAuGlobal.m() );

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Tet) )
      {
        AWResidualCell_HDG_Group_Integral<Tet, PhysDim, TopoDim, ArrayQ, VectorArrayQ>( fcn,
                                      xfld.template getCellGroup<Tet>(group),
                                      qfld.template getCellGroup<Tet>(group),
                                      afld.template getCellGroup<Tet>(group),
                                      quadratureorder[group], rsdPDEGlobal, rsdAuGlobal,
                                      efld.template getCellGroup<Tet>(group));
      }
      else
      {
        const char msg[] = "Error in AWResidualCell_HDG<TopoD2>: unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};

}

#endif  // ANALYTICWEIGHTEDRESIDUALCELL_HDG_H
