// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DISCRETIZATIONHDG_H
#define DISCRETIZATIONHDG_H

// HDG discretization:

#include <ostream>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "Discretization/HDG/AuxVarCategory.h"
#include "Discretization/HDG/TauTypeCategory.h"
#include "Field/Tuple/ParamTuple.h"

#ifdef __INTEL_COMPILER
#include "LinearAlgebra/DenseLinAlg/tools/MatrixSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#endif

namespace SANS
{

template<class ArrayQ, class VectorArrayQ>
class IntegrandHDG
{
public:
  ArrayQ PDE;
  VectorArrayQ Au;

  IntegrandHDG() {}

  // This constructor allows for the syntax
  // IntegrandDGBR2<ArrayQ,VectorArrayQ> integrand = 0;
  // cppcheck-suppress noExplicitConstructor
  IntegrandHDG( const int& v ) : PDE(v), Au(v) {}

  // Having explicit constructor avoids the mistake
  // IntegrandHDG<ArrayQ,VectorArrayQ> integrand += Real(a);
  explicit IntegrandHDG( const Real& v ) : PDE(v), Au(v) {}

  IntegrandHDG& operator=( const Real& v )
  {
    PDE = v;
    Au = v;
    return *this;
  }

  IntegrandHDG& operator+=( const IntegrandHDG& v )
  {
    PDE += v.PDE;
    Au += v.Au;
    return *this;
  }
};

template<class ArrayQ, class VectorArrayQ>
IntegrandHDG<ArrayQ,VectorArrayQ> operator*( const Real a, const IntegrandHDG<ArrayQ,VectorArrayQ>& v )
{
  IntegrandHDG<ArrayQ,VectorArrayQ> w;
  w.PDE = a*v.PDE;
  w.Au = a*v.Au;
  return w;
}

//----------------------------------------------------------------------------//
// HDG: controls tau stabilization parameter determination
//
// ctor parameters:
//   pde        PDE type
//   tauType    switch for stabilization options
//              Nothing:  advection contribution only (default)
//              Global:  advection & viscous contributions; length scale is 1
//              Local:  advection & viscous contributions; length scale as input

template <class PDE>
class DiscretizationHDG
{
public:

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // matrices

  typedef DLA::VectorS<PDE::D, Real> VectorX;

  DiscretizationHDG( const PDE& pde, const HDGTauType& tauType = Nothing, const AuxVersion& auxVersion = Gradient, const Real& globallength = 1 ) :
      auxVersion_(auxVersion), pde_(pde), tauType_(tauType), globL_(globallength)
  {
    SANS_ASSERT(globL_ > 0);
  }

  HDGTauType tauType() const { return tauType_; }

  template <class T, class Tp>
  void evalStabilization(
      const Tp& param, const VectorX& N, const Real& lengthscale,
      const ArrayQ<T>& q, MatrixQ<T>& tau ) const;

  //  template <class L, class T>
//  void evalStabilization(
//      const ParamTuple<L, VectorX, TupleClass<>>& param, const VectorX& N, const Real& lengthscale,
//      const ArrayQ<T>& q, MatrixQ<T>& tau ) const
//  {
//    const VectorX& X = param.right();  // Cartesian coordinates of evaluation point
//    evalStabilization(param, N, lengthscale, q, tau);
//  }

  AuxVersion auxVersion_;

private:
  const PDE& pde_;
  HDGTauType tauType_;         // option for tau evaluation
  Real globL_;
};


template <class PDE>
template <class T, class Tp>
void
DiscretizationHDG<PDE>::evalStabilization(
    const Tp& param, const VectorX& N, const Real& lengthscale,
    const ArrayQ<T>& q, MatrixQ<T>& tau ) const
{
  if (tauType_ != Nothing)
  {
    SANS_ASSERT(pde_.fluxViscousLinearInGradient());
    DLA::VectorS<PDE::D,ArrayQ<T>> a(0);        // auxiliary variable (gradient)

    DLA::MatrixS< PDE::D, PDE::D, MatrixQ<T> > K = 0;
#ifdef __INTEL_COMPILER
    // Intel optimization does not properly initiaize K = 0, sigh...
    for (int i = 0; i < PDE::D; i++)
      for (int j = 0; j < PDE::D; j++)
        for (int m = 0; m < DLA::MatrixSize<MatrixQ<T>>::M; m++)
          for (int n = 0; n < DLA::MatrixSize<MatrixQ<T>>::N; n++)
            DLA::index(K(i,j),m,n) = 0;
#endif
    pde_.diffusionViscous( param, q, a, K );

    MatrixQ<T> dudq = 0;
    pde_.jacobianMasterState(param, q, dudq);

    // tau += (kxx*nx*nx + (kxy + kyx)*nx*ny + kyy*ny*ny)*dudq;
    // e.g.
    // tau += n*K*n^T * dudq where n is a row vector

    MatrixQ<T> tmp = 0;
    for ( int i = 0; i < PDE::D; i++ )
      for ( int j = 0; j < PDE::D; j++ )
        tmp += N[i]*K(i,j)*N[j];

//    tmp = DLA::Identity();
//    tmp *= 2.0;

    tau = tmp*dudq;
//    tau = 0;

    // length scale
    if (tauType_ == Global)
    {
      // tau without h-dependence
      tau /= globL_;
      // std::cout<< "G Tau " << tau << " L: " << globL_ << std::endl;
    }
    else if (tauType_ == Local)
    {
      // tau with h-dependence
      tau /= lengthscale;
      // std::cout<< "L Tau " << tau << " L: " << lengthscale << std::endl;
    }
  }

  else // No viscous stabilization
    tau = 0;
}

}

#endif  // DISCRETIZATIONHDG_H
