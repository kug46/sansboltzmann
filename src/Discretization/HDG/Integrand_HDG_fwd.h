// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRAND_HDG_FWD_H
#define INTEGRAND_HDG_FWD_H

namespace SANS
{

//----------------------------------------------------------------------------//
// Tags to indicate the type of an HDG integrand

class HDG {};
class GEDG {};
class HDG_manifold {};
class HDGAdv {};
class HDGAdv_manifold {};

//----------------------------------------------------------------------------//
// Cell integrand: HDG

template <class PDE>
class IntegrandCell_HDG;

//----------------------------------------------------------------------------//
// Trace integrand: HDG

template <class PDE>
class IntegrandInteriorTrace_HDG;

}

#endif
