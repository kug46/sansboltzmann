// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_JACOBIANINTERIORTRACE_HDGADVECTIVE_PARAM_H_
#define SRC_DISCRETIZATION_HDG_JACOBIANINTERIORTRACE_HDGADVECTIVE_PARAM_H_

// interior-trace integral jacobian functions
// construct jacobian of residuals w.r.t. parameters

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/Tuple/SurrealizedElementTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{
//----------------------------------------------------------------------------//
template<class Surreal, int iParam, class IntegrandInteriorTrace, class MatrixQP_>
class JacobianInteriorTrace_HDGAdvective_Param_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_HDGAdvective_Param_impl<Surreal, iParam, IntegrandInteriorTrace, MatrixQP_> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;

  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;           // e.g. solution type
  typedef typename IntegrandInteriorTrace::template ArrayQ<Surreal> ArrayQSurreal; // e.g. integrand Surreal type

  typedef typename DLA::VectorD<ArrayQSurreal> ResidualElemSurrealClass;


  JacobianInteriorTrace_HDGAdvective_Param_impl(const IntegrandInteriorTrace& fcn,
                                                MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p,
                                                MatrixScatterAdd<MatrixQP_>& mtxGlobalINT_p) :
    fcn_(fcn),
    mtxGlobalPDE_p_(mtxGlobalPDE_p),
    mtxGlobalINT_p_(mtxGlobalINT_p) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check(const Field<PhysDim, TopoDim, ArrayQ>& qfld,
             const Field<PhysDim, TopoDim, ArrayQ>& qIfld)
  {
    SANS_ASSERT( mtxGlobalPDE_p_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalINT_p_.m() == qIfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class TupleFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename TupleFieldType                 ::template FieldCellGroupType<TopologyL>& tuplefldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename TupleFieldType                 ::template FieldCellGroupType<TopologyR>& tuplefldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XField<PhysDim, TopoDim>       ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    typedef typename TupleFieldType                 ::template FieldCellGroupType<TopologyL> TupleFieldCellGroupTypeL;
    typedef typename TupleFieldType                 ::template FieldCellGroupType<TopologyR> TupleFieldCellGroupTypeR;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename TupleType<iParam, TupleFieldCellGroupTypeL>::type ParamCellGroupTypeL;
    typedef typename TupleType<iParam, TupleFieldCellGroupTypeR>::type ParamCellGroupTypeR;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeL, Surreal, iParam>::type ElementTupleFieldClassL;
    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeR, Surreal, iParam>::type ElementTupleFieldClassR;

    typedef typename ParamCellGroupTypeL::template ElementType<Surreal> ElementParamFieldClassL;
    typedef typename ParamCellGroupTypeR::template ElementType<Surreal> ElementParamFieldClassR;

    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef typename ElementParamFieldClassL::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP; // jacobian type associated with each DOF

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass; // jacobian type associated with each element

    // field cell groups
    const ParamCellGroupTypeL& paramfldCellL = get<iParam>(tuplefldCellL);
    const ParamCellGroupTypeR& paramfldCellR = get<iParam>(tuplefldCellR);

    // field elements
    ElementTupleFieldClassL tuplefldElemL( tuplefldCellL.basis() );
    ElementTupleFieldClassR tuplefldElemR( tuplefldCellR.basis() );

    ElementParamFieldClassL& paramfldElemL = set<iParam>(tuplefldElemL);
    ElementParamFieldClassR& paramfldElemR = set<iParam>(tuplefldElemR);

    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();
    const int paramDOFL = paramfldElemL.nDOF();
    const int paramDOFR = paramfldElemR.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapQDOFGlobalL(nDOFL, -1);
    std::vector<int> mapQDOFGlobalR(nDOFR, -1);
    std::vector<int> mapDOFGlobalTrace(nDOFI, -1);
    std::vector<int> mapParamDOFGlobalL(paramDOFL, -1);
    std::vector<int> mapParamDOFGlobalR(paramDOFR, -1);

    // element integrand/residuals
    ResidualElemSurrealClass rsdPDEElemL( nDOFL );
    ResidualElemSurrealClass rsdPDEElemR( nDOFR );
    ResidualElemSurrealClass rsdINTElemTraceL( nDOFI );
    ResidualElemSurrealClass rsdINTElemTraceR( nDOFI );

    // element jacobians
    MatrixElemClass mtxPDEElemLL(nDOFL, paramDOFL); // rsdPDE L w.r.t param L
    MatrixElemClass mtxPDEElemLR(nDOFL, paramDOFR); // rsdPDE L w.r.t param R
    MatrixElemClass mtxPDEElemRL(nDOFR, paramDOFL); // rsdPDE R w.r.t param L
    MatrixElemClass mtxPDEElemRR(nDOFR, paramDOFR); // rsdPDE R w.r.t param R

    MatrixElemClass mtxINTElemLL(nDOFI, paramDOFL); // rsdINT L w.r.t param L
    MatrixElemClass mtxINTElemLR(nDOFI, paramDOFR); // rsdINT L w.r.t param R
    MatrixElemClass mtxINTElemRL(nDOFI, paramDOFL); // rsdINT R w.r.t param L
    MatrixElemClass mtxINTElemRR(nDOFI, paramDOFR); // rsdINT R w.r.t param R

    // trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, ResidualElemSurrealClass, ResidualElemSurrealClass>
      integral(quadratureorder);

    // loop over elements within the trace group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobians to zero
      mtxPDEElemLL = 0.0;
      mtxPDEElemLR = 0.0;
      mtxPDEElemRL = 0.0;
      mtxPDEElemRR = 0.0;

      mtxINTElemLL = 0.0;
      mtxINTElemLR = 0.0;
      mtxINTElemRL = 0.0;
      mtxINTElemRR = 0.0;

      // left/right elements
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // get global indexing/mapping
      qfldCellL.associativity( elemL ).getGlobalMapping( mapQDOFGlobalL.data(), nDOFL );
      qfldCellR.associativity( elemR ).getGlobalMapping( mapQDOFGlobalR.data(), nDOFR );
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalTrace.data(), nDOFI );

      paramfldCellL.associativity( elemL ).getGlobalMapping( mapParamDOFGlobalL.data(), paramDOFL );
      paramfldCellR.associativity( elemR ).getGlobalMapping( mapParamDOFGlobalR.data(), paramDOFR );

      // copy global parameter/solution DOFs to element
      tuplefldCellL.getElement( tuplefldElemL, elemL );
      tuplefldCellR.getElement( tuplefldElemR, elemR );

      qfldCellL.getElement( qfldElemL, elemL );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      auto integrandFunctorL = fcn_.integrand( xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                               tuplefldElemL, qfldElemL );
      auto integrandFunctorR = fcn_.integrand( xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                               tuplefldElemR, qfldElemR );

      // number of simultaneous derivatives per integral functor call
      int nDeriv = Surreal::N;

#if 1 //TODO: to be removed for speedup
      SANS_ASSERT_MSG(nArrayP == nDeriv, "Parameter size should match surreal size");
#endif

      // loop over derivative chunks (i.e. each entry of each parameter DOF in both left and right elements)
      for (int nchunk = 0; nchunk < nArrayP*(paramDOFL + paramDOFR); nchunk += nDeriv)
      {
        // associate derivative slots with parameter DOFs

        int slot = 0, slotOffset = 0;
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemL.DOF(j),n).deriv(slot - nchunk) = 1.0;
          }
        }

        slotOffset += paramDOFL*nArrayP;
        for (int j = 0; j < paramDOFR; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemR.DOF(j),n).deriv(slot - nchunk) = 1.0;
          }
        }

        // reset, evaluate elemental residuals
        rsdPDEElemL = 0.0;
        rsdPDEElemR = 0.0;
        integral(integrandFunctorL, xfldElemTrace, rsdPDEElemL, rsdINTElemTraceL);

        rsdINTElemTraceL = 0.0;
        rsdINTElemTraceR = 0.0;
        integral(integrandFunctorR, xfldElemTrace, rsdPDEElemR, rsdINTElemTraceR);

        // accumulate derivatives into element jacobian

        slotOffset = 0;
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElemL.DOF(j),n).deriv(slot - nchunk) = 0.0; // Reset the derivative in Surreal

              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemLL(i,j),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxINTElemLL(i,j),m,n) = DLA::index(rsdINTElemTraceL[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFR; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemRL(i,j),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxINTElemRL(i,j),m,n) = DLA::index(rsdINTElemTraceR[i],m).deriv(slot - nchunk);
            }
          }
        }

        slotOffset += paramDOFL*nArrayP;
        for (int j = 0; j < paramDOFR; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = slotOffset + nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElemR.DOF(j),n).deriv(slot - nchunk) = 0; // Reset the derivative in Surreal

              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemLR(i,j),m,n) = DLA::index(rsdPDEElemL[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxINTElemLR(i,j),m,n) = DLA::index(rsdINTElemTraceL[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFR; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemRR(i,j),m,n) = DLA::index(rsdPDEElemR[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxINTElemRR(i,j),m,n) = DLA::index(rsdINTElemTraceR[i],m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobians to global jacobians
      mtxGlobalPDE_p_.scatterAdd( mtxPDEElemLL,
                                  mapQDOFGlobalL.data(), mapQDOFGlobalL.size(),
                                  mapParamDOFGlobalL.data(), mapParamDOFGlobalL.size() );
      mtxGlobalPDE_p_.scatterAdd( mtxPDEElemLR,
                                  mapQDOFGlobalL.data(), mapQDOFGlobalL.size(),
                                  mapParamDOFGlobalR.data(), mapParamDOFGlobalR.size() );

      mtxGlobalPDE_p_.scatterAdd( mtxPDEElemRL,
                                  mapQDOFGlobalR.data(), mapQDOFGlobalR.size(),
                                  mapParamDOFGlobalL.data(), mapParamDOFGlobalL.size() );
      mtxGlobalPDE_p_.scatterAdd( mtxPDEElemRR,
                                  mapQDOFGlobalR.data(), mapQDOFGlobalR.size(),
                                  mapParamDOFGlobalR.data(), mapParamDOFGlobalR.size() );

      mtxGlobalINT_p_.scatterAdd( mtxINTElemLL,
                                  mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size(),
                                  mapParamDOFGlobalL.data(), mapParamDOFGlobalL.size() );
      mtxGlobalINT_p_.scatterAdd( mtxINTElemLR,
                                  mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size(),
                                  mapParamDOFGlobalR.data(), mapParamDOFGlobalR.size() );

      mtxGlobalINT_p_.scatterAdd( mtxINTElemRL,
                                  mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size(),
                                  mapParamDOFGlobalL.data(), mapParamDOFGlobalL.size() );
      mtxGlobalINT_p_.scatterAdd( mtxINTElemRR,
                                  mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size(),
                                  mapParamDOFGlobalR.data(), mapParamDOFGlobalR.size() );
    } // elem loop
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalINT_p_;
};

// Factory function

template<class Surreal, int iParam, class IntegrandInteriorTrace, class MatrixQP>
JacobianInteriorTrace_HDGAdvective_Param_impl<Surreal, iParam, IntegrandInteriorTrace, MatrixQP>
JacobianInteriorTrace_HDGAdvective_Param( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                          MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p,
                                          MatrixScatterAdd<MatrixQP>& mtxGlobalINT_p)
{
  return { fcn.cast(), mtxGlobalPDE_p, mtxGlobalINT_p };
}

} // namespace SANS

#endif /* SRC_DISCRETIZATION_HDG_JACOBIANINTERIORTRACE_HDGADVECTIVE_PARAM_H_ */
