// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_RESIDUALBOUNDARYTRACE_DISPATCH_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_RESIDUALBOUNDARYTRACE_DISPATCH_HDGADVECTIVE_H_

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

#include "ResidualBoundaryTrace_FieldTrace_HDGAdvective.h"
#include "ResidualBoundaryTrace_HDGAdvective.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace variables, e.g. Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class QAFieldBundleType,
         class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ>
class ResidualBoundaryTrace_FieldTrace_Dispatch_HDGAdvective_impl
{
public:
  ResidualBoundaryTrace_FieldTrace_Dispatch_HDGAdvective_impl(
    const XFieldType& xfld,
    const QAFieldBundleType& qafldBundle,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
    const int* quadratureorder, int ngroup,
    Vector<ArrayQ>& rsdPDEGlobal,
    Vector<ArrayQ>& rsdINTGlobal,
    Vector<ArrayQ>& rsdBCGlobal ) :
      xfld_(xfld), qafldBundle_(qafldBundle), qIfld_(qIfld), lgfld_(lgfld),
      quadratureorder_(quadratureorder),
      ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal),
      rsdINTGlobal_(rsdINTGlobal),
      rsdBCGlobal_(rsdBCGlobal)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      ResidualBoundaryTrace_FieldTrace_HDGAdvective(fcn, rsdPDEGlobal_, rsdINTGlobal_, rsdBCGlobal_),
      xfld_, qafldBundle_, (qIfld_, lgfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const QAFieldBundleType& qafldBundle_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdINTGlobal_;
  Vector<ArrayQ>& rsdBCGlobal_;
};

// Factory function

template<class XFieldType, class QAFieldBundleType,
         class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_FieldTrace_Dispatch_HDGAdvective_impl<XFieldType, QAFieldBundleType, PhysDim, TopoDim, Vector, ArrayQ>
ResidualBoundaryTrace_FieldTrace_Dispatch_HDGAdvective(
    const XFieldType& xfld,
    const QAFieldBundleType& qafldBundle,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
    const int* quadratureorder, int ngroup,
    Vector<ArrayQ>& rsdPDEGlobal,
    Vector<ArrayQ>& rsdINTGlobal,
    Vector<ArrayQ>& rsdBCGlobal)
{
  return ResidualBoundaryTrace_FieldTrace_Dispatch_HDGAdvective_impl<XFieldType, QAFieldBundleType, PhysDim, TopoDim, Vector, ArrayQ>(
      xfld, qafldBundle, qIfld, lgfld, quadratureorder, ngroup, rsdPDEGlobal, rsdINTGlobal, rsdBCGlobal);
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class QAFieldBundleType,
         class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ>
class ResidualBoundaryTrace_Dispatch_HDGAdvective_impl
{
public:
  ResidualBoundaryTrace_Dispatch_HDGAdvective_impl(
    const XFieldType& xfld,
    const QAFieldBundleType& qafldBundle,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const int* quadratureorder, int ngroup,
    Vector<ArrayQ>& rsdPDEGlobal,
    Vector<ArrayQ>& rsdINTGlobal) :
      xfld_(xfld),
      qafldBundle_(qafldBundle),
      qIfld_(qIfld),
      quadratureorder_(quadratureorder),
      ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal),
      rsdINTGlobal_(rsdINTGlobal)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      ResidualBoundaryTrace_HDGAdvective(fcn, rsdPDEGlobal_, rsdINTGlobal_),
      xfld_, qafldBundle_, qIfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const QAFieldBundleType& qafldBundle_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdINTGlobal_;
};

// Factory function

template<class XFieldType, class QAFieldBundleType,
         class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_Dispatch_HDGAdvective_impl<XFieldType, QAFieldBundleType, PhysDim, TopoDim, Vector, ArrayQ>
ResidualBoundaryTrace_Dispatch_HDGAdvective( const XFieldType& xfld,
                                    const QAFieldBundleType& qafldBundle,
                                    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                    const int* quadratureorder, int ngroup,
                                    Vector<ArrayQ>& rsdPDEGlobal,
                                    Vector<ArrayQ>& rsdINTGlobal)
{
  return ResidualBoundaryTrace_Dispatch_HDGAdvective_impl<XFieldType, QAFieldBundleType, PhysDim, TopoDim, Vector, ArrayQ>(
      xfld, qafldBundle, qIfld, quadratureorder, ngroup, rsdPDEGlobal, rsdINTGlobal);
}

} // namespace SANS

#endif /* SRC_DISCRETIZATION_HDG_RESIDUALBOUNDARYTRACE_DISPATCH_HDGADVECTIVE_H_ */
