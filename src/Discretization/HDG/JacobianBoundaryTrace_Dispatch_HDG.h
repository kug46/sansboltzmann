// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DISPATCH_HDG_H
#define JACOBIANBOUNDARYTRACE_DISPATCH_HDG_H

// boundary-trace integral jacobian functions

#include "JacobianBoundaryTrace_FieldTrace_HDG.h"
#include "JacobianBoundaryTrace_HDG.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class MatrixQ>
class JacobianBoundaryTrace_FieldTrace_Dispatch_HDG_impl
{
public:
  JacobianBoundaryTrace_FieldTrace_Dispatch_HDG_impl(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                             const int* quadratureorder, int ngroup,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalINT_lg,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalBC_qI,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg ) :
    xfld_(xfld),
    qfld_(qfld),
    afld_(afld),
    qIfld_(qIfld),
    lgfld_(lgfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    mtxGlobalPDE_q_(mtxGlobalPDE_q),
    mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
    mtxGlobalPDE_lg_(mtxGlobalPDE_lg),
    mtxGlobalINT_q_(mtxGlobalINT_q),
    mtxGlobalINT_qI_(mtxGlobalINT_qI),
    mtxGlobalINT_lg_(mtxGlobalINT_lg),
    mtxGlobalBC_q_(mtxGlobalBC_q),
    mtxGlobalBC_qI_(mtxGlobalBC_qI),
    mtxGlobalBC_lg_(mtxGlobalBC_lg)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_FieldTrace_HDG<Surreal>(fcn,
                                                      mtxGlobalPDE_q_, mtxGlobalPDE_qI_, mtxGlobalPDE_lg_,
                                                      mtxGlobalINT_q_, mtxGlobalINT_qI_, mtxGlobalINT_lg_,
                                                      mtxGlobalBC_q_ , mtxGlobalBC_qI_ , mtxGlobalBC_lg_ ),
        xfld_, (qfld_, afld_), (qIfld_,lgfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class MatrixQ>
JacobianBoundaryTrace_FieldTrace_Dispatch_HDG_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, MatrixQ>
JacobianBoundaryTrace_FieldTrace_Dispatch_HDG(const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                              const int* quadratureorder, int ngroup,
                                              MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                              MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                              MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
                                              MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                              MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI,
                                              MatrixScatterAdd<MatrixQ>& mtxGlobalINT_lg,
                                              MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
                                              MatrixScatterAdd<MatrixQ>& mtxGlobalBC_qI,
                                              MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg  )
{
  return { xfld, qfld, afld, qIfld, lgfld, quadratureorder, ngroup,
           mtxGlobalPDE_q, mtxGlobalPDE_qI, mtxGlobalPDE_lg,
           mtxGlobalINT_q, mtxGlobalINT_qI, mtxGlobalINT_lg,
           mtxGlobalBC_q , mtxGlobalBC_qI , mtxGlobalBC_lg };
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class IntegrandCell, class IntegrandITrace, class XFieldCellToTrace,
         class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class MatrixQ, class TensorMatrixQ, class VectorMatrixQ>
class JacobianBoundaryTrace_Dispatch_HDG_impl
{
public:
  JacobianBoundaryTrace_Dispatch_HDG_impl(const IntegrandCell& fcnCell,
                                          const IntegrandITrace& fcnTraceAUX,
                                          const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                                          const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                                          const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                                          const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                                          const XFieldCellToTrace& xfldCellToTrace,
                                          const XFieldType& xfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                          const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                          const int quadOrderCell[], const int nCellGroup,
                                          const int quadOrderITrace[], const int nITraceGroup,
                                          const int quadOrderBTrace[], const int nBTraceGroup,
                                          MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                          MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                          MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                          MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI ) :
                                            fcnCell_(fcnCell), fcnITrace_(fcnTraceAUX),
                                            invJacAUX_a_bcell_(invJacAUX_a_bcell),
                                            jacAUX_q_bcell_(jacAUX_q_bcell), jacAUX_qI_btrace_(jacAUX_qI_btrace),
                                            mapDOF_boundary_qI_(mapDOF_boundary_qI), xfldCellToTrace_(xfldCellToTrace),
                                            xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
                                            quadOrderCell_(quadOrderCell), nCellGroup_(nCellGroup),
                                            quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup),
                                            quadOrderBTrace_(quadOrderBTrace), nBTraceGroup_(nBTraceGroup),
                                            mtxGlobalPDE_q_(mtxGlobalPDE_q),
                                            mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
                                            mtxGlobalINT_q_(mtxGlobalINT_q),
                                            mtxGlobalINT_qI_(mtxGlobalINT_qI)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcnBTrace)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_HDG<Surreal>( fcnCell_, fcnITrace_, fcnBTrace,
                                            invJacAUX_a_bcell_, jacAUX_q_bcell_, jacAUX_qI_btrace_,
                                            mapDOF_boundary_qI_, xfldCellToTrace_,
                                            xfld_, qfld_, afld_, qIfld_,
                                            quadOrderCell_, nCellGroup_,
                                            quadOrderITrace_, nITraceGroup_,
                                            mtxGlobalPDE_q_, mtxGlobalPDE_qI_,
                                            mtxGlobalINT_q_, mtxGlobalINT_qI_),
        xfld_, (qfld_, afld_), qIfld_, quadOrderBTrace_, nBTraceGroup_ );
  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell_;
  const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell_;
  const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace_;
  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI_;
  const XFieldCellToTrace& xfldCellToTrace_;

  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;

  const int *quadOrderCell_, nCellGroup_;
  const int *quadOrderITrace_, nITraceGroup_;
  const int *quadOrderBTrace_, nBTraceGroup_;

  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;
};

// Factory function

template<class Surreal, class IntegrandCell, class IntegrandITrace, class XFieldCellToTrace,
         class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class MatrixQ, class TensorMatrixQ, class VectorMatrixQ>
JacobianBoundaryTrace_Dispatch_HDG_impl<Surreal, IntegrandCell, IntegrandITrace, XFieldCellToTrace,
                                        XFieldType, PhysDim, TopoDim,
                                        ArrayQ, VectorArrayQ, MatrixQ, TensorMatrixQ, VectorMatrixQ>
JacobianBoundaryTrace_Dispatch_HDG(
    const IntegrandCellType<IntegrandCell>& fcnCell,
    const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
    const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
    const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
    const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
    const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
    const XFieldCellToTrace& xfldCellToTrace,
    const XFieldType& xfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const int quadOrderCell[], const int nCellGroup,
    const int quadOrderITrace[], const int nITraceGroup,
    const int quadOrderBTrace[], const int nBTraceGroup,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q, MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
    MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q, MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI)
{
  return { fcnCell.cast(), fcnITrace.cast(),
           invJacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOF_boundary_qI,
           xfldCellToTrace, xfld, qfld, afld, qIfld,
           quadOrderCell, nCellGroup,
           quadOrderITrace, nITraceGroup,
           quadOrderBTrace, nBTraceGroup,
           mtxGlobalPDE_q, mtxGlobalPDE_qI,
           mtxGlobalINT_q, mtxGlobalINT_qI };
}

}

#endif //JACOBIANBOUNDARYTRACE_DISPATCH_HDG_H
