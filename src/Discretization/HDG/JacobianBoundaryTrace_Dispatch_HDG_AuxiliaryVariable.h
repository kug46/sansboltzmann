// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DISPATCH_HDG_AUXILIARYVARIABLE_H
#define JACOBIANBOUNDARYTRACE_DISPATCH_HDG_AUXILIARYVARIABLE_H

// boundary-trace integral jacobian functions

#include "JacobianBoundaryTrace_HDG_AuxiliaryVariable.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
//#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class VectorMatrixQ>
class JacobianBoundaryTrace_Dispatch_HDG_AuxiliaryVariable_impl
{
public:
  JacobianBoundaryTrace_Dispatch_HDG_AuxiliaryVariable_impl(const XFieldType& xfld,
                                                            const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                            const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                                            const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                                            const int quadOrderBTrace[], const int nBTraceGroup,
                                                            FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                                                            FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                                                            std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI ) :
                                                              xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
                                                              quadOrderBTrace_(quadOrderBTrace), nBTraceGroup_(nBTraceGroup),
                                                              jacAUX_q_bcell_(jacAUX_q_bcell), jacAUX_qI_btrace_(jacAUX_qI_btrace),
                                                              mapDOFGlobal_qI_(mapDOFGlobal_qI)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcnBTrace)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_HDG_AuxiliaryVariable<Surreal>( fcnBTrace, jacAUX_q_bcell_, jacAUX_qI_btrace_, mapDOFGlobal_qI_ ),
        xfld_, (qfld_, afld_), qIfld_, quadOrderBTrace_, nBTraceGroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;

  const int *quadOrderBTrace_, nBTraceGroup_;

  FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell_;
  FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace_;
  std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class VectorMatrixQ>
JacobianBoundaryTrace_Dispatch_HDG_AuxiliaryVariable_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, VectorMatrixQ>
JacobianBoundaryTrace_Dispatch_HDG_AuxiliaryVariable(
    const XFieldType& xfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const int quadOrderBTrace[], const int nBTraceGroup,
    FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
    FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
    std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI )
{
  return { xfld, qfld, afld, qIfld,
           quadOrderBTrace, nBTraceGroup,
           jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI };
}

}

#endif //JACOBIANBOUNDARYTRACE_DISPATCH_HDG_AUXILIARYVARIABLE_H
