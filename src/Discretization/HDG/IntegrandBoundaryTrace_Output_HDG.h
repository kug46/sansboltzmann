// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_HDG_SANSLG_OUTPUT_H
#define INTEGRANDBOUNDARYTRACE_HDG_SANSLG_OUTPUT_H

// boundary output functional for HDG

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/call_derived_functor.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/HDG/DiscretizationHDG.h"

#include "pde/OutputCategory.h"
#include "pde/call_derived_functional.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary output integrand

template <class PDE_, class NDOutputVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::Functional>, HDG> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::Functional>, HDG> >
{
public:
  typedef PDE_ PDE;
  typedef OutputCategory::Functional Category;
  typedef HDG DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the transpose Jacobian of this functional
  template<class T>
  using MatrixJ = typename PDE::template ArrayQ<T>;

  explicit IntegrandBoundaryTrace( const OutputBase& outputFcn, const std::vector<int>& BoundaryGroups )
    : outputFcn_(outputFcn), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,
                    class ElementParam, class BCIntegrandBoundaryTrace>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>      , TopoDimCell , TopologyL    > ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell , TopologyL    > ElementAFieldL;
    typedef Element<ArrayQ<T>      , TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

//    typedef typename BCIntegrandBoundaryTrace::template FieldWeighted<T, Real, TopoDimTrace,TopologyTrace,
//                                                                        TopoDimCell,TopologyL,ElementParam> BCFieldWeight;

    Functor( const OutputBase& outputFcn,
             const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementParam& paramfldElem,
             const ElementQFieldL& qfldElem,
             const ElementAFieldL& afldElem,
             const ElementQFieldTrace& qIfldTrace ) :
             outputFcn_(outputFcn),
             xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
             paramfldElem_(paramfldElem), xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
             qfldElem_(qfldElem), afldElem_(afldElem), qIfldTrace_(qIfldTrace),
             nDOFElem_(qfldElem_.nDOF()),
             phi_(nDOFElem_)
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFElem() const { return nDOFElem_; }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, ArrayJ<T>& integrand ) const
    {
      QuadPointCellType sRefCell;

      TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTrace_, sRefTrace, sRefCell );

      ArrayQ<T> qL = 0;   // value
      VectorArrayQ<T> aL; // gradient

      VectorX N = 0;
      ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

      // unit normal: points out of domain
      xfldElemTrace_.unitNormal( sRefTrace, N );

#if 0
      // basis value, gradient
      qfldElem_.evalBasis( sRefCell, phi_.data(), phi_.size() );

      // solution value, gradient
      qfldElem_.evalFromBasis( phi_.data(), phi_.size(), qL );
      afldElem_.eval( phi_.data(), phi_.size(), aL );
#else
      // solution value, gradient
      qIfldTrace_.eval( sRefTrace, qL );
      afldElem_.eval( sRefCell, aL );
#endif

      call_derived_functional<NDOutputVector>(outputFcn_, param, N, qL, aL, integrand);
    }

  protected:
    // const DiscretizationHDG<PDE>& disc_;
    const OutputBase& outputFcn_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementParam& paramfldElem_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
    const ElementAFieldL& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;

//    const BCFieldWeight fwBC_;

    const int nDOFElem_;
    mutable std::vector<Real> phi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell, class ElementParam, class BCIntegrandBoundaryTrace>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell, ElementParam, BCIntegrandBoundaryTrace >
  integrand(const BCIntegrandBoundaryTrace& fcnBC,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // XField must be the last parameter
            const Element<ArrayQ<T>      , TopoDimCell, TopologyCell >& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyCell >& afldElem,
            const Element<ArrayQ<T>,     TopoDimTrace, TopologyTrace >& qIfldTrace) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyCell, ElementParam, BCIntegrandBoundaryTrace>
       (outputFcn_, xfldElemTrace, canonicalTrace, paramfldElem, qfldElem, afldElem, qIfldTrace);
  }

private:
  // const DiscretizationHDG<PDE>& disc_;
  const OutputBase& outputFcn_;
  const std::vector<int> BoundaryGroups_;
};

}

#endif  // INTEGRANDBOUNDARYTRACE_HDG_SANSLG_OUTPUT_H
