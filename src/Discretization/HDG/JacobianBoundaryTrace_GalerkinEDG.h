// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_GALERKINEDG_H
#define JACOBIANBOUNDARYTRACE_GALERKINEDG_H

// HDG trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryTrace.h"
#include "Field/CellToTraceAssociativity.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Discretization/HDG/JacobianBoundaryCell_HDG_AuxiliaryVariable.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary trace group integral without lagrange multipliers
//

template<class Surreal, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class XFieldCellToTrace, class XFieldClass, class TopoDim_>
class JacobianBoundaryTrace_GalerkinEDG_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_GalerkinEDG_impl<Surreal, IntegrandCell, IntegrandITrace, IntegrandBTrace,
                                                                          XFieldCellToTrace, XFieldClass, TopoDim_> >
{
public:
  typedef typename IntegrandBTrace::PhysDim PhysDim;
  typedef typename IntegrandBTrace::PDE PDE;

  static_assert( std::is_same< PhysDim, typename IntegrandCell::PhysDim >::value, "PhysDim should be the same.");
  static_assert( std::is_same< PhysDim, typename IntegrandITrace::PhysDim >::value, "PhysDim should be the same.");

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianBoundaryTrace_GalerkinEDG_impl( const IntegrandCell& fcnCell,
                                  const IntegrandITrace& fcnITrace,
                                  const IntegrandBTrace& fcnBTrace,
                                  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                                  const XFieldCellToTrace& xfldCellToTrace,
                                  const XFieldClass& xfld,
                                  const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                                  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld,
                                  const int quadOrderCell[], const int nCellGroup,
                                  const int quadOrderITrace[], const int nITraceGroup,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI ) :
                                    fcnCell_(fcnCell), fcnITrace_(fcnITrace), fcnBTrace_(fcnBTrace),
                                    mapDOF_boundary_qI_(mapDOF_boundary_qI), xfldCellToTrace_(xfldCellToTrace),
                                    xfld_(xfld), qfld_(qfld), qIfld_(qIfld),
                                    quadOrderCell_(quadOrderCell), nCellGroup_(nCellGroup),
                                    quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup),
                                    mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
                                    mtxGlobalINT_q_(mtxGlobalINT_q), mtxGlobalINT_qI_(mtxGlobalINT_qI) {}

  std::size_t nBoundaryGroups() const { return fcnBTrace_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnBTrace_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the matrices
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalPDE_qI_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_qI_.n() == qIfld.nDOF() );

    SANS_ASSERT( mtxGlobalINT_q_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalINT_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalINT_qI_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalINT_qI_.n() == qIfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<>        ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    const int nIntegrandL = nDOFL;
    const int nIntegrandI = nDOFI;

    // variables/equations per DOF
    const int nEqn = IntegrandBTrace::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFGlobal_qI(nDOFI, -1);

    typedef ArrayQSurreal CellIntegrandType;
    typedef ArrayQSurreal TraceIntegrandType;

    // element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, TraceIntegrandType>
      integral(quadratureorder, nDOFL, nDOFI);

    // element integrand/residual
    std::vector<CellIntegrandType> rsdElemPDEL( nIntegrandL );
    std::vector<TraceIntegrandType> rsdElemINT( nIntegrandI );

    // element jacobian matrices
    // Left
    MatrixElemClass mtxElemPDEL_qL(nDOFL, nDOFL);
    MatrixElemClass mtxElemINT_qL(nDOFI, nDOFL);

    MatrixAElemClass mtxElemPDEL_aL(nDOFL, nDOFL);
    MatrixAElemClass mtxElemINT_aL(nDOFI, nDOFL);

    // element PDE jacobian matrices wrt qI on each trace of the "central" cell
    std::vector<MatrixElemClass> mtxElemPDEL_qI(TopologyL::NTrace, MatrixElemClass(0,0));
    std::vector<MatrixElemClass> mtxElemINT_qI (TopologyL::NTrace, MatrixElemClass(0,0));

    //qI global map
    std::vector<std::vector<int>> mapDOFGlobal_qI_cell(TopologyL::NTrace);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      for (int trace = 0; trace < TopologyL::NTrace; trace++)
      {
        mapDOFGlobal_qI_cell[trace].clear();

        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobalL, elemL, trace);

        if (traceinfo.type == TraceInfo::Boundary)
        {
          //This trace is a boundary trace, so save its {tracegroup, elem} info
          if ( mapDOF_boundary_qI_[traceinfo.group].size() > 0 )
          {
            //Copy the globalDOFmap for qI DOFs on this boundary trace
            mapDOFGlobal_qI_cell[trace] = mapDOF_boundary_qI_[traceinfo.group][traceinfo.elem];
          }
        }
      }

      // check if the DOF count for this trace is consistent
      SANS_ASSERT( (int) mapDOFGlobal_qI_cell[canonicalTraceL.trace].size() == nDOFI );

      //---------------------------------------------------------------------------------------------

      //Now we can compute the PDE and INT jacobians from this boundary trace, and complete their chain rules.

      // zero element Jacobians
      mtxElemPDEL_qL = 0; mtxElemPDEL_aL = 0;
      mtxElemINT_qL = 0; mtxElemINT_aL = 0;

      for (int trace = 0; trace < (int) TopologyL::NTrace; trace++)
      {
        mtxElemPDEL_qI[trace].resize( nDOFL, (int) mapDOFGlobal_qI_cell[trace].size() );
        mtxElemINT_qI [trace].resize( nDOFI, (int) mapDOFGlobal_qI_cell[trace].size() );

        mtxElemPDEL_qI[trace] = 0;
        mtxElemINT_qI [trace] = 0;
      }

      // loop over derivative chunks for derivatives wrt qL, aL and qI
      for (int nchunk = 0; nchunk < nEqn*(nDOFL + D*nDOFL + nDOFI); nchunk += nDeriv)
      {
        // associate derivative slots
        int slot, slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFI;

        //------------------------------------------------------------------------------------------------

        for (int n = 0; n < nIntegrandL; n++) rsdElemPDEL[n] = 0;
        for (int n = 0; n < nIntegrandI; n++) rsdElemINT[n] = 0;

        // trace integration
        integral( fcnBTrace_.integrand(xfldElemTrace, canonicalTraceL,
                                       xfldElemL, qfldElemL, qIfldElemTrace),
                  xfldElemTrace,
                  rsdElemPDEL.data(), nIntegrandL,
                  rsdElemINT.data(), nIntegrandI);

        //------------------------------------------------------------------------------------------------

        // accumulate derivatives into element jacobians
        slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              //PDEL_qL
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemPDEL_qL(i,j),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);

              //INT_qL
              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemINT_qL(i,j),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
            }
          } //n loop
        } //j loop
        slotOffset += nEqn*nDOFL;

        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 0;  // Reset the derivative

              //PDEL_qI
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemPDEL_qI[canonicalTraceL.trace](i,j),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);

              //INT_qI
              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemINT_qI[canonicalTraceL.trace](i,j),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
            }
          } //n loop
        } //j loop
        slotOffset += nEqn*nDOFI;

      } // nchunk loop

      // scatter-add element jacobian to global
      scatterAdd( qIfldTrace, qfldCellL,
                  elem, elemL,
                  mapDOFGlobal_qL.data(), nDOFL,
                  mapDOFGlobal_qI.data(), nDOFI,
                  mapDOFGlobal_qI_cell,

                  mtxElemPDEL_qL, mtxElemPDEL_qI,
                  mtxElemINT_qL , mtxElemINT_qI,

                  mtxGlobalPDE_q_, mtxGlobalPDE_qI_,
                  mtxGlobalINT_q_, mtxGlobalINT_qI_);
    }
  }


//----------------------------------------------------------------------------//
  template <class QFieldTraceGroupType, class QFieldCellGroupTypeL,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldTraceGroupType& qIfldTrace,
      const QFieldCellGroupTypeL& qfldCellL,
      const int elem, const int elemL,
      int mapDOFGlobal_qL[], const int nDOFL,
      int mapDOFGlobal_qI[], const int nDOFI,
      const std::vector<std::vector<int>>& mapDOFGlobal_qI_cell,

      const MatrixElemClass& mtxElemPDEL_qL,
      const std::vector<MatrixElemClass>& mtxElemPDEL_qI,
      const MatrixElemClass& mtxElemINT_qL,
      const std::vector<MatrixElemClass>& mtxElemINT_qI,

      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalINT_q,
      SparseMatrixType<MatrixQ>& mtxGlobalINT_qI)
  {
    // global mapping for qL
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    // global mapping for qI
    qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI, nDOFI );

    // jacobian wrt qL
    mtxGlobalPDE_q.scatterAdd( mtxElemPDEL_qL, mapDOFGlobal_qL, nDOFL );
    mtxGlobalINT_q.scatterAdd( mtxElemINT_qL , mapDOFGlobal_qI, nDOFI, mapDOFGlobal_qL, nDOFL );

    // jacobians wrt qI (including on traces of the central cell)
    for (int trace = 0; trace < (int) mapDOFGlobal_qI_cell.size(); trace++)
    {
      if (mapDOFGlobal_qI_cell[trace].size() > 0)  //some boundary-trace groups might have been omitted
      {
        mtxGlobalPDE_qI.scatterAdd( mtxElemPDEL_qI[trace], mapDOFGlobal_qL, nDOFL,
                                    mapDOFGlobal_qI_cell[trace].data(), mapDOFGlobal_qI_cell[trace].size() );
        mtxGlobalINT_qI.scatterAdd( mtxElemINT_qI[trace], mapDOFGlobal_qI, nDOFI,
                                    mapDOFGlobal_qI_cell[trace].data(), mapDOFGlobal_qI_cell[trace].size() );
      }
    }
  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const IntegrandBTrace& fcnBTrace_;
  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI_;

  const XFieldCellToTrace& xfldCellToTrace_;
  const XFieldClass& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld_;

  const int *quadOrderCell_, nCellGroup_;
  const int *quadOrderITrace_, nITraceGroup_;

  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;
};

// Factory function

template<class Surreal, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class XFieldCellToTrace, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class MatrixQ>
JacobianBoundaryTrace_GalerkinEDG_impl<Surreal, IntegrandCell, IntegrandITrace, IntegrandBTrace,
                               XFieldCellToTrace, XFieldType, TopoDim>
JacobianBoundaryTrace_GalerkinEDG( const IntegrandCellType<IntegrandCell>& fcnCell,
                           const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                           const IntegrandBoundaryTraceType<IntegrandBTrace>& fcnBTrace,
                           const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                           const XFieldCellToTrace& xfldCellToTrace,
                           const XFieldType& xfld,
                           const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                           const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                           const int quadOrderCell[], const int nCellGroup,
                           const int quadOrderITrace[], const int nITraceGroup,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI)
{
  return { fcnCell.cast(), fcnITrace.cast(), fcnBTrace.cast(), mapDOF_boundary_qI,
           xfldCellToTrace, xfld, qfld, qIfld,
           quadOrderCell, nCellGroup,
           quadOrderITrace, nITraceGroup,
           mtxGlobalPDE_q, mtxGlobalPDE_qI,
           mtxGlobalINT_q, mtxGlobalINT_qI };
}

}

#endif  // JACOBIANBOUNDARYTRACE_GALERKINEDG_H
