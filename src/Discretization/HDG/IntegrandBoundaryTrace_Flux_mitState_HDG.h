// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_HDG_H
#define INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_HDG_H

// boundary integrand operators

#include <ostream>
#include <vector>
#include  <type_traits> //std::is_same

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#ifdef DEBUG
  #include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
  #include "LinearAlgebra/DenseLinAlg/tools/index.h"
#endif

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{

// forward declaration
class BCTypeReflect_mitState;

//----------------------------------------------------------------------------//
// HDG element boundary integrand: State

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, HDG> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, HDG> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Flux_mitState Category;
  typedef HDG DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(
    const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const DiscretizationHDG<PDE>& disc) :
    pde_(pde), bc_(bc),  BoundaryGroups_(BoundaryGroups), disc_(disc) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  virtual ~IntegrandBoundaryTrace() {}

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementAFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandCellType;
    typedef ArrayQ<T> IntegrandTraceType;

    BasisWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem,
                   const ElementQFieldTrace& qIfldTrace ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem), afldElem_(afldElem), qIfldTrace_(qIfldTrace),
                   paramfldElem_( paramfldElem ),
                   nDOFElem_(qfldElem_.nDOF()),
                   nDOFTrace_(qIfldTrace_.nDOF()),
                   phi_( new Real[nDOFElem_] ),
                   phiT_( new Real [nDOFTrace_] ),
                   gradphi_( new VectorX[nDOFElem_] )
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    BasisWeighted( BasisWeighted&& bw ) :
                   pde_(bw.pde_), disc_(bw.disc_), bc_(bw.bc_),
                   xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                   xfldElem_(bw.xfldElem_),
                   qfldElem_(bw.qfldElem_), afldElem_(bw.afldElem_), qIfldTrace_(bw.qIfldTrace_),
                   paramfldElem_( bw.paramfldElem_ ),
                   nDOFElem_( bw.nDOFElem_ ),
                   nDOFTrace_( bw.nDOFTrace_ ),
                   phi_( bw.phi_ ),
                   phiT_( bw.phiT_ ),
                   gradphi_( bw.gradphi_ )
    {
      bw.phi_ = nullptr; bw.phiT_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] phiT_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCell[],  const int neqnCell,
                                                          ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandCell, neqnCell,
                                                              integrandTrace, neqnTrace );
    }

    FRIEND_CALL_WITH_DERIVED

  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandCell[], const int neqnCell,
                                                                       ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable Real* phi_;
    mutable Real* phiT_;
    mutable VectorX* gradphi_;
  };


  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted_AUX
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<      ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Ta>, TopoDimCell, Topology> ElementAFieldCell;
    typedef Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Ta>::type Ts;
    typedef typename ElementParam::T ParamT;

    typedef VectorArrayQ<Ts> IntegrandType;

    BasisWeighted_AUX( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                       const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                       const ElementParam& paramfldElem,
                       const ElementQFieldCell& qfldElem,
                       const ElementAFieldCell& afldElem,
                       const ElementQFieldTrace& qIfldTrace ) :
                         pde_(pde), disc_(disc), bc_(bc),
                         xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                         xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                         qfldElem_(qfldElem), afldElem_(afldElem),
                         qIfldTrace_(qIfldTrace),
                         paramfldElem_( paramfldElem ),
                         nDOFElem_(qfldElem_.nDOF()),
                         nDOFTrace_(qIfldTrace_.nDOF()),
                         phi_( new Real[nDOFElem_] ),
                         phiT_( new Real [nDOFTrace_] )
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    BasisWeighted_AUX( BasisWeighted_AUX&& bw ) :
                         pde_(bw.pde_), disc_(bw.disc_), bc_(bw.bc_),
                         xfldElemTrace_(bw.xfldElemTrace_), canonicalTrace_(bw.canonicalTrace_),
                         xfldElem_(bw.xfldElem_),
                         qfldElem_(bw.qfldElem_), afldElem_(bw.afldElem_),
                         qIfldTrace_(bw.qIfldTrace_),
                         paramfldElem_( bw.paramfldElem_ ),
                         nDOFElem_(bw.nDOFElem_),
                         nDOFTrace_(bw.nDOFTrace_),
                         phi_( bw.phi_ ),
                         phiT_( bw.phiT_ )
    {
      bw.phi_ = nullptr; bw.phiT_ = nullptr;
    }

    ~BasisWeighted_AUX()
    {
      delete [] phi_;
      delete [] phiT_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandL, neqnL );
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable Real* phi_;
    mutable Real* phiT_;
  };


  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>      , TopoDimCell, Topology      > ElementQFieldCell;
    typedef Element<VectorArrayQ<Tq>, TopoDimCell, Topology      > ElementAFieldCell;
    typedef Element<ArrayQ<Tq>      , TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef Element<ArrayQ<Tw>      , TopoDimCell, Topology      > ElementWFieldCell;
    typedef Element<VectorArrayQ<Tw>, TopoDimCell, Topology      > ElementBFieldCell;
    typedef Element<ArrayQ<Tw>      , TopoDimTrace, TopologyTrace> ElementWFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type Ti;

    typedef IntegrandHDG< Ti, Ti > IntegrandCellType;
    typedef Ti IntegrandTraceType;

    FieldWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem,
                   const ElementWFieldCell& wfldElem,
                   const ElementBFieldCell& bfldElem,
                   const ElementQFieldTrace& qIfldTrace,
                   const ElementWFieldTrace& wIfldTrace  ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem), afldElem_(afldElem),
                   wfldElem_(wfldElem), bfldElem_(bfldElem),
                   qIfldTrace_(qIfldTrace), wIfldTrace_(wIfldTrace),
                   paramfldElem_( paramfldElem ),
                   nDOFElem_(qfldElem_.nDOF()),
                   nDOFTrace_(qIfldTrace_.nDOF()),
                   nDOFElemW_(wfldElem_.nDOF()),
                   nDOFTraceW_(wIfldTrace_.nDOF()),
                   phi_( new Real[nDOFElem_] ),
                   wphi_( new Real[nDOFElemW_] )
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == bfldElem_.basis() );
    }

    FieldWeighted( FieldWeighted&& fw ) :
                   pde_(fw.pde_), disc_(fw.disc_), bc_(fw.bc_),
                   xfldElemTrace_(fw.xfldElemTrace_), canonicalTrace_(fw.canonicalTrace_),
                   xfldElem_(fw.xfldElem_),
                   qfldElem_(fw.qfldElem_), afldElem_(fw.afldElem_),
                   wfldElem_(fw.wfldElem_), bfldElem_(fw.bfldElem_),
                   qIfldTrace_(fw.qIfldTrace_), wIfldTrace_(fw.wIfldTrace_),
                   paramfldElem_( fw.paramfldElem_ ),
                   nDOFElem_( fw.nDOFElem_ ),
                   nDOFTrace_( fw.nDOFTrace_ ),
                   nDOFElemW_( fw.nDOFElemW_ ),
                   nDOFTraceW_( fw.nDOFTraceW_ ),
                   phi_( fw.phi_ ),
                   wphi_( fw.wphi_ )
    {
      fw.phi_ = nullptr; fw.wphi_ = nullptr;
    }

    ~FieldWeighted()
    {
      delete [] phi_;
      delete [] wphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCell,
                                                          IntegrandTraceType& integrandTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandCell, integrandTrace);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandCellType& integrandCell,
                                                                       IntegrandTraceType& integrandTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementBFieldCell& bfldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementWFieldTrace& wIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
    const int nDOFElemW_;
    const int nDOFTraceW_;
    mutable Real* phi_;
    mutable Real* wphi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>,       TopoDimCell, Topology >& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell, Topology >& afldElem,
            const Element<ArrayQ<T>,       TopoDimTrace, TopologyTrace>& qIfldTrace) const
  {
    return {pde_, disc_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem, qfldElem, afldElem, qIfldTrace};
  }

  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted_AUX<Tq, Ta, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand_AUX(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                const ElementParam& paramfldElem,
                const Element<ArrayQ<Tq>, TopoDimCell, Topology >& qfldElem,
                const Element<VectorArrayQ<Ta>, TopoDimCell, Topology >& afldElem,
                const Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace >& qIfldTrace) const
  {
    return {pde_, disc_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem, qfldElem, afldElem, qIfldTrace };
  }

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<Tq>,       TopoDimCell, Topology >& qfldElem,
            const Element<VectorArrayQ<Tq>, TopoDimCell, Topology >& afldElem,
            const Element<ArrayQ<Tw>,       TopoDimCell, Topology >& wfldElem,
            const Element<VectorArrayQ<Tw>, TopoDimCell, Topology >& bfldElem,
            const Element<ArrayQ<Tq>,       TopoDimTrace, TopologyTrace>& qIfldTrace,
            const Element<ArrayQ<Tw>,       TopoDimTrace, TopologyTrace>& wIfldTrace) const
  {
    return { pde_, disc_, bc_,
             xfldElemTrace, canonicalTrace,
             paramfldElem,
             qfldElem, afldElem,
             wfldElem, bfldElem,
             qIfldTrace, wIfldTrace};
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const DiscretizationHDG<PDE>& disc_;
};


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, HDG>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCell[],  const int neqnCell,
                                                              ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const
{
  SANS_ASSERT_MSG( !(std::is_same<typename BC::BCType, BCTypeReflect_mitState>::value), "Not tested for reflection BC yet" );

  SANS_ASSERT( (neqnCell == nDOFElem_) && (neqnTrace == nDOFTrace_) );

  ParamT paramI;              // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nL;                  // unit normal (points out of domain)

  ArrayQ<T> qL;               // solution
  VectorArrayQ<T> aL;         // auxiliary variable (gradient)
  ArrayQ<T> qI;               // interface solution

  QuadPointCellType sRef;     // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  paramfldElem_.eval( sRef, paramI );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, nL );
  VectorX nB = -nL;

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOFElem_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOFElem_, qL );
  afldElem_.evalFromBasis( phi_, nDOFElem_, aL );

  // interface solution basis
  qIfldTrace_.evalBasis( sRefTrace, phiT_, nDOFTrace_ );

  // interface solution
  qIfldTrace_.evalFromBasis( phiT_, nDOFTrace_ , qI );

  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOFElem_ );

  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] = 0;

  // PDE residual: weak form boundary integral, cell to interface state
  // < w, F >
  ArrayQ<Ti> FnL = 0;

  // advective flux
  if (pde_.hasFluxAdvective() )
  {
    pde_.fluxAdvectiveUpwind( paramI, qL, qI, nL, FnL );
  }

  // viscous flux
  if ( pde_.hasFluxViscous() )
  {
    VectorArrayQ<Ti> Fv = 0;     // PDE viscous flux
    pde_.fluxViscous( paramI, qL, aL, Fv );
    FnL += dot(nL,Fv);
  }

  // HDG stabilization
  MatrixQ<T> tauL = 0;
//  Real lengthL = xfldElem_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant()/Real(qfldElem_.order())/Real(qfldElem_.order())/8.;
  Real lengthL = xfldElem_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();

  disc_.evalStabilization( paramI, nL, lengthL, qI, tauL );

  // HDG stabilization term: tauL*(q - qI)
  FnL += tauL*(qL - qI);

  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] += phi_[k]*FnL;

  // Interface Residual:   <w^, [[F]] > ==  < w^, FL.n^+ + FB.n^- >
  // must be a jump in flux, defined using interior flux and the boundary flux
  // the minus is for dual consistency TODO: Need to show this exactly -- Hugh

  // get the boundary state
  ArrayQ<T> qB = 0;
  bc.state( paramI, nL, qI, qB );

  // normal flux
  ArrayQ<Ti> FnB;

  // TODO: What should the stabilization tau be computed with? qI or qB?
  MatrixQ<T> tauR = 0;
  disc_.evalStabilization( paramI, nB, lengthL, qB, tauR );

  // must add stabilization before because fluxNormal might remove
  // Stabilization based on nL pointing out of domain
  FnB = tauR*(qI - qB);
  bc.fluxNormal( paramI, nL, qI, aL, qB, FnB );

  for (int k = 0; k < neqnTrace; k++)
    integrandTrace[k] = -phiT_[k]*(FnL - FnB);
}


template <class PDE, class NDBCVector>
template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class Topology, class ElementParam >
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, HDG>::
BasisWeighted_AUX<Tq,Ta,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL) const
{
  SANS_ASSERT_MSG( !(std::is_same<typename BC::BCType, BCTypeReflect_mitState>::value), "Not tested for reflection BC yet" );
  SANS_ASSERT( (neqnL == nDOFElem_) );

  ParamT paramI;              // Elemental parameters (such as grid coordinates and distance functions)
  QuadPointCellType sRef;     // reference-element coordinates (s,t)
  VectorX nL;

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );
  paramfldElem_.eval( sRef, paramI );

  ArrayQ<Tq> qL;               // solution
  ArrayQ<Tq> qI;               // interface solution
  VectorArrayQ<Ta> aL;

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, nL );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOFElem_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOFElem_ , qL );
  afldElem_.evalFromBasis( phi_, nDOFElem_ , aL );

  // interface solution basis
  qIfldTrace_.evalBasis( sRefTrace, phiT_, nDOFTrace_ );

  // interface solution
  qIfldTrace_.evalFromBasis( phiT_, nDOFTrace_, qI );

  ArrayQ<Tq> qB = 0;
  bc.state( paramI, nL, qI, qB );

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  if (pde_.hasFluxViscous())
  {

    DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<Ti>> K = 0;  // diffusion matrix
    pde_.diffusionViscous( paramI, qL, aL, K ); // boundary state, interior gradient

    MatrixQ<Tq> dudq = 0; // conservation variable jacobians dU(Q)/dQ
    pde_.jacobianMasterState( paramI, qL, dudq );

    if (disc_.auxVersion_ == Gradient)
    {
      // - (phiL nL)*qB
      VectorArrayQ<Tq> qBnL;

      for (int d = 0; d < PhysDim::D; d++)
        qBnL[d] = qB*nL[d];

      for (int k = 0; k < neqnL; k++)
        integrandL[k] = -phi_[k]*qBnL;
    }
    else if (disc_.auxVersion_ == AugGradient)
    {
      // - (phiL nL)*qB
      VectorArrayQ<Tq> qBnL;

      for (int d = 0; d < PhysDim::D; d++)
        qBnL[d] = qB*nL[d];

      VectorArrayQ<Tq> dun;
      for (int i = 0; i < PhysDim::D; i++)
        dun[i] = dudq*qBnL[i];

      VectorArrayQ<Ti> tmpB2 = K*dun;

      for (int k = 0; k < neqnL; k++)
        integrandL[k] = -phi_[k]*tmpB2;
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandBoundaryTrace_Flux_mitState::BasisWeighted_AUX::operator() - Unknown auxiliary variable!" );


    // viscous flux dual consistency term
    //jump in q
//    VectorArrayQ<Tq> dqn;
//
//    for (int d = 0; d < PhysDim::D; d++)
//      dqn[d] = nL[d]*(qB - qL);
//
//    VectorArrayQ<Tq> dunB;
//    for (int i = 0; i < PhysDim::D; i++)
//      dunB[i] = dudq*dqn[i];
//
//    VectorArrayQ<Ti> tmpB = K*dunB;
//
//    for (int k = 0; k < neqnL; k++)
//      integrandL[k] += phi_[k]*tmpB;

  }


}


template <class PDE, class NDBCVector>
template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class Topology, class ElementParam >
template<class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, HDG>::
FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCell,
                                                              IntegrandTraceType& integrandTrace ) const
{
  ParamT paramI;              // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;                 // unit normal (points out of domain)

  ArrayQ<Tq> qL;              // solution
  VectorArrayQ<Tq> aL;        // auxiliary variable (gradient)
  ArrayQ<Tq> qI;              // interface solution

  ArrayQ<Tw> wL;              // weight
  VectorArrayQ<Tw> bL;        // weight auxiliary variable (gradient)
  ArrayQ<Tw> wI;              // interface weight

  QuadPointCellType sRef;     // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  paramfldElem_.eval( sRef, paramI );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, nL );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOFElem_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOFElem_ , qL );
  afldElem_.evalFromBasis( phi_, nDOFElem_ , aL );

  // interface solution
  qIfldTrace_.eval( sRefTrace, qI );

  // basis value, gradient
  wfldElem_.evalBasis( sRef, wphi_,nDOFElemW_ );

  // solution value, gradient
  wfldElem_.evalFromBasis( wphi_, nDOFElemW_, wL );
  bfldElem_.evalFromBasis( wphi_, nDOFElemW_, bL );

  // interface weight
  wIfldTrace_.eval( sRefTrace, wI );

  // get the boundary state
  ArrayQ<Tq> qB = 0;
  bc.state( paramI, nL, qI, qB );

  // PDE residual: weak form boundary integral, cell to interface state
  // < w, F >
  integrandCell = 0;

  ArrayQ<Tq> FnL = 0;

  // advective flux
  if (pde_.hasFluxAdvective() )
  {
    pde_.fluxAdvectiveUpwind( paramI, qL, qI, nL, FnL );
  }

  if ( pde_.hasFluxViscous() )
  {
    VectorArrayQ<Tq> Fv = 0;     // PDE viscous flux
    pde_.fluxViscous( paramI, qL, aL, Fv );
    FnL += dot(nL,Fv);
  }

  MatrixQ<Tq> tauL = 0;
//  Real lengthL = xfldElem_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant()/Real(qfldElem_.order())/Real(qfldElem_.order())/8.;
  Real lengthL = xfldElem_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  disc_.evalStabilization( paramI, nL, lengthL, qI, tauL );

  FnL += tauL*(qL - qI);

  integrandCell.PDE += dot(wL,FnL);

  // AUX residual
  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient)
    {
      // - (phiL nL)*qB
      VectorArrayQ<Tq> qBnL;

      for (int d = 0; d < PhysDim::D; d++)
        qBnL[d] = qB*nL[d];

      integrandCell.Au = -dot(bL, qBnL);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandBoundaryTrace_Flux_mitState::FieldWeighted::operator() - Unknown auxiliary variable!" );
  }


  // Interface Residual:  <w^, [[F]] > == < w^, FL.n^+ + FB.n^- >
  // must be a jump in flux, defined using interior flux and the boundary flux
  // the minus is for dual consistency TODO: Need to show this exactly -- Hugh

  // normal flux
  ArrayQ<Tq> FnB;

  // must add stabilization before because fluxNormal might remove
  // Stabilization based on nL pointing out of domain
  FnB = tauL*(qI - qB);
  bc.fluxNormal( paramI, nL, qI, aL, qB, FnB );

  integrandTrace = -dot(wI,(FnL - FnB));
}

}

#endif  // INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_HDG_H
