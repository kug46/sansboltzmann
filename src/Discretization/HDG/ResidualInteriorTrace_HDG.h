// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALINTERIORTRACE_HDG_H
#define RESIDUALINTERIORTRACE_HDG_H

// HDG interior-trace integral residual functions

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "tools/Tuple.h"

#include "DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class IntegrandInteriorTrace, template<class> class Vector>
class ResidualInteriorTrace_HDG_impl :
    public GroupIntegralInteriorTraceType< ResidualInteriorTrace_HDG_impl<IntegrandInteriorTrace, Vector> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualInteriorTrace_HDG_impl( const IntegrandInteriorTrace& fcn,
                                  const std::map<int,std::vector<int>>& cellITraceGroups,
                                  const int& cellgroup, const int& cellelem,
                                  DLA::VectorD<ArrayQ>& rsdPDEElemCell,
                                  Vector<ArrayQ>& rsdINTGlobal) :
    fcn_(fcn), cellITraceGroups_(cellITraceGroups),
    cellgroup_(cellgroup), cellelem_(cellelem),
    rsdPDEElemCell_(rsdPDEElemCell), rsdINTGlobal_(rsdINTGlobal)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    SANS_ASSERT( rsdINTGlobal_.m() == qIfld.nDOFpossessed() );

    comm_rank_ = qIfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<> ElementAFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> AFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename AFieldCellGroupTypeR::template ElementType<> ElementAFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const AFieldCellGroupTypeR& afldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldClassL afldElemL( afldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementAFieldClassR afldElemR( afldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFTrace = qIfldElemTrace.nDOF();
    const int nIntegrandL = nDOFL;
    const int nIntegrandR = nDOFR;
    const int nIntegrandTrace = nDOFTrace;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalTrace( nIntegrandTrace, -1 );
    std::vector<int> mapDOFLocalTrace( nIntegrandTrace, -1 );
    std::vector<int> maprsd( nIntegrandTrace, -1 );
    int nDOFLocalTrace = 0;
    const int nDOFpossessed = rsdINTGlobal_.m();

    // trace element integral
    typedef ArrayQ CellIntegrandType;
    typedef ArrayQ TraceIntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, TraceIntegrandType>
      integralTraceL(quadratureorder, nIntegrandL, nIntegrandTrace);

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, TraceIntegrandType>
      integralTraceR(quadratureorder, nIntegrandR, nIntegrandTrace);

    // element integrand/residuals
    DLA::VectorD<CellIntegrandType> rsdPDEElemL( nIntegrandL );
    DLA::VectorD<CellIntegrandType> rsdPDEElemR( nIntegrandR );
    DLA::VectorD<TraceIntegrandType> rsdINTElemTrace( nIntegrandTrace );

    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      //Add INT residuals from this trace to global residual vector
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size() );

      // copy over the map for each qI DOF that is possessed by this processor
      nDOFLocalTrace = 0;
      for (int n = 0; n < nIntegrandTrace; n++)
      {
        if (mapDOFGlobalTrace[n] < nDOFpossessed)
        {
          maprsd[nDOFLocalTrace] = n;
          mapDOFLocalTrace[nDOFLocalTrace] = mapDOFGlobalTrace[n];
          nDOFLocalTrace++;
        }
      }

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        //The cell that called this function is to the left of the trace
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        afldCellL.getElement( afldElemL, elemL );

        // only possessed cell elements have PDE residuals on this processor
        if ( qfldElemL.rank() != comm_rank_ && nDOFLocalTrace == 0 ) continue;

        for (int n = 0; n < nIntegrandL; n++) rsdPDEElemL[n] = 0;
        for (int n = 0; n < nIntegrandTrace; n++) rsdINTElemTrace[n] = 0;

        integralTraceL( fcn_.integrand(xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                       xfldElemL, qfldElemL, afldElemL),
                        xfldElemTrace, rsdPDEElemL.data(), nIntegrandL, rsdINTElemTrace.data(), nIntegrandTrace);

        rsdPDEElemCell_ += rsdPDEElemL;
      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        //The cell that called this function is to the right of the trace
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        afldCellR.getElement( afldElemR, elemR );

        // only possessed cell elements have PDE residuals on this processor
        if ( qfldElemR.rank() != comm_rank_ && nDOFLocalTrace == 0 ) continue;

        for (int n = 0; n < nIntegrandR; n++) rsdPDEElemR[n] = 0;
        for (int n = 0; n < nIntegrandTrace; n++) rsdINTElemTrace[n] = 0;

        integralTraceR( fcn_.integrand(xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                       xfldElemR, qfldElemR, afldElemR),
                        xfldElemTrace, rsdPDEElemR.data(), nIntegrandR, rsdINTElemTrace.data(), nIntegrandTrace);

        rsdPDEElemCell_ += rsdPDEElemR;
      }
      else
        SANS_DEVELOPER_EXCEPTION("ResidualInteriorTrace_HDG_impl::integrate - Invalid configuration!");

      for (int n = 0; n < nDOFLocalTrace; n++)
      {
        int nGlobal = mapDOFLocalTrace[n];
        rsdINTGlobal_[nGlobal] += rsdINTElemTrace[n];
      }
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;

  DLA::VectorD<ArrayQ>& rsdPDEElemCell_;
  Vector<ArrayQ>& rsdINTGlobal_;

  std::vector<int> traceGroupIndices_;

  mutable int comm_rank_;
};


// Factory function
template<class IntegrandInteriorTrace, template<class> class Vector, class ArrayQ>
ResidualInteriorTrace_HDG_impl<IntegrandInteriorTrace, Vector>
ResidualInteriorTrace_HDG( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                           const std::map<int,std::vector<int>>& cellITraceGroups,
                           const int& cellgroup, const int& cellelem,
                           DLA::VectorD<ArrayQ>& rsdPDEElemCell,
                           Vector<ArrayQ>& rsdINTGlobal)
{
  static_assert( std::is_same< ArrayQ, typename IntegrandInteriorTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualInteriorTrace_HDG_impl<IntegrandInteriorTrace, Vector>(fcn.cast(), cellITraceGroups,
                                                                        cellgroup, cellelem, rsdPDEElemCell, rsdINTGlobal);
}

}

#endif  // RESIDUALINTERIORTRACE_HDG_H
