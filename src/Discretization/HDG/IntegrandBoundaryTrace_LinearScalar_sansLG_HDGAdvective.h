// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_LINEARSCALAR_SANSLG_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_LINEARSCALAR_SANSLG_HDGADVECTIVE_H_

#include <ostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// HDG element boundary integrand: LinearScalar_sansLG

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, HDGAdv> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, HDGAdv> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef HDGAdv DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal; // Auxilliary integrand

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(
    const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const DiscretizationHDG<PDE>& disc) :
    pde_(pde), bc_(bc),  BoundaryGroups_(BoundaryGroups), disc_(disc) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  virtual ~IntegrandBoundaryTrace() {}

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<T>,       TopoDimCell, Topology> ElementQFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandCellType;
    typedef ArrayQ<T> IntegrandTraceType;

    BasisWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementQFieldTrace& qIfldTrace ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem),
                   qIfldTrace_(qIfldTrace),
                   paramfldElem_( paramfldElem ),
                   nDOFElem_(qfldElem_.nDOF()),
                   nDOFTrace_(qIfldTrace_.nDOF())
    {
      phi_.resize(nDOFElem_,0);
      phiT_.resize(nDOFTrace_,0);
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, dJ, sRefTrace, rsdPDEElemL, rsdINTElemTrace );
    }

    FRIEND_CALL_WITH_DERIVED

  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const Real dJ, const QuadPointTraceType& RefTrace,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> phiT_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>,       TopoDimCell,  Topology     >& qfldElem,
            const Element<ArrayQ<T>,       TopoDimTrace, TopologyTrace>& qIfldTrace) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, Topology, ElementParam>(pde_, disc_, bc_,
                                                                                              xfldElemTrace, canonicalTrace,
                                                                                              paramfldElem, qfldElem,
                                                                                              qIfldTrace);
  }


private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const DiscretizationHDG<PDE>& disc_;
};


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam>
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, HDGAdv>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const Real dJ, const QuadPointTraceType& sRefTrace,
           DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemeneted yet!"); //TODO

#if 0
  SANS_ASSERT( (neqnCell == nDOFElem_) && (neqnTrace == nDOFTrace_) );

  VectorX X;                // physical coordinates
  VectorX N;                // unit normal (points out of domain)

  ArrayQ<T> q;              // solution
  VectorArrayQ<T> a;        // auxiliary variable (gradient)
  ArrayQ<T> qI;             // interface solution

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, N );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

  // interface solution basis
  qIfldTrace_.evalBasis( sRefTrace, phiT_.data(), phiT_.size() );

  // interface solution
  qIfldTrace_.evalFromBasis( phiT_.data(), phiT_.size(), qI );

  // BC coefficients (convert to scalars)
  MatrixQ<Real> Amtx, Bmtx;
  bc.coefficients( X, N, Amtx, Bmtx );
  Real A = Amtx;
  Real B = Bmtx;

  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] = 0;

  ArrayQ<Ti> Fn = 0;

  // PDE residual: weak form boundary integral
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0; // PDE flux

    // advective flux
    pde_.fluxAdvective( X, q, F );

    // viscous flux
    pde_.fluxViscous( X, q, a, F );

    Fn += dot(N,F);
  }

  // HDG stabilization term: tau*(q - qI)
  MatrixQ<T> tau = 0;
  Real length = xfldElem_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  disc_.evalStabilization( X, N, length, qI, tau );

  ArrayQ<Ti> Fstab = tau*(q - qI);

  Fn += Fstab;

  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] += phi_[k]*Fn;

  for (int k = 0; k < neqnTrace; k++)
    integrandTrace[k] = -phiT_[k]*Fstab;

  // PDE residual: BC Lagrange multiplier term

  T jan; // NOTE: use of Real rather than ArrayQ/MatrixQ to as this only works for scalar equations

  DLA::VectorS< PhysDim::D, T > ja = 0;
  pde_.jacobianFluxAdvective( X, q, ja );
  jan = dot(N,ja);

  DLA::MatrixS< PhysDim::D, PhysDim::D, Ti > K = 0;
  pde_.diffusionViscous( X, q, a, K );

//  DLA::VectorS< PhysDim::D, T > Kn = K*N;
  VectorArrayQ<Ti> Ka = K*a;
  ArrayQ<Ti> KaN = dot(Ka,N);

  Real denomin = A*A + B*B;
  Ti alpha = (A*jan - B)/denomin;
//  T beta = - A/denomin;

  // BC data (convert to scalar)
  Real bcdata;
  bc.data( X, N, bcdata );
  Ti tmp = A*qI + B*KaN - bcdata;

  Ti tmpPDE = -alpha*tmp;

  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] += phi_[k]*tmpPDE;
#endif
}

}

#endif /* SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_LINEARSCALAR_SANSLG_HDGADVECTIVE_H_ */
