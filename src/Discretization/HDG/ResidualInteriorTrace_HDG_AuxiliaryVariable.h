// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALINTERIORTRACE_HDG_AUXILIARYVARIABLE_H
#define RESIDUALINTERIORTRACE_HDG_AUXILIARYVARIABLE_H

// Computes a part of the HDG auxiliary equation residual over interior trace groups

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class IntegrandInteriorTrace>
class ResidualInteriorTrace_HDG_AuxiliaryVariable_impl :
    public GroupIntegralInteriorTraceType< ResidualInteriorTrace_HDG_AuxiliaryVariable_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualInteriorTrace_HDG_AuxiliaryVariable_impl( const IntegrandInteriorTrace& fcn,
                                                    const std::map<int,std::vector<int>>& cellITraceGroups,
                                                    const int& cellgroup, const int& cellelem,
                                                    DLA::VectorD<VectorArrayQ>& rsdAUXElemCell) :
                                                      fcn_(fcn), cellBTraceGroups_(cellITraceGroups),
                                                      cellgroup_(cellgroup), cellelem_(cellelem),
                                                      rsdAUXElemCell_(rsdAUXElemCell)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellBTraceGroups_.begin(); it != cellBTraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const {}

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<> ElementAFieldClassL;

    // Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> AFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename AFieldCellGroupTypeR::template ElementType<> ElementAFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
          AFieldCellGroupTypeL& afldCellL = const_cast<AFieldCellGroupTypeL&>( get<1>(fldsCellL) );

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
          AFieldCellGroupTypeR& afldCellR = const_cast<AFieldCellGroupTypeR&>( get<1>(fldsCellR) );

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldClassL afldElemL( afldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementAFieldClassR afldElemR( afldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // number of integrals evaluated per element
    const int nDOFL = afldElemL.nDOF();
    const int nIntegrandL = nDOFL;

    const int nDOFR = afldElemR.nDOF();
    const int nIntegrandR = nDOFR;

    // trace element integral
    typedef VectorArrayQ IntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, IntegrandType> integralTraceL(quadratureorder, nIntegrandL);
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, IntegrandType> integralTraceR(quadratureorder, nIntegrandR);

    // element integrand/residuals
    DLA::VectorD<IntegrandType> rsdAUXTraceL( nIntegrandL );
    DLA::VectorD<IntegrandType> rsdAUXTraceR( nIntegrandR );

    const std::vector<int>& traceElemList = cellBTraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        //The cell that called this function is to the left of the trace
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );

        for (int n = 0; n < nIntegrandL; n++) rsdAUXTraceL[n] = 0;

        integralTraceL( fcn_.integrand_AUX(xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                           xfldElemL, qfldElemL),
                        xfldElemTrace, rsdAUXTraceL.data(), nIntegrandL);

        rsdAUXElemCell_ += rsdAUXTraceL;
      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        //The cell that called this function is to the right of the trace
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );

        for (int n = 0; n < nIntegrandR; n++) rsdAUXTraceR[n] = 0;

        integralTraceR( fcn_.integrand_AUX(xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                           xfldElemR, qfldElemR),
                        xfldElemTrace, rsdAUXTraceR.data(), nIntegrandR);

        rsdAUXElemCell_ += rsdAUXTraceR;
      }
      else
        SANS_DEVELOPER_EXCEPTION("ResidualInteriorTrace_HDG_AuxiliaryVariable_impl::integrate - Invalid configuration!");
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  const std::map<int,std::vector<int>>& cellBTraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;

  DLA::VectorD<VectorArrayQ>& rsdAUXElemCell_;

  std::vector<int> traceGroupIndices_;
};


// Factory function

template<class IntegrandInteriorTrace, class VectorArrayQ>
ResidualInteriorTrace_HDG_AuxiliaryVariable_impl<IntegrandInteriorTrace>
ResidualInteriorTrace_HDG_AuxiliaryVariable(
    const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
    const std::map<int,std::vector<int>>& cellITraceGroups,
    const int& cellgroup, const int& cellelem,
    DLA::VectorD<VectorArrayQ>& rsdAUXElemCell )
{
  return ResidualInteriorTrace_HDG_AuxiliaryVariable_impl<IntegrandInteriorTrace>(
      fcn.cast(), cellITraceGroups, cellgroup, cellelem, rsdAUXElemCell);
}

}

#endif  // RESIDUALINTERIORTRACE_HDG_AUXILIARYVARIABLE_H
