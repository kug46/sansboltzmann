// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELD_HDG_AUXILIARYVARIABLE_H
#define SETFIELD_HDG_AUXILIARYVARIABLE_H

// Computes the HDG auxiliary variable over cellgroups

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/FieldData/FieldDataVectorD_BoundaryCell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/Element/ElementalMassMatrix.h"
#include "Field/CellToTraceAssociativity.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Discretization/HDG/ResidualInteriorTrace_HDG_AuxiliaryVariable.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral
//

template<class IntegrandCell, class IntegrandTrace, class XFieldCellToTrace,
         class XFieldClass, class TopoDim_>
class SetFieldCell_HDG_AuxiliaryVariable_impl :
    public GroupIntegralCellType< SetFieldCell_HDG_AuxiliaryVariable_impl<IntegrandCell, IntegrandTrace, XFieldCellToTrace,
                                                                          XFieldClass, TopoDim_> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  static_assert( std::is_same< PhysDim, typename IntegrandTrace::PhysDim >::value, "PhysDim should be the same.");

  // Save off the boundary trace integrand and the residual vectors
  SetFieldCell_HDG_AuxiliaryVariable_impl( const IntegrandCell& fcnCell, const IntegrandTrace& fcnTrace,
                                           const FieldDataInvMassMatrix_Cell& mmfld,
                                           const XFieldCellToTrace& xfldCellToTrace,
                                           const XFieldClass& xfld,
                                           const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                                           const Field<PhysDim, TopoDim_, VectorArrayQ>& afld,
                                           const Field<PhysDim, TopoDim_, ArrayQ>& qIfld,
                                           const int quadOrderITrace[], const int nITraceGroup,
                                           const FieldDataVectorD_BoundaryCell<VectorArrayQ>& rsdAUX_bcell,
                                           const std::vector<int>& cellgroupsAux, const bool updateAux,
                                           FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& jacAUX_a_bcell ) :
                                             fcnCell_(fcnCell), fcnTrace_(fcnTrace),
                                             mmfld_(mmfld), xfldCellToTrace_(xfldCellToTrace),
                                             xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
                                             quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup),
                                             rsdAUX_bcell_(rsdAUX_bcell),
                                             updateAux_(updateAux),
                                             jacAUX_a_bcell_(jacAUX_a_bcell),
                                             comm_rank_(afld.comm()->rank())
  {
    for (std::size_t i = 0; i < cellgroupsAux.size(); i++)
    {
      bool found = false;
      for (std::size_t j = 0; j < fcnCell_.nCellGroups(); j++)
      {
        if ( cellgroupsAux[i] == (int) fcnCell_.cellGroup(j) )
        {
          active_cellgroups_.push_back(cellgroupsAux[i]);
          found = true;
          break;
        }
      }
      if (!found)
        SANS_DEVELOPER_EXCEPTION("SetFieldCell_HDG_AuxiliaryVariable_impl - Cellgroup %d does not exist in fcnCell!", cellgroupsAux[i] );
    }
  }

  std::size_t nCellGroups() const { return active_cellgroups_.size(); }
  std::size_t cellGroup(const int n) const { return active_cellgroups_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
              Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const {}

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                            ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename AFieldCellGroupType::template ElementType<> ElementAFieldClass;

    const int D = PhysDim::D;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
          AFieldCellGroupType& afldCell = const_cast<AFieldCellGroupType&>( get<1>(fldsCell) );

    const DLA::MatrixDView_Array<Real>& mmfldCell = mmfld_.getCellGroupGlobal(cellGroupGlobal);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementAFieldClass afldElem( afldCell.basis() );

    // number of integrals evaluated
    const int aDOF = afldElem.nDOF();
    const int nIntegrandAUX = aDOF;

    // element integral
    typedef VectorArrayQ AUXIntegrandType;

    GalerkinWeightedIntegral<TopoDim, Topology, AUXIntegrandType> integralAUX(quadratureorder, nIntegrandAUX);

    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == afldCell.nElem() );

    // computes the elemental mass matrix
    ElementalMassMatrix<TopoDim, Topology> massMtx(get<-1>(xfldElem), qfldElem);

    // residual array
    DLA::VectorD<AUXIntegrandType> rsdAUXElem( nIntegrandAUX );

    // Provide a vector view of the auxiliary variable DOFs
    DLA::VectorDView<VectorArrayQ> a( afldElem.vectorViewDOF() );

    //Elemental mass matrix
    MatrixElemClass mm( aDOF, aDOF );

    // loop over elements within group and solve for the auxiliary variables first
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      afldCell.getElement( afldElem, elem );

      // only possessed cell elements have PDE residuals on this processor
      if ( qfldElem.rank() != comm_rank_ ) continue;

      std::map<int,std::vector<int>> cellITraceGroups;

      bool isBoundaryCell = false;

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, elem, trace);

        if (traceinfo.type == TraceInfo::Interior)
        {
          //add this traceGroup and traceElem to the set of interior traces attached to this cell
          cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::Boundary)
        {
          isBoundaryCell = true;
        }
        else
          SANS_DEVELOPER_EXCEPTION("SetFieldCell_HDG_AuxiliaryVariable_impl - Invalid trace type.");
      }

      rsdAUXElem = 0;

      // cell integration for canonical element
      integralAUX( fcnCell_.integrand_AUX(get<-1>(xfldElem), qfldElem, afldElem), get<-1>(xfldElem), rsdAUXElem.data(), nIntegrandAUX );

      //Collect the contributions to the AUX residual from the interior traces around this cell
      IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
          ResidualInteriorTrace_HDG_AuxiliaryVariable(fcnTrace_, cellITraceGroups, cellGroupGlobal, elem, rsdAUXElem),
          xfld_, (qfld_, afld_), qIfld_, quadOrderITrace_, nITraceGroup_ );

      // Solve for the auxiliary variable on this cell
      if (!isBoundaryCell)
      {
        if (updateAux_)
        {
          a -= mmfldCell[elem]*rsdAUXElem; //if cell is not a boundary cell, then jacAUX_a = mass matrix
        }
      }
      else
      {
        //Get matrix for efficiency
        DLA::MatrixDView<TensorMatrixQ> jacAUX_a_bcell = jacAUX_a_bcell_.getCell(cellGroupGlobal, elem);
        DLA::MatrixDView<VectorArrayQ> rsdAUX_bcell = rsdAUX_bcell_.getCell(cellGroupGlobal, elem);

        //Compute the elemental mass matrix and then add the terms from the BC
        massMtx(get<-1>(xfldElem), mm);

        for (int d = 0; d < D; d++)
          for (int i = 0; i < nIntegrandAUX; i++)
            for (int j = 0; j < aDOF; j++)
            {
              jacAUX_a_bcell(i,j)(d,d) += mm(i,j);
            }

        //Compute inverse
        jacAUX_a_bcell = DLA::InverseLU::Inverse(jacAUX_a_bcell);

        //Add residual contribution from boundary trace
        rsdAUXElem += rsdAUX_bcell;

        if (updateAux_)
        {
          //Solve for auxiliary variable
          a -= jacAUX_a_bcell*rsdAUXElem;
        }
      }

      // Set the auxiliary variable element
      afldCell.setElement( afldElem, elem );

    } //elem loop
  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandTrace& fcnTrace_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  const XFieldCellToTrace& xfldCellToTrace_;
  const XFieldClass& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld_;
  const int *quadOrderITrace_, nITraceGroup_;

  const FieldDataVectorD_BoundaryCell<VectorArrayQ>& rsdAUX_bcell_;

  std::vector<int> active_cellgroups_;
  const bool updateAux_;

  FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& jacAUX_a_bcell_;

  mutable int comm_rank_;
};

// Factory function

template<class IntegrandCell, class IntegrandITrace, class XFieldCellToTrace,
         class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class TensorMatrixQ>
SetFieldCell_HDG_AuxiliaryVariable_impl<IntegrandCell, IntegrandITrace, XFieldCellToTrace,
                                        XFieldType, TopoDim>
SetFieldCell_HDG_AuxiliaryVariable( const IntegrandCellType<IntegrandCell>& fcnCell,
                                    const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                                    const FieldDataInvMassMatrix_Cell& mmfld,
                                    const XFieldCellToTrace& xfldCellToTrace,
                                    const XFieldType& xfld,
                                    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                    const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                    const int quadOrderITrace[], const int nITraceGroup,
                                    const FieldDataVectorD_BoundaryCell<VectorArrayQ>& rsdAUX_bcell,
                                    const std::vector<int>& cellgroupsAux,
                                    const bool updateAux,
                                    FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& jacAUX_a_bcell )
{
  return { fcnCell.cast(), fcnITrace.cast(),
           mmfld, xfldCellToTrace,
           xfld, qfld, afld, qIfld,
           quadOrderITrace, nITraceGroup,
           rsdAUX_bcell, cellgroupsAux, updateAux,
           jacAUX_a_bcell };
}

}

#endif  // SETFIELD_HDG_AUXILIARYVARIABLE_H
