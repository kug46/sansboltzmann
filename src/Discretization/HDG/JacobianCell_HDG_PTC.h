// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_HDG_PTC_H
#define JACOBIANCELL_HDG_PTC_H

// HDG cell integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "Discretization/HDG/DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PhysDim, class QFieldCellGroupType, class AFieldCellGroupType, class MatrixQ, template <class> class SparseMatrix>
void
JacobianCell_HDG_Group_ScatterAdd(
    const QFieldCellGroupType& qfld,
    const AFieldCellGroupType& afld,
    const int elem, const int nDOF,
    int mapDOFGlobal_u[], int mapDOFGlobal_a[],
    const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDE_u,
    SparseMatrix<MatrixQ>& mtxGlobalPDE_u )
{

  // jacobian wrt u
  qfld.associativity( elem ).getGlobalMapping( mapDOFGlobal_u, nDOF );

  mtxGlobalPDE_u.scatterAdd( mtxElemPDE_u, mapDOFGlobal_u, nDOF );

}

#if 0
//----------------------------------------------------------------------------//
template <class PhysDim, class AFieldCellGroupType, class MatrixQ, template <class> class SparseMatrix>
void
JacobianCell_HDG_Group_ScatterAdd(
    const QField2DArea<Topology,PDE>& qfld,
    const AField2DArea<Topology,PDE>& qxfld,
    const AField2DArea<Topology,PDE>& qyfld,
    const int elem, const int nDOF,
    int mapDOFGlobal[],
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemPDE_u,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemPDE_ax,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemPDE_ay,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAux_u,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAux_ax,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAux_ay,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAuy_u,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAuy_ax,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAuy_ay,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalPDE_u,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalPDE_ax,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalPDE_ay,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAux_u,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAux_ax,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAux_ay,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAuy_u,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAuy_ax,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAuy_ay )
{
  mtxGlobalPDE_u.scatterAdd(  mtxElemPDE_u,  elem, elem );
  mtxGlobalAux_u.scatterAdd(  mtxElemAux_u,  elem, elem );
  mtxGlobalAuy_u.scatterAdd(  mtxElemAuy_u,  elem, elem );

  mtxGlobalPDE_ax.scatterAdd( mtxElemPDE_ax, elem, elem );
  mtxGlobalAux_ax.scatterAdd( mtxElemAux_ax, elem, elem );
  mtxGlobalAuy_ax.scatterAdd( mtxElemAuy_ax, elem, elem );

  mtxGlobalPDE_ay.scatterAdd( mtxElemPDE_ay, elem, elem );
  mtxGlobalAux_ay.scatterAdd( mtxElemAux_ay, elem, elem );
  mtxGlobalAuy_ay.scatterAdd( mtxElemAuy_ay, elem, elem );
}
#endif

//----------------------------------------------------------------------------//
// HDG cell integral
//
// topology specific group integral
//
// template parameters:
//   Topology                                       element topology (e.g. Triangle)
//   PDE                                            PDE class
//   Surreal                                        auto-differentiation class (e.g. SurrealS)
//   IntegrandCellFunctor                           integrand functor
//   SparseMatrix                                   sparse matrix class with PDE::MatrixQ elements

template <class Surreal, class Topology, class PhysDim, class TopoDim,
class ArrayQ, class VectorArrayQ,
class IntegrandCellFunctor, class SparseMatrix>
void
JacobianCell_Group_Integral_HDG_PTC(
    const IntegrandCellFunctor& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfld,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfld,
    const typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology>& afld,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_u)
{
  typedef typename IntegrandCellFunctor::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandCellFunctor::template MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS< PhysDim::D, ArrayQSurreal > GradArrayQSurreal;

  typedef SANS::DLA::MatrixD<MatrixQ> MatrixElemClass;

  typedef typename XField<PhysDim, TopoDim            >::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldSurrealClass qfldElemSurreal( qfld.basis() );
  ElementQFieldSurrealClass qfldElemSurrealHere( qfld.basis() );

  // DOFs per element
  const int nDOF = qfldElemSurreal.nDOF();

  // variables/equations per DOF
  const int nEqn = IntegrandCellFunctor::N;

  // element-to-global DOF mapping
  int mapDOFGlobal_u[nDOF];
  int mapDOFGlobal_a[nDOF*PhysDim::D];
  for (int k = 0; k < nDOF; k++)            { mapDOFGlobal_u[k] = -1; }
  for (int k = 0; k < nDOF*PhysDim::D; k++) { mapDOFGlobal_a[k] = -1; }

  // element integral
  GalerkinWeightedIntegral<TopoDim, Topology, IntegrandHDG<ArrayQSurreal,GradArrayQSurreal> >
  integral(quadratureorder, nDOF);

  // element integrand/residual
  std::unique_ptr< IntegrandHDG<ArrayQSurreal,GradArrayQSurreal>[] >
  rsdElemSurreal( new IntegrandHDG<ArrayQSurreal,GradArrayQSurreal>[nDOF] );

  // element jacobian matrices
  MatrixElemClass mtxElemPDE_u(nDOF, nDOF);

  // number of simultaneous derivatives per functor call
  const int nDeriv = Surreal::N;

  // loop over elements within group
  const int nelem = xfld.nElem();

  for (int elem = 0; elem < nelem; elem++)
  {
    // zero element Jacobians
    mtxElemPDE_u = 0;

    // copy global grid/solution DOFs to element
    xfld.getElement( xfldElem, elem );
    qfld.getElement( qfldElemSurrealHere, elem );

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nEqn*((PhysDim::D+1)*nDOF); nchunk += nDeriv)
    {

      // associate derivative slots with solution & lifting-operator variables

      int slot;
      for (int j = 0; j < nDOF; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(qfldElemSurrealHere.DOF(j),n).deriv(k) = 0;

          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemSurrealHere.DOF(j),n).deriv(slot - nchunk) = 1;
        }
      }

      for (int n = 0; n < nDOF; n++)
        rsdElemSurreal[n] = 0;

      // cell integration for canonical element

      integral( fcn.integrand(xfldElem, qfldElemSurrealHere), xfldElem, rsdElemSurreal.get(), nDOF );

#if 0
      std::cout << "JacobianPDE_Group_Integral2DArea_HDG: nchunk = " << nchunk << std::endl;
      for (int n = 0; n < nDOF; n++)
      {
        std::cout << "  rsdPDEElemSurreal[" << n << "] =" << std::endl;
        rsdElemSurreal[n].dump(4);
      }
#endif

      // accumulate derivatives into element jacobians

      for (int j = 0; j < nDOF; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int i = 0; i < nDOF; i++)
            {
              for (int m = 0; m < nEqn; m++)
              {
                DLA::index(mtxElemPDE_u(i,j),m,n) = DLA::index(rsdElemSurreal[i].PDE,m).deriv(slot - nchunk);
              }
            }
          }
        }
      }
    }   // nchunk

    // scatter-add element jacobian to global
#if 0
    std::cout << "mtxElemPDE_u " << std::endl; mtxElemPDE_u.dump();
    std::cout << "mtxElemPDE_a " << std::endl; mtxElemPDE_a.dump();
    std::cout << "mtxElemAu_u " << std::endl; mtxElemAu_u.dump();
    std::cout << "mtxElemAu_a " << std::endl; mtxElemAu_a.dump();
#endif

    JacobianCell_HDG_Group_ScatterAdd<PhysDim>(
        qfld, afld, elem, nDOF,
        mapDOFGlobal_u, mapDOFGlobal_a,
        mtxElemPDE_u,
        mtxGlobalPDE_u );

  } // elem

}


//----------------------------------------------------------------------------//
template<class Surreal, class TopDim>
class JacobianCell_HDG_PTC;

// base class interface
template<class Surreal>
class JacobianCell_HDG_PTC<Surreal, TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  template <class IntegrandCellFunctor,
  class PhysDim, class ArrayQ, class VectorArrayQ, class SparseMatrix>
  static void
  integrate(
      const IntegrandCellFunctor& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_u )
  {
    SANS_ASSERT( ngroup == qfld.nCellGroups() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();
    std::vector < typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Line>* > qGroups(1);

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Line) )
      {

        JacobianCell_Group_Integral_HDG_PTC<Surreal, Line, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
            fcn,
            xfld.template getCellGroup<Line>(group),
            qfld.template getCellGroup<Line>(group),
            afld.template getCellGroup<Line>(group),
            quadratureorder[group],
            mtxGlobalPDE_u );
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION( "unknown topology" );
      }
    }
  }
};


template<class Surreal>
class JacobianCell_HDG_PTC<Surreal, TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class IntegrandCellFunctor,
  class PhysDim, class ArrayQ, class VectorArrayQ, class SparseMatrix>
  static void
  integrate(
      const IntegrandCellFunctor& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const std::vector< std::unique_ptr< Field<PhysDim, TopoDim, ArrayQ> > >& qflds,
      int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_u )
  {
    SANS_ASSERT( mtxGlobalPDE_u.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_u.n() == qfld.nDOF() );

    SANS_ASSERT( ngroup == qfld.nCellGroups() );
    int ntimes = qflds.size();

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();
    std::vector < typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Triangle>* > qGroups(ntimes);

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        for (int i=0; i < ntimes; i++)
          qGroups[i] = &(qflds[i]->template getCellGroup<Triangle>(group));

        JacobianCell_Group_Integral_HDG_PTC<Surreal, Triangle, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
            fcn,
            xfld.template getCellGroup<Triangle>(group),
            qfld.template getCellGroup<Triangle>(group),
            afld.template getCellGroup<Triangle>(group),
            qGroups,
            quadratureorder[group],
            mtxGlobalPDE_u );
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION( "unknown topology" );
      }
    }
  }
};

}


#endif  // JACOBIANCELL_HDG_PTC_H
