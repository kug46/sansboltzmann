// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALBOUNDARYTRACE_HDG_SANSLG_H
#define JACOBIANFUNCTIONALBOUNDARYTRACE_HDG_SANSLG_H

// jacobian boundary-trace functional jacobian functions

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryTrace.h"

#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Discretization/HDG/JacobianBoundaryCell_HDG_AuxiliaryVariable.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Functional boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace,
         class IntegrandCell, class IntegrandITrace, class XFieldCellToTrace, class XFieldClass,
         class TopoDim_, template<class> class Vector>
class JacobianFunctionalBoundaryTrace_HDG_sansLG_impl :
    public GroupIntegralBoundaryTraceType<
       JacobianFunctionalBoundaryTrace_HDG_sansLG_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace,
                                                       IntegrandCell, IntegrandITrace, XFieldCellToTrace, XFieldClass,
                                                       TopoDim_, Vector> >
{
public:

  typedef typename FunctionalIntegrandBoundaryTrace::PhysDim PhysDim;
  static const int D = PhysDim::D;

  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Real> ArrayJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template MatrixJ<Real> MatrixJ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<Surreal> ArrayJSurreal;
  typedef DLA::VectorS<D, MatrixJ> VectorMatrixJ;

  typedef typename BCIntegrandBoundaryTrace::PDE PDE;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  JacobianFunctionalBoundaryTrace_HDG_sansLG_impl(const FunctionalIntegrandBoundaryTrace& fcnJ,
                                                  const BCIntegrandBoundaryTrace& fcnBC,
                                                  const IntegrandCell& fcnCell,
                                                  const IntegrandITrace& fcnITrace,
                                                  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                                                  const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                                                  const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                                                  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                                                  const XFieldCellToTrace& xfldCellToTrace,
                                                  const XFieldClass& xfld,
                                                  const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                                                  const Field<PhysDim, TopoDim_, VectorArrayQ>& afld,
                                                  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld,
                                                  const int quadOrderCell[], const int nCellGroup,
                                                  const int quadOrderITrace[], const int nITraceGroup,
                                                  Vector<MatrixJ>& jacFunctional_qL,
                                                  Vector<MatrixJ>& jacFunctional_qI ) :
                                                    fcnJ_(fcnJ), fcnBC_(fcnBC),
                                                    fcnCell_(fcnCell), fcnITrace_(fcnITrace),
                                                    invJacAUX_a_bcell_(invJacAUX_a_bcell),
                                                    jacAUX_q_bcell_(jacAUX_q_bcell), jacAUX_qI_btrace_(jacAUX_qI_btrace),
                                                    mapDOF_boundary_qI_(mapDOF_boundary_qI), xfldCellToTrace_(xfldCellToTrace),
                                                    xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
                                                    quadOrderCell_(quadOrderCell), nCellGroup_(nCellGroup),
                                                    quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup),
                                                    jacFunctional_qL_(jacFunctional_qL),
                                                    jacFunctional_qI_(jacFunctional_qI)
  {
    // Find all groups that are common between the BC and functional
    for (std::size_t i = 0; i < fcnJ_.nBoundaryGroups(); i++)
    {
      for (std::size_t j = 0; j < fcnBC_.nBoundaryGroups(); j++)
      {
        std::size_t iBoundaryGroupJ = fcnJ_.boundaryGroup(i);
        if (iBoundaryGroupJ == fcnBC_.boundaryGroup(j))
          boundaryTraceGroups_.push_back(iBoundaryGroupJ);
      }
    }
  }

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qfld
                                                   Field<PhysDim, TopoDim, VectorArrayQ>, //afld
                                                   Field<PhysDim, TopoDim, VectorArrayQ>  //bfld
                                      >::type & flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfldTrace ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    SANS_ASSERT_MSG(qfld.nDOF() == jacFunctional_qL_.m(), "qfld.nDOF() = %d, jacFunctional_qL_.m() = %d", qfld.nDOF(), jacFunctional_qL_.m());
    SANS_ASSERT_MSG(qIfldTrace.nDOF() == jacFunctional_qI_.m(), "qIfldTrace.nDOF() = %d, jacFunctional_qI_.m() = %d",
                    qIfldTrace.nDOF(), jacFunctional_qI_.m());
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType, class XFieldTraceGroupType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qfld
                                                  Field<PhysDim, TopoDim, VectorArrayQ>, //afld
                                                  Field<PhysDim, TopoDim, VectorArrayQ>  //bfld
                                     >::type::template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const XFieldTraceGroupType& xfldTrace,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>       QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>  QFieldTraceGroupType;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<>        ElementBFieldClassL;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);
          AFieldCellGroupTypeL& bfldCellL = const_cast<AFieldCellGroupTypeL&>(get<2>(fldsCellL));

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldClassL afldElemL( afldCellL.basis() );
    ElementBFieldClassL bfldElemL( bfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace(  xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // total number of entries in ArrayQ
    const int nVar = DLA::VectorSize<ArrayQ>::M;

    // total number of outputs in the functional
    const int nJEqn = DLA::VectorSize<ArrayJ>::M;
    static_assert( nJEqn == 1 , "number of output functionals must be 1 for now");

    // variables/equations per DOF
    // const int nJEqn = IntegrandBoundaryTrace::N;

    // Inverse mass matrix used to compute auxiliary variables
//    const DLA::MatrixDView_Array<Real>& mmfldCell = mmfld_.getCellGroupGlobal(cellGroupGlobalL);

    // DOFs per element
    const int qDOFL = qfldElemL.nDOF();
    const int aDOFL = afldElemL.nDOF();
    const int qIDOF = qIfldElemTrace.nDOF();

    const int nIntegrandL = qDOFL;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(qDOFL, -1);

    typedef VectorArrayQSurreal AUXIntegrandType;

    // trace element integral
    ElementIntegral<TopoDimTrace, TopologyTrace, ArrayJSurreal> integralJ(quadratureorder);

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, AUXIntegrandType> integralAUXL(quadratureorder, qDOFL);

    // element jacobian matrices
    // Left
    // element functional jacobian vector
    DLA::VectorD<MatrixJ> jacFunctionalElem_qL(qDOFL);
    DLA::VectorD<VectorMatrixJ> jacFunctionalElem_aL(aDOFL);
    std::vector<DLA::VectorD<MatrixJ>> jacFunctionalElem_qI(TopologyL::NTrace, DLA::VectorD<MatrixJ>(0));

    // auxiliary variable residuals
    DLA::VectorD< AUXIntegrandType > rsdElemAUXL( nIntegrandL );

    MatrixAUXElemClass mtxElemAUXL_qL(aDOFL, qDOFL);
    std::vector<MatrixAUXElemClass> mtxElemAUXL_qI(TopologyL::NTrace, MatrixAUXElemClass(0, 0));

    //storage for auxiliary variable jacobians wrt q and qI on each trace of this cell
    MatrixAUXElemClass mtx_a_q(qDOFL, qDOFL);
    std::vector<MatrixAUXElemClass> mtx_a_qI(TopologyL::NTrace, MatrixAUXElemClass(0, 0));
    std::vector<std::vector<int>> mapDOFGlobal_qI_cell(TopologyL::NTrace);

    // Provide a vector view of the auxiliary adjoint variable DOFs
    DLA::VectorDView<VectorArrayQ> bL( bfldElemL.vectorViewDOF() );

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      for (int trace = 0; trace < TopologyL::NTrace; trace++)
      {
        mtxElemAUXL_qI[trace].resize(0, 0);
        mtx_a_qI[trace].resize(0, 0);
        mapDOFGlobal_qI_cell[trace].clear();

        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobalL, elemL, trace);

        if (traceinfo.type == TraceInfo::Boundary)
        {
          //This trace is a boundary trace, so save its {tracegroup, elem} info
          if ( mapDOF_boundary_qI_[traceinfo.group].size() > 0 )
          {
            //Copy the globalDOFmap for qI DOFs on this boundary trace
            mapDOFGlobal_qI_cell[trace] = mapDOF_boundary_qI_[traceinfo.group][traceinfo.elem];

            //Copy the stored AUX jacobian contribution from BC
            mtxElemAUXL_qI[trace].resize(aDOFL, mapDOFGlobal_qI_cell[trace].size());
            mtxElemAUXL_qI[trace] = jacAUX_qI_btrace_.getBoundaryTraceGroupGlobal(traceinfo.group)[traceinfo.elem];
          }
        }
      }

      // check if the DOF count for this trace is consistent
      SANS_ASSERT( (int) mapDOFGlobal_qI_cell[canonicalTraceL.trace].size() == qIDOF );

      //Get the AUX jacobian from the BC
      mtxElemAUXL_qL = jacAUX_q_bcell_.getCell(cellGroupGlobalL, elemL);

      //Accumulate the auxiliary variable jacobian contributions (AUX_qI) from the interior traces of the left boundary cell
      IntegrateCellGroups<TopoDim>::integrate(
          JacobianBoundaryCell_HDG_AuxiliaryVariable<Surreal>(fcnCell_, fcnITrace_,
                                                              cellGroupGlobalL, elemL, xfldCellToTrace_,
                                                              xfld_, qfld_, afld_, qIfld_,
                                                              quadOrderITrace_, nITraceGroup_,
                                                              mtxElemAUXL_qL, mtxElemAUXL_qI, mapDOFGlobal_qI_cell),
          xfld_, (qfld_, afld_), quadOrderCell_, nCellGroup_ );

      //Compute the auxiliary variable jacobians
      mtx_a_q = -invJacAUX_a_bcell_.getCell(cellGroupGlobalL, elemL) * mtxElemAUXL_qL;

      for (int trace = 0; trace < (int) TopologyL::NTrace; trace++)
      {
        if (mapDOFGlobal_qI_cell[trace].size() > 0)
        {
          mtx_a_qI[trace].resize(aDOFL, mapDOFGlobal_qI_cell[trace].size());
          mtx_a_qI[trace] = -invJacAUX_a_bcell_.getCell(cellGroupGlobalL, elemL) * mtxElemAUXL_qI[trace];
        }
      }

      // zero element Jacobians
      jacFunctionalElem_qL = 0;
      jacFunctionalElem_aL = 0;

      for (int trace = 0; trace < (int) TopologyL::NTrace; trace++)
      {
        if (mapDOFGlobal_qI_cell[trace].size() > 0)
        {
          jacFunctionalElem_qI[trace].resize( (int) mapDOFGlobal_qI_cell[trace].size() );
          jacFunctionalElem_qI[trace] = 0;
        }
      }

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*(qDOFL + D*aDOFL + qIDOF); nchunk += nDeriv)
      {
        // associate derivative slots
        int slot, slotOffset = 0;

        // wrt qL
        for (int j = 0; j < qDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*qDOFL;

        // wrt aL
        for (int j = 0; j < aDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nVar; n++)
            {
              slot = slotOffset + nVar*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }
        slotOffset += nVar*D*aDOFL;

        // wrt qI
        for (int j = 0; j < qIDOF; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nVar*qIDOF;

        // trace integration for canonical element
        ArrayJSurreal functional = 0;
        integralJ( fcnJ_.integrand( fcnBC_,
                                    xfldElemTrace, canonicalTraceL,
                                    xfldElemL, qfldElemL, afldElemL, qIfldElemTrace),
                   get<-1>(xfldElemTrace),
                   functional);

        // accumulate derivatives into element jacobians
        slotOffset = 0;

        // wrt qL
        for (int j = 0; j < qDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // Reset the derivative

              DLA::index(jacFunctionalElem_qL[j], n) = DLA::index(functional, n).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nVar*qDOFL;


        // wrt aL
        for (int j = 0; j < aDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nVar; n++)
            {
              slot = slotOffset + nVar*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 0;  // Reset the derivative

                DLA::index(jacFunctionalElem_aL[j][d], n) = DLA::index(functional, n).deriv(slot - nchunk);
              }
            }
          }
        }
        slotOffset += nVar*D*aDOFL;


        // wrt qI
        for (int j = 0; j < qIDOF; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 0;  // Reset the derivative

              DLA::index(jacFunctionalElem_qI[canonicalTraceL.trace][j], n) = DLA::index(functional, n).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nVar*qIDOF;
      }   // nchunk

      //Chain rule to add auxiliary variable contribution to jacobian
      jacFunctionalElem_qL += Transpose(mtx_a_q)*jacFunctionalElem_aL;

      // global mapping for qL
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL.data(), qDOFL );

      // scatter-add element jacobian to global
      for (int i = 0; i < qDOFL; i++)
        jacFunctional_qL_[mapDOFGlobal_qL[i]] += jacFunctionalElem_qL[i];

      //Complete the chain rule with respect to qI DOFs on other traces of the left cell
      for (int trace = 0; trace < TopologyL::NTrace; trace++)
      {
        if (mapDOFGlobal_qI_cell[trace].size() > 0) //some boundary-trace groups might have been omitted
        {
          jacFunctionalElem_qI[trace] += Transpose(mtx_a_qI[trace])*jacFunctionalElem_aL;

          for (int i = 0; i < (int) mapDOFGlobal_qI_cell[trace].size(); i++)
            jacFunctional_qI_[mapDOFGlobal_qI_cell[trace][i]] += jacFunctionalElem_qI[trace][i];
        }
      }

      // Set the first component of the auxiliary variable adjoint
      // bL = [dAUX/da]^-T * ([dJ/da]^T - [dPDE/da]^T * w - [dINT/da]^T * wI)
      bfldCellL.getElement( bfldElemL, elemL );
      bL += Transpose(invJacAUX_a_bcell_.getCell(cellGroupGlobalL, elemL)) * jacFunctionalElem_aL;
      bfldCellL.setElement( bfldElemL, elemL );

    } //elem loop
  }


protected:
  const FunctionalIntegrandBoundaryTrace& fcnJ_;
  const BCIntegrandBoundaryTrace& fcnBC_;
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;

  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell_;
  const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell_;
  const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace_;
  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI_;

  const XFieldCellToTrace& xfldCellToTrace_;
  const XFieldClass& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld_;

  const int *quadOrderCell_, nCellGroup_;
  const int *quadOrderITrace_, nITraceGroup_;

  std::vector<int> boundaryTraceGroups_;
  Vector<MatrixJ>& jacFunctional_qL_;
  Vector<MatrixJ>& jacFunctional_qI_;
};

// Factory function

template<class Surreal, class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, class IntegrandCell, class IntegrandITrace,
         class XFieldCellToTrace, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class TensorMatrixQ, class VectorMatrixQ,
         template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_HDG_sansLG_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, IntegrandCell, IntegrandITrace,
                                                XFieldCellToTrace, XFieldType, TopoDim, Vector>
JacobianFunctionalBoundaryTrace_HDG_sansLG( const IntegrandBoundaryTraceType<FunctionalIntegrandBoundaryTrace>& fcnJ,
                                            const IntegrandBoundaryTraceType<BCIntegrandBoundaryTrace>& fcnBC,
                                            const IntegrandCellType<IntegrandCell>& fcnCell,
                                            const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                                            const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                                            const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                                            const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                                            const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                                            const XFieldCellToTrace& xfldCellToTrace,
                                            const XFieldType& xfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                            const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                            const int quadOrderCell[], const int nCellGroup,
                                            const int quadOrderITrace[], const int nITraceGroup,
                                            Vector< MatrixJ >& func_qL,
                                            Vector< MatrixJ >& func_qI)
{
  typedef typename Scalar<MatrixJ>::type T;
  static_assert( std::is_same<typename FunctionalIntegrandBoundaryTrace::template MatrixJ<T>, MatrixJ>::value, "These should be the same");
  return JacobianFunctionalBoundaryTrace_HDG_sansLG_impl<Surreal, FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace,
                                                         IntegrandCell, IntegrandITrace,
                                                         XFieldCellToTrace, XFieldType, TopoDim, Vector>(
      fcnJ.cast(), fcnBC.cast(), fcnCell.cast(), fcnITrace.cast(),
      invJacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOF_boundary_qI,
      xfldCellToTrace, xfld, qfld, afld, qIfld,
      quadOrderCell, nCellGroup,
      quadOrderITrace, nITraceGroup,
      func_qL, func_qI );
}

}

#endif  // JACOBIANFUNCTIONALBOUNDARYTRACE_HDG_H
