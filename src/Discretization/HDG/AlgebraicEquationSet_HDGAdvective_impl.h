// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ALGEBRAICEQUATIONSET_HDGADVECTIVE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "AlgebraicEquationSet_HDGAdvective.h"

#include <type_traits>

#include "Surreal/SurrealS.h"

#include "Field/XField.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "pde/BCParameters.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDGAdvective.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDGAdvective.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDGAdvective.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_sansLG_HDGAdvective.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "ResidualInteriorTrace_HDGAdvective.h"
#include "ResidualBoundaryTrace_Dispatch_HDGAdvective.h"
#include "ResidualBoundaryTrace_FieldTrace_HDGAdvective.h"
#include "ResidualBoundaryTrace_HDGAdvective.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin.h"
#include "JacobianInteriorTrace_HDGAdvective.h"
#include "JacobianBoundaryTrace_Dispatch_HDGAdvective.h"

#include "Discretization/Galerkin/JacobianCell_Galerkin_Param.h"
#include "JacobianInteriorTrace_HDGAdvective_Param.h"
#include "JacobianBoundaryTrace_Dispatch_HDGAdvective_Param.h"

#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/isValidState/isValidStateInteriorTrace.h"
#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch.h"

#ifdef SANS_MPI
#include <boost/mpi/collectives/all_reduce.hpp>
#include <boost/serialization/vector.hpp>
#include "tools/plus_std_vector.h"
#endif

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//Protected constructor for AlgebraicEquationSet_Local_HDG: cellgroups for the auxiliary calculation can be different
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
template< class... BCArgs >
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
AlgebraicEquationSet_HDGAdvective(const XFieldType& xfld,
                         Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                         Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                         Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                         const NDPDEClass& pde,
                         const QuadratureOrder& quadratureOrder,
                         const ResidualNormType& resNormType,
                         const std::vector<Real>& tol,
                         const std::vector<int>& cellGroups,
                         const std::vector<int>& interiorTraceGroups,
                         PyDict& bcList,
                         const std::map< std::string, std::vector<int> >& bcBoundaryGroups,
                         BCArgs&... args ) :
   DebugBaseType(pde, tol),
   disc_(pde, Nothing),
   fcnCell_( pde, cellGroups ),
   fcnTrace_( pde, disc_, interiorTraceGroups ),
   BCs_(BCParams::template createBCs<BCNDConvert>(pde, bcList, args...)),
   dispatchBC_(pde, bcList, BCs_, bcBoundaryGroups, disc_),
   xfld_(xfld),
   qfld_(qfld),
   qIfld_(qIfld),
   lgfld_(lgfld),
   pde_(pde),
   quadratureOrder_(quadratureOrder), quadratureOrderMin_(get<-1>(xfld), 0),
   resNormType_(resNormType),
   tol_(tol)
{
  SANS_ASSERT( tol_.size() >= nEqnSet );

  SANS_ASSERT_MSG( isValidState(), "AlgebraicEquationSet_HDGAdvective is initialized with an invalid state");
}

//Fills jacobian or the non-zero pattern of a jacobian
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobian(SystemMatrixView& mtx)
{
  if (AES_ != nullptr) AES_->jacobian(mtx);
  this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
}
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobian(SystemNonZeroPatternView& nz)
{
  if (AES_ != nullptr) AES_->jacobian(nz);
  this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
}

//Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobianTranspose(SystemMatrixView& mtxT)
{
  if (AES_ != nullptr) AES_->jacobianTranspose(mtxT);
  jacobian(Transpose(mtxT), quadratureOrder_ );
}
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobianTranspose(SystemNonZeroPatternView& nzT)
{
  if (AES_ != nullptr) AES_->jacobianTranspose(nzT);
  jacobian(Transpose( nzT), quadratureOrderMin_ );
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
std::vector<std::vector<Real>>
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
residualNorm(const SystemVectorView& rsd) const
{
  const int nDOFPDE = rsd[iPDE].m();
  const int nDOFINT = rsd[iINT].m();
  const int nDOFBC = rsd[iBC].m();
  const int nMon = pde_.nMonitor();

  DLA::VectorD<Real> rsdPDEtmp(nMon);
  DLA::VectorD<Real> rsdINTtmp(nMon);
  DLA::VectorD<Real> rsdBCtmp(nMon);

  rsdPDEtmp = 0.0;
  rsdINTtmp = 0.0;
  rsdBCtmp = 0.0;

  std::vector<std::vector<Real>> rsdNorm(nResidNorm(), std::vector<Real>(nMon, 0.0));

  //HACKED Simple L2 norm
  //TODO: Allow for non-L2 norms

  //compute PDE residual norm
  for (int n = 0; n < nDOFPDE; n++)
  {
    pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] += pow(rsdPDEtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);

  //INT residual
  for (int n = 0; n < nDOFINT; n++)
  {
    pde_.interpResidVariable(rsd[iINT][n], rsdINTtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iINT][j] += pow(rsdINTtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iINT][j] = sqrt(rsdNorm[iINT][j]);

  //BC residual
  for (int n = 0; n < nDOFBC; n++)
  {
    pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

  return rsdNorm;
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const
{
  titles = {"PDE : ",
            "INT : ",
            "BC  : "};
  idx = {iPDE, iINT, iBC};
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
residual(SystemVectorView& rsd)
{
  SANS_ASSERT(rsd.m() >= nEqnSet);

  if (AES_ != nullptr) AES_->residual(rsd);

  IntegrateCellGroups<TopoDim>::integrate(
    ResidualCell_Galerkin(fcnCell_, rsd(iPDE)),
    xfld_, qfld_,
    quadratureOrder_.cellOrders.data(),
    quadratureOrder_.cellOrders.size() );

  IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
    ResidualInteriorTrace_HDGAdvective(fcnTrace_, rsd(iPDE), rsd(iINT)),
    xfld_, qfld_, qIfld_,
    quadratureOrder_.interiorTraceOrders.data(),
    quadratureOrder_.interiorTraceOrders.size() );

  dispatchBC_.dispatch(
    ResidualBoundaryTrace_FieldTrace_Dispatch_HDGAdvective(
      xfld_, qfld_, qIfld_, lgfld_,
      quadratureOrder_.boundaryTraceOrders.data(),
      quadratureOrder_.boundaryTraceOrders.size(),
      rsd(iPDE), rsd(iINT), rsd(iBC) ),
    ResidualBoundaryTrace_Dispatch_HDGAdvective(
      xfld_, qfld_, qIfld_,
      quadratureOrder_.boundaryTraceOrders.data(),
      quadratureOrder_.boundaryTraceOrders.size(),
      rsd(iPDE), rsd(iINT) ) );
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder,
         const std::vector<int>& interiorTraceCellJac)
{
  SANS_ASSERT(jac.m() >= nEqnSet);
  SANS_ASSERT(jac.n() >= nSolSet);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  // jacobian nonzero pattern
  //
  //        q  qI  lg
  //   PDE  X   X   X
  //   Int  X   X   0
  //   BC   X   0   0 //TODO: is BC_qI = 0 always true?

  Matrix jacPDE_q  = jac(iPDE,iq);
  Matrix jacPDE_qI = jac(iPDE,iqI);
  Matrix jacPDE_lg = jac(iPDE,ilg);

  Matrix jacINT_q  = jac(iINT,iq);
  Matrix jacINT_qI = jac(iINT,iqI);
  Matrix jacINT_lg = jac(iINT,ilg);

  Matrix jacBC_q  = jac(iBC,iq);
  Matrix jacBC_qI = jac(iBC,iqI);
  Matrix jacBC_lg = jac(iBC,ilg);

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_Galerkin(fcnCell_, jacPDE_q),
      xfld_, qfld_,
      quadratureOrder.cellOrders.data(),
      quadratureOrder.cellOrders.size() );

  IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
      JacobianInteriorTrace_HDGAdvective(fcnTrace_, jacPDE_q, jacPDE_qI, jacINT_q, jacINT_qI),
      xfld_, qfld_, qIfld_,
      quadratureOrder.interiorTraceOrders.data(),
      quadratureOrder.interiorTraceOrders.size() );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_FieldTrace_Dispatch_HDGAdvective(xfld_, qfld_, qIfld_, lgfld_,
                                                             quadratureOrder.boundaryTraceOrders.data(),
                                                             quadratureOrder.boundaryTraceOrders.size(),
                                                             jacPDE_q, jacPDE_qI, jacPDE_lg,
                                                             jacINT_q, jacINT_qI, jacINT_lg,
                                                             jacBC_q , jacBC_qI , jacBC_lg ),
      JacobianBoundaryTrace_Dispatch_HDGAdvective( xfld_, qfld_, qIfld_,
                                                   quadratureOrder.boundaryTraceOrders.data(),
                                                   quadratureOrder.boundaryTraceOrders.size(),
                                                   jacPDE_q, jacPDE_qI,
                                                   jacINT_q, jacINT_qI ) );
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
template<int iParam, class SparseMatrixType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const
{
  SANS_ASSERT(jac.m() >= nEqnSet);
  SANS_ASSERT(jac.n() > ip);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_p = jac(iPDE, ip);
  Matrix jacINT_p = jac(iINT, ip);
  Matrix jacBC_p  = jac(iBC, ip);

  typedef SurrealS<NDPDEClass::Nparam> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_Galerkin_Param<SurrealClass,iParam>(fcnCell_, jacPDE_p),
                                                       xfld_, qfld_,
                                                       quadratureOrder_.cellOrders.data(),
                                                       quadratureOrder_.cellOrders.size() );

  IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
      JacobianInteriorTrace_HDGAdvective_Param<SurrealClass,iParam>(fcnTrace_, jacPDE_p, jacINT_p),
      xfld_, qfld_, qIfld_,
      quadratureOrder_.interiorTraceOrders.data(),
      quadratureOrder_.interiorTraceOrders.size() );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_mitLG_Dispatch_HDGAdvective_Param<SurrealClass, iParam>(
          xfld_, qfld_, qIfld_, lgfld_,
          quadratureOrder_.boundaryTraceOrders.data(),
          quadratureOrder_.boundaryTraceOrders.size(),
          jacPDE_p, jacINT_p, jacBC_p ),
      JacobianBoundaryTrace_sansLG_Dispatch_HDGAdvective_Param<SurrealClass, iParam>(
          xfld_, qfld_, qIfld_,
          quadratureOrder_.boundaryTraceOrders.data(),
          quadratureOrder_.boundaryTraceOrders.size(),
          jacPDE_p, jacINT_p ) );
}


template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
setSolutionField(const SystemVectorView& q)
{
  if (AES_ != nullptr) AES_->setSolutionField(q);

  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOF();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];

  const int nDOFINT = qIfld_.nDOF();
  SANS_ASSERT( nDOFINT == q[iqI].m() );
  for (int k = 0; k < nDOFINT; k++)
    qIfld_.DOF(k) = q[iqI][k];

  const int nDOFBC = lgfld_.nDOF();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    lgfld_.DOF(k) = q[ilg][k];
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
setAdjointField(const SystemVectorView& adj,
                Field_DG_Cell<PhysDim, TopoDim, ArrayQ>&       wfld,
                Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& bfld,
                Field<PhysDim, TopoDim, ArrayQ>&               wIfld,
                Field<PhysDim, TopoDim, ArrayQ>&               mufld )
{
  //TODO
  SANS_DEVELOPER_EXCEPTION("Not implemented yet!");
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
fillSystemVector(SystemVectorView& q) const
{
  if (AES_ != nullptr) AES_->fillSystemVector(q);

  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld_.nDOF();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld_.DOF(k);

  const int nDOFInt = qIfld_.nDOF();
  SANS_ASSERT( nDOFInt == q[iqI].m() );
  for (int k = 0; k < nDOFInt; k++)
    q[iqI][k] = qIfld_.DOF(k);

  const int nDOFBC = lgfld_.nDOF();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld_.DOF(k);
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
typename AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::VectorSizeClass
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0, "Assumed ordering fails!");
  static_assert(iINT == 1, "Assumed ordering fails!");
  static_assert(iBC == 2, "Assumed ordering fails!");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFIntpos = qIfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  std::initializer_list<int> lst = {nDOFPDEpos, nDOFIntpos, nDOFBCpos};
  SANS_ASSERT_MSG(lst.size() == nEqnSet, "");

  return lst;
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
typename AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::VectorSizeClass
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
vectorStateSize() const
{
  static_assert(iq == 0, "Assumed ordering fails!");
  static_assert(iqI == 1, "Assumed ordering fails!");
  static_assert(ilg == 2, "Assumed ordering fails!");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOF();
  const int nDOFInt = qIfld_.nDOF();
  const int nDOFBC  = lgfld_.nDOF();

  std::initializer_list<int> lst = {nDOFPDE, nDOFInt, nDOFBC};
  SANS_ASSERT_MSG(lst.size() == nSolSet, "");

  return lst;
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
typename AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0, "Assumed ordering fails!");
  static_assert(iINT == 1, "Assumed ordering fails!");
  static_assert(iBC == 2, "Assumed ordering fails!");

  static_assert(iq == 0, "Assumed ordering fails!");
  static_assert(iqI == 1, "Assumed ordering fails!");
  static_assert(ilg == 2, "Assumed ordering fails!");

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFIntpos = qIfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  const int nDOFPDE = qfld_.nDOF();
  const int nDOFInt = qIfld_.nDOF();
  const int nDOFBC  = lgfld_.nDOF();

  return {{ {nDOFPDEpos, nDOFPDE}, {nDOFPDEpos, nDOFInt}, {nDOFPDEpos, nDOFBC} },
          { {nDOFIntpos, nDOFPDE}, {nDOFIntpos, nDOFInt}, {nDOFIntpos, nDOFBC} },
          { {nDOFBCpos , nDOFPDE}, {nDOFBCpos , nDOFInt}, {nDOFBCpos , nDOFBC} }};
}


template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
bool
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  // Update the solution field
  setSolutionField(q);

  bool checkAES_daisychain = true;
  if (AES_ != nullptr) checkAES_daisychain = AES_->isValidStateSystemVector(q);

  return (isValidState() && checkAES_daisychain);
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
bool
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
isValidState()
{
  bool isValidState = true;

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                           xfld_, qfld_,
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );
#if 0
  IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate( isValidStateInteriorTrace_HDG(fcnTrace_, pde_, isValidState),
                                                               xfld_, qfld_, qIfld_,
                                                               quadratureOrder_.interiorTraceOrders.data(),
                                                               quadratureOrder_.interiorTraceOrders.size());

  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                quadratureOrder_.boundaryTraceOrders.size(), isValidState ),
      isValidStateBoundaryTrace_HDG_Dispatch( pde_, xfld_, qfld_, qIfld_,
                                              quadratureOrder_.boundaryTraceOrders.data(),
                                              quadratureOrder_.boundaryTraceOrders.size(), isValidState )
    );
#else //TODO: not checking qIfld_ validity for now
  IntegrateInteriorTraceGroups<TopoDim>::integrate( isValidStateInteriorTrace(pde_, fcnTrace_.interiorTraceGroups(), isValidState),
                                                    xfld_, qfld_,
                                                    quadratureOrder_.interiorTraceOrders.data(),
                                                    quadratureOrder_.interiorTraceOrders.size() );

  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                quadratureOrder_.boundaryTraceOrders.size(), isValidState ),
      isValidStateBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_,
                                                 quadratureOrder_.boundaryTraceOrders.data(),
                                                 quadratureOrder_.boundaryTraceOrders.size(), isValidState )
    );
#endif

  return isValidState;
}

// Add an AES to this one to daisy-chain
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
addAlgebraicEquationSet(std::shared_ptr<BaseType> AES)
{
  AES_ = AES;
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
int
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
nResidNorm() const
{
  return nEqnSet;
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
std::shared_ptr<mpi::communicator>
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
comm() const
{
  return qfld_.comm();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
syncDOFs_MPI()
{
  qfld_.syncDOFs_MPI_Cached();
  lgfld_.syncDOFs_MPI_Cached();
}

template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
void
AlgebraicEquationSet_HDGAdvective<NDPDEClass_, BCNDConvert, BCVector, Traits, DiscTag, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);

//  std::string filename_iBC = filenamebase + "_iBC_iter" + std::to_string(nonlinear_iter) + ".plt";
//  this->dumpLinesearchField(lgfld_, *pStepData, iBC, filename_iBC);
}

} //namespace SANS

// Helper macros to reduce the amount of work needed to instantiate AlgebraicEquationSet_HDGAdvective
#define ALGEBRAICEQUATIONSET_HDGADVECTIVE_INSTANTIATE_TRAITS(PDEND, BCNDCONVERT, BCVECTOR, TOPODIM, TRAITS, DISCTAG, PARAMFIELDTUPLE ) \
template class AlgebraicEquationSet_HDGAdvective< PDEND, BCNDCONVERT, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE >; \
\
template AlgebraicEquationSet_HDGAdvective< PDEND, BCNDCONVERT, BCVECTOR, TRAITS, DISCTAG, PARAMFIELDTUPLE >:: \
         AlgebraicEquationSet_HDGAdvective(const PARAMFIELDTUPLE& xfld, \
                                  Field_DG_Cell<PDEND::PhysDim, \
                                                TOPODIM, \
                                                PDEND::ArrayQ<Real> >& qfld, \
                                  Field<PDEND::PhysDim, \
                                        TOPODIM, \
                                        PDEND::ArrayQ<Real> >& qIfld, \
                                  Field<PDEND::PhysDim, \
                                        TOPODIM, \
                                        PDEND::ArrayQ<Real> >& lgfld, \
                                  const PDEND& pde, \
                                  const QuadratureOrder& quadratureOrder, \
                                  const ResidualNormType& resNormType, \
                                  const std::vector<Real>& tol, \
                                  const std::vector<int>& cellGroups, \
                                  const std::vector<int>& interiorTraceGroups, \
                                  PyDict& bcList, \
                                  const std::map< std::string, std::vector<int> >& bcBoundaryGroups );

//---------------------------------------------------------------------------//
// This is the main macro used for steady instantiation
#define ALGEBRAICEQUATIONSET_HDGADVECTIVE_INSTANTIATE_SPACE( PDE, BCVECTOR, TOPODIM, TRAITS, DISCTAG, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpace<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpace); \
\
ALGEBRAICEQUATIONSET_HDGADVECTIVE_INSTANTIATE_TRAITS( BOOST_PP_CAT(PDE, NDSpace), BCNDConvertSpace, BCVECTOR, TOPODIM, \
                                                      TRAITS, DISCTAG, PARAMFIELDTUPLE )
