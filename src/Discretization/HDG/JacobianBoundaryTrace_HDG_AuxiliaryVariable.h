// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_HDG_AUXILIARYVARIABLE_H
#define JACOBIANBOUNDARYTRACE_HDG_AUXILIARYVARIABLE_H

// HDG trace integral jacobian functions

#include <vector>

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryTrace.h"
#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_HDG_AuxiliaryVariable_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_HDG_AuxiliaryVariable_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianBoundaryTrace_HDG_AuxiliaryVariable_impl( const IntegrandBoundaryTrace& fcnBTrace,
                                                    FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                                                    FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                                                    std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI ) :
                                                      fcnBTrace_(fcnBTrace),
                                                      jacAUX_q_bcell_(jacAUX_q_bcell), jacAUX_qI_btrace_(jacAUX_qI_btrace),
                                                      mapDOFGlobal_qI_(mapDOFGlobal_qI) {}

  std::size_t nBoundaryGroups() const { return fcnBTrace_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnBTrace_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    SANS_ASSERT( (int) mapDOFGlobal_qI_.size() == qIfld.getXField().nBoundaryTraceGroups() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldSurrealClassL;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldSurrealClassL afldElemL( afldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    const int nIntegrandL = nDOFL;

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // element integral
    typedef VectorArrayQSurreal IntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, IntegrandType> integralL(quadratureorder, nDOFL);

    // auxiliary variable residuals
    DLA::VectorD<IntegrandType> rsdElemAUXL( nIntegrandL );

    MatrixAUXElemClass mtxElemAUXL_qL(nDOFL, nDOFL);
    MatrixAUXElemClass mtxElemAUXL_qI(nDOFL, nDOFI);

    const int nelem = xfldTrace.nElem();

    mapDOFGlobal_qI_[traceGroupGlobal].resize(nelem, std::vector<int>(nDOFI, -1));

    // loop over trace elements
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );

      // get the global DOF mapping for qI on this trace and save for later use
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI_[traceGroupGlobal][elem].data(), nDOFI );

      // loop over derivative chunks for derivatives wrt qL and qI
      for (int nchunk = 0; nchunk < nEqn*(nDOFL + nDOFI); nchunk += nDeriv)
      {
        // associate derivative slots with solution variables
        int slot, slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFI;

        //------------------------------------------------------------------------------------------------

        for (int n = 0; n < nIntegrandL; n++) rsdElemAUXL[n] = 0;

        // AUX trace integration for canonical element
        integralL( fcnBTrace_.integrand_AUX(xfldElemTrace, canonicalTraceL, xfldElemL,
                                            qfldElemL, afldElemL, qIfldElemTrace),
                   xfldElemTrace, rsdElemAUXL.data(), nIntegrandL);

        //------------------------------------------------------------------------------------------------

        // accumulate derivatives into element jacobians
        slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              //AUXL_qL
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAUXL_qL(i,j)[d],m,n) = DLA::index(rsdElemAUXL[i][d],m).deriv(slot - nchunk);
            }
          } //n loop
        } //j loop
        slotOffset += nEqn*nDOFL;


        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              //PDEL_qI
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAUXL_qI(i,j)[d],m,n) = DLA::index(rsdElemAUXL[i][d],m).deriv(slot - nchunk);
            }
          } //n loop
        } //j loop
        slotOffset += nEqn*nDOFI;

      } // nchunk loop

//      std::cout << "elemL: " << elemL << std::endl;
//      for (int i = 0; i < nDOFL; i++)
//        for (int j = 0; j < nDOFL; j++)
//          for (int d0 = 0; d0 < D; d0++)
//            for (int d1 = 0; d1 < D; d1++)
//              std::cout << "mtxElemAUXL_aL(" << i << "," << j << ")(" << d0 << "," << d1 << ") = " << mtxElemAUXL_aL(i,j)(d0,d1) << std::endl;

      //Accumulate the auxiliary jacobians on boundary cells
      jacAUX_q_bcell_.getCell(cellGroupGlobalL, elemL) += mtxElemAUXL_qL;
      jacAUX_qI_btrace_.getBoundaryTraceGroupGlobal(traceGroupGlobal)[elem] += mtxElemAUXL_qI;

    } //elem loop
  }

protected:
  const IntegrandBoundaryTrace& fcnBTrace_;
  FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell_;
  FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace_;
  std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI_; //index: [tracegroup][traceelem][dof]
};


// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class VectorMatrixQ>
JacobianBoundaryTrace_HDG_AuxiliaryVariable_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_HDG_AuxiliaryVariable( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcnBTrace,
                                             FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                                             FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                                             std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI)
{
  return { fcnBTrace.cast(), jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobal_qI };
}

}

#endif  // JACOBIANBOUNDARYTRACE_HDG_AUXILIARYVARIABLE_H
