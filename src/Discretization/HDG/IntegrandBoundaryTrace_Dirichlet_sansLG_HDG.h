// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_DIRICHLET_SANSLG_HDG_H
#define INTEGRANDBOUNDARYTRACE_DIRICHLET_SANSLG_HDG_H

// boundary integrand operators

#include <ostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// HDG element boundary integrand: Dirichlet_sansLG

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, HDG> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, HDG> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Dirichlet_sansLG Category;
  typedef HDG DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(
    const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const DiscretizationHDG<PDE>& disc) :
    pde_(pde), bc_(bc),  BoundaryGroups_(BoundaryGroups), disc_(disc) {}

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  bool needsFieldTrace() const { return false; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementAFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandCellType;
    typedef ArrayQ<T> IntegrandTraceType;

    BasisWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem), afldElem_(afldElem),
                   paramfldElem_( paramfldElem ),
                   nDOF_(qfldElem_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      phi_.resize(nDOF_,0);
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCell[],   const int neqnCell,
                                                          ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandCell, neqnCell,
                                                              integrandTrace, neqnTrace );
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandCell[], const int neqnCell,
                                                                       ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable std::vector<Real> phi_;
  };

  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted_AUX
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>      , TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Ta>, TopoDimCell, Topology> ElementAFieldCell;
    typedef Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Ta>::type Ts;

    typedef VectorArrayQ<Ts> IntegrandType;

    BasisWeighted_AUX( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                       const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                       const ElementParam& paramfldElem,
                       const ElementQFieldCell& qfldElem,
                       const ElementAFieldCell& afldElem,
                       const ElementQFieldTrace& qIfldTrace ) :
                         pde_(pde), disc_(disc), bc_(bc),
                         xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                         xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                         qfldElem_(qfldElem), afldElem_(afldElem),
                         qIfldTrace_(qIfldTrace),
                         paramfldElem_( paramfldElem ),
                         nDOFElem_(qfldElem_.nDOF()),
                         nDOFTrace_(qIfldTrace_.nDOF())
    {
      phi_.resize(nDOFElem_,0);
      phiT_.resize(nDOFTrace_,0);
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandL, neqnL );
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> phiT_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementAFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef IntegrandHDG< T, T > IntegrandCellType;
    typedef T IntegrandTraceType;

    FieldWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem,
                   const ElementQFieldCell& wfldElem,
                   const ElementAFieldCell& bfldElem ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem), afldElem_(afldElem),
                   wfldElem_(wfldElem), bfldElem_(bfldElem),
                   paramfldElem_( paramfldElem ),
                   nDOF_(qfldElem_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == bfldElem_.basis() );
      phi_.resize(nDOF_,0);
      wphi_.resize( wfldElem_.nDOF(),0);
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType&  integrandCell,
                                                          IntegrandTraceType& integrandTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandCell, integrandTrace);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandCellType& integrandCell,
                                                                       IntegrandTraceType& integrandTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldCell& wfldElem_;
    const ElementAFieldCell& bfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> wphi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>,       TopoDimCell, Topology >& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell, Topology >& afldElem,
            const Element<ArrayQ<T>,       TopoDimTrace, TopologyTrace>& qIfldTrace) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                  xfldElemTrace, canonicalTrace,
                                                                  paramfldElem, qfldElem, afldElem);
  }

  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted_AUX<Tq, Ta, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand_AUX(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                const ElementParam& paramfldElem,
                const Element<ArrayQ<Tq>, TopoDimCell, Topology >& qfldElem,
                const Element<VectorArrayQ<Ta>, TopoDimCell, Topology >& afldElem,
                const Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace>& qIfldTrace) const
  {
    return BasisWeighted_AUX<Tq, Ta, TopoDimTrace, TopologyTrace,
                             TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                   xfldElemTrace, canonicalTrace,
                                                                   paramfldElem, qfldElem, afldElem, qIfldTrace );
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>,       TopoDimCell, Topology >& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell, Topology >& afldElem,
            const Element<ArrayQ<T>,       TopoDimCell, Topology >& wfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell, Topology >& bfldElem) const
  {
    return FieldWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                  xfldElemTrace, canonicalTrace,
                                                                  paramfldElem,
                                                                  qfldElem, afldElem,
                                                                  wfldElem, bfldElem);
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const DiscretizationHDG<PDE>& disc_;
};


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template <class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, HDG>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCell[],   const int neqnCell,
                                                              ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const
{
  SANS_DEVELOPER_EXCEPTION("HDG BasisWeighted Dirichlet sansLG BC Not implemented yet");
  #if 0
  SANS_ASSERT( neqnCell == nDOF_ );

  VectorX X;                // physical coordinates
  VectorX Nrm;                // unit normal (points out of domain)

  ArrayQ<T> q;                    // solution
  VectorArrayQ<T> a;                // auxiliary variable (gradient)

  QuadPointCellType sRef;              // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, Nrm );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // PDE residual: weak form boundary integral

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<T> F = 0;     // PDE flux

    // advective flux
    pde_.fluxAdvective( X, q, F );

    // viscous flux
    pde_.fluxViscous( X, q, a, F );

    ArrayQ<T> Fn = dot(Nrm,F);
    for (int k = 0; k < neqn; k++)
      integrand[k].PDE += phi_[k]*Fn;

  #if 0
      std::cout << "sRef = " << sRef << "  (s,t) = (" << sRef[0] << ", " << sRef[1] << ")" << std::endl;
      std::cout << "flux = " << flux(0) << std::endl;
      std::cout << "phi = " << phi[0] << " " << phi[1] << " " << phi[2] << std::endl;
      std::cout << "integrandPDE = " << integrand[0].PDE << " " << integrand[1].PDE << " " << integrand[2].PDE << std::endl;
  #endif
  }

  // PDE residual: weighted strong-form BC

  ArrayQ<T> rsdBC = 0;
  MatrixQ<T> wghtBC = 0;
  bc_.strongBC( X, Nrm, q, a, rsdBC );
  bc_.weightBC( X, Nrm, q, a, wghtBC );

  ArrayQ<T> term = wghtBC*rsdBC;
  for (int k = 0; k < neqn; k++)
    integrand[k].PDE -= phi_[k]*term;
#endif
}


template <class PDE, class NDBCVector>
template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class Topology, class ElementParam >
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, HDG>::
BasisWeighted_AUX<Tq,Ta,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL) const
{
  SANS_DEVELOPER_EXCEPTION("HDG BasisWeighted_AUX Dirichlet sansLG BC Not implemented yet");
#if 0
  SANS_ASSERT( (neqnL == nDOFElem_) );

  VectorX X;                  // physical coordinates
  VectorX nL;                 // unit normal (points out of domain)

  ArrayQ<T> qL;               // solution
  ArrayQ<T> qI;               // interface solution

  QuadPointCellType sRef;     // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, nL );
  VectorX nB = -nL;

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), qL );

  // interface solution basis
  qIfldTrace_.evalBasis( sRefTrace, phiT_.data(), phiT_.size() );

  // interface solution
  qIfldTrace_.evalFromBasis( phiT_.data(), phiT_.size(), qI );

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient)
    {
      // - (phiL nL)*qI
      VectorArrayQ<T> qInL;

      for (int d = 0; d < PhysDim::D; d++)
        qInL[d] = qI*nL[d];

      for (int k = 0; k < neqnL; k++)
        integrandL[k] = -phi_[k]*qInL;
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandBoundaryTrace_Dirichlet_sansLG::BasisWeighted_AUX::operator() - Unknown auxiliary variable!" );
  }
#endif
}

template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template <class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Dirichlet_sansLG>, HDG>::
FieldWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandCellType&  integrandCell,
                                                              IntegrandTraceType& integrandTrace  ) const
{
  SANS_DEVELOPER_EXCEPTION("HDG FieldWeighted Dirichlet sansLG BC Not implemented yet");
  #if 0
  VectorX X;                // physical coordinates
  VectorX Nrm;                // unit normal (points out of domain)

  ArrayQ<T> q;                    // solution
  VectorArrayQ<T> a;              // auxiliary variable (gradient)
  ArrayQ<T> w;                    // solution
  VectorArrayQ<T> b;              // auxiliary variable (gradient)

  QuadPointCellType sRef;         // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, Nrm );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

  // basis value, gradient
  wfldElem_.evalBasis( sRef, wphi_.data(), wphi_.size() );

  // solution value, gradient
  wfldElem_.evalFromBasis( wphi_.data(), wphi_.size(), w );

  integrand = 0;

  // PDE residual: weak form boundary integral

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<T> F = 0;     // PDE flux

    // advective flux
    pde_.fluxAdvective( X, q, F );

    // viscous flux
    pde_.fluxViscous( X, q, a, F );

    ArrayQ<T> Fn = dot(Nrm,F);
    integrand.PDE += dot(w,Fn);

  #if 0
      std::cout << "sRef = " << sRef << "  (s,t) = (" << sRef[0] << ", " << sRef[1] << ")" << std::endl;
      std::cout << "flux = " << flux(0) << std::endl;
      std::cout << "phi = " << phi[0] << " " << phi[1] << " " << phi[2] << std::endl;
      std::cout << "integrandPDE = " << integrand[0].PDE << " " << integrand[1].PDE << " " << integrand[2].PDE << std::endl;
  #endif
  }

  // PDE residual: weighted strong-form BC

  ArrayQ<T> rsdBC = 0;
  MatrixQ<T> wghtBC = 0;
  bc_.strongBC( X, Nrm, q, a, rsdBC );
  bc_.weightBC( X, Nrm, q, a, wghtBC );

  ArrayQ<T> term = wghtBC*rsdBC;
  integrand.PDE -= dot(w,term);
  #endif
}

}

#endif  // INTEGRANDBOUNDARYTRACE_DIRICHLET_SANSLG_HDG_H
