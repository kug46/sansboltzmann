// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_HDG_ELEMENT_H
#define JACOBIANINTERIORTRACE_HDG_ELEMENT_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Discretization/JacobianElementMatrix.h"

namespace SANS
{

template<class PhysDim, class MatrixQ>
struct JacobianElemInteriorTrace_HDG : JacElemMatrixType< JacobianElemInteriorTrace_HDG<PhysDim, MatrixQ> >
{
  // PDE Jacobian matrix
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  JacobianElemInteriorTrace_HDG(const JacobianElemInteriorTraceSize& size)
   : nTest(size.nTest),
     nDOFL(size.nDOFL), nDOFI(size.nDOFR),
     PDE_q(size.nTest, size.nDOFL),
     PDE_a(size.nTest, size.nDOFL),
     PDE_qI(size.nTest, size.nDOFR)
  {}

  const int nTest;
  const int nDOFL;
  const int nDOFI;

  // element PDE jacobian matrices wrt q
  MatrixElemClass PDE_q;
  MatrixAElemClass PDE_a;
  MatrixElemClass PDE_qI;

  inline Real operator=( const Real s )
  {
    PDE_q  = s;
    PDE_a  = s;
    PDE_qI = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemInteriorTrace_HDG& operator+=(
      const JacElemMulScalar< JacobianElemInteriorTrace_HDG >& mul )
  {
    PDE_q  += mul.s*mul.mtx.PDE_q;
    PDE_a  += mul.s*mul.mtx.PDE_a;
    PDE_qI += mul.s*mul.mtx.PDE_qI;

    return *this;
  }
};

}
#endif // JACOBIANINTERIORTRACE_HDG_ELEMENT_H
