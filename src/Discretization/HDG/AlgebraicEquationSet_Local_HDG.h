// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_LOCAL_HDG_H
#define ALGEBRAICEQUATIONSET_LOCAL_HDG_H

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Field/FieldTypes.h"
#include "Field/XField.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_Local_HDG : public AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>
{
public:

  typedef AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType> BaseType;

  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename BaseType::VectorSizeClass VectorSizeClass;
  typedef typename BaseType::MatrixSizeClass MatrixSizeClass;

  typedef typename BaseType::SystemMatrix SystemMatrix;
  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemVectorView SystemVectorView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::BCParams BCParams;

  static const int main_cellgroup = 0;

  template< class... BCArgs >
  AlgebraicEquationSet_Local_HDG( const XFieldType& xfld,
                                  Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                                  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& afld,
                                  Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                  Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                  const NDPDEClass& pde,
                                  DiscretizationHDG<NDPDEClass>& disc,
                                  const QuadratureOrder& quadratureOrder,
                                  std::vector<Real>& tol,
                                  const std::vector<int>& CellGroups,
                                  const std::vector<int>& InteriorTraceGroups,
                                  PyDict& BCList,
                                  const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args )
   : BaseType(xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
              CellGroups, {main_cellgroup}, InteriorTraceGroups, BCList, BCBoundaryGroups, args... )
  {
    //DOF counts for sub-system
    sub_qfld_nDOF_ = qfld_.nDOFCellGroup(main_cellgroup); //Re-solving for only main-cell q DOFs in local mesh

    sub_qIfld_nDOF_ = 0; //Re-solving for only main-main and main-neighbor trace qI DOFs in local mesh
    for (int i = 0; i < qIfld_.nInteriorTraceGroups(); i++)
      sub_qIfld_nDOF_ += qIfld_.nDOFInteriorTraceGroup(i);

    //Re-solving for only btrace DOFs of main-cells in the local mesh
    for (int i = 0; i < qIfld_.nBoundaryTraceGroups(); i++)
      sub_qIfld_nDOF_ += qIfld_.nDOFBoundaryTraceGroup(i);

    sub_lgfld_nDOF_ = 0; //Re-solving for only btrace DOFs of main-cells in the local mesh
    for (int i = 0; i < lgfld_.nBoundaryTraceGroups(); i++)
      sub_lgfld_nDOF_ += lgfld_.nDOFBoundaryTraceGroup(i);
  }

  virtual ~AlgebraicEquationSet_Local_HDG() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& jac        ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz ) override {}

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  // Returns the vector and matrix sizes needed for the sub-system (local solve system)
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  //Translates the sub-system vector into a local solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the sub-solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

protected:

  int sub_qfld_nDOF_, sub_qIfld_nDOF_, sub_lgfld_nDOF_; //DOF counts for sub-system unknowns

  void subResidual(const SystemVectorView& rsd_full, SystemVectorView& rsd_sub);

  template<class SparseMatrixType>
  void subJacobian(const SparseMatrixType& jac_full, SparseMatrixType& jac_sub);

  using BaseType::iPDE;
  using BaseType::iINT;
  using BaseType::iBC;
  using BaseType::iq;
  using BaseType::iqI;
  using BaseType::ilg;
  using BaseType::qfld_;
  using BaseType::afld_;
  using BaseType::qIfld_;
  using BaseType::lgfld_;
};

//Computes the sub-residual for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  //Compute the full residual vector of the local problem
  SystemVector rsd_full(BaseType::vectorEqSize());
  rsd_full = 0;
  BaseType::residual(rsd_full);

  //Extract the sub residual vector from the full residual, corresponding to the equations that need to be solved
  subResidual(rsd_full, rsd);
}

//Computes the sub-Jacobian for the local problem
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SystemMatrixView& jac)
{
  //Jacobian 'nonzero' pattern for dense matrix
  SystemNonZeroPattern nz_full(BaseType::matrixSize());

  //Compute the full Jacobian of the local problem
  SystemMatrix jac_full(nz_full);
  jac_full = 0;
  BaseType::jacobian(jac_full);

  //Extract the sub Jacobian from the full Jacobian, containing only the equations that need to be solved
  subJacobian<SystemMatrixView>(jac_full, jac);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
subResidual(const SystemVectorView& rsd_full, SystemVectorView& rsd_sub)
{
  //Dimension-checks
  SANS_ASSERT( rsd_sub.m() == 3 );
  SANS_ASSERT( sub_qfld_nDOF_  == rsd_sub[iq].m() );
  SANS_ASSERT( sub_qIfld_nDOF_ == rsd_sub[iINT].m() );
  SANS_ASSERT( sub_lgfld_nDOF_ == rsd_sub[iBC].m() );

  for (int i=0; i<sub_qfld_nDOF_; i++)
    rsd_sub[iPDE][i] = rsd_full[iPDE][i];

  for (int i=0; i<sub_qIfld_nDOF_; i++)
    rsd_sub[iINT][i] = rsd_full[iINT][i];

  for (int i=0; i<sub_lgfld_nDOF_; i++)
    rsd_sub[iBC][i] = rsd_full[iBC][i];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
subJacobian(const SparseMatrixType& jac_full, SparseMatrixType& jac_sub)
{
  //Dimension-checks
  SANS_ASSERT( jac_sub.m() == 3 );
  SANS_ASSERT( jac_sub.n() == 3 );

  for (int i = 0; i < 3; i++)
  {
    SANS_ASSERT( sub_qfld_nDOF_  == jac_sub(iPDE,i).m() );
    SANS_ASSERT( sub_qIfld_nDOF_ == jac_sub(iINT,i).m() );
    SANS_ASSERT( sub_lgfld_nDOF_ == jac_sub(iBC,i).m());

    SANS_ASSERT( sub_qfld_nDOF_  == jac_sub(i,iq).n() );
    SANS_ASSERT( sub_qIfld_nDOF_ == jac_sub(i,iqI).n() );
    SANS_ASSERT( sub_lgfld_nDOF_ == jac_sub(i,ilg).n());
  }

  for (int i=0; i<sub_qfld_nDOF_; i++)
  {
    for (int j=0; j<sub_qfld_nDOF_; j++)
      jac_sub(iPDE,iq)(i,j) = jac_full(iPDE,iq)(i,j);

    for (int j=0; j<sub_qIfld_nDOF_; j++)
      jac_sub(iPDE,iqI)(i,j) = jac_full(iPDE,iqI)(i,j);

    for (int j=0; j<sub_lgfld_nDOF_; j++)
      jac_sub(iPDE,ilg)(i,j) = jac_full(iPDE,ilg)(i,j);
  }

  for (int i=0; i<sub_qIfld_nDOF_; i++)
  {
    for (int j=0; j<sub_qfld_nDOF_; j++)
      jac_sub(iINT,iq)(i,j) = jac_full(iINT,iq)(i,j);

    for (int j=0; j<sub_qIfld_nDOF_; j++)
      jac_sub(iINT,iqI)(i,j) = jac_full(iINT,iqI)(i,j);

    for (int j=0; j<sub_lgfld_nDOF_; j++)
      jac_sub(iINT,ilg)(i,j) = jac_full(iINT,ilg)(i,j);
  }

  for (int i=0; i<sub_lgfld_nDOF_; i++)
  {
    for (int j=0; j<sub_qfld_nDOF_; j++)
      jac_sub(iBC,iq)(i,j) = jac_full(iBC,iq)(i,j);

    for (int j=0; j<sub_qIfld_nDOF_; j++)
      jac_sub(iBC,iqI)(i,j) = jac_full(iBC,iqI)(i,j);

    for (int j=0; j<sub_lgfld_nDOF_; j++)
      jac_sub(iBC,ilg)(i,j) = jac_full(iBC,ilg)(i,j);
  }
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
std::vector<std::vector<Real>>
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residualNorm(const SystemVectorView& rsd) const
{
  const int nMon = this->pde_.nMonitor();

  DLA::VectorD<Real> rsdPDEtmp(nMon);
  DLA::VectorD<Real> rsdINTtmp(nMon);
  DLA::VectorD<Real> rsdBCtmp(nMon);

  rsdPDEtmp = 0;
  rsdINTtmp = 0;
  rsdBCtmp = 0;

  std::vector<std::vector<Real>> rsdNorm(3, std::vector<Real>(nMon, 0));

  //HACKED Simple L2 norm
  //TODO: Allow for non-L2 norms

  //compute PDE Residual Norm
  for (int n = 0; n < sub_qfld_nDOF_; n++)
  {
    this->pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] += pow(rsdPDEtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);

  //Trace Residual
  for (int n = 0; n < sub_qIfld_nDOF_; n++)
  {
    this->pde_.interpResidVariable(rsd[iINT][n], rsdINTtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iINT][j] += pow(rsdINTtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iINT][j] = sqrt(rsdNorm[iINT][j]);

  //BC residual
  for (int n = 0; n < sub_lgfld_nDOF_; n++)
  {
    this->pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

  return rsdNorm;
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iINT == 1,"");
  static_assert(iBC == 2,"");

  return { sub_qfld_nDOF_,
           sub_qIfld_nDOF_,
           sub_lgfld_nDOF_ };
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorStateSize() const
{
  return vectorEqSize();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iINT == 1,"");
  static_assert(iBC == 2,"");

  static_assert(iq == 0,"");
  static_assert(iqI == 1,"");
  static_assert(ilg == 2,"");

  // jacobian nonzero pattern
  //
  //        q  qI  lg
  //   PDE  X   X   X
  //   Int  X   X   X
  //   BC   X   X   X
  return {{{ sub_qfld_nDOF_,sub_qfld_nDOF_},{ sub_qfld_nDOF_,sub_qIfld_nDOF_},{ sub_qfld_nDOF_,sub_lgfld_nDOF_}},
          {{sub_qIfld_nDOF_,sub_qfld_nDOF_},{sub_qIfld_nDOF_,sub_qIfld_nDOF_},{sub_qIfld_nDOF_,sub_lgfld_nDOF_}},
          {{sub_lgfld_nDOF_,sub_qfld_nDOF_},{sub_lgfld_nDOF_,sub_qIfld_nDOF_},{sub_lgfld_nDOF_,sub_lgfld_nDOF_}}};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  SANS_ASSERT( sub_qfld_nDOF_ == q[iq].m() );
  for (int k = 0; k < sub_qfld_nDOF_; k++)
    qfld_.DOF(k) = q[iq][k];

  SANS_ASSERT( sub_qIfld_nDOF_ == q[iqI].m() );
  for (int k = 0; k < sub_qIfld_nDOF_; k++)
    qIfld_.DOF(k) = q[iqI][k];

  SANS_ASSERT( sub_lgfld_nDOF_ == q[ilg].m() );
  for (int k = 0; k < sub_lgfld_nDOF_; k++)
    lgfld_.DOF(k) = q[ilg][k];

  //Update the auxiliary variable field
  this->computeAuxiliaryVariables(true);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_Local_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  SANS_ASSERT( sub_qfld_nDOF_ == q[iq].m() );
  for (int k = 0; k < sub_qfld_nDOF_; k++)
    q[iq][k] = qfld_.DOF(k);

  SANS_ASSERT( sub_qIfld_nDOF_ == q[iqI].m() );
  for (int k = 0; k < sub_qIfld_nDOF_; k++)
    q[iqI][k] = qIfld_.DOF(k);

  SANS_ASSERT( sub_lgfld_nDOF_ == q[ilg].m() );
  for (int k = 0; k < sub_lgfld_nDOF_; k++)
    q[ilg][k] = lgfld_.DOF(k);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_LOCAL_HDG_H
