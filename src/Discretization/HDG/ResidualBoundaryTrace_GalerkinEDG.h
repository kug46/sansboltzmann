// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_GALERKINEDG_H
#define RESIDUALBOUNDARYTRACE_GALERKINEDG_H

// boundary-trace integral residual functions

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "tools/Tuple.h"

#include "DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class IntegrandBTrace, template<class> class Vector>
class ResidualBoundaryTrace_GalerkinEDG_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_GalerkinEDG_impl<IntegrandBTrace, Vector> >
{
public:
  typedef typename IntegrandBTrace::PhysDim PhysDim;
  typedef typename IntegrandBTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_GalerkinEDG_impl( const IntegrandBTrace& fcn,
                                  Vector<ArrayQ>& rsdPDEGlobal, Vector<ArrayQ>& rsdINTGlobal ) :
                                    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), rsdINTGlobal_(rsdINTGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOF() );
    SANS_ASSERT( rsdINTGlobal_.m() == qIfld.nDOF() );
  }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::
                       template FieldCellGroupType<TopologyL>& qfldCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    typedef typename XFieldType                           ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>      ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // number of integrals evaluated per element
    const int nIntegrandL = qfldElemL.nDOF();
    const int nIntegrandTrace = qIfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nIntegrandL);
    std::vector<int> mapDOFGlobalTrace(nIntegrandTrace);

    // trace element integral
    typedef ArrayQ PDEIntegrandType;
    typedef ArrayQ INTIntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, PDEIntegrandType, INTIntegrandType>
      integral(quadratureorder, nIntegrandL, nIntegrandTrace);

    // element integrand/residuals
    DLA::VectorD<PDEIntegrandType> rsdPDEElemL( nIntegrandL );
    DLA::VectorD<INTIntegrandType> rsdINTElemTrace( nIntegrandTrace );

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      for (int n = 0; n < nIntegrandL; n++) rsdPDEElemL[n] = 0;
      for (int n = 0; n < nIntegrandTrace; n++) rsdINTElemTrace[n] = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL, qIfldElemTrace),
                get<-1>(xfldElemTrace),
                rsdPDEElemL.data(), nIntegrandL,
                rsdINTElemTrace.data(), nIntegrandTrace );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalTrace.data(), nIntegrandTrace );

      for (int n = 0; n < nIntegrandL; n++)
      {
        int nGlobal = mapDOFGlobalL[n];
        rsdPDEGlobal_[nGlobal] += rsdPDEElemL[n];
      }

      for (int n = 0; n < nIntegrandTrace; n++)
      {
        int nGlobal = mapDOFGlobalTrace[n];
        rsdINTGlobal_[nGlobal] += rsdINTElemTrace[n];
      }

    }
  }

protected:
  const IntegrandBTrace& fcn_;

  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdINTGlobal_;
};

// Factory function

template<class IntegrandBTrace, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_GalerkinEDG_impl<IntegrandBTrace, Vector>
ResidualBoundaryTrace_GalerkinEDG( const IntegrandBoundaryTraceType<IntegrandBTrace>& fcn,
                           Vector<ArrayQ>& rsdPDEGlobal, Vector<ArrayQ>& rsdINTGlobal)
{
//  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same< ArrayQ, typename IntegrandBTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualBoundaryTrace_GalerkinEDG_impl<IntegrandBTrace, Vector>(fcn.cast(), rsdPDEGlobal, rsdINTGlobal);
}

}

#endif  // RESIDUALBOUNDARYTRACE_GALERKINEDG_H
