// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_RESIDUALINTERIORTRACE_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_RESIDUALINTERIORTRACE_HDGADVECTIVE_H_

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "tools/Tuple.h"

#include "DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class IntegrandInteriorTrace, template<class> class Vector, class TR>
class ResidualInteriorTrace_HDGAdvective_impl :
    public GroupIntegralInteriorTraceType<
             ResidualInteriorTrace_HDGAdvective_impl<IntegrandInteriorTrace, Vector, TR> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template ArrayQ<TR> ArrayQR;

  typedef typename DLA::VectorD<ArrayQR> ResidualElemClass;

  // Save off the boundary trace integrand and the residual vectors
  ResidualInteriorTrace_HDGAdvective_impl(
    const IntegrandInteriorTrace& fcn,
    Vector<ArrayQ>& rsdPDEGlobal,
    Vector<ArrayQ>& rsdINTGlobal) :
      fcn_(fcn),
      rsdPDEGlobal_(rsdPDEGlobal),
      rsdINTGlobal_(rsdINTGlobal) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOF() );
    SANS_ASSERT( rsdINTGlobal_.m() == qIfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      const int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFTrace = qIfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nDOFL, -1 );
    std::vector<int> mapDOFGlobalR( nDOFR, -1 );
    std::vector<int> mapDOFGlobalTrace( nDOFTrace, -1 );

    // element integrand/residuals
    ResidualElemClass rsdPDEElemL( nDOFL );
    ResidualElemClass rsdPDEElemR( nDOFR );
    ResidualElemClass rsdINTElemTraceL( nDOFTrace ); // contribution of left element
    ResidualElemClass rsdINTElemTraceR( nDOFTrace ); // contribution of left element

    // trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, ResidualElemClass, ResidualElemClass>
      integral(quadratureorder);

    // loop over elements within the trace group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; ++elem)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // get residual indexing/mapping
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), mapDOFGlobalL.size() );
      qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), mapDOFGlobalR.size() );
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size() );

      //TODO: to be extended to support parallel runs

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      auto integrandFunctorL = fcn_.integrand( xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                               xfldElemL, qfldElemL );
      auto integrandFunctorR = fcn_.integrand( xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                               xfldElemR, qfldElemR );

      // reset, evaluate elemental residuals
      rsdPDEElemL = 0.0;
      rsdINTElemTraceL = 0.0;
      integral(integrandFunctorL, xfldElemTrace, rsdPDEElemL, rsdINTElemTraceL);

      rsdPDEElemR = 0.0;
      rsdINTElemTraceR = 0.0;
      integral(integrandFunctorR, xfldElemTrace, rsdPDEElemR, rsdINTElemTraceR);

      // scatter-add element residuals to global residuals
      for (int n = 0; n < nDOFL; ++n)
        rsdPDEGlobal_[ mapDOFGlobalL[n] ] += rsdPDEElemL[n];

      for (int n = 0; n < nDOFR; ++n)
        rsdPDEGlobal_[ mapDOFGlobalR[n] ] += rsdPDEElemR[n];

      for (int n = 0; n < nDOFTrace; ++n)
        rsdINTGlobal_[ mapDOFGlobalTrace[n] ] += rsdINTElemTraceL[n] + rsdINTElemTraceR[n];
    } // elem loop
  }

protected:
  const IntegrandInteriorTrace& fcn_;

  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdINTGlobal_;
};


// Factory function
template<class IntegrandInteriorTrace, template<class> class Vector, class ArrayQ>
ResidualInteriorTrace_HDGAdvective_impl<IntegrandInteriorTrace, Vector, typename Scalar<ArrayQ>::type>
ResidualInteriorTrace_HDGAdvective(
  const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
  Vector<ArrayQ>& rsdPDEGlobal, Vector<ArrayQ>& rsdINTGlobal)
{
  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same< ArrayQ, typename IntegrandInteriorTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualInteriorTrace_HDGAdvective_impl<IntegrandInteriorTrace, Vector, T>(
           fcn.cast(), rsdPDEGlobal, rsdINTGlobal);
}

}

#endif /* SRC_DISCRETIZATION_HDG_RESIDUALINTERIORTRACE_HDGADVECTIVE_H_ */
