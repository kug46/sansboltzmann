// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_FIELDTRACE_HDGADVECTIVE_PARAM_H_
#define SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_FIELDTRACE_HDGADVECTIVE_PARAM_H_


// jacobian boundary-trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/GroupElementType.h"
#include "Field/Tuple/SurrealizedElementTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral without Lagrange multipliers
//

template<class Surreal, int iParam, class IntegrandBoundaryTrace, class MatrixQP_>
class JacobianBoundaryTrace_mitLG_HDGAdvective_Param_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_mitLG_HDGAdvective_Param_impl<Surreal, iParam, IntegrandBoundaryTrace, MatrixQP_> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;

  JacobianBoundaryTrace_mitLG_HDGAdvective_Param_impl(const IntegrandBoundaryTrace& fcn,
                                                      MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p,
                                                      MatrixScatterAdd<MatrixQP_>& mtxGlobalINT_p,
                                                      MatrixScatterAdd<MatrixQP_>& mtxGlobalBC_p ) :
    fcn_(fcn),
    mtxGlobalPDE_p_(mtxGlobalPDE_p),
    mtxGlobalINT_p_(mtxGlobalINT_p),
    mtxGlobalBC_p_(mtxGlobalBC_p) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds)
  {
    const auto& qIfld = get<0>(flds);
    const auto& lgfld = get<1>(flds);

    SANS_ASSERT( mtxGlobalPDE_p_.m() == qfld.nDOF() );
//    SANS_ASSERT( mtxGlobalPDE_p_.n() == paramfld.nDOF() ); //TODO: have to be able to access paramfld

    SANS_ASSERT( mtxGlobalINT_p_.m() == qIfld.nDOF() );
//    SANS_ASSERT( mtxGlobalINT_p_.n() == paramfld.nDOF() ); //TODO: have to be able to access paramfld

    SANS_ASSERT( mtxGlobalBC_p_.m() == lgfld.nDOF() );
//    SANS_ASSERT( mtxGlobalBC_p_.n() == paramfld.nDOF() ); //TODO: have to be able to access paramfld
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class TupleFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename TupleFieldType                 ::template FieldCellGroupType<TopologyL>& tuplefldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XField<PhysDim, TopoDim>       ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                              template FieldTraceGroupType<TopologyTrace>& fldsTrace,
             int quadratureorder )
  {
    SANS_DEVELOPER_EXCEPTION("JacobianBoundaryTrace_mitLG_HDGAdvective_Param_impl::integrate - Not implemented yet.");
#if 0
    typedef typename TupleFieldType                 ::template FieldCellGroupType<TopologyL> TupleFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename TupleType<iParam, TupleFieldCellGroupTypeL>::type                       ParamCellGroupTypeL;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeL, Surreal, iParam>::type ElementTupleFieldClassL;

    typedef typename ParamCellGroupTypeL ::template ElementType<Surreal> ElementParamFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementQFieldClassL;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename TupleFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef typename ElementParamFieldClassL::T ArrayPSurreal;

    const int nArrayP = DLA::VectorSize<ArrayPSurreal>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

//    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP;

    typedef DLA::MatrixD<MatrixQP_> MatrixElemClass;

    // field groups
    const ParamCellGroupTypeL& paramfldCellL = get<iParam>(tuplefldCellL);

    // field elements
    ElementTupleFieldClassL  tuplefldElemL( tuplefldCellL.basis() );
    ElementQFieldClassL      qfldElemL( qfldCellL.basis() );
    ElementParamFieldClassL& paramfldElemL = set<iParam>(tuplefldElemL);

    ElementXFieldTraceClass  xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

    // DOF counts
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFTrace = lgfldElemTrace.nDOF();
    const int paramDOFL = paramfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapQDOFGlobalL(nDOFL,-1);
    std::vector<int> mapDOFGlobalTrace(nDOFTrace,-1);
    std::vector<int> mapParamDOFGlobalL(paramDOFL,-1);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal> integral(quadratureorder, nDOFL, nDOFTrace);

    // element integrand/residual
    std::vector<ArrayQSurreal> rsdPDEElemL(nDOFL);
    std::vector<ArrayQSurreal> rsdBCTrace(nDOFTrace);

    // element jacobian matrix
    MatrixElemClass mtxPDEElemL_pL(nDOFL, paramDOFL);
    MatrixElemClass mtxBCElemL_pL(nDOFTrace, paramDOFL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobian to zero
      mtxPDEElemL_pL = 0;
      mtxBCElemL_pL = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // right element, hub trace or field trace
      int elemR;
      if ( xfldTrace.getGroupRightType() == eHubTraceGroup )
        elemR = xfldTrace.getElementRight( elem );
      else
        elemR = elem;

      // copy global parameter/solution DOFs to element
      tuplefldCellL.getElement( tuplefldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement(  xfldElemTrace, elem );

      lgfldTrace.getElement( lgfldElemTrace, elemR );

      // number of simultaneous derivatives per functor call
#if 1 // since we are taking derivatives w.r.t. param
      const int nDeriv = DLA::index(paramfldElemL.DOF(0), 0).size();
#else
      const int nDeriv = Surreal::N;
#endif

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nArrayP*paramDOFL; nchunk += nDeriv)
      {
        // associate derivative slots with solution variables

        int slot = 0;
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(paramfldElemL.DOF(j), n).deriv(k) = 0;

            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element

        // reset PDE residuals to zero
        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        for (int n = 0; n < nDOFTrace; n++)
          rsdBCTrace[n] = 0;

        integral(fcn_.integrand(xfldElemTrace, canonicalTraceL,
                                tuplefldElemL, qfldElemL,
                                lgfldElemTrace),
                 get<-1>(xfldElemTrace),
                 rsdPDEElemL.data(), nDOFL,
                 rsdBCTrace.data(), nDOFTrace );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemL_pL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFTrace; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxBCElemL_pL(i,j), m,n) = DLA::index(rsdBCTrace[i], m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global

      scatterAdd( qfldCellL, lgfldTrace, paramfldCellL,
                  elemL, elemR,
                  mapQDOFGlobalL.data(), nDOFL,
                  mapDOFGlobalTrace.data(), nDOFTrace,
                  mapParamDOFGlobalL.data(), paramDOFL,
                  mtxPDEElemL_pL, mtxBCElemL_pL,
                  mtxGlobalPDE_p_, mtxGlobalBC_p_ );
    }
#endif
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalINT_p_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalBC_p_;
};

// Factory function
template<class Surreal, int iParam, class IntegrandBoundaryTrace, class MatrixQP>
JacobianBoundaryTrace_mitLG_HDGAdvective_Param_impl<Surreal, iParam, IntegrandBoundaryTrace, MatrixQP>
JacobianBoundaryTrace_mitLG_HDGAdvective_Param( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                                MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p,
                                                MatrixScatterAdd<MatrixQP>& mtxGlobalBC_p )
{
  return { fcn.cast(), mtxGlobalPDE_p, mtxGlobalBC_p };
}

} // namespace SANS

#endif /* SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_FIELDTRACE_HDGADVECTIVE_PARAM_H_ */
