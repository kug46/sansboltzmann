// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_HDG_H
#define JACOBIANCELL_HDG_H

// HDG cell integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryTrace.h"
#include "Field/XField_CellToTrace.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Discretization/HDG/JacobianInteriorTrace_HDG.h"
#include "Discretization/HDG/JacobianInteriorTrace_HDG_AuxiliaryVariable.h"

#include "JacobianCell_HDG_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral jacobian
//

template<class Surreal, class IntegrandCell, class IntegrandITrace, class XFieldType_, class TopoDim_>
class JacobianCell_HDG_impl :
    public GroupIntegralCellType< JacobianCell_HDG_impl<Surreal, IntegrandCell, IntegrandITrace, XFieldType_, TopoDim_> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::PDE PDE;

  static_assert( std::is_same< PhysDim, typename IntegrandITrace::PhysDim >::value, "PhysDim should be the same.");

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename IntegrandCell::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandCell::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;


  // Save off the boundary trace integrand and the residual vectors
  JacobianCell_HDG_impl( const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace,
                         const FieldDataInvMassMatrix_Cell& mmfld,
                         const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                         const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                         const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                         const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                         const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace,
                         const XFieldType_& xfld,
                         const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                         const Field<PhysDim, TopoDim_, VectorArrayQ>& afld,
                         const Field<PhysDim, TopoDim_, ArrayQ>& qIfld,
                         const int quadOrderITrace[], const int nITraceGroup,
                         const std::vector<int>& cellgroupsAux,
                         MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                         MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                         MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                         MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI ) :
                           fcnCell_(fcnCell), fcnITrace_(fcnITrace),
                           mmfld_(mmfld), invJacAUX_a_bcell_(invJacAUX_a_bcell),
                           jacAUX_q_bcell_(jacAUX_q_bcell), jacAUX_qI_btrace_(jacAUX_qI_btrace),
                           mapDOF_boundary_qI_(mapDOF_boundary_qI), xfldCellToTrace_(xfldCellToTrace),
                           xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
                           quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup),
                           cellgroupsAux_(cellgroupsAux),
                           mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
                           mtxGlobalINT_q_(mtxGlobalINT_q), mtxGlobalINT_qI_(mtxGlobalINT_qI),
                           comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_qI_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_qI_.n() == qIfld_.nDOFpossessed() + qIfld_.nDOFghost() );

    SANS_ASSERT( mtxGlobalINT_q_.m() == qIfld_.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalINT_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalINT_qI_.m() == qIfld_.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalINT_qI_.n() == qIfld_.nDOFpossessed() + qIfld_.nDOFghost() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                          ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;
    typedef typename AFieldCellGroupType::template ElementType<Surreal> ElementAFieldSurrealClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename AFieldCellGroupType::template ElementType<> ElementAFieldClass;

    typedef JacobianElemCell_HDG<PhysDim,MatrixQ> JacobianElemCellType;

    const int D = PhysDim::D;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const AFieldCellGroupType& afldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldSurrealClass qfldElemSurreal( qfldCell.basis() );
    ElementAFieldSurrealClass afldElemSurreal( afldCell.basis() );
    ElementQFieldClass        qfldElem       ( qfldCell.basis() );
    ElementAFieldClass        afldElem       ( afldCell.basis() );

    // Inverse mass matrix used to compute auxiliary variables
    const DLA::MatrixDView_Array<Real>& mmfldCell = mmfld_.getCellGroupGlobal(cellGroupGlobal);

    // Check if this auxiliary variables were computed for this cellgroup, if so, complete the chain rules
    bool completeAuxChainRule = false;
    for (std::size_t i = 0; i < cellgroupsAux_.size(); i++)
    {
      if (cellgroupsAux_[i] == cellGroupGlobal)
      {
        completeAuxChainRule = true;
        break;
      }
    }

    // DOFs per element
    const int qDOF = qfldElemSurreal.nDOF();
    const int aDOF = afldElemSurreal.nDOF();

    const int nIntegrandPDE = qDOF;
    const int nIntegrandAUX = aDOF;

    // variables/equations per DOF
    const int nEqn = IntegrandCell::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_q(qDOF, -1);

    // element integrals
    //GalerkinWeightedIntegral<TopoDim, Topology, ArrayQSurreal> integralPDE(quadratureorder, nIntegrandPDE);
    GalerkinWeightedIntegral_New<TopoDim, Topology, JacobianElemCellType > integralPDE(quadratureorder);
    GalerkinWeightedIntegral<TopoDim, Topology, VectorArrayQSurreal> integralAUX(quadratureorder, nIntegrandAUX);

    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == afldCell.nElem() );

    // element integrand/residual
    DLA::VectorD<ArrayQSurreal> rsdElemPDE( nIntegrandPDE );

    // auxiliary variable residuals
    DLA::VectorD<VectorArrayQSurreal> rsdElemAUX( nIntegrandAUX );

    // element jacobian matrices
    //MatrixElemClass mtxElemPDE_q(qDOF, qDOF);
    //MatrixAElemClass mtxElemPDE_a(qDOF, aDOF);
    JacobianElemCellType mtxElem(qDOF);

    MatrixAUXElemClass mtxElemAUX_q(aDOF, qDOF);

    //storage for auxiliary variable jacobians wrt q and qI on each trace of this cell
    MatrixAUXElemClass mtx_a_q(aDOF, qDOF);
    std::vector<MatrixAUXElemClass> mtx_a_qI(Topology::NTrace, MatrixAUXElemClass(0, 0));
    std::vector<std::vector<int>> mapDOFGlobal_qI(Topology::NTrace);

    std::vector<MatrixAUXElemClass> mtxElemAUX_qI(Topology::NTrace, MatrixAUXElemClass(0, 0));

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group and solve for the auxiliary variables first
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElemSurreal, elem );
      qfldCell.getElement( qfldElem       , elem );
      afldCell.getElement( afldElemSurreal, elem );
      afldCell.getElement( afldElem       , elem );


      std::map<int,std::vector<int>> cellITraceGroups;
      bool isBoundaryCell = false;

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, elem, trace);

        mtxElemAUX_qI[trace].resize(0, 0);
        mtx_a_qI[trace].resize(0, 0);
        mapDOFGlobal_qI[trace].clear();

        if (traceinfo.type == TraceInfo::Interior)
        {
          //add this traceGroup and traceElem to the set of interior traces attached to this cell
          //but only if the qIfld actually has this traceGroup
          if (qIfld_.getLocalInteriorTraceGroups()[traceinfo.group] >= 0)
            cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::Boundary)
        {
          isBoundaryCell = true;

          if ( mapDOF_boundary_qI_[traceinfo.group].size() > 0 ) //some boundary-trace groups might have been omitted
          {
            //Copy the globalDOFmap for qI DOFs on this boundary trace
            mapDOFGlobal_qI[trace] = mapDOF_boundary_qI_[traceinfo.group][traceinfo.elem];

            //Copy the previously stored auxiliary variable jacobian
            mtxElemAUX_qI[trace].resize(aDOF, mapDOFGlobal_qI[trace].size());
            mtxElemAUX_qI[trace] = jacAUX_qI_btrace_.getBoundaryTraceGroupGlobal(traceinfo.group)[traceinfo.elem];
          }
        }
      }

      // zero element Jacobians
      mtxElemAUX_q = 0;
      mtx_a_q = 0;

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*qDOF; nchunk += nDeriv)
      {
        // associate derivative slots with solution & auxiliary variables

        int slot, slotOffset = 0;

        //wrt q
        for (int j = 0; j < qDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemSurreal.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset = nEqn*qDOF;

        //-----------------------------------------------------------------------------

        for (int n = 0; n < nIntegrandAUX; n++) rsdElemAUX[n] = 0;

        // cell integration for canonical element
        integralAUX( fcnCell_.integrand_AUX(get<-1>(xfldElem), qfldElemSurreal, afldElem),
                     get<-1>(xfldElem), rsdElemAUX.data(), nIntegrandAUX );

        //-----------------------------------------------------------------------------

        // accumulate derivatives into element jacobians
        slotOffset = 0;

        for (int j = 0; j < qDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemSurreal.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              for (int i = 0; i < qDOF; i++)
              {
                for (int m = 0; m < nEqn; m++)
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAUX_q(i,j)[d],m,n) = DLA::index(rsdElemAUX[i][d],m).deriv(slot - nchunk);
              }
            }
          }
        }
        slotOffset = nEqn*qDOF;

      }  // nchunk


      //Get auxiliary jacobian contributions from interior traces around this cell
      IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
          JacobianInteriorTrace_HDG_AuxiliaryVariable<Surreal>(fcnITrace_, cellITraceGroups,
                                                               cellGroupGlobal, elem,
                                                               mtxElemAUX_q, mtxElemAUX_qI, mapDOFGlobal_qI),
          xfld_, (qfld_, afld_), qIfld_,
          quadOrderITrace_, nITraceGroup_ );

      // Solve for the auxiliary variable jacobians on this cell
      if (!isBoundaryCell)
      {
        if (completeAuxChainRule)
          mtx_a_q = -mmfldCell[elem] * mtxElemAUX_q;

        for (int trace = 0; trace < (int) Topology::NTrace; trace++)
        {
          if (mapDOFGlobal_qI[trace].size() > 0) //some interior-trace groups might have been omitted
          {
            mtx_a_qI[trace].resize(aDOF, mapDOFGlobal_qI[trace].size());

            if (completeAuxChainRule)
              mtx_a_qI[trace] = -mmfldCell[elem] * mtxElemAUX_qI[trace];
            else
              mtx_a_qI[trace] = 0.0;
          }
        }
      }
      else
      {
        if (completeAuxChainRule)
        {
          //Add the auxiliary jacobian contributions from the boundary conditions
          mtxElemAUX_q += jacAUX_q_bcell_.getCell(cellGroupGlobal, elem);
          mtx_a_q = -invJacAUX_a_bcell_.getCell(cellGroupGlobal, elem) * mtxElemAUX_q;
        }

        for (int trace = 0; trace < (int) Topology::NTrace; trace++)
        {
          if (mapDOFGlobal_qI[trace].size() > 0) //some boundary-trace groups might have been omitted
          {
            mtx_a_qI[trace].resize(aDOF, mapDOFGlobal_qI[trace].size());

            if (completeAuxChainRule)
              mtx_a_qI[trace] = -invJacAUX_a_bcell_.getCell(cellGroupGlobal, elem) * mtxElemAUX_qI[trace];
            else
              mtx_a_qI[trace] = 0.0;
          }
        }
      }

      //-----------------------------------------------------------------------------------------

      // cell integration for canonical element
      integralPDE( fcnCell_.integrand_PDE(xfldElem, qfldElem, afldElem),
                   get<-1>(xfldElem), mtxElem );

#if 0
      // zero element Jacobians
      mtxElemPDE_q = 0;
      mtxElemPDE_a = 0;

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(qDOF + D*aDOF); nchunk += nDeriv)
      {
        // associate derivative slots with solution & auxiliary variables

        int slot, slotOffset = 0;

        //wrt q
        for (int j = 0; j < qDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemSurreal.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset = nEqn*qDOF;

        //wrt a
        for (int j = 0; j < aDOF; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemSurreal.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }
        slotOffset += nEqn*D*aDOF;

        //-----------------------------------------------------------------------------

        for (int n = 0; n < nIntegrandPDE; n++) rsdElemPDE[n] = 0;

        // cell integration for canonical element
        integralPDE( fcnCell_.integrand_PDE(xfldElem, qfldElemSurreal, afldElemSurreal),
                     get<-1>(xfldElem), rsdElemPDE.data(), nIntegrandPDE );

        //-----------------------------------------------------------------------------

        // accumulate derivatives into element jacobians
        slotOffset = 0;

        //wrt q
        for (int j = 0; j < qDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemSurreal.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              for (int i = 0; i < qDOF; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemPDE_q(i,j),m,n) = DLA::index(rsdElemPDE[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset = nEqn*qDOF;

        //wrt a
        for (int j = 0; j < aDOF; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(afldElemSurreal.DOF(j)[d],n).deriv(slot - nchunk) = 0; //unset derivative

                for (int i = 0; i < qDOF; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemPDE_a(i,j)(0,d),m,n) = DLA::index(rsdElemPDE[i],m).deriv(slot - nchunk);
              }
            }
          }
        }
        slotOffset += nEqn*D*aDOF;

      } // nchunk
#endif

      std::vector<MatrixElemClass> mtxElemPDE_qI(Topology::NTrace, MatrixElemClass(0,0));

      //Chain rule for auxiliary variables
      if (completeAuxChainRule)
        mtxElem.PDE_q += mtxElem.PDE_a * mtx_a_q;

      //Chain rule with respect to qI DOFs on the interior traces of this cell
      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        if (mapDOFGlobal_qI[trace].size() > 0) //some boundary-trace groups might have been omitted
        {
          mtxElemPDE_qI[trace].resize( qDOF, (int) mapDOFGlobal_qI[trace].size() );

          if (completeAuxChainRule)
            mtxElemPDE_qI[trace] = mtxElem.PDE_a * mtx_a_qI[trace];
          else
            mtxElemPDE_qI[trace] = 0.0;
        }
      }

      //Collect the contributions to the PDE residual jacobian wrt q from the traces
      //Other jacobians (i.e. PDE_qI, INT_q, and INT_qI) will be scatter-added inside JacobianInteriorTrace_HDG

      IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
          JacobianInteriorTrace_HDG<Surreal>(fcnITrace_, cellITraceGroups,
                                             cellGroupGlobal, elem,
                                             mtx_a_q, mtx_a_qI, mapDOFGlobal_qI,
                                             mtxElem.PDE_q, mtxGlobalPDE_qI_,
                                             mtxGlobalINT_q_, mtxGlobalINT_qI_),
          xfld_, (qfld_, afld_), qIfld_,
          quadOrderITrace_, nITraceGroup_ );

      // only possessed cell elements have PDE residuals on this processor
      if ( qfldElem.rank() != comm_rank_ ) continue;

      // scatter-add element jacobian to global
      scatterAdd( qfldCell, elem,
                  mapDOFGlobal_q.data(), qDOF, mapDOFGlobal_qI,
                  mtxElem.PDE_q, mtxElemPDE_qI,
                  mtxGlobalPDE_q_, mtxGlobalPDE_qI_);
    } //elem
  }

//----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, template <class> class SparseMatrixType>
  void
  scatterAdd( const QFieldCellGroupType& qfld,
              const int elem, int mapDOFGlobal_q[], const int qDOF,
              std::vector<std::vector<int>> mapDOFGlobal_qI,
              const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDE_q,
              const std::vector<MatrixElemClass>& mtxElemPDE_qI,
              SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
              SparseMatrixType<MatrixQ>& mtxGlobalPDE_qI )
  {
    // jacobian wrt q
    qfld.associativity( elem ).getGlobalMapping( mapDOFGlobal_q, qDOF );

    mtxGlobalPDE_q.scatterAdd( mtxElemPDE_q, mapDOFGlobal_q, qDOF );

    // jacobians wrt qI
    for (int trace = 0; trace < (int) mapDOFGlobal_qI.size(); trace++)
    {
      if (mapDOFGlobal_qI[trace].size() > 0) //some boundary-trace groups might have been omitted
      {
        mtxGlobalPDE_qI.scatterAdd( mtxElemPDE_qI[trace], mapDOFGlobal_q, qDOF, mapDOFGlobal_qI[trace].data(), mapDOFGlobal_qI[trace].size() );
      }
    }
  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell_;
  const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell_;
  const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace_;
  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI_;

  const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace_;
  const XFieldType_& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld_;

  const int *quadOrderITrace_;
  const int nITraceGroup_;
  const std::vector<int>& cellgroupsAux_; //cellgroups for which auxiliary variables were computed

  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;

  mutable int comm_rank_;
};

// Factory function

template<class Surreal, class IntegrandCell, class IntegrandITrace,
         class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class MatrixQ, class TensorMatrixQ, class VectorMatrixQ>
JacobianCell_HDG_impl<Surreal, IntegrandCell, IntegrandITrace, XFieldType, TopoDim>
JacobianCell_HDG( const IntegrandCellType<IntegrandCell>& fcnCell,
                  const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                  const FieldDataInvMassMatrix_Cell& mmfld,
                  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                  const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                  const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                  const XField_CellToTrace<PhysDim, TopoDim>& xfldCellToTrace,
                  const XFieldType& xfld,
                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                  const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                  const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                  const int quadOrderITrace[], const int nITraceGroup,
                  const std::vector<int>& cellgroupsAux,
                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI )
{
  return { fcnCell.cast(), fcnITrace.cast(),
           mmfld, invJacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOF_boundary_qI,
           xfldCellToTrace, xfld, qfld, afld, qIfld,
           quadOrderITrace, nITraceGroup, cellgroupsAux,
           mtxGlobalPDE_q, mtxGlobalPDE_qI,
           mtxGlobalINT_q, mtxGlobalINT_qI };
}

}

#endif  // JACOBIANCELL_HDG_H
