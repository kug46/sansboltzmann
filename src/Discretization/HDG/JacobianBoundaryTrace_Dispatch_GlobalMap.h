// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DISPATCH_GLOBALMAP_H
#define JACOBIANBOUNDARYTRACE_DISPATCH_GLOBALMAP_H

// boundary-trace integral jacobian functions

#include "JacobianBoundaryTrace_GlobalMap.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
//#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class JacobianBoundaryTrace_Dispatch_GlobalMap_impl
{
public:
  JacobianBoundaryTrace_Dispatch_GlobalMap_impl(const XFieldType& xfld,
                                                            const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                            const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                                            const int quadOrderBTrace[], const int nBTraceGroup,
                                                            std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI ) :
                                                              xfld_(xfld), qfld_(qfld), qIfld_(qIfld),
                                                              quadOrderBTrace_(quadOrderBTrace), nBTraceGroup_(nBTraceGroup),
                                                              mapDOFGlobal_qI_(mapDOFGlobal_qI)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcnBTrace)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_GlobalMap<Surreal>( fcnBTrace, mapDOFGlobal_qI_ ),
        xfld_, qfld_, qIfld_, quadOrderBTrace_, nBTraceGroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;

  const int *quadOrderBTrace_, nBTraceGroup_;

  std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
JacobianBoundaryTrace_Dispatch_GlobalMap_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ>
JacobianBoundaryTrace_Dispatch_GlobalMap(
    const XFieldType& xfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const int quadOrderBTrace[], const int nBTraceGroup,
    std::vector<std::vector<std::vector<int>>>& mapDOFGlobal_qI )
{
  return { xfld, qfld, qIfld,
           quadOrderBTrace, nBTraceGroup, mapDOFGlobal_qI };
}

}

#endif //JACOBIANBOUNDARYTRACE_DISPATCH_GLOBALMAP_H
