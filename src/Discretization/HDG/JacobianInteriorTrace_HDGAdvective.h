// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_JACOBIANINTERIORTRACE_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_JACOBIANINTERIORTRACE_HDGADVECTIVE_H_

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin_Element.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class IntegrandInteriorTrace>
class JacobianInteriorTrace_HDGAdvective_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_HDGAdvective_impl<IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;

  typedef typename IntegrandInteriorTrace::JacobianElemType JacobianElemType;
  typedef typename IntegrandInteriorTrace::JacobianElemSizeType JacobianElemSizeType;

  // Save off the boundary trace integrand and the residual vectors
  JacobianInteriorTrace_HDGAdvective_impl( const IntegrandInteriorTrace& fcn,
                                           MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                           MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                           MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                           MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q),
    mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
    mtxGlobalINT_q_(mtxGlobalINT_q),
    mtxGlobalINT_qI_(mtxGlobalINT_qI) {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalPDE_qI_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_qI_.n() == qIfld.nDOF() );

    SANS_ASSERT( mtxGlobalINT_q_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalINT_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalINT_qI_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalINT_qI_.n() == qIfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Cell types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL, -1);
    std::vector<int> mapDOFGlobalR(nDOFR, -1);
    std::vector<int> mapDOFGlobalTrace(nDOFI, -1);

    // element jacobians
    // jacobian of PDE associated with left element
    JacobianElemType mtxElemL(JacobianElemSizeType(nDOFL, nDOFL, nDOFI));
    // jacobian of interface equation's contribution from left element
    JacobianElemType mtxElemIL(JacobianElemSizeType(nDOFI, nDOFL, nDOFI));
    // jacobian of interface equation's contribution from right element
    JacobianElemType mtxElemIR(JacobianElemSizeType(nDOFI, nDOFR, nDOFI));
    // jacobian of PDE associated with right element
    JacobianElemType mtxElemR(JacobianElemSizeType(nDOFR, nDOFR, nDOFI));

    // trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemType, JacobianElemType>
      integralPDEL(quadratureorder);

    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemType, JacobianElemType>
      integralPDER(quadratureorder);

    // loop over elements within the trace group
    const int nelem = qIfldTrace.nElem();
    for (int elem = 0; elem < nelem; ++elem)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // get residual indexing/mapping
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), mapDOFGlobalL.size() );
      qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), mapDOFGlobalR.size() );
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size() );

      //TODO: to be extended to support parallel runs

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      auto integrandFunctorL = fcn_.integrand( xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                               xfldElemL, qfldElemL );
      auto integrandFunctorR = fcn_.integrand( xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                               xfldElemR, qfldElemR );

      // reset and then evaluate jacobians
      mtxElemL = 0.0;
      mtxElemIL = 0.0;
      integralPDEL(integrandFunctorL, xfldElemTrace, mtxElemL, mtxElemIL);

      mtxElemR = 0.0;
      mtxElemIR = 0.0;
      integralPDER(integrandFunctorR, xfldElemTrace, mtxElemR, mtxElemIR);

      // scatter-add element jacobians to global jacobians
      mtxGlobalPDE_q_.scatterAdd( mtxElemL.PDE_qL,
                                  mapDOFGlobalL.data(), mapDOFGlobalL.size(),
                                  mapDOFGlobalL.data(), mapDOFGlobalL.size() );
      mtxGlobalPDE_q_.scatterAdd( mtxElemR.PDE_qL,
                                  mapDOFGlobalR.data(), mapDOFGlobalR.size(),
                                  mapDOFGlobalR.data(), mapDOFGlobalR.size() );

      mtxGlobalPDE_qI_.scatterAdd( mtxElemL.PDE_qR,
                                   mapDOFGlobalL.data(), mapDOFGlobalL.size(),
                                   mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size() );
      mtxGlobalPDE_qI_.scatterAdd( mtxElemR.PDE_qR,
                                   mapDOFGlobalR.data(), mapDOFGlobalR.size(),
                                   mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size() );

      mtxGlobalINT_q_.scatterAdd( mtxElemIL.PDE_qL,
                                  mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size(),
                                  mapDOFGlobalL.data(), mapDOFGlobalL.size() );
      mtxGlobalINT_q_.scatterAdd( mtxElemIR.PDE_qL,
                                  mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size(),
                                  mapDOFGlobalR.data(), mapDOFGlobalR.size() );

      mtxGlobalINT_qI_.scatterAdd( mtxElemIL.PDE_qR,
                                   mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size(),
                                   mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size() );
      mtxGlobalINT_qI_.scatterAdd( mtxElemIR.PDE_qR,
                                   mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size(),
                                   mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size() );
    } // elem loop
  }

protected:
  const IntegrandInteriorTrace& fcn_;

  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;
};

// Factory function
template<class IntegrandInteriorTrace, class MatrixQ>
JacobianInteriorTrace_HDGAdvective_impl<IntegrandInteriorTrace>
JacobianInteriorTrace_HDGAdvective( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                    MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                    MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI)
{
  return { fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_qI, mtxGlobalINT_q, mtxGlobalINT_qI };
}

}

#endif /* SRC_DISCRETIZATION_HDG_JACOBIANINTERIORTRACE_HDGADVECTIVE_H_ */
