// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALCELL_HDG_H
#define RESIDUALCELL_HDG_H

// Cell integral residual functions

#include <map>

#include "tools/Tuple.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/CellToTraceAssociativity.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Discretization/HDG/ResidualInteriorTrace_HDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral
//

template<class IntegrandCell, class IntegrandTrace, class XFieldCellToTrace,
         class XFieldClass, class QFieldType, class AFieldType, class QIFieldType, template<class> class Vector>
class ResidualCell_HDG_impl :
    public GroupIntegralCellType< ResidualCell_HDG_impl<IntegrandCell, IntegrandTrace, XFieldCellToTrace,
                                                        XFieldClass, QFieldType, AFieldType, QIFieldType, Vector> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<Real> VectorArrayQ;

  static_assert( std::is_same< PhysDim, typename IntegrandTrace::PhysDim >::value, "PhysDim should be the same.");

  // Save off the boundary trace integrand and the residual vectors
  ResidualCell_HDG_impl( const IntegrandCell& fcnCell, const IntegrandTrace& fcnTrace,
                         const XFieldCellToTrace& xfldCellToTrace,
                         const XFieldClass& xfld, const QFieldType& qfld, const AFieldType& afld, const QIFieldType& qIfld,
                         const int quadOrderITrace[], const int nITraceGroup,
                         Vector<ArrayQ>& rsdPDEGlobal, Vector<ArrayQ>& rsdINTGlobal ) :
                           fcnCell_(fcnCell), fcnTrace_(fcnTrace),
                           xfldCellToTrace_(xfldCellToTrace),
                           xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
                           quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup),
                           rsdPDEGlobal_(rsdPDEGlobal), rsdINTGlobal_(rsdINTGlobal),
                           comm_rank_(0) {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );

    SANS_ASSERT( rsdINTGlobal_.m() == qIfld_.nDOFpossessed() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                            ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename AFieldCellGroupType::template ElementType<> ElementAFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
          AFieldCellGroupType& afldCell = const_cast<AFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementAFieldClass afldElem( afldCell.basis() );

    // number of integrals evaluated
    const int qDOF = qfldElem.nDOF();
    const int nIntegrandPDE = qDOF;

    // element integral
    typedef ArrayQ PDEIntegrandType;

    GalerkinWeightedIntegral<TopoDim, Topology, PDEIntegrandType> integralPDE(quadratureorder, nIntegrandPDE);

    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == afldCell.nElem() );

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( qDOF, -1 );

    // residual array
    DLA::VectorD<PDEIntegrandType> rsdPDEElem( nIntegrandPDE );

    // loop over elements within group and solve for the auxiliary variables first

    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      afldCell.getElement( afldElem, elem );

      std::map<int,std::vector<int>> cellITraceGroups;

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, elem, trace);

        if (traceinfo.type == TraceInfo::Interior)
        {
          //add this traceGroup and traceElem to the set of interior traces attached to this cell,
          //but only if the qIfld actually has this traceGroup
          if (qIfld_.getLocalInteriorTraceGroups()[traceinfo.group] >= 0)
            cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
      }

      //-----------------------------------------------------------------------------------------

      for (int n = 0; n < nIntegrandPDE; n++) rsdPDEElem[n] = 0;

      // cell integration for canonical element
      integralPDE( fcnCell_.integrand_PDE(xfldElem, qfldElem, afldElem), get<-1>(xfldElem), rsdPDEElem.data(), nIntegrandPDE );

      //Collect the contributions to the PDE residual from the traces
      IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(ResidualInteriorTrace_HDG(fcnTrace_, cellITraceGroups,
                                                                                            cellGroupGlobal, elem,
                                                                                            rsdPDEElem, rsdINTGlobal_),
                                                                  xfld_, (qfld_, afld_), qIfld_,
                                                                  quadOrderITrace_, nITraceGroup_ );

      // only possessed cell elements have PDE residuals on this processor
      if ( qfldElem.rank() != comm_rank_ ) continue;

      // scatter-add element integral to global
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), qDOF );

      for (int n = 0; n < qDOF; n++)
      {
        int nGlobal = mapDOFGlobal[n];
        rsdPDEGlobal_[nGlobal] += rsdPDEElem[n];
      }
    } //elem loop

  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandTrace& fcnTrace_;
  const XFieldCellToTrace& xfldCellToTrace_;
  const XFieldClass& xfld_;
  const QFieldType& qfld_;
  const AFieldType& afld_;
  const QIFieldType& qIfld_;

  const int *quadOrderITrace_;
  const int nITraceGroup_;

  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdINTGlobal_;

  mutable int comm_rank_;
};

// Factory function

template<class IntegrandCell, class IntegrandTrace, class XFieldCellToTrace,
         class XFieldType, class QFieldType, class AFieldType, class QIFieldType,
         template<class> class Vector, class ArrayQ>
ResidualCell_HDG_impl<IntegrandCell, IntegrandTrace, XFieldCellToTrace,
                      XFieldType, QFieldType, AFieldType, QIFieldType, Vector>
ResidualCell_HDG( const IntegrandCellType<IntegrandCell>& fcnCell,
                  const IntegrandInteriorTraceType<IntegrandTrace>& fcnITrace,
                  const XFieldCellToTrace& xfldCellToTrace,
                  const XFieldType& xfld, const QFieldType& qfld, const AFieldType& afld, const QIFieldType& qIfld,
                  const int quadOrderITrace[], const int nITraceGroup,
                  Vector<ArrayQ>& rsdPDEGlobal, Vector<ArrayQ>& rsdINTGlobal )
{
  static_assert( std::is_same< ArrayQ, typename IntegrandCell::template ArrayQ<Real> >::value, "These should be the same.");
  return { fcnCell.cast(), fcnITrace.cast(),
           xfldCellToTrace, xfld, qfld, afld, qIfld,
           quadOrderITrace, nITraceGroup,
           rsdPDEGlobal, rsdINTGlobal };
}

}

#endif  // RESIDUALCELL_HDG_H
