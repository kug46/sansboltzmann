// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_HDGADVECTIVE_PARAM_H_
#define SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_HDGADVECTIVE_PARAM_H_

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "Field/Field.h"
#include "Field/Tuple/SurrealizedElementTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral without Lagrange multipliers
//

template<class Surreal, int iParam, class IntegrandBoundaryTrace, class MatrixQP_>
class JacobianBoundaryTrace_HDGAdvective_Param_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_HDGAdvective_Param_impl<Surreal, iParam, IntegrandBoundaryTrace, MatrixQP_> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;

  typedef typename DLA::VectorD<ArrayQSurreal> ResidualElemSurrealClass;

  JacobianBoundaryTrace_HDGAdvective_Param_impl(const IntegrandBoundaryTrace& fcn,
                                                MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p,
                                                MatrixScatterAdd<MatrixQP_>& mtxGlobalINT_p) :
    fcn_(fcn),
    mtxGlobalPDE_p_(mtxGlobalPDE_p),
    mtxGlobalINT_p_(mtxGlobalINT_p) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld)
  {
    SANS_ASSERT( mtxGlobalPDE_p_.m() == qfld.nDOF() );
//    SANS_ASSERT( mtxGlobalPDE_p_.n() == paramfld.nDOF() ); //TODO: have to be able to access paramfld

    SANS_ASSERT( mtxGlobalINT_p_.m() == qIfld.nDOF() );
//    SANS_ASSERT( mtxGlobalINT_p_.n() == paramfld.nDOF() ); //TODO: have to be able to access paramfld
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class TupleFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename TupleFieldType                 ::template FieldCellGroupType<TopologyL>& tuplefldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XField<PhysDim, TopoDim>       ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
             int quadratureorder )
  {
    typedef typename TupleFieldType                 ::template FieldCellGroupType<TopologyL> TupleFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename TupleType<iParam, TupleFieldCellGroupTypeL>::type ParamCellGroupTypeL;

    typedef typename SurrealizedElementTuple<TupleFieldCellGroupTypeL, Surreal, iParam>::type ElementTupleFieldClassL;

    typedef typename ParamCellGroupTypeL ::template ElementType<Surreal> ElementParamFieldClassL;

    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef typename ElementParamFieldClassL::T ArrayP;

    const int nArrayP = DLA::VectorSize<ArrayP>::M;
    const int nArrayQ = DLA::VectorSize<ArrayQ>::M;

    typedef typename DLA::MatrixS_or_T<nArrayQ, nArrayP, Real>::type MatrixQP;

    typedef DLA::MatrixD<MatrixQP> MatrixElemClass;

    // field groups
    const ParamCellGroupTypeL& paramfldCellL = get<iParam>(tuplefldCellL);

    // field elements
    ElementTupleFieldClassL  tuplefldElemL( tuplefldCellL.basis() );
    ElementParamFieldClassL& paramfldElemL = set<iParam>(tuplefldElemL);

    ElementQFieldClassL      qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();
    const int paramDOFL = paramfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapQDOFGlobalL(nDOFL,-1);
    std::vector<int> mapDOFGlobalTrace(nDOFI, -1);
    std::vector<int> mapParamDOFGlobalL(paramDOFL,-1);

    // element integrand/residual
    ResidualElemSurrealClass rsdPDEElemL(nDOFL);
    ResidualElemSurrealClass rsdINTElemTrace(nDOFI);

    // element jacobian matrix
    MatrixElemClass mtxPDEElemLL(nDOFL, paramDOFL);
    MatrixElemClass mtxINTElemL(nDOFI, paramDOFL);

    // trace element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, ResidualElemSurrealClass, ResidualElemSurrealClass>
      integral(quadratureorder);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // initialize element Jacobian to zero
      mtxPDEElemLL = 0.0;
      mtxINTElemL = 0.0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );

      // get global indexing/mapping
      qfldCellL.associativity( elemL ).getGlobalMapping( mapQDOFGlobalL.data(), nDOFL );
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalTrace.data(), nDOFI );

      paramfldCellL.associativity( elemL ).getGlobalMapping( mapParamDOFGlobalL.data(), paramDOFL );

      // copy global parameter/solution DOFs to element
      tuplefldCellL.getElement( tuplefldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement(  xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      auto integrandFunctor = fcn_.integrand( xfldElemTrace, canonicalTraceL,
                                              tuplefldElemL, qfldElemL, qIfldElemTrace );

      // number of simultaneous derivatives per functor call
#if 0
      const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();
#else
      const int nDeriv = Surreal::N;
#endif

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nArrayP*paramDOFL; nchunk += nDeriv)
      {
        // associate derivative slots with solution variables

        int slot = 0;
        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(paramfldElemL.DOF(j), n).deriv(slot - nchunk) = 1.0;
          }
        }

        // reset, evaluate elemental residuals
        rsdPDEElemL = 0.0;
        rsdINTElemTrace = 0.0;
        integral(integrandFunctor, xfldElemTrace, rsdPDEElemL, rsdINTElemTrace );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < paramDOFL; j++)
        {
          for (int n = 0; n < nArrayP; n++)
          {
            slot = nArrayP*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(paramfldElemL.DOF(j), n).deriv(slot - nchunk) = 0.0; // Reset the derivative

              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxPDEElemLL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nArrayQ; m++)
                  DLA::index(mtxINTElemL(i,j), m,n) = DLA::index(rsdINTElemTrace[i], m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobians to global jacobians
      mtxGlobalPDE_p_.scatterAdd( mtxPDEElemLL,
                                  mapQDOFGlobalL.data(), mapQDOFGlobalL.size(),
                                  mapParamDOFGlobalL.data(), mapParamDOFGlobalL.size() );
      mtxGlobalINT_p_.scatterAdd( mtxINTElemL,
                                  mapDOFGlobalTrace.data(), mapDOFGlobalTrace.size(),
                                  mapParamDOFGlobalL.data(), mapParamDOFGlobalL.size() );
    } // elem loop
  }

protected:

//----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, class ParamFieldCellGroupType, class MatrixQP,
            class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfldCellL,
      const ParamFieldCellGroupType& paramfldCellL,
      const int elemL,
      int mapQDOFGlobalL[], const int nDOFL,
      int mapParamDOFGlobalL[], const int paramDOFL,
      SANS::DLA::MatrixD<MatrixQP>& mtxPDEElemLL,
      SparseMatrixType& mtxGlobalPDE_p )
  {
    qfldCellL.associativity( elemL ).getGlobalMapping( mapQDOFGlobalL, nDOFL );

    paramfldCellL.associativity( elemL ).getGlobalMapping( mapParamDOFGlobalL, paramDOFL );

    mtxGlobalPDE_p.scatterAdd( mtxPDEElemLL, mapQDOFGlobalL, nDOFL, mapParamDOFGlobalL, paramDOFL );
  }

#if 0
//----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class TopoDim, class MatrixQP,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int elemL,
      int mapDOFGlobalL[], const int nDOFL,
      DLA::MatrixD<MatrixQP>& mtxPDEElemLL,
      SparseMatrixType< DLA::MatrixD<MatrixQP> >& mtxGlobalPDE_p )
  {
    mtxGlobalPDE_p.scatterAdd( mtxPDEElemLL, elemL, elemL );
  }
#endif

protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalPDE_p_;
  MatrixScatterAdd<MatrixQP_>& mtxGlobalINT_p_;
};

// Factory function

template<class Surreal, int iParam, class IntegrandBoundaryTrace, class MatrixQP>
JacobianBoundaryTrace_HDGAdvective_Param_impl<Surreal, iParam, IntegrandBoundaryTrace, MatrixQP>
JacobianBoundaryTrace_HDGAdvective_Param( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                          MatrixScatterAdd<MatrixQP>& mtxGlobalPDE_p,
                                          MatrixScatterAdd<MatrixQP>& mtxGlobalINT_p)
{
  return { fcn.cast(), mtxGlobalPDE_p, mtxGlobalINT_p };
}


} // namespace SANS


#endif /* SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_HDGADVECTIVE_PARAM_H_ */
