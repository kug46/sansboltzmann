// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HDG_TAUTYPECATEGORY_H
#define HDG_TAUTYPECATEGORY_H

// basis function category
  enum HDGTauType
  {
    Nothing = 0,
    Global = 1,
    Local = 2,
  };


#endif // HDG_AUXVARCATEGORY_H
