// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDBOUNDARYTRACE_HDG_AUXILIARYVARIABLE_H
#define SETFIELDBOUNDARYTRACE_HDG_AUXILIARYVARIABLE_H

// HDG trace integral residual functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataVectorD_BoundaryCell.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateCellGroups.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary trace group integral without lagrange multipliers
//

template<class IntegrandBTrace>
class SetFieldBoundaryTrace_HDG_AuxiliaryVariable_impl :
public GroupIntegralBoundaryTraceType< SetFieldBoundaryTrace_HDG_AuxiliaryVariable_impl<IntegrandBTrace> >
{
public:
  typedef typename IntegrandBTrace::PhysDim PhysDim;
  typedef typename IntegrandBTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef SurrealS<PDE::N> Surreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;

  // Save off the boundary trace integrand and the residual vectors
  SetFieldBoundaryTrace_HDG_AuxiliaryVariable_impl( const IntegrandBTrace& fcnBTrace,
                                                    FieldDataVectorD_BoundaryCell<VectorArrayQ>& rsdAUX_bcell,
                                                    FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& jacAUX_a_bcell ) :
                                                      fcnBTrace_(fcnBTrace),
                                                      rsdAUX_bcell_(rsdAUX_bcell), jacAUX_a_bcell_(jacAUX_a_bcell) {}

  std::size_t nBoundaryGroups() const { return fcnBTrace_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnBTrace_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the matrices
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const {}

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                             template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldSurrealClassL;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldSurrealClassL afldElemSurrealL( afldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // number of integrals evaluated per element
    const int nDOFL = qfldElemL.nDOF();
    const int nIntegrandL = nDOFL;

    // variables/equations per DOF
    const int nEqn = IntegrandBTrace::N;

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // trace element integral
    typedef VectorArrayQSurreal IntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, IntegrandType> integralL(quadratureorder, nIntegrandL);

    // element integrand/residuals
    DLA::VectorD<IntegrandType> rsdElemAUXL( nIntegrandL );

    DLA::MatrixD<TensorMatrixQ> mtxElemAUXL_aL( nDOFL, nDOFL );

    // loop over trace elements
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemSurrealL, elemL );

      // loop over derivative chunks for derivatives wrt aL
      for (int nchunk = 0; nchunk < nEqn*D*nDOFL; nchunk += nDeriv)
      {
        // associate derivative slots with solution variables
        int slot, slotOffset = 0;

        // wrt aL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemSurrealL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }
        slotOffset += nEqn*D*nDOFL;

        //------------------------------------------------------------------------------------------------

        for (int n = 0; n < nIntegrandL; n++) rsdElemAUXL[n] = 0;

        // AUX trace integration for canonical element
        integralL( fcnBTrace_.integrand_AUX(xfldElemTrace, canonicalTraceL, xfldElemL,
                                            qfldElemL, afldElemSurrealL, qIfldElemTrace),
                   xfldElemTrace, rsdElemAUXL.data(), nIntegrandL);

        //------------------------------------------------------------------------------------------------

        // accumulate derivatives into element jacobians
        slotOffset = 0;

        // wrt aL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d1 = 0; d1 < D; d1++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d1) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(afldElemSurrealL.DOF(j)[d1],n).deriv(slot - nchunk) = 0; //unset derivative

                //AUXL_aL
                for (int i = 0; i < nDOFL; i++)
                  for (int d0 = 0; d0 < D; d0++)
                    for (int m = 0; m < nEqn; m++)
                      DLA::index(mtxElemAUXL_aL(i,j)(d0, d1),m,n) = DLA::index(rsdElemAUXL[i][d0],m).deriv(slot - nchunk);

              }
            } //n loop
          } //d1 loop
        } //j loop
        slotOffset += nEqn*D*nDOFL;

      } // nchunk loop


      // get the matricies
      DLA::MatrixDView<VectorArrayQ>  rsdAUX_bcell = rsdAUX_bcell_.getCell(cellGroupGlobalL, elemL);
      DLA::MatrixDView<TensorMatrixQ> jacAUX_a_bcell = jacAUX_a_bcell_.getCell(cellGroupGlobalL, elemL);

      //Accumulate the auxiliary jacobian on boundary cells
      jacAUX_a_bcell += mtxElemAUXL_aL;

      //Save off auxiliary residual to be used in SetFieldCell_HDG_AuxiliaryVariable
      for (int i = 0; i < nDOFL; i++)
        for (int d = 0; d < D; d++)
          for (int m = 0; m < nEqn; m++)
            DLA::index(rsdAUX_bcell(i,0)[d],m) += DLA::index(rsdElemAUXL[i][d],m).value();

    } //elem loop
  }

protected:
  const IntegrandBTrace& fcnBTrace_;
  FieldDataVectorD_BoundaryCell<VectorArrayQ>& rsdAUX_bcell_;
  FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& jacAUX_a_bcell_;
};

// Factory function

template<class IntegrandBTrace, class VectorArrayQ, class TensorMatrixQ>
SetFieldBoundaryTrace_HDG_AuxiliaryVariable_impl<IntegrandBTrace>
SetFieldBoundaryTrace_HDG_AuxiliaryVariable( const IntegrandBoundaryTraceType<IntegrandBTrace>& fcnBTrace,
                                             FieldDataVectorD_BoundaryCell<VectorArrayQ>& rsdAUX_bcell,
                                             FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& jacAUX_a_bcell)
{
  return { fcnBTrace.cast(), rsdAUX_bcell, jacAUX_a_bcell };
}

}

#endif  // SETFIELDBOUNDARYTRACE_HDG_AUXILIARYVARIABLE_H
