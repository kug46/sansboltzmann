// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PROJECTSOLN_HDG_H
#define PROJECTSOLN_HDG_H

#include "Field/FieldTypes.h"

#include "Field/ProjectSoln/ProjectSolnCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectSolnGradientCell_Discontinuous.h"
#include "Field/ProjectSoln/ProjectSolnTrace_Discontinuous.h"
#include "Field/tools/for_each_CellGroup.h"
#include "Field/tools/for_each_InteriorFieldTraceGroup.h"

namespace SANS
{

template< class PhysDim, class TopoDim, class SOLN, class ArrayQ, class VectorArrayQ >
void ProjectSoln_HDG( Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                      Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& afld,
                      Field_DG_Trace<PhysDim, TopoDim, ArrayQ>& qIfld,
                      const SOLN& soln )
{
  for_each_CellGroup<TopoDim>::apply( ProjectSolnCell_Discontinuous(soln, qfld.getGlobalCellGroups()),
                                      (qfld.getXField(), qfld) );

  for_each_CellGroup<TopoDim>::apply( ProjectSolnGradientCell_Discontinuous(soln, afld.getGlobalCellGroups()),
                                      (afld.getXField(), afld) );

  for_each_InteriorFieldTraceGroup<TopoDim>::apply( ProjectSolnTrace_Discontinuous(soln, qIfld.getGlobalInteriorTraceGroups()),
                                               (qIfld.getXField(), qIfld) );
}

}

#endif // PROJECTSOLN_HDG_H
