// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AWINTEGRANDBCFUNCTOR_HDG_H
#define AWINTEGRANDBCFUNCTOR_HDG_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// HDG element boundary integrand: PDE

template <class PDE, class WeightFunction, class BC >
class AWIntegrandBC_HDG
{
public:
  // typedef typename BC::PDE PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  AWIntegrandBC_HDG(
    const PDE& pde, const WeightFunction& wfcn, const DiscretizationHDG<PDE>& disc, const BC& bc,
    const std::vector<int>& BoundaryGroups) :
    pde_(pde), wfcn_(wfcn), disc_(disc), bc_(bc),  BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementAFieldL;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef IntegrandHDG< ArrayQ<T>, VectorArrayQ<T> > IntegrandCellType;

    Functor( const PDE& pde, const WeightFunction& wfcn, const DiscretizationHDG<PDE>& disc, const BC& bc,
             const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementXFieldL& xfldElem,
             const ElementQFieldL& qfldElem,
             const ElementAFieldL& afldElem,
             const ElementQFieldTrace& lgfldTrace ) :
             pde_(pde), wfcn_(wfcn), disc_(disc), bc_(bc),
             xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
             xfldElem_(xfldElem), qfldElem_(qfldElem), afldElem_(afldElem),
             lgfldTrace_(lgfldTrace) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return afldElem_.nDOF(); }
    int nDOFTrace() const { return lgfldTrace_.nDOF(); }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType* integrandL, const int nrsdPDE,
                                                          ArrayQ<T>* integrandBC, const int nrsdBC ) const;
  protected:
    const PDE& pde_;
    const WeightFunction& wfcn_;
    const DiscretizationHDG<PDE>& disc_;
    const BC& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
    const ElementAFieldL& afldElem_;
    const ElementQFieldTrace& lgfldTrace_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysDim, TopoDimCell , TopologyL    >& xfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell , TopologyL    >& afldElem,
            const Element<ArrayQ<T>    , TopoDimTrace, TopologyTrace>& lgfldTrace) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL>(pde_, wfcn_, disc_, bc_,
                                                                           xfldElemTrace, canonicalTrace,
                                                                           xfldElem, qfldElem, afldElem,
                                                                           lgfldTrace);
  }

private:
  const PDE& pde_;
  const WeightFunction& wfcn_;
  const DiscretizationHDG<PDE>& disc_;
  const BC& bc_;
  const std::vector<int> BoundaryGroups_;
};


template <class PDE, class WeightFunction, class BC >
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL>
void
AWIntegrandBC_HDG<PDE, WeightFunction, BC >::Functor<T,TopoDimTrace,TopologyTrace,TopoDimCell,TopologyL>::operator()(
    const QuadPointTraceType& sRefTrace, IntegrandCellType* integrandL, const int nrsdPDE, ArrayQ<T>* integrandBC, const int nrsdBC ) const
{
  SANS_ASSERT( (nrsdPDE == 1) && (nrsdBC == 1) );

  VectorX X;                // physical coordinates
  VectorX N;                // unit normal (points out of domain)

  Real psi;                                  // weight
  DLA::VectorS<PhysDim::D, Real> gradpsi;       // weight gradient
  DLA::MatrixSymS<PhysDim::D, Real> hessianpsi; // weight hessian

  ArrayQ<T> q;                    // solution
  VectorArrayQ<T> a;                // auxiliary variable (gradient)
  ArrayQ<T> lg;                                 // Lagrange multiplier

  QuadPointCellType sRef;              // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // calculate weighting function
  wfcn_.secondGradient(X,psi,gradpsi,hessianpsi);

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, N );

  // basis value, gradient
  qfldElem_.eval( sRef, q );
  afldElem_.eval( sRef, a );

  // Lagrange multiplier basis
  lgfldTrace_.eval( sRefTrace, lg );

  // BC coefficients (convert to scalars)
  MatrixQ<Real> Amtx, Bmtx;
  bc_.coefficients( X, N, Amtx, Bmtx );
  Real A = Amtx;
  Real B = Bmtx;

  integrandL[0] = 0;

  // PDE residual: BC Lagrange multiplier term

  T jan;          // NOTE: use of Real rather than ArrayQ/MatrixQ to override

  DLA::VectorS< PhysDim::D, MatrixQ<T> > ja = 0;
  pde_.jacobianFluxAdvective( X, q, ja );
  jan = dot(N,ja);

  DLA::VectorS< PhysDim::D, T > Kn;

  DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<T> > K = 0;
  pde_.diffusionViscous( X, q, a, K );

  //kx = nx*kxx(0,0) + ny*kyx(0,0);
  //ky = nx*kxy(0,0) + ny*kyy(0,0);

  Kn = 0;
  for ( int i = 0; i < PhysDim::D; i++ )
    for ( int j = 0; j < PhysDim::D; j++ )
      Kn[i] += (K(i,j))*N[j];

  T tmp = (- B*q + A*dot(Kn,a))/(A*A + B*B) + lg;
  // lambda + alpha_bar q + beta_bar(K a.n)  in the boundary residual

  T tmpPDE = (A + B*jan)*tmp;
  DLA::VectorS< PhysDim::D, T > tmpAu;
  if (!pde_.hasFluxViscous() || disc_.auxVersion_ == Gradient )
    tmpAu = tmp*(-B);
  else
    tmpAu = (Kn)*tmp*(-B);

  integrandL[0].PDE += psi*tmpPDE;
  for (int d=0; d< PhysDim::D; d++)
    integrandL[0].Au[d] = -gradpsi[d]*tmpAu[d];

  // BC residual: BC weak form
  // NOTE: conversion to scalars to override matrix-vector rank checks

  // BC data (convert to scalar)
  ArrayQ<Real> bcdatavec;
  bc_.data( X, N, bcdatavec );
  Real bcdata = bcdatavec;

  T residualBC = A*q + B*dot(Kn,a) - bcdata;

  // calculate exact lagrange multiplier from psi
  // mu = - alpha*psi + beta*(K gradpsi .n) + h* ---- + beta because p = -gradpsi

  T mu;

  mu =  ((B - A*jan)*psi - A*dot(Kn,gradpsi))/(A*A + B*B);
  // mu = - alpha*psi - beta* K*gradpsi . n
  // currently missing the additional h* required for boundary functionals

  integrandBC[0] = mu*residualBC;

  //std::cout<< "A: " << A << std::endl;
  //std::cout<< "B: " << B << std::endl;
  //std::cout<< "InBCAW:Rs:" << residualBC <<std::endl;
  //std::cout<< "mu" << mu << std::endl;
  //std::cout << "p :" << psi(0) << std::endl;
  //std::cout << "gp:" << gradpsi(0) <<std::endl;
}

}

#endif  // AWINTEGRANDBCFUNCTOR_HDG_H
