// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_HDG_H
#define ALGEBRAICEQUATIONSET_HDG_H

#include <vector>
#include <map>
#include <string>

#include "tools/SANSnumerics.h"

#include "pde/BCParameters.h"

#include "Field/FieldTypes.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/XField_CellToTrace.h"

#include "Discretization/QuadratureOrder.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrateBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Dirichlet_mitLG_HDG.h"

#include "ErrorEstimate/ErrorEstimate_fwd.h"

namespace SANS
{

template<> struct DiscBCTag<BCCategory::Dirichlet_mitLG, HDG>     { typedef HDG type; };
template<> struct DiscBCTag<BCCategory::Dirichlet_sansLG, HDG>    { typedef HDG type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_mitLG, HDG>  { typedef HDG type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG, HDG> { typedef HDG type; };
template<> struct DiscBCTag<BCCategory::Flux_mitState, HDG>       { typedef HDG type; };
template<> struct DiscBCTag<BCCategory::None, HDG>                { typedef HDG type; };

//----------------------------------------------------------------------------//
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_HDG : public AlgebraicEquationSet_Debug<NDPDEClass, Traits>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;
  typedef typename NDPDEClass::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename NDPDEClass::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_HDG<NDPDEClass> IntegrandCellClass;
  typedef IntegrandInteriorTrace_HDG<NDPDEClass> IntegrandTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_HDG<NDPDEClass, BCNDConvert, BCVector, HDG> IntegrateBoundaryTrace_Dispatch;

  template< class... BCArgs >
  AlgebraicEquationSet_HDG(const XFieldType& xfld,
                           Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                           Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& afld,
                           Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                           Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                           const NDPDEClass& pde,
                           DiscretizationHDG<NDPDEClass> &disc,
                           const QuadratureOrder& quadratureOrder,
                           const std::vector<Real>& tol,
                           const std::vector<int>& CellGroups,
                           const std::vector<int>& InteriorTraceGroups,
                           PyDict& BCList,
                           const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                           BCArgs&... args ) :
    AlgebraicEquationSet_HDG( xfld, qfld, afld, qIfld, lgfld, pde, disc, quadratureOrder, tol,
                              CellGroups, CellGroups, InteriorTraceGroups, BCList, BCBoundaryGroups, args... ) {}

  virtual ~AlgebraicEquationSet_HDG() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx) override;
  virtual void jacobian(SystemNonZeroPatternView& nz) override;

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT) override;
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override;

  // Returns norm of the residual
  virtual std::vector<std::vector<Real>> residualNorm(const SystemVectorView& rsd) const override;

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override;

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Sets the primal adjoint field and computes the lifting operator adjoint
  void setAdjointField(const SystemVectorView& adj,
                       Field_DG_Cell<PhysDim, TopoDim, ArrayQ>&       wfld,
                       Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& bfld,
                       Field<PhysDim, TopoDim, ArrayQ>&               wIfld,
                       Field<PhysDim, TopoDim, ArrayQ>&               mufld );

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;
  bool isValidState();

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override;

  virtual void syncDOFs_MPI() override;

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iINT = 1;
  static const int iBC = 2;
  static const int iq = 0;
  static const int iqI = 1;
  static const int ilg = 2;

  const IntegrateBoundaryTrace_Dispatch& dispatchBC() const { return dispatchBC_; }

  const IntegrandCellClass& getCellIntegrand() const { return fcnCell_; };
  const IntegrandTraceClass& getInteriorTraceIntegrand() const { return fcnInt_; };
  const XField_CellToTrace<PhysDim, TopoDim>& getXField_CellToTrace() const { return xfldCellToTrace_; }
  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& getAuxiliaryJacobianInverse() const { return jacAUX_a_bcell_; }

protected:

  //Protected constructor for AlgebraicEquationSet_Local_HDG: cellgroups for the auxiliary calculation can be different
  template< class... BCArgs >
  AlgebraicEquationSet_HDG(const XFieldType& xfld,
                           Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                           Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& afld,
                           Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                           Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                           const NDPDEClass& pde,
                           DiscretizationHDG<NDPDEClass> &disc,
                           const QuadratureOrder& quadratureOrder,
                           const std::vector<Real>& tol,
                           const std::vector<int>& CellGroups,
                           const std::vector<int>& CellGroupsAUX,
                           const std::vector<int>& InteriorTraceGroups,
                           PyDict& BCList,
                           const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                           BCArgs&... args );

  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const QuadratureOrder& quadratureOrder);

  void computeAuxiliaryVariables(const bool update);

  DiscretizationHDG<NDPDEClass> disc_;
  IntegrandCellClass fcnCell_;
//  IntegrandCellClass fcnCellAUX_; //integrand cell to be used for auxiliary variable calc, may have different cellgroups
  IntegrandTraceClass fcnInt_;
  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  // HDG solution fields and mesh
  const XFieldType& xfld_;
  XField_CellToTrace<PhysDim, TopoDim> xfldCellToTrace_;

  // cell solution
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  // auxiliary variable
  Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& afld_;
  // interface solution
  Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  // Lagrange multiplier
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;

  const FieldDataInvMassMatrix_Cell mmfld_; // Inverse mass matrix field
  FieldDataMatrixD_BoundaryCell<TensorMatrixQ> jacAUX_a_bcell_; //auxiliary jacobian for boundary cells

  // The PDE
  const NDPDEClass& pde_;
  QuadratureOrder quadratureOrder_;
  QuadratureOrder quadratureOrderMin_;
  const std::vector<Real> tol_;

  const std::vector<int> CellGroupsAux_; //cellgroups to loop over when computing auxiliary variables
};

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_HDG_H
