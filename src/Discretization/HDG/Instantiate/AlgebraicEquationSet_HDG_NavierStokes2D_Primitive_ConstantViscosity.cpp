// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE
#include "Discretization/HDG/AlgebraicEquationSet_HDG_impl.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldArea.h"

namespace SANS
{

typedef QTypePrimitiveRhoPressure QType;
typedef ViscosityModel_Const ViscosityModelType;
typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass>::type BCVector;

typedef XField<PhysD2, TopoD2> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Dense , ParamFieldTupleType )

}
