// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE
#include "Discretization/HDG/AlgebraicEquationSet_HDG_impl.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "pde/Burgers/PDEBurgers1D.h"
#include "pde/Burgers/BCBurgers1D.h"
#include "pde/Burgers/BurgersConservative1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/XFieldLine.h"

namespace SANS
{
typedef PDEBurgers< PhysD1,
                    BurgersConservative1D,
                    ViscousFlux1D_Uniform,
                    Source1D_Uniform > PDEClass;
typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

typedef BCBurgers1DVector<BurgersConservative1D,ViscousFlux1D_Uniform,Source1D_Uniform> BCVector;

ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Sparse, ParamFieldTupleType )

}
