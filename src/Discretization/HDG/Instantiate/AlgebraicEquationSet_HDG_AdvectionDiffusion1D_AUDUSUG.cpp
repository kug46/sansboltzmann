// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE
#include "Discretization/HDG/AlgebraicEquationSet_HDG_impl.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion1D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "Field/XFieldLine.h"

namespace SANS
{
//----------------------------------------------------------------------------//
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_Uniform,
                              Source1D_UniformGrad > PDEClass;
typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_Uniform> BCVector;

ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Sparse, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Dense , ParamFieldTupleType )

//----------------------------------------------------------------------------//
typedef PDEAdvectionDiffusion<PhysD1,
                              AdvectiveFlux1D_Uniform,
                              ViscousFlux1D_None,
                              Source1D_UniformGrad > PDEClass_NoDiffusion;
typedef XField<PhysD1, TopoD1> ParamFieldTupleType_NoDiffusion;

typedef BCAdvectionDiffusion1DVector<AdvectiveFlux1D_Uniform, ViscousFlux1D_None> BCVector_NoDiffusion;

ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass_NoDiffusion, BCVector_NoDiffusion, TopoD1,
                                            AlgEqSetTraits_Sparse, ParamFieldTupleType_NoDiffusion )
ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass_NoDiffusion, BCVector_NoDiffusion, TopoD1,
                                            AlgEqSetTraits_Dense, ParamFieldTupleType_NoDiffusion )

}
