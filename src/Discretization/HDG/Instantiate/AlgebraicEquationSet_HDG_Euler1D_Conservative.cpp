// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE
#include "Discretization/HDG/AlgebraicEquationSet_HDG_impl.h"

#include "Field/FieldLine_DG_Cell.h"
#include "Field/FieldLine_DG_InteriorTrace.h"
#include "Field/FieldLine_DG_BoundaryTrace.h"

#include "pde/NS/TraitsEuler.h"
#include "pde/NS/Q1DConservative.h"
#include "pde/NS/PDEEuler1D.h"
#include "pde/NS/BCEuler1D.h"

#include "pde/NDConvert/PDENDConvertSpace1D.h"
#include "pde/NDConvert/BCNDConvertSpace1D.h"

#include "pde/NDConvert/PDENDConvertSpaceTime1D.h"
#include "pde/NDConvert/BCNDConvertSpaceTime1D.h"

#include "Field/XFieldLine.h"

namespace SANS
{

typedef QTypeConservative QType;
typedef TraitsModelEuler<QType, GasModel> TraitsModelEulerClass;
typedef PDEEuler1D<TraitsSizeEuler, TraitsModelEulerClass> PDEClass;
typedef BCEuler1DVector<TraitsSizeEuler, TraitsModelEulerClass> BCVector;
typedef XField<PhysD1, TopoD1> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD1, AlgEqSetTraits_Sparse, ParamFieldTupleType )

//typedef XField<PhysD2, TopoD2> ParamFieldTupleSpaceTime;

//ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACETIME( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, ParamFieldTupleSpaceTime )

}
