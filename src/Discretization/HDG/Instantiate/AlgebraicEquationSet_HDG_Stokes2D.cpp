// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE
#define ALGEBRAICEQUATIONSET_HDG_NOAUX_INSTANTIATE
#include "Discretization/HDG/AlgebraicEquationSet_HDG_impl.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"

#include "pde/Stokes/TraitsStokes.h"
#include "pde/Stokes/PDEStokes2D.h"
#include "pde/Stokes/BCStokes2D.h"

#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/XFieldArea.h"

namespace SANS
{

typedef PDEStokes2D<TraitsSizeStokes> PDEClass;
typedef BCStokes2DVector<TraitsSizeStokes>::type BCVector;

typedef XField<PhysD2, TopoD2> ParamFieldTupleType;

ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Sparse, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD2, AlgEqSetTraits_Dense , ParamFieldTupleType )

}
