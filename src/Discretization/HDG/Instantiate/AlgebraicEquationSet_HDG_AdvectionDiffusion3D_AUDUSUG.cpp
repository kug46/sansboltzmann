// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE
#include "Discretization/HDG/AlgebraicEquationSet_HDG_impl.h"

#include "Field/FieldVolume_DG_Cell.h"
#include "Field/FieldVolume_DG_InteriorTrace.h"
#include "Field/FieldVolume_DG_BoundaryTrace.h"

#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion3D.h"
#include "pde/AdvectionDiffusion/BCAdvectionDiffusion3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Field/XFieldVolume.h"

namespace SANS
{
typedef PDEAdvectionDiffusion<PhysD3,
                              AdvectiveFlux3D_Uniform,
                              ViscousFlux3D_Uniform,
                              Source3D_UniformGrad > PDEClass;
typedef XField<PhysD3, TopoD3> ParamFieldTupleType;

typedef BCAdvectionDiffusion3DVector<AdvectiveFlux3D_Uniform, ViscousFlux3D_Uniform> BCVector;

ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Sparse, ParamFieldTupleType )
ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDEClass, BCVector, TopoD3, AlgEqSetTraits_Dense , ParamFieldTupleType )

}
