// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDINTERIORTRACE_HDG_AUXILIARYADJOINT_H
#define SETFIELDINTERIORTRACE_HDG_AUXILIARYADJOINT_H

// HDG trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class Surreal, class IntegrandInteriorTrace, class InverseJacMatrix>
class SetFieldInteriorTrace_HDG_AuxiliaryAdjoint_impl :
    public GroupIntegralInteriorTraceType< SetFieldInteriorTrace_HDG_AuxiliaryAdjoint_impl<Surreal, IntegrandInteriorTrace, InverseJacMatrix> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,1,ArrayQ> RowArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  // Save off the boundary trace integrand and the residual vectors
  SetFieldInteriorTrace_HDG_AuxiliaryAdjoint_impl( const IntegrandInteriorTrace& fcn,
                                                   const std::map<int,std::vector<int>>& cellITraceGroups,
                                                   const int& cellgroup, const int& cellelem,
                                                   const InverseJacMatrix& invJacAUX_a_elem ) :
                                                     fcn_(fcn), cellITraceGroups_(cellITraceGroups),
                                                     cellgroup_(cellgroup), cellelem_(cellelem),
                                                     invJacAUX_a_elem_(invJacAUX_a_elem)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qfld
                                                   Field<PhysDim, TopoDim, VectorArrayQ>, //afld
                                                   Field<PhysDim, TopoDim, ArrayQ>,       //wfld
                                                   Field<PhysDim, TopoDim, VectorArrayQ>  //bfld
                             >::type & flds,
              const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qIfld
                                                   Field<PhysDim, TopoDim, ArrayQ>        //wIfld
                             >::type & fldsTrace ) const {}

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qfld
                                           Field<PhysDim, TopoDim, VectorArrayQ>, //afld
                                           Field<PhysDim, TopoDim, ArrayQ>,       //wfld
                                           Field<PhysDim, TopoDim, VectorArrayQ>  //bfld
                              >::type::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qfld
                                           Field<PhysDim, TopoDim, VectorArrayQ>, //afld
                                           Field<PhysDim, TopoDim, ArrayQ>,       //wfld
                                           Field<PhysDim, TopoDim, VectorArrayQ>  //bfld
                              >::type::template FieldCellGroupType<TopologyL>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qIfld
                                           Field<PhysDim, TopoDim, ArrayQ>        //wIfld
                              >::type::template FieldTraceGroupType<TopologyTrace>& fldsTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldSurrealClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementWFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<>        ElementBFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> AFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<Surreal> ElementQFieldSurrealClassR;
    typedef typename AFieldCellGroupTypeR::template ElementType<Surreal> ElementAFieldSurrealClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<>        ElementWFieldClassR;
    typedef typename AFieldCellGroupTypeR::template ElementType<>        ElementBFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldSurrealTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<>        ElementWFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<2>(fldsCellL);
          AFieldCellGroupTypeL& bfldCellL = const_cast<AFieldCellGroupTypeL&>(get<3>(fldsCellL));

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const AFieldCellGroupTypeR& afldCellR = get<1>(fldsCellR);
    const QFieldCellGroupTypeR& wfldCellR = get<2>(fldsCellR);
          AFieldCellGroupTypeR& bfldCellR = const_cast<AFieldCellGroupTypeL&>(get<3>(fldsCellR));

    const QFieldTraceGroupType& qIfldTrace = get<0>(fldsTrace);
    const QFieldTraceGroupType& wIfldTrace = get<1>(fldsTrace);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldSurrealClassL afldElemL( afldCellL.basis() );
    ElementWFieldClassL        wfldElemL( wfldCellL.basis() );
    ElementBFieldClassL        bfldElemL( bfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldSurrealClassR qfldElemR( qfldCellR.basis() );
    ElementAFieldSurrealClassR afldElemR( afldCellR.basis() );
    ElementWFieldClassR        wfldElemR( wfldCellR.basis() );
    ElementBFieldClassR        bfldElemR( bfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldSurrealTraceClass qIfldElemTrace( qIfldTrace.basis() );
    ElementWFieldTraceClass wIfldElemTrace( wIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    const int nIntegrandL = nDOFL;
    const int nIntegrandR = nDOFR;
    const int nIntegrandI = nDOFI;

    // variables/equations per DOF
    const int nEqn = IntegrandInteriorTrace::N;

    // element integral
    typedef ArrayQSurreal CellIntegrandType;
    typedef ArrayQSurreal TraceIntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, TraceIntegrandType>
      integralPDEL(quadratureorder, nDOFL, nDOFI);

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, TraceIntegrandType>
      integralPDER(quadratureorder, nDOFR, nDOFI);

    // element integrand/residual
    DLA::VectorD< CellIntegrandType > rsdElemPDEL( nIntegrandL );
    DLA::VectorD< CellIntegrandType > rsdElemPDER( nIntegrandR );
    DLA::VectorD< TraceIntegrandType > rsdElemINT( nIntegrandI );

    // element PDE jacobian matrices wrt a - transposed
    MatrixAUXElemClass mtxTElemPDEL_aL(nDOFL, nDOFL);
    MatrixAUXElemClass mtxTElemPDER_aR(nDOFR, nDOFR);
    MatrixAUXElemClass mtxTElemINT_aL(nDOFL, nDOFI);
    MatrixAUXElemClass mtxTElemINT_aR(nDOFR, nDOFI);

    // temporary storage for matrix-vector multiplication
    DLA::VectorD<VectorArrayQ> tmpL( nDOFL );
    DLA::VectorD<VectorArrayQ> tmpR( nDOFR );

    // Provide a vector of the primal adjoint
    DLA::VectorD<RowArrayQ> wL( nDOFL );
    DLA::VectorD<RowArrayQ> wR( nDOFR );
    DLA::VectorD<RowArrayQ> wI( nDOFI );

    // Provide a vector view of the auxiliary adjoint variable DOFs
    DLA::VectorDView<VectorArrayQ> bL( bfldElemL.vectorViewDOF() );
    DLA::VectorDView<VectorArrayQ> bR( bfldElemR.vectorViewDOF() );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );
      wIfldTrace.getElement( wIfldElemTrace, elem );

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        //The cell that called this function is to the left of the trace
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        afldCellL.getElement( afldElemL, elemL );

        wfldCellL.getElement( wfldElemL, elemL );

        // zero PDE/INT element Jacobians
        mtxTElemPDEL_aL = 0;
        mtxTElemINT_aL = 0;

        // loop over derivative chunks for derivatives wrt aL
        for (int nchunk = 0; nchunk < nEqn*(D*nDOFL); nchunk += nDeriv)
        {
          // associate derivative slots with solution variables
          int slot, slotOffset = 0;

          // wrt aL
          for (int j = 0; j < nDOFL; j++)
          {
            for (int d = 0; d < D; d++)
            {
              for (int n = 0; n < nEqn; n++)
              {
                slot = slotOffset + nEqn*(D*j + d) + n;
                if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                  DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
              }
            }
          }
          slotOffset += nEqn*D*nDOFL;

          //------------------------------------------------------------------------------------------------

          for (int n = 0; n < nIntegrandL; n++) rsdElemPDEL[n] = 0;
          for (int n = 0; n < nIntegrandI; n++) rsdElemINT[n] = 0;

          // PDE trace integration for canonical element
          integralPDEL( fcn_.integrand(xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                       xfldElemL, qfldElemL, afldElemL),
                        xfldElemTrace,
                        rsdElemPDEL.data(), nIntegrandL,
                        rsdElemINT.data(), nIntegrandI);

          //------------------------------------------------------------------------------------------------

          // accumulate derivatives into element jacobians
          slotOffset = 0;

          // wrt aL
          for (int j = 0; j < nDOFL; j++)
          {
            for (int d = 0; d < D; d++)
            {
              for (int n = 0; n < nEqn; n++)
              {
                slot = slotOffset + nEqn*(D*j + d) + n;
                if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                {
                  DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 0; //unset derivative

                  //PDEL_aL
                  for (int i = 0; i < nDOFL; i++)
                    for (int m = 0; m < nEqn; m++)
                    {
                      // Fill in as transposed (j,i)[d](n,m)
                      DLA::index(mtxTElemPDEL_aL(j,i)[d],n,m) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);
                    }

                  //INT_aL
                  for (int i = 0; i < nDOFI; i++)
                    for (int m = 0; m < nEqn; m++)
                    {
                      // Fill in as transposed (j,i)[d](n,m)
                      DLA::index(mtxTElemINT_aL(j,i)[d],n,m) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
                    }
                }
              } //n loop
            } //d loop
          } //j loop
          slotOffset += nEqn*D*nDOFL;

        } // nchunk loop


        // Copy over adjoints for matrix-vector multiplication
        for (int i = 0; i < nDOFL; i++) wL[i] = wfldElemL.DOF(i);
        for (int i = 0; i < nDOFI; i++) wI[i] = wIfldElemTrace.DOF(i);

        // Accumulate the second and third components of the auxiliary variable adjoint
        // b = [dAUX/da]^-T * ([dJ/da]^T - [dPDE/da]^T * w - [dINT/da]^T * wI)
        tmpL = mtxTElemPDEL_aL*wL + mtxTElemINT_aL*wI;

        bfldCellL.getElement( bfldElemL, elemL );
        bL -= Transpose(invJacAUX_a_elem_) * tmpL;
        bfldCellL.setElement( bfldElemL, elemL );

      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        //The cell that called this function is to the right of the trace
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        afldCellR.getElement( afldElemR, elemR );

        wfldCellR.getElement( wfldElemR, elemR );

        // zero PDE/INT element Jacobians
        mtxTElemPDER_aR = 0;
        mtxTElemINT_aR  = 0;

        // loop over derivative chunks for derivatives wrt aR
        for (int nchunk = 0; nchunk < nEqn*(D*nDOFR); nchunk += nDeriv)
        {
          // associate derivative slots with solution variables
          int slot, slotOffset = 0;

          // wrt aR
          for (int j = 0; j < nDOFR; j++)
          {
            for (int d = 0; d < D; d++)
            {
              for (int n = 0; n < nEqn; n++)
              {
                slot = slotOffset + nEqn*(D*j + d) + n;
                if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                  DLA::index(afldElemR.DOF(j)[d],n).deriv(slot - nchunk) = 1;
              }
            }
          }
          slotOffset += nEqn*D*nDOFR;

          //------------------------------------------------------------------------------------------------

          for (int n = 0; n < nIntegrandR; n++) rsdElemPDER[n] = 0;
          for (int n = 0; n < nIntegrandI; n++) rsdElemINT[n] = 0;

          // PDE trace integration for canonical element
          integralPDER( fcn_.integrand(xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                       xfldElemR, qfldElemR, afldElemR),
                        xfldElemTrace,
                        rsdElemPDER.data(), nIntegrandR,
                        rsdElemINT.data(), nIntegrandI);

          //------------------------------------------------------------------------------------------------

          // accumulate derivatives into element jacobians
          slotOffset = 0;

          // wrt aR
          for (int j = 0; j < nDOFR; j++)
          {
            for (int d = 0; d < D; d++)
            {
              for (int n = 0; n < nEqn; n++)
              {
                slot = slotOffset + nEqn*(D*j + d) + n;
                if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                {
                  DLA::index(afldElemR.DOF(j)[d],n).deriv(slot - nchunk) = 0; //unset derivative

                  //PDER_aR
                  for (int i = 0; i < nDOFR; i++)
                    for (int m = 0; m < nEqn; m++)
                    {
                      // Fill in as transposed (j,i)[d](n,m)
                      DLA::index(mtxTElemPDER_aR(j,i)[d],n,m) = DLA::index(rsdElemPDER[i],m).deriv(slot - nchunk);
                    }

                  //INT_aR
                  for (int i = 0; i < nDOFI; i++)
                    for (int m = 0; m < nEqn; m++)
                    {
                      // Fill in as transposed (j,i)[d](n,m)
                      DLA::index(mtxTElemINT_aR(j,i)[d],n,m) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
                    }
                }
              } //n loop
            } //d1 loop
          } //j loop
          slotOffset += nEqn*D*nDOFR;

        } // nchunk loop


        // Copy over adjoints for matrix-vector multiplication
        for (int i = 0; i < nDOFR; i++) wR[i] = wfldElemR.DOF(i);
        for (int i = 0; i < nDOFI; i++) wI[i] = wIfldElemTrace.DOF(i);

        // Accumulate the second and third components of the auxiliary variable adjoint
        // b = [dAUX/da]^-T * ([dJ/da]^T - [dPDE/da]^T * w - [dINT/da]^T * wI)
        tmpR = mtxTElemPDER_aR*wR + mtxTElemINT_aR*wI;

        bfldCellR.getElement( bfldElemR, elemR );
        bR -= Transpose(invJacAUX_a_elem_) * tmpR;
        bfldCellR.setElement( bfldElemR, elemR );

      }
      else
        SANS_DEVELOPER_EXCEPTION("SetFieldInteriorTrace_HDG_AuxiliaryAdjoint_impl::integrate - "
                                 "Invalid configuration!");

    } //elem loop
  }


protected:
  const IntegrandInteriorTrace& fcn_;
  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;
  const InverseJacMatrix& invJacAUX_a_elem_;

  std::vector<int> traceGroupIndices_;
};


// Factory function

template<class Surreal, class IntegrandInteriorTrace, class InverseJacMatrix>
SetFieldInteriorTrace_HDG_AuxiliaryAdjoint_impl<Surreal, IntegrandInteriorTrace, InverseJacMatrix>
SetFieldInteriorTrace_HDG_AuxiliaryAdjoint( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                                            const std::map<int,std::vector<int>>& cellITraceGroups,
                                            const int& cellgroup, const int& cellelem,
                                            const InverseJacMatrix& invJacAUX_a_elem)
{
  return { fcn.cast(), cellITraceGroups, cellgroup, cellelem, invJacAUX_a_elem };
}

}


#endif  // SETFIELDINTERIORTRACE_HDG_AUXILIARYADJOINT_H
