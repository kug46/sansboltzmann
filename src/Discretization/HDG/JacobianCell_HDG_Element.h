// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_HDG_ELEMENT_H
#define JACOBIANCELL_HDG_ELEMENT_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Discretization/JacobianElementMatrix.h"

namespace SANS
{


template<class PhysDim, class MatrixQ>
struct JacobianElemCell_HDG : JacElemMatrixType< JacobianElemCell_HDG<PhysDim,MatrixQ> >
{
  // PDE Jacobian wrt q
  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // PDE Jacobian wrt a
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;
  typedef DLA::MatrixD<RowMatrixQ> MatrixRElemClass;

  JacobianElemCell_HDG(const int nDOF)
   : nDOF(nDOF),
     PDE_q(nDOF, nDOF),
     PDE_a(nDOF, nDOF)
  {}

  const int nDOF;

  // element PDE jacobian matrices wrt q
  MatrixElemClass PDE_q;

  // element PDE jacobian matrices wrt a (gradient auxiliary variable)
  MatrixRElemClass PDE_a;

  inline Real operator=( const Real s )
  {
    PDE_q = s;

    PDE_a = s;

    return s;
  }

  // needed for Galerkin weighted integrand
  inline JacobianElemCell_HDG& operator+=(
      const JacElemMulScalar< JacobianElemCell_HDG >& mul )
  {
    PDE_q += mul.s*mul.mtx.PDE_q;

    PDE_a += mul.s*mul.mtx.PDE_a;

    return *this;
  }
};

}
#endif // JACOBIANCELL_HDG_ELEMENT_H
