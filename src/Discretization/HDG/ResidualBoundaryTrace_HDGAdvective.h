// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_RESIDUALBOUNDARYTRACE_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_RESIDUALBOUNDARYTRACE_HDGADVECTIVE_H_

// boundary-trace integral residual function
// similar to ResidualBoundaryTrace_mitLG_Galerkin //TODO possibly can merge the two in the future

#include "Field/Field.h"
#include "Field/GroupElementType.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"
#include "Field/Tuple/FieldTuple.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "tools/Tuple.h"

#include "DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class IntegrandBTrace, template<class> class VectorRsdGlobal>
class ResidualBoundaryTrace_HDGAdvective_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_HDGAdvective_impl<IntegrandBTrace, VectorRsdGlobal> >
{
public:
  typedef typename IntegrandBTrace::PhysDim PhysDim;
  typedef typename IntegrandBTrace::template ArrayQ<Real> ArrayQ;

  typedef DLA::VectorD<ArrayQ> ResidualElemClass;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_HDGAdvective_impl( const IntegrandBTrace& fcn,
                                           VectorRsdGlobal<ArrayQ>& rsdPDEGlobal,
                                           VectorRsdGlobal<ArrayQ>& rsdINTGlobal ) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), rsdINTGlobal_(rsdINTGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOF() );
    SANS_ASSERT( rsdINTGlobal_.m() == qIfld.nDOF() );
  }

  //----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;
    typedef          ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceClass;

    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL);
    std::vector<int> mapDOFGlobalTrace(nDOFI);

    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, ResidualElemClass, ResidualElemClass>
      integral(quadratureorder);

    // element integrand/residual
    ResidualElemClass rsdElemPDEL( nDOFL );
    ResidualElemClass rsdElemINT( nDOFI );

//    SANS_ASSERT_MSG( xfldTrace.getGroupRightType() == eCellGroup, "Ensure the current trace is not a HubTrace!" );

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // reset and evaluate residuals
      rsdElemPDEL = 0.0;
      rsdElemINT = 0.0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL, qIfldElemTrace),
                get<-1>(xfldElemTrace),
                rsdElemPDEL, rsdElemINT );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalTrace.data(), nDOFI );

      for (int n = 0; n < nDOFL; n++)
      {
        int nGlobal = mapDOFGlobalL[n];
        rsdPDEGlobal_[nGlobal] += rsdElemPDEL[n];
      }

      for (int n = 0; n < nDOFI; n++)
      {
        int nGlobal = mapDOFGlobalTrace[n];
        rsdINTGlobal_[nGlobal] += rsdElemINT[n];
      }

    }
  }

protected:
  const IntegrandBTrace& fcn_;
  VectorRsdGlobal<ArrayQ>& rsdPDEGlobal_;
  VectorRsdGlobal<ArrayQ>& rsdINTGlobal_;
};

// Factory function
template<class IntegrandBTrace, template<class> class VectorRsdGlobal, class ArrayQ>
ResidualBoundaryTrace_HDGAdvective_impl<IntegrandBTrace, VectorRsdGlobal>
ResidualBoundaryTrace_HDGAdvective( const IntegrandBoundaryTraceType<IntegrandBTrace>& fcn,
                                    VectorRsdGlobal<ArrayQ>& rsdPDEGlobal,
                                    VectorRsdGlobal<ArrayQ>& rsdINTGlobal)
{
//  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same< ArrayQ, typename IntegrandBTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return ResidualBoundaryTrace_HDGAdvective_impl<IntegrandBTrace, VectorRsdGlobal>(fcn.cast(), rsdPDEGlobal, rsdINTGlobal);
}

}

#endif /* SRC_DISCRETIZATION_HDG_RESIDUALBOUNDARYTRACE_HDGADVECTIVE_H_ */
