// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_FIELDTRACE_HDG_H
#define RESIDUALBOUNDARYTRACE_FIELDTRACE_HDG_H

// boundary-trace integral residual functions

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "tools/Tuple.h"

#include "DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary-trace integral
//

template<class IntegrandBoundaryTrace, template<class> class Vector>
class ResidualBoundaryTrace_FieldTrace_HDG_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_FieldTrace_HDG_impl<IntegrandBoundaryTrace, Vector> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_FieldTrace_HDG_impl( const IntegrandBoundaryTrace& fcn,
                                             Vector<ArrayQ>& rsdPDEGlobal,
                                             Vector<ArrayQ>& rsdINTGlobal,
                                             Vector<ArrayQ>& rsdBCGlobal) :
                                               fcn_(fcn),
                                               rsdPDEGlobal_(rsdPDEGlobal),
                                               rsdINTGlobal_(rsdINTGlobal),
                                               rsdBCGlobal_(rsdBCGlobal) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& fldsTrace ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&  qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld = get<0>(fldsTrace);
    const Field<PhysDim, TopoDim, ArrayQ>& lgfld = get<1>(fldsTrace);

    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOF() );
    SANS_ASSERT( rsdINTGlobal_.m() == qIfld.nDOF() );
    SANS_ASSERT( rsdBCGlobal_.m()  == lgfld.nDOF() );
  }

  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                       template FieldTraceGroupType<TopologyTrace>& fldsTrace,
      const int quadratureorder )
  {
    typedef typename XFieldType                         ::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<> ElementAFieldClassL;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    const QFieldTraceGroupType& qIfldTrace = get<0>(fldsTrace);
    const QFieldTraceGroupType& lgfldTrace = get<1>(fldsTrace);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldClassL afldElemL( afldCellL.basis() );

    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );
    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    const int nIntegrandL   = qfldElemL.nDOF();
    const int nIntegrandINT = qIfldElemTrace.nDOF();
    const int nIntegrandBC  = lgfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL );
    std::vector<int> mapDOFGlobalINT( nIntegrandINT );
    std::vector<int> mapDOFGlobalBC( nIntegrandBC );

    // trace element integral
    typedef ArrayQ CellIntegrandType;
    typedef ArrayQ TraceIntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, TraceIntegrandType, TraceIntegrandType>
      integral(quadratureorder, nIntegrandL, nIntegrandINT, nIntegrandBC);

    // element integrand/residuals
    std::vector<CellIntegrandType> rsdElemPDEL( nIntegrandL );
    std::vector<TraceIntegrandType> rsdElemINT( nIntegrandINT );
    std::vector<TraceIntegrandType> rsdElemBC( nIntegrandBC );

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );

      qIfldTrace.getElement( qIfldElemTrace, elem );
      lgfldTrace.getElement( lgfldElemTrace, elem );

      xfldTrace.getElement( xfldElemTrace, elem );

      for (int n = 0; n < nIntegrandL;   n++) rsdElemPDEL[n]   = 0;
      for (int n = 0; n < nIntegrandINT; n++) rsdElemINT[n] = 0;
      for (int n = 0; n < nIntegrandBC;  n++) rsdElemBC[n]  = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL, afldElemL,
                               qIfldElemTrace, lgfldElemTrace),
                xfldElemTrace,
                rsdElemPDEL.data(), nIntegrandL,
                rsdElemINT.data(), nIntegrandINT,
                rsdElemBC.data(), nIntegrandBC );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalINT.data(), nIntegrandINT );
      lgfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalBC.data(), nIntegrandBC );

      for (int n = 0; n < nIntegrandL; n++)
      {
        int nGlobal = mapDOFGlobalL[n];
        rsdPDEGlobal_[nGlobal] += rsdElemPDEL[n];
      }

      for (int n = 0; n < nIntegrandINT; n++)
      {
        int nGlobal = mapDOFGlobalINT[n];
        rsdINTGlobal_[nGlobal] += rsdElemINT[n];
      }

      for (int n = 0; n < nIntegrandBC; n++)
      {
        int nGlobal = mapDOFGlobalBC[n];
        rsdBCGlobal_[nGlobal] += rsdElemBC[n];
      }
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdINTGlobal_;
  Vector<ArrayQ>& rsdBCGlobal_;
};

// Factory function

template<class IntegrandBoundaryTrace, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_FieldTrace_HDG_impl<IntegrandBoundaryTrace, Vector>
ResidualBoundaryTrace_FieldTrace_HDG( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                      Vector<ArrayQ>& rsdPDEGlobal,
                                      Vector<ArrayQ>& rsdINTGlobal,
                                      Vector<ArrayQ>& rsdBCGlobal)
{
  static_assert( std::is_same< ArrayQ, typename IntegrandBoundaryTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return {fcn.cast(), rsdPDEGlobal, rsdINTGlobal, rsdBCGlobal};
}

}

#endif  // RESIDUALBOUNDARYTRACE_FIELDTRACE_HDG_H
