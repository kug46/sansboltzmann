// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATEBOUNDARYTRACE_DISPATCH_HDG_H
#define INTEGRATEBOUNDARYTRACE_DISPATCH_HDG_H

#include "Python/PyDict.h"

#include <map>

#include "Discretization/IntegrateBoundaryTrace_Dispatch.h"
#include "DiscretizationHDG.h"
#include "Integrand_HDG_fwd.h"

namespace SANS
{

template<class DiscTag, class BC>
struct is_HDG : public boost::mpl::bool_< std::is_same< typename DiscBCTag<typename BC::Category, DiscTag >::type, HDG>::value > {};

//===========================================================================//
template<class PDEND, template<class,class> class BCNDConvert, class BCVector, class DiscTag>
struct IntegrateBoundaryTrace_Dispatch_HDG : public IntegrateBoundaryTrace_Dispatch<PDEND, BCNDConvert, BCVector, DiscTag>
{
  typedef IntegrateBoundaryTrace_Dispatch<PDEND, BCNDConvert, BCVector, DiscTag> BaseType;

  typedef typename BaseType::BCNDVectorsmitFTcat BCNDVectorsmitFTcat;
  typedef typename BaseType::BCNDVectorsmitHTcat BCNDVectorsmitHTcat;
  typedef typename BaseType::BCNDVectorssansFTcat BCNDVectorssansFTcat;

  typedef typename BaseType::def_IntegrandBoundaryTraceOp def_IntegrandBoundaryTraceOp;
  typedef typename BaseType::IntegrandBoundaryTracemitFTVector IntegrandBoundaryTracemitFTVector;
  typedef typename BaseType::IntegrandBoundaryTracesansFTVector IntegrandBoundaryTracesansFTVector;

  // First separate the BCVectorsansFT into a pair of vectors depending on if they need Lifting operators or not
#if 0
  typedef typename boost::mpl::partition< typename BaseType::BCVectorsansFT,
                                          is_HDG<DiscTag, boost::mpl::_1 >,
                                          boost::mpl::back_inserter< boost::mpl::vector<> >,
                                          boost::mpl::back_inserter< boost::mpl::vector<> >
                                        >::type PairBCVectorsansFT;

  // Define the two extra BCVectors
  typedef typename PairBCVectorsansFT::first  BCVectorHDG;
  typedef typename PairBCVectorsansFT::second BCVectorsansFT;

  // Create two integrand boundary trace vectors
  typedef typename boost::mpl::transform< BCVectorHDG, def_IntegrandBoundaryTraceOp >::type IntegrandBoundaryTraceHDGVector;
  typedef typename boost::mpl::transform< BCVectorsansFT, def_IntegrandBoundaryTraceOp >::type IntegrandBoundaryTracesansFTVector;
#endif

//---------------------------------------------------------------------------//
  // A function for generating a std::vector of IntegrandBoundaryTrace instances
  template<class... Args>
  IntegrateBoundaryTrace_Dispatch_HDG( const PDEND& pde,
                                       PyDict& BCList,
                                       const std::map< std::string, std::shared_ptr<BCBase> >& BCs,
                                       const std::map< std::string, std::vector<int> >& BoundaryGroups,
                                       const DiscretizationHDG<PDEND>& disc,
                                       const Args&&... args)
  {
    // Extract the keys from the dictionary
    std::vector<std::string> keys = BCList.stringKeys();
    std::vector<int> BoundaryGroups_key;

    for (std::size_t i = 0; i < keys.size(); i++)
    {
      // Use a try...catch because then we can report the problem
      try
      {
        BoundaryGroups_key = BoundaryGroups.at(keys[i]);
      }
      catch (const std::out_of_range&)
      {
        //Create a more useful error message
        this->BCKeyError(keys[i], BoundaryGroups);
      }

      // Create the next boundary trace type
      std::shared_ptr<IntegrandBoundaryTraceBase> integrand =
          detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorsmitFTcat >::type,
                                                     typename boost::mpl::end< BCNDVectorsmitFTcat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key, disc, std::forward<Args>(args)... );

      if ( integrand == NULL )
        integrand =
            detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorsmitHTcat >::type,
                                                       typename boost::mpl::end< BCNDVectorsmitHTcat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key, disc, std::forward<Args>(args)... );

      if ( integrand == NULL )
        integrand =
            detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorssansFTcat >::type,
                                                       typename boost::mpl::end< BCNDVectorssansFTcat >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key, disc, std::forward<Args>(args)... );
#if 0
      if ( integrand == NULL )
        integrand =
            detail::IntegrandBoundaryTraceConstructor< typename boost::mpl::begin< BCNDVectorHDG >::type,
                                                       typename boost::mpl::end< BCNDVectorHDG >::type, PDEND, DiscTag >::
            create( pde, BCs.at(keys[i]), BoundaryGroups_key, disc, args... );
#endif
      if ( integrand == NULL )
        SANS_DEVELOPER_EXCEPTION("Should not be possible to get here.");

      integrandBoundaryTraces_.push_back(integrand);
    }

    // Just in case the vector is longer than needed
    integrandBoundaryTraces_.shrink_to_fit();
  }

#if 0
//---------------------------------------------------------------------------//
  template<class DispatchmitFTType, class DispatchHDGType, class DispatchsansFTType>
  void dispatch( DispatchmitFTType&& dispatchmitFT,
                 DispatchHDGType&& dispatchHDG,
                 DispatchsansFTType&& dispatchsansFT)
  {
    // Loop over all integrand boundary traces and dispatch the call
    // with the IntegrandBoundaryTraceBase cast to the derived IntegrandBoundaryTrace
    for (std::size_t n = 0; n < integrandBoundaryTraces_.size(); n++ )
    {
      // Hub trace is handled separately
      if ( integrandBoundaryTraces_[n]->needsHubTrace() ) continue;

      if ( integrandBoundaryTraces_[n]->needsFieldTrace() )
        call_with_derived<IntegrandBoundaryTracemitFTVector, DispatchmitFTType&>( dispatchmitFT, *integrandBoundaryTraces_[n] );
      else
      {
        if ( integrandBoundaryTraces_[n]->discTagID() == typeid(HDG) )
          call_with_derived<IntegrandBoundaryTraceHDGVector, DispatchHDGType&>( dispatchHDG, *integrandBoundaryTraces_[n] );
        else
          call_with_derived<IntegrandBoundaryTracesansFTVector, DispatchsansFTType&>( dispatchsansFT, *integrandBoundaryTraces_[n] );
      }
    }
  }
#endif

//---------------------------------------------------------------------------//
  template<class DispatchHDGType>
  void dispatch_HDG( DispatchHDGType&& dispatchHDG) const
  {
    // Loop over all integrand boundary traces and dispatch the call
    // with the IntegrandBoundaryTraceBase cast to the derived IntegrandBoundaryTrace
    // This function only dispatches to HDG type integrands
    for (std::size_t n = 0; n < integrandBoundaryTraces_.size(); n++ )
    {
      if ( integrandBoundaryTraces_[n]->discTagID() == typeid(HDG) )
        call_with_derived<IntegrandBoundaryTracesansFTVector, DispatchHDGType&>( dispatchHDG, *integrandBoundaryTraces_[n] );
    }
  }

  // Expose the generic dispatch with just mitFT and sansFT options
  using BaseType::dispatch;

protected:
  using BaseType::integrandBoundaryTraces_;
};

}

#endif //INTEGRATEBOUNDARYTRACE_DISPATCH_HDG_H
