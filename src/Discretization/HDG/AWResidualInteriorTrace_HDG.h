// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ANALYTICWEIGHTEDRESIDUALINTERIORTRACE_HDG_H
#define ANALYTICWEIGHTEDRESIDUALINTERIORTRACE_HDG_H

// HDG interior-trace integral residual functions

#include <memory>     // std::unique_ptr

#include "Discretization/HDG/AWIntegrandFunctor_HDG.h"

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Discretization/HDG/DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
//  functions dispatched based on left (L) and right (R) topologies
//
//  process:
//  (a) loop over groups; dispatch to L (ResidualInteriorTrace_LeftTopology)
//  (b) dispatch to R (ResidualInteriorTrace_RightTopology)
//  (c) call base class routine with specific L and R (ResidualInteriorTrace_Group w/ Base& params)
//  (d) cast to specific L and R and call L/R specific topology routine (ResidualInteriorTrace_Group_Integral)
//  (e) loop over traces and integrate

template <class TopologyTrace, class TopologyL, class TopologyR,
          class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ,
          class PDE, class WeightFunction>
void
AWResidualInteriorTrace_HDG_Group_Integral(
    const AWIntegrandTrace_HDG<PDE, WeightFunction>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTraceGroup,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTraceGroup,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellGroupL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellGroupL,
    const typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL>& afldCellGroupL,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR>& xfldCellGroupR,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellGroupR,
    const typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR>& afldCellGroupR,
    int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
    SLA::SparseVector<ArrayQ>& rsdAuGlobal,
    SLA::SparseVector<ArrayQ>& rsdIntGlobal,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& efldCellGroupL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& efldCellGroupR,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& eIfldTraceGroup)
{
  // Left types
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
  typedef typename AFieldCellGroupTypeL::template ElementType<> ElementAFieldClassL;

  //Right types
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
  typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> AFieldCellGroupTypeR;

  typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
  typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
  typedef typename AFieldCellGroupTypeR::template ElementType<> ElementAFieldClassR;

  //Trace types
  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

  typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellGroupL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellGroupL.basis() );
  ElementAFieldClassL afldElemL( afldCellGroupL.basis() );

  ElementXFieldClassR xfldElemR( xfldCellGroupR.basis() );
  ElementQFieldClassR qfldElemR( qfldCellGroupR.basis() );
  ElementAFieldClassR afldElemR( afldCellGroupR.basis() );

  ElementXFieldTraceClass xfldElemTrace( xfldTraceGroup.basis() );
  ElementQFieldTraceClass qIfldElemTrace( qIfldTraceGroup.basis() );

  const int nIntegrandL = 1;
  const int nIntegrandR = 1;
  const int nIntegrandTrace = 1;

  // trace element integral
  typedef IntegrandHDG<ArrayQ,VectorArrayQ> CellIntegrandType;
  GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, CellIntegrandType, ArrayQ>
    integral(quadratureorder, nIntegrandL, nIntegrandR, nIntegrandTrace);

  // element-to-global DOF mapping
  std::unique_ptr<int[]> mapDOFGlobalL( new int[1] );
  std::unique_ptr<int[]> mapDOFGlobalR( new int[1] );
  std::unique_ptr<int[]> mapDOFGlobalTrace( new int[1] );

  // element integrand/residuals
  std::unique_ptr< CellIntegrandType[] > rsdElemL( new CellIntegrandType[nIntegrandL] );
  std::unique_ptr< CellIntegrandType[] > rsdElemR( new CellIntegrandType[nIntegrandR] );
  std::unique_ptr<ArrayQ[]> rsdElemTrace( new ArrayQ[nIntegrandTrace] );

  // loop over elements within group
  int nelem = xfldTraceGroup.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int elemL = xfldTraceGroup.getElementLeft( elem );
    const int elemR = xfldTraceGroup.getElementRight( elem );
    const CanonicalTraceToCell& canonicalTraceL = xfldTraceGroup.getCanonicalTraceLeft( elem );
    const CanonicalTraceToCell& canonicalTraceR = xfldTraceGroup.getCanonicalTraceRight( elem );

    // copy global grid/solution DOFs to element
    xfldCellGroupL.getElement( xfldElemL, elemL );
    qfldCellGroupL.getElement( qfldElemL, elemL );
    afldCellGroupL.getElement( afldElemL, elemL );

    xfldCellGroupR.getElement( xfldElemR, elemR );
    qfldCellGroupR.getElement( qfldElemR, elemR );
    afldCellGroupR.getElement( afldElemR, elemR );

    xfldTraceGroup.getElement( xfldElemTrace, elem );
    qIfldTraceGroup.getElement( qIfldElemTrace, elem );

    rsdElemL[0] = 0;
    rsdElemR[0] = 0;
    rsdElemTrace[0] = 0;

    integral(
        fcn.integrand(xfldElemTrace, qIfldElemTrace,
                      canonicalTraceL, canonicalTraceR,
                      xfldElemL, qfldElemL, afldElemL,
                      xfldElemR, qfldElemR, afldElemR),
        xfldElemTrace,
        rsdElemL.get(), nIntegrandL,
        rsdElemR.get(), nIntegrandR,
        rsdElemTrace.get(), nIntegrandTrace);

    // scatter-add element residuals to global
    efldCellGroupL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.get(), 1 );
    efldCellGroupR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.get(), 1 );
    eIfldTraceGroup.associativity( elem ).getGlobalMapping( mapDOFGlobalTrace.get(), 1 );

    int nGlobal;
    nGlobal = mapDOFGlobalL[0];
    rsdPDEGlobal[nGlobal] += rsdElemL[0].PDE;
    for (int d = 0; d < PhysDim::D; d++)
    {
      nGlobal = PhysDim::D*mapDOFGlobalL[0]+d;
      rsdAuGlobal[nGlobal] += rsdElemL[0].Au[d];
    }
    nGlobal = mapDOFGlobalR[0];
    rsdPDEGlobal[nGlobal] += rsdElemR[0].PDE;

    for (int d = 0; d < PhysDim::D; d++)
    {
      nGlobal = PhysDim::D*mapDOFGlobalR[0]+d;
      rsdAuGlobal[nGlobal] += rsdElemR[0].Au[d];
    }
    nGlobal = mapDOFGlobalTrace[0];
    rsdIntGlobal[nGlobal] += rsdElemTrace[0];
  }
}


template <class TopologyTrace, class TopologyL, class TopologyR,
          class PDE, class WeightFunction,
          class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
void
AWResidualInteriorTrace_HDG_Group(
    const AWIntegrandTrace_HDG<PDE, WeightFunction>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTraceGroup,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTraceGroup,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
    const int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
    SLA::SparseVector<ArrayQ>& rsdAuGlobal,
    SLA::SparseVector<ArrayQ>& rsdIntGlobal,
    const Field<PhysDim, TopoDim, ArrayQ>& efld,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& eIfldTraceGroup )
{
  const XField<PhysDim, TopoDim>& xfld = afld.getXField();

  int groupL = xfldTraceGroup.getGroupLeft();
  int groupR = xfldTraceGroup.getGroupRight();

  // Integrate over the trace group

  AWResidualInteriorTrace_HDG_Group_Integral<TopologyTrace, TopologyL, TopologyR, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
      fcn, xfldTraceGroup, qIfldTraceGroup,
      xfld.template getCellGroup<TopologyL>(groupL), qfld.template getCellGroup<TopologyL>(groupL), afld.template getCellGroup<TopologyL>(groupL),
      xfld.template getCellGroup<TopologyR>(groupR), qfld.template getCellGroup<TopologyR>(groupR), afld.template getCellGroup<TopologyR>(groupR),
      quadratureorder,
      rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal,
      efld.template getCellGroup<TopologyR>(groupL), efld.template getCellGroup<TopologyR>(groupR), eIfldTraceGroup);

}

template<class TopDim>
class AWResidualInteriorTrace_HDG;

template<>
class AWResidualInteriorTrace_HDG<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

protected:
  template <class TopologyTrace, class TopologyL,
            class PDE, class WeightFunction,
            class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  RightTopology(
      const AWIntegrandTrace_HDG<PDE,WeightFunction>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTraceGroup,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTraceGroup,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdIntGlobal,
      const Field<PhysDim, TopoDim, ArrayQ>& efld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& eIfldTraceGroup )
  {
    // determine topology for Right cell group
    int groupR = xfldTraceGroup.getGroupRight();

    // dispatch integration over elements of group
    if ( afld.getCellGroupBase(groupR).topoTypeID() == typeid(Line) )
    {
      AWResidualInteriorTrace_HDG_Group<TopologyTrace, TopologyL, Line>(
          fcn, xfldTraceGroup, qIfldTraceGroup, qfld, afld, quadratureorder,
          rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal,
          efld, eIfldTraceGroup );
    }
    else
    {
      char msg[] = "Error in ResidualInteriorTrace_HDG<TopoD1>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  template <class TopologyTrace, class PDE, class WeightFunction,
            class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  LeftTopology(
      const AWIntegrandTrace_HDG<PDE, WeightFunction>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTraceGroup,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTraceGroup,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdIntGlobal,
      const Field<PhysDim, TopoDim, ArrayQ>& efld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& eIfldTraceGroup )
  {

    // determine topology for Left cell group
    int groupL = xfldTraceGroup.getGroupLeft();
    if ( afld.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Line>(
          fcn, xfldTraceGroup, qIfldTraceGroup, qfld, afld, quadratureorder,
          rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal,
          efld, eIfldTraceGroup);
    }
    else
    {
      char msg[] = "Error in ResidualInteriorTrace_HDG<TopoD1>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class PDE,  class WeightFunction,
  class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  integrate(
      const AWIntegrandTrace_HDG<PDE,WeightFunction>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdIntGlobal)
  {
    SANS_ASSERT( &qfld.getXField() == &afld.getXField() );
    SANS_ASSERT( &qfld.getXField() == &qIfld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = afld.getXField();
    // Error Est Field - for associativity maps
    Field_DG_Cell<PhysDim, TopoDim, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);
    efld = 0;
    SANS_ASSERT(efld.nDOF() == rsdPDEGlobal.m() ); // checking the rsdFields are P0
    SANS_ASSERT(efld.nDOF()*PhysDim::D == rsdAuGlobal.m() );
    // interface solution: P1 (aka Q1)
    Field_DG_InteriorTrace<PhysDim, TopoDim, ArrayQ> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
    eIfld = 0;
    SANS_ASSERT(eIfld.nDOF() == rsdIntGlobal.m() ); // checking int field is p0

    SANS_ASSERT_MSG( ngroup == xfld.nInteriorTraceGroups(), "ngroup = %d, xfld.nInteriorTraceGroups() = %d", ngroup, xfld.nInteriorTraceGroups() );
    SANS_ASSERT_MSG( ngroup == qIfld.nInteriorTraceGroups(), "ngroup = %d, xfld.nInteriorTraceGroups() = %d", ngroup, qIfld.nInteriorTraceGroups() );

    // loop over line element groups
    for (int group = 0; group < ngroup; group++)
    {
      if ( xfld.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node>( fcn,
            xfld.template getInteriorTraceGroup<Node>(group),
            qIfld.template getInteriorTraceGroup<Node>(group),
            qfld, afld, quadratureorder[group], rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal,
            efld, eIfld.template getInteriorTraceGroup<Node>(group)  );
      }
      else
      {
        char msg[] = "Error in ResidualInteriorTrace_HDG<TopoD1>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};


template<>
class AWResidualInteriorTrace_HDG<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

protected:
  template <class TopologyTrace, class TopologyL,
            class PDE, class WeightFunction,
            class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  RightTopology(
      const AWIntegrandTrace_HDG<PDE,WeightFunction>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTraceGroup,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTraceGroup,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdIntGlobal,
      const Field<PhysDim, TopoDim, ArrayQ>& efld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& eIfldTraceGroup)
  {
    // determine topology for Right cell group
    int groupR = xfldTraceGroup.getGroupRight();

    // dispatch integration over elements of group
    if ( afld.getCellGroupBase(groupR).topoTypeID() == typeid(Triangle) )
    {
      AWResidualInteriorTrace_HDG_Group<TopologyTrace, TopologyL, Triangle>(
          fcn, xfldTraceGroup, qIfldTraceGroup, qfld, afld, quadratureorder,
          rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal,
          efld, eIfldTraceGroup);
    }
    else
    {
      char msg[] = "Error in ResidualInteriorTrace_HDG<TopoD2>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class TopologyTrace, class PDE, class WeightFunction,
            class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  LeftTopology(
      const AWIntegrandTrace_HDG<PDE,WeightFunction>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTraceGroup,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTraceGroup,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdIntGlobal,
      const Field<PhysDim, TopoDim, ArrayQ>& efld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& eIfldTraceGroup)
  {

    // determine topology for Left cell group
    int groupL = xfldTraceGroup.getGroupLeft();

    if ( afld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Triangle>(
          fcn, xfldTraceGroup, qIfldTraceGroup, qfld, afld, quadratureorder,
          rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal,
          efld, eIfldTraceGroup );
    }
    else
    {
      char msg[] = "Error in ResidualInteriorTrace_HDG<TopoD2>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class PDE, class WeightFunction,
  class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  integrate(
      const AWIntegrandTrace_HDG<PDE,WeightFunction>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdIntGlobal)
  {
    SANS_ASSERT( &qfld.getXField() == &afld.getXField() );
    SANS_ASSERT( &qfld.getXField() == &qIfld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = afld.getXField();
    // Error Est Field - for associativity maps
    Field_DG_Cell<PhysDim, TopoDim, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);
    efld = 0;
    SANS_ASSERT(efld.nDOF() == rsdPDEGlobal.m() ); // checking the rsdFields are P0
    SANS_ASSERT(efld.nDOF()*PhysDim::D == rsdAuGlobal.m() );
    // interface solution: P1 (aka Q1)
    Field_DG_InteriorTrace<PhysDim, TopoDim, ArrayQ> eIfld(xfld, 0, BasisFunctionCategory_Legendre);
    eIfld = 0;
    SANS_ASSERT(eIfld.nDOF() == rsdIntGlobal.m() ); // checking int field is p0

    SANS_ASSERT_MSG( ngroup == xfld.nInteriorTraceGroups(), "ngroup = %d, xfld.nInteriorTraceGroups() = %d", ngroup, xfld.nInteriorTraceGroups() );
    SANS_ASSERT_MSG( ngroup == qIfld.nInteriorTraceGroups(), "ngroup = %d, xfld.nInteriorTraceGroups() = %d", ngroup, qIfld.nInteriorTraceGroups() );

    // loop over line element groups
    for (int group = 0; group < ngroup; group++)
    {
      if ( xfld.getInteriorTraceGroupBase(group).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line>( fcn,
            xfld.template getInteriorTraceGroup<Line>(group),
            qIfld.template getInteriorTraceGroup<Line>(group),
            qfld, afld, quadratureorder[group], rsdPDEGlobal, rsdAuGlobal, rsdIntGlobal,
            efld, eIfld.template getInteriorTraceGroup<Line>(group) );
      }
      else
      {
        char msg[] = "Error in ResidualInteriorTrace_HDG<TopoD2>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};


}

#endif  // ANALYTICWEIGHTEDRESIDUALINTERIORTRACE_HDG_H
