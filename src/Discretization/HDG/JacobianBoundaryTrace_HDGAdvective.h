// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_HDGADVECTIVE_H_

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin_Element.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary trace group integral without lagrange multipliers
//

template<class IntegrandBTrace,
         class XFieldClass, class QAFieldBundleType, class TopoDim>
class JacobianBoundaryTrace_HDGAdvective_impl :
    public GroupIntegralBoundaryTraceType<
             JacobianBoundaryTrace_HDGAdvective_impl<IntegrandBTrace, XFieldClass, QAFieldBundleType, TopoDim> >
{
public:
  typedef typename IntegrandBTrace::PhysDim PhysDim;
  typedef typename IntegrandBTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;

  typedef SurrealS<PDE::N> SurrealType;
  typedef typename PDE::template ArrayQ<SurrealType> ArrayQSurreal;

  typedef DLA::VectorD<ArrayQSurreal> ResidualElemClass;

  // hijacking existing machinery (for Galerkin) instead of inventing new ones for HDGAdvective
  typedef JacobianElemInteriorTrace_Galerkin<MatrixQ> JacobianElemType;
  typedef JacobianElemInteriorTraceSize JacobianElemSizeType;

  // Save off the boundary trace integrand and the residual vectors
  JacobianBoundaryTrace_HDGAdvective_impl(
    const IntegrandBTrace& fcnBTrace,
    const XFieldClass& xfld,
    const QAFieldBundleType& qafldBundle,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
    MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
    MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI ) :
      fcnBTrace_(fcnBTrace), xfld_(xfld), qafldBundle_(qafldBundle), qIfld_(qIfld),
      mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
      mtxGlobalINT_q_(mtxGlobalINT_q), mtxGlobalINT_qI_(mtxGlobalINT_qI) {}

  std::size_t nBoundaryGroups() const { return fcnBTrace_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnBTrace_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the matrices
  template<class TopoDim_>
  void check( const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim_, ArrayQ>& qIfld ) const
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalPDE_qI_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_qI_.n() == qIfld.nDOF() );

    SANS_ASSERT( mtxGlobalINT_q_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalINT_q_.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalINT_qI_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxGlobalINT_qI_.n() == qIfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim_, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    static_assert(std::is_same<TopoDim, TopoDim_>::value, "Mismatching topology dimension!");

    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<SurrealType> ElementQFieldSurrealClassL;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<>        ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<SurrealType> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    // variables/equations per DOF
    const int nEqn = IntegrandBTrace::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL, -1);
    std::vector<int> mapDOFGlobalI(nDOFI, -1);

    // element integral
    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, ResidualElemClass, ResidualElemClass>
      integral(quadratureorder);

    // element integrand/residual
    ResidualElemClass rsdElemPDEL( nDOFL );
    ResidualElemClass rsdElemINT( nDOFI );

    // element jacobian matrices
    // jacobian of PDE associated with left element
    JacobianElemType mtxElemL(JacobianElemSizeType(nDOFL, nDOFL, nDOFI));
    // jacobian of interface equation
    JacobianElemType mtxElemI(JacobianElemSizeType(nDOFI, nDOFL, nDOFI));

    // number of simultaneous derivatives per functor call
    const int nDeriv = SurrealType::N;

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; ++elem)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(nDOFL+nDOFI); nchunk += nDeriv)
      {
        int slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0.0;

            const int slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1.0;
          }
        }

        slotOffset = nEqn*nDOFL;
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qIfldElemTrace.DOF(j), n).deriv(k) = 0.0;

            const int slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j), n).deriv(slot - nchunk) = 1.0;
          }
        }

        // reset and evaluate residuals
        rsdElemPDEL = 0.0;
        rsdElemINT = 0.0;

        integral( fcnBTrace_.integrand(xfldElemTrace, canonicalTraceL,
                                       xfldElemL, qfldElemL, qIfldElemTrace),
                  get<-1>(xfldElemTrace),
                  rsdElemPDEL, rsdElemINT);

        // jacobians w.r.t. qL
        slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            const int slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int k = 0; k < nDOFL; ++k)
                for (int m = 0; m < nEqn; ++m)
                  DLA::index(mtxElemL.PDE_qL(k,j), m,n) = DLA::index(rsdElemPDEL[k], m).deriv(slot - nchunk);

              for (int k = 0; k < nDOFI; ++k)
                for (int m = 0; m < nEqn; ++m)
                  DLA::index(mtxElemI.PDE_qL(k,j), m,n) = DLA::index(rsdElemINT[k], m).deriv(slot - nchunk);
            }
          }
        }

        // jacobians w.r.t. qI
        slotOffset = nEqn*nDOFL;
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            const int slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int k = 0; k < nDOFL; ++k)
                for (int m = 0; m < nEqn; ++m)
                  DLA::index(mtxElemL.PDE_qR(k,j), m,n) = DLA::index(rsdElemPDEL[k], m).deriv(slot - nchunk);

              for (int k = 0; k < nDOFI; ++k)
                for (int m = 0; m < nEqn; ++m)
                  DLA::index(mtxElemI.PDE_qR(k,j), m,n) = DLA::index(rsdElemINT[k], m).deriv(slot - nchunk);
            }
          }
        }
      } // nchunk loop

      // scatter-add element jacobians to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalI.data(), nDOFI );

      mtxGlobalPDE_q_.scatterAdd( mtxElemL.PDE_qL,
                                  mapDOFGlobalL.data(), mapDOFGlobalL.size(),
                                  mapDOFGlobalL.data(), mapDOFGlobalL.size() );

      mtxGlobalPDE_qI_.scatterAdd( mtxElemL.PDE_qR,
                                   mapDOFGlobalL.data(), mapDOFGlobalL.size(),
                                   mapDOFGlobalI.data(), mapDOFGlobalI.size() );

      mtxGlobalINT_q_.scatterAdd( mtxElemI.PDE_qL,
                                  mapDOFGlobalI.data(), mapDOFGlobalI.size(),
                                  mapDOFGlobalL.data(), mapDOFGlobalL.size() );

      mtxGlobalINT_qI_.scatterAdd( mtxElemI.PDE_qR,
                                   mapDOFGlobalI.data(), mapDOFGlobalI.size(),
                                   mapDOFGlobalI.data(), mapDOFGlobalI.size() );
    } // elem loop
  }

protected:
  const IntegrandBTrace& fcnBTrace_;

  const XFieldClass& xfld_;
  const QAFieldBundleType& qafldBundle_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;

  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;
};

// Factory function

template<class IntegrandBTrace, class XFieldType, class QAFieldBundleType,
         class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
JacobianBoundaryTrace_HDGAdvective_impl<IntegrandBTrace, XFieldType, QAFieldBundleType, TopoDim>
JacobianBoundaryTrace_HDGAdvective(
  const IntegrandBoundaryTraceType<IntegrandBTrace>& fcnBTrace,
  const XFieldType& xfld,
  const QAFieldBundleType& qafldBundle,
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI)
{
  return { fcnBTrace.cast(), xfld, qafldBundle, qIfld,
           mtxGlobalPDE_q, mtxGlobalPDE_qI,
           mtxGlobalINT_q, mtxGlobalINT_qI };
}

}

#endif /* SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_HDGADVECTIVE_H_ */
