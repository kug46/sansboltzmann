// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_OUTPUTWEIGHTRSD_HDG_H
#define INTEGRANDBOUNDARYTRACE_OUTPUTWEIGHTRSD_HDG_H

// boundary output functional for HDG

#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/call_derived_functor.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"
#include "BasisFunction/BasisFunctionCategory.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/HDG/DiscretizationHDG.h"

#include "pde/OutputCategory.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// element boundary output integrand

template <class PDE_, class NDOutputVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::WeightedResidual>, HDG> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDOutputVector, OutputCategory::WeightedResidual>, HDG> >
{
public:
  typedef PDE_ PDE;
  typedef OutputCategory::WeightedResidual Category;
  typedef HDG DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  // A weighted residual output functional is always a scalar quantity
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = ArrayQ<T>;

  explicit IntegrandBoundaryTrace( const OutputBase& outputWeights, const std::vector<int>& BoundaryGroups )
    : outputWeights_(outputWeights), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class TopologyL,
                    class ElementParam, class BCIntegrandBoundaryTrace>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>      , TopoDimCell , TopologyL    > ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell , TopologyL    > ElementAFieldL;
    typedef Element<ArrayQ<T>      , TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef Element<ArrayQ<Real>      , TopoDimCell , TopologyL    > ElementWFieldL;
    typedef Element<VectorArrayQ<Real>, TopoDimCell , TopologyL    > ElementBFieldL;
    typedef Element<ArrayQ<Real>      , TopoDimTrace, TopologyTrace> ElementWFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;

    typedef typename BCIntegrandBoundaryTrace::template FieldWeighted<T,Real,TopoDimTrace,TopologyTrace,
                                                                             TopoDimCell,TopologyL,ElementParam> BCFieldWeight;

    Functor( const BCIntegrandBoundaryTrace& fcnBC,
             const OutputBase& outputWeights,
             const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementParam& paramfldElem,
             const ElementQFieldL& qfldElem,
             const ElementAFieldL& afldElem,
             const ElementQFieldTrace& qIfldTrace ) :
               fcnBC_(fcnBC),
               outputWeights_(outputWeights),
               callWeights_(outputWeights_),
               xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
               paramfldElem_(paramfldElem), xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
               qfldElem_(qfldElem), afldElem_(afldElem), qIfldTrace_(qIfldTrace),
               wfldElem_( 0, BasisFunctionCategory_Legendre ),
               bfldElem_( 0, BasisFunctionCategory_Legendre ),
               wIfldTrace_( 0, BasisFunctionCategory_Legendre ),
               fwBC_( fcnBC.integrand(xfldElemTrace_, canonicalTrace_, paramfldElem_,
                                      qfldElem_, afldElem_, wfldElem_, bfldElem_,
                                      qIfldTrace_, wIfldTrace_) )
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == bfldElem_.basis() );
    }

    Functor( Functor&& f ) :
               fcnBC_(f.fcnBC_),
               outputWeights_(f.outputWeights_),
               callWeights_(outputWeights_),
               xfldElemTrace_(f.xfldElemTrace_), canonicalTrace_(f.canonicalTrace_),
               paramfldElem_(f.paramfldElem_), xfldElem_(f.xfldElem_),
               qfldElem_(f.qfldElem_), afldElem_(f.afldElem_), qIfldTrace_(f.qIfldTrace_),
               wfldElem_( 0, BasisFunctionCategory_Legendre ),
               bfldElem_( 0, BasisFunctionCategory_Legendre ),
               wIfldTrace_( 0, BasisFunctionCategory_Legendre ),
               fwBC_( fcnBC_.integrand(xfldElemTrace_, canonicalTrace_, paramfldElem_,
                                       qfldElem_, afldElem_, wfldElem_, bfldElem_,
                                       qIfldTrace_, wIfldTrace_) )
    {
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, ArrayJ<T>& integrand ) const
    {
      typename BCFieldWeight::IntegrandCellType rsdCell = 0;
      typename BCFieldWeight::IntegrandTraceType rsdTrace = 0;

      VectorX X;
      xfldElemTrace_.eval(sRefTrace, X);

      // Compute the desired weight at this quadrature point
      callWeights_(X, wfldElem_.DOF(0));
      callWeights_(X, wIfldTrace_.DOF(0));

      fwBC_( sRefTrace, rsdCell, rsdTrace );

//      integrand = rsdCell.PDE;
      integrand = rsdCell.PDE + rsdTrace;
    }

  protected:
    const BCIntegrandBoundaryTrace& fcnBC_;
    const OutputBase& outputWeights_;
    const call_derived_functor<NDOutputVector,const OutputBase> callWeights_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementParam& paramfldElem_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
    const ElementAFieldL& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;

    mutable ElementWFieldL wfldElem_;
    mutable ElementBFieldL bfldElem_;
    mutable ElementWFieldTrace wIfldTrace_;

    const BCFieldWeight fwBC_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell, class ElementParam, class BCIntegrandBoundaryTrace>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyCell, ElementParam, BCIntegrandBoundaryTrace >
  integrand(const BCIntegrandBoundaryTrace& fcnBC,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // XField must be the last parameter
            const Element<ArrayQ<T>      , TopoDimCell, TopologyCell >& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyCell >& afldElem,
            const Element<ArrayQ<T>,     TopoDimTrace, TopologyTrace >& qIfldTrace) const
  {
    return {fcnBC, outputWeights_, xfldElemTrace, canonicalTrace, paramfldElem, qfldElem, afldElem, qIfldTrace};
  }

private:
  const OutputBase& outputWeights_;
  const std::vector<int> BoundaryGroups_;
};

}

#endif  // INTEGRANDBOUNDARYTRACE_OUTPUTWEIGHTRSD_HDG_H
