// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_HDG_H
#define INTEGRANDCELL_HDG_H

// cell integrand operators: HDG

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "JacobianCell_HDG_Element.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Cell integrand: HDG
//
// integrandPDE = - gradient(phi)*F + phi*S - phi*RHS
// integrandAux = phi*(ax - qx)
// integrandAuy = phi*(ay - qy)
//
// where
//   phi              basis function
//   q                solution
//   qx, qy           solution gradient
//   ax, ay           auxiliary variable
//   F(x,y,q,ax,ay)   flux (advective & viscous)
//   S(x,y,d,q)       solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//   K                diffusion matrix

template <class PDE_>
class IntegrandCell_HDG : public IntegrandCellType< IntegrandCell_HDG<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D,Real> VecReal;

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  static const int D = PhysDim::D;          // Physical dimensions
  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_HDG(
    const PDE& pde,
    const DiscretizationHDG<PDE>& disc, const std::vector<int>& CellGroups )
    : pde_(pde), disc_(disc), cellGroups_(CellGroups) {}

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class Ta, class TopoDim, class Topology, class ElementParam>
  class BasisWeighted_PDE
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<VectorArrayQ<Ta>, TopoDim, Topology> ElementAFieldType;

    typedef typename ElementXFieldType::RefCoordType RefCoordType;
    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;


    typedef JacobianElemCell_HDG<PhysDim,MatrixQ<Real>> JacobianElemCellType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef typename promote_Surreal<T, Ta>::type Ts;
    typedef ArrayQ<Ts> IntegrandType;

    BasisWeighted_PDE( const PDE& pde,
                       const DiscretizationHDG<PDE>& disc,
                       const ElementParam& paramfldElem,
                       const ElementQFieldType& qfldElem,
                       const ElementAFieldType& afldElem ) :
                         pde_(pde), disc_(disc),
                         xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                         qfldElem_(qfldElem), afldElem_(afldElem),
                         paramfldElem_(paramfldElem),
                         nDOF_( qfldElem_.nDOF() ),
                         phi_( new Real[nDOF_] ),
                         gradphi_( new VectorX[nDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    BasisWeighted_PDE( BasisWeighted_PDE&& bw ) :
                         pde_(bw.pde_), disc_(bw.disc_),
                         xfldElem_(bw.xfldElem_),
                         qfldElem_(bw.qfldElem_), afldElem_(bw.afldElem_),
                         paramfldElem_(bw.paramfldElem_),
                         nDOF_( bw.nDOF_ ),
                         phi_( bw.phi_ ),
                         gradphi_( bw.gradphi_ )
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted_PDE()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    template <class Ti>
    void operator()( const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn ) const;

    // cell element Jacobian integrand
    void operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem ) const;

  protected:
    template<class Tq, class Tg, class Ti>
    void Integrand( const ParamT& param,
                    const ArrayQ<Tq>& q,
                    const VectorArrayQ<Tg>& a,
                    ArrayQ<Ti> integrand[], int neqn ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementAFieldType& afldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    mutable Real* phi_;
    mutable VectorX* gradphi_;
  };

  template<class Tq, class Ta, class TopoDim, class Topology>
  class BasisWeighted_AUX
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef Element<ArrayQ<Tq>, TopoDim, Topology> ElementQFieldType;
    typedef Element<VectorArrayQ<Ta>, TopoDim, Topology> ElementAFieldType;

    typedef typename ElementXFieldType::RefCoordType RefCoordType;
    typedef typename ElementXFieldType::VectorX VectorX;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef typename promote_Surreal<Tq, Ta>::type Ts;
    typedef VectorArrayQ<Ts> IntegrandType;

    BasisWeighted_AUX( const PDE& pde,
                       const DiscretizationHDG<PDE>& disc,
                       const ElementXFieldType& xfldElem,
                       const ElementQFieldType& qfldElem,
                       const ElementAFieldType& afldElem ) :
                         pde_(pde), disc_(disc),
                         xfldElem_(xfldElem),
                         qfldElem_(qfldElem), afldElem_(afldElem),
                         nDOF_( qfldElem_.nDOF() ),
                         phi_( new Real[nDOF_] ),
                         gradphi_( new VectorX[nDOF_] )
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    BasisWeighted_AUX( BasisWeighted_AUX&& bw ) :
                         pde_(bw.pde_), disc_(bw.disc_),
                         xfldElem_(bw.xfldElem_),
                         qfldElem_(bw.qfldElem_), afldElem_(bw.afldElem_),
                         nDOF_( bw.nDOF_ ),
                         phi_( bw.phi_ ),
                         gradphi_( bw.gradphi_ )
    {
      bw.phi_ = nullptr; bw.gradphi_ = nullptr;
    }

    ~BasisWeighted_AUX()
    {
      delete [] phi_;
      delete [] gradphi_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    void operator()( const QuadPointType& sRef, IntegrandType integrand[], int neqn ) const;

  protected:
    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementAFieldType& afldElem_;

    const int nDOF_;
    mutable Real *phi_;
    mutable VectorX *gradphi_;
  };


  template<class T, class TopoDim, class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<VectorArrayQ<T>, TopoDim, Topology> ElementAFieldType;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef IntegrandHDG< Real, Real > IntegrandType;

    FieldWeighted(  const PDE& pde,
                    const DiscretizationHDG<PDE>& disc,
                    const ElementParam& paramfldElem,
                    const ElementQFieldType& qfldElem,
                    const ElementAFieldType& afldElem,
                    const ElementQFieldType& wfldElem,
                    const ElementAFieldType& bfldElem) :
                    pde_(pde), disc_(disc),
                    xfldElem_(get<-1>(paramfldElem)),  // XField must be the last parameter
                    qfldElem_(qfldElem), afldElem_(afldElem),
                    wfldElem_(wfldElem), bfldElem_(bfldElem),
                    paramfldElem_(paramfldElem),
                    nDOF_( qfldElem_.nDOF() ),
                    nwDOF_( wfldElem_.nDOF() )
    {
      phi_.resize(  nDOF_,0);
      wphi_.resize(nwDOF_,0);
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == bfldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return nDOF_; }

    // Cell integrand
    void operator()( const QuadPointType& sRef, IntegrandType& integrand ) const;

  protected:
    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementAFieldType& afldElem_;
    const ElementQFieldType& wfldElem_;
    const ElementAFieldType& bfldElem_;
    const ElementParam& paramfldElem_;

    const int nDOF_;
    const int nwDOF_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> wphi_;
  };

  template<class Tq, class Ta, class TopoDim, class Topology, class ElementParam>
  BasisWeighted_PDE<Tq, Ta, TopoDim, Topology, ElementParam>
  integrand_PDE(const ElementParam& paramfldElem,
                const Element<ArrayQ<Tq>, TopoDim, Topology>& qfldElem,
                const Element<VectorArrayQ<Ta>, TopoDim, Topology>& afldElem) const
  {
    return BasisWeighted_PDE<Tq,Ta,TopoDim,Topology,ElementParam>( pde_, disc_, paramfldElem, qfldElem, afldElem);
  }

  template<class Tq, class Ta, class TopoDim, class Topology>
  BasisWeighted_AUX<Tq, Ta, TopoDim, Topology>
  integrand_AUX(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                const Element<ArrayQ<Tq>, TopoDim, Topology>& qfldElem,
                const Element<VectorArrayQ<Ta>, TopoDim, Topology>& afldElem) const
  {
    return BasisWeighted_AUX<Tq,Ta,TopoDim,Topology>( pde_, disc_, xfldElem, qfldElem, afldElem);
  }

  template<class T, class TopoDim, class Topology, class ElementParam>
  FieldWeighted<T, TopoDim, Topology, ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<VectorArrayQ<T>, TopoDim, Topology>& afldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& wfldElem,
            const Element<VectorArrayQ<T>, TopoDim, Topology>& bfldElem) const
  {
    return FieldWeighted<T,TopoDim,Topology,ElementParam>( pde_, disc_,
                                                           paramfldElem,
                                                           qfldElem, afldElem,
                                                           wfldElem, bfldElem);
  }

protected:
  const PDE& pde_;
  const DiscretizationHDG<PDE>& disc_;
  const std::vector<int> cellGroups_;
};


template <class PDE>
template <class Tq, class Ta, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_HDG<PDE>::BasisWeighted_PDE<Tq,Ta,TopoDim,Topology,ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

template <class PDE>
template <class Tq, class Ta, class TopoDim, class Topology>
bool
IntegrandCell_HDG<PDE>::BasisWeighted_AUX<Tq,Ta,TopoDim,Topology>::needsEvaluation() const
{
  return ( pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource()) );
}

template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
bool
IntegrandCell_HDG<PDE>::FieldWeighted<T,TopoDim,Topology,ElementParam>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

//---------------------------------------------------------------------------//
// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,d,a)       solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE>
template <class Tq, class Ta, class TopoDim, class Topology, class ElementParam>
template <class Ti>
void
IntegrandCell_HDG<PDE>::BasisWeighted_PDE<Tq,Ta,TopoDim,Topology,ElementParam>::operator()(
    const QuadPointType& sRef, ArrayQ<Ti> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Tq> q;                // solution
  VectorArrayQ<Ta> a;          // auxiliary variable (gradient)

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value and gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  afldElem_.evalFromBasis( phi_, nDOF_, a );

  // compute the residual
  Integrand(param, q, a, integrand, neqn);
}

//---------------------------------------------------------------------------//
template <class PDE>
template <class Tq, class Tr, class TopoDim, class Topology, class ElementParam >
void
IntegrandCell_HDG<PDE>::
BasisWeighted_PDE<Tq,Tr,TopoDim,Topology,ElementParam>::
operator()( const Real dJ, const QuadPointType& sRef, JacobianElemCellType& mtxElem ) const
{
  typedef SurrealS<PDE::N> SurrealClass;

  SANS_ASSERT(mtxElem.nDOF == nDOF_);

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiuple of PDE::N");

  ParamT param;                // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<Real> q;             // solution
  VectorArrayQ<Real> a;       // gradient

  ArrayQ<SurrealClass> qSurreal = 0;           // solution
  VectorArrayQ<SurrealClass> aSurreal = 0;     // gradient

  MatrixQ<Real> PDE_q = 0;   // temporary Jacobian storage

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  afldElem_.evalFromBasis( phi_, nDOF_, a );

  qSurreal = q;
  aSurreal = a;

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandSurreal( nDOF_ );

  // loop over derivative chunks
  for (int nchunk = 0; nchunk < PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandSurreal = 0;

    Integrand(param, qSurreal, a, integrandSurreal.data(), integrandSurreal.size());

    // accumulate derivatives into element jacobian

    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurreal, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOF_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOF_; j++)
          mtxElem.PDE_q(i,j) += dJ*phi_[j]*PDE_q;
      }
    }
    slot += PDE::N;

  } // nchunk


  if (needsSolutionGradient)
  {
    MatrixQ<Real> PDE_a = 0;

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(aSurreal[d], n).deriv(slot + n - nchunk) = 1;

        slot += PDE::N;
      }

      integrandSurreal = 0;

      Integrand(param, q, aSurreal, integrandSurreal.data(), integrandSurreal.size());

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(aSurreal[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOF_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_a,m,n) = DLA::index(integrandSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOF_; j++)
              mtxElem.PDE_a(i,j)(0,d) += dJ*phi_[j]*PDE_a;

          }
        }
        slot += PDE::N;
      }
    } // nchunk
  }
}

//---------------------------------------------------------------------------//
template <class PDE>
template <class T, class Ta, class TopoDim, class Topology, class ElementParam>
template <class Tq, class Tg, class Ti>
void
IntegrandCell_HDG<PDE>::BasisWeighted_PDE<T,Ta,TopoDim,Topology,ElementParam>::
Integrand( const ParamT& param,
           const ArrayQ<Tq>& q,
           const VectorArrayQ<Tg>& a,
           ArrayQ<Ti> integrand[], int neqn ) const
{
  // PDE

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // Galerkin flux term: -gradient(phi) . F

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0;

    // advective flux
    pde_.fluxAdvective( param, q, F );

    if (pde_.hasFluxViscous())
      pde_.fluxViscous( param, q, a, F );

    for (int k = 0; k < neqn; k++)
      integrand[k] -= dot(gradphi_[k],F);
  }

  // Galerkin source term: +phi S
  // solution gradient augmented by lifting operator (asymptotically dual consistent)

  if (pde_.hasSource())
  {
    ArrayQ<Ti> source = 0;

    pde_.source( param, q, a, source );

    for (int k = 0; k < neqn; k++)
      integrand[k] += phi_[k]*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    for (int k = 0; k < neqn; k++)
      integrand[k] -= phi_[k]*forcing;
  }
}

//---------------------------------------------------------------------------//
template <class PDE>
template <class Tq, class Ta, class TopoDim, class Topology>
void
IntegrandCell_HDG<PDE>::BasisWeighted_AUX<Tq,Ta,TopoDim,Topology>::operator()(
    const QuadPointType& sRef, VectorArrayQ<Ts> integrand[], int neqn ) const
{
  SANS_ASSERT( neqn == nDOF_ );

  VectorX X;                  // physical coordinates

  ArrayQ<Tq> q;                // solution
  VectorArrayQ<Ta> a;          // auxiliary variable (gradient)

  // physical coordinates
  xfldElem_.coordinates( sRef, X );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_, nDOF_ );
  xfldElem_.evalBasisGradient( sRef, qfldElem_, gradphi_, nDOF_ );

  // solution value
  qfldElem_.evalFromBasis( phi_, nDOF_, q );
  afldElem_.evalFromBasis( phi_, nDOF_, a );

  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  // auxiliary variable definition

  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient)
    {
      // (phi . a) + div(phi)*q
      for (int k = 0; k < neqn; k++)
        integrand[k] = phi_[k]*a + gradphi_[k]*q;
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandCell_HDG::BasisWeighted_AUX::operator() - Unknown auxiliary variable!" );
  }
}

//---------------------------------------------------------------------------//
template <class PDE>
template <class T, class TopoDim, class Topology, class ElementParam>
void
IntegrandCell_HDG<PDE>::FieldWeighted<T,TopoDim,Topology,ElementParam>::operator()(
    const QuadPointType& sRef, IntegrandType& integrand ) const
{

  ParamT param;               // Elemental parameters (such as grid coordinates and distance functions)

  ArrayQ<T> q;                // solution
  VectorArrayQ<T> a;          // auxiliary variable (gradient)

  ArrayQ<T> w;                // weight
  VectorArrayQ<T> gradw;      // weight gradient
  VectorArrayQ<T> b;          // auxiliary weight
  DLA::VectorS<PhysDim::D, VectorArrayQ<T>> gradb; // auxiliary weight gradient

  // Elemental parameters
  paramfldElem_.eval( sRef, param );

  // SOLUTION
  // basis value
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

  // WEIGHT
  // basis value
  wfldElem_.evalBasis( sRef, wphi_.data(), wphi_.size() );
  wfldElem_.evalFromBasis( wphi_.data(), wphi_.size(), w );
  bfldElem_.evalFromBasis( wphi_.data(), wphi_.size(), b );

  // PDE
  integrand = 0;

  // Galerkin flux term: -gradient(phi) . F
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<T> F = 0;

    // gradient
    xfldElem_.evalGradient( sRef, wfldElem_, gradw );

    // advective flux
    if ( pde_.hasFluxAdvective() )
      pde_.fluxAdvective( param, q, F );

    if (pde_.hasFluxViscous())
      pde_.fluxViscous( param, q, a, F );

    integrand.PDE -= dot(gradw,F);
  }

  // Galerkin source term: +phi S

  if (pde_.hasSource())
  {
    ArrayQ<T> source=0;                         // PDE source S(X, D, U, UX), S(X)
    pde_.source( param, q, a, source );

    integrand.PDE += dot(w,source);
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( param, forcing );

    integrand.PDE -= dot(w,forcing);
  }

  // auxiliary variable definition

  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient)
    {
      // (phi . a) + div(phi)*q

      // gradient of b
      // TODO: Should be replaced with a divergence function in the element class
      xfldElem_.evalGradient( sRef, bfldElem_, gradb );

      integrand.Au = dot(b,a);
      for (int d = 0; d < D; d++)
        integrand.Au += dot(gradb[d][d], q);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandCell_HDG::FieldWeighted::operator() - Unknown auxiliary variable!" );
  }
}


} // namespace SANS

#endif  // INTEGRANDCELL_HDG_H
