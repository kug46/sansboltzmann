// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_NONE_HDG_H
#define INTEGRANDBOUNDARYTRACE_NONE_HDG_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/output_std_vector.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "pde/BCNone.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// HDG element boundary integrand: None

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::None>, HDG> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::None>, HDG> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::None Category;
  typedef HDG DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(
    const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const DiscretizationHDG<PDE>& disc) :
    pde_(pde), bc_(bc),  BoundaryGroups_(BoundaryGroups), disc_(disc) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  virtual ~IntegrandBoundaryTrace() {}

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementAFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandCellType;
    typedef ArrayQ<T> IntegrandTraceType;

    BasisWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem,
                   const ElementQFieldTrace& qIfldTrace ) :
                     pde_(pde), disc_(disc), bc_(bc),
                     xfldElemTrace_(xfldElemTrace),
                     canonicalTrace_(canonicalTrace),
                     xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                     qfldElem_(qfldElem), afldElem_(afldElem),
                     qIfldTrace_(qIfldTrace),
                     paramfldElem_( paramfldElem ),
                     useFluxViscous_((static_cast<const BCNone<PhysDim,PDE::N>*>(&bc_))->useFluxViscous()),
                     nDOFElem_(qfldElem_.nDOF()),
                     nDOFTrace_(qIfldTrace_.nDOF()),
                     phi_(nDOFElem_),
                     phiI_(nDOFTrace_)

    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOFElem_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCell[], const int neqnCell,
                                                          ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandCell, neqnCell,
                                                              integrandTrace, neqnTrace );
    }

    FRIEND_CALL_WITH_DERIVED

  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandCell[], const int neqnCell,
                                                                       ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;
    const bool useFluxViscous_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> phiI_;
  };

  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted_AUX
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<      ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Ta>, TopoDimCell, Topology> ElementAFieldCell;
    typedef Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Ta>::type Ts;

    typedef VectorArrayQ<Ts> IntegrandType;

    BasisWeighted_AUX( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                       const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                       const ElementParam& paramfldElem,
                       const ElementQFieldCell& qfldElem,
                       const ElementAFieldCell& afldElem,
                       const ElementQFieldTrace& qIfldTrace ) :
                         pde_(pde), disc_(disc), bc_(bc),
                         xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                         xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                         qfldElem_(qfldElem), afldElem_(afldElem),
                         qIfldTrace_(qIfldTrace),
                         paramfldElem_( paramfldElem ),
                         useFluxViscous_((static_cast<const BCNone<PhysDim,PDE::N>*>(&bc_))->useFluxViscous()),
                         nDOFElem_(qfldElem_.nDOF()),
                         nDOFTrace_(qIfldTrace_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      phi_.resize(nDOFElem_,0);
      phiT_.resize(nDOFTrace_,0);
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandL, neqnL );
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;
    const bool useFluxViscous_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> phiT_;
  };

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell, class Topology, class ElementParam >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<      ArrayQ<Tq>, TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Tq>, TopoDimCell, Topology> ElementAFieldCell;
    typedef Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef Element<      ArrayQ<Tw>, TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<VectorArrayQ<Tw>, TopoDimCell, Topology> ElementBFieldCell;
    typedef Element<ArrayQ<Tw>, TopoDimTrace, TopologyTrace> ElementWFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type Ti;

    typedef IntegrandHDG< Ti, Ti > IntegrandCellType;
    typedef Ti IntegrandTraceType;

    FieldWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem,
                   const ElementWFieldCell& wfldElem,
                   const ElementBFieldCell& bfldElem,
                   const ElementQFieldTrace& qIfldTrace,
                   const ElementWFieldTrace& wIfldTrace ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem), afldElem_(afldElem),
                   wfldElem_(wfldElem), bfldElem_(bfldElem),
                   qIfldTrace_(qIfldTrace), wIfldTrace_(wIfldTrace),
                   paramfldElem_( paramfldElem ),
                   useFluxViscous_((static_cast<const BCNone<PhysDim,PDE::N>*>(&bc_))->useFluxViscous()),
                   nDOFElem_(qfldElem_.nDOF()),
                   nDOFTrace_(qIfldTrace_.nDOF()),
                   phi_(nDOFElem_), wphi_(wfldElem_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == bfldElem_.basis() );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCell,
                                                          IntegrandTraceType& integrandTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandCell, integrandTrace);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandCellType& integrandCell,
                                                                       IntegrandTraceType& integrandTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementBFieldCell& bfldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementWFieldTrace& wIfldTrace_;
    const ElementParam& paramfldElem_;
    const bool useFluxViscous_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> wphi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>,       TopoDimCell , Topology>& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell , Topology>& afldElem,
            const Element<ArrayQ<T>,       TopoDimTrace, TopologyTrace>& qIfldTrace ) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                  xfldElemTrace, canonicalTrace,
                                                                  paramfldElem, qfldElem, afldElem, qIfldTrace);
  }

  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted_AUX<Tq, Ta, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand_AUX(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                const ElementParam& paramfldElem,
                const Element<ArrayQ<Tq>, TopoDimCell, Topology >& qfldElem,
                const Element<VectorArrayQ<Ta>, TopoDimCell, Topology >& afldElem,
                const Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace >& qIfldTrace) const
  {
    return BasisWeighted_AUX<Tq, Ta, TopoDimTrace, TopologyTrace,
                             TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                   xfldElemTrace, canonicalTrace,
                                                                   paramfldElem, qfldElem, afldElem, qIfldTrace );
  }

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<Tq>,       TopoDimCell, Topology >& qfldElem,
            const Element<VectorArrayQ<Tq>, TopoDimCell, Topology >& afldElem,
            const Element<ArrayQ<Tw>,       TopoDimCell, Topology >& wfldElem,
            const Element<VectorArrayQ<Tw>, TopoDimCell, Topology >& bfldElem,
            const Element<ArrayQ<Tq>,       TopoDimTrace, TopologyTrace>& qIfldTrace,
            const Element<ArrayQ<Tw>,       TopoDimTrace, TopologyTrace>& wIfldTrace ) const
  {
    return FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace,
                                 TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                       xfldElemTrace,
                                                                       canonicalTrace,
                                                                       paramfldElem,
                                                                       qfldElem, afldElem,
                                                                       wfldElem, bfldElem,
                                                                       qIfldTrace, wIfldTrace);
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const DiscretizationHDG<PDE>& disc_;
};


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam>
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::None>, HDG>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology, ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCell[], const int neqnCell,
                                                              ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const
{
  SANS_ASSERT( (neqnCell == nDOFElem_) && (neqnTrace == nDOFTrace_) );

  VectorX X;                // physical coordinates
  VectorX nL;               // unit normal (points out of domain)

  ArrayQ<T> qI;             // interface solution
  ArrayQ<T> q;              // solution
  VectorArrayQ<T> a;        // auxiliary variable (gradient)

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, nL );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );
  qIfldTrace_.evalBasis( sRefTrace, phiI_.data(), phiI_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

  qIfldTrace_.evalFromBasis( phiI_.data(), phiI_.size(), qI );

  DLA::VectorS<PhysDim::D, ArrayQ<Ti> > F = 0;     // PDE flux
  DLA::VectorS<PhysDim::D, ArrayQ<Ti> > FI = 0;

  // PDE residual: weak form boundary integral
  // advective flux
  if (pde_.hasFluxAdvective())
  {
    pde_.fluxAdvective( X, q, F );
    pde_.fluxAdvective( X, qI, FI );
  }

  // Generally no viscous flux (talk to Prof. Darmofal about it)
  if (pde_.hasFluxViscous() && useFluxViscous_ )
  {
    pde_.fluxViscous( X, q, a, F );
    pde_.fluxViscous( X, qI, a, FI );
  }

  ArrayQ<Ti> Fn = dot(nL,F);
  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] = phi_[k]*Fn;

  ArrayQ<Ti> FIn = dot(nL,FI);
  ArrayQ<Ti> Fsum = (Fn - FIn);

  for (int k = 0; k < neqnTrace; k++)
    integrandTrace[k] = -phiI_[k]*Fsum;

}


template <class PDE, class NDBCVector>
template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class Topology, class ElementParam >
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::None>, HDG>::
BasisWeighted_AUX<Tq,Ta,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL) const
{
  SANS_ASSERT( (neqnL == nDOFElem_) );

  VectorX X;                  // physical coordinates
  VectorX nL;                 // unit normal (points out of domain)

  ArrayQ<Tq> qL;               // solution
  ArrayQ<Tq> qI;               // interface solution

  QuadPointCellType sRef;     // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, nL );
  VectorX nB = -nL;

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), qL );

  // interface solution basis
  qIfldTrace_.evalBasis( sRefTrace, phiT_.data(), phiT_.size() );

  // interface solution
  qIfldTrace_.evalFromBasis( phiT_.data(), phiT_.size(), qI );

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient)
    {
      // - (phiL nL)*qI
      VectorArrayQ<Tq> qInL;

      for (int d = 0; d < PhysDim::D; d++)
        qInL[d] = qI*nL[d];

      for (int k = 0; k < neqnL; k++)
        integrandL[k] = -phi_[k]*qInL;
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandBoundaryTrace_None::BasisWeighted_AUX::operator() - Unknown auxiliary variable!" );
  }
}


template <class PDE, class NDBCVector>
template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class Topology, class ElementParam>
template<class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::None>, HDG>::
FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology, ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCell,
                                                              IntegrandTraceType& integrandTrace ) const
{
  VectorX X;                // physical coordinates
  VectorX N;                // unit normal (points out of domain)

  ArrayQ<Tq> qI;             // interface solution
  ArrayQ<Tq> q;              // solution
  VectorArrayQ<Tq> a;        // auxiliary variable (gradient)

  ArrayQ<Tw> w;              // weight
  VectorArrayQ<Tw> b;        // weight auxiliary variable (gradient)
  ArrayQ<Tw> wI;             // interface weight

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, N );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

  // interface solution
  qIfldTrace_.eval( sRefTrace, qI );

  // basis value, gradient
  wfldElem_.evalBasis( sRef, wphi_.data(), wphi_.size() );

  // weight value, gradient
  wfldElem_.evalFromBasis( wphi_.data(), wphi_.size(), w );
  bfldElem_.evalFromBasis( wphi_.data(), wphi_.size(), b );

  // interface weight
  wIfldTrace_.eval( sRefTrace,  wI );

  integrandCell = 0;
  integrandTrace = 0;


  DLA::VectorS<PhysDim::D, ArrayQ<Tq> > F = 0;     // PDE flux
  DLA::VectorS<PhysDim::D, ArrayQ<Tq> > FI = 0;

  // PDE residual: weak form boundary integral

  // advective flux
  if (pde_.hasFluxAdvective())
  {
    pde_.fluxAdvective( X, q, F );
    pde_.fluxAdvective( X, qI, FI );
  }

  // Generally no viscous flux (talk to Prof. Darmofal about it)
  if (pde_.hasFluxViscous() & useFluxViscous_ )
  {
    pde_.fluxViscous( X, q, a, F );
    pde_.fluxViscous( X, qI, a, FI );
  }

  ArrayQ<Tq> Fn = dot(N,F);
  integrandCell.PDE += dot(w,Fn);

  ArrayQ<Tq> FIn = dot(N,FI);
  ArrayQ<Tq> Fsum = (Fn - FIn);
  integrandTrace += -dot(wI, Fsum);

  // AUX residual
  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient)
    {
      // - (phiL nL)*qI
      VectorArrayQ<Tq> qInL;

      for (int d = 0; d < PhysDim::D; d++)
        qInL[d] = qI*N[d];

      integrandCell.Au = -dot(b, qInL);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandBoundaryTrace_None::FieldWeighted::operator() - Unknown auxiliary variable!" );
  }
}

}

#endif  // INTEGRANDBOUNDARYTRACE_NONE_HDG_H
