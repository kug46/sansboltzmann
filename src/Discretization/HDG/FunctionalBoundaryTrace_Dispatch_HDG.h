// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTINOALBOUNDARYTRACE_DISPATCH_HDG_H
#define FUNCTINOALBOUNDARYTRACE_DISPATCH_HDG_H

// boundary-trace integral residual functions


//#include "Discretization/HDG/FunctionalBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/FunctionalBoundaryTrace_HDG_sansLG.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/Integrand_Type.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace (e.g. with Lagrange multipliers)
//
//---------------------------------------------------------------------------//
template< class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
          class ArrayQ, class VectorArrayQ, class ArrayJ>
class FunctionalBoundaryTrace_FieldTrace_Dispatch_HDG_impl
{
public:
  FunctionalBoundaryTrace_FieldTrace_Dispatch_HDG_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      ArrayJ& functional ) :
      fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld), lgfld_(lgfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      functional_(functional)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Functionals with field trace not implemented yet.");
#if 0
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        FunctionalBoundaryTrace_FieldTrace_HDG(fcn, rsdPDEGlobal_, rsdBCGlobal_),
        xfld_, (qfld_, rfld_), lgfld_, quadratureorder_, ngroup_ );
#endif
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  ArrayJ& functional_;
};

// Factory function

template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class ArrayJ>
FunctionalBoundaryTrace_FieldTrace_Dispatch_HDG_impl<FunctionalIntegrand, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, ArrayJ>
FunctionalBoundaryTrace_FieldTrace_Dispatch_HDG( const FunctionalIntegrand& fcnOutput,
                                                 const XFieldType& xfld,
                                                 const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                 const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                                 const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                                 const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                 const int* quadratureorder, int ngroup,
                                                 ArrayJ& functional)
{
  return FunctionalBoundaryTrace_FieldTrace_Dispatch_HDG_impl<FunctionalIntegrand, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, ArrayJ>(
      xfld, qfld, afld, qIfld, lgfld, quadratureorder, ngroup, functional);
}

//---------------------------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class ArrayJ>
class FunctionalBoundaryTrace_Dispatch_HDG_impl
{
public:
  FunctionalBoundaryTrace_Dispatch_HDG_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
      const int* quadratureorder, int ngroup,
      ArrayJ& functional )
    : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      functional_(functional)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
      FunctionalBoundaryTrace_HDG_sansLG( fcnOutput_, fcn.cast(), functional_ ), xfld_, (qfld_, afld_), qIfld_, quadratureorder_, ngroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const int* quadratureorder_;
  const int ngroup_;
  ArrayJ& functional_;
};

// Factory function

template<class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class ArrayJ>
FunctionalBoundaryTrace_Dispatch_HDG_impl<FunctionalIntegrand, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, ArrayJ>
FunctionalBoundaryTrace_Dispatch_HDG( const FunctionalIntegrand& fcnOutput,
                                      const XFieldType& xfld,
                                      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                      const int* quadratureorder, int ngroup,
                                      ArrayJ& functional )
{
  return FunctionalBoundaryTrace_Dispatch_HDG_impl<FunctionalIntegrand, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ, ArrayJ>(
      fcnOutput, xfld, qfld, afld, qIfld, quadratureorder, ngroup, functional);
}


}

#endif //FUNCTINOALBOUNDARYTRACE_DISPATCH_HDG_H
