// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALCELL_HDG_HB_H
#define RESIDUALCELL_HDG_HB_H

// Cell integral residual functions

#include <memory> //unique_ptr

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/HDG/IntegrandFunctor_HDG_HB.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group residual
//
// topology specific group residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   IntegrandCellFunctor              integrand functor

template <class Topology, class PhysDim, class TopoDim,
class ArrayQ, class VectorArrayQ,
class PDE>
void
ResidualCell_HDG_HB_Group_Integral(
    const IntegrandCell_HDG_HB<PDE>& fcn, const int mytime,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfld,
    const std::vector< typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>* >& qfldgroups,
    const int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
{
  typedef typename XField<PhysDim, TopoDim            >::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  SANS_ASSERT( xfld.nElem() == qfldgroups[0]->nElem() );
  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldClass qfldElem( qfldgroups[0]->basis() );
  ElementQFieldClass qfldElemHere( qfldgroups[0]->basis() );

  // number of integrals evaluated
  const int nDOF = qfldElem.nDOF();
  const int ntimes = qfldgroups.size();
  const int nIntegrand = nDOF;
  //std::cout << "\nDOF: " << nDOF << "\n";

  // element integral
  GalerkinWeightedIntegral<TopoDim, Topology, IntegrandHDG<ArrayQ,VectorArrayQ> > integral(quadratureorder, nIntegrand);

  // just to make sure things are consistent
  SANS_ASSERT( xfld.nElem() == qfldgroups[mytime]->nElem() );
  // element-to-global DOF mapping
  std::unique_ptr<int[]> mapDOFGlobal( new int[nDOF] );

  for (int n = 0; n < nDOF; n++) mapDOFGlobal[n] = -1; // to remove clang analyzer warnings

  // residual array
  std::unique_ptr< IntegrandHDG<ArrayQ,VectorArrayQ>[] > rsdElem( new IntegrandHDG<ArrayQ,VectorArrayQ>[nIntegrand] );
  std::vector < Element<ArrayQ, TopoDim, Topology> > qfldElemVec;

  qfldElemVec.assign(ntimes, qfldElem);

  // loop over elements within group
  const int nelem = xfld.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    xfld.getElement( xfldElem, elem );

    // copy global grid/solution DOFs to element for each time
    for (int time = 0; time < ntimes; time ++ )
    {
      qfldgroups[time]->getElement( qfldElem, elem );
      qfldElemVec[time] = qfldElem;
    }

    for (int n = 0; n < nIntegrand; n++)
      rsdElem[n] = 0;

    // cell integration for canonical element for all time
    integral( fcn.integrand(xfldElem, qfldElemVec, mytime), xfldElem, rsdElem.get(), nIntegrand );

    //assuming same grid for all time...
    qfldgroups[mytime]->associativity( elem ).getGlobalMapping( mapDOFGlobal.get(), nDOF );

    int nGlobal;
    for (int n = 0; n < nDOF; n++)
    {
      nGlobal = mapDOFGlobal[n];
      rsdPDEGlobal[nGlobal] += rsdElem[n].PDE;
      //std::cout << "ntime: " << mytime << ", n: " << n << " rsd: " << rsdPDEGlobal[n] << "\n";
    }
  }

}

//----------------------------------------------------------------------------//
template<class TopDim>
class ResidualCell_HDG_HB;

// base class interface
template<>
class ResidualCell_HDG_HB<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  template <class PDE,
  class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandCell_HDG_HB<PDE>& fcn, const int mytime,
      const std::vector< std::unique_ptr< Field<PhysDim, TopoDim, ArrayQ> > >& qflds, //array of pointers to qfld objects
      int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal )
  {
    using VectorArrayQ =  DLA::VectorS<PhysDim::D, ArrayQ>;
    int ntimes = qflds.size();
    SANS_ASSERT( rsdPDEGlobal.m() == qflds[0]->nDOF() );
    SANS_ASSERT( ngroup == qflds[0]->nCellGroups() );

    const XField<PhysDim, TopoDim>& xfld = qflds[0]->getXField();
    std::vector < typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Line>* > qfldGroups(ntimes);

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Line) )
      {
        for (int i=0; i < ntimes; i++)
          qfldGroups[i] = &(qflds[i]->template getCellGroup<Line>(group));

        ResidualCell_HDG_HB_Group_Integral<Line, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
            fcn, mytime,
            xfld.template getCellGroup<Line>(group),
            qfldGroups,
            quadratureorder[group], rsdPDEGlobal );
      }
      else
      {
        const char msg[] = "Error in ResidualCell_Galerkin<TopoD1>: unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};


template<>
class ResidualCell_HDG_HB<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class PDE,
  class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandCell_HDG_HB<PDE>& fcn,
      const int mytime,
      const std::vector< std::unique_ptr <Field<PhysDim, TopoDim, ArrayQ>> >& qflds, //array of pointers to qfld objects
      int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ> & rsdPDEGlobal  )
  {
    typedef DLA::VectorS<PhysDim::D, ArrayQ> VectorArrayQ;
    int ntimes = qflds.size();
    SANS_ASSERT( rsdPDEGlobal.m() == qflds[0]->nDOF() );
    SANS_ASSERT( ngroup == qflds[0]->nCellGroups() );

    const XField<PhysDim, TopoDim>& xfld = qflds[0]->getXField();
    std::vector < typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Triangle>* > qGroups(ntimes);

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        for (int i=0; i < ntimes; i++)
          qGroups[i] = &(qflds[i]->template getCellGroup<Triangle>(group));


        ResidualCell_HDG_HB_Group_Integral<Triangle, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
            fcn, mytime,
            xfld.template getCellGroup<Triangle>(group),
            qGroups,
            quadratureorder[group], rsdPDEGlobal );
      }
      else
      {
        const char msg[] = "Error in ResidualCell_Galerkin<TopoD1>: unknown topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};

}

#endif  // RESIDUALCELL_HDG_HB_H
