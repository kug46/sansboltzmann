// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANINTERIORTRACE_HDG_H
#define JACOBIANINTERIORTRACE_HDG_H

// HDG trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral_New.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "JacobianInteriorTrace_HDG_Element.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//
template<class Surreal, class IntegrandInteriorTrace>
class JacobianInteriorTrace_HDG_impl :
    public GroupIntegralInteriorTraceType< JacobianInteriorTrace_HDG_impl<Surreal, IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianInteriorTrace_HDG_impl( const IntegrandInteriorTrace& fcn,
                                  const std::map<int,std::vector<int>>& cellITraceGroups,
                                  const int& cellgroup, const int& cellelem,
                                  const MatrixAUXElemClass& mtx_a_q,
                                  const std::vector<MatrixAUXElemClass>& mtx_a_qI,
                                  const std::vector<std::vector<int>>& mapDOFGlobal_qI,
                                  MatrixElemClass& mtxElemPDE_q,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI ) :
    fcn_(fcn), cellITraceGroups_(cellITraceGroups),
    cellgroup_(cellgroup), cellelem_(cellelem),
    mtx_a_q_(mtx_a_q), mtx_a_qI_(mtx_a_qI), mapDOFGlobal_qI_(mapDOFGlobal_qI),
    mtxElemPDE_q_(mtxElemPDE_q),
    mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
    mtxGlobalINT_q_(mtxGlobalINT_q), mtxGlobalINT_qI_(mtxGlobalINT_qI),
    comm_rank_(0)
  {
    for (std::map<int,std::vector<int>>::const_iterator it = cellITraceGroups_.begin(); it != cellITraceGroups_.end(); ++it)
      traceGroupIndices_.push_back( it->first );
  }

  std::size_t nInteriorTraceGroups() const { return traceGroupIndices_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return traceGroupIndices_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    SANS_ASSERT( mtxGlobalPDE_qI_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_qI_.n() == qIfld.nDOFpossessed() +  qIfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalINT_q_.m() == qIfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalINT_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalINT_qI_.m() == qIfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalINT_qI_.n() == qIfld.nDOFpossessed() + qIfld.nDOFghost() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<> ElementAFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyR> AFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;
    typedef typename AFieldCellGroupTypeR::template ElementType<> ElementAFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef JacobianElemInteriorTrace_HDG<PhysDim, MatrixQ> JacobianElemInteriorTraceType;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);
    const AFieldCellGroupTypeR& afldCellR = get<1>(fldsCellR);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldClassL afldElemL( afldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );
    ElementAFieldClassR afldElemR( afldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    //Check if sizes are consistent
    if (xfldTrace.getGroupLeft() == cellgroup_)
    {
      SANS_ASSERT( mtx_a_qI_.size() == TopologyL::NTrace );
      SANS_ASSERT( mapDOFGlobal_qI_.size() == TopologyL::NTrace );
    }
    else if (xfldTrace.getGroupRight() == cellgroup_)
    {
      SANS_ASSERT( mtx_a_qI_.size() == TopologyR::NTrace );
      SANS_ASSERT( mapDOFGlobal_qI_.size() == TopologyR::NTrace );
    }
    else
      SANS_DEVELOPER_EXCEPTION("JacobianInteriorTrace_HDG_impl::integrate - Invalid configuration!");

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFGlobal_qR(nDOFR, -1);
    std::vector<int> mapDOFGlobal_qI(nDOFI, -1);

    std::vector<int> mapDOFLocal_qI( nDOFI, -1 );
    std::vector<int> maprsd( nDOFI, -1 );
    int nDOFLocalI = 0;
    const int nDOFpossessed = mtxGlobalINT_qI_.m();

    MatrixElemClass mtxElemLocal( nDOFI, std::max(std::max(nDOFI, nDOFL), nDOFR) );


    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType, JacobianElemInteriorTraceType>
      integralPDEL(quadratureorder);

    GalerkinWeightedIntegral_New<TopoDimTrace, TopologyTrace, JacobianElemInteriorTraceType, JacobianElemInteriorTraceType>
      integralPDER(quadratureorder);

    JacobianElemInteriorTraceSize sizeL(nDOFL, nDOFL, nDOFI);
    JacobianElemInteriorTraceSize sizeIL(nDOFI, nDOFL, nDOFI);
    JacobianElemInteriorTraceSize sizeIR(nDOFI, nDOFR, nDOFI);
    JacobianElemInteriorTraceSize sizeR(nDOFR, nDOFR, nDOFI);

    // element jacobian matrices
    JacobianElemInteriorTraceType mtxElemL(sizeL);
    JacobianElemInteriorTraceType mtxElemIL(sizeIL);
    JacobianElemInteriorTraceType mtxElemIR(sizeIR);
    JacobianElemInteriorTraceType mtxElemR(sizeR);


    // element PDE jacobian matrices wrt qI on each trace of the "central" cell
    std::vector<MatrixElemClass> mtxElemPDEL_qI(mapDOFGlobal_qI_.size(), MatrixElemClass(0,0));
    std::vector<MatrixElemClass> mtxElemPDER_qI(mapDOFGlobal_qI_.size(), MatrixElemClass(0,0));
    std::vector<MatrixElemClass> mtxElemINT_qI (mapDOFGlobal_qI_.size(), MatrixElemClass(0,0));

    for (int trace = 0; trace < (int) mapDOFGlobal_qI_.size(); trace++)
    {
      mtxElemPDEL_qI[trace].resize( nDOFL, (int) mapDOFGlobal_qI_[trace].size() );
      mtxElemPDER_qI[trace].resize( nDOFR, (int) mapDOFGlobal_qI_[trace].size() );
      mtxElemINT_qI [trace].resize( nDOFI, (int) mapDOFGlobal_qI_[trace].size() );
    }

    const std::vector<int>& traceElemList = cellITraceGroups_.at(traceGroupGlobal);

    // loop over trace elements in list
    for (int i = 0; i < (int) traceElemList.size(); i++)
    {
      const int elem = traceElemList[i];

      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // global mapping for qI
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI.data(), nDOFI );

      // copy over the map for each qI DOF that is possessed by this processor
      nDOFLocalI = 0;
      for (int n = 0; n < nDOFI; n++)
      {
        if (mapDOFGlobal_qI[n] < nDOFpossessed)
        {
          maprsd[nDOFLocalI] = n;
          mapDOFLocal_qI[nDOFLocalI] = mapDOFGlobal_qI[n];
          nDOFLocalI++;
        }
      }

      if (xfldTrace.getGroupLeft() == cellgroup_ && elemL == cellelem_)
      {
        //The cell that called this function is to the left of the trace
        const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

        // copy global grid/solution DOFs to element
        xfldCellL.getElement( xfldElemL, elemL );
        qfldCellL.getElement( qfldElemL, elemL );
        afldCellL.getElement( afldElemL, elemL );

        // only possessed cell elements have PDE residuals on this processor
        if ( qfldElemL.rank() != comm_rank_ && nDOFLocalI == 0 ) continue;

        for (int trace = 0; trace < TopologyL::NTrace; trace++)
        {
          mtxElemPDEL_qI[trace] = 0;
          mtxElemINT_qI [trace] = 0;
        }

        // PDE trace integration for canonical element
        integralPDEL( fcn_.integrand(xfldElemTrace, qIfldElemTrace, canonicalTraceL, +1,
                                     xfldElemL, qfldElemL, afldElemL),
                          xfldElemTrace, mtxElemL, mtxElemIL );

        mtxElemPDEL_qI[canonicalTraceL.trace] = mtxElemL.PDE_qI;
        mtxElemINT_qI[canonicalTraceL.trace] = mtxElemIL.PDE_qI;

        //Complete the chain rule for the auxiliary variables
         mtxElemL.PDE_q +=  mtxElemL.PDE_a*mtx_a_q_;
        mtxElemIL.PDE_q += mtxElemIL.PDE_a*mtx_a_q_;

        //Complete the chain rule with respect to qI DOFs on other traces of the "central" cell
        for (int trace = 0; trace < TopologyL::NTrace; trace++)
        {
          if (mapDOFGlobal_qI_[trace].size() > 0)
          {
            mtxElemPDEL_qI[trace] +=  mtxElemL.PDE_a*mtx_a_qI_[trace];
            mtxElemINT_qI [trace] += mtxElemIL.PDE_a*mtx_a_qI_[trace];
          }
        }

        // The PDE_q jacobian will be added to global matrix in the JacobianCell routine,
        // so add the contribution from this trace and send it back.
        mtxElemPDE_q_ += mtxElemL.PDE_q;

        // scatter-add other element jacobians to global matrix directly
        scatterAdd( qIfldTrace, qfldCellL,
                    elem, elemL, qfldElemL.rank(),
                    mapDOFGlobal_qL.data(), nDOFL,
                    mapDOFGlobal_qI.data(), nDOFI,
                    maprsd.data(), nDOFLocalI,

                    mtxElemPDEL_qI,
                    mtxElemIL.PDE_q, mtxElemINT_qI,
                    mtxElemLocal,

                    mtxGlobalPDE_qI_,
                    mtxGlobalINT_q_, mtxGlobalINT_qI_ );

      }
      else if (xfldTrace.getGroupRight() == cellgroup_ && elemR == cellelem_)
      {
        //The cell that called this function is to the right of the trace
        const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

        // copy global grid/solution DOFs to element
        xfldCellR.getElement( xfldElemR, elemR );
        qfldCellR.getElement( qfldElemR, elemR );
        afldCellR.getElement( afldElemR, elemR );

        // only possessed cell elements have PDE residuals on this processor
        if ( qfldElemR.rank() != comm_rank_ && nDOFLocalI == 0 ) continue;

        for (int trace = 0; trace < TopologyR::NTrace; trace++)
        {
          mtxElemPDER_qI[trace] = 0;
          mtxElemINT_qI [trace] = 0;
        }

        // PDE trace integration for canonical element
        integralPDER( fcn_.integrand(xfldElemTrace, qIfldElemTrace, canonicalTraceR, -1,
                                     xfldElemR, qfldElemR, afldElemR),
                      xfldElemTrace, mtxElemR, mtxElemIR );

        mtxElemPDER_qI[canonicalTraceR.trace] = mtxElemR.PDE_qI;
        mtxElemINT_qI[canonicalTraceR.trace] = mtxElemIR.PDE_qI;

        //Complete the chain rule for the auxiliary variables
         mtxElemR.PDE_q +=  mtxElemR.PDE_a*mtx_a_q_;
        mtxElemIR.PDE_q += mtxElemIR.PDE_a*mtx_a_q_;

        //Complete the chain rule with respect to qI DOFs on other traces of the "central" cell
        for (int trace = 0; trace < TopologyR::NTrace; trace++)
        {
          if (mapDOFGlobal_qI_[trace].size() > 0)
          {
            mtxElemPDER_qI[trace] +=  mtxElemR.PDE_a*mtx_a_qI_[trace];
            mtxElemINT_qI [trace] += mtxElemIR.PDE_a*mtx_a_qI_[trace];
          }
        }

        // The PDE_q jacobian will be added to global matrix in the JacobianCell routine,
        // so add the contribution from this trace and send it back.
        mtxElemPDE_q_ += mtxElemR.PDE_q;

        // scatter-add other element jacobians to global matrix directly
        scatterAdd( qIfldTrace, qfldCellR,
                    elem, elemR, qfldElemR.rank(),
                    mapDOFGlobal_qR.data(), nDOFR,
                    mapDOFGlobal_qI.data(), nDOFI,
                    maprsd.data(), nDOFLocalI,

                    mtxElemPDER_qI,
                    mtxElemIR.PDE_q, mtxElemINT_qI,
                    mtxElemLocal,

                    mtxGlobalPDE_qI_,
                    mtxGlobalINT_q_, mtxGlobalINT_qI_ );
      }
      else
        SANS_DEVELOPER_EXCEPTION("JacobianInteriorTrace_HDG_impl::integrate - Invalid configuration!");

    } //elem loop
  }

  //----------------------------------------------------------------------------//
  template <class QFieldTraceGroupType,
            class QFieldCellGroupType,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldTraceGroupType& qfldTrace,
      const QFieldCellGroupType& qfldCell,
      const int elemTrace, const int elemCell, const int elemCellRank,
      int mapDOFGlobal_q[], const int qDOF,
      int mapDOFLocal_qI[], const int qIDOF,
      int maprsd[], const int qILocalDOF,

      const std::vector<MatrixElemClass>& mtxElemPDE_qI,
      const MatrixElemClass& mtxElemINT_q,
      const std::vector<MatrixElemClass>& mtxElemINT_qI,
      MatrixElemClass& mtxElemLocal,

      SparseMatrixType<MatrixQ>& mtxGlobalPDE_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalINT_q,
      SparseMatrixType<MatrixQ>& mtxGlobalINT_qI)
  {
    // global mapping for q
    qfldCell.associativity( elemCell ).getGlobalMapping( mapDOFGlobal_q, qDOF );

    if (elemCellRank == comm_rank_)
    {
      // jacobians wrt qI (including on traces of the central cell)
      for (int trace = 0; trace < (int) mapDOFGlobal_qI_.size(); trace++)
      {
        if (mapDOFGlobal_qI_[trace].size() > 0)
          mtxGlobalPDE_qI.scatterAdd( mtxElemPDE_qI[trace], mapDOFGlobal_q, qDOF, mapDOFGlobal_qI_[trace].data(), mapDOFGlobal_qI_[trace].size() );
      }
    }

    if (qILocalDOF == qIDOF)
    {
      // jacobian wrt q
      mtxGlobalINT_q.scatterAdd( mtxElemINT_q, mapDOFLocal_qI, qIDOF, mapDOFGlobal_q, qDOF );

      // jacobians wrt qI (including on traces of the central cell)
      for (int trace = 0; trace < (int) mapDOFGlobal_qI_.size(); trace++)
      {
        if (mapDOFGlobal_qI_[trace].size() > 0)
          mtxGlobalINT_qI.scatterAdd( mtxElemINT_qI[trace], mapDOFLocal_qI, qIDOF, mapDOFGlobal_qI_[trace].data(), mapDOFGlobal_qI_[trace].size() );
      }
    }
    else
    {
      // compress in only the DOFs possessed by this processor
      for (int i = 0; i < qILocalDOF; i++)
        for (int j = 0; j < qDOF; j++)
          mtxElemLocal(i,j) = mtxElemINT_q(maprsd[i],j);

      // jacobian wrt q
      mtxGlobalINT_q.scatterAdd( mtxElemLocal, mapDOFLocal_qI, qILocalDOF, mapDOFGlobal_q, qDOF );

      // jacobians wrt qI (including on traces of the central cell)
      for (int trace = 0; trace < (int) mapDOFGlobal_qI_.size(); trace++)
      {
        if (mapDOFGlobal_qI_[trace].size() > 0)
        {
          // compress in only the DOFs possessed by this processor
          for (int i = 0; i < qILocalDOF; i++)
            for (std::size_t j = 0; j < mapDOFGlobal_qI_[trace].size(); j++)
              mtxElemLocal(i,j) = mtxElemINT_qI[trace](maprsd[i],j);

          mtxGlobalINT_qI.scatterAdd( mtxElemLocal, mapDOFLocal_qI, qILocalDOF, mapDOFGlobal_qI_[trace].data(), mapDOFGlobal_qI_[trace].size() );
        }
      }
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  const std::map<int,std::vector<int>>& cellITraceGroups_;
  const int& cellgroup_;
  const int& cellelem_;
  const MatrixAUXElemClass& mtx_a_q_; //jacobian of auxiliary variable wrt q of the "central" cell
  const std::vector<MatrixAUXElemClass>& mtx_a_qI_; //jacobians of auxiliary variable wrt qI of all traces around "central" cell
  const std::vector<std::vector<int>>& mapDOFGlobal_qI_; //global DOF maps for qI DOFs on all traces around "central" cell

  MatrixElemClass& mtxElemPDE_q_; //jacobian of PDE residual wrt q for the central cell
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;

  std::vector<int> traceGroupIndices_;

  mutable int comm_rank_;
};


// Factory function

template<class Surreal, class IntegrandInteriorTrace, class VectorMatrixQ, class MatrixQ>
JacobianInteriorTrace_HDG_impl<Surreal, IntegrandInteriorTrace>
JacobianInteriorTrace_HDG( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                           const std::map<int,std::vector<int>>& cellITraceGroups,
                           const int& cellgroup, const int& cellelem,
                           const DLA::MatrixD<VectorMatrixQ>& mtx_a_q,
                           const std::vector<DLA::MatrixD<VectorMatrixQ>>& mtx_a_qI,
                           const std::vector<std::vector<int>>& mapDOFGlobal_qI,
                           DLA::MatrixD<MatrixQ>& mtxElemPDE_q,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI)
{
  return { fcn.cast(), cellITraceGroups, cellgroup, cellelem,
           mtx_a_q, mtx_a_qI, mapDOFGlobal_qI,
           mtxElemPDE_q, mtxGlobalPDE_qI,
           mtxGlobalINT_q, mtxGlobalINT_qI };
}

}


#endif  // JACOBIANINTERIORTRACE_HDG_H
