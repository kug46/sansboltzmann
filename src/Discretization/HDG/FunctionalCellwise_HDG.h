// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONALCELLWISE_HDG_H
#define FUNCTIONALCELLWISE_HDG_H

// Cell integral functional

#include "tools/Tuple.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group functional
//

template<class IntegrandCell, class T>
class FunctionalCellwise_HDG_impl :
    public GroupIntegralCellType< FunctionalCellwise_HDG_impl<IntegrandCell, T> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<T> ArrayQ;
  typedef typename IntegrandCell::template VectorArrayQ<T> VectorArrayQ;
  typedef typename IntegrandCell::template ArrayJ<T> ArrayJ;

  // Save off the cell integrand and the residual vector
  FunctionalCellwise_HDG_impl( const IntegrandCell& fcn,
                                 std::vector<std::vector<ArrayJ>>& functionalArray ) :
                                 fcn_(fcn), functionalArray_(functionalArray) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);
    SANS_ASSERT((int) functionalArray_.size() == qfld.getXField().nCellGroups());
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                                      template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                            ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ      >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename AFieldCellGroupType::template ElementType<> ElementAFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const AFieldCellGroupType& afldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementAFieldClass afldElem( afldCell.basis() );

    // element integral
    ElementIntegral<TopoDim, Topology, ArrayJ> integral(quadratureorder);

    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == afldCell.nElem() );

    functionalArray_[cellGroupGlobal].resize(nelem);

    // loop over elements within group
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      afldCell.getElement( afldElem, elem );

      ArrayJ result = 0;

      // cell integration for canonical element
      integral( fcn_.integrand(xfldElem, qfldElem, afldElem), get<-1>(xfldElem), result );

      // store the functional
      functionalArray_[cellGroupGlobal][elem] = result;
    }
  }

protected:
  const IntegrandCell& fcn_;
  std::vector<std::vector<ArrayJ>>& functionalArray_; //index [cellgroup][elem]
};

// Factory function

template<class IntegrandCell, class ArrayJ>
FunctionalCellwise_HDG_impl<IntegrandCell,typename Scalar<ArrayJ>::type>
FunctionalCellwise_HDG( const IntegrandCellType<IntegrandCell>& fcn,
                          std::vector<std::vector<ArrayJ>>& functionalArray )
{
  typedef typename Scalar<ArrayJ>::type T;
  static_assert( std::is_same<typename IntegrandCell::template ArrayJ<T>, ArrayJ>::value, "These should be the same");
  return FunctionalCellwise_HDG_impl<IntegrandCell,T>(fcn.cast(), functionalArray);
}

}

#endif  // FUNCTIONALCELLWISE_HDG_H
