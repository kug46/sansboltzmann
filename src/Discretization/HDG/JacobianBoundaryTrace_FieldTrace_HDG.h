// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_FIELDTRACE_HDG_H
#define JACOBIANBOUNDARYTRACE_FIELDTRACE_HDG_H

// HDG trace integral jacobian functions

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "tools/Tuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary trace group integral with lagrange multipliers
//

template<class Surreal, class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_FieldTrace_HDG_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_FieldTrace_HDG_impl<Surreal, IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename IntegrandBoundaryTrace::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandBoundaryTrace::template VectorArrayQ<Surreal> GradArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianBoundaryTrace_FieldTrace_HDG_impl( const IntegrandBoundaryTrace& fcn,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalINT_lg,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalBC_qI,
                                             MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg ) :
    fcn_(fcn), mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_qI_(mtxGlobalPDE_qI), mtxGlobalPDE_lg_(mtxGlobalPDE_lg),
               mtxGlobalINT_q_(mtxGlobalINT_q), mtxGlobalINT_qI_(mtxGlobalINT_qI), mtxGlobalINT_lg_(mtxGlobalINT_lg),
               mtxGlobalBC_q_(mtxGlobalBC_q)  , mtxGlobalBC_qI_(mtxGlobalBC_qI)  , mtxGlobalBC_lg_(mtxGlobalBC_lg)
  {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the matricies
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& fldsTrace ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&  qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld = get<0>(fldsTrace);
    const Field<PhysDim, TopoDim, ArrayQ>& lgfld = get<1>(fldsTrace);

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_qI_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_qI_.n() == qIfld.nDOFpossessed() + qIfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_lg_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_lg_.n() == lgfld.nDOFpossessed() + lgfld.nDOFghost() );


    SANS_ASSERT( mtxGlobalINT_q_.m() == qIfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalINT_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalINT_qI_.m() == qIfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalINT_qI_.n() == qIfld.nDOFpossessed() + qIfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalINT_lg_.m() == qIfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalINT_lg_.n() == lgfld.nDOFpossessed() + lgfld.nDOFghost() );


    SANS_ASSERT( mtxGlobalBC_q_.m() == lgfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalBC_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalBC_qI_.m() == lgfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalBC_qI_.n() == qIfld.nDOFpossessed() + qIfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalBC_lg_.m() == lgfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalBC_lg_.n() == lgfld.nDOFpossessed() + lgfld.nDOFghost() );
  }


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                       template FieldTraceGroupType<TopologyTrace>& fldsTrace,
      int quadratureorder )
  {
    SANS_DEVELOPER_EXCEPTION("JacobianBoundaryTrace_FieldTrace_HDG_impl::integrate - Not implemented yet.");
#if 0
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldSurrealClassL;

    //Trace types
    typedef typename XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldSurrealTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    const QFieldTraceGroupType& qIfldTrace = get<0>(fldsTrace);
    const QFieldTraceGroupType& lgfldTrace = get<1>(fldsTrace);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldSurrealClassL afldElemL( afldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    ElementQFieldSurrealTraceClass qIfldElemTrace( qIfldTrace.basis() );
    ElementQFieldSurrealTraceClass lgfldElemTrace( lgfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();
    const int nDOFBC = lgfldElemTrace.nDOF();

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL,-1);
    std::vector<int> mapDOFGlobal_aL(nDOFL*D,-1);

    std::vector<int> mapDOFGlobal_qI(nDOFI,-1);
    std::vector<int> mapDOFGlobal_lg(nDOFBC,-1);

    // element integral
    typedef IntegrandHDG<ArrayQSurreal,GradArrayQSurreal> CellIntegrandType;
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, ArrayQSurreal, ArrayQSurreal>
      integral(quadratureorder, nDOFL, nDOFI, nDOFBC);

    // element integrand/residual
    std::vector< CellIntegrandType > rsdElemL( nDOFL );
    std::vector< ArrayQSurreal > rsdElemInt( nDOFI );
    std::vector< ArrayQSurreal> rsdElemBC( nDOFBC );

    // element jacobian matrices
    // Left
    MatrixElemClass mtxElemPDEL_qL(nDOFL, nDOFL);
    MatrixElemClass mtxElemPDEL_aL(nDOFL, D*nDOFL);
    MatrixElemClass mtxElemPDEL_qI(nDOFL, nDOFI);
    MatrixElemClass mtxElemPDEL_lg(nDOFL, nDOFBC);

    MatrixElemClass mtxElemAuL_qL(D*nDOFL, nDOFL);
    MatrixElemClass mtxElemAuL_aL(D*nDOFL, D*nDOFL);
    MatrixElemClass mtxElemAuL_qI(D*nDOFL, nDOFI);
    MatrixElemClass mtxElemAuL_lg(D*nDOFL, nDOFBC);

    //trace
    MatrixElemClass mtxElemInt_qL(nDOFI, nDOFL);
    MatrixElemClass mtxElemInt_aL(nDOFI, D*nDOFL);
    MatrixElemClass mtxElemInt_qI(nDOFI, nDOFI);
    MatrixElemClass mtxElemInt_lg(nDOFI, nDOFBC);

    MatrixElemClass mtxElemBC_qL(nDOFBC, nDOFL);
    MatrixElemClass mtxElemBC_aL(nDOFBC, D*nDOFL);
    MatrixElemClass mtxElemBC_qI(nDOFBC, nDOFI);
    MatrixElemClass mtxElemBC_lg(nDOFBC, nDOFBC);


    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxElemPDEL_qL = 0; mtxElemPDEL_aL = 0; mtxElemPDEL_qI = 0; mtxElemPDEL_lg = 0;
      mtxElemAuL_qL  = 0; mtxElemAuL_aL  = 0; mtxElemAuL_qI  = 0; mtxElemAuL_lg  = 0;
      mtxElemInt_qL  = 0; mtxElemInt_aL  = 0; mtxElemInt_qI  = 0; mtxElemInt_lg  = 0;
      mtxElemBC_qL   = 0; mtxElemBC_aL   = 0; mtxElemBC_lg   = 0; mtxElemBC_qI   = 0;

      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );
      lgfldTrace.getElement( lgfldElemTrace, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*((D+1)*nDOFL + nDOFI + nDOFBC); nchunk += nDeriv)
      {

        // associate derivative slots

        // wrt qL
        int slot, slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j),n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt aL
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              for (int k = 0; k < nDeriv; k++)
                DLA::index(afldElemL.DOF(j)[d],n).deriv(k) = 0;

              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
          slotOffset += nEqn*nDOFL;
        }

        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFI;

        // wrt lg
        for (int j = 0; j < nDOFBC; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(lgfldElemTrace.DOF(j),n).deriv(k) = 0;

            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(lgfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFBC;

        for (int n = 0; n < nDOFL; n++) rsdElemL[n] = 0;
        for (int n = 0; n < nDOFBC; n++) rsdElemBC[n] = 0;

        // trace integration
        integral(
            fcn_.integrand(xfldElemTrace, canonicalTraceL,
                           xfldElemL, qfldElemL, afldElemL,
                           qIfldElemTrace, lgfldElemTrace),
            xfldElemTrace,
            rsdElemL.data(), nDOFL,
            rsdElemInt.data(), nDOFI,
            rsdElemBC.data(), nDOFBC);

        // accumulate derivatives into element jacobians
        slotOffset = 0;
        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDEL_qL(i,j),m,n) = DLA::index(rsdElemL[i].PDE,m).deriv(slot - nchunk);

                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAuL_qL(D*i+d,j),m,n) = DLA::index(rsdElemL[i].Au[d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemInt_qL(i,j),m,n) = DLA::index(rsdElemInt[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFBC; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemBC_qL(i,j),m,n) = DLA::index(rsdElemBC[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt aL
        for (int d = 0; d < D; d++)
        {
          for (int j = 0; j < nDOFL; j++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*j + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    DLA::index(mtxElemPDEL_aL(i,D*j+d),m,n) = DLA::index(rsdElemL[i].PDE,m).deriv(slot - nchunk);

                    for (int di = 0; di < D; di++)
                      DLA::index(mtxElemAuL_aL(D*i+di,D*j+d),m,n) = DLA::index(rsdElemL[i].Au[di],m).deriv(slot - nchunk);
                  }

                for (int i = 0; i < nDOFI; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemInt_aL(i,j),m,n) = DLA::index(rsdElemInt[i],m).deriv(slot - nchunk);

                for (int i = 0; i < nDOFBC; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemBC_aL(i,D*j+d),m,n) = DLA::index(rsdElemBC[i],m).deriv(slot - nchunk);
              }
            }
          }
          slotOffset += nEqn*nDOFL;
        }

        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDEL_qI(i,j),m,n) = DLA::index(rsdElemL[i].PDE,m).deriv(slot - nchunk);
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAuL_qI(D*i+d,j),m,n) = DLA::index(rsdElemL[i].Au[d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemInt_qI(i,j),m,n) = DLA::index(rsdElemInt[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFBC; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemBC_qI(i,j),m,n) = DLA::index(rsdElemBC[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nEqn*nDOFI;

        // wrt lg
        for (int j = 0; j < nDOFBC; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxElemPDEL_lg(i,j),m,n) = DLA::index(rsdElemL[i].PDE,m).deriv(slot - nchunk);
                  for (int d = 0; d < D; d++)
                    DLA::index(mtxElemAuL_lg(D*i+d,j),m,n) = DLA::index(rsdElemL[i].Au[d],m).deriv(slot - nchunk);
                }

              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemInt_lg(i,j),m,n) = DLA::index(rsdElemInt[i],m).deriv(slot - nchunk);

              for (int i = 0; i < nDOFBC; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemBC_lg(i,j),m,n) = DLA::index(rsdElemBC[i],m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nEqn*nDOFBC;

      }   // nchunk


      // scatter-add element jacobian to global

      scatterAdd(
          qfldCellL, afldCellL, qIfldTrace, lgfldTrace,
          elem, elemL,
          mapDOFGlobal_qL.data(), mapDOFGlobal_aL.data(), nDOFL,
          mapDOFGlobal_qI.data(), nDOFI, mapDOFGlobal_lg.data(), nDOFBC,

          mtxElemPDEL_qL, mtxElemPDEL_aL, mtxElemPDEL_qI, mtxElemPDEL_lg,
          mtxElemAuL_qL , mtxElemAuL_aL , mtxElemAuL_qI , mtxElemAuL_lg ,
          mtxElemInt_qL , mtxElemInt_aL , mtxElemInt_qI , mtxElemInt_lg ,
          mtxElemBC_qL  , mtxElemBC_aL  , mtxElemBC_qI  , mtxElemBC_lg  ,

          mtxGlobalPDE_q_, mtxGlobalPDE_a_, mtxGlobalPDE_qI_, mtxGlobalPDE_lg_,
          mtxGlobalAu_q_ , mtxGlobalAu_a_ , mtxGlobalAu_qI_ , mtxGlobalAu_lg_ ,
          mtxGlobalINT_q_, mtxGlobalInt_a_, mtxGlobalINT_qI_, mtxGlobalINT_lg_ ,
          mtxGlobalBC_q_ , mtxGlobalBC_a_ , mtxGlobalBC_qI_ , mtxGlobalBC_lg_);
    }
#endif
  }


//----------------------------------------------------------------------------//
  template <class QFieldCellGroupTypeL, class AFieldCellGroupTypeL, class QFieldTraceGroupType,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupTypeL& qfldL,
      const AFieldCellGroupTypeL& afldL,
      const QFieldTraceGroupType& qIfldTrace,
      const QFieldTraceGroupType& lgfldTrace,
      const int elem, const int elemL,
      int mapDOFGlobal_qL[], int mapDOFGlobal_aL[], const int nDOFL,
      int mapDOFGlobal_qI[], const int nDOFI,
      int mapDOFGlobal_lg[], const int nDOFBC,

      const MatrixElemClass& mtxElemPDEL_qL,
      const MatrixElemClass& mtxElemPDEL_aL,
      const MatrixElemClass& mtxElemPDEL_qI,
      const MatrixElemClass& mtxElemPDEL_lg,
      const MatrixElemClass& mtxElemAuL_qL,
      const MatrixElemClass& mtxElemAuL_aL,
      const MatrixElemClass& mtxElemAuL_qI ,
      const MatrixElemClass& mtxElemAuL_lg ,
      const MatrixElemClass& mtxElemInt_qL,
      const MatrixElemClass& mtxElemInt_aL,
      const MatrixElemClass& mtxElemInt_qI ,
      const MatrixElemClass& mtxElemInt_lg ,
      const MatrixElemClass& mtxElemBC_qL,
      const MatrixElemClass& mtxElemBC_aL ,
      const MatrixElemClass& mtxElemBC_qI,
      const MatrixElemClass& mtxElemBC_lg,

      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_a,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_lg,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_q,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_a,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalAu_lg,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_q,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_a,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalInt_lg,
      SparseMatrixType<MatrixQ>& mtxGlobalBC_q,
      SparseMatrixType<MatrixQ>& mtxGlobalBC_a,
      SparseMatrixType<MatrixQ>& mtxGlobalBC_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalBC_lg)
  {

    // global mapping for uL
    qfldL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    // global mapping for qL
    for (int n = 0; n < nDOFL; n++)
      for (int d = 0; d < PhysDim::D; d++)
        mapDOFGlobal_aL[PhysDim::D*n+d] = mapDOFGlobal_qL[n]*PhysDim::D+d;

    // global mapping for qI
    qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI, nDOFI );

    // global mapping for lg
    lgfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_lg, nDOFBC );

    // jacobian wrt qL
    mtxGlobalPDE_q.scatterAdd( mtxElemPDEL_qL, mapDOFGlobal_qL, nDOFL );
     mtxGlobalAu_q.scatterAdd( mtxElemAuL_qL , mapDOFGlobal_aL, nDOFL*PhysDim::D, mapDOFGlobal_qL, nDOFL );
    mtxGlobalInt_q.scatterAdd( mtxElemInt_qL , mapDOFGlobal_qI, nDOFI           , mapDOFGlobal_qL, nDOFL );
     mtxGlobalBC_q.scatterAdd( mtxElemBC_qL  , mapDOFGlobal_lg, nDOFBC          , mapDOFGlobal_qL, nDOFL );

    // jacobian wrt aL
    mtxGlobalPDE_a.scatterAdd( mtxElemPDEL_aL, mapDOFGlobal_qL, nDOFL , mapDOFGlobal_aL, nDOFL*PhysDim::D );
     mtxGlobalAu_a.scatterAdd( mtxElemAuL_aL , mapDOFGlobal_aL, nDOFL*PhysDim::D );
    mtxGlobalInt_a.scatterAdd( mtxElemInt_aL , mapDOFGlobal_qI, nDOFI,  mapDOFGlobal_aL, nDOFL*PhysDim::D );
     mtxGlobalBC_a.scatterAdd( mtxElemBC_aL  , mapDOFGlobal_lg, nDOFBC, mapDOFGlobal_aL, nDOFL*PhysDim::D );

   // jacobian wrt qI
   mtxGlobalPDE_qI.scatterAdd( mtxElemPDEL_qI, mapDOFGlobal_qL, nDOFL           , mapDOFGlobal_qI, nDOFI );
    mtxGlobalAu_qI.scatterAdd( mtxElemAuL_qI , mapDOFGlobal_aL, nDOFL*PhysDim::D, mapDOFGlobal_qI, nDOFI );
   mtxGlobalInt_qI.scatterAdd( mtxElemInt_qI , mapDOFGlobal_qI, nDOFI );
    mtxGlobalBC_qI.scatterAdd( mtxElemBC_qI  , mapDOFGlobal_lg, nDOFBC          , mapDOFGlobal_qI, nDOFI );

    // jacobian wrt lg
    mtxGlobalPDE_lg.scatterAdd( mtxElemPDEL_lg, mapDOFGlobal_qL, nDOFL           , mapDOFGlobal_lg, nDOFBC );
     mtxGlobalAu_lg.scatterAdd( mtxElemAuL_lg , mapDOFGlobal_aL, nDOFL*PhysDim::D, mapDOFGlobal_lg, nDOFBC );
    mtxGlobalInt_lg.scatterAdd( mtxElemInt_lg , mapDOFGlobal_qI, nDOFI           , mapDOFGlobal_lg, nDOFBC );
     mtxGlobalBC_lg.scatterAdd( mtxElemBC_lg  , mapDOFGlobal_lg, nDOFBC );
  }


protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class MatrixQ>
JacobianBoundaryTrace_FieldTrace_HDG_impl<Surreal, IntegrandBoundaryTrace>
JacobianBoundaryTrace_FieldTrace_HDG( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalINT_lg,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_qI,
                                      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg )
{
  return { fcn.cast(), mtxGlobalPDE_q, mtxGlobalPDE_qI, mtxGlobalPDE_lg,
                       mtxGlobalINT_q, mtxGlobalINT_qI, mtxGlobalINT_lg,
                       mtxGlobalBC_q , mtxGlobalBC_qI , mtxGlobalBC_lg };
}

}

#endif  // JACOBIANBOUNDARYTRACE_FIELDTRACE_HDG_H
