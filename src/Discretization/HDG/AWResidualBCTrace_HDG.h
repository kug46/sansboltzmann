// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AWRESIDUALBCTRACE_HDG_H
#define AWRESIDUALBCTRACE_HDG_H

// boundary-trace integral residual functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Discretization/HDG/AWIntegrandBCFunctor_HDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//
//  functions dispatched based on left (L) element topologies
//
//  process:
//  (a) loop over groups; dispatch to L (ResidualBCTrace_LeftTopology)
//  (b) dispatch to R (ResidualBCTrace_RightTopology)
//  (c) call base class routine with specific L and R (ResidualBCTrace_Group w/ Base& params)
//  (d) cast to specific L and R and call L/R specific topology routine (ResidualBCTrace_Group_Integral)
//  (e) loop over traces and integrate

template <class TopologyTrace, class TopologyL,
          class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ,
          class PDE, class WeightFunction, class BC >
void
AWResidualBCTrace_Group_Integral_HDG(
    const AWIntegrandBC_HDG<PDE, WeightFunction, BC>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
    const typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL>& afldCellL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
    int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
    SLA::SparseVector<ArrayQ>& rsdAuGlobal,
    SLA::SparseVector<ArrayQ>& rsdBCGlobal,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& efldCellGroupL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& elgfldTraceGroup)
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
  typedef typename AFieldCellGroupTypeL::template ElementType<> ElementAFieldClassL;

  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );
  ElementAFieldClassL afldElemL( afldCellL.basis() );

  ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

  ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

  // number of integrals evaluated per element
  int nIntegrandL = 1;
  int nIntegrandBC = 1;

  // element-to-global DOF mapping
  std::unique_ptr<int[]> mapDOFGlobalL( new int[nIntegrandL] );
  std::unique_ptr<int[]> mapDOFGlobalBC( new int[nIntegrandBC] );

  // trace element integral
  typedef IntegrandHDG<ArrayQ,VectorArrayQ> CellIntegrandType;
  GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, ArrayQ>
    integral(quadratureorder, nIntegrandL, nIntegrandBC);

  // element integrand/residuals
  std::unique_ptr<CellIntegrandType[]> rsdElemL( new CellIntegrandType[nIntegrandL] );
  std::unique_ptr<ArrayQ[]> rsdElemBC( new ArrayQ[nIntegrandBC] );

  // loop over elements within group
  const int nelem = xfldTrace.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int elemL = xfldTrace.getElementLeft( elem );
    const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );
    afldCellL.getElement( afldElemL, elemL );

    lgfldTrace.getElement( lgfldElemTrace, elem );

    xfldTrace.getElement( xfldElemTrace, elem );

    rsdElemL[0] = 0;

    rsdElemBC[0] = 0;

    integral(
        fcn.integrand(xfldElemTrace, canonicalTraceL,
                      xfldElemL, qfldElemL, afldElemL,
                      lgfldElemTrace),
        xfldElemTrace,
        rsdElemL.get(), nIntegrandL,
        rsdElemBC.get(), nIntegrandBC );

    // scatter-add element residuals to global
    efldCellGroupL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.get(), nIntegrandL );
    elgfldTraceGroup.associativity( elem ).getGlobalMapping( mapDOFGlobalBC.get(), nIntegrandBC );

    rsdPDEGlobal[mapDOFGlobalL[0]] += rsdElemL[0].PDE;

    for (int d = 0; d < PhysDim::D; d++)
    {
      rsdAuGlobal[PhysDim::D*mapDOFGlobalL[0]+d] += rsdElemL[0].Au[d];
    }

    rsdBCGlobal[mapDOFGlobalBC[0]] += rsdElemBC[0];
  }
}

template <class TopologyTrace, class TopologyL,
          class PDE, class WeightFunction, class BC,
          class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
void
AWResidualBCTrace_Group_HDG(
    const AWIntegrandBC_HDG<PDE, WeightFunction, BC>& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
    const int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
    SLA::SparseVector<ArrayQ>& rsdAuGlobal,
    SLA::SparseVector<ArrayQ>& rsdBCGlobal,
    const Field<PhysDim, TopoDim, ArrayQ>& efld,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& elgfldTrace )
{
  const XField<PhysDim, TopoDim>& xfld = afld.getXField();

  int groupL = xfldTrace.getGroupLeft();

  // Integrate over the trace group
  AWResidualBCTrace_Group_Integral_HDG<TopologyTrace, TopologyL, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
      fcn, xfldTrace,
      xfld.template getCellGroup<TopologyL>(groupL), qfld.template getCellGroup<TopologyL>(groupL), afld.template getCellGroup<TopologyL>(groupL),
      lgfldTrace, quadratureorder, rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal,
      efld.template getCellGroup<TopologyL>(groupL), elgfldTrace );
}

template<class TopDim>
class AWResidualBCTrace_HDG;

template<>
class AWResidualBCTrace_HDG<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  template <class TopologyTrace, class PDE, class WeightFunction, class BC,
            class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  LeftTopology(
      const AWIntegrandBC_HDG<PDE, WeightFunction, BC>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdBCGlobal,
      const Field<PhysDim, TopoDim, ArrayQ>& efld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& elgfldTrace)
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( afld.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // determine topology for R
      AWResidualBCTrace_Group_HDG<TopologyTrace, Line>( fcn, xfldTrace, qfld, afld, lgfldTrace, quadratureorder,
                                                            rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal,
                                                            efld, elgfldTrace );
    }
    else
    {
      char msg[] = "Error in AWResidualBCTrace_HDG<TopoD1>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class PDE, class WeightFunction, class BC,
  class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  integrate(
      const AWIntegrandBC_HDG<PDE, WeightFunction, BC>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdBCGlobal )
  {
    SANS_ASSERT( &qfld.getXField() == &afld.getXField() );
    SANS_ASSERT( &afld.getXField() == &lgfld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = afld.getXField();
    // Error Est Field - for associativity maps
    Field_DG_Cell<PhysDim, TopoDim, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);
    efld = 0;
    SANS_ASSERT(efld.nDOF() == rsdPDEGlobal.m() ); // checking the rsdFields are P0
    SANS_ASSERT(efld.nDOF()*PhysDim::D == rsdAuGlobal.m() );
    // interface solution: P1 (aka Q1)
    Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> elgfld(xfld, 0, BasisFunctionCategory_Legendre);
    elgfld = 0;
    SANS_ASSERT(elgfld.nDOF() == rsdBCGlobal.m() ); // checking int field is p0

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node>( fcn,
            xfld.template getBoundaryTraceGroup<Node>(boundaryGroup),
            qfld, afld,
            lgfld.template getBoundaryTraceGroupGlobal<Node>(boundaryGroup),
            quadratureorder[boundaryGroup], rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal,
            efld, elgfld.template getBoundaryTraceGroup<Node>(boundaryGroup) );
      }
      else
      {
        char msg[] = "Error in AWResidualBCTrace_HDG<TopoD1>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};


template<>
class AWResidualBCTrace_HDG<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class TopologyTrace, class PDE, class WeightFunction, class BC,
            class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  LeftTopology(
      const AWIntegrandBC_HDG<PDE, WeightFunction, BC>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdBCGlobal,
      const Field<PhysDim, TopoDim, ArrayQ>& efld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& elgfldTrace)
  {
    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( afld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      AWResidualBCTrace_Group_HDG<TopologyTrace, Triangle>( fcn, xfldTrace, qfld, afld, lgfldTrace, quadratureorder,
                                                                rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal,
                                                                efld, elgfldTrace);
    }
    else
    {
      char msg[] = "Error in AWResidualBCTrace_HDG<TopoD2>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class PDE, class WeightFunction, class BC,
  class PhysDim, class ArrayQ, class VectorArrayQ>
  static void
  integrate(
      const AWIntegrandBC_HDG<PDE, WeightFunction, BC>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdAuGlobal,
      SLA::SparseVector<ArrayQ>& rsdBCGlobal )
  {
    SANS_ASSERT( &qfld.getXField() == &afld.getXField() );
    SANS_ASSERT( &afld.getXField() == &lgfld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = afld.getXField();
    // Error Est Field - for associativity maps
    Field_DG_Cell<PhysDim, TopoDim, ArrayQ> efld(xfld, 0, BasisFunctionCategory_Legendre);
    efld = 0;
    SANS_ASSERT(efld.nDOF() == rsdPDEGlobal.m() ); // checking the rsdFields are P0
    SANS_ASSERT(efld.nDOF()*PhysDim::D == rsdAuGlobal.m() );
    // interface solution: P1 (aka Q1)
    Field_DG_BoundaryTrace<PhysDim, TopoDim, ArrayQ> elgfld(xfld, 0, BasisFunctionCategory_Legendre);
    elgfld = 0;
    SANS_ASSERT(elgfld.nDOF() == rsdBCGlobal.m() ); // checking int field is p0

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line>( fcn,
            xfld.template getBoundaryTraceGroup<Line>(boundaryGroup),
            qfld, afld,
            lgfld.template getBoundaryTraceGroupGlobal<Line>(boundaryGroup),
            quadratureorder[boundaryGroup], rsdPDEGlobal, rsdAuGlobal, rsdBCGlobal,
            efld, elgfld.template getBoundaryTraceGroup<Line>(boundaryGroup) );
      }
      else
      {
        char msg[] = "Error in AWResidualBCTrace_HDG<TopoD2>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};

}

#endif  // AWRESIDUALBCTRACE_HDG_H
