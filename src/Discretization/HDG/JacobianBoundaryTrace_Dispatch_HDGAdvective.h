// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_DISPATCH_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_DISPATCH_HDGADVECTIVE_H_

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#include "JacobianBoundaryTrace_FieldTrace_HDGAdvective.h"
#include "JacobianBoundaryTrace_HDGAdvective.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class QAFieldBundleType,
         class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
class JacobianBoundaryTrace_FieldTrace_Dispatch_HDGAdvective_impl
{
public:
  JacobianBoundaryTrace_FieldTrace_Dispatch_HDGAdvective_impl(
      const XFieldType& xfld,
      const QAFieldBundleType& qafldBundle,
      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureOrder, const int nBTraceGroup,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
      MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
      MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI,
      MatrixScatterAdd<MatrixQ>& mtxGlobalINT_lg,
      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_qI,
      MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg ) :
    xfld_(xfld),
    qafldBundle_(qafldBundle),
    qIfld_(qIfld),
    lgfld_(lgfld),
    quadratureOrder_(quadratureOrder),
    nBTraceGroup_(nBTraceGroup),
    mtxGlobalPDE_q_(mtxGlobalPDE_q),
    mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
    mtxGlobalPDE_lg_(mtxGlobalPDE_lg),
    mtxGlobalINT_q_(mtxGlobalINT_q),
    mtxGlobalINT_qI_(mtxGlobalINT_qI),
    mtxGlobalINT_lg_(mtxGlobalINT_lg),
    mtxGlobalBC_q_(mtxGlobalBC_q),
    mtxGlobalBC_qI_(mtxGlobalBC_qI),
    mtxGlobalBC_lg_(mtxGlobalBC_lg)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_mitLG_HDGAdvective(fcn,
                                                      mtxGlobalPDE_q_, mtxGlobalPDE_qI_, mtxGlobalPDE_lg_,
                                                      mtxGlobalINT_q_, mtxGlobalINT_qI_, mtxGlobalINT_lg_,
                                                      mtxGlobalBC_q_ , mtxGlobalBC_qI_ , mtxGlobalBC_lg_ ),
        xfld_, qafldBundle_, (qIfld_,lgfld_), quadratureOrder_, nBTraceGroup_ );
  }

protected:
  const XFieldType& xfld_;
  const QAFieldBundleType& qafldBundle_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int *quadratureOrder_, nBTraceGroup_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_lg_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg_;
};

// Factory function

template<class XFieldType, class QAFieldBundleType,
         class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
JacobianBoundaryTrace_FieldTrace_Dispatch_HDGAdvective_impl<XFieldType, QAFieldBundleType, PhysDim, TopoDim, ArrayQ, MatrixQ>
JacobianBoundaryTrace_FieldTrace_Dispatch_HDGAdvective(
  const XFieldType& xfld,
  const QAFieldBundleType& qafldBundle,
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
  const int* quadratureOrder, int nBTraceGroup,
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_lg,
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI,
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_lg,
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_q,
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_qI,
  MatrixScatterAdd<MatrixQ>& mtxGlobalBC_lg  )
{
  return { xfld, qafldBundle, qIfld, lgfld, quadratureOrder, nBTraceGroup,
           mtxGlobalPDE_q, mtxGlobalPDE_qI, mtxGlobalPDE_lg,
           mtxGlobalINT_q, mtxGlobalINT_qI, mtxGlobalINT_lg,
           mtxGlobalBC_q , mtxGlobalBC_qI , mtxGlobalBC_lg };
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class QAFieldBundleType,
         class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
class JacobianBoundaryTrace_Dispatch_HDGAdvective_impl
{
public:
  JacobianBoundaryTrace_Dispatch_HDGAdvective_impl(
                                          const XFieldType& xfld,
                                          const QAFieldBundleType& qafldBundle,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                          const int quadOrderBTrace[], const int nBTraceGroup,
                                          MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                          MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                          MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                          MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI ) :
                                            xfld_(xfld), qafldBundle_(qafldBundle), qIfld_(qIfld),
                                            quadOrderBTrace_(quadOrderBTrace), nBTraceGroup_(nBTraceGroup),
                                            mtxGlobalPDE_q_(mtxGlobalPDE_q),
                                            mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
                                            mtxGlobalINT_q_(mtxGlobalINT_q),
                                            mtxGlobalINT_qI_(mtxGlobalINT_qI)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcnBTrace)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_HDGAdvective( fcnBTrace,
                                            xfld_, qafldBundle_, qIfld_,
                                            mtxGlobalPDE_q_, mtxGlobalPDE_qI_,
                                            mtxGlobalINT_q_, mtxGlobalINT_qI_),
        xfld_, qafldBundle_, qIfld_, quadOrderBTrace_, nBTraceGroup_ );
  }

protected:
  const XFieldType& xfld_;
  const QAFieldBundleType& qafldBundle_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;

  const int *quadOrderBTrace_, nBTraceGroup_;

  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;
};

// Factory function

template<class XFieldType, class QAFieldBundleType,
         class PhysDim, class TopoDim, class ArrayQ, class MatrixQ>
JacobianBoundaryTrace_Dispatch_HDGAdvective_impl<XFieldType, QAFieldBundleType,
                                                 PhysDim, TopoDim, ArrayQ, MatrixQ>
JacobianBoundaryTrace_Dispatch_HDGAdvective(
    const XFieldType& xfld,
    const QAFieldBundleType& qafldBundle,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const int quadOrderBTrace[], const int nBTraceGroup,
    MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q, MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
    MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q, MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI)
{
  return { xfld, qafldBundle, qIfld,
           quadOrderBTrace, nBTraceGroup,
           mtxGlobalPDE_q, mtxGlobalPDE_qI,
           mtxGlobalINT_q, mtxGlobalINT_qI };
}

}

#endif /* SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_DISPATCH_HDGADVECTIVE_H_ */
