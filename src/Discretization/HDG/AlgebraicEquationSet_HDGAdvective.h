// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_ALGEBRAICEQUATIONSET_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_ALGEBRAICEQUATIONSET_HDGADVECTIVE_H_

#include <vector>
#include <map>
#include <string>

#include "tools/SANSnumerics.h"

#include "pde/BCParameters.h"

#include "Field/FieldTypes.h"

#include "Discretization/ResidualNormType.h"
#include "Discretization/QuadratureOrder.h"
#include "Discretization/AlgebraicEquationSet_Debug.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandCell_Galerkin_manifold.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDGAdvective.h"
#include "Discretization/HDG/IntegrateBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Dirichlet_mitLG_HDG.h"

namespace SANS
{

// Discretization (specifically, integrands) identification tag
class HDGAdv;
class HDGAdv_manifold;

template<class DiscTag>
struct HDGAdvIntegrands;

template<>
struct HDGAdvIntegrands<HDGAdv>
{
  template <class PDE>
  using IntegrandCell = IntegrandCell_Galerkin<PDE>;

  template <class PDE>
  using IntegrandInteriorTrace = IntegrandInteriorTrace_HDGAdvective<PDE, HDGAdv>;
};

template<>
struct HDGAdvIntegrands<HDGAdv_manifold>
{
  template <class PDE>
  using IntegrandCell = IntegrandCell_Galerkin_manifold<PDE>;

  template <class PDE>
  using IntegrandInteriorTrace = IntegrandInteriorTrace_HDGAdvective<PDE, HDGAdv_manifold>;
};

template<> struct DiscBCTag<BCCategory::LinearScalar_mitLG, HDGAdv>  { typedef HDGAdv type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG, HDGAdv> { typedef HDGAdv type; };
template<> struct DiscBCTag<BCCategory::Flux_mitState, HDGAdv>       { typedef HDGAdv type; };
template<> struct DiscBCTag<BCCategory::None, HDGAdv>                { typedef HDGAdv type; };

//template<> struct DiscBCTag<BCCategory::LinearScalar_mitLG, HDGAdv_manifold>  { typedef HDGAdv type; };
//template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG, HDGAdv_manifold> { typedef HDGAdv type; };
//template<> struct DiscBCTag<BCCategory::Flux_mitState, HDGAdv_manifold>       { typedef HDGAdv type; };
template<> struct DiscBCTag<BCCategory::None, HDGAdv_manifold>                { typedef HDGAdv_manifold type; };


//----------------------------------------------------------------------------//
template<class NDPDEClass_, template<class,class> class BCNDConvert, class BCVector,
         class Traits, class DiscTag, class XFieldType>
class AlgebraicEquationSet_HDGAdvective : public AlgebraicEquationSet_Debug<NDPDEClass_, Traits>
{
public:
  typedef NDPDEClass_ NDPDEClass;
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template VectorArrayQ<Real> VectorArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef Traits TraitsTag;
  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef BCParameters<BCVector> BCParams;

  typedef typename HDGAdvIntegrands<DiscTag>::template IntegrandCell<NDPDEClass> IntegrandCellClass;
  typedef typename HDGAdvIntegrands<DiscTag>::template IntegrandInteriorTrace<NDPDEClass> IntegrandInteriorTraceClass;
  typedef IntegrateBoundaryTrace_Dispatch_HDG<NDPDEClass, BCNDConvert, BCVector, DiscTag> IntegrateBoundaryTrace_DispatchClass;

  // Indexes to order the equations and the solution vectors
  static const int nEqnSet = 3;
  static const int iPDE = 0;
  static const int iINT = 1;
  static const int iBC = 2;

  static const int nSolSet = 3;
  static const int iq = 0;
  static const int iqI = 1;
  static const int ilg = 2;

  template< class... BCArgs >
  AlgebraicEquationSet_HDGAdvective(const XFieldType& xfld,
                           Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                           Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                           Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                           const NDPDEClass& pde,
                           const QuadratureOrder& quadratureOrder,
                           const ResidualNormType& resNormType,
                           const std::vector<Real>& tol,
                           const std::vector<int>& cellGroups,
                           const std::vector<int>& interiorTraceGroups,
                           PyDict& bcList,
                           const std::map< std::string, std::vector<int> >& bcBoundaryGroups,
                           BCArgs&... args );

  virtual ~AlgebraicEquationSet_HDGAdvective() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx) override;
  virtual void jacobian(SystemNonZeroPatternView& nz) override;

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT) override;
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override;

  // Used to compute jacobians wrt. parameters
  template<int iParam, class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac, const QuadratureOrder& quadratureOrder, int ip ) const;

  // Returns norm of the residual
  virtual std::vector<std::vector<Real>> residualNorm(const SystemVectorView& rsd) const override;

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override;

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override; //TODO: to be updated

  //Sets the primal adjoint field and computes the lifting operator adjoint
  void setAdjointField(const SystemVectorView& adj,
                       Field_DG_Cell<PhysDim, TopoDim, ArrayQ>&       wfld,
                       Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& bfld,
                       Field<PhysDim, TopoDim, ArrayQ>&               wIfld,
                       Field<PhysDim, TopoDim, ArrayQ>&               mufld );

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;
  bool isValidState();

  // Add an AES to this one to daisy-chain
  virtual void addAlgebraicEquationSet(std::shared_ptr<BaseType> AES);

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override;

  virtual void syncDOFs_MPI() override;

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  const IntegrateBoundaryTrace_DispatchClass& dispatchBC() const { return dispatchBC_; }

  const IntegrandCellClass& getCellIntegrand() const { return fcnCell_; };
  const IntegrandInteriorTraceClass& getInteriorTraceIntegrand() const { return fcnTrace_; };

  const QuadratureOrder& quadratureOrder() const { return quadratureOrder_; }
  const QuadratureOrder& quadratureOrderMin() const { return quadratureOrderMin_; }

protected:

  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const QuadratureOrder& quadratureOrder,
                const std::vector<int>& interiorTraceCellJac = {});

  DiscretizationHDG<NDPDEClass> disc_; // TODO: to be removed
  IntegrandCellClass fcnCell_;
  IntegrandInteriorTraceClass fcnTrace_;
  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_DispatchClass dispatchBC_;

  // HDG solution fields and mesh
  const XFieldType& xfld_;

  // cell solution
  Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  // interface solution
  Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  // Lagrange multiplier
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;

  // The PDE
  const NDPDEClass& pde_;
  QuadratureOrder quadratureOrder_;
  QuadratureOrder quadratureOrderMin_;
  const ResidualNormType resNormType_; // TODO: to be extended to allow for other options
  const std::vector<Real> tol_;
  std::shared_ptr<BaseType> AES_;
};

} // namespace SANS

#endif /* SRC_DISCRETIZATION_HDG_ALGEBRAICEQUATIONSET_HDGADVECTIVE_H_ */
