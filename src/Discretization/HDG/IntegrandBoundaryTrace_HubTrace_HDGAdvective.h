// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_HUBTRACE_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_HUBTRACE_HDGADVECTIVE_H_

#include <ostream>
#include <vector>

#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "DiscretizationHDG.h"
#include "Integrand_HDG_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
namespace IntegrandBoundaryTrace_HubTrace_HDGAdv_detail
{
// worker object that can be partially specialized to do specific tasks depending on
// the type of dicretization (as in DiscTag)
template <class PDENDConvert, class NDBCVecCategory, class DiscTag>
struct worker;
} // namespace IntegrandBoundaryTrace_HubTrace_HDGAdv_detail


//----------------------------------------------------------------------------//
template<class PDE_, class NDBCVector, class DiscTag_>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::HubTrace>, DiscTag_> :
  public IntegrandBoundaryTraceType<
    IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::HubTrace>, DiscTag_> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::HubTrace Category;
  typedef NDVectorCategory<NDBCVector, Category> NDBCVecCategory;
  typedef DiscTag_ DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename PDE::template VectorArrayQ<T>; // solution gradient arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;        // matrices

  static const int N = PDE::N;

  IntegrandBoundaryTrace(const PDE& pde,
                         const BCBase& bc,
                         const std::vector<int>& boundaryGroups,
                         const DiscretizationHDG<PDE>& disc) :
    pde_(pde),
    bc_(bc),
    boundaryGroups_(boundaryGroups),
    disc_(disc) {}

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return boundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return boundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  private:
    friend struct IntegrandBoundaryTrace_HubTrace_HDGAdv_detail::worker<PDE, NDBCVecCategory, DiscTag>; // make a friend struct to do some labor

  public:
    typedef typename ElementParam::T ParamT;

    typedef Element<ArrayQ<T>, TopoDimCell,  Topology> ElementQFieldCell;
    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTraceType;

    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    template <class Z>
    using IntegrandCellType = ArrayQ<Z>;

    template <class Z>
    using IntegrandTraceType = ArrayQ<Z>;

    BasisWeighted(const PDE& pde,
                  const BCBase& bc,
                  const ElementXFieldTrace& xfldElemTrace,
                  const CanonicalTraceToCell& canonicalTrace,
                  const ElementParam& paramfldElem, // Xfield must be the last parameter in the tuple
                  const ElementQFieldCell& qfldElem,
                  const ElementQFieldTraceType& hbfldTrace) :
      pde_(pde),
      bc_(bc),
      xfldElemTrace_(xfldElemTrace),
      canonicalTrace_(canonicalTrace),
      xfldElem_(get<-1>(paramfldElem)),
      qfldElem_(qfldElem),
      hbfldTrace_(hbfldTrace),
      paramfldElem_(paramfldElem),
      nDOFCell_(qfldElem_.nDOF()),
      nDOFTrace_(hbfldTrace_.nDOF()),
      phi_( new Real[nDOFCell_] ),
      phiT_( new Real[nDOFTrace_] ) {}

    ~BasisWeighted()
    {
      delete [] phi_;
      delete [] phiT_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFCell_; }
    int nDOFTrace() const { return nDOFTrace_; }


    // handle PDE and HubTrace equations
    template <class Ti>
    void operator()(const QuadPointTraceType& RefTrace, IntegrandCellType<Ti> integrandCell[], int neqnCell,
                                                        IntegrandTraceType<Ti> integrandTrace[], int neqnTrace) const
    {
      call_with_derived<NDBCVector>(*this, bc_, RefTrace, integrandCell, neqnCell,
                                                          integrandTrace, neqnTrace );
    }

    // handle interface equations
    template <class Ti>
    void operator()(const Real dJ, const QuadPointTraceType& sRefTrace,
                    DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace) const
    {
      call_with_derived<NDBCVector>(*this, bc_, dJ, sRefTrace, rsdPDEElemL, rsdINTElemTrace );
    }

    FRIEND_CALL_WITH_DERIVED

  protected:
    template<class BC_, class Ti>
    void operator()( const BC_& bc,
                     const QuadPointTraceType& RefTrace, IntegrandCellType<Ti> integrandCell[], int neqnCell,
                                                         IntegrandTraceType<Ti> integrandTrace[], int neqnTrace ) const;

    template<class BC_, class Ti>
    void operator()( const BC_& bc, const Real dJ, const QuadPointTraceType& sRefTrace,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const;

    const PDE& pde_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldTraceType& hbfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFCell_;
    const int nDOFTrace_;
    mutable Real *phi_;
    mutable Real *phiT_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem, // Xfield must be the last parameter
            const Element<ArrayQ<T>, TopoDimCell , Topology     >& qfldElem,
            const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& hbfldTrace) const
  {
    return BasisWeighted< T,TopoDimTrace,TopologyTrace,
                            TopoDimCell, Topology, ElementParam>(pde_, bc_,
                                                                 xfldElemTrace, canonicalTrace,
                                                                 paramfldElem,
                                                                 qfldElem,
                                                                 hbfldTrace);
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> boundaryGroups_;
  const DiscretizationHDG<PDE>& disc_; //TODO: dummy here
};

//----------------------------------------------------------------------------//
template<class PDE_, class NDBCVector, class DiscTag_>
template<class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam>
template<class BC_, class Ti>
void
IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::HubTrace>, DiscTag_>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC_& bc, const QuadPointTraceType& sRefTrace,
           IntegrandCellType<Ti>  integrandCell[],  int neqnCell,
           IntegrandTraceType<Ti> integrandTrace[], int neqnTrace) const
{
  static_assert(TopoDimCell::D == 1, "Only TopoD1 element is implemented so far."); // TODO: generalize
  static_assert(PhysDim::D == 2, "Only PhysD2 is implemented so far."); // TODO: generalize

  SANS_ASSERT((neqnCell == nDOFCell_) && (neqnTrace == nDOFTrace_));

  typedef DLA::VectorS<TopoDimCell::D, VectorX> LocalAxes;  // manifold local axes type

  ParamT param;              // Elemental parameters (such as grid coordinates and distance functions)

  VectorX nrm;               // unit normal (points out of domain)

  VectorX e01L;                              // basis direction vector 1
  LocalAxes e0L;                             // basis direction vector

  ArrayQ<T> q;               // solution
  ArrayQ<T> hb;              // Hub-trace variables

  QuadPointCellType sRefL;   // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval(canonicalTrace_, sRefTrace, sRefL);

  // Elemental parameters
  paramfldElem_.eval(sRefL, param);

  // physical coordinates
  xfldElem_.unitTangent(sRefL, e01L);
  e0L[0] = e01L;

  // unit normal: points out of domain
  traceUnitNormal(xfldElem_, sRefL, xfldElemTrace_, sRefTrace, nrm);

  // basis value, gradient
  qfldElem_.evalBasis(sRefL, phi_, nDOFCell_);

  // solution value, gradient
  qfldElem_.evalFromBasis(phi_, nDOFCell_, q);

  // Hub trace basis
  hbfldTrace_.evalBasis(sRefTrace, phiT_, nDOFTrace_);

  // Hub trace
  hbfldTrace_.evalFromBasis(phiT_, nDOFTrace_, hb);

  //-------  -------//
  ArrayQ<Ti> f = 0.0, sourcehub = 0.0;
  bc.fluxMatchingSourceHubtrace(param, e0L, nrm, q, hb, f, sourcehub);

  for (int k = 0; k < neqnCell; ++k)
    integrandCell[k] = phi_[k]*f; // PDE residual: weak form boundary integral

  for (int k = 0; k < neqnTrace; ++k)
    integrandTrace[k] = phiT_[k]*sourcehub; // Matching condition residual at the hubtrace
}

//----------------------------------------------------------------------------//
template<class PDE_, class NDBCVector, class DiscTag_>
template<class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class Topology, class ElementParam>
template<class BC_, class Ti>
void
IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::HubTrace>, DiscTag_>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC_& bc, const Real dJ, const QuadPointTraceType& sRefTrace,
           DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace) const
{
  static_assert(TopoDimCell::D == 1, "Only TopoD1 element is implemented so far."); // TODO: generalize
  static_assert(PhysDim::D == 2, "Only PhysD2 is implemented so far."); // TODO: generalize

  SANS_ASSERT((rsdPDEElemL.m() == nDOFCell_) && (rsdINTElemTrace.m() == nDOFTrace_));

  // Hub trace basis
  hbfldTrace_.evalBasis(sRefTrace, phiT_, nDOFTrace_);

  // Hub trace
  ArrayQ<T> hb;              // Hub-trace variables
  hbfldTrace_.evalFromBasis(phiT_, nDOFTrace_, hb);

  const ArrayQ<T> dummyEqn = hb - ArrayQ<Real>(pde_.varMatchDummy_); // dummy interface equation

  for (int k = 0; k < nDOFTrace_; ++k)
    rsdINTElemTrace[k] = phiT_[k]*dummyEqn;
}

//----------------------------------------------------------------------------//
// TODO: to be completed and used when necessary
#if 0
namespace IntegrandBoundaryTrace_HubTrace_HDGAdv_detail
{
// DiscTag = HDGAdv_manifold
template <class PDENDConvert, class NDBCVecCategory>
struct worker<PDENDConvert, NDBCVecCategory, HDGAdv_manifold>
{
  typedef IntegrandBoundaryTrace<PDENDConvert,NDBCVecCategory,HDGAdv_manifold> IntegrandType;

  template<class T>
  using ArrayQ = typename IntegrandType::template ArrayQ<T>;

  template<class T>
  using VectorArrayQ = typename IntegrandType::template VectorArrayQ<T>;

  template<class T, class TopoDimTrace,class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParamL>
  using BasisWeightedIntegrandClass =
      typename IntegrandType::template BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>;

//  // compute the weighted integrands
//  template<class T, class TopoDimTrace,class TopologyTrace,
//                    class TopoDimCell, class TopologyL, class ElementParamL,
//           class Tq, class Ti>
//  static void
//  weightedIntegrand(
//      const BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>* integrandFcn,
//      const Real dJ,
//      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
//            QuadPointTraceType& sRefTrace,
//      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
//            QuadPointCellType& sRefL,
//      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
//            ParamT& paramL,
//      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qI,
//      DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace )
//  {
//    typedef BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL> BasisWeightedIntegrandType;
//    typedef typename BasisWeightedIntegrandType::VectorX VectorX;
//
//    SANS_ASSERT_MSG(!integrandFcn->pde_.hasFluxViscous(), "Should not have viscous flux!");
//
//    // trace unit normal
//    VectorX nL; // unit normal for left element (points to right element)
//    traceUnitNormal( integrandFcn->xfldElem_, sRefL,
//                     integrandFcn->xfldElemTrace_, sRefTrace, nL);
//    // adjust the sign of the trace normal vector so that it points out of the left adjacent cell element
//
//    VectorArrayQ<Ti> FL = 0.0, FI = 0.0;
//    integrandFcn->pde_.fluxAdvective( paramL, qL, FL );
//    integrandFcn->pde_.fluxAdvective( paramL, qI, FI );
//
//    const ArrayQ<Ti> FnL = dot(nL,FL);
//
//    rsdPDEElemL = 0.0;
//    for (int k = 0; k < integrandFcn->nDOFElem_; k++)
//      rsdPDEElemL[k] += FnL*integrandFcn->phi_[k]*dJ;
//
//    const ArrayQ<Ti> FnI = dot(nL,FI);
//    const ArrayQ<Ti> Ftmp = dJ*(FnI - FnL);
//
//    rsdINTElemTrace = 0.0;
//    for (int k = 0; k < integrandFcn->nDOFTrace_; k++)
//      rsdINTElemTrace[k] += Ftmp*integrandFcn->phiI_[k];
//  }
};

} // namespace IntegrandBoundaryTrace_Hubtrace_HDGAdv_detail
#endif

} // namespace SANS

#endif /* SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_HUBTRACE_HDGADVECTIVE_H_ */
