// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDBOUNDARYTRACE_HDG_AUXILIARYADJOINT_H
#define SETFIELDBOUNDARYTRACE_HDG_AUXILIARYADJOINT_H

// HDG trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateCellGroups.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary trace group integral without lagrange multipliers
//

template<class Surreal, class IntegrandBTrace>
class SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint_impl :
public GroupIntegralBoundaryTraceType< SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint_impl<Surreal, IntegrandBTrace> >
{
public:
  typedef typename IntegrandBTrace::PhysDim PhysDim;
  typedef typename IntegrandBTrace::PDE PDE;

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,1,ArrayQ> RowArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  // Save off the boundary trace integrand and the residual vectors
  SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint_impl( const IntegrandBTrace& fcnBTrace,
                                                   const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell ) :
                                                     fcnBTrace_(fcnBTrace), invJacAUX_a_bcell_(invJacAUX_a_bcell) {}

  std::size_t nBoundaryGroups() const { return fcnBTrace_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnBTrace_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the matrices
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qfld
                                                   Field<PhysDim, TopoDim, VectorArrayQ>, //afld
                                                   Field<PhysDim, TopoDim, ArrayQ>,       //wfld
                                                   Field<PhysDim, TopoDim, VectorArrayQ>  //bfld
                                      >::type & flds,
              const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qIfld
                                                   Field<PhysDim, TopoDim, ArrayQ>        //wIfld
                                      >::type & fldsTrace ) const {}

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qfld
                                           Field<PhysDim, TopoDim, VectorArrayQ>, //afld
                                           Field<PhysDim, TopoDim, ArrayQ>,       //wfld
                                           Field<PhysDim, TopoDim, VectorArrayQ>  //bfld
                              >::type::template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qIfld
                                           Field<PhysDim, TopoDim, ArrayQ>        //wIfld
                              >::type::template FieldTraceGroupType<TopologyTrace>& fldsTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldSurrealClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<>        ElementWFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<>        ElementBFieldClassL;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<>        ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<>        ElementWFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);
    const QFieldCellGroupTypeL& wfldCellL = get<2>(fldsCellL);
          AFieldCellGroupTypeL& bfldCellL = const_cast<AFieldCellGroupTypeL&>(get<3>(fldsCellL));

    const QFieldTraceGroupType& qIfldTrace = get<0>(fldsTrace);
    const QFieldTraceGroupType& wIfldTrace = get<1>(fldsTrace);

    // element field variables
    ElementXFieldClassL        xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldSurrealClassL afldElemL( afldCellL.basis() );
    ElementWFieldClassL        wfldElemL( wfldCellL.basis() );
    ElementBFieldClassL        bfldElemL( bfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );
    ElementWFieldTraceClass wIfldElemTrace( wIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    const int nIntegrandL = nDOFL;
    const int nIntegrandI = nDOFI;

    // variables/equations per DOF
    const int nEqn = IntegrandBTrace::N;

    // element integral
    typedef ArrayQSurreal CellIntegrandType;
    typedef ArrayQSurreal TraceIntegrandType;

    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, TraceIntegrandType>
      integral(quadratureorder, nDOFL, nDOFI);

    // element integrand/residual
    std::vector<CellIntegrandType> rsdElemPDEL( nIntegrandL );
    std::vector<TraceIntegrandType> rsdElemINT( nIntegrandI );

    // element jacobian matrices - transposed
    MatrixAUXElemClass mtxTElemPDEL_aL(nDOFL, nDOFL);
    MatrixAUXElemClass mtxTElemINT_aL(nDOFL, nDOFI);

    // temporary storage for matrix-vector multiplication
    DLA::VectorD<VectorArrayQ> tmpL( nDOFL );

    // Provide a vector of the primal adjoint
    DLA::VectorD<RowArrayQ> wL( nDOFL );
    DLA::VectorD<RowArrayQ> wI( nDOFI );

    // Provide a vector view of the auxiliary adjoint variable DOFs
    DLA::VectorDView<VectorArrayQ> bL( bfldElemL.vectorViewDOF() );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // zero element Jacobians
      mtxTElemPDEL_aL = 0;
      mtxTElemINT_aL = 0;

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );
      wfldCellL.getElement( wfldElemL, elemL );
      bfldCellL.getElement( bfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );
      wIfldTrace.getElement( wIfldElemTrace, elem );

      // loop over derivative chunks for derivatives wrt aL
      for (int nchunk = 0; nchunk < nEqn*(D*nDOFL); nchunk += nDeriv)
      {
        // associate derivative slots
        int slot, slotOffset = 0;

        // wrt aL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }
        slotOffset += nEqn*D*nDOFL;

        //------------------------------------------------------------------------------------------------

        for (int n = 0; n < nIntegrandL; n++) rsdElemPDEL[n] = 0;
        for (int n = 0; n < nIntegrandI; n++) rsdElemINT[n] = 0;

        // trace integration
        integral( fcnBTrace_.integrand(xfldElemTrace, canonicalTraceL,
                                       xfldElemL, qfldElemL, afldElemL, qIfldElemTrace),
                  xfldElemTrace,
                  rsdElemPDEL.data(), nIntegrandL,
                  rsdElemINT.data(), nIntegrandI);

        //------------------------------------------------------------------------------------------------

        // accumulate derivatives into element jacobians
        slotOffset = 0;

        // wrt aL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 0; //unset derivative

                //PDEL_aL
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    // Fill in as transposed (j,i)[d](n,m)
                    DLA::index(mtxTElemPDEL_aL(j,i)[d],n,m) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);
                  }

                //INT_aL
                for (int i = 0; i < nDOFI; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    // Fill in as transposed (j,i)[d](n,m)
                    DLA::index(mtxTElemINT_aL(j,i)[d],n,m) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
                  }
              }
            } //n loop
          } //d loop
        } //j loop
        slotOffset += nEqn*D*nDOFL;

      } // nchunk loop

      // Copy over adjoints for matrix-vector multiplication
      for (int i = 0; i < nDOFL; i++) wL[i] = wfldElemL.DOF(i);
      for (int i = 0; i < nDOFI; i++) wI[i] = wIfldElemTrace.DOF(i);

      // Accumulate the second and third components of the auxiliary variable adjoint
      // b = [dAUX/da]^-T * ([dJ/da]^T - [dPDE/da]^T * w - [dINT/da]^T * wI)
      tmpL = mtxTElemPDEL_aL*wL + mtxTElemINT_aL*wI;

      bfldCellL.getElement( bfldElemL, elemL );
      bL -= Transpose(invJacAUX_a_bcell_.getCell(cellGroupGlobalL, elemL)) * tmpL;
      bfldCellL.setElement( bfldElemL, elemL );
    }
  }

protected:
  const IntegrandBTrace& fcnBTrace_;
  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell_;
};

// Factory function

template<class Surreal, class IntegrandBTrace, class TensorMatrixQ>
SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint_impl<Surreal, IntegrandBTrace>
SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint( const IntegrandBoundaryTraceType<IntegrandBTrace>& fcnBTrace,
                                            const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell )
{
  return { fcnBTrace.cast(), invJacAUX_a_bcell };
}

}

#endif  // SETFIELDBOUNDARYTRACE_HDG_AUXILIARYADJOINT_H
