// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDFUNCTOR_HDG_PTC_H
#define INTEGRANDFUNCTOR_HDG_PTC_H

// cell/trace integrand operators: HDG

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "Discretization/HDG/DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: HDG PTC
//
// integrandPDE = phi*sum( w_n * U_n)/dt
// integrandAux = (K phi)*(qx - ux)
// integrandAuy = (K phi)*(qy - uy)
//
// where
//   phi              basis function
//   U_n              solution at timestep n
//   w_n              backwards difference weight for timestep n

template <class PDE>
class IntegrandCell_HDG_PTC
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_HDG_PTC( const PDE& pde, const DiscretizationHDG<PDE>& disc)
    : pde_(pde), disc_(disc), dt_(0.0) {}

  void setTimeStep(Real dt)
  {
    dt_ = dt;
  }

  template<class T, class TopoDim, class Topology>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef typename ElementXFieldType::RefCoordType RefCoordType;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef std::vector <ElementQFieldType> ElementQFieldVecType;
    typedef Element<VectorArrayQ<T>, TopoDim, Topology> ElementAFieldType;
    typedef IntegrandHDG< ArrayQ<T>, VectorArrayQ<T> > IntegrandType;

    BasisWeighted( const PDE& pde,
                   const DiscretizationHDG<PDE>& disc,
                   const Real dt,
                   const ElementXFieldType& xfldElem,
                   const ElementQFieldType& qfldElem) :
                     pde_(pde), disc_(disc), dt_(dt),
                     xfldElem_(xfldElem), qfldElem_(qfldElem) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qfldElem_.nDOF(); }

    // Cell integrand
    void operator()( const RefCoordType& sRef, IntegrandType integrand[], int neqn ) const;

  protected:
    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const Real dt_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
  };

  template<class T, class TopoDim, class Topology>
  BasisWeighted<T, TopoDim, Topology> integrand(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                                          const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem) const
  {
    return BasisWeighted<T, TopoDim, Topology>(pde_, disc_, dt_, xfldElem, qfldElem);
  }

protected:
  const PDE& pde_;
  const DiscretizationHDG<PDE>& disc_;
  Real dt_;
  //weights and solution vector start at current timestep and go backwards
  //i.e. weights_[0] is the weight for timestep n, weights_[1] is the weight for timestep n-1, etc...
};


template <class PDE>
template <class T, class TopoDim, class Topology>
bool
IntegrandCell_HDG_PTC<PDE>::BasisWeighted<T,TopoDim,Topology>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

// Cell integrand
//
// integrand = phi*sum(w_n*U_n)/dt
//
// where
//   phi                basis function
//   U_n(x,y)           Solution Vector at timestep n
//   w_n                BDF weight for timestep n
//
template <class PDE>
template <class T, class TopoDim, class Topology>
void
IntegrandCell_HDG_PTC<PDE>::BasisWeighted<T,TopoDim,Topology>::operator()(
    const RefCoordType& sRef, IntegrandType integrand[], int neqn ) const
{
  const int nDOF = qfldElem_.nDOF();          // total solution DOFs in element

  SANS_ASSERT( neqn == nDOF );

  Real phi[nDOF];             // basis

  ArrayQ<T> qtemp, dudt = 0;                    // solution, time derivative
  ArrayQ<T> utemp;

  // evaluate basis fcn
  qfldElem_.evalBasis( sRef, phi, nDOF );

  // BDF term
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  if (pde_.hasFluxAdvectiveTime())
  {
    utemp = 0;
    // evaluate solution
    qfldElem_.evalFromBasis( phi, nDOF, qtemp);
    pde_.fluxAdvectiveTime(qtemp, utemp);
    dudt += 1.0*utemp/dt_;

    for (int k=0; k < nDOF; k++)
      integrand[k].PDE += phi[k]*dudt;
  }
}

}

#endif  // INTEGRANDFUNCTOR_HDG_PTC_H
