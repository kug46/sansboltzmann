// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_DISPATCH_HDGADVECTIVE_PARAM_H_
#define SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_DISPATCH_HDGADVECTIVE_PARAM_H_

#include "Discretization/IntegrateBoundaryTraceGroups.h"
#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups_HubTrace.h"

#include "JacobianBoundaryTrace_HDGAdvective_Param.h"
#include "JacobianBoundaryTrace_FieldTrace_HDGAdvective_Param.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
class JacobianBoundaryTrace_mitLG_Dispatch_HDGAdvective_Param_impl
{
public:
  JacobianBoundaryTrace_mitLG_Dispatch_HDGAdvective_Param_impl(
    const XFieldType& xfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
    const int* quadratureorder, int ngroup,
    SparseMatrix& mtxGlobalPDE_p,
    SparseMatrix& mtxGlobalINT_p,
    SparseMatrix& mtxGlobalBC_p ) :
      xfld_(xfld), qfld_(qfld), qIfld_(qIfld), lgfld_(lgfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_p_(mtxGlobalPDE_p),
      mtxGlobalINT_p_(mtxGlobalINT_p),
      mtxGlobalBC_p_(mtxGlobalBC_p) {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_mitLG_HDGAdvective_Param<Surreal,iParam>(fcn, mtxGlobalPDE_p_, mtxGlobalINT_p_, mtxGlobalBC_p_),
        xfld_, qfld_, (qIfld_, lgfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  SparseMatrix& mtxGlobalPDE_p_;
  SparseMatrix& mtxGlobalINT_p_;
  SparseMatrix& mtxGlobalBC_p_;
};

// Factory function

template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
JacobianBoundaryTrace_mitLG_Dispatch_HDGAdvective_Param_impl<Surreal, iParam, XFieldType, PhysDim, TopoDim, ArrayQ, SparseMatrix>
JacobianBoundaryTrace_mitLG_Dispatch_HDGAdvective_Param(
  const XFieldType& xfld,
  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
  const int* quadratureorder, int ngroup,
  SparseMatrix& mtxGlobalPDE_p,
  SparseMatrix& mtxGlobalINT_p,
  SparseMatrix& mtxGlobalBC_p )
{
  return {xfld, qfld, qIfld, lgfld, quadratureorder, ngroup, mtxGlobalPDE_p, mtxGlobalINT_p, mtxGlobalBC_p};
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
class JacobianBoundaryTrace_sansLG_Dispatch_HDGAdvective_Param_impl
{
public:
  JacobianBoundaryTrace_sansLG_Dispatch_HDGAdvective_Param_impl(
    const XFieldType& xfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const int* quadratureorder, int ngroup,
    SparseMatrix& mtxGlobalPDE_p,
    SparseMatrix& mtxGlobalINT_p ) :
      xfld_(xfld), qfld_(qfld), qIfld_(qIfld),
      quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_p_(mtxGlobalPDE_p), mtxGlobalINT_p_(mtxGlobalINT_p) {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianBoundaryTrace_HDGAdvective_Param<Surreal,iParam>(fcn, mtxGlobalPDE_p_, mtxGlobalINT_p_),
        xfld_, qfld_, qIfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const int* quadratureorder_;
  const int ngroup_;
  SparseMatrix& mtxGlobalPDE_p_;
  SparseMatrix& mtxGlobalINT_p_;
};

// Factory function

template<class Surreal, int iParam, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
JacobianBoundaryTrace_sansLG_Dispatch_HDGAdvective_Param_impl<Surreal, iParam, XFieldType, PhysDim, TopoDim, ArrayQ, SparseMatrix>
JacobianBoundaryTrace_sansLG_Dispatch_HDGAdvective_Param(
  const XFieldType& xfld,
  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
  const int* quadratureorder, int ngroup,
  SparseMatrix& mtxGlobalPDE_p,
  SparseMatrix& mtxGlobalINT_p)
{
  return {xfld, qfld, qIfld, quadratureorder, ngroup, mtxGlobalPDE_p, mtxGlobalINT_p};
}


}

#endif /* SRC_DISCRETIZATION_HDG_JACOBIANBOUNDARYTRACE_DISPATCH_HDGADVECTIVE_PARAM_H_ */
