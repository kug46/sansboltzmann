// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDCELL_HDG_OUTPUT
#define INTEGRANDCELL_HDG_OUTPUT

#include <vector>

#include "Field/Element/Element.h"

#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "Discretization/Integrand_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand for generic output functional

template <class OutputFunctional>
class IntegrandCell_HDG_Output :
    public IntegrandCellType< IntegrandCell_HDG_Output<OutputFunctional> >
{
public:
  typedef typename OutputFunctional::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename OutputFunctional::template ArrayQ<T>;

  template <class T>
  using VectorArrayQ = typename OutputFunctional::template VectorArrayQ<T>; // solution gradient arrays

  // Array of output functionals
  template<class T>
  using ArrayJ = typename OutputFunctional::template ArrayJ<T>;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = typename OutputFunctional::template MatrixJ<T>;

  // cppcheck-suppress noExplicitConstructor
  IntegrandCell_HDG_Output( const OutputFunctional& outputFcn, const std::vector<int>& cellGroups ) :
    outputFcn_(outputFcn), cellGroups_(cellGroups)
  {
    // Nothing
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

  template<class T, class TopoDim, class Topology, class ElementParam>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim        , TopoDim, Topology> ElementXFieldType;
    typedef Element      <ArrayQ<T>      , TopoDim, Topology> ElementQFieldType;
    typedef Element      <VectorArrayQ<T>, TopoDim, Topology> ElementAFieldType;

    typedef typename ElementXFieldType::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    static const int nTrace = Topology::NTrace;

    Functor( const OutputFunctional& outputFcn,
             const ElementParam& paramfldElem,
             const ElementQFieldType& qfldElem,
             const ElementAFieldType& afldElem) :
      outputFcn_(outputFcn),
      paramfldElem_(paramfldElem),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
      qfldElem_(qfldElem),
      afldElem_(afldElem),
      nDOF_( qfldElem_.nDOF() )
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      phi_.resize(nDOF_,0);
    }

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // element output integrand
    void operator()( const QuadPointType& sRef, ArrayJ<T>& integrand ) const
    {
      ParamT param;    // Elemental parameters (such as grid coordinates and distance functions)

      ArrayQ<T> q;          // solution
      VectorArrayQ<T> a;    // auxilliary variable (gradient)

      const bool needsSolutionGradient = outputFcn_.needsSolutionGradient();

      // Elemental parameters
      paramfldElem_.eval( sRef, param );

      // basis value, gradient
      qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

      // solution value, gradient
      qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
      if (needsSolutionGradient)
        afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

      outputFcn_(param, q, a, integrand);
    }

  protected:
    const OutputFunctional& outputFcn_;
    const ElementParam& paramfldElem_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementAFieldType& afldElem_;  // lifting operators, one per edge;
//    const DiscretizationHDG& disc_;

    const int nDOF_;
    mutable std::vector<Real> phi_;
  };

  template<class T, class TopoDim, class Topology, class ElementParam>
  Functor<T,TopoDim,Topology,ElementParam>
  integrand(const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
            const Element<VectorArrayQ<T>, TopoDim, Topology>& afldElem) const
  {
    return Functor<T,TopoDim,Topology,ElementParam>(outputFcn_,
                                                    paramfldElem,
                                                    qfldElem,
                                                    afldElem);
  }

private:
  const OutputFunctional& outputFcn_;
  const std::vector<int> cellGroups_;
};

}

#endif //INTEGRANDCELL_HDG_OUTPUT
