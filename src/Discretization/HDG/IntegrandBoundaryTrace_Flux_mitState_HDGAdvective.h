// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_HDGADVECTIVE_H_

#include <ostream>
#include <vector>
#include  <type_traits> //std::is_same

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#ifdef DEBUG
  #include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
  #include "LinearAlgebra/DenseLinAlg/tools/index.h"
#endif

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{

// forward declaration
class BCTypeReflect_mitState;

//----------------------------------------------------------------------------//
// HDG element boundary integrand: State

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, HDGAdv> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, HDGAdv> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::Flux_mitState Category;
  typedef HDGAdv DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal;

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(
    const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const DiscretizationHDG<PDE>& disc) :
    pde_(pde), bc_(bc),  BoundaryGroups_(BoundaryGroups), disc_(disc) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  virtual ~IntegrandBoundaryTrace() {}

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementQFieldTrace& qIfldTrace ) :
      pde_(pde), disc_(disc), bc_(bc),
      xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
      qfldElem_(qfldElem), qIfldTrace_(qIfldTrace),
      paramfldElem_( paramfldElem ),
      nDOFL_(qfldElem_.nDOF()),
      nDOFT_(qIfldTrace_.nDOF()),
      phi_(nDOFL_),
      phiT_(nDOFT_) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFL_; }
    int nDOFTrace() const { return nDOFT_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, dJ, sRefTrace, rsdPDEElemL, rsdINTElemTrace );
    }

    FRIEND_CALL_WITH_DERIVED

  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const Real dJ, const QuadPointTraceType& RefTrace,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFL_; // number of DOFs in left element
    const int nDOFT_; // number of DOFs at trace
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> phiT_;
  };


  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>, TopoDimCell, Topology >& qfldElem,
            const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& qIfldTrace) const
  {
    return {pde_, disc_, bc_,
            xfldElemTrace, canonicalTrace,
            paramfldElem, qfldElem, qIfldTrace};
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const DiscretizationHDG<PDE>& disc_;
};


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam >
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::Flux_mitState>, HDGAdv>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const Real dJ, const QuadPointTraceType& sRefTrace,
           DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const
{
  SANS_ASSERT_MSG( rsdPDEElemL.m() == nDOFL_, "neqnL = %d  nDOFL = %d", rsdPDEElemL.m(), nDOFL_ );
  SANS_ASSERT_MSG( rsdINTElemTrace.m() == nDOFT_, "neqnTrace = %d  nDOFI = %d", rsdINTElemTrace.m(), nDOFT_ );

  SANS_ASSERT_MSG( !(std::is_same<typename BC::BCType, BCTypeReflect_mitState>::value), "Not tested for reflection BC yet" );

  // reference-element coordinate as in left element
  QuadPointCellType sRef; // s (s,t)
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  ParamT paramI; // Elemental parameters (such as grid coordinates and distance functions)
  paramfldElem_.eval( sRef, paramI );

  // unit normal: points out of domain
  VectorX nL; // unit normal (points out of left element)
  xfldElemTrace_.unitNormal( sRefTrace, nL );

  // interior and interface solution bases
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );
  qIfldTrace_.evalBasis( sRefTrace, phiT_.data(), phiT_.size() );

  // interior and interface solution
  ArrayQ<T> qL, qI;
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), qL );
  qIfldTrace_.evalFromBasis( phiT_.data(), phiT_.size(), qI );

  // PDE residual: weak form boundary integral, cell to interface state
  ArrayQ<Ti> FnL = 0.0;

  // advective flux
  if (pde_.hasFluxAdvective() )
    pde_.fluxAdvectiveUpwind( paramI, qL, qI, nL, FnL );

  if (pde_.hasFluxViscous() )
    SANS_DEVELOPER_EXCEPTION("Assume there is no viscous flux");

  // HDG stabilization
//  MatrixQ<T> tauL = 0;
//  Real lengthL = xfldElem_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
//  disc_.evalStabilization( paramI, nL, lengthL, qI, tauL );
//
//  // HDG stabilization term: tauL*(q - qI)
//  FnL += tauL*(qL - qI);

  // PDE residual associated with left element
  rsdPDEElemL = 0.0;
  for (int k = 0; k < nDOFL_; k++)
    rsdPDEElemL[k] += FnL*phi_[k]*dJ;

  // Interface residual

  // get the boundary state
  ArrayQ<T> qB = 0.0;
  bc.state( paramI, nL, qI, qB );

  // normal flux
  const VectorArrayQ<T> aL = 0.0;   // TODO: dummy
  ArrayQ<Ti> FnB = 0.0;

  bc.fluxNormal( paramI, nL, qI, aL, qB, FnB );

  rsdINTElemTrace = 0.0;
  for (int k = 0; k < nDOFT_; k++)
    rsdINTElemTrace[k] = (FnL - FnB)*phiT_[k]*dJ;
}

}

#endif /* SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_FLUX_MITSTATE_HDGADVECTIVE_H_ */
