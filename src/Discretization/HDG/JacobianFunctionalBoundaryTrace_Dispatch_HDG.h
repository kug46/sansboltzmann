// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_HDG_H
#define JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_HDG_H

// boundary-trace integral residual functions

#include "Field/XField_CellToTrace.h"

//#include "Discretization/DG/JacobianFunctionalBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/JacobianFunctionalBoundaryTrace_HDG_sansLG.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
// #include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with field trace (e.g. with Lagrange multipliers)
//
//---------------------------------------------------------------------------//
template< class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
          class ArrayQ, class VectorArrayQ, template<class> class Vector, class MatrixJ >
class JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_HDG_impl
{
public:
  JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_HDG_impl(
      const FunctionalIntegrand& fcnOutput,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
      const int* quadratureorder, int ngroup,
      Vector< MatrixJ >& func_qL,
      Vector< MatrixJ >& func_qI )
  : fcnOutput_(fcnOutput), xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld), lgfld_(lgfld), bfld_(bfld),
    quadratureorder_(quadratureorder), ngroup_(ngroup),
    func_qL_(func_qL), func_qI_(func_qI)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Functionals with field trace not implemented yet.");
#if 0
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianFunctionalBoundaryTrace_FieldTrace_HDG(fcnOutput_, fcn.cast(), func_qL_ , func_qI_ ),
        xfld_, (qfld_, afld_, bfld_), (qIfld_, lgfld_), quadratureorder_, ngroup_ );
#endif
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& bfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector< MatrixJ >& func_qL_;
  Vector< MatrixJ >& func_qI_;
};

// Factory function

template<class Surreal, class FunctionalIntegrand, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_HDG_impl< Surreal, FunctionalIntegrand, XFieldType, PhysDim, TopoDim,
                                                              ArrayQ, VectorArrayQ, Vector, MatrixJ>
JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_HDG( const FunctionalIntegrand& fcnOutput,
                                                         const XFieldType& xfld,
                                                         const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                         const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                                         const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                                         const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                         const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
                                                         const int* quadratureorder, int ngroup,
                                                         Vector< MatrixJ >& func_qL,
                                                         Vector< MatrixJ >& func_qI)
{
  return JacobianFunctionalBoundaryTrace_FieldTrace_Dispatch_HDG_impl<Surreal, FunctionalIntegrand, XFieldType, PhysDim, TopoDim,
                                                                      ArrayQ, VectorArrayQ, Vector, MatrixJ>(
      fcnOutput, xfld, qfld, afld, qIfld, lgfld, bfld, quadratureorder, ngroup, func_qL, func_qI);
}

//---------------------------------------------------------------------------//
//
// Dispatch class for outputs without field trace
//
//---------------------------------------------------------------------------//
template<class Surreal, class FunctionalIntegrand, class IntegrandCell, class IntegrandITrace,
         class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class TensorMatrixQ, class VectorMatrixQ,
         template<class> class Vector, class MatrixJ>
class JacobianFunctionalBoundaryTrace_Dispatch_HDG_impl
{
public:
  JacobianFunctionalBoundaryTrace_Dispatch_HDG_impl(
      const FunctionalIntegrand& fcnOutput,
      const IntegrandCell& fcnCell,
      const IntegrandITrace& fcnITrace,
      const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
      const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
      const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
      const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
      const XField_CellToTrace<PhysDim, TopoDim>& xfldCellToTrace,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
      const int quadOrderCell[], const int nCellGroup,
      const int quadOrderITrace[], const int nITraceGroup,
      const int quadOrderBTrace[], const int nBTraceGroup,
      Vector< MatrixJ >& func_qL,
      Vector< MatrixJ >& func_qI ) :
        fcnOutput_(fcnOutput), fcnCell_(fcnCell), fcnITrace_(fcnITrace),
        invJacAUX_a_bcell_(invJacAUX_a_bcell), jacAUX_q_bcell_(jacAUX_q_bcell), jacAUX_qI_btrace_(jacAUX_qI_btrace),
        mapDOF_boundary_qI_(mapDOF_boundary_qI), xfldCellToTrace_(xfldCellToTrace),
        xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld), bfld_(bfld),
        quadOrderCell_(quadOrderCell), nCellGroup_(nCellGroup),
        quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup),
        quadOrderBTrace_(quadOrderBTrace), nBTraceGroup_(nBTraceGroup),
        func_qL_(func_qL), func_qI_(func_qI) {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcnBTrace)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoD2>::integrate(
        JacobianFunctionalBoundaryTrace_HDG_sansLG<Surreal>( fcnOutput_, fcnBTrace, fcnCell_, fcnITrace_,
                                                             invJacAUX_a_bcell_, jacAUX_q_bcell_, jacAUX_qI_btrace_,
                                                             mapDOF_boundary_qI_, xfldCellToTrace_,
                                                             xfld_, qfld_, afld_, qIfld_,
                                                             quadOrderCell_, nCellGroup_,
                                                             quadOrderITrace_, nITraceGroup_,
                                                             func_qL_, func_qI_ ),
        xfld_, (qfld_, afld_, bfld_), qIfld_, quadOrderBTrace_, nBTraceGroup_ );
  }

protected:
  const FunctionalIntegrand& fcnOutput_;
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell_;
  const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell_;
  const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace_;
  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI_;
  const XField_CellToTrace<PhysDim, TopoDim>& xfldCellToTrace_;
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& bfld_;

  const int *quadOrderCell_, nCellGroup_;
  const int *quadOrderITrace_, nITraceGroup_;
  const int *quadOrderBTrace_, nBTraceGroup_;

  Vector< MatrixJ >& func_qL_;
  Vector< MatrixJ >& func_qI_;
};

// Factory function

template<class Surreal, class FunctionalIntegrand, class IntegrandCell, class IntegrandITrace,
         class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class TensorMatrixQ, class VectorMatrixQ,
         template<class> class Vector, class MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_HDG_impl<Surreal, FunctionalIntegrand, IntegrandCell, IntegrandITrace,
                                                  XFieldType, PhysDim, TopoDim,
                                                  ArrayQ, VectorArrayQ, TensorMatrixQ, VectorMatrixQ, Vector, MatrixJ>
JacobianFunctionalBoundaryTrace_Dispatch_HDG(const FunctionalIntegrand& fcnOutput,
                                             const IntegrandCellType<IntegrandCell>& fcnCell,
                                             const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                                             const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                                             const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                                             const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                                             const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                                             const XField_CellToTrace<PhysDim, TopoDim>& xfldCellToTrace,
                                             const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                             const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
                                             const int quadOrderCell[], const int nCellGroup,
                                             const int quadOrderITrace[], const int nITraceGroup,
                                             const int quadOrderBTrace[], const int nBTraceGroup,
                                             Vector< MatrixJ >& func_qL,
                                             Vector< MatrixJ >& func_qI )
{
  return JacobianFunctionalBoundaryTrace_Dispatch_HDG_impl< Surreal, FunctionalIntegrand, IntegrandCell, IntegrandITrace,
                                                            XFieldType, PhysDim, TopoDim,
                                                            ArrayQ, VectorArrayQ, TensorMatrixQ, VectorMatrixQ, Vector, MatrixJ>(
      fcnOutput, fcnCell.cast(), fcnITrace.cast(),
      invJacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace,
      mapDOF_boundary_qI, xfldCellToTrace,
      xfld, qfld, afld, qIfld, bfld,
      quadOrderCell, nCellGroup,
      quadOrderITrace, nITraceGroup,
      quadOrderBTrace, nBTraceGroup,
      func_qL, func_qI);
}


}

#endif //JACOBIANFUNCTIONALBOUNDARYTRACE_DISPATCH_HDG_H
