// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_DISPATCH_GALERKINEDG_H
#define RESIDUALBOUNDARYTRACE_DISPATCH_GALERKINEDG_H

// boundary-trace integral residual functions

#include "ResidualBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_GalerkinEDG.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ>
class ResidualBoundaryTrace_FieldTrace_Dispatch_GalerkinEDG_impl
{
public:
  ResidualBoundaryTrace_FieldTrace_Dispatch_GalerkinEDG_impl( const XFieldType& xfld,
                                                      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                                      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                      const int* quadratureorder, int ngroup,
                                                      Vector<ArrayQ>& rsdPDEGlobal,
                                                      Vector<ArrayQ>& rsdINTGlobal,
                                                      Vector<ArrayQ>& rsdBCGlobal ) :
    xfld_(xfld), qfld_(qfld), qIfld_(qIfld), lgfld_(lgfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    rsdPDEGlobal_(rsdPDEGlobal),
    rsdINTGlobal_(rsdINTGlobal),
    rsdBCGlobal_(rsdBCGlobal)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        ResidualBoundaryTrace_FieldTrace_HDG(fcn, rsdPDEGlobal_, rsdINTGlobal_, rsdBCGlobal_),
        xfld_, qfld_, (qIfld_, lgfld_), quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdINTGlobal_;
  Vector<ArrayQ>& rsdBCGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_FieldTrace_Dispatch_GalerkinEDG_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ>
ResidualBoundaryTrace_FieldTrace_Dispatch_GalerkinEDG( const XFieldType& xfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                               const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                               const int* quadratureorder, int ngroup,
                                               Vector<ArrayQ>& rsdPDEGlobal,
                                               Vector<ArrayQ>& rsdINTGlobal,
                                               Vector<ArrayQ>& rsdBCGlobal)
{
  return ResidualBoundaryTrace_FieldTrace_Dispatch_GalerkinEDG_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ>(
      xfld, qfld, qIfld, lgfld, quadratureorder, ngroup, rsdPDEGlobal, rsdINTGlobal, rsdBCGlobal);
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ>
class ResidualBoundaryTrace_Dispatch_GalerkinEDG_impl
{
public:
  ResidualBoundaryTrace_Dispatch_GalerkinEDG_impl(const XFieldType& xfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                          const int* quadratureorder, int ngroup,
                                          Vector<ArrayQ>& rsdPDEGlobal,
                                          Vector<ArrayQ>& rsdINTGlobal) :
    xfld_(xfld),
    qfld_(qfld),
    qIfld_(qIfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    rsdPDEGlobal_(rsdPDEGlobal),
    rsdINTGlobal_(rsdINTGlobal)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        ResidualBoundaryTrace_GalerkinEDG(fcn, rsdPDEGlobal_, rsdINTGlobal_),
        xfld_, qfld_, qIfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdINTGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_Dispatch_GalerkinEDG_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ>
ResidualBoundaryTrace_Dispatch_GalerkinEDG( const XFieldType& xfld,
                                    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                    const int* quadratureorder, int ngroup,
                                    Vector<ArrayQ>& rsdPDEGlobal,
                                    Vector<ArrayQ>& rsdINTGlobal)
{
  return ResidualBoundaryTrace_Dispatch_GalerkinEDG_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ>(
      xfld, qfld, qIfld, quadratureorder, ngroup, rsdPDEGlobal, rsdINTGlobal);
}

}

#endif //RESIDUALBOUNDARYTRACE_HDG_H
