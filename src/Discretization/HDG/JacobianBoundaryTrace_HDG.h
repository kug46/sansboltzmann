// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_HDG_H
#define JACOBIANBOUNDARYTRACE_HDG_H

// HDG trace integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryTrace.h"
#include "Field/CellToTraceAssociativity.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateCellGroups.h"

#include "Discretization/HDG/JacobianBoundaryCell_HDG_AuxiliaryVariable.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG boundary trace group integral without lagrange multipliers
//

template<class Surreal, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class XFieldCellToTrace, class XFieldClass, class TopoDim_>
class JacobianBoundaryTrace_HDG_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_HDG_impl<Surreal, IntegrandCell, IntegrandITrace, IntegrandBTrace,
                                                                          XFieldCellToTrace, XFieldClass, TopoDim_> >
{
public:
  typedef typename IntegrandBTrace::PhysDim PhysDim;
  typedef typename IntegrandBTrace::PDE PDE;

  static_assert( std::is_same< PhysDim, typename IntegrandCell::PhysDim >::value, "PhysDim should be the same.");
  static_assert( std::is_same< PhysDim, typename IntegrandITrace::PhysDim >::value, "PhysDim should be the same.");

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianBoundaryTrace_HDG_impl( const IntegrandCell& fcnCell,
                                  const IntegrandITrace& fcnITrace,
                                  const IntegrandBTrace& fcnBTrace,
                                  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                                  const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                                  const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                                  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                                  const XFieldCellToTrace& xfldCellToTrace,
                                  const XFieldClass& xfld,
                                  const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                                  const Field<PhysDim, TopoDim_, VectorArrayQ>& afld,
                                  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld,
                                  const int quadOrderCell[], const int nCellGroup,
                                  const int quadOrderITrace[], const int nITraceGroup,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                                  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI ) :
    fcnCell_(fcnCell), fcnITrace_(fcnITrace), fcnBTrace_(fcnBTrace),
    invJacAUX_a_bcell_(invJacAUX_a_bcell),
    jacAUX_q_bcell_(jacAUX_q_bcell), jacAUX_qI_btrace_(jacAUX_qI_btrace),
    mapDOF_boundary_qI_(mapDOF_boundary_qI), xfldCellToTrace_(xfldCellToTrace),
    xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
    quadOrderCell_(quadOrderCell), nCellGroup_(nCellGroup),
    quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_qI_(mtxGlobalPDE_qI),
    mtxGlobalINT_q_(mtxGlobalINT_q), mtxGlobalINT_qI_(mtxGlobalINT_qI),
    comm_rank_(0) {}

  std::size_t nBoundaryGroups() const { return fcnBTrace_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcnBTrace_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the matrices
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>& qfld = get<0>(flds);

    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalPDE_qI_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_qI_.n() == qIfld.nDOFpossessed() + qIfld.nDOFghost() );

    SANS_ASSERT( mtxGlobalINT_q_.m() == qIfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalINT_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost());

    SANS_ASSERT( mtxGlobalINT_qI_.m() == qIfld.nDOFpossessed() );
    SANS_ASSERT( mtxGlobalINT_qI_.n() == qIfld.nDOFpossessed() + qIfld.nDOFghost() );

    comm_rank_ = qfld.comm()->rank();
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldSurrealClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<Surreal> ElementAFieldSurrealClassL;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<>        ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const int D = PhysDim::D;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldSurrealClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldSurrealClassL afldElemL( afldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFI = qIfldElemTrace.nDOF();

    const int nIntegrandL = nDOFL;
    const int nIntegrandI = nDOFI;

    // variables/equations per DOF
    const int nEqn = IntegrandBTrace::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qL(nDOFL, -1);
    std::vector<int> mapDOFGlobal_qI(nDOFI, -1);

    std::vector<int> mapDOFLocal_qI( nDOFI, -1 );
    std::vector<int> maprsd( nDOFI, -1 );
    int nDOFLocalI = 0;
    const int nDOFpossessed = mtxGlobalINT_qI_.m();

    MatrixElemClass mtxElemLocal( nDOFI, std::max(nDOFI, nDOFL) );


    typedef ArrayQSurreal CellIntegrandType;
    typedef ArrayQSurreal TraceIntegrandType;

    // element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, CellIntegrandType, TraceIntegrandType>
      integral(quadratureorder, nDOFL, nDOFI);

    // element integrand/residual
    std::vector<CellIntegrandType> rsdElemPDEL( nIntegrandL );
    std::vector<TraceIntegrandType> rsdElemINT( nIntegrandI );

    // element auxiliary variable equation jacobian matrices (Array length D of nDOFxnDOF matrices)
    MatrixAUXElemClass mtxElemAUXL_qL(nDOFL, nDOFL);
    std::vector<MatrixAUXElemClass> mtxElemAUXL_qI(TopologyL::NTrace, MatrixAUXElemClass(0, 0));

    // element jacobian matrices
    // Left
    MatrixElemClass mtxElemPDEL_qL(nDOFL, nDOFL);
    MatrixElemClass mtxElemINT_qL(nDOFI, nDOFL);

    MatrixAElemClass mtxElemPDEL_aL(nDOFL, nDOFL);
    MatrixAElemClass mtxElemINT_aL(nDOFI, nDOFL);

    // element PDE jacobian matrices wrt qI on each trace of the "central" cell
    std::vector<MatrixElemClass> mtxElemPDEL_qI(TopologyL::NTrace, MatrixElemClass(0,0));
    std::vector<MatrixElemClass> mtxElemINT_qI (TopologyL::NTrace, MatrixElemClass(0,0));

    //storage for auxiliary variable jacobians wrt q and qI on each trace of this cell
    MatrixAUXElemClass mtx_a_q(nDOFL, nDOFL);
    std::vector<MatrixAUXElemClass> mtx_a_qI(TopologyL::NTrace, MatrixAUXElemClass(0, 0));
    std::vector<std::vector<int>> mapDOFGlobal_qI_cell(TopologyL::NTrace);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // global mapping for qI
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI.data(), nDOFI );

      // copy over the map for each qI DOF that is possessed by this processor
      nDOFLocalI = 0;
      for (int n = 0; n < nDOFI; n++)
      {
        if (mapDOFGlobal_qI[n] < nDOFpossessed)
        {
          maprsd[nDOFLocalI] = n;
          mapDOFLocal_qI[nDOFLocalI] = mapDOFGlobal_qI[n];
          nDOFLocalI++;
        }
      }

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // only possessed cell elements have PDE residuals on this processor
      if ( qfldElemL.rank() != comm_rank_ && nDOFLocalI == 0 ) continue;

      for (int trace = 0; trace < TopologyL::NTrace; trace++)
      {
        mtxElemAUXL_qI[trace].resize(0, 0);
        mtx_a_qI[trace].resize(0, 0);
        mapDOFGlobal_qI_cell[trace].clear();

        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobalL, elemL, trace);

        if (traceinfo.type == TraceInfo::Boundary)
        {
          //This trace is a boundary trace, so save its {tracegroup, elem} info
          if ( mapDOF_boundary_qI_[traceinfo.group].size() > 0 )
          {
            //Copy the globalDOFmap for qI DOFs on this boundary trace
            mapDOFGlobal_qI_cell[trace] = mapDOF_boundary_qI_[traceinfo.group][traceinfo.elem];

            //Copy the stored AUX jacobian contribution from BC
            mtxElemAUXL_qI[trace].resize(nDOFL, mapDOFGlobal_qI_cell[trace].size());
            mtxElemAUXL_qI[trace] = jacAUX_qI_btrace_.getBoundaryTraceGroupGlobal(traceinfo.group)[traceinfo.elem];
          }
        }
      }

      // check if the DOF count for this trace is consistent
      SANS_ASSERT( (int) mapDOFGlobal_qI_cell[canonicalTraceL.trace].size() == nDOFI );

      //Get the AUX jacobian from the BC
      mtxElemAUXL_qL = jacAUX_q_bcell_.getCell(cellGroupGlobalL, elemL);

      //Accumulate the auxiliary variable jacobian contributions (AUX_qI) from the interior traces of the left boundary cell
      IntegrateCellGroups<TopoDim>::integrate(
          JacobianBoundaryCell_HDG_AuxiliaryVariable<Surreal>(fcnCell_, fcnITrace_,
                                                              cellGroupGlobalL, elemL, xfldCellToTrace_,
                                                              xfld_, qfld_, afld_, qIfld_,
                                                              quadOrderITrace_, nITraceGroup_,
                                                              mtxElemAUXL_qL, mtxElemAUXL_qI, mapDOFGlobal_qI_cell),
          xfld_, (qfld_, afld_), quadOrderCell_, nCellGroup_ );

      //Compute the auxiliary variable jacobians
      mtx_a_q = -invJacAUX_a_bcell_.getCell(cellGroupGlobalL, elemL) * mtxElemAUXL_qL;

      for (int trace = 0; trace < (int) TopologyL::NTrace; trace++)
      {
        if (mapDOFGlobal_qI_cell[trace].size() > 0)
        {
          mtx_a_qI[trace].resize(nDOFL, mapDOFGlobal_qI_cell[trace].size());
          mtx_a_qI[trace] = -invJacAUX_a_bcell_.getCell(cellGroupGlobalL, elemL) * mtxElemAUXL_qI[trace];
        }
      }

      //---------------------------------------------------------------------------------------------

      //Now we can compute the PDE and INT jacobians from this boundary trace, and complete their chain rules.

      // zero element Jacobians
      mtxElemPDEL_qL = 0; mtxElemPDEL_aL = 0;
      mtxElemINT_qL = 0; mtxElemINT_aL = 0;

      for (int trace = 0; trace < (int) TopologyL::NTrace; trace++)
      {
        mtxElemPDEL_qI[trace].resize( nDOFL, (int) mapDOFGlobal_qI_cell[trace].size() );
        mtxElemINT_qI [trace].resize( nDOFI, (int) mapDOFGlobal_qI_cell[trace].size() );

        mtxElemPDEL_qI[trace] = 0;
        mtxElemINT_qI [trace] = 0;
      }

      // loop over derivative chunks for derivatives wrt qL, aL and qI
      for (int nchunk = 0; nchunk < nEqn*(nDOFL + D*nDOFL + nDOFI); nchunk += nDeriv)
      {
        // associate derivative slots
        int slot, slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFL;

        // wrt aL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }
        slotOffset += nEqn*D*nDOFL;

        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nEqn*nDOFI;

        //------------------------------------------------------------------------------------------------

        for (int n = 0; n < nIntegrandL; n++) rsdElemPDEL[n] = 0;
        for (int n = 0; n < nIntegrandI; n++) rsdElemINT[n] = 0;

        // trace integration
        integral( fcnBTrace_.integrand(xfldElemTrace, canonicalTraceL,
                                       xfldElemL, qfldElemL, afldElemL, qIfldElemTrace),
                  xfldElemTrace,
                  rsdElemPDEL.data(), nIntegrandL,
                  rsdElemINT.data(), nIntegrandI);

        //------------------------------------------------------------------------------------------------

        // accumulate derivatives into element jacobians
        slotOffset = 0;

        // wrt qL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

              //PDEL_qL
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemPDEL_qL(i,j),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);

              //INT_qL
              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemINT_qL(i,j),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
            }
          } //n loop
        } //j loop
        slotOffset += nEqn*nDOFL;


        // wrt aL
        for (int j = 0; j < nDOFL; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(afldElemL.DOF(j)[d],n).deriv(slot - nchunk) = 0; //unset derivative

                //PDEL_aL
                for (int i = 0; i < nDOFL; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemPDEL_aL(i,j)(0,d),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);

                //INT_aL
                for (int i = 0; i < nDOFI; i++)
                  for (int m = 0; m < nEqn; m++)
                    DLA::index(mtxElemINT_aL(i,j)(0,d),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
              }
            } //n loop
          } //d loop
        } //j loop
        slotOffset += nEqn*D*nDOFL;


        // wrt qI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = slotOffset + nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 0;  // Reset the derivative

              //PDEL_qI
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemPDEL_qI[canonicalTraceL.trace](i,j),m,n) = DLA::index(rsdElemPDEL[i],m).deriv(slot - nchunk);

              //INT_qI
              for (int i = 0; i < nDOFI; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemINT_qI[canonicalTraceL.trace](i,j),m,n) = DLA::index(rsdElemINT[i],m).deriv(slot - nchunk);
            }
          } //n loop
        } //j loop
        slotOffset += nEqn*nDOFI;

      } // nchunk loop

      //Complete the chain rule with respect to q on the left cell
      mtxElemPDEL_qL += mtxElemPDEL_aL*mtx_a_q;
      mtxElemINT_qL  += mtxElemINT_aL *mtx_a_q;

      //Complete the chain rule with respect to qI DOFs on all traces of the left cell
      for (int trace = 0; trace < TopologyL::NTrace; trace++)
      {
        if (mapDOFGlobal_qI_cell[trace].size() > 0) //some boundary-trace groups might have been omitted
        {
          mtxElemPDEL_qI[trace] += mtxElemPDEL_aL*mtx_a_qI[trace];
          mtxElemINT_qI [trace] += mtxElemINT_aL *mtx_a_qI[trace];
        }
      }

      // scatter-add element jacobian to global
      scatterAdd( qIfldTrace, qfldCellL,
                  elem, elemL, qfldElemL.rank(),
                  mapDOFGlobal_qL.data(), nDOFL,
                  mapDOFLocal_qI.data(), nDOFI,
                  mapDOFGlobal_qI_cell,
                  maprsd.data(), nDOFLocalI,

                  mtxElemPDEL_qL, mtxElemPDEL_qI,
                  mtxElemINT_qL , mtxElemINT_qI,
                  mtxElemLocal,

                  mtxGlobalPDE_q_, mtxGlobalPDE_qI_,
                  mtxGlobalINT_q_, mtxGlobalINT_qI_);
    }
  }


//----------------------------------------------------------------------------//
  template <class QFieldTraceGroupType, class QFieldCellGroupTypeL,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldTraceGroupType& qIfldTrace,
      const QFieldCellGroupTypeL& qfldCellL,
      const int elem, const int elemL, const int elemLRank,
      int mapDOFGlobal_qL[], const int nDOFL,
      int mapDOFLocal_qI[], const int nDOFI,
      const std::vector<std::vector<int>>& mapDOFGlobal_qI_cell,
      int maprsd[], const int nDOFLocalI,

      const MatrixElemClass& mtxElemPDEL_qL,
      const std::vector<MatrixElemClass>& mtxElemPDEL_qI,
      const MatrixElemClass& mtxElemINT_qL,
      const std::vector<MatrixElemClass>& mtxElemINT_qI,
      MatrixElemClass& mtxElemLocal,

      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_qI,
      SparseMatrixType<MatrixQ>& mtxGlobalINT_q,
      SparseMatrixType<MatrixQ>& mtxGlobalINT_qI)
  {
    // global mapping for qL
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobal_qL, nDOFL );

    if (elemLRank == comm_rank_)
    {
      // jacobian wrt qL
      mtxGlobalPDE_q.scatterAdd( mtxElemPDEL_qL, mapDOFGlobal_qL, nDOFL );

      // jacobians wrt qI (including on traces of the central cell)
      for (int trace = 0; trace < (int) mapDOFGlobal_qI_cell.size(); trace++)
      {
        if (mapDOFGlobal_qI_cell[trace].size() > 0)  //some boundary-trace groups might have been omitted
        {
          mtxGlobalPDE_qI.scatterAdd( mtxElemPDEL_qI[trace], mapDOFGlobal_qL, nDOFL,
                                      mapDOFGlobal_qI_cell[trace].data(), mapDOFGlobal_qI_cell[trace].size() );
        }
      }
    }

    if (nDOFLocalI == nDOFI)
    {
      // jacobians wrt qL
      mtxGlobalINT_q.scatterAdd( mtxElemINT_qL , mapDOFLocal_qI, nDOFLocalI, mapDOFGlobal_qL, nDOFL );

      // jacobians wrt qI (including on traces of the central cell)
      for (int trace = 0; trace < (int) mapDOFGlobal_qI_cell.size(); trace++)
      {
        if (mapDOFGlobal_qI_cell[trace].size() > 0)  //some boundary-trace groups might have been omitted
        {
          mtxGlobalINT_qI.scatterAdd( mtxElemINT_qI[trace], mapDOFLocal_qI, nDOFLocalI,
                                      mapDOFGlobal_qI_cell[trace].data(), mapDOFGlobal_qI_cell[trace].size() );
        }
      }
    }
    else
    {
      // compress in only the DOFs possessed by this processor
      for (int i = 0; i < nDOFLocalI; i++)
        for (int j = 0; j < nDOFL; j++)
          mtxElemLocal(i,j) = mtxElemINT_qL(maprsd[i],j);

      // jacobians wrt qL
      mtxGlobalINT_q.scatterAdd( mtxElemLocal , mapDOFLocal_qI, nDOFLocalI, mapDOFGlobal_qL, nDOFL );

      // jacobians wrt qI (including on traces of the central cell)
      for (int trace = 0; trace < (int) mapDOFGlobal_qI_cell.size(); trace++)
      {
        if (mapDOFGlobal_qI_cell[trace].size() > 0)  //some boundary-trace groups might have been omitted
        {
          // compress in only the DOFs possessed by this processor
          for (int i = 0; i < nDOFLocalI; i++)
            for (std::size_t j = 0; j < mapDOFGlobal_qI_cell[trace].size(); j++)
              mtxElemLocal(i,j) = mtxElemINT_qI[trace](maprsd[i],j);

          mtxGlobalINT_qI.scatterAdd( mtxElemLocal, mapDOFLocal_qI, nDOFLocalI,
                                      mapDOFGlobal_qI_cell[trace].data(), mapDOFGlobal_qI_cell[trace].size() );
        }
      }
    }
  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const IntegrandBTrace& fcnBTrace_;
  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell_;
  const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell_;
  const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace_;
  const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI_;

  const XFieldCellToTrace& xfldCellToTrace_;
  const XFieldClass& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld_;

  const int *quadOrderCell_, nCellGroup_;
  const int *quadOrderITrace_, nITraceGroup_;

  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q_;
  MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI_;

  mutable int comm_rank_;
};

// Factory function

template<class Surreal, class IntegrandCell, class IntegrandITrace, class IntegrandBTrace,
         class XFieldCellToTrace, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class MatrixQ, class TensorMatrixQ, class VectorMatrixQ>
JacobianBoundaryTrace_HDG_impl<Surreal, IntegrandCell, IntegrandITrace, IntegrandBTrace,
                               XFieldCellToTrace, XFieldType, TopoDim>
JacobianBoundaryTrace_HDG( const IntegrandCellType<IntegrandCell>& fcnCell,
                           const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                           const IntegrandBoundaryTraceType<IntegrandBTrace>& fcnBTrace,
                           const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                           const FieldDataMatrixD_BoundaryCell<VectorMatrixQ>& jacAUX_q_bcell,
                           const FieldDataMatrixD_BoundaryTrace<VectorMatrixQ>& jacAUX_qI_btrace,
                           const std::vector<std::vector<std::vector<int>>>& mapDOF_boundary_qI,
                           const XFieldCellToTrace& xfldCellToTrace,
                           const XFieldType& xfld,
                           const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                           const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                           const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                           const int quadOrderCell[], const int nCellGroup,
                           const int quadOrderITrace[], const int nITraceGroup,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_q,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalPDE_qI,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalINT_q,
                           MatrixScatterAdd<MatrixQ>& mtxGlobalINT_qI)
{
  return { fcnCell.cast(), fcnITrace.cast(), fcnBTrace.cast(),
           invJacAUX_a_bcell, jacAUX_q_bcell, jacAUX_qI_btrace, mapDOF_boundary_qI,
           xfldCellToTrace, xfld, qfld, afld, qIfld,
           quadOrderCell, nCellGroup,
           quadOrderITrace, nITraceGroup,
           mtxGlobalPDE_q, mtxGlobalPDE_qI,
           mtxGlobalINT_q, mtxGlobalINT_qI };
}

}

#endif  // JACOBIANBOUNDARYTRACE_HDG_H
