// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ALGEBRAICEQUATIONSET_HDG_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "AlgebraicEquationSet_HDG.h"

#include <type_traits>

#include "Surreal/SurrealS.h"

#include "Field/XField.h"
#include "Field/Tuple/FieldTuple.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "pde/BCParameters.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Discretization/HDG/IntegrandCell_HDG.h"
#include "Discretization/HDG/IntegrandInteriorTrace_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_None_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_mitLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_LinearScalar_sansLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Dirichlet_sansLG_HDG.h"
#include "Discretization/HDG/IntegrandBoundaryTrace_Flux_mitState_HDG.h"

#include "Discretization/HDG/ResidualCell_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_Dirichlet_mitLG_HDG.h"
#include "Discretization/HDG/ResidualBoundaryTrace_HDG.h"

#include "Discretization/HDG/JacobianCell_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_Dispatch_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_Dispatch_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/JacobianBoundaryTrace_FieldTrace_HDG.h"
#include "Discretization/HDG/JacobianBoundaryTrace_HDG.h"

#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/isValidState/isValidStateInteriorTrace_HDG.h"
#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch.h"
#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch_HDG.h"

#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryVariable.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_Dispatch_AuxiliaryVariable.h"

#include "Discretization/HDG/SetFieldCell_HDG_AuxiliaryAdjoint.h"
#include "Discretization/HDG/SetFieldBoundaryTrace_Dispatch_AuxiliaryAdjoint.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//Protected constructor for AlgebraicEquationSet_Local_HDG: cellgroups for the auxiliary calculation can be different
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template< class... BCArgs >
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
AlgebraicEquationSet_HDG(const XFieldType& xfld,
                         Field_DG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                         Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& afld,
                         Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                         Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                         const NDPDEClass& pde,
                         DiscretizationHDG<NDPDEClass> &disc,
                         const QuadratureOrder& quadratureOrder,
                         const std::vector<Real>& tol,
                         const std::vector<int>& CellGroups,
                         const std::vector<int>& CellGroupsAux,
                         const std::vector<int>& InteriorTraceGroups,
                         PyDict& BCList,
                         const std::map< std::string, std::vector<int> >& BCBoundaryGroups,
                         BCArgs&... args ) :
   DebugBaseType(pde, tol),
   disc_(disc),
   fcnCell_( pde, disc_, CellGroups ),
   fcnInt_( pde, disc_, InteriorTraceGroups ),
   BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
   dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, disc_),
   xfld_(xfld),
   xfldCellToTrace_(xfld_.getXField()),
   qfld_(qfld),
   afld_(afld),
   qIfld_(qIfld),
   lgfld_(lgfld),
   mmfld_(qfld),
   jacAUX_a_bcell_(qfld),
   pde_(pde),
   quadratureOrder_(quadratureOrder), quadratureOrderMin_(get<-1>(xfld), 0),
   tol_(tol),
   CellGroupsAux_(CellGroupsAux)
{
  SANS_ASSERT(tol_.size() == 3);
  jacAUX_a_bcell_ = 0.0;

  SANS_ASSERT_MSG( isValidState(), "AlgebraicEquationSet_HDG is initialized with an invalid state");
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_HDG( fcnCell_, fcnInt_, xfldCellToTrace_,
                                                             xfld_, qfld_, afld_, qIfld_,
                                                             quadratureOrder_.interiorTraceOrders.data(),
                                                             quadratureOrder_.interiorTraceOrders.size(),
                                                             rsd(iPDE), rsd(iINT) ),
                                           xfld_, (qfld_, afld_), quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_HDG( xfld_, qfld_, afld_, qIfld_, lgfld_,
                                                     quadratureOrder_.boundaryTraceOrders.data(),
                                                     quadratureOrder_.boundaryTraceOrders.size(),
                                                     rsd(iPDE), rsd(iINT), rsd(iBC) ),
      ResidualBoundaryTrace_Dispatch_HDG( xfld_, qfld_, afld_, qIfld_,
                                          quadratureOrder_.boundaryTraceOrders.data(),
                                          quadratureOrder_.boundaryTraceOrders.size(),
                                          rsd(iPDE), rsd(iINT) ) );
}

//Fills jacobian or the non-zero pattern of a jacobian
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SystemMatrixView& mtx)
{
  this->template jacobian<SystemMatrixView&>(mtx, quadratureOrder_ );
}
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SystemNonZeroPatternView& nz)
{
  this->template jacobian<SystemNonZeroPatternView&>(nz, quadratureOrderMin_ );
}

//Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobianTranspose(SystemMatrixView& mtxT)
{
  jacobian(Transpose(mtxT), quadratureOrder_ );
}
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobianTranspose(SystemNonZeroPatternView& nzT)
{
  jacobian(Transpose( nzT), quadratureOrderMin_ );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const QuadratureOrder& quadratureOrder)
{
  SANS_ASSERT(jac.m() == 3);
  SANS_ASSERT(jac.n() == 3);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  // jacobian nonzero pattern
  //
  //        q  qI  lg
  //   PDE  X   X   X
  //   Int  X   X   0
  //   BC   X   0   0

  Matrix jacPDE_q  = jac(iPDE,iq);
  Matrix jacPDE_qI = jac(iPDE,iqI);
  Matrix jacPDE_LG = jac(iPDE,ilg);

  Matrix jacINT_q  = jac(iINT,iq);
  Matrix jacINT_qI = jac(iINT,iqI);
  Matrix jacINT_LG = jac(iINT,ilg);

  Matrix jacBC_q  = jac(iBC,iq);
  Matrix jacBC_qI = jac(iBC,iqI);
  Matrix jacBC_LG = jac(iBC,ilg);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  computeAuxiliaryVariables(false);

  FieldDataMatrixD_BoundaryCell<VectorMatrixQ> jacAUX_q_bcell(qfld_);
  FieldDataMatrixD_BoundaryTrace<VectorMatrixQ> jacAUX_qI_btrace(qfld_, qIfld_);
  jacAUX_q_bcell = 0.0;
  jacAUX_qI_btrace = 0.0;

  std::vector<std::vector<std::vector<int>>> mapDOFGlobalBoundary_qI(xfld_.getXField().nBoundaryTraceGroups());

  dispatchBC_.dispatch_HDG(
      JacobianBoundaryTrace_Dispatch_HDG_AuxiliaryVariable<SurrealClass>( xfld_, qfld_, afld_, qIfld_,
                                                                          quadratureOrder_.boundaryTraceOrders.data(),
                                                                          quadratureOrder_.boundaryTraceOrders.size(),
                                                                          jacAUX_q_bcell, jacAUX_qI_btrace, mapDOFGlobalBoundary_qI ) );

  IntegrateCellGroups<TopoDim>::integrate(
      JacobianCell_HDG<SurrealClass>( fcnCell_, fcnInt_,
                                      mmfld_, jacAUX_a_bcell_, jacAUX_q_bcell, jacAUX_qI_btrace,
                                      mapDOFGlobalBoundary_qI, xfldCellToTrace_,
                                      xfld_, qfld_, afld_, qIfld_,
                                      quadratureOrder.interiorTraceOrders.data(),
                                      quadratureOrder.interiorTraceOrders.size(),
                                      CellGroupsAux_,
                                      jacPDE_q, jacPDE_qI,
                                      jacINT_q, jacINT_qI ),
      xfld_, (qfld_, afld_), quadratureOrder.cellOrders.data(), quadratureOrder.cellOrders.size() );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_FieldTrace_Dispatch_HDG<SurrealClass>(xfld_, qfld_, afld_, qIfld_, lgfld_,
                                                                  quadratureOrder.boundaryTraceOrders.data(),
                                                                  quadratureOrder.boundaryTraceOrders.size(),
                                                                  jacPDE_q, jacPDE_qI, jacPDE_LG,
                                                                  jacINT_q, jacINT_qI, jacINT_LG,
                                                                  jacBC_q , jacBC_qI , jacBC_LG ),
      JacobianBoundaryTrace_Dispatch_HDG<SurrealClass>( fcnCell_, fcnInt_,
                                                        jacAUX_a_bcell_, jacAUX_q_bcell, jacAUX_qI_btrace,
                                                        mapDOFGlobalBoundary_qI, xfldCellToTrace_,
                                                        xfld_, qfld_, afld_, qIfld_,
                                                        quadratureOrder.cellOrders.data(),
                                                        quadratureOrder.cellOrders.size(),
                                                        quadratureOrder.interiorTraceOrders.data(),
                                                        quadratureOrder.interiorTraceOrders.size(),
                                                        quadratureOrder.boundaryTraceOrders.data(),
                                                        quadratureOrder.boundaryTraceOrders.size(),
                                                        jacPDE_q, jacPDE_qI,
                                                        jacINT_q, jacINT_qI ) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];

  const int nDOFINT = qIfld_.nDOFpossessed() + qIfld_.nDOFghost();
  SANS_ASSERT( nDOFINT == q[iqI].m() );
  for (int k = 0; k < nDOFINT; k++)
    qIfld_.DOF(k) = q[iqI][k];

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    lgfld_.DOF(k) = q[ilg][k];

  computeAuxiliaryVariables(true);
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::computeAuxiliaryVariables(const bool update)
{
  jacAUX_a_bcell_ = 0.0;

  FieldDataVectorD_BoundaryCell<VectorArrayQ> rsdAUX_bcell(qfld_);
  rsdAUX_bcell = 0.0;

  dispatchBC_.dispatch_HDG(
      SetFieldBoundaryTrace_Dispatch_AuxiliaryVariable( xfld_, qfld_, afld_, qIfld_,
                                                        quadratureOrder_.boundaryTraceOrders.data(),
                                                        quadratureOrder_.boundaryTraceOrders.size(),
                                                        rsdAUX_bcell, jacAUX_a_bcell_ ) );

  //Note: jacAUX_a_bcell_ gets completed and inverted inside SetFieldCell_HDG_AuxiliaryVariable
  IntegrateCellGroups<TopoDim>::integrate(
      SetFieldCell_HDG_AuxiliaryVariable( fcnCell_, fcnInt_, mmfld_, xfldCellToTrace_,
                                          xfld_, qfld_, afld_, qIfld_,
                                          quadratureOrder_.interiorTraceOrders.data(),
                                          quadratureOrder_.interiorTraceOrders.size(),
                                          rsdAUX_bcell, CellGroupsAux_, update, jacAUX_a_bcell_ ),
      xfld_, (qfld_, afld_), quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  afld_.syncDOFs_MPI_Cached();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
setAdjointField(const SystemVectorView& adj,
                Field_DG_Cell<PhysDim, TopoDim, ArrayQ>&       wfld,
                Field_DG_Cell<PhysDim, TopoDim, VectorArrayQ>& bfld,
                Field<PhysDim, TopoDim, ArrayQ>&               wIfld,
                Field<PhysDim, TopoDim, ArrayQ>&               mufld )
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = wfld.nDOF();
  SANS_ASSERT( nDOFPDE == adj[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    wfld.DOF(k) = adj[iq][k];

  const int nDOFINT = wIfld.nDOF();
  SANS_ASSERT( nDOFINT == adj[iqI].m() );
  for (int k = 0; k < nDOFINT; k++)
    wIfld.DOF(k) = adj[iqI][k];

  const int nDOFBC = mufld.nDOF();
  SANS_ASSERT( nDOFBC == adj[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    mufld.DOF(k) = adj[ilg][k];

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  // zero out efficiently for accumulation (bfld = 0 uses projection)
  for (int n = 0; n < bfld.nDOF(); n++)
    bfld.DOF(n) = 0;

  IntegrateCellGroups<TopoDim>::integrate(
      SetFieldCell_HDG_AuxiliaryAdjoint<SurrealClass>( fcnCell_, fcnInt_,
                                                       mmfld_, jacAUX_a_bcell_, xfldCellToTrace_,
                                                       xfld_, qfld_, afld_, qIfld_, wfld, bfld, wIfld,
                                                       quadratureOrder_.interiorTraceOrders.data(),
                                                       quadratureOrder_.interiorTraceOrders.size() ),
      xfld_, (qfld_, afld_, wfld, bfld), quadratureOrder_.cellOrders.data(), quadratureOrder_.cellOrders.size() );

  dispatchBC_.dispatch_HDG(
      SetFieldBoundaryTrace_Dispatch_AuxiliaryAdjoint<SurrealClass>( jacAUX_a_bcell_, xfld_, qfld_, afld_, qIfld_, wfld, bfld, wIfld,
                                                                     quadratureOrder_.boundaryTraceOrders.data(),
                                                                     quadratureOrder_.boundaryTraceOrders.size() ) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld_.DOF(k);

  const int nDOFInt = qIfld_.nDOFpossessed() + qIfld_.nDOFghost();
  SANS_ASSERT( nDOFInt == q[iqI].m() );
  for (int k = 0; k < nDOFInt; k++)
    q[iqI][k] = qIfld_.DOF(k);

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld_.DOF(k);
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iINT == 1,"");
  static_assert(iBC == 2,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFIntpos = qIfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  return {nDOFPDEpos, nDOFIntpos, nDOFBCpos};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorStateSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iINT == 1,"");
  static_assert(iBC == 2,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFInt = qIfld_.nDOFpossessed() + qIfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {nDOFPDE, nDOFInt, nDOFBC};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iINT == 1,"");
  static_assert(iBC == 2,"");

  static_assert(iq == 0,"");
  static_assert(iqI == 1,"");
  static_assert(ilg == 2,"");

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFIntpos = qIfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFInt = qIfld_.nDOFpossessed() + qIfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {{ {nDOFPDEpos, nDOFPDE}, {nDOFPDEpos, nDOFInt}, {nDOFPDEpos, nDOFBC} },
          { {nDOFIntpos, nDOFPDE}, {nDOFIntpos, nDOFInt}, {nDOFIntpos, nDOFBC} },
          { {nDOFBCpos , nDOFPDE}, {nDOFBCpos , nDOFInt}, {nDOFBCpos , nDOFBC} }};
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
bool
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  // Update the solution field
  setSolutionField(q);

  return isValidState();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
bool
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
isValidState()
{
  bool isValidState = true;

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                           xfld_, qfld_,
                                           quadratureOrder_.cellOrders.data(),
                                           quadratureOrder_.cellOrders.size() );

  IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate( isValidStateInteriorTrace_HDG(fcnInt_, pde_, isValidState),
                                                               xfld_, (qfld_, afld_), qIfld_,
                                                               quadratureOrder_.interiorTraceOrders.data(),
                                                               quadratureOrder_.interiorTraceOrders.size());

  //TODO: This is a mess...
  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                quadratureOrder_.boundaryTraceOrders.data(),
                                                quadratureOrder_.boundaryTraceOrders.size(), isValidState ),
      isValidStateBoundaryTrace_HDG_Dispatch( pde_, xfld_, qfld_, qIfld_,
                                              quadratureOrder_.boundaryTraceOrders.data(),
                                              quadratureOrder_.boundaryTraceOrders.size(), isValidState )
    );

  return isValidState;
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
std::vector<std::vector<Real>>
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residualNorm(const SystemVectorView& rsd) const
{
  const int nDOFPDE = rsd[iPDE].m();
  const int nDOFINT = rsd[iINT].m();
  const int nDOFBC = rsd[iBC].m();
  const int nMon = pde_.nMonitor();

  DLA::VectorD<Real> rsdPDEtmp(nMon);
  DLA::VectorD<Real> rsdINTtmp(nMon);
  DLA::VectorD<Real> rsdBCtmp(nMon);

  rsdPDEtmp = 0;
  rsdINTtmp = 0;
  rsdBCtmp = 0;

  std::vector<std::vector<Real>> rsdNorm(nResidNorm(), std::vector<Real>(nMon, 0));

  //HACKED Simple L2 norm
  //TODO: Allow for non-L2 norms

  //compute PDE residual norm
  for (int n = 0; n < nDOFPDE; n++)
  {
    pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] += pow(rsdPDEtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);

  //INT residual
  for (int n = 0; n < nDOFINT; n++)
  {
    pde_.interpResidVariable(rsd[iINT][n], rsdINTtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iINT][j] += pow(rsdINTtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iINT][j] = sqrt(rsdNorm[iINT][j]);

  //BC residual
  for (int n = 0; n < nDOFBC; n++)
  {
    pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
  }

  for (int j = 0; j < nMon; j++)
    rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

  return rsdNorm;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const
{
  titles = {"PDE : ",
            "INT : ",
            "BC  : "};
  idx = {iPDE, iINT, iBC};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
int
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
nResidNorm() const
{
  return 3;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
std::shared_ptr<mpi::communicator>
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
comm() const
{
  return qfld_.comm();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
syncDOFs_MPI()
{
  qfld_.syncDOFs_MPI_Cached();
  afld_.syncDOFs_MPI_Cached();
  lgfld_.syncDOFs_MPI_Cached();
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_HDG<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);

//  std::string filename_iBC = filenamebase + "_iBC_iter" + std::to_string(nonlinear_iter) + ".plt";
//  this->dumpLinesearchField(lgfld_, *pStepData, iBC, filename_iBC);
}

} //namespace SANS

// Helper macros to reduce the amount of work needed to instantiate AlgebraicEquationSet_HDG
#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_TRAITS(PDEND, BCNDCONVERT, BCVECTOR, TOPODIM, TRAITS, PARAMFIELDTUPLE ) \
template class AlgebraicEquationSet_HDG< PDEND, BCNDCONVERT, BCVECTOR, TRAITS, PARAMFIELDTUPLE >; \
\
template AlgebraicEquationSet_HDG< PDEND, BCNDCONVERT, BCVECTOR, TRAITS, PARAMFIELDTUPLE >:: \
         AlgebraicEquationSet_HDG(const PARAMFIELDTUPLE& xfld, \
                                  Field_DG_Cell<PDEND::PhysDim, \
                                                TOPODIM, \
                                                PDEND::ArrayQ<Real> >& qfld, \
                                  Field_DG_Cell<PDEND::PhysDim, \
                                                TOPODIM, \
                                                PDEND::VectorArrayQ<Real> >& afld, \
                                  Field<PDEND::PhysDim, \
                                        TOPODIM, \
                                        PDEND::ArrayQ<Real> >& qIfld, \
                                  Field<PDEND::PhysDim, \
                                        TOPODIM, \
                                        PDEND::ArrayQ<Real> >& lgfld, \
                                  const PDEND& pde, \
                                  DiscretizationHDG<PDEND>& disc, \
                                  const QuadratureOrder& quadratureOrder, \
                                  const std::vector<Real>& tol, \
                                  const std::vector<int>& CellGroups, \
                                  const std::vector<int>& CellGroupsAUX, \
                                  const std::vector<int>& InteriorTraceGroups, \
                                  PyDict& BCList, \
                                  const std::map< std::string, std::vector<int> >& BCBoundaryGroups );


#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_UNSTEADY_TRAITS(PDEND, BCVECTOR, TOPODIM, TRAITS, PARAMFIELDTUPLE ) \
\
template AlgebraicEquationSet_HDG< PDEND, BCNDConvertSpace, BCVECTOR, TRAITS, PARAMFIELDTUPLE >:: \
         AlgebraicEquationSet_HDG(const PARAMFIELDTUPLE& xfld, \
                                  Field_DG_Cell<PDEND::PhysDim, \
                                                TOPODIM, \
                                                PDEND::ArrayQ<Real> >& qfld, \
                                  Field_DG_Cell<PDEND::PhysDim, \
                                                TOPODIM, \
                                                PDEND::VectorArrayQ<Real> >& afld, \
                                  Field<PDEND::PhysDim, \
                                        TOPODIM, \
                                        PDEND::ArrayQ<Real> >& qIfld, \
                                  Field<PDEND::PhysDim, \
                                        TOPODIM, \
                                        PDEND::ArrayQ<Real> >& lgfld, \
                                  const PDEND& pde, \
                                  DiscretizationHDG<PDEND>& disc, \
                                  const QuadratureOrder& quadratureOrder, \
                                  const std::vector<Real>& tol, \
                                  const std::vector<int>& CellGroups, \
                                  const std::vector<int>& CellGroupsAUX, \
                                  const std::vector<int>& InteriorTraceGroups, \
                                  PyDict& BCList, \
                                  const std::map< std::string, std::vector<int> >& BCBoundaryGroups, GlobalTime& );

//---------------------------------------------------------------------------//
// This is the main macro used for steady instantiation
#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACE( PDE, BCVECTOR, TOPODIM, TRAITS, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpace<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpace); \
\
ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_TRAITS( BOOST_PP_CAT(PDE, NDSpace), BCNDConvertSpace, BCVECTOR, TOPODIM, TRAITS, PARAMFIELDTUPLE ) \
\
ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_UNSTEADY_TRAITS( BOOST_PP_CAT(PDE, NDSpace), BCVECTOR, TOPODIM, TRAITS, PARAMFIELDTUPLE )

//---------------------------------------------------------------------------//
// This is the main macro used for space-time instantiation
#define ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_SPACETIME( PDE, BCVECTOR, TOPODIM, TRAITS, PARAMFIELDTUPLE ) \
\
typedef PDENDConvertSpaceTime<PDE::PhysDim, PDE> BOOST_PP_CAT(PDE, NDSpaceTime); \
\
ALGEBRAICEQUATIONSET_HDG_INSTANTIATE_TRAITS( BOOST_PP_CAT(PDE, NDSpaceTime), BCNDConvertSpaceTime, BCVECTOR, TOPODIM, TRAITS, PARAMFIELDTUPLE )
