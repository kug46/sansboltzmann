// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDCELL_HDG_AUXILIARYADJOINT_H
#define SETFIELDCELL_HDG_AUXILIARYADJOINT_H

// HDG cell integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/FieldData/FieldDataInvMassMatrix_Cell.h"
#include "Field/FieldData/FieldDataMatrixD_BoundaryCell.h"
#include "Field/XField_CellToTrace.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Discretization/HDG/SetFieldInteriorTrace_HDG_AuxiliaryAdjoint.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral jacobian
//

template<class Surreal, class IntegrandCell, class IntegrandITrace, class XFieldType_, class TopoDim_>
class SetFieldCell_HDG_AuxiliaryAdjoint_impl :
    public GroupIntegralCellType< SetFieldCell_HDG_AuxiliaryAdjoint_impl<Surreal, IntegrandCell, IntegrandITrace, XFieldType_, TopoDim_> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::PDE PDE;

  static_assert( std::is_same< PhysDim, typename IntegrandITrace::PhysDim >::value, "PhysDim should be the same.");

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;
  typedef DLA::MatrixS<1,1,ArrayQ> RowArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef typename PDE::template TensorMatrixQ<Real> TensorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename IntegrandCell::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandCell::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;


  // Save off the boundary trace integrand and the residual vectors
  SetFieldCell_HDG_AuxiliaryAdjoint_impl( const IntegrandCell& fcnCell,
                                          const IntegrandITrace& fcnITrace,
                                          const FieldDataInvMassMatrix_Cell& mmfld,
                                          const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                                          const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace,
                                          const XFieldType_& xfld,
                                          const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                                          const Field<PhysDim, TopoDim_, VectorArrayQ>& afld,
                                          const Field<PhysDim, TopoDim_, ArrayQ>& qIfld,
                                          const Field<PhysDim, TopoDim_, ArrayQ>& wfld,
                                          const Field<PhysDim, TopoDim_, VectorArrayQ>& bfld,
                                          const Field<PhysDim, TopoDim_, ArrayQ>& wIfld,
                                          const int quadOrderITrace[], const int nITraceGroup ) :
                                            fcnCell_(fcnCell), fcnITrace_(fcnITrace),
                                            mmfld_(mmfld), invJacAUX_a_bcell_(invJacAUX_a_bcell),
                                            xfldCellToTrace_(xfldCellToTrace), xfld_(xfld),
                                            qfld_(qfld), afld_(afld), qIfld_(qIfld),
                                            wfld_(wfld), bfld_(bfld), wIfld_(wIfld),
                                            quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup) {}

  std::size_t nCellGroups() const { return fcnCell_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcnCell_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qfld
                                                   Field<PhysDim, TopoDim, VectorArrayQ>, //afld
                                                   Field<PhysDim, TopoDim, ArrayQ>,       //wfld
                                                   Field<PhysDim, TopoDim, VectorArrayQ>  //bfld
                                      >::type & flds ) const {}

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename MakeTuple<FieldTuple, Field<PhysDim, TopoDim, ArrayQ>,       //qfld
                                                  Field<PhysDim, TopoDim, VectorArrayQ>, //afld
                                                  Field<PhysDim, TopoDim, ArrayQ>,       //wfld
                                                  Field<PhysDim, TopoDim, VectorArrayQ>  //bfld
                                     >::type::template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                          ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;
    typedef typename AFieldCellGroupType::template ElementType<Surreal> ElementAFieldSurrealClass;
    typedef typename AFieldCellGroupType::template ElementType<>        ElementAFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<>        ElementWFieldClass;
    typedef typename AFieldCellGroupType::template ElementType<>        ElementBFieldClass;

    const int D = PhysDim::D;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const AFieldCellGroupType& afldCell = get<1>(fldsCell);
    const QFieldCellGroupType& wfldCell = get<2>(fldsCell);
          AFieldCellGroupType& bfldCell = const_cast<AFieldCellGroupType&>(get<3>(fldsCell));

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldSurrealClass qfldElemSurreal( qfldCell.basis() );
    ElementAFieldSurrealClass afldElemSurreal( afldCell.basis() );
    ElementAFieldClass        afldElem       ( afldCell.basis() );
    ElementWFieldClass        wfldElem       ( wfldCell.basis() );
    ElementBFieldClass        bfldElem       ( bfldCell.basis() );

    // Inverse mass matrix used to compute auxiliary variables
    const DLA::MatrixDView_Array<Real>& mmfldCell = mmfld_.getCellGroupGlobal(cellGroupGlobal);

    // DOFs per element
    const int qDOF = qfldElemSurreal.nDOF();
    const int aDOF = afldElemSurreal.nDOF();

    const int nIntegrandPDE = qDOF;

    // variables/equations per DOF
    const int nEqn = IntegrandCell::N;

    // element integrals
    GalerkinWeightedIntegral<TopoDim, Topology, ArrayQSurreal> integralPDE(quadratureorder, nIntegrandPDE);

    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == afldCell.nElem() );

    // element integrand/residual
    DLA::VectorD<ArrayQSurreal> rsdElemPDE( nIntegrandPDE );

    // element jacobian matrices - transposed
    MatrixAUXElemClass mtxTElemPDE_a(aDOF, qDOF);

    // temporary storage for matrix-vector multiplication
    DLA::VectorD<VectorArrayQ> tmp( qDOF );

    // Provide a vector of the primal adjoint
    DLA::VectorD<RowArrayQ> w( qDOF );

    // Provide a vector view of the auxiliary adjoint variable DOFs
    DLA::VectorDView<VectorArrayQ> b( bfldElem.vectorViewDOF() );

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group and solve for the auxiliary variables first

    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElemSurreal, elem );
      afldCell.getElement( afldElemSurreal, elem );
      afldCell.getElement( afldElem       , elem );
      wfldCell.getElement( wfldElem       , elem );
      bfldCell.getElement( bfldElem       , elem );

      std::map<int,std::vector<int>> cellITraceGroups;
      bool isBoundaryCell = false;

      for (int trace = 0; trace < Topology::NTrace; trace++)
      {
        const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, elem, trace);

        if (traceinfo.type == TraceInfo::Interior)
        {
          //add this traceGroup and traceElem to the set of interior traces attached to this cell
          cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
        }
        else if (traceinfo.type == TraceInfo::Boundary)
        {
          isBoundaryCell = true;
        }
        else
          SANS_DEVELOPER_EXCEPTION("SetFieldCell_HDG_AuxiliaryAdjoint_impl - Invalid trace type.");
      }

      //-----------------------------------------------------------------------------------------

      // zero element Jacobians
      mtxTElemPDE_a = 0;

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(D*aDOF); nchunk += nDeriv)
      {
        // associate derivative slots with solution & auxiliary variables

        int slot, slotOffset = 0;

        //wrt a
        for (int j = 0; j < aDOF; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
                DLA::index(afldElemSurreal.DOF(j)[d],n).deriv(slot - nchunk) = 1;
            }
          }
        }
        slotOffset += nEqn*D*aDOF;

        for (int n = 0; n < nIntegrandPDE; n++) rsdElemPDE[n] = 0;

        // cell integration for canonical element

        integralPDE( fcnCell_.integrand_PDE(xfldElem, qfldElemSurreal, afldElemSurreal),
                     get<-1>(xfldElem), rsdElemPDE.data(), nIntegrandPDE );

        // accumulate derivatives into element jacobians
        slotOffset = 0;

        //wrt a
        for (int j = 0; j < aDOF; j++)
        {
          for (int d = 0; d < D; d++)
          {
            for (int n = 0; n < nEqn; n++)
            {
              slot = slotOffset + nEqn*(D*j + d) + n;
              if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              {
                DLA::index(afldElemSurreal.DOF(j)[d],n).deriv(slot - nchunk) = 0; //unset derivative

                for (int i = 0; i < qDOF; i++)
                  for (int m = 0; m < nEqn; m++)
                  {
                    // Fill in as transposed (j,i)[d](n,m)
                    DLA::index(mtxTElemPDE_a(j,i)[d],n,m) = DLA::index(rsdElemPDE[i],m).deriv(slot - nchunk);
                  }
              }
            }
          }
        }
        slotOffset += nEqn*D*aDOF;

      } // nchunk loop

      // Copy over adjoints for matrix-vector multiplication
      for (int i = 0; i < qDOF; i++) w[i] = wfldElem.DOF(i);

      // Accumulate the second component of the auxiliary variable adjoint (due to cell PDE residual)
      // b = [dAUX/da]^-T * ([dJ/da]^T - [dPDE/da]^T * w - [dINT/da]^T * wI)
      tmp = mtxTElemPDE_a*w;


      if (!isBoundaryCell)
      {
        //Interior cell: mass-matrix is symmetric so don't need transpose
        bfldCell.getElement( bfldElem, elem );
        b -= mmfldCell[elem] * tmp;
        bfldCell.setElement( bfldElem, elem );

        //Collect the contributions to the auxiliary variable adjoint from the interior traces (due to trace PDE/INT residuals)
        IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
            SetFieldInteriorTrace_HDG_AuxiliaryAdjoint<Surreal>(fcnITrace_, cellITraceGroups,
                                                                cellGroupGlobal, elem,
                                                                mmfldCell[elem]),
            xfld_, (qfld_, afld_, wfld_, bfld_), (qIfld_, wIfld_),
            quadOrderITrace_, nITraceGroup_ );
      }
      else
      {
        //Boundary cell: AUX_a jacobian is not simply the mass matrix
        bfldCell.getElement( bfldElem, elem );
        b -= Transpose(invJacAUX_a_bcell_.getCell(cellGroupGlobal, elem)) * tmp;
        bfldCell.setElement( bfldElem, elem );

        //Collect the contributions to the auxiliary variable adjoint from the interior traces (due to trace PDE/INT residuals)
        IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
            SetFieldInteriorTrace_HDG_AuxiliaryAdjoint<Surreal>(fcnITrace_, cellITraceGroups,
                                                                cellGroupGlobal, elem,
                                                                invJacAUX_a_bcell_.getCell(cellGroupGlobal, elem)),
            xfld_, (qfld_, afld_, wfld_, bfld_), (qIfld_, wIfld_),
            quadOrderITrace_, nITraceGroup_ );
      }

    } //elem
  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const FieldDataInvMassMatrix_Cell& mmfld_;
  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell_;

  const XField_CellToTrace<PhysDim, TopoDim_>& xfldCellToTrace_;
  const XFieldType_& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& wfld_;
  const Field<PhysDim, TopoDim_, VectorArrayQ>& bfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& wIfld_;

  const int *quadOrderITrace_;
  const int nITraceGroup_;
};

// Factory function

template<class Surreal, class IntegrandCell, class IntegrandITrace,
         class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ, class TensorMatrixQ>
SetFieldCell_HDG_AuxiliaryAdjoint_impl<Surreal, IntegrandCell, IntegrandITrace, XFieldType, TopoDim>
SetFieldCell_HDG_AuxiliaryAdjoint( const IntegrandCellType<IntegrandCell>& fcnCell,
                                   const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                                   const FieldDataInvMassMatrix_Cell& mmfld,
                                   const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
                                   const XField_CellToTrace<PhysDim, TopoDim>& xfldCellToTrace,
                                   const XFieldType& xfld,
                                   const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                   const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                   const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                   const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                   const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
                                   const Field<PhysDim, TopoDim, ArrayQ>& wIfld,
                                   const int quadOrderITrace[], const int nITraceGroup )
{
  return { fcnCell.cast(), fcnITrace.cast(),
           mmfld, invJacAUX_a_bcell, xfldCellToTrace,
           xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
           quadOrderITrace, nITraceGroup };
}

}

#endif  // SETFIELDCELL_HDG_AUXILIARYADJOINT_H
