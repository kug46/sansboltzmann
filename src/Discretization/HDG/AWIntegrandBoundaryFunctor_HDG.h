// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AWINTEGRANDBOUNDARYFUNCTOR_HDG_H
#define AWINTEGRANDBOUNDARYFUNCTOR_HDG_H

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// HDG element boundary integrand: PDE

template <class PDE, class WeightFunction >
class AWIntegrandBoundary_HDG
{
public:
  // typedef typename BC::PDE PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  AWIntegrandBoundary_HDG(
    const PDE& pde, const WeightFunction& wfcn, const DiscretizationHDG<PDE>& disc, const std::vector<int>& BoundaryGroups) :
    pde_(pde), wfcn_(wfcn), disc_(disc), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementAFieldL;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef IntegrandHDG< ArrayQ<T>, VectorArrayQ<T> > IntegrandCellType;

    Functor( const PDE& pde,
             const WeightFunction& wfcn,
             const DiscretizationHDG<PDE>& disc,
             const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementXFieldL& xfldElem,
             const ElementQFieldL& qfldElem,
             const ElementAFieldL& afldElem ) :
             pde_(pde), wfcn_(wfcn), disc_(disc),
             xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
             xfldElem_(xfldElem), qfldElem_(qfldElem), afldElem_(afldElem) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return afldElem_.nDOF(); }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType* integrandL, const int nrsdPDE,
                                                          ArrayQ<T>* integrandBC, const int nrsdBC ) const;
  protected:
    const PDE& pde_;
    const WeightFunction& wfcn_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
    const ElementAFieldL& afldElem_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysDim, TopoDimCell , TopologyL    >& xfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell , TopologyL    >& afldElem) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL>(pde_, wfcn_, disc_,
                                                                           xfldElemTrace, canonicalTrace,
                                                                           xfldElem, qfldElem, afldElem);
  }


private:
  const PDE& pde_;
  const WeightFunction& wfcn_;
  const DiscretizationHDG<PDE>& disc_;
  const std::vector<int> BoundaryGroups_;
};


template <class PDE, class WeightFunction>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL>
void
AWIntegrandBoundary_HDG<PDE, WeightFunction>::Functor<T,TopoDimTrace,TopologyTrace,TopoDimCell,TopologyL>::operator()(
    const QuadPointTraceType& sRefTrace, IntegrandCellType* integrandL, const int nrsdPDE, ArrayQ<T>* integrandBC, const int nrsdBC ) const
{
  SANS_ASSERT( (nrsdPDE == 1) );
  SANS_ASSERT( (nrsdBC <= 1) );

  VectorX X;                // physical coordinates
  VectorX N;                // unit normal (points out of domain)

  ArrayQ<T> psi;                                  // weight
  DLA::VectorS<PhysDim::D, ArrayQ<T>> gradpsi;       // weight gradient
  DLA::MatrixSymS<PhysDim::D, ArrayQ<T>> hessianpsi; // weight hessian

  ArrayQ<T> q;                                  // solution
  VectorArrayQ<T> a;                              // auxiliary variable (gradient)

  QuadPointCellType sRef;              // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // calculate weighting function
  wfcn_.secondGradient(X,psi,gradpsi,hessianpsi);

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, N );

  // basis value, gradient
  qfldElem_.eval( sRef, q );
  afldElem_.eval( sRef, a );

  integrandL[0] = 0;

  // PDE residual: weak form boundary integral

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    DLA::VectorS<PhysDim::D, ArrayQ<T> > F = 0;     // PDE flux

    // advective flux
    pde_.fluxAdvective( X, q, F );

    // viscous flux
    pde_.fluxViscous( X, q, a, F );

    ArrayQ<T> flux = dot(N,F);
    integrandL[0].PDE += psi*flux;

    //std::cout<< "inBound:Flux:" << flux << std::endl;
  }
}
}

#endif  // AWINTEGRANDBOUNDARYFUNCTOR_HDG_H
