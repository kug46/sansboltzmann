// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AWINTEGRANDFUNCTOR_HDG_H
#define AWINTEGRANDFUNCTOR_HDG_H

// cell/trace integrand operators: HDG

#include <ostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// Cell integrand: HDG
//
// integrandPDE = - gradient(phi)*F + phi*S - phi*RHS
// integrandAux = (K phi)*(ax - qx)
// integrandAuy = (K phi)*(ay - qy)
//
// WeightFunction     ND Interfaced Analytic Expression
//
// where
//   phi              basis function
//   q                solution
//   qx, qy           solution gradient
//   ax, ay           auxiliary variable
//   F(x,y,q,ax,ay)   flux (advective & viscous)
//   S(x,y,d,q)       solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//   K                diffusion matrix

template <class PDE, class WeightFunction>
class AWIntegrandCell_HDG
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  AWIntegrandCell_HDG( const PDE& pde, const WeightFunction& wfcn, const DiscretizationHDG<PDE>& disc )
    : pde_(pde), wfcn_(wfcn), disc_(disc) {}

  template<class T, class TopoDim, class Topology>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef typename ElementXFieldType::VectorX VectorX;
    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef Element<VectorArrayQ<T>, TopoDim, Topology> ElementAFieldType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    typedef IntegrandHDG< ArrayQ<T>, VectorArrayQ<T> > IntegrandType;

    Functor( const PDE& pde,
             const WeightFunction& wfcn,
             const DiscretizationHDG<PDE>& disc,
             const ElementXFieldType& xfldElem,
             const ElementQFieldType& qfldElem,
             const ElementAFieldType& afldElem ) :
             pde_(pde), wfcn_(wfcn), disc_(disc), xfldElem_(xfldElem), qfldElem_(qfldElem), afldElem_(afldElem) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return afldElem_.nDOF(); }

    // Cell integrand
    void operator()( const QuadPointType& sRef, IntegrandType* integrand, const int nIntegrand ) const;

  protected:
    const PDE& pde_;
    const WeightFunction& wfcn_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldType& qfldElem_;
    const ElementAFieldType& afldElem_;
  };

  template<class T, class TopoDim, class Topology>
  Functor<T, TopoDim, Topology> integrand(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                                          const Element<ArrayQ<T>, TopoDim, Topology>& qfldElem,
                                          const Element<VectorArrayQ<T>, TopoDim, Topology>& afldElem) const
  {
    return Functor<T, TopoDim, Topology>(pde_, wfcn_, disc_, xfldElem, qfldElem, afldElem);
  }

protected:
  const PDE& pde_;
  const WeightFunction& wfcn_;
  const DiscretizationHDG<PDE>& disc_;
};


template <class PDE, class WeightFunction>
template <class T, class TopoDim, class Topology>
bool
AWIntegrandCell_HDG<PDE, WeightFunction>::Functor<T,TopoDim,Topology>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

// Cell integrand
//
// integrand = - gradient(phi)*F + phi*S - phi*RHS
//
// where
//   psi              exact
//   F(x,y,q,ax,ay)   flux (advective and viscous)
//   S(x,y,d,q)       solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
template <class PDE, class WeightFunction>
template <class T, class TopoDim, class Topology>
void
AWIntegrandCell_HDG<PDE, WeightFunction>::Functor<T,TopoDim,Topology>::operator()(
    const QuadPointType& sRef, IntegrandType* integrand, const int nIntegrand ) const
{
  SANS_ASSERT( nIntegrand == 1 );

  VectorX X;                  // physical coordinates

  Real psi;               // weight
  DLA::VectorS<PhysDim::D, Real> gradpsi;       // weight gradient
  DLA::MatrixSymS<PhysDim::D, Real> hessianpsi; // weight hessian

  ArrayQ<T> q;                    // solution
  VectorArrayQ<T> gradu;            // solution gradient
  VectorArrayQ<T> a;                // auxiliary variable (gradient)

  DLA::VectorS<PhysDim::D, ArrayQ<T> > F;   // PDE flux F(X, U), Fv(X, U, UX)
  ArrayQ<T> source;                         // PDE source S(X, U, UX), S(X)

  X = 0;
  // physical coordinates
  xfldElem_.coordinates( sRef, X );

  // weighting value, gradient, hessian
  wfcn_.secondGradient(X,psi,gradpsi,hessianpsi);

  //std::cout<< "X" << X[0] << std::endl;
  //std::cout<< "psi" << psi[0] << std::endl;

  // solution value, gradient
  qfldElem_.eval( sRef, q );
  xfldElem_.evalGradient( sRef, qfldElem_, gradu );
  afldElem_.eval( sRef, a );

  // PDE
  integrand[0] = 0;

  // Galerkin flux term: -gradient(phi) . (F(q) + G(q,gradu))
  // G = - Kq for instance
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    F = 0;

    // advective flux
    pde_.fluxAdvective( X, q, F );

    // viscous flux
    if (pde_.hasFluxViscous())
      pde_.fluxViscous( X, q, a, F );

    integrand[0].PDE -= dot(gradpsi,F);
  }

  // Galerkin source term: +phi S
  // solution gradient augmented by lifting operator (asymptotically dual consistent)

  if (pde_.hasSource())
  {
    source = 0;

    pde_.source( X, q, a, source );

    integrand[0].PDE += psi*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    ArrayQ<Real> forcing = 0;
    pde_.forcingFunction( X, forcing );

    integrand[0].PDE -= psi*forcing;
  }

  //std::cout << "PDE :" << integrand[0].PDE << std::endl;
  // auxiliary variable definition

  // NOTE: should be conservation variables here!
  if (!pde_.hasFluxViscous() || disc_.auxVersion_ == Gradient)
  {
    // volume term: (phi)*(a - gradu)
    VectorArrayQ<T> tmp;

    tmp = a - gradu;
    for (int d=0; d< PhysDim::D; d++)
      integrand[0].Au[d] = -gradpsi[d]*tmp[d];

    //std::cout << "a    :" << a << std::endl;
    //std::cout << "gradu:" << gradu << std::endl;
    //std::cout << "tmp1 :" << tmp <<std::endl;
    //std::cout << "AU   :" << integrand[0].Au << std::endl;

  }
  else if (disc_.auxVersion_ == AugGradient)
  {
    DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<T> > K = 0;  // diffusion matrix
    VectorArrayQ<T> tmp = 0, da;

    pde_.diffusionViscous( X, q, a, K );

    //tmpx = kxx*(ax - qx) + kyx*(ay - qy);
    //tmpy = kxy*(ax - qx) + kyy*(ay - qy);

    da = a - gradu;
    for ( int i = 0; i < PhysDim::D; i++ )
      for ( int j = 0; j < PhysDim::D; j++ )
        tmp[i] += K(i,j)*da[j];

    for (int d=0; d< PhysDim::D; d++)
      integrand[0].Au[d] = -gradpsi[d]*tmp[d];
    //std::cout << "a    :" << a << std::endl;
    //std::cout << "gradu:" << gradu << std::endl;
    //std::cout << "tmp2 :" << tmp <<std::endl;
    //std::cout << "AU   :" << integrand[0].Au << std::endl;

  }
  else
  {
    const char msg[] = "Error in HDG Integrand Functor: unknown auxiliary variable defn \n";
    SANS_DEVELOPER_EXCEPTION( msg );
  }
  //std::cout << std::endl;
}

//----------------------------------------------------------------------------//
// Trace integrand: HDG
//
// integrandL = phiL * fnL
// integrandR = phiR * fnR
// integrandI = phiI * (fnR + fnL) = phiI * [[fn]]
//
// fnL = Fadv(qI) + Fvis(qL,qxL,qyL) + tauL*(qL - qI)
// fnR = Fadv(qI) + Fvis(qR,qxR,qyR) + tauR*(qR - qI)
//
// where
//   phi                basis function
//   qI                 interface solution
//   qL, axL, ayL       solution, auxiliary variable (left)
//   qR, axR, ayR       solution, auxiliary variable (right)
//   Fadv(x,y,qI)       advective flux
//   Fvis(x,y,q,qx,qy)  viscous flux
//   tau                HDG stabilization parameter

template <class PDE, class WeightFunction>
class AWIntegrandTrace_HDG
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  AWIntegrandTrace_HDG(const PDE& pde, const WeightFunction& wfcn, const DiscretizationHDG<PDE>& disc )
    : pde_(pde), wfcn_(wfcn), disc_(disc) {}

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementAFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyR> ElementAFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef IntegrandHDG< ArrayQ<T>, VectorArrayQ<T> > IntegrandCellType;
    typedef ArrayQ<T> IntegrandTraceType;

    Functor( const PDE& pde,
             const WeightFunction& wfcn,
             const DiscretizationHDG<PDE>& disc,
             const ElementXFieldTrace& xfldElemTrace,
             const ElementTraceField& qfldElemTrace,
             const CanonicalTraceToCell& canonicalTraceL,
             const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldL& xfldElemL,
             const ElementQFieldL& qfldElemL,
             const ElementAFieldL& afldElemL,
             const ElementXFieldR& xfldElemR,
             const ElementQFieldL& qfldElemR,
             const ElementAFieldR& afldElemR ) :
             pde_(pde), wfcn_(wfcn), disc_(disc),
             xfldElemTrace_(xfldElemTrace), qfldElemTrace_(qfldElemTrace),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL), afldElemL_(afldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR), afldElemR_(afldElemR) {}


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return afldElemL_.nDOF(); }
    int nDOFRight() const { return afldElemR_.nDOF(); }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType* integrandCellL, const int nrsdL,
                                                          IntegrandCellType* integrandCellR, const int nrsdR,
                                                          ArrayQ<T>* integrandIntI, const int nrsdI ) const;

  protected:
    const PDE& pde_;
    const WeightFunction& wfcn_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementTraceField& qfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementAFieldL& afldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
    const ElementAFieldR& afldElemR_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL, TopologyR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& uIfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell, TopologyL>& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell, TopologyL>& qfldElemL,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& afldElemL,
            const ElementXField<PhysDim, TopoDimCell, TopologyR>& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell, TopologyR>& qfldElemR,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyR>& afldElemR) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>(pde_, wfcn_, disc_,
                                                                                      xfldElemTrace, uIfldElemTrace,
                                                                                      canonicalTraceL, canonicalTraceR,
                                                                                      xfldElemL, qfldElemL, afldElemL,
                                                                                      xfldElemR, qfldElemR, afldElemR);
  }

protected:
  const PDE& pde_;
  const WeightFunction& wfcn_;
  const DiscretizationHDG<PDE>& disc_;
};


template <class PDE, class WeightFunction>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
bool
AWIntegrandTrace_HDG<PDE, WeightFunction>::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() );
}


template <class PDE, class WeightFunction>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
void
AWIntegrandTrace_HDG<PDE, WeightFunction>::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::operator()(
    const QuadPointTraceType& sRefTrace, IntegrandCellType* integrandCellL, const int nrsdL,
                                         IntegrandCellType* integrandCellR, const int nrsdR,
                                         ArrayQ<T>* integrandIntI, const int nrsdI ) const
{
  SANS_ASSERT( nrsdL == 1 );
  SANS_ASSERT( nrsdR == 1 );
  SANS_ASSERT( nrsdI == 1 );

  VectorX X,XL,XR;                // physical coordinates
  VectorX nL, nR;                 // unit normal for left element (points to right element)

  Real psiI;                                  // weight
  DLA::VectorS<PhysDim::D, Real> gradpsiI;       // weight gradient
  DLA::MatrixSymS<PhysDim::D, Real> hessianpsiI; // weight hessian

  Real psiL;                                  // weight
  DLA::VectorS<PhysDim::D, Real> gradpsiL;       // weight gradient
  DLA::MatrixSymS<PhysDim::D, Real> hessianpsiL; // weight hessian

  Real psiR;                                  // weight
  DLA::VectorS<PhysDim::D, Real> gradpsiR;       // weight gradient
  DLA::MatrixSymS<PhysDim::D, Real> hessianpsiR; // weight hessian

  ArrayQ<T> qI;                   // interface solution
  ArrayQ<T> qL, qR;               // solution
  VectorArrayQ<T> aL, aR;           // auxiliary variable (gradient)

  ArrayQ<T> uConsL, uConsR;       // left/right conservation variables
  ArrayQ<T> uConsI;               // interface conservation variables

  DLA::VectorS< PhysDim::D, ArrayQ<T> > F;  // PDE flux F(X, U), Fv(X, U, UX)

  QuadPointCellType sRefL;              // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  ArrayQ<T> FnL = 0, FnR = 0; // normal advective flux

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, sRefTrace, sRefR );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );
  xfldElemL_.coordinates( sRefL, XL);
  xfldElemR_.coordinates( sRefR, XR);

  // weighting value, gradient, hessian
  wfcn_.secondGradient(X,psiI,gradpsiI,hessianpsiI);
  wfcn_.secondGradient(X,psiL,gradpsiL,hessianpsiL);
  wfcn_.secondGradient(X,psiR,gradpsiR,hessianpsiR);

  // unit normal: points to R
  xfldElemTrace_.unitNormal( sRefTrace, nL );
  nR = -nL;

  // solution value, gradient
  qfldElemL_.eval( sRefL, qL );
  qfldElemR_.eval( sRefR, qR );
  if (needsSolutionGradient)
  {
    afldElemL_.eval( sRefL, aL );
    afldElemR_.eval( sRefR, aR );
  }
  qfldElemTrace_.eval( sRefTrace, qI );

  integrandCellL[0] = 0;
  integrandCellR[0] = 0;

  // advective flux term: FadvUpwindL(qL,qI),  FadvUpwindR(qI,qR)

  if (pde_.hasFluxAdvective())
  {
    pde_.fluxAdvectiveUpwind( X, qL, qI, nL, FnL );
    pde_.fluxAdvectiveUpwind( X, qR, qI, nR, FnR );
  }

  // viscous flux term: Fvis(q,qx,qy)

  if (pde_.hasFluxViscous())
  {
    DLA::VectorS< PhysDim::D, ArrayQ<T> > FL = 0;
    DLA::VectorS< PhysDim::D, ArrayQ<T> > FR = 0;

    pde_.fluxViscous( X, qL, aL, FL );
    pde_.fluxViscous( X, qR, aR, FR );

    FnL += dot(nL,FL);
    FnR += dot(nR,FR);
  }

  // HDG stabilization term: tau*(q - qI)

  MatrixQ<T> tauL, tauR;
  Real lengthL = xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  Real lengthR = xfldElemR_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  disc_.evalStabilization( X, nL, lengthL, qI, tauL );
  disc_.evalStabilization( X, nR, lengthR, qI, tauR );

  FnL += tauL*(qL - qI);
  FnR += tauR*(qR - qI);

  integrandCellL[0].PDE = psiL*FnL;
  integrandCellR[0].PDE = psiR*FnR;

  ArrayQ<T> Fsum = (FnR + FnL);

  integrandIntI[0] = psiI*Fsum;
  //std::cout<< "inIFunc:Fsum:" << Fsum << std::endl;

  // auxiliary variable definition

  // trace term: (K psi . normal)*(q - qI)

  uConsI = 0;
  pde_.masterState( qI, uConsI );

  uConsL = 0;
  pde_.masterState( qL, uConsL );

  uConsR = 0;
  pde_.masterState( qR, uConsR );

  if (!pde_.hasFluxViscous() || disc_.auxVersion_ == Gradient )
  {
    VectorArrayQ<T> tmp = 0;
    ArrayQ<T> dq;

    dq = (qL - qI);

    for ( int i = 0; i < PhysDim::D; i++ )
      tmp[i] = nL[i]*dq;

    for (int d=0; d< PhysDim::D; d++)
      integrandCellL[0].Au[d] = -gradpsiL[d]*tmp[d];

    dq = (qR - qI);

    for ( int i = 0; i < PhysDim::D; i++ )
      tmp[i] = nR[i]*dq;

    for (int d=0; d< PhysDim::D; d++)
      integrandCellR[0].Au[d] = -gradpsiR[d]*tmp[d];
    //std::cout<< "inFunc:tmp12:" << tmp << std::endl;
  }
  else
  {
    DLA::MatrixS< PhysDim::D, PhysDim::D, MatrixQ<T> > KL = 0, KR = 0;  // diffusion matrix
    VectorArrayQ<T> tmp = 0;
    ArrayQ<T> du;

    // left
    pde_.diffusionViscous( X, qL, aL, KL );

    //tmpx = (nxL*kxx + nyL*kyx)*(uConsL - uConsI);
    //tmpy = (nxL*kxy + nyL*kyy)*(uConsL - uConsI);

    //todo: fix! dq

    du = (uConsL - uConsI);

    tmp = 0;
    for ( int i = 0; i < PhysDim::D; i++ )
      for ( int j = 0; j < PhysDim::D; j++ )
        tmp[i] += (KL(i,j)*nL[j])*du;

    for (int d=0; d< PhysDim::D; d++)
      integrandCellL[0].Au[d] = -gradpsiL[d]*tmp[d];

    // right
    pde_.diffusionViscous( X, qR, aR, KR );

    //tmpx = (nxR*kxx + nyR*kyx)*(uConsR - uConsI);
    //tmpy = (nxR*kxy + nyR*kyy)*(uConsR - uConsI);

    du = (uConsR - uConsI);

    tmp = 0;
    for ( int i = 0; i < PhysDim::D; i++ )
      for ( int j = 0; j < PhysDim::D; j++ )
        tmp[i] += (KR(i,j)*nR[j])*du;

    for (int d=0; d< PhysDim::D; d++)
      integrandCellR[0].Au[d] = -gradpsiR[d]*tmp[d];

  }

}

}

#endif  // ANALYTICINTEGRANDFUNCTOR_HDG_H
