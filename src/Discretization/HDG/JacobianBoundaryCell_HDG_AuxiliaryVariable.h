// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYCELL_HDG_AUXILIARYVARIABLE_H
#define JACOBIANBOUNDARYCELL_HDG_AUXILIARYVARIABLE_H

// HDG boundary cell integral jacobian functions

#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/CellToTraceAssociativity.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"
#include "Discretization/IntegrateInteriorTraceGroups_FieldTrace.h"

#include "Discretization/HDG/JacobianInteriorTrace_HDG_AuxiliaryVariable.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG cell group integral jacobian
//

template<class Surreal, class IntegrandCell, class IntegrandITrace, class XFieldCellToTrace,
         class XFieldClass, class TopoDim_>
class JacobianBoundaryCell_HDG_AuxiliaryVariable_impl :
    public GroupIntegralCellType< JacobianBoundaryCell_HDG_AuxiliaryVariable_impl<Surreal, IntegrandCell, IntegrandITrace,
                                                                                  XFieldCellToTrace, XFieldClass, TopoDim_> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::PDE PDE;

  static_assert( std::is_same< PhysDim, typename IntegrandITrace::PhysDim >::value, "PhysDim should be the same.");

  typedef typename PDE::template ArrayQ<Real> ArrayQ;
  typedef typename PDE::template VectorArrayQ<Real> VectorArrayQ;

  typedef typename PDE::template MatrixQ<Real> MatrixQ;
  typedef typename PDE::template VectorMatrixQ<Real> VectorMatrixQ;
  typedef DLA::MatrixS<1,PhysDim::D,MatrixQ> RowMatrixQ;

  typedef typename IntegrandCell::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandCell::template VectorArrayQ<Surreal> VectorArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;
  typedef DLA::MatrixD<VectorMatrixQ> MatrixAUXElemClass;
  typedef DLA::MatrixD<RowMatrixQ> MatrixAElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianBoundaryCell_HDG_AuxiliaryVariable_impl( const IntegrandCell& fcnCell, const IntegrandITrace& fcnITrace,
                                                   const int cellgroup, const int cellelem,
                                                   const XFieldCellToTrace& xfldCellToTrace,
                                                   const XFieldClass& xfld,
                                                   const Field<PhysDim, TopoDim_, ArrayQ>& qfld,
                                                   const Field<PhysDim, TopoDim_, VectorArrayQ>& afld,
                                                   const Field<PhysDim, TopoDim_, ArrayQ>& qIfld,
                                                   const int quadOrderITrace[], const int nITraceGroup,
                                                   MatrixAUXElemClass& mtxAUX_q,
                                                   std::vector<MatrixAUXElemClass>& mtxAUX_qI,
                                                   std::vector<std::vector<int>>& mapDOFGlobal_qI ) :
                                                     fcnCell_(fcnCell), fcnITrace_(fcnITrace),
                                                     cellgroup_(cellgroup), cellelem_(cellelem),
                                                     xfldCellToTrace_(xfldCellToTrace),
                                                     xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
                                                     quadOrderITrace_(quadOrderITrace), nITraceGroup_(nITraceGroup),
                                                     mtxAUX_q_(mtxAUX_q), mtxAUX_qI_(mtxAUX_qI), mapDOFGlobal_qI_(mapDOFGlobal_qI) {}

  std::size_t nCellGroups() const { return 1; }
  std::size_t cellGroup(const int n) const { return cellgroup_; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds ) const {}

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType                          ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field< PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<Topology> AFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;
    typedef typename AFieldCellGroupType::template ElementType<Surreal> ElementAFieldSurrealClass;

    const int D = PhysDim::D;

    SANS_ASSERT( cellGroupGlobal == cellgroup_ );

    //Check if sizes are consistent
    SANS_ASSERT( mtxAUX_qI_.size() == Topology::NTrace );
    SANS_ASSERT( mapDOFGlobal_qI_.size() == Topology::NTrace );

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const AFieldCellGroupType& afldCell = get<1>(fldsCell);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldSurrealClass qfldElemSurreal( qfldCell.basis() );
    ElementAFieldSurrealClass afldElemSurreal( afldCell.basis() );

    // DOFs per element
    const int qDOF = qfldElemSurreal.nDOF();
    const int aDOF = afldElemSurreal.nDOF();

    const int nIntegrandAUX = aDOF;

    // variables/equations per DOF
    const int nEqn = IntegrandCell::N;

    // element integrals
    GalerkinWeightedIntegral<TopoDim, Topology, VectorArrayQSurreal> integralAUX(quadratureorder, nIntegrandAUX);

    const int nelem = xfldCell.nElem();

    // just to make sure things are consistent
    SANS_ASSERT( nelem == qfldCell.nElem() );
    SANS_ASSERT( nelem == afldCell.nElem() );

    // auxiliary variable residuals
    DLA::VectorD<VectorArrayQSurreal> rsdElemAUX( nIntegrandAUX );

    // element jacobian matrices
    MatrixAUXElemClass mtxElemAUX_q(aDOF, qDOF);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // copy global grid/solution DOFs to element
    xfldCell.getElement( xfldElem, cellelem_ );
    qfldCell.getElement( qfldElemSurreal, cellelem_ );
    afldCell.getElement( afldElemSurreal, cellelem_ );

    std::map<int,std::vector<int>> cellITraceGroups;

    for (int trace = 0; trace < Topology::NTrace; trace++)
    {
      const TraceInfo& traceinfo = xfldCellToTrace_.getTrace(cellGroupGlobal, cellelem_, trace);

      if (traceinfo.type == TraceInfo::Interior)
      {
        //add this traceGroup and traceElem to the set of interior traces attached to this cell
        cellITraceGroups[traceinfo.group].push_back(traceinfo.elem);
      }
    }

    //---------------------------------------------------------------------------------------------
    //Compute the auxiliary variable jacobian contributions (a_q and a_qI) from this cell
    //and its interior traces, to be passed back to boundary-trace.

    // zero element Jacobians
    mtxElemAUX_q = 0;

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nEqn*qDOF; nchunk += nDeriv)
    {
      // associate derivative slots with solution & auxiliary variables

      int slot, slotOffset = 0;

      //wrt q
      for (int j = 0; j < qDOF; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = slotOffset + nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemSurreal.DOF(j),n).deriv(slot - nchunk) = 1;
        }
      }
      slotOffset = nEqn*qDOF;

      //-----------------------------------------------------------------------------

      for (int n = 0; n < nIntegrandAUX; n++) rsdElemAUX[n] = 0;

      // cell integration for canonical element
      integralAUX( fcnCell_.integrand_AUX(get<-1>(xfldElem), qfldElemSurreal, afldElemSurreal),
                   get<-1>(xfldElem), rsdElemAUX.data(), nIntegrandAUX );

      //-----------------------------------------------------------------------------

      // accumulate derivatives into element jacobians
      slotOffset = 0;

      for (int j = 0; j < qDOF; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = slotOffset + nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            DLA::index(qfldElemSurreal.DOF(j),n).deriv(slot - nchunk) = 0; // unset derivative

            for (int i = 0; i < qDOF; i++)
            {
              for (int m = 0; m < nEqn; m++)
                for (int d = 0; d < D; d++)
                  DLA::index(mtxElemAUX_q(i,j)[d],m,n) = DLA::index(rsdElemAUX[i][d],m).deriv(slot - nchunk);
            }
          }
        }
      }
      slotOffset = nEqn*qDOF;

    }  // nchunk

    // Accumulate the cell contribution
    mtxAUX_q_ += mtxElemAUX_q;

    IntegrateInteriorTraceGroups_FieldTrace<TopoDim>::integrate(
        JacobianInteriorTrace_HDG_AuxiliaryVariable<Surreal>(fcnITrace_, cellITraceGroups,
                                                             cellGroupGlobal, cellelem_,
                                                             mtxAUX_q_, mtxAUX_qI_, mapDOFGlobal_qI_),
        xfld_, (qfld_, afld_), qIfld_,
        quadOrderITrace_, nITraceGroup_ );
  }

protected:
  const IntegrandCell& fcnCell_;
  const IntegrandITrace& fcnITrace_;
  const int cellgroup_;
  const int cellelem_;
  const XFieldCellToTrace& xfldCellToTrace_;
  const XFieldClass& xfld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim_, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim_, ArrayQ>& qIfld_;

  const int *quadOrderITrace_;
  const int nITraceGroup_;

  MatrixAUXElemClass& mtxAUX_q_;
  std::vector<MatrixAUXElemClass>& mtxAUX_qI_;
  std::vector<std::vector<int>>& mapDOFGlobal_qI_;
};

// Factory function

template<class Surreal, class IntegrandCell, class IntegrandITrace, class XFieldCellToTrace,
         class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ,class VectorMatrixQ>
JacobianBoundaryCell_HDG_AuxiliaryVariable_impl<Surreal, IntegrandCell, IntegrandITrace, XFieldCellToTrace,
                                                XFieldType, TopoDim>
JacobianBoundaryCell_HDG_AuxiliaryVariable( const IntegrandCellType<IntegrandCell>& fcnCell,
                                            const IntegrandInteriorTraceType<IntegrandITrace>& fcnITrace,
                                            const int cellgroup, const int cellelem,
                                            const XFieldCellToTrace& xfldCellToTrace,
                                            const XFieldType& xfld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                            const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                            const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                            const int quadOrderITrace[], const int nITraceGroup,
                                            DLA::MatrixD<VectorMatrixQ>& mtxAUX_q,
                                            std::vector<DLA::MatrixD<VectorMatrixQ>>& mtxAUX_qI,
                                            std::vector<std::vector<int>>& mapDOFGlobal_qI )
{
  return { fcnCell.cast(), fcnITrace.cast(),
           cellgroup, cellelem, xfldCellToTrace,
           xfld, qfld, afld, qIfld,
           quadOrderITrace, nITraceGroup,
           mtxAUX_q, mtxAUX_qI, mapDOFGlobal_qI };
}

}

#endif  // JACOBIANBOUNDARYCELL_HDG_AUXILIARYVARIABLE_H
