// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDINTERIORTRACE_HDG_H
#define INTEGRANDINTERIORTRACE_HDG_H

// interior trace integrand operators: HDG

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"
#include "JacobianInteriorTrace_HDG_Element.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// Trace integrand: HDG
//
// integrandL = phiL * fnL
// integrandR = phiR * fnR
// integrandI = phiI * (fnR + fnL) = phiI * [[fn]]
//
// fnL = Fadv(qI) + Fvis(qL,axL,ayL) + taqL*(qL - qI)
// fnR = Fadv(qI) + Fvis(qR,axR,ayR) + taqR*(qR - qI)
//
// where
//   phi                basis function
//   qI                 interface solution
//   qL, axL, ayL       solution, auxiliary variable (left)
//   qR, axR, ayR       solution, auxiliary variable (right)
//   Fadv(x,y,qI)       advective flux
//   Fvis(x,y,u,qx,qy)  viscous flux
//   tau                HDG stabilization parameter

template <class PDE_>
class IntegrandInteriorTrace_HDG :
  public IntegrandInteriorTraceType< IntegrandInteriorTrace_HDG<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D,Real> VecReal;

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int D = PhysDim::D;          // Physical dimensions
  static const int N = PDE::N;

  IntegrandInteriorTrace_HDG(const PDE& pde, const DiscretizationHDG<PDE>& disc,
                             const std::vector<int>& InteriorTraceGroups )
    : pde_(pde), disc_(disc), interiorTraceGroups_(InteriorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<      ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;
    typedef Element<      ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementAFieldL;

    typedef JacobianElemInteriorTrace_HDG<PhysDim, MatrixQ<Real>> JacobianElemInteriorTraceType;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandCellType;
    typedef ArrayQ<T> IntegrandTraceType;

    BasisWeighted( const PDE& pde,
                   const DiscretizationHDG<PDE>& disc,
                   const ElementXFieldTrace& xfldElemTrace,
                   const ElementTraceField& qfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL, // XField must be the last parameter
                   const ElementQFieldL& qfldElemL,
                   const ElementAFieldL& afldElemL ) :
                     pde_(pde), disc_(disc),
                     xfldElemTrace_(xfldElemTrace), qfldElemTrace_(qfldElemTrace),
                     canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
                     xfldElemL_(get<-1>(paramfldElemL)),
                     qfldElemL_(qfldElemL), afldElemL_(afldElemL),
                     paramfldElemL_(paramfldElemL),
                     nDOFL_( qfldElemL_.nDOF() ),
                     nDOFI_( qfldElemTrace_.nDOF() ),
                     phiL_( new Real[nDOFL_] ),
                     phiI_( new Real [nDOFI_] ),
                     gradphiL_( new VectorX[nDOFL_] ),
                     lengthL_(xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant())
    {
      SANS_ASSERT( qfldElemL_.basis() == afldElemL_.basis() );
      SANS_ASSERT( normalSign_ == 1 || normalSign_ == -1 );
    }

    BasisWeighted( BasisWeighted&& bw ) :
                     pde_(bw.pde_), disc_(bw.disc_),
                     xfldElemTrace_(bw.xfldElemTrace_), qfldElemTrace_(bw.qfldElemTrace_),
                     canonicalTraceL_(bw.canonicalTraceL_), normalSign_(bw.normalSign_),
                     xfldElemL_(bw.xfldElemL_),
                     qfldElemL_(bw.qfldElemL_), afldElemL_(bw.afldElemL_),
                     paramfldElemL_(bw.paramfldElemL_),
                     nDOFL_( bw.nDOFL_ ),
                     nDOFI_( bw.nDOFI_ ),
                     phiL_( bw.phiL_ ),
                     phiI_( bw.phiI_ ),
                     gradphiL_( bw.gradphiL_ ),
                     lengthL_( bw.lengthL_ )
    {
      bw.phiL_ = nullptr;
      bw.phiI_ = nullptr;
      bw.gradphiL_ = nullptr;
    }

    ~BasisWeighted()
    {
      delete [] phiL_;
      delete [] phiI_;
      delete [] gradphiL_;
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() ); }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFTrace() const { return qfldElemTrace_.nDOF(); }

    // element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCellL[], const int neqnL,
                                                          ArrayQ<Ti> integrandIntI[] , const int neqnI ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemInteriorTraceType& mtxElemL,
                     JacobianElemInteriorTraceType& mtxElemI ) const;

  protected:

    template<class Tq, class Tg, class Ti>
    void weightedIntegrand( const VectorX& nL,
                            const ParamTL& paramL,
                            const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& aL,
                            const ArrayQ<Tq>& qI,
                            ArrayQ<Ti> integrandCellL[], const int neqnL,
                            ArrayQ<Ti> integrandIntI[], const int neqnI ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementTraceField& qfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementAFieldL& afldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFI_;
    mutable Real* phiL_;
    mutable Real* phiI_;
    mutable VectorX* gradphiL_;
    Real lengthL_;
  };

#if 1
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  class BasisWeighted_AUX
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;

    typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
    typedef typename ElementXFieldL::RefCoordType RefCoordCellType;
    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef VectorArrayQ<T> IntegrandType;

    BasisWeighted_AUX( const PDE& pde,
                       const DiscretizationHDG<PDE>& disc,
                       const ElementXFieldTrace& xfldElemTrace,
                       const ElementTraceField& qfldElemTrace,
                       const CanonicalTraceToCell& canonicalTraceL,
                       const int normalSign,
                       const ElementParamL& paramfldElemL, // XField must be the last parameter
                       const ElementQFieldL& qfldElemL ) :
                         pde_(pde), disc_(disc),
                         xfldElemTrace_(xfldElemTrace), qfldElemTrace_(qfldElemTrace),
                         canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
                         xfldElemL_(get<-1>(paramfldElemL)),
                         qfldElemL_(qfldElemL),
                         paramfldElemL_(paramfldElemL),
                         nDOFL_( qfldElemL_.nDOF() ),
                         nDOFI_( qfldElemTrace_.nDOF() )
    {
      phiL_.resize(nDOFL_,0);
      phiI_.resize(nDOFI_,0);

      SANS_ASSERT( normalSign_ == 1 || normalSign_ == -1 );
    }

    BasisWeighted_AUX( BasisWeighted_AUX&& bw ) :
                         pde_(bw.pde_), disc_(bw.disc_),
                         xfldElemTrace_(bw.xfldElemTrace_), qfldElemTrace_(bw.qfldElemTrace_),
                         canonicalTraceL_(bw.canonicalTraceL_), normalSign_(bw.normalSign_),
                         xfldElemL_(bw.xfldElemL_),
                         qfldElemL_(bw.qfldElemL_),
                         paramfldElemL_(bw.paramfldElemL_),
                         nDOFL_( bw.nDOFL_ ),
                         nDOFI_( bw.nDOFI_ ),
                         phiL_(std::move(bw.phiL_)),
                         phiI_(std::move(bw.phiI_))
    {
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return pde_.hasFluxViscous( ); }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFTrace() const { return qfldElemTrace_.nDOF(); }

    // element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const;

  protected:
    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementTraceField& qfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFI_;
    mutable std::vector<Real> phiL_;
    mutable std::vector<Real> phiI_;
  };
#endif

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,    class ElementParamL >
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<      ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;
    typedef Element<      ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<VectorArrayQ<T>, TopoDimCell, TopologyL> ElementAFieldL;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef IntegrandHDG< Real, Real > IntegrandCellType;
    typedef Real IntegrandTraceType;

    FieldWeighted( const PDE& pde,
                   const DiscretizationHDG<PDE>& disc,
                   const ElementXFieldTrace& xfldElemTrace,
                   const ElementTraceField& qIfldElemTrace,
                   const ElementTraceField& wIfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL, // XField must be the last parameter
                   const ElementQFieldL& qfldElemL,
                   const ElementAFieldL& afldElemL,
                   const ElementQFieldL& wfldElemL,
                   const ElementAFieldL& bfldElemL ) :
                   pde_(pde), disc_(disc),
                   xfldElemTrace_(xfldElemTrace),
                   qIfldElemTrace_(qIfldElemTrace), wIfldElemTrace_(wIfldElemTrace),
                   canonicalTraceL_(canonicalTraceL), normalSign_(normalSign),
                   xfldElemL_(get<-1>(paramfldElemL)),
                   qfldElemL_(qfldElemL), afldElemL_(afldElemL),
                   wfldElemL_(wfldElemL), bfldElemL_(bfldElemL),
                   paramfldElemL_(paramfldElemL),
                   nDOFL_( qfldElemL_.nDOF() ),
                   nDOFI_( qIfldElemTrace_.nDOF() ),
                   nwDOFL_( wfldElemL_.nDOF() ),
                   nwDOFI_( wIfldElemTrace_.nDOF() ),
                   lengthL_(xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant())
    {
      SANS_ASSERT( qfldElemL_.basis() == afldElemL_.basis() );
      phiL_.resize(nDOFL_,0);
      phiI_.resize(nDOFI_,0);
      wphiL_.resize(nwDOFL_,0);
      wphiI_.resize(nwDOFI_,0);

      SANS_ASSERT( normalSign_ == 1 || normalSign_ == -1 );
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() || pde_.hasSource() ); }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }

    // element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCellL,
                                                          IntegrandTraceType& integrandIntI ) const;

  protected:
    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementTraceField& qIfldElemTrace_;
    const ElementTraceField& wIfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementAFieldL& afldElemL_;
    const ElementQFieldL& wfldElemL_;
    const ElementAFieldL& bfldElemL_;
    const ElementParamL& paramfldElemL_;

    const int nDOFL_;
    const int nDOFI_;
    const int nwDOFL_;
    const int nwDOFI_;
    mutable std::vector<Real> phiL_;
    mutable std::vector<Real> phiI_;
    mutable std::vector<Real> wphiL_;
    mutable std::vector<Real> wphiI_;
    const Real lengthL_;
  };

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                       TopoDimCell,  TopologyL,     ElementParamL >
  integrand( const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
             const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& qIfldElemTrace,
             const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
             const ElementParamL& paramfldElemL,     // XField must be the last parameter
             const Element<ArrayQ<T>      , TopoDimCell, TopologyL>& qfldElemL,
             const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& afldElemL ) const
  {
    return { pde_, disc_,
             xfldElemTrace, qIfldElemTrace,
             canonicalTraceL, normalSign,
             paramfldElemL, qfldElemL, afldElemL };
  }

#if 1
  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  BasisWeighted_AUX<T, TopoDimTrace, TopologyTrace,
                       TopoDimCell,  TopologyL,     ElementParamL >
  integrand_AUX(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
                const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& qIfldElemTrace,
                const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
                const ElementParamL& paramfldElemL,     // XField must be the last parameter
                const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL) const
  {
    return { pde_, disc_,
             xfldElemTrace, qIfldElemTrace,
             canonicalTraceL, normalSign,
             paramfldElemL, qfldElemL };
  }
#endif

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell,  class TopologyL,     class ElementParamL >
  FieldWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell,  TopologyL,     ElementParamL >
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& qIfldElemTrace,
            const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& wIfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const int normalSign,
            const ElementParamL& paramfldElemL,     // XField must be the last parameter
            const Element<ArrayQ<T>      , TopoDimCell, TopologyL>& qfldElemL,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& afldElemL,
            const Element<ArrayQ<T>      , TopoDimCell, TopologyL>& wfldElemL,
            const Element<VectorArrayQ<T>, TopoDimCell, TopologyL>& bfldElemL) const
  {
    return { pde_, disc_,
             xfldElemTrace,
             qIfldElemTrace, wIfldElemTrace,
             canonicalTraceL, normalSign,
             paramfldElemL,
             qfldElemL, afldElemL,
             wfldElemL, bfldElemL };
  }

protected:
  const PDE& pde_;
  const DiscretizationHDG<PDE>& disc_;
  const std::vector<int> interiorTraceGroups_;
};

template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class ElementParamL >
template <class Ti>
void
IntegrandInteriorTrace_HDG<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     ElementParamL>::
operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCellL[], const int neqnL,
                                                 ArrayQ<Ti> integrandIntI[] , const int neqnI ) const
{
  SANS_ASSERT_MSG( neqnL == nDOFL_, "neqnL = %d  nDOFL = %d", neqnL, nDOFL_ );
  SANS_ASSERT_MSG( neqnI == nDOFI_, "neqnI = %d  nDOFI = %d", neqnI, nDOFI_ );

  ParamTL paramL;             // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;                 // unit normal for left element (points to right element)

  ArrayQ<T> qI;               // interface solution
  ArrayQ<T> qL;               // solution
  VectorArrayQ<T> aL;         // auxiliary variable (gradient)

  QuadPointCellType sRefL;    // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // left reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // parameters
  paramfldElemL_.eval( sRefL, paramL );

  // unit normal
  xfldElemTrace_.unitNormal( sRefTrace, nL ); //this always points from L to R
  nL *= normalSign_; //Flip normal if needed (i.e. if the cell is to the right of the trace)

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemTrace_.evalBasis( sRefTrace, phiI_, nDOFI_ );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );

  if (needsSolutionGradient)
    afldElemL_.evalFromBasis( phiL_, nDOFL_, aL );
  else
    aL = 0;

  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );

  qfldElemTrace_.evalFromBasis( phiI_, nDOFI_, qI );

  weightedIntegrand( nL,  paramL,
                     qL,  aL,
                     qI,
                     integrandCellL, neqnL,
                     integrandIntI, neqnI );
}

template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class ElementParamL >
void
IntegrandInteriorTrace_HDG<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     ElementParamL>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemInteriorTraceType& mtxElemL,
            JacobianElemInteriorTraceType& mtxElemI ) const
{
  // Must be a multiple of PDE::N
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFI == nDOFI_);

  SANS_ASSERT(mtxElemI.nTest == nDOFI_);
  SANS_ASSERT(mtxElemI.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemI.nDOFI == nDOFI_);

  ParamTL paramL;             // Elemental parameters (such as grid coordinates and distance functions)
  VectorX nL;                 // unit normal for left element (points to right element)

  ArrayQ<T> qI;               // interface solution
  ArrayQ<T> qL;               // solution
  VectorArrayQ<T> aL;         // auxiliary variable (gradient)

  QuadPointCellType sRefL;    // reference-element coordinates (s,t)

  const bool needsSolutionGradient = pde_.hasFluxViscous();

  // left reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // parameters
  paramfldElemL_.eval( sRefL, paramL );

  // unit normal
  xfldElemTrace_.unitNormal( sRefTrace, nL ); //this always points from L to R
  nL *= normalSign_; //Flip normal if needed (i.e. if the cell is to the right of the trace)

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_, nDOFL_ );
  qfldElemTrace_.evalBasis( sRefTrace, phiI_, nDOFI_ );

  // solution value
  qfldElemL_.evalFromBasis( phiL_, nDOFL_, qL );
  qfldElemTrace_.evalFromBasis( phiI_, nDOFI_, qI );

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL, qSurrealI;                // solution
  VectorArrayQ<SurrealClass> aSurrealL;                     // gradient

  qSurrealL = qL;
  qSurrealI = qI;

  if (needsSolutionGradient)
  {
    afldElemL_.evalFromBasis( phiL_, nDOFL_, aL );
    aSurrealL = aL;
  }
  else
  {
    aL = 0;
    aSurrealL = 0;
  }
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL_, nDOFL_ );

  MatrixQ<Real> PDE_q = 0, PDE_a = 0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandISurreal( nDOFI_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealI, n).deriv(slot + n - nchunk) = 1;
    slot += PDE::N;

    integrandLSurreal = 0;
    integrandISurreal = 0;

    // compute the integrand
    weightedIntegrand( nL,
                       paramL,
                       qSurrealL, aL,
                       qSurrealI,
                       integrandLSurreal.data(), nDOFL_,
                       integrandISurreal.data(), nDOFI_ );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_q(i,j) += dJ*phiL_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFI_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandISurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemI.PDE_q(i,j) += dJ*phiL_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;


    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealI, n).deriv(slot + n - nchunk) = 0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFI_; j++)
          mtxElemL.PDE_qI(i,j) += dJ*phiI_[j]*PDE_q;
      }

      for (int i = 0; i < nDOFI_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandISurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFI_; j++)
          mtxElemI.PDE_qI(i,j) += dJ*phiI_[j]*PDE_q;
      }
    } // if slot
    slot += PDE::N;

  } // nchunk


  // loop over derivative chunks to computes derivatives w.r.t aL
  if (needsSolutionGradient)
  {
    for (int nchunk = 0; nchunk < PhysDim::D*PDE::N; nchunk += nDeriv)
    {
      // associate derivative slots with solution variables

      int slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < PDE::N; n++)
            DLA::index(aSurrealL[d], n).deriv(slot + n - nchunk) = 1;
        slot += PDE::N;
      }

      integrandLSurreal = 0;
      integrandISurreal = 0;

      // compute the integrand
      weightedIntegrand( nL,
                         paramL,
                         qL, aSurrealL,
                         qI,
                         integrandLSurreal.data(), nDOFL_,
                         integrandISurreal.data(), nDOFI_ );

      // accumulate derivatives into element jacobian
      slot = 0;
      for (int d = 0; d < PhysDim::D; d++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int n = 0; n < PDE::N; n++)
            DLA::index(aSurrealL[d], n).deriv(slot + n - nchunk) = 0; // Reset the derivative

          for (int i = 0; i < nDOFL_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_a,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemL.PDE_a(i,j)(0,d) += dJ*phiL_[j]*PDE_a;
          }

          for (int i = 0; i < nDOFI_; i++)
          {
            // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
            for (int m = 0; m < PDE::N; m++)
              for (int n = 0; n < PDE::N; n++)
                DLA::index(PDE_a,m,n) = DLA::index(integrandISurreal[i], m).deriv(slot + n - nchunk);

            for (int j = 0; j < nDOFL_; j++)
              mtxElemI.PDE_a(i,j)(0,d) += dJ*phiL_[j]*PDE_a;
          }
        }
        slot += PDE::N;
      }
    } // nchunk
  } //  needsSolutionGradient
}

template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class ElementParamL >
template <class Tq, class Tg, class Ti>
void
IntegrandInteriorTrace_HDG<PDE>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     ElementParamL>::
weightedIntegrand( const VectorX& nL,
                   const ParamTL& paramL,
                   const ArrayQ<Tq>& qL, const VectorArrayQ<Tg>& aL,
                   const ArrayQ<Tq>& qI,
                   ArrayQ<Ti> integrandCellL[], const int neqnL,
                   ArrayQ<Ti> integrandIntI[], const int neqnI ) const
{
  ArrayQ<Ti> FnL = 0;          // normal flux

  for (int k = 0; k < neqnL; k++)
    integrandCellL[k] = 0;

  // advective flux term: FadvL(qL, qI)
  if (pde_.hasFluxAdvective())
  {
    pde_.fluxAdvectiveUpwind( paramL, qL, qI, nL, FnL );
  }

  // viscous flux term: Fvis(u,ax,ay)
  if (pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> FL = 0;
    pde_.fluxViscous( paramL, qL, aL, FL );
    FnL += dot(nL,FL);
  }

//  Real nOrder = (Real) qfldElemL_.basis()->order();

  // HDG stabilization term: tau*(q - qI)
  MatrixQ<Tq> tauL = 0;
//  Real lengthL = xfldElemL_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  disc_.evalStabilization( paramL, nL, lengthL_, qI, tauL );

  FnL += tauL*(qL - qI);

  for (int k = 0; k < neqnL; k++)
    integrandCellL[k] += phiL_[k]*FnL;

  //Interior equations
  for (int k = 0; k < neqnI; k++)
    integrandIntI[k] = -phiI_[k]*FnL;

}


#if 1
template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class ElementParamL >
template <class Ti>
void
IntegrandInteriorTrace_HDG<PDE>::
BasisWeighted_AUX<T, TopoDimTrace, TopologyTrace,
                     TopoDimCell,  TopologyL,     ElementParamL>::
operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const
{
  SANS_ASSERT_MSG( neqnL == nDOFL_, "neqnL = %d  nDOFL = %d", neqnL, nDOFL_ );

  VectorX nL;                 // unit normal for left element (points to right element)

  ArrayQ<T> qI;               // interface solution

  QuadPointCellType sRefL;    // reference-element coordinates (s,t)
  ParamTL paramI;              // Elemental parameters (such as grid coordinates and distance functions)

  // left reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  paramfldElemL_.eval( sRefL, paramI );

  // unit normal
  xfldElemTrace_.unitNormal( sRefTrace, nL ); //this always points from L to R
  nL *= normalSign_; //Flip normal if needed (i.e. if the cell is to the right of the trace)

  // basis value
  ArrayQ<T> qL = 0;
  VectorArrayQ<T> aL = 0;
  qfldElemL_.evalBasis( sRefL, phiL_.data(), phiL_.size() );
  qfldElemL_.evalFromBasis( phiL_.data(), phiL_.size(), qL );
//  afldElemL_.evalFromBasis( phiL_.data(), phiL_.size(), aL );

  qfldElemTrace_.evalBasis( sRefTrace, phiI_.data(), phiI_.size() );

  // solution value
  qfldElemTrace_.evalFromBasis( phiI_.data(), phiI_.size(), qI );

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0.0;

  // auxiliary variable definition

  MatrixQ<T> dudq = 0; // conservation variable jacobians dU(Q)/dQ
  pde_.jacobianMasterState( paramI, qL, dudq );

  DLA::MatrixS<PhysDim::D, PhysDim::D, MatrixQ<Ti>> K = 0;  // diffusion matrix
  pde_.diffusionViscous( paramI, qL, aL, K ); // boundary state, interior gradient

  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient)
    {
      // - (phiL nL)*qI
      VectorArrayQ<T> qInL = nL*qI;

      for (int k = 0; k < neqnL; k++)
        integrandL[k] += -phiL_[k]*qInL;
    }
    else if (disc_.auxVersion_ == AugGradient)
    {
      // - (phiL nL)*qB
      VectorArrayQ<T> qInL;

      for (int d = 0; d < PhysDim::D; d++)
        qInL[d] = qI*nL[d];

      VectorArrayQ<T> dun;
      for (int i = 0; i < PhysDim::D; i++)
        dun[i] = dudq*qInL[i];

      VectorArrayQ<Ti> tmpB2 = K*dun;

      for (int k = 0; k < neqnL; k++)
        integrandL[k] = -phiL_[k]*tmpB2;
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandInteriorTrace_HDG::BasisWeighted_AUX::operator() - Unknown auxiliary variable!" );
  }

  // viscous flux dual consistency term?
  //jump in q
//  VectorArrayQ<T> dqn;
//
//  for (int d = 0; d < PhysDim::D; d++)
//    dqn[d] = nL[d]*(qI - qL);
//
//  VectorArrayQ<T> dunB;
//  for (int i = 0; i < PhysDim::D; i++)
//    dunB[i] = dudq*dqn[i];
//
//  VectorArrayQ<Ti> tmpB = K*dunB;
//
//  for (int k = 0; k < neqnL; k++)
//    integrandL[k] += phiL_[k]*tmpB;

}
#endif

template <class PDE>
template< class T, class TopoDimTrace, class TopologyTrace,
                   class TopoDimCell,  class TopologyL,     class ElementParamL >
void
IntegrandInteriorTrace_HDG<PDE>::
FieldWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell,  TopologyL,     ElementParamL>::operator()(
    const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCellL,
                                         IntegrandTraceType& integrandIntI ) const
{
  VectorX X;                  // physical coordinates
  VectorX nL;                 // unit normal for left element (points to right element)

  ArrayQ<T> qL;               // solution
  VectorArrayQ<T> aL;         // auxiliary variable (gradient)
  ArrayQ<T> qI;               // interface solution

  ArrayQ<T> wL;               // weight
  VectorArrayQ<T> bL;         // auxiliary variable (gradient) weight
  ArrayQ<T> wI;               // interface weight

  QuadPointCellType sRefL;    // reference-element coordinates (s,t)

  ArrayQ<T> FnL = 0;          // normal advective flux

  const bool needsSolutionGradient = pde_.hasFluxViscous() || (pde_.hasSource() && pde_.needsSolutionGradientforSource());

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal
  xfldElemTrace_.unitNormal( sRefTrace, nL ); //this always points from L to R
  nL *= normalSign_; //Flip normal if needed (i.e. if the cell is to the right of the trace)

  // basis value
  qfldElemL_.evalBasis( sRefL, phiL_.data(), phiL_.size() );
  qIfldElemTrace_.evalBasis( sRefTrace, phiI_.data(), phiI_.size() );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL_.data(), phiL_.size(), qL );

  if (needsSolutionGradient)
    afldElemL_.evalFromBasis( phiL_.data(), phiL_.size(), aL );
  else
    aL = 0;

  qIfldElemTrace_.evalFromBasis( phiI_.data(), phiI_.size(), qI );

  // weight basis
  wfldElemL_.evalBasis( sRefL, wphiL_.data(), wphiL_.size() );
  wIfldElemTrace_.evalBasis( sRefTrace, wphiI_.data(), wphiI_.size() );

  // weight value
  wfldElemL_.evalFromBasis( wphiL_.data(), wphiL_.size(), wL );
  wIfldElemTrace_.evalFromBasis( wphiI_.data(), wphiI_.size(), wI );

  // weight basis
  bfldElemL_.evalBasis( sRefL, wphiL_.data(), wphiL_.size() );

  // weight value
  bfldElemL_.evalFromBasis( wphiL_.data(), wphiL_.size(), bL );

  // advective flux term: FadvL(qL, qI), FadvR(qR, qI)
  if (pde_.hasFluxAdvective())
  {
    pde_.fluxAdvectiveUpwind( X, qL, qI, nL, FnL );
  }
  // viscous flux term: Fvis(u,ax,ay)

  if (pde_.hasFluxViscous())
  {
    VectorArrayQ<T> FL = 0;

    pde_.fluxViscous( X, qL, aL, FL );

    FnL += dot(nL,FL);
  }

  // HDG stabilization term: tau*(q - qI)
  MatrixQ<T> tauL = 0;

  disc_.evalStabilization( X, nL, lengthL_, qI, tauL );

  FnL += tauL*(qL - qI);

  integrandCellL.PDE = dot(wL,FnL);

  // auxiliary variable definition
  integrandCellL.Au = 0.0;

  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient)
    {
      // -(phiL nL + phiR nR)*qI
      VectorArrayQ<T> qInL;

      for (int d = 0; d < D; d++)
        qInL[d] = qI*nL[d];

      integrandCellL.Au = -dot( bL, qInL );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandInteriorTrace_HDG::FieldWeighted::operator() - Unknown auxiliary variable!" );
  }

  integrandIntI = -dot(wI,FnL);
}

}

#endif  // INTEGRANDINTERIORTRACE_HDG_H
