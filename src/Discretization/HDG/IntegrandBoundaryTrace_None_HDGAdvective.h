// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_NONE_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_NONE_HDGADVECTIVE_H_

// boundary integrand operators

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/output_std_vector.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{

//----------------------------------------------------------------------------//
namespace IntegrandBoundaryTrace_None_HDGAdv_detail
{
// worker object that can be partially specialized to do specific tasks depending on
// the type of dicretization (as in DiscTag)
template <class PDENDConvert, class NDBCVecCategory, class DiscTag>
struct worker;
} // namespace IntegrandBoundaryTrace_None_HDGAdv_detail

//----------------------------------------------------------------------------//
// HDG element boundary integrand: None

template <class PDE_, class NDBCVector, class DiscTag_>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::None>, DiscTag_> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::None>, DiscTag_> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::None Category;
  typedef NDVectorCategory<NDBCVector, Category> NDBCVecCategory;
  typedef DiscTag_ DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(const PDE& pde,
                         const BCBase& bc,
                         const std::vector<int>& boundaryGroups,
                         const DiscretizationHDG<PDE>& disc) :
    pde_(pde), bc_(bc),  boundaryGroups_(boundaryGroups), disc_(disc) {}

  virtual ~IntegrandBoundaryTrace() {}

  std::size_t nBoundaryGroups() const { return boundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return boundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class Topology, class ElementParam >
  class BasisWeighted
  {
  private:
    friend struct IntegrandBoundaryTrace_None_HDGAdv_detail::worker<PDE, NDBCVecCategory, DiscTag>; // make a friend struct to do some labor

  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology    > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimCell, Topology> ElementQFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;
    typedef typename ElementParam::T ParamT;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace,
                   const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementQFieldTrace& qIfldTrace ) :
      pde_(pde), disc_(disc), bc_(bc),
      xfldElemTrace_(xfldElemTrace),
      canonicalTrace_(canonicalTrace),
      xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
      qfldElem_(qfldElem),
      qIfldTrace_(qIfldTrace),
      paramfldElem_( paramfldElem ),
      nDOFElem_(qfldElem_.nDOF()),
      nDOFTrace_(qIfldTrace_.nDOF()),
      phi_(nDOFElem_),
      phiI_(nDOFTrace_) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOF() const { return nDOFElem_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, dJ, sRefTrace, rsdPDEElemL, rsdINTElemTrace);
    }

    FRIEND_CALL_WITH_DERIVED

  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const Real dJ, const QuadPointTraceType& sRefTrace,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> phiI_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>,       TopoDimCell , Topology>& qfldElem,
            const Element<ArrayQ<T>,       TopoDimTrace, TopologyTrace>& qIfldTrace ) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                            TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                  xfldElemTrace, canonicalTrace,
                                                                  paramfldElem, qfldElem, qIfldTrace);
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> boundaryGroups_;
  const DiscretizationHDG<PDE>& disc_;
};


template <class PDE_, class NDBCVector, class DiscTag_>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam>
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::None>, DiscTag_>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology, ElementParam>::
operator()(const BC& bc, const Real dJ, const QuadPointTraceType& sRefTrace,
           DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const
{
  SANS_ASSERT_MSG( rsdPDEElemL.m() == nDOFElem_, "neqnL = %d  nDOFL = %d", rsdPDEElemL.m(), nDOFElem_ );
  SANS_ASSERT_MSG( rsdINTElemTrace.m() == nDOFTrace_, "neqnTrace = %d  nDOFI = %d", rsdINTElemTrace.m(), nDOFTrace_ );

  // adjacent area-element reference coords
  QuadPointCellType sRef;   // reference-element coordinates (s,t)
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  ParamT param;                // physical coordinates
  paramfldElem_.eval( sRef, param );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );
  qIfldTrace_.evalBasis( sRefTrace, phiI_.data(), phiI_.size() );

  // solution value, gradient
  ArrayQ<T> q;              // solution
  ArrayQ<T> qI;             // interface solution
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  qIfldTrace_.evalFromBasis( phiI_.data(), phiI_.size(), qI );

  // PDE residual: weak form boundary integral
  IntegrandBoundaryTrace_None_HDGAdv_detail::worker<PDE,NDBCVecCategory,DiscTag>::weightedIntegrand(
      this, dJ, sRefTrace, sRef, param, q, qI, rsdPDEElemL, rsdINTElemTrace );
}

//----------------------------------------------------------------------------//
namespace IntegrandBoundaryTrace_None_HDGAdv_detail
{
// DiscTag = HDGAdv
template <class PDENDConvert, class NDBCVecCategory>
struct worker<PDENDConvert, NDBCVecCategory, HDGAdv>
{
  typedef IntegrandBoundaryTrace<PDENDConvert,NDBCVecCategory,HDGAdv> IntegrandType;

  template<class T>
  using ArrayQ = typename IntegrandType::template ArrayQ<T>;

  template<class T>
  using VectorArrayQ = typename IntegrandType::template VectorArrayQ<T>;

  template<class T, class TopoDimTrace,class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParamL>
  using BasisWeightedIntegrandClass =
      typename IntegrandType::template BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>;

  // compute the weighted integrands
  template<class T, class TopoDimTrace,class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParamL,
           class Tq, class Ti>
  static void
  weightedIntegrand(
      const BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>* integrandFcn,
      const Real dJ,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            QuadPointTraceType& sRefTrace,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            QuadPointCellType& sRefL,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            ParamT& paramL,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qI,
      DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace )
  {
    typedef BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL> BasisWeightedIntegrandType;
    typedef typename BasisWeightedIntegrandType::VectorX VectorX;

    SANS_ASSERT_MSG(!integrandFcn->pde_.hasFluxViscous(), "Should not have viscous flux!");

    // trace unit normal
    VectorX nL; // unit normal for left element (points to right element)
    traceUnitNormal( integrandFcn->xfldElem_, sRefL,
                     integrandFcn->xfldElemTrace_, sRefTrace, nL);
    // adjust the sign of the trace normal vector so that it points out of the left adjacent cell element

    VectorArrayQ<Ti> FL = 0.0, FI = 0.0;
    integrandFcn->pde_.fluxAdvective( paramL, qL, FL );
    integrandFcn->pde_.fluxAdvective( paramL, qI, FI );

    const ArrayQ<Ti> FnL = dot(nL,FL);

    rsdPDEElemL = 0.0;
    for (int k = 0; k < integrandFcn->nDOFElem_; k++)
      rsdPDEElemL[k] += FnL*integrandFcn->phi_[k]*dJ;

    const ArrayQ<Ti> FnI = dot(nL,FI);
    const ArrayQ<Ti> Ftmp = dJ*(FnI - FnL);

    rsdINTElemTrace = 0.0;
    for (int k = 0; k < integrandFcn->nDOFTrace_; k++)
      rsdINTElemTrace[k] += Ftmp*integrandFcn->phiI_[k];
  }
};

// DiscTag = HDGAdv_manifold
template <class PDENDConvert, class NDBCVecCategory>
struct worker<PDENDConvert, NDBCVecCategory, HDGAdv_manifold>
{
  typedef IntegrandBoundaryTrace<PDENDConvert,NDBCVecCategory,HDGAdv_manifold> IntegrandType;

  template<class T>
  using ArrayQ = typename IntegrandType::template ArrayQ<T>;

  template<class T>
  using VectorArrayQ = typename IntegrandType::template VectorArrayQ<T>;

  template<class T, class TopoDimTrace,class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParamL>
  using BasisWeightedIntegrandClass =
      typename IntegrandType::template BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>;

  // compute the weighted integrands
  template<class T, class TopoDimTrace,class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParamL,
           class Tq, class Ti>
  static void
  weightedIntegrand(
      const BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>* integrandFcn,
      const Real dJ,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            QuadPointTraceType& sRefTrace,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            QuadPointCellType& sRefL,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            ParamT& paramL,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qI,
      DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace )
  {
    typedef BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL> BasisWeightedIntegrandType;
    typedef typename BasisWeightedIntegrandType::VectorX VectorX;

    SANS_ASSERT_MSG(!integrandFcn->pde_.hasFluxViscous(), "Should not have viscous flux!");

    // trace unit normal
    VectorX nL; // unit normal for left element (points to right element)
    traceUnitNormal( integrandFcn->xfldElem_, sRefL,
                     integrandFcn->xfldElemTrace_, sRefTrace, nL);
    // adjust the sign of the trace normal vector so that it points out of the left adjacent cell element

    // basis vectors
    VectorX e01L;// basis direction vector
    integrandFcn->xfldElem_.unitTangent(sRefL, e01L);

    DLA::VectorS<TopoDimCell::D, VectorX> e0L; // basis direction vector
    e0L[0] = e01L;

    //----------------------------------------------------------------------------//
    // contribution to PDE weighted residual

    VectorArrayQ<Ti> FL = 0.0;

    integrandFcn->pde_.fluxAdvective( paramL, e01L, qL, FL );

    const ArrayQ<Ti> FnL = dot(nL,FL);

    rsdPDEElemL = 0.0;

    for (int k = 0; k < integrandFcn->nDOFElem_; k++)
      rsdPDEElemL[k] += dJ*integrandFcn->phi_[k]*FnL;

    //----------------------------------------------------------------------------//
    // residuals corresponding to the flux matching equation at interior trace
    const ArrayQ<T> dummyEqn = qI - ArrayQ<Real>(integrandFcn->pde_.varMatchDummy_); // dummy interface equation

    rsdINTElemTrace = 0.0;
    for (int k = 0; k < integrandFcn->nDOFTrace_; k++)
      rsdINTElemTrace[k] += dJ*integrandFcn->phiI_[k]*dummyEqn;
  }
};

} // namespace IntegrandBoundaryTrace_None_HDGAdv_detail

} // namespace SANS

#endif /* SRC_DISCRETIZATION_HDG_INTEGRANDBOUNDARYTRACE_NONE_HDGADVECTIVE_H_ */
