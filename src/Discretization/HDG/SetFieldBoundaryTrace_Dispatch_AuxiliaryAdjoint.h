// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETFIELDBOUNDARYTRACE_DISPATCH_AUXILIARYADJOINT_H
#define SETFIELDBOUNDARYTRACE_DISPATCH_AUXILIARYADJOINT_H

// boundary-trace integral jacobian functions

#include "Discretization/HDG/SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

#if 0
//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
class SetFieldBoundaryTrace_FieldTrace_Dispatch_AuxiliaryAdjoint_impl
{
public:
  SetFieldBoundaryTrace_FieldTrace_Dispatch_AuxiliaryAdjoint_impl(const XFieldType& xfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                             const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                             const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                             const int* quadratureorder, int ngroup ) :
    xfld_(xfld),
    qfld_(qfld),
    afld_(afld),
    qIfld_(qIfld),
    lgfld_(lgfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Can't compute auxiliary adjoints with mitLG BC's");
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class VectorArrayQ>
SetFieldBoundaryTrace_FieldTrace_Dispatch_AuxiliaryAdjoint_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, VectorArrayQ>
SetFieldBoundaryTrace_FieldTrace_Dispatch_AuxiliaryAdjoint(const XFieldType& xfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                              const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& wfld,
                                              const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& wIfld,
                                              const Field<PhysDim, TopoDim, ArrayQ>& mufld,
                                              const int* quadratureorder, int ngroup  )
{
  return { xfld, qfld, afld, qIfld, lgfld, wfld, bfld, wIfld, mufld, quadratureorder, ngroup };
}
#endif

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class TensorMatrixQ>
class SetFieldBoundaryTrace_Dispatch_AuxiliaryAdjoint_impl
{
public:
  SetFieldBoundaryTrace_Dispatch_AuxiliaryAdjoint_impl(
      const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wfld,
      const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
      const Field<PhysDim, TopoDim, ArrayQ>& wIfld,
      const int quadOrderBTrace[], const int nBTraceGroup ) :
        invJacAUX_a_bcell_(invJacAUX_a_bcell),
        xfld_(xfld), qfld_(qfld), afld_(afld), qIfld_(qIfld),
        wfld_(wfld), bfld_(bfld), wIfld_(wIfld),
        quadOrderBTrace_(quadOrderBTrace), nBTraceGroup_(nBTraceGroup)
  {
    // Nothing
  }

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcnBTrace)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        SetFieldBoundaryTrace_HDG_AuxiliaryAdjoint<Surreal>( fcnBTrace, invJacAUX_a_bcell_),
        xfld_, (qfld_, afld_, wfld_, bfld_), (qIfld_, wIfld_), quadOrderBTrace_, nBTraceGroup_ );
  }

protected:
  const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell_;

  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& afld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;

  const Field<PhysDim, TopoDim, ArrayQ>& wfld_;
  const Field<PhysDim, TopoDim, VectorArrayQ>& bfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& wIfld_;

  const int *quadOrderBTrace_, nBTraceGroup_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim,
         class ArrayQ, class VectorArrayQ, class TensorMatrixQ>
SetFieldBoundaryTrace_Dispatch_AuxiliaryAdjoint_impl<Surreal, XFieldType, PhysDim, TopoDim,
                                                     ArrayQ, VectorArrayQ, TensorMatrixQ>
SetFieldBoundaryTrace_Dispatch_AuxiliaryAdjoint(
    const FieldDataMatrixD_BoundaryCell<TensorMatrixQ>& invJacAUX_a_bcell,
    const XFieldType& xfld,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld,
    const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
    const Field<PhysDim, TopoDim, ArrayQ>& wfld,
    const Field<PhysDim, TopoDim, VectorArrayQ>& bfld,
    const Field<PhysDim, TopoDim, ArrayQ>& wIfld,
    const int quadOrderBTrace[], const int nBTraceGroup)
{
  return { invJacAUX_a_bcell, xfld, qfld, afld, qIfld, wfld, bfld, wIfld,
           quadOrderBTrace, nBTraceGroup};
}

}

#endif //SETFIELDBOUNDARYTRACE_DISPATCH_AUXILIARYADJOINT_H
