// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_LINEARSCALAR_SANSLG_HDG_H
#define INTEGRANDBOUNDARYTRACE_LINEARSCALAR_SANSLG_HDG_H

// boundary integrand operators

#include <ostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/call_with_derived.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/HDG/DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// HDG element boundary integrand: LinearScalar_sansLG

template <class PDE_, class NDBCVector>
class IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, HDG> :
    public IntegrandBoundaryTraceType< IntegrandBoundaryTrace<PDE_, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, HDG> >
{
public:
  typedef PDE_ PDE;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef HDG DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D, Real> VecReal; // Auxilliary integrand

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(
    const PDE& pde, const BCBase& bc, const std::vector<int>& BoundaryGroups, const DiscretizationHDG<PDE>& disc) :
    pde_(pde), bc_(bc),  BoundaryGroups_(BoundaryGroups), disc_(disc) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

  virtual ~IntegrandBoundaryTrace() {}

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<T>,       TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<T>, TopoDimCell, Topology> ElementAFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef ArrayQ<T> IntegrandCellType;
    typedef ArrayQ<T> IntegrandTraceType;

    BasisWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem,
                   const ElementQFieldTrace& qIfldTrace ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem), afldElem_(afldElem),
                   qIfldTrace_(qIfldTrace),
                   paramfldElem_( paramfldElem ),
                   nDOFElem_(qfldElem_.nDOF()),
                   nDOFTrace_(qIfldTrace_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      phi_.resize(nDOFElem_,0);
      phiT_.resize(nDOFTrace_,0);
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCell[],   const int neqnCell,
                                                          ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandCell, neqnCell,
                                                           integrandTrace, neqnTrace );
    }

    FRIEND_CALL_WITH_DERIVED

  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandCell[], const int neqnCell,
                                                                       ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> phiT_;
  };

  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                      class TopoDimCell,  class Topology, class ElementParam>
  class BasisWeighted_AUX
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>,       TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Ta>, TopoDimCell, Topology> ElementAFieldCell;
    typedef Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Ta>::type Ts;

    typedef VectorArrayQ<Ts> IntegrandType;

    BasisWeighted_AUX( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                       const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                       const ElementParam& paramfldElem,
                       const ElementQFieldCell& qfldElem,
                       const ElementAFieldCell& afldElem,
                       const ElementQFieldTrace& qIfldTrace ) :
                         pde_(pde), disc_(disc), bc_(bc),
                         xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                         xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                         qfldElem_(qfldElem), afldElem_(afldElem),
                         qIfldTrace_(qIfldTrace),
                         paramfldElem_( paramfldElem ),
                         nDOFElem_(qfldElem_.nDOF()),
                         nDOFTrace_(qIfldTrace_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      phi_.resize(nDOFElem_,0);
      phiT_.resize(nDOFTrace_,0);
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    template <class Ti>
    void operator()( const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandL, neqnL );
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC, class Ti>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> phiT_;
  };

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam>
  class FieldWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , Topology     > ElementXFieldCell;

    typedef Element<ArrayQ<Tq>,       TopoDimCell, Topology> ElementQFieldCell;
    typedef Element<VectorArrayQ<Tq>, TopoDimCell, Topology> ElementAFieldCell;
    typedef Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef Element<ArrayQ<Tw>,       TopoDimCell, Topology> ElementWFieldCell;
    typedef Element<VectorArrayQ<Tw>, TopoDimCell, Topology> ElementBFieldCell;
    typedef Element<ArrayQ<Tw>, TopoDimTrace, TopologyTrace> ElementWFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    typedef typename promote_Surreal<Tq, Tw>::type Ti;

    typedef IntegrandHDG< Ti, Ti > IntegrandCellType;
    typedef Ti IntegrandTraceType;

    FieldWeighted( const PDE& pde, const DiscretizationHDG<PDE>& disc, const BCBase& bc,
                   const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                   const ElementParam& paramfldElem,
                   const ElementQFieldCell& qfldElem,
                   const ElementAFieldCell& afldElem,
                   const ElementWFieldCell& wfldElem,
                   const ElementBFieldCell& bfldElem,
                   const ElementQFieldTrace& qIfldTrace,
                   const ElementWFieldTrace& wIfldTrace ) :
                   pde_(pde), disc_(disc), bc_(bc),
                   xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
                   xfldElem_(get<-1>(paramfldElem)), // XField must be the last parameter
                   qfldElem_(qfldElem), afldElem_(afldElem),
                   wfldElem_(wfldElem), bfldElem_(bfldElem),
                   qIfldTrace_(qIfldTrace),
                   wIfldTrace_(wIfldTrace),
                   paramfldElem_( paramfldElem ),
                   nDOFElem_(qfldElem_.nDOF()),
                   nDOFTrace_(qIfldTrace_.nDOF())
    {
      SANS_ASSERT( qfldElem_.basis() == afldElem_.basis() );
      SANS_ASSERT( wfldElem_.basis() == bfldElem_.basis() );

      phi_.resize(nDOFElem_,0);
      wphi_.resize(wfldElem_.nDOF(),0);
    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total DOFs
    int nDOFCell() const { return nDOFElem_; }
    int nDOFTrace() const { return nDOFTrace_; }

    // boundary element trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCell,
                                                          IntegrandTraceType& integrandTrace ) const
    {
      call_with_derived<NDBCVector>(*this, bc_, sRefTrace, integrandCell, integrandTrace);
    }

    FRIEND_CALL_WITH_DERIVED
  protected:
    template<class BC>
    void operator()( const BC& bc, const QuadPointTraceType& RefTrace, IntegrandCellType& integrandCell,
                                                                       IntegrandTraceType& integrandTrace ) const;

    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const BCBase& bc_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
    const ElementAFieldCell& afldElem_;
    const ElementWFieldCell& wfldElem_;
    const ElementBFieldCell& bfldElem_;
    const ElementQFieldTrace& qIfldTrace_;
    const ElementWFieldTrace& wIfldTrace_;
    const ElementParam& paramfldElem_;

    const int nDOFElem_;
    const int nDOFTrace_;
    mutable std::vector<Real> phi_;
    mutable std::vector<Real> wphi_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell,  class Topology, class ElementParam>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<T>,       TopoDimCell,  Topology     >& qfldElem,
            const Element<VectorArrayQ<T>, TopoDimCell,  Topology     >& afldElem,
            const Element<ArrayQ<T>,       TopoDimTrace, TopologyTrace>& qIfldTrace) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, Topology, ElementParam>(pde_, disc_, bc_,
                                                                                              xfldElemTrace, canonicalTrace,
                                                                                              paramfldElem, qfldElem, afldElem,
                                                                                              qIfldTrace);
  }

  template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam >
  BasisWeighted_AUX<Tq, Ta, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand_AUX(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
                const ElementParam& paramfldElem,
                const Element<ArrayQ<Tq>, TopoDimCell, Topology >& qfldElem,
                const Element<VectorArrayQ<Ta>, TopoDimCell, Topology >& afldElem,
                const Element<ArrayQ<Tq>, TopoDimTrace, TopologyTrace>& qIfldTrace) const
  {
    return BasisWeighted_AUX<Tq, Ta, TopoDimTrace, TopologyTrace,
                             TopoDimCell,  Topology, ElementParam>(pde_, disc_, bc_,
                                                                   xfldElemTrace, canonicalTrace,
                                                                   paramfldElem, qfldElem, afldElem, qIfldTrace );
  }

  template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                               class TopoDimCell,  class Topology, class ElementParam>
  FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace,TopoDimCell, Topology, ElementParam>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementParam& paramfldElem,
            const Element<ArrayQ<Tq>,       TopoDimCell,  Topology     >& qfldElem,
            const Element<VectorArrayQ<Tq>, TopoDimCell,  Topology     >& afldElem,
            const Element<ArrayQ<Tw>,       TopoDimCell,  Topology     >& wfldElem,
            const Element<VectorArrayQ<Tw>, TopoDimCell,  Topology     >& bfldElem,
            const Element<ArrayQ<Tq>,       TopoDimTrace, TopologyTrace>& qIfldTrace,
            const Element<ArrayQ<Tw>,       TopoDimTrace, TopologyTrace>& wIfldTrace) const
  {
    return FieldWeighted<Tq, Tw, TopoDimTrace, TopologyTrace, TopoDimCell, Topology, ElementParam>(pde_, disc_, bc_,
                                                                                                   xfldElemTrace, canonicalTrace,
                                                                                                   paramfldElem,
                                                                                                   qfldElem, afldElem,
                                                                                                   wfldElem, bfldElem,
                                                                                                   qIfldTrace, wIfldTrace);
  }

private:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  const DiscretizationHDG<PDE>& disc_;
};


template <class PDE, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell,  class Topology, class ElementParam>
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, HDG>::
BasisWeighted<T,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, ArrayQ<Ti> integrandCell[], const int neqnCell,
                                                              ArrayQ<Ti> integrandTrace[], const int neqnTrace ) const
{
  SANS_ASSERT( (neqnCell == nDOFElem_) && (neqnTrace == nDOFTrace_) );

  VectorX X;                // physical coordinates
  VectorX N;                // unit normal (points out of domain)

  ArrayQ<T> q;              // solution
  VectorArrayQ<T> a;        // auxiliary variable (gradient)
  ArrayQ<T> qI;             // interface solution

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, N );

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

  // interface solution basis
  qIfldTrace_.evalBasis( sRefTrace, phiT_.data(), phiT_.size() );

  // interface solution
  qIfldTrace_.evalFromBasis( phiT_.data(), phiT_.size(), qI );

  // BC coefficients (convert to scalars)
  MatrixQ<Real> Amtx, Bmtx;
  bc.coefficients( X, N, Amtx, Bmtx );
  Real A = Amtx;
  Real B = Bmtx;

  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] = 0;

  ArrayQ<Ti> Fn = 0;

  // PDE residual: weak form boundary integral
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    VectorArrayQ<Ti> F = 0; // PDE flux

    // advective flux
    pde_.fluxAdvective( X, q, F );

    // viscous flux
    pde_.fluxViscous( X, q, a, F );

    Fn += dot(N,F);
  }

  // HDG stabilization term: tau*(q - qI)
  MatrixQ<T> tau = 0;
  Real length = xfldElem_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  disc_.evalStabilization( X, N, length, qI, tau );

  ArrayQ<Ti> Fstab = tau*(q - qI);

  Fn += Fstab;

  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] += phi_[k]*Fn;

  for (int k = 0; k < neqnTrace; k++)
    integrandTrace[k] = -phiT_[k]*Fstab;

  // PDE residual: BC Lagrange multiplier term

  T jan; // NOTE: use of Real rather than ArrayQ/MatrixQ to as this only works for scalar equations

  DLA::VectorS< PhysDim::D, T > ja = 0;
  pde_.jacobianFluxAdvective( X, q, ja );
  jan = dot(N,ja);

  DLA::MatrixS< PhysDim::D, PhysDim::D, Ti > K = 0;
  pde_.diffusionViscous( X, q, a, K );

//  DLA::VectorS< PhysDim::D, T > Kn = K*N;
  VectorArrayQ<Ti> Ka = K*a;
  ArrayQ<Ti> KaN = dot(Ka,N);

  Real denomin = A*A + B*B;
  Ti alpha = (A*jan - B)/denomin;
//  T beta = - A/denomin;

  // BC data (convert to scalar)
  Real bcdata;
  bc.data( X, N, bcdata );
  Ti tmp = A*qI + B*KaN - bcdata;

  Ti tmpPDE = -alpha*tmp;

  for (int k = 0; k < neqnCell; k++)
    integrandCell[k] += phi_[k]*tmpPDE;
}

template <class PDE, class NDBCVector>
template<class Tq, class Ta, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class Topology, class ElementParam >
template<class BC, class Ti>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, HDG>::
BasisWeighted_AUX<Tq,Ta,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, VectorArrayQ<Ti> integrandL[], const int neqnL) const
{
  SANS_ASSERT( (neqnL == nDOFElem_) );

  VectorX X;                  // physical coordinates
  VectorX nL;                 // unit normal (points out of domain)

  ArrayQ<Tq> qL;               // solution
  VectorArrayQ<Ta> aL;        // auxiliary variable (gradient)
  ArrayQ<Tq> qI;               // interface solution

  QuadPointCellType sRef;     // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, nL );
  VectorX nB = -nL;

  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), qL );
  afldElem_.evalFromBasis( phi_.data(), phi_.size(), aL );

  // interface solution basis
  qIfldTrace_.evalBasis( sRefTrace, phiT_.data(), phiT_.size() );

  // interface solution
  qIfldTrace_.evalFromBasis( phiT_.data(), phiT_.size(), qI );

  // BC coefficients (convert to scalars)
  MatrixQ<Real> Amtx, Bmtx;
  bc.coefficients( X, nL, Amtx, Bmtx );
  Real A = Amtx;
  Real B = Bmtx;

  for (int k = 0; k < neqnL; k++)
    integrandL[k] = 0;

  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient)
    {
      DLA::MatrixS< PhysDim::D, PhysDim::D, Tq > K = 0;
      pde_.diffusionViscous( X, qL, aL, K );

      VectorArrayQ<Ti> Ka = K*aL;
      ArrayQ<Ti> KaN = dot(Ka,nL);

      Real denomin = A*A + B*B;
      Real beta = - A/denomin;

      // BC data (convert to scalar)
      Real bcdata;
      bc.data( X, nL, bcdata );
      Ti tmp = A*qI + B*KaN - bcdata;

      VectorArrayQ<Ti> tmpAu = -(qI + beta*tmp)*nL;

      for (int k = 0; k < neqnL; k++)
        integrandL[k] += phi_[k]*tmpAu;
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandBoundaryTrace_LinearScalar_sansLG::BasisWeighted_AUX::operator() - Unknown auxiliary variable!" );
  }
}

template <class PDE, class NDBCVector>
template<class Tq, class Tw, class TopoDimTrace, class TopologyTrace,
                             class TopoDimCell,  class Topology, class ElementParam>
template<class BC>
void
IntegrandBoundaryTrace<PDE, NDVectorCategory<NDBCVector, BCCategory::LinearScalar_sansLG>, HDG>::
FieldWeighted<Tq,Tw,TopoDimTrace,TopologyTrace,TopoDimCell,Topology,ElementParam>::
operator()(const BC& bc, const QuadPointTraceType& sRefTrace, IntegrandCellType& integrandCell,
                                                              IntegrandTraceType& integrandTrace ) const
{
  VectorX X;                // physical coordinates
  VectorX N;                // unit normal (points out of domain)

  ArrayQ<Tq> q;              // solution
  VectorArrayQ<Tq> a;        // solution auxiliary variable (gradient)
  ArrayQ<Tq> qI;             // interface solution

  ArrayQ<Tw> w;              // weight
  VectorArrayQ<Tw> b;        // weight auxiliary variable (gradient)
  ArrayQ<Tw> wI;             // interface weight

  QuadPointCellType sRef;   // reference-element coordinates (s,t)

  // adjacent area-element reference coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, Topology>::eval( canonicalTrace_, sRefTrace, sRef );

  // physical coordinates
  xfldElemTrace_.coordinates( sRefTrace, X );

  // unit normal: points out of domain
  xfldElemTrace_.unitNormal( sRefTrace, N );

  // Solution
  // basis value, gradient
  qfldElem_.evalBasis( sRef, phi_.data(), phi_.size() );

  // solution value, gradient
  qfldElem_.evalFromBasis( phi_.data(), phi_.size(), q );
  afldElem_.evalFromBasis( phi_.data(), phi_.size(), a );

  // interface solution
  qIfldTrace_.eval( sRefTrace, qI );

  // Weight
  // basis value, gradient
  wfldElem_.evalBasis( sRef, wphi_.data(), wphi_.size() );

  // solution value, gradient
  wfldElem_.evalFromBasis( wphi_.data(), wphi_.size(), w );
  bfldElem_.evalFromBasis( wphi_.data(), wphi_.size(), b );

  // interface weight
  wIfldTrace_.eval( sRefTrace, wI );

  // BC coefficients (convert to scalars)
  MatrixQ<Real> Amtx, Bmtx;
  bc.coefficients( X, N, Amtx, Bmtx );
  Real A = Amtx;
  Real B = Bmtx;

  integrandCell = 0;

  ArrayQ<Tq> Fn = 0;

  // PDE residual: weak form boundary integral
  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    DLA::VectorS<PhysDim::D, ArrayQ<Tq> > F = 0;     // PDE flux

    // advective flux
    pde_.fluxAdvective( X, q, F );

    // viscous flux
    pde_.fluxViscous( X, q, a, F );

    Fn += dot(N,F);
  }

  // HDG stabilization term: tau*(q - qI)
  MatrixQ<Tq> tau = 0;
  Real length = xfldElem_.jacobianDeterminant()/xfldElemTrace_.jacobianDeterminant();
  disc_.evalStabilization( X, N, length, qI, tau );

  ArrayQ<Tq> Fstab = tau*(q - qI);

  Fn += Fstab;

  integrandCell.PDE += dot(w,Fn);

  integrandTrace = -dot(wI,Fstab);

  // PDE residual: BC Lagrange multiplier term

  Tq jan; // NOTE: use of T rather than ArrayQ/MatrixQ as this only works for scalar equations

  DLA::VectorS< PhysDim::D, Tq > ja = 0;
  pde_.jacobianFluxAdvective( X, q, ja );
  jan = dot(N,ja);

  DLA::MatrixS< PhysDim::D, PhysDim::D, Tq > K = 0;
  pde_.diffusionViscous( X, q, a, K );

  DLA::VectorS< PhysDim::D, Tq > Kn = K*N;
  VectorArrayQ<Tq> Ka = K*a;
  ArrayQ<Tq> KaN = dot(Ka,N);

  Real denomin = A*A + B*B;
  Real alpha = (A*jan - B)/denomin;
  Real beta = - A/denomin;

  // BC data (convert to scalar)
  Real bcdata;
  bc.data( X, N, bcdata );
  Tq tmp = A*qI + B*KaN - bcdata;

  Tq tmpPDE = -alpha*tmp;

  integrandCell.PDE += dot(w, tmpPDE);

  // AUX residual
  if (pde_.hasFluxViscous())
  {
    if (disc_.auxVersion_ == Gradient )
    {
      VectorArrayQ<Tq> tmpAu = -(qI + beta*tmp)*N;

      integrandCell.Au = dot(b,tmpAu);
    }
    else
      SANS_DEVELOPER_EXCEPTION( "IntegrandBoundaryTrace_LinearScalar_sansLG::FieldWeighted::operator() - Unknown auxiliary variable!" );
  }
}

}

#endif  // INTEGRANDBOUNDARYTRACE_LINEARSCALAR_SANSLG_HDG_H
