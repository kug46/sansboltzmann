// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_HDG_HB_H
#define JACOBIANCELL_HDG_HB_H

// HDG cell integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "Discretization/HDG/DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PhysDim, class QFieldCellGroupType, class MatrixQ, template <class> class SparseMatrix>
void
JacobianCell_HDG_HB_Group_ScatterAdd(
    const QFieldCellGroupType& qfld,
    const int elem, const int nDOF, const int ntimes,
    int mapDOFGlobal_q[],
    const SANS::DLA::MatrixD<MatrixQ>& mtxElemPDE_qMat,
    SparseMatrix<MatrixQ>& mtxGlobalPDE_qMat )
{
  // jacobian wrt u
  qfld.associativity( elem ).getGlobalMapping( mapDOFGlobal_q, nDOF );

  mtxGlobalPDE_qMat.scatterAdd( mtxElemPDE_qMat, mapDOFGlobal_q, nDOF );
}

#if 0
//----------------------------------------------------------------------------//
template <class PhysDim, class QFieldCellGroupType, class MatrixQ, template <class> class SparseMatrix>
void
JacobianCell_HDG_Group_ScatterAdd(
    const QField2DArea<Topology,PDE>& qfld,
    const QField2DArea<Topology,PDE>& qxfld,
    const QField2DArea<Topology,PDE>& qyfld,
    const int elem, const int nDOF,
    int mapDOFGlobal[],
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemPDE_q,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemPDE_ax,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemPDE_ay,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAux_q,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAux_ax,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAux_ay,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAuy_q,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAuy_ax,
    const SANS::DLA::MatrixD<typename PDE::template MatrixQ<Real>>& mtxElemAuy_ay,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalPDE_q,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalPDE_ax,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalPDE_ay,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAux_q,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAux_ax,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAux_ay,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAuy_q,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAuy_ax,
    SparseMatrix< SANS::DLA::MatrixD< typename PDE::template MatrixQ<Real> > >& mtxGlobalAuy_ay )
{
  mtxGlobalPDE_q.scatterAdd(  mtxElemPDE_q,  elem, elem );
  mtxGlobalAux_q.scatterAdd(  mtxElemAux_q,  elem, elem );
  mtxGlobalAuy_q.scatterAdd(  mtxElemAuy_q,  elem, elem );

  mtxGlobalPDE_ax.scatterAdd( mtxElemPDE_ax, elem, elem );
  mtxGlobalAux_ax.scatterAdd( mtxElemAux_ax, elem, elem );
  mtxGlobalAuy_ax.scatterAdd( mtxElemAuy_ax, elem, elem );

  mtxGlobalPDE_ay.scatterAdd( mtxElemPDE_ay, elem, elem );
  mtxGlobalAux_ay.scatterAdd( mtxElemAux_ay, elem, elem );
  mtxGlobalAuy_ay.scatterAdd( mtxElemAuy_ay, elem, elem );
}
#endif

//----------------------------------------------------------------------------//
// HDG cell integral
//
// topology specific group integral
//
// template parameters:
//   Topology                                       element topology (e.g. Triangle)
//   PDE                                            PDE class
//   Surreal                                        auto-differentiation class (e.g. SurrealS)
//   IntegrandCellFunctor                           integrand functor
//   SparseMatrix                                   sparse matrix class with PDE::MatrixQ elements

template <class Surreal, class Topology, class PhysDim, class TopoDim,
class ArrayQ, class VectorArrayQ,
class IntegrandCellFunctor, class SparseMatrix>
void
JacobianCell_Group_Integral_HDG_HB(
    const IntegrandCellFunctor& fcn, const int t1, const int t2,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<Topology>& xfld,
    const std::vector< typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>*>& qflds,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q )
{
  typedef typename IntegrandCellFunctor::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandCellFunctor::template MatrixQ<Real> MatrixQ;
  typedef DLA::VectorS< PhysDim::D, ArrayQSurreal > GradArrayQSurreal;

  typedef SANS::DLA::MatrixD<MatrixQ> MatrixElemClass;

  typedef typename XField<PhysDim, TopoDim            >::template FieldCellGroupType<Topology> XFieldCellGroupType;
  typedef typename Field< PhysDim, TopoDim, ArrayQ    >::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;

  // element field variables
  ElementXFieldClass xfldElem( xfld.basis() );
  ElementQFieldSurrealClass qfldElemSurreal( qflds[0]->basis() );

  // variables/equations per DOF
  const int nEqn = IntegrandCellFunctor::N;
  const int ntimes = qflds.size();

  // DOFs per element
  const int nDOF = qfldElemSurreal.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobal_q(nDOF);

  // element integral
  GalerkinWeightedIntegral<TopoDim, Topology, IntegrandHDG<ArrayQSurreal,GradArrayQSurreal> >
  integral(quadratureorder, nDOF);

  // element integrand/residual
  std::unique_ptr< IntegrandHDG<ArrayQSurreal,GradArrayQSurreal>[] >
  rsdElemSurreal( new IntegrandHDG<ArrayQSurreal,GradArrayQSurreal>[nDOF] );

  // element jacobian matrices
  MatrixElemClass mtxElemPDE_q(nDOF, nDOF);

  std::vector < Element<ArrayQSurreal, TopoDim, Topology> > qfldElemSurrealVec;

//  for (int i=0; i<ntimes; i++)
//    mtxElemPDE_qMat[i].resize(ntimes);

  qfldElemSurrealVec.assign(ntimes, qfldElemSurreal);

  SANS_ASSERT(qflds.size() == qfldElemSurrealVec.size());

  // number of simultaneous derivatives per functor call
  const int nDeriv = Surreal::N;

  // loop over elements within group
  const int nelem = xfld.nElem();

  //NEED TO FIGURE OUT JACOBIAN!

  for (int elem = 0; elem < nelem; elem++)
  {
    // zero element Jacobians
        for (int i=0; i<nDOF; i++)
          for (int j=0; j<nDOF; j++)
            mtxElemPDE_q(i,j) = 0;

    // copy global grid/solution DOFs to element - create vector of elements here...
    xfld.getElement( xfldElem, elem );

    for (int t=0; t<ntimes; t++) //loop over qflds to fill out ElementVec
    {
      qflds[t]->getElement( qfldElemSurreal, elem );
      qfldElemSurrealVec[t] = qfldElemSurreal;
    }


    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nEqn*nDOF; nchunk += nDeriv)
    {

      for (int j = 0; j < nDOF; j++)
        for (int n = 0; n < nEqn; n++)
          for (int k = 0; k < nDeriv; k++)
            DLA::index(qfldElemSurrealVec[t2].DOF(j),n).deriv(k) = 0;

      // associate derivative slots with solution & lifting-operator variables
      int slot;
      for (int j = 0; j < nDOF; j++)
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemSurrealVec[t2].DOF(j),n).deriv(slot - nchunk) = 1;
        }

      for (int n = 0; n < nDOF; n++)
        rsdElemSurreal[n] = 0;

      // cell integration for canonical element

      integral( fcn.integrand(xfldElem, qfldElemSurrealVec, t1), xfldElem, rsdElemSurreal.get(), nDOF );


#if 0
      std::cout << "JacobianPDE_Group_Integral2DArea_HDG: nchunk = " << nchunk << std::endl;
      for (int n = 0; n < nDOF; n++)
      {
        std::cout << "  rsdPDEElemSurreal[" << n << "] =" << std::endl;
        rsdElemSurreal[n].dump(4);
      }
#endif

      // accumulate derivatives into element jacobians - what to do with this?

      for (int j = 0; j < nDOF; j++)
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            for (int i = 0; i < nDOF; i++)
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxElemPDE_q(i,j),m,n) = DLA::index(rsdElemSurreal[i].PDE,m).deriv(slot - nchunk);
        }

    }   // nchunk


    // scatter-add element jacobian to global
#if 0
    std::cout << "mtxElemPDE_q " << std::endl; mtxElemPDE_q.dump();
    std::cout << "mtxElemPDE_a " << std::endl; mtxElemPDE_a.dump();
    std::cout << "mtxElemAu_q " << std::endl; mtxElemAu_q.dump();
    std::cout << "mtxElemAu_a " << std::endl; mtxElemAu_a.dump();
#endif

    JacobianCell_HDG_HB_Group_ScatterAdd<PhysDim>(
        *qflds[0], elem, nDOF, ntimes,
        mapDOFGlobal_q.data(),
        mtxElemPDE_q,
        mtxGlobalPDE_q );

  } // elem
}



//----------------------------------------------------------------------------//
template<class Surreal, class TopDim>
class JacobianCell_HDG_HB;

// base class interface
template<class Surreal>
class JacobianCell_HDG_HB<Surreal, TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  template <class IntegrandCellFunctor,
  class PhysDim, class ArrayQ, class SparseMatrix>
  static void
  integrate(
      const IntegrandCellFunctor& fcn, const int t1, const int t2,
      const std::vector< std::unique_ptr< Field<PhysDim, TopoDim, ArrayQ> > >& qflds,
      int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE )
  {
    using VectorArrayQ =  DLA::VectorS<PhysDim::D, ArrayQ>;
    SANS_ASSERT( ngroup == qflds[0]->nCellGroups() );
    int ntimes = qflds.size();

    const XField<PhysDim, TopoDim>& xfld = qflds[0]->getXField();
    std::vector < typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Line>* > qGroups(ntimes);

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Line) )
      {
        for (int i=0; i < ntimes; i++)
          qGroups[i] = &(qflds[i]->template getCellGroup<Line>(group));

        JacobianCell_Group_Integral_HDG_HB<Surreal, Line, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
            fcn, t1, t2,
            xfld.template getCellGroup<Line>(group),
            qGroups,
            quadratureorder[group],
            mtxGlobalPDE );
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION( "unknown topology" );
      }
    }
  }
};


template<class Surreal>
class JacobianCell_HDG_HB<Surreal, TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class IntegrandCellFunctor,
  class PhysDim, class ArrayQ, class SparseMatrix>
  static void
  integrate(
      const IntegrandCellFunctor& fcn, const int t1, const int t2,
      const std::vector< std::unique_ptr< Field<PhysDim, TopoDim, ArrayQ> > >& qflds,
      int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE )
  {
    using VectorArrayQ =  DLA::VectorS<PhysDim::D, ArrayQ>;
    SANS_ASSERT( mtxGlobalPDE.m() == qflds[0]->nDOF() );
    SANS_ASSERT( mtxGlobalPDE.n() == qflds[0]->nDOF() );

    SANS_ASSERT( ngroup == qflds[0]->nCellGroups() );
    int ntimes = qflds.size();

    const XField<PhysDim, TopoDim>& xfld = qflds[0]->getXField();
    std::vector < typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Triangle>* > qGroups(ntimes);

    // loop over element groups
    for (int group = 0; group < ngroup; group++)
    {
      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(group).topoTypeID() == typeid(Triangle) )
      {
        for (int i=0; i < ntimes; i++)
          qGroups[i] = &(qflds[i]->template getCellGroup<Triangle>(group));

        JacobianCell_Group_Integral_HDG_HB<Surreal, Triangle, PhysDim, TopoDim, ArrayQ, VectorArrayQ>(
            fcn, t1, t2,
            xfld.template getCellGroup<Triangle>(group),
            qGroups,
            quadratureorder[group],
            mtxGlobalPDE );
      }
      else
      {
        SANS_DEVELOPER_EXCEPTION( "unknown topology" );
      }
    }
  }
};

}
#endif  // JACOBIANCELL_HDG_HB_H
