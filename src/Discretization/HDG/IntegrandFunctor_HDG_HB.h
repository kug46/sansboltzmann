// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDFUNCTOR_HDG_HB_H
#define INTEGRANDFUNCTOR_HDG_HB_H

// cell/trace integrand operators: HDG

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/Quadrature_Cache.h"

#include "Discretization/HDG/DiscretizationHDG.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Cell integrand: HDG HB
//
// integrandPDE = phi*sum( w_n * q_n)/dt
// integrandAux = (K phi)*(ax - qx)
// integrandAuy = (K phi)*(ay - qy)
//
// where
//   phi              basis function
//   q_n              solution at timestep n
//   w_n              backwards difference weight for timestep n

template <class PDE>
class IntegrandCell_HDG_HB
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual arrays

  static const int N = PDE::N;              // total PDE equations

  IntegrandCell_HDG_HB( const PDE& pde, const DiscretizationHDG<PDE>& disc, const Real period,  const int ntimes)
    : pde_(pde), disc_(disc), period_(period), ntimes_(ntimes) {}

  template<class T, class TopoDim, class Topology>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDim, Topology> ElementXFieldType;
    typedef typename ElementXFieldType::VectorX VectorX;

    typedef Element<ArrayQ<T>, TopoDim, Topology> ElementQFieldType;
    typedef std::vector<ElementQFieldType> ElementQFieldVecType;
    typedef Element<VectorArrayQ<T>, TopoDim, Topology> ElementAFieldType;
    typedef IntegrandHDG< ArrayQ<T>, VectorArrayQ<T> > IntegrandType;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    Functor( const PDE& pde,
             const DiscretizationHDG<PDE>& disc,
             const Real period,
             const int ntimes,
             const ElementXFieldType& xfldElem,
             const ElementQFieldVecType& qfldElems,
             const int mytime) :
             pde_(pde), disc_(disc), period_(period), ntimes_(ntimes),
             xfldElem_(xfldElem), qfldElems_(qfldElems), mytime_(mytime), weights_(ntimes)
    {
      //compute weights - equally spaced for now, can do fancier stuff later

        for (int j=0; j<ntimes; j++)
          if (mytime_ != j)
            weights_[j] = (PI / period) * ( pow(-1, mytime_-j) / sin( PI*(mytime_-j)/ntimes ) );

    }

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const;

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOF() const { return qfldElems_[0].nDOF(); }

    // Cell integrand
    void operator()( const QuadPointType& sRef, IntegrandType integrand[], int neqn ) const;


  protected:
    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const Real period_;
    const int ntimes_;
    const ElementXFieldType& xfldElem_;
    const ElementQFieldVecType& qfldElems_;
    const int mytime_;
    std::vector< Real >  weights_;
  };

  template<class T, class TopoDim, class Topology>
  Functor<T, TopoDim, Topology> integrand(const ElementXField<PhysDim, TopoDim, Topology>& xfldElem,
                                          const std::vector< Element<ArrayQ<T>, TopoDim, Topology> >& qfldElems,
                                          int mytime) const
  {
    return Functor<T, TopoDim, Topology>(pde_, disc_, period_, ntimes_, xfldElem, qfldElems, mytime);
  }

protected:
  const PDE& pde_;
  const DiscretizationHDG<PDE>& disc_;
  const Real period_;
  const int ntimes_;
  //weights and solution vector start at current timestep and go backwards
  //i.e. weights_[0] is the weight for timestep n, weights_[1] is the weight for timestep n-1, etc...
};


template <class PDE>
template <class T, class TopoDim, class Topology>
bool
IntegrandCell_HDG_HB<PDE>::Functor<T,TopoDim,Topology>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}

// Cell integrand
//
// integrand = phi*sum(w_n*U_n)/dt
//
// where
//   phi                basis function
//   U_n(x,y)           Solution Vector at timestep n
//   w_n                HB weight for timestep n in relation to timestep 'mytime'
//
template <class PDE>
template <class T, class TopoDim, class Topology>
void
IntegrandCell_HDG_HB<PDE>::Functor<T,TopoDim,Topology>::operator()(
    const QuadPointType& sRef, IntegrandType integrand[], int neqn ) const
{
  const int nDOFelem = qfldElems_[0].nDOF();          // total solution DOFs in element

  SANS_ASSERT( neqn == nDOFelem );

  std::vector<Real> phi(nDOFelem);         // basis

  ArrayQ<T> qtmp, dudt;                    // solution, time derivative
  ArrayQ<T> utmp;
  VectorX X;

  // evaluate basis fcn
  qfldElems_[0].evalBasis( sRef, phi.data(), nDOFelem );

  xfldElem_.eval( sRef, X );

  // HB term
  for (int k = 0; k < neqn; k++)
    integrand[k] = 0;

  if (pde_.hasFluxAdvectiveTime())
  {
    // evaluate solution at different timesteps
    dudt = 0;
    for (int j = 0; j < ntimes_; j++)
    {
      qtmp = 0; utmp = 0;
      if (mytime_ != j)
      {
        qfldElems_[j].evalFromBasis( phi.data(), nDOFelem, qtmp);
        pde_.fluxAdvectiveTime(X, qtmp, utmp);
        dudt += weights_[j] * utmp;
        //std::cout << i << " " << j << " weight: " << weights_[i][j] << " qtmp: " << qtmp << "\n";
      }
    }

    for (int k=0; k < nDOFelem; k++)
      integrand[k].PDE += phi[k]*dudt;
  }
}


} // SANS

#endif  // INTEGRANDFUNCTOR_HDG_HB_H
