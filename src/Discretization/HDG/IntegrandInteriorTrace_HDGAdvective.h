// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_HDG_INTEGRANDINTERIORTRACE_HDGADVECTIVE_H_
#define SRC_DISCRETIZATION_HDG_INTEGRANDINTERIORTRACE_HDGADVECTIVE_H_

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "DiscretizationHDG.h"
#include "Discretization/Integrand_Type.h"

#include "Discretization/Galerkin/JacobianInteriorTrace_Galerkin_Element.h"

#include "Integrand_HDG_fwd.h"

namespace SANS
{

template <class PDE_, class DiscTag>
class IntegrandInteriorTrace_HDGAdvective;

//----------------------------------------------------------------------------//
namespace IntegrandInteriorTrace_HDGAdv_detail
{
// worker object that can be partially specialized to do specific tasks depending on
// the type of dicretization (as in DiscTag)
template <class PDENDConvert, class DiscTag>
struct worker;
} // namespace IntegrandInteriorTrace_HDGAdv_detail

//----------------------------------------------------------------------------//
// TODO: the following documentation needs updating since the code is evovling
// Trace integrand: HDG
//
// integrandL = phiL * fnL
// integrandR = phiR * fnR
// integrandI = phiI * (fnR + fnL) = phiI * [[fn]]
//
// fnL = Fadv(qI) + taqL*(qL - qI)
// fnR = Fadv(qI) + taqR*(qR - qI)
//
// where
//   phi                basis function
//   qI                 interface solution
//   qL                 solution (left)
//   qR                 solution (right)
//   Fadv(x,y,qI)       advective flux
//   tau                HDG stabilization parameter

template <class PDE_, class DiscTag>
class IntegrandInteriorTrace_HDGAdvective :
  public IntegrandInteriorTraceType< IntegrandInteriorTrace_HDGAdvective<PDE_, DiscTag> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class Z>
  using VectorArrayQ = typename PDE::template VectorArrayQ<Z>; // solution gradient arrays

  typedef typename DLA::VectorS<PhysDim::D,Real> VecReal;

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  // hijacking existing machinery (for Galerkin) instead of inventing new ones for HDGAdvective
  typedef JacobianElemInteriorTrace_Galerkin<MatrixQ<Real>> JacobianElemType;
  typedef JacobianElemInteriorTraceSize JacobianElemSizeType;

  static const int D = PhysDim::D;          // Physical dimensions
  static const int N = PDE::N;

  IntegrandInteriorTrace_HDGAdvective(
    const PDE& pde, const DiscretizationHDG<PDE>& disc,
    const std::vector<int>& interiorTraceGroups ) :
      pde_(pde), disc_(disc), interiorTraceGroups_(interiorTraceGroups) {}

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  template< class T, class TopoDimTrace, class TopologyTrace,
                     class TopoDimCell, class TopologyL, class ElementParamL>
  class BasisWeighted
  {
  private:
    friend struct IntegrandInteriorTrace_HDGAdv_detail::worker<PDE_, DiscTag>; // make a friend struct to do some labor

  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTraceType;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementTraceField;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;

    typedef typename ElementXFieldTraceType::VectorX VectorX;
    typedef typename ElementParamL::T ParamTL;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde,
                   const DiscretizationHDG<PDE>& disc,
                   const ElementXFieldTraceType& xfldElemTrace,
                   const ElementTraceField& qIfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL,
                   const int normalSign,
                   const ElementParamL& paramfldElemL,
                   const ElementQFieldL& qfldElemL ) :
      pde_(pde), disc_(disc),
      xfldElemTrace_(xfldElemTrace),
      qIfldElemTrace_(qIfldElemTrace),
      canonicalTraceL_(canonicalTraceL),
      normalSign_(normalSign),
      paramfldElemL_(paramfldElemL),
      xfldElemL_(get<-1>(paramfldElemL)),
      qfldElemL_(qfldElemL),
      nDOFL_( qfldElemL_.nDOF() ),
      nDOFI_( qIfldElemTrace_.nDOF() ),
      phiL_( nDOFL_ ),
      phiI_( nDOFI_ ) {}

    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return ( pde_.hasFluxAdvective() || pde_.hasFluxViscous() ); }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }

    // element trace integrand
    template <class Ti>
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const;

    // trace element Jacobian integrand
    void operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
                     JacobianElemType& mtxElemL, JacobianElemType& mtxElemI ) const;

  protected:
    const PDE& pde_;
    const DiscretizationHDG<PDE>& disc_;
    const ElementXFieldTraceType& xfldElemTrace_;
    const ElementTraceField& qIfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const int normalSign_;
    const ElementParamL& paramfldElemL_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;

    const int nDOFL_;
    const int nDOFI_;
    mutable std::vector<Real> phiL_;
    mutable std::vector<Real> phiI_;
  };

  template< class T, class TopoDimTrace,class TopologyTrace,
                     class TopoDimCell, class TopologyL, class ElementParamL>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,
                   TopoDimCell, TopologyL,ElementParamL>
  integrand( const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
             const Element<ArrayQ<T>, TopoDimTrace, TopologyTrace>& qIfldElemTrace,
             const CanonicalTraceToCell& canonicalTraceL,
             const int normalSign,
             const ElementParamL& paramfldElemL,     // XField must be the last parameter
             const Element<ArrayQ<T>, TopoDimCell, TopologyL>& qfldElemL ) const
  {
    return BasisWeighted<T, TopoDimTrace, TopologyTrace,
                         TopoDimCell, TopologyL,ElementParamL>(
             pde_, disc_,
             xfldElemTrace, qIfldElemTrace, canonicalTraceL, normalSign,
             paramfldElemL, qfldElemL );
  }

protected:
  const PDE& pde_;
  const DiscretizationHDG<PDE>& disc_;
  const std::vector<int> interiorTraceGroups_;
};

template <class PDE, class DiscTag>
template< class T, class TopoDimTrace,class TopologyTrace,
                   class TopoDimCell, class TopologyL, class ElementParamL>
template <class Ti>
void
IntegrandInteriorTrace_HDGAdvective<PDE,DiscTag>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell, TopologyL,ElementParamL>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace ) const
{
  SANS_ASSERT_MSG( rsdPDEElemL.m() == nDOFL_, "neqnL = %d  nDOFL = %d", rsdPDEElemL.m(), nDOFL_ );
  SANS_ASSERT_MSG( rsdINTElemTrace.m() == nDOFI_, "neqnTrace = %d  nDOFI = %d", rsdINTElemTrace.m(), nDOFI_ );

  const bool needsSolutionGradient = pde_.hasFluxViscous();
  SANS_ASSERT_MSG( !needsSolutionGradient, "Should not need solution gradient");

  // left and right element reference coordinates
  QuadPointCellType sRefL;    // reference-element coordinates (s,t)
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // parameters
  ParamTL paramL;  // Elemental parameters (such as grid coordinates and distance functions)

  paramfldElemL_.eval( sRefL, paramL );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_.data(), phiL_.size() );
  qIfldElemTrace_.evalBasis( sRefTrace, phiI_.data(), phiI_.size() );

  // solution value, gradient
  ArrayQ<T> qL;           // solution
  qfldElemL_.evalFromBasis( phiL_.data(), phiL_.size(), qL );

  ArrayQ<T> qI;               // interface solution
  qIfldElemTrace_.evalFromBasis( phiI_.data(), phiI_.size(), qI );

  rsdPDEElemL = 0.0;
  rsdINTElemTrace = 0.0;

  IntegrandInteriorTrace_HDGAdv_detail::template worker<PDE,DiscTag>::weightedIntegrand(
      this, sRefTrace, sRefL, paramL, qL, qI, rsdPDEElemL, rsdINTElemTrace );

  rsdPDEElemL *= dJ;
  rsdINTElemTrace *= dJ;
}


template <class PDE, class DiscTag>
template< class T, class TopoDimTrace,class TopologyTrace,
                   class TopoDimCell, class TopologyL, class ElementParamL>
void
IntegrandInteriorTrace_HDGAdvective<PDE,DiscTag>::
BasisWeighted<T, TopoDimTrace, TopologyTrace,
                 TopoDimCell, TopologyL,ElementParamL>::
operator()( const Real dJ, const QuadPointTraceType& sRefTrace,
            JacobianElemType& mtxElemL, JacobianElemType& mtxElemI ) const
{
  // Must be a multiple of PDE::N
  typedef SurrealS<PDE::N> SurrealClass;

  // number of simultaneous derivatives per functor call
  static const int nDeriv = SurrealClass::N;
  static_assert(nDeriv % PDE::N == 0, "Surreal derivatives must be a multiple of PDE::N");

  SANS_ASSERT(mtxElemL.nTest == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemL.nDOFR == nDOFI_);

  SANS_ASSERT(mtxElemI.nTest == nDOFI_);
  SANS_ASSERT(mtxElemI.nDOFL == nDOFL_);
  SANS_ASSERT(mtxElemI.nDOFR == nDOFI_);


  const bool needsSolutionGradient = pde_.hasFluxViscous();
  SANS_ASSERT_MSG(!needsSolutionGradient, "Should not use solution gradient!");

  // left reference-element coords
  QuadPointCellType sRefL;    // reference-element coordinates (s,t)
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, sRefTrace, sRefL );

  // parameters
  ParamTL paramL; // Elemental parameters (such as grid coordinates and distance functions)
  paramfldElemL_.eval( sRefL, paramL );

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL_.data(), phiL_.size() );
  qIfldElemTrace_.evalBasis( sRefTrace, phiI_.data(), phiI_.size() );

  // solution value
  ArrayQ<T> qL;               // solution
  ArrayQ<T> qI;               // interface solution
  qfldElemL_.evalFromBasis( phiL_.data(), phiL_.size(), qL );
  qIfldElemTrace_.evalFromBasis( phiI_.data(), phiI_.size(), qI );

  // construct jacobian of integrand w.r.t. solution q DOFs using automatic differentiation with Surreal
  ArrayQ<SurrealClass> qSurrealL, qSurrealI; // solution

  qSurrealL = qL;
  qSurrealI = qI;

  if (needsSolutionGradient)
    SANS_DEVELOPER_EXCEPTION("Should not use solution gradient!");

  MatrixQ<Real> PDE_q = 0.0; // temporary storage

  // element integrand/residual
  DLA::VectorD<ArrayQ<SurrealClass>> integrandLSurreal( nDOFL_ );
  DLA::VectorD<ArrayQ<SurrealClass>> integrandISurreal( nDOFI_ );

  // loop over derivative chunks to computes derivatives w.r.t q
  for (int nchunk = 0; nchunk < 2*PDE::N; nchunk += nDeriv)
  {
    // associate derivative slots with solution variables

    int slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 1.0;

    slot = nDeriv;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealI, n).deriv(slot + n - nchunk) = 1.0;

    integrandLSurreal = 0.0;
    integrandISurreal = 0.0;

    // compute the integrand
    IntegrandInteriorTrace_HDGAdv_detail::template worker<PDE,DiscTag>::weightedIntegrand(
        this, sRefTrace, sRefL, paramL, qSurrealL, qSurrealI, integrandLSurreal, integrandISurreal );

    // accumulate derivatives into element jacobian
    slot = 0;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealL, n).deriv(slot + n - nchunk) = 0.0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemL.PDE_qL(i,j) += PDE_q*phiL_[j]*dJ;
      }

      for (int i = 0; i < nDOFI_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandISurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFL_; j++)
          mtxElemI.PDE_qL(i,j) += PDE_q*phiL_[j]*dJ;
      }
    } // if slot

    slot = nDeriv;
    if ((slot >= nchunk) && (slot < nchunk + nDeriv))
    {
      for (int n = 0; n < PDE::N; n++)
        DLA::index(qSurrealI, n).deriv(slot + n - nchunk) = 0.0; // Reset the derivative

      for (int i = 0; i < nDOFL_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandLSurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFI_; j++)
          mtxElemL.PDE_qR(i,j) += PDE_q*phiI_[j]*dJ;
      }

      for (int i = 0; i < nDOFI_; i++)
      {
        // storing the Jacobian in MatrixQ reduces cache misses and increases efficiency
        for (int m = 0; m < PDE::N; m++)
          for (int n = 0; n < PDE::N; n++)
            DLA::index(PDE_q,m,n) = DLA::index(integrandISurreal[i], m).deriv(slot + n - nchunk);

        for (int j = 0; j < nDOFI_; j++)
          mtxElemI.PDE_qR(i,j) += PDE_q*phiI_[j]*dJ;
      }
    } // if slot
  } // nchunk
}

//----------------------------------------------------------------------------//
namespace IntegrandInteriorTrace_HDGAdv_detail
{
// DiscTag = HDGAdv
template <class PDENDConvert>
struct worker<PDENDConvert, HDGAdv>
{
  typedef IntegrandInteriorTrace_HDGAdvective<PDENDConvert,HDGAdv> IntegrandType;

  template<class T>
  using ArrayQ = typename IntegrandType::template ArrayQ<T>;

  template<class T, class TopoDimTrace,class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParamL>
  using BasisWeightedIntegrandClass =
      typename IntegrandType::template BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>;

  // compute the weighted integrands
  template<class T, class TopoDimTrace,class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParamL,
           class Tq, class Ti>
  static void
  weightedIntegrand(
      const BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>* integrandFcn,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            QuadPointTraceType& sRefTrace,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            QuadPointCellType& sRefL,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            ParamTL& paramL,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qI,
      DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace )
  {
    typedef BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL> BasisWeightedIntegrandType;
    typedef typename BasisWeightedIntegrandType::VectorX VectorX;

    // trace unit normal
    VectorX nL; // unit normal for left element (points to right element)
    traceUnitNormal( integrandFcn->xfldElemL_, sRefL,
                     integrandFcn->xfldElemTrace_, sRefTrace, nL);
    // adjust the sign of the trace normal vector so that it points out of the left adjacent cell element
    nL *= static_cast<Real>(integrandFcn->normalSign_);

    ArrayQ<Ti> FnL = 0.0;          // normal flux

    // advective flux term: FadvL(qL, qI)
    if (integrandFcn->pde_.hasFluxAdvective())
      integrandFcn->pde_.fluxAdvectiveUpwind( paramL, qL, qI, nL, FnL );

    // PDE residuals
    rsdPDEElemL = 0.0;

    for (int k = 0; k < integrandFcn->nDOFL_; k++)
      rsdPDEElemL[k] += integrandFcn->phiL_[k]*FnL;

    // residuals corresponding to the flux matching equation at interior trace
    rsdINTElemTrace = 0.0;

    for (int k = 0; k < integrandFcn->nDOFI_; k++)
      rsdINTElemTrace[k] += integrandFcn->phiI_[k]*FnL;
  }
};

// DiscTag = HDGAdv_manifold
template <class PDENDConvert>
struct worker<PDENDConvert, HDGAdv_manifold>
{
  typedef IntegrandInteriorTrace_HDGAdvective<PDENDConvert,HDGAdv_manifold> IntegrandType;

  template<class T>
  using ArrayQ = typename IntegrandType::template ArrayQ<T>;

  template<class T, class TopoDimTrace,class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParamL>
  using BasisWeightedIntegrandClass =
      typename IntegrandType::template BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>;

  // compute the weighted integrands
  template<class T, class TopoDimTrace,class TopologyTrace,
                    class TopoDimCell, class TopologyL, class ElementParamL,
           class Tq, class Ti>
  static void
  weightedIntegrand(
      const BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>* integrandFcn,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            QuadPointTraceType& sRefTrace,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            QuadPointCellType& sRefL,
      const typename BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL>::
            ParamTL& paramL,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qI,
      DLA::VectorD<ArrayQ<Ti> >& rsdPDEElemL, DLA::VectorD<ArrayQ<Ti> >& rsdINTElemTrace )
  {
    typedef BasisWeightedIntegrandClass<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, ElementParamL> BasisWeightedIntegrandType;
    typedef typename BasisWeightedIntegrandType::VectorX VectorX;

    SANS_ASSERT_MSG(!integrandFcn->pde_.hasFluxViscous(), "Should not have viscous flux!");

    // trace unit normal
    VectorX nL; // unit normal for left element (points to right element)
    traceUnitNormal( integrandFcn->xfldElemL_, sRefL,
                     integrandFcn->xfldElemTrace_, sRefTrace, nL);
    // adjust the sign of the trace normal vector so that it points out of the left adjacent cell element
    nL *= static_cast<Real>(integrandFcn->normalSign_);

    // basis vectors
    VectorX e01L;// basis direction vector
    integrandFcn->xfldElemL_.unitTangent(sRefL, e01L);

    DLA::VectorS<TopoDimCell::D, VectorX> e0L; // basis direction vector
    e0L[0] = e01L;

    //----------------------------------------------------------------------------//
    // contribution to PDE weighted residual

    ArrayQ<Ti> FnL = 0.0; // normal flux
    ArrayQ<Ti> sourceL = 0.0; // normal flux

    integrandFcn->pde_.interfaceFluxAndSourceHDGIBL(paramL, e0L, qL, qI, nL, FnL, sourceL);

    // PDE residuals
    rsdPDEElemL = 0.0;

    for (int k = 0; k < integrandFcn->nDOFL_; k++)
      rsdPDEElemL[k] += integrandFcn->phiL_[k]*FnL;

    //----------------------------------------------------------------------------//
    // residuals corresponding to the flux matching equation at interior trace

    rsdINTElemTrace = 0.0;

    for (int k = 0; k < integrandFcn->nDOFI_; k++)
      rsdINTElemTrace[k] += integrandFcn->phiI_[k]*sourceL;
  }
};

} // namespace IntegrandInteriorTrace_HDGAdv_detail

} // namespace SANS

#endif /* SRC_DISCRETIZATION_HDG_INTEGRANDINTERIORTRACE_HDGADVECTIVE_H_ */
