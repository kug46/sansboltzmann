// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HDG_AUXVARCATEGORY_H
#define HDG_AUXVARCATEGORY_H

// basis function category
  enum AuxVersion
  {
    Gradient,
    AugGradient,
    DiffusiveFlux
  };


#endif // HDG_AUXVARCATEGORY_H
