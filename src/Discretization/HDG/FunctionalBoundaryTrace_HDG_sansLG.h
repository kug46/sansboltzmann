// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FUNCTIONALBOUNDARYTRACE_HDG_SANSLG_H
#define FUNCTIONALBOUNDARYTRACE_HDG_SANSLG_H

// boundary-trace integral functional

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/ElementIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Functional boundary-trace integral
//

template<class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, class T>
class FunctionalBoundaryTrace_HDG_sansLG_impl :
    public GroupIntegralBoundaryTraceType< FunctionalBoundaryTrace_HDG_sansLG_impl<FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, T> >
{
public:
  typedef typename FunctionalIntegrandBoundaryTrace::PhysDim PhysDim;
  // typedef typename FunctionalIntegrandBoundaryTrace::PDE PDE;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayQ<T> ArrayQ;
  typedef DLA::VectorS<PhysDim::D, ArrayQ > VectorArrayQ;
  typedef typename FunctionalIntegrandBoundaryTrace::template ArrayJ<T> ArrayJ;

  // Save off the boundary trace integrand and the residual vectors
  FunctionalBoundaryTrace_HDG_sansLG_impl( const FunctionalIntegrandBoundaryTrace& fcnJ,
                                           const BCIntegrandBoundaryTrace& fcnBC,
                                           ArrayJ& functional ) :
    fcnJ_(fcnJ), fcnBC_(fcnBC), functional_(functional)
  {
    // Find all groups that are common between the BC and functional
    for (std::size_t i = 0; i < fcnJ_.nBoundaryGroups(); i++)
    {
      for (std::size_t j = 0; j < fcnBC_.nBoundaryGroups(); j++)
      {
        std::size_t iBoundaryGroupJ = fcnJ_.boundaryGroup(i);
        if (iBoundaryGroupJ == fcnBC_.boundaryGroup(j))
          boundaryTraceGroups_.push_back(iBoundaryGroupJ);
      }
    }
  }

  std::size_t nBoundaryGroups() const { return boundaryTraceGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return boundaryTraceGroups_[n]; }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>,
                               Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    // Nothing
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                              template FieldCellGroupType<TopologyL>& fldsCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>      ::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, VectorArrayQ>::template FieldCellGroupType<TopologyL> AFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename AFieldCellGroupTypeL::template ElementType<> ElementAFieldClassL;

    typedef typename XFieldType::XFieldType         ::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const AFieldCellGroupTypeL& afldCellL = get<1>(fldsCellL);

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementAFieldClassL afldElemL( afldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // trace element integral
    ElementIntegral<TopoDimTrace, TopologyTrace, ArrayJ> integral(quadratureorder);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );
      afldCellL.getElement( afldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );
      ArrayJ result = 0;

      integral( fcnJ_.integrand(fcnBC_,
                                xfldElemTrace, canonicalTraceL,
                                xfldElemL, qfldElemL, afldElemL,
                                qIfldElemTrace),
                xfldElemTrace, result );

      // sum up the functional
      functional_ += result;
    }
  }

protected:
  const FunctionalIntegrandBoundaryTrace& fcnJ_;
  const BCIntegrandBoundaryTrace& fcnBC_;
  ArrayJ& functional_;
  std::vector<int> boundaryTraceGroups_;
};

// Factory function

template<class FunctionalIntegrandBoundaryTrace, class BCIntegrandBoundaryTrace, class ArrayJ>
FunctionalBoundaryTrace_HDG_sansLG_impl<FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, typename Scalar<ArrayJ>::type>
FunctionalBoundaryTrace_HDG_sansLG( const IntegrandBoundaryTraceType<FunctionalIntegrandBoundaryTrace>& fcnJ,
                                    const IntegrandBoundaryTraceType<BCIntegrandBoundaryTrace>& fcnBC,
                                    ArrayJ& functional )
{
  typedef typename Scalar<ArrayJ>::type T;
  static_assert( std::is_same<ArrayJ, typename FunctionalIntegrandBoundaryTrace::template ArrayJ<T> >::value, "These should be the same.");
  return FunctionalBoundaryTrace_HDG_sansLG_impl<FunctionalIntegrandBoundaryTrace, BCIntegrandBoundaryTrace, T>(
      fcnJ.cast(), fcnBC.cast(), functional);
}

}

#endif  // FUNCTIONALBOUNDARYTRACE_HDG_SANSLG_H
