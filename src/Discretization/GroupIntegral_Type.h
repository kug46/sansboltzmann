// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GROUPINTEGRAL_TYPES_H
#define GROUPINTEGRAL_TYPES_H

namespace SANS
{

// A set of base classes to indicate a specific class is an implementation of an integrand functor

template<class Derived>
struct GroupIntegralCellType
{
  GroupIntegralCellType() {}
  GroupIntegralCellType(const GroupIntegralCellType&&) {}
  GroupIntegralCellType(const GroupIntegralCellType&) = delete;
  const GroupIntegralCellType& operator=(const GroupIntegralCellType&) = delete;

  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
  inline       Derived& cast()       { return static_cast<      Derived&>(*this); }
};

template<class Derived>
struct GroupIntegralInteriorTraceType
{
  GroupIntegralInteriorTraceType() {}
  GroupIntegralInteriorTraceType(const GroupIntegralInteriorTraceType&&) {}
  GroupIntegralInteriorTraceType(const GroupIntegralInteriorTraceType&) = delete;
  const GroupIntegralInteriorTraceType& operator=(const GroupIntegralInteriorTraceType&) = delete;

  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
  inline       Derived& cast()       { return static_cast<      Derived&>(*this); }
};

template<class Derived>
struct GroupIntegralBoundaryTraceType
{
  GroupIntegralBoundaryTraceType() {}
  GroupIntegralBoundaryTraceType(const GroupIntegralBoundaryTraceType&&) {}
  GroupIntegralBoundaryTraceType(const GroupIntegralBoundaryTraceType&) = delete;
  const GroupIntegralBoundaryTraceType& operator=(const GroupIntegralBoundaryTraceType&) = delete;

  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
  inline       Derived& cast()       { return static_cast<      Derived&>(*this); }
};

template<class Derived>
struct GroupIntegralCellTraceType
{
  GroupIntegralCellTraceType() {}
  GroupIntegralCellTraceType(const GroupIntegralCellTraceType&&) {}
  GroupIntegralCellTraceType(const GroupIntegralCellTraceType&) = delete;
  const GroupIntegralCellTraceType& operator=(const GroupIntegralCellTraceType&) = delete;

  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
  inline       Derived& cast()       { return static_cast<      Derived&>(*this); }
};


}

#endif //INTEGRAL_TYPES_H
