// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYFRAME_CG_POTENTIAL_ADJOINT_H
#define RESIDUALBOUNDARYFRAME_CG_POTENTIAL_ADJOINT_H

// boundary-frame integral functional

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/Element.h"

#include "Field/XFieldVolume.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "Discretization/Potential_Adjoint/IntegrandBoundaryFrame_CG_LIP_Adjoint.h"

namespace SANS
{


template <class TopologyFrame,
          class TopologyTrace,
          class TopologyCellL, class TopologyCellR,
          class XFieldType,
          class ArrayQ,
          class PDE>
void
ResidualBoundaryFrame_CG_Potential_Adjoint_Group_Integral(
    const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& gamfldTrace,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL>& xfldCellL,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL>& qfldCellL,
    const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR>& xfldCellR,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR>& qfldCellR,
    int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
    SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL> XFieldCellGroupTypeL;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR> XFieldCellGroupTypeR;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR> QFieldCellGroupTypeR;

  typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
  typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
  typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

  typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

  typedef typename XFieldType::FieldFrameGroupType XFieldFrameGroupType;
  typedef typename XFieldFrameGroupType::template ElementType<> ElementXFieldFrameClass;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  ElementXFieldClassR xfldElemR( xfldCellR.basis() );
  ElementQFieldClassR qfldElemR( qfldCellR.basis() );

  ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
  ElementQFieldTraceClass gamfldElemTrace( gamfldTrace.basis() );

  ElementXFieldFrameClass xfldElemFrame( xfldFrame.basis() );

  // number of integrals evaluated per element
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();
  int nIntegrandT = gamfldElemTrace.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobalL( nIntegrandL, -1 );
  std::vector<int> mapDOFGlobalR( nIntegrandR, -1 );
  std::vector<int> mapDOFGlobalT( nIntegrandT, -1 );

  // frame element integral
  GalerkinWeightedIntegral<TopoD1, Line, ArrayQ, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR, nIntegrandT);

  // element integrand/residuals
  std::vector<ArrayQ> rsdPDEElemL( nIntegrandL );
  std::vector<ArrayQ> rsdPDEElemR( nIntegrandR );
  std::vector<ArrayQ> rsdPDEElemT( nIntegrandT );

  // loop over elements within group
  int nelem = xfldFrame.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    xfldFrame.getElement(xfldElemFrame, elem);

    const int elemTrace = xfldFrame.getElementLeft( elem );
    CanonicalTraceToCell& canonicalFrame = xfldFrame.getCanonicalTraceLeft( elem );

    xfldTrace.getElement( xfldElemTrace, elemTrace );
    gamfldTrace.getElement( gamfldElemTrace, elemTrace );

    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elemTrace );
    CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elemTrace );

    const int elemL = xfldTrace.getElementLeft( elemTrace );
    const int elemR = xfldTrace.getElementRight( elemTrace );

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldCellR.getElement( xfldElemR, elemR );
    qfldCellR.getElement( qfldElemR, elemR );

    for (int n = 0; n < nIntegrandL; n++)
      rsdPDEElemL[n] = 0;

    for (int n = 0; n < nIntegrandR; n++)
      rsdPDEElemR[n] = 0;

    for (int n = 0; n < nIntegrandT; n++)
      rsdPDEElemT[n] = 0;

    integral(
        fcn.integrand(xfldElemFrame,
                      xfldElemTrace,
                      gamfldElemTrace,
                      canonicalFrame,
                      canonicalTraceL, canonicalTraceR,
                      xfldElemL, qfldElemL,
                      xfldElemR, qfldElemR ),
        xfldElemFrame,
        rsdPDEElemL.data(), nIntegrandL,
        rsdPDEElemR.data(), nIntegrandR,
        rsdPDEElemT.data(), nIntegrandT );

    // scatter-add element residuals to global
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nIntegrandR );
    gamfldTrace.associativity( elemTrace ).getGlobalMapping( mapDOFGlobalT.data(), nIntegrandT );

    int nGlobal;
    for (int n = 0; n < nIntegrandL; n++)
    {
      nGlobal = mapDOFGlobalL[n];
      rsdPDEGlobal[nGlobal] += rsdPDEElemL[n];
    }

    for (int n = 0; n < nIntegrandR; n++)
    {
      nGlobal = mapDOFGlobalR[n];
      rsdPDEGlobal[nGlobal] += rsdPDEElemR[n];
    }

    for (int n = 0; n < nIntegrandT; n++)
    {
      nGlobal = mapDOFGlobalT[n];
      rsdWakeGlobal[nGlobal] += rsdPDEElemT[n];
    }
  }
}



template <class TopologyFrame,
          class TopologyTrace,
          class TopologyCellL, class TopologyCellR,
          class XFieldType,
          class PDE,
          class ArrayQ>
static void
ResidualBoundaryFrame_CG_Potential_Adjoint_CellGroups(
    const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& gamfldTrace,
    const Field<PhysD3, TopoD3, ArrayQ>& qfld,
    const int quadratureorder,
    SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
    SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
{
  const XField<PhysD3, TopoD3>& xfld = qfld.getXField();

  int groupCellL = xfldTrace.getGroupLeft();
  int groupCellR = xfldTrace.getGroupRight(); // This is not a typo as xfldTraceR is a BC field

  // Integrate over the trace group
  ResidualBoundaryFrame_CG_Potential_Adjoint_Group_Integral<TopologyFrame, TopologyTrace,
                                                TopologyCellL, TopologyCellR, XFieldType, ArrayQ>( fcn, xfldFrame, gamfldTrace,
                    xfldTrace,
                    xfld.template getCellGroup<TopologyCellL>(groupCellL), qfld.template getCellGroup<TopologyCellL>(groupCellL),
                    xfld.template getCellGroup<TopologyCellR>(groupCellR), qfld.template getCellGroup<TopologyCellR>(groupCellR),
                    quadratureorder,
                    rsdPDEGlobal, rsdWakeGlobal );
}


//----------------------------------------------------------------------------//
// Residual Boundary Frame
//
template<class TopDim>
class ResidualBoundaryFrame_CG_Potential_Adjoint;

template<>
class ResidualBoundaryFrame_CG_Potential_Adjoint<TopoD3>
{
protected:
  typedef TopoD3 TopoDim;

  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyCellL,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellRight_Triangle(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<Triangle>& gamfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      ResidualBoundaryFrame_CG_Potential_Adjoint_CellGroups<TopologyFrame, Triangle, TopologyCellL, Tet, XFieldType>(
          fcn, xfldFrame, xfldTrace, gamfldTrace, qfld,
          quadratureorder,
          rsdPDEGlobal, rsdWakeGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellLeft_Triangle(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<Triangle>& gamfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      TopologyCellRight_Triangle<TopologyFrame, Tet, XFieldType>(
          fcn, xfldFrame, xfldTrace, gamfldTrace, qfld,
          quadratureorder,
          rsdPDEGlobal, rsdWakeGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


//----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyCellL,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellRight_Quad(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<Quad>& gamfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      ResidualBoundaryFrame_CG_Potential_Adjoint_CellGroups<TopologyFrame, Quad, TopologyCellL, Hex, XFieldType>(
          fcn, xfldFrame, xfldTrace, gamfldTrace, qfld,
          quadratureorder,
          rsdPDEGlobal, rsdWakeGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ>
  static void
  TopologyCellLeft_Quad(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<Quad>& gamfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {
    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      TopologyCellRight_Quad<TopologyFrame, Hex, XFieldType>(
          fcn, xfldFrame, xfldTrace, gamfldTrace, qfld,
          quadratureorder,
          rsdPDEGlobal, rsdWakeGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class TopologyFrame, class XFieldType, class PDE,
            class PhysDim, class ArrayQ>
  static void
  TraceTopology(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const Field<PhysDim, TopoDim, ArrayQ>& gamfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {
    const int boundaryGroupL = xfldFrame.getGroupLeft();

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // get the boundary group
    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Triangle) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Triangle<TopologyFrame, XFieldType>(
          fcn, xfldFrame,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupL),
          gamfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          qfld,
          quadratureorder,
          rsdPDEGlobal, rsdWakeGlobal );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Quad) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Quad<TopologyFrame, XFieldType>(
          fcn, xfldFrame,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupL),
          gamfld.template getBoundaryTraceGroupGlobal<Quad>(boundaryGroupL),
          qfld,
          quadratureorder,
          rsdPDEGlobal, rsdWakeGlobal );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class XFieldType, class PDE,
            class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& gamfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<ArrayQ>& rsdPDEGlobal,
      SLA::SparseVector<ArrayQ>& rsdWakeGlobal )
  {
    SANS_ASSERT( &gamfld.getXField() == &xfld );
    SANS_ASSERT( &qfld.getXField() == &xfld );

    SANS_ASSERT( ngroup == xfld.nBoundaryFrameGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryFrameGroups(); group++)
    {
      const int boundaryFrameGroup = fcn.boundaryFrameGroup(group);

      TraceTopology<Line, XFieldType>( fcn,
          xfld.getBoundaryFrameGroup(boundaryFrameGroup),
          gamfld,
          qfld,
          quadratureorder[boundaryFrameGroup],
          rsdPDEGlobal,
          rsdWakeGlobal );

    }
  }
};

}

#endif  // RESIDUALBOUNDARYFRAME_CG_POTENTIAL_ADJOINT_H
