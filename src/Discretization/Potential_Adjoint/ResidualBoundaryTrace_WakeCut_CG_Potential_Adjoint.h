// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_ADJOINT_H
#define RESIDUALBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_ADJOINT_H

// boundary-trace integral residual functions

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin interior-trace integral
//
template<class IntegrandBoundaryTrace, template<class> class Vector>
class ResidualBoundaryTrace_WakeCut_CG_Potential_Adjoint_impl :
    public GroupIntegralInteriorTraceType< ResidualBoundaryTrace_WakeCut_CG_Potential_Adjoint_impl<IntegrandBoundaryTrace, Vector> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_WakeCut_CG_Potential_Adjoint_impl( const IntegrandBoundaryTrace& fcn,
                                                           Vector<ArrayQ>& rsdPDEGlobal, Vector<ArrayQ>& rsdWakeGlobal) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), rsdWakeGlobal_(rsdWakeGlobal), comm_rank_(0) {}

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& gamfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( rsdWakeGlobal_.m() == gamfld.nDOFpossessed() );

#ifdef SANS_MPI
    //comm_rank_ = qfld.comm()->rank();

    // MPI ranks assume a DG space below
    //SANS_ASSERT(qfld.spaceType() == SpaceType::Discontinuous);
#endif
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType                     ::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType                     ::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& gamfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass gamfldElemTrace( gamfldTrace.basis() );

    // number of integrals evaluated per element
    const int nIntegrandL = qfldElemL.nDOF();
    const int nIntegrandR = qfldElemR.nDOF();
    const int nIntegrandT = gamfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL, -1 );
    std::vector<int> mapDOFGlobalR( nIntegrandR, -1 );
    std::vector<int> mapDOFGlobalT( nIntegrandT, -1 );

    // element integrand/residuals
    std::vector<ArrayQ> rsdElemL( nIntegrandL );
    std::vector<ArrayQ> rsdElemR( nIntegrandR );
    std::vector<ArrayQ> rsdElemT( nIntegrandT );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQ, ArrayQ, ArrayQ> integral(quadratureorder, nIntegrandL, nIntegrandR, nIntegrandT);

    // loop over elements within the trace group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; ++elem)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );
      gamfldTrace.getElement( gamfldElemTrace, elem );

      const int rankL = qfldElemL.rank();
      const int rankR = qfldElemR.rank();

      // nothing to do if both elements are ghost elements
      if (rankL != comm_rank_ && rankR != comm_rank_) continue;

      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      auto integrandFunctor = fcn_.integrand(xfldElemTrace, gamfldElemTrace,
                                             canonicalTraceL, canonicalTraceR,
                                             xfldElemL, qfldElemL,
                                             xfldElemR, qfldElemR);

      for (int n = 0; n < nIntegrandL; n++)
        rsdElemL[n] = 0;

      for (int n = 0; n < nIntegrandR; n++)
        rsdElemR[n] = 0;

      for (int n = 0; n < nIntegrandT; n++)
        rsdElemT[n] = 0;

      integral(integrandFunctor, xfldElemTrace,
               rsdElemL.data(), nIntegrandL,
               rsdElemR.data(), nIntegrandR,
               rsdElemT.data(), nIntegrandT);

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );
      qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nIntegrandR );
      gamfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalT.data(), nIntegrandT );

      int nGlobal;
      if ( rankL == comm_rank_ )
        for (int n = 0; n < nIntegrandL; n++)
        {
          nGlobal = mapDOFGlobalL[n];
          rsdPDEGlobal_[nGlobal] += rsdElemL[n];
        }

      if ( rankR == comm_rank_ )
        for (int n = 0; n < nIntegrandR; n++)
        {
          nGlobal = mapDOFGlobalR[n];
          rsdPDEGlobal_[nGlobal] += rsdElemR[n];
        }

      for (int n = 0; n < nIntegrandT; n++)
      {
        nGlobal = mapDOFGlobalT[n];
        rsdWakeGlobal_[nGlobal] += rsdElemT[n];
      }
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  Vector<ArrayQ>& rsdPDEGlobal_;
  Vector<ArrayQ>& rsdWakeGlobal_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandBoundaryTrace, class ArrayQ, template<class> class Vector>
ResidualBoundaryTrace_WakeCut_CG_Potential_Adjoint_impl<IntegrandBoundaryTrace, Vector>
ResidualBoundaryTrace_WakeCut_CG_Potential_Adjoint( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                                    Vector<ArrayQ>& rsdPDEGlobal, Vector<ArrayQ>& rsdWakeGlobal)
{
  static_assert( std::is_same<ArrayQ, typename IntegrandBoundaryTrace::template ArrayQ<Real> >::value, "These should be the same.");
  return {fcn.cast(), rsdPDEGlobal, rsdWakeGlobal};
}

} //namespace SANS

#endif  // RESIDUALBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_ADJOINT_H
