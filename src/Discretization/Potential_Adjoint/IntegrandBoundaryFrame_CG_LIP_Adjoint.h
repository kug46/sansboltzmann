// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFRAME_CG_POTENTIAL_ADJOINT_H
#define INTEGRANDBOUNDARYFRAME_CG_POTENTIAL_ADJOINT_H

// boundary-frame integral functional
#include "tools/SANSnumerics.h"
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PDE>
class IntegrandBoundaryFrame_CG_Potential_Adjoint
{
public:
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryFrame_CG_Potential_Adjoint( const PDE& pde, const std::vector<int>& BoundaryFrameGroups )
    : pde_(pde), BoundaryFrameGroups_(BoundaryFrameGroups)
  {}

  std::size_t nBoundaryFrameGroups() const { return BoundaryFrameGroups_.size(); }
  std::size_t boundaryFrameGroup(const int n) const { return BoundaryFrameGroups_[n]; }

  template<class T, class TopoDimFrame, class TopologyFrame,
                    class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCellL, class TopologyCellR>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimFrame, TopologyFrame > ElementXFieldFrame;
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace > ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyCellL > ElementXFieldCellL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyCellR > ElementXFieldCellR;

    typedef Element<ArrayQ<T>, TopoDimTrace , TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell  , TopologyCellL> ElementQFieldCellL;
    typedef Element<ArrayQ<T>, TopoDimCell  , TopologyCellR> ElementQFieldCellR;

    typedef typename ElementXFieldFrame::RefCoordType RefCoordFrameType;
    typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
    typedef typename ElementXFieldCellL::RefCoordType RefCoordCellTypeL;
    typedef typename ElementXFieldCellR::RefCoordType RefCoordCellTypeR;
    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<typename TopologyFrame::TopoDim> QuadPointFrameType;
    //typedef QuadraturePoint<typename TopologyTrace::TopoDim> QuadPointTraceType;
    //typedef QuadratureCellTracePoint<typename TopologyCellL::TopoDim> QuadPointCellLType;

    Functor( const PDE& pde,
             const ElementXFieldFrame& xfldElemFrame,
             const ElementXFieldTrace& xfldElemTrace,
             const ElementQFieldTrace& gamfldElemTrace,
             const CanonicalTraceToCell& canonicalFrame,
             const CanonicalTraceToCell& canonicalTraceL,
             const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldCellL& xfldElemL,
             const ElementQFieldCellL& qfldElemL,
             const ElementXFieldCellR& xfldElemR,
             const ElementQFieldCellR& qfldElemR ) :
               pde_(pde), xfldElemFrame_(xfldElemFrame),
               xfldElemTrace_(xfldElemTrace),
               gamfldElemTrace_(gamfldElemTrace),
               canonicalFrame_(canonicalFrame),
               canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
               xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
               xfldElemR_(xfldElemR), qfldElemR_(qfldElemR)
             {}

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // trace integrand
    void operator()( const QuadPointFrameType& sRefFrame,
                     ArrayQ<T> integrandPDEL[], int nrsdPDEL,
                     ArrayQ<T> integrandPDER[], int nrsdPDER,
                     ArrayQ<T> integrandWakeT[], int nrsdWakeT ) const
    {
      typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

      const int nDOFL = qfldElemL_.nDOF();             // total solution DOFs in left element
      const int nDOFR = qfldElemR_.nDOF();             // total solution DOFs in right element
      const int nDOFT = gamfldElemTrace_.nDOF();

      SANS_ASSERT(nrsdPDEL  == nDOFL);
      SANS_ASSERT(nrsdPDER  == nDOFR);
      SANS_ASSERT(nrsdWakeT == nDOFT);
      SANS_ASSERT(nDOFL     == nDOFR );

      VectorX xL, xR;
      VectorX nL, nR;           // unit normal for left element (points to right element)

      std::vector<Real> phiL(nDOFL);         // basis (left)
      std::vector<VectorX> gradphiL(nDOFL);  // basis gradient (left)
      std::vector<Real> phiR(nDOFR);         // basis (right)
      std::vector<VectorX> gradphiR(nDOFR);  // basis gradient (right)

      ArrayQ<T> qL, qR, gam;
      VectorArrayQ gradqL, gradqR;  // gradients

      DLA::VectorS< PhysDim::D, Real > U; // Freestream Velocity

      RefCoordTraceType sRefTrace;
      RefCoordCellTypeL sRefCellL;
      RefCoordCellTypeR sRefCellR;

      TraceToCellRefCoord<TopologyFrame, TopoDimTrace, TopologyTrace>::eval( canonicalFrame_, sRefFrame.ref, sRefTrace );
      TraceToCellRefCoord<TopologyTrace, TopoDimCell , TopologyCellL>::eval( canonicalTraceL_, sRefTrace, sRefCellL );
      TraceToCellRefCoord<TopologyTrace, TopoDimCell , TopologyCellR>::eval( canonicalTraceR_, sRefTrace, sRefCellR );

      // unit normals
      xfldElemTrace_.unitNormal( sRefTrace, nL );
      nR = -nL;

      // basis value, gradient
      qfldElemL_.evalBasis( sRefCellL, phiL.data(), nDOFL );
      qfldElemR_.evalBasis( sRefCellR, phiR.data(), nDOFR );
      xfldElemL_.evalBasisGradient( sRefCellL, qfldElemL_, gradphiL.data(), nDOFL );
      xfldElemR_.evalBasisGradient( sRefCellR, qfldElemR_, gradphiR.data(), nDOFR );

      xfldElemL_.eval( sRefCellL, xL );
      xfldElemR_.eval( sRefCellR, xR );

      // circulation
      gamfldElemTrace_.eval(sRefTrace, gam);

      // solution value, gradient
      qfldElemL_.eval( sRefCellL, qL );
      qfldElemR_.eval( sRefCellR, qR );
      qfldElemL_.evalFromBasis( gradphiL.data(), nDOFL, gradqL );
      qfldElemR_.evalFromBasis( gradphiR.data(), nDOFR, gradqR );

      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] = 0;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] = 0;

      for (int k = 0; k < nDOFT; k++)
        integrandWakeT[k] = 0;

      //pde_.freestream(U);
      U = pde_.Up(xL);

      for (int n = 0; n < PhysDim::D; n++)
        SANS_ASSERT( fabs(xL[n] - xR[n]) < 1e-12 );

      //std::cout << "nL = " << nL << "  nR = " << nR << std::endl;

      //VectorArrayQ gradqjump = gradqR - gradqL;
      //ArrayQ<T> Ugradqjump = dot(U,gradqjump);

      //Ugradqjump += 0.5*(dot(gradqR,gradqR) - dot(gradqL,gradqL));
#if 0
      //Real hL = pow( xfldElemTraceL_.jacobianDeterminant(), 1./TopoDimTrace::D );
      Real UnL = dot(nL,U); // Normal velocity
      DLA::VectorS< PhysDim::D, Real > UsL = U - UnL*nL; // Tangential velocity
      //Real VL = sqrt( dot(UsL, UsL) );
#endif
#if 0
      //Real hR = pow( xfldElemTraceR_.jacobianDeterminant(), 1./TopoDimTrace::D );
      Real UnR = dot(nR,U); // Normal velocity
      DLA::VectorS< PhysDim::D, Real > UsR = U - UnR*nR; // Tangential velocity
      //Real VR = sqrt( dot(UsR, UsR) );
#endif

      //Pressure jump
#if 0
      // Compute the surface tangent gradient
      gradqL -= dot(nL,gradqL)*nL;
      gradqR -= dot(nR,gradqR)*nR;

      ArrayQ<T> UgradqR = dot(UsR,gradqR) - UnR*UnR;
      ArrayQ<T> UgradqL = dot(UsL,gradqL) - UnL*UnL;
      ArrayQ<T> Ugradqjump = UgradqR - UgradqL;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += phiR[k]*Ugradqjump;
#endif

#if 1
      Real aTE = 1;
      ArrayQ<T> delq = qR - qL;

      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += aTE*phiL[k]*(delq + gam);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += aTE*phiR[k]*(delq + gam);
#endif
    }

  protected:
    const PDE& pde_;
    const ElementXFieldFrame& xfldElemFrame_;
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementQFieldTrace& gamfldElemTrace_;
    const CanonicalTraceToCell canonicalFrame_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldCellL& xfldElemL_;
    const ElementQFieldCellL& qfldElemL_;
    const ElementXFieldCellR& xfldElemR_;
    const ElementQFieldCellR& qfldElemR_;
  };

  template<class T, class TopoDimFrame, class TopologyFrame,
                    class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCellL, class TopologyCellR>
  Functor<T, TopoDimFrame, TopologyFrame,
             TopoDimTrace, TopologyTrace,
             TopoDimCell, TopologyCellL, TopologyCellR>
  integrand(const ElementXField<PhysDim, TopoDimFrame, TopologyFrame>& xfldElemFrame,
            const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const Element<ArrayQ<T>    , TopoDimTrace, TopologyTrace>& gamfldElemTrace,
            const CanonicalTraceToCell& canonicalFrame,
            const CanonicalTraceToCell& canonicalTraceL,
            const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell , TopologyCellL >& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyCellL >& qfldElemL,
            const ElementXField<PhysDim, TopoDimCell , TopologyCellR >& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyCellR >& qfldElemR ) const
  {
    return {pde_, xfldElemFrame,
            xfldElemTrace,
            gamfldElemTrace,
            canonicalFrame,
            canonicalTraceL, canonicalTraceR,
            xfldElemL, qfldElemL,
            xfldElemR, qfldElemR };
  }

protected:
  const PDE& pde_;
  const std::vector<int> BoundaryFrameGroups_;
};

} //namespace SANS

#endif // INTEGRANDBOUNDARYFRAME_CG_POTENTIAL_ADJOINT_H
