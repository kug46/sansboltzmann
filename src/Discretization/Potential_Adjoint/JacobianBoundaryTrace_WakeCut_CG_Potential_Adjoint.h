// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_ADJOINT_H
#define JACOBIANBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_ADJOINT_H

// jacobian boundary-trace integral jacobian functions

#include "Surreal/SurrealS.h"

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
template<class IntegrandBoundaryTrace>
class JacobianBoundaryTrace_WakeCut_CG_Potential_Adjoint_impl :
    public GroupIntegralInteriorTraceType< JacobianBoundaryTrace_WakeCut_CG_Potential_Adjoint_impl<IntegrandBoundaryTrace> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template MatrixQ<Real> MatrixQ;

  typedef SurrealS<DLA::VectorSize<ArrayQ>::M> Surreal;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianBoundaryTrace_WakeCut_CG_Potential_Adjoint_impl(const IntegrandBoundaryTrace& fcn,
                                                          MatrixScatterAdd<MatrixQ>& mtxPDE_q, MatrixScatterAdd<MatrixQ>& mtxPDE_gam,
                                                          MatrixScatterAdd<MatrixQ>& mtxWake_q, MatrixScatterAdd<MatrixQ>& mtxWake_gam) :
    fcn_(fcn),
    mtxPDE_q_(mtxPDE_q), mtxPDE_gam_(mtxPDE_gam),
    mtxWake_q_(mtxWake_q), mtxWake_gam_(mtxWake_gam),
    comm_rank_(0) {}

  std::size_t nPeriodicTraceGroups() const { return fcn_.nPeriodicTraceGroups(); }
  std::size_t periodicTraceGroup(const int n) const { return fcn_.periodicTraceGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& gamfld )
  {
    SANS_ASSERT( mtxPDE_q_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxPDE_gam_.m() == qfld.nDOFpossessed() );
    SANS_ASSERT( mtxPDE_gam_.n() == gamfld.nDOFpossessed() + gamfld.nDOFghost() );

    SANS_ASSERT( mtxWake_q_.m() == gamfld.nDOFpossessed() );
    SANS_ASSERT( mtxWake_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );

    SANS_ASSERT( mtxWake_gam_.m() == gamfld.nDOFpossessed() );
    SANS_ASSERT( mtxWake_gam_.n() == gamfld.nDOFpossessed() + gamfld.nDOFghost() );

#ifdef SANS_MPI
    //comm_rank_ = qfld.comm()->rank();

    // MPI ranks assume a DG space below
    //SANS_ASSERT(qfld.spaceType() == SpaceType::Discontinuous);
#endif
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& gamfldTrace,
      int quadratureorder )
  {
    // Left types
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

    //Right types
    typedef typename XFieldType::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<Surreal> ElementQFieldClassR;

    //Trace types
    typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass gamfldElemTrace( gamfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    const int nDOFL = qfldElemL.nDOF();
    const int nDOFR = qfldElemR.nDOF();
    const int nDOFT = gamfldElemTrace.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL);
    std::vector<int> mapDOFGlobalR(nDOFR);
    std::vector<int> mapDOFGlobalT(nDOFT);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal, ArrayQSurreal> integral(quadratureorder, nDOFL, nDOFR, nDOFT);

    // element integrand/residuals
    std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
    std::vector<ArrayQSurreal> rsdPDEElemR( nDOFR );
    std::vector<ArrayQSurreal> rsdWakeTrace( nDOFT );

    // element PDE jacobian matrices wrt q and gam
    MatrixElemClass mtxElemL_PDE_qL (nDOFL,nDOFL);
    MatrixElemClass mtxElemL_PDE_qR (nDOFL,nDOFR);
    MatrixElemClass mtxElemL_PDE_gam(nDOFL,nDOFT);
    MatrixElemClass mtxElemR_PDE_qL (nDOFR,nDOFL);
    MatrixElemClass mtxElemR_PDE_qR (nDOFR,nDOFR);
    MatrixElemClass mtxElemR_PDE_gam(nDOFR,nDOFT);

    MatrixElemClass mtxElemT_Wake_qL (nDOFT,nDOFL);
    MatrixElemClass mtxElemT_Wake_qR (nDOFT,nDOFR);
    MatrixElemClass mtxElemT_Wake_gam(nDOFT,nDOFT);

    // number of simultaneous derivatives per functor call
    const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // left/right elements
      int elemL = xfldTrace.getElementLeft( elem );
      int elemR = xfldTrace.getElementRight( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );
      gamfldTrace.getElement( gamfldElemTrace, elem );

      //const int rankL = qfldElemL.rank();
      //const int rankR = qfldElemR.rank();

      // nothing to do if both elements are ghost elements
      //if (rankL != comm_rank_ && rankR != comm_rank_) continue;


      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(nDOFL + nDOFR + nDOFT); nchunk += nDeriv)
      {
        // associate derivative slots with solution variables

        int slot = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            for (int n = 0; n < nEqn; n++)
              DLA::index(qfldElemL.DOF(j), n).deriv(slot + n - nchunk) = 1;
          slot += nEqn;
        }
        for (int j = 0; j < nDOFR; j++)
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            for (int n = 0; n < nEqn; n++)
              DLA::index(qfldElemR.DOF(j), n).deriv(slot + n - nchunk) = 1;
          slot += nEqn;
        }
        for (int j = 0; j < nDOFT; j++)
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            for (int n = 0; n < nEqn; n++)
              DLA::index(gamfldElemTrace.DOF(j), n).deriv(slot + n - nchunk) = 1;
          slot += nEqn;
        }


        // reset PDE residuals to zero
        for (int n = 0; n < nDOFL; n++) rsdPDEElemL[n] = 0;
        for (int n = 0; n < nDOFR; n++) rsdPDEElemR[n] = 0;
        for (int n = 0; n < nDOFT; n++) rsdWakeTrace[n] = 0;

        // trace integration for canonical element
        integral( fcn_.integrand( xfldElemTrace, gamfldElemTrace,
                             canonicalTraceL, canonicalTraceR,
                             xfldElemL, qfldElemL,
                             xfldElemR, qfldElemR),
                  xfldElemTrace,
                  rsdPDEElemL.data(), nDOFL,
                  rsdPDEElemR.data(), nDOFR,
                  rsdWakeTrace.data(), nDOFT);

        // accumulate derivatives into element jacobian

        slot = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int n = 0; n < nEqn; n++)
              DLA::index(qfldElemL.DOF(j), n).deriv(slot + n - nchunk) = 0;

            for (int i = 0; i < nDOFL; i++)
              for (int m = 0; m < nEqn; m++)
                for (int n = 0; n < nEqn; n++)
                  DLA::index(mtxElemL_PDE_qL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot + n - nchunk);

            for (int i = 0; i < nDOFR; i++)
              for (int m = 0; m < nEqn; m++)
                for (int n = 0; n < nEqn; n++)
                  DLA::index(mtxElemR_PDE_qL(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot + n - nchunk);

            for (int i = 0; i < nDOFT; i++)
              for (int m = 0; m < nEqn; m++)
                for (int n = 0; n < nEqn; n++)
                  DLA::index(mtxElemT_Wake_qL(i,j), m,n) = DLA::index(rsdWakeTrace[i], m).deriv(slot + n - nchunk);
          }
          slot += nEqn;
        }

        for (int j = 0; j < nDOFR; j++)
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int n = 0; n < nEqn; n++)
              DLA::index(qfldElemR.DOF(j), n).deriv(slot + n - nchunk) = 0;

            for (int i = 0; i < nDOFL; i++)
              for (int m = 0; m < nEqn; m++)
                for (int n = 0; n < nEqn; n++)
                  DLA::index(mtxElemL_PDE_qR(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot + n - nchunk);

            for (int i = 0; i < nDOFR; i++)
              for (int m = 0; m < nEqn; m++)
                for (int n = 0; n < nEqn; n++)
                  DLA::index(mtxElemR_PDE_qR(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot + n - nchunk);

            for (int i = 0; i < nDOFT; i++)
              for (int m = 0; m < nEqn; m++)
                for (int n = 0; n < nEqn; n++)
                  DLA::index(mtxElemT_Wake_qR(i,j), m,n) = DLA::index(rsdWakeTrace[i], m).deriv(slot + n - nchunk);
          }
          slot += nEqn;
        }

        for (int j = 0; j < nDOFT; j++)
        {
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int n = 0; n < nEqn; n++)
              DLA::index(gamfldElemTrace.DOF(j), n).deriv(slot + n - nchunk) = 0;

            for (int i = 0; i < nDOFL; i++)
              for (int m = 0; m < nEqn; m++)
                for (int n = 0; n < nEqn; n++)
                  DLA::index(mtxElemL_PDE_gam(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot + n - nchunk);

            for (int i = 0; i < nDOFR; i++)
              for (int m = 0; m < nEqn; m++)
                for (int n = 0; n < nEqn; n++)
                  DLA::index(mtxElemR_PDE_gam(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot + n - nchunk);

            for (int i = 0; i < nDOFT; i++)
              for (int m = 0; m < nEqn; m++)
                for (int n = 0; n < nEqn; n++)
                  DLA::index(mtxElemT_Wake_gam(i,j), m,n) = DLA::index(rsdWakeTrace[i], m).deriv(slot + n - nchunk);
          }
          slot += nEqn;
        }
      }   // nchunk

      // scatter-add element jacobian to global

      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );
      qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nDOFR );
      gamfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobalT.data(), nDOFT );

      // jacobian wrt qL
      mtxPDE_q_.scatterAdd( mtxElemL_PDE_qL, mapDOFGlobalL.data(), nDOFL, mapDOFGlobalL.data(), nDOFL );
      mtxPDE_q_.scatterAdd( mtxElemR_PDE_qL, mapDOFGlobalR.data(), nDOFR, mapDOFGlobalL.data(), nDOFL );
      mtxWake_q_.scatterAdd( mtxElemT_Wake_qL, mapDOFGlobalT.data(), nDOFT, mapDOFGlobalL.data(), nDOFL );

      // jacobian qrt qR
      mtxPDE_q_.scatterAdd( mtxElemL_PDE_qR, mapDOFGlobalL.data(), nDOFL, mapDOFGlobalR.data(), nDOFR );
      mtxPDE_q_.scatterAdd( mtxElemR_PDE_qR, mapDOFGlobalR.data(), nDOFR, mapDOFGlobalR.data(), nDOFR );
      mtxWake_q_.scatterAdd( mtxElemT_Wake_qR, mapDOFGlobalT.data(), nDOFT, mapDOFGlobalR.data(), nDOFR );

      // jacobian wrt gam
      mtxPDE_gam_.scatterAdd( mtxElemL_PDE_gam, mapDOFGlobalL.data(), nDOFL, mapDOFGlobalT.data(), nDOFT );
      mtxPDE_gam_.scatterAdd( mtxElemR_PDE_gam, mapDOFGlobalR.data(), nDOFR, mapDOFGlobalT.data(), nDOFT );
      mtxWake_gam_.scatterAdd( mtxElemT_Wake_gam, mapDOFGlobalT.data(), nDOFT, mapDOFGlobalT.data(), nDOFT );
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxPDE_q_;
  MatrixScatterAdd<MatrixQ>& mtxPDE_gam_;
  MatrixScatterAdd<MatrixQ>& mtxWake_q_;
  MatrixScatterAdd<MatrixQ>& mtxWake_gam_;
  mutable int comm_rank_;
};

// Factory function

template<class IntegrandBoundaryTrace, class MatrixQ>
JacobianBoundaryTrace_WakeCut_CG_Potential_Adjoint_impl<IntegrandBoundaryTrace>
JacobianBoundaryTrace_WakeCut_CG_Potential_Adjoint( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                                    MatrixScatterAdd<MatrixQ>& mtxPDE_q, MatrixScatterAdd<MatrixQ>& mtxPDE_gam,
                                                    MatrixScatterAdd<MatrixQ>& mtxWake_q, MatrixScatterAdd<MatrixQ>& mtxWake_gam )
{
  return { fcn.cast(), mtxPDE_q, mtxPDE_gam,
                       mtxWake_q, mtxWake_gam };
}

} // namespace SANS

#endif  // JACOBIANBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_ADJOINT_H
