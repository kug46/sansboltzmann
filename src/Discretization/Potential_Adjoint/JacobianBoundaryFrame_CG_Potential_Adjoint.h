// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYFRAME_CG_POTENTIAL_ADJOINT_H
#define JACOBIANBOUNDARYFRAME_CG_POTENTIAL_ADJOINT_H

// jacobian boundary-frame integral jacobian functions

#include <vector>

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/Element.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Potential_Adjoint/IntegrandBoundaryFrame_CG_LIP_Adjoint.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  CG_Potential_Adjoint boundary-trace integral
//
//  functions dispatched based on left (L) element topology
//
//  process:
//  (a) loop over groups; dispatch to L (JacobianPDE_LeftTopology_Integral2DBoundaryEdge)
//  (b) call base class routine with specific L (JacobianPDE_Group_Integral2DBoundaryEdge w/ Base& params)
//  (c) cast to specific L and call L-specific topology routine (JacobianPDE_Group_Integral2DBoundaryEdge)
//  (d) loop over edges and integrate


template <class Surreal,
          class TopologyFrame,
          class TopologyTrace,
          class TopologyCellL, class TopologyCellR,
          class XFieldType,
          class PhysDim, class TopoDim, class ArrayQ,
          class PDE,
          class SparseMatrix>
void
JacobianBoundaryFrame_CG_Potential_Adjoint_Group_Integral(
    const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& gamfldTrace,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCellL>& xfldCellL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCellL>& qfldCellL,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCellR>& xfldCellR,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCellR>& qfldCellR,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_gam,
    SparseMatrix& mtxGlobalWake_q, SparseMatrix& mtxGlobalWake_gam)
{
  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCellL> XFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCellL> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCellR> XFieldCellGroupTypeR;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCellR> QFieldCellGroupTypeR;

  typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
  typedef typename QFieldCellGroupTypeR::template ElementType<Surreal> ElementQFieldClassR;

  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
  typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldTraceClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

  typedef typename XFieldType::FieldFrameGroupType XFieldFrameGroupType;
  typedef typename XFieldFrameGroupType::template ElementType<> ElementXFieldFrameClass;
  typedef typename ElementXFieldFrameClass::TopoDim TopoDimFrame;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementXFieldClassR xfldElemR( xfldCellR.basis() );

  ElementQFieldClassL qfldElemL( qfldCellL.basis() );
  ElementQFieldClassR qfldElemR( qfldCellR.basis() );

  ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
  ElementQFieldTraceClass gamfldElemTrace( gamfldTrace.basis() );

  ElementXFieldFrameClass xfldElemFrame( xfldFrame.basis() );

  // variables/equations per DOF
  const int nEqn = PDE::N;

  // DOFs per element
  const int nDOFL = qfldElemL.nDOF();
  const int nDOFR = qfldElemR.nDOF();
  const int nDOFT = gamfldElemTrace.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobalL(nDOFL);
  std::vector<int> mapDOFGlobalR(nDOFR);
  std::vector<int> mapDOFGlobalT( nDOFT );

  // trace element integral
  GalerkinWeightedIntegral<TopoDimFrame, TopologyFrame, ArrayQSurreal, ArrayQSurreal, ArrayQSurreal>
      integral(quadratureorder, nDOFL, nDOFR, nDOFT);

  // element integrand/residuals
  std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
  std::vector<ArrayQSurreal> rsdPDEElemR( nDOFR );
  std::vector<ArrayQSurreal> rsdPDEElemT( nDOFT );

  // element jacobians
  MatrixElemClass mtxPDEElemL_qL(nDOFL, nDOFL);
  MatrixElemClass mtxPDEElemL_qR(nDOFL, nDOFR);
  MatrixElemClass mtxPDEElemL_gam(nDOFL, nDOFT);

  MatrixElemClass mtxPDEElemR_qL(nDOFR, nDOFL);
  MatrixElemClass mtxPDEElemR_qR(nDOFR, nDOFR);
  MatrixElemClass mtxPDEElemR_gam(nDOFR, nDOFT);

  MatrixElemClass mtxWakeElem_qL(nDOFT, nDOFL);
  MatrixElemClass mtxWakeElem_qR(nDOFT, nDOFR);
  MatrixElemClass mtxWakeElem_gam(nDOFT, nDOFT);

  // loop over elements within group
  int nelem = xfldFrame.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // zero element Jacobians
    mtxPDEElemL_qL = 0;
    mtxPDEElemL_qR = 0;
    mtxPDEElemL_gam = 0;

    mtxPDEElemR_qL = 0;
    mtxPDEElemR_qR = 0;
    mtxPDEElemR_gam = 0;

    mtxWakeElem_qL = 0;
    mtxWakeElem_qR = 0;
    mtxWakeElem_gam = 0;

    xfldFrame.getElement(xfldElemFrame, elem);

    const int elemTrace = xfldFrame.getElementLeft( elem );
    CanonicalTraceToCell& canonicalFrame  = xfldFrame.getCanonicalTraceLeft( elem );

    xfldTrace.getElement( xfldElemTrace, elemTrace );
    gamfldTrace.getElement( gamfldElemTrace, elemTrace );

    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elemTrace );
    CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elemTrace );

    const int elemL = xfldTrace.getElementLeft( elemTrace );
    const int elemR = xfldTrace.getElementRight( elemTrace );

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldCellR.getElement( xfldElemR, elemR );
    qfldCellR.getElement( qfldElemR, elemR );

    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nDOFR );
    gamfldTrace.associativity( elemTrace ).getGlobalMapping( mapDOFGlobalT.data(), nDOFT );

    // number of simultaneous derivatives per functor call
    const int nDeriv = PDE::N;

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nEqn*(nDOFL+nDOFR+nDOFT); nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot = 0;
      for (int j = 0; j < nDOFL; j++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < nEqn; n++)
            DLA::index(qfldElemL.DOF(j), n).deriv(slot + n - nchunk) = 1;

        slot += nEqn;
      }
      for (int j = 0; j < nDOFR; j++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < nEqn; n++)
            DLA::index(qfldElemR.DOF(j), n).deriv(slot + n - nchunk) = 1;

        slot += nEqn;
      }
      for (int j = 0; j < nDOFT; j++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          for (int n = 0; n < nEqn; n++)
            DLA::index(gamfldElemTrace.DOF(j), n).deriv(slot + n - nchunk) = 1;

        slot += nEqn;
      }

      // line integration for canonical element
      for (int n = 0; n < nDOFL; n++)
        rsdPDEElemL[n] = 0;

      for (int n = 0; n < nDOFR; n++)
        rsdPDEElemR[n] = 0;

      for (int n = 0; n < nDOFT; n++)
        rsdPDEElemT[n] = 0;

      integral(
          fcn.integrand(xfldElemFrame,
                        xfldElemTrace,
                        gamfldElemTrace,
                        canonicalFrame,
                        canonicalTraceL, canonicalTraceR,
                        xfldElemL, qfldElemL,
                        xfldElemR, qfldElemR ),
          xfldElemFrame,
          rsdPDEElemL.data(), nDOFL,
          rsdPDEElemR.data(), nDOFR,
          rsdPDEElemT.data(), nDOFT );

      // accumulate derivatives into element jacobian

      slot = 0;
      for (int j = 0; j < nDOFL; j++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int k = 0; k < nDeriv; k++)
            for (int n = 0; n < nEqn; n++)
              DLA::index(qfldElemL.DOF(j), n).deriv(slot + n - nchunk) = 0;

          for (int i = 0; i < nDOFL; i++)
            for (int m = 0; m < nEqn; m++)
              for (int n = 0; n < nEqn; n++)
                DLA::index(mtxPDEElemL_qL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot + n - nchunk);

          for (int i = 0; i < nDOFR; i++)
            for (int m = 0; m < nEqn; m++)
              for (int n = 0; n < nEqn; n++)
                DLA::index(mtxPDEElemR_qL(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot + n - nchunk);

          for (int i = 0; i < nDOFT; i++)
            for (int m = 0; m < nEqn; m++)
              for (int n = 0; n < nEqn; n++)
                DLA::index(mtxWakeElem_qL(i,j), m,n) = DLA::index(rsdPDEElemT[i], m).deriv(slot + n - nchunk);
        }
        slot += nEqn;
      }

      for (int j = 0; j < nDOFR; j++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int k = 0; k < nDeriv; k++)
            for (int n = 0; n < nEqn; n++)
              DLA::index(qfldElemR.DOF(j), n).deriv(slot + n - nchunk) = 0;

          for (int i = 0; i < nDOFL; i++)
            for (int m = 0; m < nEqn; m++)
              for (int n = 0; n < nEqn; n++)
                DLA::index(mtxPDEElemL_qR(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot + n - nchunk);

          for (int i = 0; i < nDOFR; i++)
            for (int m = 0; m < nEqn; m++)
              for (int n = 0; n < nEqn; n++)
                DLA::index(mtxPDEElemR_qR(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot + n - nchunk);

          for (int i = 0; i < nDOFT; i++)
            for (int m = 0; m < nEqn; m++)
              for (int n = 0; n < nEqn; n++)
                DLA::index(mtxWakeElem_qR(i,j), m,n) = DLA::index(rsdPDEElemT[i], m).deriv(slot + n - nchunk);
        }
        slot += nEqn;
      }

      for (int j = 0; j < nDOFT; j++)
      {
        if ((slot >= nchunk) && (slot < nchunk + nDeriv))
        {
          for (int k = 0; k < nDeriv; k++)
            for (int n = 0; n < nEqn; n++)
              DLA::index(gamfldElemTrace.DOF(j), n).deriv(slot + n - nchunk) = 0;

          for (int i = 0; i < nDOFL; i++)
            for (int m = 0; m < nEqn; m++)
              for (int n = 0; n < nEqn; n++)
                DLA::index(mtxPDEElemL_gam(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot + n - nchunk);

          for (int i = 0; i < nDOFR; i++)
            for (int m = 0; m < nEqn; m++)
              for (int n = 0; n < nEqn; n++)
                DLA::index(mtxPDEElemR_gam(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot + n - nchunk);

          for (int i = 0; i < nDOFT; i++)
            for (int m = 0; m < nEqn; m++)
              for (int n = 0; n < nEqn; n++)
                DLA::index(mtxWakeElem_gam(i,j), m,n) = DLA::index(rsdPDEElemT[i], m).deriv(slot + n - nchunk);
        }
        slot += nEqn;
      }

    }   // nchunk

    // scatter-add element jacobian to global

    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qL, mapDOFGlobalL.data(), nDOFL, mapDOFGlobalL.data(), nDOFL );
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qR, mapDOFGlobalL.data(), nDOFL, mapDOFGlobalR.data(), nDOFR );
    mtxGlobalPDE_gam.scatterAdd( mtxPDEElemL_gam, mapDOFGlobalL.data(), nDOFL, mapDOFGlobalT.data(), nDOFT );

    mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qL, mapDOFGlobalR.data(), nDOFR, mapDOFGlobalL.data(), nDOFL );
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qR, mapDOFGlobalR.data(), nDOFR, mapDOFGlobalR.data(), nDOFR );
    mtxGlobalPDE_gam.scatterAdd( mtxPDEElemR_gam, mapDOFGlobalR.data(), nDOFR, mapDOFGlobalT.data(), nDOFT );

    mtxGlobalWake_q.scatterAdd( mtxWakeElem_qL, mapDOFGlobalT.data(), nDOFT, mapDOFGlobalL.data(), nDOFL );
    mtxGlobalWake_q.scatterAdd( mtxWakeElem_qR, mapDOFGlobalT.data(), nDOFT, mapDOFGlobalR.data(), nDOFR );
    mtxGlobalWake_gam.scatterAdd( mtxWakeElem_gam, mapDOFGlobalT.data(), nDOFT, mapDOFGlobalT.data(), nDOFT );
  }
}


//----------------------------------------------------------------------------//
template <class Surreal, class TopologyFrame,
          class TopologyTrace,
          class TopologyCellL, class TopologyCellR,
          class XFieldType,
          class PDE,
          class PhysDim, class TopoDim, class ArrayQ,
          class SparseMatrix>
void
JacobianBoundaryFrame_CG_Potential_Adjoint_Group(
    const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
    const typename XFieldType::FieldFrameGroupType& xfldFrame,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& gamfldTrace,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_gam,
    SparseMatrix& mtxGlobalWake_q, SparseMatrix& mtxGlobalWake_gam)
{
  const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

  int groupL = xfldTrace.getGroupLeft();
  int groupR = xfldTrace.getGroupRight();

  // Integrate over the trace group
  JacobianBoundaryFrame_CG_Potential_Adjoint_Group_Integral<Surreal,
                                                TopologyFrame, TopologyTrace,
                                                TopologyCellL, TopologyCellR,
                                                XFieldType,PhysDim,TopoDim,ArrayQ>(
      fcn, xfldFrame, gamfldTrace, xfldTrace,
      xfld.template getCellGroup<TopologyCellL>(groupL), qfld.template getCellGroup<TopologyCellL>(groupL),
      xfld.template getCellGroup<TopologyCellR>(groupR), qfld.template getCellGroup<TopologyCellR>(groupR),
      quadratureorder,
      mtxGlobalPDE_q, mtxGlobalPDE_gam,
      mtxGlobalWake_q, mtxGlobalWake_gam );
}

template<class Surreal, class TopoDim>
class JacobianBoundaryFrame_CG_Potential_Adjoint;
#if 0
template<class Surreal>
class JacobianBoundaryFrame_CG_Potential_Adjoint<Surreal,TopoD1>
{
public:
  typedef TopoD1 TopoDim;

  //----------------------------------------------------------------------------//
  template <class TopologyTrace,
            class PDE, class BC,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  LeftTopology(
      const IntegrandBoundary_CG_Potential_Adjoint_PDE<PDE, BC>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_CG_Potential_Adjoint_Group<Surreal, TopologyTrace, Line>(
          fcn, xfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD1>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class PDE, class BC,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandBoundary_CG_Potential_Adjoint_PDE<PDE, BC>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q )
  {
    SANS_ASSERT( mtxGlobalPDE_q.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q.n() == qfld.nDOF() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over line element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Node) )
      {
        // dispatch to determine left topology
        LeftTopology<Node>(
            fcn,
            xfld.template getBoundaryTraceGroup<Node>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup],
            mtxGlobalPDE_q );
      }
      else
      {
        char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD1>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};


template<class Surreal>
class JacobianBoundaryFrame_CG_Potential_Adjoint<Surreal,TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  //----------------------------------------------------------------------------//
  template <class TopologyTrace,
            class PDE, class BC,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  LeftTopology(
      const IntegrandBoundary_CG_Potential_Adjoint_PDE<PDE, BC>& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_CG_Potential_Adjoint_Group<Surreal, TopologyTrace, Triangle>(
          fcn, xfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class PDE, class BC,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandBoundary_CG_Potential_Adjoint_PDE<PDE, BC>& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q )
  {
    SANS_ASSERT( mtxGlobalPDE_q.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q.n() == qfld.nDOF() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over line element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        // dispatch to determine left topology
        LeftTopology<Line>(
            fcn,
            xfld.template getBoundaryTraceGroup<Line>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup],
            mtxGlobalPDE_q );
      }
      else
      {
        char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};
#endif


template<class Surreal>
class JacobianBoundaryFrame_CG_Potential_Adjoint<Surreal,TopoD3>
{
  typedef TopoD3 TopoDim;
protected:

//----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyCellL,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellRight_Triangle(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<Triangle>& gamfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_gam,
      SparseMatrix& mtxGlobalWake_q, SparseMatrix& mtxGlobalWake_gam)
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_CG_Potential_Adjoint_Group<Surreal, TopologyFrame, Triangle, TopologyCellL, Tet, XFieldType>(
          fcn, xfldFrame, xfldTrace, gamfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_gam,
          mtxGlobalWake_q, mtxGlobalWake_gam );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellLeft_Triangle(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<Triangle>& gamfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_gam,
      SparseMatrix& mtxGlobalWake_q, SparseMatrix& mtxGlobalWake_gam)
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      TopologyCellRight_Triangle<TopologyFrame, Tet, XFieldType>(
          fcn, xfldFrame, xfldTrace, gamfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_gam,
          mtxGlobalWake_q, mtxGlobalWake_gam );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//----------------------------------------------------------------------------//
  template <class TopologyFrame, class TopologyCellL,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellRight_Quad(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<Quad>& gamfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_gam,
      SparseMatrix& mtxGlobalWake_q, SparseMatrix& mtxGlobalWake_gam)
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_CG_Potential_Adjoint_Group<Surreal, TopologyFrame, Quad, TopologyCellL, Hex, XFieldType>(
          fcn, xfldFrame, xfldTrace, gamfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_gam,
          mtxGlobalWake_q, mtxGlobalWake_gam );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//----------------------------------------------------------------------------//
  template <class TopologyFrame,
            class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellLeft_Quad(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<Quad>& gamfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_gam,
      SparseMatrix& mtxGlobalWake_q, SparseMatrix& mtxGlobalWake_gam)
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      TopologyCellRight_Quad<TopologyFrame, Hex, XFieldType>(
          fcn, xfldFrame, xfldTrace, gamfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_gam,
          mtxGlobalWake_q, mtxGlobalWake_gam );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

//----------------------------------------------------------------------------//
  template <class TopologyFrame, class XFieldType, class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TraceTopologyLeft(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const Field<PhysDim, TopoDim, ArrayQ>& gamfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_gam,
      SparseMatrix& mtxGlobalWake_q, SparseMatrix& mtxGlobalWake_gam)
  {
    const int boundaryGroupL = xfldFrame.getGroupLeft();

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    // get the boundary group
    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Triangle) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Triangle<TopologyFrame, XFieldType>(
          fcn, xfldFrame,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupL),
          gamfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_gam,
          mtxGlobalWake_q, mtxGlobalWake_gam );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Quad) )
    {
      // dispatch to determine left cell topology
      TopologyCellLeft_Quad<TopologyFrame, XFieldType>(
          fcn, xfldFrame,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupL),
          gamfld.template getBoundaryTraceGroupGlobal<Quad>(boundaryGroupL),
          qfld,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_gam,
          mtxGlobalWake_q, mtxGlobalWake_gam );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryFrame_CG_Potential_Adjoint<TopoD3>::integrate: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class XFieldType, class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandBoundaryFrame_CG_Potential_Adjoint<PDE>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& gamfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q, SparseMatrix& mtxGlobalPDE_gam,
      SparseMatrix& mtxGlobalWake_q, SparseMatrix& mtxGlobalWake_gam)
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );

    SANS_ASSERT( ngroup == xfld.nBoundaryFrameGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryFrameGroups(); group++)
    {
      const int boundaryFrameGroup = fcn.boundaryFrameGroup(group);

      TraceTopologyLeft<Line, XFieldType>( fcn,
          xfld.getBoundaryFrameGroup(boundaryFrameGroup),
          gamfld,
          qfld,
          quadratureorder[boundaryFrameGroup],
          mtxGlobalPDE_q, mtxGlobalPDE_gam,
          mtxGlobalWake_q, mtxGlobalWake_gam );

    }
  }
};


}

#endif  // JACOBIANBOUNDARYFRAME_CG_POTENTIAL_ADJOINT_H
