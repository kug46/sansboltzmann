// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_GALERKIN_WAKECUT_CG_LIP_ADJOINT_H
#define INTEGRANDBOUNDARYTRACE_GALERKIN_WAKECUT_CG_LIP_ADJOINT_H

// cell/trace integrand operators: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"

#include "BC_WakeCut_Potential_Adjoint.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Trace integrand: Galerkin

template <template<class,class> class NDConvert, class Tg, class NDBCVector>
class IntegrandBoundaryTrace<NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>>,
                             NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Adjoint>, Galerkin > :
  public IntegrandBoundaryTraceType<IntegrandBoundaryTrace<NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>>,
                                                           NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Adjoint>, Galerkin> >
{
public:
  typedef NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>> PDE;
  typedef BC_WakeCut_Potential_Adjoint BC;
  typedef BCCategory::WakeCut_Potential_Adjoint Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(const PDE& pde, const std::vector<int>& BoundaryGroups)
    :  pde_(pde), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nPeriodicTraceGroups() const { return BoundaryGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getPeriodicTraceGroups() const { return BoundaryGroups_; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  class BasisWeighted
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    BasisWeighted( const PDE& pde,
                   const ElementXFieldTrace& xfldElemTrace,
                   const ElementQFieldTrace& gamfldElemTrace,
                   const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
                   const ElementXFieldL& xfldElemL,
                   const ElementQFieldL& qfldElemL,
                   const ElementXFieldR& xfldElemR,
                   const ElementQFieldR& qfldElemR ) :
                     pde_(pde),
                     xfldElemTrace_(xfldElemTrace), gamfldElemTrace_(gamfldElemTrace),
                     canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
                     xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
                     xfldElemR_(xfldElemR), qfldElemR_(qfldElemR) {}


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int nrsdL,
                                                     ArrayQ<Ti> integrandR[], const int nrsdR,
                                                     ArrayQ<Ti> integrandT[], const int nrsdT ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const ElementQFieldTrace& gamfldElemTrace_;
    const CanonicalTraceToCell& canonicalTraceL_;
    const CanonicalTraceToCell& canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  BasisWeighted<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL, TopologyR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const Element<ArrayQ<T>    , TopoDimTrace, TopologyTrace>& gamfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell , TopologyL    >& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElemL,
            const ElementXField<PhysDim, TopoDimCell , TopologyR    >& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyR    >& qfldElemR) const
  {
    return {pde_,
            xfldElemTrace, gamfldElemTrace,
            canonicalTraceL, canonicalTraceR,
            xfldElemL, qfldElemL,
            xfldElemR, qfldElemR};
  }

protected:
  const PDE& pde_;
  const std::vector<int> BoundaryGroups_;
};


template <template<class,class> class NDConvert, class Tg, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
template <class Ti>
void
IntegrandBoundaryTrace<NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>>,
                             NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Adjoint>, Galerkin >::
BasisWeighted<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::operator()(
    const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandL[], const int nrsdL,
                                        ArrayQ<Ti> integrandR[], const int nrsdR,
                                        ArrayQ<Ti> integrandT[], const int nrsdT) const
{
  typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

  const int nDOFL = qfldElemL_.nDOF();             // total solution DOFs in left element
  const int nDOFR = qfldElemR_.nDOF();             // total solution DOFs in right element
  const int nDOFT = gamfldElemTrace_.nDOF();       // total solution DOFs in trace element

  SANS_ASSERT(nrsdL == nDOFL);
  SANS_ASSERT(nrsdR == nDOFR);
  SANS_ASSERT(nrsdT == nDOFT);

  VectorX X = 0;
  VectorX nL, nR;           // unit normal for left element (points to right element)

  std::vector<Real> phiL(nDOFL);         // basis (left)
  std::vector<VectorX> gradphiL(nDOFL);  // basis gradient (left)
  std::vector<Real> phiR(nDOFR);         // basis (right)
  std::vector<VectorX> gradphiR(nDOFR);  // basis gradient (right)
  std::vector<Real> phiT(nDOFT);         // basis (trace)
  std::vector<VectorX> gradphiT(nDOFT);  // basis gradient (trace)

  ArrayQ<T> qL, qR, gam;             // solution
  VectorArrayQ gradqL, gradqR, gradgam;  // gradient

  DLA::VectorS< PhysDim::D, Ti > U; // Freestream Velocity

  T wa, wb, wc; // weights

  QuadPointCellType sRefL;              // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, RefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, RefTrace, sRefR );

  // physical coordinates
  xfldElemTrace_.coordinates( RefTrace, X );

  // unit normal: L points to R
  xfldElemTrace_.unitNormal( RefTrace, nL );
  nR = -nL;

  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL.data(), nDOFL );
  qfldElemR_.evalBasis( sRefR, phiR.data(), nDOFR );
  gamfldElemTrace_.evalBasis( RefTrace, phiT.data(), nDOFT );

  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL.data(), nDOFL );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR.data(), nDOFR );
  xfldElemTrace_.evalBasisGradient( RefTrace, gamfldElemTrace_, gradphiT.data(), nDOFT );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL.data(), nDOFL, qL );
  qfldElemR_.evalFromBasis( phiR.data(), nDOFR, qR );
  gamfldElemTrace_.evalFromBasis( phiT.data(), nDOFT, gam );

  qfldElemL_.evalFromBasis( gradphiL.data(), nDOFL, gradqL );
  qfldElemR_.evalFromBasis( gradphiR.data(), nDOFR, gradqR );
  gamfldElemTrace_.evalFromBasis( gradphiT.data(), nDOFT, gradgam );

  // change to tangential gradient
  for (int k = 0; k < nDOFT; k++)
    gradphiT[k] -= dot(nL,gradphiT[k])*nL;

  gradgam -= dot(nL,gradgam)*nL;

  for (int k = 0; k < nDOFL; k++) integrandL[k] = 0;
  for (int k = 0; k < nDOFR; k++) integrandR[k] = 0;
  for (int k = 0; k < nDOFT; k++) integrandT[k] = 0;

  Real h = pow( xfldElemTrace_.jacobianDeterminant(), 1./TopoDimTrace::D );

  U = pde_.Up(X);                                // freestream velocity
  // TODO: need gradU

  Ti Un = dot(nL,U);                             // Normal velocity
  DLA::VectorS< PhysDim::D, Ti > Us = U - Un*nL; // Tangential velocity

  Ti V = sqrt( dot(Us, Us) );

  // Units of the weights
  //
  // wa O(1)
  // wb O(1/L)
  // wc O(L)

  Real a1 = 1;    // O(1)
  Real a3 = 0;    // O(h)
  Real a4 = -h;   // O(h)

  Real b1 = -1./h; // O(1/h)
  Real b3 = 0;     // O(1)
  Real b4 = a1;    // O(1)

  Ti c1 = 1/V; // O(1/V)
  Ti c3 = h/V; // O(h/V)
  Ti c4 = h/V; // O(h/V)

  // Theta is O(1/V)
  Ti t1 = 0;
  Ti t4 = 0;
  Ti t7 = 0;
  Ti t8 = 1/V;
  Ti t9 = 1/V;

  T gradqnL = dot(nL,gradqL);
  T gradqnR = dot(nR,gradqR);

  ArrayQ<T> delq      = qR - qL;
  ArrayQ<T> sumgradqn = gradqnR + gradqnL;

  ArrayQ<T> Ugradgam = dot(Us,gradgam);

//  sumphi = phiR + phiL;
//  delphi = phiR - phiL;
//
//  sumgradphin = dot(nR,gradphiR) + dot(nL,gradphiL);
//  delgradphin = dot(nR,gradphiR) - dot(nL,gradphiL);
//
//  sumgradphi = (gradphiR - dot(nR,gradphiR)*nR) + (gradphiL - dot(nL,gradphiL)*nL);
//  delgradphi = (gradphiR - dot(nR,gradphiR)*nR) - (gradphiL - dot(nL,gradphiL)*nL);


  for (int k = 0; k < nDOFR; k++)
  {
    Real sumphi = phiR[k];
    Real delphi = phiR[k];

    Real sumgradphin = dot(nR,gradphiR[k]);
    Real delgradphin = dot(nR,gradphiR[k]);

    VectorX gradsumphi = (gradphiR[k] - dot(nR,gradphiR[k])*nR);
    VectorX graddelphi = (gradphiR[k] - dot(nR,gradphiR[k])*nR);

    wa = a1*sumphi
       - 0.5*delphi
       + a3*sumgradphin
       + a4*delgradphin
       + 2*a3*t8*( /* gradU + */  dot(Us,graddelphi))
       - 2*(a4*t1 - a3*(t7 - t4))*dot(Us,gradsumphi);

    wb = b1*sumphi
       + b3*sumgradphin
       + b4*delgradphin
       - (1 - 2*b3)*t8*( /* gradU + */   dot(Us,graddelphi))
       - (t7 - 2*b3*(t7 - t4) + 2*b4*t1)*dot(Us,gradsumphi);

    wc = c1*sumphi
       + c3*sumgradphin
       + c4*delgradphin
       - t8*delphi
       + 2*c3*( /* gradU + */     dot(Us,graddelphi))
       + 2*(c3*(t7 - t4) - c4*t1)*dot(Us,gradsumphi);

    integrandR[k] = -phiR[k]*gradqnR // integration by parts term
                  + wa*sumgradqn
                  + wb*(delq + gam)
                  + wc*Ugradgam;
  }

  for (int k = 0; k < nDOFL; k++)
  {
    Real sumphi =   phiL[k];
    Real delphi = - phiL[k];

    Real sumgradphin =   dot(nL,gradphiL[k]);
    Real delgradphin = - dot(nL,gradphiL[k]);

    VectorX gradsumphi =   (gradphiL[k] - dot(nL,gradphiL[k])*nL);
    VectorX graddelphi = - (gradphiL[k] - dot(nL,gradphiL[k])*nL);

    wa = a1*sumphi
       - 0.5*delphi
       + a3*sumgradphin
       + a4*delgradphin
       + 2*a3*t8*( /* gradU + */  dot(Us,graddelphi))
       - 2*(a4*t1 - a3*(t7 - t4))*dot(Us,gradsumphi);

    wb = b1*sumphi
       + b3*sumgradphin
       + b4*delgradphin
       - (1 - 2*b3)*t8*( /* gradU + */   dot(Us,graddelphi))
       - (t7 - 2*b3*(t7 - t4) + 2*b4*t1)*dot(Us,gradsumphi);

    wc = c1*sumphi
       + c3*sumgradphin
       + c4*delgradphin
       - t8*delphi
       + 2*c3*( /* gradU + */     dot(Us,graddelphi))
       + 2*(c3*(t7 - t4) - c4*t1)*dot(Us,gradsumphi);

    integrandL[k] = -phiL[k]*gradqnL // integration by parts term
                  + wa*sumgradqn
                  + wb*(delq + gam)
                  + wc*Ugradgam;
  }

  for (int k = 0; k < nDOFT; k++)
  {
    Ti UgradphiT = dot(Us,gradphiT[k]);

    wa = 2*a3*t9*( /* gradU + */ UgradphiT);

    wb = -(1 - 2*b3)*t9*( /* gradU + */ UgradphiT);

    wc = -t9*phiT[k]
       + 2*c3*t9*( /* gradU + */  UgradphiT);

    integrandT[k] = wa*sumgradqn
                  + wb*(delq + gam)
                  + wc*Ugradgam;
  }
}

}

#endif  // INTEGRANDBOUNDARYTRACE_GALERKIN_WAKECUT_CG_LIP_ADJOINT_H
