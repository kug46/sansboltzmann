// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATECELLTRACEGROUPS_FIELDTRACE_H
#define INTEGRATECELLTRACEGROUPS_FIELDTRACE_H

// Cell integral + trace-of-cell integral residual functions

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/XField_CellToTrace.h"

#include "GroupIntegral_Type.h"

namespace SANS
{

template <class TopologyTrace, class TopologyCell, class TopoDim,
          class IntegralCellTrace,
          class XFieldType, class FieldCellGroupType, class FieldTraceType>
void
IntegrateCellTraceGroups_FieldTrace_Integral(
    IntegralCellTrace& integral,
    const int cellGroupGlobal,
    const typename XFieldType::XFieldType::template FieldCellGroupType<TopologyCell>& xfldCellGroup,
    const XField_CellToTrace<typename IntegralCellTrace::PhysDim, TopoDim>& xfldCellToTrace,
    const FieldCellGroupType& fldCellGroup,
    const XFieldType& xfld,
    const FieldTraceType& fldTrace,
    const int quadratureOrderCell,
    const int quadratureOrderTrace[] )
{
  // Integrate over the cell and its traces
  integral.template integrate<TopologyTrace, TopologyCell, TopoDim, XFieldType>(
      cellGroupGlobal,
      xfldCellGroup,
      xfldCellToTrace,
      fldCellGroup,
      xfld, fldTrace,
      quadratureOrderCell, quadratureOrderTrace );
}

//----------------------------------------------------------------------------//
template<class TopDim>
class IntegrateCellTraceGroups_FieldTrace;

// base class interface
template<>
class IntegrateCellTraceGroups_FieldTrace<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

private:
  template <class TopologyCell, class FieldCellType, class IntegralCellTrace, class XFieldType, class FieldTraceType >
  static void
  TraceTopology(
      IntegralCellTrace& integral,
      const int cellGroupGlobal,
      const typename XFieldType::XFieldType::template FieldCellGroupType<TopologyCell>& xfldCellGroup,
      const XField_CellToTrace<typename IntegralCellTrace::PhysDim, TopoDim>& xfldCellToTrace,
      const typename FieldCellType::template FieldCellGroupType<TopologyCell>& fldCellGroup,
      const XFieldType& xfld,
      const FieldTraceType& fldTrace,
      const int quadratureOrderCell,
      const int quadratureOrderTrace[])
  {

    for (int group = 0; group < xfld.getXField().nInteriorTraceGroups(); group++)
    {
      if (xfld.getXField().getInteriorTraceGroupBase(group).topoTypeID() != typeid(Node))
        SANS_DEVELOPER_EXCEPTION( "IntegrateCellTraceGroups_FieldTrace<TopoD1>::TraceTopology - Unknown interior trace topology." );
    }

    for (int group = 0; group < xfld.getXField().nBoundaryTraceGroups(); group++)
    {
      if (xfld.getXField().getBoundaryTraceGroupBase(group).topoTypeID() != typeid(Node))
        SANS_DEVELOPER_EXCEPTION( "IntegrateCellTraceGroups_FieldTrace<TopoD1>::TraceTopology - Unknown boundary trace topology." );
    }

    //If it reaches here, it means that all tracegroups are of the topology "Node"

    IntegrateCellTraceGroups_FieldTrace_Integral<Node, Line, TopoDim>(
        integral, cellGroupGlobal, xfldCellGroup, xfldCellToTrace, fldCellGroup, xfld, fldTrace, quadratureOrderCell, quadratureOrderTrace  );
  }

public:
  template <class IntegralCellTrace, class XFieldType, class FieldCellType, class FieldTraceType>
  static void
  integrate( GroupIntegralCellTraceType<IntegralCellTrace>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const XField_CellToTrace<typename IntegralCellTrace::PhysDim, TopoDim>& xfldCellToTrace,
             const FieldType<FieldCellType>& fldCellType,
             const FieldType<FieldTraceType>& fldTraceType,
             const int quadratureOrderCell[], const int nCellGroup,
             const int quadratureOrderTrace[], const int nTraceGroup)
  {
    IntegralCellTrace& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const FieldCellType& fldCell = fldCellType.cast();
    const FieldTraceType& fldTrace = fldTraceType.cast();

    integral.template check<TopoDim>( fldCell, fldTrace );

    SANS_ASSERT( &xfld.getXField() == &fldCell.getXField() );
    SANS_ASSERT( &fldCell.getXField() == &fldTrace.getXField() );

    SANS_ASSERT_MSG( nCellGroup == xfld.nCellGroups(),
                     "nCellGroup = %d, xfld.nCellGroups() = %d", nCellGroup, xfld.nCellGroups() );
    SANS_ASSERT_MSG( nTraceGroup >= xfld.nInteriorTraceGroups(),
                     "nTraceGroup = %d, xfld.nInteriorTraceGroups() = %d", nTraceGroup, xfld.nInteriorTraceGroups() );
//    SANS_ASSERT_MSG( nTraceGroup >= xfld.nBoundaryTraceGroups(),
//                     "nTraceGroup = %d, xfld.nBoundaryTraceGroups() = %d", nTraceGroup, xfld.nBoundaryTraceGroups() );

    // loop over element groups
    for (std::size_t group = 0; group < integral.nCellGroups(); group++)
    {
      const int cellGroupGlobal = integral.cellGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Line) )
      {
        TraceTopology<Line, FieldCellType>( integral,
                                            cellGroupGlobal,
                                            xfld.template getCellGroup<Line>(cellGroupGlobal),
                                            xfldCellToTrace,
                                            fldCell.template getCellGroup<Line>(cellGroupGlobal),
                                            xfld,
                                            fldTrace,
                                            quadratureOrderCell[cellGroupGlobal], quadratureOrderTrace );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "IntegrateCellTraceGroups_FieldTrace<TopoD1>::integrate - Unknown cell topology." );

    }
  }
};

#if 0

template<>
class IntegrateCellGroups<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  template <class IntegralCell, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralCellType<IntegralCell>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], const int ngroup )
  {
    IntegralCell& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT_MSG( ngroup == xfld.nCellGroups(), "ngroup = %d, xfld.nCellGroups() = %d", ngroup, xfld.nCellGroups() );

    // loop over element groups
    for (std::size_t group = 0; group < integral.nCellGroups(); group++)
    {
      const int cellGroupGlobal = integral.cellGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        integral.template integrate<Triangle, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Triangle>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Triangle>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] );
      }
      else if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        integral.template integrate<Quad, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Quad>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Quad>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};



template<>
class IntegrateCellGroups<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

  template <class IntegralCell, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralCellType<IntegralCell>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], const int ngroup )
  {
    IntegralCell& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT_MSG( ngroup == xfld.nCellGroups(), "ngroup = %d, xfld.nCellGroups() = %d", ngroup, xfld.nCellGroups() );

    // loop over element groups
    for (std::size_t group = 0; group < integral.nCellGroups(); group++)
    {
      const int cellGroupGlobal = integral.cellGroup(group);

      // dispatch integration over elements of group
      if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        integral.template integrate<Tet, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Tet>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Tet>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] );
      }
      else if ( xfld.getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Hex) )
      {
        integral.template integrate<Hex, TopoDim, XFieldType>( cellGroupGlobal,
                                      xfld.template getCellGroup<Hex>(cellGroupGlobal),
                                      qfld.template getCellGroupGlobal<Hex>(cellGroupGlobal),
                                      quadratureorder[cellGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown cell topology\n" );

    }
  }
};

#endif

}

#endif  // INTEGRATECELLTRACEGROUPS_FIELDTRACE_H
