// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANPARAM_H_
#define JACOBIANPARAM_H_

#include <map>

#include "Discretization/JacobianParamBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "Discretization/QuadratureOrder.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for constructing jacobian of the algebraic equation set w.r.t selected paramters
//
template<class iParams, class AESClass>
class JacobianParam
  : public JacobianParamBase<
      typename AlgebraicEquationSetTraits<typename AESClass::NDPDEClass::template MatrixParam<Real>,
                                          typename AESClass::NDPDEClass::template ArrayQ<Real>,
                                          typename AESClass::TraitsTag
                                         >::SystemMatrix
                            >
{
public:
  typedef typename AESClass::NDPDEClass NDPDEClass;
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixParam<Real> MatrixParam;

  typedef typename AESClass::TraitsTag TraitsTag;
  typedef AlgebraicEquationSetTraits<MatrixParam, ArrayQ, TraitsTag> TraitsType;

  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef JacobianParamBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemMatrixTranspose SystemMatrixTranspose;
  typedef typename BaseType::SystemNonZeroPatternTranspose SystemNonZeroPatternTranspose;

  // Constructor
  JacobianParam( const AESClass& AES, const std::map<int,int>& columnMap ) :
    AES_( AES ),
    columnMap_(columnMap),
    quadratureOrder_( AES_.quadratureOrder() ),
    quadratureOrderMin_( AES_.quadratureOrderMin() ),
    JacParamChained_(nullptr){}

  virtual ~JacobianParam() {}

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override;
  virtual void jacobian(SystemNonZeroPatternView& nz) override;

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobian(SystemMatrixTranspose mtxT       ) override;
  virtual void jacobian(SystemNonZeroPatternTranspose nzT) override;

  // Append an JacobianParam set to "this" one
  virtual void addJacobianParamSet(std::shared_ptr<BaseType> JacParam) { JacParamChained_ = JacParam; }

protected:
  template<class SparseMatrixType>
  void jacobian(SparseMatrixType& mtx, const QuadratureOrder& quadratureOrder);

  const AESClass& AES_;
  const std::map<int,int>& columnMap_;  // columnMap[iParam] gives the column index for the parameter

  const QuadratureOrder& quadratureOrder_;
  const QuadratureOrder& quadratureOrderMin_;

  std::shared_ptr<BaseType> JacParamChained_;
};

} //namespace SANS

#endif // JACOBIANPARAM_H_
