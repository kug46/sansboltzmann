// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATEPERIODICTRACEGRUOPS_H
#define INTEGRATEPERIODICTRACEGRUOPS_H

// interior-trace group integral functions

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "GroupIntegral_Type.h"

namespace SANS
{

template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim,
          class IntegralInteriorTrace,
          class XFieldType, class QFieldType>
void
IntegratePeriodicTraceGroups_Integral(      // changed from boundary
    IntegralInteriorTrace& integral,
    const XFieldType& xfld,
    const QFieldType& qfld,
    const int traceGroupGlobal,
    const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const int quadratureorder )
{
  int cellGroupGlobalL = xfldTrace.getGroupLeft();
  int cellGroupGlobalR = xfldTrace.getGroupRight();

  // Integrate over the trace group
  integral.template integrate<TopologyTrace, TopologyL, TopologyR, TopoDim, XFieldType>(
      cellGroupGlobalL,
      xfld.template getCellGroup<TopologyL>(cellGroupGlobalL),
      qfld.template getCellGroupGlobal<TopologyL>(cellGroupGlobalL),
      cellGroupGlobalR,
      xfld.template getCellGroup<TopologyR>(cellGroupGlobalR),
      qfld.template getCellGroupGlobal<TopologyR>(cellGroupGlobalR),
      traceGroupGlobal,
      xfldTrace,
      quadratureorder );
}


template<class TopDim>
class IntegratePeriodicTraceGroups;


template<>
class IntegratePeriodicTraceGroups<TopoD1>
{
public:
  typedef TopoD1 TopoDim;

protected:
  template <class TopologyTrace, class TopologyL,
            class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  RightTopology(IntegralInteriorTrace& integral,
                const XFieldType& xfld,
                const QFieldType& qfld,
                const int traceGroupGlobal,
                const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const int quadratureorder )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Line) )
    {
      IntegratePeriodicTraceGroups_Integral<TopologyTrace, TopologyL, Line, TopoDim>(   // KGchanged
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );

  }


  template <class TopologyTrace, class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  LeftTopology( IntegralInteriorTrace& integral,
                const XFieldType& xfld,
                const QFieldType& qfld,
                const int traceGroupGlobal,
                const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
                const int quadratureorder )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Line) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Line>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );

  }

public:
  template <class IntegralInteriorTrace, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralInteriorTraceType<IntegralInteriorTrace>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], int ngroup )
  {
    IntegralInteriorTrace& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT_MSG( ngroup == xfld.getXField().nBoundaryTraceGroups(),
                     "ngroup = %d, xfld.getXField().nBoundaryTraceGroups() = %d", ngroup, xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nPeriodicTraceGroups(); group++)
    {
      const int traceGroupGlobal = integral.periodicTraceGroup(group);

      if ( xfld.getXField().getBoundaryTraceGroupBase(traceGroupGlobal).topoTypeID() == typeid(Node) )
      {
        LeftTopology<Node>( integral,
                            xfld, qfld,
                            traceGroupGlobal,
                            xfld.getXField().template getBoundaryTraceGroup<Node>(traceGroupGlobal),
                            quadratureorder[traceGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION("Unknown trace topology." );

    }
  }
};


template<>
class IntegratePeriodicTraceGroups<TopoD2>
{
public:
  typedef TopoD2 TopoDim;

protected:
  template <class TopologyTrace, class TopologyL,
            class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  RightTopology(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Triangle) )
    {
      IntegratePeriodicTraceGroups_Integral<TopologyTrace, TopologyL, Triangle, TopoDim>(   // KG Changed boundary
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Quad) )
    {
      IntegratePeriodicTraceGroups_Integral<TopologyTrace, TopologyL, Quad, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }


  template <class TopologyTrace, class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  LeftTopology(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const int quadratureorder )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Triangle>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Quad) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Quad>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }

public:
  template <class IntegralInteriorTrace, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralInteriorTraceType<IntegralInteriorTrace>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], int ngroup )
  {
    IntegralInteriorTrace& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT_MSG( ngroup == xfld.getXField().nBoundaryTraceGroups(),
                     "ngroup = %d, xfld.getXField().nBoundaryTraceGroups() = %d", ngroup, xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nPeriodicTraceGroups(); group++)
    {
      const int traceGroupGlobal = integral.periodicTraceGroup(group);

      if ( xfld.getXField().getBoundaryTraceGroupBase(traceGroupGlobal).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line>( integral,
                            xfld, qfld,
                            traceGroupGlobal,
                            xfld.getXField().template getBoundaryTraceGroup<Line>(traceGroupGlobal),
                            quadratureorder[traceGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown trace topology." );
    }
  }
};


template<>
class IntegratePeriodicTraceGroups<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

protected:
  template <class TopologyL,
            class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  RightTopology_Triangle(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      IntegratePeriodicTraceGroups_Integral<Triangle, TopologyL, Tet, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }

  template <class TopologyL,
            class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  RightTopology_Quad(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();
    //std::cout << "Groups " << groupR << std::endl;
    // dispatch integration over elements of group
    if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      IntegratePeriodicTraceGroups_Integral<Quad, TopologyL, Hex, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }


  template <class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  LeftTopology_Triangle(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Triangle>& xfldTrace,
      const int quadratureorder )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      RightTopology_Triangle<Tet>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }

  template <class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  LeftTopology_Quad(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Quad>& xfldTrace,
      const int quadratureorder )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      RightTopology_Quad<Hex>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }

public:
  template <class IntegralInteriorTrace, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralInteriorTraceType<IntegralInteriorTrace>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], int ngroup )
  {
    IntegralInteriorTrace& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT( ngroup == xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nPeriodicTraceGroups(); group++)
    {
      const int traceGroupGlobal = integral.periodicTraceGroup(group);

      if ( xfld.getXField().getBoundaryTraceGroupBase(traceGroupGlobal).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle( integral,
                              xfld, qfld,
                              traceGroupGlobal,
                              xfld.getXField().template getBoundaryTraceGroup<Triangle>(traceGroupGlobal),
                              quadratureorder[traceGroupGlobal] );
      }
      else if ( xfld.getXField().getBoundaryTraceGroupBase(traceGroupGlobal).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad( integral,
                           xfld, qfld,
                           traceGroupGlobal,
                           xfld.getXField().template getBoundaryTraceGroup<Quad>(traceGroupGlobal),
                           quadratureorder[traceGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown trace topology." );
    }
  }
};

template<>
class IntegratePeriodicTraceGroups<TopoD4>
{
public:
  typedef TopoD4 TopoDim;

protected:
  template <class TopologyL,
            class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  RightTopology_Tet(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
      const int quadratureorder )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( xfld.getXField().getCellGroupBase(groupR).topoTypeID() == typeid(Pentatope) )
    {
      IntegratePeriodicTraceGroups_Integral<Tet, TopologyL, Pentatope, TopoDim>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }


  template <class IntegralInteriorTrace,
            class XFieldType, class QFieldType>
  static void
  LeftTopology_Tet(
      IntegralInteriorTrace& integral,
      const XFieldType& xfld,
      const QFieldType& qfld,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<Tet>& xfldTrace,
      const int quadratureorder )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( xfld.getXField().getCellGroupBase(groupL).topoTypeID() == typeid(Pentatope) )
    {
      // determine topology for R
      RightTopology_Tet<Pentatope>(
          integral, xfld, qfld, traceGroupGlobal, xfldTrace, quadratureorder );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "Unknown cell topology." );
  }

public:
  template <class IntegralInteriorTrace, class XFieldType, class QFieldType>
  static void
  integrate( GroupIntegralInteriorTraceType<IntegralInteriorTrace>&& integralType,
             const FieldType<XFieldType>& xfldType,
             const FieldType<QFieldType>& qfldType,
             const int quadratureorder[], int ngroup )
  {
    IntegralInteriorTrace& integral = integralType.cast();
    const XFieldType& xfld = xfldType.cast();
    const QFieldType& qfld = qfldType.cast();

    integral.template check<TopoDim>( qfld );

    SANS_ASSERT( &xfld.getXField() == &qfld.getXField() );
    SANS_ASSERT( ngroup == xfld.getXField().nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < integral.nPeriodicTraceGroups(); group++)
    {
      const int traceGroupGlobal = integral.periodicTraceGroup(group);

      if ( xfld.getXField().getBoundaryTraceGroupBase(traceGroupGlobal).topoTypeID() == typeid(Tet) )
      {
        LeftTopology_Tet( integral,
                          xfld, qfld,
                          traceGroupGlobal,
                          xfld.getXField().template getBoundaryTraceGroup<Tet>(traceGroupGlobal),
                          quadratureorder[traceGroupGlobal] );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "Unknown trace topology." );
    }
  }
};


}

#endif  // INTEGRATEPERIODICTRACEGRUOPS_H
