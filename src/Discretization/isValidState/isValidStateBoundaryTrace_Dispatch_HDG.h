// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ISVALIDSTATEBOUNDARYTRACE_DISPATCH_HDG_H
#define ISVALIDSTATEBOUNDARYTRACE_DISPATCH_HDG_H

#include "isValidStateBoundaryTrace_HDG.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with qI Trace
//
//---------------------------------------------------------------------------//
template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class isValidStateBoundaryTrace_HDG_Dispatch_impl
{
public:
  isValidStateBoundaryTrace_HDG_Dispatch_impl(
      PDE& pde,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
      const int* quadratureorder, int ngroup,
      bool& isValidState ) :
    xfld_(xfld),
    qfld_(qfld),
    qIfld_(qIfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    pde_(pde),
    isValidState_(isValidState)
  {
    // Nothing
  }

  template<class NDPDE, class NDBCVector, class Category, class DiscTag>
  void operator()(const IntegrandBoundaryTrace<NDPDE, NDVectorCategory<NDBCVector, Category>, DiscTag>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        isValidStateBoundaryTrace_HDG<NDBCVector>(pde_, fcn.getBC(), fcn.getBoundaryGroups(), isValidState_),
        xfld_, qfld_, qIfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qIfld_;
  const int* quadratureorder_;
  const int ngroup_;
  PDE& pde_;
  bool& isValidState_;
};

// Factory function

template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
isValidStateBoundaryTrace_HDG_Dispatch_impl<PDE, XFieldType, PhysDim, TopoDim, ArrayQ>
isValidStateBoundaryTrace_HDG_Dispatch( PDE& pde,
                                        const XFieldType& xfld,
                                        const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                        const Field<PhysDim, TopoDim, ArrayQ>& qIfld,
                                        const int* quadratureorder, int ngroup,
                                        bool& isValidState )
{
  return { pde, xfld, qfld, qIfld, quadratureorder, ngroup, isValidState };
}

}

#endif //ISVALIDSTATEBOUNDARYTRACE_DISPATCH_HDG_H
