// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ISVALIDSTATEINTERIORTRACE_GALERKIN_H
#define ISVALIDSTATEINTERIORTRACE_GALERKIN_H

// interior-trace integral isValidState functions

#include "Field/Field.h"

#include "Quadrature/Quadrature.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin interior-trace integral
//

template<class PDE>
class isValidStateInteriorTrace_impl :
    public GroupIntegralInteriorTraceType< isValidStateInteriorTrace_impl<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the isValidState vectors
  isValidStateInteriorTrace_impl( const PDE& pde,
                                  const std::vector<int>& interiorTraceGroups,
                                  bool& isValidState) :
    pde_(pde),
    interiorTraceGroups_(interiorTraceGroups),
    isValidState_(isValidState)
  {
  }

  std::size_t nInteriorTraceGroups() const            { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const   { return interiorTraceGroups_[n]; }
  const std::vector<int>& interiorTraceGroups() const { return interiorTraceGroups_; }

  std::size_t nPeriodicTraceGroups() const            { return interiorTraceGroups_.size(); }
  std::size_t periodicTraceGroup(const int n) const   { return interiorTraceGroups_[n]; }
  const std::vector<int>& periodicTraceGroups() const { return interiorTraceGroups_; }

//----------------------------------------------------------------------------//
  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    // Nothing
  }


//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType                     ::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType                     ::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType         ::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      int quadratureorder )
  {
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDim> QuadPointCellType;

    ArrayQ q;
    QuadPointCellType sRefL;      // reference-element coordinates left
    QuadPointCellType sRefR;      // reference-element coordinates right

    // use quadrature rule that matches the polynomial order
    Quadrature<TopoDimTrace, TopologyTrace> quadrature( quadratureorder );

    // element field variables
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    const int nquad = quadrature.nQuadrature();

    // Left
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem && isValidState_; elem++)
    {
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // copy global solution DOFs to element
      qfldCellL.getElement( qfldElemL, elemL );
      qfldCellR.getElement( qfldElemR, elemR );

      // loop over quadrature points
      for (int iquad = 0; iquad < nquad && isValidState_; iquad++)
      {
        // reference-trace coordinates
        QuadPointTraceType sRefTrace = quadrature.coordinates_cache( iquad );

        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalTraceL, sRefTrace, sRefL );
        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::eval( canonicalTraceR, sRefTrace, sRefR );

        qfldElemL.eval( sRefL, q );
        isValidState_ &= pde_.isValidState(q);

        qfldElemR.eval( sRefR, q );
        isValidState_ &= pde_.isValidState(q);
      }
    }
  }

protected:
  const PDE& pde_;
  const std::vector<int> interiorTraceGroups_;
  bool& isValidState_;
};

// Factory function

template<class PDE>
isValidStateInteriorTrace_impl<PDE>
isValidStateInteriorTrace( const PDE& pde,
                           const std::vector<int>& interiorTraceGroups,
                           bool& isValidState )
{
  return isValidStateInteriorTrace_impl<PDE>(pde, interiorTraceGroups, isValidState);
}

} //namespace SANS

#endif  // ISVALIDSTATEINTERIORTRACE_GALERKIN_H
