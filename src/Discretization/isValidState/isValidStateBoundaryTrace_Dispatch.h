// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ISVALIDSTATEBOUNDARYTRACE_DISPATCH_H
#define ISVALIDSTATEBOUNDARYTRACE_DISPATCH_H

// boundary-trace integral residual functions

#include "isValidStateBoundaryTrace_mitLG.h"
#include "isValidStateBoundaryTrace_sansLG.h"

#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups_HubTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class isValidStateBoundaryTrace_mitLG_Dispatch_impl
{
public:
  isValidStateBoundaryTrace_mitLG_Dispatch_impl(
      PDE& pde,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      bool& isValidState ) :
    xfld_(xfld),
    qfld_(qfld),
    lgfld_(lgfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    pde_(pde),
    isValidState_(isValidState)
  {
    // Nothing
  }

  template<class NDPDE, class NDBCVector, class Category, class DiscTag>
  void operator()(const IntegrandBoundaryTrace<NDPDE, NDVectorCategory<NDBCVector, Category>, DiscTag>& fcn)
  {
    IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
        isValidStateBoundaryTrace_mitLG<NDBCVector>(pde_, fcn.getBC(), fcn.getBoundaryGroups(), isValidState_),
        xfld_, qfld_, lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  PDE& pde_;
  bool& isValidState_;
};

// Factory function

template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
isValidStateBoundaryTrace_mitLG_Dispatch_impl<PDE, XFieldType, PhysDim, TopoDim, ArrayQ>
isValidStateBoundaryTrace_mitLG_Dispatch( PDE& pde,
                                          const XFieldType& xfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                          const int* quadratureorder, int ngroup,
                                          bool& isValidState )
{
  return { pde, xfld, qfld, lgfld, quadratureorder, ngroup, isValidState };
}


//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Hub Trace
//
//---------------------------------------------------------------------------//
template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class isValidStateBoundaryTrace_mitHT_Dispatch_impl
{
public:
  isValidStateBoundaryTrace_mitHT_Dispatch_impl(
      PDE& pde,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& hbfld,
      const int* quadratureorder, int ngroup,
      bool& isValidState ) :
    xfld_(xfld),
    qfld_(qfld),
    hbfld_(hbfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    pde_(pde),
    isValidState_(isValidState)
  {
    // Nothing
  }

  template<class NDPDE, class NDBCVector, class Category, class DiscTag>
  void operator()(const IntegrandBoundaryTrace<NDPDE, NDVectorCategory<NDBCVector, Category>, DiscTag>& fcn)
  {
    IntegrateBoundaryTraceGroups_HubTrace<TopoDim>::integrate(
        isValidStateBoundaryTrace_mitLG<NDBCVector>(pde_, fcn.getBC(), fcn.getBoundaryGroups(), isValidState_),
        xfld_, qfld_, hbfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& hbfld_;
  const int* quadratureorder_;
  const int ngroup_;
  PDE& pde_;
  bool& isValidState_;
};

// Factory function

template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
isValidStateBoundaryTrace_mitHT_Dispatch_impl<PDE, XFieldType, PhysDim, TopoDim, ArrayQ>
isValidStateBoundaryTrace_mitHT_Dispatch( PDE& pde,
                                          const XFieldType& xfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                          const Field<PhysDim, TopoDim, ArrayQ>& hbfld,
                                          const int* quadratureorder, int ngroup,
                                          bool& isValidState )
{
  return { pde, xfld, qfld, hbfld, quadratureorder, ngroup, isValidState };
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
class isValidStateBoundaryTrace_sansLG_Dispatch_impl
{
public:
  isValidStateBoundaryTrace_sansLG_Dispatch_impl(
      PDE& pde,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      bool& isValidState ):
    xfld_(xfld),
    qfld_(qfld),
    quadratureorder_(quadratureorder),
    ngroup_(ngroup),
    pde_(pde),
    isValidState_(isValidState)
  {
    // Nothing
  }

  template<class NDPDE, class NDBCVector, class Category, class DiscTag>
  void operator()(const IntegrandBoundaryTrace<NDPDE, NDVectorCategory<NDBCVector, Category>, DiscTag>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        isValidStateBoundaryTrace_sansLG<NDBCVector>(pde_, fcn.getBC(), fcn.getBoundaryGroups(), isValidState_),
        xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  PDE& pde_;
  bool& isValidState_;
};

// Factory function
template<class PDE, class XFieldType, class PhysDim, class TopoDim, class ArrayQ>
isValidStateBoundaryTrace_sansLG_Dispatch_impl<PDE, XFieldType, PhysDim, TopoDim, ArrayQ>
isValidStateBoundaryTrace_sansLG_Dispatch( PDE& pde,
                                           const XFieldType& xfld,
                                           const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                           const int* quadratureorder, int ngroup,
                                           bool& isValidState )
{
  return {pde, xfld, qfld, quadratureorder, ngroup, isValidState};
}


}

#endif //ISVALIDSTATEBOUNDARYTRACE_DISPATCH_H
