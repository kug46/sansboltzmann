// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef CALL_DERIVED_ISVALIDSTATE_H
#define CALL_DERIVED_ISVALIDSTATE_H

#include <type_traits>
#include <typeinfo>

#include <boost/mpl/begin_end.hpp>
#include <boost/mpl/next.hpp>
#include <boost/mpl/deref.hpp>

#include "tools/SANSException.h"
#include "tools/demangle.h"

#include "pde/BCCategory.h"

namespace SANS
{

namespace detail
{

// Looping class. Check the current derived type stored in 'iter' to see if the 'base' is a base class of it.
// Otherwise continue to the next derived class in the sequence.
template < class iter, class end, class VectorX, class ArrayQ>
struct call_derived_isValidState
{
  typedef typename boost::mpl::deref<iter>::type currentType;
  typedef typename boost::mpl::next<iter>::type nextType;

  static bool call( const std::type_info& bcType, const BCBase& bc, const VectorX& nL, const ArrayQ& q, bool& isValidState )
  {
    // If the current type matches the derived type, then call f with the base class casted
    if (typeid(currentType) == bcType)
    {
      isValidState &= bc.template cast<currentType>().isValidState(nL, q);
      return false;
    }

    // Check the next type to see if it's the derived type
    return call_derived_isValidState<nextType, end, VectorX, ArrayQ >::
        call(bcType, bc, nL, q, isValidState);
  }
};

// Recursive loop terminates when 'iter' is the same as 'end'
template < class end, class VectorX, class ArrayQ >
struct call_derived_isValidState< end, end, VectorX, ArrayQ >
{
  static bool call( const std::type_info& bcType, const BCBase& bc, const VectorX& nL, const ArrayQ& q, bool& isValidState )
  {
    // Did not find the derived type of base in the sequence
    return true;
  }
};

} // namespace detail

template < class NDBCVector, class VectorX, class ArrayQ>
void call_derived_isValidState( const BCBase& bc, const VectorX& nL, const ArrayQ& q, bool& isValidState )
{
  // Get the start and end iterators from the sequence
  typedef typename boost::mpl::begin< NDBCVector >::type begin;
  typedef typename boost::mpl::end< NDBCVector >::type   end;

  // Call the implementation and throw an exception if the derived class is not in NDBCVector
  if ( detail::call_derived_isValidState< begin, end, VectorX, ArrayQ >::
               call(bc.derivedTypeID(), bc, nL, q, isValidState) )
    SANS_DEVELOPER_EXCEPTION("\nCould not find derived type:\n\n%s\n\nin the sequence:\n\n%s",
                             demangle(bc.derivedTypeID().name()).c_str(), demangle(typeid(NDBCVector).name()).c_str() );
}

}

#endif //CALL_DERIVED_ISVALIDSTATE_H
