// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SETVALIDSTATECELL_H
#define SETVALIDSTATECELL_H

// Projects down the order of a polynomial expansion until isValidState is satisfied

#include "Field/Field.h"

#include "Quadrature/Quadrature.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "BasisFunction/BasisFunctionLine_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//

template<class PDE>
class SetValidStateCell_impl :
    public GroupIntegralCellType< SetValidStateCell_impl<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;

  // Save off the cell integrand, the pde, and the reference to our final result
  SetValidStateCell_impl( const PDE& pde,
                          const std::vector<int>& cellGroups) :
    pde_(pde),
    cellGroups_(cellGroups)
  {
    // Nothing
  }

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

//----------------------------------------------------------------------------//
  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    // Nothing
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    typedef QuadraturePoint<TopoDim> QuadPointType;

    ArrayQ q;


    BasisFunctionCategory cat = qfldCell.basis()->category();
#if 0
    int orderMin;
    if ( cat == BasisFunctionCategory_Legendre )
      orderMin = 0;
    else if ( (cat == BasisFunctionCategory_Lagrange) ||
              (cat == BasisFunctionCategory_Hierarchical ) )
      orderMin = 1;
    else
      SANS_DEVELOPER_EXCEPTION("Unknown basis category");
#endif

    // uses same quadrature rule as residual evaluations
    Quadrature<TopoDim, Topology> quadrature( quadratureorder );

    // also check the corners nodes of the element
    std::vector<DLA::VectorS<TopoDim::D,Real>> sRefNodes;
    LagrangeNodes<Topology>::get(1, sRefNodes);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      bool isValidState = false;
      int order = qfldElem.order();

      while (!isValidState)
      {
        isValidState = true;

        // loop over quadrature points
        const int nquad = quadrature.nQuadrature();
        for (int iquad = 0; iquad < nquad && isValidState; iquad++)
        {
          QuadPointType Ref = quadrature.coordinates_cache( iquad );

          // state value
          qfldElem.eval( Ref, q );

          isValidState &= pde_.isValidState(q);
        }

        for (std::size_t i = 0; i < sRefNodes.size() && isValidState; i++)
        {
          // state value
          qfldElem.eval( sRefNodes[i], q );

          isValidState &= pde_.isValidState(q);
        }

        if (!isValidState)
        {
          order--;
          ElementQFieldClass qfldElemLow( BasisFunctionAreaBase<Topology>::getBasisFunction(order, cat) );

          qfldElem.projectTo(qfldElemLow);
          qfldElemLow.projectTo(qfldElem);
        }
      }

      // set the possibly modified element
      const_cast<QFieldCellGroupType&>(qfldCell).setElement( qfldElem, elem );
    }
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
};

// Factory function

template<class PDE>
SetValidStateCell_impl<PDE>
SetValidStateCell( const PDE& pde,
                   const std::vector<int>& cellGroups )
{
  return SetValidStateCell_impl<PDE>(pde, cellGroups);
}

} //namespace SANS

#endif  // SETVALIDSTATECELL_H
