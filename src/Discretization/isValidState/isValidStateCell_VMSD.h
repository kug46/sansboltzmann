// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ISVALIDSTATECELL_VMSD_H
#define ISVALIDSTATECELL_VMSD_H

// Cell isValidState functions

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"

#include "Quadrature/Quadrature.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

#include "BasisFunction/BasisFunctionLine_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Tetrahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionVolume_Hexahedron_Lagrange.h"
#include "BasisFunction/BasisFunctionSpacetime_Pentatope_Lagrange.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group physicality check
//

template<class PDE>
class isValidStateCell_VMSD_impl :
    public GroupIntegralCellType< isValidStateCell_VMSD_impl<PDE> >
{
public:
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;

  // Save off the cell integrand, the pde, and the reference to our final result
  isValidStateCell_VMSD_impl( const PDE& pde,
                         const std::vector<int>& cellGroups,
                         bool& isValidState) :
    pde_(pde),
    cellGroups_(cellGroups),
    isValidState_(isValidState)
  {
    // Nothing
  }

  std::size_t nCellGroups() const { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n]; }
  const std::vector<int>& cellGroups() const { return cellGroups_; }

//----------------------------------------------------------------------------//
  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>& flds ) const
  {
    // Nothing
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>::
                            template FieldCellGroupType<Topology>& fldsCell,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const QFieldCellGroupType& qfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qpfldCell = get<1>(fldsCell);

    typedef QuadraturePoint<TopoDim> QuadPointType;
    //typedef DLA::VectorS<PhysDim::D,Real> VectorX;

    ArrayQ q;
    ArrayQ qp;
    //VectorX X;

    // uses same quadrature rule as residual evaluations
    Quadrature<TopoDim, Topology> quadrature( quadratureorder );

    // also check the corners nodes of the element
    std::vector<DLA::VectorS<TopoDim::D,Real>> sRefNodes;
    LagrangeNodes<Topology>::get(1, sRefNodes);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementQFieldClass qpfldElem( qpfldCell.basis() );

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );
    SANS_ASSERT( xfldCell.nElem() == qpfldCell.nElem() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem && isValidState_; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      qpfldCell.getElement( qpfldElem, elem );

      // loop over quadrature points
      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad && isValidState_; iquad++)
      {
        QuadPointType Ref = quadrature.coordinates_cache( iquad );

        // state value
        qfldElem.eval( Ref, q );
        qpfldElem.eval( Ref, qp );

        isValidState_ = (isValidState_ && pde_.isValidState(q));

        q += qp;

        isValidState_ = (isValidState_ && pde_.isValidState(q));

//        std::cout << q << "\n";
        //if (!isValidState_)
        //{
        //  get<-1>(xfldElem).eval( Ref, X );
        //  std::cout << "Invalid state q = " << q << " : X = " << X << std::endl;
        //}
      }

      for (std::size_t i = 0; i < sRefNodes.size() && isValidState_; i++)
      {
        // state value
        qfldElem.eval( sRefNodes[i], q );
        qpfldElem.eval( sRefNodes[i], qp );

        isValidState_ = (isValidState_ && pde_.isValidState(q));
        q += qp;
        isValidState_ = (isValidState_ && pde_.isValidState(q));
        //if (!isValidState_)
        //{
        //  get<-1>(xfldElem).eval( sRefNodes[i], X );
        //  std::cout << "Invalid state q = " << q << " : X = " << X << std::endl;
        //}
      }
    }
  }

protected:
  const PDE& pde_;
  const std::vector<int> cellGroups_;
  bool& isValidState_;
};

// Factory function

template<class PDE>
isValidStateCell_VMSD_impl<PDE>
isValidStateCell_VMSD( const PDE& pde,
                  const std::vector<int>& cellGroups,
                  bool& isValidState )
{
  return isValidStateCell_VMSD_impl<PDE>(pde, cellGroups, isValidState);
}

} //namespace SANS

#endif  // ISVALIDSTATECELL_VMSD_H
