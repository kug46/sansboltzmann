// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ISVALIDSTATEINTERIORTRACE_HDG_H
#define ISVALIDSTATEINTERIORTRACE_HDG_H

// interior-trace integral isValidState functions

#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"

#include "Quadrature/Quadrature.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"


#include "BasisFunction/BasisFunctionNode.h"
#include "BasisFunction/BasisFunctionLine_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Quad_Lagrange.h"
#include "BasisFunction/BasisFunctionArea_Triangle_Lagrange.h"

#include "tools/Tuple.h"
#include "tools/minmax.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  HDG interior-trace integral
//

template<class PDE, class IntegrandInteriorTrace>
class isValidStateInteriorTrace_HDG_impl :
    public GroupIntegralInteriorTraceType< isValidStateInteriorTrace_HDG_impl<PDE, IntegrandInteriorTrace> >
{
public:
  typedef typename IntegrandInteriorTrace::PhysDim PhysDim;
  typedef typename IntegrandInteriorTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandInteriorTrace::template VectorArrayQ<Real> VectorArrayQ;

  // Save off the boundary trace integrand and the isValidState vectors
  isValidStateInteriorTrace_HDG_impl( const IntegrandInteriorTrace& fcn,
                                    const PDE& pde,
                                    bool& isValidState) :
    fcn_(fcn),
    pde_(pde),
    isValidState_(isValidState)
  {
  }

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    // Nothing
  }

  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    // Nothing
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>::
                       template FieldCellGroupType<TopologyL>& fldsCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left cell types
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    // Right cell types
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDim> QuadPointCellType;

    const QFieldCellGroupTypeL& qfldCellL = get<0>(fldsCellL);
    const QFieldCellGroupTypeR& qfldCellR = get<0>(fldsCellR);

    ArrayQ qI, q;
    QuadPointCellType sRefL; // reference-element coordinates left
    QuadPointCellType sRefR; // reference-element coordinates right

    // use quadrature rule that matches the polynomial order
    Quadrature<TopoDimTrace, TopologyTrace> quadrature( quadratureorder );

    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // also check the corners nodes of the trace element
    std::vector<DLA::VectorS<MAX(TopoDimTrace::D,1),Real>> sRefNodes;
    LagrangeNodes<TopologyTrace>::get(1, sRefNodes);

    // element field variables
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    // number of integrals evaluated per element
    const int nElem = xfldTrace.nElem();

    // Trace
    for (int elem = 0; elem < nElem && isValidState_; elem++)
    {
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // copy global solution DOFs to element
      qfldCellL.getElement( qfldElemL, elemL );
      qfldCellR.getElement( qfldElemR, elemR );

      qIfldTrace.getElement( qIfldElemTrace, elem );

      // loop over quadrature points
      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        // get the quadrature reference coordinate
        QuadPointTraceType sRefTrace = quadrature.coordinates_cache( iquad );

        // check the trace state
        qIfldElemTrace.eval( sRefTrace, qI );
        isValidState_ &= pde_.isValidState(qI);

        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalTraceL, sRefTrace, sRefL );
        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::eval( canonicalTraceR, sRefTrace, sRefR );

        // check the L/R cell states
        qfldElemL.eval( sRefL, q );
        isValidState_ &= pde_.isValidState(q);

        qfldElemR.eval( sRefR, q );
        isValidState_ &= pde_.isValidState(q);
      }

      // Only check the corner nodes of the trace element.
      // is valid state cell will check the cell elements
      for (std::size_t i = 0; i < sRefNodes.size() && isValidState_; i++)
      {
        // state value
        qIfldElemTrace.eval( sRefNodes[i], q );

        isValidState_ &= pde_.isValidState(q);
      }
    }
  }

  // Integration function that integrates each element in the interior trace group
  template <class TopologyTrace, class TopologyL, class TopologyR, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int cellGroupGlobalR,
      const typename XFieldType::template FieldCellGroupType<TopologyR>& xfldCellR,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& qIfldTrace,
      int quadratureorder )
  {
    // Left cell types
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    // Right cell types
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    //Trace types
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDim> QuadPointCellType;

    ArrayQ qI, q;
    QuadPointCellType sRefL; // reference-element coordinates left
    QuadPointCellType sRefR; // reference-element coordinates right

    // use quadrature rule that matches the polynomial order
    Quadrature<TopoDimTrace, TopologyTrace> quadrature( quadratureorder );

    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // also check the corners nodes of the trace element
    std::vector<DLA::VectorS<MAX(TopoDimTrace::D,1),Real>> sRefNodes;
    LagrangeNodes<TopologyTrace>::get(1, sRefNodes);

    // element field variables
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

    // number of integrals evaluated per element
    const int nElem = xfldTrace.nElem();

    // Trace
    for (int elem = 0; elem < nElem && isValidState_; elem++)
    {
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // copy global solution DOFs to element
      qfldCellL.getElement( qfldElemL, elemL );
      qfldCellR.getElement( qfldElemR, elemR );

      qIfldTrace.getElement( qIfldElemTrace, elem );

      // loop over quadrature points
      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad; iquad++)
      {
        // get the quadrature reference coordinate
        QuadPointTraceType sRefTrace = quadrature.coordinates_cache( iquad );

        // check the trace state
        qIfldElemTrace.eval( sRefTrace, qI );
        isValidState_ &= pde_.isValidState(qI);

        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalTraceL, sRefTrace, sRefL );
        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::eval( canonicalTraceR, sRefTrace, sRefR );

        // check the L/R cell states
        qfldElemL.eval( sRefL, q );
        isValidState_ &= pde_.isValidState(q);

        qfldElemR.eval( sRefR, q );
        isValidState_ &= pde_.isValidState(q);
      }

      // Only check the corner nodes of the trace element.
      // is valid state cell will check the cell elements
      for (std::size_t i = 0; i < sRefNodes.size() && isValidState_; i++)
      {
        // state value
        qIfldElemTrace.eval( sRefNodes[i], q );

        isValidState_ &= pde_.isValidState(q);
      }
    }
  }

protected:
  const IntegrandInteriorTrace& fcn_;
  const PDE& pde_;
  bool& isValidState_;
};

// Factory function

template<class PDE, class IntegrandInteriorTrace>
isValidStateInteriorTrace_HDG_impl<PDE, IntegrandInteriorTrace>
isValidStateInteriorTrace_HDG( const IntegrandInteriorTraceType<IntegrandInteriorTrace>& fcn,
                               const PDE& pde,
                               bool& isValidState)
{
  return isValidStateInteriorTrace_HDG_impl<PDE, IntegrandInteriorTrace>(fcn.cast(), pde, isValidState);
}

} //namespace SANS

#endif  // ISVALIDSTATEINTERIORTRACE_HDG_H
