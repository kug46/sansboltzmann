// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ISVALIDSTATEBOUNDARYTRACE_MITLG_H
#define ISVALIDSTATEBOUNDARYTRACE_MITLG_H

// boundary-trace integral residual functions

#include "Field/GroupElementType.h"
#include "Field/Field.h"
#include "Field/Element/TraceUnitNormal.h"

#include "Quadrature/Quadrature.h"

#include "call_derived_isValidState.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class PDE, class NDBCVector>
class isValidStateBoundaryTrace_mitLG_impl :
    public GroupIntegralBoundaryTraceType< isValidStateBoundaryTrace_mitLG_impl<PDE, NDBCVector> >
{
public:
  typedef typename PDE::PhysDim PhysDim;
  typedef typename PDE::template ArrayQ<Real> ArrayQ;

  // Save off the boundary trace integrand and the residual vectors
  isValidStateBoundaryTrace_mitLG_impl( const PDE& pde,
                                        const BCBase& bc,
                                        const std::vector<int>& BoundaryGroups,
                                        bool& isValidState) :
   pde_(pde),
   bc_(bc),
   BoundaryGroups_(BoundaryGroups),
   isValidState_(isValidState)
  {
  }

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const BCBase& getBC() const { return bc_; }

//----------------------------------------------------------------------------//
  // We need this to have a consistent interface
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld,
              const Field<PhysDim, TopoDim, ArrayQ>& lgfld ) const
  {
    // Nothing
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate(
      const int cellGroupGlobalL,
      const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int traceGroupGlobal,
      const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace> QFieldTraceGroupType;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename ElementXFieldTraceClass::RefCoordType RefCoordType;

    typedef typename TopologyTrace::TopoDim TopoDimTrace;
    typedef QuadraturePoint<TopoDimTrace> QuadPointTrace;
    typedef QuadratureCellTracePoint<TopoDim> QuadPointCell;

    typedef typename ElementXFieldTraceClass::VectorX VectorX;

    ArrayQ q, qhub;
    VectorX X;                   // Cartesian coordinate
    VectorX nL;                  // unit normal (points out of domain)
    QuadPointCell sRefL;         // reference-element coordinates left

    // use quadrature rule that matches the polynomial order
    Quadrature<TopoDimTrace, TopologyTrace> quadrature( quadratureorder );

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qhubfldElemTrace( lgfldTrace.basis() );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem && isValidState_; ++elem)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );

      // loop over quadrature points
      const int nquad = quadrature.nQuadrature();
      for (int iquad = 0; iquad < nquad && isValidState_; iquad++)
      {
        QuadPointTrace sRefTrace = quadrature.coordinates_cache( iquad );

        TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalTraceL, sRefTrace, sRefL );

        // basis value, gradient
        qfldElemL.eval( sRefL, q );

        // unit normal: points out of domain
        traceUnitNormal( get<-1>(xfldElemL), sRefL, get<-1>(xfldElemTrace), sRefTrace, nL);

        isValidState_ &= pde_.isValidState(q);
        if ( isValidState_ )
        {
          call_derived_isValidState<NDBCVector>(bc_, nL, q, isValidState_);
          if (!isValidState_)
          {
            get<-1>(xfldElemL).eval( sRefL, X );
            std::cout << "Cartesian X = " << X << std::endl;
          }
        }
      }
    }

#if 1 // also check hubtrace DOFs if lgfldtrace is actually a hubtrace

    if (xfldTrace.getGroupRightType() == eHubTraceGroup)
    {
      int nelem_hb = lgfldTrace.nElem();
      for (int elem = 0; elem < nelem_hb && isValidState_; ++elem)
      {
        lgfldTrace.getElement( qhubfldElemTrace, elem );

        RefCoordType sRefTrace = 0;
        qhubfldElemTrace.eval(sRefTrace, qhub);

        isValidState_ &= pde_.isValidState(qhub);
      }
    }
#endif
  }

protected:
  const PDE& pde_;
  const BCBase& bc_;
  const std::vector<int> BoundaryGroups_;
  bool& isValidState_;
};

// Factory function

template<class NDBCVector, class PDE>
isValidStateBoundaryTrace_mitLG_impl<PDE, NDBCVector>
isValidStateBoundaryTrace_mitLG( const PDE& pde,
                                 const BCBase& bc,
                                 const std::vector<int>& BoundaryGroups,
                                 bool& isValidState )
{
  return {pde, bc, BoundaryGroups, isValidState};
}


}

#endif  // ISVALIDSTATEBOUNDARYTRACE_MITLG_GALERKIN_H
