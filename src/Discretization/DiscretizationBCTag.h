// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DISCRETIZATIONBCTAG_H
#define DISCRETIZATIONBCTAG_H

#include "pde/BCCategory.h"

namespace SANS
{

// The generic Galerkin discretization tag
class Galerkin;

//----------------------------------------------------------------------------//
// Used to choose the appropriate discretization tag based on the BC Category and interior discretization
template<class BCCategry, class DiscTag>
struct DiscBCTag;

template<> struct DiscBCTag<BCCategory::Dirichlet_mitLG, Galerkin>             { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::Dirichlet_sansLG, Galerkin>            { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_mitLG, Galerkin>          { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG, Galerkin>         { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::Flux_mitState, Galerkin>               { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::None, Galerkin>                        { typedef Galerkin type; };

template<> struct DiscBCTag<BCCategory::LinearScalar_sansLG_FP_HACK, Galerkin> { typedef Galerkin type; };
template<> struct DiscBCTag<BCCategory::WakeCut_Potential_Drela, Galerkin>         { typedef Galerkin type; };
}

#endif //DISCRETIZATIONBCTAG_H
