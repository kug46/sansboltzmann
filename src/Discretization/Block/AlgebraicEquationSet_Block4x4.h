// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_BLOCK_ALGEBRAICEQUATIONSET_BLOCK4X4_H_
#define SRC_DISCRETIZATION_BLOCK_ALGEBRAICEQUATIONSET_BLOCK4X4_H_

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/output_std_vector.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_4x4.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_4.h"

#include "LinearAlgebra/MatrixSizeType.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "Discretization/JacobianParamBase.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Abbreviations:
// SM = system matrix
//

template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base = AlgebraicEquationSetBase>
class AlgebraicEquationSet_Block4x4
    : public Base< BLA::MatrixBlock_4x4<SM00, SM01, SM02, SM03,
                                        SM10, SM11, SM12, SM13,
                                        SM20, SM21, SM22, SM23,
                                        SM30, SM31, SM32, SM33> >
{
public:
  typedef BLA::MatrixBlock_4x4<SM00, SM01, SM02, SM03,
                               SM10, SM11, SM12, SM13,
                               SM20, SM21, SM22, SM23,
                               SM30, SM31, SM32, SM33> SystemMatrix;

  typedef Base<SystemMatrix> BaseType;

  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;
  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemVectorView SystemVectorView;

  typedef AlgebraicEquationSetBase<SM00> BaseType00;
  typedef JacobianParamBase       <SM01> BaseType01;
  typedef JacobianParamBase       <SM02> BaseType02;
  typedef JacobianParamBase       <SM03> BaseType03;

  typedef JacobianParamBase       <SM10> BaseType10;
  typedef AlgebraicEquationSetBase<SM11> BaseType11;
  typedef JacobianParamBase       <SM12> BaseType12;
  typedef JacobianParamBase       <SM13> BaseType13;

  typedef JacobianParamBase       <SM20> BaseType20;
  typedef JacobianParamBase       <SM21> BaseType21;
  typedef AlgebraicEquationSetBase<SM22> BaseType22;
  typedef JacobianParamBase       <SM23> BaseType23;

  typedef JacobianParamBase       <SM30> BaseType30;
  typedef JacobianParamBase       <SM31> BaseType31;
  typedef JacobianParamBase       <SM32> BaseType32;
  typedef AlgebraicEquationSetBase<SM33> BaseType33;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  typedef typename BaseType::LinesearchData LinesearchData;

  // constructor
  AlgebraicEquationSet_Block4x4(BaseType00& AE00, BaseType01& JP01, BaseType02& JP02, BaseType03& JP03,
                                BaseType10& JP10, BaseType11& AE11, BaseType12& JP12, BaseType13& JP13,
                                BaseType20& JP20, BaseType21& JP21, BaseType22& AE22, BaseType23& JP23,
                                BaseType30& JP30, BaseType31& JP31, BaseType32& JP32, BaseType33& AE33) :
    AE00_(AE00), JP01_(JP01), JP02_(JP02), JP03_(JP03),
    JP10_(JP10), AE11_(AE11), JP12_(JP12), JP13_(JP13),
    JP20_(JP20), JP21_(JP21), AE22_(AE22), JP23_(JP23),
    JP30_(JP30), JP31_(JP31), JP32_(JP32), AE33_(AE33)
  {
    // Nothing
  }

  virtual ~AlgebraicEquationSet_Block4x4() {}

  //Inherit generic routines from the base type
  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrix& mtx) override;
  virtual void jacobian(SystemNonZeroPattern& nz) override;

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrix& mtxT) override;
  virtual void jacobianTranspose(SystemNonZeroPattern& nzT) override;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  //Convergence check of the residual norm
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override;

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override;

  // is the residual norm decreased?
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override;

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override;

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override
  {
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block4x4::indexPDE() not implemented");
    return -1;
  }
  virtual int indexQ() const override
  {
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block4x4::indexQ() not implemented");
    return -1;
  }

  // update fraction needed for physically valid state
  virtual Real updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const override;

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override
  {
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block4x4::nResidNorm() not implemented");
    return -1;
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override
  {
//    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block4x4::comm() not implemented");
    return AE00_.comm();
  }

  virtual void syncDOFs_MPI() override
  {
    AE00_.syncDOFs_MPI();
    AE11_.syncDOFs_MPI();
    AE22_.syncDOFs_MPI();
    AE33_.syncDOFs_MPI();
  }

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override;

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;


  // Are we having machine precision issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override;

protected:
  // dispatch the jacobian routine based on the template
  template<class SystemMatrixType>
  void jacobian(SystemMatrixType mtx);

  template<class SystemMatrixType>
  void jacobianTranspose(SystemMatrixType mtx);

  // matrix block component data
  BaseType00& AE00_; BaseType01& JP01_; BaseType02& JP02_; BaseType03& JP03_;
  BaseType10& JP10_; BaseType11& AE11_; BaseType12& JP12_; BaseType13& JP13_;
  BaseType20& JP20_; BaseType21& JP21_; BaseType22& AE22_; BaseType23& JP23_;
  BaseType30& JP30_; BaseType31& JP31_; BaseType32& JP32_; BaseType33& AE33_;
};

} // namespace SANS

#endif /* SRC_DISCRETIZATION_BLOCK_ALGEBRAICEQUATIONSET_BLOCK4X4_H_ */
