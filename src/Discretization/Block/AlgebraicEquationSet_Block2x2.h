// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_BLOCK_ALGEBRAICEQUATIONSET_BLOCK2X2_H_
#define SRC_DISCRETIZATION_BLOCK_ALGEBRAICEQUATIONSET_BLOCK2X2_H_

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/output_std_vector.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "Discretization/JacobianParamBase.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Abbreviations:
// SM = system matrix
//
template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base = AlgebraicEquationSetBase>
class AlgebraicEquationSet_Block2x2
    : public Base< BLA::MatrixBlock_2x2< SM00, SM01,
                                         SM10, SM11 > >
{
public:
  typedef BLA::MatrixBlock_2x2< SM00, SM01,
                                SM10, SM11 > SystemMatrix;

  typedef Base<SystemMatrix> BaseType;

  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;
  typedef typename BaseType::SystemVector SystemVector;
  typedef typename BaseType::SystemVectorView SystemVectorView;

  typedef AlgebraicEquationSetBase<SM00> BaseType00;
  typedef JacobianParamBase       <SM01> BaseType01;
  typedef JacobianParamBase       <SM10> BaseType10;
  typedef AlgebraicEquationSetBase<SM11> BaseType11;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  typedef typename BaseType::LinesearchData LinesearchData;

  AlgebraicEquationSet_Block2x2(BaseType00& AES00,
                                BaseType01& JP01,
                                BaseType10& JP10,
                                BaseType11& AES11) :
    AES00_(AES00),
    JP01_(JP01),
    JP10_(JP10),
    AES11_(AES11)
  {
    // Nothing
  }

  virtual ~AlgebraicEquationSet_Block2x2() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrix& mtx) override;
  virtual void jacobian(SystemNonZeroPattern& nz) override;

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrix& mtxT) override;
  virtual void jacobianTranspose(SystemNonZeroPattern& nzT) override;

  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override;

  //Convergence check of the residual norm
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const override;

  //Convergence check of the residual
  virtual bool convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const override;

  // is the residual norm decreased?
  virtual bool decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                                 const std::vector<std::vector<Real>>& rsdNormNew) const override;

  //prints out a residual that could not be decreased and the convergence tolerances
  virtual void printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm, std::ostream& os = std::cout) const override
  {
    std::size_t nRsd = AES00_.nResidNorm();
    std::vector<std::vector<Real>> AES00rsdNormOld(rsdNorm.begin(), rsdNorm.begin() + nRsd);
    std::vector<std::vector<Real>> AES11rsdNormOld(rsdNorm.begin() + nRsd, rsdNorm.end());

    AES00_.printDecreaseResidualFailure(AES00rsdNormOld, os);
    AES11_.printDecreaseResidualFailure(AES11rsdNormOld, os);
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override
  {
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block2x2::indexPDE not implemented");
    return -1;
  }
  virtual int indexQ() const override
  {
    SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block2x2::indexQ not implemented");
    return -1;
  }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return AES00_.comm(); }

  virtual void syncDOFs_MPI() override
  {
    AES00_.syncDOFs_MPI();
    AES11_.syncDOFs_MPI();
  }

  virtual bool updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                                         LinesearchData& pResData,
                                         LinesearchData& pStepData) const override;

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;


  // Are we having machine precision issues
  virtual bool atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm) override;

protected:
  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx );

  template<class SparseMatrixType>
  void jacobianTranspose(SparseMatrixType mtx );

  BaseType00& AES00_;
  BaseType01& JP01_;
  BaseType10& JP10_;
  BaseType11& AES11_;
};

} //namespace SANS

#endif /* SRC_DISCRETIZATION_BLOCK_ALGEBRAICEQUATIONSET_BLOCK2X2_H_ */
