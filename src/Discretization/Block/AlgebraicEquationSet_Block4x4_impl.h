// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ALGEBRAICEQUATIONSET_BLOCK4X4_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "AlgebraicEquationSet_Block4x4.h"

namespace SANS
{
// residual
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
residual(SystemVectorView& rsd)
{
  AE00_.residual(rsd.v0);
  AE11_.residual(rsd.v1);
  AE22_.residual(rsd.v2);
  AE33_.residual(rsd.v3);
}

// jacobian routines
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
jacobian(SystemMatrix& mtx)
{
  this->template jacobian<SystemMatrix&>(mtx);
}


template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
jacobian(SystemNonZeroPattern& mtx)
{
  this->template jacobian<SystemNonZeroPattern&>(mtx);
}


template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
template<class SystemMatrixType>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
jacobian(SystemMatrixType jac)
{
  AE00_.jacobian(jac.m00); JP01_.jacobian(jac.m01); JP02_.jacobian(jac.m02); JP03_.jacobian(jac.m03);
  JP10_.jacobian(jac.m10); AE11_.jacobian(jac.m11); JP12_.jacobian(jac.m12); JP13_.jacobian(jac.m13);
  JP20_.jacobian(jac.m20); JP21_.jacobian(jac.m21); AE22_.jacobian(jac.m22); JP23_.jacobian(jac.m23);
  JP30_.jacobian(jac.m30); JP31_.jacobian(jac.m31); JP32_.jacobian(jac.m32); AE33_.jacobian(jac.m33);
}


// jacobian tranpose routines
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
jacobianTranspose(SystemMatrix& jac)
{
  this->template jacobianTranspose<SystemMatrix&>(jac);
}


template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
jacobianTranspose(SystemNonZeroPattern& jac)
{
  this->template jacobianTranspose<SystemNonZeroPattern&>(jac);
}


template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
template<class SystemMatrixType>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
jacobianTranspose(SystemMatrixType jac)
{
  AE00_.jacobianTranspose(jac.m00);   JP01_.jacobian(Transpose(jac.m10)); JP02_.jacobian(Transpose(jac.m20)); JP03_.jacobian(Transpose(jac.m30));
  JP10_.jacobian(Transpose(jac.m01)); AE11_.jacobianTranspose(jac.m11);   JP12_.jacobian(Transpose(jac.m21)); JP13_.jacobian(Transpose(jac.m31));
  JP20_.jacobian(Transpose(jac.m02)); JP21_.jacobian(Transpose(jac.m12)); AE22_.jacobianTranspose(jac.m22);   JP23_.jacobian(Transpose(jac.m32));
  JP30_.jacobian(Transpose(jac.m03)); JP31_.jacobian(Transpose(jac.m13)); JP32_.jacobian(Transpose(jac.m23)); AE33_.jacobianTranspose(jac.m33);
}


// evaluate residual norm
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
std::vector<std::vector<Real>>
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
residualNorm(const SystemVectorView& rsd) const
{
  std::vector<std::vector<Real>> merged;

  std::vector<std::vector<Real>> normAE00 = AE00_.residualNorm(rsd.v0);
  std::vector<std::vector<Real>> normAE11 = AE11_.residualNorm(rsd.v1);
  std::vector<std::vector<Real>> normAE22 = AE22_.residualNorm(rsd.v2);
  std::vector<std::vector<Real>> normAE33 = AE33_.residualNorm(rsd.v3);

  merged.reserve( normAE00.size() + normAE11.size() + normAE22.size() + normAE33.size() ); // preallocate memory
  merged.insert( merged.end(), normAE00.begin(), normAE00.end() );
  merged.insert( merged.end(), normAE11.begin(), normAE11.end() );
  merged.insert( merged.end(), normAE22.begin(), normAE22.end() );
  merged.insert( merged.end(), normAE33.begin(), normAE33.end() );

  return merged;
}

// check residual convergence
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
bool
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const
{
  std::vector<std::size_t> blockpartition(5,0);
  blockpartition.at(1) = AE00_.nResidNorm();
  blockpartition.at(2) = blockpartition.at(1) + AE11_.nResidNorm();
  blockpartition.at(3) = blockpartition.at(2) + AE22_.nResidNorm();
  blockpartition.at(4) = blockpartition.at(3) + AE33_.nResidNorm();

  std::vector<std::vector<Real>> AE00rsdNorm(rsdNorm.begin(),
                                                rsdNorm.begin() + blockpartition.at(1) );
  std::vector<std::vector<Real>> AE11rsdNorm(rsdNorm.begin() + blockpartition.at(1),
                                                rsdNorm.begin() + blockpartition.at(2) ) ;
  std::vector<std::vector<Real>> AE22rsdNorm(rsdNorm.begin() + blockpartition.at(2),
                                                rsdNorm.begin() + blockpartition.at(3) );
  std::vector<std::vector<Real>> AE33rsdNorm(rsdNorm.begin() + blockpartition.at(3),
                                                rsdNorm.end() );

  bool convergedAE00 = AE00_.convergedResidual(AE00rsdNorm);
  bool convergedAE11 = AE11_.convergedResidual(AE11rsdNorm);
  bool convergedAE22 = AE22_.convergedResidual(AE22rsdNorm);
  bool convergedAE33 = AE33_.convergedResidual(AE33rsdNorm);

  return (convergedAE00 && convergedAE11 && convergedAE22 && convergedAE33);
}

// check residual convergence
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
bool
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const
{
  SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block4x4<>::convergedResidual not implemented");
  return false;
}


// check residual decrease
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
bool
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                  const std::vector<std::vector<Real>>& rsdNormNew) const
{
  std::vector<std::size_t> blockpartition(5,0);
  blockpartition.at(1) = AE00_.nResidNorm();
  blockpartition.at(2) = blockpartition.at(1) + AE11_.nResidNorm();
  blockpartition.at(3) = blockpartition.at(2) + AE22_.nResidNorm();
  blockpartition.at(4) = blockpartition.at(3) + AE33_.nResidNorm();

  std::vector<std::vector<Real>> AE00rsdNormOld(rsdNormOld.begin(),
                                                rsdNormOld.begin() + blockpartition.at(1) );
  std::vector<std::vector<Real>> AE11rsdNormOld(rsdNormOld.begin() + blockpartition.at(1),
                                                rsdNormOld.begin() + blockpartition.at(2) ) ;
  std::vector<std::vector<Real>> AE22rsdNormOld(rsdNormOld.begin() + blockpartition.at(2),
                                                rsdNormOld.begin() + blockpartition.at(3) );
  std::vector<std::vector<Real>> AE33rsdNormOld(rsdNormOld.begin() + blockpartition.at(3),
                                                rsdNormOld.end() );

  std::vector<std::vector<Real>> AE00rsdNormNew(rsdNormNew.begin(),
                                                rsdNormNew.begin() + blockpartition.at(1) );
  std::vector<std::vector<Real>> AE11rsdNormNew(rsdNormNew.begin() + blockpartition.at(1),
                                                rsdNormNew.begin() + blockpartition.at(2) ) ;
  std::vector<std::vector<Real>> AE22rsdNormNew(rsdNormNew.begin() + blockpartition.at(2),
                                                rsdNormNew.begin() + blockpartition.at(3) );
  std::vector<std::vector<Real>> AE33rsdNormNew(rsdNormNew.begin() + blockpartition.at(3),
                                                rsdNormNew.end() );

  SANS_ASSERT_MSG( (rsdNormOld.begin() + blockpartition.at(4)) == rsdNormOld.end(), "Iterator increment mismatches container size." );

  bool decreasedAE00 = AE00_.decreasedResidual(AE00rsdNormOld, AE00rsdNormNew);
  bool decreasedAE11 = AE11_.decreasedResidual(AE11rsdNormOld, AE11rsdNormNew);
  bool decreasedAE22 = AE22_.decreasedResidual(AE22rsdNormOld, AE22rsdNormNew);
  bool decreasedAE33 = AE33_.decreasedResidual(AE33rsdNormOld, AE33rsdNormNew);

  return (decreasedAE00 && decreasedAE11 && decreasedAE22 && decreasedAE33);
}


// print residual norm upon failure of decreasing residuals
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
printDecreaseResidualFailure(const std::vector<std::vector<Real>>& rsdNorm,
                             std::ostream& os) const
{
  std::vector<std::size_t> blockpartition(5,0);
  blockpartition.at(1) = AE00_.nResidNorm();
  blockpartition.at(2) = blockpartition.at(1) + AE11_.nResidNorm();
  blockpartition.at(3) = blockpartition.at(2) + AE22_.nResidNorm();
  blockpartition.at(4) = blockpartition.at(3) + AE33_.nResidNorm();

  std::vector<std::vector<Real>> AE00rsdNormOld(rsdNorm.begin(),
                                                rsdNorm.begin() + blockpartition.at(1) );
  std::vector<std::vector<Real>> AE11rsdNormOld(rsdNorm.begin() + blockpartition.at(1),
                                                rsdNorm.begin() + blockpartition.at(2) ) ;
  std::vector<std::vector<Real>> AE22rsdNormOld(rsdNorm.begin() + blockpartition.at(2),
                                                rsdNorm.begin() + blockpartition.at(3) );
  std::vector<std::vector<Real>> AE33rsdNormOld(rsdNorm.begin() + blockpartition.at(3),
                                                rsdNorm.end() );

  SANS_ASSERT_MSG( (rsdNorm.begin() + blockpartition.at(4)) == rsdNorm.end(), "Iterator increment mismatches container size." );

  AE00_.printDecreaseResidualFailure(AE00rsdNormOld, os);
  AE11_.printDecreaseResidualFailure(AE11rsdNormOld, os);
  AE22_.printDecreaseResidualFailure(AE22rsdNormOld, os);
  AE33_.printDecreaseResidualFailure(AE33rsdNormOld, os);
}


// set solution field from system vector
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
setSolutionField(const SystemVectorView& q)
{
  AE00_.setSolutionField(q.v0);
  AE11_.setSolutionField(q.v1);
  AE22_.setSolutionField(q.v2);
  AE33_.setSolutionField(q.v3);
}


// fill system vector from solution field
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
fillSystemVector(SystemVectorView& q) const
{
  AE00_.fillSystemVector(q.v0);
  AE11_.fillSystemVector(q.v1);
  AE22_.fillSystemVector(q.v2);
  AE33_.fillSystemVector(q.v3);
}


// get system vector size
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
typename AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                                       SM10, SM11, SM12, SM13,
                                       SM20, SM21, SM22, SM23,
                                       SM30, SM31, SM32, SM33, Base>::VectorSizeClass
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
vectorEqSize() const
{
  return VectorSizeClass(AE00_.vectorEqSize(),
                         AE11_.vectorEqSize(),
                         AE22_.vectorEqSize(),
                         AE33_.vectorEqSize());
}


// get system vector size
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
typename AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                                       SM10, SM11, SM12, SM13,
                                       SM20, SM21, SM22, SM23,
                                       SM30, SM31, SM32, SM33, Base>::VectorSizeClass
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
vectorStateSize() const
{
  return VectorSizeClass(AE00_.vectorStateSize(),
                         AE11_.vectorStateSize(),
                         AE22_.vectorStateSize(),
                         AE33_.vectorStateSize());
}


// get system matrix size
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
typename AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                                       SM10, SM11, SM12, SM13,
                                       SM20, SM21, SM22, SM23,
                                       SM30, SM31, SM32, SM33, Base>::MatrixSizeClass
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
matrixSize() const
{
  // Build up the off diagonal sizes from the diagonals
  typename MatrixSizeType<SM00>::type size00( AE00_.matrixSize() );
  typename MatrixSizeType<SM11>::type size11( AE11_.matrixSize() );
  typename MatrixSizeType<SM22>::type size22( AE22_.matrixSize() );
  typename MatrixSizeType<SM33>::type size33( AE33_.matrixSize() );

  // Assume each coupling jacobian is of type MatrixD<SparseMatrix<MatrixQ> >
  // First set the size of MatrixD
  typename MatrixSizeType<SM01>::type size01(size00.m(), size11.n());
  typename MatrixSizeType<SM02>::type size02(size00.m(), size22.n());
  typename MatrixSizeType<SM03>::type size03(size00.m(), size33.n());

  typename MatrixSizeType<SM10>::type size10(size11.m(), size00.n());
  typename MatrixSizeType<SM12>::type size12(size11.m(), size22.n());
  typename MatrixSizeType<SM13>::type size13(size11.m(), size33.n());

  typename MatrixSizeType<SM20>::type size20(size22.m(), size00.n());
  typename MatrixSizeType<SM21>::type size21(size22.m(), size11.n());
  typename MatrixSizeType<SM23>::type size23(size22.m(), size33.n());

  typename MatrixSizeType<SM30>::type size30(size33.m(), size00.n());
  typename MatrixSizeType<SM31>::type size31(size33.m(), size11.n());
  typename MatrixSizeType<SM32>::type size32(size33.m(), size22.n());

  // Then set the size of SparseMatrix which is inside MatrixD
  // block row 1: off-diagonal
  for (int i = 0; i < size01.m(); ++i)
    for (int j = 0; j < size01.n(); ++j)
      size01(i,j).resize(size00(i,0).m(), size11(0,j).n());

  for (int i = 0; i < size02.m(); ++i)
    for (int j = 0; j < size02.n(); ++j)
      size02(i,j).resize(size00(i,0).m(), size22(0,j).n());

  for (int i = 0; i < size03.m(); ++i)
    for (int j = 0; j < size03.n(); ++j)
      size03(i,j).resize(size00(i,0).m(), size33(0,j).n());

  // block row 2: off-diagonal
  for (int i = 0; i < size10.m(); i++)
    for (int j = 0; j < size10.n(); j++)
      size10(i,j).resize(size11(i,0).m(), size00(0,j).n());

  for (int i = 0; i < size12.m(); i++)
    for (int j = 0; j < size12.n(); j++)
      size12(i,j).resize(size11(i,0).m(), size22(0,j).n());

  for (int i = 0; i < size13.m(); i++)
    for (int j = 0; j < size13.n(); j++)
      size13(i,j).resize(size11(i,0).m(), size33(0,j).n());

  // block row 3: off-diagonal
  for (int i = 0; i < size20.m(); i++)
    for (int j = 0; j < size20.n(); j++)
      size20(i,j).resize(size22(i,0).m(), size00(0,j).n());

  for (int i = 0; i < size21.m(); i++)
    for (int j = 0; j < size21.n(); j++)
      size21(i,j).resize(size22(i,0).m(), size22(0,j).n());

  for (int i = 0; i < size23.m(); i++)
    for (int j = 0; j < size23.n(); j++)
      size23(i,j).resize(size22(i,0).m(), size33(0,j).n());

  // block row 4: off-diagonal
  for (int i = 0; i < size30.m(); i++)
    for (int j = 0; j < size30.n(); j++)
      size30(i,j).resize(size33(i,0).m(), size00(0,j).n());

  for (int i = 0; i < size31.m(); i++)
    for (int j = 0; j < size31.n(); j++)
      size31(i,j).resize(size33(i,0).m(), size11(0,j).n());

  for (int i = 0; i < size32.m(); i++)
    for (int j = 0; j < size32.n(); j++)
      size32(i,j).resize(size33(i,0).m(), size22(0,j).n());

  MatrixSizeClass size(size00, size01, size02, size03,
                       size10, size11, size12, size13,
                       size20, size21, size22, size23,
                       size30, size31, size32, size33);

  return size;
}

// update fraction needed for physically valid state
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
Real
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
updateFraction(const SystemVectorView& q, const SystemVectorView& dq, const Real maxChangeFraction) const
{
  Real updateFraction = 1.0;
  updateFraction = MIN(updateFraction, AE00_.updateFraction(q.v0, dq.v0, maxChangeFraction));
  updateFraction = MIN(updateFraction, AE11_.updateFraction(q.v1, dq.v1, maxChangeFraction));
  updateFraction = MIN(updateFraction, AE22_.updateFraction(q.v2, dq.v2, maxChangeFraction));
  updateFraction = MIN(updateFraction, AE33_.updateFraction(q.v3, dq.v3, maxChangeFraction));

  return updateFraction;
}

// check state validity
template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
bool
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
isValidStateSystemVector(SystemVectorView& q)
{
  bool check0 = AE00_.isValidStateSystemVector(q.v0);
  bool check1 = AE11_.isValidStateSystemVector(q.v1);
  bool check2 = AE22_.isValidStateSystemVector(q.v2);
  bool check3 = AE33_.isValidStateSystemVector(q.v3);

  return (check0 && check1 && check2 && check3);
}


template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
bool
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                          LinesearchData& pResData,
                          LinesearchData& pStepData) const
{
  SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block4x4::updateLinesearchDebugInfo() not unit tested!");
  int isComplete0 = AE00_.updateLinesearchDebugInfo(s, rsd.v0, pResData[0], pStepData[0]);
  int isComplete1 = AE11_.updateLinesearchDebugInfo(s, rsd.v1, pResData[1], pStepData[1]);
  int isComplete2 = AE22_.updateLinesearchDebugInfo(s, rsd.v2, pResData[2], pStepData[2]);
  int isComplete3 = AE33_.updateLinesearchDebugInfo(s, rsd.v3, pResData[3], pStepData[3]);
  return (isComplete0 && isComplete1 && isComplete2 && isComplete3);
}


template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
void
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block4x4::dumpLinesearchDebugInfo() not unit tested!");
  AE00_.dumpLinesearchDebugInfo(filenamebase + "0", nonlinear_iter, pStepData[0]);
  AE11_.dumpLinesearchDebugInfo(filenamebase + "1", nonlinear_iter, pStepData[1]);
  AE22_.dumpLinesearchDebugInfo(filenamebase + "2", nonlinear_iter, pStepData[2]);
  AE33_.dumpLinesearchDebugInfo(filenamebase + "3", nonlinear_iter, pStepData[3]);
}


template<class SM00, class SM01, class SM02, class SM03,
         class SM10, class SM11, class SM12, class SM13,
         class SM20, class SM21, class SM22, class SM23,
         class SM30, class SM31, class SM32, class SM33,
         template<class> class Base>
bool
AlgebraicEquationSet_Block4x4<SM00, SM01, SM02, SM03,
                              SM10, SM11, SM12, SM13,
                              SM20, SM21, SM22, SM23,
                              SM30, SM31, SM32, SM33, Base>::
atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm)
{
  std::vector<std::size_t> blockpartition(5,0);
  blockpartition.at(1) = AE00_.nResidNorm();
  blockpartition.at(2) = blockpartition.at(1) + AE11_.nResidNorm();
  blockpartition.at(3) = blockpartition.at(2) + AE22_.nResidNorm();
  blockpartition.at(4) = blockpartition.at(3) + AE33_.nResidNorm();

  std::vector<std::vector<Real>> AE00rsdNorm(R0norm.begin(),
                                             R0norm.begin() + blockpartition.at(1) );
  std::vector<std::vector<Real>> AE11rsdNorm(R0norm.begin() + blockpartition.at(1),
                                             R0norm.begin() + blockpartition.at(2) ) ;
  std::vector<std::vector<Real>> AE22rsdNorm(R0norm.begin() + blockpartition.at(2),
                                             R0norm.begin() + blockpartition.at(3) );
  std::vector<std::vector<Real>> AE33rsdNorm(R0norm.begin() + blockpartition.at(3),
                                             R0norm.end() );

  bool mp1 = AE00_.atMachinePrecision(q.v0, AE00rsdNorm);
  bool mp2 = AE11_.atMachinePrecision(q.v1, AE11rsdNorm);
  bool mp3 = AE22_.atMachinePrecision(q.v2, AE22rsdNorm);
  bool mp4 = AE33_.atMachinePrecision(q.v3, AE33rsdNorm);

  return (mp1 || mp2 || mp3 || mp4);
}

} // namespace SANS
