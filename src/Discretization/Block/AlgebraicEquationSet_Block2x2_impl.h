// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(ALGEBRAICEQUATIONSET_BLOCK2X2_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "AlgebraicEquationSet_Block2x2.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_NonZeroPattern.h"

#include <type_traits>

#include "tools/SANSnumerics.h"
#include "tools/output_std_vector.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Abbreviations:
// SV = system vector, SM = system matrix, NZP = nonzero pattern
//

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
residual(SystemVectorView& rsd)
{
  AES00_.residual(rsd.v0);
  AES11_.residual(rsd.v1);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
jacobian(SystemMatrix& mtx)
{
  this->template jacobian<SystemMatrix&>(mtx);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
jacobian(SystemNonZeroPattern& nz)
{
  this->template jacobian<SystemNonZeroPattern&>(nz);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
jacobianTranspose(SystemMatrix& mtx)
{
  this->template jacobianTranspose<SystemMatrix&>(mtx);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
jacobianTranspose(SystemNonZeroPattern& nz)
{
  this->template jacobianTranspose<SystemNonZeroPattern&>(nz);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
template<class SparseMatrixType>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
jacobian(SparseMatrixType jac)
{
  AES00_.jacobian(jac.m00);
   JP01_.jacobian(jac.m01);
   JP10_.jacobian(jac.m10);
  AES11_.jacobian(jac.m11);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
template<class SparseMatrixType>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
jacobianTranspose(SparseMatrixType jac)
{
  AES00_.jacobianTranspose(jac.m00);
  JP10_.jacobian(Transpose(jac.m01));
  JP01_.jacobian(Transpose(jac.m10));
  AES11_.jacobianTranspose(jac.m11);
}


//Evaluate Residual Norm
template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
std::vector<std::vector<Real>>
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
residualNorm( const SystemVectorView& rsd ) const
{
  std::vector<std::vector<Real>> merged;

  std::vector<std::vector<Real>> normAES00 = AES00_.residualNorm(rsd.v0);
  std::vector<std::vector<Real>> normAES11 = AES11_.residualNorm(rsd.v1);

  merged.reserve( normAES00.size() + normAES11.size() ); // preallocate memory
  merged.insert( merged.end(), normAES00.begin(), normAES00.end() );
  merged.insert( merged.end(), normAES11.begin(), normAES11.end() );

  return merged;
}

//Convergence check of the residual norm
template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
bool
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
convergedResidual(const std::vector<std::vector<Real>>& rsdNorm) const
{
  std::size_t nRsd0 = AES00_.nResidNorm();
  std::vector<std::vector<Real>> AES00rsdNorm(rsdNorm.begin(), rsdNorm.begin() + nRsd0);
  std::vector<std::vector<Real>> AES11rsdNorm(rsdNorm.begin() + nRsd0, rsdNorm.end());
  bool convergedAES00 = AES00_.convergedResidual(AES00rsdNorm);
  bool convergedAES11 = AES11_.convergedResidual(AES11rsdNorm);

  return (convergedAES00 && convergedAES11);
}

//Convergence check of the residual
template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
bool
AlgebraicEquationSet_Block2x2<SM00, SM01,
                     SM10, SM11, Base>::
                     convergedResidual(const std::vector<std::vector<Real>>& rsdNorm, int iEq, int iMon) const
{
  SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block2x2<>::convergedResidual not implemented");
  return false;
}

// is the residual norm decreased?
template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
bool
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
decreasedResidual(const std::vector<std::vector<Real>>& rsdNormOld,
                  const std::vector<std::vector<Real>>& rsdNormNew) const
{
  std::size_t nRsd0 = AES00_.nResidNorm();
  std::vector<std::vector<Real>> AES00rsdNormOld(rsdNormOld.begin(), rsdNormOld.begin() + nRsd0);
  std::vector<std::vector<Real>> AES11rsdNormOld(rsdNormOld.begin() + nRsd0, rsdNormOld.end());
  std::vector<std::vector<Real>> AES00rsdNormNew(rsdNormNew.begin(), rsdNormNew.begin() + nRsd0);
  std::vector<std::vector<Real>> AES11rsdNormNew(rsdNormNew.begin() + nRsd0, rsdNormNew.end());

  bool decreasedAES00 = AES00_.decreasedResidual(AES00rsdNormOld, AES00rsdNormNew);
  bool decreasedAES11 = AES11_.decreasedResidual(AES11rsdNormOld, AES11rsdNormNew);

  return (decreasedAES00 && decreasedAES11);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
setSolutionField(const SystemVectorView& q)
{
  AES00_.setSolutionField(q.v0);
  AES11_.setSolutionField(q.v1);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
fillSystemVector(SystemVectorView& q) const
{
  AES00_.fillSystemVector(q.v0);
  AES11_.fillSystemVector(q.v1);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
typename AlgebraicEquationSet_Block2x2<SM00, SM01,
                                       SM10, SM11, Base>::VectorSizeClass
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
vectorEqSize() const
{
  return VectorSizeClass(AES00_.vectorEqSize(), AES11_.vectorEqSize());
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
typename AlgebraicEquationSet_Block2x2<SM00, SM01,
                                       SM10, SM11, Base>::VectorSizeClass
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
vectorStateSize() const
{
  return VectorSizeClass(AES00_.vectorStateSize(), AES11_.vectorStateSize());
}


template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
typename AlgebraicEquationSet_Block2x2<SM00, SM01,
                                       SM10, SM11, Base>::MatrixSizeClass
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
matrixSize() const
{
  // Build up the off diagonal sizes from the diagonals
  typename MatrixSizeType<SM00>::type size00( AES00_.matrixSize() );
  typename MatrixSizeType<SM11>::type size11( AES11_.matrixSize() );

  // Assume each coupling jacobian is of type MatrixD<SparseMatrix<MatrixQ> >
  // First set the size of MatrixD
  typename MatrixSizeType<SM01>::type size01(size00.m(), size11.n());
  typename MatrixSizeType<SM10>::type size10(size11.m(), size00.n());

  // Then set the size of SparseMatrix which is inside MatrixD
  for (int i = 0; i < size01.m(); i++)
    for (int j = 0; j < size01.n(); j++)
      size01(i,j).resize(size00(i,0).m(), size11(0,j).n());

  for (int i = 0; i < size10.m(); i++)
    for (int j = 0; j < size10.n(); j++)
      size10(i,j).resize(size11(i,0).m(), size00(0,j).n());

  MatrixSizeClass size(size00, size01,
                       size10, size11);

  return size;
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
bool
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
isValidStateSystemVector(SystemVectorView& q)
{
  bool checkAES00 = AES00_.isValidStateSystemVector(q.v0);
  bool checkAES11 = AES11_.isValidStateSystemVector(q.v1);

  return (checkAES00 && checkAES11);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
int
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
nResidNorm() const
{
  SANS_DEVELOPER_EXCEPTION("AlgebraicEquationSet_Block2x2::nResidNorm not implemented");
  return -1;
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
bool
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
updateLinesearchDebugInfo(const Real& s, const SystemVectorView& rsd,
                          LinesearchData& pResData,
                          LinesearchData& pStepData) const
{
  int isComplete0 = AES00_.updateLinesearchDebugInfo(s, rsd.v0, pResData[0], pStepData[0]);
  int isComplete1 = AES11_.updateLinesearchDebugInfo(s, rsd.v1, pResData[1], pStepData[1]);
  return (isComplete0 && isComplete1);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
void
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  AES00_.dumpLinesearchDebugInfo(filenamebase + "0", nonlinear_iter, pStepData[0]);
  AES11_.dumpLinesearchDebugInfo(filenamebase + "1", nonlinear_iter, pStepData[1]);
}

template<class SM00, class SM01,
         class SM10, class SM11,
         template<class> class Base>
bool
AlgebraicEquationSet_Block2x2<SM00, SM01,
                              SM10, SM11, Base>::
atMachinePrecision(const SystemVectorView& q, const std::vector<std::vector<Real>>& R0norm)
{
  std::size_t nRsd0 = AES00_.nResidNorm();
  std::vector<std::vector<Real>> AES00rsdNorm(R0norm.begin(), R0norm.begin() + nRsd0);
  std::vector<std::vector<Real>> AES11rsdNorm(R0norm.begin() + nRsd0, R0norm.end());

  bool mp1 = AES00_.atMachinePrecision(q.v0, AES00rsdNorm);
  bool mp2 = AES11_.atMachinePrecision(q.v1, AES11rsdNorm);

  return (mp1 || mp2);
}

} //namespace SANS
