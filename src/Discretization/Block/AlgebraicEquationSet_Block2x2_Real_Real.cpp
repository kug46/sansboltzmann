// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define ALGEBRAICEQUATIONSET_BLOCK2X2_INSTANTIATE
#include "AlgebraicEquationSet_Block2x2_impl.h"

namespace SANS
{

typedef Real ArrayQ0;
typedef Real ArrayQ1;

typedef Real MatrixQ00;
typedef Real MatrixQ01;
typedef Real MatrixQ10;
typedef Real MatrixQ11;

// Sparse types

typedef SLA::SparseMatrix_CRS<MatrixQ00> SparseMatrixClass00;
typedef SLA::SparseMatrix_CRS<MatrixQ01> SparseMatrixClass01;
typedef SLA::SparseMatrix_CRS<MatrixQ10> SparseMatrixClass10;
typedef SLA::SparseMatrix_CRS<MatrixQ11> SparseMatrixClass11;

typedef DLA::MatrixD<SparseMatrixClass00> SparseSystemMatrix00;
typedef DLA::MatrixD<SparseMatrixClass01> SparseSystemMatrix01;
typedef DLA::MatrixD<SparseMatrixClass10> SparseSystemMatrix10;
typedef DLA::MatrixD<SparseMatrixClass11> SparseSystemMatrix11;

// Dense types

typedef DLA::MatrixD<MatrixQ00> DenseMatrixClass00;
typedef DLA::MatrixD<MatrixQ01> DenseMatrixClass01;
typedef DLA::MatrixD<MatrixQ10> DenseMatrixClass10;
typedef DLA::MatrixD<MatrixQ11> DenseMatrixClass11;

typedef DLA::MatrixD<DenseMatrixClass00> DenseSystemMatrix00;
typedef DLA::MatrixD<DenseMatrixClass01> DenseSystemMatrix01;
typedef DLA::MatrixD<DenseMatrixClass10> DenseSystemMatrix10;
typedef DLA::MatrixD<DenseMatrixClass11> DenseSystemMatrix11;

// Sparse instantiation
template class AlgebraicEquationSet_Block2x2<SparseSystemMatrix00, SparseSystemMatrix01,
                                             SparseSystemMatrix10, SparseSystemMatrix11>;

// Dense instantiation
template class AlgebraicEquationSet_Block2x2<DenseSystemMatrix00, DenseSystemMatrix01,
                                             DenseSystemMatrix10, DenseSystemMatrix11>;
}
