// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_DISCRETIZATION_BLOCK_JACOBIANPARAM_DECOUPLED_H_
#define SRC_DISCRETIZATION_BLOCK_JACOBIANPARAM_DECOUPLED_H_

#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"
#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "Discretization/JacobianParamBase.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class SystemMatrix,
         class AlgEqSetTraits_PDE>
class JacobianParam_Decoupled
    : public JacobianParamBase< SystemMatrix >
{
public:
  typedef JacobianParamBase< SystemMatrix > BaseType;

  typedef typename BaseType::MatrixSizeClass MatrixSizeClass;
  typedef typename BaseType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename BaseType::SystemMatrixTranspose SystemMatrixTranspose;
  typedef typename BaseType::SystemNonZeroPatternTranspose SystemNonZeroPatternTranspose;

  typedef typename BaseType::SystemMatrixView SystemMatrixView;
  typedef typename BaseType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename AlgEqSetTraits_PDE::SystemMatrix SystemMatrix_PDE;

  explicit JacobianParam_Decoupled(AlgebraicEquationSetBase<SystemMatrix_PDE>& AES_PDE)
  {
    // Nothing
  }

  virtual ~JacobianParam_Decoupled() {}

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx) override {}
  virtual void jacobian(SystemNonZeroPatternView& nz) override {}

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobian(SystemMatrixTranspose mtxT       ) override {};
  virtual void jacobian(SystemNonZeroPatternTranspose nzT) override {};
};

} //namespace SANS


#endif // SRC_DISCRETIZATION_BLOCK_JACOBIANPARAM_DECOUPLED_H_
