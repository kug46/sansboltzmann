// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRATEBOUNDARYTRACE_DISPATCH_H
#define INTEGRATEBOUNDARYTRACE_DISPATCH_H

#include "tools/call_with_derived.h"
#include "tools/add_decorator.h"

#include "pde/BCCategory.h"

#include "Integrand_Type.h"
#include "DiscretizationBCTag.h"

#include <boost/mpl/partition.hpp>
#include <boost/mpl/back_inserter.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/transform.hpp>
#include <boost/mpl/copy_if.hpp>
#include <boost/mpl/fold.hpp>

#include <memory> // shared_ptr
#include <sstream> // stringstream
#include <vector>
#include <map>

namespace SANS
{

//===========================================================================//
namespace detail
{

// Looping class: Get the current BC type check if it matches the BC parameter
template < class iter, class end, class PDEND, class NDVectorCategory, class DiscTag>
struct IntegrandBoundaryTraceConstructor_BCVector
{
  template<class... Args>
  static std::shared_ptr<IntegrandBoundaryTraceBase> create( const PDEND& pde,
                                                             const std::shared_ptr<BCBase>& BC,
                                                             const std::vector<int>& BoundaryGroups,
                                                             const Args&... args)
  {
    typedef typename boost::mpl::deref<iter>::type currentBCType;
    typedef typename currentBCType::Category Category;
    typedef typename DiscBCTag<Category, DiscTag>::type BCTag;

    // Check if the BC matches the name of the current BC type
    if ( BC->derivedTypeID() == typeid(currentBCType) )
    {
      return std::shared_ptr<IntegrandBoundaryTraceBase>( static_cast<IntegrandBoundaryTraceBase*>(
          new IntegrandBoundaryTrace<PDEND,NDVectorCategory,BCTag>(pde, *BC, BoundaryGroups, args... ) ) );
    }

    // Iterate recursively to the next option
    typedef typename boost::mpl::next<iter>::type next;
    return IntegrandBoundaryTraceConstructor_BCVector< next, end, PDEND, NDVectorCategory, DiscTag>::create(pde, BC, BoundaryGroups, args...);
  }
};

// Recursive loop terminates when 'iter' is the same as 'end'
template < class end, class PDEND, class NDVectorCategory, class DiscTag >
struct IntegrandBoundaryTraceConstructor_BCVector< end, end, PDEND, NDVectorCategory, DiscTag >
{
  template<class... Args>
  static std::shared_ptr<IntegrandBoundaryTraceBase> create( const PDEND& pde,
                                                             const std::shared_ptr<BCBase>& BC,
                                                             const std::vector<int>& BoundaryGroups,
                                                             const Args&... args )
  {
    return std::shared_ptr<IntegrandBoundaryTraceBase>(NULL);
  }
};

// Looping class: Get the current vector of BC types, and loop over that vector
template < class iter, class end, class PDEND, class DiscTag>
struct IntegrandBoundaryTraceConstructor
{
  template<class... Args>
  static std::shared_ptr<IntegrandBoundaryTraceBase> create( const PDEND& pde,
                                                             const std::shared_ptr<BCBase>& BC,
                                                             const std::vector<int>& BoundaryGroups,
                                                             const Args&... args)
  {
    typedef typename boost::mpl::deref<iter>::type currentVectorCategory;
    typedef typename currentVectorCategory::NDVector NDVector;
    typedef typename boost::mpl::begin<NDVector>::type NDVectorBegin;
    typedef typename boost::mpl::end<NDVector>::type NDVectorEnd;

    std::shared_ptr<IntegrandBoundaryTraceBase> integrand(
        IntegrandBoundaryTraceConstructor_BCVector< NDVectorBegin, NDVectorEnd, PDEND, currentVectorCategory, DiscTag>::
          create(pde, BC, BoundaryGroups, args...) );

    // return if the integrand was created
    if (integrand != nullptr) return integrand;

    // Iterate recursively to the next option
    typedef typename boost::mpl::next<iter>::type next;
    return IntegrandBoundaryTraceConstructor< next, end, PDEND, DiscTag>::create(pde, BC, BoundaryGroups, args...);
  }
};

// Recursive loop terminates when 'iter' is the same as 'end'
template < class end, class PDEND, class DiscTag >
struct IntegrandBoundaryTraceConstructor< end, end, PDEND, DiscTag >
{
  template<class... Args>
  static std::shared_ptr<IntegrandBoundaryTraceBase> create( const PDEND& pde,
                                                             const std::shared_ptr<BCBase>& BC,
                                                             const std::vector<int>& BoundaryGroups,
                                                             const Args&... args )
  {
    return std::shared_ptr<IntegrandBoundaryTraceBase>(NULL);
  }
};

} // namespace detail

//===========================================================================//
// Meta function to generate a pair with BCVector and Category
template<class BCVector, class Category>
struct BCVectorByCategory
{
  // Make a vector with all BCs of the provided category
  typedef typename boost::mpl::copy_if<
            BCVector,
            std::is_same< getCategory<boost::mpl::_1>, Category >,
            boost::mpl::back_inserter< boost::mpl::vector0<> > >::type BCVecByCat;

  //Wrap the vector so we know what category is in all BCs of the vector
  typedef NDVectorCategory<BCVecByCat, Category> type;
};

//===========================================================================//
// Meta function to check if a BCVector contains a specific category
template<class BCVector>
struct BCVectorHasCategory
{
  template<class Category>
  struct apply :
    boost::mpl::bool_<(boost::mpl::count_if< BCVector, std::is_same< getCategory<boost::mpl::_1>, Category > >::value > 0)>
                    {};
};

//===========================================================================//
// Meta function to create vectors of vectors with the same BC category
template<class BCVector>
struct GroupBCVectorByCategory
{
  typedef typename boost::mpl::fold<
            BCCategoryVector,
            boost::mpl::vector0<>,
            boost::mpl::if_< typename BCVectorHasCategory<BCVector>::template apply<boost::mpl::_2>,  // if BCVecor has Category
                             boost::mpl::push_back<boost::mpl::_1,
                             BCVectorByCategory<BCVector, boost::mpl::_2> >, // (true) add all BCs with the Category
                             boost::mpl::_1 >                                // (false) do nothing
            >::type type;
};

//===========================================================================//
// Template metafunction to define an IntegrandBoundaryTrace type
template<class PDEND, class DiscTag, class BCNDVecCat>
struct def_IntegrandBoundaryTrace
{
  typedef typename DiscBCTag<typename BCNDVecCat::Category, DiscTag>::type BCTag;
  typedef IntegrandBoundaryTrace<PDEND, BCNDVecCat, BCTag> type;
};


//===========================================================================//
template<class PDEND, template<class,class> class BCNDConvert, class BCVector_, class DiscTag>
struct IntegrateBoundaryTrace_Dispatch
{
  // Remove SpaceTime BCs if this is only a spatial configuration
  typedef typename BCVectorSanitizeSpaceTime<BCNDConvert, BCVector_>::type BCVector;

  // First separate the BCVector into a pair of vectors depending on if they need field trace elements or not
  typedef typename boost::mpl::partition< BCVector,
                                          CategoryNeedsFieldTrace< getCategory<boost::mpl::_1> >,
                                          boost::mpl::back_inserter< boost::mpl::vector0<> >,
                                          boost::mpl::back_inserter< boost::mpl::vector0<> >
                                        >::type PairBCVectors;

  // Define the two BCVectors
  typedef typename PairBCVectors::first  BCVectormitFT;
  typedef typename PairBCVectors::second BCVector2;

  // Separate out any hub trace boundaries
  typedef typename boost::mpl::partition< BCVector2,
                                          CategoryNeedsHubTrace< getCategory<boost::mpl::_1> >,
                                          boost::mpl::back_inserter< boost::mpl::vector0<> >,
                                          boost::mpl::back_inserter< boost::mpl::vector0<> >
                                        >::type PairBCVectors2;

  // Define the two BCVectors with hub trace and those without either hub trace or field trace
  typedef typename PairBCVectors2::first  BCVectormitHT;
  typedef typename PairBCVectors2::second BCVectorsansFT;

  // Define the operator that transforms an entry in the BCVector to an IntegrandBoundaryTrace type
  typedef def_IntegrandBoundaryTrace<PDEND, DiscTag, boost::mpl::_1> def_IntegrandBoundaryTraceOp;

  // Decorate the BCVector with the NDConvert class
  typedef typename boost::mpl::transform< BCVectormitFT , add_ND_decorator<BCNDConvert, boost::mpl::_1> >::type BCNDVectormitFT;
  typedef typename boost::mpl::transform< BCVectormitHT , add_ND_decorator<BCNDConvert, boost::mpl::_1> >::type BCNDVectormitHT;
  typedef typename boost::mpl::transform< BCVectorsansFT, add_ND_decorator<BCNDConvert, boost::mpl::_1> >::type BCNDVectorsansFT;

  // Group the BCVectors by category
  typedef typename GroupBCVectorByCategory<BCNDVectormitFT>::type BCNDVectorsmitFTcat;
  typedef typename GroupBCVectorByCategory<BCNDVectormitHT>::type BCNDVectorsmitHTcat;
  typedef typename GroupBCVectorByCategory<BCNDVectorsansFT>::type BCNDVectorssansFTcat;

  // Create integrand boundary trace vectors
  typedef typename boost::mpl::transform< BCNDVectorsmitFTcat , def_IntegrandBoundaryTraceOp >::type IntegrandBoundaryTracemitFTVector;
  typedef typename boost::mpl::transform< BCNDVectorsmitHTcat , def_IntegrandBoundaryTraceOp >::type IntegrandBoundaryTracemitHTVector;
  typedef typename boost::mpl::transform< BCNDVectorssansFTcat, def_IntegrandBoundaryTraceOp >::type IntegrandBoundaryTracesansFTVector;

//---------------------------------------------------------------------------//
  template<class DispatchmitFTType, class DispatchsansFTType>
  void dispatch( DispatchmitFTType&& dispatchmitFT,
                 DispatchsansFTType&& dispatchsansFT ) const
  {
    // Loop over all integrand boundary traces and dispatch the call
    // with the IntegrandBoundaryTraceBase cast to the derived IntegrandBoundaryTrace
    for (std::size_t n = 0; n < integrandBoundaryTraces_.size(); n++ )
    {
      // Hub trace is handled separately
      if ( integrandBoundaryTraces_[n]->needsHubTrace() ) continue;

      if ( integrandBoundaryTraces_[n]->needsFieldTrace() )
        call_with_derived<IntegrandBoundaryTracemitFTVector, DispatchmitFTType&>( dispatchmitFT, *integrandBoundaryTraces_[n] );
      else
        call_with_derived<IntegrandBoundaryTracesansFTVector, DispatchsansFTType&>( dispatchsansFT, *integrandBoundaryTraces_[n] );
    }
  }

  //---------------------------------------------------------------------------//
    template<class DispatchsansFTType>
    void dispatchSANSFT( DispatchsansFTType&& dispatchsansFT ) const
    {
      // Loop over all integrand boundary traces and dispatch the call
      // with the IntegrandBoundaryTraceBase cast to the derived IntegrandBoundaryTrace
      for (std::size_t n = 0; n < integrandBoundaryTraces_.size(); n++ )
      {
        // Hub trace is handled separately
        if ( integrandBoundaryTraces_[n]->needsHubTrace() ) continue;

        if ( integrandBoundaryTraces_[n]->needsFieldTrace() )
        {
          SANS_DEVELOPER_EXCEPTION("BC NEEDS FIELDTRACE, NOT IMPLEMENTED YET");
        }
        else
          call_with_derived<IntegrandBoundaryTracesansFTVector, DispatchsansFTType&>( dispatchsansFT, *integrandBoundaryTraces_[n] );
      }
    }

//---------------------------------------------------------------------------//
  template<class DispatchmitHTType>
  void dispatch_HubTrace( DispatchmitHTType&& dispatchmitHT ) const
  {
    // Loop over all integrand boundary traces and dispatch the call
    // with the IntegrandBoundaryTraceBase cast to the derived IntegrandBoundaryTrace
    for (std::size_t n = 0; n < integrandBoundaryTraces_.size(); n++ )
    {
      // Only consider hub trace functions
      if ( integrandBoundaryTraces_[n]->needsHubTrace() )
        call_with_derived<IntegrandBoundaryTracemitHTVector, DispatchmitHTType&>( dispatchmitHT, *integrandBoundaryTraces_[n] );
    }
  }

  //---------------------------------------------------------------------------//
  template<class DispatchmitHTType>
  void dispatch_HubTrace_FieldTraceDummy( DispatchmitHTType&& dispatchmitHT ) const
  {
    // Loop over all integrand boundary traces and dispatch the call
    // with the IntegrandBoundaryTraceBase cast to the derived IntegrandBoundaryTrace
    for (std::size_t n = 0; n < integrandBoundaryTraces_.size(); n++ )
    {
      // Only consider hub trace functions
      if ( integrandBoundaryTraces_[n]->needsHubTrace() )
        call_with_derived<IntegrandBoundaryTracemitHTVector, DispatchmitHTType&>( dispatchmitHT, *integrandBoundaryTraces_[n] );
    }
  }

protected:
  std::vector< std::shared_ptr<IntegrandBoundaryTraceBase> > integrandBoundaryTraces_;

  void BCKeyError(const std::string& key, const std::map< std::string, std::vector<int> >& BoundaryGroups)
  {
    std::stringstream error_msg;
    // construct error message
    error_msg << "Could not find: '" << key << "' in BoundaryGroups:" << std::endl;
    for (auto it = BoundaryGroups.begin(); it != BoundaryGroups.end(); ++it)
    {
      error_msg << " " <<  it->first << std::endl;
    }
    // Output the error message
    SANS_DEVELOPER_EXCEPTION(error_msg.str());
  }
};

}

#endif //INTEGRATEBOUNDARYTRACE_DISPATCH_H
