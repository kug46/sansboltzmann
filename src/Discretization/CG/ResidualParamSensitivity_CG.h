// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALPARAMSENSITIVITY_CG_H
#define RESIDUALPARAMSENSITIVITY_CG_H

#include <type_traits>

#include "../Galerkin/IntegrandBoundaryTrace_Flux_mitState_Nitsche.h"
#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/Galerkin/ResidualCell_Galerkin.h"
#include "Discretization/Galerkin/ResidualBoundaryTrace_Dispatch_Galerkin.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"

#include "Discretization/Galerkin/Stabilization_Nitsche.h"

namespace SANS
{

// Forward declare
template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;


//----------------------------------------------------------------------------//
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class SurrealClass, class XFieldType>
class ResidualParamSensitivity_CG
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template ArrayQ<SurrealClass> ArrayQSurreal;

  typedef SLA::SparseVector<ArrayQSurreal> VectorClass;
  typedef DLA::VectorD<VectorClass> SystemVector;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandCellClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;

  template< class... BCArgs >
  ResidualParamSensitivity_CG(const XFieldType& xfld,
                              Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                              Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                              const NDPDEClass& pde,
                              const StabilizationNitsche& stab,
                              const std::vector<int>& CellGroups,
                              PyDict& BCList,
                              const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args ):
    fcnCell_(pde, CellGroups),
    BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
    dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab),
    xfld_(xfld),
    qfld_(qfld),
    lgfld_(lgfld),
    pde_(pde)
  {
    quadratureOrder_.resize(std::max(xfld.nCellGroups(), xfld.getXField().nBoundaryTraceGroups()), -1);
  }

  ~ResidualParamSensitivity_CG() {}

  //Computes the residual
  void residualSensitivity(SystemVector& rsd);

  // Gives the PDE and solution indices in the system
  int indexPDE() const { return iPDE; }
  int indexQ() const { return iq; }

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iBC = 1;
  static const int iq = 0;
  static const int ilg = 1;

protected:
  IntegrandCellClass fcnCell_;
  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  const XFieldType& xfld_;
  Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const NDPDEClass& pde_;

  std::vector<int> quadratureOrder_;
};

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class SurrealClass, class XFieldType>
void
ResidualParamSensitivity_CG<NDPDEClass, BCNDConvert, BCVector, SurrealClass, XFieldType>::
residualSensitivity(SystemVector& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate( ResidualCell_Galerkin(fcnCell_, rsd(iPDE)),
                                           xfld_, qfld_, &quadratureOrder_[0], xfld_.nCellGroups() );

  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin( xfld_, qfld_, lgfld_,
                                                     &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                     rsd(iPDE), rsd(iBC) ),
      ResidualBoundaryTrace_Dispatch_Galerkin( xfld_, qfld_,
                                                      &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                      rsd(iPDE) ) );
}


} //namespace SANS

#endif //RESIDUALPARAMSENSITIVITY_CG_H
