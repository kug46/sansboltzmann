// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRAND2DFUNCTOR_CGSTABILIZED_H
#define INTEGRAND2DFUNCTOR_CGSTABILIZED_H

// 2-D area/edge integrand operators: CG w/ multiscale/adjoint stabilization

#include <ostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "ElementXField2DLine.h"
#include "ElementXField2DArea.h"
#include "ElementDField2DArea.h"
#include "ElementQField2DArea.h"
#include "ReferenceElement.h"
#include "DenseLinAlg/InverseLU.h"


//----------------------------------------------------------------------------//
// 2D element area integrand: CG w/ multiscale/adjoint stabilization

template <class Topology, class PDE, class T>
class Integrand2DAreaFunctor_CGStabilized
{
public:
  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // jacobian matrices

  typedef std::array<int,Topology::NEdge> IntNEdge;     // edge signs

  static const int N = PDE::N;              // total PDE equations

  Integrand2DAreaFunctor_CGStabilized(
    const PDE& pde,
    const ElementXField2DArea<Topology>& xfldElem,
    const ElementDField2DArea<Topology>& dfldElem,
    const ElementQField2DArea<Topology,PDE,T>& qfldElem ) :
    pde_(pde), xfldElem_(xfldElem), dfldElem_(dfldElem), qfldElem_(qfldElem) {}

  // check whether integrand needs to be evaluated
  bool needsEvaluation() const;

  // total PDE equations
  int nEqn() const { return PDE::N; }

  // total DOFs
  int nDOF() const { return qfldElem_.nDOF(); }

  // 2D element area integrand
  void operator()( const Real& s, const Real& t, ArrayQ<T> integrand[], int neq ) const;

private:
  const PDE& pde_;
  const ElementXField2DArea<Topology>&       xfldElem_;
  const ElementDField2DArea<Topology>&       dfldElem_;
  const ElementQField2DArea<Topology,PDE,T>& qfldElem_;
};


template <class Topology, class PDE, class T>
bool
Integrand2DAreaFunctor_CGStabilized<Topology,PDE,T>::needsEvaluation() const
{
  return ( pde_.hasFluxAdvective() ||
           pde_.hasFluxViscous() ||
           pde_.hasSource() ||
           pde_.hasForcingFunction() );
}


// 2D element area integrand
//
// integrand = - gradient(phi)*F + L'(phi;q)*tau*L(q) + phi*S - phi*RHS
//
// where
//   phi              basis function
//   F(x,y,q,qx,qy)   flux (advective and viscous)
//   S(x,y,d,q)       solution-dependent source
//   RHS(x,y)         right-hand side forcing function
//
//   L'(phi;x,y,d,q)                strong-form adjoint PDE
//   tau(x,y,d,q)                   stabilization matrix
//   L(x,y,d,q,qx,qy,qxx,qxy,qyy)   strong-form primal PDE
//
template <class Topology, class PDE,class T>
void
Integrand2DAreaFunctor_CGStabilized<Topology,PDE,T>::operator()(
    const Real& s, const Real& t, ArrayQ<T> integrand[], int neqn ) const
{
  const int nDOF = qfldElem_.nDOF();         // total solution DOFs in element

  SANS_ASSERT(neqn == nDOF);

  Real x, y;                  // physical coordinates
  Real dist;                  // distance function

  Real phi[nDOF];             // basis
  Real phix[nDOF];            // basis gradient
  Real phiy[nDOF];
  Real phixx[nDOF];           // basis hessian
  Real phixy[nDOF];
  Real phiyy[nDOF];

  ArrayQ<T> q;                // solution
  ArrayQ<T> qx, qy;           // solution gradient
  ArrayQ<T> qxx, qxy, qyy;    // solution hessian

  ArrayQ<T> f, g;             // PDE flux F(X, U), Fv(X, U, UX)
  ArrayQ<T> source;           // PDE source S(X, D, U, UX)
  ArrayQ<T> rhs;              // PDE forcing function RHS(X)

  const bool needsSolutionGradient =
      pde_.hasFluxViscous() ||
      (pde_.hasSource() && pde_.needsSolutionGradientforSource());
  const bool needsSolutionHessian =
      pde_.hasFluxViscous() && false;

  // physical coordinates
  xfldElem_.coordinates( s, t, x, y );

  // basis value, gradient
  IntNEdge edgeSign = xfldElem_.edgeSign();
  const BasisFunctionAreaBase<Triangle>& qbasis = qfldElem_.basis();
  qbasis.evalBasis( s, t, edgeSign, phi, nDOF );
  evalBasisGradient( s, t, xfldElem_, qbasis, phix, phiy, nDOF );

  // solution value, gradient
  qfldElem_.evalSolutionFromBasis( phi, nDOF, q );
  if (needsSolutionGradient)
  {
    qfldElem_.evalSolutionFromBasis( phix, nDOF, qx );
    qfldElem_.evalSolutionFromBasis( phiy, nDOF, qy );
  }
  if (needsSolutionHessian)
  {
    qfldElem_.evalSolutionFromBasis( phixx, nDOF, qxx );
    qfldElem_.evalSolutionFromBasis( phixy, nDOF, qxy );
    qfldElem_.evalSolutionFromBasis( phiyy, nDOF, qyy );
  }

  //printf( "phix = %e %e %e\n", phix[0], phix[1], phix[2] );
  //printf( "phiy = %e %e %e\n", phiy[0], phiy[1], phiy[2] );

  for (int k = 0; k < nDOF; k++)
    integrand[k] = 0;

  // Galerkin flux term: -gradient(phi) . F

  if (pde_.hasFluxAdvective() || pde_.hasFluxViscous())
  {
    f = g = 0;

    // advective flux
    pde_.fluxAdvective( x, y, 0, q, f, g );
    //printf( "f = %e %e\n", f[0], f[1] );
    //printf( "g = %e %e\n", g[0], g[1] );

    // viscous flux
    pde_.fluxViscous( x, y, 0, q, qx, qy, f, g );

    for (int k = 0; k < nDOF; k++)
      integrand[k] -= phix[k]*f + phiy[k]*g;
  }

  // Galerkin source term: +phi S

  if (pde_.hasSource())
  {
    if (pde_.needsDistanceforSource())
      dfldElem_.distance( s, t, dist );
    else
      dist = 0;

    source = 0;
    if (pde_.needsSolutionGradientforSource() && pde_.needsDistanceforSource())
      pde_.source( x, y, 0, q, qx, qy, source ); //Distance removed for ND framework

    for (int k = 0; k < nDOF; k++)
      integrand[k] += phi[k]*source;
  }

  // Galerkin right-hand-side forcing function: -phi RHS

  if (pde_.hasForcingFunction())
  {
    rhs = 0;
    pde_.forcingFunction( x, y, 0, rhs );

    for (int k = 0; k < nDOF; k++)
      integrand[k] -= phi[k]*rhs;
  }

  // adjoint stabilization: - adjointPDE(phi) . tau . primalPDE(q)

  if (false)
  {

    // strong form PDE residual

    ArrayQ<T> strongPDE = 0;

    if (pde_.hasFluxAdvective())
      pde_.strongFluxAdvective( x, y, 0, q, qx, qy, strongPDE );

    if (pde_.hasFluxViscous())
      pde_.strongFluxViscous( x, y, 0, q, qx, qy, qxx, qxy, qyy, strongPDE );

    if (pde_.hasSource())
      strongPDE += source;

    if (pde_.hasForcingFunction())
      strongPDE -= rhs;

    // adjoint PDE

    MatrixQ<T> dfdu = 0;
    MatrixQ<T> dgdu = 0;
    if (pde_.hasFluxAdvective())
      pde_.jacobianFluxAdvective( x, y, 0, q, dfdu, dgdu );

    MatrixQ<T> kxx = 0;
    MatrixQ<T> kxy = 0;
    MatrixQ<T> kyx = 0;
    MatrixQ<T> kyy = 0;
    if (pde_.hasFluxViscous())
    {
      pde_.diffusionViscous( x, y, 0, q, kxx, kxy, kyx, kyy );
      pde_.jacobianFluxViscous( x, y, 0, q, qx, qy, dfdu, dgdu );
    }

    MatrixQ<T> dsdu = 0;
    if (pde_.hasSource())
    {
      pde_.jacobianSource( x, y, 0, q, qx, qy, dsdu ); //Distance removed for ND framework
    }

    // stabilization matrix: tau

    MatrixQ<T> tau = 0;

    if (pde_.hasFluxAdvective())
    {
      for (int k = 0; k < nDOF; k++)
        pde_.jacobianFluxAdvectiveAbsoluteValue( x, y, 0, q, phix[k], phiy[k], tau );
    }

    if (pde_.hasFluxViscous())
    {
      for (int k = 0; k < nDOF; k++)
        tau += phix[k]*(kxx*phix[k] + kxy*phiy[k]) + phiy[k]*(kxy*phix[k] + kyy*phiy[k]);
    }

    if (pde_.hasSource())
    {
      tau += dsdu;
    }

    // LU solve: strongPDE -> Inverse[tau].strongPDE

    strongPDE = SANS::DLA::InverseLU::Solve(tau, strongPDE);

    // stabilization term

    ArrayQ<T> term   = dsdu        * strongPDE;
    ArrayQ<T> termx  = dfdu        * strongPDE;
    ArrayQ<T> termy  = dgdu        * strongPDE;
    ArrayQ<T> termxx = kxx         * strongPDE;
    ArrayQ<T> termxy = (kxy + kyx) * strongPDE;
    ArrayQ<T> termyy = kyy         * strongPDE;
    for (int k = 0; k < nDOF; k++)
      integrand[k] += phi[k]*term + phix[k]*termx + phiy[k]*termy +
                      phixx[k]*termxx + phixy[k]*termxy + phiyy[k]*termyy;
  }
}

#endif  // INTEGRAND2DFUNCTOR_CGSTABILIZED_H
