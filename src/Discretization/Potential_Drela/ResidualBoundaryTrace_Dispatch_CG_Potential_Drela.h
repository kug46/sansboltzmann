// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_DISPATCH_CG_POTENTIAL_DRELA_H
#define RESIDUALBOUNDARYTRACE_DISPATCH_CG_POTENTIAL_DRELA_H

// boundary-trace integral residual functions

//#include "ResidualBoundaryTrace_mitLG_Galerkin_LIP.h"
#include "Discretization/Potential_Drela/ResidualBoundaryTrace_CG_Potential_Drela.h"

//#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, class Vector, class ArrayQ>
class ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_LIP_impl
{
public:
  ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_LIP_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      Vector& rsdPDEGlobal,
      Vector& rsdBCGlobal )
    : xfld_(xfld), qfld_(qfld), lgfld_(lgfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal), rsdBCGlobal_(rsdBCGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented yet...");
    //IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
    //    ResidualBoundaryTrace_mitLG_Galerkin(fcn, rsdPDEGlobal_, rsdBCGlobal_),
    //    xfld_, qfld_, lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector& rsdPDEGlobal_;
  Vector& rsdBCGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, class Vector, class ArrayQ>
ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_LIP_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ>
ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_LIP(const XFieldType& xfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                  const int* quadratureorder, int ngroup,
                                                  Vector& rsdPDEGlobal,
                                                  Vector& rsdBCGlobal)
{
  return {xfld, qfld, lgfld, quadratureorder, ngroup, rsdPDEGlobal, rsdBCGlobal};
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class XFieldType, class PhysDim, class TopoDim, class Vector, class ArrayQ>
class ResidualBoundaryTrace_Dispatch_CG_Potential_Drela_impl
{
public:
  ResidualBoundaryTrace_Dispatch_CG_Potential_Drela_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      Vector& rsdPDEGlobal )
    : xfld_(xfld), qfld_(qfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      rsdPDEGlobal_(rsdPDEGlobal)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
      ResidualBoundaryTrace_CG_Potential_Drela(fcn, rsdPDEGlobal_, get<-1>(xfld_).dupPointOffset_, get<-1>(xfld_).invPointMap_),
                                                xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  Vector& rsdPDEGlobal_;
};

// Factory function

template<class XFieldType, class PhysDim, class TopoDim, class Vector, class ArrayQ>
ResidualBoundaryTrace_Dispatch_CG_Potential_Drela_impl<XFieldType, PhysDim, TopoDim, Vector, ArrayQ>
ResidualBoundaryTrace_Dispatch_CG_Potential_Drela(const XFieldType& xfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                  const int* quadratureorder, int ngroup,
                                                  Vector& rsdPDEGlobal)
{
  return {xfld, qfld, quadratureorder, ngroup, rsdPDEGlobal};
}


}

#endif //RESIDUALBOUNDARYTRACE_DISPATCH_CG_POTENTIAL_DRELA_H
