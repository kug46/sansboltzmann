// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_DRELA_H
#define JACOBIANBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_DRELA_H

// jacobian boundary-trace integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class QFieldCellGroupTypeL, class QFieldCellGroupTypeR,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryTrace_WakeCut_CG_Potential_Drela_ScatterAdd(
    const QFieldCellGroupTypeL& qfldCellL,
    const QFieldCellGroupTypeR& qfldCellR,
    const int elemL, const int elemR,
    int mapDOFGlobalL[], const int nDOFL,
    int mapDOFGlobalR[], const int nDOFR,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemR,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemR_qElemR,
    SparseMatrix<MatrixQ>& mtxGlobalPDE_q )
{
  qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL, nDOFL );
  qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR, nDOFR );

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, mapDOFGlobalL, nDOFL, mapDOFGlobalL, nDOFL );
  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemR, mapDOFGlobalL, nDOFL, mapDOFGlobalR, nDOFR );

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qElemL, mapDOFGlobalR, nDOFR, mapDOFGlobalL, nDOFL );
  mtxGlobalPDE_q.scatterAdd( mtxPDEElemR_qElemR, mapDOFGlobalR, nDOFR, mapDOFGlobalR, nDOFR );
}

#if 0
//----------------------------------------------------------------------------//
template <class QFieldCellGroupType, class QFieldTraceGroupType,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryTrace_WakeCut_CG_Potential_Drela_ScatterAdd(
    const QFieldCellGroupType& qfldCellL,
    const int elemL, const int elem,
    int mapDOFGlobalL[], const int nDOFL,
    int mapDOFGlobalWake[], const int nDOFWake,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_lgTrace,
    SANS::DLA::MatrixD<MatrixQ>& mtxWake_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxWake_lgTrace,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_lg,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalWake_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalWake_lg )
{

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryWake_ScatterAdd: elemL, elem = " << elemL << ," " << elem << std::endl;
  std::cout << "JacobianPDE_Group_2DBoundaryWake_ScatterAdd (before): mtxPDEGlobal = ";
  mtxPDEGlobal.dump(2);
#endif

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryWake_ScatterAdd: mtxGlobalPDE_q = ";  mtxGlobalPDE_q.dump(2);
#endif

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
  mtxGlobalPDE_lg.scatterAdd( mtxPDEElemL_lgTrace, elemL, elem );

  mtxGlobalWake_q.scatterAdd( mtxWake_qElemL, elem, elemL );
  mtxGlobalWake_lg.scatterAdd( mtxWake_lgTrace, elem, elem );
}
#endif

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//
//  functions dispatched based on left (L) element topology
//
//  process:
//  (a) loop over groups; dispatch to L (JacobianPDE_LeftTopology_Integral2DBoundaryWake)
//  (b) call base class routine with specific L (JacobianPDE_Group_Integral2DBoundaryWake w/ Base& params)
//  (c) cast to specific L and call L-specific topology routine (JacobianPDE_Group_Integral2DBoundaryWake)
//  (d) loop over Wakes and integrate


template <class Surreal, class TopologyTrace, class TopologyL, class TopologyR,
          class PhysDim, class TopoDim, class ArrayQ,
          class IntegrandTrace_Galerkin_WakeCut,
          class SparseMatrix>
void
JacobianBoundaryTrace_WakeCut_CG_Potential_Drela_Group_Integral(
    const IntegrandTrace_Galerkin_WakeCut& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR>& xfldCellR,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q )
{
  typedef typename IntegrandTrace_Galerkin_WakeCut::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandTrace_Galerkin_WakeCut::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

  typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
  typedef typename QFieldCellGroupTypeR::template ElementType<Surreal> ElementQFieldClassR;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  ElementXFieldClassR xfldElemR( xfldCellR.basis() );
  ElementQFieldClassR qfldElemR( qfldCellR.basis() );

  ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );

  // variables/equations per DOF
  const int nEqn = IntegrandTrace_Galerkin_WakeCut::N;

  // DOFs per element
  int nDOFL = qfldElemL.nDOF();
  int nDOFR = qfldElemR.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobalL(nDOFL);
  std::vector<int> mapDOFGlobalR(nDOFR);

  // trace element integral
  GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal, ArrayQSurreal>
    integral(quadratureorder, nDOFL, nDOFR);

  // element integrand/residuals
  std::vector<ArrayQSurreal> rsdPDEElemL( nDOFL );
  std::vector<ArrayQSurreal> rsdPDEElemR( nDOFR );

  // element jacobians
  MatrixElemClass mtxPDEElemL_qElemL(nDOFL, nDOFL);
  MatrixElemClass mtxPDEElemL_qElemR(nDOFL, nDOFR);

  MatrixElemClass mtxPDEElemR_qElemL(nDOFR, nDOFL);
  MatrixElemClass mtxPDEElemR_qElemR(nDOFR, nDOFR);

  // loop over elements within group
  int nelem = xfldTrace.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    // zero element Jacobians
    mtxPDEElemL_qElemL = 0;
    mtxPDEElemL_qElemR = 0;

    mtxPDEElemR_qElemL = 0;
    mtxPDEElemR_qElemR = 0;

    // left element
    const int elemL = xfldTrace.getElementLeft( elem );
    const int elemR = xfldTrace.getElementRight( elem );
    const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldCellR.getElement( xfldElemR, elemR );
    qfldCellR.getElement( qfldElemR, elemR );

    xfldTrace.getElement(  xfldElemTrace, elem );

    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nDOFR );

    // number of simultaneous derivatives per functor call
    const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();

    // loop over derivative chunks
    for (int nchunk = 0; nchunk < nEqn*(nDOFL + nDOFR); nchunk += nDeriv)
    {

      // associate derivative slots with solution variables

      int slot;
      for (int j = 0; j < nDOFL; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
        }
      }
      for (int j = 0; j < nDOFR; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          for (int k = 0; k < nDeriv; k++)
            DLA::index(qfldElemR.DOF(j), n).deriv(k) = 0;

          slot = nEqn*(nDOFL + j) + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            DLA::index(qfldElemR.DOF(j), n).deriv(slot - nchunk) = 1;
        }
      }

      // line integration for canonical element

      for (int n = 0; n < nDOFL; n++)
        rsdPDEElemL[n] = 0;

      for (int n = 0; n < nDOFR; n++)
        rsdPDEElemR[n] = 0;

      integral(
          fcn.integrand(xfldElemTrace,
                        canonicalTraceL, canonicalTraceR,
                        xfldElemL, qfldElemL,
                        xfldElemR, qfldElemR),
          xfldElemTrace,
          rsdPDEElemL.data(), nDOFL,
          rsdPDEElemR.data(), nDOFR );

      // accumulate derivatives into element jacobian

      for (int j = 0; j < nDOFL; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*j + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int i = 0; i < nDOFL; i++)
            {
              SANS_ASSERT(mapDOFGlobalL[i] < fcn.dupPointOffset);
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemL_qElemL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);
            }

            for (int i = 0; i < nDOFR; i++)
            {
              //Skip non-duplicated points
              if (mapDOFGlobalR[i] < fcn.dupPointOffset) continue;
              //Skip Kutta points if found
              if ( std::find(fcn.KuttaPoints.begin(), fcn.KuttaPoints.end(), mapDOFGlobalR[i]) != fcn.KuttaPoints.end() )
                DLA::index(rsdPDEElemR[i], 0) = 0;
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemR_qElemL(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot - nchunk);
            }
          }
        }
      }

      for (int j = 0; j < nDOFR; j++)
      {
        for (int n = 0; n < nEqn; n++)
        {
          slot = nEqn*(nDOFL + j) + n;
          if ((slot >= nchunk) && (slot < nchunk + nDeriv))
          {
            for (int i = 0; i < nDOFL; i++)
            {
              SANS_ASSERT(mapDOFGlobalL[i] < fcn.dupPointOffset);
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemL_qElemR(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);
            }

            for (int i = 0; i < nDOFR; i++)
            {
              //Skip non-duplicated points
              if (mapDOFGlobalR[i] < fcn.dupPointOffset) continue;
              //Skip Kutta points if found
              if ( std::find(fcn.KuttaPoints.begin(), fcn.KuttaPoints.end(), mapDOFGlobalR[i]) != fcn.KuttaPoints.end() )
                 DLA::index(rsdPDEElemR[i], 0) = 0;
              for (int m = 0; m < nEqn; m++)
                DLA::index(mtxPDEElemR_qElemR(i,j), m,n) = DLA::index(rsdPDEElemR[i], m).deriv(slot - nchunk);
            }
          }
        }
      }
    }   // nchunk

    // scatter-add element jacobian to global

    JacobianBoundaryTrace_WakeCut_CG_Potential_Drela_ScatterAdd(
        qfldCellL, qfldCellR,
        elemL, elemR,
        mapDOFGlobalL.data(), nDOFL,
        mapDOFGlobalR.data(), nDOFR,
        mtxPDEElemL_qElemL, mtxPDEElemL_qElemR,
        mtxPDEElemR_qElemL, mtxPDEElemR_qElemR,
        mtxGlobalPDE_q );

  }
}


//----------------------------------------------------------------------------//
template <class Surreal, class TopologyTrace, class TopologyL, class TopologyR,
          class IntegrandTrace_Galerkin_WakeCut,
          class PhysDim, class TopoDim, class ArrayQ,
          class SparseMatrix>
void
JacobianBoundaryTrace_WakeCut_CG_Potential_Drela_Group(
    const IntegrandTrace_Galerkin_WakeCut& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q )
{
  const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

  const int groupL = xfldTrace.getGroupLeft();
  const int groupR = xfldTrace.getGroupRight();

  // Integrate over the trace group
  JacobianBoundaryTrace_WakeCut_CG_Potential_Drela_Group_Integral<Surreal,TopologyTrace,TopologyL,TopologyR,PhysDim,TopoDim,ArrayQ>(
      fcn, xfldTrace,
      xfld.template getCellGroup<TopologyL>(groupL), qfld.template getCellGroup<TopologyL>(groupL),
      xfld.template getCellGroup<TopologyR>(groupR), qfld.template getCellGroup<TopologyR>(groupR),
      quadratureorder,
      mtxGlobalPDE_q );
}

template<class Surreal, class TopoDim>
class JacobianBoundaryTrace_WakeCut_CG_Potential_Drela;

#if 0
template<class Surreal>
class JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<Surreal,TopoD2>
{
public:
  typedef TopoD2 TopoDim;

  //----------------------------------------------------------------------------//
  template <class TopologyTrace,
            class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  LeftTopology(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q,
      SparseMatrix& mtxGlobalPDE_lg,
      SparseMatrix& mtxGlobalWake_q,
      SparseMatrix& mtxGlobalWake_lg )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // integrate over the boundary
      JacobianBoundaryTrace_WakeCut_CG_Potential_Drela_Group<Surreal, TopologyTrace, Triangle>(
          fcn, xfldTrace, qfld, lgfldTrace,
          quadratureorder,
          mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalWake_q, mtxGlobalWake_lg );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryTrace_Galerkin<TopoD2>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q,
      SparseMatrix& mtxGlobalPDE_lg,
      SparseMatrix& mtxGlobalWake_q,
      SparseMatrix& mtxGlobalWake_lg )
  {
    SANS_ASSERT( mtxGlobalPDE_q.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalPDE_lg.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_lg.n() == lgfld.nDOF() );

    SANS_ASSERT( mtxGlobalWake_q.m() == lgfld.nDOF() );
    SANS_ASSERT( mtxGlobalWake_q.n() == qfld.nDOF() );

    SANS_ASSERT( mtxGlobalWake_lg.m() == lgfld.nDOF() );
    SANS_ASSERT( mtxGlobalWake_lg.n() == lgfld.nDOF() );

    SANS_ASSERT( &qfld.getXField() == &lgfld.getXField() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over line element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        // dispatch to determine left topology
        LeftTopology<Line>(
            fcn,
            xfld.template getBoundaryTraceGroup<Line>(boundaryGroup),
            qfld,
            lgfld.template getBoundaryTraceGroup<Line>(boundaryGroup),
            quadratureorder[boundaryGroup],
            mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalWake_q, mtxGlobalWake_lg );
      }
      else
      {
        char msg[] = "Error in JacobianBoundaryTrace_Galerkin<TopoD2>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};
#endif


template<class Surreal>
class JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<Surreal,TopoD3>
{
public:
  typedef TopoD3 TopoDim;
  //----------------------------------------------------------------------------//
  template <class TopologyL,
            class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  RightTopology_Triangle(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {
    // determine topology for R
    const int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      JacobianBoundaryTrace_WakeCut_CG_Potential_Drela_Group<Surreal, Triangle, TopologyL, Tet>(
          fcn, xfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::RightTopology: unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class TopologyL,
            class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  RightTopology_Quad(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {
    // determine topology for R
    const int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      JacobianBoundaryTrace_WakeCut_CG_Potential_Drela_Group<Surreal, Quad, TopologyL, Hex>(
          fcn, xfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::RightTopology: unknown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  LeftTopology_Triangle(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {

    // determine topology for L
    const int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      RightTopology_Triangle<Tet>(
          fcn, xfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  //----------------------------------------------------------------------------//
  template <class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  LeftTopology_Quad(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {

    // determine topology for L
    const int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      RightTopology_Quad<Hex>(
          fcn, xfldTrace, qfld,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
    {
      char msg[] = "Error in JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::LeftTopology: unkown topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  //----------------------------------------------------------------------------//
  template <class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q )
  {
    SANS_ASSERT( mtxGlobalPDE_q.m() == qfld.nDOF() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      // dispatch to determine left topology
      const int boundaryGroup = fcn.boundaryGroup(group);
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle(
            fcn,
            xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup],
            mtxGlobalPDE_q );
      }
      else if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad(
            fcn,
            xfld.template getBoundaryTraceGroup<Quad>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup],
            mtxGlobalPDE_q );
      }
      else
      {
        char msg[] = "Error in JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }

};


}

#endif  // JACOBIANBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_DRELA_H
