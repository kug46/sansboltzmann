// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_DRELA_H
#define RESIDUALBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_DRELA_H

// boundary-trace integral residual functions

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Linearized Incompressible Potential Flow Galerkin Wake boundary-trace integral
//
//  functions dispatched based on left (L) element topologies
//
//  process:
//  (a) loop over groups; dispatch to L (ResidualBoundaryTrace_LeftTopology)
//  (b) dispatch to R (ResidualBoundaryTrace_RightTopology)
//  (c) call base class routine with specific L and R (ResidualBoundaryTrace_Group w/ Base& params)
//  (d) cast to specific L and R and call L/R specific topology routine (ResidualBoundaryTrace_Group_Integral)
//  (e) loop over traces and integrate

template <class TopologyTrace, class TopologyL, class TopologyR,
          class PhysDim, class TopoDim, class ArrayQ, class ArrayQR,
          class IntegrandTrace_Galerkin_WakeCut>
void
ResidualBoundaryTrace_WakeCut_CG_Potential_Drela_Group_Integral(
    const IntegrandTrace_Galerkin_WakeCut& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL>& xfldCellL,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR>& xfldCellR,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR>& qfldCellR,
    int quadratureorder,
    SLA::SparseVector<ArrayQR>& rsdPDEGlobal )
{
  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

  typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
  typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyR> XFieldCellGroupTypeR;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyR> QFieldCellGroupTypeR;

  typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
  typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
  typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

  // element field variables
  ElementXFieldClassL xfldElemL( xfldCellL.basis() );
  ElementQFieldClassL qfldElemL( qfldCellL.basis() );

  ElementXFieldClassR xfldElemR( xfldCellR.basis() );
  ElementQFieldClassR qfldElemR( qfldCellR.basis() );

  ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

  // number of integrals evaluated per element
  int nIntegrandL = qfldElemL.nDOF();
  int nIntegrandR = qfldElemR.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobalL( nIntegrandL );
  std::vector<int> mapDOFGlobalR( nIntegrandR );

  // trace element integral
  GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQR, ArrayQR>
    integral(quadratureorder, nIntegrandL, nIntegrandR);

  // element integrand/residuals
  std::vector<ArrayQR> rsdPDEElemL( nIntegrandL );
  std::vector<ArrayQR> rsdPDEElemR( nIntegrandR );

  // loop over elements within group
  int nelem = xfldTrace.nElem();
  for (int elem = 0; elem < nelem; elem++)
  {
    const int elemL = xfldTrace.getElementLeft( elem );
    const int elemR = xfldTrace.getElementRight( elem );
    CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
    CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

    // copy global grid/solution DOFs to element
    xfldCellL.getElement( xfldElemL, elemL );
    qfldCellL.getElement( qfldElemL, elemL );

    xfldCellR.getElement( xfldElemR, elemR );
    qfldCellR.getElement( qfldElemR, elemR );

    xfldTrace.getElement( xfldElemTrace, elem );

    for (int n = 0; n < nIntegrandL; n++)
      rsdPDEElemL[n] = 0;

    for (int n = 0; n < nIntegrandR; n++)
      rsdPDEElemR[n] = 0;
#if 0
    if (std::find(mapDOFGlobalR.get(), mapDOFGlobalR.get()+nIntegrandR,1485) != mapDOFGlobalR.get()+nIntegrandR)
      nIntegrandL = qfldElemL.nDOF();
#endif
    integral(
        fcn.integrand(xfldElemTrace,
                      canonicalTraceL, canonicalTraceR,
                      xfldElemL, qfldElemL,
                      xfldElemR, qfldElemR ),
        xfldElemTrace,
        rsdPDEElemL.data(), rsdPDEElemL.size(),
        rsdPDEElemR.data(), rsdPDEElemR.size() );

    // scatter-add element residuals to global
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), mapDOFGlobalL.size() );
    qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), mapDOFGlobalR.size() );

    int nGlobal;
    for (int n = 0; n < nIntegrandL; n++)
    {
      nGlobal = mapDOFGlobalL[n];
      //std::cout << "*: n = " << n << "  nGlobal = " << nGlobal << "  dupPointOffset = " << fcn.dupPointOffset << std::endl;
      SANS_ASSERT( nGlobal < fcn.dupPointOffset );
      rsdPDEGlobal[nGlobal] += rsdPDEElemL[n];
    }
    for (int n = 0; n < nIntegrandR; n++)
    {
      nGlobal = mapDOFGlobalR[n];
      if (nGlobal < fcn.dupPointOffset) continue;

      //Zero out the potential equation if a trailing edges point, but don't zero out density for full potential
      if ( std::find(fcn.KuttaPoints.begin(), fcn.KuttaPoints.end(), nGlobal) != fcn.KuttaPoints.end() )
        DLA::index(rsdPDEElemR[n],0) = 0;

      rsdPDEGlobal[nGlobal] += rsdPDEElemR[n];
    }

#if 0
    if (std::find(mapDOFGlobalL.get(), mapDOFGlobalL.get()+nIntegrandL,1485) != mapDOFGlobalL.get()+nIntegrandL)
    {
      bool endLine = false;
      for (int n = 0; n < nIntegrandL; n++)
        if ( fabs(rsdPDEElemL[n]) > 1e-12 )
        {
          std::cout << "rsdPDEElemL[" << mapDOFGlobalL[n] << "]=" << rsdPDEElemL[n] << ", ";
          endLine=true;
        }
      if (endLine) std::cout << std::endl;
    }

    if (std::find(mapDOFGlobalR.get(), mapDOFGlobalR.get()+nIntegrandR,1485) != mapDOFGlobalR.get()+nIntegrandR)
    {
      bool endLine = false;
      for (int n = 0; n < nIntegrandR; n++)
        if ( fabs(rsdPDEElemR[n]) > 1e-12 )
        {
          std::cout << "rsdPDEElemR[" << mapDOFGlobalR[n] << "]=" << rsdPDEElemR[n] << ", ";
          endLine=true;
        }
      if (endLine) std::cout << std::endl;
    }
#endif
  }
}


template <class TopologyTrace, class TopologyL, class TopologyR,
          class IntegrandTrace_Galerkin_WakeCut,
          class PhysDim, class TopoDim, class ArrayQ, class T>
void
ResidualBoundaryTrace_WakeCut_CG_Potential_Drela_Group(
    const IntegrandTrace_Galerkin_WakeCut& fcn,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld,
    const int quadratureorder,
    SLA::SparseVector<T>& rsdPDEGlobal )
{
  const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

  const int groupL = xfldTrace.getGroupLeft();
  const int groupR = xfldTrace.getGroupRight();

  // Integrate over the trace group
  ResidualBoundaryTrace_WakeCut_CG_Potential_Drela_Group_Integral<TopologyTrace, TopologyL, TopologyR, PhysDim, TopoDim, ArrayQ>(
      fcn, xfldTrace,
      xfld.template getCellGroup<TopologyL>(groupL), qfld.template getCellGroup<TopologyL>(groupL),
      xfld.template getCellGroup<TopologyR>(groupR), qfld.template getCellGroup<TopologyR>(groupR),
      quadratureorder,
      rsdPDEGlobal );

}


template<class TopDim>
class ResidualBoundaryTrace_WakeCut_CG_Potential_Drela;


template<>
class ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD2>
{
public:
  typedef TopoD2 TopoDim;
  template <class TopologyTrace, class TopologyL,
            class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ, class T>
  static void
  RightTopology(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Triangle) )
    {
      ResidualBoundaryTrace_WakeCut_CG_Potential_Drela_Group<TopologyTrace, TopologyL, Triangle>(
          fcn, xfldTrace, qfld, quadratureorder, rsdPDEGlobal );
    }
    else if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Quad) )
    {
      ResidualBoundaryTrace_WakeCut_CG_Potential_Drela_Group<TopologyTrace, TopologyL, Quad>(
          fcn, xfldTrace, qfld, quadratureorder, rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class TopologyTrace, class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ, class T>
  static void
  LeftTopology(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Triangle) )
    {
      // determine topology for R
      RightTopology<TopologyTrace, Triangle>( fcn, xfldTrace, qfld, quadratureorder, rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in ResidualBoundaryTrace_Galerkin_LG<TopoD2>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class IntegrandTrace_Galerkin_WakeCut,
  class PhysDim, class ArrayQ, class T>
  static void
  integrate(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {
    SANS_ASSERT( rsdPDEGlobal.m() == qfld.nDOF() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);

      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Line) )
      {
        LeftTopology<Line>( fcn,
            xfld.template getBoundaryTraceGroup<Line>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup], rsdPDEGlobal );
      }
      else
      {
        char msg[] = "Error in ResidualBoundaryTrace_Galerkin_LG<TopoD2>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};



template<>
class ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>
{
public:
  typedef TopoD3 TopoDim;

protected:
  template <class TopologyL,
            class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ, class T>
  static void
  RightTopology_Triangle(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      ResidualBoundaryTrace_WakeCut_CG_Potential_Drela_Group<Triangle, TopologyL, Tet>(
          fcn, xfldTrace, qfld, quadratureorder, rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class TopologyL,
            class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ, class T>
  static void
  RightTopology_Quad(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {
    // determine topology for Right cell group
    int groupR = xfldTrace.getGroupRight();

    // dispatch integration over elements of group
    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      ResidualBoundaryTrace_WakeCut_CG_Potential_Drela_Group<Quad, TopologyL, Hex>(
          fcn, xfldTrace, qfld, quadratureorder, rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  template <class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ, class T>
  static void
  LeftTopology_Triangle(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      RightTopology_Triangle<Tet>( fcn, xfldTrace, qfld, quadratureorder, rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class IntegrandTrace_Galerkin_WakeCut,
            class PhysDim, class ArrayQ, class T>
  static void
  LeftTopology_Quad(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      RightTopology_Quad<Hex>( fcn, xfldTrace, qfld, quadratureorder, rsdPDEGlobal );
    }
    else
    {
      char msg[] = "Error in ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class IntegrandTrace_Galerkin_WakeCut,
  class PhysDim, class ArrayQ, class T>
  static void
  integrate(
      const IntegrandTrace_Galerkin_WakeCut& fcn,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {
    SANS_ASSERT( rsdPDEGlobal.m() == qfld.nDOF() );

    const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

    SANS_ASSERT( ngroup == xfld.nBoundaryTraceGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryGroups(); group++)
    {
      const int boundaryGroup = fcn.boundaryGroup(group);

      if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Triangle) )
      {
        LeftTopology_Triangle( fcn,
            xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup], rsdPDEGlobal );
      }
      else if ( xfld.getBoundaryTraceGroupBase(boundaryGroup).topoTypeID() == typeid(Quad) )
      {
        LeftTopology_Quad( fcn,
            xfld.template getBoundaryTraceGroup<Quad>(boundaryGroup),
            qfld,
            quadratureorder[boundaryGroup], rsdPDEGlobal );
      }
      else
      {
        char msg[] = "Error in ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate: unknown trace topology\n";
        SANS_DEVELOPER_EXCEPTION( msg );
      }
    }
  }
};

}

#endif  // RESIDUALBOUNDARYTRACE_WAKECUT_CG_POTENTIAL_DRELA_H
