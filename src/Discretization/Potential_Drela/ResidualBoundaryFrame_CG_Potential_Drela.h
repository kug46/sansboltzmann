// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ResidualBoundaryFrame_CG_Potential_Drela_H
#define ResidualBoundaryFrame_CG_Potential_Drela_H

// boundary-frame area integral functional (yeah, a 1D topological integral in topological 2D...)

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/XField3D_Wake.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/Element.h"

#include "Field/XFieldVolume.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"

namespace SANS
{
template <class PDE>
class IntegrandBoundaryFrame_CG_Potential_Drela;


template <class TopologyTrace, class TopologyCell,
          class XFieldType,
          class ArrayQ, class ArrayQR,
          class PDE>
void
ResidualBoundaryFrame_CG_Potential_Drela_Group_Integral(
    const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
    const std::map<int,NodeToBCElemMap>& nodeElemMap,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCell>& xfldCell,
    const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCell>& qfldCell,
    const int sgn,
    int quadratureorder,
    SLA::SparseVector<ArrayQR>& rsdPDEGlobal )
{
  typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

  typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );
  ElementQFieldClass qfldElem( qfldCell.basis() );

  ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

  // number of integrals evaluated per element
  int nIntegrand = qfldElem.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobal(nIntegrand);

  // frame element integral
  GalerkinWeightedIntegral<TopoD2, TopologyTrace, ArrayQR> integral(quadratureorder, nIntegrand);

  // element integrand/residuals
  std::vector<ArrayQR> rsdPDEElem( nIntegrand );

  // loop over elements within group
  for (auto map = nodeElemMap.begin(); map != nodeElemMap.end(); map++)
  {
    SANS_ASSERT( map->second.KuttaPoint >= fcn.dupPointOffset);
    int nTraceElemPoint = map->first;
    int nelem = map->second.elems.size();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemTrace = map->second.elems[ elem ];

      xfldTrace.getElement( xfldElemTrace, elemTrace );

      CanonicalTraceToCell& canonicalTrace = xfldTrace.getCanonicalTraceLeft( elemTrace );

      const int elemCell = xfldTrace.getElementLeft( elemTrace );

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elemCell );
      qfldCell.getElement( qfldElem, elemCell );

      for (int n = 0; n < nIntegrand; n++)
        rsdPDEElem[n] = 0;

      integral(
          fcn.integrand(xfldElemTrace,
                        canonicalTrace,
                        xfldElem, qfldElem ),
                        xfldElemTrace,
                        rsdPDEElem.data(), nIntegrand );

      // scatter-add element residuals to global
      qfldCell.associativity( elemCell ).getGlobalMapping( mapDOFGlobal.data(), nIntegrand );

      for (int n = 0; n < nIntegrand; n++)
      {
        if ( mapDOFGlobal[n] != nTraceElemPoint ) continue;
        //if (nGlobal < fcn.dupPointOffset) continue;
        //SANS_ASSERT ( std::find(fcn.KuttaPoints.begin(), fcn.KuttaPoints.end(), nGlobal) != fcn.KuttaPoints.end() );
        rsdPDEGlobal[map->second.KuttaPoint] += sgn*rsdPDEElem[n];
      }
    }
  }
}



template <class TopologyTrace, class TopologyCell,
          class XFieldType,
          class PDE,
          class ArrayQ, class T>
static void
ResidualBoundaryFrame_CG_Potential_Drela_CellGroups(
    const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
    const std::map<int,NodeToBCElemMap>& nodeElemMap,
    const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const Field<PhysD3, TopoD3, ArrayQ>& qfld, const int sgn,
    const int quadratureorder,
    SLA::SparseVector<T>& rsdPDEGlobal )
{
  const XField<PhysD3, TopoD3>& xfld = qfld.getXField();

  int groupCell = xfldTrace.getGroupLeft();

  // Integrate over the trace group
  ResidualBoundaryFrame_CG_Potential_Drela_Group_Integral<TopologyTrace, TopologyCell,
                                                      XFieldType, ArrayQ>( fcn, nodeElemMap,
                                                                           xfldTrace,
                                                                           xfld.template getCellGroup<TopologyCell>(groupCell),
                                                                           qfld.template getCellGroup<TopologyCell>(groupCell),
                                                                           sgn,
                                                                           quadratureorder,
                                                                           rsdPDEGlobal );
}


//----------------------------------------------------------------------------//
// Residual Boundary Frame
//
template<class TopDim>
class ResidualBoundaryFrame_CG_Potential_Drela;

template<>
class ResidualBoundaryFrame_CG_Potential_Drela<TopoD3>
{
protected:
  typedef TopoD3 TopoDim;

//----------------------------------------------------------------------------//
  template <class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ, class T>
  static void
  TopologyCellLeft_Triangle(
      const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
      const std::map<int,NodeToBCElemMap>& nodeElemMap,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld, const int sgn,
      int quadratureorder,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {
    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      ResidualBoundaryFrame_CG_Potential_Drela_CellGroups<Triangle, Tet, XFieldType>(
          fcn, nodeElemMap, xfldTrace, qfld, sgn,
          quadratureorder,
          rsdPDEGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unkown topology" );
  }

  //----------------------------------------------------------------------------//
  template <class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ, class T>
  static void
  TopologyCellLeft_Quad(
      const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
      const std::map<int,NodeToBCElemMap>& nodeElemMap,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld, const int sgn,
      int quadratureorder,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {
    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      ResidualBoundaryFrame_CG_Potential_Drela_CellGroups<Quad, Hex, XFieldType>(
          fcn, nodeElemMap, xfldTrace, qfld, sgn,
          quadratureorder,
          rsdPDEGlobal );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unkown topology" );
  }

public:
  template <class XFieldType, class PDE,
            class PhysDim, class ArrayQ, class T>
  static void
  integrate(
      const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SLA::SparseVector<T>& rsdPDEGlobal )
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );
#if 1
    for (auto it = xfld.KuttaBCTraceElemLeft_.begin(); it != xfld.KuttaBCTraceElemLeft_.end(); it++)
    {
      int boundaryGroupL = it->first;

      // get the boundary group
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Triangle) )
      {
        // dispatch to determine left cell topology
        TopologyCellLeft_Triangle<XFieldType>(
            fcn, it->second,
            xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupL),
            qfld, 1,
            quadratureorder[boundaryGroupL],
            rsdPDEGlobal );
      }
      else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Quad) )
      {
        // dispatch to determine left cell topology
        TopologyCellLeft_Quad<XFieldType>(
            fcn, it->second,
            xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupL),
            qfld, 1,
            quadratureorder[boundaryGroupL],
            rsdPDEGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
#endif
#if 1
    for (auto it = xfld.KuttaBCTraceElemRight_.begin(); it != xfld.KuttaBCTraceElemRight_.end(); it++)
    {
      int boundaryGroupR = it->first;

      // get the boundary group
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Triangle) )
      {
        // dispatch to determine left cell topology
        TopologyCellLeft_Triangle<XFieldType>(
            fcn, it->second,
            xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupR),
            qfld, -1,
            quadratureorder[boundaryGroupR],
            rsdPDEGlobal );
      }
      else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Quad) )
      {
        // dispatch to determine left cell topology
        TopologyCellLeft_Quad<XFieldType>(
            fcn, it->second,
            xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupR),
            qfld, -1,
            quadratureorder[boundaryGroupR],
            rsdPDEGlobal );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
#endif
  }
};

}

#endif  // ResidualBoundaryFrame_CG_Potential_Drela_H
