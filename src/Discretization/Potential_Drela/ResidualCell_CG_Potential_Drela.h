// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALCELL_CG_POTENTIAL_DRELA_H
#define RESIDUALCELL_CG_POTENTIAL_DRELA_H

// Cell integral residual functions

#include <memory> //unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Field Cell group residual
//
// topology specific group residual
//
// template parameters:
//   Topology                          element topology (e.g. Triangle)
//   IntegrandCell_Galerkin            integrand functor
//   XFieldType                        Grid data type, possibly a tuple with parameters

//----------------------------------------------------------------------------//
//  Galerkin cell group integral
//

template<class IntegrandCell, template<class> class Vector, class TR>
class ResidualCell_CG_Potential_Drela_impl :
    public GroupIntegralCellType< ResidualCell_CG_Potential_Drela_impl<IntegrandCell, Vector, TR> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template ArrayQ<TR> ArrayQR;

  // Save off the cell integrand and the residual vector
  ResidualCell_CG_Potential_Drela_impl( const IntegrandCell& fcn,
                                  Vector<ArrayQR>& rsdPDEGlobal, const int dupPointOffset, const std::vector<int>& invPointMap ) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), dupPointOffset_(dupPointOffset), invPointMap_(invPointMap) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template <class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfldCell,
             const int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    // number of integrals evaluated
    const int nIntegrand = qfldElem.nDOF();

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, ArrayQR> integral(quadratureorder, nIntegrand);

    // just to make sure things are consistent
    SANS_ASSERT( xfldCell.nElem() == qfldCell.nElem() );

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal( nIntegrand, -1 );

    // residual array
    std::vector<ArrayQR> rsdPDEElem( nIntegrand );

    // loop over elements within group
    const int nelem = xfldCell.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      for (int n = 0; n < nIntegrand; n++)
        rsdPDEElem[n] = 0;

      // cell integration for canonical element
      integral( fcn_.integrand( xfldElem, qfldElem ), get<-1>(xfldElem), rsdPDEElem.data(), nIntegrand );

      // scatter-add element integral to global
      qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nIntegrand );

      int nGlobal;
      for (int n = 0; n < nIntegrand; n++)
      {
        nGlobal = mapDOFGlobal[n];

        // Apply the Nishida HACK only adding volume integrals to "Left" residuals
        if (nGlobal >= dupPointOffset_)
          nGlobal = invPointMap_[nGlobal-dupPointOffset_];

        rsdPDEGlobal_[nGlobal] += rsdPDEElem[n];
      }
    }
  }

protected:
  const IntegrandCell& fcn_;
  Vector<ArrayQR>& rsdPDEGlobal_;
  const int dupPointOffset_;
  const std::vector<int>& invPointMap_;
};

// Factory function

template<class IntegrandCell, template<class> class Vector, class ArrayQ>
ResidualCell_CG_Potential_Drela_impl<IntegrandCell, Vector, typename Scalar<ArrayQ>::type>
ResidualCell_CG_Potential_Drela( const IntegrandCellType<IntegrandCell>& fcn,
                           Vector<ArrayQ>& rsdPDEGlobal, const int dupPointOffset,
                           const std::vector<int>& invPointMap )
{
  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same<ArrayQ, typename IntegrandCell::template ArrayQ<T> >::value, "These should be the same.");
  return ResidualCell_CG_Potential_Drela_impl<IntegrandCell, Vector, T>(fcn.cast(), rsdPDEGlobal, dupPointOffset, invPointMap );
}


} //namespace SANS

#endif  // RESIDUALCELL_CG_POTENTIAL_DRELA_H
