// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_CG_POTENTIAL_DRELA_H
#define JACOBIANBOUNDARYTRACE_CG_POTENTIAL_DRELA_H

// jacobian boundary-trace integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{


//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral without Lagrange multipliers
//

template<class Surreal, class IntegrandBoundaryTrace, class SparseMatrix>
class JacobianBoundaryTrace_CG_Potential_Drela_impl :
    public GroupIntegralBoundaryTraceType< JacobianBoundaryTrace_CG_Potential_Drela_impl<Surreal, IntegrandBoundaryTrace, SparseMatrix> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandBoundaryTrace::template ArrayQ<Surreal> ArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianBoundaryTrace_CG_Potential_Drela_impl(const IntegrandBoundaryTrace& fcn,
                                                 SparseMatrix& mtxGlobalPDE_q, const int dupPointOffset, const std::vector<int>& invPointMap ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), dupPointOffset_(dupPointOffset), invPointMap_(invPointMap) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass  xfldElemTrace(  xfldTrace.basis() );

    // variables/equations per DOF
    const int nEqn = IntegrandBoundaryTrace::N;

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL);
    std::vector<int> mapDOFGlobalNishida(nDOFL);

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQSurreal> integral(quadratureorder, nDOFL);

    // element integrand/residuals
    std::unique_ptr<ArrayQSurreal[]> rsdPDEElemL( new ArrayQSurreal[nDOFL] );

    // element jacobians
    MatrixElemClass mtxPDEElemL_qElemL(nDOFL, nDOFL);

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxPDEElemL_qElemL = 0;

      // left element
      const int elemL = xfldTrace.getElementLeft( elem );
      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement(  xfldElemTrace, elem );

      // number of simultaneous derivatives per functor call
      const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*(nDOFL); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemL.DOF(j), n).deriv(k) = 0;

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element

        for (int n = 0; n < nDOFL; n++)
          rsdPDEElemL[n] = 0;

        integral(
            fcn_.integrand(xfldElemTrace, canonicalTraceL,
                           xfldElemL, qfldElemL),
            get<-1>(xfldElemTrace),
            rsdPDEElemL.get(), nDOFL );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFL; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElemL_qElemL(i,j), m,n) = DLA::index(rsdPDEElemL[i], m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global

      scatterAdd<TopologyTrace, TopologyL, TopoDim>(
          qfldCellL,
          elemL,
          mapDOFGlobalL.data(), mapDOFGlobalNishida.data(), nDOFL,
          mtxPDEElemL_qElemL,
          mtxGlobalPDE_q_ );
    }
  }

protected:

//----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class TopoDim,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int elemL,
      int mapDOFGlobalL[], int mapDOFGlobalNishida[], const int nDOFL,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
  {
    qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL, nDOFL );

    // Apply the Nishida HACK only adding volume integrals to "Left" residuals
    for (int n = 0; n < nDOFL; n++)
    {
      if (mapDOFGlobalL[n] >= dupPointOffset_)
        mapDOFGlobalNishida[n] = invPointMap_[mapDOFGlobalL[n]-dupPointOffset_];
      else
        mapDOFGlobalNishida[n] = mapDOFGlobalL[n];
    }

    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, mapDOFGlobalNishida, nDOFL, mapDOFGlobalL, nDOFL );
  }


//----------------------------------------------------------------------------//
  template <class TopologyTrace, class TopologyL, class TopoDim,
            template <class> class SparseMatrixType>
  void
  scatterAdd(
      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
      const int elemL,
      int mapDOFGlobalL[], const int nDOFL,
      DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
      SparseMatrixType< DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  SparseMatrix& mtxGlobalPDE_q_;
  const int dupPointOffset_;
  const std::vector<int>& invPointMap_;
};

// Factory function

template<class Surreal, class IntegrandBoundaryTrace, class SparseMatrix>
JacobianBoundaryTrace_CG_Potential_Drela_impl<Surreal, IntegrandBoundaryTrace, SparseMatrix>
JacobianBoundaryTrace_CG_Potential_Drela( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                    SparseMatrix& mtxGlobalPDE_q, const int dupPointOffset,
                                    const std::vector<int>& invPointMap )
{
  return JacobianBoundaryTrace_CG_Potential_Drela_impl<Surreal, IntegrandBoundaryTrace, SparseMatrix>(
      fcn.cast(), mtxGlobalPDE_q, dupPointOffset, invPointMap );
}


}

#endif  // JACOBIANBOUNDARYTRACE_CG_POTENTIAL_DRELA_H
