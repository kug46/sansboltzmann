// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BC_WAKECUT_POTENTIAL_DRELA_H
#define BC_WAKECUT_POTENTIAL_DRELA_H

#include "pde/BCCategory.h"

#include "Python/PyDict.h"
#include "Python/Parameter.h"

namespace SANS
{

struct BC_WakeCut_Potential_DrelaParams : noncopyable
{
  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    d.checkUnknownInputs(allParams);
  }

  static constexpr const char* BCName{"WakeCut_Potential_Drela"};
  struct Option
  {
    const DictOption WakeCut_Potential_Drela{BC_WakeCut_Potential_DrelaParams::BCName, BC_WakeCut_Potential_DrelaParams::checkInputs};
  };

  static BC_WakeCut_Potential_DrelaParams params;
};

// Dummy placeholder BC class for now...
struct BC_WakeCut_Potential_Drela
{
  typedef BCCategory::WakeCut_Potential_Drela Category;

  explicit BC_WakeCut_Potential_Drela( PyDict& d ) {}
};

} // namepspace SANS

#endif //BC_WAKECUT_POTENTIAL_DRELA_H
