// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_GALERKIN_WAKECUT_CG_LIP_DRELA_H
#define INTEGRANDBOUNDARYTRACE_GALERKIN_WAKECUT_CG_LIP_DRELA_H

// cell/trace integrand operators: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"

#include "BC_WakeCut_Potential_Drela.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Trace integrand: Galerkin

template <template<class,class> class NDConvert, class Tg, class NDBCVector>
class IntegrandBoundaryTrace<NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>>,
                             NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin > :
  public IntegrandBoundaryTraceType<IntegrandBoundaryTrace<NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>>,
                                                           NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin> >
{
public:
  typedef NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>> PDE;
  typedef BC_WakeCut_Potential_Drela BC;
  typedef BCCategory::WakeCut_Potential_Drela Category;
  typedef Galerkin DiscTag;

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(const PDE& pde, const std::vector<int>& BoundaryGroups,
                                  const int dupPointOffset, const std::vector<int>& KuttaPoints)
    : dupPointOffset(dupPointOffset), KuttaPoints(KuttaPoints), pde_(pde), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const int dupPointOffset;
  const std::vector<int> KuttaPoints;

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const PDE& pde,
             const ElementXFieldTrace& xfldElemTrace,
             const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldL& xfldElemL,
             const ElementQFieldL& qfldElemL,
             const ElementXFieldR& xfldElemR,
             const ElementQFieldR& qfldElemR ) :
             pde_(pde),
             xfldElemTrace_(xfldElemTrace),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR) {}


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const { return true; }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int nrsdL,
                                                     ArrayQ<Ti> integrandR[], const int nrsdR ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL, TopologyR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell , TopologyL    >& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElemL,
            const ElementXField<PhysDim, TopoDimCell , TopologyR    >& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyR    >& qfldElemR) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>(pde_,
                                                                                      xfldElemTrace,
                                                                                      canonicalTraceL, canonicalTraceR,
                                                                                      xfldElemL, qfldElemL,
                                                                                      xfldElemR, qfldElemR);
  }

protected:
  const PDE& pde_;
  const std::vector<int> BoundaryGroups_;
};


template <template<class,class> class NDConvert, class Tg, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
template <class Ti>
void
IntegrandBoundaryTrace<NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>>,
                             NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin >::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::operator()(
    const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandL[], const int nrsdL,
                                        ArrayQ<Ti> integrandR[], const int nrsdR ) const
{
  typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

  const int nDOFL = qfldElemL_.nDOF();             // total solution DOFs in left element
  const int nDOFR = qfldElemR_.nDOF();             // total solution DOFs in right element

  SANS_ASSERT(nrsdL == nDOFL);
  SANS_ASSERT(nrsdR == nDOFR);

  VectorX X = 0;
  VectorX nL, nR;           // unit normal for left element (points to right element)

  std::vector<Real> phiL(nDOFL);                                   // basis (left)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiL(nDOFL);  // basis gradient (left)
  std::vector<Real> phiR(nDOFR);                                   // basis (right)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiR(nDOFR);  // basis gradient (right)

  ArrayQ<T> qL, qR;           // solution
  VectorArrayQ gradqL, gradqR;  // gradient

  DLA::VectorS< PhysDim::D, Ti > U; // Freestream Velocity

  QuadPointCellType sRefL;              // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, RefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, RefTrace, sRefR );

  // physical coordinates
  xfldElemTrace_.coordinates( RefTrace, X );

  // unit normal: L points to R
  xfldElemTrace_.unitNormal( RefTrace, nL );
  nR = -nL;


  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL.data(), nDOFL );
  qfldElemR_.evalBasis( sRefR, phiR.data(), nDOFR );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL.data(), nDOFL );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR.data(), nDOFR );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL.data(), nDOFL, qL );
  qfldElemR_.evalFromBasis( phiR.data(), nDOFR, qR );
  qfldElemL_.evalFromBasis( gradphiL.data(), nDOFL, gradqL );
  qfldElemR_.evalFromBasis( gradphiR.data(), nDOFR, gradqR );

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] = 0;

  // tangential BC
#if 0
  U = pde_.freestream();

  ArrayQ<T> gradqnL = dot(nL,gradqL);
  ArrayQ<T> gradqnR = dot(nR,gradqR);


  Real h = pow( xfldElemTrace_.jacobianDeterminant(), 1./TopoDimTrace::D );

  Real UnL = dot(nL,U); // Normal velocity
  Real UnR = dot(nR,U); // Normal velocity

  DLA::VectorS< PhysDim::D, Real > Us = U - UnL*nL; // Tangential velocity
  Real V = sqrt( dot(Us, Us) );
  Us /= V;

  ArrayQ<T> gradqNsum = gradqnL + gradqnR;

  // Compute the surface tangent gradient
  gradqL -= gradqnL*nL;
  gradqR -= gradqnR*nR;

  ArrayQ<T> Ugradqsum = dot(Us,gradqL) + dot(Us,gradqR);
  ArrayQ<T> Ugradqjump = dot(Us,gradqL) - dot(Us,gradqR);

  Real eps = 0.5;

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += 0.5*phiL[k]*gradqNsum - phiL[k]*gradqnL
                   + 0.5*phiL[k]*Ugradqsum
                   + 0.5*phiL[k]*Ugradqjump;
//                   + 0.5*(phiL[k] + eps*h/V*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*Ugradqjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += 0.5*phiR[k]*gradqNsum - phiR[k]*gradqnR
                   + 0.5*phiR[k]*Ugradqsum
                   - 0.5*phiR[k]*Ugradqjump;
//                   - 0.5*(phiR[k] + eps*h/V*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*Ugradqjump;
#endif

  // zero linearized tangential pressure jump
#if 1
  //U = pde_.freestream();
  U = pde_.Up(X);

  // Compute the surface tangent gradient
  gradqL -= dot(nL,gradqL)*nL;
  gradqR -= dot(nR,gradqR)*nR;

  Real h = pow( xfldElemTrace_.jacobianDeterminant(), 1./TopoDimTrace::D );

  Ti Un = dot(nL,U); // Normal velocity

  DLA::VectorS< PhysDim::D, Ti > Us = U - Un*nL; // Tangential velocity
  Ti V = sqrt( dot(Us, Us) );

  ArrayQ<Ti> Ugradqjump = dot(Us,gradqR) - dot(Us,gradqL);

  Real eps = 0.5;
  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += (phiR[k] + eps*h/V*(dot(U,gradphiR[k]) - Un*dot(nL,gradphiR[k])) )*Ugradqjump;
#endif

  // zero tangential pressure jump with average velocity
#if 0
  U = pde_.Up(X);

  typedef DLA::VectorS< PhysDim::D, Ti > VectorT;

  VectorT VL = U + gradqL;
  VectorT VR = U + gradqR;
  VectorT Vavg = (VL + VR)/2.;

  Ti Vn = dot(nL,Vavg); // Normal velocity
  DLA::VectorS< PhysDim::D, Ti > Vs = Vavg - Vn*nL; // Tangential velocity
  Ti Vm = sqrt( dot(Vs, Vs) );

  // Compute the surface tangent velocity
  VL -= dot(nL,VL)*nL;
  VR -= dot(nR,VR)*nR;

  Real h = pow( xfldElemTrace_.jacobianDeterminant(), 1./TopoDimTrace::D );

  ArrayQ<Ti> V2jump = dot(VL,VL) - dot(VR,VR);

  Real eps = 0.5;
  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += (phiR[k] + eps*h/Vm*(dot(Vavg,gradphiR[k]) - Vn*dot(nL,gradphiR[k])) )*V2jump;
#endif

  // zero pressure jump
#if 0
  U = pde_.freestream();

  Real h = pow( xfldElemTrace_.jacobianDeterminant(), 1./TopoDimTrace::D );

  Ti Un = dot(nL,U); // Normal velocity

  DLA::VectorS< PhysDim::D, Ti > Us = U - Un*nL; // Tangential velocity
  Ti V = sqrt( dot(Us, Us) );

  DLA::VectorS< PhysDim::D, Ti > VR = gradqR + U;
  DLA::VectorS< PhysDim::D, Ti > VL = gradqL + U;

  VL -= dot(nL,VL)*nL;
  VR -= dot(nR,VR)*nR;

  ArrayQ<Ti> Pjump = dot(VR,VR) - dot(VL,VL);

  Real eps = 0.5;
  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += (phiR[k] + eps*h/V*(dot(U,gradphiR[k]) - Un*dot(nL,gradphiR[k])) )*Pjump;
#endif

  // mass conservation Exact BC (The PDE flux should be turned off for this one)
#if 0
  EllipticPotential exact;

  exact( {X[0], X[1], X[2]-nL[2]*1e-12}, gradqL);
  exact( {X[0], X[1], X[2]+nL[2]*1e-12}, gradqR);

  ArrayQ<T> qnL = dot(nL,gradqL);
  ArrayQ<T> qnR = dot(nR,gradqR);

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] -= phiL[k]*qnL;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] -= phiR[k]*qnR;
#endif

  // zero pressure jump ala David Eller
#if 0
  pde_.freestream(U);

  VectorArrayQ gradqjump = gradqR - gradqL;
  ArrayQ<T> Ugradqjump = dot(U,gradqjump);

  //for (int k = 0; k < nDOFL; k++)
  //  integrandL[k] += -0.5*dot(U,gradphiL[k])*Ugradqjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += dot(U,gradphiR[k])*Ugradqjump;
#endif

  // zero pressure jump ala Brian Nishida (without stabilization)
#if 0
  pde_.freestream(U);

  VectorArrayQ gradqjump = gradqR - gradqL;
  ArrayQ<T> Ugradqjump = dot(U,gradqjump);

  //for (int k = 0; k < nDOFL; k++)
  //  integrandL[k] += phiL[k]*Ugradqjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += phiR[k]*Ugradqjump;
#endif

  // zero pressure jump ala Steve Allmaras
#if 0
  U = pde_.freestream();

  VectorArrayQ gradqjump = gradqR - gradqL;
  ArrayQ<T> Ugradqjump = dot(U,gradqjump);
  //Ugradqjump += 0.5*(dot(gradqR,gradqR) - dot(gradqL,gradqL));

  Real h = pow( xfldElemTrace_.jacobianDeterminant(), 1./TopoDimTrace::D );

  Real Un = dot(nR,U); // Normal velocity

  DLA::VectorS< PhysDim::D, Real > Us = U - Un*nR; // Tangential velocity
  Real V = sqrt( dot(Us, Us) );

  Real eps = 0.5;
  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += (phiR[k] + eps*h/V*(dot(U,gradphiR[k]) - Un*dot(nR,gradphiR[k])) )*Ugradqjump;

#endif

// Zero jump
#if 0
  ArrayQ<T> qjump = qR - qL;

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] += phiL[k]*qjump;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += phiR[k]*qjump;
#endif

  //Exact potential jump and mass flux
#if 0
  EllipticPotential exact;
/*
  DLA::VectorS< PhysDim::D, Real> gradgL, gradgR;  // exact gradient

  exact( {X[0], X[1], X[2]-nL[2]*1e-11}, gradgL);
  exact( {X[0], X[1], X[2]+nL[2]*1e-11}, gradgR);

  Real gnL = dot(nL,gradgL);
  Real gnR = dot(nR,gradgR);

  Real gnsum = gnR + gnL;
*/
  Real gL = exact( {X[0], X[1], X[2]-nL[2]*1e-11});
  Real gR = exact( {X[0], X[1], X[2]+nL[2]*1e-11});

  ArrayQ<T> G = (qR - qL) - (gR - gL);

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] +=  0.5*dot(nL,gradphiL[k])*G;// + 0.5*phiL[k]*gnsum;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += -0.5*dot(nR,gradphiR[k])*G;// + 0.5*phiR[k]*gnsum;
#endif

}

}

#endif  // INTEGRANDBOUNDARYTRACE_GALERKIN_WAKECUT_CG_LIP_DRELA_H
