// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYTRACE_WAKECUT_CG_FP_DRELA_H
#define INTEGRANDBOUNDARYTRACE_WAKECUT_CG_FP_DRELA_H

// cell/trace integrand operators: Galerkin

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"
#include "Field/Element/TraceUnitNormal.h"

#include "pde/BCCategory.h"

#include "BasisFunction/TraceToCellRefCoord.h"

#include "Discretization/Integrand_Type.h"

#include "pde/FullPotential/PDEFullPotential3D.h"

#include "Discretization/Galerkin/Integrand_Galerkin_fwd.h"

#include "BC_WakeCut_Potential_Drela.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Trace integrand: Galerkin


template <template<class,class> class NDConvert, class Tg, class NDBCVector>
class IntegrandBoundaryTrace<NDConvert<PhysD3,PDEFullPotential3D<Tg>>,
                             NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin > :
  public IntegrandBoundaryTraceType<IntegrandBoundaryTrace<NDConvert<PhysD3,PDEFullPotential3D<Tg>>,
                                                           NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin> >
{
public:
  typedef NDConvert<PhysD3,PDEFullPotential3D<Tg>> PDE;
  typedef BC_WakeCut_Potential_Drela BC;
  typedef typename BC::Category Category;
  typedef Galerkin DiscTag;
  static_assert(std::is_same<Category, BCCategory::WakeCut_Potential_Drela>::value, "The BC has the wrong category!" );

  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;     // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryTrace(const PDE& pde, const std::vector<int>& BoundaryGroups,
                                  const int dupPointOffset, const std::vector<int>& KuttaPoints)
    : dupPointOffset(dupPointOffset), KuttaPoints(KuttaPoints), pde_(pde), BoundaryGroups_(BoundaryGroups) {}

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }
  std::vector<int> getBoundaryGroups() const { return BoundaryGroups_; }

  const int dupPointOffset;
  const std::vector<int> KuttaPoints;

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyL    > ElementXFieldL;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyR    > ElementXFieldR;

    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimCell, TopologyR> ElementQFieldR;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const PDE& pde,
             const ElementXFieldTrace& xfldElemTrace,
             const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldL& xfldElemL,
             const ElementQFieldL& qfldElemL,
             const ElementXFieldR& xfldElemR,
             const ElementQFieldR& qfldElemR ) :
             pde_(pde),
             xfldElemTrace_(xfldElemTrace),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR) {}


    // check whether integrand needs to be evaluated
    bool needsEvaluation() const
    {
      return true;
    }

    // total PDE equations
    int nEqn() const { return PDE::N; }

    // total DOFs
    int nDOFLeft() const { return qfldElemL_.nDOF(); }
    int nDOFRight() const { return qfldElemR_.nDOF(); }

    // element trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRef, ArrayQ<Ti> integrandL[], const int nrsdL,
                                                     ArrayQ<Ti> integrandR[], const int nrsdR ) const;

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldL& xfldElemL_;
    const ElementQFieldL& qfldElemL_;
    const ElementXFieldR& xfldElemR_;
    const ElementQFieldR& qfldElemR_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class TopologyR>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL, TopologyR>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTraceL, const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysDim, TopoDimCell , TopologyL    >& xfldElemL,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyL    >& qfldElemL,
            const ElementXField<PhysDim, TopoDimCell , TopologyR    >& xfldElemR,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyR    >& qfldElemR) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>(pde_,
                                                                                      xfldElemTrace,
                                                                                      canonicalTraceL, canonicalTraceR,
                                                                                      xfldElemL, qfldElemL,
                                                                                      xfldElemR, qfldElemR);
  }

protected:
  const PDE& pde_;
  const std::vector<int> BoundaryGroups_;
};


template <template<class,class> class NDConvert, class Tg, class NDBCVector>
template<class T, class TopoDimTrace, class TopologyTrace,
                  class TopoDimCell, class TopologyL, class TopologyR>
template<class Ti>
void
IntegrandBoundaryTrace<NDConvert<PhysD3,PDEFullPotential3D<Tg>>, NDVectorCategory<NDBCVector, BCCategory::WakeCut_Potential_Drela>, Galerkin>::
Functor<T, TopoDimTrace, TopologyTrace, TopoDimCell, TopologyL, TopologyR>::operator()(
    const QuadPointTraceType& RefTrace, ArrayQ<Ti> integrandL[], const int nrsdL,
                                        ArrayQ<Ti> integrandR[], const int nrsdR ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;
  typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;
  typedef DLA::VectorS< PhysDim::D, Ts > VectorT;

  const int nDOFL = qfldElemL_.nDOF();             // total solution DOFs in left element
  const int nDOFR = qfldElemR_.nDOF();             // total solution DOFs in right element

  SANS_ASSERT(nrsdL == nDOFL);
  SANS_ASSERT(nrsdR == nDOFR);

  VectorX X = 0;
  VectorX nL, nR;           // unit normal for left element (points to right element)

  std::vector<Real> phiL(nDOFL);                                   // basis (left)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiL(nDOFL);  // basis gradient (left)
  std::vector<Real> phiR(nDOFR);                                   // basis (right)
  std::vector< DLA::VectorS< PhysDim::D, Real> > gradphiR(nDOFR);  // basis gradient (right)

  ArrayQ<T> qL, qR;           // solution
  VectorArrayQ gradqL, gradqR;  // gradient
  VectorT gradPhiL, gradPhiR;

  DLA::VectorS< PhysDim::D, Tg > U; // Freestream Velocity

  QuadPointCellType sRefL;              // reference-element coordinates (s,t)
  QuadPointCellType sRefR;

  // left/right reference-element coords
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTraceL_, RefTrace, sRefL );
  TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyR>::eval( canonicalTraceR_, RefTrace, sRefR );

  // physical coordinates
  xfldElemTrace_.coordinates( RefTrace, X );

  // unit normal: L points to R
  xfldElemTrace_.unitNormal( RefTrace, nL );
  nR = -nL;


  // basis value, gradient
  qfldElemL_.evalBasis( sRefL, phiL.data(), nDOFL );
  qfldElemR_.evalBasis( sRefR, phiR.data(), nDOFR );
  xfldElemL_.evalBasisGradient( sRefL, qfldElemL_, gradphiL.data(), nDOFL );
  xfldElemR_.evalBasisGradient( sRefR, qfldElemR_, gradphiR.data(), nDOFR );

  // solution value, gradient
  qfldElemL_.evalFromBasis( phiL.data(), nDOFL, qL );
  qfldElemR_.evalFromBasis( phiR.data(), nDOFR, qR );
  qfldElemL_.evalFromBasis( gradphiL.data(), nDOFL, gradqL );
  qfldElemR_.evalFromBasis( gradphiR.data(), nDOFR, gradqR );

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] = 0;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] = 0;

  for (int d = 0; d < PhysDim::D; d++)
  {
    gradPhiL[d] = gradqL[d][0];
    gradPhiR[d] = gradqR[d][0];
  }

  // zero tangential pressure jump
#if 1
  U = pde_.freestream();

  //const T& rhoL = qL[1];
  //const T& rhoR = qR[1];
  Ti rhoL = pde_.density(qL);
  Ti rhoR = pde_.density(qR);

  // Compute the surface tangent gradient
  VectorT VL = gradPhiL + U;
  VectorT VR = gradPhiR + U;

  VL -= dot(nL,VL)*nL;
  VR -= dot(nR,VR)*nR;

  VectorT Vavg = (VL + VR)/2.;

  //Ts pL = pde_.pressure(VL);
  //Ts pR = pde_.pressure(VR);

  Real h = pow( xfldElemTrace_.jacobianDeterminant(), 1./TopoDimTrace::D );

  //Tg Un = dot(nL,U); // Normal velocity

  //DLA::VectorS< PhysDim::D, Tg > Us = U - Un*nL; // Tangential velocity
  //Tg V = sqrt( dot(Us, Us) );

  Ti Vn = dot(nL,Vavg); // Normal velocity
  DLA::VectorS< PhysDim::D, Ti > Vs = Vavg - Vn*nL; // Tangential velocity
  Ts Vm = sqrt( dot(Vs, Vs) );

  //Ts pjump = pR - pL;

  Ts V2jump = dot(VL,VL) - dot(VR,VR);
  //Ts V2jump = dot(Us,VL) - dot(Us,VR);

  Real eps = 0.5;
  for (int k = 0; k < nDOFR; k++)
  {
    //integrandR[k][0] += (phiR[k] + eps*h/V*(dot(U,gradphiR[k]) - Un*dot(nL,gradphiR[k])) )*pjump;
    //integrandR[k][0] += (phiR[k] + eps*h/V*(dot(U,gradphiR[k]) - Un*dot(nL,gradphiR[k])) )*V2jump;

    integrandR[k][0] += (phiR[k] + eps*h/Vm*(dot(Vavg,gradphiR[k]) - Vn*dot(nL,gradphiR[k])) )*V2jump;

    //DLA::VectorS< PhysDim::D, Real> gradphis = gradphiR[k] - nL*dot(nL,gradphiR[k]);
    //integrandR[k][0] += (phiR[k] + eps*h/V*dot(Vavg,gradphis))*V2jump;

    integrandR[k][1] += phiR[k]*(rhoL - rhoR);
  }

#endif

  //Exact potential jump and mass flux
#if 0
  EllipticPotential exact;
/*
  DLA::VectorS< PhysDim::D, Real> gradgL, gradgR;  // exact gradient

  exact( {X[0], X[1], X[2]-nL[2]*1e-11}, gradgL);
  exact( {X[0], X[1], X[2]+nL[2]*1e-11}, gradgR);

  Real gnL = dot(nL,gradgL);
  Real gnR = dot(nR,gradgR);

  Real gnsum = gnR + gnL;
*/
  Real gL = exact( {X[0], X[1], X[2]-nL[2]*1e-11});
  Real gR = exact( {X[0], X[1], X[2]+nL[2]*1e-11});

  ArrayQ<T> G = (qR - qL) - (gR - gL);

  for (int k = 0; k < nDOFL; k++)
    integrandL[k] +=  0.5*dot(nL,gradphiL[k])*G;// + 0.5*phiL[k]*gnsum;

  for (int k = 0; k < nDOFR; k++)
    integrandR[k] += -0.5*dot(nR,gradphiR[k])*G;// + 0.5*phiR[k]*gnsum;
#endif

}

}

#endif  // INTEGRANDBOUNDARYTRACE_WAKECUT_CG_FP_DRELA_H
