// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANBOUNDARYTRACE_DISPATCH_CG_POTENTIAL_DRELA_H
#define JACOBIANBOUNDARYTRACE_DISPATCH_CG_POTENTIAL_DRELA_H

// boundary-trace integral jacobian functions

//#include "JacobianBoundaryTrace_mitLG_Galerkin.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_CG_Potential_Drela.h"

//#include "Discretization/IntegrateBoundaryTraceGroups_FieldTrace.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

namespace SANS
{

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's with Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
class JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_LIP_impl
{
public:
  JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_LIP_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int* quadratureorder, int ngroup,
      SparseMatrix& mtxGlobalPDE_q,
      SparseMatrix& mtxGlobalPDE_lg,
      SparseMatrix& mtxGlobalBC_q,
      SparseMatrix& mtxGlobalBC_lg )
    : xfld_(xfld), qfld_(qfld), lgfld_(lgfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_q_(mtxGlobalPDE_q), mtxGlobalPDE_lg_(mtxGlobalPDE_lg), mtxGlobalBC_q_(mtxGlobalBC_q), mtxGlobalBC_lg_(mtxGlobalBC_lg)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented yet...");
    //IntegrateBoundaryTraceGroups_FieldTrace<TopoDim>::integrate(
    //    JacobianBoundaryTrace_mitLG_Galerkin<Surreal>(fcn, mtxGlobalPDE_q_, mtxGlobalPDE_lg_, mtxGlobalBC_q_, mtxGlobalBC_lg_),
    //    xfld_, qfld_, lgfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const int* quadratureorder_;
  const int ngroup_;
  SparseMatrix& mtxGlobalPDE_q_;
  SparseMatrix& mtxGlobalPDE_lg_;
  SparseMatrix& mtxGlobalBC_q_;
  SparseMatrix& mtxGlobalBC_lg_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_LIP_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, SparseMatrix>
JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_LIP(const XFieldType& xfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                                                  const int* quadratureorder, int ngroup,
                                                  SparseMatrix& mtxGlobalPDE_q,
                                                  SparseMatrix& mtxGlobalPDE_lg,
                                                  SparseMatrix& mtxGlobalBC_q,
                                                  SparseMatrix& mtxGlobalBC_lg )
{
  return {xfld, qfld, lgfld, quadratureorder, ngroup, mtxGlobalPDE_q, mtxGlobalPDE_lg, mtxGlobalBC_q, mtxGlobalBC_lg};
}

//---------------------------------------------------------------------------//
//
// Dispatch class for BC's without Lagrange multipliers
//
//---------------------------------------------------------------------------//
template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
class JacobianBoundaryTrace_Dispatch_CG_Potential_Drela_impl
{
public:
  JacobianBoundaryTrace_Dispatch_CG_Potential_Drela_impl(
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int* quadratureorder, int ngroup,
      SparseMatrix& mtxGlobalPDE_q )
    : xfld_(xfld), qfld_(qfld), quadratureorder_(quadratureorder), ngroup_(ngroup),
      mtxGlobalPDE_q_(mtxGlobalPDE_q)
  {}

  template<class IntegrandBoundaryTrace>
  void operator()(const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn)
  {
    IntegrateBoundaryTraceGroups<TopoDim>::integrate(
        JacobianBoundaryTrace_CG_Potential_Drela<Surreal>(fcn, mtxGlobalPDE_q_, get<-1>(xfld_).dupPointOffset_, get<-1>(xfld_).invPointMap_),
        xfld_, qfld_, quadratureorder_, ngroup_ );
  }

protected:
  const XFieldType& xfld_;
  const Field<PhysDim, TopoDim, ArrayQ>& qfld_;
  const int* quadratureorder_;
  const int ngroup_;
  SparseMatrix& mtxGlobalPDE_q_;
};

// Factory function

template<class Surreal, class XFieldType, class PhysDim, class TopoDim, class ArrayQ, class SparseMatrix>
JacobianBoundaryTrace_Dispatch_CG_Potential_Drela_impl<Surreal, XFieldType, PhysDim, TopoDim, ArrayQ, SparseMatrix>
JacobianBoundaryTrace_Dispatch_CG_Potential_Drela(const XFieldType& xfld,
                                                  const Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                  const int* quadratureorder, int ngroup,
                                                  SparseMatrix& mtxGlobalPDE_q)
{
  return {xfld, qfld, quadratureorder, ngroup, mtxGlobalPDE_q};
}


}

#endif //JACOBIANBOUNDARYTRACE_DISPATCH_CG_POTENTIAL_DRELA_H
