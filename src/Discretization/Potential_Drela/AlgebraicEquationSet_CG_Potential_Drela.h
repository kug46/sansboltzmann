// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ALGEBRAICEQUATIONSET_CG_POTENTIAL_DRELA_H
#define ALGEBRAICEQUATIONSET_CG_POTENTIAL_DRELA_H

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Transpose.h"

#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "Discretization/AlgebraicEquationSet_Debug.h"
#include "Discretization/IntegrateCellGroups.h"


#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Potential_Drela/ResidualCell_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianCell_CG_Potential_Drela.h"

#include "Discretization/Potential_Drela/ResidualBoundaryTrace_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_CG_Potential_Drela.h"

#include "Discretization/Potential_Drela/ResidualBoundaryFrame_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryFrame_CG_Potential_Drela.h"

#include "Discretization/Potential_Drela/IntegrandBoundaryTrace_WakeCut_CG_LIP_Drela.h"
#include "Discretization/Potential_Drela/ResidualBoundaryTrace_WakeCut_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_WakeCut_CG_Potential_Drela.h"

#include "Discretization/Potential_Drela/ResidualBoundaryTrace_Dispatch_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/JacobianBoundaryTrace_Dispatch_CG_Potential_Drela.h"

#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"
#include "Discretization/Galerkin/Stabilization_Nitsche.h"


#include "Discretization/isValidState/isValidStateCell.h"
#include "Discretization/isValidState/isValidStateBoundaryTrace_Dispatch.h"

namespace SANS
{

// Forward declare
template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;


//----------------------------------------------------------------------------//
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
class AlgebraicEquationSet_CG_Potential_Drela : public AlgebraicEquationSet_Debug<NDPDEClass, Traits>
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template MatrixQ<Real> MatrixQ;

  typedef AlgebraicEquationSetTraits<MatrixQ, ArrayQ, Traits> TraitsType;

  typedef AlgebraicEquationSet_Debug<NDPDEClass, Traits> DebugBaseType;
  typedef typename TraitsType::AlgebraicEquationSetBaseClass BaseType;

  typedef typename TraitsType::VectorSizeClass VectorSizeClass;
  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemVector SystemVector;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemVectorView SystemVectorView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef typename BaseType::LinesearchData LinesearchData;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandCellClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;
  //TODO: disc tag to be updated

  typedef IntegrandBoundaryTrace<NDPDEClass, NDVectorCategory<boost::mpl::vector1<BC_WakeCut_Potential_Drela>, BCCategory::WakeCut_Potential_Drela>,
                                 Galerkin> IntegrandWake;
  typedef IntegrandBoundaryFrame_CG_Potential_Drela<NDPDEClass> IntegrandKutta;

  template< class... BCArgs >
  AlgebraicEquationSet_CG_Potential_Drela(const XFieldType& xfld,
                          Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                          Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                          const NDPDEClass& pde,
                          const StabilizationNitsche& stab,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& WakeGroups,
                          PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args )
   : DebugBaseType(pde, {1e-12, 1e-12}),
     stab_(stab),
     fcnCell_(pde, CellGroups),
     fcnWake_(pde, WakeGroups, get<-1>(xfld).dupPointOffset_, get<-1>(xfld).KuttaPoints_),
     fcnKutta_(pde, get<-1>(xfld).dupPointOffset_, get<-1>(xfld).KuttaPoints_),
     BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)),
     dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab_),
     xfld_(xfld), qfld_(qfld), lgfld_(lgfld), pde_(pde)
  {
    quadratureOrder_.resize(std::max(std::max(xfld.nCellGroups(),
                                              xfld.getXField().nBoundaryTraceGroups()),
                                              get<-1>(xfld).nBoundaryFrameGroups()), 2);
    quadratureOrderMin_.resize(quadratureOrder_.size(), 0);
  }

  virtual ~AlgebraicEquationSet_CG_Potential_Drela() {}

  using BaseType::residual;
  using BaseType::jacobian;
  using BaseType::jacobianTranspose;

  //Computes the residual
  virtual void residual(SystemVectorView& rsd) override;

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override { this->template jacobian<        SystemMatrixView&>(mtx, quadratureOrder_ );    }
  virtual void jacobian(SystemNonZeroPatternView& nz) override { this->template jacobian<SystemNonZeroPatternView&>( nz, quadratureOrderMin_ ); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobianTranspose(SystemMatrixView& mtxT       ) override { jacobian(Transpose(mtxT), quadratureOrder_ );    }
  virtual void jacobianTranspose(SystemNonZeroPatternView& nzT) override { jacobian(Transpose( nzT), quadratureOrderMin_ ); }


  //Evaluate Residual Norm
  virtual std::vector<std::vector<Real>> residualNorm( const SystemVectorView& rsd ) const override
  {
    const int nDOFPDE = qfld_.nDOF();
    const int nDOFBC = lgfld_.nDOF();
    const int nMon = pde_.nMonitor();

    DLA::VectorD<Real> rsdPDEtmp(nMon);
    DLA::VectorD<Real> rsdBCtmp(nMon);

    rsdPDEtmp = 0;
    rsdBCtmp = 0;

    std::vector<std::vector<Real>> rsdNorm(2, std::vector<Real>(nMon, 0));

    //compute residual norm
    //HACKED Simple L2 norm
    //TODO: Allow for non-L2 norms
    for (int n = 0; n < nDOFPDE; n++)
    {
      pde_.interpResidVariable(rsd[iPDE][n], rsdPDEtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iPDE][j] += pow(rsdPDEtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iPDE][j] = sqrt(rsdNorm[iPDE][j]);

    //BC residual
    for (int n = 0; n < nDOFBC; n++)
    {
      pde_.interpResidBC(rsd[iBC][n], rsdBCtmp);

      for (int j = 0; j < nMon; j++)
        rsdNorm[iBC][j] += pow(rsdBCtmp[j],2);
    }

    for (int j = 0; j < nMon; j++)
      rsdNorm[iBC][j] = sqrt(rsdNorm[iBC][j]);

    return rsdNorm;

  }

  //provides info about the residuals
  virtual void residualInfo(std::vector<std::string>& titles, std::vector<int>& idx) const override
  {
    titles = {"PDE : ",
              "BC  : "};
    idx = {iPDE, iBC};
  }

  //Translates the system vector into a solution field
  virtual void setSolutionField(const SystemVectorView& q) override;

  //Translates the solution field into a system vector
  virtual void fillSystemVector(SystemVectorView& q) const override;

  // Returns the vector an matrix sizes needed for the linear algebra system
  virtual VectorSizeClass vectorEqSize() const override;    // vector for equations (rows in matrixSize)
  virtual VectorSizeClass vectorStateSize() const override; // vector for state DOFs (columns in matrixSize)
  virtual MatrixSizeClass matrixSize() const override;

  // Gives the PDE and solution indices in the system
  virtual int indexPDE() const override { return iPDE; }
  virtual int indexQ() const override { return iq; }

  // Checks to see if proposed solution is physical
  virtual bool isValidStateSystemVector(SystemVectorView& q) override;

  // Returns the side of the residual norm outer vector
  virtual int nResidNorm() const override;

  // Converts processor local indexing to a processor continuous indexing
  virtual std::vector<GlobalContinuousMap> continuousGlobalMap() const override
  {
    return {qfld_.continuousGlobalMap(0, lgfld_.nDOFpossessed()),
            lgfld_.continuousGlobalMap(qfld_.nDOFpossessed(), 0)};
  }

  // MPI communicator for this algebraic equation set
  virtual std::shared_ptr<mpi::communicator> comm() const override { return xfld_.comm(); }

  virtual void syncDOFs_MPI() override
  {
    qfld_.syncDOFs_MPI_Cached();
    lgfld_.syncDOFs_MPI_Cached();
  }

  // Dump the localized linesearch parameter info (for debugging purposes)
  virtual void dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                                       const LinesearchData& pStepData) const override;

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iBC = 1;
  static const int iq = 0;
  static const int ilg = 1;

protected:
  template<class SparseMatrixType>
  void jacobian(SparseMatrixType mtx, const std::vector<int>& quadratureOrder );

  const StabilizationNitsche& stab_;
  IntegrandCellClass fcnCell_;
  IntegrandWake fcnWake_;
  IntegrandKutta fcnKutta_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  const XFieldType& xfld_;
  Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const NDPDEClass& pde_;

  std::vector<int> quadratureOrder_;
  std::vector<int> quadratureOrderMin_;
};

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residual(SystemVectorView& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate(
      ResidualCell_CG_Potential_Drela(fcnCell_, rsd(iq), get<-1>(xfld_).dupPointOffset_, get<-1>(xfld_).invPointMap_),
      xfld_, qfld_, &quadratureOrder_[0], xfld_.nCellGroups() );

  ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake_, qfld_,
                                                                       &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(), rsd(iq) );

  ResidualBoundaryFrame_CG_Potential_Drela<TopoD3>::integrate( fcnKutta_,
                                                           get<-1>(xfld_), qfld_, &quadratureOrder_[0],
                                                           get<-1>(xfld_).nBoundaryFrameGroups(), rsd(iq) );

  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_LIP( xfld_, qfld_, lgfld_,
                                                              &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                              rsd(iq), rsd(ilg) ),
      ResidualBoundaryTrace_Dispatch_CG_Potential_Drela( xfld_, qfld_,
                                                   &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                   rsd(iq) ) );
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
template<class SparseMatrixType>
void
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
jacobian(SparseMatrixType jac, const std::vector<int>& quadratureOrder)
{
  SANS_ASSERT(jac.m() == 2);
  SANS_ASSERT(jac.n() == 2);

  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacPDE_q  = jac(iPDE,iq);
  Matrix jacPDE_lg = jac(iPDE,ilg);
  Matrix jacBC_q   = jac(iBC,iq);
  Matrix jacBC_lg  = jac(iBC,ilg);

  typedef SurrealS<NDPDEClass::N> SurrealClass;

  IntegrateCellGroups<TopoDim>::integrate( JacobianCell_CG_Potential_Drela<SurrealClass>(fcnCell_, jacPDE_q,
                                                                                   get<-1>(xfld_).dupPointOffset_, get<-1>(xfld_).invPointMap_),
                                           xfld_, qfld_, &quadratureOrder[0], xfld_.nCellGroups() );

  JacobianBoundaryTrace_WakeCut_CG_Potential_Drela<SurrealClass,TopoD3>::integrate( fcnWake_, qfld_, &quadratureOrder[0],
                                                                                 xfld_.getXField().nBoundaryTraceGroups(),
                                                                                 jacPDE_q );

  JacobianBoundaryFrame_CG_Potential_Drela<SurrealClass,TopoD3>::integrate(fcnKutta_, get<-1>(xfld_), qfld_, &quadratureOrder[0],
                                                                       get<-1>(xfld_).nBoundaryFrameGroups(),
                                                                       jacPDE_q );

  dispatchBC_.dispatch(
      JacobianBoundaryTrace_mitLG_Dispatch_Galerkin_LIP<SurrealClass>( xfld_, qfld_, lgfld_,
                                                                       &quadratureOrder[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                                       jacPDE_q, jacPDE_lg, jacBC_q, jacBC_lg ),
      JacobianBoundaryTrace_Dispatch_CG_Potential_Drela<SurrealClass>( xfld_, qfld_,
                                                                        &quadratureOrder[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                                        jacPDE_q ) );
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::setSolutionField(const SystemVectorView& q)
{
  // Copy the solution from the linear algebra vector to the field variables
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    qfld_.DOF(k) = q[iq][k];

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    lgfld_.DOF(k) = q[ilg][k];
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::fillSystemVector(SystemVectorView& q) const
{
  // Copy the solution from the field variables to the linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  SANS_ASSERT( nDOFPDE == q[iq].m() );
  for (int k = 0; k < nDOFPDE; k++)
    q[iq][k] = qfld_.DOF(k);

  const int nDOFBC = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();
  SANS_ASSERT( nDOFBC == q[ilg].m() );
  for (int k = 0; k < nDOFBC; k++)
    q[ilg][k] = lgfld_.DOF(k);
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorEqSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the equations linear algebra vector
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  return {nDOFPDEpos,
          nDOFBCpos};
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::VectorSizeClass
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
vectorStateSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");

  // Create the size that represents the number of unknowns (possessed + ghost) linear algebra vector
  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {nDOFPDE,
          nDOFBC};
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
typename AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::MatrixSizeClass
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::matrixSize() const
{
  static_assert(iPDE == 0,"");
  static_assert(iBC == 1,"");
  static_assert(iq == 0,"");
  static_assert(ilg == 1,"");

  // Create the size that represents the size of a sparse linear algebra matrix
  const int nDOFPDEpos = qfld_.nDOFpossessed();
  const int nDOFBCpos  = lgfld_.nDOFpossessed();

  const int nDOFPDE = qfld_.nDOFpossessed() + qfld_.nDOFghost();
  const int nDOFBC  = lgfld_.nDOFpossessed() + lgfld_.nDOFghost();

  return {{ {nDOFPDEpos, nDOFPDE}, {nDOFPDEpos, nDOFBC} },
          { { nDOFBCpos, nDOFPDE}, { nDOFBCpos, nDOFBC} }};
}


template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
bool
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
isValidStateSystemVector(SystemVectorView& q)
{
  bool isValidState = true;

  // Update the solution field
  setSolutionField(q);

  IntegrateCellGroups<TopoDim>::integrate( isValidStateCell(pde_, fcnCell_.cellGroups(), isValidState),
                                             xfld_, qfld_, &quadratureOrder_[0], xfld_.nCellGroups() );
  dispatchBC_.dispatch(
      isValidStateBoundaryTrace_mitLG_Dispatch( pde_, xfld_, qfld_, lgfld_,
                                                &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(), isValidState ),
      isValidStateBoundaryTrace_sansLG_Dispatch( pde_, xfld_, qfld_,
                                                 &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(), isValidState )
    );

  return isValidState;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
int
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
nResidNorm() const
{
  return 2;
}

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
AlgebraicEquationSet_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
dumpLinesearchDebugInfo(const std::string& filenamebase, const int& nonlinear_iter,
                        const LinesearchData& pStepData) const
{
  std::string filename_iPDE = filenamebase + "_iPDE_iter" + std::to_string(nonlinear_iter) + ".plt";
  this->dumpLinesearchField(qfld_, *pStepData, iPDE, filename_iPDE);

//  std::string filename_iBC = filenamebase + "_iBC_iter" + std::to_string(nonlinear_iter) + ".plt";
//  this->dumpLinesearchField(lgfld_, *pStepData, iBC, filename_iBC);
}

} //namespace SANS

#endif //ALGEBRAICEQUATIONSET_CG_POTENTIAL_DRELA_H
