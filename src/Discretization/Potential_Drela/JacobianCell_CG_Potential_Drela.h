// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCELL_CG_POTENTIAL_DRELA_H
#define JACOBIANCELL_CG_POTENTIAL_DRELA_H

// cell integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin cell integral
//

template<class Surreal, class IntegrandCell, class SparseMatrix>
class JacobianCell_CG_Potential_Drela_impl :
    public GroupIntegralCellType< JacobianCell_CG_Potential_Drela_impl<Surreal, IntegrandCell, SparseMatrix> >
{
public:
  typedef typename IntegrandCell::PhysDim PhysDim;
  typedef typename IntegrandCell::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandCell::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandCell::template ArrayQ<Surreal> ArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  JacobianCell_CG_Potential_Drela_impl(const IntegrandCell& fcn,
                             SparseMatrix& mtxGlobalPDE_q, const int dupPointOffset, const std::vector<int>& invPointMap ) :
    fcn_(fcn),
    mtxGlobalPDE_q_(mtxGlobalPDE_q), dupPointOffset_(dupPointOffset), invPointMap_(invPointMap) {}

  std::size_t nCellGroups() const { return fcn_.nCellGroups(); }
  std::size_t cellGroup(const int n) const { return fcn_.cellGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld )
  {
    SANS_ASSERT( mtxGlobalPDE_q_.m() == qfld.nDOF() );
    SANS_ASSERT( mtxGlobalPDE_q_.n() == qfld.nDOFpossessed() + qfld.nDOFghost() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the cell group
  template<class Topology, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobal,
             const typename XFieldType::template FieldCellGroupType<Topology>& xfld,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology>& qfld,
             const int quadratureorder )
  {
    typedef typename XFieldType                     ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldSurrealClass;

    // element field variables
    ElementXFieldClass xfldElem( xfld.basis() );
    ElementQFieldSurrealClass qfldElemSurreal( qfld.basis() );

    // DOFs per element
    const int nDOF = qfldElemSurreal.nDOF();

    // variables/equations per DOF
    const int nEqn = IntegrandCell::N;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal(nDOF);
    std::vector<int> mapDOFGlobalNishida(nDOF);

    // element integral
    GalerkinWeightedIntegral<TopoDim, Topology, ArrayQSurreal> integral(quadratureorder, nDOF);

    // element integrand/residual
    std::vector<ArrayQSurreal> rsdPDEElemSurreal(nDOF);

    // element jacobian matrix
    MatrixElemClass mtxPDEElem(nDOF, nDOF);

    // number of simultaneous derivatives per functor call
    const int nDeriv = DLA::index(qfldElemSurreal.DOF(0),0).size();

    // loop over elements within group
    const int nelem = xfld.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobian
      mtxPDEElem = 0;

      // copy global grid/solution DOFs to element
      xfld.getElement( xfldElem, elem );
      qfld.getElement( qfldElemSurreal, elem );

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*nDOF; nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot;
        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElemSurreal.DOF(j), n).deriv(k) = 0;

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemSurreal.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        for (int n = 0; n < nDOF; n++)
          rsdPDEElemSurreal[n] = 0;

        // cell integration for canonical element
        integral( fcn_.integrand( xfldElem, qfldElemSurreal ), get<-1>(xfldElem), rsdPDEElemSurreal.data(), nDOF );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOF; i++)
              {
                for (int m = 0; m < nEqn; m++)
                {
                  DLA::index(mtxPDEElem(i,j), m,n) = DLA::index(rsdPDEElemSurreal[i], m).deriv(slot - nchunk);
                }
              }
            }
          }
        }
      }   // nchunk

      // scatter-add element jacobian to global
      scatterAdd( qfld, elem, mapDOFGlobal.data(), mapDOFGlobalNishida.data(), nDOF, mtxPDEElem, mtxGlobalPDE_q_ );
    }
  }

protected:
  //----------------------------------------------------------------------------//
  template <class QFieldCellGroupType, class MatrixQ, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfld,
      const int elem,
      int mapDOFGlobal[], int mapDOFGlobalNishida[], const int nDOF,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElem,
      SparseMatrixType<MatrixQ>& mtxGlobalPDE_q )
  {
    qfld.associativity( elem ).getGlobalMapping( mapDOFGlobal, nDOF );

    // Apply the Nishida HACK only adding volume integrals to "Left" residuals
    for (int n = 0; n < nDOF; n++)
    {
      if (mapDOFGlobal[n] >= dupPointOffset_)
        mapDOFGlobalNishida[n] = invPointMap_[mapDOFGlobal[n]-dupPointOffset_];
      else
        mapDOFGlobalNishida[n] = mapDOFGlobal[n];
    }

    mtxGlobalPDE_q.scatterAdd( mtxPDEElem, mapDOFGlobalNishida, nDOF, mapDOFGlobal, nDOF );
  }


  //----------------------------------------------------------------------------//
#if 0 // Nishida HACK not available here
  template <class QFieldCellGroupType, class MatrixQ, template <class> class SparseMatrixType>
  void
  scatterAdd(
      const QFieldCellGroupType& qfld,
      const int elem,
      int mapDOFGlobal[], const int nDOF,
      SANS::DLA::MatrixD<MatrixQ>& mtxPDEElem,
      SparseMatrixType< SANS::DLA::MatrixD< MatrixQ > >& mtxGlobalPDE_q )
  {
    mtxGlobalPDE_q.scatterAdd( mtxPDEElem, elem, elem );
  }
#endif

protected:
  const IntegrandCell& fcn_;
  SparseMatrix& mtxGlobalPDE_q_;
  const int dupPointOffset_;
  const std::vector<int>& invPointMap_;
};


// Factory function

template<class Surreal, class IntegrandCell, class SparseMatrix>
JacobianCell_CG_Potential_Drela_impl<Surreal, IntegrandCell, SparseMatrix>
JacobianCell_CG_Potential_Drela( const IntegrandCellType<IntegrandCell>& fcn,
                           SparseMatrix& mtxGlobalPDE_q, const int dupPointOffset,
                           const std::vector<int>& invPointMap )
{
  return {fcn.cast(), mtxGlobalPDE_q, dupPointOffset, invPointMap};
}


} //namespace SANS

#endif  // JACOBIANCELL_CG_POTENTIAL_DRELA_H
