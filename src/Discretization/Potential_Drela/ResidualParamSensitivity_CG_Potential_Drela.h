// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALPARAMSENSITIVITY_CG_POTENTIAL_DRELA_H
#define RESIDUALPARAMSENSITIVITY_CG_POTENTIAL_DRELA_H

#include <type_traits>

#include "tools/SANSnumerics.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "pde/BCParameters.h"

#include "Field/XField.h"
#include "Field/Field.h"

#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"
#include "Discretization/IntegrateCellGroups.h"
#include "Discretization/Galerkin/IntegrateBoundaryTrace_Dispatch_Galerkin.h"

#include "ResidualCell_CG_Potential_Drela.h"
#include "ResidualBoundaryTrace_CG_Potential_Drela.h"
#include "ResidualBoundaryFrame_CG_Potential_Drela.h"
#include "ResidualBoundaryTrace_Dispatch_CG_Potential_Drela.h"

#include "ResidualBoundaryTrace_WakeCut_CG_Potential_Drela.h"

#include "BC_WakeCut_Potential_Drela.h"

#include "Discretization/Galerkin/IntegrandCell_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_mitLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_LinearScalar_sansLG_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_None_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Dirichlet_sansLG_Galerkin.h"

#include "Discretization/Galerkin/Stabilization_Nitsche.h"

namespace SANS
{

// Forward declare
template <class PhysDim, class TopoDim, class T>
class Field_CG_Cell;

template <class PDE>
class IntegrandBoundaryFrame_CG_Potential_Drela;

//----------------------------------------------------------------------------//
template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class SurrealClass, class XFieldType>
class ResidualParamSensitivity_CG_Potential_Drela
{
public:
  typedef typename NDPDEClass::PhysDim PhysDim;
  typedef typename XFieldType::TopoDim TopoDim;
  typedef typename NDPDEClass::template ArrayQ<Real> ArrayQ;
  typedef typename NDPDEClass::template ArrayQ<SurrealClass> ArrayQSurreal;

  typedef SLA::SparseVector<ArrayQSurreal> VectorClass;
  typedef DLA::VectorD<VectorClass> SystemVectorClass;

  typedef BCParameters<BCVector> BCParams;
  typedef IntegrandCell_Galerkin<NDPDEClass> IntegrandCellClass;
  typedef IntegrateBoundaryTrace_Dispatch_Galerkin<NDPDEClass, BCNDConvert, BCVector, Galerkin> IntegrateBoundaryTrace_Dispatch;

  typedef IntegrandBoundaryTrace<NDPDEClass,
                                 NDVectorCategory<boost::mpl::vector1<BC_WakeCut_Potential_Drela>, BCCategory::WakeCut_Potential_Drela>,
                                 Galerkin> IntegrandWake;
  typedef IntegrandBoundaryFrame_CG_Potential_Drela<NDPDEClass> IntegrandKutta;

  template< class... BCArgs >
  ResidualParamSensitivity_CG_Potential_Drela(const XFieldType& xfld,
                          Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld,
                          Field<PhysDim, TopoDim, ArrayQ>& lgfld,
                          const NDPDEClass& pde,
                          const StabilizationNitsche& stab,
                          const std::vector<int>& CellGroups,
                          const std::vector<int>& WakeGroups,
                          const PyDict& BCList,
                          const std::map< std::string, std::vector<int> >& BCBoundaryGroups, BCArgs&... args )
   : stab_(stab),
     fcnCell_(pde, CellGroups),
     fcnWake_(pde, WakeGroups, get<-1>(xfld).dupPointOffset_, get<-1>(xfld).KuttaPoints_),
     fcnKutta_(pde, get<-1>(xfld).dupPointOffset_, get<-1>(xfld).KuttaPoints_),
     BCs_(BCParams::template createBCs<BCNDConvert>(pde, BCList, args...)), dispatchBC_(pde, BCList, BCs_, BCBoundaryGroups, stab_),
     xfld_(xfld), qfld_(qfld), lgfld_(lgfld), pde_(pde)
  {
    quadratureOrder_.resize(std::max(std::max(xfld.nCellGroups(),
                                              xfld.getXField().nBoundaryTraceGroups()),
                                              get<-1>(xfld).nBoundaryFrameGroups()), 2);
  }

  ~ResidualParamSensitivity_CG_Potential_Drela() {}

  //Computes the residual
  void residualSensitivity(SystemVectorClass& rsd);

  // Gives the PDE and solution indices in the system
  int indexPDE() const { return iPDE; }
  int indexQ() const { return iq; }

  // Indexes to order the equations and the solution vectors
  static const int iPDE = 0;
  static const int iBC = 1;
  static const int iq = 0;
  static const int ilg = 1;

protected:
  const StabilizationNitsche& stab_;
  IntegrandCellClass fcnCell_;
  IntegrandWake fcnWake_;
  IntegrandKutta fcnKutta_;

  std::map< std::string, std::shared_ptr<BCBase> > BCs_;
  IntegrateBoundaryTrace_Dispatch dispatchBC_;

  const XFieldType& xfld_;
  Field_CG_Cell<PhysDim, TopoDim, ArrayQ>& qfld_;
  Field<PhysDim, TopoDim, ArrayQ>& lgfld_;
  const NDPDEClass& pde_;

  std::vector<int> quadratureOrder_;
};

template<class NDPDEClass, template<class,class> class BCNDConvert, class BCVector, class Traits, class XFieldType>
void
ResidualParamSensitivity_CG_Potential_Drela<NDPDEClass, BCNDConvert, BCVector, Traits, XFieldType>::
residualSensitivity(SystemVectorClass& rsd)
{
  IntegrateCellGroups<TopoDim>::integrate(
      ResidualCell_CG_Potential_Drela(fcnCell_, rsd(iq), get<-1>(xfld_).dupPointOffset_, get<-1>(xfld_).invPointMap_),
      xfld_, qfld_, &quadratureOrder_[0], xfld_.nCellGroups() );

  ResidualBoundaryTrace_WakeCut_CG_Potential_Drela<TopoD3>::integrate( fcnWake_, qfld_,
                                                                       &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(), rsd(iq) );

  ResidualBoundaryFrame_CG_Potential_Drela<TopoD3>::integrate( fcnKutta_,
                                                               get<-1>(xfld_), qfld_, &quadratureOrder_[0],
                                                               get<-1>(xfld_).nBoundaryFrameGroups(), rsd(iq) );

  dispatchBC_.dispatch(
      ResidualBoundaryTrace_FieldTrace_Dispatch_Galerkin_LIP( xfld_, qfld_, lgfld_,
                                                              &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                              rsd(iq), rsd(ilg) ),
      ResidualBoundaryTrace_Dispatch_CG_Potential_Drela( xfld_, qfld_,
                                                         &quadratureOrder_[0], xfld_.getXField().nBoundaryTraceGroups(),
                                                         rsd(iq) ) );
}


} //namespace SANS

#endif //RESIDUALPARAMSENSITIVITY_CG_POTENTIAL_DRELA_H
