// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFRAME_CG_POTENTIAL_DRELA_H
#define INTEGRANDBOUNDARYFRAME_CG_POTENTIAL_DRELA_H

// boundary-frame integral functional
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/TraceToCellRefCoord.h"

#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"

namespace SANS
{
template <class PDE>
class IntegrandBoundaryFrame_CG_Potential_Drela;

//----------------------------------------------------------------------------//
template <template<class,class> class NDConvert, class Tg>
class IntegrandBoundaryFrame_CG_Potential_Drela<NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>>>
{
public:
  typedef NDConvert<PhysD3,PDELinearizedIncompressiblePotential3D<Tg>> PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryFrame_CG_Potential_Drela( const PDE& pde,
                                       const int dupPointOffset, const std::vector<int>& KuttaPoints )
    : dupPointOffset(dupPointOffset), KuttaPoints(KuttaPoints), pde_(pde)
  {}

  const int dupPointOffset;
  const std::vector<int> KuttaPoints;

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace > ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyCell > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace , TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell  , TopologyCell> ElementQFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const PDE& pde,
             const ElementXFieldTrace& xfldElemTrace,
             const CanonicalTraceToCell& canonicalTrace,
             const ElementXFieldCell& xfldElem,
             const ElementQFieldCell& qfldElem ) :
             pde_(pde), xfldElemTrace_(xfldElemTrace),
             canonicalTrace_(canonicalTrace),
             xfldElem_(xfldElem), qfldElem_(qfldElem)
             {}

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRefTrace,
                     ArrayQ<Ti> integrandKutta[], int nrsdKutta ) const
    {
      typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;

      const int nDOF = qfldElem_.nDOF();             // total solution DOFs in left element

      SANS_ASSERT(nrsdKutta == nDOF);

      VectorX X;
      VectorX N;           // unit normal for left element (points to right element)

      std::vector<Real> phi(nDOF);                                   // basis
      std::vector< DLA::VectorS< PhysDim::D, Real> > gradphi(nDOF);  // basis gradient

      ArrayQ<T> q;
      VectorArrayQ gradq;  // gradient

      DLA::VectorS< PhysDim::D, Ti > U; // Freestream Velocity

      QuadPointCellType sRefCell;

      TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyCell>::eval( canonicalTrace_, sRefTrace, sRefCell );

      // unit normal
      xfldElemTrace_.unitNormal( sRefTrace, N );
      Real dJ = xfldElemTrace_.jacobianDeterminant( sRefTrace );

      xfldElemTrace_.coordinates(sRefTrace, X);

      // basis value, gradient
      qfldElem_.evalBasis( sRefCell, phi.data(), nDOF );
      xfldElem_.evalBasisGradient( sRefCell, qfldElem_, gradphi.data(), nDOF );

      // solution value, gradient
      qfldElem_.eval( sRefCell, q );
      qfldElem_.evalFromBasis( gradphi.data(), nDOF, gradq );

      for (int k = 0; k < nDOF; k++)
        integrandKutta[k] = 0;

      //U = pde_.freestream();
      U = pde_.Up(X);

      //VectorArrayQ gradqjump = gradqR - gradqL;
      //ArrayQ<T> Ugradqjump = dot(U,gradqjump);

      //Ugradqjump += 0.5*(dot(gradqR,gradqR) - dot(gradqL,gradqL));
#if 1
      Real h = pow( dJ, 1./TopoDimTrace::D );
      Ti Un = dot(N,U); // Normal velocity
      DLA::VectorS< PhysDim::D, Ti > Us = U - Un*N; // Tangential velocity
      Ti V = sqrt( dot(Us, Us) );
#endif

      // tangential BC
#if 0
      // Compute the surface tangent gradient
      gradqL -= dot(nL,gradqL)*nL;
      gradqR -= dot(nR,gradqR)*nR;

      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += phiL[k]*dot(UsL,gradqL);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += phiR[k]*dot(UsR,gradqR);
#endif

#if 0
      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += -(phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*Ugradqjump;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] +=  (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*Ugradqjump;
#endif

      //Linearized Pressure jump
#if 0
      ArrayQ<T> Ugradq = dot(U,gradq);

      for (int k = 0; k < nDOF; k++)
        integrandKutta[k] += phi[k]*Ugradq;
#endif

      //Pressure jump
#if 0
      DLA::VectorS< PhysDim::D, Ti > V = gradq + U;

      ArrayQ<Ti> P = dot(V,V);

      for (int k = 0; k < nDOF; k++)
        integrandKutta[k] += phi[k]*P/dJ;
#endif

      //Pressure jump
#if 0
      for (int k = 0; k < nDOFR; k++)
      {
        Real wR = dot(nR,gradphiR[k]);
        Real wL = dot(nL,gradphiL[k]);
        integrandPDER[k] += wR*dot(U,gradqR) + wL*dot(U,gradqL);
      }
#endif

      //Pressure jump
#if 0
      DLA::VectorS< PhysDim::D, T > VR = gradqR + U;
      DLA::VectorS< PhysDim::D, T > VL = gradqL + U;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += dot(nR,gradphiR[k])*dot(VR,VR) + dot(nL,gradphiL[k])*dot(VL,VL);
#endif

      //Pressure jump
#if 0
      // Compute the surface tangent gradient
      gradqL -= dot(nL,gradqL)*nL;
      gradqR -= dot(nR,gradqR)*nR;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*dot(UsR,gradqR)
                          - (phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*dot(UsL,gradqL);
#endif

     //Pressure jump this works
#if 0
      ArrayQ<Ti> Ugradq = dot(U,gradq);

      for (int k = 0; k < nDOF; k++)
        integrandKutta[k] += -phi[k]*Ugradq/dJ;
#endif

#if 1
      ArrayQ<Ti> Ugradq = dot(U,gradq);

      Real eps = 0.5;
      for (int k = 0; k < nDOF; k++)
        integrandKutta[k] += -( phi[k] + eps*h/V*(dot(U,gradphi[k]) - Un*dot(N,gradphi[k])) )*Ugradq/dJ;
#endif

#if 0
      Real h = pow( dJ, 1./TopoDimTrace::D );

      DLA::VectorS< PhysDim::D, Ti > V = U + gradq;
      Ti Vn = dot(N,V); // Normal velocity
      DLA::VectorS< PhysDim::D, Ti > Vs = V - Vn*N;
      Ti Vm = sqrt( dot(Vs, Vs) );

      ArrayQ<Ti> V2 = dot(V,V);

      Real eps = 0.5;
      for (int k = 0; k < nDOF; k++)
        integrandKutta[k] += -(phi[k] + eps*h/Vm*(dot(V,gradphi[k]) - Vn*dot(N,gradphi[k])))*V2/dJ;
#endif

#if 0
      ArrayQ<T> Ugradqjump = dot(U,gradqR) - dot(U,gradqL);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] +=  (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*Ugradqjump;
#endif

#if 0
      VectorArrayQ gradqsum = gradqR + gradqL;
      ArrayQ<T> Ugradqsum = dot(U,gradqsum);

      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += (phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*Ugradqsum;

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*Ugradqsum;
#endif

#if 0
      VectorArrayQ gradqsum = gradqR + gradqL;
      ArrayQ<T> Ugradqsum = dot(U,gradqsum);

      for (int k = 0; k < nDOFL; k++)
      {
        Real PhiL = phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k]));
        integrandPDEL[k] += 0.5*PhiL*Ugradqsum - 0.5*PhiL*Ugradqjump;
      }

      for (int k = 0; k < nDOFR; k++)
      {
        Real PhiR = phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k]));
        integrandPDER[k] += 0.5*PhiR*Ugradqsum + 0.5*PhiR*Ugradqjump;
      }
#endif

#if 0
      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += -(phiL[k] + 0.5*hL/VL*(dot(U,gradphiL[k]) - UnL*dot(nL,gradphiL[k])) )*(0.5*dot(gradqL,gradqL) + dot(U,gradqL));

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] +=  (phiR[k] + 0.5*hR/VR*(dot(U,gradphiR[k]) - UnR*dot(nR,gradphiR[k])) )*(0.5*dot(gradqR,gradqR) + dot(U,gradqR));
#endif

#if 0
      for (int k = 0; k < nDOFL; k++)
        integrandPDEL[k] += 0.5*phiL[k]*(qR - qL);

      for (int k = 0; k < nDOFR; k++)
        integrandPDER[k] += 0.5*phiR[k]*(qR - qL);
#endif
    }

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell>
  Functor<T, TopoDimTrace, TopologyTrace,
             TopoDimCell, TopologyCell>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysDim, TopoDimCell , TopologyCell >& xfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyCell >& qfldElem ) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace,
                      TopoDimCell, TopologyCell>(pde_, xfldElemTrace,
                                                 canonicalTrace,
                                                 xfldElem, qfldElem );
  }

protected:
  const PDE& pde_;
};

} //namespace SANS

#endif // INTEGRANDBOUNDARYFRAME_CG_POTENTIAL_DRELA_H
