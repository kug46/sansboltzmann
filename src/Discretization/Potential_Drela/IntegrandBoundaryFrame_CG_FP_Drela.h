// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARYFRAME_CG_FP_DRELA_H
#define INTEGRANDBOUNDARYFRAME_CG_FP_DRELA_H

// boundary-frame integral functional
#include "tools/SANSException.h"

#include "Field/Element/Element.h"

#include "BasisFunction/TraceToCellRefCoord.h"

#include "pde/FullPotential/PDEFullPotential3D.h"

namespace SANS
{
template <class PDE>
class IntegrandBoundaryFrame_CG_Potential_Drela;

//----------------------------------------------------------------------------//
template <template<class,class> class NDConvert, class Tg>
class IntegrandBoundaryFrame_CG_Potential_Drela<NDConvert<PhysD3,PDEFullPotential3D<Tg>>>
{
public:
  typedef NDConvert<PhysD3,PDEFullPotential3D<Tg>> PDE;
  typedef typename PDE::PhysDim PhysDim;

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;    // solution/residual jacobians

  static const int N = PDE::N;

  IntegrandBoundaryFrame_CG_Potential_Drela( const PDE& pde,
                                         const int dupPointOffset, const std::vector<int>& KuttaPoints )
    : dupPointOffset(dupPointOffset), KuttaPoints(KuttaPoints), pde_(pde)
  {}

  const int dupPointOffset;
  const std::vector<int> KuttaPoints;

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell>
  class Functor
  {
  public:
    typedef ElementXField<PhysDim, TopoDimTrace, TopologyTrace > ElementXFieldTrace;
    typedef ElementXField<PhysDim, TopoDimCell , TopologyCell > ElementXFieldCell;

    typedef Element<ArrayQ<T>, TopoDimTrace , TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoDimCell  , TopologyCell> ElementQFieldCell;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const PDE& pde,
             const ElementXFieldTrace& xfldElemTrace,
             const CanonicalTraceToCell& canonicalTrace,
             const ElementXFieldCell& xfldElem,
             const ElementQFieldCell& qfldElem ) :
             pde_(pde), xfldElemTrace_(xfldElemTrace),
             canonicalTrace_(canonicalTrace),
             xfldElem_(xfldElem), qfldElem_(qfldElem)
             {}

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    // trace integrand
    template<class Ti>
    void operator()( const QuadPointTraceType& sRefTrace,
                     ArrayQ<Ti> integrandKutta[], int nrsdKutta ) const
    {
      typedef DLA::VectorS< PhysDim::D, ArrayQ<T> > VectorArrayQ;
      typedef DLA::VectorS< PhysDim::D, T > VectorT;

      const int nDOF = qfldElem_.nDOF();             // total solution DOFs in left element

      SANS_ASSERT(nrsdKutta == nDOF);

      VectorX N;           // unit normal for left element (points to right element)

      std::vector<Real> phi(nDOF);                                   // basis
      std::vector< DLA::VectorS< PhysDim::D, Real> > gradphi(nDOF);  // basis gradient

      ArrayQ<T> q;
      VectorArrayQ gradq;  // gradient
      VectorT gradPhi;

      DLA::VectorS< PhysDim::D, Ti > U; // Freestream Velocity

      QuadPointCellType sRefCell;

      TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyCell>::eval( canonicalTrace_, sRefTrace, sRefCell );

      // unit normal
      xfldElemTrace_.unitNormal( sRefTrace, N );
      Real dJ = xfldElemTrace_.jacobianDeterminant( sRefTrace );

      // basis value, gradient
      qfldElem_.evalBasis( sRefCell, phi.data(), nDOF );
      xfldElem_.evalBasisGradient( sRefCell, qfldElem_, gradphi.data(), nDOF );

      // solution value, gradient
      qfldElem_.eval( sRefCell, q );
      qfldElem_.evalFromBasis( gradphi.data(), nDOF, gradq );

      for (int k = 0; k < nDOF; k++)
        integrandKutta[k] = 0;

      U = pde_.freestream();

      //Ti Un = dot(N,U); // Normal velocity
      //DLA::VectorS< PhysDim::D, Ti > Us = U - Un*N; // Tangential velocity
      //Ti Usm = sqrt( dot(Us, Us) );

      Real h = pow( xfldElemTrace_.jacobianDeterminant(), 1./TopoDimTrace::D );

      for (int d = 0; d < PhysDim::D; d++)
        gradPhi[d] = gradq[d][0];

      //Pressure jump based on tangential velocity
      DLA::VectorS< PhysDim::D, Ti > V = gradPhi + U;

      Ti Vn = dot(N,V); // Normal velocity
      DLA::VectorS< PhysDim::D, Ti > Vs = V - Vn*N;
      Ti Vm = sqrt( dot(Vs, Vs) );

      Ti V2 = dot(V,V);

      //Ti p = pde_.pressure(V);

      Real eps = 0.5;
      for (int k = 0; k < nDOF; k++)
      {
        //integrandKutta[k][0] += -phi[k]*p/dJ;
        //integrandKutta[k][0] += -phi[k]*V2/dJ;
        //integrandKutta[k][1] += -phi[k]*rho;

        //integrandKutta[k][0] += -(phi[k] + eps*h/Usm*(dot(U,gradphi[k]) - Un*dot(N,gradphi[k])) )*V2/dJ;
        integrandKutta[k][0] += -(phi[k] + eps*h/Vm*(dot(V,gradphi[k]) - Vn*dot(N,gradphi[k])))*V2/dJ;
      }
    }

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldCell& xfldElem_;
    const ElementQFieldCell& qfldElem_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyCell>
  Functor<T, TopoDimTrace, TopologyTrace,
             TopoDimCell, TopologyCell>
  integrand(const ElementXField<PhysDim, TopoDimTrace, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysDim, TopoDimCell , TopologyCell >& xfldElem,
            const Element<ArrayQ<T>    , TopoDimCell , TopologyCell >& qfldElem ) const
  {
    return Functor<T, TopoDimTrace, TopologyTrace,
                      TopoDimCell, TopologyCell>(pde_, xfldElemTrace,
                                                 canonicalTrace,
                                                 xfldElem, qfldElem );
  }

protected:
  const PDE& pde_;
};

} //namespace SANS

#endif // INTEGRANDBOUNDARYFRAME_CG_FP_DRELA_H
