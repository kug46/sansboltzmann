// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JacobianBoundaryFrame_CG_Potential_Drela_H
#define JacobianBoundaryFrame_CG_Potential_Drela_H

// jacobian boundary-frame integral jacobian functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/XField3D_Wake.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/Element/Element.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

namespace SANS
{
template <class PDE>
class IntegrandBoundaryFrame_CG_Potential_Drela;

//----------------------------------------------------------------------------//
template <class QFieldCellGroupType,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryFrame_CG_Potential_Drela_ScatterAdd(
    const QFieldCellGroupType& qfldCell, const int elemCell,
    int mapDOFGlobal[], int mapDOFGlobalKutta[], const int nDOF,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElem_qElem,
    SparseMatrix<MatrixQ>& mtxGlobalPDE_q )
{
  mtxGlobalPDE_q.scatterAdd( mtxPDEElem_qElem, mapDOFGlobalKutta, nDOF, mapDOFGlobal, nDOF );
}

#if 0
//----------------------------------------------------------------------------//
template <class QFieldCellGroupType,
          class MatrixQ,
          template <class> class SparseMatrix>
void
JacobianBoundaryFrame_CG_Potential_Drela_ScatterAdd(
    const QFieldTraceGroupType& lgfldTrace,
    const QFieldCellGroupType& qfldCellL,
    const int elemL, const int elem,
    int mapDOFGlobalL[], const int nDOFL,
    int mapDOFGlobalEdge[], const int nDOFEdge,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxPDEElemL_lgEdge,
    SANS::DLA::MatrixD<MatrixQ>& mtxBC_qElemL,
    SANS::DLA::MatrixD<MatrixQ>& mtxBC_lgEdge,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalPDE_lg,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalBC_q,
    SparseMatrix< SANS::DLA::MatrixD<MatrixQ> >& mtxGlobalBC_lg )
{

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryEdge_ScatterAdd: elemL, elem = " << elemL << ," " << elem << std::endl;
  std::cout << "JacobianPDE_Group_2DBoundaryEdge_ScatterAdd (before): mtxPDEGlobal = ";
  mtxPDEGlobal.dump(2);
#endif

#if 0
  std::cout << "JacobianPDE_Group_2DBoundaryEdge_ScatterAdd: mtxGlobalPDE_q = ";  mtxGlobalPDE_q.dump(2);
#endif

  mtxGlobalPDE_q.scatterAdd( mtxPDEElemL_qElemL, elemL, elemL );
  mtxGlobalPDE_lg.scatterAdd( mtxPDEElemL_lgEdge, elemL, elem );

  mtxGlobalBC_q.scatterAdd( mtxBC_qElemL, elem, elemL );
  mtxGlobalBC_lg.scatterAdd( mtxBC_lgEdge, elem, elem );
}
#endif

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//
//  functions dispatched based on left (L) element topology
//
//  process:
//  (a) loop over groups; dispatch to L (JacobianPDE_LeftTopology_Integral2DBoundaryEdge)
//  (b) call base class routine with specific L (JacobianPDE_Group_Integral2DBoundaryEdge w/ Base& params)
//  (c) cast to specific L and call L-specific topology routine (JacobianPDE_Group_Integral2DBoundaryEdge)
//  (d) loop over edges and integrate


template <class Surreal,
          class TopologyTrace, class TopologyCell,
          class XFieldType,
          class PhysDim, class TopoDim, class ArrayQ,
          class PDE,
          class SparseMatrix>
void
JacobianBoundaryFrame_CG_Potential_Drela_Group_Integral(
    const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
    const std::map<int,NodeToBCElemMap>& nodeElemMap,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCell>& xfldCell,
    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCell>& qfldCell,
    const int sgn,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q )
{
  typedef typename PDE::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename PDE::template MatrixQ<Real> MatrixQ;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldCellGroupType<TopologyCell> XFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

  typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
  typedef typename QFieldCellGroupType::template ElementType<Surreal> ElementQFieldClass;

  typedef typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
  typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

  // element field variables
  ElementXFieldClass xfldElem( xfldCell.basis() );

  ElementQFieldClass qfldElem( qfldCell.basis() );

  ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

  // variables/equations per DOF
  const int nEqn = PDE::N;

  // DOFs per element
  int nDOF = qfldElem.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobal(nDOF);
  std::vector<int> mapDOFGlobalKutta(nDOF);

  // trace element integral
  GalerkinWeightedIntegral<TopoD2, TopologyTrace, ArrayQSurreal> integral(quadratureorder, nDOF);

  // element integrand/residuals
  std::unique_ptr<ArrayQSurreal[]> rsdPDEElem( new ArrayQSurreal[nDOF] );

  // element jacobians
  MatrixElemClass mtxPDEElem_qElem(nDOF, nDOF);

  // loop over elements within group
  for (auto map = nodeElemMap.begin(); map != nodeElemMap.end(); map++)
  {
    int nTraceElemPoint = map->first;
    int nelem = map->second.elems.size();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians
      mtxPDEElem_qElem = 0;

      const int elemTrace = map->second.elems[ elem ];

      xfldTrace.getElement( xfldElemTrace, elemTrace );

      CanonicalTraceToCell& canonicalTrace = xfldTrace.getCanonicalTraceLeft( elemTrace );

      const int elemCell = xfldTrace.getElementLeft( elemTrace );

      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elemCell );
      qfldCell.getElement( qfldElem, elemCell );

      qfldCell.associativity( elemCell ).getGlobalMapping( mapDOFGlobal.data(), nDOF );

      // number of simultaneous derivatives per functor call
      const int nDeriv = DLA::index(qfldElem.DOF(0), 0).size();

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nEqn*nDOF; nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot;
        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qfldElem.DOF(j), n).deriv(k) = 0;

            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElem.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // line integration for canonical element
        for (int n = 0; n < nDOF; n++)
          rsdPDEElem[n] = 0;

        integral(
            fcn.integrand(xfldElemTrace,
                          canonicalTrace,
                          xfldElem, qfldElem ),
                          xfldElemTrace,
                          rsdPDEElem.get(), nDOF );

        // accumulate derivatives into element jacobian

        for (int j = 0; j < nDOF; j++)
        {
          for (int n = 0; n < nEqn; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOF; i++)
              {
                if ( mapDOFGlobal[i] != nTraceElemPoint ) continue;

                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxPDEElem_qElem(i,j), m,n) = sgn*DLA::index(rsdPDEElem[i], m).deriv(slot - nchunk);
              }
            }
          }
        }
      }   // nchunk

      // Create the map for the Kutta equations
      for (int n = 0; n < nDOF; n++)
      {
        mapDOFGlobalKutta[n] = 0;
        if ( mapDOFGlobal[n] == nTraceElemPoint )
          mapDOFGlobalKutta[n] = map->second.KuttaPoint;
      }

      // scatter-add element jacobian to global

      JacobianBoundaryFrame_CG_Potential_Drela_ScatterAdd(
          qfldCell, elemCell,
          mapDOFGlobal.data(), mapDOFGlobalKutta.data(), nDOF,
          mtxPDEElem_qElem,
          mtxGlobalPDE_q );

    } // elem
  }
}


//----------------------------------------------------------------------------//
template <class Surreal,
          class TopologyTrace, class TopologyCell,
          class XFieldType,
          class PDE,
          class PhysDim, class TopoDim, class ArrayQ,
          class SparseMatrix>
void
JacobianBoundaryFrame_CG_Potential_Drela_Group(
    const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
    const std::map<int,NodeToBCElemMap>& nodeElemMap,
    const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
    const Field<PhysDim, TopoDim, ArrayQ>& qfld, const int sgn,
    const int quadratureorder,
    SparseMatrix& mtxGlobalPDE_q )
{
  const XField<PhysDim, TopoDim>& xfld = qfld.getXField();

  int group = xfldTrace.getGroupLeft();

  // Integrate over the trace group
  JacobianBoundaryFrame_CG_Potential_Drela_Group_Integral<Surreal,
                                                TopologyTrace,TopologyCell,
                                                XFieldType,PhysDim,TopoDim,ArrayQ>(
      fcn, nodeElemMap,
      xfldTrace, xfld.template getCellGroup<TopologyCell>(group), qfld.template getCellGroup<TopologyCell>(group), sgn,
      quadratureorder,
      mtxGlobalPDE_q );
}

template<class Surreal, class TopoDim>
class JacobianBoundaryFrame_CG_Potential_Drela;

template<class Surreal>
class JacobianBoundaryFrame_CG_Potential_Drela<Surreal,TopoD3>
{
  typedef TopoD3 TopoDim;
protected:

//----------------------------------------------------------------------------//
  template <class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellLeft_Triangle(
      const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
      const std::map<int,NodeToBCElemMap>& nodeElemMap,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld, const int sgn,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_CG_Potential_Drela_Group<Surreal, Triangle, Tet, XFieldType>(
          fcn, nodeElemMap, xfldTrace, qfld, sgn,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unkown topology" );
  }

  //----------------------------------------------------------------------------//
  template <class XFieldType,
            class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  TopologyCellLeft_Quad(
      const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
      const std::map<int,NodeToBCElemMap>& nodeElemMap,
      const typename XField<PhysDim, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld, const int sgn,
      int quadratureorder,
      SparseMatrix& mtxGlobalPDE_q )
  {

    // determine topology for L
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // integrate over the boundary
      JacobianBoundaryFrame_CG_Potential_Drela_Group<Surreal, Quad, Hex, XFieldType>(
          fcn, nodeElemMap, xfldTrace, qfld, sgn,
          quadratureorder,
          mtxGlobalPDE_q );
    }
    else
      SANS_DEVELOPER_EXCEPTION( "unkown topology" );
  }

public:
  template <class XFieldType, class PDE,
            class PhysDim, class ArrayQ,
            class SparseMatrix>
  static void
  integrate(
      const IntegrandBoundaryFrame_CG_Potential_Drela<PDE>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
      const int quadratureorder[], int ngroup,
      SparseMatrix& mtxGlobalPDE_q )
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );

#if 1
    for (auto it = xfld.KuttaBCTraceElemLeft_.begin(); it != xfld.KuttaBCTraceElemLeft_.end(); it++)
    {
      int boundaryGroupL = it->first;

      // get the boundary group
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Triangle) )
      {
        // dispatch to determine left cell topology
        TopologyCellLeft_Triangle<XFieldType>(
            fcn, it->second,
            xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupL),
            qfld, 1,
            quadratureorder[boundaryGroupL],
            mtxGlobalPDE_q );
      }
      else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Quad) )
      {
        // dispatch to determine left cell topology
        TopologyCellLeft_Quad<XFieldType>(
            fcn, it->second,
            xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupL),
            qfld, 1,
            quadratureorder[boundaryGroupL],
            mtxGlobalPDE_q );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
#endif
#if 1
    for (auto it = xfld.KuttaBCTraceElemRight_.begin(); it != xfld.KuttaBCTraceElemRight_.end(); it++)
    {
      int boundaryGroupR = it->first;

      // get the boundary group
      if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Triangle) )
      {
        // dispatch to determine left cell topology
        TopologyCellLeft_Triangle<XFieldType>(
            fcn, it->second,
            xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupR),
            qfld, -1,
            quadratureorder[boundaryGroupR],
            mtxGlobalPDE_q );
      }
      else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupR).topoTypeID() == typeid(Quad) )
      {
        // dispatch to determine left cell topology
        TopologyCellLeft_Quad<XFieldType>(
            fcn, it->second,
            xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupR),
            qfld, -1,
            quadratureorder[boundaryGroupR],
            mtxGlobalPDE_q );
      }
      else
        SANS_DEVELOPER_EXCEPTION( "unknown trace topology" );
    }
#endif
  }
};


}

#endif  // JacobianBoundaryFrame_CG_Potential_Drela_H
