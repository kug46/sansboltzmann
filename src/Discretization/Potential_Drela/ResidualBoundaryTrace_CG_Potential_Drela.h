// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RESIDUALBOUNDARYTRACE_CG_POTENTIAL_DRELA_H
#define RESIDUALBOUNDARYTRACE_CG_POTENTIAL_DRELA_H

// boundary-trace integral residual functions

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "Field/Field.h"
#include "Field/Element/GalerkinWeightedIntegral.h"

#include "Discretization/Integrand_Type.h"
#include "Discretization/GroupIntegral_Type.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Galerkin boundary-trace integral
//

template<class IntegrandBoundaryTrace, template<class> class Vector, class TR>
class ResidualBoundaryTrace_CG_Potential_Drela_impl :
    public GroupIntegralBoundaryTraceType< ResidualBoundaryTrace_CG_Potential_Drela_impl<IntegrandBoundaryTrace, Vector, TR> >
{
public:
  typedef typename IntegrandBoundaryTrace::PhysDim PhysDim;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandBoundaryTrace::template ArrayQ<TR> ArrayQR;

  // Save off the boundary trace integrand and the residual vectors
  ResidualBoundaryTrace_CG_Potential_Drela_impl( const IntegrandBoundaryTrace& fcn,
                                           Vector<ArrayQR>& rsdPDEGlobal, const int dupPointOffset, const std::vector<int>& invPointMap ) :
    fcn_(fcn), rsdPDEGlobal_(rsdPDEGlobal), dupPointOffset_(dupPointOffset), invPointMap_(invPointMap) {}

  std::size_t nBoundaryGroups() const { return fcn_.nBoundaryGroups(); }
  std::size_t boundaryGroup(const int n) const { return fcn_.boundaryGroup(n); }

//----------------------------------------------------------------------------//
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const Field<PhysDim, TopoDim, ArrayQ>& qfld ) const
  {
    SANS_ASSERT( rsdPDEGlobal_.m() == qfld.nDOF() );
  }

//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the boundary trace group
  template <class TopologyTrace, class TopologyL, class TopoDim, class XFieldType>
  void
  integrate( const int cellGroupGlobalL,
             const typename XFieldType::template FieldCellGroupType<TopologyL>& xfldCellL,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL>& qfldCellL,
             const int traceGroupGlobal,
             const typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
             int quadratureorder )
  {
    typedef typename XFieldType::template FieldCellGroupType<TopologyL> XFieldCellGroupTypeL;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<TopologyL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename XFieldType::XFieldType::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename TopologyTrace::TopoDim TopoDimTrace;


    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // number of integrals evaluated per element
    int nIntegrandL = qfldElemL.nDOF();

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL( nIntegrandL );

    // trace element integral
    GalerkinWeightedIntegral<TopoDimTrace, TopologyTrace, ArrayQR> integral(quadratureorder, nIntegrandL);

    // element integrand/residuals
    std::vector<ArrayQR> rsdPDEElemL( nIntegrandL );

    // loop over elements within group
    int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldTrace.getElement( xfldElemTrace, elem );

      for (int n = 0; n < nIntegrandL; n++)
        rsdPDEElemL[n] = 0;

      integral( fcn_.integrand(xfldElemTrace, canonicalTraceL,
                               xfldElemL, qfldElemL),
                get<-1>(xfldElemTrace),
                rsdPDEElemL.data(), nIntegrandL );

      // scatter-add element residuals to global
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nIntegrandL );

      int nGlobal;
      for (int n = 0; n < nIntegrandL; n++)
      {
        nGlobal = mapDOFGlobalL[n];

        // Apply the Nishida HACK only adding volume integrals to "Left" residuals
        if (nGlobal >= dupPointOffset_)
          nGlobal = invPointMap_[nGlobal-dupPointOffset_];

        rsdPDEGlobal_[nGlobal] += rsdPDEElemL[n];
      }
    }
  }

protected:
  const IntegrandBoundaryTrace& fcn_;
  Vector<ArrayQR>& rsdPDEGlobal_;
  const int dupPointOffset_;
  const std::vector<int>& invPointMap_;
};

// Factory function

template<class IntegrandBoundaryTrace, template<class> class Vector, class ArrayQ>
ResidualBoundaryTrace_CG_Potential_Drela_impl<IntegrandBoundaryTrace, Vector, typename Scalar<ArrayQ>::type>
ResidualBoundaryTrace_CG_Potential_Drela( const IntegrandBoundaryTraceType<IntegrandBoundaryTrace>& fcn,
                                           Vector<ArrayQ>& rsdPDEGlobal, const int dupPointOffset,
                                           const std::vector<int>& invPointMap )
{
  typedef typename Scalar<ArrayQ>::type T;
  static_assert( std::is_same<ArrayQ, typename IntegrandBoundaryTrace::template ArrayQ<T> >::value, "These should be the same.");
  return {fcn.cast(), rsdPDEGlobal, dupPointOffset, invPointMap};
}


}

#endif  // RESIDUALBOUNDARYTRACE_CG_POTENTIAL_DRELA_H
