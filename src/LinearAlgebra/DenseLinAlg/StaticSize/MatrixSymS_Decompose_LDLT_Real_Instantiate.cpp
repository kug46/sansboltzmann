// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define MATRIXSYMS_DECOMPOSE_LDLT_INSTANTIATE
#include "MatrixSymS_Decompose_LDLT_impl.h"

#include "VectorS.h"

#include <boost/preprocessor/repetition/repeat_from_to.hpp>

namespace SANS
{
namespace DLA
{

//Explicit instantiation
#define DECL(z, n, text) template struct MatrixSymSLDLT<n,Real>;
BOOST_PP_REPEAT_FROM_TO(1, 17, DECL, )

}
}
