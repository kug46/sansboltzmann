// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php


#ifndef K_REORDER
#define K_REORDER

#include <petscsys.h>


#ifdef __cplusplus
extern "C"
{
#endif

/**
 * Generate k-reordering pattern for a matrix described by
 * Compressed Row Storage.
 *
 * @param nnodes  number of nodes (AKA rows) in the matrix partition
 * @param ia  array of indices into the values array for the first
 *            non-zero on each row
 * @param ja  array of column indices for each non-zero value
 * @param perm  P permutation array (typically applied to rows)
 **/
void
k_reorder(
  PetscInt nnodes,
  const PetscInt * ia,
  const PetscInt * ja,
  PetscInt * perm);
#ifdef __cplusplus
}
#endif

#endif // K_REORDER
