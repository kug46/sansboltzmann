// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "k_reorder.h"
#include "k_slat_calloc.h"

extern "C"
void
sortlist(
  PetscInt   nnodes,
  PetscInt * nodelist,
  PetscInt * idegree,
  PetscInt   istart,
  PetscInt   iend);

// ================================== k_reorder ================================80
//
// K numbering of the unknowns
// Need to return oldtonew and newtoold
// ported from Kyle Anderson's k_order.f90
//
// =============================================================================80
extern "C"
void
k_reorder(
  PetscInt    nnodes,
  const PetscInt *  ia,
  const PetscInt *  ja,
  PetscInt *  perm)
{
  PetscInt basenode = 0;
  PetscInt istart = 0, mindeg = 0;
  PetscInt new_min_node = 0;
  PetscInt sort_start = 0, sort_end = 0;
  PetscInt jstart = 0, jend = 0, icount = 0, nthis = 0, column = 0;
  PetscInt * idegree = nullptr;
  PetscInt * tag     = nullptr;
  PetscInt * list    = nullptr;

  SLAT_CALLOC(idegree, nnodes);
  SLAT_CALLOC(tag, nnodes);
  memset(tag, '\0', nnodes * sizeof(*tag));
  SLAT_CALLOC(list, nnodes);

  //
  // First find the minimum degree
  //
  for (PetscInt i = 0; i < nnodes; ++i)
  {
    jstart     = ia[i];
    jend       = ia[i + 1] - 1;
    idegree[i] = jend - jstart + 1;
  }
  //
  // Loop over all the nodes and find the starting one
  //
  istart = 0;
  mindeg = idegree[istart];
  for (PetscInt i = 0; i < nnodes; ++i)
  {
    if (idegree[i] < mindeg)
    {
      istart = i;
      mindeg = idegree[i];
    }
  }
  //     istart = 1
  //
  // We need to tag all the nodes so we can figure out when we
  // have renumbered them all. In cases with multiple partitions,
  // the nodes may be grouped disjointly
  //
  // Now go grab all the entries that connect to this node in the number list
  //
  //     write(6,*)"Entering reOrder"
  icount       = 0;
  list[icount] = istart; // This is newtoold()
  nthis        = 1;
  tag[istart]  = 1;
  sort_start   = 0;
  //
  // We have added the first node to our list of renumbered nodes
  // Now find the rest of them, starting with the first in our list
  // icount keeps track of how many nodes we have in our list
  // istart is the latest location in our list to start searching
  //
  jstart     = 0;
  jend       = jstart + nthis - 1;
  sort_start = 0;
g6000: // continue;
  //
  //     sort_start = jend + 1 // Keep track of beginning of the current level that needs to be sorted
  nthis = 0; // How many nodes on this level
  for (PetscInt i = jstart; i <= jend; ++i)
  {
    basenode = list[i];
    for (PetscInt j = ia[basenode]; j <= ia[basenode + 1] - 1; ++j)
    {
      column = ja[j];
      if (tag[column] == 0)
      {
        nthis        = nthis + 1;
        icount       = icount + 1;
        list[icount] = column;
        tag[column]  = 1;
      }
    }
  }
  //
  // If nothing was found, got through the tag array until we find a new starting point
  //
  if (nthis == 0)
  {
    mindeg = 10000000;
    for (PetscInt i = 0; i < nnodes; ++i)
    {
      if (tag[i] == 0 && idegree[i] < mindeg)
      {
        mindeg       = idegree[i];
        new_min_node = i;
      }
    }
    icount            = icount + 1;
    if (icount < nnodes)
    {
      list[icount]      = new_min_node;
      tag[new_min_node] = 1;
      nthis      = 1;
      jstart     = icount;
      jend       = jstart + nthis - 1;
      sort_start = icount;
      goto g6000;
    }
  }
  else
  {
    //
    // Sort the nodes we found for this level
    //
    sort_end = icount;
    sortlist(nnodes, list, idegree, sort_start, sort_end);
    sort_start = sort_end + 1;
    jstart     = jend + 1;
    jend       = jstart + nthis - 1;
    if (icount < nnodes)
      goto g6000;
  }

  //     list(nnodes+1) = nnodes + 1
  //     write(6,*)"Exiting reOrder"
  //
  // At this point, list(i) is a list of the old node
  // numbers stored in the new order
  // For example, if list() = {1,3,4,6,5,2}
  // then list(1) = old node 1
  //      list(2) = old node 3
  //      list(3) = old node 4
  //      list(4) = old node 6
  //      list(5) = old node 5
  //      list(6) = old node 2
  //
  //
  // newtoold is in the list array.
  //
  free(idegree);
  free(tag);
  for (PetscInt i = 0; i < nnodes; ++i)
  {
    perm[ list[i] ] = i;
  }
  //
  // free(idegree);
  // free(tag);
  free(list);
} // k_reorder

// ============================== SORTLIST ===================================
//
// Sorts the nodes on this level
// ported from Kyle Anderson's k_order.f90
//
// ===========================================================================
// subroutine sortlist(nnodes,nodelist,idegree,istart,iend)
extern "C"
void
sortlist(
  PetscInt   nnodes,
  PetscInt * nodelist,
  PetscInt * idegree,
  PetscInt   istart,
  PetscInt   iend)
{
  // PetscInt i, j;
  PetscInt node1, node2, iswap;

  //
  for (PetscInt i = istart; i <= iend; ++i)
  {
    node1 = nodelist[i];
    for (PetscInt j = i + 1; j <= iend; ++j)
    {
      node2 = nodelist[j];
      if (abs(idegree[node2]) <= abs(idegree[node1]) )
      {
        //         if(abs(idegree(node2)).gt.abs(idegree(node1)))then
        iswap       = nodelist[i];
        nodelist[i] = nodelist[j];
        nodelist[j] = iswap;
      }
    }
  }
  //
}
