// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SLAT_CALLOC

#include <cerrno>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits>

#define SLAT_ALIGN (64)

#define SLAT_CALLOC(ptr, nmemb)                                                \
  slat_calloc((void **)&(ptr), nmemb, sizeof(*(ptr)), #ptr, __FILE__, __LINE__)

static inline void
slat_calloc(void **ptr, const int64_t nmemb, const size_t size,
            const char *name, const char *file, const int32_t line)
{
  size_t numerator = ((size_t)nmemb * size);
  size_t ns = numerator % SLAT_ALIGN == 0 ? numerator / SLAT_ALIGN
                                          : numerator / SLAT_ALIGN + 1;

#ifdef _WIN32
  *ptr = _aligned_malloc((ns)*SLAT_ALIGN, SLAT_ALIGN);
  if (*ptr == NULL)
  {
    fprintf(stderr,
            "ERROR (memory): Unable to allocate memory [%zu bytes for %zd "
            "elements] for %s (%s, line %d) ns = %zd SLAT_ALIGN = %d "
            "insufficient memory.\n",
            ns * SLAT_ALIGN, nmemb, name, file, line,
            ns, SLAT_ALIGN);
#ifdef USING_MPI
    MPI_Abort(MPI_COMM_WORLD, -1);
#else
    exit(-1);
#endif
  }
#else
  int32_t err = posix_memalign(ptr, SLAT_ALIGN, (ns)*SLAT_ALIGN);
  if (err == ENOMEM)
  {
    fprintf(stderr,
            "ERROR (memory): Unable to allocate memory [%zu bytes for %ld "
            "elements] for %s (%s, line %d) ns = %ld SLAT_ALIGN = %d "
            "insufficient memory.\n",
            ns * SLAT_ALIGN, (long int)(nmemb), name, file, line,
            (long int)(ns), SLAT_ALIGN);
#ifdef USING_MPI
    MPI_Abort(MPI_COMM_WORLD, -1);
#else
    exit(-1);
#endif
  }
  else if (err == EINVAL)
  {
    fprintf(stderr,
            "ERROR (memory): Unable to allocate memory [%zu bytes for %ld "
            "elements] for %s (%s, line %d) ns = %ld SLAT_ALIGN = %d boundary "
            "is not a power of two.\n",
            ns * SLAT_ALIGN, (long int)(nmemb), name, file, line,
            (long int)(ns), SLAT_ALIGN);
#ifdef USING_MPI
    MPI_Abort(MPI_COMM_WORLD, -1);
#else
    exit(-1);
#endif
  }
#endif
}

#endif
