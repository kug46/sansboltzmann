// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(UMFPACK_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "UMFPACKSolver.h"

namespace SANS
{
namespace SLA
{
//-----------------------------------------------------------------------------
template< class Matrix_type >
LinearSolveStatus UMFPACK<Matrix_type>::solve(SparseVectorView_type& b, SparseVectorView_type& x)
{
  if ( staticCondensed_ )
  {
    factorize(b, transpose_);

    SANS_ASSERT( x.v0.m() == 3 && b.v0.m() == 3 ); //hardcode for VMSD

    typedef typename SparseVectorView_type::Vector0::node_type T;

    T bcondensed_v0_1 = b.v0(1);
    T bcondensed_v0_2 = b.v0(2);
    typename SparseVectorView_type::Vector0 bcondensed_v0(2);
    bcondensed_v0(0).resize(bcondensed_v0_1.m());
    bcondensed_v0(0) = bcondensed_v0_1;
    bcondensed_v0(1).resize(bcondensed_v0_2.m());
    bcondensed_v0(1) = bcondensed_v0_2;
    typename SparseVectorView_type::Vector1 bcondensed_v1 = b.v1;

    T xcondensed_v0_1 = x.v0(1);
    T xcondensed_v0_2 = x.v0(2);
    typename SparseVectorView_type::Vector0 xcondensed_v0(2);
    xcondensed_v0(0).resize(xcondensed_v0_1.m());
    xcondensed_v0(0) = xcondensed_v0_1;
    xcondensed_v0(1).resize(xcondensed_v0_2.m());
    xcondensed_v0(1) = xcondensed_v0_2;
    typename SparseVectorView_type::Vector1 xcondensed_v1 = x.v1;

    SparseVectorView_type bcondensed(bcondensed_v0, bcondensed_v1);
    SparseVectorView_type xcondensed(xcondensed_v0, xcondensed_v1);

    LinearSolveStatus status = backsolve(bcondensed, xcondensed);

    f_.completeUpdate(b, xcondensed, x);

    return status;
  }
  else
  {
    // Generate A
    factorize();
    // Solve Ax = b (for x)
    return backsolve( b, x );
  }
}

} // namespace SLA
} // namespace SANS
