// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//#undef SANS_AFLR

#include <boost/python/module.hpp>
#include <boost/python/exception_translator.hpp>
#include <boost/python/class.hpp>
#include <boost/python/def.hpp>
#include <boost/python/dict.hpp>

#include "UserInterfaces/LIPSolver.h"
#include "python_to_vector.h"

#include "Meshing/EGTess/WakedFarFieldBox.h"

namespace SANS
{

//=============================================================================
static void stdExceptionTranslator(std::exception const& e)
{
  PyErr_SetString(PyExc_RuntimeError, e.what());
}

//---------------------------------------------------------------------------//
LIPSolver::
LIPSolver(const std::string& egadsFile,
          const boost::python::list& boxsize,
          const PyDict& caseParams
          ) : context_((EGADS::CreateContext())),
              tessModel_( NULL ),
              pde_( 1, 0, 0 ), Sref_(0), Cref_(0), Bref_(0), Xref_(0), Yref_(0), Zref_(0)
{
  LIPCaseParams::checkInputs(caseParams);

  // construct the model
  EGADS::EGModel<3> model( context_, egadsFile );

  // add the farfield box
  bodies_ = EGADS::WakedFarFieldBox( model.getBodies(), to_std_vector<double>(boxsize), TrefftzFrames_ );

  // initialize the solver
  initMesh(caseParams);
}

//---------------------------------------------------------------------------//
boost::python::dict
LIPSolver::solve_dict(boost::python::dict& params)
{
  PyDict solveparams(params);

  return solve(solveparams);
}

}

//=============================================================================
BOOST_PYTHON_MODULE( PySANS_LIP )
{
  //Exception translation from C++ to Python
  boost::python::register_exception_translator<std::exception>(&SANS::stdExceptionTranslator);

  boost::python::class_<SANS::LIPSolver, boost::noncopyable >(
      "LIPSolver", boost::python::init<const std::string, boost::python::list, boost::python::dict>() )
      .def("solve", &SANS::LIPSolver::solve_dict)
      ;

}
