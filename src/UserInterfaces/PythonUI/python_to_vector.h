// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PYTHON_TO_VECTOR_H
#define PYTHON_TO_VECTOR_H

#include <vector>
#include <boost/python/stl_iterator.hpp>

namespace SANS
{

//=============================================================================
template< typename T >
inline
std::vector< T > to_std_vector( const boost::python::object& iterable )
{
    return std::vector< T >( boost::python::stl_input_iterator< T >( iterable ),
                             boost::python::stl_input_iterator< T >( ) );
}

}

#endif //PYTHON_TO_VECTOR_H
