// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include <boost/version.hpp>
#include <boost/python/module.hpp>
#include <boost/python/exception_translator.hpp>
#include <boost/python/class.hpp>
#include <boost/python/def.hpp>
#include <boost/python/stl_iterator.hpp>

#if BOOST_VERSION <= 106500
#include <boost/python/numeric.hpp>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/ndarrayobject.h> // numpy

// both numpy and scipy might define this macro...
#ifdef __STDC_FORMAT_MACROS
#undef __STDC_FORMAT_MACROS
#endif

#else
#include <boost/python/numpy.hpp>
#endif

#include "python_to_vector.h"

#include "NonLinearSolver/NewtonSolver.h"

#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/PDENavierStokes2D.h"
#include "pde/NS/BCNavierStokes2D.h"
#include "pde/NS/TraitsNavierStokes.h"
#include "pde/NDConvert/PDENDConvertSpace2D.h"
#include "pde/NDConvert/BCNDConvertSpace2D.h"

#include "Field/FieldArea_DG_Cell.h"
#include "Field/FieldArea_DG_InteriorTrace.h"
#include "Field/FieldArea_DG_BoundaryTrace.h"
#include "Field/FieldLiftArea_DG_Cell.h"

#include "Field/output_Tecplot.h"

#include "Discretization/HDG/AlgebraicEquationSet_HDG.h"
#include "SolutionTrek/TimeMarching/BackwardsDifference/BDF.h"

#include "unit/UnitGrids/XField2D_SymAirfoil_X1.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// NACA 4-digit rescaled via Rumsey; gives sharp TE at x = 1
//
//  t = nominal thickness (e.g. 0.12 gives NACA0012)

Real naca( Real x, Real t )
{
  Real y;

  y = 4.955743175*t*( 0.298222773*sqrt(x) - 0.127125232*x - 0.357907906*x*x + 0.291984971*x*x*x - 0.105174606*x*x*x*x );
  return y;
}


typedef QTypePrimitiveRhoPressure QType;
//  typedef QTypeConservative QType;
typedef QTypePrimitiveRhoPressure QType;
typedef ViscosityModel_Const ViscosityModelType;
typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelNavierStokesClass;
typedef PDENavierStokes2D<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> PDEClass;
typedef PDENDConvertSpace<PhysD2, PDEClass> NDPDEClass;
typedef NDPDEClass::ArrayQ<Real> ArrayQ;
typedef NDPDEClass::VectorArrayQ<Real> VectorArrayQ;


//this will be handled in BC class eventually...
typedef BCNavierStokes2DVector<TraitsSizeNavierStokes, TraitsModelNavierStokesClass> BCVector;

typedef AlgebraicEquationSet_HDG< NDPDEClass,BCNDConvertSpace, BCVector, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > PrimalEquationSetClass;
typedef PrimalEquationSetClass::BCParams BCParams;

typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;

typedef BDF< NDPDEClass, AlgEqSetTraits_Sparse, XField<PhysD2, TopoD2> > BDFClass;


//=============================================================================
static void stdExceptionTranslator(std::exception const& e)
{
  PyErr_SetString(PyExc_RuntimeError, e.what());
}

//---------------------------------------------------------------------------//
boost::python::object to_numpyArray( const std::vector<double>& vec )
{
#if BOOST_VERSION <= 106500
  npy_intp size = vec.size();

  // create a PyObject * from pointer and data
  PyObject * pyObj = PyArray_SimpleNewFromData( 1, &size, NPY_DOUBLE, const_cast<double*>(vec.data()) );
  boost::python::handle<> handle( pyObj );
  boost::python::numeric::array arr( handle );
#else
  namespace np = boost::python::numpy;

  np::ndarray arr = np::from_data(vec.data(), np::dtype::get_builtin<double>(),
                                  boost::python::make_tuple(vec.size()),
                                  boost::python::make_tuple(sizeof(double)),
                                  boost::python::object());
#endif

  // The problem of returning arr is twofold: firstly the user can modify
  // the data which will betray the const-correctness
  // Secondly the lifetime of the data is managed by the C++ API and not the
  // lifetime of the numpy array whatsoever. But we have a simple solution..

  return arr.copy(); // copy the object. numpy owns the copy now.
}

//=============================================================================
class NACASolver : public boost::noncopyable
{
protected:

  const Real gamma = 1.4;
  const Real R     = 1.0;        // J/(kg K)

  const Real muRef = 0.001;
  const Real Prandtl = 0.72;

  GlobalTime time_;
public:

//---------------------------------------------------------------------------//
  NACASolver() :
    gas(gamma, R), visc(muRef), tcond(Prandtl, gas.Cp()),
    pde( time_, gas, visc, tcond, Euler_ResidInterp_Raw ),
    pxfld_(NULL),
    pqfld_(NULL) ,pafld_(NULL), pqIfld_(NULL), plgfld_(NULL),
    pPrimalEqSet_(NULL), pBDF_(NULL)
  {

    Real xle = 0.0;
    Real xte = 1.0;
    Real t = 0.12;

    int iiairf = 256;
    std::vector< DLA::VectorS<2,Real> > airf(iiairf+1);

    // airfoil: cosine distribution
    Real x,y;
    for ( int i = 0; i <= iiairf; i++)
    {
      x = xle + (xte - xle)*(0.5 - 0.5*cos(PI*i/((Real) iiairf)));
      y = naca( x, t );
      if (i == iiairf)    // needed for roundoff
      {
        x = xte;
        y = 0;
      }

      airf[i][0] =  x;
      airf[i][1] =  y;
    }

    Real rffd = 50;
    int iistag = 32;
    int iiwake = 64;
    int iiffd = 16;
    pxfld_ = new XField2D_SymAirfoil( airf, rffd, iistag, iiwake, iiffd );

    const Real M0 = 0.3;
    const Real rho0 = 1;
    const Real u0 = 1;
    const Real v0 = 0;
    const Real p0 = 1.0/(gamma*M0*M0);

    const int order = 1;

    // solution field at inlet
    ArrayQ q0;
    pde.setDOFFrom( q0, DensityVelocityPressure2D<Real>(rho0, u0, v0, p0) );



    // HDG discretization
    DiscretizationHDG<NDPDEClass> disc( pde, Local, Gradient );


    // BCS
    PyDict qRef;
    qRef[NSVariableType2DParams::params.StateVector.Variables] = NSVariableType2DParams::params.StateVector.PrimitivePressure;
    qRef[DensityVelocityPressure2DParams::params.rho] = rho0;
    qRef[DensityVelocityPressure2DParams::params.u] = u0;
    qRef[DensityVelocityPressure2DParams::params.v] = v0;
    qRef[DensityVelocityPressure2DParams::params.p] = p0;


    // Create a BC dictionary
    PyDict BCFarField;
    BCFarField[BCParams::params.BC.BCType] = BCParams::params.BC.FullState;
    BCFarField[BCEuler2DFullStateParams<NSVariableType2DParams>::params.Characteristic] = true;
    BCFarField[BCEuler2DFullStateParams<NSVariableType2DParams>::params.StateVector] = qRef;

    PyDict BCWall;
    BCWall[BCParams::params.BC.BCType] = BCParams::params.BC.WallNoSlipAdiabatic_mitState;

    PyDict BCList;
    BCList["Wall"] = BCWall;
    BCList["FarField"] = BCFarField;

    std::map<std::string, std::vector<int>> BCBoundaryGroups;

    BCBoundaryGroups["Wall"]     = {XField2D_SymAirfoil::iAirfoilBoundaryGroup};
    BCBoundaryGroups["FarField"] = {XField2D_SymAirfoil::iFarfieldBoundaryGroup};

    BCParams::checkInputs( BCList );

    // Set up Newton Solver
    PyDict UMFPACKDict;

    UMFPACKDict[SLA::LinearSolverParam::params.LinearSolver.Solver] = SLA::LinearSolverParam::params.LinearSolver.UMFPACK;

    PyDict NewtonLineUpdateDict;
    NewtonLineUpdateDict[LineUpdateParam::params.LineUpdate.Method] = LineUpdateParam::params.LineUpdate.HalvingSearch;

    PyDict NewtonSolverDict;
    NewtonSolverDict[NewtonSolverParam::params.LinearSolver] = UMFPACKDict;
    NewtonSolverDict[NewtonSolverParam::params.LineUpdate] = NewtonLineUpdateDict;
    NewtonSolverDict[NewtonSolverParam::params.MinIterations] = 0;
    NewtonSolverDict[NewtonSolverParam::params.MaxIterations] = 100;
    NewtonSolverDict[NewtonSolverParam::params.Verbose] =  true;

    NewtonSolverParam::checkInputs(NewtonSolverDict);

    PyDict NonLinearSolverDict;
    NonLinearSolverDict[NonLinearSolverParam::params.NonLinearSolver] = NewtonSolverDict;

    pqfld_ = new Field_DG_Cell<PhysD2, TopoD2, ArrayQ>(*pxfld_, order, BasisFunctionCategory_Legendre);
    pafld_ = new Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ>(*pxfld_, order, BasisFunctionCategory_Legendre);
    pqIfld_ = new Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ>(*pxfld_, order, BasisFunctionCategory_Legendre);
    plgfld_ = new Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>(*pxfld_, order, BasisFunctionCategory_Legendre,
                                                                 BCParams::getLGBoundaryGroups(BCList, BCBoundaryGroups) );

    *pqfld_ = q0;
    *pafld_ = 0;
    *pqIfld_ = q0;
    *plgfld_ = 0;

    QuadratureOrder quadratureOrder( *pxfld_, -1 );

    std::vector<Real> tol = {1e-9, 1e-9, 1e-9, 1e-9};

    std::vector<int> interiorTraceGroups;
    for ( int i = 0; i < pxfld_->nInteriorTraceGroups(); i++)
      interiorTraceGroups.push_back(i);

    pPrimalEqSet_ = new PrimalEquationSetClass(*pxfld_, *pqfld_, *pafld_, *pqIfld_, *plgfld_, pde, disc, quadratureOrder, tol,
                                               {0}, interiorTraceGroups, BCList, BCBoundaryGroups, time_);

    int BDForder = 1;
    Real dt = 1;

    pBDF_ = new BDFClass(BDForder, dt, time_, *pxfld_, *pqfld_, NonLinearSolverDict, pde, quadratureOrder, {0}, *pPrimalEqSet_);
  }

//---------------------------------------------------------------------------//
  boost::python::object state()
  {
    std::vector<double> qvec(ArrayQ::M*pqfld_->nDOF()*(pBDF_->nFieldPast()+1));

    for (int j = 0; j < pqfld_->nDOF(); j++ )
    {
      for (int i = 0; i < ArrayQ::M; i++)
        qvec[j*ArrayQ::M + i] = pqfld_->DOF(j)[i];
    }

    for (int k = 0; k < pBDF_->nFieldPast(); k++)
    {
      const int nDOF = pBDF_->qfldpast(k).nDOF();
      for (int j = 0; j < nDOF; j++)
        for (int i = 0; i < ArrayQ::M; i++)
          qvec[((k+1)*nDOF + j)*ArrayQ::M + i] = pBDF_->qfldpast(k).DOF(j)[i];
    }

    return to_numpyArray(qvec);
  }

//---------------------------------------------------------------------------//
  boost::python::object iterate(const boost::python::object& q0, int nTimeSteps)
  {
    // Copy over the solution and march the requested number of time steps

    std::vector<double> qvec(to_std_vector<double>(q0));
    SANS_ASSERT( (int)qvec.size() == (ArrayQ::M*pqfld_->nDOF()*(pBDF_->nFieldPast()+1)));

    for (int j = 0; j < pqfld_->nDOF(); j++)
    {
      for (int i = 0; i < ArrayQ::M; i++)
        pqfld_->DOF(j)[i] = qvec[j*ArrayQ::M + i];
    }

    for (int k = 0; k < pBDF_->nFieldPast(); k++)
    {
      const int nDOF = pBDF_->qfldpast(k).nDOF();
      for (int j = 0; j < nDOF; j++)
        for (int i = 0; i < ArrayQ::M; i++)
          pBDF_->qfldpast(k).DOF(j)[i] = qvec[((k+1)*nDOF + j)*ArrayQ::M + i];
    }


    pBDF_->march(nTimeSteps);

    return state();
  }

//---------------------------------------------------------------------------//
  void dumpTecplot(const std::string& filename)
  {
    output_Tecplot( *pqfld_, filename );
  }

//---------------------------------------------------------------------------//
  ~NACASolver()
  {
    delete pxfld_;
    delete pqfld_;
    delete pafld_;
    delete pqIfld_;
    delete plgfld_;
    delete pPrimalEqSet_;
    delete pBDF_;
  }

protected:

  GasModel gas;
  ViscosityModelType visc;
  ThermalConductivityModel tcond;
  NDPDEClass pde;

  XField2D_SymAirfoil *pxfld_;

  Field_DG_Cell<PhysD2, TopoD2, ArrayQ> *pqfld_;
  Field_DG_Cell<PhysD2, TopoD2, VectorArrayQ> *pafld_;
  Field_DG_InteriorTrace<PhysD2, TopoD2, ArrayQ> *pqIfld_;
  Field_DG_BoundaryTrace<PhysD2, TopoD2, ArrayQ>  *plgfld_;
  PrimalEquationSetClass *pPrimalEqSet_;
  BDFClass *pBDF_;
};

}

#if BOOST_VERSION <= 106500
// HACK needed for python 3, should be fixed in boost 1.63, which will also have a new interface
#if PY_VERSION_HEX >= 0x03000000
void *
#else
void
#endif
initialize()
{
  import_array();
#if PY_VERSION_HEX >= 0x03000000
  return NULL;
#endif
}
#endif // BOOST_VERSION

//=============================================================================
BOOST_PYTHON_MODULE( PySANS_NS_NACA0012 )
{
#if BOOST_VERSION <= 106500
  // numpy requires this
  boost::python::numeric::array::set_module_and_type("numpy", "ndarray");
  initialize();
#else
  boost::python::numpy::initialize();
#endif

  //Exception translation from C++ to Python
  boost::python::register_exception_translator<std::exception>(&SANS::stdExceptionTranslator);

  boost::python::class_<SANS::NACASolver, boost::noncopyable >(
      "NACASolver" )
      .def("iterate", &SANS::NACASolver::iterate)
      .def("state", &SANS::NACASolver::state)
      .def("dumpTecplot", &SANS::NACASolver::dumpTecplot)
      ;

}
