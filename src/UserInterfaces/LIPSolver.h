// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SANS_LIPSOLVER_H
#define SANS_LIPSOLVER_H

#undef SANS_PETSC

#include <memory> // std::shared_ptr

#include "tools/noncopyable.h"

#include "Meshing/EGADS/EGContext.h"
#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGTess/EGTessModel.h"

#include "Meshing/AFLR/AFLR3.h"
#include "Meshing/TetGen/EGTetGen.h"

#include "pde/FullPotential/BCLinearizedIncompressiblePotential3D.h"
#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"

#include "pde/NDConvert/PDENDConvertSpace3D.h"
#include "pde/NDConvert/BCNDConvertSpace3D.h"

#include "Discretization/Potential_Drela/AlgebraicEquationSet_CG_Potential_Drela.h"
#include "Discretization/Potential_Drela/IntegrandBoundaryFrame_CG_LIP_Drela.h"
#include "Discretization/Potential_Drela/IntegrandBoundaryTrace_WakeCut_CG_LIP_Drela.h"
#include "Discretization/IntegrateBoundaryTraceGroups.h"

#ifdef SANS_PETSC
#include "LinearAlgebra/SparseLinAlg/PETSc/PETScSolver.h"
#elif INTEL_MKL
#include "LinearAlgebra/SparseLinAlg/Direct/MKL_PARDISO/MKL_PARDISOSolver.h"
#else
#include "LinearAlgebra/SparseLinAlg/Direct/UMFPACK/UMFPACKSolver.h"
#endif

namespace SANS
{


//=============================================================================
class LIPSolver : public noncopyable
{
public:
  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D<Real>> PDEClass;
  typedef PDEClass::template ArrayQ<Real> ArrayQ;

  typedef AlgebraicEquationSet_CG_Potential_Drela<PDEClass, BCNDConvertSpace, BCLinearizedIncompressiblePotential3DVector<Real>,
      AlgEqSetTraits_Sparse, XField3D_Wake> PrimalEquationSetClass;
  typedef PrimalEquationSetClass::BCParams BCParams;
  typedef PrimalEquationSetClass::SystemMatrix SystemMatrixClass;
  typedef PrimalEquationSetClass::SystemVector SystemVectorClass;
  typedef PrimalEquationSetClass::SystemNonZeroPattern SystemNonZeroPattern;


#ifdef SANS_AFLR

//---------------------------------------------------------------------------//
  struct AFLR3Params : noncopyable
  {
    static void checkInputs(PyDict d) {}
  };
#endif

//---------------------------------------------------------------------------//
  struct TetGenParams : noncopyable
  {
    const ParameterNumeric<Real> Quality_Rad_Edge{"Quality_Rad_Edge", 1.5, 1, NO_LIMIT, "Maximum radius-edge ratio"};
    const ParameterNumeric<Real> Quality_Angle{"Quality_Angle", 0, 0, 45, "Minimum dihedral angle (in degrees)"};

    static void checkInputs(PyDict d);
    static TetGenParams params;
  };

//---------------------------------------------------------------------------//
  struct LIPCaseParams : EGADS::EGTessModelParams
  {
    const ParameterNumeric<Real> Sref{"Sref", NO_DEFAULT, 0, NO_LIMIT, "Reference Area"};
    const ParameterNumeric<Real> Cref{"Cref", NO_DEFAULT, 0, NO_LIMIT, "Reference Chord"};
    const ParameterNumeric<Real> Bref{"Bref", NO_DEFAULT, 0, NO_LIMIT, "Reference Span"};

    const ParameterNumeric<Real> Xref{"Xref", NO_DEFAULT, NO_RANGE, "Moment Reference x-location"};
    const ParameterNumeric<Real> Yref{"Yref", NO_DEFAULT, NO_RANGE, "Moment Reference y-location"};
    const ParameterNumeric<Real> Zref{"Zref", NO_DEFAULT, NO_RANGE, "Moment Reference z-location"};

    const ParameterDict BCDict{"BCDict", NO_DEFAULT, BCParams::checkInputs, "Boundary Condition Definitions Dictionary"};

    using EGADS::EGTessModelParams::SurfaceMesher;

    struct VolumeMesherOptions
    {
      typedef DictKeyPair ExtractType;
      const ParameterString Name{"Name", "TetGen", "Volume Mesh Generator Name"};
      const ParameterString& key = Name;

      const DictOption TetGen{"TetGen", TetGenParams::checkInputs};
#ifdef SANS_AFLR
      const DictOption AFLR3{"AFLR3", AFLR3Params::checkInputs};
#endif

      const std::vector<DictOption> options{TetGen
#ifdef SANS_AFLR
                                           , AFLR3
#endif
                                           };
    };
    const ParameterOption<VolumeMesherOptions> VolumeMesher{"VolumeMesher", "TetGen", "Volume mesh generator"};

    // cppcheck-suppress passedByValue
    static void checkInputs(PyDict d);
    static LIPCaseParams params;
  };

//---------------------------------------------------------------------------//
  struct LIPSolveParams : SANS::noncopyable
  {
    const ParameterNumeric<Real> alpha{"alpha", NO_DEFAULT, -180, 180, "Angle of Attack"};
    const ParameterNumeric<Real> beta{"beta", 0, -180, 180, "Sideslip Angle"};

    const ParameterNumeric<Real> rollRate {"rollRate" , 0, NO_RANGE, "Roll rate"};
    const ParameterNumeric<Real> pitchRate{"pitchRate", 0, NO_RANGE, "Pitch rate"};
    const ParameterNumeric<Real> yawRate  {"yawRate"  , 0, NO_RANGE, "Yaw rate"};

    // cppcheck-suppress passedByValue
    static void checkInputs(PyDict d)
    {
      std::vector<const ParameterBase*> allParams;
      allParams.push_back(d.checkInputs(params.alpha));
      allParams.push_back(d.checkInputs(params.beta));

      allParams.push_back(d.checkInputs(params.rollRate));
      allParams.push_back(d.checkInputs(params.pitchRate));
      allParams.push_back(d.checkInputs(params.yawRate));

      d.checkUnknownInputs(allParams);
    }
    static LIPSolveParams params;
  };

//---------------------------------------------------------------------------//
  LIPSolver(const std::string& egadsFile,
            const boost::python::list& boxsize,
            const PyDict& caseParams);

  LIPSolver(const std::vector<EGADS::EGBody<3>>& bodies,
            const std::vector<Real>& boxsize,
            const PyDict& caseParams);

  LIPSolver(const std::vector<EGADS::EGBody<3>>& bodies,
            const PyDict& caseParams);

  LIPSolver(std::shared_ptr<XField3D_Wake>& pxfld,
            const std::map<std::string, std::vector<int> >& BCFaces,
            const std::vector<int>& WakeFaces,
            const std::vector<int>& TrefftzFrames,
            const PyDict& caseParams );

  void writeTecplot(const std::string& filename) const;

  boost::python::dict solve_dict(boost::python::dict& params);

  PyDict solve(PyDict& params);

  void StabilityDerivatives(PyDict& result) const;

  ~LIPSolver();

  const EGADS::EGTessModel& getTessModel() const { return *tessModel_; }
  const Field_CG_Cell<PhysD3, TopoD3, ArrayQ>& qfld() { return *pqfld_; }
  const PDEClass& pde() { return pde_; }

protected:

  void initMesh(const PyDict& caseParams);

  void init(const PyDict& caseParams);

  EGADS::EGContext context_;
  std::vector<EGADS::EGBody<3>> bodies_;
  EGADS::EGTessModel* tessModel_;
#ifdef SANS_PETSC
  SLA::PETScSolver<SystemMatrixClass> *solver_;
#elif INTEL_MKL
  SLA::MKL_PARDISO<SystemMatrixClass> *solver_;
#else
  SLA::UMFPACK<SystemMatrixClass> *solver_;
#endif
  PDEClass pde_;
  StabilizationNitsche stab_;

  PyDict PyBCList_;
  std::map<std::string, std::vector<int> > BCFaces_;
  std::vector<int> WakeFaces_;

  std::shared_ptr<XField3D_Wake> pxfld_;
  Field_CG_Cell<PhysD3, TopoD3, ArrayQ> *pqfld_;
  Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ> *plgfld_;
  PrimalEquationSetClass *pPrimalEqSet_;

  Real Sref_, Cref_, Bref_;
  Real Xref_, Yref_, Zref_;

  Real alpha_, beta_;
  Real rollr_, pitchr_, yawr_;

  std::vector<int> TrefftzFrames_;
  std::vector<int> solidFaces_;
};

} // namespace SANS

#endif //SANS_LIPSOLVER_H
