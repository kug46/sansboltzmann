// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifdef _WIN32
#define WIN32 // aimUtil uses WIN32 to identify windoze
#endif

#include <aimUtil.h>

#include <string.h> // strcasecmp

#include "UserInterfaces/LIPSolver.h"
#include "Meshing/EGADS/EGBody.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include <memory> // shared_ptr
#include <vector>

#include <boost/python/str.hpp>
#include <boost/python/dict.hpp>
#include <boost/python/extract.hpp>

static std::vector<std::shared_ptr<SANS::LIPSolver>> solvers;
static std::vector<SANS::PyDict> outputs;

#define NUMINPUT  11                          /* number of inputs */

#define NUMJ      16
#define NUMB      6                           /* number of body axis forces and moments */
#define NUMBD     6                           /* number of body axis derivatives */
#define NUMS      5                           /* number of stability axis forces and moments */
#define NUMSD     2                           /* number of stability axis derivatives */
#define NUMOUT    NUMJ+NUMB*NUMBD+NUMS*NUMSD  /* number of outputs */

//#define DEBUG

#define CROSS(a,b,c)      a[0] = (b[1]*c[2]) - (b[2]*c[1]);\
                          a[1] = (b[2]*c[0]) - (b[0]*c[2]);\
                          a[2] = (b[0]*c[1]) - (b[1]*c[0])
#define DOT(a,b)         (a[0]*b[0] + a[1]*b[1] + a[2]*b[2])

namespace // no-name namespace restricts to this file
{

void checkAndCreateMesh(int inst, void* aimInfo)
{
  int status, nBody=0;
  int atype, alen;
  const int    *ints;
  const char   *string, *intents;
  const double *reals;
  ego     *bodies;

  // Get AIM bodies
  status = aim_getBodies(aimInfo, &intents, &nBody, &bodies);
  if (status != CAPS_SUCCESS)
    SANS_RUNTIME_EXCEPTION(" sansLIPAIM: aim_getBodies = %d\n", status);

  if (nBody < 1)
    SANS_RUNTIME_EXCEPTION(" sansLIPAIM: There must be at least one body: instance = %d, nBody = %d!\n",
           inst, nBody);

  bool needMesh = true;
  for (int i = 0; i < nBody; i++)
    needMesh = needMesh && (bodies[nBody+i] == NULL);

  // the mesh has already been generated
  if (needMesh == false) return;

  // Initialize reference values
  double Sref = 1.0, Cref = 1.0, Bref = 1.0;
  double Xref = 0.0, Yref = 0.0, Zref = 0.0;

  // Loop over bodies and look for reference quantity attributes on any body
  for (int i=0; i < nBody; i++)
  {
    status = EG_attributeRet(bodies[i], "capsReferenceArea", &atype, &alen, &ints, &reals, &string);
    if (status == EGADS_SUCCESS) Sref = reals[0];

    status = EG_attributeRet(bodies[i], "capsReferenceChord", &atype, &alen, &ints, &reals, &string);
    if (status == EGADS_SUCCESS) Cref = reals[0];

    status = EG_attributeRet(bodies[i], "capsReferenceSpan", &atype, &alen, &ints, &reals, &string);
    if (status == EGADS_SUCCESS) Bref = reals[0];

    status = EG_attributeRet(bodies[i], "capsReferenceX", &atype, &alen, &ints, &reals, &string);
    if (status == EGADS_SUCCESS) Xref = reals[0];

    status = EG_attributeRet(bodies[i], "capsReferenceY", &atype, &alen, &ints, &reals, &string);
    if (status == EGADS_SUCCESS) Yref = reals[0];

    status = EG_attributeRet(bodies[i], "capsReferenceZ", &atype, &alen, &ints, &reals, &string);
    if (status == EGADS_SUCCESS) Zref = reals[0];
  }

  SANS::PyDict caseParams;

  caseParams[SANS::LIPSolver::LIPCaseParams::params.Sref] = Sref;
  caseParams[SANS::LIPSolver::LIPCaseParams::params.Cref] = Cref;
  caseParams[SANS::LIPSolver::LIPCaseParams::params.Bref] = Bref;

  caseParams[SANS::LIPSolver::LIPCaseParams::params.Xref] = Xref;
  caseParams[SANS::LIPSolver::LIPCaseParams::params.Yref] = Yref;
  caseParams[SANS::LIPSolver::LIPCaseParams::params.Zref] = Zref;

  capsValue *Surface_Mesher = NULL;
  aim_getValue(aimInfo, aim_getIndex(aimInfo, "Surface_Mesher", ANALYSISIN), ANALYSISIN, &Surface_Mesher);
  SANS::PyDict SurfaceMesherDict;
  SurfaceMesherDict.json_parse(Surface_Mesher->vals.string);
  caseParams[SANS::LIPSolver::LIPCaseParams::params.SurfaceMesher] = SurfaceMesherDict;

  capsValue *Volume_Mesher = NULL;
  aim_getValue(aimInfo, aim_getIndex(aimInfo, "Volume_Mesher", ANALYSISIN), ANALYSISIN, &Volume_Mesher);
  SANS::PyDict VolumeMesherDict;
  VolumeMesherDict[SANS::LIPSolver::LIPCaseParams::params.VolumeMesher.Name] = std::string(Volume_Mesher->vals.string);
  caseParams[SANS::LIPSolver::LIPCaseParams::params.VolumeMesher] = VolumeMesherDict;


  typedef SANS::LIPSolver::BCParams BCParams;
  typedef SANS::BCLinearizedIncompressiblePotential3DParams<SANS::BCTypeNeumann>   BCNeumannParamsType;
  typedef SANS::BCLinearizedIncompressiblePotential3DParams<SANS::BCTypeDirichlet> BCDirichletParamsType;
  typedef SANS::BCLinearizedIncompressiblePotential3DParams<SANS::BCTypeControl>   BCControlParamsType;

  // BC
  SANS::PyDict BCOutflowDict;
  BCOutflowDict[BCParams::params.BC.BCType] = BCParams::params.BC.Neumann;
  BCOutflowDict[BCNeumannParamsType::params.gradqn] = 0;

  SANS::PyDict BCInflowDict;
  BCInflowDict[BCParams::params.BC.BCType] = BCParams::params.BC.Dirichlet;
  BCInflowDict[BCDirichletParamsType::params.q] = 0;

  capsValue *pBoundary_Condition = NULL;
  aim_getValue(aimInfo, aim_getIndex(aimInfo, "Boundary_Condition", ANALYSISIN), ANALYSISIN, &pBoundary_Condition);
  if (pBoundary_Condition->nullVal == IsNull)
    SANS_RUNTIME_EXCEPTION("Boundary conditions must be specified\n");

  SANS::PyDict PyBCList;
  std::string Boundary_Condition = pBoundary_Condition->vals.string;
  PyBCList.json_parse(Boundary_Condition);

  // Extract the keys from the dictionary
  std::vector<std::string> keys = PyBCList.stringKeys();

  for ( const std::string& key : keys )
  {
    boost::python::api::object obj = PyBCList[key];
    boost::python::extract<boost::python::dict> val(obj);
    if ( !val.check() )
    {
      std::stringstream msg;
      msg << "Boundary_Condition must be a dictionary of dictionaries." << std::endl << std::endl;
      msg << "key '" << key << "' contains : ";
      msg << (std::string)boost::python::extract<std::string>( boost::python::str( obj ) );
      SANS_RUNTIME_EXCEPTION(msg.str());
    }

    SANS::PyDict BCDict( static_cast<boost::python::dict&>(obj) );

    // SANSify the CAPS 'standard'
    if (BCDict.has_key("bcType"))
    {
      BCDict["BCType"] = BCDict["bcType"];
      BCDict["bcType"].del();
      PyBCList[key] = BCDict;
    }
  }
  // Add boundary conditions on the box
  PyBCList["Inflow"] = BCInflowDict;
  PyBCList["Outflow"] = BCOutflowDict;


  std::vector<SANS::EGADS::EGBody<3>> egBodies;

  // Used to compute hinges from the geometry
  //std::map<std::string, SANS::EGADS::EGEdge<3>> hingeLower, hingeUpper;

  // make copies of all the bodes as models eat bodies
  for (int i = 0; i < nBody; i++)
  {
    SANS::EGADS::EGBody<3> body = SANS::EGADS::EGObject( bodies[i] );

    // Wire bodies specify control surface hinge lines
    if ( body.isWire() )
    {
      std::string nameControl;
      body.getAttribute("capsGroup", nameControl);
      std::vector<SANS::EGADS::EGEdge<3>> edges = body.getEdges();

      if (edges.size() != 1)
        SANS_RUNTIME_EXCEPTION("WIREBODY with 'capsGruop'=%s should only contain a single edge! Found %ld edges.\n",
                               nameControl.c_str(), edges.size());

      SANS::PyDict BCControl;
      if ( PyBCList.has_key(nameControl) )
      {
        // Create a BC parameter for the given key
        const SANS::ParameterOption<BCParams::BCOptions> BCParam{nameControl, NO_DEFAULT, "Boundary Condition Dictionary"};

        BCControl = PyBCList.get(BCParam);
      }

      BCControl[BCParams::params.BC.BCType] = BCParams::params.BC.ControlSurface;

      if ( !BCControl.has_key(BCControlParamsType::params.hinge) )
      {
        std::vector<SANS::EGADS::EGNode<3>> nodes = body.getNodes();
        SANS_ASSERT( nodes.size() == 2 );

        typedef SANS::EGADS::EGNode<3>::CartCoord CartCoord;

        // Per John's definition, hinge vectors always point in the positive y-direction
        if ( ((CartCoord)nodes[1])[1] < ((CartCoord)nodes[0])[1] )
          std::swap(nodes[0], nodes[1]);

        CartCoord dX = (CartCoord)nodes[1] - (CartCoord)nodes[0];
        dX /= sqrt(SANS::dot(dX,dX));
        std::vector<Real> hinge = {dX[0], dX[1], dX[2]};

        BCControl[BCControlParamsType::params.hinge] = hinge;
      }

      // Get the control surface deflection from the hinge if it is present and not already set
      if ( !BCControl.has_key(BCControlParamsType::params.deflect) )
      {
        if ( body.hasAttribute(BCControlParamsType::params.deflect) && edges[0].hasAttribute(BCControlParamsType::params.deflect))
          SANS_RUNTIME_EXCEPTION("Wirebody with 'capsGruop'=%s has 'deflect' attribute both on the body and edge!\n", nameControl.c_str());

        double deflect;
        if ( body.hasAttribute(BCControlParamsType::params.deflect) )
          body.getAttribute(BCControlParamsType::params.deflect, deflect);
        else
          edges[0].getAttribute(BCControlParamsType::params.deflect, deflect);

        BCControl[BCControlParamsType::params.deflect] = deflect;
      }

      //std::cout << nameControl << " " << BCControl.str() << std::endl;

      PyBCList[nameControl] = BCControl;
      continue;
    }

    egBodies.emplace_back( body );

    std::vector<SANS::EGADS::EGFace<3>> faces = body.getFaces();
    for (SANS::EGADS::EGFace<3>& face : faces)
    {
      if (body.isSheet())
      {
        if (face.hasAttribute("capsGroup"))
        {
          std::string WakeName;
          face.getAttribute("capsGroup", WakeName);
          face.addAttribute("Wake", WakeName);
          face.addAttribute("BCName", WakeName);
        }
      }
      else if (face.hasAttribute("capsGroup"))
      {
        std::string BCName;
        face.getAttribute("capsGroup", BCName);
        face.addAttribute("BCName", BCName);

#ifdef SANS_AFLR
        // Identify "farfield" boundaries for AFLR
        if (BCName == "Inflow" || BCName == "Outflow")
          face.addAttribute(AFLR_FARFIELD, 1);
#endif
      }
    }

    if (body.isSheet())
    {
      if (body.hasAttribute("capsGroup"))
      {
        std::string WakeName;
        body.getAttribute("capsGroup", WakeName);
        body.addAttribute("Wake", WakeName);
        body.addAttribute("BCName", WakeName);
      }
    }
    else if (body.hasAttribute("capsGroup"))
    {
      std::string BCName;
      body.getAttribute("capsGroup", BCName);
      body.addAttribute("BCName", BCName);
    }

#if 0
    // Find all edges used for implicit hinge calculations
    std::vector<SANS::EGADS::EGEdge<3>> edges = body.getEdges();
    for (SANS::EGADS::EGEdge<3>& edge : edges)
    {
      if (edge.hasAttribute("HingeUpper"))
      {
        std::string ControlSurf;
        edge.getAttribute("HingeUpper", ControlSurf);
        hingeUpper.emplace(std::make_pair(ControlSurf, edge));
      }

      if (edge.hasAttribute("HingeLower"))
      {
        std::string ControlSurf;
        edge.getAttribute("HingeLower", ControlSurf);
        hingeLower.emplace(std::make_pair(ControlSurf, edge));
      }
    }
#endif
  }

  caseParams[SANS::LIPSolver::LIPCaseParams::params.BCDict] = PyBCList;

#if 0
  for (std::pair<std::string, SANS::EGADS::EGEdge<3>>& upper : hingeUpper)
  {
    std::pair<std::string, SANS::EGADS::EGEdge<3>>& lower = hingeLower.at(upper.first);

    SANS::EGADS::EGEdge<3>::JType dXu = upper.second.J(upper.second.midRange());
    SANS::EGADS::EGEdge<3>::JType dXl = lower.second.J(lower.second.midRange());

    //if (dXu[1] > 0 && SANS::dot(dXu, dy) < 0) dXu = -dXu;
  }
#endif

  solvers[inst] = std::make_shared<SANS::LIPSolver>(egBodies, caseParams);

  const SANS::EGADS::EGTessModel& tessModel = solvers[inst]->getTessModel();

  // Assign the tessellation from the solver to CAPS
  // bodies array might contain control surface lines, so must be careful to match
  // solid and sheet bodies that were used in the LIPSolver
  for (std::size_t ibody = 0; ibody < egBodies.size(); ibody++)
  {
    ego tess = tessModel.getTessEgo( egBodies[ibody] );

    status = aim_setTess(aimInfo, tess);
    if (status != EGADS_SUCCESS)
      SANS_RUNTIME_EXCEPTION(" sansLIPAIM: aim_setTess = %d\n", status);
  }
}

} // namespace


extern "C"
{

// ********************** AIM Function Break *****************************
int
aimInitialize(int ngIn, capsValue *gIn, int *qeFlag, const char *unitSys,
              int *nIn, int *nOut, int *nFields, char ***fnames, int **ranks)
{
  int *ints, flag;
  char **strs;

#ifdef DEBUG
  printf("\n sansLIPAIM/aimInitialize   ngIn = %d!\n", ngIn);
#endif
  flag    = *qeFlag;
  *qeFlag = 0;

  /* specify the number of analysis input and out "parameters" */
  *nIn     = NUMINPUT;
  *nOut    = NUMOUT;
  if (flag == 1) return CAPS_SUCCESS;

  /* specify the field variables this analysis can generate */
  *nFields = 4;
  ints     = (int *) EG_alloc(*nFields*sizeof(int));
  if (ints == NULL)
    return EGADS_MALLOC;

  ints[0]  = 1;
  ints[1]  = 1;
  ints[2]  = 1;
  ints[3]  = 1;
  *ranks   = ints;

  strs     = (char **) EG_alloc(*nFields*sizeof(char *));
  if (strs == NULL)
  {
    EG_free(*ranks);
    *ranks   = NULL;
    return EGADS_MALLOC;
  }

  strs[0]  = EG_strdup("Pressure");
  strs[1]  = EG_strdup("P");
  strs[2]  = EG_strdup("Cp");
  strs[3]  = EG_strdup("CoefficientofPressure");
  *fnames  = strs;

  // increment the number of instances
  solvers.push_back(NULL);
  outputs.resize(solvers.size());

  return CAPS_SUCCESS;
}

// ********************** AIM Function Break *****************************
int aimInputs(/*@unused@*/ int inst, /*@unused@*/ void *aimInfo, int index,
          char **ainame, capsValue *defval)
{
  int status = CAPS_SUCCESS;

    /*! \page aimInputsCART3D AIM Inputs
     * The following list outlines the CART3D inputs along with their default value available
     * through the AIM interface.
     */

#ifdef DEBUG
  printf(" sansLIPAIM/aimInputs  instance = %d  index = %d!\n", inst, index);
#endif

  if (index == 1)
  {
    *ainame           = EG_strdup("Alpha");
    defval->type      = Double;
    defval->vals.real = 0.0;
      /*! \page aimInputs
     * - <B> alpha = double</B>. <Default 0.0> <br>
     *  Angle of attack [degree].
     */
  }
  else if (index == 2)
  {
    *ainame           = EG_strdup("Beta");
    defval->type      = Double;
    defval->vals.real = 0.0;
      /*! \page aimInputs
     * - <B> beta = double</B>. <Default 0.0> <br>
     *  Sideslip angle [degree].
     */
  }
  else if (index == 3)
  {
    *ainame           = EG_strdup("RollRate");
    defval->type      = Double;
    defval->vals.real = 0.0;
      /*! \page aimInputs
     * - <B> roll rate = double</B>. <Default 0.0> <br>
     *  Non-dimensional roll rate.
     */
  }
  else if (index == 4)
  {
    *ainame           = EG_strdup("PitchRate");
    defval->type      = Double;
    defval->vals.real = 0.0;
      /*! \page aimInputs
     * - <B> pitch rate = double</B>. <Default 0.0> <br>
     *  Non-dimensional pitch rate.
     */
  }
  else if (index == 5)
  {
    *ainame           = EG_strdup("YawRate");
    defval->type      = Double;
    defval->vals.real = 0.0;
      /*! \page aimInputs
     * - <B> yaw rate = double</B>. <Default 0.0> <br>
     *  Non-dimensional yaw rate.
     */
  }
  else if (index == 6)
  {
    *ainame              = EG_strdup("Boundary_Condition");
    defval->type         = String;
    defval->nullVal      = IsNull;
    defval->vals.string  = NULL;

    /*! \page aimInputs
     * - <B>Boundary_Condition = Json string of python dictonaries </B> <br>
     */
  }
  else if (index == 7)
  {
    *ainame           = EG_strdup("Tecplot");
    defval->type      = String;
    defval->vals.string = EG_strdup("");

      /*! \page aimInputs
     * - <B> Tecplot = string</B>. <Default 0.0> <br>
     * Tecplot file name
     */
  }
  else if (index == 8)
  {
    *ainame              = EG_strdup("Pressure_Scale_Factor");
    defval->type         = Double;
    defval->vals.real    = 1.0;
    defval->units        = NULL;

    /*! \page aimInputsFUN3D
     * - <B>Pressure_Scale_Factor = 1.0</B> <br>
     * Value to scale Cp data when transferring data. Data is scaled based on Pressure = Pressure_Scale_Factor*Cp + Pressure_Scale_Offset.
     */
  }
  else if (index == 9)
  {
    *ainame              = EG_strdup("Pressure_Scale_Offset");
    defval->type         = Double;
    defval->vals.real    = 0.0;
    defval->units        = NULL;

    /*! \page aimInputsFUN3D
     * - <B>Pressure_Scale_Offset = 0.0</B> <br>
     * Value to scale Cp data when transferring data. Data is scaled based on Pressure = Pressure_Scale_Factor*Cp + Pressure_Scale_Offset.
     */
  }
  else if (index == 10)
  {
    *ainame               = EG_strdup("Surface_Mesher");
    defval->type          = String;
    defval->vals.string   = EG_strdup("{\"Name\":\"EGADS\", \"Tess_Params\":[0.025,0.001,15.00]}");

    /*! \page aimInputsTetGen
     * - <B> Surface_Mesher = "{\"Name\":\"EGADS\", \"Tess_Params\":[0.025,0.001,15.00]}"</B> <br>
     * Surface mesh generator. Available names include: "EGADS", "AFLR4".
     *
     * The EGADS parameters are used to create the surface mesh. There order definition is as follows.
     *  1. Max Edge Length (0 is any length)
     *  2. Max Sag or distance from mesh segment and actual curved geometry
     *  3. Max angle in degrees between triangle facets
     *
     */
  }
  else if (index == 11)
  {
    *ainame               = EG_strdup("Volume_Mesher");
    defval->type          = String;
    defval->vals.string   = EG_strdup("TetGen"); // TetGen, AFLR3

    /*! \page aimInputsTetGen
     * - <B> Volume_Mesher = "TetGen"</B> <br>
     * Volume mesh generator. Available names include: "TetGen", "AFLR3".
     */
  }

  static_assert(NUMINPUT == 11, "");

  return status;
}




// ********************** AIM Function Break *****************************
int aimPreAnalysis(/*@unused@*/ int inst, void *aimInfo,
                    const char *analysisPath, /*@null@*/ capsValue *aimInputs, capsErrs **errs)
{
  char currentPath[PATH_MAX];

  *errs = NULL;

  /* get where we are and set the path to our input */
  if (getcwd(currentPath, PATH_MAX) == NULL)
  {
    printf(" sansLIPAIM/aimPreAnalysis: Failed to retrieve the current directory!\n");
    return CAPS_DIRERR;
  }

  if (chdir(analysisPath) != 0)
  {
    printf(" sansLIPAIM/aimPreAnalysis: Cannot chdir to %s!\n", analysisPath);
    return CAPS_DIRERR;
  }

  try
  {
  #ifdef DEBUG
    printf(" sansLIPAIM/aimPreAnalysis: instance = %d, nbody = %d\n", inst, nBody);
  #endif

    if (aim_newGeometry(aimInfo) == CAPS_SUCCESS )
    {
      //printf(" sansLIPAIM/aimPreAnalysis: New geometry!\n");
      checkAndCreateMesh(inst, aimInfo);
    }

    int alphaInd   = aim_getIndex(aimInfo, "Alpha"    ,  ANALYSISIN)-1;
    int betaInd    = aim_getIndex(aimInfo, "Beta"     ,  ANALYSISIN)-1;
    int rollInd    = aim_getIndex(aimInfo, "RollRate" ,  ANALYSISIN)-1;
    int pitchInd   = aim_getIndex(aimInfo, "PitchRate",  ANALYSISIN)-1;
    int yawInd     = aim_getIndex(aimInfo, "YawRate"  ,  ANALYSISIN)-1;
    int tecplotInd = aim_getIndex(aimInfo, "Tecplot"  ,  ANALYSISIN)-1;

    SANS::PyDict solveparams;

    solveparams[SANS::LIPSolver::LIPSolveParams::params.alpha]     = aimInputs[alphaInd].vals.real;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.beta]      = aimInputs[betaInd ].vals.real;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.rollRate]  = aimInputs[rollInd ].vals.real;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.pitchRate] = aimInputs[pitchInd].vals.real;
    solveparams[SANS::LIPSolver::LIPSolveParams::params.yawRate]   = aimInputs[yawInd  ].vals.real;

    std::string tecfile = aimInputs[tecplotInd].vals.string;

    outputs[inst] = solvers[inst]->solve(solveparams);

    // Compute stability derivatives
    solvers[inst]->StabilityDerivatives(outputs[inst]);

    if (!tecfile.empty())
      solvers[inst]->writeTecplot(tecfile);
  }
  catch (std::exception& e)
  {
    std::cout << e.what();

    // Restore the path we came in with
    if ( chdir(currentPath) != 0 ) return CAPS_DIRERR;

    return CAPS_EXECERR;
  }

  // Restore the path we came in with
  if ( chdir(currentPath) != 0 ) return CAPS_DIRERR;

  return CAPS_SUCCESS;
}

static const char *names[NUMJ] =
{
  // Forces in body axis
  "CXtot",
  "CYtot",
  "CZtot",

  // Moments in body axis
  "Cltot",
  "Cmtot",
  "Cntot",

  // Forces in stability axis
  "CLtot",
  "CDtot",
  "etot",

  // Moments in stability axis
  "Cl'tot",
  "Cm'tot",
  "Cn'tot",

  // Trefftz plane forces
  "CLff",
  "CYff",
  "CDff",
  "eff",
};


static const char *namesB[NUMB] =
{
  // Forces in body axis
  "CX",
  "CY",
  "CZ",

  // Moments in body axis
  "Cl",
  "Cm",
  "Cn",
};

static const char *namesS[NUMS] =
{
  // Forces in stability axis
  "CL",
  "CY",

  // Moments in stability axis
  "Cl'",
  "Cm'",
  "Cn'",
};

static const char *namesBD[NUMBD] =
{
  // Body axis derivatives
 "u", "v", "w", "p", "q", "r"
};

static const char *namesSD[NUMSD] =
{
  // Stability axis derivatives
 "a", "b"
};


// ********************** AIM Function Break *****************************
int aimOutputs( int inst, void *aimInfo, int index, char **aoname, capsValue *form)
{
#ifdef DEBUG
  printf(" sansLIPAIM/aimOutputs instance = %d  index = %d!\n", inst, index);
#endif

  /*! \page aimOutputs SANS LIP AIM Outputs
  * Integrated force outputs on the entire body are available as outputs.
  * - <B> C_D</B>. entire Drag Force
  * - <B> C_L</B>. entire Lift Force
  */

       if ( index <= NUMJ )
    *aoname = EG_strdup(names[index-1]);
  else if ( index <= NUMJ + NUMB*NUMBD )
  {
    int idx = index-1 - NUMJ;
    int idxB = idx % NUMB;
    int idxBD = idx/NUMB % NUMBD;
    std::string var   =  namesB[idxB];
    std::string deriv = namesBD[idxBD];
    //std::cout << "idxB, idxBD " << idxB << ", " << idxBD << std::endl;
    *aoname = EG_strdup((var+deriv).c_str());
  }
  else if ( index <= NUMJ + NUMB*NUMBD + NUMS*NUMSD )
  {
    int idx = index-1 - NUMJ - NUMB*NUMBD;
    int idxS = idx % NUMS;
    int idxSD = idx/NUMS % NUMSD;
    std::string var   =  namesS[idxS];
    std::string deriv = namesSD[idxSD];
    //std::cout << "idxS, idxSD " << idxS << ", " << idxSD << std::endl;
    *aoname = EG_strdup((var+deriv).c_str());
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown output index : %d", index);

  //std::cout << "name = " << *aoname << std::endl;
  form->type = Double;

  return CAPS_SUCCESS;
}

// ********************** AIM Function Break *****************************
int aimCalcOutput(int inst, /*@unused@*/ void *aimInfo, const char *apath, int index,
                  capsValue *val, capsErrs **errors)
{
  /* return the output value */

  std::string name;
      if ( index <= NUMJ )
  {
   name = names[index-1];
  }
  else if ( index <= NUMJ + NUMB*NUMBD )
  {
    int idx = index-1 - NUMJ;
    int idxB = idx % NUMB;
    int idxBD = idx/NUMB % NUMBD;
    std::string var   =  namesB[idxB];
    std::string deriv = namesBD[idxBD];
    name = var + deriv;
  }
  else if ( index <= NUMJ + NUMB*NUMBD + NUMS*NUMSD )
  {
    int idx = index-1 - NUMJ - NUMB*NUMBD;
    int idxS = idx % NUMS;
    int idxSD = idx/NUMS % NUMSD;
    std::string var   =  namesS[idxS];
    std::string deriv = namesSD[idxSD];
    name = var + deriv;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown output index : %d", index);

  //std::cout << "Extracting " << inst << " " << name << std::endl;
  //std::cout << "outputs = " << outputs[inst].str() << std::endl;

  boost::python::extract<double> value(outputs[inst][name]);
  if (!value.check())
    return CAPS_BADVALUE;
  val->vals.real = static_cast<double>(value);

  //std::string json = outputs[inst].json();
  //json.insert(json.begin(),'\"');
  //json.insert(json.end(),'\"');
  //std::cout << json << std::endl;

  return CAPS_SUCCESS;
}

// ********************** AIM Function Break *****************************
void aimCleanup()
{
#ifdef DEBUG
  printf(" sansLIPAIM/aimCleanup!\n");
#endif

  // free all the solver memory
  solvers.clear();
  outputs.clear();
}

// ==========================================================================//
// Discretization structures
struct FaceBodyPair
{
  int body;
  std::vector<int> faces;
};

struct discStorage
{
  std::vector<int> gIndices;
  std::vector<int> dIndices;
  std::vector<FaceBodyPair> bounds;

  discStorage( const int ntris, const std::vector<FaceBodyPair>& bounds ) :
    gIndices(6*ntris), dIndices(ntris), bounds(bounds)
  {}

  ~discStorage() {}
};


// ********************** AIM Function Break *****************************
int
aimFreeDiscr(/*@null@*/ capsDiscr *discr)
{
  // Free up this capsDiscr
  discr->nPoints = 0; // Points

  EG_free(discr->mapping); discr->mapping = NULL;

  for (int i = 0; i < discr->nTypes; i++)
  {
    EG_free(discr->types[i].gst);
    EG_free(discr->types[i].dst);
    EG_free(discr->types[i].matst);
    EG_free(discr->types[i].tris);
  }

  discr->nTypes  = 0;
  EG_free(discr->types); discr->types   = NULL;

  discr->nElems  = 0;
  EG_free(discr->elems); discr->elems = NULL;

  discr->nDtris = 0;
  EG_free(discr->dtris); discr->dtris = NULL;

  discr->nVerts = 0;
  EG_free(discr->verts); discr->verts  = NULL;
  EG_free(discr->celem); discr->celem = NULL;

  discStorage *storage = (discStorage *)discr->ptrm;
  delete storage; discr->ptrm = NULL; // Extra information to store into the discr void pointer

  return CAPS_SUCCESS;
}

// ********************** AIM Function Break *****************************
int aimDiscr(char *tname, capsDiscr *discr)
{
  int          tessState, vID, ielem, status, iIndex, nFace, atype, alen, tlen;
  int          npts, ntris, nBody, nGlobal, global;
  discStorage  *storage = NULL;
  const int    *ints, *ptype, *pindex, *tris, *nei;
  const double *reals, *xyz, *uv;
  const char   *string, *intents;
  ego          body, *bodies = NULL, *tessBodies = NULL, *faces = NULL;

  // vertex ID
  std::vector<int> vid;

  iIndex = discr->instance;

#ifdef DEBUG
  printf(" sansLIPAIM/aimDiscr: tname = %s, instance = %d!\n", tname, iIndex);
#endif

  if ((iIndex < 0) || (iIndex >= (int)solvers.size())) return CAPS_BADINDEX;
  if (tname == NULL) return CAPS_NOTFOUND;

  try
  {
    // check and generate the mesh if needed
    checkAndCreateMesh(discr->instance, discr->aInfo);
  }
  catch (std::exception& e)
  {
    std::cout << e.what();
    return CAPS_EXECERR;
  }

  status = aimFreeDiscr(discr);
  if (status != CAPS_SUCCESS) return status;

  status = aim_getBodies(discr->aInfo, &intents, &nBody, &bodies);
  if (status != CAPS_SUCCESS)
  {
    printf(" sansLIPAIM/aimDiscr: %d aim_getBodies = %d!\n", iIndex, status);
    return status;
  }

  // The bodies is length 2*nBody, with the last nBody tessellation objects of the first nBody
  tessBodies = bodies + nBody;

  /* find any faces with our boundary marker */
  int nBodyWithBound = 0;
  for (int ibody = 0; ibody < nBody; ibody++)
  {
    status = EG_getBodyTopos(bodies[ibody], NULL, FACE, &nFace, &faces);
    if (status != EGADS_SUCCESS)
    {
      printf(" sansLIPAIM: getBodyTopos (Face) = %d for Body %d!\n", status, ibody+1);
      return status;
    }

    for (int iface = 0; iface < nFace; iface++)
    {
      status = EG_attributeRet(faces[iface], "capsBound", &atype, &alen, &ints, &reals, &string);
      if (status != EGADS_SUCCESS)    continue;
      if (atype  != ATTRSTRING)       continue;
      if (strcmp(string, tname) != 0) continue;
      nBodyWithBound++;
      break;
#ifdef DEBUG
      printf(" sansLIPAIM/aimDiscr: Body %d/Face %d matches %s!\n", ibody+1, iface+1, tname);
#endif
    }
    EG_free(faces);
  }

  if (nBodyWithBound == 0)
  {
    printf(" sansLIPAIM/aimDiscr: No Faces match %s!\n", tname);
    return CAPS_SUCCESS;
  }

  /* store away the faces */
  std::vector<FaceBodyPair> bounds(nBodyWithBound);

  nBodyWithBound = npts = ntris = 0;
  for (int ibody = 0; ibody < nBody; ibody++)
  {
    status = EG_getBodyTopos(bodies[ibody], NULL, FACE, &nFace, &faces);
    if (status != EGADS_SUCCESS)
    {
      printf(" sansLIPAIM: getBodyTopos (Face) = %d for Body %d!\n", status, ibody+1);
      goto cleanup;
    }

    bool found = false;
    for (int iface = 0; iface < nFace; iface++)
    {
      status = EG_attributeRet(faces[iface], "capsBound", &atype, &alen, &ints, &reals, &string);
      if (status  != EGADS_SUCCESS)   continue;
      if (atype != ATTRSTRING)        continue;
      if (strcmp(string, tname) != 0) continue;
      found = true;
      bounds[nBodyWithBound].faces.push_back(iface+1);
      status = EG_getTessFace(tessBodies[ibody], iface+1, &alen, &xyz, &uv, &ptype, &pindex,
                                                          &tlen, &tris, &nei);
      if (status != EGADS_SUCCESS)
      {
        printf(" sansLIPAIM: EG_getTessFace %d = %d for Body %d!\n", iface+1, status, ibody+1);
        continue;
      }
      npts  += alen;
      ntris += tlen;
    }
    EG_free(faces); faces = NULL;
    if (found)
    {
      bounds[nBodyWithBound].body = ibody+1;
      nBodyWithBound++;
    }
  }

  if ((npts == 0) || (ntris == 0))
  {
#ifdef DEBUG
    printf(" sansLIPAIM/aimDiscr: ntris = %d, npts = %d!\n", ntris, npts);
#endif
    status = CAPS_SOURCEERR;
    goto cleanup;
  }

  discr->nElems = ntris;
  discr->nVerts = ntris;

  // Specify our single element type
  status = EGADS_MALLOC;
  discr->nTypes = 1;

  discr->types  = (capsEleType *) EG_alloc(discr->nTypes*sizeof(capsEleType));
  if (discr->types == NULL) goto cleanup;
  discr->types[0].tris = NULL;
  discr->types[0].gst  = NULL;

  discr->types[0].nref  = 3;
  discr->types[0].ndata = 1;            /* data at 1 reference positions */
  discr->types[0].ntri  = 1;
  discr->types[0].nmat  = 1;            /* match points at geom ref positions */
  discr->types[0].tris  = NULL;
  discr->types[0].gst   = NULL;
  discr->types[0].dst   = NULL;
  discr->types[0].matst = NULL;

  discr->types[0].tris    = (int *) EG_alloc(3*sizeof(int));
  if (discr->types[0].tris == NULL) goto cleanup;
  discr->types[0].tris[0] = 1;
  discr->types[0].tris[1] = 2;
  discr->types[0].tris[2] = 3;

  discr->types[0].gst    = (double *) EG_alloc(6*sizeof(double));
  if (discr->types[0].gst == NULL) goto cleanup;
  discr->types[0].gst[0] = 0.0;
  discr->types[0].gst[1] = 0.0;
  discr->types[0].gst[2] = 1.0;
  discr->types[0].gst[3] = 0.0;
  discr->types[0].gst[4] = 0.0;
  discr->types[0].gst[5] = 1.0;

  discr->types[0].dst    = (double *) EG_alloc(2*sizeof(double));
  if (discr->types[0].dst == NULL) goto cleanup;
  discr->types[0].dst[0] = 1./3.;
  discr->types[0].dst[1] = 1./3.;

  discr->types[0].matst    = (double *) EG_alloc(2*sizeof(double));
  if (discr->types[0].matst == NULL) goto cleanup;
  discr->types[0].matst[0] = 1./3.;
  discr->types[0].matst[1] = 1./3.;

  /* get the tessellation and
     make up a simple linear continuous triangle discretization */
#ifdef DEBUG
  printf(" sansLIPAIM/aimDiscr: ntris = %d, npts = %d!\n", ntris, npts);
#endif

  discr->mapping = (int *) EG_alloc(2*npts*sizeof(int));
  if (discr->mapping == NULL) goto cleanup;

  discr->elems = (capsElement *) EG_alloc(ntris*sizeof(capsElement));
  if (discr->elems == NULL) goto cleanup;

  discr->celem = (int *) EG_alloc(ntris*sizeof(int));
  if (discr->celem == NULL) goto cleanup;

  discr->verts = (double *) EG_alloc(3*ntris*sizeof(double));
  if (discr->verts == NULL) goto cleanup;

  discr->ptrm = storage = new discStorage(ntris, bounds);
  if (storage == NULL) goto cleanup;

  vID = 0;
  ielem = 0;
  for (std::size_t ibound = 0; ibound < bounds.size(); ibound++)
  {
    int ibody = bounds[ibound].body;
    status = EG_statusTessBody(tessBodies[ibody-1], &body, &tessState, &nGlobal);
    if ((status < EGADS_SUCCESS) || (nGlobal == 0))
    {
      printf(" sansLIPAIM/aimDiscr: EG_statusTessBody = %d, nGlobal = %d!\n", status, nGlobal);
      goto cleanup;
    }
    vid.resize(nGlobal, 0);

    for (int iface : bounds[ibound].faces)
    {
      status = EG_getTessFace(tessBodies[ibody-1], iface, &alen, &xyz, &uv,
                              &ptype, &pindex, &tlen, &tris, &nei);
      if (status != EGADS_SUCCESS) continue;

      for (int j = 0; j < alen; j++)
      {
        status = EG_localToGlobal(tessBodies[ibody-1], iface, j+1, &global);
        if (status != EGADS_SUCCESS)
        {
          printf(" sansLIPAIM/aimDiscr: %d %d - %d %d EG_localToGlobal = %d!\n", ibody, j+1, ptype[j], pindex[j], status);
          goto cleanup;
        }
        if (vid[global-1] != 0) continue;
        discr->mapping[2*vID+0] = ibody;
        discr->mapping[2*vID+1] = global;
        vid[global-1]           = vID+1;
        vID++;
      }

      for (int itri = 0; itri < tlen; itri++, ielem++)
      {
        discr->elems[ielem].bIndex      = ibody;
        discr->elems[ielem].tIndex      = 1;
        discr->elems[ielem].eIndex      = iface;
        discr->elems[ielem].gIndices    = &storage->gIndices[6*ielem];
        discr->elems[ielem].dIndices    = &storage->dIndices[ielem];
        discr->elems[ielem].eTris.tq[0] = itri+1;

        status = EG_localToGlobal(tessBodies[ibody-1], iface, tris[3*itri+0], &global);
        if (status != EGADS_SUCCESS)
          printf(" sansLIPAIM/aimDiscr: tri %d/0 EG_localToGlobal = %d\n", itri+1, status);
        storage->gIndices[6*ielem+0] = vid[global-1];
        storage->gIndices[6*ielem+1] = tris[3*itri+0];

        status = EG_localToGlobal(tessBodies[ibody-1], iface, tris[3*itri+1], &global);
        if (status != EGADS_SUCCESS)
          printf(" sansLIPAIM/aimDiscr: tri %d/1 EG_localToGlobal = %d\n", itri+1, status);
        storage->gIndices[6*ielem+2] = vid[global-1];
        storage->gIndices[6*ielem+3] = tris[3*itri+1];

        status = EG_localToGlobal(tessBodies[ibody-1], iface, tris[3*itri+2], &global);
        if (status != EGADS_SUCCESS)
          printf(" sansLIPAIM/aimDiscr: tri %d/2 EG_localToGlobal = %d\n", itri+1, status);
        storage->gIndices[6*ielem+4] = vid[global-1];
        storage->gIndices[6*ielem+5] = tris[3*itri+2];

        discr->celem[ielem] = ielem+1;
        discr->elems[ielem].dIndices[0] = ielem+1;
        for (int n = 0; n < 3; n++)
          discr->verts[3*ielem+n] = (xyz[3*(tris[3*itri+0]-1)+n] + xyz[3*(tris[3*itri+1]-1)+n] + xyz[3*(tris[3*itri+2]-1)+n])/3.;
      }
    }
  }
  discr->nPoints = vID;

  status = CAPS_SUCCESS;

cleanup:
  if (faces != NULL) EG_free(faces);

  if (status != CAPS_SUCCESS)
  {
    aimFreeDiscr(discr);
  }

  return status;
}


// ********************** AIM Function Break *****************************
int aimLocateElement(/*@unused@*/ capsDiscr *discr, /*@unused@*/ double *params,
                     /*@unused@*/ double *param,    /*@unused@*/ int *eIndex,
                     /*@unused@*/ double *bary)
{
  int    i, in[3], stat, ismall;
  double we[3], w, smallw = -1.e300;

//#ifdef DEBUG
//  printf(" sansLIPAIM/aimLocateElement instance = %d!\n", discr->instance);
//#endif

  if (discr == NULL) return CAPS_NULLOBJ;

  for (ismall = i = 0; i < discr->nElems; i++)
  {
    in[0] = discr->elems[i].gIndices[0] - 1;
    in[1] = discr->elems[i].gIndices[2] - 1;
    in[2] = discr->elems[i].gIndices[4] - 1;
    stat  = EG_inTriExact(&params[2*in[0]], &params[2*in[1]], &params[2*in[2]], param, we);

    if (stat == EGADS_SUCCESS)
    {
      *eIndex = i+1;
      bary[0] = we[1];
      bary[1] = we[2];
      return CAPS_SUCCESS;
    }

    w = we[0];
    if (we[1] < w) w = we[1];
    if (we[2] < w) w = we[2];
    if (w > smallw)
    {
      ismall = i+1;
      smallw = w;
    }
  }

  /* must extrapolate! */
  if (ismall == 0) return CAPS_NOTFOUND;
  in[0] = discr->elems[ismall-1].gIndices[0] - 1;
  in[1] = discr->elems[ismall-1].gIndices[2] - 1;
  in[2] = discr->elems[ismall-1].gIndices[4] - 1;
  EG_inTriExact(&params[2*in[0]], &params[2*in[1]], &params[2*in[2]], param, we);

  *eIndex = ismall;
  bary[0] = we[1];
  bary[1] = we[2];

  /*
  printf(" aimLocateElement: extropolate to %d (%lf %lf %lf)  %lf\n", ismall, we[0], we[1], we[2], smallw);
  */
  return CAPS_SUCCESS;
}


// ********************** AIM Function Break *****************************
int aimTransfer(capsDiscr *discr, /*@unused@*/ const char *dataName, int npts, int rank,
                double *data, /*@unused@*/ char **units)
{
#ifdef DEBUG
  printf(" sansLIPAIM/aimTransfer name = %s  instance = %d  npts = %d/%d!\n",
         dataName, discr->instance, npts, rank);
#endif

  int iIndex = discr->instance;

  if ((iIndex < 0) || (iIndex >= (int)solvers.size())) return CAPS_BADINDEX;
  if (dataName == NULL) return CAPS_NOTFOUND;


  if (strcasecmp(dataName, "Pressure") != 0 &&
      strcasecmp(dataName, "P")        != 0 &&
      strcasecmp(dataName, "Cp")       != 0 &&
      strcasecmp(dataName, "CoefficientOfPressure") != 0)
  {

    printf("Unrecognized data transfer variable - %s\n", dataName);
    return CAPS_NOTFOUND;
  }

  const SANS::Field_CG_Cell<PhysD3, TopoD3, Real>& qfld = solvers[iIndex]->qfld();
  const SANS::XField<PhysD3, TopoD3>& xfld = qfld.getXField();

  capsValue *pPressure_Scale_Factor = NULL;
  aim_getValue(discr->aInfo, aim_getIndex(discr->aInfo, "Pressure_Scale_Factor", ANALYSISIN), ANALYSISIN, &pPressure_Scale_Factor);

  capsValue *pPressure_Scale_Offset = NULL;
  aim_getValue(discr->aInfo, aim_getIndex(discr->aInfo, "Pressure_Scale_Offset", ANALYSISIN), ANALYSISIN, &pPressure_Scale_Offset);

  Real Pressure_Scale_Factor = pPressure_Scale_Factor->vals.real;
  Real Pressure_Scale_Offset = pPressure_Scale_Offset->vals.real;

  discStorage *storage = (discStorage *)discr->ptrm;

  // Zero out data
  for (int i = 0; i < npts; i++)
    for (int j = 0; j < rank; j++ )
      data[rank*i+j] = 0;

  int ielem = 0;
  for (std::size_t ibound = 0; ibound < storage->bounds.size(); ibound++)
  {
    for (int iface : storage->bounds[ibound].faces)
    {
      if ( xfld.getBoundaryTraceGroupBase(iface-1).topoTypeID() == typeid(SANS::Triangle) )
      {
        typedef SANS::Triangle TopologyTrace;
        typedef SANS::Tet TopologyCell;

        typedef typename SANS::XField<PhysD3, TopoD3>::FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
        typedef typename SANS::XField<PhysD3, TopoD3>::FieldCellGroupType<TopologyCell> XFieldCellGroupType;
        typedef typename SANS::Field<PhysD3, TopoD3, Real >::template FieldCellGroupType<TopologyCell> QFieldCellGroupType;

        typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
        typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
        typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

        int groupCellL = xfld.getBoundaryTraceGroup<SANS::Triangle>(iface-1).getGroupLeft();

        const XFieldTraceGroupType& xfldTrace = xfld.getBoundaryTraceGroup<SANS::Triangle>(iface-1);
        const XFieldCellGroupType& xfldCell = xfld.getCellGroup<SANS::Tet>(groupCellL);
        const QFieldCellGroupType& qfldCell = qfld.getCellGroup<SANS::Tet>(groupCellL);

        // element field variables
        ElementXFieldTraceClass xfldTraceElem( xfldTrace.basis() );
        ElementXFieldClass xfldElem( xfldCell.basis() );
        ElementQFieldClass qfldElem( qfldCell.basis() );

        typename ElementXFieldTraceClass::RefCoordType sRefTrace = SANS::Triangle::centerRef;
        typename ElementQFieldClass::RefCoordType stu;
        typename ElementXFieldTraceClass::VectorX X, N;
        Real q, magq, Cp, Pressure;
        SANS::DLA::VectorS<3, Real > gradq;
        SANS::DLA::VectorS<3, Real > U, V;
        int elemCell;
        SANS::CanonicalTraceToCell canonicalTrace;

        solvers[iIndex]->pde().freestream(U);
        Real Vinf2 = SANS::dot(U,U);

        const int nelem = xfldTrace.nElem();
        for (int elem = 0; elem < nelem; elem++)
        {
          xfldTrace.getElement( xfldTraceElem, elem );

          elemCell = xfldTrace.getElementLeft( elem );
          canonicalTrace = xfldTrace.getCanonicalTraceLeft( elem );

          xfldCell.getElement( xfldElem, elemCell );
          qfldCell.getElement( qfldElem, elemCell );

          xfldTraceElem.eval( sRefTrace, X );
          xfldTraceElem.unitNormal( sRefTrace, N );

          SANS::TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCell>::eval( canonicalTrace, sRefTrace, stu );
          qfldElem.eval( stu, q );

          xfldElem.evalGradient( stu, qfldElem, gradq );
          magq = SANS::dot(gradq,gradq);
          V = U + gradq;
          //V2 = dot(V,V);
          Cp = (-0.5*magq - SANS::dot(gradq,U))/(0.5*Vinf2);
          //Cp = (0.5*Vinf2 - 0.5*V2)/(0.5*Vinf2);

          Pressure = Pressure_Scale_Factor*Cp + Pressure_Scale_Offset;

          SANS_ASSERT_MSG(ielem < npts, "ielem =%d, npts = %d", ielem, npts);
          data[ielem] = Pressure;
          ielem++;
        }
      }
      else
        SANS_DEVELOPER_EXCEPTION("Not implemented.");
    }
  }

#if 0
  for (int i = 0; i < npts; i++)
  {
    for (int j = 0; j < rank; j++)
      data[rank*i+j] = qfld.DOF( storage->bound2vol[i] );
  }
#endif

  return CAPS_SUCCESS;
}


// ********************** AIM Function Break *****************************
int aimInterpolation(capsDiscr *discr, /*@unused@*/ const char *name, int eIndex,
                     double *bary, int rank, double *data, double *result)
{
  int    in[3], i, ind;
  double we[3];

//#ifdef DEBUG
//  printf(" sansLIPAIM/aimInterpolation  %s  instance = %d!\n", name, discr->instance);
//#endif

  if ((eIndex <= 0) || (eIndex > discr->nElems))
  {
    printf(" sansLIPAIM/Interpolation: eIndex = %d [1-%d]!\n",eIndex, discr->nElems);
    return CAPS_BADINDEX;
  }

  if (strcasecmp(name, "paramd") == 0)
  {
    we[0] = 1.0 - bary[0] - bary[1];
    we[1] = bary[0];
    we[2] = bary[1];
    in[0] = discr->elems[eIndex-1].gIndices[0] - 1;
    in[1] = discr->elems[eIndex-1].gIndices[2] - 1;
    in[2] = discr->elems[eIndex-1].gIndices[4] - 1;
    for (i = 0; i < rank; i++)
      result[i] = data[rank*in[0]+i]*we[0] + data[rank*in[1]+i]*we[1] + data[rank*in[2]+i]*we[2];
  }
  else
  {
    ind = discr->elems[eIndex-1].dIndices[0] - 1;
    for (i = 0; i < rank; i++)
      result[i] = data[rank*ind+i];
  }

  return CAPS_SUCCESS;
}


// ********************** AIM Function Break *****************************
int aimInterpolateBar(capsDiscr *discr, /*@unused@*/ const char *name, int eIndex,
                      double *bary, int rank, double *r_bar, double *d_bar)
{
  int    in[3], i;
  double we[3];

  if ((eIndex <= 0) || (eIndex > discr->nElems))
  {
    printf(" sansLIPAIM/InterpolateBar: eIndex = %d [1-%d]!\n",
           eIndex, discr->nElems);
    return CAPS_BADINDEX;
  }

  we[0] = 1.0 - bary[0] - bary[1];
  we[1] = bary[0];
  we[2] = bary[1];
  in[0] = discr->elems[eIndex-1].gIndices[0] - 1;
  in[1] = discr->elems[eIndex-1].gIndices[2] - 1;
  in[2] = discr->elems[eIndex-1].gIndices[4] - 1;
  for (i = 0; i < rank; i++)
  {
/*  result[i] = data[rank*in[0]+i]*we[0] + data[rank*in[1]+i]*we[1] +
                data[rank*in[2]+i]*we[2];  */
    d_bar[rank*in[0]+i] += we[0]*r_bar[i];
    d_bar[rank*in[1]+i] += we[1]*r_bar[i];
    d_bar[rank*in[2]+i] += we[2]*r_bar[i];
  }

  return CAPS_SUCCESS;
}


// ********************** AIM Function Break *****************************
int aimIntegration(capsDiscr *discr, /*@unused@*/ const char *name, int eIndex, int rank,
                   /*@null@*/ double *data, double *result)
{
  int    i, in[3], stat, ptype, pindex, nBody;
  double x1[3], x2[3], x3[3], xyz1[3], xyz2[3], xyz3[3], area;
  const char *intents;
  ego    *bodies;
/*
#ifdef DEBUG
  printf(" sansLIPAIM/aimIntegration  %s  instance = %d!\n",
         name, discr->instance);
#endif
*/
  if ((eIndex <= 0) || (eIndex > discr->nElems))
    printf(" sansLIPAIM/aimIntegration: eIndex = %d [1-%d]!\n", eIndex, discr->nElems);

  stat = aim_getBodies(discr->aInfo, &intents, &nBody, &bodies);
  if (stat != CAPS_SUCCESS)
  {
    printf(" sansLIPAIM/aimIntegration: %d aim_getBodies = %d!\n", discr->instance, stat);
    return stat;
  }

  /* element indices */

  in[0] = discr->elems[eIndex-1].gIndices[0] - 1;
  in[1] = discr->elems[eIndex-1].gIndices[2] - 1;
  in[2] = discr->elems[eIndex-1].gIndices[4] - 1;
  stat = EG_getGlobal(bodies[discr->mapping[2*in[0]]+nBody-1],
                      discr->mapping[2*in[0]+1], &ptype, &pindex, xyz1);
  if (stat != CAPS_SUCCESS)
    printf(" sansLIPAIM/aimIntegration: %d EG_getGlobal %d = %d!\n",
           discr->instance, in[0], stat);
  stat = EG_getGlobal(bodies[discr->mapping[2*in[1]]+nBody-1],
                      discr->mapping[2*in[1]+1], &ptype, &pindex, xyz2);
  if (stat != CAPS_SUCCESS)
    printf(" sansLIPAIM/aimIntegration: %d EG_getGlobal %d = %d!\n",
           discr->instance, in[1], stat);
  stat = EG_getGlobal(bodies[discr->mapping[2*in[2]]+nBody-1],
                      discr->mapping[2*in[2]+1], &ptype, &pindex, xyz3);
  if (stat != CAPS_SUCCESS)
    printf(" sansLIPAIM/aimIntegration: %d EG_getGlobal %d = %d!\n",
           discr->instance, in[2], stat);

  x1[0] = xyz2[0] - xyz1[0];
  x2[0] = xyz3[0] - xyz1[0];
  x1[1] = xyz2[1] - xyz1[1];
  x2[1] = xyz3[1] - xyz1[1];
  x1[2] = xyz2[2] - xyz1[2];
  x2[2] = xyz3[2] - xyz1[2];
  CROSS(x3, x1, x2);
  area  = sqrt(DOT(x3, x3))/6.0;      /* 1/2 for area and then 1/3 for sum */
  if (data == NULL)
  {
    *result = 3.0*area;
    return CAPS_SUCCESS;
  }

  for (i = 0; i < rank; i++)
    result[i] = (data[rank*in[0]+i] + data[rank*in[1]+i] +
                 data[rank*in[2]+i])*area;

  return CAPS_SUCCESS;
}


// ********************** AIM Function Break *****************************
int aimIntegrateBar(capsDiscr *discr, /*@unused@*/ const char *name, int eIndex, int rank,
                    double *r_bar, double *d_bar)
{
  int    i, in[3], stat, ptype, pindex, nBody;
  double x1[3], x2[3], x3[3], xyz1[3], xyz2[3], xyz3[3], area;
  const char *intents;
  ego    *bodies;
/*
#ifdef DEBUG
  printf(" sansLIPAIM/aimIntegrateBar  %s  instance = %d!\n",
         name, discr->instance);
#endif
*/
  if ((eIndex <= 0) || (eIndex > discr->nElems))
    printf(" sansLIPAIM/aimIntegrateBar: eIndex = %d [1-%d]!\n", eIndex, discr->nElems);

  stat = aim_getBodies(discr->aInfo, &intents, &nBody, &bodies);
  if (stat != CAPS_SUCCESS)
  {
    printf(" sansLIPAIM/aimIntegrateBar: %d aim_getBodies = %d!\n", discr->instance, stat);
    return stat;
  }

  /* element indices */

  in[0] = discr->elems[eIndex-1].gIndices[0] - 1;
  in[1] = discr->elems[eIndex-1].gIndices[2] - 1;
  in[2] = discr->elems[eIndex-1].gIndices[4] - 1;
  stat = EG_getGlobal(bodies[discr->mapping[2*in[0]]+nBody-1],
                      discr->mapping[2*in[0]+1], &ptype, &pindex, xyz1);
  if (stat != CAPS_SUCCESS)
    printf(" sansLIPAIM/aimIntegrateBar: %d EG_getGlobal %d = %d!\n",
           discr->instance, in[0], stat);
  stat = EG_getGlobal(bodies[discr->mapping[2*in[1]]+nBody-1],
                      discr->mapping[2*in[1]+1], &ptype, &pindex, xyz2);
  if (stat != CAPS_SUCCESS)
    printf(" sansLIPAIM/aimIntegrateBar: %d EG_getGlobal %d = %d!\n",
           discr->instance, in[1], stat);
  stat = EG_getGlobal(bodies[discr->mapping[2*in[2]]+nBody-1],
                      discr->mapping[2*in[2]+1], &ptype, &pindex, xyz3);
  if (stat != CAPS_SUCCESS)
    printf(" sansLIPAIM/aimIntegrateBar: %d EG_getGlobal %d = %d!\n",
           discr->instance, in[2], stat);

  x1[0] = xyz2[0] - xyz1[0];
  x2[0] = xyz3[0] - xyz1[0];
  x1[1] = xyz2[1] - xyz1[1];
  x2[1] = xyz3[1] - xyz1[1];
  x1[2] = xyz2[2] - xyz1[2];
  x2[2] = xyz3[2] - xyz1[2];
  CROSS(x3, x1, x2);
  area  = sqrt(DOT(x3, x3))/6.0;      /* 1/2 for area and then 1/3 for sum */

  for (i = 0; i < rank; i++)
  {
/*  result[i] = (data[rank*in[0]+i] + data[rank*in[1]+i] +
                 data[rank*in[2]+i])*area;  */
    d_bar[rank*in[0]+i] += area*r_bar[i];
    d_bar[rank*in[1]+i] += area*r_bar[i];
    d_bar[rank*in[2]+i] += area*r_bar[i];
  }

  return CAPS_SUCCESS;
}



} /* extern "C" */
