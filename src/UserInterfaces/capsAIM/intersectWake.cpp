// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/EGIntersection.h"
#include "Meshing/EGADS/solidBoolean.h"
#include "Meshing/EGADS/clone.h"
#include "Meshing/EGADS/isSame.h"
#include "Meshing/EGADS/replaceFaces.h"

#include "Meshing/EGTess/EGTessModel.h"
#include "Meshing/EGTess/intersectWake.h"

#include "tools/timer.h"

//#define DEBUG

extern "C"
{
__ProtoExt__ /*@null@*/ /*@only@*/
             char *EG_strdup( /*@null@*/ const char *str );

#define NUMUDPARGS 1
#define NUMUDPINPUTBODYS 2
#include "udpUtilities.h"
}

/* data about possible arguments */
static const char  *argNames[NUMUDPARGS] = {""};
static       int    argTypes[NUMUDPARGS] = {ATTRINT};
static       int    argIdefs[NUMUDPARGS] = {0};
static       double argDdefs[NUMUDPARGS] = {0};

extern "C"
{
/* get utility routines: udpErrorStr, udpInitialize, udpReset, udpSet,
                         udpGet, udpVel, udpClean, udpMesh */
#include "udpUtilities.c"
}

using namespace SANS;

/*
 ************************************************************************
 *                                                                      *
 *   udpExecute - execute the primitive                                 *
 *                                                                      *
 ************************************************************************
 */

extern "C"
int
udpExecute(ego  emodel,                 /* (in)  Model containing Body */
           ego  *ebody,                 /* (out) Body pointer */
           int  *nMesh,                 /* (out) number of associated meshes */
           char *string[])              /* (out) error message */
{
  int     status = EGADS_SUCCESS;

  std::string indent = "                          ";

  ROUTINE(udpExecute);

  /* --------------------------------------------------------------- */

  /* default return values */
  *ebody  = NULL;
  *nMesh  = 0;
  *string = NULL;

  /* cache copy of arguments for future use */
  status = cacheUdp();
  if (status < 0)
  {
    printf(" udpExecute: problem caching arguments\n");
    return status;
  }

  try
  {
    EGADS::EGModel<3> model(emodel);
    model.setAutoDelete(false);

    const EGADS::EGContext& context = model.getContext();

    //model.save("udfModel.egads");

    const std::vector< EGADS::EGBody<3> > bodies = model.getBodies();

    EGADS::EGBody<3> solid(context);
    EGADS::EGBody<3> wakelong(context);

    // split the list of bodies into solids and wakes
    for (std::size_t ibody = 0; ibody < bodies.size(); ibody++)
    {
      if (bodies[ibody].isSolid())
        solid = bodies[ibody];
      else if (bodies[ibody].isSheet())
        wakelong = bodies[ibody];
    }

    if ((ego)solid == NULL || (ego)wakelong == NULL || bodies.size() != 2)
    {
      std::cout << " udpExecute: Expected one solid and one sheet body." << std::endl;
      return EGADS_GEOMERR;
    }

    std::string wakeName;

    if (wakelong.hasAttribute("capsGroup"))
      wakelong.getAttribute("capsGroup", wakeName);

    std::vector< EGADS::EGFace<3> > sheetFaces = wakelong.getFaces();

    for ( const EGADS::EGFace<3>& face : sheetFaces )
    {
      std::string wakeNameFace;
      face.getAttribute("capsGroup", wakeNameFace);
      if (wakeName.empty()) wakeName = wakeNameFace;
      SANS_ASSERT(wakeName == wakeNameFace);
    }

#if 0
    // Remove all the portion of the wakes extended beyond the box
    std::cout << indent << "Intersecting wake... " << std::flush;
    timer intersect_time;
    std::vector<EGADS::EGBody<3>> wakes = EGADS::clone( EGADS::intersect( EGADS::EGModel<3>(wakelong.clone()), solid ).getBodies() );
    std::cout << intersect_time.elapsed() << " s " <<  std::endl;

    SANS_ASSERT(wakes.size() == 1);
    EGADS::EGBody<3> wake = wakes[0];
#endif

    // Scribe wake intersections onto the box
    std::cout << indent << "Wake intersection... " << std::flush;
    timer intersection_time;
    EGADS::EGIntersection<3> box_wake_intersect(solid, wakelong);

    // Scribe the outflow face on the wake
    std::vector<EGADS::EGFace<3>> faces( box_wake_intersect.uniqueFaces() );
    SANS_ASSERT_MSG( faces.size() == 1, "faces.size() = %d", faces.size() );
    EGADS::EGBody<3> tool( faces[0] );
    EGADS::EGIntersection<3> wake_face_intersect( wakelong, tool );
    EGADS::EGBody<3> scribed_wake( wake_face_intersect );

    std::vector<double> bbox = solid.getBoundingBox();
    std::vector<EGADS::EGFace<3>> wake_faces = scribed_wake.getFaces();
    std::vector<ego> removeFaces;
    for ( const EGADS::EGFace<3>& face : wake_faces )
    {
      // Skip faces that are inside the box and need to be retained
      std::vector<double> CG = face.getCG();
      if ( (bbox[0] < CG[0] && CG[0] < bbox[3]) &&
           (bbox[1] < CG[1] && CG[1] < bbox[4]) &&
           (bbox[2] < CG[2] && CG[2] < bbox[5]) )
        continue;

      // indicate that the face should be removed
      removeFaces.push_back(face);
      removeFaces.push_back(nullptr);
    }

    // remove the faces outside of the domain
    EGADS::EGBody<3> wake = replaceFaces(scribed_wake, removeFaces);
    std::cout << intersection_time.elapsed() << " s " <<  std::endl;

    //box_wake_intersect.saveModel("wakeIntersection_" + stringify(iwakelong) + ".egads");
    try
    {
      // Scribe the box-wake intersection onto the box
      std::cout << indent << "Scribing solid" << std::endl;
      solid = EGADS::EGBody<3>( box_wake_intersect );
    }
    catch ( const EGADS::EGADSException& e )
    {
      // EAGDS generates a construction error when the scribe fails, which means there are only floating edges
      if ( e.status != EGADS_CONSTERR )
        throw;
    }

    std::cout << indent << "Marking Trailing Vortex/Trefftz edges" << std::endl;

    // Any edge in the box-wake intersection not in the box is a floating edge
    std::vector< EGADS::EGEdge<3> > solidEdges = solid.getEdges();
    std::vector< EGADS::EGEdge<3> > wakeEdges  = wake.getEdges();

    for (std::size_t iWakeEdge = 0; iWakeEdge < wakeEdges.size(); iWakeEdge++)
    {
      // Look for the intersected edge on the solid
      bool found = false;
      for (std::size_t iBoxEdge = 0; iBoxEdge < solidEdges.size(); iBoxEdge++)
        if ( EGADS::isSame(wakeEdges[iWakeEdge], solidEdges[iBoxEdge]) )
        {
          found = true;
          // Mark the edge that is used for Kutta plane integrals
          wakeEdges[iWakeEdge].addAttribute("Kutta", wakeName);
          break;
        }
      // If the edge was found, then this is not a floating edge
      if (found) continue;

      for (int iIntesectEdge = 0; iIntesectEdge < box_wake_intersect.nEdge(); iIntesectEdge++)
      {
        EGADS::EGEdge<3> intesectEdge = box_wake_intersect.edge(iIntesectEdge);
        if ( EGADS::isSame(intesectEdge, wakeEdges[iWakeEdge]) )
        {
          EGADS::EGFace<3> solidFace = box_wake_intersect.face(iIntesectEdge);
          int solidFaceIdx = solid.getBodyIndex( solidFace );

          // Mark the edge that is used for Trefftz plane integrals
          // TODO: This assumes the edge is only on the outflow plane
          wakeEdges[iWakeEdge].addAttribute("Trefftz", wakeName);

          // The first index is the body with the face in the model.
          // The second index is the face index in the body.
          wakeEdges[iWakeEdge].addAttribute(EGTessModel_FLOATINGEDGE, {1, solidFaceIdx});

          std::cout << indent << "Wake Trefftz        edge : " << iWakeEdge+1 << " " << wakeName << " -- "
                              << "solid face : " << solidFaceIdx << std::endl;
          found = true;
          break;
        }
      }
      // If the edge was found, then this is not a vortex edge
      if (found) continue;

      std::vector<EGADS::EGFace<3>> wake_faces = wake.getFaces(wakeEdges[iWakeEdge]);
      if (wake_faces.size() == 1)
      {
        std::cout << indent << "Wake TrailingVortex edge : " << iWakeEdge+1 << " " << wakeName << std::endl;
        // Mark the edge as qn outboard vortex edge
        wakeEdges[iWakeEdge].addAttribute("TrailingVortex", wakeName);
      }
    }

    wake.addAttribute("__markFaces__", "udfintersectWake");

    if (wakelong.hasAttribute("_name"))
    {
      std::string name;
      wakelong.getAttribute("_name", name);
      wake.addAttribute("_name", name);
    }

    // return the intersected wake
    udps[numUdp].ebody = *ebody = wake;
  }
  catch ( const std::exception& e )
  {
    status = -1; // A better error code?
    *string = EG_strdup(e.what());
  }

  return status;
}

/*
 ************************************************************************
 *                                                                      *
 *   udpSensitivity - return sensitivity derivatives for the "real" argument *
 *                                                                      *
 ************************************************************************
 */

extern "C"
int
udpSensitivity(ego    ebody,            /* (in)  Body pointer */
   /*@unused@*/int    npnt,             /* (in)  number of points */
   /*@unused@*/int    entType,          /* (in)  OCSM entity type */
   /*@unused@*/int    entIndex,         /* (in)  OCSM entity index (bias-1) */
   /*@unused@*/double uvs[],            /* (in)  parametric coordinates for evaluation */
   /*@unused@*/double vels[])           /* (out) velocities */
{
  int iudp, judp;

  ROUTINE(udpSensitivity);

  /* --------------------------------------------------------------- */

  /* check that ebody matches one of the ebodys */
  iudp = 0;
  for (judp = 1; judp <= numUdp; judp++)
  {
    if (ebody == udps[judp].ebody)
    {
      iudp = judp;
      break;
    }
  }
  if (iudp <= 0)
  {
    return EGADS_NOTMODEL;
  }

  /* this routine is not written yet */
  return EGADS_NOLOAD;
}
