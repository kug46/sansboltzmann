// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGTess/WakedFarFieldBox.h"
#include "Meshing/EGADS/clone.h"
#include "tools/output_std_vector.h"
//#define DEBUG

extern "C"
{
// Why is this not in an EGADS header?
__ProtoExt__  /*@null@*/ /*@only@*/ char *
  EG_strdup( /*@null@*/ const char *str );

#define NUMUDPARGS 6
#define NUMUDPINPUTBODYS 2
#include "udpUtilities.h"
}

/* shorthands for accessing argument values and velocities */
#define DX(        IUDP  )  ((double *) (udps[IUDP].arg[0].val))[0]
#define DY(        IUDP  )  ((double *) (udps[IUDP].arg[1].val))[0]
#define DZ(        IUDP  )  ((double *) (udps[IUDP].arg[2].val))[0]
#define CENTER(    IUDP,I)  ((double *) (udps[IUDP].arg[3].val))[I]
#define CENTER_SIZ(IUDP  )               udps[IUDP].arg[3].size
#define IBODY(     IUDP  )  ((int    *) (udps[IUDP].arg[4].val))[0]
#define NWAKE(     IUDP  )  ((int    *) (udps[IUDP].arg[5].val))[0]

/* data about possible arguments */
static const char  *argNames[NUMUDPARGS] = {"dx",     "dy",     "dz",     "center", "ibody",  "nwake" };
static       int    argTypes[NUMUDPARGS] = {ATTRREAL, ATTRREAL, ATTRREAL, ATTRREAL, ATTRINT, -ATTRINT };
static       int    argIdefs[NUMUDPARGS] = {0,        0,        0,        0,        0,       0        };
static       double argDdefs[NUMUDPARGS] = {1.,       1.,       1.,       0.,       0.,      0.       };

extern "C"
{
/* get utility routines: udpErrorStr, udpInitialize, udpReset, udpSet,
                         udpGet, udpVel, udpClean, udpMesh */
#include "udpUtilities.c"
}

using namespace SANS;

static std::vector<EGADS::EGBody<3>> bodies;

/*
 ************************************************************************
 *                                                                      *
 *   udpExecute - execute the primitive                                 *
 *                                                                      *
 ************************************************************************
 */

extern "C"
int
udpExecute(ego  emodel,                 /* (in)  Model containing Body */
           ego  *ebody,                 /* (out) Body pointer */
           int  *nMesh,                 /* (out) number of associated meshes */
           char *string[])              /* (out) error message */
{
  int     status = EGADS_SUCCESS;

  ROUTINE(udpExecute);

  /* --------------------------------------------------------------- */

#ifdef DEBUG
  printf("udpExecute(emodel=%llx)\n", (long long)emodel);
  printf("DX( 0) = %f\n", DX( 0));
  printf("DY( 0) = %f\n", DY( 0));
  printf("DZ( 0) = %f\n", DZ( 0));
  if (CENTER_SIZ(0) == 3)
      printf("center[%d]     = %f %f %f\n", 0, CENTER(    0,0), CENTER(    0,1), CENTER(    0,2));
  printf("IBODY( 0) = %d\n", IBODY( 0));
#endif

  /* default return values */
  *ebody  = NULL;
  *nMesh  = 0;
  *string = NULL;

  if (udps[0].arg[0].size > 1)
  {
    printf(" udpExecute: dx should be a scalar\n");
    return EGADS_RANGERR;
  }
  else if (DX(0) < 0)
  {
    printf(" udpExecute: dx = %f < 0\n", DX(0));
    return EGADS_RANGERR;
  }
  else if (udps[0].arg[1].size > 1)
  {
    printf(" udpExecute: dy should be a scalar\n");
    return EGADS_RANGERR;
  }
  else if (DY(0) < 0)
  {
    printf(" udpExecute: dy = %f < 0\n", DY(0));
    return EGADS_RANGERR;
  }
  else if (udps[0].arg[2].size > 1)
  {
    printf(" udpExecute: dz should be a scalar\n");
    return EGADS_RANGERR;
  }
  else if (DZ(0) < 0)
  {
    printf(" udpExecute: dz = %f < 0\n", DZ(0));
    return EGADS_RANGERR;
  }
  else if (DX(0) <= 0 && DY(0) <= 0 && DZ(0) <= 0)
  {
    printf(" udpExecute: dx=dy=dz=0\n");
    return EGADS_GEOMERR;
  }
  else if (CENTER_SIZ(0) == 1)
  {
    // CENTER is not used
  }
  else if (CENTER_SIZ(0) != 3)
  {
    printf(" udpExecute: center should contain 3 entries\n");
    return EGADS_GEOMERR;
  }

  /* cache copy of arguments for future use */
  status = cacheUdp();
  if (status < 0)
  {
    printf(" udpExecute: problem caching arguments\n");
    return status;
  }


#ifdef DEBUG
  printf("dx[%d] = %f\n", numUdp, DX(    numUdp));
  printf("dy[%d] = %f\n", numUdp, DY(    numUdp));
  printf("dz[%d] = %f\n", numUdp, DZ(    numUdp));
  if (CENTER_SIZ(numUdp) == 3)
      printf("center[%d] = %f %f %f\n", numUdp, CENTER(    numUdp,0), CENTER(    numUdp,1), CENTER(    numUdp,2));
  printf("ibody( %d) = %d\n", numUdp, IBODY( numUdp));
#endif

  try
  {
    if (IBODY(numUdp) == 0)
    {
      std::vector<Real> boxsize(6);
      std::vector<int> TrefftzFrames;

      boxsize[0] = -DX(numUdp) / 2;
      boxsize[1] = -DY(numUdp) / 2;
      boxsize[2] = -DZ(numUdp) / 2;
      boxsize[3] =  DX(numUdp);
      boxsize[4] =  DY(numUdp);
      boxsize[5] =  DZ(numUdp);

      /* move the Body if CENTER has 3 values */
      if (CENTER_SIZ(0) == 3)
      {
        boxsize[0] += CENTER(numUdp,0);
        boxsize[1] += CENTER(numUdp,1);
        boxsize[2] += CENTER(numUdp,2);
      }


      EGADS::EGModel<3> model(emodel);
      model.setAutoDelete(false);

      //model.save("udfModel.egads");

      bodies = WakedFarFieldBox( model.getBodies(), boxsize, TrefftzFrames );
      if (TrefftzFrames.size() > 0)
        bodies[0].addAttribute("sansTrefftzFrames", TrefftzFrames);
      std::cout << "Adding attribute sansTrefftzFrames: " << TrefftzFrames << std::endl;
      for ( EGADS::EGBody<3>& body : bodies )
        body.addAttribute("__markFaces__", "udfWakeBox");
    }

    /* set the output value */
    int nwake = bodies.size()-1;
    NWAKE(numUdp) = nwake;

    /* the copy of the Body that was annotated is returned */
    udps[numUdp].ebody = *ebody = bodies[IBODY(numUdp)];
    std::cout << "*ebody = " << *ebody << std::endl;

  }
  catch ( const std::exception& e )
  {
    status = -1; // A better error code?
    *string = EG_strdup(e.what());
  }

  return status;
}

/*
 ************************************************************************
 *                                                                      *
 *   udpSensitivity - return sensitivity derivatives for the "real" argument *
 *                                                                      *
 ************************************************************************
 */

extern "C"
int
udpSensitivity(ego    ebody,            /* (in)  Body pointer */
   /*@unused@*/int    npnt,             /* (in)  number of points */
   /*@unused@*/int    entType,          /* (in)  OCSM entity type */
   /*@unused@*/int    entIndex,         /* (in)  OCSM entity index (bias-1) */
   /*@unused@*/double uvs[],            /* (in)  parametric coordinates for evaluation */
   /*@unused@*/double vels[])           /* (out) velocities */
{
  int iudp, judp;

  ROUTINE(udpSensitivity);

  /* --------------------------------------------------------------- */

  /* check that ebody matches one of the ebodys */
  iudp = 0;
  for (judp = 1; judp <= numUdp; judp++)
  {
    if (ebody == udps[judp].ebody)
    {
      iudp = judp;
      break;
    }
  }
  if (iudp <= 0)
  {
    return EGADS_NOTMODEL;
  }

  /* this routine is not written yet */
  return EGADS_NOLOAD;
}
