// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Meshing/EGADS/EGModel.h"
#include "Meshing/EGADS/isSame.h"

//#define DEBUG

extern "C"
{
// Why is this not in an EGADS header?
__ProtoExt__  /*@null@*/ /*@only@*/ char *
  EG_strdup( /*@null@*/ const char *str );

#define NUMUDPARGS 1
#define NUMUDPINPUTBODYS 2
#include "udpUtilities.h"
}

/* data about possible arguments */
static const char  *argNames[NUMUDPARGS] = {""};
static       int    argTypes[NUMUDPARGS] = {ATTRINT};
static       int    argIdefs[NUMUDPARGS] = {0};
static       double argDdefs[NUMUDPARGS] = {0};

extern "C"
{
/* get utility routines: udpErrorStr, udpInitialize, udpReset, udpSet,
                         udpGet, udpVel, udpClean, udpMesh */
#include "udpUtilities.c"
}

using namespace SANS;

/*
 ************************************************************************
 *                                                                      *
 *   udpExecute - execute the primitive                                 *
 *                                                                      *
 ************************************************************************
 */

extern "C"
int
udpExecute(ego  emodel,                 /* (in)  Model containing Body */
           ego  *ebody,                 /* (out) Body pointer */
           int  *nMesh,                 /* (out) number of associated meshes */
           char *string[])              /* (out) error message */
{
  int     status = EGADS_SUCCESS;

  std::string indent = "                          ";

  ROUTINE(udpExecute);

  /* --------------------------------------------------------------- */

  /* default return values */
  *ebody  = NULL;
  *nMesh  = 0;
  *string = NULL;

  /* cache copy of arguments for future use */
  status = cacheUdp();
  if (status < 0)
  {
    printf(" udpExecute: problem caching arguments\n");
    return status;
  }

  try
  {

    EGADS::EGModel<3> model(emodel);
    model.setAutoDelete(false);

    const EGADS::EGContext& context = model.getContext();

    //model.save("udfModel.egads");

    const std::vector< EGADS::EGBody<3> > bodies = model.getBodies();

    EGADS::EGBody<3> solid(context);
    EGADS::EGBody<3> wake(context);

    for (std::size_t ibody = 0; ibody < bodies.size(); ibody++)
    {
      if (bodies[ibody].isSolid())
        solid = bodies[ibody].clone();
      else if (bodies[ibody].isSheet())
        wake = bodies[ibody];
    }

    if ((ego)solid == NULL || (ego)wake == NULL)
    {
      std::cout << " udpExecute: Expected one solid and one sheet body." << std::endl;
      return EGADS_GEOMERR;
    }

    //-------------------------------------------------------------------------//

    //Find all sheet body edges that also exist in solid bodies
    //Then mark those as kutta edges if the sheet is a wake sheet
    std::string wakeName;

    if (wake.hasAttribute("capsGroup"))
      wake.getAttribute("capsGroup", wakeName);

    std::vector< EGADS::EGFace<3> > sheetFaces = wake.getFaces();

    for ( const EGADS::EGFace<3>& face : sheetFaces )
    {
      std::string wakeNameFace;
      face.getAttribute("capsGroup", wakeNameFace);
      if (wakeName.empty()) wakeName = wakeNameFace;
      SANS_ASSERT(wakeName == wakeNameFace);
    }

    std::vector< EGADS::EGEdge<3> > sheetEdges = wake.getEdges();
    std::vector< EGADS::EGEdge<3> > solidEdges = solid.getEdges();

    //Loop over all edges in the sheet body and solid body
    for ( std::size_t isheetedge = 0; isheetedge < sheetEdges.size(); isheetedge++ )
    {
      for ( std::size_t isolidedge = 0; isolidedge < solidEdges.size(); isolidedge++ )
      {
        // Check if the sheet edge and solid edge are on the same geometry
        if (EGADS::isSame(sheetEdges[isheetedge], solidEdges[isolidedge]))
        {
          // Get the nodes on the two edges
          std::vector< EGADS::EGNode<3> > solidNodes = solidEdges[isolidedge].getNodes();
          std::vector< EGADS::EGNode<3> > sheetNodes = sheetEdges[isheetedge].getNodes();

          // Check that the nodes match on the edges
          if (solidEdges[isolidedge].isOneNode())
          {
            if ( !EGADS::isSame(solidNodes[0], sheetNodes[0]) ) continue;
          }
          else
          {
            if ( !( (EGADS::isSame(solidNodes[0], sheetNodes[0]) && EGADS::isSame(solidNodes[1], sheetNodes[1])) ||
                    (EGADS::isSame(solidNodes[0], sheetNodes[1]) && EGADS::isSame(solidNodes[1], sheetNodes[0])) ) ) continue;
          }

          // Mark the solid edge as a wake edge
          solidEdges[isolidedge].addAttribute("Wake", wakeName);

          std::cout << indent << "Marking Solid Edge " << isolidedge+1 << " : " << wakeName << std::endl;
        }
      }
    }


    /* the copy of the Body that was annotated is returned */
    udps[numUdp].ebody = *ebody = solid;
  }
  catch ( const std::exception& e )
  {
    status = -1; // A better error code?
    *string = EG_strdup(e.what());
  }

  return status;
}

/*
 ************************************************************************
 *                                                                      *
 *   udpSensitivity - return sensitivity derivatives for the "real" argument *
 *                                                                      *
 ************************************************************************
 */

extern "C"
int
udpSensitivity(ego    ebody,            /* (in)  Body pointer */
   /*@unused@*/int    npnt,             /* (in)  number of points */
   /*@unused@*/int    entType,          /* (in)  OCSM entity type */
   /*@unused@*/int    entIndex,         /* (in)  OCSM entity index (bias-1) */
   /*@unused@*/double uvs[],            /* (in)  parametric coordinates for evaluation */
   /*@unused@*/double vels[])           /* (out) velocities */
{
  int iudp, judp;

  ROUTINE(udpSensitivity);

  /* --------------------------------------------------------------- */

  /* check that ebody matches one of the ebodys */
  iudp = 0;
  for (judp = 1; judp <= numUdp; judp++)
  {
    if (ebody == udps[judp].ebody)
    {
      iudp = judp;
      break;
    }
  }
  if (iudp <= 0)
  {
    return EGADS_NOTMODEL;
  }

  /* this routine is not written yet */
  return EGADS_NOLOAD;
}
