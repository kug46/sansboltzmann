// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Python/InputException.h"

#include "LIPSolver.h"

#include "Surreal/SurrealS.h"

#include "Adaptation/MOESS/NodalMetrics.h"
#include "Adaptation/TargetMetric.h"

#include "Meshing/EGADS/EGObject.h"
#include "Meshing/EGTess/WakedFarFieldBox.h"

#ifdef SANS_REFINE
#include "Meshing/refine/MultiScale_metric.h"
#endif
#ifdef SANS_AVRO
#include "Meshing/avro/XField3D_Wake_avro.h"
#include "Meshing/avro/MesherInterface_Wake_avro.h"
#endif

#define  USE_ADAPT 0

#include "Field/FieldVolume_CG_Cell.h"
#include "Field/FieldVolume_CG_BoundaryTrace.h"
#include "Field/FieldVolume_CG_BoundaryFrame.h"
#include "Field/XFieldLine_Traits.h"

#include "pde/FullPotential/LinearizedIncompressiblePotential3D_Trefftz.h"
#include "pde/FullPotential/IntegrandFunctorBoundary3D_LIP_Vn2.h"
#include "pde/FullPotential/OutputLIP3D.h"

#include "pde/NDConvert/OutputNDConvertSpace3D.h"

#include "Discretization/Galerkin/FunctionalBoundaryTrace_Galerkin.h"
#include "Discretization/Galerkin/JacobianFunctionalBoundaryTrace_Galerkin.h"
#include "Discretization/Galerkin/IntegrandBoundaryTrace_Output_Galerkin.h"

#include "Discretization/Potential_Drela/IntegrandBoundaryFrame_CG_LIP_Drela.h"
#include "Discretization/Potential_Drela/IntegrandBoundaryTrace_WakeCut_CG_LIP_Drela.h"

#include "Discretization/Potential_Drela/ResidualParamSensitivity_CG_Potential_Drela.h"

#include "Field/output_Tecplot.h"

#include "LinearAlgebra/SparseLinAlg/SparseNonZeroPattern.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "tools/output_std_vector.h"

#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

#ifdef SANS_PETSC
struct PETSc_fixture
{

  PETSc_fixture()
  {
    int narg = 0;
    char** argv = NULL;

    PetscInitialize(&narg, &argv, (char *)0, (char *)0);
  }

  ~PETSc_fixture()
  {
    PetscFinalize();
  }

};

static const PETSc_fixture PETSc_init;
#endif

// instantiations
LIPSolver::TetGenParams LIPSolver::TetGenParams::params;
LIPSolver::LIPCaseParams LIPSolver::LIPCaseParams::params;
LIPSolver::LIPSolveParams LIPSolver::LIPSolveParams::params;

PYDICT_PARAMETER_OPTION_INSTANTIATE(LIPSolver::LIPCaseParams::VolumeMesherOptions)

// cppcheck-suppress passedByValue
void LIPSolver::LIPCaseParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Sref));
  allParams.push_back(d.checkInputs(params.Cref));
  allParams.push_back(d.checkInputs(params.Bref));

  allParams.push_back(d.checkInputs(params.Xref));
  allParams.push_back(d.checkInputs(params.Yref));
  allParams.push_back(d.checkInputs(params.Zref));

  allParams.push_back(d.checkInputs(params.BCDict));

  allParams.push_back(d.checkInputs(params.SurfaceMesher));
  allParams.push_back(d.checkInputs(params.VolumeMesher));

  d.checkUnknownInputs(allParams);
}

// cppcheck-suppress passedByValue
void LIPSolver::TetGenParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Quality_Rad_Edge));
  allParams.push_back(d.checkInputs(params.Quality_Angle));

  d.checkUnknownInputs(allParams);
}


//---------------------------------------------------------------------------//
LIPSolver::
LIPSolver(const std::vector<EGADS::EGBody<3>>& bodies,
          const std::vector<Real>& boxsize,
          const PyDict& caseParams ) :
              context_( bodies.front() ),
              bodies_( EGADS::WakedFarFieldBox( bodies, boxsize, TrefftzFrames_ ) ), // add the farfield box
              tessModel_( nullptr ),
              solver_( nullptr ),
              pde_( 1, 0, 0 ), Sref_(0), Cref_(0), Bref_(0), Xref_(0), Yref_(0), Zref_(0),
              alpha_(0), beta_(0),
              rollr_(0), pitchr_(0), yawr_(0)

{
  LIPCaseParams::checkInputs(caseParams);

  // initialize the solver
  initMesh(caseParams);
}

//---------------------------------------------------------------------------//
LIPSolver::
LIPSolver(const std::vector<EGADS::EGBody<3>>& bodies,
          const PyDict& caseParams) :
              context_( bodies.front() ),
              bodies_( bodies ),
              tessModel_( nullptr ),
              solver_( nullptr ),
              pde_( 1, 0, 0 ), Sref_(0), Cref_(0), Bref_(0), Xref_(0), Yref_(0), Zref_(0),
              alpha_(0), beta_(0),
              rollr_(0), pitchr_(0), yawr_(0)
{
  LIPCaseParams::checkInputs(caseParams);

  // initialize the solver
  initMesh(caseParams);
}


//---------------------------------------------------------------------------//
LIPSolver::
LIPSolver( std::shared_ptr<XField3D_Wake>& pxfld,
           const std::map<std::string, std::vector<int> >& BCFaces,
           const std::vector<int>& WakeFaces,
           const std::vector<int>& TrefftzFrames,
           const PyDict& caseParams
          ) : context_( (EGADS::CreateContext()) ),
              tessModel_( nullptr ),
              solver_( nullptr ),
              pde_( 1, 0, 0 ),
              stab_(1),
              BCFaces_(BCFaces),
              WakeFaces_(WakeFaces),
              pxfld_(pxfld),
              Sref_(0), Cref_(0), Bref_(0), Xref_(0), Yref_(0), Zref_(0),
              alpha_(0), beta_(0),
              rollr_(0), pitchr_(0), yawr_(0),
              TrefftzFrames_(TrefftzFrames)
{
  LIPCaseParams::checkInputs(caseParams);

  // initialize the solver
  init(caseParams);
}

//---------------------------------------------------------------------------//
void
LIPSolver::
initMesh(const PyDict& caseParams)
{
  // Find the Wake and BC faces
  std::map<int,std::vector<int>> missingBC;
  {
    int BCFace = 0;
    for ( std::size_t ibody = 0; ibody < bodies_.size(); ibody++ ) //Solid bodies
    {
      if (!bodies_[ibody].isSolid()) continue;

      std::vector< EGADS::EGFace<3> > faces = bodies_[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        //faces[i].printAttributes(); std::cout << std::endl;
        if ( faces[i].hasAttribute("BCName") )
        {
          std::string BCName;
          faces[i].getAttribute("BCName", BCName);
          BCFaces_[BCName].push_back(BCFace);
          BCFace++;
        }
        else
          missingBC[ibody+1].push_back(i+1);
      }
    }

    for ( std::size_t ibody = 0; ibody < bodies_.size(); ibody++ ) //Sheet bodies
    {
      if (bodies_[ibody].isSolid()) continue;
      std::vector< EGADS::EGFace<3> > faces = bodies_[ibody].getFaces();
      for (std::size_t i = 0; i < faces.size(); i++)
      {
        if ( faces[i].hasAttribute("BCName") )
        {
          WakeFaces_.push_back(BCFace);
          BCFace++;
        }
        else
          missingBC[ibody+1].push_back(i+1);
      }
    }
  }

  if (!missingBC.empty())
  {
    std::cout << "Boundary conditions are missing on:" << std::endl;
    for (const std::pair<int,std::vector<int>>& faces : missingBC)
    {
      std::cout << "Body  : " << faces.first << std::endl;
      std::cout << "Faces : " << faces.second << std::endl;
      std::cout << std::endl;
    }
    SANS_ASSERT(false);
  }


  int nKuttaEdge = 0;

  for (std::size_t ibody = 0; ibody < bodies_.size(); ibody++)
  {
    if (bodies_[ibody].isSolid())
    {
      std::vector< EGADS::EGEdge<3> > solidEdges = bodies_[ibody].getEdges();
      for ( const EGADS::EGEdge<3>& edge : solidEdges )
      {
        //edge.printAttributes(); std::cout << std::endl;
        if (edge.hasAttribute(XField3D_Wake::WAKESHEET))
          nKuttaEdge++;
      }
    }
  }

  int nTrefftzEdge = nKuttaEdge;

  for (std::size_t ibody = 0; ibody < bodies_.size(); ibody++)
  {
    if (bodies_[ibody].isSheet())
    {
      std::vector< EGADS::EGEdge<3> > sheetEdges = bodies_[ibody].getEdges();
      for ( const EGADS::EGEdge<3>& edge : sheetEdges )
      {
        //edge.printAttributes(); std::cout << std::endl;
        if (edge.hasAttribute(XField3D_Wake::TREFFTZ))
        {
          TrefftzFrames_.push_back(nTrefftzEdge);
          nTrefftzEdge++;
        }
      }
    }
  }

  // Check that all BC types have a BC group, and vice versa
  PyDict PyBCList = caseParams.get(LIPCaseParams::params.BCDict);
  checkBCInputs(PyBCList, BCFaces_);

  // surface mesh:

  tessModel_ = new EGADS::EGTessModel( bodies_, caseParams );

  // volume mesh:
#if defined(SANS_REFINE) && defined(SANS_AVRO) && USE_ADAPT

  pxfld_ = std::make_shared<XField3D_Wake_avro>(*tessModel_);

#else
  DictKeyPair VolumeMesher = caseParams.get(LIPCaseParams::params.VolumeMesher);

#ifdef SANS_AFLR
  if (VolumeMesher == LIPCaseParams::params.VolumeMesher.AFLR3)
    pxfld_ = std::make_shared<AFLR3>(*tessModel_);
#endif

  if (VolumeMesher == LIPCaseParams::params.VolumeMesher.TetGen)
  {
    PyDict TetGen = VolumeMesher;

    Real maxRadiusEdgeRatio = caseParams.get(TetGenParams::params.Quality_Rad_Edge);
    Real minDihedralAngle = caseParams.get(TetGenParams::params.Quality_Angle);
    pxfld_ = std::make_shared<EGADS::EGTetGen>(*tessModel_, maxRadiusEdgeRatio, minDihedralAngle);
  }
#endif
  //output_Tecplot( *pxfld_, "grid.dat" );

  init(caseParams);
}


//---------------------------------------------------------------------------//
void
LIPSolver::
init(const PyDict& caseParams)
{
  Sref_ = caseParams.get(LIPCaseParams::params.Sref);
  Cref_ = caseParams.get(LIPCaseParams::params.Cref);
  Bref_ = caseParams.get(LIPCaseParams::params.Bref);

  Xref_ = caseParams.get(LIPCaseParams::params.Xref);
  Yref_ = caseParams.get(LIPCaseParams::params.Yref);
  Zref_ = caseParams.get(LIPCaseParams::params.Zref);

  pde_.setRefLocation({Xref_, Yref_, Zref_});

  PyBCList_ = caseParams.get(LIPCaseParams::params.BCDict);

  // Extract the keys from the dictionary after the BC's have been checked for correctness in the EquationSet
  std::vector<std::string> keys = PyBCList_.stringKeys();

  for (const std::string& key : keys)
  {
    // Create a BC parameter for the given key
    const ParameterOption<BCParams::BCOptions> BCParam{key, NO_DEFAULT, "Boundary Condition Dictionary"};

    DictKeyPair BC = PyBCList_.get(BCParam);

    // Gather up all solid faces
    if (BC == BCParams::params.BC.Wall || BC == BCParams::params.BC.ControlSurface)
      solidFaces_.insert(solidFaces_.end(), BCFaces_.at(key).begin(), BCFaces_.at(key).end());
  }

#ifdef SANS_PETSC
  PyDict PreconditionerDict;
  PyDict PreconditionerLU;
  PyDict PETScDict;

  PreconditionerDict[SLA::PETScSolverParam::params.Preconditioner.Name] = SLA::PETScSolverParam::params.Preconditioner.ILU;
  PreconditionerDict[SLA::PreconditionerILUParam::params.Ordering] = SLA::PreconditionerILUParam::params.Ordering.QDM;
  PreconditionerDict[SLA::PreconditionerILUParam::params.FillLevel] = 4;

  PETScDict[SLA::PETScSolverParam::params.RelativeTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.AbsoluteTolerance] = 1e-11;
  PETScDict[SLA::PETScSolverParam::params.MaxIterations] = 900;
  PETScDict[SLA::PETScSolverParam::params.GMRES_Restart] = 300;
  PETScDict[SLA::PETScSolverParam::params.Preconditioner] = PreconditionerDict;
  PETScDict[SLA::PETScSolverParam::params.Verbose] = true;
  PETScDict[SLA::PETScSolverParam::params.computeSingularValues] = false;
  PETScDict[SLA::PETScSolverParam::params.Timing] = true;
  PETScDict[SLA::PETScSolverParam::params.ResidualHistoryFile] = "";

  SLA::PETScSolverParam::checkInputs(PETScDict);
#endif

  int order = 1;
  stab_.setNitscheOrder(order);

#if defined(SANS_REFINE) && defined(SANS_AVRO) && USE_ADAPT
  int itmax = 10;
#else
  int itmax = 1;
#endif
  int iadapt = 0;

  pqfld_ = nullptr;
  plgfld_ = nullptr;
  pPrimalEqSet_ = nullptr;
  while (true)
  {

    // solution: Hierarchical, C0
    delete pqfld_;
    pqfld_ = new Field_CG_Cell<PhysD3, TopoD3, ArrayQ>(*pxfld_, order, BasisFunctionCategory_Hierarchical);
    *pqfld_ = 0;

    delete plgfld_;
    plgfld_ = new Field_CG_BoundaryTrace<PhysD3, TopoD3, ArrayQ>( *pxfld_, order, BasisFunctionCategory_Hierarchical, {} );
    *plgfld_ = 0;

    delete pPrimalEqSet_;
    pPrimalEqSet_ = new PrimalEquationSetClass(*pxfld_, *pqfld_, *plgfld_, pde_, stab_,
                                               {0}, WakeFaces_, PyBCList_, BCFaces_);

#ifdef SANS_PETSC
    solver_ = new SLA::PETScSolver<SystemMatrixClass>(PETScDict, *pPrimalEqSet_);
#elif INTEL_MKL
    solver_ = new SLA::MKL_PARDISO<SystemMatrixClass>(*pPrimalEqSet_, SLA::RegularSolve, true);
#else
    solver_ = new SLA::UMFPACK<SystemMatrixClass>(*pPrimalEqSet_, SLA::RegularSolve, true);
#endif

    std::cout << "Total Degrees of Freedom : " << pqfld_->nDOF() << std::endl;

    std::cout << "Factorizing matrix..." << std::endl;
    solver_->factorize();
    std::cout << "Done" << std::endl;

    if ( iadapt == itmax-1 ) break;

    //**********************
#if defined(SANS_REFINE) && defined(SANS_AVRO) && USE_ADAPT

    SystemVectorClass q(pPrimalEqSet_->vectorStateSize());
    SystemVectorClass rsd(pPrimalEqSet_->vectorEqSize()), dq(q.size());
    q = 0;
    rsd = 0;
    dq = 0;

    // residual
    pPrimalEqSet_->residual(q, rsd);

    // solve
    std::cout << "Solving primal system : ";
    solver_->backsolve(rsd, dq);

    // update the solution
    q -= dq;

    // set the field
    pPrimalEqSet_->setSolutionField(q);

    writeTecplot("grid_" + std::to_string(iadapt) + ".dat");


    refine_recon_reconstructions recon = refine_recon_Kexact;

    int p = 2;
    Real gradation = 3;

    int targetCost = 500000;

    Real nDOFperCell = 4;
    Real equiVol = sqrt(2.0)/12.0;
    Real targetComplexity = targetCost*equiVol/nDOFperCell;

    typedef DLA::MatrixSymS<PhysD3::D, Real> MatrixSym;

    // compute the multi-scale metric
    std::cout << "Computing multi-scale metric..." << std::endl;
    Field_CG_Cell<PhysD3, TopoD3, MatrixSym> metric_request(*pxfld_, 1, BasisFunctionCategory_Hierarchical);
    //MatrixSym M0 = DLA::Identity();
    //M0 = 15*M0;
    //metric_request = M0;

    MultiScale_metric(recon, p, gradation, targetComplexity, *pqfld_, metric_request);
    //output_Tecplot_Metric(metric_request, "metric.dat");
    std::cout << "Done!" << std::endl;

#if 0
    PyDict MOESSDict;
    MOESSDict[MOESSParams::params.CostModel] = MOESSParams::params.CostModel.LogEuclidean;
    MOESSDict[MOESSParams::params.Verbosity] = MOESSParams::VerbosityOptions::Progressbar;
    MOESSDict[MOESSParams::params.ImpliedMetric] = MOESSParams::params.ImpliedMetric.Optimized;

    SolverInterfaceDummy<PhysD3,TopoD3> problem;
    NodalMetrics<PhysD3,TopoD3> nodalMetrics(*pxfld_, {0}, problem, MOESSDict );

    // compute the nodal metrics which get as close as possible to the
    // target without changing the current mesh (implied metric) by a certain factor
    nodalMetrics.matchTargetMetric(metric_request);
    nodalMetrics.getNodalMetricRequestField(metric_request);
#endif

    CostModel_LogEuclidean<PhysD3,TopoD3> costModel(metric_request, {0});

    //Setting nDOF0 to zero computes the complexity rather than cost
    Real nDOF0 = 0;

    std::cout << "Target, Actual Complexity : " << targetComplexity << ", "
                                                << costModel.computeCost_Implied(nDOF0) << std::endl;

    PyDict d;
    MesherInterface<PhysD3, TopoD3, avroWakeMesher> mesher(iadapt, d);
    pxfld_ = mesher.adapt(metric_request, *pxfld_);

#if 0
    // create the output xfld
    const XField3D_Wake_avro& xfld_in = static_cast<const XField3D_Wake_avro&>(*pxfld_);
    std::shared_ptr< XField3D_Wake_avro > xfld_out;
    xfld_out = std::make_shared< XField3D_Wake_avro>(xfld_in, XFieldEmpty());

    avro::Mesh<avro::Simplex> avro_mesh( 3, 3 );

    xfld_in.convert( avro_mesh );
    xfld_out->import( avro_mesh );

    pxfld_ = xfld_out;
#endif

#endif
    iadapt++;
    //**********************
  }
}

//---------------------------------------------------------------------------//
PyDict
LIPSolver::solve(PyDict& solveparams)
{
  LIPSolveParams::checkInputs(solveparams);

  Real Vinf = 1;

  alpha_ = solveparams.get(LIPSolveParams::params.alpha);
  beta_  = solveparams.get(LIPSolveParams::params.beta);

  rollr_  = solveparams.get(LIPSolveParams::params.rollRate);
  pitchr_ = solveparams.get(LIPSolveParams::params.pitchRate);
  yawr_   = solveparams.get(LIPSolveParams::params.yawRate);

  Real cosa = cos(alpha_*PI/180);
  Real sina = sin(alpha_*PI/180);

  Real cosb = cos(beta_*PI/180);
  Real sinb = sin(beta_*PI/180);

  Real rollr  = rollr_  * 2*Vinf/Bref_;
  Real pitchr = pitchr_ * 2*Vinf/Cref_;
  Real yawr   = yawr_   * 2*Vinf/Bref_;

  DLA::VectorS<3, Real> Ddir = { cosb*cosa, -sinb, cosb*sina };
  DLA::VectorS<3, Real> Ldir = {     -sina,     0,      cosa };

  //Update the freestream direction and rotation rates
  pde_.setFreestream(Ddir);
  pde_.setRotationRates({rollr, pitchr, yawr});

  Ddir /= Vinf;
  Ldir /= Vinf;

  SystemVectorClass q(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass rsd(pPrimalEqSet_->vectorEqSize()), dq(q.size());
  q = 0;
  rsd = 0;
  dq = 0;

  // residual
  pPrimalEqSet_->residual(q, rsd);

  // solve
  std::cout << "Solving primal system : ";
  solver_->backsolve(rsd, dq);

  // update the solution
  q -= dq;

  // set the field
  pPrimalEqSet_->setSolutionField(q);


  typedef OutputNDConvertSpace<PhysD3, OutputLIP3D_Force<PDEClass>> NDOutputForce;
  typedef OutputNDConvertSpace<PhysD3, OutputLIP3D_Moment<PDEClass>> NDOutputMoment;
  typedef NDVectorCategory<boost::mpl::vector2<NDOutputForce, NDOutputMoment>, NDOutputForce::Category> NDOutVecCat;
  typedef IntegrandBoundaryTrace<PDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;

  std::vector<int> quadratureOrderTrace(pxfld_->nBoundaryTraceGroups(), 4);
  std::vector<int> quadratureOrderFrame(pxfld_->nBoundaryFrameGroups(), 4);


  // Cartesian forces and integrands
  NDOutputForce fcnBodyForceX(pde_, 1., 0., 0.);
  OutputIntegrandClass outputIntegrandForceX( fcnBodyForceX, solidFaces_ );

  NDOutputForce fcnBodyForceY(pde_, 0., 1., 0.);
  OutputIntegrandClass outputIntegrandForceY( fcnBodyForceY, solidFaces_ );

  NDOutputForce fcnBodyForceZ(pde_, 0., 0., 1.);
  OutputIntegrandClass outputIntegrandForceZ( fcnBodyForceZ, solidFaces_ );


  NDOutputMoment fcnMomentX(pde_, 1., 0., 0.);
  OutputIntegrandClass outputIntegrandMomentX( fcnMomentX, solidFaces_ );

  NDOutputMoment fcnMomentY(pde_, 0., 1., 0.);
  OutputIntegrandClass outputIntegrandMomentY( fcnMomentY, solidFaces_ );

  NDOutputMoment fcnMomentZ(pde_, 0., 0., 1.);
  OutputIntegrandClass outputIntegrandMomentZ( fcnMomentZ, solidFaces_ );

  // Foces in the body axis
  Real Fx = 0, Fy = 0, Fz = 0;

  // Compute Cartesian forces
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      FunctionalBoundaryTrace_Galerkin( outputIntegrandForceX, Fx ),
      *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      FunctionalBoundaryTrace_Galerkin( outputIntegrandForceY, Fy ),
      *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      FunctionalBoundaryTrace_Galerkin( outputIntegrandForceZ, Fz ),
      *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );

  // Moments in the body axis
  Real Mx = 0, My = 0, Mz = 0;

  // Compute moments
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      FunctionalBoundaryTrace_Galerkin( outputIntegrandMomentX, Mx ),
      *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      FunctionalBoundaryTrace_Galerkin( outputIntegrandMomentY, My ),
      *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
  IntegrateBoundaryTraceGroups<TopoD3>::integrate(
      FunctionalBoundaryTrace_Galerkin( outputIntegrandMomentZ, Mz ),
      *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );

  // Trefftz plane forces
  Real TrefftzDrag = 0;
  Real TrefftzLift = 0;
  Real TrefftzY = 0;

  IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzInducedDrag, Real> fcnTrefftzDrag(pde_, TrefftzFrames_);
  IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Real> fcnTrefftzLift(pde_, Ldir, TrefftzFrames_);
  IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Real> fcnTrefftzY(pde_, {0., 1., 0.}, TrefftzFrames_);

  FunctionalTrefftz::integrate(fcnTrefftzDrag, *pxfld_, *pqfld_, quadratureOrderFrame.data(), quadratureOrderFrame.size(), TrefftzDrag);
  FunctionalTrefftz::integrate(fcnTrefftzLift, *pxfld_, *pqfld_, quadratureOrderFrame.data(), quadratureOrderFrame.size(), TrefftzLift);
  FunctionalTrefftz::integrate(fcnTrefftzY,    *pxfld_, *pqfld_, quadratureOrderFrame.data(), quadratureOrderFrame.size(), TrefftzY);


  Real rho = 1;
  Real Vinf2 = 1;

  Real AR = Bref_/Cref_;

  Real Q = (0.5*rho*Vinf2*Sref_);

  // Forces in body axis
  Real CXtot = -Fx/Q;
  Real CYtot =  Fy/Q;
  Real CZtot = -Fz/Q;

  // Moments in body axis
  Real Cltot = -Mx / (Q*Bref_);
  Real Cmtot =  My / (Q*Cref_);
  Real Cntot = -Mz / (Q*Bref_);

  // Forces in the stability axis
  Real CLtot = (-sina*Fx + cosa*Fz) / Q;
  Real CDtot = ( cosa*Fx + sina*Fz) / Q;

  // Span efficiency
  Real spane = 0;
  if (CDtot != 0.0)
    spane = (CLtot*CLtot + CYtot*CYtot) / (PI * AR * CDtot);

  // Moments in the stability axis
  Real cltot = (-cosa*Mx - sina*Mz) / (Q*Bref_);
  Real cmtot = (         My       ) / (Q*Cref_);
  Real cntot = ( sina*Mx - cosa*Mz) / (Q*Bref_);

  Real CLff = TrefftzLift / Q;
  Real CYff = TrefftzY    / Q;
  Real CDff = TrefftzDrag / Q;

  // Span efficiency
  Real spaneff = 0;
  if (CDff != 0.0)
    spaneff = (CLff*CLff + CYff*CYff) / (PI * AR * CDff);

  //std::cout << "Cref = " << Cref_ << std::endl;
  //std::cout << "Bref = " << Bref_ << std::endl;
  //std::cout << "Xref, Yref, Zref = " << Xref_ << ", " << Yref_ << ", " << Zref_ << std::endl;

#if 0
  std::cout << std::setprecision(16);
  // Body axis
  std::cout << "CXtot = " << CXtot << std::endl;
  std::cout << "CYtot = " << CYtot << std::endl;
  std::cout << "CZtot = " << CZtot << std::endl;

  std::cout << "Cltot = " << Cltot << std::endl;
  std::cout << "Cmtot = " << Cmtot << std::endl;
  std::cout << "Cntot = " << Cntot << std::endl;

  // Stability axis
  std::cout << "CLtot = " << CLtot << std::endl;
  std::cout << "CDtot = " << CDtot << std::endl;

  std::cout << "Cl'tot = " << cltot << std::endl;
  std::cout << "Cm'tot = " << cmtot << std::endl;
  std::cout << "Cn'tot = " << cntot << std::endl;

  // Trefftz plane
  std::cout << "CLff = " << CLff << std::endl;
  std::cout << "CYff = " << CYff << std::endl;
  std::cout << "CDff = " << CDff << std::endl;
  std::cout << "   e = " << spanef << std::endl;
#endif

  PyDict results;
  // Body axis force and moments
  results["CXtot"] = CXtot;
  results["CYtot"] = CYtot;
  results["CZtot"] = CZtot;

  results["Cltot"] = Cltot;
  results["Cmtot"] = Cmtot;
  results["Cntot"] = Cntot;

  // Stability axis force and moments
  results["CLtot"] = CLtot;
  results["CDtot"] = CDtot;
  results["etot"]  = spane;

  results["Cl'tot"] = cltot;
  results["Cm'tot"] = cmtot;
  results["Cn'tot"] = cntot;

  // Trefftz plane forces
  results["CLff"] = CLff;
  results["CYff"] = CYff;
  results["CDff"] = CDff;
  results["eff"]  = spaneff;

  return results;
}

//---------------------------------------------------------------------------//
void
LIPSolver::StabilityDerivatives(PyDict& result) const
{
  typedef SurrealS<6> Surreal;

  typedef PDENDConvertSpace<PhysD3, PDELinearizedIncompressiblePotential3D<Surreal>> PDESensClass;

  typedef ResidualParamSensitivity_CG_Potential_Drela< PDESensClass, BCNDConvertSpace, BCLinearizedIncompressiblePotential3DVector<Surreal>,
                                                       Surreal, XField3D_Wake > SensEquationSetClass;
  typedef SensEquationSetClass::SystemVectorClass SystemVectorSurrealClass;

  Real Vinf = 1;

  Real cosar = cos(alpha_*PI/180);
  Real sinar = sin(alpha_*PI/180);

  Real cosbr = cos(beta_*PI/180);
  Real sinbr = sin(beta_*PI/180);

  Surreal u = cosbr*cosar;  u.deriv(0) = 1;
  Surreal v = -sinbr;       v.deriv(1) = 1;
  Surreal w = cosbr*sinar;  w.deriv(2) = 1;

  Surreal rollr  = rollr_;   rollr.deriv(3) = 1;
  Surreal pitchr = pitchr_; pitchr.deriv(4) = 1;
  Surreal yawr   = yawr_;     yawr.deriv(5) = 1;

  rollr  *= 2*Vinf/Bref_;
  pitchr *= 2*Vinf/Cref_;
  yawr   *= 2*Vinf/Bref_;

  DLA::VectorS<3, Surreal> Ddir = {  u, v, w };
  DLA::VectorS<3, Surreal> Ldir = { -w, v, u };
  DLA::VectorS<3, Surreal> Ydir = {  0, 1, 0 };

  DLA::VectorS<3, Surreal> Omega = {rollr, pitchr, yawr};
  DLA::VectorS<3, Surreal> Xdir  = {Xref_, Yref_, Zref_};

  //Update the freestream direction and rotation rates
  PDESensClass pde(Ddir, Omega, Xdir);


  std::vector<int> quadratureOrderTrace(pxfld_->nBoundaryTraceGroups(), 4);

  Surreal Fx = 0, Fy = 0, Fz = 0;
  Surreal Mx = 0, My = 0, Mz = 0;

  {
    typedef OutputNDConvertSpace<PhysD3, OutputLIP3D_Force<PDESensClass>> NDOutputForce;
    typedef OutputNDConvertSpace<PhysD3, OutputLIP3D_Moment<PDESensClass>> NDOutputMoment;
    typedef NDVectorCategory<boost::mpl::vector2<NDOutputForce, NDOutputMoment>, NDOutputForce::Category> NDOutVecCat;
    typedef IntegrandBoundaryTrace<PDESensClass, NDOutVecCat, Galerkin> OutputIntegrandClass;

    // Cartesian forces and integrands
    NDOutputForce fcnBodyForceX(pde, 1., 0., 0.);
    OutputIntegrandClass outputIntegrandForceX( fcnBodyForceX, solidFaces_ );

    NDOutputForce fcnBodyForceY(pde, 0., 1., 0.);
    OutputIntegrandClass outputIntegrandForceY( fcnBodyForceY, solidFaces_ );

    NDOutputForce fcnBodyForceZ(pde, 0., 0., 1.);
    OutputIntegrandClass outputIntegrandForceZ( fcnBodyForceZ, solidFaces_ );


    NDOutputMoment fcnMomentX(pde, 1., 0., 0.);
    OutputIntegrandClass outputIntegrandMomentX( fcnMomentX, solidFaces_ );

    NDOutputMoment fcnMomentY(pde, 0., 1., 0.);
    OutputIntegrandClass outputIntegrandMomentY( fcnMomentY, solidFaces_ );

    NDOutputMoment fcnMomentZ(pde, 0., 0., 1.);
    OutputIntegrandClass outputIntegrandMomentZ( fcnMomentZ, solidFaces_ );

    // Compute Cartesian forces
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( outputIntegrandForceX, Fx ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( outputIntegrandForceY, Fy ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( outputIntegrandForceZ, Fz ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );

    // Compute Cartesian moments
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( outputIntegrandMomentX, Mx ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( outputIntegrandMomentY, My ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        FunctionalBoundaryTrace_Galerkin( outputIntegrandMomentZ, Mz ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
  }

  SystemVectorClass dFx_dq(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass dFy_dq(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass dFz_dq(pPrimalEqSet_->vectorStateSize());

  SystemVectorClass dMx_dq(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass dMy_dq(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass dMz_dq(pPrimalEqSet_->vectorStateSize());

  dFx_dq = 0;
  dFy_dq = 0;
  dFz_dq = 0;

  dMx_dq = 0;
  dMy_dq = 0;
  dMz_dq = 0;

  {
    typedef OutputNDConvertSpace<PhysD3, OutputLIP3D_Force<PDEClass>> NDOutputForce;
    typedef OutputNDConvertSpace<PhysD3, OutputLIP3D_Moment<PDEClass>> NDOutputMoment;
    typedef NDVectorCategory<boost::mpl::vector2<NDOutputForce, NDOutputMoment>, NDOutputForce::Category> NDOutVecCat;
    typedef IntegrandBoundaryTrace<PDEClass, NDOutVecCat, Galerkin> OutputIntegrandClass;


    // Cartesian forces and integrands
    NDOutputForce fcnBodyForceX(pde_, 1., 0., 0.);
    OutputIntegrandClass outputIntegrandForceX( fcnBodyForceX, solidFaces_ );

    NDOutputForce fcnBodyForceY(pde_, 0., 1., 0.);
    OutputIntegrandClass outputIntegrandForceY( fcnBodyForceY, solidFaces_ );

    NDOutputForce fcnBodyForceZ(pde_, 0., 0., 1.);
    OutputIntegrandClass outputIntegrandForceZ( fcnBodyForceZ, solidFaces_ );

    // Cartesian moments and integrands
    NDOutputMoment fcnMomentX(pde_, 1., 0., 0.);
    OutputIntegrandClass outputIntegrandMomentX( fcnMomentX, solidFaces_ );

    NDOutputMoment fcnMomentY(pde_, 0., 1., 0.);
    OutputIntegrandClass outputIntegrandMomentY( fcnMomentY, solidFaces_ );

    NDOutputMoment fcnMomentZ(pde_, 0., 0., 1.);
    OutputIntegrandClass outputIntegrandMomentZ( fcnMomentZ, solidFaces_ );

    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        JacobianFunctionalBoundaryTrace_Galerkin<SurrealS<1>>( outputIntegrandForceX, dFx_dq[0] ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        JacobianFunctionalBoundaryTrace_Galerkin<SurrealS<1>>( outputIntegrandForceY, dFy_dq[0] ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        JacobianFunctionalBoundaryTrace_Galerkin<SurrealS<1>>( outputIntegrandForceZ, dFz_dq[0] ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );

    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        JacobianFunctionalBoundaryTrace_Galerkin<SurrealS<1>>( outputIntegrandMomentX, dMx_dq[0] ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        JacobianFunctionalBoundaryTrace_Galerkin<SurrealS<1>>( outputIntegrandMomentY, dMy_dq[0] ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
    IntegrateBoundaryTraceGroups<TopoD3>::integrate(
        JacobianFunctionalBoundaryTrace_Galerkin<SurrealS<1>>( outputIntegrandMomentZ, dMz_dq[0] ),
        *pxfld_, *pqfld_, quadratureOrderTrace.data(), quadratureOrderTrace.size() );
  }



  std::vector<int> quadratureOrderFrame(pxfld_->nBoundaryFrameGroups(), 4);

  Surreal TrefftzLift = 0;
  Surreal TrefftzY = 0;
  Surreal TrefftzDrag = 0;

  {
    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Surreal> fcnTrefftzLift(pde, Ldir, TrefftzFrames_);
    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Surreal> fcnTrefftzY(pde, Ydir, TrefftzFrames_);
    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzInducedDrag, Surreal> fcnTrefftzDrag(pde, TrefftzFrames_);

    FunctionalTrefftz::integrate(fcnTrefftzLift, *pxfld_, *pqfld_, quadratureOrderFrame.data(), quadratureOrderFrame.size(), TrefftzLift);
    FunctionalTrefftz::integrate(fcnTrefftzY,    *pxfld_, *pqfld_, quadratureOrderFrame.data(), quadratureOrderFrame.size(), TrefftzY);
    FunctionalTrefftz::integrate(fcnTrefftzDrag, *pxfld_, *pqfld_, quadratureOrderFrame.data(), quadratureOrderFrame.size(), TrefftzDrag);
  }

  SystemVectorClass dTrefftzLift_dq(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass dTrefftzY_dq(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass dTrefftzDrag_dq(pPrimalEqSet_->vectorStateSize());

  dTrefftzLift_dq = 0;
  dTrefftzY_dq = 0;
  dTrefftzDrag_dq = 0;

  {
    DLA::VectorS<3, Real> Ldir = { cosbr*cosar, -sinbr, cosbr*sinar };
    DLA::VectorS<3, Real> Ydir = {           0,      1,           0 };

    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzInducedDrag, Real> fcnTrefftzDrag(pde_, TrefftzFrames_);
    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Real> fcnTrefftzLift(pde_, Ldir, TrefftzFrames_);
    IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Real> fcnTrefftzY(pde_, Ydir, TrefftzFrames_);

    JacobianFunctionalTrefftz<SurrealS<1>>::integrate(fcnTrefftzLift, *pxfld_, *pqfld_,
                                                      quadratureOrderFrame.data(), quadratureOrderFrame.size(),
                                                      dTrefftzLift_dq[0]);
    JacobianFunctionalTrefftz<SurrealS<1>>::integrate(fcnTrefftzY, *pxfld_, *pqfld_,
                                                      quadratureOrderFrame.data(), quadratureOrderFrame.size(),
                                                      dTrefftzY_dq[0]);
    JacobianFunctionalTrefftz<SurrealS<1>>::integrate(fcnTrefftzDrag, *pxfld_, *pqfld_,
                                                      quadratureOrderFrame.data(), quadratureOrderFrame.size(),
                                                      dTrefftzDrag_dq[0]);
  }

  SystemVectorClass psi_Fx(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass psi_Fy(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass psi_Fz(pPrimalEqSet_->vectorStateSize());

  SystemVectorClass psi_Mx(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass psi_My(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass psi_Mz(pPrimalEqSet_->vectorStateSize());

  SystemVectorClass psi_TrefftzDrag(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass psi_TrefftzLift(pPrimalEqSet_->vectorStateSize());
  SystemVectorClass psi_TrefftzY(pPrimalEqSet_->vectorStateSize());

  psi_Fx = 0;
  psi_Fy = 0;
  psi_Fz = 0;

  psi_Mx = 0;
  psi_My = 0;
  psi_Mz = 0;

// Not solving for Trefftz derivatives for now...
  psi_TrefftzLift = 0;
  psi_TrefftzY    = 0;
  psi_TrefftzDrag = 0;

  // Both PETSc and UMFPACK have backsolveTranspose
  std::cout << "Solving Fx adjoint : ";
  solver_->backsolveTranspose(dFx_dq, psi_Fx);
  std::cout << "Solving Fy adjoint : ";
  solver_->backsolveTranspose(dFy_dq, psi_Fy);
  std::cout << "Solving Fz adjoint : ";
  solver_->backsolveTranspose(dFz_dq, psi_Fz);

  std::cout << "Solving Mx adjoint : ";
  solver_->backsolveTranspose(dMx_dq, psi_Mx);
  std::cout << "Solving My adjoint : ";
  solver_->backsolveTranspose(dMy_dq, psi_My);
  std::cout << "Solving Mz adjoint : ";
  solver_->backsolveTranspose(dMz_dq, psi_Mz);

  //solver_->backsolveTranspose(dTrefftzLift_dq, psi_TrefftzLift);
  //solver_->backsolveTranspose(dTrefftzY_dq   , psi_TrefftzY   );
  //solver_->backsolveTranspose(dTrefftzDrag_dq, psi_TrefftzDrag);

  StabilizationNitsche stab(1);
  SensEquationSetClass SensEqSet(*pxfld_, *pqfld_, *plgfld_, pde, stab,
                                 {0}, WakeFaces_, PyBCList_, BCFaces_);


  SystemVectorSurrealClass rsd(pPrimalEqSet_->vectorEqSize());

  rsd = 0;
  SensEqSet.residualSensitivity(rsd);

  // Add the sensitivity from the adjoints
  for (int j = 0; j < rsd.m(); j++)
    for (int k = 0; k < rsd[j].m(); k++)
      for (int n = 0; n < Surreal::N; n++)
      {
        Fx.deriv(n) -= psi_Fx[j][k]*rsd[j][k].deriv(n);
        Fy.deriv(n) -= psi_Fy[j][k]*rsd[j][k].deriv(n);
        Fz.deriv(n) -= psi_Fz[j][k]*rsd[j][k].deriv(n);

        Mx.deriv(n) -= psi_Mx[j][k]*rsd[j][k].deriv(n);
        My.deriv(n) -= psi_My[j][k]*rsd[j][k].deriv(n);
        Mz.deriv(n) -= psi_Mz[j][k]*rsd[j][k].deriv(n);

        TrefftzLift.deriv(n) -= psi_TrefftzLift[j][k]*rsd[j][k].deriv(n);
        TrefftzY.deriv(n)    -= psi_TrefftzY[j][k]   *rsd[j][k].deriv(n);
        TrefftzDrag.deriv(n) -= psi_TrefftzDrag[j][k]*rsd[j][k].deriv(n);
      }

  Real rho = 1;
  Real Vinf2 = 1;

  Real Q = (0.5*rho*Vinf2*Sref_);

  {
    // Forces in body axis
    Surreal CXtot = -Fx / Q;
    Surreal CYtot =  Fy / Q;
    Surreal CZtot = -Fz / Q;

    // Moments in body axis
    Surreal Cltot = -Mx / (Q*Bref_);
    Surreal Cmtot =  My / (Q*Cref_);
    Surreal Cntot = -Mz / (Q*Bref_);

    {
      std::vector<std::string> names = {"u", "v", "w", "p", "q", "r"};

      for (int n = 0; n < Surreal::N; n++)
      {
        // Body axis
        result["CX" + names[n]] = CXtot.deriv(n);
        result["CY" + names[n]] = CYtot.deriv(n);
        result["CZ" + names[n]] = CZtot.deriv(n);
        result["Cl" + names[n]] = Cltot.deriv(n);
        result["Cm" + names[n]] = Cmtot.deriv(n);
        result["Cn" + names[n]] = Cntot.deriv(n);
      }
    }
  }

  // Setup to compute the chain rule to get derivatives w.r.t alpha and beta
  Surreal alpha = alpha_; alpha.deriv(0) = 1;
  Surreal beta  = beta_;  beta.deriv(1)  = 1;

  Surreal cosa = cos(alpha*PI/180);
  Surreal sina = sin(alpha*PI/180);

  Surreal cosb = cos(beta*PI/180);
  Surreal sinb = sin(beta*PI/180);

  //DLA::VectorS<3, Surreal> Ddir = { cosb*cosa, -sinb, cosb*sina };
  u = cosb*cosa;
  v = -sinb;
  w = cosb*sina;

  // Swap derivatives if Body forces and moments w.r.t {u, v, w} to {alpha, beta}
  Surreal tmp[6];

  tmp[0] = Fx.value();
  tmp[1] = Fy.value();
  tmp[2] = Fz.value();

  tmp[3] = Mx.value();
  tmp[4] = My.value();
  tmp[5] = Mz.value();

  for (int n = 0; n < 2; n++)
  {
    //                    d?/du * du/d[a,b] +      d?/dv * dv/d[a,b] +      d?/dw * dw/d[a,b]
    tmp[0].deriv(n) = Fx.deriv(0)*u.deriv(n) + Fx.deriv(1)*v.deriv(n) + Fx.deriv(2)*w.deriv(n);
    tmp[1].deriv(n) = Fy.deriv(0)*u.deriv(n) + Fy.deriv(1)*v.deriv(n) + Fy.deriv(2)*w.deriv(n);
    tmp[2].deriv(n) = Fz.deriv(0)*u.deriv(n) + Fz.deriv(1)*v.deriv(n) + Fz.deriv(2)*w.deriv(n);

    tmp[3].deriv(n) = Mx.deriv(0)*u.deriv(n) + Mx.deriv(1)*v.deriv(n) + Mx.deriv(2)*w.deriv(n);
    tmp[4].deriv(n) = My.deriv(0)*u.deriv(n) + My.deriv(1)*v.deriv(n) + My.deriv(2)*w.deriv(n);
    tmp[5].deriv(n) = Mz.deriv(0)*u.deriv(n) + Mz.deriv(1)*v.deriv(n) + Mz.deriv(2)*w.deriv(n);
  }

  Fx = tmp[0];
  Fy = tmp[1];
  Fz = tmp[2];

  Mx = tmp[3];
  My = tmp[4];
  Mz = tmp[5];

  {
    // Forces in the stability axis
    Surreal CLtot = (-sina*Fx + cosa*Fz) / Q;
    Surreal CYtot = (         Fy       ) / Q;
    Surreal CDtot = ( cosa*Fx + sina*Fz) / Q;

    // Moments in the stability axis
    Surreal cltot = (-cosa*Mx - sina*Mz) / (Q*Bref_);
    Surreal cmtot = (         My       ) / (Q*Cref_);
    Surreal cntot = ( sina*Mx - cosa*Mz) / (Q*Bref_);

    {
      std::vector<std::string> names = {"a", "b"};

      for (int n = 0; n < 2; n++)
      {
        result["CL"  + names[n]] = CLtot.deriv(n);
        result["CY"  + names[n]] = CYtot.deriv(n);
        result["Cl'" + names[n]] = cltot.deriv(n);
        result["Cm'" + names[n]] = cmtot.deriv(n);
        result["Cn'" + names[n]] = cntot.deriv(n);
      }
    }
  }

  // Trefftz plane forces
  Surreal CLff = TrefftzLift / Q;
  Surreal CYff = TrefftzY    / Q;
  Surreal CDff = TrefftzDrag / Q;
}

//---------------------------------------------------------------------------//
void
LIPSolver::writeTecplot(const std::string& filename) const
{
  output_Tecplot_LIP( pde_, *pqfld_, filename );
}

//---------------------------------------------------------------------------//
LIPSolver::~LIPSolver()
{
  delete tessModel_;
  delete pqfld_;
  delete plgfld_;
  delete pPrimalEqSet_;
  delete solver_;
}

}
