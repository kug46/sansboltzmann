INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

IF( USE_GLPK )
  ADD_SUBDIRECTORY( Linear )
ENDIF()
