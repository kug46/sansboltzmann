// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LOPTSOLVERBASE_H
#define LOPTSOLVERBASE_H

#include "LinearAlgebra/VectorType.h"

namespace SANS
{
namespace LOPT
{

//Forward declare
template<class Matrix_type_, class Vector_type_edge_>
class LOptSolverBase;

//=============================================================================


//----------------------------------------------------------------------------//
// A base class for all linear optimization solvers
//----------------------------------------------------------------------------//

// In linear optimization we solve problems of the form
//    min_x   c^T*x
//    s.t.    A*x <= a
//            B*x  = b
//        lb <= x <= ub
//
//   Constrait vector       : Vector_C_type (i.e. a, b)
//   Design Variable vector : Vector_DV_type (i.e. x)

template<class Matrix_type_, class Vector_C_type_>
class LOptSolverBase
{
public:
  typedef Matrix_type_ Matrix_type;
  typedef Vector_C_type_ Vector_C_type;

  typedef typename VectorType<Matrix_type>::type Vector_DV_type;

  LOptSolverBase() {}
  virtual ~LOptSolverBase() {}

  virtual void setConstraints( const Matrix_type& A, const Matrix_type& B,
                               const Vector_C_type& a, const Vector_C_type& b ) = 0;

  virtual void setConstraints( const Matrix_type& A, const Vector_C_type& a, const bool eq ) = 0;

  virtual void solve(const Vector_DV_type& c, Vector_DV_type& x, double& z ) = 0;
};

} //namespace LOPT
} //namespace SANS


#endif //LOPTSOLVERBASE_H
