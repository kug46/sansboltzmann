// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define GLPK_INSTANTIATE
#include "GLPKSolver_impl.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

#include <memory>

using namespace SANS::SLA;

namespace SANS
{
namespace LOPT
{

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::setConstraints( const Matrix_type& A, const Matrix_type& B,
    const Vector_C_type& a, const Vector_C_type& b )
{
  deallocate();

  lp = glp_create_prob();

  SANS_ASSERT( B.m() == b.m() );
  SANS_ASSERT( A.n() == B.n() );

  // double info[UMFPACK_INFO] = {0};
  // SANS_GLPK_INT m_a   = A.m();
  SANS_GLPK_INT nnz_a = A.getNumNonZero();
  // SANS_GLPK_INT m_b   = b.m();
  SANS_GLPK_INT nnz_b = B.getNumNonZero();

  // initialize rows
  glp_add_rows(lp, nnz_a + nnz_b);

  // convert from CRS to GLPK storage format (which is more memory intensive, i.e. 3x nnz)
  SANS_GLPK_INT *ia = new SANS_GLPK_INT[nnz_a + nnz_b + 1]; // Note indexed from 1!
  SANS_GLPK_INT *ja = new SANS_GLPK_INT[nnz_a + nnz_b + 1]; // Note indexed from 1!
  double *ar        = new double[nnz_a + nnz_b + 1]; // Note indexed from 1!

  //Copy over the index values as they SANS_GLPK_INT may not be an int
  int *row_ptr_a = A.get_row_ptr();
  int *col_ind_a = A.get_col_ind();
  int *row_ptr_b = B.get_row_ptr();
  int *col_ind_b = B.get_col_ind();

  double *Rx_a = A.get_values();
  double *Rx_b = B.get_values();

  // convert A matrix
  ia[0] = 0; // not used
  ja[0] = 0; // not used
  int kk = 0; // counter over rows (for CRS storage)
  for (SANS_GLPK_INT jj = 0; jj < nnz_a; jj++)
  {
    ja[jj+1] = col_ind_a[jj] + 1; // Note indexed from 1!

    if ( jj < row_ptr_a[kk+1] )
    {
      ia[jj+1] = kk + 1; // Note indexed from 1!
    }

    if ( jj == (row_ptr_a[kk+1] - 1) )
      kk++;

    ar[jj+1] = Rx_a[jj];
  }

  // convert B matrix
  kk = 0; // counter over rows (for CRS storage)
  for (SANS_GLPK_INT jj = 0; jj < nnz_b; jj++)
  {
    ja[jj+1+nnz_a] = col_ind_b[jj] + 1; // Note indexed from 1!

    if ( jj < row_ptr_b[kk+1] )
    {
      ia[jj+1+nnz_a] = kk + 1 + a.m(); // Note indexed from 1!
    }

    if ( jj == (row_ptr_b[kk+1] - 1) )
      kk++;

    ar[jj+1+nnz_a] = Rx_b[jj];          // Note indexed from 1!
  }

  // Add constraints - equality
  for (SANS_GLPK_INT jj = 0; jj < a.m(); jj++)
  {
    glp_set_row_bnds(lp, jj+1, GLP_UP, 0.0, a[jj]); // Note indexed from 1!
  }
  // Add constraints - inequality
  for (SANS_GLPK_INT jj = 0; jj < b.m(); jj++)
  {
    glp_set_row_bnds(lp, jj+a.m()+1, GLP_FX, b[jj], b[jj]); // Note indexed from 1!
  }

  // Add constraints on design variables
  // NOTE: we set constraints on each design variable from 0 to infinity.
  glp_add_cols(lp, A.n());
  for ( SANS_GLPK_INT jj = 0; jj < A.n(); jj++ )
  {
    glp_set_col_bnds(lp, jj + 1, GLP_LO, 0.0, 0.0);
    // should we set name?
  }

  // Load matrix into linear program
  glp_load_matrix(lp, nnz_a + nnz_b, ia, ja, ar); // By doing this, should ia_, ja_, ar_ actually be part of the class?

  // deallocate ia, ja, ar
  delete [] ia;
  delete [] ja;
  delete [] ar;

}

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::setConstraints( const Matrix_type& A,
    const Vector_C_type& a, const bool eq )
{
  deallocate();

  lp = glp_create_prob();

  SANS_ASSERT( A.m() == a.m() );

  // double info[UMFPACK_INFO] = {0};
  // SANS_GLPK_INT m_a   = A.m();
  SANS_GLPK_INT nnz = A.getNumNonZero();

  // initialize rows
  glp_add_rows(lp, nnz);

  // convert from CRS to GLPK storage format (which is more memory intensive, i.e. 3x nnz)
  SANS_GLPK_INT *ia = new SANS_GLPK_INT[nnz + 1]; // Note indexed from 1!
  SANS_GLPK_INT *ja = new SANS_GLPK_INT[nnz + 1]; // Note indexed from 1!
  double *ar        = new double[nnz + 1]; // Note indexed from 1!

  //Copy over the index values as they SANS_GLPK_INT may not be an int
  int *row_ptr = A.get_row_ptr();
  int *col_ind = A.get_col_ind();

  double *Rx = A.get_values();

  // convert A matrix
  ia[0]  = 0; // not used
  ja[0]  = 0; // not used
  int kk = 0; // counter over rows (for CRS storage)
  for (SANS_GLPK_INT jj = 0; jj < nnz; jj++)
  {
    ja[jj+1] = col_ind[jj] + 1; // Note indexed from 1!

    if ( jj < row_ptr[kk+1] )
    {
      ia[jj+1] = kk + 1; // Note indexed from 1!
    }

    if ( jj == (row_ptr[kk+1] - 1) )
      kk++;

    ar[jj+1] = Rx[jj];
  }

  // Add constraints
  if (eq)
  {
    for (SANS_GLPK_INT jj = 0; jj < a.m(); jj++)
    {
      glp_set_row_bnds(lp, jj+1, GLP_FX, a[jj], a[jj]); // Note indexed from 1!
    }
  }
  else
  {
    for (SANS_GLPK_INT jj = 0; jj < a.m(); jj++)
    {
      glp_set_row_bnds(lp, jj+1, GLP_UP, 0.0, a[jj]); // Note indexed from 1!
    }
  }

  // Add constraints on design variables
  // NOTE: we set constraints on each design variable from 0 to infinity.
  glp_add_cols(lp, A.n());
  for ( SANS_GLPK_INT jj = 0; jj < A.n(); jj++ )
  {
    glp_set_col_bnds(lp, jj + 1, GLP_LO, 0.0, 0.0);
    // should we set name?
  }

  // Load matrix into linear program
  glp_load_matrix(lp, nnz, ia, ja, ar); // By doing this, should ia_, ja_, ar_ actually be part of the class?

  // deallocate indices
  delete [] ia;
  delete [] ja;
  delete [] ar;
}

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::solve( const Vector_DV_type& c, Vector_DV_type& x, double &z )
{ // can't be const because we are changing the linear program, by solving it.

  SANS_ASSERT( c.m() == x.m()  );

  // set objective vector
  for (SANS_GLPK_INT jj = 0; jj < c.m(); jj++)
  {
    glp_set_obj_coef(lp, jj+1, c[jj]); // Note indexed from 1!
  }

  // solve LP

  SANS_GLPK_INT status = 0;

  DictKeyPair TypeParam = SolverParam_.get(params.SolutionMethod);
   if (TypeParam == params.SolutionMethod.Simplex)
   {
     PyDict simplexparam = TypeParam;

     glp_smcp parm;
     glp_init_smcp(&parm);
     parm.msg_lev  = simplexparam.get(GLPKParam::SimplexParam::params.msg_lev);
     parm.meth     = simplexparam.get(GLPKParam::SimplexParam::params.meth);
     parm.pricing  = simplexparam.get(GLPKParam::SimplexParam::params.pricing);
     parm.it_lim   = simplexparam.get(GLPKParam::SimplexParam::params.it_lim);
     parm.presolve = simplexparam.get(GLPKParam::SimplexParam::params.presolve);

     status = glp_simplex(lp, &parm);
   }
   else if (TypeParam == params.SolutionMethod.InteriorPoint)
   {
     PyDict interiorparam = TypeParam;

     glp_iptcp parm;
     glp_init_iptcp(&parm);
     parm.msg_lev = interiorparam.get(GLPKParam::InteriorPointParam::params.msg_lev);
     parm.ord_alg = interiorparam.get(GLPKParam::InteriorPointParam::params.ord_alg);

     status = glp_interior(lp, &parm);
   }
   else
     SANS_DEVELOPER_EXCEPTION("Unknown solution method");


  if (status != 0)
  {
    BOOST_THROW_EXCEPTION( GLPKException( status ) );
  }

  // recover solution
  if (TypeParam == params.SolutionMethod.Simplex)
  {
    for (SANS_GLPK_INT jj = 0; jj < x.m(); jj++)
    {
      x[jj] = glp_get_col_prim(lp, jj+1);
    }
    z = glp_get_obj_val(lp);
  }
  else if (TypeParam == params.SolutionMethod.InteriorPoint)
  {
    for (SANS_GLPK_INT jj = 0; jj < x.m(); jj++)
    {
      x[jj] = glp_ipt_col_prim(lp, jj+1);
    }
    z = glp_ipt_obj_val(lp);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown solution method");
}

template class GLPK< SparseMatrix_CRS<double>, SparseVector<double> >;

} //namespace LOPT
} //namespace SANS
