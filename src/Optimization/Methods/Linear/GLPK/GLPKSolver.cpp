// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "GLPKSolver.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

PYDICT_PARAMETER_OPTION_INSTANTIATE(LOPT::GLPKParam::SolutionOptions)

namespace LOPT
{

//=============================================================================
GLPKException::GLPKException( const int status )
{
  errString += "GLPK Error\n";

  switch (status)
  {
  case GLP_EBADB:
    errString += "GLPK: Unable to start the search, because the initial basis\n"
                  "specified in the problem object is invalid -- the number of\n"
                  "basic (auxialiary and structural) variables is not the same as\n"
                  "the number of rows in the problem object.\n";
    break;
  case GLP_ESING:
    errString += "GLPK: Unable to start the search, because the basis matrix\n"
                  "corresponding to the initial basis is exactly singular.\n";
    break;
  case GLP_EBOUND:
    errString += "GLPK: Unable to start the search, because some double-bounded\n"
                  "(auxiliary or structural) variables have incorrect bounds.\n";
    break;
  case GLP_EFAIL:
    errString += "GLPK: The problem instance has no rows/columns!\n";
    break;
  case GLP_EITLIM:
    errString += "GLPK: Iteration limit exceeded.\n";
    break;
  case GLP_ETMLIM:
    errString += "GLPK: The search was prematurely terminated, because the time\n"
                  "limit has been exceeded.\n";
    break;
  case GLP_ENOCVG:
    errString += "GLPK: Very slow convergence or divergence.\n";
    break;
  case GLP_EINSTAB:
    errString += "GLPK: Numerical instability on solving Newtonian system.\n";
    break;
  case GLP_ENOPFS:
    errString += "GLPK: LP has no primal feasible solution.\n";
    break;
  case GLP_ENODFS:
    errString += "GLPK: LP has no dual feasible solution.\n";
    break;
  }

}

void GLPKParam::checkInputs(PyDict d)
{
  // Note not all inputs from GLPK included
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.SolutionMethod));
  d.checkUnknownInputs(allParams);
}
GLPKParam GLPKParam::params;

void GLPKParam::SimplexParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.msg_lev));
  allParams.push_back(d.checkInputs(params.meth));
  allParams.push_back(d.checkInputs(params.pricing));
  allParams.push_back(d.checkInputs(params.it_lim));
  allParams.push_back(d.checkInputs(params.out_frq));
  allParams.push_back(d.checkInputs(params.presolve));
  d.checkUnknownInputs(allParams);
}
GLPKParam::SimplexParam GLPKParam::SimplexParam::params;

void GLPKParam::InteriorPointParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.msg_lev));
  allParams.push_back(d.checkInputs(params.ord_alg));
  d.checkUnknownInputs(allParams);
}
GLPKParam::InteriorPointParam GLPKParam::InteriorPointParam::params;

} //namespace LOPT
} //namespace SANS
