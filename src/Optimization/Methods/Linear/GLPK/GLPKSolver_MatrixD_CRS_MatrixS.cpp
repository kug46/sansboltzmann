// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define GLPK_INSTANTIATE
#include "GLPKSolver_impl.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include <memory>

using namespace SANS::SLA;

namespace SANS
{
namespace LOPT
{

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::setConstraints( const Matrix_type& A, const Matrix_type& B,
    const Vector_C_type& a, const Vector_C_type& b )
{
  deallocate();

  // MatrixS sizes
  const int MM = A.m();

  lp = glp_create_prob();

  SANS_ASSERT( A.n() == 1 ); // This only works for where MatrixD is VectorD
  SANS_ASSERT( B.n() == 1 ); // This only works for where MatrixD is VectorD
  SANS_ASSERT( A(0,0).m() == a.m() );
  SANS_ASSERT( B(0,0).m() == b.m() );
  SANS_ASSERT( A(0,0).n() == B(0,0).n() );
  for (int mm = 1; mm < MM; mm++)
  {
    SANS_ASSERT( A(mm,0).m() == A(mm-1,0).m() );
    SANS_ASSERT( B(mm,0).m() == B(mm-1,0).m() );
    SANS_ASSERT( A(mm,0).n() == B(mm,  0).n() );
  }

  std::vector<SANS_GLPK_INT> nnz_a(MM);
  std::vector<SANS_GLPK_INT> nnz_b(MM);
  std::vector<SANS_GLPK_INT> nnz_a_cnt(MM);
  std::vector<SANS_GLPK_INT> nnz_b_cnt(MM);
  std::vector<SANS_GLPK_INT> no_col(MM); // no. columns from A and B should be the same

  SANS_GLPK_INT nnz_a_tot = 0;
  SANS_GLPK_INT nnz_b_tot = 0;

  for (int mm = 0; mm < MM; mm++)
  {
    nnz_a[mm] = A(mm,0).getNumNonZero();
    nnz_b[mm] = B(mm,0).getNumNonZero();

    nnz_a_tot += nnz_a[mm];
    nnz_b_tot += nnz_b[mm];

    if (mm == 0)
    {
      nnz_a_cnt[mm] = 0;
      nnz_b_cnt[mm] = 0;

      no_col[mm] = 0;
    }
    else
    {
      nnz_a_cnt[mm] = nnz_a_cnt[mm-1] + nnz_a[mm-1];
      nnz_b_cnt[mm] = nnz_b_cnt[mm-1] + nnz_b[mm-1];

      no_col[mm] = no_col[mm-1] + A(mm-1,0).n();
    }
  }



  // Copy over the index values as they SANS_GLPK_INT may not be an int
  std::vector<int*> row_ptr_a(MM);
  std::vector<int*> row_ptr_b(MM);
  std::vector<int*> col_ind_a(MM);
  std::vector<int*> col_ind_b(MM);
  std::vector< DLA::MatrixS<2, 1, double> *> Rx_a(MM);
  std::vector< DLA::MatrixS<2, 1, double> *> Rx_b(MM);

  for (int mm = 0; mm < MM; mm++)
  {
    row_ptr_a[mm] = A(mm,0).get_row_ptr();
    col_ind_a[mm] = A(mm,0).get_col_ind();
    row_ptr_b[mm] = B(mm,0).get_row_ptr();
    col_ind_b[mm] = B(mm,0).get_col_ind();

    Rx_a[mm] = A(mm,0).get_values();
    Rx_b[mm] = B(mm,0).get_values();
  }

  const int M = A(0,0)(row_ptr_a[0][0],col_ind_a[0][0]).M;
  const int N = A(0,0)(row_ptr_a[0][0],col_ind_a[0][0]).N;

  // initialize rows
  glp_add_rows(lp, (nnz_a_tot + nnz_b_tot) * N * M );

  // convert from CRS to GLPK storage format (which is more memory intensive, i.e. 3x nnz)
  SANS_GLPK_INT *ia = new SANS_GLPK_INT[ (nnz_a_tot + nnz_b_tot) * N * M + 1]; // Note indexed from 1!
  SANS_GLPK_INT *ja = new SANS_GLPK_INT[ (nnz_a_tot + nnz_b_tot) * N * M + 1]; // Note indexed from 1!
  double *ar        = new double[ (nnz_a_tot + nnz_b_tot) * N * M + 1]; // Note indexed from 1!

  // convert A matrix
  ia[0] = 0; // not used
  ja[0] = 0; // not used
  int kk = 0; // counter over rows (for CRS storage)
  int indc = 0; // internal counter
  for (int mm = 0; mm < MM; mm++)
  {
    kk = 0;
    for (SANS_GLPK_INT jj = 0; jj < nnz_a[mm]; jj++) //
    {
      // Loop over the sparse matrix (which contains statically sized matrices)
      for (int n = 0; n < N; n++ )
      {
        for (int m = 0; m < M; m++ )
        {
          indc = jj * N * M + M*n + m + nnz_a_cnt[mm]*M*N;

          ja[indc + 1] = col_ind_a[mm][jj]*N + n + 1 + no_col[mm]*N; // Note indexed from 1!

          if ( jj < row_ptr_a[mm][kk+1] )
          {
            ia[indc + 1] = kk*M + m + 1; // Note indexed from 1!
          }

          ar[indc + 1] = Rx_a[mm][jj](m, n);
        }
      }

      if ( jj == (row_ptr_a[mm][kk+1] - 1) )
        kk++;

    }

  }

  for (int mm = 0; mm < MM; mm++)
  {
    // convert B matrix
    kk = 0; // counter over rows (for CRS storage)
    for (SANS_GLPK_INT jj = 0; jj < nnz_b[mm]; jj++)
    {
      // Loop over the sparse matrix (which contains statically sized matrices)
      for (int n = 0; n < N; n++ )
      {
        for (int m = 0; m < M; m++ )
        {
          indc = jj * N * M + M*n + m + nnz_a_tot * M * N + nnz_b_cnt[mm]*M*N;

          ja[indc + 1] = col_ind_b[mm][jj]*N + n + 1 + no_col[mm]*N; // Note indexed from 1!

          if ( jj < row_ptr_b[mm][kk+1] )
          {
            ia[indc + 1] = kk*M + m + 1 + a.m()*M; // Note indexed from 1!
          }

          ar[indc + 1] = Rx_b[mm][jj](m, n);          // Note indexed from 1!
        }
      }

      if ( jj == (row_ptr_b[mm][kk+1] - 1) )
        kk++;

    }

  }

  // Add constraints - inequality
  int idxa = 0;
  for (SANS_GLPK_INT ii = 0; ii < a.m(); ii++)
  {
    for (SANS_GLPK_INT jj = 0; jj < a[ii].m(); jj++)
    {
      for (int m = 0; m < M; m++)
      {
        glp_set_row_bnds(lp, idxa + 1, GLP_UP, 0.0, a[ii][jj][m] ); // Note indexed from 1!
        idxa++;
      }
    }
  }

  // Add constraints - equality
  int idxb = 0;
  for (SANS_GLPK_INT ii = 0; ii < b.m(); ii++)
  {
    for (SANS_GLPK_INT jj = 0; jj < b[ii].m(); jj++)
    {
      for (int m = 0; m < M; m++)
      {
        glp_set_row_bnds(lp, idxa + idxb + 1, GLP_FX, b[ii][jj][m], b[ii][jj][m]); // Note indexed from 1!
        idxb++;
      }
    }
  }
  // Add constraints on design variables
  // NOTE: we set constraints on each design variable from 0 to infinity.
  glp_add_cols(lp, A(MM-1,0).n() + no_col[MM-1]);
  for (int mm = 0; mm < MM; mm++)
  {
    for ( SANS_GLPK_INT jj = 0; jj < A(mm,0).n(); jj++ )
    {
      glp_set_col_bnds(lp, jj + 1 + no_col[mm], GLP_LO, 0.0, 0.0);
      // should we set name?
    }
  }

  // Load matrix into linear program
  glp_load_matrix(lp, (nnz_a_tot + nnz_b_tot) * M * N, ia, ja, ar);

  // deallocate ia, ja, ar
  delete [] ia;
  delete [] ja;
  delete [] ar;

}

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::setConstraints( const Matrix_type& A,
    const Vector_C_type& a, const bool eq )
{
  deallocate();

  lp = glp_create_prob();

  // MatrixS sizes
  const int MM = A.m();

  SANS_ASSERT( A.n() == 1 ); // This only works for where MatrixD is VectorD
  SANS_ASSERT( A.m()/2 == a.m()    );
  SANS_ASSERT( a.m() > 0 );

  SANS_ASSERT( A(0,0).m() == a[0].m()    );
  for (int mm = 1; mm < MM; mm++)
    SANS_ASSERT( A(mm,0).m() == A(mm-1,0).m() );

  std::vector<SANS_GLPK_INT> nnz(MM);
  std::vector<SANS_GLPK_INT> nnz_cnt(MM);
  std::vector<SANS_GLPK_INT> no_col(MM);

  SANS_GLPK_INT nnz_tot = 0;

  for (int mm = 0; mm < MM; mm++)
  {
    nnz[mm] = A(mm,0).getNumNonZero();

    nnz_tot += nnz[mm];

    if (mm == 0)
    {
      nnz_cnt[mm] = 0;
      no_col[mm] = 0;
    }
    else
    {
      nnz_cnt[mm] = nnz_cnt[mm-1] + nnz[mm-1];
      no_col[mm]  = no_col[mm-1] + A(mm-1,0).n();
    }
  }

  //Copy over the index values as they SANS_GLPK_INT may not be an int
  std::vector<int*> row_ptr(MM);
  std::vector<int*> col_ind(MM);
  std::vector< DLA::MatrixS<2, 1, double> *> Rx(MM);

  for (int mm = 0; mm < MM; mm++)
  {
    row_ptr[mm] = A(mm,0).get_row_ptr();
    col_ind[mm] = A(mm,0).get_col_ind();

    Rx[mm] = A(mm,0).get_values();
  }

  const int M = A(0,0)(row_ptr[0][0], col_ind[0][0]).M;
  const int N = A(0,0)(row_ptr[0][0], col_ind[0][0]).N;

  // initialize rows
  glp_add_rows(lp, nnz_tot * N * M );

  // convert from CRS to GLPK storage format (which is more memory intensive, i.e. 3x nnz)
  SANS_GLPK_INT *ia = new SANS_GLPK_INT[nnz_tot * M * N + 1]; // Note indexed from 1!
  SANS_GLPK_INT *ja = new SANS_GLPK_INT[nnz_tot * M * N + 1]; // Note indexed from 1!
  double *ar        = new double[nnz_tot * M * N + 1]; // Note indexed from 1!

  // convert A matrix
  ia[0]  = 0; // not used
  ja[0]  = 0; // not used
  int kk = 0; // counter over rows (for CRS storage)
  int indc = 0; // internal counter
  for (int mm = 0; mm < MM; mm++)
  {
    kk = 0;
    for (SANS_GLPK_INT jj = 0; jj < nnz[mm]; jj++) //
    {
      // Loop over the sparse matrix (which contains statically sized matrices)
      for (int n = 0; n < N; n++ )
      {
        for (int m = 0; m < M; m++ )
        {
          indc = jj * N * M + M*n + m + nnz_cnt[mm]*M*N;

          ja[indc + 1] = col_ind[mm][jj]*N + n + 1 + no_col[mm]*N; // Note indexed from 1!

          if ( jj < row_ptr[mm][kk+1] )
          {
            ia[indc + 1] = kk*M + m + 1; // Note indexed from 1!
          }

          ar[indc + 1] = Rx[mm][jj](m, n);

        }
      }

      if ( jj == (row_ptr[mm][kk+1] - 1) )
        kk++;

    }

  }

  // Add constraints
  if (eq)
  {
    int idxa = 0;
    for (SANS_GLPK_INT ii = 0; ii < a.m(); ii++)
    {
      for (SANS_GLPK_INT jj = 0; jj < a[ii].m(); jj++)
      {
        for (int m = 0; m < M; m++)
        {
          glp_set_row_bnds(lp, idxa + 1, GLP_FX, a[ii][jj][m], a[ii][jj][m]); // Note indexed from 1!
          idxa++;
        }
      }
    }
  }
  else
  {
    int idxa = 0;
    for (SANS_GLPK_INT ii = 0; ii < a.m(); ii++)
    {
      for (SANS_GLPK_INT jj = 0; jj < a[ii].m(); jj++)
      {
        for (int m = 0; m < M; m++)
        {
          glp_set_row_bnds(lp, idxa + 1, GLP_UP, 0.0, a[ii][jj][m]); // Note indexed from 1!
          idxa++;
        }
      }
    }
  }

  // Add constraints on design variables
  // NOTE: we set constraints on each design variable from 0 to infinity.
  glp_add_cols(lp, A(MM-1,0).n() + no_col[MM-1]);
  for (int mm = 0; mm < MM; mm++)
  {
    for ( SANS_GLPK_INT jj = 0; jj < A(mm,0).n(); jj++ )
    {
      glp_set_col_bnds(lp, jj + 1 + no_col[mm], GLP_LO, 0.0, 0.0);
      // should we set name?
    }
  }

  // Load matrix into linear program
  glp_load_matrix(lp, nnz_tot * M * N, ia, ja, ar);

  // deallocate indices
  delete [] ia;
  delete [] ja;
  delete [] ar;

}

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::solve( const Vector_DV_type& c, Vector_DV_type& x, double &z )
{ // can't be const because we are changing the linear program by solving it.

  const int MM = c.m();
  for (int mm = 0; mm < MM; mm++)
  {
    SANS_ASSERT( c[mm].m() == x[mm].m()  );
  }

  std::vector<SANS_GLPK_INT> no_col(MM);
  no_col[0] = 0;
  for (int mm = 1; mm < MM; mm++)
  {
    no_col[mm] = no_col[mm-1] + x[mm-1].m();
  }

  // set objective vector
  for (int mm = 0; mm < MM; mm++)
  {
    for (SANS_GLPK_INT jj = 0; jj < c[mm].m(); jj++)
    {
      glp_set_obj_coef(lp, jj + 1 + no_col[mm], c[mm][jj]); // Note indexed from 1!
    }
  }

  // solve LP

  SANS_GLPK_INT status = 0;

  DictKeyPair TypeParam = SolverParam_.get(params.SolutionMethod);
   if (TypeParam == params.SolutionMethod.Simplex)
   {
     PyDict simplexparam = TypeParam;

     glp_smcp parm;
     glp_init_smcp(&parm);
     parm.msg_lev  = simplexparam.get(GLPKParam::SimplexParam::params.msg_lev);
     parm.meth     = simplexparam.get(GLPKParam::SimplexParam::params.meth);
     parm.pricing  = simplexparam.get(GLPKParam::SimplexParam::params.pricing);
     parm.it_lim   = simplexparam.get(GLPKParam::SimplexParam::params.it_lim);
     parm.presolve = simplexparam.get(GLPKParam::SimplexParam::params.presolve);

     status = glp_simplex(lp, &parm);
   }
   else if (TypeParam == params.SolutionMethod.InteriorPoint)
   {
     PyDict interiorparam = TypeParam;

     glp_iptcp parm;
     glp_init_iptcp(&parm);
     parm.msg_lev = interiorparam.get(GLPKParam::InteriorPointParam::params.msg_lev);
     parm.ord_alg = interiorparam.get(GLPKParam::InteriorPointParam::params.ord_alg);

     status = glp_interior(lp, &parm);
   }
   else
     SANS_DEVELOPER_EXCEPTION("Unknown solution method");


  if (status != 0)
  {
    BOOST_THROW_EXCEPTION( GLPKException( status ) );
  }

  // recover solution
  if (TypeParam == params.SolutionMethod.Simplex)
  {
    for (int mm = 0; mm < MM; mm++)
    {
      for (SANS_GLPK_INT jj = 0; jj < c[mm].m(); jj++)
      {
        x[mm][jj] = glp_get_col_prim(lp, jj + 1 + no_col[mm]);
      }
    }
    z = glp_get_obj_val(lp);
  }
  else if (TypeParam == params.SolutionMethod.InteriorPoint)
  {
    for (int mm = 0; mm < MM; mm++)
    {
      for (SANS_GLPK_INT jj = 0; jj < c[mm].m(); jj++)
      {
        x[mm][jj] = glp_ipt_col_prim(lp, jj + 1 + no_col[mm]);
      }
    }
    z = glp_ipt_obj_val(lp);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown solution method");
}

template class GLPK< DLA::MatrixD < SparseMatrix_CRS< DLA::MatrixS<2, 1, double> > >, DLA::VectorD < SparseVector< DLA::VectorS<2,double> > > >;

} //namespace LOPT
} //namespace SANS
