// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GLPKSOLVER_DEFINES_H
#define GLPKSOLVER_DEFINES_H

// GLPK interface defines

#include <glpk.h>

namespace SANS
{
namespace LOPT
{

    #define SANS_GLPK_INT              int

} //namespace LOPT
} //namespace SANS

#endif //GLPKSOLVER_DEFINES_H
