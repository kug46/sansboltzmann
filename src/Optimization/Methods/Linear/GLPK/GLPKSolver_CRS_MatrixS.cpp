// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define GLPK_INSTANTIATE
#include "GLPKSolver_impl.h"

#include "LinearAlgebra/SparseLinAlg/SparseVector.h"
#include "LinearAlgebra/SparseLinAlg/SparseMatrix_CRS.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include <memory>

using namespace SANS::SLA;

namespace SANS
{
namespace LOPT
{

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::setConstraints( const Matrix_type& A, const Matrix_type& B,
    const Vector_C_type& a, const Vector_C_type& b )
{
  deallocate();

  // MatrixS sizes
  const int M = A(0,0).M;
  const int N = A(0,0).N;

  lp = glp_create_prob();

  SANS_ASSERT( B.m() == b.m() );
  SANS_ASSERT( A.n() == B.n() );

  SANS_GLPK_INT nnz_a = A.getNumNonZero();
  SANS_GLPK_INT nnz_b = B.getNumNonZero();

  // initialize rows
  glp_add_rows(lp, (nnz_a + nnz_b) * N * M );

  // convert from CRS to GLPK storage format (which is more memory intensive, i.e. 3x nnz)
  SANS_GLPK_INT *ia = new SANS_GLPK_INT[ (nnz_a + nnz_b) * N * M + 1]; // Note indexed from 1!
  SANS_GLPK_INT *ja = new SANS_GLPK_INT[ (nnz_a + nnz_b) * N * M + 1]; // Note indexed from 1!
  double *ar        = new double[ (nnz_a + nnz_b) * N * M + 1]; // Note indexed from 1!

  // Copy over the index values as they SANS_GLPK_INT may not be an int
  int *row_ptr_a = A.get_row_ptr();
  int *col_ind_a = A.get_col_ind();
  int *row_ptr_b = B.get_row_ptr();
  int *col_ind_b = B.get_col_ind();

  DLA::MatrixS<2, 1, double> *Rx_a = A.get_values();
  DLA::MatrixS<2, 1, double> *Rx_b = B.get_values();

  // convert A matrix
  ia[0] = 0; // not used
  ja[0] = 0; // not used
  int kk = 0; // counter over rows (for CRS storage)
  int indc = 0; // internal counter
  for (SANS_GLPK_INT jj = 0; jj < nnz_a; jj++) //
  {
    // Loop over the sparse matrix (which contains statically sized matrices)
    for (int n = 0; n < N; n++ )
    {
      for (int m = 0; m < M; m++ )
      {
        indc = jj * N * M + M*n + m;

        ja[indc + 1] = col_ind_a[jj]*N + n + 1; // Note indexed from 1!

        if ( jj < row_ptr_a[kk+1] )
        {
          ia[indc + 1] = kk*M + m + 1; // Note indexed from 1!
        }

        ar[indc + 1] = Rx_a[jj](m, n);
      }
    }

    if ( jj == (row_ptr_a[kk+1] - 1) )
      kk++;

  }

  // convert B matrix
  kk = 0; // counter over rows (for CRS storage)
  for (SANS_GLPK_INT jj = 0; jj < nnz_b; jj++)
  {
    // Loop over the sparse matrix (which contains statically sized matrices)
    for (int n = 0; n < N; n++ )
    {
      for (int m = 0; m < M; m++ )
      {
        indc = jj * N * M + M*n + m + nnz_a * M * N;

        ja[indc + 1] = col_ind_b[jj]*N + n + 1; // Note indexed from 1!

        if ( jj < row_ptr_b[kk+1] )
        {
          ia[indc + 1] = kk*M + m + 1 + a.m()*M; // Note indexed from 1!
        }

        ar[indc + 1] = Rx_b[jj](m, n);          // Note indexed from 1!
      }
    }

    if ( jj == (row_ptr_b[kk+1] - 1) )
      kk++;

  }

  // Add constraints - inequality
  for (SANS_GLPK_INT jj = 0; jj < a.m(); jj++)
  {
    for (int m = 0; m < M; m++)
      glp_set_row_bnds(lp, jj*M + m + 1, GLP_UP, 0.0, a[jj][m] ); // Note indexed from 1!
  }
  // Add constraints - equality
  for (SANS_GLPK_INT jj = 0; jj < b.m(); jj++)
  {
    for (int m = 0; m < M; m++)
      glp_set_row_bnds(lp, (jj+a.m())*M + m + 1, GLP_FX, b[jj][m], b[jj][m]); // Note indexed from 1!
  }

  // Add constraints on design variables
  // NOTE: we set constraints on each design variable from 0 to infinity.
  glp_add_cols(lp, A.n() );
  for ( SANS_GLPK_INT jj = 0; jj < A.n(); jj++ )
  {
    glp_set_col_bnds(lp, jj + 1, GLP_LO, 0.0, 0.0);
    // should we set name?
  }

  // Load matrix into linear program
  glp_load_matrix(lp, (nnz_a + nnz_b) * M * N, ia, ja, ar);

  // deallocate ia, ja, ar
  delete [] ia;
  delete [] ja;
  delete [] ar;

}

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::setConstraints( const Matrix_type& A,
    const Vector_C_type& a, const bool eq )
{
  deallocate();

  lp = glp_create_prob();

  // MatrixS sizes
  int M = A(0,0).M;
  int N = A(0,0).N;

  SANS_ASSERT( A.m() == a.m() );

  SANS_GLPK_INT nnz = A.getNumNonZero();

  // initialize rows
  glp_add_rows(lp, nnz);

  // convert from CRS to GLPK storage format (which is more memory intensive, i.e. 3x nnz)
  SANS_GLPK_INT *ia = new SANS_GLPK_INT[nnz * M * N + 1]; // Note indexed from 1!
  SANS_GLPK_INT *ja = new SANS_GLPK_INT[nnz * M * N + 1]; // Note indexed from 1!
  double *ar        = new double[nnz * M * N + 1]; // Note indexed from 1!

  //Copy over the index values as they SANS_GLPK_INT may not be an int
  int *row_ptr = A.get_row_ptr();
  int *col_ind = A.get_col_ind();

  DLA::MatrixS<2, 1, double> *Rx = A.get_values();

  // convert A matrix
  ia[0]  = 0; // not used
  ja[0]  = 0; // not used
  int kk = 0; // counter over rows (for CRS storage)
  int indc = 0; // internal counter
  for (SANS_GLPK_INT jj = 0; jj < nnz; jj++) //
  {
    // Loop over the sparse matrix (which contains statically sized matrices)
    for (int n = 0; n < N; n++ )
    {
      for (int m = 0; m < M; m++ )
      {
        indc = jj * N * M + M*n + m;

        ja[indc + 1] = col_ind[jj]*N + n + 1; // Note indexed from 1!

        if ( jj < row_ptr[kk+1] )
          ia[indc + 1] = kk*M + m + 1; // Note indexed from 1!

        ar[indc + 1] = Rx[jj](m, n);
      }
    }

    if ( jj == (row_ptr[kk+1] - 1) )
      kk++;

  }

  // Add constraints
  if (eq)
  {
    for (SANS_GLPK_INT jj = 0; jj < a.m(); jj++)
    {
      for (int m = 0; m < M; m++)
      {
        glp_set_row_bnds(lp, jj*M + m + 1, GLP_FX, a[jj][m], a[jj][m]); // Note indexed from 1!
      }
    }
  }
  else
  {
    for (SANS_GLPK_INT jj = 0; jj < a.m(); jj++)
    {
      for (int m = 0; m < M; m++)
      {
        glp_set_row_bnds(lp, jj*M + m + 1, GLP_UP, 0.0, a[jj][m]); // Note indexed from 1!
      }
    }
  }

  // Add constraints on design variables
  // NOTE: we set constraints on each design variable from 0 to infinity.
  glp_add_cols(lp, A.n()*N );
  for ( SANS_GLPK_INT jj = 0; jj < A.n()*N; jj++ )
  {
    glp_set_col_bnds(lp, jj + 1, GLP_LO, 0.0, 0.0);
  }

  // Load matrix into linear program
  glp_load_matrix(lp, nnz * M * N, ia, ja, ar);

  // deallocate indices
  delete [] ia;
  delete [] ja;
  delete [] ar;
}

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::solve( const Vector_DV_type& c, Vector_DV_type& x, double &z )
{ // can't be const because we are changing the linear program by solving it.

  // these vectors are still "normal", i.e. filled with scalars

  // int N = A(0,0).n();

  SANS_ASSERT( c.m() == x.m()  );

  // set objective vector
  for (SANS_GLPK_INT jj = 0; jj < c.m(); jj++)
  {
    glp_set_obj_coef(lp, jj+1, c[jj]); // Note indexed from 1!
  }

  // solve LP

  SANS_GLPK_INT status = 0;

  DictKeyPair TypeParam = SolverParam_.get(params.SolutionMethod);
   if (TypeParam == params.SolutionMethod.Simplex)
   {
     PyDict simplexparam = TypeParam;

     glp_smcp parm;
     glp_init_smcp(&parm);
     parm.msg_lev  = simplexparam.get(GLPKParam::SimplexParam::params.msg_lev);
     parm.meth     = simplexparam.get(GLPKParam::SimplexParam::params.meth);
     parm.pricing  = simplexparam.get(GLPKParam::SimplexParam::params.pricing);
     parm.it_lim   = simplexparam.get(GLPKParam::SimplexParam::params.it_lim);
     parm.presolve = simplexparam.get(GLPKParam::SimplexParam::params.presolve);

     status = glp_simplex(lp, &parm);
   }
   else if (TypeParam == params.SolutionMethod.InteriorPoint)
   {
     PyDict interiorparam = TypeParam;

     glp_iptcp parm;
     glp_init_iptcp(&parm);
     parm.msg_lev = interiorparam.get(GLPKParam::InteriorPointParam::params.msg_lev);
     parm.ord_alg = interiorparam.get(GLPKParam::InteriorPointParam::params.ord_alg);

     status = glp_interior(lp, &parm);
   }
   else
     SANS_DEVELOPER_EXCEPTION("Unknown solution method");


  if (status != 0)
  {
    BOOST_THROW_EXCEPTION( GLPKException( status ) );
  }

  // recover solution
  if (TypeParam == params.SolutionMethod.Simplex)
  {
    for (SANS_GLPK_INT jj = 0; jj < x.m(); jj++)
    {
      x[jj] = glp_get_col_prim(lp, jj+1);
    }
    z = glp_get_obj_val(lp);
  }
  else if (TypeParam == params.SolutionMethod.InteriorPoint)
  {
    for (SANS_GLPK_INT jj = 0; jj < x.m(); jj++)
    {
      x[jj] = glp_ipt_col_prim(lp, jj+1);
    }
    z = glp_ipt_obj_val(lp);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown solution method");
}

template class GLPK< SparseMatrix_CRS< DLA::MatrixS<2, 1, double> >, SparseVector< DLA::VectorS<2,double> > >;

} //namespace LOPT
} //namespace SANS
