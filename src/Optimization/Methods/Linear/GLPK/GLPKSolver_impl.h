// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(GLPK_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "GLPKSolver.h"

namespace SANS
{
namespace LOPT
{

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
GLPK<Matrix_type, Vector_C_type>::GLPK( const PyDict& d ) :
  params(GLPKParam::params),
  lp(NULL),
  SolverParam_(d)
{
}

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
GLPK<Matrix_type, Vector_C_type>::~GLPK()
{
  deallocate();
}

//-----------------------------------------------------------------------------
template< class Matrix_type, class Vector_C_type >
void GLPK<Matrix_type, Vector_C_type>::deallocate()
{

  if (lp)
  {
    glp_delete_prob(lp);
  }
}


} //namespace LOPT
} //namespace SANS
