// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GLPKSOLVER_H
#define GLPKSOLVER_H

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <glpk.h>

#include "tools/SANSException.h"

#include "Optimization/Methods/Linear/LOptSolverBase.h"

#include <memory>
#include <climits> //INT_MAX

#include "GLPKSolver_defines.h"

namespace SANS
{
namespace LOPT
{

//=============================================================================
struct GLPKException : public SANSException
{
  explicit GLPKException(const int status);
  // UMFPACKException(const int status, const double *info);

  virtual ~GLPKException() throw() {}
};

//=============================================================================
//Forward declare
template< class Matrix_type, class Vector_C_type >
class GLPK;

//=============================================================================
struct GLPKParam : noncopyable
{

  struct SimplexParam : noncopyable
  {
    const ParameterNumeric<int> msg_lev{"msg_lev", GLP_MSG_OFF , 0, NO_LIMIT, "Message level for terminal output"};
    const ParameterNumeric<int> meth{"meth", GLP_PRIMAL, 0, NO_LIMIT, "Simplex method option"};
    const ParameterNumeric<int> pricing{"pricing", GLP_PT_PSE, 0, NO_LIMIT, "Pricing technique"};
    const ParameterNumeric<int> it_lim{"it_lim", INT_MAX, 0, NO_LIMIT, "Simplex iteration limit"};
    const ParameterNumeric<int> out_frq{"out_frq", 500, 0, NO_LIMIT, "Output frequency"};
    const ParameterBool presolve{"presolve", true, "Presolve, yes, no"};

    static void checkInputs(PyDict d);
    static SimplexParam params;
  };

  struct InteriorPointParam : noncopyable
  {

    const ParameterNumeric<int> msg_lev{"msg_lev", GLP_MSG_OFF , 0, NO_LIMIT, "Message level for terminal output"};
    const ParameterNumeric<int> ord_alg{"ord_alg", GLP_ORD_AMD, 0, NO_LIMIT,"Ordering alg. used prior to Cholesky factorization"};

    static void checkInputs(PyDict d);
    static InteriorPointParam params;
  };

  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Name of solution method" };
    const ParameterString& key = Name;

    const DictOption Simplex{"Simplex", SimplexParam::checkInputs};
    const DictOption InteriorPoint{"InteriorPoint"  , InteriorPointParam::checkInputs  };

    const std::vector<DictOption> options{Simplex, InteriorPoint};
  };

  // const ParameterOption<SolutionOptions> SolutionMethod{"SolutionMethod", DictKeyPair( "Simplex" ) , "Solution method for linear program"};
  const ParameterOption<SolutionOptions> SolutionMethod{"SolutionMethod", NO_DEFAULT , "Solution method for linear program"};

  template<class Matrix_type, class Vector_C_type>
  static std::shared_ptr< LOptSolverBase<Matrix_type, Vector_C_type> > newSolver(const PyDict& SolverParam)
  {
    typedef std::shared_ptr< LOptSolverBase<Matrix_type, Vector_C_type> > Solver_ptr;

    return Solver_ptr( new GLPK<Matrix_type, Vector_C_type>( SolverParam ) );
  }

  static void checkInputs(PyDict d);
  static GLPKParam params;
};

//=============================================================================
template< class Matrix_type, class Vector_C_type >
class GLPK : public LOptSolverBase< Matrix_type, Vector_C_type >
{
public:
  typedef LOptSolverBase< Matrix_type, Vector_C_type > Base_type;

  typedef typename Base_type::Vector_DV_type Vector_DV_type;

  GLPKParam& params;

//-----------------------------------------------------------------------------
  // GLPK( );
  explicit GLPK( const PyDict& d );
  virtual ~GLPK();

//-----------------------------------------------------------------------------
  //  A = inequality constraints
  //  B = equality constraints
  //  a = inequality constraint vector
  //  b = equality constraint vector
  virtual void setConstraints( const Matrix_type& A, const Matrix_type& B,
                               const Vector_C_type& a, const Vector_C_type& b ) override;

//-----------------------------------------------------------------------------
  //  A  = constraints
  //  a  = constraint vector
  //  eq = bool (if true: equality constraints, if false: inequality constraints)
  virtual void setConstraints( const Matrix_type& A,
                               const Vector_C_type& a, const bool eq ) override;

//-----------------------------------------------------------------------------

  virtual void solve( const Vector_DV_type& c, Vector_DV_type& x, double& z ) override;

protected:
//-----------------------------------------------------------------------------
  void deallocate();

protected:
  // using Base_type::A_;
  glp_prob        *lp;
  // SANS_GLPK_INT   *ia_, *ja_;
  // double          *ar_;

  const PyDict SolverParam_;
};

} //namespace LOPT
} //namespace SANS

#endif //GLPKSOLVER_H
