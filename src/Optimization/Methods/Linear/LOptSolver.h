// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LOPTSOLVER_H
#define LOPTSOLVER_H

//Python must be included first
#include "Python/PyDict.h"

#include <memory>

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Type.h"
#include "LOptSolverBase.h"
// #include "SparseLinAlg_Inverse.h"

#include "GLPK/GLPKSolver.h"

namespace SANS
{
namespace LOPT
{

//=============================================================================
struct LOptSolverParam : noncopyable
{
  struct LOptSolverOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Solver{"Solver", "GLPK", "Linear Optimization solver"};
    const ParameterString& key = Solver;

    const DictOption GLPK{"GLPK", GLPKParam::checkInputs};
    // const DictOption UMFPACK{"UMFPACK", UMFPACKParam::checkInputs};

    const std::vector<DictOption> options{GLPK}; // {FGMRES, UMFPACK};
  };
  const ParameterOption<LOptSolverOptions> LOptSolver{"LOptSolver", NO_DEFAULT, "Library for linear optimization"};

  static void checkInputs(PyDict d);
  static LOptSolverParam params;
};


//=============================================================================
template<class Matrix_type, class Vector_C_type>
class LOptSolver
{
public:
  typedef std::shared_ptr< LOptSolverBase<Matrix_type, Vector_C_type> > Solver_ptr;

  typedef LOptSolverBase< Matrix_type, Vector_C_type > Base_type;

  typedef typename Base_type::Vector_DV_type Vector_DV_type;

  LOptSolverParam& params = LOptSolverParam::params;

  explicit LOptSolver( const PyDict& d )
  {
    DictKeyPair SolverParam = d.get(params.LOptSolver);

    if ( SolverParam == params.LOptSolver.GLPK )
      solver_ = GLPKParam::newSolver<Matrix_type, Vector_C_type>( SolverParam );
    // else if ( SolverParam == params.LinearSolver.UMFPACK )
    //   solver_ = UMFPACKParam::newSolver<Matrix_type>( SolverParam );
  }

//-----------------------------------------------------------------------------
  void
  setConstraints( const Matrix_type& A, const Matrix_type& B,
      const Vector_C_type& a, const Vector_C_type& b )
  {
        solver_->setConstraints( A, B, a, b );
  }

  void
  setConstraints( const Matrix_type& A, const Vector_C_type& a, const bool eq )
  {
        solver_->setConstraints( A, a, eq );
  }

//-----------------------------------------------------------------------------
  void
  solve(const Vector_DV_type& c, Vector_DV_type& x, double& z )
  {
    solver_->solve( c, x, z );
  };

private:
  Solver_ptr solver_;
};


} //namespace LOPT
} //namespace SANS

#endif //LOPTSOLVER_H
