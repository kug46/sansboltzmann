// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LENGTHFRAME_TRUSS_H
#define LENGTHFRAME_TRUSS_H

// integrand operator for truss design problem

#include <ostream>
#include <vector>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "Field/Element/Element.h"

#include "BasisFunction/TraceToCellRefCoord.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Edge integrand: Truss
//
// Computing equilibrium of forces

template <class PhysDim_>
class LengthFrame_Truss
{
public:
  typedef PhysDim_ PhysDim;
  typedef typename ElementXField<PhysDim, TopoD1, Line>::VectorX VectorX;

  template <class Z>
  using ArrayQ = Z;

  LengthFrame_Truss( const std::vector<int>& InteriorTraceGroups,
    const std::vector<int>& BoundaryTraceGroups ) :
    interiorTraceGroups_(InteriorTraceGroups),
    BoundaryTraceGroups_(BoundaryTraceGroups) { }

  std::size_t nInteriorTraceGroups() const { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_[n]; }

  std::size_t nBoundaryTraceGroups() const { return BoundaryTraceGroups_.size(); }
  std::size_t boundaryTraceGroup(const int n) const { return BoundaryTraceGroups_[n]; }

  template <class T>
  void length(const ElementXField<PhysDim, TopoD1, Line>& xfldElemTrace,
            std::vector<ArrayQ<T>>& rsdElemTrace) const;

protected:
  const std::vector<int> interiorTraceGroups_;
  const std::vector<int> BoundaryTraceGroups_;

};

template <class PhysDim> // template for class
template <class T> // template for function
void LengthFrame_Truss<PhysDim>::length(const ElementXField<PhysDim, TopoD1, Line>& xfldElemTrace,
          std::vector<ArrayQ<T>>& rsdElemTrace) const
{

  SANS_ASSERT( rsdElemTrace.size() == 1 ); // 1 trace

  SANS_ASSERT( xfldElemTrace.nDOF() == 2 );

  // get coordinates for node 1 and 2
  VectorX x0 = xfldElemTrace.DOF(0);
  VectorX x1 = xfldElemTrace.DOF(1);

  Real len = sqrt( pow( x1[0] - x0[0], 2 ) + pow( x1[1] - x0[1], 2 ) ); // length of edge

  // first node
  rsdElemTrace[0] = len;

}


}

#endif  // LengthFrame_TRUSS_H
