// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LENGTHOBJECTIVE_TRUSS_H
#define LENGTHOBJECTIVE_TRUSS_H

// Jacobian for truss optimization

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/tools/GroupFunctorType.h"

// #include "Discretization/GroupIntegral_Type.h"

#include "tools/Tuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Obtain lengths for objective
//
template<class LengthTrace, template<class> class Vector >
class LengthObjective_Truss_impl :
    public GroupFunctorInteriorTraceType< LengthObjective_Truss_impl<LengthTrace, Vector> >
{
public:
  typedef typename LengthTrace::PhysDim PhysDim;
  // typedef typename LengthTrace::template ArrayEqn<Real> ArrayEqn;
  typedef typename LengthTrace::template ArrayQ<Real> ArrayQ;
  // typedef typename LengthTrace::template MatrixQ<Real> MatrixQ;

  // typedef typename LengthTrace::template ArrayQ<Surreal> ArrayQSurreal;
  // typedef typename LengthTrace::template ArrayEqn<Surreal> ArrayEqnSurreal;
  // typedef DLA::VectorS< PhysDim::D, ArrayQSurreal > GradArrayQSurreal;

  // typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  LengthObjective_Truss_impl( const LengthTrace& fcn,
                                  Vector<ArrayQ>& lGlobal) :
    fcn_(fcn), lGlobal_(lGlobal)
  {}

  std::size_t nInteriorTraceGroups() const { return fcn_.nInteriorTraceGroups(); }
  std::size_t interiorTraceGroup(const int n) const { return fcn_.interiorTraceGroup(n); }

//----------------------------------------------------------------------------//
  #if 0
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&     qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld = get<1>(flds);

    SANS_ASSERT( mtxCon_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxCon_.n() == qIfld.nDOF() );
  }
  #endif
//----------------------------------------------------------------------------//
  // Function that computes the length of each trace element
  template <class TopologyTrace>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename TopologyTrace::CellTopoDim>,
                                    Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>, TupleClass<>>::
               template FieldTraceGroupType<Line>& fldsTrace,
        const int traceGroupGlobal )
  {

    //Trace types
    typedef typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<Line> XFieldTraceGroupType;
    typedef typename Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>::template FieldTraceGroupType<Line> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<> ElementQFieldTraceClass;
    // typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldSurrealTraceClass;

    // typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const XFieldTraceGroupType& xfldTrace  = get<0>(fldsTrace);
    const QFieldTraceGroupType& qIfldTrace = get<1>(fldsTrace);

    // element field variables

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFI = qIfldElemTrace.nDOF();
    // const int nDOFX = xfldElemTrace.nDOF();

    // variables/equations per DOF
    // const int nEqn = PhysDim::D; // Number of equations per node
    // const int M = DLA::VectorSize<ArrayQ>::M;
    // const int nVar = 1; // This is basically the number of unknowns on each edge (1 in this case)

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qI(nDOFI);

    // int mapDOFGlobal_x[nDOFX];
    // for (int k = 0; k < nDOFX; k++)   { mapDOFGlobal_x[k] = -1; }

    // element integrand/residual
    std::vector<ArrayQ> rsdElemTrace( nDOFI );

    // element jacobian matrices

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians

      // mtxElemInt_qI = 0;

      // copy global grid/solution DOFs to element

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // trace integration
      fcn_.length( xfldElemTrace, rsdElemTrace );

      // scatter-add element jacobian to global
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI.data(), nDOFI );

      // mtxCon_.scatterAdd( mtxElemInt_qI, mapDOFGlobal_x, nDOFX , mapDOFGlobal_qI, nDOFI );

      int nGlobal;
      for (int n = 0; n < nDOFI; n++)
      {
        nGlobal = mapDOFGlobal_qI[n];
        lGlobal_[nGlobal] = rsdElemTrace[0];
      }


    }
  }

protected:
  const LengthTrace& fcn_;
  Vector<ArrayQ>& lGlobal_;
};

// Factory function

template<class LengthTrace, template<class> class Vector, class ArrayQ >
LengthObjective_Truss_impl<LengthTrace, Vector>
LengthObjective_Truss( const LengthTrace& fcn,
                          Vector<ArrayQ>& mtxCon)
{
  return LengthObjective_Truss_impl<LengthTrace, Vector>(
           fcn, mtxCon);
}


}


#endif  // LENGTHOBJECTIVE_TRUSS_H
