// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef JACOBIANCONSTRAINT_BOUNDARY_H
#define JACOBIANCONSTRAINT_BOUNDARY_H

// Jacobian for truss optimization

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/XField.h"
#include "Field/Field.h"
#include "Field/Tuple/FieldTuple.h"
#include "Field/Element/GalerkinWeightedIntegral.h"
#include "Field/tools/GroupFunctorType.h"

// #include "Discretization/GroupIntegral_Type.h"

#include "tools/Tuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Jacobian for constraint matrix
//
template<class Surreal, class IntegrandTrace >
class JacobianConstraint_Truss_Boundary_impl :
    public GroupFunctorBoundaryTraceType< JacobianConstraint_Truss_Boundary_impl<Surreal, IntegrandTrace> >
{
public:
  typedef typename IntegrandTrace::PhysDim PhysDim;
  typedef typename IntegrandTrace::template ArrayEqn<Real> ArrayEqn;
  typedef typename IntegrandTrace::template ArrayQ<Real> ArrayQ;
  typedef typename IntegrandTrace::template MatrixQ<Real> MatrixQ;

  typedef typename IntegrandTrace::template ArrayQ<Surreal> ArrayQSurreal;
  typedef typename IntegrandTrace::template ArrayEqn<Surreal> ArrayEqnSurreal;
  typedef DLA::VectorS< PhysDim::D, ArrayQSurreal > GradArrayQSurreal;

  typedef DLA::MatrixD<MatrixQ> MatrixElemClass;

  // Save off the boundary trace integrand and the residual vectors
  JacobianConstraint_Truss_Boundary_impl( const IntegrandTrace& fcn,
                                          MatrixScatterAdd<MatrixQ>& mtxCon) :
    fcn_(fcn), mtxCon_(mtxCon)
  {}

  std::size_t nBoundaryTraceGroups() const { return fcn_.nBoundaryTraceGroups(); }
  std::size_t boundaryTraceGroup(const int n) const { return fcn_.boundaryTraceGroup(n); }

//----------------------------------------------------------------------------//
  #if 0
  // A function for checking the correct size of the residual vectors
  template <class TopoDim>
  void check( const FieldTuple<Field<PhysDim, TopoDim, ArrayQ>, Field<PhysDim, TopoDim, VectorArrayQ>, TupleClass<>>& flds,
              const Field<PhysDim, TopoDim, ArrayQ>& qIfld ) const
  {
    const Field<PhysDim, TopoDim, ArrayQ>&     qfld = get<0>(flds);
    const Field<PhysDim, TopoDim, VectorArrayQ>& afld = get<1>(flds);

    SANS_ASSERT( mtxCon_.m() == qIfld.nDOF() );
    SANS_ASSERT( mtxCon_.n() == qIfld.nDOF() );
  }
  #endif
//----------------------------------------------------------------------------//
  // Integration function that integrates each element in the trace group
  template <class TopologyTrace>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename TopologyTrace::CellTopoDim>,
                                    Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>, TupleClass<>>::
               template FieldTraceGroupType<Line>& fldsTrace,
        const int traceGroupGlobal )
  {

    //Trace types
    typedef typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<Line> XFieldTraceGroupType;
    typedef typename Field<PhysDim, typename TopologyTrace::CellTopoDim, ArrayQ>::template FieldTraceGroupType<Line> QFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;
    typedef typename QFieldTraceGroupType::template ElementType<Surreal> ElementQFieldSurrealTraceClass;

    // typedef typename ElementXFieldTraceClass::TopoDim TopoDimTrace;

    const XFieldTraceGroupType& xfldTrace  = get<0>(fldsTrace);
    const QFieldTraceGroupType& qIfldTrace = get<1>(fldsTrace);

    // element field variables

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );
    ElementQFieldSurrealTraceClass qIfldElemTrace( qIfldTrace.basis() );

    // DOFs per element
    const int nDOFI = qIfldElemTrace.nDOF();
    const int nDOFX = xfldElemTrace.nDOF();

    // variables/equations per DOF
    const int nEqn = PhysDim::D; // Number of equations per node
    const int M = DLA::VectorSize<ArrayQ>::M;
    // const int nVar = 1; // This is basically the number of unknowns on each edge (1 in this case)

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobal_qI(nDOFI);
    std::vector<int> mapDOFGlobal_x(nDOFX);

    // element integrand/residual
    std::vector<ArrayEqnSurreal> rsdElemTrace( nDOFX );

    // element jacobian matrices

    //trace

    MatrixElemClass mtxElemInt_qI(nDOFX, nDOFI);

    // number of simultaneous derivatives per functor call
    const int nDeriv = Surreal::N;

    // loop over elements within group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      // zero element Jacobians

      mtxElemInt_qI = 0;

      // copy global grid/solution DOFs to element

      xfldTrace.getElement( xfldElemTrace, elem );
      qIfldTrace.getElement( qIfldElemTrace, elem );

      // loop over derivative chunks
      // for (int nchunk = 0; nchunk < nEqn*nDOFI; nchunk += nDeriv)
      for (int nchunk = 0; nchunk < nDeriv; nchunk += nDeriv)
      {

        // associate derivative slots
        int slot = 0;

        // wrt qI
        for (int j = 0; j < nDOFI; j++)   // loop over number of unknowns
        {
          for (int n = 0; n < M; n++)  // loop over number of equations per trace element
          {
            // start by setting all partial derivatives to zero
            for (int k = 0; k < nDeriv; k++)
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(k) = 0;

            // then set one per row equal to 1 (this is the one we'll take the partial derivative with respect to)
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qIfldElemTrace.DOF(j),n).deriv(slot - nchunk) = 1;
          }
        }

        for (int n = 0; n < nDOFI; n++) rsdElemTrace[n] = 0;

        // trace integration
        fcn_.constraint( xfldElemTrace, qIfldElemTrace, rsdElemTrace );

        // accumulate derivatives into element jacobians

        // wrt uI
        for (int j = 0; j < nDOFI; j++)
        {
          for (int n = 0; n < M; n++)
          {
            slot = nEqn*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              for (int i = 0; i < nDOFX; i++)
                for (int m = 0; m < nEqn; m++)
                  DLA::index(mtxElemInt_qI(i,j),m,n) = DLA::index(rsdElemTrace[i],m).deriv(slot - nchunk);
            }
          }
        }

      }   // nchunk

      // scatter-add element jacobian to global
      xfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_x.data(), nDOFX );
      qIfldTrace.associativity( elem ).getGlobalMapping( mapDOFGlobal_qI.data(), nDOFI );

      mtxCon_.scatterAdd( mtxElemInt_qI, mapDOFGlobal_x.data(), nDOFX , mapDOFGlobal_qI.data(), nDOFI );


    }
  }

protected:
  const IntegrandTrace& fcn_;
  MatrixScatterAdd<MatrixQ>& mtxCon_;
};

// Factory function

template<class Surreal, class IntegrandTrace, class MatrixQ >
JacobianConstraint_Truss_Boundary_impl<Surreal, IntegrandTrace>
JacobianConstraint_Truss_Boundary( const IntegrandTrace& fcn,
                                   MatrixScatterAdd<MatrixQ>& mtxCon)
{
  return { fcn, mtxCon };
}


}


#endif  // JACOBIANCONSTRAINT_BOUNDARY_H
