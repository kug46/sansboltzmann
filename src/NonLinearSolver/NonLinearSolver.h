// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NONLINEARSOLVER_H
#define NONLINEARSOLVER_H

//Python must be included first
#include "Python/PyDict.h"

#include <memory>

#include "NonLinearSolverBase.h"

#include "NewtonSolver.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"

namespace SANS
{

//=============================================================================
struct NonLinearSolverParam : noncopyable
{
  struct SolverOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Solver{"Solver", "Newton", "Non-Linear Solver Name" };
    const ParameterString& key = Solver;

    const DictOption Newton{"Newton", NewtonSolverParam::checkInputs};

    const std::vector<DictOption> options{Newton};
  };
  const ParameterOption<SolverOptions> NonLinearSolver{"NonLinearSolver", NO_DEFAULT, "Non-Linear Solver"};

  static void checkInputs(PyDict d);
  static NonLinearSolverParam params;
};


//=============================================================================
template<class SystemMatrix>
class NonLinearSolver : public NonLinearSolverBase<SystemMatrix>
{
public:
  typedef std::shared_ptr< NonLinearSolverBase<SystemMatrix> > Solver_ptr;

  typedef typename VectorType<SystemMatrix>::type SystemVector;
  typedef AlgebraicEquationSetBase<SystemMatrix> EquationSetBase;

  const NonLinearSolverParam& params = NonLinearSolverParam::params;

  explicit NonLinearSolver( EquationSetBase& f, const PyDict& d )
  {
    DictKeyPair SolverParam = d.get(params.NonLinearSolver);

    if ( SolverParam == params.NonLinearSolver.Newton )
    {
      solver_ = Solver_ptr( new NewtonSolver<SystemMatrix>( f, SolverParam ) );
      return;
    }

    // Should not be able to get here unless checkInputs was not called
    SANS_DEVELOPER_EXCEPTION("Unknown non-linear solver: %s", SolverParam.key().c_str());
  }

  virtual ~NonLinearSolver() {}

  //-----------------------------------------------------------------------------
  virtual SolveStatus solve( const SystemVector& qold, SystemVector& q ) override
  {
    return solver_->solve( qold, q );
  }

  //-----------------------------------------------------------------------------
  virtual std::vector<Real>& getLineSearchHistory() override
  {
    return solver_->getLineSearchHistory();
  }

private:
  Solver_ptr solver_;
};


} //namespace SANS

#endif //SPARSELINALG_LINEARSOLVER_H
