// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NEWTONSOLVER_H
#define NEWTONSOLVER_H

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/VectorType.h"
#include "LinearAlgebra/VectorSizeType.h"
#include "LinearAlgebra/MatrixSizeType.h"

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_LinearSolver.h"

#include "NonLinearSolverBase.h"
#include "LineUpdate/LineUpdate.h"

namespace SANS
{

//=============================================================================
struct NewtonSolverParam : noncopyable
{
  const ParameterOption<LineUpdateParam::LineUpdateOptions>& LineUpdate;
  const ParameterOption<SLA::LinearSolverParam::LinearSolverOptions>& LinearSolver;

  const ParameterNumeric<int > MinIterations{"MinIterations", 0  , 0, NO_LIMIT, "Minimum Number of Newton Iterations"};
  const ParameterNumeric<int > MaxIterations{"MaxIterations", 100, 0, NO_LIMIT, "Maximum Number of Newton Iterations"};

  const ParameterNumeric<Real>
    MaxChangeFraction{"MaxChangeFraction", 0, 0, NO_LIMIT, "Maximum allowed change fraction in PDE quantities, e.g. density for fluids"};

  const ParameterBool Verbose{"Verbose", false, "Verbose Newton Solve"};
  const ParameterBool Timing{"Timing", false, "Time Components of the Newton Solve"};
  const ParameterBool DumpJacobian{"DumpJacobian", false, "Dump out Jacobian matrices at each Newton iteration"};
  const ParameterBool TestMachinePrecision{"TestMachinePrecision", false, "Test for convergence due to machine precision effects"};
  const ParameterFileName ResidualHistoryFile{"ResidualHistoryFile", std::ios_base::out, "", "Dumps spatial residual history to given file name"};

  NewtonSolverParam();

  static void checkInputs(PyDict d);
  static NewtonSolverParam params;
};


//----------------------------------------------------------------------------//
// NewtonSolver solves a system of non-linear equations using Newton-Raphson method with a line search
//
//
// template parameters:
//
template<class SystemMatrix>
class NewtonSolver : public NonLinearSolverBase<SystemMatrix>
{
public:
  NewtonSolverParam& params;

  typedef typename VectorType<SystemMatrix>::type SystemVector;
  typedef typename NonZeroPatternType<SystemMatrix>::type SystemNonZeroPattern;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  typedef AlgebraicEquationSetBase<SystemMatrix> EquationSetBase;

  NewtonSolver( EquationSetBase& f, const PyDict& d );

  virtual ~NewtonSolver() {}

  virtual SolveStatus solve( const SystemVector& qold, SystemVector& q ) override;

  virtual std::vector<Real>& getLineSearchHistory() override;

protected:

  //Writing out residual vs line search s
  void writeLineSearch(const SystemVector& q0, const SystemVector& dq, const int iter);

  EquationSetBase& f_;                               //The multivariate non-linear function that is solved
  SLA::LinearSolver< SystemMatrix > linearSolver_;   //The sparse matrix linear solver
  LineUpdate< SystemMatrix > LineUpdate_;            //Applies the update vector from the linear solve
  int MinIterations_;                                //Minimum number of Newton iterations
  int MaxIterations_;                                //Maximum number of Newton iterations
  Real maxChangeFraction_;                           //Fraction of allowed change in PDE quantities
  bool verbose_;
  bool dumpJacobian_;
  bool timing_;
  bool testMachinePrecision_;                        // Do we want to test for machine precision issues near convergence
  std::ofstream fhist_;                              // residual history file
  std::vector<Real> lineSearchHistory_;              // line search step size history for that solve
};

}

#endif //NEWTONSOLVER_H
