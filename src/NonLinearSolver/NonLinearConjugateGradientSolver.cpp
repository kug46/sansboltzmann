// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "NonLinearConjugateGradientSolver.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void NonLinearConjugateGradientSolverParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.LinearSolver));
  allParams.push_back(d.checkInputs(params.MaxIterations));
  allParams.push_back(d.checkInputs(params.ResidualTolerance));
  allParams.push_back(d.checkInputs(params.UpdateTolerance));
  d.checkUnknownInputs(allParams);
}
NonLinearConjugateGradientSolverParam NonLinearConjugateGradientSolverParam::params;

}
