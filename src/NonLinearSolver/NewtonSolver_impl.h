// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(NEWTONSOLVER_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

//#define NEWTONSOLVER_DEBUG_DUMPVECTOR // turned on just for debugging

#include "NewtonSolver.h"

#include "LinearAlgebra/SparseLinAlg/WriteMatrixMarketFile.h"

#ifdef NEWTONSOLVER_DEBUG_DUMPVECTOR
#define SCALARVECTOR_INSTANTIATE
#include "LinearAlgebra/SparseLinAlg/ScalarVector_impl.h"

#include "LinearAlgebra/SparseLinAlg/WritePlainVector.h"
#endif

#include "tools/output_std_vector.h"
#include "tools/timer.h"

#include <vector>

// This is ok because this implementation file is only ever indluced in a cpp file
#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

template<class SystemMatrix>
NewtonSolver<SystemMatrix>::
NewtonSolver( EquationSetBase& f, const PyDict& d ) :
  params(NewtonSolverParam::params),
  f_(f),
  linearSolver_(d, f),
  LineUpdate_(d, f),
  MinIterations_(d.get(params.MinIterations)),
  MaxIterations_(d.get(params.MaxIterations)),
  maxChangeFraction_(d.get(params.MaxChangeFraction)),
  verbose_(d.get(params.Verbose)),
  dumpJacobian_(d.get(params.DumpJacobian)),
  timing_(d.get(params.Timing)),
  testMachinePrecision_(d.get(params.TestMachinePrecision))
{
  if ( MaxIterations_ < MinIterations_ )
  {
    MaxIterations_ = MinIterations_;
  }

  const std::string residualfilename = d.get(params.ResidualHistoryFile);
  if (!residualfilename.empty() && f_.comm()->rank() == 0)
  {
    std::cout << "...set to write Newton solver residual history to file:" << residualfilename << std::endl;
    fhist_.open(residualfilename, std::ios_base::out);
  }
}


template<class SystemMatrix>
SolveStatus
NewtonSolver<SystemMatrix>::solve( const SystemVector& qold, SystemVector& q )
{
  SystemVector rsd(f_.vectorEqSize());
  SystemVector dq(f_.vectorStateSize());
  SystemVector q_prev(f_.vectorStateSize());

  // Default initialization
  int comm_rank = f_.comm()->rank();

  if (fhist_.is_open() && comm_rank == 0)
    fhist_ << "ZONE" << std::endl;

  // Clear out the contents of the old lineSearch history and fill with ones
  lineSearchHistory_.assign(MaxIterations_, 1.0);

  q = qold;

  // sync ghost/zombie DOFs just to be safe
  f_.syncDOFs_MPI();

  // Newton iterations
  SolveStatus status;
  for (int iter = 0; iter <= MaxIterations_; ++iter)
  {
    // evaluate residual
    rsd = 0.0; // resetting residual value to zero is required!
    const timer rsdtime;
    f_.residual(q, rsd);
    if (timing_ && comm_rank == 0) std::cout << "Residual time : " << rsdtime.elapsed() << " s" << std::endl;

    const std::vector<std::vector<Real>> nrmRsd = f_.residualNorm(rsd);

    if (comm_rank == 0)
    {
      // print residual to screen if requested
      if (verbose_)
        std::cout << "Newton iteration: " << iter << ", Residual Norm: " << std::setprecision(5) << std::scientific << nrmRsd << std::endl;

      // dump residual history to file if requested
      if (fhist_.is_open())
        fhist_ << std::setw(6) << iter << ", " << std::setprecision(16) << std::scientific << nrmRsd << std::endl;
    }

    // check convergence
    if (f_.convergedResidual(nrmRsd) && iter >= MinIterations_)
    {
      status.converged = true;
      return status;
    }

    // exit loop if the max iteration is reached
    if (iter == MaxIterations_) break;

    // linear solution for solution update direction dq
    dq = 0.0; // set initial guess
    const timer solvetime; // timing here includes the Jacobian evaluation

    status.linearSolveStatus = linearSolver_.solve(rsd, dq); // note that dq here is defined by jac * dq = rsd (instead of jac * dq + rsd = 0)
    
    if (timing_ && comm_rank == 0) std::cout << "Linear Solve time : " << solvetime.elapsed() << " s" << std::endl;

    // Hacky machine precision check...
    if (testMachinePrecision_)
    {
      if (f_.atMachinePrecision(q, nrmRsd))
      {
//        std::cout << "Detected machine precision issues" << std::endl;
        status.atMachinePrecision = true;
      }
    }

    // dump jacobian to file
    if (dumpJacobian_ && comm_rank == 0)
    {
      std::string filename = "tmp/jac_iter" + std::to_string(iter) + ".mtx";
      linearSolver_.dumpJacobian(filename);
    }

#if defined(NEWTONSOLVER_DEBUG_DUMPVECTOR)
    // TODO: This printout is intended for debugging, and can be generalized to be a function
    { // dump formatted vector
      std::cout << "...Writing Newton system vectors to files (including residuals, solution, and update) ..." << std::endl;

      //dump vectors: require overloaded insertion operator "<<" for printing out vectors
      std::string res_filename = "tmp/rsd_iter" + std::to_string(iter) + ".dat";
      std::fstream fout_res( res_filename, std::fstream::out );
      fout_res << rsd << std::endl;

      std::string sol_filename = "tmp/sln_iter" + std::to_string(iter) + ".dat";
      std::fstream fout_sol( sol_filename, std::fstream::out );
      fout_sol << q << std::endl;

      std::string dq_filename = "tmp/dq_iter" + std::to_string(iter) + ".dat";
      std::fstream fout_dq( dq_filename, std::fstream::out );
      fout_dq << dq << std::endl;
    }

//    { // dump plain scalar vector
//      std::string rsd_filename = "tmp/rsd_plain_iter" + std::to_string(iter) + ".dat";
//      WritePlainVector( rsd, rsd_filename );
//
//      std::string sln_filename = "tmp/sln_plain_iter" + std::to_string(iter) + ".dat";
//      WritePlainVector( q, sln_filename );
//
//      std::string dq_filename = "tmp/dq_plain_iter" + std::to_string(iter) + ".dat";
//      WritePlainVector( dq, dq_filename );
//
//      SystemVector b(f_.vectorEqSize());
//      b = linearSolver_.A() * dq;
//      std::string b_filename = "tmp/b_plain_iter" + std::to_string(iter) + ".dat";
//      WritePlainVector( b, b_filename );
//    }
#endif

    // ----- compute maximum allowed change fraction ----- //
    Real step_limit = 1.0;
    if ( maxChangeFraction_ > 0.0 )
    {
      step_limit = f_.updateFraction(q, dq, maxChangeFraction_);

      if (step_limit < 1.0)
      {
        status.linesearch.status = LineSearchStatus::LimitedStep;
        if (verbose_)
          std::cout << "Newton step is limited: underrelaxation factor = " << step_limit << std::endl;
      }
    }

    // ----- line search ----- //
    Real step_length = step_limit; // set the initial line-search step length to the maximum allowed change fraction
    q_prev = q;
    status.linesearch = LineUpdate_(rsd, nrmRsd, q_prev, dq, iter, q, step_length);
    if (status.linesearch.status == LineSearchStatus::Failure)
    {
      // Line search failed...
      lineSearchHistory_.at(iter) = step_length;
      return status;
    }

    // the line search didn't manage the maximum allowed change fraction
    if (step_length < step_limit)
    {
      status.linesearch.status = LineSearchStatus::PartialStep;
    }

    // sync ghost/zombie DOFs
    f_.syncDOFs_MPI();
  } // Newton iteration ends

  return status;
}


template<class SystemMatrix>
std::vector<Real>&
NewtonSolver<SystemMatrix>::getLineSearchHistory()
{
  return lineSearchHistory_;
}

} //namespace SANS
