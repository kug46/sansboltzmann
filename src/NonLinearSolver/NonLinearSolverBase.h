// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NONLINEARSOLVERBASE_H
#define NONLINEARSOLVERBASE_H

#include "LinearAlgebra/VectorType.h"
#include "LinearAlgebra/SparseLinAlg/LinearSolverBase.h"

#include "LineUpdate/LineSearchStatus.h"

#include <vector>

namespace SANS
{

// Solve status returned
struct SolveStatus
{
  SolveStatus() :
    converged(false),
    atMachinePrecision(false),
    linesearch()
  {
    // Nothing
  }

  bool converged;
  bool atMachinePrecision;
  LineSearchStatus linesearch;
  SLA::LinearSolveStatus linearSolveStatus;
};

//----------------------------------------------------------------------------//
// NonLinearSolverBase is the base class for objects that solve a system of non-linear equations
//
//
// template parameters:
//
template<class SystemMatrix>
class NonLinearSolverBase
{
public:

  typedef typename VectorType<SystemMatrix>::type SystemVector;
  typedef typename VectorType<SystemMatrix>::Viewtype SystemVectorView;

  virtual ~NonLinearSolverBase() {}

  virtual SolveStatus solve( const SystemVector& qold, SystemVector& q ) = 0;
  virtual std::vector<Real>& getLineSearchHistory() = 0;

protected:
};
}

#endif //NONLINEARSOLVERBASE_H
