// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NONLINEARCONJUGATEGRADIENTSOLVER_H
#define NONLINEARCONJUGATEGRADIENTSOLVER_H

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"
#include "LinearAlgebra/VectorType.h"
#include "LinearAlgebra/VectorSizeType.h"
#include "LinearAlgebra/MatrixSizeType.h"

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_LinearSolver.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD.h"
#include "LinearAlgebra/DenseLinAlg/tools/norm.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"
#include "NonLinearSolver/NonLinearSolverBase.h"
#include "tools/stringify.h"

#include <initializer_list>
#include <iostream>
#include <fstream>

namespace SANS
{

//=============================================================================
struct NonLinearConjugateGradientSolverParam : noncopyable
{
  const ParameterDict LinearSolver{"LinearSolver", NO_DEFAULT, SLA::LinearSolverParam::checkInputs, "The Linear Solver Options for Non-Linear Solve"};

  const ParameterNumeric<int > MaxIterations    {"MaxIterations"    , 400  , 0, NO_LIMIT, "Maximum Number of Iterations"};
  const ParameterNumeric<Real> ResidualTolerance{"ResidualTolerance", 1e-12, 0, NO_LIMIT, "Convergence Tolerance on the Residual Norm"};
  const ParameterNumeric<Real> UpdateTolerance  {"UpdateTolerance"  , 0    , 0, NO_LIMIT, "Convergence Tolerance on the Update Vector"};

  static void checkInputs(PyDict d);
  static NonLinearConjugateGradientSolverParam params;
};


//----------------------------------------------------------------------------//
// NonLinearConjugateGradientSolver solves a system of non-linear equations by
// minimising the L2 norm of the residual using the non-linear conjugate gradient
// method with Polak–Ribiere conjugation with automatic reset
//
// template parameters:
//
template<class SystemMatrix>
class NonLinearConjugateGradientSolver : public NonLinearSolverBase<SystemMatrix>
{
public:
  NonLinearConjugateGradientSolverParam& params;

  typedef typename VectorType<SystemMatrix>::type SystemVector;
  typedef typename NonZeroPatternType<SystemMatrix>::type SystemNonZeroPattern;

  typedef typename MatrixSizeType<SystemMatrix>::type MatrixSizeClass;
  typedef typename VectorSizeType<SystemVector>::type VectorSizeClass;

  typedef AlgebraicEquationSetBase<SystemMatrix> EquationSetBase;

  //The constructor for the non-zero pattern likely needs work
  template<class U>
  NonLinearConjugateGradientSolver( EquationSetBase& f, const PyDict& d, const std::initializer_list<U>& nz ) :
    params(NonLinearConjugateGradientSolverParam::params),
    f_(f), nz_(nz),
    linearSolver_(d.get(params.LinearSolver)),
    MaxIterations_(d.get(params.MaxIterations)),
    ResidualTolerance_(d.get(params.ResidualTolerance)),
    UpdateTolerance_(d.get(params.UpdateTolerance))
  {}

  //The constructor for the non-zero pattern likely needs work
  NonLinearConjugateGradientSolver( EquationSetBase& f, const PyDict& d,SystemNonZeroPattern& nz ) :
    params(NonLinearConjugateGradientSolverParam::params),
    f_(f), nz_(nz),
    linearSolver_(d.get(params.LinearSolver)),
    MaxIterations_(d.get(params.MaxIterations)),
    ResidualTolerance_(d.get(params.ResidualTolerance)),
    UpdateTolerance_(d.get(params.UpdateTolerance))
  {}

  bool solve( const SystemVector& Xold, SystemVector& X );

protected:
  EquationSetBase& f_;                             //The multivariate non-linear function that is solved
  SystemNonZeroPattern nz_;                        //The non-zero pattern of the sparse matrix
  SLA::LinearSolver< SystemMatrix > linearSolver_; //The sparse matrix linear solver
  int MaxIterations_;                              //Maximum number of Newton iterations
  Real ResidualTolerance_;                         //Convergence tolerance on the residual vector
  Real UpdateTolerance_;                           //Convergence tolerance on update vector
};


template<class SystemMatrix>
bool
NonLinearConjugateGradientSolver<SystemMatrix>::solve( const SystemVector& xold, SystemVector& x )
{
  Real beta = 0;
  SystemVector R(xold.size());
  SystemVector gradF(xold.size());
  SystemVector dx(xold.size());
  SystemVector dx_old(xold.size());
  SystemVector s(xold.size());
  SystemVector s_old(xold.size());
  SystemVector delta(xold.size());
  SystemVector y(xold.size());

  DLA::MatrixD<Real> B(xold.size(), xold.size());

  f_.jacobian(xold, nz_);

  //Should we create just a temporary matrix here? Should we pass in the matrix instead?
  SystemNonZeroPattern nzT(Transpose(nz_));
  SystemMatrix A(nzT);
  auto AT = Transpose(A);

  Real nrmUpd = UpdateTolerance_ + 1;

  x = xold;
  dx = 1.0;
  dx_old = 1.0;
  s = 0.0;
  s_old = 0.0;

  R = 0.0;
  Real nrmRsd = 0.0;
  f_.residual(x, R);
  f_.normResidual(R, nrmRsd);
  f_.normUpdate(dx, nrmUpd);

  std::cout << "0, " << nrmRsd << std::endl;

  for (int i = 0; i < MaxIterations_; i++)
  {
    R = 0.0;
    A = 0.0;
    f_.residual(x, R);
    f_.jacobian(x, AT);
    gradF = A * R;

    dx = -1.0 * gradF;
    // P-R with automatic reset
    delta = dx - dx_old;
//    beta = max(0.0, dot(dx, delta) / norm(dx_old, 2));
    beta = max(0.0, dot(dx, delta) / norm(dx_old, 2));
//    std::cout << " " << beta << std::endl;
//    beta = 0;
    // Conjugate the gradient
    s = dx + (beta * s_old);
    s = -1.0 * s;

#if 1
    SystemVector Rd(s.size());
    SystemVector Xd(s.size());
    Real fd = 0;
    std::string filename = "tmp/line";
    filename += stringify(i);
    filename += ".dat";
    std::fstream flout( filename, std::fstream::out );
    int N = 1000;
    for (int j = 0; j < N+1; j++)
    {
      Real d = 4.0 * j / N - 2.0;
      Rd = 0.0;
      fd = 0.0;
      Xd = x - d*s;
      f_.residual(Xd , Rd);
      f_.normResidual(Rd, fd);
      flout << d << " " << fd << std::endl;
    }
#endif

    f_.lineSearch(x, s);

    x -= s;

    R = 0.0;
    nrmRsd = 0.0;
    f_.residual(x, R);
    f_.normResidual(R, nrmRsd);
    f_.normUpdate(dx, nrmUpd);

    R = 0.0;
    A = 0.0;
    f_.residual(x, R);
    f_.jacobian(x, AT);
    y = -1.0 * A * R - gradF;
//    B = B + y*Transpose(y) / dot(y,y) - (B*s)*(Transpose(s)*B) / dot(s,B*s);

#if 0
    std::string filename = "tmp/jac";
    //filename += stringify(i);
    filename += ".mtx";
    std::fstream fout( filename, std::fstream::out );
    WriteMatrixMarketFile( A, fout );
#endif

    std::cout << i+1 << ", " << nrmRsd << std::endl;

    if (nrmRsd < ResidualTolerance_ || nrmUpd < UpdateTolerance_)
    {
      return true;
    }

    dx_old = dx;
    s_old = -1.0 * s;

  }
  return false;
}

}

#endif //NONLINEARCONJUGATEGRADIENTSOLVER_H
