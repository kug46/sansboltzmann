// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LINEUPDATEBASE_H
#define LINEUPDATEBASE_H

#include "tools/SANSnumerics.h"

#include "LineSearchStatus.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Applies a linear update dq to the solution q0
//
//
// template parameters:
//

template<class SystemVector>
class LineUpdateBase
{
public:
  virtual ~LineUpdateBase() {};

  //--------------------------------------------------------------------------//
  // Operator to perform the line update
  virtual LineSearchStatus operator()(const SystemVector& rsd0, const std::vector<std::vector<Real>>& normRsd0,
                                      const SystemVector& q0, const SystemVector& dq, const int& iter,
                                      SystemVector& q, Real& s) const = 0;
};

}

#endif //LINEUPDATEBASE_H
