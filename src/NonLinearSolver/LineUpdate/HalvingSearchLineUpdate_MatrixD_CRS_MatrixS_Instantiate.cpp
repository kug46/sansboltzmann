// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define HALVINGLINESEARCH_INSTANTIATE
#include "HalvingSearchLineUpdate_impl.h"
#include "UserVariables/BoltzmannNVar.h"

namespace SANS
{

// explicit instantiations
template class HalvingSearchLineUpdate<SLA::SparseMatrix_CRS<Real>>;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<Real>> >;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<2, 2, Real>>> >;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<3, 3, Real>>> >;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<4, 4, Real>>> >;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<5, 5, Real>>> >;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<6, 6, Real>>> >;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<7, 7, Real>>> >;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<8, 8, Real>>> >;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<9, 9, Real>>> >;
//template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<13, 13, Real>>> >;
//template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<16, 16, Real>>> >;
template class HalvingSearchLineUpdate<DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<NVar, NVar, Real>>> >;

}
