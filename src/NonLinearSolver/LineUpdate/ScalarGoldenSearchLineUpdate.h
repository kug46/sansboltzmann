// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SCALARGOLDENSEARCH_H
#define SCALARGOLDENSEARCH_H

#include <memory> // shared_ptr

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "LineUpdateBase.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"
#include "LinearAlgebra/SparseLinAlg/tools/norm.h"

#include "LinearAlgebra/DenseLinAlg/tools/norm.h"

#include "LinearAlgebra/BlockLinAlg/BlockLinAlg_Mul.h"
#include "LinearAlgebra/BlockLinAlg/BlockLinAlg_Add.h"
#include "LinearAlgebra/BlockLinAlg/tools/norm.h"

#include "tools/output_std_vector.h"

namespace SANS
{

// Forward declare

template<class SystemMatrix>
class ScalarGoldenSearchLineUpdate;

//=============================================================================
struct ScalarGoldenSearchLineUpdateParam : noncopyable
{
  const ParameterNumeric<Real> tol{"tol", 1.0e-15, 0, NO_LIMIT, "Convergence tolerance"};
  const ParameterBool Verbose{"Verbose", false, "Verbosity flag"};

  static void checkInputs(PyDict d);

  template<class SystemMatrix>
  static std::shared_ptr< LineUpdateBase<typename VectorType<SystemMatrix>::type> >
  newLineUpdate(const PyDict& d, AlgebraicEquationSetBase<SystemMatrix> &f)
  {
    typedef typename VectorType<SystemMatrix>::type SystemVector;
    typedef std::shared_ptr< LineUpdateBase<SystemVector> > LineUpdate_ptr;

    return LineUpdate_ptr( new ScalarGoldenSearchLineUpdate<SystemMatrix>(f, d) );
  }
  static ScalarGoldenSearchLineUpdateParam params;
};


template<class SystemMatrix>
class ScalarGoldenSearchLineUpdate : public LineUpdateBase<typename VectorType<SystemMatrix>::type>
{
public:
  typedef typename VectorType<SystemMatrix>::type SystemVector;
  typedef AlgebraicEquationSetBase<SystemMatrix> EquationSetBase;

  // cppcheck-suppress noExplicitConstructor
  ScalarGoldenSearchLineUpdate(EquationSetBase &f, const PyDict& d) :
    f_(f),
    tol_(d.get(ScalarGoldenSearchLineUpdateParam::params.tol)),
    verbose_(d.get(ScalarGoldenSearchLineUpdateParam::params.Verbose))
  {
    // Nothing
  };

  explicit ScalarGoldenSearchLineUpdate(EquationSetBase &f, const Real tol = 1e-15, const bool verbose = false) :
    f_(f), tol_(tol), verbose_(verbose)
  {};

  virtual ~ScalarGoldenSearchLineUpdate() {};

  // Apply the Scalar Golden Search update
  // TODO: norm() should be replaced with something better
  virtual LineSearchStatus operator()(const SystemVector& rsd0, const std::vector<std::vector<Real>>& normRsd0,
                                      const SystemVector& q0, const SystemVector& dq,
                                      const int& iter, SystemVector& q, Real& s) const override
  {
    SANS_ASSERT_MSG( s > 0 && s <= 1, "s = %f", s);

    LineSearchStatus status;

    Real r = (sqrt(5) - 1)/ 2; // Golden search ratio

    Real a = 0.0, b = s*2.0; // HACK: need to actually calculate where the 'other-side' is

    // Interior test points
    Real c = b - r*(b - a);
    Real d = a + r*(b - a);
    SystemVector Rc(f_.vectorEqSize());
    SystemVector Rd(f_.vectorEqSize());
    SystemVector Xc(f_.vectorStateSize());
    SystemVector Xd(f_.vectorStateSize());
    Real fc = 0;
    Real fd = 0;

    // Search
    while (fabs(b - a) > tol_)
    {
      Rc = 0;
      Xc = q0 - c*dq;
      f_.residual(Xc, Rc);
      fc = norm(Rc,2);
      Rd = 0;
      Xd = q0 - d*dq;
      f_.residual(Xd, Rd);
      fd = norm(Rd,2);

      // update the test point locations
      if (fc < fd)
      {
        b = d;
        d = c;
        c = b - r*(b - a);
      }
      else
      {
        a = c;
        c = d;
        d = a + r*(b - a);
      }

      if (verbose_)
      {
        SystemVector Xs(f_.vectorStateSize());
        SystemVector Rs(f_.vectorEqSize());
        Real s2 = (a + b) / 2.0;
        Xs = q0 - s2*dq;
        Rs = 0;
        f_.residual(Xs, Rs);
        std::vector<std::vector<Real>> nrmRsd( f_.residualNorm(Rs) );
        std::cout  << nrmRsd << std::setprecision(3) << std::scientific << "  Linesearch: s = " << s2 << ", Res_L2norm = " << std::endl;
      }
    }
    s = (a + b) / 2.0;
    q = q0 - s * dq;
    status.minimizingStepSize = s;
    return status;
  }

private:
  EquationSetBase &f_;
  Real tol_;
  bool verbose_;
};

}

#endif //SCALARGOLDENSEARCH_H
