// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define HALVINGLINESEARCH_INSTANTIATE
#include "HalvingSearchLineUpdate_impl.h"

#include "LinearAlgebra/BlockLinAlg/MatrixBlock_2x2.h"
#include "LinearAlgebra/BlockLinAlg/VectorBlock_2.h"

namespace SANS
{

// explicit instantiations

template class HalvingSearchLineUpdate<
                BLA::MatrixBlock_2x2<
                  DLA::MatrixD<SLA::SparseMatrix_CRS<Real>>, DLA::MatrixD<SLA::SparseMatrix_CRS<Real>>,
                  DLA::MatrixD<SLA::SparseMatrix_CRS<Real>>, DLA::MatrixD<SLA::SparseMatrix_CRS<Real>> >  >;

template class HalvingSearchLineUpdate<
                BLA::MatrixBlock_2x2<
                  DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<2,2,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<2,1,Real>>>,
                  DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<1,2,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<Real>> >  >;

template class HalvingSearchLineUpdate<
                BLA::MatrixBlock_2x2<
                  DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<3,3,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<3,1,Real>>>,
                  DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<1,3,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<Real>> >  >;

template class HalvingSearchLineUpdate<
                BLA::MatrixBlock_2x2<
                  DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<4,4,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<4,1,Real>>>,
                  DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<1,4,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<Real>> >  >;

template class HalvingSearchLineUpdate<
                BLA::MatrixBlock_2x2<
                  DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<5,5,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<5,1,Real>>>,
                  DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<1,5,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<Real>> >  >;

template class HalvingSearchLineUpdate<
                 BLA::MatrixBlock_2x2<
                   DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<8,8,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<8,2,Real>>>,
                   DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<2,8,Real>>>, DLA::MatrixD<SLA::SparseMatrix_CRS<DLA::MatrixS<2,2,Real>>>
                                     >
                           >;
}
