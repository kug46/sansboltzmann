// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LINEUPDATE_H
#define LINEUPDATE_H

#include "Python/PyDict.h"

#include <memory> // shared_ptr

#include "LineUpdateBase.h"

#include "NewtonLineUpdate.h"
#include "ScalarGoldenSearchLineUpdate.h"
#include "HalvingSearchLineUpdate.h"

namespace SANS
{

//============================================================================//
struct LineUpdateParam : noncopyable
{
  struct LineUpdateOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Method{"LineUpdateMethod", "Newton", "Line Update Method Name" };
    const ParameterString& key = Method;

    const DictOption Newton{"Newton", NewtonLineUpdateParam::checkInputs};
    const DictOption ScalarGoldenSearch{"ScalarGoldenSearch", ScalarGoldenSearchLineUpdateParam::checkInputs};
    const DictOption HalvingSearch{"HalvingSearch", HalvingSearchLineUpdateParam::checkInputs};

    const std::vector<DictOption> options{Newton, ScalarGoldenSearch, HalvingSearch};
  };
  const ParameterOption<LineUpdateOptions> LineUpdate{"LineUpdate", NO_DEFAULT, "Line update for a non-linear solver"};

  static void checkInputs(PyDict d);
  static LineUpdateParam params;
};


//============================================================================//
template<class SystemMatrix>
class LineUpdate : public LineUpdateBase<typename VectorType<SystemMatrix>::type>
{
public:
  typedef typename VectorType<SystemMatrix>::type SystemVector;
  typedef std::shared_ptr< LineUpdateBase<SystemVector> > LineUpdate_ptr;
  typedef AlgebraicEquationSetBase<SystemMatrix> EquationSetBase;

  LineUpdateParam& params = LineUpdateParam::params;

  explicit LineUpdate( const PyDict& d, EquationSetBase &f )
  {
    DictKeyPair UpdateParam = d.get(params.LineUpdate);

    if ( UpdateParam == params.LineUpdate.Newton )
    {
      lineUpdate_ = NewtonLineUpdateParam::newLineUpdate<SystemVector>( UpdateParam );
      return;
    }
    else if ( UpdateParam == params.LineUpdate.ScalarGoldenSearch )
    {
      lineUpdate_ = ScalarGoldenSearchLineUpdateParam::newLineUpdate<SystemMatrix>( UpdateParam, f );
      return;
    }
    else if ( UpdateParam == params.LineUpdate.HalvingSearch )
    {
      lineUpdate_ = HalvingSearchLineUpdateParam::newLineUpdate<SystemMatrix>( UpdateParam, f );
      return;
    }

    // Should never get here if checkInputs was called
    SANS_DEVELOPER_EXCEPTION("Unrecognized line search algorithm specified");
  }

  virtual ~LineUpdate() {}

  //--------------------------------------------------------------------------//
  // Operator to perform the line update
  virtual LineSearchStatus operator()(const SystemVector& rsd0, const std::vector<std::vector<Real>>& normRsd0,
                                      const SystemVector& q0, const SystemVector& dq,
                                      const int& iter, SystemVector& q, Real& s) const
  {
    return (*lineUpdate_)(rsd0, normRsd0, q0, dq, iter, q, s);
  }

private:
  LineUpdate_ptr lineUpdate_;
};


} //namespace SANS

#endif //LINEUPDATE_H
