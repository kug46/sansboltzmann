// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "NewtonLineUpdate.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void NewtonLineUpdateParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}

} //namespace SANS
