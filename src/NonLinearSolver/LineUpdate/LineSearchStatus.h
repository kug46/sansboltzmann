// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LINESEARCHSTATUS_H
#define LINESEARCHSTATUS_H

#include <vector>

#include "tools/SANSnumerics.h"

namespace SANS
{

// Linesearch status returned
struct LineSearchStatus
{
  LineSearchStatus() :
    validStepSize(1.0),
    minimizingStepSize(1.0),
    status(FullStep)
  {
    // Nothing
  }

  enum Status
  {
    // ----- success flags -----
    FullStep,     // Solved and only took 'full' (read Newton) steps
    PartialStep,  // Solved and was limited by the line search at some point
    LimitedStep,  // Solved and was limited by maximum change fraction
    // ----- non-success flags -----
    Failure,      // Line search fails
  };

  Real validStepSize;
  Real minimizingStepSize;
  Status status;
  std::vector<std::vector<Real>> initialResidual;
  std::vector<std::vector<Real>> validResidual;
  std::vector<std::vector<Real>> minimizedResidual;
};

}

#endif //LINESEARCHSTATUS_H
