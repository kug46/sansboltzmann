// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ScalarGoldenSearchLineUpdate.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void ScalarGoldenSearchLineUpdateParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.tol));
  allParams.push_back(d.checkInputs(params.Verbose));
  d.checkUnknownInputs(allParams);
}
ScalarGoldenSearchLineUpdateParam ScalarGoldenSearchLineUpdateParam::params;

} //namespace SANS
