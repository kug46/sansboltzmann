// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "HalvingSearchLineUpdate.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void HalvingSearchLineUpdateParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.minStepSize));
  allParams.push_back(d.checkInputs(params.reductionFactor));
  allParams.push_back(d.checkInputs(params.maxResidualGrowthFactor));
  allParams.push_back(d.checkInputs(params.verbose));
  allParams.push_back(d.checkInputs(params.dumpLineSearchResiduals));
  allParams.push_back(d.checkInputs(params.dumpLineSearchField));
  d.checkUnknownInputs(allParams);
}
HalvingSearchLineUpdateParam HalvingSearchLineUpdateParam::params;

} //namespace SANS
