// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(HALVINGLINESEARCH_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "HalvingSearchLineUpdate.h"
#include "tools/SANSnumerics.h"
#include <limits>

// This is ok because this implementation file is only ever indluced in a cpp file
#define MPI_COMMUNICATOR_IN_CPP
#include "MPI/communicator.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class SystemMatrix>
LineSearchStatus
HalvingSearchLineUpdate<SystemMatrix>::
operator()( const SystemVector& rsd0, const std::vector<std::vector<Real>>& normRsd0,
            const SystemVector& q0, const SystemVector& dq,
            const int& iter, SystemVector& q, Real& s) const
{
  SANS_ASSERT_MSG( s > 0 && s <= 1, "Line-search step size: s = %f", s);

  LineSearchStatus status;

  // temporary solution and residual containers
  SystemVector qd(f_.vectorStateSize());
  SystemVector rsd(f_.vectorEqSize());

  if (dumpField_) // dump localized linesearch step-size data to tecplot file
  {
    // Data structures needed for debugging - to localize line-search stepsize
    LinesearchData pResData;
    LinesearchData pStepData;

    f_.updateLinesearchDebugInfo(0.0, rsd0, pResData, pStepData);

    Real sd = 1;
    bool isDataComplete = false;
    status.validStepSize = sd;

#if 0
    qd = q0 - dq;
    f_.setSolutionField(qd);
    f_.dumpSolution("tmp/qfld_fullstep.dat");
#endif

    Real macheps = std::numeric_limits<Real>::epsilon();
    while ((isDataComplete == false) && (sd > macheps))
    {
      qd = q0 - sd*dq;
//        std::cout << std::setprecision(3) << std::scientific << "  Linesearch debug: s = " << stmp << std::endl;

      bool isValidState = f_.isValidStateSystemVector(qd);
      if (isValidState)
      {
        rsd = 0;
        f_.residual(qd, rsd);

        // update debug information
        isDataComplete = f_.updateLinesearchDebugInfo(sd, rsd, pResData, pStepData);
        status.validStepSize = sd;
      }

      sd *= reductionFactor_;
    }

    // dump line-search step-sizes to Tecplot
    f_.dumpLinesearchDebugInfo("tmp/linesearchfld", iter, pStepData);
  }

  // step-length subdivision until solution is valid and residual reduces

  const int comm_rank = f_.comm()->rank();

  // reference normed residual: line search tries to find an updated state that yields a lower residual than that
  std::vector<std::vector<Real>> normRsdRef(normRsd0);
  status.initialResidual = normRsdRef;
  for (std::size_t i = 0; i < normRsdRef.size(); i++)
    for (std::size_t j = 0; j < normRsdRef[i].size(); j++)
      normRsdRef[i][j] *= residualGrowthFactor_;

  // initialize the valid step and residual to a zero step (the last solution must have been valid)
  status.validStepSize = 0;
  status.validResidual = normRsd0;

  bool is_linesearch_success = false; // whether line search succeeds
  bool firstValid = true;             // if the step size is the first valid one
  status.status = LineSearchStatus::FullStep;

  while (s > tol_)
  {
    qd = q0 - s*dq;

    if (f_.isValidStateSystemVector(qd))
    {
      // check residual reduction
      rsd = 0.0;
      f_.residual(qd, rsd);
      const std::vector<std::vector<Real>> normRsd(f_.residualNorm(rsd));

      if (firstValid)
      {
        status.validStepSize = s;
        status.validResidual = normRsd;
        firstValid = false;
      }

      if (verbose_)
      {
        if (comm_rank == 0)
          std::cout << std::setprecision(3) << std::scientific << "  Linesearch: s = " << s << ", Res_L2norm = " << normRsd << std::endl;
      }

      // line search succeeds when state is valid and residual is reduced
      if (f_.decreasedResidual(normRsdRef, normRsd))
      {
        status.minimizedResidual = normRsd;
        status.minimizingStepSize = s;
        is_linesearch_success = true;
        break;
      }
    }
    else
    {
      if (comm_rank == 0 && verbose_)
        std::cout << "Validity check in line search failed: s = " << s << std::endl;
    }

    s *= reductionFactor_;
  }

  if (s < 1.0)
  {
    status.status = LineSearchStatus::PartialStep;
  }

  // set output solution q
  if (is_linesearch_success)
    q = qd;
  else
  {
    status.status = LineSearchStatus::Failure;
    if (comm_rank == 0 && verbose_)
    {
      std::cout << "  Linesearch failed!" << std::endl;
      std::cout << "  Could not reduce the residuals: s = " << s << ", allowable residualGrowthFactor = " << residualGrowthFactor_ << std::endl;
      f_.printDecreaseResidualFailure(normRsd0);
    }
    status.minimizedResidual = normRsdRef;
    status.minimizingStepSize = s;

    if (dumpResiduals_) writeLineSearch(q0, dq, iter); // dump only when line search fails

#if 0 // TODO: Marshall, do we still need to keep this?
    std::cout << "dumping tmp/linesearchResiduals.dat" << std::endl;
    std::ofstream linesearchfail("tmp/linesearchResiduals.dat");
    linesearchfail << std::setprecision(16);

    Real macheps = std::numeric_limits<Real>::epsilon();
    Real s = 1/reductionFactor_;
    while (s > macheps)
    {
      s *= reductionFactor_;
      qd = q0 - s*dq;

      rsd = 0;
      f_.residual(qd, rsd);

      linesearchfail << s << " ";
      for (int elem : {1414, 1397})
      {
        //for (int i = 0; i < rsd.m(); i++)
          //for (int j = 0; j < rsd[i].m(); j++)
        for (int j = 0; j < 3; j++)
        {
          int iDOF = 3*elem + j;
          for (int k = 0; k < DLA::VectorSize<typename SystemVector::node_type::Ttype>::M; k++)
            linesearchfail << DLA::index(rsd[0][iDOF],k) << " ";
        }
      }
      linesearchfail << std::endl;
    }
    linesearchfail.close();
#endif

    q = q0;
    f_.setSolutionField(q);
  }

  return status;
}


//----------------------------------------------------------------------------//
template<class SystemMatrix>
void
HalvingSearchLineUpdate<SystemMatrix>::
writeLineSearch(const SystemVector& q0, const SystemVector& dq, const int nonlinear_iter) const
{
  SystemVector rsd(f_.vectorEqSize());
  SystemVector q(f_.vectorStateSize());

  const int comm_rank = f_.comm()->rank();

  std::string filename_linesearch = "tmp/linesearch_iter" + std::to_string(nonlinear_iter) + ".dat";
  std::fstream fout_linesearch;
  if (comm_rank == 0)
  {
    fout_linesearch.open( filename_linesearch, std::fstream::out );
    std::cout << "Writing line search residuals to: " << filename_linesearch << std::endl;
  }

  const int N = 1000;
  const Real s_max = 1.0;
  const Real delta = 5;

  for (int j = 0; j < N+1; j++)
  {
    Real t = ((Real)j/(Real)N);
    Real s =  s_max*(1 + tanh( delta*( t - 1 ) ) / tanh(delta));
    //Real s = s_min + (s_max - s_min) * ((Real)j/(Real)N);

    rsd = 0;

    q = q0 - s*dq;
    f_.residual(q, rsd);
    std::vector<std::vector<Real>> norm_tmp( f_.residualNorm(rsd) );

    if (comm_rank == 0)
      fout_linesearch << std::setprecision(4)  << std::scientific << s << ", "
                      << std::setprecision(10) << std::scientific << norm_tmp << std::endl;
  }

  fout_linesearch.close();

  // wait for rank 0 to finish writing the file
  f_.comm()->barrier();
}

} // namespace SANS
