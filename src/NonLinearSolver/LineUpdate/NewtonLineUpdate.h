// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NEWTONLINEUPDATE
#define NEWTONLINEUPDATE

//Python must be included first
#include "Python/PyDict.h"

#include "tools/noncopyable.h"

#include "LineUpdateBase.h"

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"

#include <memory>  // shared_ptr

namespace SANS
{

// Forward declare
template<class SystemVector>
class NewtonLineUpdate;

//=============================================================================
struct NewtonLineUpdateParam : noncopyable
{
  static void checkInputs(PyDict d);


  template<class SystemVector>
  static std::shared_ptr< LineUpdateBase<SystemVector> > newLineUpdate(const PyDict& d)
  {
    typedef std::shared_ptr< LineUpdateBase<SystemVector> > LineUpdate_ptr;
    return LineUpdate_ptr( new NewtonLineUpdate<SystemVector> );
  }
};


template<class SystemVector>
class NewtonLineUpdate : public LineUpdateBase<SystemVector>
{
public:
  virtual ~NewtonLineUpdate() {};

  // Apply the full newton update
  virtual LineSearchStatus operator()(const SystemVector& rsd0, const std::vector<std::vector<Real>>& normRsd0,
                                      const SystemVector& q0, const SystemVector& dq, const int& iter,
                                      SystemVector& q, Real& s) const override
  {
    SANS_ASSERT_MSG( s > 0 && s <= 1, "s = %f", s);

    LineSearchStatus status; // default constructor is sucess and full step sizes

    q = q0 - s*dq;

    return status; // Linear search is considered successful in this case.
  }
};

}

#endif //NEWTONLINEUPDATE
