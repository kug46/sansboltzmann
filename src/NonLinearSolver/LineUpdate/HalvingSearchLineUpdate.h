// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HALVINGLINESEARCH_H
#define HALVINGLINESEARCH_H

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <iostream>
#include <fstream>
#include <string>
#include <memory> //shared_ptr

#include "LineUpdateBase.h"

#include "LinearAlgebra/AlgebraicEquationSetBase.h"

#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Add.h"
#include "LinearAlgebra/SparseLinAlg/SparseLinAlg_Mul.h"

#include "LinearAlgebra/DenseLinAlg/tools/index.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"

#include "tools/output_std_vector.h"


namespace SANS
{

// Forward declare

template<class SystemMatrix>
class HalvingSearchLineUpdate;

//============================================================================//
struct HalvingSearchLineUpdateParam : noncopyable
{
  const ParameterNumeric<Real> minStepSize{"minStepSize", 1.0e-8, 0, NO_LIMIT, "Minimum step size (relative to full Newton)"};
  const ParameterNumeric<Real> reductionFactor{"reductionFactor", 0.5, 0., 1., "Step-length reduction factor: s *= reductionFactor"};
  const ParameterNumeric<Real> maxResidualGrowthFactor{"maxResidualGrowthFactor", 1.0, 1.0, NO_LIMIT,
                                                       "Maximum residual growth factor per Newton step"
                                                      };
  const ParameterBool verbose{"verbose", false, "[T/F] Verbose screen output"};
  const ParameterBool dumpLineSearchResiduals{"dumpLineSearchResiduals", false, "[T/F] Dump out steps and residuals when line search fails"};
  const ParameterBool dumpLineSearchField{"dumpLineSearchField", false, "[T/F] Dump out line-search step-size field to Tecplot"};

  static void checkInputs(PyDict d);

  template<class SystemMatrix>
  static std::shared_ptr< LineUpdateBase<typename VectorType<SystemMatrix>::type> >
  newLineUpdate(const PyDict& d, AlgebraicEquationSetBase<SystemMatrix> &f)
  {
    typedef typename VectorType<SystemMatrix>::type SystemVector;
    typedef std::shared_ptr< LineUpdateBase<SystemVector> > LineUpdate_ptr;

    return LineUpdate_ptr( new HalvingSearchLineUpdate<SystemMatrix>(f, d) );
  }
  static HalvingSearchLineUpdateParam params;
};


//============================================================================//
template<class SystemMatrix>
class HalvingSearchLineUpdate : public LineUpdateBase<typename VectorType<SystemMatrix>::type>
{
public:

  typedef AlgebraicEquationSetBase<SystemMatrix> EquationSetBase;
  typedef typename VectorType<SystemMatrix>::type SystemVector;

  typedef typename EquationSetBase::LinesearchData LinesearchData;

  // cppcheck-suppress noExplicitConstructor
  HalvingSearchLineUpdate(EquationSetBase &f, const PyDict& d) :
    f_(f),
    tol_(d.get(HalvingSearchLineUpdateParam::params.minStepSize)),
    reductionFactor_(d.get(HalvingSearchLineUpdateParam::params.reductionFactor)),
    residualGrowthFactor_(d.get(HalvingSearchLineUpdateParam::params.maxResidualGrowthFactor)),
    verbose_(d.get(HalvingSearchLineUpdateParam::params.verbose)),
    dumpResiduals_(d.get(HalvingSearchLineUpdateParam::params.dumpLineSearchResiduals)),
    dumpField_(d.get(HalvingSearchLineUpdateParam::params.dumpLineSearchField)) {};

  explicit HalvingSearchLineUpdate( EquationSetBase &f,
                                    const Real tol = 1e-8, const Real reductionFactor = 0.5, const Real residualGrowthFactor = 1.0,
                                    const bool verbose = false, const bool dumpResiduals = false, const bool dumpField = false) :
    f_(f), tol_(tol), reductionFactor_(reductionFactor), residualGrowthFactor_(residualGrowthFactor),
    verbose_(verbose), dumpResiduals_(dumpResiduals), dumpField_(dumpField) {};

  virtual ~HalvingSearchLineUpdate() {};

  // Apply the Halving Search update
  virtual LineSearchStatus operator()(const SystemVector& rsd0, const std::vector<std::vector<Real>>& normRsd0,
                                      const SystemVector& q0, const SystemVector& dq,
                                      const int& iter, SystemVector& q, Real& s) const override;

protected:

  //Writing out residual vs line search s
  void writeLineSearch(const SystemVector& q0, const SystemVector& dq, const int nonlinear_iter) const;

private:
  EquationSetBase &f_;
  Real tol_;
  Real reductionFactor_;             // step-length reduction factor; s *= reductionFactor_; (default = 1/2)
  Real residualGrowthFactor_;
  bool verbose_;
  bool dumpResiduals_;
  bool dumpField_;
};

}

#endif //HALVINGLINESEARCH_H
