// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "NewtonSolver.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

NewtonSolverParam::NewtonSolverParam()
  : LineUpdate(LineUpdateParam::params.LineUpdate),
    LinearSolver(SLA::LinearSolverParam::params.LinearSolver)
{}

// cppcheck-suppress passedByValue
void NewtonSolverParam::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.LinearSolver));
  allParams.push_back(d.checkInputs(params.LineUpdate));
  allParams.push_back(d.checkInputs(params.MaxIterations));
  allParams.push_back(d.checkInputs(params.MinIterations));
  allParams.push_back(d.checkInputs(params.MaxChangeFraction));
  allParams.push_back(d.checkInputs(params.Verbose));
  allParams.push_back(d.checkInputs(params.DumpJacobian));
  allParams.push_back(d.checkInputs(params.ResidualHistoryFile));
  allParams.push_back(d.checkInputs(params.Timing));
  allParams.push_back(d.checkInputs(params.TestMachinePrecision));
  d.checkUnknownInputs(allParams);
}
NewtonSolverParam NewtonSolverParam::params;

}
