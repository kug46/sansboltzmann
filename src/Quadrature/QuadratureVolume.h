// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QUADRATUREVOLUME_H
#define QUADRATUREVOLUME_H

// volume quadrature rules for reference tet/hex

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "BasisFunction/Quadrature_Cache.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// QuadratureVolume:  volume quadrature rules for reference tetrahedron/hexahedron
//
//   reference tet: x in [ 0, 1]; y in [0, 1-x]; z in [0, 1-x-y]
//   reference hex: x in [-1, 1]; y in [-1, 1]; z in [-1, 1]
//
// template parameters:
//    Topology                area-element topology (Triangle, Quad)
//
// member functions:
//   .order                   order of quadrature rule => order of the polynomial that needs to be integrated exactly
//   .nQuadrature             number of quadrature points
//   .weight                  weight at given quadrature point; zero-based indexing
//   .coordinates             coordinate (x in [0, 1]) at given quadrature point; zero-based indexing
//
//   .dump                    debug dump of private data
//
// Note: ctor called with requested quadrature order (orderReqd)
//    n < 0     max quadrature rule
//    n >= 0    at least n-th order quadrature rule
//    n > MAX   max quadrature rule (see implementations for max-degree polynomial)
//----------------------------------------------------------------------------//

template <class Topology>
class QuadratureVolume
{
public:
  explicit QuadratureVolume( const int orderReqd );
  QuadratureVolume(const QuadratureVolume&) = delete;
  ~QuadratureVolume();

  static const int nOrderIdx;
  static int getOrderIndex( const int orderReqd );
  static int getOrderFromIndex( const int orderidx );

  int order() const { return order_; }
  int nQuadrature() const { return nQuad_; }

  void weight( int n, Real& wght ) const;
  void coordinates( int, Real xyz[] ) const;
  void coordinates( int, Real& x, Real& y, Real& z ) const;
  void coordinates( int n, SANS::DLA::VectorS<3,Real>& xyz ) const { coordinates(n, xyz[0], xyz[1], xyz[2]); }

  Real weight( int n ) const;
  SANS::DLA::VectorS<3,Real> coordinates( int n ) const;
  SANS::QuadraturePoint<TopoD3> coordinates_cache( int n ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  int order_;                 // quadrature rule order
  int orderidx_;              // quadrature rule order index for caching
  int nQuad_;                 // total quadrature points
  Real* weight_;              // weights at quadrature points
  Real* xcoord_;              // coordinates at quadrature points
  Real* ycoord_;
  Real* zcoord_;
};


template<class Topology>
std::ostream&
operator<<( std::ostream& out, const QuadratureVolume<Topology>& quadrature );

} // namespace SANS

#endif  // QUADRATUREVOLUME_H
