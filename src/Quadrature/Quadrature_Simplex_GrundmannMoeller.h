// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// TODO rewrite this after reading Grundmann, Moeller 1978
//
#ifndef QUADRATURE_SIMPLEX_GRUNDMANN_MOELLER_H_
#define QUADRATURE_SIMPLEX_GRUNDMANN_MOELLER_H_

#include <algorithm> // min, max

double
simplex_unit_volume( int m )
//
//    computes the volume volume = 1/M!.
//
{
  int i;
  double volume;

  volume = 1.0;
  for ( i = 1; i <= m; i++ )
  {
    volume = volume / ( ( double ) i );
  }

  return volume;
}

int
i4_choose( int n, int k )
//
//    computes the binomial coefficient C(N,K).
//
//    The formula used is:
//
//      C(N,K) = N! / ( K! * (N-K)! )
//
{
  int i;
  int mn;
  int mx;
  int value;

  //mn = i4_min ( k, n - k );
  mn = std::min( k , n -k );

  if ( mn < 0 )
  {
    value = 0;
  }
  else if ( mn == 0 )
  {
    value = 1;
  }
  else
  {
    //mx = i4_max ( k, n - k );
    mx = std::max( k , n -k );
    value = mx + 1;

    for ( i = 2; i <= mn; i++ )
    {
      value = ( value * ( mx + i ) ) / i;
    }
  }

  return value;
}


void
comp_next( int n, int k, int a[], bool &more, int &h, int &t )

//****************************************************************************80
//
//  computes the compositions of the integer N into K parts.
//
//  Discussion:
//
//    A composition of the integer N into K parts is an ordered sequence
//    of K nonnegative integers which sum to N.  The compositions (1,2,1)
//    and (1,1,2) are considered to be distinct.
//
//    The routine computes one composition on each call until there are no more.
//    For instance, one composition of 6 into 3 parts is
//    3+2+1, another would be 6+0+0.
//
//    On the first call to this routine, set MORE = FALSE.  The routine
//    will compute the first element in the sequence of compositions, and
//    return it, as well as setting MORE = TRUE.  If more compositions
//    are desired, call again, and again.  Each time, the routine will
//    return with a new composition.
//
//    However, when the LAST composition in the sequence is computed
//    and returned, the routine will reset MORE to FALSE, signaling that
//    the end of the sequence has been reached.
//
//    This routine originally used a SAVE statement to maintain the
//    variables H and T.  I have decided that it is safer
//    to pass these variables as arguments, even though the user should
//    never alter them.  This allows this routine to safely shuffle
//    between several ongoing calculations.
//
//
//    There are 28 compositions of 6 into three parts.  This routine will
//    produce those compositions in the following order:
//
//     I         A
//     -     ---------
//     1     6   0   0
//     2     5   1   0
//     3     4   2   0
//     4     3   3   0
//     5     2   4   0
//     6     1   5   0
//     7     0   6   0
//     8     5   0   1
//     9     4   1   1
//    10     3   2   1
//    11     2   3   1
//    12     1   4   1
//    13     0   5   1
//    14     4   0   2
//    15     3   1   2
//    16     2   2   2
//    17     1   3   2
//    18     0   4   2
//    19     3   0   3
//    20     2   1   3
//    21     1   2   3
//    22     0   3   3
//    23     2   0   4
//    24     1   1   4
//    25     0   2   4
//    26     1   0   5
//    27     0   1   5
//    28     0   0   6
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    02 July 2008
//
//  Author:
//
//    Original FORTRAN77 version by Albert Nijenhuis, Herbert Wilf.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Albert Nijenhuis, Herbert Wilf,
//    Combinatorial Algorithms for Computers and Calculators,
//    Second Edition,
//    Academic Press, 1978,
//    ISBN: 0-12-519260-6,
//    LC: QA164.N54.
//
//  Parameters:
//
//    Input, int N, the integer whose compositions are desired.
//
//    Input, int K, the number of parts in the composition.
//
//    Input/output, int A[K], the parts of the composition.
//
//    Input/output, bool &MORE.
//    Set MORE = FALSE on first call.  It will be reset to TRUE on return
//    with a new composition.  Each new call returns another composition until
//    MORE is set to FALSE when the last composition has been computed
//    and returned.
//
//    Input/output, int &H, &T, two internal parameters needed for the
//    computation.  The user should allocate space for these in the calling
//    program, include them in the calling sequence, but never alter them!
//
{
  int i;

  if ( !( more ) )
  {
    t = n;
    h = 0;
    a[0] = n;
    for ( i = 1; i < k; i++ )
    {
       a[i] = 0;
    }
  }
  else
  {
    if ( 1 < t )
    {
      h = 0;
    }
    h = h + 1;
    t = a[h-1];
    a[h-1] = 0;
    a[0] = t - 1;
    a[h] = a[h] + 1;
  }

  more = ( a[k-1] != n );

  return;
}
//*****************************************************************************/

//****************************************************************************80

int
gm_rule_size( int rule, int m )

//****************************************************************************80
//
//  determines the size of a Grundmann-Moeller rule.
//
//  Discussion:
//
//    This rule returns the value of N, the number of points associated
//    with a GM rule of given index.
//
//    After calling this rule, the user can use the value of N to
//    allocate space for the weight vector as W(N) and the abscissa
//    vector as X(M,N), and then call GM_RULE_SET.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 July 2007
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Axel Grundmann, Michael Moeller,
//    Invariant Integration Formulas for the N-Simplex
//    by Combinatorial Methods,
//    SIAM Journal on Numerical Analysis,
//    Volume 15, Number 2, April 1978, pages 282-290.
//
//  Parameters:
//
//    Input, int RULE, the index of the rule.
//    0 <= RULE.
//
//    Input, int M, the spatial dimension.
//    1 <= M.
//
//    Output, int GM_RULE_SIZE, the number of points in the rule.
//
{
  int arg1;
  int n;

  arg1 = m + rule + 1;

  n = i4_choose ( arg1, rule );

  return n;
}
//****************************************************************************80

void
gm_unit_rule_set ( int rule, int m, int n, double w[], double x[] )

//****************************************************************************80
//
//  sets a Grundmann-Moeller rule over the unit m-simplex
//
//  Discussion:
//
//    This is a revised version of the calculation which seeks to compute
//    the value of the weight in a cautious way that avoids intermediate
//    overflow.  Thanks to John Peterson for pointing out the problem on
//    26 June 2008.
//
//    This rule returns weights and abscissas of a Grundmann-Moeller
//    quadrature rule for the M-dimensional unit simplex.
//
//    The dimension N can be determined by calling GM_RULE_SIZE.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    26 June 2008
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Axel Grundmann, Michael Moeller,
//    Invariant Integration Formulas for the N-Simplex
//    by Combinatorial Methods,
//    SIAM Journal on Numerical Analysis,
//    Volume 15, Number 2, April 1978, pages 282-290.
//
//  Parameters:
//
//    Input, int RULE, the index of the rule.
//    0 <= RULE.
//
//    Input, int M, the spatial dimension.
//    1 <= M.
//
//    Input, int N, the number of points in the rule.
//
//    Output, double W[N], the weights.
//
//    Output, double X[M*N], the abscissas.
//
{
  int *beta;
  int beta_sum;
  int d;
  int dim;
  int h;
  int i;
  int j;
  int j_hi;
  int k;
  bool more;
  double one_pm;
  int s;
  int t;
  //double volume1;
  double weight;

  s = rule;
  d = 2 * s + 1;
  k = 0;
  one_pm = 1.0;

  beta = new int[m+1];

  for ( i = 0; i <= s; i++ )
  {
    weight = ( double ) one_pm;

    //j_hi = i4_max ( m, i4_max ( d, d + m - i ) );
    j_hi = std::max( m , std::max( d , d +m -i ) );

    for ( j = 1; j <= j_hi; j++ )
    {
      if ( j <= m )
      {
        weight = weight * ( double ) ( j );
      }
      if ( j <= d )
      {
        weight = weight * ( double ) ( d + m - 2 * i );
      }
      if ( j <= 2 * s )
      {
        weight = weight / 2.0;
      }
      if ( j <= i )
      {
        weight = weight / ( double ) ( j );
      }
      if ( j <= d + m - i )
      {
        weight = weight / ( double ) ( j );
      }
    }

    one_pm = - one_pm;

    beta_sum = s - i;
    more = false;
    h = 0;
    t = 0;

    for ( ; ; )
    {
      comp_next ( beta_sum, m + 1, beta, more, h, t );

      w[k] = weight;
      for ( dim = 0; dim < m; dim++ )
      {
        x[dim+k*m] = ( double ) ( 2 * beta[dim+1] + 1 )
                   / ( double ) ( d + m - 2 * i );
      }
      k = k + 1;

      if ( !more )
      {
        break;
      }
    }
  }

  // normalize.
  // NOTE not for SANS!
  /*volume1 = simplex_unit_volume ( m );
  for ( i = 0; i < n; i++ )
  {
    w[i] = w[i] * volume1;
  }*/

  // free memory.
  delete [] beta;

  return;
}

#endif
