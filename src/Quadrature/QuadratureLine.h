// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QUADRATURELINE_H
#define QUADRATURELINE_H

// quadrature rules for line segment

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "BasisFunction/Quadrature_Cache.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// QuadratureLine:  quadrature rules for line segment; x in [0, 1]
//
// member functions:
//   .order                   order of quadrature rule => order of the polynomial that needs to be integrated exactly
//   .nQuadrature             number of quadrature points
//   .weight                  weight at given quadrature point; zero-based indexing
//   .coordinate              coordinate (x in [0, 1]) at given quadrature point; zero-based indexing
//
//   .dump                    debug dump of private data
//
// Note: ctor called with requested quadrature order (orderReqd)
//    n < 0     max quadrature rule
//    n >= 0    at least n-th order quadrature rule
//    n > 9     max quadrature rule (degree 9 polynomial)
//----------------------------------------------------------------------------//

class QuadratureLine
{
public:
  explicit QuadratureLine( int orderReqd );
  QuadratureLine(const QuadratureLine&) = delete;
  QuadratureLine& operator=(const QuadratureLine&) = delete;
  ~QuadratureLine();

  static const int nOrderIdx;
  static int getOrderIndex( const int orderReqd );
  static int getOrderFromIndex( const int orderidx );

  int order() const { return order_; }
  int nQuadrature() const { return nQuad_; }

  void weight( int n, Real& wght ) const;
  void coordinate( int, Real& coord ) const;
  void coordinates( int n, SANS::DLA::VectorS<1,Real>& coord ) const { coordinate(n, coord[0]); }

  Real weight( int n ) const;
  Real coordinate( int ) const;
  SANS::QuadraturePoint<TopoD1> coordinates_cache( int n ) const;

  const Real* coordinates() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  int order_;                 // quadrature rule order
  int orderidx_;              // quadrature rule order index for caching
  int nQuad_;                 // total quadrature points
  Real* weight_;              // weights at quadrature points
  Real* coord_;               // coordinates at quadrature points
};

// I/O
std::ostream&
operator<<( std::ostream& out, const QuadratureLine& quadrature );

} // namespace SANS

#endif  // QUADRATURELINE_H
