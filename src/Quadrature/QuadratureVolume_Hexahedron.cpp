// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QuadratureVolume<Hex>
// area quadrature rules for reference hexahedron (x in [ 0, 1]; y in [0, 1]; z in [0, 1])

#include <string>
#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "QuadratureVolume.h"
#include "QuadratureLine.h"

namespace SANS
{

// number of order indexes, must be consistent with QuadratureLine::nOrderIdx
template<>
const int QuadratureVolume<Hex>::nOrderIdx = 13;

template <>
int
QuadratureVolume<Hex>::getOrderIndex( const int orderReqd )
{
  return QuadratureLine::getOrderIndex( orderReqd );
}

template <>
int
QuadratureVolume<Hex>::getOrderFromIndex( const int orderidx )
{
  return QuadratureLine::getOrderFromIndex( orderidx );
}

template <>
QuadratureVolume<Hex>::QuadratureVolume( const int orderReqd ) :
    order_(0), orderidx_(getOrderIndex(orderReqd)), nQuad_(0), weight_(NULL), xcoord_(NULL), ycoord_(NULL), zcoord_(NULL)
{
  //orderReqd: order of the polynomial that needs to be integrated exactly

  // I really want QuadratureVolume<Hex>::nOrderIdx = QuadratureLine::nOrderIdx above, but it does not work...
  SANS_ASSERT( nOrderIdx == QuadratureLine::nOrderIdx );

  // Simple tensor product quadrature
  QuadratureLine quadLine(orderReqd);

  int nQuadLine = quadLine.nQuadrature();

  order_  = quadLine.order();
  nQuad_  = nQuadLine*nQuadLine*nQuadLine;
  weight_ = new Real[nQuad_];
  xcoord_ = new Real[nQuad_];
  ycoord_ = new Real[nQuad_];
  zcoord_ = new Real[nQuad_];


  for (int k = 0; k < nQuadLine; k++)
    for (int j = 0; j < nQuadLine; j++)
      for (int i = 0; i < nQuadLine; i++)
      {
        int n = i + nQuadLine*(j + nQuadLine*k);
        weight_[n] = quadLine.weight(i)*quadLine.weight(j)*quadLine.weight(k);
        xcoord_[n] = quadLine.coordinate(i);
        ycoord_[n] = quadLine.coordinate(j);
        zcoord_[n] = quadLine.coordinate(k);
      }
}


template <>
QuadratureVolume<Hex>::~QuadratureVolume()
{
  delete [] ycoord_;
  delete [] xcoord_;
  delete [] zcoord_;
  delete [] weight_;
  nQuad_ = 0;
}


template <>
void
QuadratureVolume<Hex>::weight( int n, Real& wght ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  wght = weight_[n];
}


template <>
void
QuadratureVolume<Hex>::coordinates( int n, Real xyz[] ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  xyz[0] = xcoord_[n];
  xyz[1] = ycoord_[n];
  xyz[2] = zcoord_[n];
}


template <>
void
QuadratureVolume<Hex>::coordinates( int n, Real& x, Real& y, Real& z ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  x = xcoord_[n];
  y = ycoord_[n];
  z = zcoord_[n];
}


template <>
SANS::QuadraturePoint<TopoD3>
QuadratureVolume<Hex>::coordinates_cache( int n ) const
{
  return SANS::QuadraturePoint<TopoD3>(SANS::QuadratureRule::eGauss, n, orderidx_, {xcoord_[n], ycoord_[n], zcoord_[n]});
}


template <>
void
QuadratureVolume<Hex>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "QuadratureVolume<Hex>: total points = " << nQuad_ << std::endl;
  if (weight_ != 0)
  {
    out << indent << "  weights = ";
    for (int k = 0; k < nQuad_; k++)
      out << weight_[k] << " ";
    out << std::endl;
  }
  if (xcoord_ != 0)
  {
    out << indent << "  xcoords = ";
    for (int k = 0; k < nQuad_; k++)
      out << xcoord_[k] << " ";
    out << std::endl;
  }
  if (ycoord_ != 0)
  {
    out << indent << "  ycoords = ";
    for (int k = 0; k < nQuad_; k++)
      out << ycoord_[k] << " ";
    out << std::endl;
  }
  if (zcoord_ != 0)
  {
    out << indent << "  zcoords = ";
    for (int k = 0; k < nQuad_; k++)
      out << zcoord_[k] << " ";
    out << std::endl;
  }
}


// I/O
template <>
std::ostream&
operator<<( std::ostream& out, const QuadratureVolume<Hex>& quadrature )
{
  int nquad = quadrature.nQuadrature();
  Real w, xy[3];
  out << "QuadratureVolume<Hex> " << nquad << ": ";
  for (int k = 0; k < nquad; k++)
  {
    quadrature.weight(k, w);
    quadrature.coordinates(k, xy);
    out << "(" << w << ", " << xy[0] <<  ", " << xy[1] <<") ";
  }
  return out;
}

} // namespace SANS
