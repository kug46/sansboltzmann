// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QUADRATURESPACETIME_H
#define QUADRATURESPACETIME_H

// volume quadrature rules for reference tet/hex

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "BasisFunction/Quadrature_Cache.h"

#include <iostream>

#define USE_SPARSE
// #define USE_CONICAL

namespace SANS
{

//----------------------------------------------------------------------------//
// QuadratureSpacetime:  volume quadrature rules for reference pentatope
//
// template parameters:
//    Topology                area-element topology (Tet)
//
// member functions:
//   .order                   order of quadrature rule => order of the polynomial that needs to be integrated exactly
//   .nQuadrature             number of quadrature points
//   .weight                  weight at given quadrature point; zero-based indexing
//   .coordinates             coordinate (x in [0, 1]) at given quadrature point; zero-based indexing
//
//   .dump                    debug dump of private data
//
// Note: ctor called with requested quadrature order (orderReqd)
//    n < 0     max quadrature rule
//    n >= 0    at least n-th order quadrature rule
//    n > MAX   max quadrature rule (see implementations for max-degree polynomial)
//----------------------------------------------------------------------------//

template <class Topology>
class QuadratureSpacetime
{
public:
  explicit QuadratureSpacetime( const int orderReqd );
  QuadratureSpacetime(const QuadratureSpacetime&) = delete;
  ~QuadratureSpacetime();

  static const int nOrderIdx;
  static int getOrderIndex( const int orderReqd );
  static int getOrderFromIndex( const int orderidx );

  int order() const { return order_; }
  int nQuadrature() const { return nQuad_; }

  void weight( int n, Real& wght ) const;
  void coordinates( int, Real xyzt[] ) const;
  void coordinates( int, Real& x, Real& y, Real& z , Real& t ) const;
  void coordinates( int n, SANS::DLA::VectorS<4,Real>& xyzt ) const { coordinates(n, xyzt[0], xyzt[1], xyzt[2], xyzt[3]); }

  Real weight( int n ) const;
  SANS::DLA::VectorS<4,Real> coordinates( int n ) const;
  SANS::QuadraturePoint<TopoD4> coordinates_cache( int n ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  int order_;                 // quadrature rule order
  int orderidx_;              // quadrature rule order index for caching
  int nQuad_;                 // total quadrature points
  Real* weight_;              // weights at quadrature points
  Real* xcoord_;              // coordinates at quadrature points
  Real* ycoord_;
  Real* zcoord_;
  Real* tcoord_;
};


template<class Topology>
std::ostream&
operator<<( std::ostream& out, const QuadratureSpacetime<Topology>& quadrature );

} // namespace SANS

#endif  // QUADRATUREVOLUME_H
