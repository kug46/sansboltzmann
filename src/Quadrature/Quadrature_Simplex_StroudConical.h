// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
#ifndef QUADRATURE_SIMPLEX_STROUDCONICAL_H_
#define QUADRATURE_SIMPLEX_STROUDCONICAL_H_

/**
 * nPointsStroudQuadrature calculates the number of points necessary for a quadrature
 * rule of the conical method of Stroud.
 *
 * Returns order^nDim.
 *
 * @param nDim[in]    number of dimensions of simplex of interest
 * @param order[in]   order of integration requested
 * @return            number of points for a quadrature rule
 */
double nPointsStroudQuadrature(int nDim, int order);

/**
 * calculateStroudQuadrature calculates a quadrature rule with the conical method of Stroud.
 *
 * Resulting numerical integration of polynomials of up to a given order in any given
 * direction will be integrated exactly on a simplex of arbitrary dimension using the
 * resulting rule.
 *
 * @param nDim[in]    the number of dimensions of the simplex of interest
 * @param order[in]   the order of integration requested
 * @param x[out]      the nDim*order points requested, each point of nDim coordinates
 * @param w[out]      the nDim*order weights requested
 *
 */
void calculateStroudQuadrature(int nDim, int order, double *x, double *w);

/**
 * calculateJacobiQuadrature calculates a Gauss-Jacobi quadrature rule.
 *
 * $\int_{0}^{1} (1 - x)^\alpha x^\beta f(x) \mathrm{~d} x \approx \sum_i w_i x_i$;
 *
 * This function exists to wrap the code of John Burkardt.
 * @see http://people.sc.fsu.edu/~jburkardt/cpp_src/jacobi_rule/jacobi_rule.html
 *
 * @param n[in]       the number of points (i.e. quadrature order) requested
 * @param alpha[in]   $\alpha$
 * @param beta[in]    $\beta$
 * @param t[out]      the $n$ quadrature points, $x_i$
 * @param wts[out]    the $n$ quadrature weights, $w_i$
 */
void calculateJacobiQuadrature(int order, double alpha, double beta, double *t, double *wts);

/**
 * PARCHK checks parameters ALPHA and BETA for classical weight functions.
 *
 * Purpose:
 *   PARCHK checks parameters ALPHA and BETA for classical weight functions.
 *
 * Licensing:
 *   This code is distributed under the GNU LGPL license.
 *
 * Modified:
 *   07 January 2010
 *
 * Author:
 *   Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
 *   C++ version by John Burkardt.
 *
 * Reference:
 *   Sylvan Elhay, Jaroslav Kautsky,
 *   Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of
 *   Interpolatory Quadrature,
 *   ACM Transactions on Mathematical Software,
 *   Volume 13, Number 4, December 1987, pages 399-415.
 *
 * Various quadrature rules can be selected by this program, indicated by
 * the parameter kind, which is an integer. Options include:
 *   - 1, Legendre,             (a,b)       1.0
 *   - 2, Chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
 *   - 3, Gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
 *   - 4, Jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
 *   - 5, Generalized Laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
 *   - 6, Generalized Hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
 *   - 7, Exponential,          (a,b)       |x-(a+b)/2.0|^alpha
 *   - 8, Rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
 *
 * Converted for SANS by Cory Frontin in 2019
 *
 * @param kind    the type of quadrature rule used
 * @param m       the order of the highest moment to be calculated; only for kind = 8
 * @param alpha   floating point number for alpha possibly required for a given quadrature
 * @param beta    floating point number for beta possibly required for a given quadrature
 *
 */
void parchk(int kind, int m, double alpha, double beta);

/**
 * IMTQLX diagonalizes a symmetric tridiagonal matrix.
 *
 * Purpose:
 *   IMTQLX diagonalizes a symmetric tridiagonal matrix.
 *
 * Discussion:
 *   This routine is a slightly modified version of the EISPACK routine to
 *   perform the implicit QL algorithm on a symmetric tridiagonal matrix.
 *
 *   The authors thank the authors of EISPACK for permission to use this
 *   routine.
 *
 *   It has been modified to produce the product Q' * Z, where Z is an input
 *   vector and Q is the orthogonal matrix diagonalizing the input matrix.
 *   The changes consist (essentialy) of applying the orthogonal transformations
 *   directly to Z as they are generated.
 *
 * Licensing:
 *   This code is distributed under the GNU LGPL license.
 *
 * Modified:
 *   08 January 2010
 *
 * Author:
 *   Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
 *   C++ version by John Burkardt.
 *
 * Reference:
 *   Sylvan Elhay, Jaroslav Kautsky,
 *   Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of
 *   Interpolatory Quadrature,
 *   ACM Transactions on Mathematical Software,
 *   Volume 13, Number 4, December 1987, pages 399-415.
 *
 *   Roger Martin, James Wilkinson,
 *   The Implicit QL Algorithm,
 *   Numerische Mathematik,
 *   Volume 12, Number 5, December 1968, pages 377-383.
 *
 *
 * @param n       the order of the matrix
 *
 * @param d[in]   the n diagonal entries of the input matrix
 * @param d[out]  the n diagonal entries of the output matrix
 *
 * @param e[in]   the n - 1 subdiagonal entries of the input matrix; extra entry on end
 * @param e[out]  the n - 1 subdiagonal entries of the output matrix; extra entry on end
 *
 * @param z[in]   an input vector
 * @param z[out]  the value of Q'*Z where Q is the matrix that diagonalizes the input matrix
 *
 */
void imtqlx(int n, double *d, double *e, double *z);

/**
 * CDGQF computes a Gauss quadrature formula on the unit interval
 *
 * Purpose:
 *   CDGQF computes a Gauss quadrature formula with default A, B and simple knots.
 *
 * Discussion:
 *   This routine computes all the knots and weights of a Gauss quadrature
 *   formula with a classical weight function with default values for A and B,
 *   and only simple knots.
 *
 *   There are no moments checks and no printing is done.
 *
 * Licensing:
 *   This code is distributed under the GNU LGPL license.
 *
 * Modified:
 *   08 January 2010
 *
 * Author:
 *   Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
 *   C++ version by John Burkardt.
 *
 * Reference:
 *   Sylvan Elhay, Jaroslav Kautsky,
 *   Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of
 *   Interpolatory Quadrature,
 *   ACM Transactions on Mathematical Software,
 *   Volume 13, Number 4, December 1987, pages 399-415.
 *
 * Various quadrature rules can be selected by this program, indicated by
 * the parameter kind, which is an integer. Options include:
 *   - 1, Legendre,             (a,b)       1.0
 *   - 2, Chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
 *   - 3, Gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
 *   - 4, Jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
 *   - 5, Generalized Laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
 *   - 6, Generalized Hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
 *   - 7, Exponential,          (a,b)       |x-(a+b)/2.0|^alpha
 *   - 8, Rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
 *
 * Converted for SANS by Cory Frontin in 2019
 *
 * @param nt      the number of knots
 * @param kind    the type of quadrature used
 * @param alpha   floating point number for alpha, possibly required for a given quadrature
 * @param beta    floating point number for beta, possibly required for a given quadrature
 * @param t       the knots of the quadrature rule
 * @param wts     the weights of the quadrature rule
 *
 */
void cdgqf(int nt, int kind, double alpha, double beta, double *t, double *wts);

/**
 * CLASS_MATRIX computes the Jacobi matrix for a quadrature rule.
 *
 * Purpose:
 *   CLASS_MATRIX computes the Jacobi matrix for a quadrature rule.
 *
 * Discussion:
 *   This routine computes the diagonal AJ and sub-diagonal BJ
 *   elements of the order M tridiagonal symmetric Jacobi matrix
 *   associated with the polynomials orthogonal with respect to
 *   the weight function specified by KIND.
 *
 *   For weight functions 1-7, M elements are defined in BJ even
 *   though only M-1 are needed.  For weight function 8, BJ(M) is
 *   set to zero.
 *
 *   The zero-th moment of the weight function is returned in ZEMU.
 *
 * Licensing:
 *   This code is distributed under the GNU LGPL license.
 *
 * Modified:
 *   08 January 2010
 *
 * Author:
 *   Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
 *   C++ version by John Burkardt.
 *
 * Reference:
 *   Sylvan Elhay, Jaroslav Kautsky,
 *   Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of
 *   Interpolatory Quadrature,
 *   ACM Transactions on Mathematical Software,
 *   Volume 13, Number 4, December 1987, pages 399-415.
 *
 * Various quadrature rules can be selected by this program, indicated by
 * the parameter kind, which is an integer. Options include:
 *   - 1, Legendre,             (a,b)       1.0
 *   - 2, Chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
 *   - 3, Gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
 *   - 4, Jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
 *   - 5, Generalized Laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
 *   - 6, Generalized Hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
 *   - 7, Exponential,          (a,b)       |x-(a+b)/2.0|^alpha
 *   - 8, Rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
 *
 * Converted for SANS by Cory Frontin in 2019
 *
 * @param kind[in]        the type of quadrature used
 * @param m[in]           the order of the Jacobi matrix
 * @param alpha[in]       floating point number for alpha, possibly required for a given quadrature
 * @param beta[in]        floating point number for beta, possibly required for a given quadrature
 * @param aj[in]          the diagonal of the Jacobi matrix
 * @param bj[in]          the subdiagonal of the Jacobi matrix
 * @return class_matrix   the zeroth moment
 *
 */
double class_matrix(int kind, int m, double alpha, double beta, double *aj, double *bj);

/**
 * SGQF computes knots and weights of a Gauss Quadrature formula.
 *
 * Purpose:
 *   SGQF computes knots and weights of a Gauss Quadrature formula.
 *
 * Discussion:
 *   This routine computes all the knots and weights of a Gauss quadrature
 *   formula with simple knots from the Jacobi matrix and the zero-th
 *   moment of the weight function, using the Golub-Welsch technique.
 *
 * Licensing:
 *   This code is distributed under the GNU LGPL license.
 *
 * Modified:
 *   08 January 2010
 *
 * Author:
 *   Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
 *   C++ version by John Burkardt.
 *
 * Reference:
 *   Sylvan Elhay, Jaroslav Kautsky,
 *   Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of
 *   Interpolatory Quadrature,
 *   ACM Transactions on Mathematical Software,
 *   Volume 13, Number 4, December 1987, pages 399-415.
 *
 * @param nt              the number of knots
 * @param aj[in]          the diagonal of the Jacobi matrix
 * @param bj[in]          the subdiagonal of the Jacobi matrix
 * @param bj[out]         the overwritten subdiagonal of the Jacobi matrix
 * @param zemu[in]        the zeroth moment of the weight function
 * @oaram t[out]          the knots of the quadrature rule
 * @oaram wts[out]        the weights of the quadrature rule
 *
 */
void sgqf(int nt, double *aj, double *bj, double zemu, double *t, double *wts);

/**
 * SCQF scales a quadrature formula to a nonstandard interval.
 *
 * Purpose:
 *   SCQF scales a quadrature formula to a nonstandard interval.
 *
 * Discussion:
 *   The arrays WTS and SWTS may coincide.
 *
 *   The arrays T and ST may coincide.
 *
 * Licensing:
 *   This code is distributed under the GNU LGPL license.
 *
 * Modified:
 *   16 February 2010
 *
 * Author:
 *   Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
 *   C++ version by John Burkardt.
 *
 * Reference:
 *   Sylvan Elhay, Jaroslav Kautsky,
 *   Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of
 *   Interpolatory Quadrature,
 *   ACM Transactions on Mathematical Software,
 *   Volume 13, Number 4, December 1987, pages 399-415.
 *
 * @param nt[in]      the number of knots
 * @param t[in]       the original knots of the quadrature rule
 * @param mlt[in]     the multiplicity of the knots
 * @param wts[in]     the weights
 * @param nwts[in]    the number of weights
 * @param ndx[in]     used to index the array wts
 * @param swts[out]   the scaled output weights
 * @param st[out]     the scaled knows
 * @param kind[in]    the type of quadrature used
 * @param alpha[in]   the value of alpha, if needed
 * @param beta[in]    the value of beta, if needed
 * @param a[in]       the left endpoint of the interval
 * @param b[in]       the right endpoint of the interval
 *
 */
void scqf(int nt, double *t, int *mlt, double *wts, int nwts, int *ndx, double *swts,
           double *st, int kind, double alpha, double beta, double a, double b);

/**
 * CGQF computes knots and weights of a Gauss quadrature formula.
 *
 * Purpose:
 *   CGQF computes knots and weights of a Gauss quadrature formula.
 *
 * Discussion:
 *   The user may specify the interval (A,B).
 *
 *   Only simple knots are produced.
 *
 * Licensing:
 *   This code is distributed under the GNU LGPL license.
 *
 * Modified:
 *   16 February 2010
 *
 * Author:
 *   Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
 *   C++ version by John Burkardt.
 *
 * Reference:
 *   Sylvan Elhay, Jaroslav Kautsky,
 *   Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of
 *   Interpolatory Quadrature,
 *   ACM Transactions on Mathematical Software,
 *   Volume 13, Number 4, December 1987, pages 399-415.
 *
 * Various quadrature rules can be selected by this program, indicated by
 * the parameter kind, which is an integer. Options include:
 *   - 1, Legendre,             (a,b)       1.0
 *   - 2, Chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
 *   - 3, Gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
 *   - 4, Jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
 *   - 5, Generalized Laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
 *   - 6, Generalized Hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
 *   - 7, Exponential,          (a,b)       |x-(a+b)/2.0|^alpha
 *   - 8, Rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
 *
 * Converted for SANS by Cory Frontin in 2019
 *
 * @param nt      the number of knots
 * @param kind    the type of quadrature used
 * @param alpha   floating point number for alpha, possibly required for a given quadrature
 * @param beta    floating point number for beta, possibly required for a given quadrature
 * @param a       left endpoint for interval domain
 * @param b       right endpoint for interval domain
 * @param t       the knots of the quadrature rule
 * @param wts     the weights of the quadrature rule
 *
 */
void cgqf(int nt, int kind, double alpha, double beta, double a, double b, double *t, double *wts);

#endif
