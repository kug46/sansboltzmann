// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// quadrature rules for line segment

#include <cmath>
#include <string>
#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "QuadratureLine.h"

namespace SANS
{

// number of order indexes
const int QuadratureLine::nOrderIdx = 13;

int
QuadratureLine::getOrderIndex( const int orderReqd )
{
  if (orderReqd < 0)          // max quadrature
    return nOrderIdx-1;
  else if (orderReqd <= 1)
    return 0;
  else if (orderReqd <= 3)
    return 1;
  else if (orderReqd <= 5)
    return 2;
  else if (orderReqd <= 7)
    return 3;
  else if (orderReqd <= 9)
    return 4;
  else if (orderReqd <= 11)
    return 5;
  else if (orderReqd <= 13)
    return 6;
  else if (orderReqd <= 15)
    return 7;
  else if (orderReqd <= 17)
    return 8;
  else if (orderReqd <= 19)
    return 9;
  else if (orderReqd <= 21)
    return 10;
  else if (orderReqd <= 23)
    return 11;
  else if (orderReqd <= 39)
    return 12;
  else
    SANS_DEVELOPER_EXCEPTION("Quadrature order=%d is not available", orderReqd);

  // suppress compiler warnings
  return -1;
}

int
QuadratureLine::getOrderFromIndex( const int orderidx )
{
  // returns the quadrature order for a given order index into the quadrature cache
  switch (orderidx)
  {
  case 0:
    return 1;
  case 1:
    return 3;
  case 2:
    return 5;
  case 3:
    return 7;
  case 4:
    return 9;
  case 5:
    return 11;
  case 6:
    return 13;
  case 7:
    return 15;
  case 8:
    return 17;
  case 9:
    return 19;
  case 10:
    return 21;
  case 11:
    return 23;
  case 12:
    return 39;
  default:
    SANS_DEVELOPER_EXCEPTION("Please add orderidx=%d mapping", orderidx);
  }

  // suppress compiler warnings
  return -1;
}

QuadratureLine::QuadratureLine( int orderReqd ) :
    order_(0), orderidx_(getOrderIndex(orderReqd)), nQuad_(0), weight_(NULL), coord_(NULL)
{
  //orderReqd: order of the polynomial that needs to be integrated exactly

  int order;
  int orderMax;         // maximum allowed order
  int nQuad = 0;
  Real wght, x;

  // set quadrature rule order

  orderMax = 39;

  if (orderReqd < 0)          // max quadrature
    order = orderMax;
  else if (orderReqd <= 1)
    order = 1;
  else if (orderReqd <= 3)
    order = 3;
  else if (orderReqd <= 5)
    order = 5;
  else if (orderReqd <= 7)
    order = 7;
  else if (orderReqd <= 9)
    order = 9;
  else if (orderReqd <= 11)
    order = 11;
  else if (orderReqd <= 13)
    order = 13;
  else if (orderReqd <= 15)
    order = 15;
  else if (orderReqd <= 17)
    order = 17;
  else if (orderReqd <= 19)
    order = 19;
  else if (orderReqd <= 21)
    order = 21;
  else if (orderReqd <= 23)
    order = 23;
  else if (orderReqd <= 39)
    order = 39;
  else
    SANS_DEVELOPER_EXCEPTION("Quadrature order=%d is not available", orderReqd);

  // set weights, coordinates for x in [-1, 1]

  // degree 1 polynomial; 1 point
  if (order == 1)
  {
    nQuad = 1;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    weight_[0] = 2;
    coord_[0]  = 0;
  }

  // degree 3 polynomial; 2 points
  else if (order == 3)
  {
    nQuad = 2;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    wght = 1;
    x    = 1./sqrt(3.);
    weight_[0] = wght;
    weight_[1] = wght;
    coord_[0]  = -x;
    coord_[1]  =  x;
  }

  // degree 5 polynomial: 3 points
  else if (order == 5)
  {
    nQuad = 3;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    wght = 5./9.;
    x    = sqrt(3./5.);
    weight_[0] = wght;
    weight_[2] = wght;
    coord_[0]  = -x;
    coord_[2]  =  x;

    wght = 8./9.;
    x    = 0;
    weight_[1] = wght;
    coord_[1]  = x;
  }

  // degree 7 polynomial: 4 points
  else if (order == 7)
  {
    nQuad = 4;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    wght = 0.5 - sqrt(5./6.)/6.;
    x    = sqrt((3 + 2*sqrt(6./5.))/7.);
    weight_[0] = wght;
    weight_[3] = wght;
    coord_[0]  = -x;
    coord_[3]  =  x;

    wght = 0.5 + sqrt(5./6.)/6.;
    x    = sqrt((3 - 2*sqrt(6./5.))/7.);
    weight_[1] = wght;
    weight_[2] = wght;
    coord_[1]  = -x;
    coord_[2]  =  x;
  }

  // degree 9 polynomial; 5 points
  else if (order == 9)
  {
    nQuad = 5;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    wght = (322 - 13*sqrt(70.))/900.;
    x    = sqrt(5 + 2*sqrt(10./7.))/3.;
    weight_[0] = wght;
    weight_[4] = wght;
    coord_[0]  = -x;
    coord_[4]  =  x;

    wght = (322 + 13*sqrt(70.))/900.;
    x    = sqrt(5 - 2*sqrt(10./7.))/3.;
    weight_[1] = wght;
    weight_[3] = wght;
    coord_[1]  = -x;
    coord_[3]  =  x;

    wght = 128./225.;
    x    = 0;
    weight_[2] = wght;
    coord_[2]  = x;
  }

  // degree 11 polynomial; 6 points (PXE_QuadRule5)
  else if (order == 11)
  {
    nQuad = 6;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    // scale into reference element 0-1
    coord_[0] = -0.9324695142031520278123016;
    coord_[1] = -0.6612093864662645136613996;
    coord_[2] = -0.2386191860831969086305017;
    coord_[3] =  0.2386191860831969086305017;
    coord_[4] =  0.6612093864662645136613996;
    coord_[5] =  0.9324695142031520278123016;

    weight_[0] = 0.1713244923791703450402961;
    weight_[1] = 0.3607615730481386075698335;
    weight_[2] = 0.4679139345726910473898703;
    weight_[3] = 0.4679139345726910473898703;
    weight_[4] = 0.3607615730481386075698335;
    weight_[5] = 0.1713244923791703450402961;
  }

  // degree 13 polynomial; 7 points (PXE_QuadRule6)
  else if (order == 13)
  {
    nQuad = 7;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    // scale into reference element 0-1
    coord_[0] = -0.9491079123427585245261897;
    coord_[1] = -0.7415311855993944398638648;
    coord_[2] = -0.4058451513773971669066064;
    coord_[3] =  0.0000000000000000000000000;
    coord_[4] =  0.4058451513773971669066064;
    coord_[5] =  0.7415311855993944398638648;
    coord_[6] =  0.9491079123427585245261897;

    weight_[0] = 0.1294849661688696932706114;
    weight_[1] = 0.2797053914892766679014678;
    weight_[2] = 0.3818300505051189449503698;
    weight_[3] = 0.4179591836734693877551020;
    weight_[4] = 0.3818300505051189449503698;
    weight_[5] = 0.2797053914892766679014678;
    weight_[6] = 0.1294849661688696932706114;

  }

  // degree 15 polynomial; 8 points (PXE_QuadRule7)
  else if (order == 15)
  {
    nQuad = 8;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    // scale into reference element 0-1
    coord_[0] = -0.9602898564975362316835609;
    coord_[1] = -0.7966664774136267395915539;
    coord_[2] = -0.5255324099163289858177390;
    coord_[3] = -0.1834346424956498049394761;
    coord_[4] =  0.1834346424956498049394761;
    coord_[5] =  0.5255324099163289858177390;
    coord_[6] =  0.7966664774136267395915539;
    coord_[7] =  0.9602898564975362316835609;

    weight_[0] = 0.1012285362903762591525314;
    weight_[1] = 0.2223810344533744705443560;
    weight_[2] = 0.3137066458778872873379622;
    weight_[3] = 0.3626837833783619829651504;
    weight_[4] = 0.3626837833783619829651504;
    weight_[5] = 0.3137066458778872873379622;
    weight_[6] = 0.2223810344533744705443560;
    weight_[7] = 0.1012285362903762591525314;
  }

  // degree 17 polynomial; 9 points (PXE_QuadRule8)
  else if (order == 17)
  {
    nQuad = 9;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    // scale into reference element 0-1
    coord_[0] = -0.9681602395076260898355762;
    coord_[1] = -0.8360311073266357942994297;
    coord_[2] = -0.6133714327005903973087020;
    coord_[3] = -0.3242534234038089290385380;
    coord_[4] =  0.0000000000000000000000000;
    coord_[5] =  0.3242534234038089290385380;
    coord_[6] =  0.6133714327005903973087020;
    coord_[7] =  0.8360311073266357942994298;
    coord_[8] =  0.9681602395076260898355762;

    weight_[0] = 0.0812743883615744119718922;
    weight_[1] = 0.1806481606948574040584720;
    weight_[2] = 0.2606106964029354623187429;
    weight_[3] = 0.3123470770400028400686304;
    weight_[4] = 0.3302393550012597631645251;
    weight_[5] = 0.3123470770400028400686304;
    weight_[6] = 0.2606106964029354623187429;
    weight_[7] = 0.1806481606948574040584720;
    weight_[8] = 0.0812743883615744119718922;
  }

  // degree 19 polynomial; 10 points (PXE_QuadRule9)
  else if (order == 19)
  {
    nQuad = 10;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    // scale into reference element 0-1
    coord_[0] = -0.9739065285171717200779640;
    coord_[1] = -0.8650633666889845107320967;
    coord_[2] = -0.6794095682990244062343274;
    coord_[3] = -0.4333953941292471907992659;
    coord_[4] = -0.1488743389816312108848260;
    coord_[5] =  0.1488743389816312108848260;
    coord_[6] =  0.4333953941292471907992659;
    coord_[7] =  0.6794095682990244062343274;
    coord_[8] =  0.8650633666889845107320967;
    coord_[9] =  0.9739065285171717200779640;

    weight_[0] = 0.0666713443086881375935688;
    weight_[1] = 0.1494513491505805931457763;
    weight_[2] = 0.2190863625159820439955349;
    weight_[3] = 0.2692667193099963550912269;
    weight_[4] = 0.2955242247147528701738930;
    weight_[5] = 0.2955242247147528701738930;
    weight_[6] = 0.2692667193099963550912269;
    weight_[7] = 0.2190863625159820439955349;
    weight_[8] = 0.1494513491505805931457763;
    weight_[9] = 0.0666713443086881375935688;
  }

  // degree 21 polynomial; 11 points (PXE_QuadRule10)
  else if (order == 21)
  {
    nQuad = 11;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    // scale into reference element 0-1
    coord_[0]  = -0.9782286581460569928039380;
    coord_[1]  = -0.8870625997680952990751578;
    coord_[2]  = -0.7301520055740493240934162;
    coord_[3]  = -0.5190961292068118159257257;
    coord_[4]  = -0.2695431559523449723315320;
    coord_[5]  =  0.0000000000000000000000000;
    coord_[6]  =  0.2695431559523449723315320;
    coord_[7]  =  0.5190961292068118159257257;
    coord_[8]  =  0.7301520055740493240934163;
    coord_[9]  =  0.8870625997680952990751578;
    coord_[10] =  0.9782286581460569928039380;

    weight_[0]  = 0.0556685671161736664827537;
    weight_[1]  = 0.1255803694649046246346943;
    weight_[2]  = 0.1862902109277342514260976;
    weight_[3]  = 0.2331937645919904799185237;
    weight_[4]  = 0.2628045445102466621806889;
    weight_[5]  = 0.2729250867779006307144835;
    weight_[6]  = 0.2628045445102466621806889;
    weight_[7]  = 0.2331937645919904799185237;
    weight_[8]  = 0.1862902109277342514260980;
    weight_[9]  = 0.1255803694649046246346940;
    weight_[10] = 0.0556685671161736664827537;
  }

  // degree 23 polynomial; 12 points (PXE_QuadRule11)
  else if (order == 23)
  {
    nQuad = 12;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    // scale into reference element 0-1
    coord_[0] = -0.9815606342467192506905491;
    coord_[1] = -0.9041172563704748566784659;
    coord_[2] = -0.7699026741943046870368938;
    coord_[3] = -0.5873179542866174472967024;
    coord_[4] = -0.3678314989981801937526915;
    coord_[5] = -0.1252334085114689154724414;
    coord_[6] =  0.1252334085114689154724414;
    coord_[7] =  0.3678314989981801937526915;
    coord_[8] =  0.5873179542866174472967024;
    coord_[9] =  0.7699026741943046870368938;
    coord_[10] = 0.9041172563704748566784659;
    coord_[11] = 0.9815606342467192506905491;

    weight_[0]  = 0.0471753363865118271946160;
    weight_[1]  = 0.1069393259953184309602547;
    weight_[2]  = 0.1600783285433462263346525;
    weight_[3]  = 0.2031674267230659217490645;
    weight_[4]  = 0.2334925365383548087608499;
    weight_[5]  = 0.2491470458134027850005624;
    weight_[6]  = 0.2491470458134027850005624;
    weight_[7]  = 0.2334925365383548087608499;
    weight_[8]  = 0.2031674267230659217490645;
    weight_[9]  = 0.1600783285433462263346525;
    weight_[10] = 0.1069393259953184309602547;
    weight_[11] = 0.0471753363865118271946160;
  }
  // degree 39 polynomial; 20 points (PXE_QuadRule12)
  else if (order == 39)
  {
    nQuad = 20;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    coord_  = new Real[nQuad];

    coord_[ 0] = -0.9931285991850949247861224;
    coord_[ 1] = -0.9639719272779137912676661;
    coord_[ 2] = -0.9122344282513259058677524;
    coord_[ 3] = -0.8391169718222188233945291;
    coord_[ 4] = -0.7463319064601507926143051;
    coord_[ 5] = -0.6360536807265150254528367;
    coord_[ 6] = -0.5108670019508270980043641;
    coord_[ 7] = -0.3737060887154195606725482;
    coord_[ 8] = -0.2277858511416450780804962;
    coord_[ 9] = -0.0765265211334973337546404;
    coord_[10] =  0.0765265211334973337546404;
    coord_[11] =  0.2277858511416450780804962;
    coord_[12] =  0.3737060887154195606725482;
    coord_[13] =  0.5108670019508270980043641;
    coord_[14] =  0.6360536807265150254528367;
    coord_[15] =  0.7463319064601507926143051;
    coord_[16] =  0.8391169718222188233945291;
    coord_[17] =  0.9122344282513259058677524;
    coord_[18] =  0.9639719272779137912676661;
    coord_[19] =  0.9931285991850949247861224;

    weight_[ 0] = 0.0176140071391521183118620;
    weight_[ 1] = 0.0406014298003869413310400;
    weight_[ 2] = 0.0626720483341090635695065;
    weight_[ 3] = 0.0832767415767047487247581;
    weight_[ 4] = 0.1019301198172404350367501;
    weight_[ 5] = 0.1181945319615184173123774;
    weight_[ 6] = 0.1316886384491766268984945;
    weight_[ 7] = 0.1420961093183820513292983;
    weight_[ 8] = 0.1491729864726037467878287;
    weight_[ 9] = 0.1527533871307258506980843;
    weight_[10] = 0.1527533871307258506980843;
    weight_[11] = 0.1491729864726037467878287;
    weight_[12] = 0.1420961093183820513292983;
    weight_[13] = 0.1316886384491766268984945;
    weight_[14] = 0.1181945319615184173123774;
    weight_[15] = 0.1019301198172404350367501;
    weight_[16] = 0.0832767415767047487247581;
    weight_[17] = 0.0626720483341090635695065;
    weight_[18] = 0.0406014298003869413310400;
    weight_[19] = 0.0176140071391521183118620;
  }


  // convert quadrature rules to x = [0, 1]
  for (int n = 0; n < nQuad; n++)
  {
    weight_[n] *= 0.5;
    coord_[n] = (1 + coord_[n])/2.;
  }

}


QuadratureLine::~QuadratureLine()
{
  delete [] coord_;
  delete [] weight_;
  nQuad_ = 0;
  order_ = 0;
}


void
QuadratureLine::weight( int n, Real& wght ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  wght = weight_[n];
}


void
QuadratureLine::coordinate( int n, Real& x ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  x = coord_[n];
}

Real
QuadratureLine::weight( int n ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  return weight_[n];
}


Real
QuadratureLine::coordinate( int n ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  return coord_[n];
}

const Real*
QuadratureLine::coordinates() const
{
  return coord_;
}

SANS::QuadraturePoint<TopoD1>
QuadratureLine::coordinates_cache( int n ) const
{
  return SANS::QuadraturePoint<TopoD1>(SANS::QuadratureRule::eGauss, n, orderidx_, {coord_[n]});
}

void
QuadratureLine::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "order = " << order_ << "  total points = " << nQuad_ << std::endl;
  if (weight_ != 0)
  {
    out << indent << "  weights = ";
    for (int k = 0; k < nQuad_; k++)
      out << weight_[k] << " ";
    out << std::endl;
  }
  if (coord_ != 0)
  {
    out << indent << "  coords = ";
    for (int k = 0; k < nQuad_; k++)
      out << coord_[k] << " ";
    out << std::endl;
  }
}


// I/O
std::ostream&
operator<<( std::ostream& out, const QuadratureLine& quadrature )
{
  int order = quadrature.order();
  int nquad = quadrature.nQuadrature();
  Real w, x;
  out << "QuadratureLine  order = " << order << " nquad = " << nquad << ": ";
  for (int k = 0; k < nquad; k++)
  {
    quadrature.weight(k, w);
    quadrature.coordinate(k, x);
    out << "(" << w << ", " << x << ") ";
  }
  return out;
}

} // namespace SANS
