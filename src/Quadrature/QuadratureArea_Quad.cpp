// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// QuadratureArea<Quad>
// area quadrature rules for reference square (x in [0, 1]; y in [0, 1])

#include <cmath> // sqrt
#include <string>
#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/ElementTopology.h"
#include "QuadratureArea.h"
#include "QuadratureLine.h"

#include "BasisFunction/Quadrature_Cache.h"

namespace SANS
{

// number of order indexes, must be consistent with QuadratureLine::nOrderIdx
template<>
const int QuadratureArea<Quad>::nOrderIdx = 13;

template <>
int
QuadratureArea<Quad>::getOrderIndex( const int orderReqd )
{
  return QuadratureLine::getOrderIndex( orderReqd );
#if 0
  if (orderReqd < 0)          // max quadrature
    return nOrderIdx-1;
  else if (orderReqd <= 1)
    return 0;
  else if (orderReqd <= 3)
    return 1;
  else if (orderReqd <= 5)
    return 2;
  else if (orderReqd <= 7)
    return 3;
  else
    SANS_DEVELOPER_EXCEPTION("Quadrature order=%d is not available", orderReqd);

  // suppress compiler warnings
  return -1;
#endif
}

template <>
int
QuadratureArea<Quad>::getOrderFromIndex( const int orderidx )
{
  // returns the quadrature order for a given order index into the quadrature cache
  return QuadratureLine::getOrderFromIndex( orderidx );
#if 0
  switch (orderidx)
  {
  case 0:
    return 1;
  case 1:
    return 3;
  case 2:
    return 5;
  case 3:
    return 7;
  default:
    SANS_DEVELOPER_EXCEPTION("Please add orderidx=%d mapping", orderidx);
  }

  // suppress compiler warnings
  return -1;
#endif
}

template <>
QuadratureArea<Quad>::QuadratureArea( const int orderReqd ) :
    order_(0), orderidx_(getOrderIndex(orderReqd)), nQuad_(0), weight_(NULL), xcoord_(NULL), ycoord_(NULL)
{
  //orderReqd: order of the polynomial that needs to be integrated exactly


  // I really want QuadratureVolume<Hex>::nOrderIdx = QuadratureLine::nOrderIdx above, but it does not work...
  SANS_ASSERT( nOrderIdx == QuadratureLine::nOrderIdx );

  // Simple tensor product quadrature
  QuadratureLine quadLine(orderReqd);

  int nQuadLine = quadLine.nQuadrature();

  order_  = quadLine.order();
  nQuad_  = nQuadLine*nQuadLine;
  weight_ = new Real[nQuad_];
  xcoord_ = new Real[nQuad_];
  ycoord_ = new Real[nQuad_];

  for (int j = 0; j < nQuadLine; j++)
    for (int i = 0; i < nQuadLine; i++)
    {
      int n = i + nQuadLine*j;
      weight_[n] = quadLine.weight(i)*quadLine.weight(j);
      xcoord_[n] = quadLine.coordinate(i);
      ycoord_[n] = quadLine.coordinate(j);
    }

#if 0
  int order;
  int orderMax;         // maximum allowed order
  int nQuad;

  // set quadrature rule order

  orderMax = 7;

  if (orderReqd < 0)          // max quadrature
    order = orderMax;
  else if (orderReqd <= 1)
    order = 1;
  else if (orderReqd <= 3)
    order = 3;
  else if (orderReqd <= 5)
    order = 5;
  else if (orderReqd <= 7)
    order = 7;
  else
    SANS_DEVELOPER_EXCEPTION("Quadrature order=%d is not available", orderReqd);

  // set weights, coordinates for reference square (x in [0, 1]; y in [0, 1])
  // Note: weights sum to 1 (area of reference square)

  // degree 1 polynomial; 1 point
  if (order == 1)
  {
    nQuad = 1;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    xcoord_ = new Real[nQuad];
    ycoord_ = new Real[nQuad];

    weight_[0] = 1;
    xcoord_[0] = 0.5;
    ycoord_[0] = 0.5;
  }

  // degree 3 polynomial; 2*2 = 4 points
  else if (order == 3)
  {
    nQuad = 4;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    xcoord_ = new Real[nQuad];
    ycoord_ = new Real[nQuad];

    Real w = 0.25;
    Real x = 0.5*(1 - 1./sqrt(3.));

    weight_[0] = w;
    xcoord_[0] = x;
    ycoord_[0] = x;

    weight_[1] = w;
    xcoord_[1] = x;
    ycoord_[1] = 1-x;

    weight_[2] = w;
    xcoord_[2] = 1-x;
    ycoord_[2] = x;

    weight_[3] = w;
    xcoord_[3] = 1-x;
    ycoord_[3] = 1-x;
  }

  // degree 5 polynomial; 3*3 = 9 points
  else if (order == 5)
  {
    nQuad = 9;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    xcoord_ = new Real[nQuad];
    ycoord_ = new Real[nQuad];

    Real w  = 5./18.;
    Real x  = 0.5*(1 - sqrt(3./5.));
    Real w0 = 4./9.;
    Real x0 = 0.5;

    weight_[0] = w*w;
    xcoord_[0] = x;
    ycoord_[0] = x;

    weight_[1] = w0*w;
    xcoord_[1] = x0;
    ycoord_[1] = x;

    weight_[2] = w*w;
    xcoord_[2] = 1-x;
    ycoord_[2] = x;

    weight_[3] = w*w0;
    xcoord_[3] = x;
    ycoord_[3] = x0;

    weight_[4] = w0*w0;
    xcoord_[4] = x0;
    ycoord_[4] = x0;

    weight_[5] = w*w0;
    xcoord_[5] = 1-x;
    ycoord_[5] = x0;

    weight_[6] = w*w;
    xcoord_[6] = x;
    ycoord_[6] = 1-x;

    weight_[7] = w0*w;
    xcoord_[7] = x0;
    ycoord_[7] = 1-x;

    weight_[8] = w*w;
    xcoord_[8] = 1-x;
    ycoord_[8] = 1-x;
  }

  // degree 7 polynomial; 4*4 = 16 points
  else if (order == 7)
  {
    nQuad = 16;

    order_  = order;
    nQuad_  = nQuad;
    weight_ = new Real[nQuad];
    xcoord_ = new Real[nQuad];
    ycoord_ = new Real[nQuad];

    Real w0 = 0.25 - sqrt(5./6.)/12.;
    Real x0 = 0.5*(1.0 - sqrt((3 + 2*sqrt(6./5.))/7.));

    Real w1 = 0.25 + sqrt(5./6.)/12.;
    Real x1 = 0.5*(1.0 - sqrt((3 - 2*sqrt(6./5.))/7.));

    weight_[0] = w0*w0;
    xcoord_[0] = x0;
    ycoord_[0] = x0;

    weight_[1] = w1*w0;
    xcoord_[1] = x1;
    ycoord_[1] = x0;

    weight_[2] = w1*w0;
    xcoord_[2] = 1-x1;
    ycoord_[2] = x0;

    weight_[3] = w0*w0;
    xcoord_[3] = 1-x0;
    ycoord_[3] = x0;


    weight_[4] = w0*w1;
    xcoord_[4] = x0;
    ycoord_[4] = x1;

    weight_[5] = w1*w1;
    xcoord_[5] = x1;
    ycoord_[5] = x1;

    weight_[6] = w1*w1;
    xcoord_[6] = 1-x1;
    ycoord_[6] = x1;

    weight_[7] = w0*w1;
    xcoord_[7] = 1-x0;
    ycoord_[7] = x1;


    weight_[8] = w0*w1;
    xcoord_[8] = x0;
    ycoord_[8] = 1-x1;

    weight_[9] = w1*w1;
    xcoord_[9] = x1;
    ycoord_[9] = 1-x1;

    weight_[10] = w1*w1;
    xcoord_[10] = 1-x1;
    ycoord_[10] = 1-x1;

    weight_[11] = w0*w1;
    xcoord_[11] = 1-x0;
    ycoord_[11] = 1-x1;


    weight_[12] = w0*w0;
    xcoord_[12] = x0;
    ycoord_[12] = 1-x0;

    weight_[13] = w1*w0;
    xcoord_[13] = x1;
    ycoord_[13] = 1-x0;

    weight_[14] = w1*w0;
    xcoord_[14] = 1-x1;
    ycoord_[14] = 1-x0;

    weight_[15] = w0*w0;
    xcoord_[15] = 1-x0;
    ycoord_[15] = 1-x0;
  }
#endif
}


template <>
QuadratureArea<Quad>::~QuadratureArea()
{
  delete [] ycoord_;
  delete [] xcoord_;
  delete [] weight_;
  nQuad_ = 0;
}


template <>
void
QuadratureArea<Quad>::weight( int n, Real& wght ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  wght = weight_[n];
}


template <>
void
QuadratureArea<Quad>::coordinates( int n, Real xy[] ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  xy[0] = xcoord_[n];
  xy[1] = ycoord_[n];
}


template <>
void
QuadratureArea<Quad>::coordinates( int n, Real& x, Real& y ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  x = xcoord_[n];
  y = ycoord_[n];
}

template <>
Real
QuadratureArea<Quad>::weight( int n ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  return weight_[n];
}


template <>
SANS::DLA::VectorS<2,Real>
QuadratureArea<Quad>::coordinates( int n ) const
{
  SANS_ASSERT ((n >= 0) && (n < nQuad_));
  return {xcoord_[n], ycoord_[n]};
}

template <>
SANS::QuadraturePoint<TopoD2>
QuadratureArea<Quad>::coordinates_cache( int n ) const
{
  return SANS::QuadraturePoint<TopoD2>(SANS::QuadratureRule::eGauss, n, orderidx_, {xcoord_[n], ycoord_[n]});
}


template <>
void
QuadratureArea<Quad>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "QuadratureArea<Quad>: total points = " << nQuad_ << std::endl;
  if (weight_ != 0)
  {
    out << indent << "  weights = ";
    for (int k = 0; k < nQuad_; k++)
      out << weight_[k] << " ";
    out << std::endl;
  }
  if (xcoord_ != 0)
  {
    out << indent << "  xcoords = ";
    for (int k = 0; k < nQuad_; k++)
      out << xcoord_[k] << " ";
    out << std::endl;
  }
  if (ycoord_ != 0)
  {
    out << indent << "  ycoords = ";
    for (int k = 0; k < nQuad_; k++)
      out << ycoord_[k] << " ";
    out << std::endl;
  }
}


// I/O
template <>
std::ostream&
operator<<( std::ostream& out, const QuadratureArea<Quad>& quadrature )
{
  int nquad = quadrature.nQuadrature();
  Real w, xy[2];
  out << "QuadratureArea<Quad> " << nquad << ": ";
  for (int k = 0; k < nquad; k++)
  {
    quadrature.weight(k, w);
    quadrature.coordinates(k, xy);
    out << "(" << w << ", " << xy[0] <<  ", " << xy[1] <<") ";
  }
  return out;
}

} // namespace SANS
