// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QUADRATURENODE_H
#define QUADRATURENODE_H

// quadrature rules for node segment

#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/ElementTopology.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "BasisFunction/Quadrature_Cache.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// QuadratureLine:  quadrature rules for line segment; x in [0, 1]
//
// member functions:
//   .order                   order of quadrature rule (max degree polynomial)
//   .nQuadrature             total quadrature points
//   .weight                  weight at given quadrature point; zero-based indexing
//   .coordinate              coordinate (x in [0, 1]) at given quadrature point; zero-based indexing
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//

class QuadratureNode
{
public:
  explicit QuadratureNode( int orderReqd ) { SANS_ASSERT( orderReqd <= 0 ); }
  QuadratureNode(const QuadratureNode&) = delete;
  ~QuadratureNode() {}

  int order() const { return 0; }
  int nQuadrature() const { return 1; }

  void weight( int n, Real& wght ) const { wght = 1;}
  void coordinate( int, Real& coord ) const { coord = 0; }
  void coordinates( int n, SANS::DLA::VectorS<1,Real>& coord ) const { coordinate(n, coord[0]); }

  Real weight( int n ) const;
  Real coordinate( int ) const;
  SANS::QuadraturePoint<TopoD0> coordinates_cache( int n ) const { return SANS::QuadraturePoint<TopoD0>(SANS::QuadratureRule::eNone, n, 0, 0.); }

  void dump( int indentSize, std::ostream& out = std::cout ) const;
};

// I/O
std::ostream&
operator<<( std::ostream& out, const QuadratureNode& quadrature );

} // namespace SANS

#endif  // QUADRATURENODE_H
