// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QUADRATURE_H
#define QUADRATURE_H

#include "Topology/Dimension.h"
#include "Topology/ElementTopology.h"
#include "QuadratureNode.h"
#include "QuadratureLine.h"
#include "QuadratureArea.h"
#include "QuadratureVolume.h"
#include "QuadratureSpacetime.h"

namespace SANS
{

template<class TopoDim, class Topology>
class Quadrature;

template<>
class Quadrature<TopoD0, Node> : public QuadratureNode
{
public:
  explicit Quadrature( const int orderReqd ) : QuadratureNode(orderReqd) {}
  Quadrature( const Quadrature& ) = delete;
  Quadrature& operator= ( const Quadrature& ) = delete;
};

template<>
class Quadrature<TopoD1, Line> : public QuadratureLine
{
public:
  explicit Quadrature( const int orderReqd ) : QuadratureLine(orderReqd) {}
  Quadrature( const Quadrature& ) = delete;
  Quadrature& operator= ( const Quadrature& ) = delete;
};

template<class Topology>
class Quadrature<TopoD2, Topology> : public QuadratureArea<Topology>
{
public:
  explicit Quadrature( const int orderReqd ) : QuadratureArea<Topology>(orderReqd) {}
  Quadrature( const Quadrature& ) = delete;
  Quadrature& operator= ( const Quadrature& ) = delete;
};

template<class Topology>
class Quadrature<TopoD3, Topology> : public QuadratureVolume<Topology>
{
public:
  explicit Quadrature( const int orderReqd ) : QuadratureVolume<Topology>(orderReqd) {}
  Quadrature( const Quadrature& ) = delete;
  Quadrature& operator= ( const Quadrature& ) = delete;
};

template<class Topology>
class Quadrature<TopoD4, Topology> : public QuadratureSpacetime<Topology>
{
public:
  explicit Quadrature( const int orderReqd ) : QuadratureSpacetime<Topology>(orderReqd) {}
  Quadrature( const Quadrature& ) = delete;
  Quadrature& operator= ( const Quadrature& ) = delete;
};

}

#endif //QUADRATURE_H
