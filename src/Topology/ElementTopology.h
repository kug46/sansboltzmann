// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ELEMENTTOPOLOGY_H
#define ELEMENTTOPOLOGY_H

#include "tools/SANSnumerics.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Type.h"
#include "Dimension.h"

namespace SANS
{

// enum of topologies, useful for grid construction
enum TopologyTypes
{
  eNode,
  eLine,
  eTriangle,
  eQuad,
  eTet,
  eHex,
  ePentatope
};

int topoDim( const TopologyTypes& topo );
int topoNNode( const TopologyTypes& topo );

class TopologyNull;

// 0D element topology

class Node
{
public:
  typedef TopoD0 TopoDim;
  typedef TopoD1 CellTopoDim;
  typedef Node TopologyTrace; // the bottom of the recursion
  static const TopologyTypes Topology = eNode;
  static const int NEdge = 0;
  static const int NNode = 1;
  static const int NTrace = NNode;
  static const SANS::DLA::VectorS<1, Real> centerRef;
  static const int NPermutation = 1;
};

// 1D element topology

class Line
{
public:
  typedef TopoD1 TopoDim;
  typedef TopoD2 CellTopoDim;
  typedef Node TopologyTrace;
  static const TopologyTypes Topology = eLine;
  static const int NEdge = 1;
  static const int NNode = 2;
  static const int NTrace = NNode;
  static const SANS::DLA::VectorS<1, Real> centerRef;
  static const int NPermutation = 1;
};

// 2D element topology

class Triangle
{
public:
  typedef TopoD2 TopoDim;
  typedef TopoD3 CellTopoDim;
  typedef Line TopologyTrace;
  static const TopologyTypes Topology = eTriangle;
  static const int NEdge = 3;
  static const int NNode = 3;
  static const int NTrace = NEdge;
  static const Real areaRef;
  static const SANS::DLA::VectorS<2, Real> centerRef;
  static const int NPermutation = 3;
};

class Quad
{
public:
  typedef TopoD2 TopoDim;
  typedef TopoD3 CellTopoDim;
  typedef Line TopologyTrace;
  static const TopologyTypes Topology = eQuad;
  static const int NEdge = 4;
  static const int NNode = 4;
  static const int NTrace = NEdge;
  static const Real areaRef;
  static const SANS::DLA::VectorS<2, Real> centerRef;
  static const int NPermutation = 4;
};

// 3D element topology

class Tet
{
public:
  typedef TopoD3 TopoDim;
  typedef TopoD4 CellTopoDim;
  typedef Triangle TopologyTrace;
  static const TopologyTypes Topology = eTet;
  static const int NFace = 4;
  static const int NEdge = 6;
  static const int NNode = 4;
  static const int NTrace = NFace;
  static const Real volumeRef;
  static const SANS::DLA::VectorS<3, Real> centerRef;
  static const int NPermutation = 12;
};

class Hex
{
public:
  typedef TopoD3 TopoDim;
  typedef TopoD4 CellTopoDim;
  typedef Quad TopologyTrace;
  static const TopologyTypes Topology = eHex;
  static const int NFace = 6;
  static const int NEdge = 12;
  static const int NNode = 8;
  static const int NTrace = NFace;
  static const Real volumeRef;
  static const SANS::DLA::VectorS<3, Real> centerRef;
};

class Pentatope
{
public:
  typedef TopoD4 TopoDim;
  typedef TopoD4 CellTopoDim; // the top of the recursion
  typedef Tet TopologyTrace;
  typedef Triangle TopologyFrame;
  static const TopologyTypes Topology = ePentatope;
  static const int NFace = 5;
  static const int NEdge = 10;
  static const int NTriangle = 10;
  static const int NNode = 5;
  static const int NTrace = NFace;
  static const int NFrame = NTriangle;
  static const Real volumeRef;
  static const SANS::DLA::VectorS<4, Real> centerRef;
};

template < class TopoDim >
struct Simplex;

template<> struct Simplex<TopoD1> { typedef Line type; };
template<> struct Simplex<TopoD2> { typedef Triangle type; };
template<> struct Simplex<TopoD3> { typedef Tet type; };
template<> struct Simplex<TopoD4> { typedef Pentatope type; };

} // namespace SANS

#endif    // ELEMENTTOPOLOGY_H
