// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "ElementTopology.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

namespace SANS
{

const DLA::VectorS<1, Real> Node::centerRef = {0.};


const DLA::VectorS<1, Real> Line::centerRef = {1./2.};


const Real Triangle::areaRef = 0.5;
const DLA::VectorS<2, Real> Triangle::centerRef = {1./3., 1./3.};

const Real Quad::areaRef = 1;
const DLA::VectorS<2, Real> Quad::centerRef = {1./2., 1./2.};


const Real Tet::volumeRef = 1./6.;
const DLA::VectorS<3, Real> Tet::centerRef = {1./4., 1./4., 1./4.};

const Real Hex::volumeRef = 1.;
const DLA::VectorS<3, Real> Hex::centerRef = {1./2., 1./2., 1./2.};


const Real Pentatope::volumeRef = 1./24.;
const DLA::VectorS<4, Real> Pentatope::centerRef = {1./5.,1./5.,1./5.,1./5.};

// Instantiate
const TopologyTypes Node::Topology;
const TopologyTypes Line::Topology;
const TopologyTypes Triangle::Topology;
const TopologyTypes Quad::Topology;
const TopologyTypes Tet::Topology;
const TopologyTypes Hex::Topology;
const TopologyTypes Pentatope::Topology;

int topoDim( const TopologyTypes& topo )
{
  switch (topo)
  {
  case eNode:
    return Node::TopoDim::D;
  case eLine:
    return Line::TopoDim::D;
  case eTriangle:
    return Triangle::TopoDim::D;
  case eQuad:
    return Quad::TopoDim::D;
  case eTet:
    return Tet::TopoDim::D;
  case eHex:
    return Hex::TopoDim::D;
  case ePentatope:
    return Pentatope::TopoDim::D;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown topology: %d", topo);
  }
  return 0;
}

int topoNNode( const TopologyTypes& topo )
{
  switch (topo)
  {
  case eNode:
    return Node::NNode;
  case eLine:
    return Line::NNode;
  case eTriangle:
    return Triangle::NNode;
  case eQuad:
    return Quad::NNode;
  case eTet:
    return Tet::NNode;
  case eHex:
    return Hex::NNode;
  case ePentatope:
    return Pentatope::NNode;
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown topology: %d", topo);
  }
  return 0;
}

} // namespace SANS
