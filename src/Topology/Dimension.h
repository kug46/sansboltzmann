// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef DIMENSION_H
#define DIMENSION_H

struct PhysD0 { static const int D = 0; };
struct PhysD1 { static const int D = 1; };
struct PhysD2 { static const int D = 2; };
struct PhysD3 { static const int D = 3; };
struct PhysD4 { static const int D = 4; };

struct TopoD0 { static const int D = 0; };
struct TopoD1 { static const int D = 1; typedef TopoD0 TopoDTrace; };
struct TopoD2 { static const int D = 2; typedef TopoD1 TopoDTrace; };
struct TopoD3 { static const int D = 3; typedef TopoD2 TopoDTrace; };
struct TopoD4 { static const int D = 4; typedef TopoD3 TopoDTrace; };

#endif    // DIMENSION_H
