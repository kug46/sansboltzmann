// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCCATEGORY_H
#define BCCATEGORY_H

#include <type_traits>
#include <typeinfo>

#include <boost/mpl/bool.hpp>
#include <boost/mpl/count_if.hpp>
#include <boost/mpl/size.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/remove_if.hpp>

#include "tools/SANSException.h"

#include "pde/NDConvert/BCNDConvert_fwd.h"

namespace SANS
{
//---------------------------------------------------------------------------//
//
// Tags used to indicate the BC category
//
//---------------------------------------------------------------------------//
struct BCCategory
{
  struct Dirichlet_mitLG {};
  struct Dirichlet_sansLG {};
  struct LinearScalar_mitLG {};
  struct LinearScalar_sansLG {};
  struct Flux_mitState {};
  struct None {};

  struct LinearScalar_sansLG_FP_HACK {};
  struct WakeCut_Potential_Drela {};
  struct WakeCut_Potential_Adjoint {};

  struct IPTF_sansLG {};    // two-field incompressible potential (wall, farfield, wake-cut)

  // Generalize this category at some point...
  struct HubTrace {};
};

typedef boost::mpl::vector<
    BCCategory::Dirichlet_mitLG,
    BCCategory::Dirichlet_sansLG,
    BCCategory::LinearScalar_mitLG,
    BCCategory::LinearScalar_sansLG,
    BCCategory::Flux_mitState,
    BCCategory::None,

    BCCategory::LinearScalar_sansLG_FP_HACK,
    BCCategory::WakeCut_Potential_Drela,

    BCCategory::IPTF_sansLG,

    BCCategory::HubTrace
    > BCCategoryVector;

template<class Category>
struct CategoryNeedsFieldTrace;

// Create the specializations to indicate which categories require lagrange multipliers
template<> struct CategoryNeedsFieldTrace<BCCategory::Dirichlet_mitLG    > : boost::mpl::bool_<true> {};
template<> struct CategoryNeedsFieldTrace<BCCategory::Dirichlet_sansLG   > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsFieldTrace<BCCategory::LinearScalar_mitLG > : boost::mpl::bool_<true> {};
template<> struct CategoryNeedsFieldTrace<BCCategory::LinearScalar_sansLG> : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsFieldTrace<BCCategory::Flux_mitState      > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsFieldTrace<BCCategory::None               > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsFieldTrace<BCCategory::HubTrace           > : boost::mpl::bool_<false> {};

template<> struct CategoryNeedsFieldTrace<BCCategory::LinearScalar_sansLG_FP_HACK> : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsFieldTrace<BCCategory::WakeCut_Potential_Drela> : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsFieldTrace<BCCategory::WakeCut_Potential_Adjoint> : boost::mpl::bool_<false> {};

template<> struct CategoryNeedsFieldTrace<BCCategory::IPTF_sansLG> : boost::mpl::bool_<false> {};


template<class Category>
struct CategoryNeedsHubTrace;

// Create the specializations to indicate which categories require hub trace field
template<> struct CategoryNeedsHubTrace<BCCategory::Dirichlet_mitLG    > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsHubTrace<BCCategory::Dirichlet_sansLG   > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsHubTrace<BCCategory::LinearScalar_mitLG > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsHubTrace<BCCategory::LinearScalar_sansLG> : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsHubTrace<BCCategory::Flux_mitState      > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsHubTrace<BCCategory::None               > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsHubTrace<BCCategory::HubTrace           > : boost::mpl::bool_<true> {};

template<> struct CategoryNeedsHubTrace<BCCategory::LinearScalar_sansLG_FP_HACK> : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsHubTrace<BCCategory::WakeCut_Potential_Drela> : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsHubTrace<BCCategory::WakeCut_Potential_Adjoint> : boost::mpl::bool_<false> {};

template<> struct CategoryNeedsHubTrace<BCCategory::IPTF_sansLG> : boost::mpl::bool_<false> {};

// Template meta function to extract the category from a type
template<class BC>
struct getCategory
{
  typedef typename BC::Category type;
};

// A container to storing a BCVector-Category pair, where all BCs in the vector have the same category
template<class NDVector_, class Category_>
struct NDVectorCategory
{
  typedef Category_ Category;
  typedef NDVector_ NDVector;

  static_assert( boost::mpl::size<NDVector>::value > 0, "Should not be a zero length vector" );

  // check that all BCs in the vector have the same category
  static_assert( (boost::mpl::count_if< NDVector, std::is_same< getCategory<boost::mpl::_1>, Category > >::value) == boost::mpl::size<NDVector>::value
                 , "All types should be of the same category");
};

//===========================================================================//

// Tag to indicate that a boundary condition is actually a space time boundary condition
template<class BC>
struct SpaceTimeBC : public BC
{
  SpaceTimeBC();
  SpaceTimeBC(const SpaceTimeBC&) = delete;
  SpaceTimeBC& operator=(const SpaceTimeBC&) = delete;
};

template<template<class,class> class decorator, class BCVector_>
struct BCVectorSanitizeSpaceTime
{
  typedef BCVector_ type;
};

// Template meta function to determine if a BC type is a SpaceTimeBC
template<class BC> struct IsSpaceTimeBC                  : boost::mpl::bool_<false> {};
template<class BC> struct IsSpaceTimeBC<SpaceTimeBC<BC>> : boost::mpl::bool_<true> {};

template<class BCVector_>
struct BCVectorSanitizeSpaceTime<BCNDConvertSpace, BCVector_>
{
  // Remove the SpaceTime BCs for purely Spatial calculations
  typedef typename boost::mpl::remove_if< BCVector_,
                                          IsSpaceTimeBC< boost::mpl::_1 >
                                        >::type type;
};

//===========================================================================//

class BCBase
{
public:
  virtual ~BCBase() {}

  virtual const std::type_info& derivedTypeID() const = 0;

  // Safe methods for casting the BC type
  template<class BC>
  BC& cast()
  {
    SANS_ASSERT( typeid(BC) == derivedTypeID() );
    return static_cast<BC&>(*this);
  }

  template<class BC>
  const BC& cast() const
  {
    SANS_ASSERT( typeid(BC) == derivedTypeID() );
    return static_cast<const BC&>(*this);
  }
};


template<class Derived>
struct BCType : public BCBase
{
  virtual const std::type_info& derivedTypeID() const override
  {
    static_assert( std::is_base_of<BCType<Derived>, Derived>::value, "This is usually a copy/paste mistake" );

    return typeid(Derived);
  }

  virtual ~BCType() {}

  // A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

}

#endif //BCCATEGORY_H
