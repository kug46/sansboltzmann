// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTCELL_SOLUTIONSQUARED_H
#define OUTPUTCELL_SOLUTIONSQUARED_H

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Pow.h"

#include "OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand: solution

template <class PDE_>
class OutputCell_SolutionSquared : public OutputType< OutputCell_SolutionSquared<PDE_> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;

  //Array of solution variables
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  // Array of outputs
  template<class T>
  using ArrayJ = typename PDE::template ArrayQ<T>;

  // Matrix required to represent the Jacobian of this output functional
  template<class T>
  using MatrixJ = typename PDE::template MatrixQ<T>;

  explicit OutputCell_SolutionSquared() {}
  explicit OutputCell_SolutionSquared( const PyDict& d ) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    output = pow(q, 2.0);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    output = pow(q, 2.0);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    output = pow(q, 2.0);
  }


  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, MatrixJ<To>& dJdu) const
  {
    dJdu = 2.0*q;
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {

    dJdu = 2.0*q;
  }


  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& z, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                      MatrixJ<To>& dJdu) const
  {

    dJdu = 2.0*q;
  }

};

}

#endif //INTEGRANDCELL_SOLUTION_H
