// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTCELL_SOLUTIONERROR_H
#define OUTPUTCELL_SOLUTIONERROR_H

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real

#include "OutputCategory.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Pow.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand: solution error

template <class PDE_, class SolutionFunction>
class OutputCell_SolutionError :
    public OutputType< OutputCell_SolutionError<PDE_, SolutionFunction> >
{
public:
  typedef PDE_ PDE;
  typedef typename PDE::PhysDim PhysDim;
  static const int D = PhysDim::D;

  //Array of solution variables
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  // Array of outputs
  template<class T>
  using ArrayJ = typename PDE::template ArrayQ<T>;

  // Matrix required to represent the Jacobian of this output functional
  template<class T>
  using MatrixJ = typename PDE::template MatrixQ<T>;

  explicit OutputCell_SolutionError( const SolutionFunction& solFcn ) : solFcn_(solFcn) {}
  explicit OutputCell_SolutionError( const PyDict& d ) : solFcn_(d) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact = solFcn_(x, time);
    output = fabs(q - qExact);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact = solFcn_(x, y, time);
    output = fabs(q - qExact);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    ArrayQ<Real> qExact = solFcn_(x, y, z, time);
    output = fabs(q - qExact);
  }


  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, MatrixJ<To>& dJdu) const
  {
    SANS_DEVELOPER_EXCEPTION("OUTPUT_SOLUTIONERROR OUTPUT JACOBIAN NOT IMPLEMENTED");
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    SANS_DEVELOPER_EXCEPTION("OUTPUT_SOLUTIONERROR OUTPUT JACOBIAN NOT IMPLEMENTED");
  }


  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& z, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                      MatrixJ<To>& dJdu) const
  {
    SANS_DEVELOPER_EXCEPTION("OUTPUT_SOLUTIONERROR OUTPUT JACOBIAN NOT IMPLEMENTED");
  }

protected:
  const SolutionFunction& solFcn_;
};

}

#endif //OUTPUTCELL_SOLUTIONERRORSQUARED_H
