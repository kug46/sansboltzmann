// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_SETIBLOUTPUTCELLGROUP_H_
#define SRC_PDE_IBL_SETIBLOUTPUTCELLGROUP_H_

#include "Field/FieldLine_DG_Cell.h"
#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

#include "ElementProjectionIBLoutput_L2.h"

namespace SANS
{

template <class VarType>
class SetIBLoutputCellGroup_impl:
    public GroupFunctorCellType< SetIBLoutputCellGroup_impl<VarType> >
{
public:
  typedef ElementProjectionIBLoutput_L2<VarType> ElementProjectionType;

  typedef typename ElementProjectionType::PhysDim PhysDim;
  typedef typename ElementProjectionType::PDEClass PDEClass;

  template <class T>
  using ArrayQ = typename ElementProjectionType::template ArrayQ<T>;

  template <class T>
  using ArrayParam = typename ElementProjectionType::template ArrayParam<T>;

  template <class T>
  using ArrayOutput = typename ElementProjectionType::template ArrayOutput<T>;

  template <class T>
  using QFieldType = Field_DG_Cell<PhysDim,TopoD1,ArrayQ<T>>;

  template <class T>
  using ParamFieldType = Field_DG_Cell<PhysDim,TopoD1,ArrayParam<T>>;

  template <class T>
  using OutputFieldType = Field_DG_Cell<PhysDim,TopoD1,ArrayOutput<T>>;

  // ctor
  SetIBLoutputCellGroup_impl(const PDEClass& pde,
                             const Real& Vinf,
                             const QFieldType<Real>& qfld,
                             const ParamFieldType<Real>& paramfld,
                             const std::vector<int>& cellGroups,
                             const int quadratureOrder) :
    pde_(pde),
    Vinf_(Vinf),
    qfld_(qfld),
    paramfld_(paramfld),
    cellGroups_(cellGroups),
    quadratureOrder_( quadratureOrder ) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // generic functor to set cell group DOFs
  template <class Topology>
  void
  apply(const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>, OutputFieldType<Real>, TupleClass<>>
                        ::template FieldCellGroupType<Topology>& fldsCell,
        const int cellGroupGlobal)
  {
    SANS_ASSERT_MSG( ((std::is_same<TopoD1,typename Topology::TopoDim>::value) && (std::is_same<Line,Topology>::value)),
                     "Only implemented for line grids");

    typedef typename XField<PhysDim,typename Topology::TopoDim>                 ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim,typename Topology::TopoDim, ArrayOutput<Real>>::template FieldCellGroupType<Topology> OutputFieldCellGroupType;
    typedef typename Field<PhysDim,typename Topology::TopoDim, ArrayQ<Real>>::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field<PhysDim,typename Topology::TopoDim, ArrayParam<Real>>::template FieldCellGroupType<Topology> ParamFieldCellGroupType;

    typedef typename XFieldCellGroupType    ::template ElementType<> ElementXFieldClass;
    typedef typename OutputFieldCellGroupType::template ElementType<> ElementOutputFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
    OutputFieldCellGroupType& outputfldCell = const_cast<OutputFieldCellGroupType&>( get<1>(fldsCell) );

    const QFieldCellGroupType& qfldCell = qfld_.template getCellGroupGlobal<Line>(cellGroupGlobal);
    const ParamFieldCellGroupType& paramfldCell = paramfld_.template getCellGroupGlobal<Line>(cellGroupGlobal);

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementOutputFieldClass outputfldElem( outputfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementParamFieldClass paramfldElem( paramfldCell.basis() );

    // loop over elements within group
    const int nelem = xfldCell.nElem();

    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      paramfldCell.getElement( paramfldElem, elem );

      const ElementProjectionType elemProjectOutput(outputfldElem.basis(), quadratureOrder_);
      elemProjectOutput.project(pde_, Vinf_, qfldElem, paramfldElem, xfldElem, outputfldElem);

      // set the projected solution
      outputfldCell.setElement( outputfldElem, elem );
    }
  }

protected:
  const PDEClass& pde_;
  const Real Vinf_;
  const QFieldType<Real>& qfld_;
  const ParamFieldType<Real>& paramfld_;
  const std::vector<int> cellGroups_;
  const int quadratureOrder_; // quadrature order for elemental projection onto Qauxv
};


// Factory function

template <class VarType>
SetIBLoutputCellGroup_impl<VarType>
SetIBLoutputCellGroup(const typename SetIBLoutputCellGroup_impl<VarType>::PDEClass& pde,
                      const Real& Vinf,
                      const typename SetIBLoutputCellGroup_impl<VarType>::template QFieldType<Real>& qfld,
                      const typename SetIBLoutputCellGroup_impl<VarType>::template ParamFieldType<Real>& paramfld,
                      const std::vector<int>& cellGroups,
                      const int quadratureOrder )
{
  return SetIBLoutputCellGroup_impl<VarType>(pde, Vinf, qfld, paramfld, cellGroups, quadratureOrder);
}

} // namespace SANS

#endif /* SRC_PDE_IBL_SETIBLOUTPUTCELLGROUP_H_ */
