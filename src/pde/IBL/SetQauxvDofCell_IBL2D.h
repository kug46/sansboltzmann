// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_SETQAUXVDOFCELL_IBL2D_H_
#define SRC_PDE_IBL_SETQAUXVDOFCELL_IBL2D_H_

#include "VarInterpret2D.h"

#include <fstream>
#include <sstream>

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

#include "QauxvInterpret2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Assign nodal DOFs of an IBL2D solution field

//----------------------------------------------------------------------------//
class SetQauxvDofCell_IBL2D_impl :
    public GroupFunctorCellType< SetQauxvDofCell_IBL2D_impl >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename QauxvInterpret<PhysDim>::type QauxvInterpretType;
  typedef typename QauxvInterpretType::ArrayQ<Real> ArrayQ;

  SetQauxvDofCell_IBL2D_impl( const std::vector<int>& cellGroups,
                              const int order,
                              const std::string& filename ) :
    qauxvInterpret_(),
    cellGroups_(cellGroups),
    order_(order)
  {
//    SANS_ASSERT( (int)cellGroups.size() == 1); // one cell group at a time
    SANS_ASSERT( order_==0 || order_==1 ); // TODO: only these two orders are currently supported

    // read in file
    std::ifstream datafile(filename);
    int nline = 0;

    // line-by-line data parsing
    if (datafile.is_open())
    {
      std::string line;
      while (std::getline(datafile,line))
      {
        std::istringstream dataline(line);
        Real qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0;
        if ( dataline >> qx >> qz >> qx_x >> qx_z >> qz_x >> qz_z >> p0 >> T0 )
        {
          const ArrayQ qauxv = qauxvInterpret_.setDOFFrom(qx, qz, qx_x, qx_z, qz_x, qz_z, p0, T0);
          qdata_vec_.push_back(qauxv);
          nline++;
        }
        else
        {
          SANS_DEVELOPER_EXCEPTION( "Line %d of data is corrupted: data = %s",
                                    nline, dataline.str().c_str() );
        }
      }
      datafile.close();

      SANS_ASSERT_MSG( nline == (int) qdata_vec_.size(), "nline = %d", "data.size() = %d", nline, (int) qdata_vec_.size() );
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "Error: the data file \"%s\" cannot be opened.", filename.c_str() );
    }
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
#if 0
  // Assign nodal DOFs to the solution field
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>,
                                   Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>
                        ::template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    SANS_ASSERT( (int)cellGroups_.size() == 1); // one cell group at a time

    typedef typename Topology::TopoDim TopoDim;
    typedef typename XField<PhysDim, TopoDim>       ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
          QFieldCellGroupType& qfldCell = const_cast<QFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    SANS_ASSERT_MSG( qfldCell.order() == order_, "Solution order does not match" );

    const int nDOFelem = qfldElem.nDOF();

    // loop over elements within group
    const int nelem = xfldCell.nElem();

    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/parameter DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // assign DOFs to parameter element field

      if (qfldCell.order() == 0) // piecewise constant solution
      {
        const int nodalIndxGlobal = elem; // Indexing nodal DOFs in the input data
        qfldElem.DOF(0) = 0.5*(qdata_vec_[nodalIndxGlobal] + qdata_vec_[nodalIndxGlobal+1]);
      }
      else if (qfldCell.order() == 1) // piecewise linear solution
      {
        SANS_ASSERT_MSG( qfldCell.basis()->category() == BasisFunctionCategory_Hierarchical,
                         "The following method assumes hierarchical basis function");

        for ( int i = 0; i < nDOFelem; i++ )
        {
          const int nodalIndxGlobal = elem + i; // Indexing nodal DOFs in the input data

            qfldElem.DOF(i) = qdata_vec_[nodalIndxGlobal];
        }
      }
      else
        SANS_DEVELOPER_EXCEPTION("Not implemented!");

      // set the projected solution
      qfldCell.setElement( qfldElem, elem );
    }
  }
#else
  std::vector<ArrayQ> getDOFvec() const
  {
    std::vector<ArrayQ> qauxv_vec(qdata_vec_);
    return qauxv_vec;
  }
#endif

protected:
  QauxvInterpretType qauxvInterpret_;
  const std::vector<int> cellGroups_;
  const int order_;
  std::vector<ArrayQ> qdata_vec_;
};


//----------------------------------------------------------------------------//
// Factory function
SetQauxvDofCell_IBL2D_impl
SetQauxvDofCell_IBL2D( const std::vector<int>& cellGroups, const int order,
                       const std::string& filename)
{
  return SetQauxvDofCell_IBL2D_impl(cellGroups, order, filename);
}

} //namespace SANS

#endif /* SRC_PDE_IBL_SETQAUXVDOFCELL_IBL2D_H_ */
