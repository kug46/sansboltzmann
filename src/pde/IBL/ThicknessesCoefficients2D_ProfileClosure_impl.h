// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(THICKNESSESCOEFFICIENTS2D_PROFILECLOSURE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "ThicknessesCoefficients2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::
getCalculatorThicknessesCoefficients(const IBLProfileCategory& profileCat, const ArrayParam<Tparam>& params, const ArrayQ<Tibl>& var,
                                     const Tparam& nue) const
{
  return Functor<Tparam, Tibl>(profileCat, params, var, nue,
                               nameCDclosure_, nquad_, w_, eta_, varInterpret_, paramInterpret_,
                               velocityProfile_, densityProfile_, viscosityProfile_);
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
Functor(const IBLProfileCategory& profileCat,
        const ArrayParam<Tparam>& params,
        const ArrayQ<Tibl>& var,
        const Tparam& nue,
        const std::string& nameCDclosure,
        const int& nquad,
        const std::vector<Real>& w,
        const std::vector<Real>& eta,
        const VarInterpType& varInterpret,
        const ParamInterpType& paramInterpret,
        const VelocityProfileType& velocityProfile,
        const DensityProfile2DType& densityProfile,
        const ViscosityProfile2DType& viscosityProfile) :
  profileCat_(profileCat),
  params_(params),
  var_(var),
  nue_(nue),
  nameCDclosure_(nameCDclosure),
  nquad_(nquad),
  w_(w),
  eta_(eta),
  varInterpret_(varInterpret),
  paramInterpret_(paramInterpret),
  velocityProfile_(velocityProfile),
  densityProfile_(densityProfile),
  viscosityProfile_(viscosityProfile),
  delta_(varInterpret_.getdelta(profileCat_, var_)),
  A_(varInterpret_.getA(profileCat_, var_)),
  ctau_(varInterpret_.getctau(var_)),
  Redelta_(paramInterpret_.getqe(params_)*delta_/nue_)
{
  // compute/cache velocity profile information
  switch (profileCat_)
  {
    case IBLProfileCategory::LaminarBL:
    case IBLProfileCategory::LaminarWake:
    {
      velocityInfo_ = std::map<VelocityInfoType, std::vector<Tout>>({{VelocityInfoType::U,std::vector<Tout>(nquad_)},
        {VelocityInfoType::dUdeta,std::vector<Tout>(nquad_)}});
      break;
    }
    case IBLProfileCategory::TurbulentBL:
    case IBLProfileCategory::TurbulentWake:
    {
      velocityInfo_ = std::map<VelocityInfoType, std::vector<Tout>>({{VelocityInfoType::U,std::vector<Tout>(nquad_)}});
      break;
    }
    default:
      SANS_DEVELOPER_EXCEPTION("Unknown Profile Category = %d", profileCat_);
  }

  velocityProfile_.velocityProfile(profileCat_, delta_, A_, Redelta_, eta_, velocityInfo_);
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getdelta1s() const
{
  Tout delta1s = 0.0;
  Tout U = 0.0, R = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    delta1s += w_[i] * (1 - R*U); // delta_1^* / delta
  }
  delta1s *= varInterpret_.getdelta(profileCat_, var_);
  return delta1s;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
gettheta11() const
{
  Tout theta11 = 0.0;
  Tout U = 0.0, R = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    theta11 += w_[i] * R*U*(1-U); // theta_11 / delta
  }
  theta11 *= varInterpret_.getdelta(profileCat_, var_);
  return theta11;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getH() const
{
  return getdelta1s() / gettheta11();
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getHk() const
{
  //TODO: compressibility to be accounted for later
  SANS_ASSERT_MSG((std::is_same<CompressibilityType, IncompressibleBL>::value),
                  "CompressibilityType = %s is required to be IncompressibleBL");
  return getH();
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getHs() const
{
  return gettheta1s() / gettheta11();
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
gettheta1s() const
{
  Tout theta1s = 0.0;
  Tout U = 0.0, R = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    theta1s += w_[i] * R*U*(1-pow(U,2.0)); // theta_1^* / delta
  }
  theta1s *= varInterpret_.getdelta(profileCat_, var_);
  return theta1s;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getdelta1p() const
{
  Tout delta1p = 0.0;
  Tout U = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);

    delta1p += w_[i] * (1-U); // delta_1^' / delta
  }
  delta1p *= varInterpret_.getdelta(profileCat_, var_);
  return delta1p;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getdeltarho() const
{
  Tout deltarho = 0.0;
  Tout U = 0.0, R = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    deltarho += w_[i] * (1 - R); // delta_rho / delta
  }
  deltarho *= varInterpret_.getdelta(profileCat_, var_);
  return deltarho;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
gettheta0s() const
{
  // Note: theta0s = theta11 + delta1s - deltarho
  Tout theta0s = 0.0;
  Tout U = 0.0, R = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    theta0s += w_[i] * R*(1 - pow(U,2.0)); // delta_q / delta
  }
  theta0s *= varInterpret_.getdelta(profileCat_, var_);
  return theta0s;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetCf1() const
{
  Tout RetCf1 = 0.0;

  switch (profileCat_)
  {
    case IBLProfileCategory::LaminarWake:
    case IBLProfileCategory::TurbulentWake:
    {
      RetCf1 = 0.0;
      break;
    }
    case IBLProfileCategory::LaminarBL:
    {
      std::vector<Real> eta0(1,0.0); // coordinate at wall
      std::map<VelocityInfoType,std::vector<Tout>> velocityInfoWall = {{VelocityInfoType::U,std::vector<Tout>(1)},
                                                                       {VelocityInfoType::dUdeta,std::vector<Tout>(1)}};
      velocityProfile_.velocityProfile(profileCat_, delta_, A_, Redelta_, eta0, velocityInfoWall);

      Tout U_w = velocityInfoWall[VelocityInfoType::U].at(0);
      Tout V_w = viscosityProfile_.viscosityProfile(U_w);

      Tout dUdeta_w = velocityInfoWall[VelocityInfoType::dUdeta].at(0);

      RetCf1 = 2.0*V_w*dUdeta_w * gettheta11()/delta_;
      break;
    }
    case IBLProfileCategory::TurbulentBL:
    {
      std::vector<Real> eta0(1,0.0); // coordinate at wall
      std::map<VelocityInfoType,std::vector<Tout>> velocityInfoWall = {{VelocityInfoType::U,std::vector<Tout>(1)}};
      velocityProfile_.velocityProfile(profileCat_, delta_, A_, Redelta_, eta0, velocityInfoWall);

      Tout U_w = velocityInfoWall[VelocityInfoType::U].at(0);
      Tout V_w = viscosityProfile_.viscosityProfile(U_w);

      RetCf1 = 2.0*V_w*A_  * gettheta11()/delta_;
      break;
    }
  }

  return RetCf1;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetCD() const
{
  Tout RetCD = 0.0;

  switch (profileCat_)
  {
    case IBLProfileCategory::LaminarBL:
    case IBLProfileCategory::LaminarWake:
    {
      Tout dUdeta = 0.0, U =0.0, V = 0.0;

      for (int i = 0; i < nquad_; ++i)
      {
        dUdeta = velocityInfo_[VelocityInfoType::dUdeta].at(i);
        U = velocityInfo_[VelocityInfoType::U].at(i);
        V = viscosityProfile_.viscosityProfile(U);
        RetCD += w_[i] * (V*pow(dUdeta,2.0)) * gettheta11()/delta_;
      }

      if (profileCat_==IBLProfileCategory::LaminarWake)
        RetCD *= 4.0; // correction factor accounts for two halves of the wake

      break;
    }
    case IBLProfileCategory::TurbulentBL:
    {
      // todo: MERGE turb bl and wake?

      // TODO: here, the quadrature is carried out again, which is a bit redundant, but seems the optimal as of now
      Tout delta1s = getdelta1s();
      Tout theta11 = gettheta11();
      Tout theta1s = gettheta1s();

      Tout RetCf1 = getRetCf1();

      // compute shape parameters
      Tout H  = delta1s/theta11;
      Tout Hs = theta1s/theta11;

      const Tout Ret = Redelta_ * gettheta11()/delta_;

      if (nameCDclosure_ == ClassParamsType::params.nameCDclosure.ibl3eqm)
      {
        Tout GH = (H-1.0)/H;

        Tout GA = GH/A_GbetaLocus_;
        Tout GB = GH/B_GbetaLocus_;

        RetCD = 0.5*Hs * ( 0.5*RetCf1*(1-GB) + Ret*GB*pow(GA,2.0) ); // equilibrium flow value
      }
      else if (nameCDclosure_ == ClassParamsType::params.nameCDclosure.ibl3lag)
      {
        Tout Us = 0.5*(-1.0 + 3.0/H);
        RetCD = 0.5*(0.5*RetCf1 + Ret*ctau_)*Us + Ret*ctau_*(1.0 - Us);
      }
      else if (nameCDclosure_ == ClassParamsType::params.nameCDclosure.xfoillag)
      {
        Tout GH = (H-1.0)/H;

        Tout Us = 0.5*Hs*(1-GH/B_GbetaLocus_);
        RetCD = 0.5*RetCf1*Us + Ret*ctau_*(0.995-Us);

        // Add laminar stress contribution to outer layer CD.  XFOIL v6.99, xblsys.f ~Line 1027
        RetCD += 0.15*pow(0.995-Us, 2.0);
      }

      break;
    }
    case IBLProfileCategory::TurbulentWake:
    {
      // TODO: here, the quadrature is carried out again, which is a bit redundant, but seems the optimal as of now
      Tout delta1s = getdelta1s();
      Tout theta11 = gettheta11();
      Tout theta1s = gettheta1s();

      // compute shape parameters
      Tout H  = delta1s/theta11;
      Tout Hs = theta1s/theta11;

      const Tout Ret = Redelta_ * gettheta11()/delta_;

      if (nameCDclosure_ == ClassParamsType::params.nameCDclosure.ibl3eqm)
      {
        Tout GH = (H-1.0)/H;
        Tout GA = GH/A_GbetaLocus_;
        Tout GB = GH/B_GbetaLocus_;
        RetCD = 0.5*Hs * Ret * GB*pow(GA,2.0);
      }
      else if (nameCDclosure_ == ClassParamsType::params.nameCDclosure.ibl3lag)
      {
        Tout Us = 0.5*(-1.0 + 3.0/H);
        RetCD = Ret*ctau_*(1.0 - Us);
      }
      else if (nameCDclosure_ == ClassParamsType::params.nameCDclosure.xfoillag)
      {
        Tout GH = (H-1.0)/H;
        Tout Us = 0.5*Hs*(1-GH/B_GbetaLocus_);
        RetCD = Ret*ctau_*(0.995-Us);

        // Add laminar stress contribution to outer layer CD
        RetCD += 0.15*pow(0.995-Us, 2.0);
      }

      // Note that Cf1 or the inner layer term is omitted here since it vanishes in the wake

      RetCD *= 2.0; // the factor 2.0 accounts for double layers in the wake.
      break;
    }
  }

  return RetCD;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getctaueq() const
{
  // TODO: here, the quadrature is carried out again, which is a bit redundant, but seems the optimal as of now
  const Tout delta1s = getdelta1s();
  const Tout theta11 = gettheta11();
  const Tout theta1s = gettheta1s();

  // compute shape parameters
  const Tout H  = delta1s/theta11;
  const Tout Hs = theta1s/theta11;
  const Tout GH = (H-1.0)/H;

  Tout ctaueq = 0.0;

  if (nameCDclosure_ == ClassParamsType::params.nameCDclosure.ibl3eqm ||
      nameCDclosure_ == ClassParamsType::params.nameCDclosure.ibl3lag )
  {
    const Tout Us = 0.5*(-1.0 + 3.0/H);

    ctaueq = Hs / (2.0-Us) * pow(GH,3.0) / (B_GbetaLocus_*pow(A_GbetaLocus_,2.0));
  }
  else if (nameCDclosure_ == ClassParamsType::params.nameCDclosure.xfoillag)
  {
    const Tout Us = 0.5*Hs*(1-GH/B_GbetaLocus_);

    // TODO: the following quantity can be pre-computed
    const Real CTCON = 0.5 / (pow(A_GbetaLocus_,2.0) * B_GbetaLocus_); // ~0.015 for A = 6.7 and B = 0.75
    // Ref: equation (24) in (MSES,1987); and also in XFOIL v6.99, xbl.f ~Line 1592

    ctaueq = Hs * CTCON/(1.0-Us) * pow(GH,3.0);
  }

#if 0 // Based on (Drela, 1989, blunt trailing edge IBL) not used by XFOIL though
  if (profileCat_ == VelocityProfileType::TurbulentWake)
    ctaueq *= 4.0;
#endif

  return ctaueq;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getUslip(const Tout& Hk, const Tout& H, const Tout& Hs) const
{
  Tout Us = 0.5*Hs*(1.0-(Hk-1.0)/(H*B_GbetaLocus_));

#if ISUSEXFOIL_CORRELATIONCLOSURE && 0 // The following are tweaks in XFOIL v6.99
  // set upper limit
  if (profileCat_ == IBLProfileCategory::LaminarBL || profileCat_ == IBLProfileCategory::TurbulentBL)
    Us = min(Us, 0.98); // TODO: to be smoothened
  else if (profileCat_ == IBLProfileCategory::TurbulentWake)
    Us = min(Us, 0.99995); // TODO: to be smoothened
#endif

  return Us;
}

} // namespace SANS
