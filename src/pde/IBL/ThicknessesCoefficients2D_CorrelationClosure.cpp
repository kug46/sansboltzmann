// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define THICKNESSESCOEFFICIENTS2D_CORRELATIONCLOSURE_INSTANTIATE
#include "ThicknessesCoefficients2D_CorrelationClosure_impl.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

#include "Surreal/SurrealS.h"

namespace SANS
{
PYDICT_PARAMETER_OPTION_INSTANTIATE( typename ThicknessesCoefficientsParams2D_CorrelationClosure::DissipationClosureOptions )

void ThicknessesCoefficientsParams2D_CorrelationClosure::checkInputs(PyDict d)
{
std::vector<const ParameterBase*> allParams;
allParams.push_back(d.checkInputs(params.nameCDclosure));
d.checkUnknownInputs(allParams);
}
ThicknessesCoefficientsParams2D_CorrelationClosure ThicknessesCoefficientsParams2D_CorrelationClosure::params;

// ----- Correlation closure, VarTypeDsThNG, IncompressibleBL ----- //

template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::Functor<Real, Real>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::Functor<Real, Real>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<Real>& var,
    const Real& nue) const;


typedef SurrealS<6> SurrealS6;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::Functor<Real, SurrealS6>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::Functor<Real, SurrealS6>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, SurrealS6>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<SurrealS6>& var,
    const Real& nue) const;

template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::Functor<SurrealS6, SurrealS6>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::Functor<SurrealS6, SurrealS6>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS6, SurrealS6>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS6>& params, const ArrayQ<SurrealS6>& var,
    const SurrealS6& nue) const;

typedef SurrealS<8> SurrealS8;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::Functor<SurrealS8, Real>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::Functor<SurrealS8, Real>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS8, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS8>& params, const ArrayQ<Real>& var,
    const SurrealS8& nue) const;


// ----- Correlation closure, VarTypeDsTsNCt, IncompressibleBL ----- //

template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::Functor<Real, Real>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::Functor<Real, Real>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<Real>& var,
    const Real& nue) const;


typedef SurrealS<6> SurrealS6;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::Functor<Real, SurrealS6>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::Functor<Real, SurrealS6>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, SurrealS6>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<SurrealS6>& var,
    const Real& nue) const;

template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::Functor<SurrealS6, SurrealS6>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::Functor<SurrealS6, SurrealS6>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS6, SurrealS6>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS6>& params, const ArrayQ<SurrealS6>& var,
    const SurrealS6& nue) const;

typedef SurrealS<8> SurrealS8;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::Functor<SurrealS8, Real>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::Functor<SurrealS8, Real>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNCt, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS8, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS8>& params, const ArrayQ<Real>& var,
    const SurrealS8& nue) const;

// ----- Correlation closure, VarTypeDsThG, IncompressibleBL ----- //

template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>::Functor<Real, Real>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>::Functor<Real, Real>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<Real>& var,
    const Real& nue) const;


typedef SurrealS<3> SurrealS3;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>::Functor<Real, SurrealS3>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>::Functor<Real, SurrealS3>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, SurrealS3>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<SurrealS3>& var,
    const Real& nue) const;

template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>::Functor<SurrealS8, Real>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>::Functor<SurrealS8, Real>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS8, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS8>& params, const ArrayQ<Real>& var,
    const SurrealS8& nue) const;


// ----- Correlation closure, VarTypeDsThNGsplit, IncompressibleBL ----- //

template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::Functor<Real, Real>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::Functor<Real, Real>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<Real>& var,
    const Real& nue) const;


typedef SurrealS<4> SurrealS4;
template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::Functor<Real, SurrealS4>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::Functor<Real, SurrealS4>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, SurrealS4>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<SurrealS4>& var,
    const Real& nue) const;

template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::Functor<SurrealS4, SurrealS4>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::Functor<SurrealS4, SurrealS4>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS4, SurrealS4>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS4>& params, const ArrayQ<SurrealS4>& var,
    const SurrealS4& nue) const;

template class ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::Functor<SurrealS8, Real>;

template
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::Functor<SurrealS8, Real>
ThicknessesCoefficients2D<CorrelationClosure, VarTypeDsThNGsplit, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS8, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS8>& params, const ArrayQ<Real>& var,
    const SurrealS8& nue) const;

}
