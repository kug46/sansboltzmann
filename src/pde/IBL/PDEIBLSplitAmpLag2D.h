// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_PDEIBLSPLITAMPLAG2D_H_
#define SRC_PDE_IBL_PDEIBLSPLITAMPLAG2D_H_

#define ISIBLLFFLUX_PDEIBLSplitAmpLag 0 // true if LF flux scheme is used; false if full upwinding is used

#define IsExplicitCheckingIBLSplitAmpLag 1

#define IS_LAGFACTOR_XFOILPAPER_NOT_XFOILV699_IBLSplitAmpLag 0
// determines which lag factor to use: true if using the XFOIL paper; false if using XFOIL v6.99 source code

#define IS_AMPLIFICATION_RATE_IBL_NOT_XFOILV699_IBLSplitAmpLag 0

#define IS_AMP_RATE_TURB_BL_UNRAMPED 0

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"   // required for matrix transpose
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"                      // required for dot product
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Surreal/PromoteSurreal.h"

#include "tools/smoothmath.h"

#include "IBL_Traits.h"

#include "IBLMiscClosures.h"
#include "FluidModels.h"
#include "VarInterpret2D.h"
#include "QauxvInterpret2D.h"
#include "ThicknessesCoefficients2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// template parameters:
//   T                        solution DOF data type (e.g. double, SurrealS)
//   VarType                  solution variable type set (e.g. primitive variables, conservative variables)
//   ProfileType              boundary layer profile type (e.g. laminar vs turbulent, incompressible vs compressible)
//
// Nodal solution variable vector (var):
//  Depends on the definition in solution interpreter class (VarType).
//
// Residual vector R
//  [0] R^(e)  momentum equation
//  [1] R^(*)  kinetic energy equation
//  [2] R^(n)  amplification factor growth equation
//  [3] R^(G)  lag equation
//
// member functions:
//   .hasFluxConservative        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//   .hasSource                  T/F: PDE has source term
//   .hasForcingFunction         T/F: PDE has forcing function term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .source                     solution-dependent sources: S(X, Q, QX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <class VarType>   // required for compiler to understand that a template is used
class PDEIBLSplitAmpLag<PhysD2, VarType>
{
public:

  enum IBLResidualInterpCategory
  {
    IBL_ResidInterp_Raw
  };

  typedef PhysD2 PhysDim;
  typedef IBLSplitAmpLagField IBLFormulationType;
  typedef IBLTraits<PhysDim, IBLFormulationType> IBLTraitsType;

  static const int D = IBLTraitsType::D;           // physical dimensions
  static const int N = IBLTraitsType::N;           // total solution variables
  static const int Nparam = IBLTraitsType::Nparam; // total solution variables

  // equation indexing
  static const int ir_mom = 0;
  static const int ir_ke = 1;
  static const int ir_amp = 2;
  static const int ir_lag = 3;

  static_assert(ir_mom < N, "Residual equation index overflows.");
  static_assert(ir_ke < N, "Residual equation index overflows.");
  static_assert(ir_amp < N, "Residual equation index overflows.");
  static_assert(ir_lag < N, "Residual equation index overflows.");

  template <class T>
  using ArrayQ = IBLTraitsType::template ArrayQ<T>;     // solution/residual array type

  template <class T>
  using MatrixQ = IBLTraitsType::template MatrixQ<T>;   // matrix type

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class TP>
  using ArrayParam = IBLTraitsType::template ArrayParam<TP>;     // solution/residual array type

  template <class TP>
  using MatrixParam = IBLTraitsType::template MatrixParam<TP>;   // matrix type

  template <class TX>
  using VectorX = IBLTraitsType::VectorX<TX>; // size-D vector (e.g. basis unit vector) type

  template <class TX>
  using TensorX = IBLTraitsType::TensorX<TX>; // size-D vector (e.g. basis unit vector) type

  typedef VarInterpret2D<VarType> VarInterpType; // solution variable interpreter type
  typedef QauxvInterpret2D ParamInterpType;

  typedef VelocityProfile2D VelocityProfileType;
  typedef IBLProfileCategory ProfileCategory;
  typedef GasModelIBL GasModelType;
  typedef ViscosityModel<ViscosityConstant> ViscosityModelType;
  typedef TransitionModel TransitionModelType;

  typedef typename IBLClosureType<VarType>::type ClosureType;
  typedef IncompressibleBL CompressibilityType; // TODO: currently assume incompressible viscous layer
  typedef ThicknessesCoefficients2D<ClosureType, VarType, CompressibilityType> ThicknessesCoefficientsType;

  template<class Tparam, class Tibl>
  using ThicknessesCoefficientsFunctor = typename ThicknessesCoefficientsType::template Functor<Tparam, Tibl>;

  //-----------------------Constructors-----------------------//
  PDEIBLSplitAmpLag( const GasModelType& gasModel,
                     const ViscosityModelType& viscosityModel,
                     const TransitionModelType& transitionModel,
                     const PyDict d_thicknessesCoefficients = PyDict(),
                     const IBLResidualInterpCategory rsdCat=IBL_ResidInterp_Raw ) :
    gasModel_(gasModel),
    viscosityModel_(viscosityModel),
    transitionModel_(transitionModel),
    varInterpret_(),
    paramInterpret_(),
    thicknessesCoefficientsObj_(varInterpret_, paramInterpret_, d_thicknessesCoefficients),
    rsdCat_(rsdCat)
  {
    SANS_ASSERT( rsdCat_ == IBL_ResidInterp_Raw );
  }

  //-----------------------Default class methods-----------------------//
  ~PDEIBLSplitAmpLag() {}
  PDEIBLSplitAmpLag( const PDEIBLSplitAmpLag& ) = delete;
  PDEIBLSplitAmpLag& operator=( const PDEIBLSplitAmpLag& )  = delete;

  //-----------------------Members-----------------------//
  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  bool needsSolutionGradientforSource() const { return (hasSource() && false); }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tp, class Tout>
  void fluxAdvectiveTime(const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                         const Real& x, const Real& z, const ArrayQ<T>& var, ArrayQ<Tout>& ft ) const
  {
//    Tout ftLocal = 0;
//    masterState(params, e1, x, z, var, ftLocal);
//    ft += ftLocal;
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tp>
  void jacobianFluxAdvectiveTime(
      const ArrayParam<Tp>& params, const VectorX<Real>& e1,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // master state: U(Q)
  template <class T, class Tp, class Tout>
  void masterState( const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                    const Real& x, const Real& z, const ArrayQ<T>& var,
                    ArrayQ<Tout>& uCons ) const;

  template <class T, class Tp, class Tout>
  void masterState( const ProfileCategory profileCat, const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                    const Real& x, const Real& z, const ArrayQ<T>& var,
                    ArrayQ<Tout>& uCons ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState( const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime( const ArrayQ<T>& var, const ArrayQ<T>& vart, ArrayQ<T>& strongPDE ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // advective flux: F(Q)
  template <class T, class Tp, class Tout>
  void fluxAdvective(
      const ArrayParam<Tp>& params, const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time, const ArrayQ<T>& var,
      ArrayQ<Tout>& fx, ArrayQ<Tout>& fz ) const;

  template <class T, class Tp, class Tout>
  void fluxAdvective(
      const ProfileCategory profileCat, const ArrayParam<Tp>& params, const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time, const ArrayQ<T>& var,
      ArrayQ<Tout>& fx, ArrayQ<Tout>& fz ) const;

  template <class TL, class TR, class Tp, class Tout>
  void fluxAdvectiveUpwind(
      const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
      const VectorX<Real>& e1L, const VectorX<Real>& e1R,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
      const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
      ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const;

  template <class TL, class TR, class Tp, class Tout>
  void fluxAdvectiveUpwind_FullUpwind(
      const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
      const VectorX<Real>& e1L, const VectorX<Real>& e1R,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
      const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
      ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const;

  template <class TL, class TR, class Tp, class Tout>
  void fluxAdvectiveUpwind_LaxFriedrichs(
      const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
      const VectorX<Real>& e1L, const VectorX<Real>& e1R,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
      const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
      ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const;

  template <class TL, class TR, class Tp, class Tout>
  void fluxAdvectiveUpwind_Roe(
      const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
      const VectorX<Real>& e1L, const VectorX<Real>& e1R,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
      const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
      ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const;

  template <class T, class Tp, class Tout>
  void fluxDissipationPart_Roe(
      const IBLProfileCategory& profileCat, const ArrayParam<Tp>& params,
      const VectorX<Real>& e1, const Real& x, const Real& z, const Real& time,
      const Real& nx, const Real& nz,
      const ArrayQ<T>& var, const ArrayQ<Tout>& duCons,
      ArrayQ<Tout>& f_diss ) const;

  template <class T>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<T>& f ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var,
      MatrixQ<T>& ax, MatrixQ<T>& ay ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var, const Real& nx, const Real& ny,
      MatrixQ<T>& a ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<T>& a ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      ArrayQ<T>& strongPDE ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // viscous flux: Fv(Q, QX)
  template <class T, class Tg, class Tp, class Tout>
  void fluxViscous(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      ArrayQ<Tout>& f, ArrayQ<Tout>& g ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  template <class T, class Tg, class Tp, class Tout>
  void fluxViscous(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& varL, const ArrayQ<Tg>& varxL, const ArrayQ<Tg>& varyL,
      const ArrayQ<T>& varR, const ArrayQ<Tg>& varxR, const ArrayQ<Tg>& varyR,
      const Real& nx, const Real& ny,
      ArrayQ<Tout>& f ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class T, class Tg, class Tp, class Tk>
  void diffusionViscous(
      const ArrayParam<Tp>& params, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, MatrixQ<T>& ax, MatrixQ<T>& ay ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      const ArrayQ<T>& varxx,
      const ArrayQ<T>& varxy, const ArrayQ<T>& varyy,
      ArrayQ<T>& strongPDE ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // solution-dependent source: S(X, Q, QX)
  template <class T, class Tg, class Tp, class Tout>
  void source(
      const ArrayParam<Tp>& params,
      const VectorX<Real>& e1, const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& var_x, const ArrayQ<Tg>& var_z,
      ArrayQ<Tout>& src) const;

  template <class T, class Tg, class Tp, class Tout>
  void source(
      const ProfileCategory profileCat, const ArrayParam<Tp>& params,
      const VectorX<Real>& e1, const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& var_x, const ArrayQ<Tg>& var_z,
      ArrayQ<Tout>& src) const;

  // dual-consistent source
  template <class T>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<T>& varL, const ArrayQ<T>& varxL, const ArrayQ<T>& varyL,
      const ArrayQ<T>& varR, const ArrayQ<T>& varxR, const ArrayQ<T>& varyR,
      ArrayQ<T>& sourceL, ArrayQ<T>& sourceR ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

#if 1
  //TODO: Empty now since the analytic jacobian is not readily available.
  //      But may need to fill this up later for better robustness using pseudo time continuation

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tg, class Tp>
  void jacobianSource(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ< typename promote_Surreal<T,Tg,Tp>::type >& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tg, class Tp>
  void jacobianSourceAbsoluteValue(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ< typename promote_Surreal<T,Tg,Tp>::type >& dsdu) const {}
#endif

  // right-hand-side forcing function
  template <class T, class Tp>
  void forcingFunction( const ArrayParam<Tp>& params, const Real& x, const Real& y, const Real& time,
                        ArrayQ<T>& forcing ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  // characteristic speed (needed for timestep)
  template <class T, class Tp>
  void speedCharacteristic(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, Tp& speed ) const;

  // is state physically valid
  template<class T>
  bool isValidState( const ArrayQ<T>& var ) const;

  // update fraction needed for physically valid state
  template <class T>
  void updateFraction(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& dq,
      Real maxChangeFraction, Real& updateFraction ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented.");
  }

  template <class T, class Tp>
  void updateFraction(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& dq,
      const Real& maxChangeFraction, Real& updateFraction ) const;

  // set from variable array
  template<class T, class IBLVarData>
  void setDOFFrom( const IBLVarDataType<IBLVarData>& data, ArrayQ<T>& var ) const;

  // set from variable array
  template<class IBLVarData>
  ArrayQ<Real> setDOFFrom( const IBLVarDataType<IBLVarData>& data ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the interp type
  IBLResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // print out parameters
  void dump( int indentSize, std::ostream& out = std::cout ) const;

  // access members
  const GasModelType& getGasModel() const { return gasModel_; }
  const ViscosityModelType& getViscosityModel() const { return viscosityModel_; }
  const TransitionModelType& getTransitionModel() const { return transitionModel_; }
  const ThicknessesCoefficientsType& getSecondaryVarObj() const { return thicknessesCoefficientsObj_; }
  const VarInterpType& getVarInterpreter() const { return varInterpret_; }
  const ParamInterpType& getParamInterpreter() const { return paramInterpret_; }

  // evaluate parameters
  void evalParam( Real& gam, Real& R, Real& mue) const
  {
    gam = gasModel_.gamma();
    R = gasModel_.R();
    mue = viscosityModel_.dynamicViscosity();
  }

  Real ampGrowthRateTurb() const
  {
    const Real chord = 1.0;
    return 4.0*(transitionModel_.ntcrit() - transitionModel_.ntinit())/chord;
  }

  // compute intermittency
  template <class T, class Tp>
  typename promote_Surreal<T,Tp>::type
  calcIntermittency(const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                    const Real& x, const Real& z, const ArrayQ<T>& var) const;

  // determine profile/closure category/type
  template <class T>
  ProfileCategory getProfileCategory(const ArrayQ<T>& var, const Real& x) const
  {
    const T& nt = varInterpret_.getnt(var);
    return transitionModel_.getProfileCategory(nt,x);
  }

protected:
  const GasModelType& gasModel_;
  const ViscosityModelType& viscosityModel_;
  const TransitionModelType& transitionModel_;

  const VarInterpType varInterpret_; // solution variable interpreter object
  const ParamInterpType paramInterpret_;// parameter interpreter object
  const ThicknessesCoefficientsType thicknessesCoefficientsObj_; // secondary variable object
  const IBLResidualInterpCategory rsdCat_; // ResidInterp type identifier

  const Real ctau_min_ = 1e-4;
};

//---------------------Member function definitions of PDEIBL2D---------------------//

// unsteady conservative flux: U(Q) (aka conservative variables/quantities)
template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2, VarType>::
masterState( const ArrayParam<Tp>& params, const VectorX<Real>& e1,
             const Real& x, const Real& z, const ArrayQ<T>& var,
             ArrayQ<Tout>& uCons ) const
{
  const Tout gamma = calcIntermittency(params,e1,x,z,var);
  const ProfileCategory profileCat = getProfileCategory(var,x);

  if (0.0 < gamma &&  gamma < 1.0) // mixture of laminar and turbulent
  {
    ArrayQ<Tout> uConsLami = 0.0, uConsTurb = 0.0;

    if (profileCat == ProfileCategory::LaminarBL ||
        profileCat == ProfileCategory::TurbulentBL)
    {
      masterState(ProfileCategory::LaminarBL, params, e1, x, z, var, uConsLami);
      masterState(ProfileCategory::TurbulentBL, params, e1, x, z, var, uConsTurb);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Blending should be used for BL, not wake");

    uCons = (1.0-gamma)*uConsLami + gamma*uConsTurb;
  }
  else // either laminar or turbulent
    masterState(profileCat, params, e1, x, z, var, uCons);
}

template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2, VarType>::
masterState(const ProfileCategory profileCat, const ArrayParam<Tp>& params, const VectorX<Real>& e1,
            const Real& x, const Real& z, const ArrayQ<T>& var,
            ArrayQ<Tout>& uCons) const
{
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Tp nue = viscosityModel_.dynamicViscosity() / rhoe;

  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  typedef typename ThicknessesCoefficientsFunctor<Tp, T>::Tout Tpi;

  const Tpi delta1s  = thicknessesCoefficientsFunctor.getdelta1s();
  const Tpi deltarho = thicknessesCoefficientsFunctor.getdeltarho();
  const Tpi theta0s = thicknessesCoefficientsFunctor.gettheta0s();

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);

  uCons[ir_mom] = dot(rhoe*(delta1s-deltarho)*q1,e1);
  uCons[ir_ke] = rhoe*theta0s;
  uCons[ir_amp] = varInterpret_.getnt(var);
  uCons[ir_lag] = varInterpret_.getG(var);
}

//----------------------------------------------------------------------------//
template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2, VarType>::fluxAdvective(
    const ArrayParam<Tp>& params, const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time, const ArrayQ<T>& var,
    ArrayQ<Tout>& fx, ArrayQ<Tout>& fz ) const
{
  const Tout gamma = calcIntermittency(params,e1,x,z,var);
  const ProfileCategory profileCat = getProfileCategory(var,x);

  if (0.0 < gamma &&  gamma < 1.0) // mixture of laminar and turbulent
  {
    ArrayQ<Tout> fxLami = 0.0, fxTurb = 0.0;
    ArrayQ<Tout> fzLami = 0.0, fzTurb = 0.0;

    if (profileCat == ProfileCategory::LaminarBL ||
        profileCat == ProfileCategory::TurbulentBL)
    {
      fluxAdvective(ProfileCategory::LaminarBL, params, e1, x, z, time, var, fxLami, fzLami);
      fluxAdvective(ProfileCategory::TurbulentBL, params, e1, x, z, time, var, fxTurb, fzTurb);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Blending should be used for BL, not wake");

    fx += (1.0-gamma)*fxLami + gamma*fxTurb;
    fz += (1.0-gamma)*fzLami + gamma*fzTurb;
  }
  else // either laminar or turbulent
    fluxAdvective(profileCat, params, e1, x, z, time, var, fx, fz);
}

template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2, VarType>::fluxAdvective(
    const ProfileCategory profileCat, const ArrayParam<Tp>& params, const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time, const ArrayQ<T>& var,
    ArrayQ<Tout>& fx, ArrayQ<Tout>& fz ) const
{
  const T G = varInterpret_.getG(var);
  const T nt = varInterpret_.getnt(var);

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Tp nue = viscosityModel_.dynamicViscosity() / rhoe;

  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tout theta11 = thicknessesCoefficientsFunctor.gettheta11();
  const Tout theta1s = thicknessesCoefficientsFunctor.gettheta1s();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  // P happens to be symmetric in 2D IBL. Note "*" in () is an outer product
  const TensorX<Tout> P = rhoe*theta11*(q1*Transpose(q1)); // [tensor] P: momentum defect flux
  const VectorX<Tout> PTdote = Transpose(P)*e1; // P^T dot e

  const VectorX<Tout> K = rhoe*theta1s*q1;

  // UPDATE/OUTPUT ADVECTIVE FLUX TERMS -----------------------------------------------------------
  fx[ir_mom] += PTdote(0);
  fz[ir_mom] += PTdote(1);

  fx[ir_ke] += K(0);
  fz[ir_ke] += K(1);

  fx[ir_amp] += nt * q1[0];
  fz[ir_amp] += nt * q1[1];

  fx[ir_lag] += G * q1[0];
  fz[ir_lag] += G * q1[1];
}

//----------------------------------------------------------------------------//
template <class VarType>
template <class TL, class TR, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::fluxAdvectiveUpwind(
    const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
    const VectorX<Real>& e1L, const VectorX<Real>& e1R,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
    const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
    ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const
{
  // TODO: can be refactored to differentiate flux choice at compile time rather than macros
#if ISIBLLFFLUX_PDEIBLSplitAmpLag
  fluxAdvectiveUpwind_LaxFriedrichs(paramsL, paramsR, e1L, e1R, x, z, time, varL, varR, nxL, nzL, nxR, nzR, fL, fR);
#elif 0
  fluxAdvectiveUpwind_Roe(paramsL, paramsR, e1L, e1R, x, z, time, varL, varR, nxL, nzL, nxR, nzR, fL, fR);
#else
  fluxAdvectiveUpwind_FullUpwind(paramsL, paramsR, e1L, e1R, x, z, time, varL, varR, nxL, nzL, nxR, nzR, fL, fR);
#endif
}

//----------------------------------------------------------------------------//
template <class VarType>
template <class TL, class TR, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::fluxAdvectiveUpwind_FullUpwind(
    const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
    const VectorX<Real>& e1L, const VectorX<Real>& e1R,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
    const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
    ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const
{
  ArrayQ<Tout> fxL = 0.0, fzL = 0.0;
  fluxAdvective(paramsL, e1L, x, z, time, varL, fxL, fzL);

  ArrayQ<Tout> fxR = 0.0, fzR = 0.0;
  fluxAdvective(paramsR, e1R, x, z, time, varR, fxR, fzR);

  const VectorX<Real> nrmL = {nxL, nzL}; // trace normal vector
  const VectorX<Tp> q1L = paramInterpret_.getq1(paramsL);
  const Tp qnL = dot(q1L, nrmL);

  // standard full upwinding
  const ArrayQ<Tout> f_unique = (qnL > 0) ? fxL*nxL + fzL*nzL : fxR*nxR + fzR*nzR;

  fL += f_unique;
  fR += f_unique;

  // add mass defect flux jump correction
  const Tp qeL = paramInterpret_.getqe(paramsL);
  const Tp rhoeL = gasModel_.density(qeL, paramInterpret_.getp0(paramsL), paramInterpret_.getT0(paramsL)); // rho_e: edge static density
  const Tp nueL = viscosityModel_.dynamicViscosity() / rhoeL;
  const ProfileCategory profileCatL = getProfileCategory(varL,x);
  const ThicknessesCoefficientsFunctor<Tp, TL> thicknessesCoefficientsFunctorL
  = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatL, paramsL, varL, nueL);

  const Tp qeR = paramInterpret_.getqe(paramsR);
  const Tp rhoeR = gasModel_.density(qeR, paramInterpret_.getp0(paramsR), paramInterpret_.getT0(paramsR)); // rho_e: edge static density
  const Tp nueR = viscosityModel_.dynamicViscosity() / rhoeR;
  const ProfileCategory profileCatR = getProfileCategory(varR,x);
  const ThicknessesCoefficientsFunctor<Tp, TR> thicknessesCoefficientsFunctorR
  = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatR, paramsR, varR, nueR);

  const Tout delta1sL = thicknessesCoefficientsFunctorL.getdelta1s();
  const Tout delta1sR = thicknessesCoefficientsFunctorR.getdelta1s();

  const VectorX<Real> nrmR = {nxR, nzR}; // trace normal vector
  const VectorX<Tp> q1R = paramInterpret_.getq1(paramsR);
  const Tp qnR = dot(q1R, nrmR);

  const Real alpha = 1.0;
  const Tout f_mass_L = rhoeL*delta1sL*dot(q1L,e1L)*qnL;
  const Tout f_mass_R = rhoeR*delta1sR*dot(q1R,e1R)*qnR;
  const Tout f_mass_upwind = (qnL > 0) ? f_mass_L : f_mass_R;
  fL[ir_mom] += alpha*(f_mass_upwind - f_mass_L);
  fR[ir_mom] += alpha*(f_mass_upwind - f_mass_R);
}

//----------------------------------------------------------------------------//
template <class VarType>
template <class TL, class TR, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::fluxAdvectiveUpwind_LaxFriedrichs(
    const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
    const VectorX<Real>& e1L, const VectorX<Real>& e1R,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
    const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
    ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const
{
  ArrayQ<Tout> fxL = 0.0, fzL = 0.0;
  fluxAdvective(paramsL, e1L, x, z, time, varL, fxL, fzL);
  ArrayQ<Tout> uConsL = 0.0;
  masterState(paramsL, e1L, x, z, varL, uConsL);

  ArrayQ<Tout> fxR = 0.0, fzR = 0.0;
  fluxAdvective(paramsR, e1R, x, z, time, varR, fxR, fzR);
  ArrayQ<Tout> uConsR = 0.0;
  masterState(paramsR, e1R, x, z, varR, uConsR);

  // standard LF flux
  const Tp alpha = std::max( paramInterpret_.getqe(paramsL), paramInterpret_.getqe(paramsR) );  // dissipation coefficient
  ArrayQ<Tout> f_unique = 0.5*(fxL*nxL + fzL*nzL + fxR*nxR + fzR*nzR) + 0.5*alpha*(uConsL - uConsR);

  fL += f_unique;
  fR += f_unique;

  // add mass defect flux jump correction
  const Tp qeL = paramInterpret_.getqe(paramsL);
  const Tp rhoeL = gasModel_.density(qeL, paramInterpret_.getp0(paramsL), paramInterpret_.getT0(paramsL)); // rho_e: edge static density
  const Tp nueL = viscosityModel_.dynamicViscosity() / rhoeL;
  const ProfileCategory profileCatL = getProfileCategory(varL,x);
  const ThicknessesCoefficientsFunctor<Tp, TL> thicknessesCoefficientsFunctorL
  = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatL, paramsL, varL, nueL);

  const Tp qeR = paramInterpret_.getqe(paramsR);
  const Tp rhoeR = gasModel_.density(qeR, paramInterpret_.getp0(paramsR), paramInterpret_.getT0(paramsR)); // rho_e: edge static density
  const Tp nueR = viscosityModel_.dynamicViscosity() / rhoeR;
  const ProfileCategory profileCatR = getProfileCategory(varR,x);
  const ThicknessesCoefficientsFunctor<Tp, TR> thicknessesCoefficientsFunctorR
  = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatR, paramsR, varR, nueR);

  const Tout delta1sL = thicknessesCoefficientsFunctorL.getdelta1s();
  const Tout delta1sR = thicknessesCoefficientsFunctorR.getdelta1s();

  const VectorX<Real> nrmL = {nxL, nzL}; // trace normal vector
  const VectorX<Tp> q1L = paramInterpret_.getq1(paramsL);
  const Tp qnL = dot(q1L, nrmL);

  const VectorX<Real> nrmR = {nxR, nzR}; // trace normal vector
  const VectorX<Tp> q1R = paramInterpret_.getq1(paramsR);
  const Tp qnR = dot(q1R, nrmR);

  const Tout f_mass_L = rhoeL*delta1sL*dot(q1L,e1L)*qnL;
  const Tout f_mass_R = rhoeR*delta1sR*dot(q1R,e1R)*qnR;
  const Tout f_mass_upwind = (qnL > 0) ? f_mass_L : f_mass_R;
  //  const Tout f_mass_upwind = 0.5*(f_mass_L + f_mass_R); // also provides stabilization
  fL[ir_mom] += f_mass_upwind - f_mass_L;
  fR[ir_mom] += f_mass_upwind - f_mass_R;
}

//----------------------------------------------------------------------------//
template <class VarType>
template <class TL, class TR, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::fluxAdvectiveUpwind_Roe(
    const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
    const VectorX<Real>& e1L, const VectorX<Real>& e1R,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
    const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
    ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const
{
  // left contributions
  ArrayQ<Tout> fxL = 0.0, fzL = 0.0;
  fluxAdvective(paramsL, e1L, x, z, time, varL, fxL, fzL);
  ArrayQ<Tout> fxR = 0.0, fzR = 0.0;
  fluxAdvective(paramsR, e1R, x, z, time, varR, fxR, fzR);

  // add dissipation flux component
  ArrayQ<Tout> uConsL = 0.0;
  masterState(paramsL, e1L, x, z, varL, uConsL);
  ArrayQ<Tout> uConsR = 0.0;
  masterState(paramsR, e1R, x, z, varR, uConsR);

  const ArrayQ<Tout> duCons = uConsL - uConsR;

  const VectorX<Real> nrmL = {nxL, nzL}; // trace normal vector
  const VectorX<Tp> q1L = paramInterpret_.getq1(paramsL);
  const Tp qnL = dot(q1L, nrmL);

  Tp alpha = fabs(qnL);
  const ArrayQ<Tout> f_lf = 0.5*(fxL*nxL + fzL*nzL + fxR*nxR + fzR*nzR) + 0.5*alpha*duCons;

  if (qnL > 0) // flow from left to right
  {
    const ArrayQ<Tout> f_upwind = fxL*nxL + fzL*nzL;

    // outflow from element
    fL += f_upwind;

    // inflow to element
    fR[ir_mom] += f_lf[ir_mom];
//    fR[ir_mom] += f_upwind[ir_mom];
//    fR[ir_ke]  += f_lf[ir_ke];
    fR[ir_ke] += f_upwind[ir_ke];
//    fR[ir_amp] += f_lf[ir_amp];
//    fR[ir_lag] += f_lf[ir_lag];
    fR[ir_amp] += f_upwind[ir_amp];
    fR[ir_lag] += f_upwind[ir_lag];
  }
  else
  {
    const ArrayQ<Tout> f_upwind = fxR*nxR + fzR*nzR;

    // inflow to element
    fL[ir_mom] += f_lf[ir_mom];
//    fL[ir_mom] += f_upwind[ir_mom];
//    fL[ir_ke]  += f_lf[ir_ke];
    fL[ir_ke] += f_upwind[ir_ke];
//    fL[ir_amp] += f_lf[ir_amp];
//    fL[ir_lag] += f_lf[ir_lag];
    fL[ir_amp] += f_upwind[ir_amp];
    fL[ir_lag] += f_upwind[ir_lag];

    // outflow from element
    fR += f_upwind;
  }

  return;

//
//#if 0 // average jabobian
//  const ProfileCategory profileCatL = getProfileCategory(varL,x);
//  const ProfileCategory profileCatR = getProfileCategory(varR,x);
//
//  if ( (profileCatL == IBLProfileCategory::LaminarBL || profileCatL == IBLProfileCategory::TurbulentBL)
//    && (profileCatR == IBLProfileCategory::LaminarBL || profileCatR == IBLProfileCategory::TurbulentBL) )
//  {
//    ArrayQ<Tout> f_diss_lami_L = 0.0, f_diss_lami_R = 0.0;
//    fluxDissipationPart_Roe(IBLProfileCategory::LaminarBL, paramsL, e1L, x, z, time, nxL, nzL, varL, duCons, f_diss_lami_L);
//    fluxDissipationPart_Roe(IBLProfileCategory::LaminarBL, paramsR, e1R, x, z, time, nxR, nzR, varR, duCons, f_diss_lami_R);
//
//    ArrayQ<Tout> f_diss_turb_L = 0.0, f_diss_turb_R = 0.0;
//    fluxDissipationPart_Roe(IBLProfileCategory::TurbulentBL, paramsL, e1L, x, z, time, nxL, nzL, varL, duCons, f_diss_turb_L);
//    fluxDissipationPart_Roe(IBLProfileCategory::TurbulentBL, paramsR, e1R, x, z, time, nxR, nzR, varR, duCons, f_diss_turb_R);
//
//    const Tout gammaL = calcIntermittency(paramsL,e1L,x,z,varL);
//    const Tout gammaR = calcIntermittency(paramsR,e1R,x,z,varR);
//
//    f_unique += 0.5*( (1.0-gammaL)*f_diss_lami_L + gammaL*f_diss_turb_L
//                    + (1.0-gammaR)*f_diss_lami_R + gammaR*f_diss_turb_R );
//
////    const Tout gamma_avg = 0.5*(gammaL + gammaR);
////    f_unique += (1.0-gamma_avg)*0.5*(f_diss_lami_L + f_diss_lami_R)
////                    + gamma_avg*0.5*(f_diss_turb_L + f_diss_turb_R);
//  }
//  else if (profileCatL == IBLProfileCategory::TurbulentWake && profileCatR == IBLProfileCategory::TurbulentWake)
//  {
//    ArrayQ<Tout> f_diss_L = 0.0, f_diss_R = 0.0;
//    fluxDissipationPart_Roe(profileCatL, paramsL, e1L, x, z, time, nxL, nzL, varL, duCons, f_diss_L);
//    fluxDissipationPart_Roe(profileCatR, paramsR, e1R, x, z, time, nxR, nzR, varR, duCons, f_diss_R);
//
//    f_unique += 0.5*(f_diss_L + f_diss_R);
//  }
//  else
//    SANS_DEVELOPER_EXCEPTION("Not implemented yet");
//#else // average state
//  const ArrayQ<typename promote_Surreal<TL,TR>::type> var_avg = 0.5*(varL+varR);
//  SANS_ASSERT_MSG(isValidState(var_avg), "Average state should be valid!");
//
//  ArrayQ<Tout> f_diss = 0.0;
//  const ProfileCategory profileCatL = getProfileCategory(varL,x);
////  const ProfileCategory profileCatR = getProfileCategory(varR,x);
//  fluxDissipationPart_Roe(profileCatL, paramsL, e1L, x, z, time, nxL, nzL, var_avg, duCons, f_diss);
//
//  f_unique += f_diss;
//#endif
//
//  fL += f_unique;
//  fR += f_unique;
}


template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::fluxDissipationPart_Roe(
    const IBLProfileCategory& profileCat, const ArrayParam<Tp>& params,
    const VectorX<Real>& e1, const Real& x, const Real& z, const Real& time,
    const Real& nx, const Real& nz,
    const ArrayQ<T>& var, const ArrayQ<Tout>& duCons,
    ArrayQ<Tout>& f_diss ) const
{
  const VectorX<Tp> q1 = paramInterpret_.getq1(params);
  const Tp qn = q1[0]*nx + q1[1]*nz;
#if 1
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Tp nue = viscosityModel_.dynamicViscosity() / rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  typename ThicknessesCoefficientsFunctor<Tp, T>::Tout theta1s_delta1s = 0.0;
  typename ThicknessesCoefficientsFunctor<Tp, T>::Tout theta1s_theta11 = 0.0;

  if (profileCat == IBLProfileCategory::LaminarBL || profileCat == IBLProfileCategory::TurbulentBL)
  { // laminar turbulent blending
    const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctorLami
      = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);

    typename ThicknessesCoefficientsFunctor<Tp, T>::Tout theta1s_delta1s_lami = 0.0;
    typename ThicknessesCoefficientsFunctor<Tp, T>::Tout theta1s_theta11_lami = 0.0;
    thicknessesCoefficientsFunctorLami.gettheta1s_derivatives(theta1s_delta1s_lami, theta1s_theta11_lami);

    const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctorTurb
      = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

    typename ThicknessesCoefficientsFunctor<Tp, T>::Tout theta1s_delta1s_turb = 0.0;
    typename ThicknessesCoefficientsFunctor<Tp, T>::Tout theta1s_theta11_turb = 0.0;
    thicknessesCoefficientsFunctorTurb.gettheta1s_derivatives(theta1s_delta1s_turb, theta1s_theta11_turb);

    const auto gamma = calcIntermittency(params,e1,x,z,var);

    theta1s_delta1s = (1.0-gamma)*theta1s_delta1s_lami + gamma*theta1s_delta1s_turb;
    theta1s_theta11 = (1.0-gamma)*theta1s_theta11_lami + gamma*theta1s_theta11_turb;
  }
  else if (profileCat == IBLProfileCategory::TurbulentWake)
  { // completely turbulent
    const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
      = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

    thicknessesCoefficientsFunctor.gettheta1s_derivatives(theta1s_delta1s, theta1s_theta11);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Should not get here!");

  const Tout b = theta1s_theta11-1.0;
  const Tout sqrt_del = sqrt(pow(b, 2.0) + 4.0*theta1s_delta1s);

  const Real dir_n = (qn > 0.0) ? 1.0 : -1.0; // direction of trace normal vector relative to edge velocity

  const Tout tmp = sqrt_del*sqrt(pow(dir_n,2.0));

  const Tout eigm_nd = 0.5*(b*dir_n - tmp);
  const Tout eigp_nd = 0.5*(b*dir_n + tmp); // non-dimensional = eig_ibl / qe

//  if (std::is_same<Tout, Real>::value == true)
//  {
//    std::cout << std::scientific << std::setprecision(16) << std::endl;
//    std::cout << "Nondimensional eigenvalues at x = " << x << ", z = " << z << std::endl;
//    std::cout << "  minus, plus:" << eigm_nd << ", " << eigp_nd << std::endl << std::endl;
//  }

  Tout abs_eigm_nd = fabs(eigm_nd);
  Tout abs_eigp_nd = fabs(eigp_nd);

//  // entropy fix to guard against close-to-zero eigenvalues away from stagnation point
////  const Tp eps_eig_nd = 5e-1;
////  const Tp eps_eig_nd = 1e-1;
//  const Tp eps_eig_nd = 1e-2;
////  const Tp eps_eig_nd = 5e-3;
////  const Tp eps_eig_nd = 1e-3;
//  if (abs_eigm_nd < eps_eig_nd) abs_eigm_nd = 0.5*(eps_eig_nd + abs_eigm_nd*abs_eigm_nd/eps_eig_nd);
//  if (abs_eigp_nd < eps_eig_nd) abs_eigp_nd = 0.5*(eps_eig_nd + abs_eigp_nd*abs_eigp_nd/eps_eig_nd);

#if 0
  const Real dir_e = (dot(q1,e1) > 0.0) ? 1.0 : -1.0; // direction of basis direction vector relative to edge velocity

  const Tout deigpm_nd = eigp_nd - eigm_nd;

  const Tout A00 = abs_eigm_nd/deigpm_nd * qe*(dir_n + eigp_nd)
                 - abs_eigp_nd/deigpm_nd * qe*(dir_n + eigm_nd);
  const Tout A01 = dir_e*dir_n*(-abs_eigm_nd + abs_eigp_nd)/deigpm_nd;
  const Tout A10 = (abs_eigm_nd - abs_eigp_nd)/(dir_e*dir_n*deigpm_nd)
                  * pow(qe,2.0)*(dir_n + eigm_nd)*(dir_n + eigp_nd);
  const Tout A11 = -abs_eigm_nd/deigpm_nd * qe*(dir_n + eigm_nd)
                  + abs_eigp_nd/deigpm_nd * qe*(dir_n + eigp_nd);
#else // reverts to LF flux
  const Tout alpha = fabs(qn) * std::max(abs_eigm_nd, abs_eigp_nd);
  const Tout A00 = alpha;
  const Tout A01 = 0.0;
  const Tout A10 = 0.0;
  const Tout A11 = alpha;

//  std::cout << "LF flux with max(eig) for mom/ke equations" << std::endl;
#endif

#else // reverts to heuristic LF flux
  const Tout alpha = fabs(qn);
  const Tout A00 = alpha;
  const Tout A01 = 0.0;
  const Tout A10 = 0.0;
  const Tout A11 = alpha;
#endif

  // f_diss = |A| * dU
  f_diss[ir_mom] += A00*duCons[ir_mom] + A01*duCons[ir_ke];
  f_diss[ir_ke]  += A10*duCons[ir_mom] + A11*duCons[ir_ke];
  f_diss[ir_amp] += fabs(qn)*duCons[ir_amp];
  f_diss[ir_lag] += fabs(qn)*duCons[ir_lag];
}

//----------------------------------------------------------------------------//
// solution-dependent source: S(X, Q, QX)
template<class VarType>
template <class T, class Tg, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::source(
    const ArrayParam<Tp>& params,
    const VectorX<Real>& e1, const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<T>& var, const ArrayQ<Tg>& var_x, const ArrayQ<Tg>& var_z,
    ArrayQ<Tout>& src) const
{
  const Tout gamma = calcIntermittency(params,e1,x,z,var);
  const ProfileCategory profileCat = getProfileCategory(var,x);

  if (0.0 < gamma &&  gamma < 1.0) // mixture of laminar and turbulent
  {
    ArrayQ<Tout> srcLami = 0.0, srcTurb = 0.0;

    if (profileCat == ProfileCategory::LaminarBL ||
        profileCat == ProfileCategory::TurbulentBL)
    {
      source(ProfileCategory::LaminarBL,   params, e1, e1_x, e1_z, x, z, time, var, var_x, var_z, srcLami);
      source(ProfileCategory::TurbulentBL, params, e1, e1_x, e1_z, x, z, time, var, var_x, var_z, srcTurb);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Blending should be used for BL, not wake");

    src += (1.0-gamma)*srcLami + gamma*srcTurb;
  }
  else // either laminar or turbulent
    source(profileCat, params, e1, e1_x, e1_z, x, z, time, var, var_x, var_z, src);
}

template<class VarType>
template <class T, class Tg, class Tp, class Tout>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::source(
    const ProfileCategory profileCat, const ArrayParam<Tp>& params,
    const VectorX<Real>& e1, const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<T>& var, const ArrayQ<Tg>& var_x, const ArrayQ<Tg>& var_z,
    ArrayQ<Tout>& src) const
{
  typedef typename ThicknessesCoefficientsFunctor<Tp, T>::Tout Tpi;

  const T nt = varInterpret_.getnt(var);
  const T G = varInterpret_.getG(var);
  const T ctau = varInterpret_.getctau(var);

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);
  const TensorX<Tp> nablasq1 = paramInterpret_.getgradq1(params);
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Real mue = viscosityModel_.dynamicViscosity();
  const Tp nue = mue / rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tpi delta1s = thicknessesCoefficientsFunctor.getdelta1s();
  const Tpi theta11 = thicknessesCoefficientsFunctor.gettheta11();
  const Tpi RetCf1 = thicknessesCoefficientsFunctor.getRetCf1();
  const Tpi RetCDiss = thicknessesCoefficientsFunctor.getRetCD();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  const VectorX<Tout> tauw = (0.5*mue/theta11*RetCf1)*q1; // tau_w: wall shear stress

  const VectorX<Tout> M = rhoe*delta1s*q1; // mass flux defect

  const Tout Diss = mue*RetCDiss/theta11; // {\cal D}: dissipation integral

  // -- momentum and kinetic energy equation source terms --
#if 1
  // P is symmetric in 2D IBL. Note "*" in () is an outer product
  const TensorX<Tout> P = rhoe*theta11*(q1*Transpose(q1)); // [tensor] P: momentum defect flux

  // (P  dot  nablas)  dot  e1 (as in IBL3) or trace(P^T  (nablas e1)) as in Shun's Master thesis
  const Tout Pdotnablasdote1 = P(0,0)*e1_x[0] + P(1,0)*e1_x[1]
                             + P(0,1)*e1_z[0] + P(1,1)*e1_z[1];

  src[ir_mom] += -Pdotnablasdote1 + dot(e1, nablasq1*M) - dot(tauw,e1); // source corresponding to R^e in momentum residual

#if IsExplicitCheckingIBLSplitAmpLag && 1
  if (fabs(Pdotnablasdote1) > 1e-16)
  {
    std::cout << "Pdotnablasdote1 = " << Pdotnablasdote1 << std::endl << std::endl;
    SANS_DEVELOPER_EXCEPTION("Pdotnablasdote1 == 0 for linear (Q=1) grids");
  }

  {
    const Tout tmp1 = dot(e1, nablasq1*M);
    const Tout tmp2 = dot(e1, M)*paramInterpret_.getdivq1(params);
    if (fabs(tmp1-tmp2) > 1e-12 && std::is_same<Tout,Real>::value)
    {
      std::cout << std::scientific << std::setprecision(16);
      std::cout << "dot(e1, nablasq1*M) = " << tmp1 << std::endl;
      std::cout << "dot(e1, M)*divq1    = " << tmp2 << std::endl;
      std::cout << "diff                = " << fabs(tmp1-tmp2) << std::endl << std::endl;
//      SANS_DEVELOPER_EXCEPTION("dot(e1, nablasq1*M) == dot(e1, M)*divq1");
    }
  }
#endif

#else
  src[ir_mom] += dot(e1,M)*paramInterpret_.getdivq1(params) - dot(tauw,e1); // source corresponding to R^e in momentum residual
#endif

  const Tpi delta1p = thicknessesCoefficientsFunctor.getdelta1p();
  const Tpi theta1s = thicknessesCoefficientsFunctor.gettheta1s();
  const VectorX<Tout> K = rhoe*theta1s*q1;

  const Tp divq1 = paramInterpret_.getdivq1(params);

#if IsExplicitCheckingIBLSplitAmpLag
  if ( fabs(delta1s-delta1p) > 100*std::numeric_limits<Real>::epsilon() )
    SANS_DEVELOPER_EXCEPTION("D should be zero for incompressible flow.");

  {
    const Tout tmp1 = dot(q1, nablasq1*q1);
    const Tout tmp2 = qe*qe*divq1;
    if (fabs(tmp1-tmp2) > 1e-12 && std::is_same<Tout,Real>::value)
    {
      std::cout << std::scientific << std::setprecision(16);
      std::cout << "dot(q1, nablasq1*q1) = " << tmp1 << std::endl;
      std::cout << "qe*qe*divq1          = " << tmp2 << std::endl;
      std::cout << "diff                 = " << fabs(tmp1-tmp2) << std::endl << std::endl;
    }
  }
#endif

  src[ir_ke] += rhoe*(theta1s+delta1s-delta1p)*2*divq1 - 2.0*Diss; // source corresponding to R^* in kinetic energy residual

  // -- amplification equation source term -- //
  const Tpi Hk = thicknessesCoefficientsFunctor.getHk(); // kinematic shape factor

  if (profileCat == ProfileCategory::LaminarBL)
  {
    const Tpi Ret = qe*theta11/nue;

    // source term intended to impose the inflow (at stagnation point) boundary condition on n_tilde
    // TODO: this correction along with the diffusion time scale works pretty well with ninit_=0 but a bit worse when ninit is nonzero
    // reference time scale: diffusion time scale: small near stagnation point and larger as BL thickens
#if 1
    Tout t_ref = pow(theta11,2.0)/nue;
#else // TODO: hack for old pyrite checks
    Tout t_ref = pow(varInterpret_.getdelta(profileCat,var),2.0)/nue; // TODO: only works when delta is a primary unknown;  to be removed
#endif
    Tout q_ref = 1.0; // TODO: hard-coded for now

#if IS_AMPLIFICATION_RATE_IBL_NOT_XFOILV699_IBLSplitAmpLag // The two correlations are almost identical
    // cut off inflow-BC source after amplification factor growth source term starts to ramp up
    const Tout Samp_infowbc = (1.0 - IBLMiscClosures::AmplificationClosure_IBL::ampGrowthOnsetRamp(Hk,Ret))
        *1.0/t_ref * exp(-pow(qe/q_ref,2.0)) * (nt-transitionModel_.ntinit()); // TODO: the sign is currently positive; to be examined

    const Tout amprate = IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRate(Hk,Ret,theta11);
#else
    // cut off inflow-BC source after amplification factor growth source term starts to ramp up
    const Tout Samp_infowbc = (1.0 - IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthOnsetRamp(Hk,Ret))
        *1.0/t_ref * exp(-pow(qe/q_ref,2.0)) * (nt-transitionModel_.ntinit()); // TODO: the sign is currently positive; to be examined

    const Tout amprate = IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthRate(Hk,Ret,theta11);
#endif

    src[ir_amp] += -nt*divq1 - qe*amprate + Samp_infowbc;
  }
  else if (profileCat == ProfileCategory::TurbulentBL)
  {
#if IS_AMP_RATE_TURB_BL_UNRAMPED
#if IS_AMPLIFICATION_RATE_IBL_NOT_XFOILV699_IBLSplitAmpLag // The two correlations are almost identical
    const Tout amprate = IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRateUnramped(Hk, theta11);
#else
    const Tout amprate = IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthRateUnramped(Hk, theta11);
#endif
#else // TODO: mucking with artificial growth in turbulent BL
    const Real amprate = ampGrowthRateTurb();
#endif

    src[ir_amp] += -nt*divq1 - qe*amprate;
  }
  else if (profileCat == ProfileCategory::TurbulentWake)
    src[ir_amp] += -nt*divq1;
  else if (profileCat == ProfileCategory::LaminarWake)
    SANS_DEVELOPER_EXCEPTION("Laminar wake is not supported!");
#if IsExplicitCheckingIBLSplitAmpLag
  else
    SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");
#endif

#if 0 //TODO
  {
    const Real c1 = 50.0;
    const Real dnt_lami = 1.0, dnt_turb = 5.0; // only C1 continuous source if dnt_lami != dnt_turb
    const Real ntcrit = 9.0;
    if (profileCat == ProfileCategory::LaminarBL)
      src[ir_amp] += -qe*c1*exp(-pow((ntcrit-nt)/dnt_lami, 2.0));
    else if (profileCat == ProfileCategory::TurbulentBL)
      src[ir_amp] += -qe*c1*exp(-pow((ntcrit-nt)/dnt_turb, 2.0));

  }
#endif

#if 0 //TODO
  if (profileCat == ProfileCategory::LaminarBL ||
      profileCat == ProfileCategory::TurbulentBL)
  {
    const Real c1 = 10.0;
    const Real ntcrit = 9.0;
    T arg = -(ntcrit-nt)/1.0;
    const Real arg_max = 2.5;
    if (arg > arg_max) arg = arg_max;
    src[ir_amp] += -qe*c1*exp(arg);
  }
#endif

  // -- lag equation source term -- //
  const Tout ctaueq = thicknessesCoefficientsFunctor.getctaueq();

  if (profileCat == ProfileCategory::LaminarBL)
  {
    Tout ctau_Tinit = IBLMiscClosures::getctauTurbInit_XFOIL(Tout(Hk), ctaueq);
    if (ctau_Tinit < ctau_min_) // TODO: the C1-discontinuity may need to be smoothed out
      ctau_Tinit = ctau_min_;

#if 0
    const Real dx = 1.0; // TODO a length scale to be finalized later.  Potential local scale can be come multiples of theta
//    const Real K_G = 100.0;
    const Real K_G = 30.0;
//    const Real K_G = 5.0;
    const Tout Slag_turb = -qe*K_G/dx*(varInterpret_.getG_from_ctau(ctau_Tinit) - G);
#else // TODO: without caring about dimension consistency yet
    const Real K_G = 100.0;
//    const Real K_G = 30.0;
    Tout dG = (varInterpret_.getG_from_ctau(ctau_Tinit) - G);
    if (dG < 0.0) dG = dG*exp(dG/0.05); // preventing ctau to drop with ctau_Tinit after reaching its peak
    const Tout Slag_turb = -K_G*dG;
#endif

#if 1
    src[ir_lag] += -G*divq1 + Slag_turb;
#else
    const Tpi Ret = qe*theta11/nue;

    // source term intended to impose the inflow (at stagnation point) boundary condition on n_tilde
    // TODO: this correction along with the diffusion time scale works pretty well with ninit_=0 but a bit worse when ninit is nonzero
    // reference time scale: diffusion time scale: small near stagnation point and larger as BL thickens
#if 1
    Tout t_ref = pow(theta11,2.0)/nue;
#else // TODO: hack for old pyrite checks
    Tout t_ref = pow(varInterpret_.getdelta(profileCat,var),2.0)/nue; // TODO: only works when delta is a primary unknown;  to be removed
#endif
    Tout q_ref = 1.0; // TODO: hard-coded for now

    const Tout ampGrowthOnsetRamp = IBLMiscClosures::AmplificationClosure_IBL::ampGrowthOnsetRamp(Hk,Ret);

    const Tout Slag_inflowbc = 1.0/t_ref * exp(-pow(qe/q_ref,2.0)) * (G-varInterpret_.getG_from_ctau(ctau_min_));

    src[ir_lag] += -G*divq1 + Slag_turb*ampGrowthOnsetRamp + Slag_inflowbc*(1.0 - ampGrowthOnsetRamp);
#endif
  }
  else if (profileCat == ProfileCategory::TurbulentBL ||
           profileCat == ProfileCategory::TurbulentWake)
  {
    // -- lag equation source terms --
    const Tout delta_nom = IBLMiscClosures::getdeltaNominal(Hk, delta1s, theta11);

    // dissipation length ratio wall/wake (XFOIL v6.99)
    const Real ALD = (profileCat == ProfileCategory::TurbulentWake) ? 0.9 : 1.0;

    const Real Gacon = thicknessesCoefficientsFunctor.getA_GbetaLocus();
    const Real Gbcon = thicknessesCoefficientsFunctor.getB_GbetaLocus();
    const Real duxcon = 1.0;
//    const Real duxcon = 0.0;

#if IS_LAGFACTOR_XFOILPAPER_NOT_XFOILV699_IBLSplitAmpLag
    const Tout scc = 5.6;
    const Tout Hkc = Hk - 1.0;
#else
    const Tpi H = thicknessesCoefficientsFunctor.getH();
    const Tpi Hs = thicknessesCoefficientsFunctor.getHs();
    const Tout scc = 5.6*1.333/(1.0+thicknessesCoefficientsFunctor.getUslip(Hk, H, Hs));

    Tout Hkc = 0.0;
    if (profileCat == IBLProfileCategory::TurbulentBL)
    {
      const Tpi Ret = qe*theta11/nue;
      Hkc = Hk - 1.0 - 18.0/Ret;
      if (Hkc < 0.01)
        Hkc = 0.01; //TODO: to be smoothened
    }
    else
      Hkc = Hk - 1.0;
#endif

    const Tout HR = Hkc/(Gacon*ALD*Hk);

    const Tout Slag_turb =
        - 0.5*qe/delta_nom * scc*(pow(ctaueq,0.5) - pow(ctau,0.5)*ALD)
        - duxcon*(0.5*RetCf1*nue/theta11 - qe*pow(HR,2.0)) / (Gbcon*delta1s)
        + duxcon*divq1; // XFOIL, xblsys.f, BLDIF, ~L1783

    src[ir_lag] += -G*divq1 + Slag_turb;
  }
  else if (profileCat == ProfileCategory::LaminarWake)
    SANS_DEVELOPER_EXCEPTION("Laminar wake is not supported!");
#if IsExplicitCheckingIBLSplitAmpLag
  else
    SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");
#endif
}

//----------------------------------------------------------------------------//
// Pseudo transient continuation (PTC) uses this function
template <class VarType>
template <class T, class Tp>
void
PDEIBLSplitAmpLag<PhysD2,VarType>::
speedCharacteristic(
    const ArrayParam<Tp>& params,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& var, Tp& speed ) const
{
  // velocity vector q_1, defined as q_1 = q_i
  VectorX<Tp> q1 = paramInterpret_.getq1(params);

  speed = paramInterpret_.getqe(params); // q_e: edge speed. Equal to magnitude of q_1.

#if 1
  speed = max(speed, 1.0); //TODO: hard-coded so that speed is larger than zero even at stagnation point
#endif
}

//----------------------------------------------------------------------------//
// check state validity
template <class VarType>
template<class T>
bool
PDEIBLSplitAmpLag<PhysD2,VarType>::isValidState( const ArrayQ<T>& var ) const
{
  bool isValidStateFlag = varInterpret_.isValidState(var);

  static_assert(std::is_same<CompressibilityType, IncompressibleBL>::value, "Assumed incompressible shear layer!");
  const T Hk = varInterpret_.getH(var); //TODO: this only works for incompressible flow

  // TODO: should reduce copy-paste in other pde classes
  if (transitionModel_.getProfileCategoryDefault() == IBLProfileCategory::LaminarBL ||
      transitionModel_.getProfileCategoryDefault() == IBLProfileCategory::TurbulentBL)
    isValidStateFlag = isValidStateFlag && (Hk >= 1.02); // BL limit
  else if (transitionModel_.getProfileCategoryDefault() == IBLProfileCategory::TurbulentWake)
    isValidStateFlag = isValidStateFlag && (Hk >= 1.00005); // wake limit

//  const Real Hkmax = 20.0;
  const Real Hkmax = 30.0;
//  const Real Hkmax = 100.0;
//    if (Hk >= Hkmax)
//      SANS_DEVELOPER_EXCEPTION("H is too large!");
  isValidStateFlag = isValidStateFlag && (Hk < Hkmax);

//  isValidStateFlag = isValidStateFlag && (varInterpret_.getG(var) < 0.0);

  return isValidStateFlag;
}

template <class VarType>
template <class T, class Tp>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::updateFraction(
    const ArrayParam<Tp>& params,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& var, const ArrayQ<T>& dvar,
    const Real& maxChangeFraction, Real& updateFraction) const
{
  // analogous implementation can be found in XFOIL, xbl.f SUBROUTINE UPDATE
  // The purpose of this function is to ensure state validity since plain Newton line update does not check validity
  // TODO: can be extended to ensure positive delta*, theta etc.
  const ProfileCategory profileCat = getProfileCategory(var,x);
  varInterpret_.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
}

// set from variable array
template <class VarType>
template <class T, class IBLVarData>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::setDOFFrom( const IBLVarDataType<IBLVarData>& data,
                                    ArrayQ<T>& var ) const
{
  varInterpret_.setDOFFrom( data.cast(), var);
}

// set from variable array
template <class VarType>
template <class IBLVarData>
inline typename PDEIBLSplitAmpLag<PhysD2,VarType>::template ArrayQ<Real>
PDEIBLSplitAmpLag<PhysD2,VarType>::setDOFFrom( const IBLVarDataType<IBLVarData>& data ) const
{
  ArrayQ<Real> var;
  setDOFFrom( data.cast(), var );
  return var;
}

// interpret residuals of the solution variable
template <class VarType>
template<class T>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT( rsdPDEIn.M == N );

  int nOut = rsdPDEOut.m();

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT( nOut == N );
    for ( int i = 0; i < N; i++ )
      rsdPDEOut[i] = rsdPDEIn[i];
  }
}

// interpret residuals of the gradients in the solution variable
template <class VarType>
template<class T>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT( rsdAuxIn.M == N );

  int nOut = rsdAuxOut.m();

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT( nOut == N );
    for ( int i = 0; i < N; i++ )
      rsdAuxOut[i] = rsdAuxIn[i];
  }
}

// interpret residuals at the boundary (should forward to BCs)
template <class VarType>
template<class T>
inline void
PDEIBLSplitAmpLag<PhysD2,VarType>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT( rsdBCIn.M == N );

  int nOut = rsdBCOut.m();

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT( nOut == N );
    for ( int i = 0; i < N; i++ )
      rsdBCOut[i] = rsdBCIn[i];
  }
}

// return the interp type
template <class VarType>
inline typename PDEIBLSplitAmpLag<PhysD2,VarType>::IBLResidualInterpCategory
PDEIBLSplitAmpLag<PhysD2,VarType>::category() const
{
  return rsdCat_;
}

// how many residual equations are we dealing with
template <class VarType>
inline int
PDEIBLSplitAmpLag<PhysD2,VarType>::nMonitor() const
{
  int nMon = -1;

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
    nMon = N;

  return nMon;
}

template <class VarType>
template <class T, class Tp>
inline typename promote_Surreal<T,Tp>::type
PDEIBLSplitAmpLag<PhysD2,VarType>::calcIntermittency(
    const ArrayParam<Tp>& params, const VectorX<Real>& e1,
    const Real& x, const Real& z, const ArrayQ<T>& var) const
{
  typedef typename promote_Surreal<T,Tp>::type Tout;

  const T nt = varInterpret_.getnt(var);
  const ProfileCategory profileCat = getProfileCategory(var,x);
  const Real intermittencyLength = transitionModel_.getIntermittencyLength();

  if ( (profileCat == ProfileCategory::LaminarBL || profileCat == ProfileCategory::TurbulentBL)
       && transitionModel_.isTransitionActive() && intermittencyLength > 0.0 )
  {
    // intermittency attributed to natural transition
    Tout dnt = 0.5*intermittencyLength*ampGrowthRateTurb();
    // TODO: the use of ampGrowthRateTurb() is pragmatic but arbitrary; better/more rigorous choice may exist

    if (dnt < 0.0)
    {
      std::cout << "dnt = " << dnt << std::endl;
      SANS_DEVELOPER_EXCEPTION("dnt < 0.0 is not allowed!");
    }

    const Real dnt_max = (transitionModel_.ntcrit()-transitionModel_.ntinit())*0.9;
    if (dnt > dnt_max) dnt = dnt_max;

    Tout frac = (nt - transitionModel_.ntcrit()) / dnt; // fractional perturbation from equal laminar/turbulent mix

    // intermittency attributed to forced transition
    const Tout frac_trip = (x-transitionModel_.xtr()) / (0.5*intermittencyLength);
    // TODO: a more accurate formulation should be based on the distance to transition front along curved surface rather that just x

    // overall intermittency
    if (frac < frac_trip)
      frac = frac_trip; // activates blending whichever mechanism triggers the first (natural vs forced transition)

    if (frac < -1.0) // fully laminar
      return 0.0;
    else if (frac > 1.0) // fully turbulent
      return 1.0;
    else // blended
      return 0.5*(1.0 + sin(0.5*PI*frac));
//        return 3.0*pow(0.5*frac+0.5,2.0) - 2.0*pow(0.5*frac+0.5,3.0); // the choice does not seem to matter much as long as it's continuous
  }
  else
    return transitionModel_.binaryIntermittency(nt,x);
}

} // namespace SANS

#endif /* SRC_PDE_IBL_PDEIBLSPLITAMPLAG2D_H_ */
