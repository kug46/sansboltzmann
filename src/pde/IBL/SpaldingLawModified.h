// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SPALDINGLAWMODIFIED_H
#define SPALDINGLAWMODIFIED_H

// whether to use IBL3 source code turbulent formulation. If not, then ibl3 writeup
#define ISIBL3SRCTURB 0
//#define ISIBL3SRCTURB 1

#include <limits>

#include "tools/SANSnumerics.h" // Real

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: Modified Spalding law of turbulent velocity profile
//
// Class variables:
//   kappa_             kappa: constant parameter
//   n_                 n: constant parameter.
//   B_                 B: constant parameter.
//   newtonRelTol_;     Relative residual tolerance of Newton solver
//   newtonAbsTol_;     Absolute residual tolerance of Newton solver
// Member functions:
//   .operator          solve the modified Spalding law for u^plus
//   .residual          compute residual and its gradient
//----------------------------------------------------------------------------//
class SpaldingLawModified
{
public:
  //-----------------------Constructors-----------------------//
  // default parameters
#if ISIBL3SRCTURB
  SpaldingLawModified() : SpaldingLawModified(0.40, 5.5, 1e-10, 1e-12, 10) {}
#else
  SpaldingLawModified() : SpaldingLawModified(0.41, 5.0) {}
#endif

  // custom parameters
  SpaldingLawModified(const Real kappa, const Real B,
                      const Real newtonRelTol = 1e-10,
                      const Real newtonAbsTol = 1e-12,
                      const int maxNewtonIter = 20) :
    kappa_(kappa),
    B_(B),
    newtonRelTol_(newtonRelTol),
    newtonAbsTol_(newtonAbsTol),
    maxNewtonIter_(maxNewtonIter) {}

  //-----------------------Members-----------------------//
  // [Input]
  // yplus        y^+: normalized coordinates in wall unit. y^+ = eta * delta.
  // eta          \bar{eta}: scaled normalized wall coordinate
  // [Output]
  // uplus        u^+: solution to the modified Spalding law
  template<class T>
  T wallprofile(const T& yplus, const Real eta) const;

  void getParameter(Real& kappa, Real& B, Real& newtonRelTol, Real& newtonAbsTol, int& maxNewtonIter) const
  {
    kappa = kappa_;
    B = B_;
    newtonRelTol = newtonRelTol_;
    newtonAbsTol = newtonAbsTol_;
    maxNewtonIter = maxNewtonIter_;
  }

protected:
  const Real kappa_;
  const Real B_;
  const Real newtonRelTol_; // relative residual tolerance of Newton solver
  const Real newtonAbsTol_; // absolute residual tolerance of Newton solver
  const int maxNewtonIter_; // maximum number of Newton iterations

  // [Input]
  // yplus        y^+: normalized coordinates in wall unit. y^+ = eta * delta.
  // eta          \bar{eta}: scaled normalized wall coordinate
  // uplus        u^+: solution to the modified Spalding law
  // [Output]
  // R            Residual
  // dRduplus     Residual gradient (d R)/ (d u^+)
  template<class T>
  void residual(const T& yplus, const Real eta, const T& uplus,
                T& R, T& dRduplus) const;
};

}

#endif // SPALDINGLAWMODIFIED_H
