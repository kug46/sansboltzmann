// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_IBLVARDATATYPE2D_H_
#define SRC_PDE_IBL_IBLVARDATATYPE2D_H_

#include <initializer_list>
#include <sstream>

#include "IBL_Traits.h"

namespace SANS
{

//-----------------------parent struct-----------------------//
template<class Derived>
struct IBLVarDataType
{
  // A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//-----------------------derived IBL variable input data types-----------------------//

// Duplicate fields
struct VarData2DDANCt : public IBLVarDataType<VarData2DDANCt>
{
  static const int N = IBLTraits<PhysD2, IBLTwoField>::N;

  VarData2DDANCt(const Real& deltaLami, const Real& ALami,
                 const Real& deltaTurb, const Real& ATurb,
                 const Real& nt, const Real& ctau ) :
    deltaLami_(deltaLami),
    ALami_(ALami),
    deltaTurb_(deltaTurb),
    ATurb_(ATurb),
    nt_(nt),
    ctau_(ctau) {}

  explicit VarData2DDANCt(std::istringstream& ss) :
    deltaLami_(0.0),
    ALami_(0.0),
    deltaTurb_(0.0),
    ATurb_(0.0),
    nt_(0.0),
    ctau_(0.0)
  {
    if ( !( ss >> deltaLami_ >> ALami_ >> deltaTurb_ >> ATurb_ >> nt_ >> ctau_ ) )
    {
      SANS_DEVELOPER_EXCEPTION( "Input string corrupted: ss = %s", ss.str().c_str() );
    }
  }

  VarData2DDANCt& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == N);
    auto q = s.begin();
    deltaLami_ = *(q++);
    ALami_     = *(q++);
    deltaTurb_ = *(q++);
    ATurb_     = *(q++);
    nt_        = *(q++);
    ctau_      = *(q++);
    SANS_ASSERT(q == s.end());

    return *this;
  }

  Real deltaLami_;
  Real ALami_;
  Real deltaTurb_;
  Real ATurb_;
  Real nt_;
  Real ctau_;
};

// Single field
struct VarData2DDAG : public IBLVarDataType<VarData2DDAG>
{
  static const int N = IBLTraits<PhysD2,IBLUniField>::N;

  VarData2DDAG(const Real& delta, const Real& A, const Real& G ) :
    delta_(delta),
    A_(A),
    G_(G) {}

  explicit VarData2DDAG(std::istringstream& ss) :
    delta_(0.0),
    A_(0.0),
    G_(0.0)
  {
    if ( !( ss >> delta_ >> A_ >> G_ ) )
    {
      SANS_DEVELOPER_EXCEPTION( "Input string corrupted: ss = %s", ss.str().c_str() );
    }
  }

  VarData2DDAG& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == N);
    auto q = s.begin();
    delta_ = *(q++);
    A_     = *(q++);
    G_     = *(q++);
    SANS_ASSERT(q == s.end());

    return *this;
  }

  Real delta_;
  Real A_;
  Real G_;
};

// Single field
struct VarData2DDsThG : public IBLVarDataType<VarData2DDsThG>
{
  static const int N = IBLTraits<PhysD2,IBLUniField>::N;

  VarData2DDsThG(const Real& delta1s, const Real& theta11, const Real& G) :
    delta1s_(delta1s),
    theta11_(theta11),
    G_(G) {}

  VarData2DDsThG& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == N);
    auto q = s.begin();
    delta1s_ = *(q++);
    theta11_     = *(q++);
    G_     = *(q++);
    SANS_ASSERT(q == s.end());

    return *this;
  }

  Real delta1s_;
  Real theta11_;
  Real G_;
};

// Two-field
struct VarData2DDsThNCt : public IBLVarDataType<VarData2DDsThNCt>
{
  static const int N = IBLTraits<PhysD2,IBLTwoField>::N;

  VarData2DDsThNCt(const Real& delta1sLami, const Real& theta11Lami,
                   const Real& delta1sTurb, const Real& theta11Turb,
                   const Real& nt, const Real& ctau) :
    delta1sLami_(delta1sLami),
    theta11Lami_(theta11Lami),
    delta1sTurb_(delta1sTurb),
    theta11Turb_(theta11Turb),
    nt_(nt),
    ctau_(ctau) {}

  explicit VarData2DDsThNCt(std::istringstream& ss) :
    delta1sLami_(0.0),
    theta11Lami_(0.0),
    delta1sTurb_(0.0),
    theta11Turb_(0.0),
    nt_(0.0),
    ctau_(0.0)
  {
    if ( !( ss >> delta1sLami_ >> theta11Lami_ >> delta1sTurb_ >> theta11Turb_ >> nt_ >> ctau_ ) )
    {
      SANS_DEVELOPER_EXCEPTION( "Input string corrupted: ss = %s", ss.str().c_str() );
    }
  }

  VarData2DDsThNCt& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == N);
    auto q = s.begin();
    delta1sLami_ = *(q++);
    theta11Lami_ = *(q++);
    delta1sTurb_ = *(q++);
    theta11Turb_ = *(q++);
    nt_          = *(q++);
    ctau_        = *(q++);
    SANS_ASSERT(q == s.end());

    return *this;
  }

  Real delta1sLami_;
  Real theta11Lami_;
  Real delta1sTurb_;
  Real theta11Turb_;
  Real nt_;
  Real ctau_;
};

struct VarData2DDsThNGsplit : public IBLVarDataType<VarData2DDsThNGsplit>
{
  static const int N = IBLTraits<PhysD2,IBLSplitAmpLagField>::N;

  VarData2DDsThNGsplit(const Real& delta1s, const Real& theta11,
                       const Real& nt, const Real& G) :
    delta1s_(delta1s),
    theta11_(theta11),
    nt_(nt),
    G_(G) {}

  explicit VarData2DDsThNGsplit(std::istringstream& ss) :
    delta1s_(0.0),
    theta11_(0.0),
    nt_(0.0),
    G_(0.0)
  {
    if ( !( ss >> delta1s_ >> theta11_ >> nt_ >> G_ ) )
    {
      SANS_DEVELOPER_EXCEPTION( "Input string corrupted: ss = %s", ss.str().c_str() );
    }
  }

  VarData2DDsThNGsplit& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == N);
    auto q = s.begin();
    delta1s_ = *(q++);
    theta11_ = *(q++);
    nt_      = *(q++);
    G_    = *(q++);
    SANS_ASSERT(q == s.end());

    return *this;
  }

  Real delta1s_;
  Real theta11_;
  Real nt_;
  Real G_;
};

}


#endif /* SRC_PDE_IBL_IBLVARDATATYPE2D_H_ */
