// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_VARINTERPRET2D_H_
#define SRC_PDE_IBL_VARINTERPRET2D_H_

// 2-D IBL solution interpreter class
#include <cmath>

#include "IBLVarDataType2D.h"

namespace SANS
{

// IBL solution variable interpreter class
template <class VarType>
class VarInterpret2D;

//----------------------------------------------------------------------------//
// VarInterpret2D<VarTypeDANCt>
//   {BL shape parameters} = (deltaLami, ALami, deltaTurb, ATurb, nt, c_tau)
//----------------------------------------------------------------------------//
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//   VarType                state vector type (e.g. VarTypeDANCt)
//
// IBL solution unknowns (var):
//  [0] deltaLami   laminar boundary layer thickness delta
//  [1] ALami       laminar boundary layer profile shape parameter A
//  [2] deltaTurb   turbulent boundary layer thickness delta
//  [3] ATurb       turbulent boundary layer profile shape parameter A
//  [4] nt          n_tilde, e^N envelope amplification factor
//  [5] c_tau       c_tau, characterizes turbulent shear stress
//
//----------------------------------------------------------------------------//
template <>
class VarInterpret2D<VarTypeDANCt>
{
public:
  typedef PhysD2 PhysDim;
  typedef IBLTwoField IBLFormulationType;

  typedef IBLTraits<PhysDim, IBLFormulationType> IBLTraitsType;

  static const int D = PhysDim::D;
  static const int N = IBLTraitsType::N;

  // unknown variable indexing
  static const int ideltaLami = 0;
  static const int iALami = 1;
  static const int ideltaTurb = 2;
  static const int iATurb = 3;
  static const int i_nt = 4;
  static const int ictau = 5;

  static_assert(ictau == N-1, "Primary unknown index overflows."); // only checks the largest index

  template <class T>
  using ArrayQ = IBLTraitsType::template ArrayQ<T>;    // solution/residual arrays

  //-----------------------Constructors-----------------------//
  VarInterpret2D() {}

  //-----------------------Methods-----------------------//
  // accessors: get individual unknown
  template <class T>
  void getdeltaLami(const ArrayQ<T>& var, T& deltaLami) const { deltaLami = var[ideltaLami]; }

  template <class T>
  T getdeltaLami(const ArrayQ<T>& var) const
  {
    T deltaLami = 0.0;
    getdeltaLami(var,deltaLami);
    return deltaLami;
  }

  template <class T>
  void getALami(const ArrayQ<T>& var, T& ALami) const { ALami = var[iALami]; }

  template <class T>
  T getALami(const ArrayQ<T>& var) const
  {
    T ALami = 0.0;
    getALami(var,ALami);
    return ALami;
  }

  template <class T>
  void getdeltaTurb(const ArrayQ<T>& var, T& deltaTurb) const { deltaTurb = var[ideltaTurb]; }

  template <class T>
  T getdeltaTurb(const ArrayQ<T>& var) const
  {
    T deltaTurb = 0.0;
    getdeltaTurb(var,deltaTurb);
    return deltaTurb;
  }

  template <class T>
  void getATurb(const ArrayQ<T>& var, T& ATurb) const { ATurb = var[iATurb]; }

  template <class T>
  T getATurb(const ArrayQ<T>& var) const
  {
    T ATurb = 0.0;
    getATurb(var,ATurb);
    return ATurb;
  }

  template <class T>
  T getdelta(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const
  {
    T delta = 0.0;
    if (profileCat==IBLProfileCategory::LaminarBL || profileCat==IBLProfileCategory::LaminarWake)
      delta = getdeltaLami(var);
    else if (profileCat==IBLProfileCategory::TurbulentBL || profileCat==IBLProfileCategory::TurbulentWake)
      delta = getdeltaTurb(var);
    else
      SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");

    return delta;
  }

  template <class T>
  T getA(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const
  {
    T A = 0.0;
    if (profileCat==IBLProfileCategory::LaminarBL || profileCat==IBLProfileCategory::LaminarWake)
      A = getALami(var);
    else if (profileCat==IBLProfileCategory::TurbulentBL || profileCat==IBLProfileCategory::TurbulentWake)
      A = getATurb(var);
    else
      SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");

    return A;
  }

  template <class T>
  void getnt(const ArrayQ<T>& var, T& nt) const { nt = var[i_nt]; }

  template <class T>
  T getnt(const ArrayQ<T>& var) const
  {
    T nt = 0.0;
    getnt(var,nt);
    return nt;
  }

#if 1 //TODO: not needed.  maybe remove in the futures
  template <class T>
  void getG(const ArrayQ<T>& var, T& G) const { G = log(var[ictau]); }

  template <class T>
  T getG(const ArrayQ<T>& var) const
  {
    T G = 0.0;
    getG(var,G);
    return G;
  }
#endif

  template <class T>
  void getctau(const ArrayQ<T>& var, T& ctau) const { ctau = var[ictau]; }

  template <class T>
  T getctau(const ArrayQ<T>& var) const
  {
    T ctau = 0.0;
    getctau(var,ctau);
    return ctau;
  }

  // check state validity
  bool isValidState(const ArrayQ<Real>& var) const
  {
    const Real deltaLami = getdeltaLami(var);
    const Real deltaTurb = getdeltaTurb(var);
    const Real ctau = getctau(var);

    // non-negative boundary layer thickness and turbulent shear stress coefficient
    return ((deltaLami > 0) && (deltaTurb > 0) && (ctau > 0));
  }

  // set DOF from data
  template<class T>
  void setDOFFrom(const VarData2DDANCt& data, ArrayQ<T>& var) const
  {
    var[ideltaLami] = data.deltaLami_;
    var[iALami] = data.ALami_;
    var[ideltaTurb] = data.deltaTurb_;
    var[iATurb] = data.ATurb_;
    var[i_nt] = data.nt_;
    var[ictau] = data.ctau_;
  }

  template<class T>
  ArrayQ<T> setDOFFrom(const VarData2DDANCt& data) const
  {
    ArrayQ<T> var;
    setDOFFrom(data, var);
    return var;
  }

  template<class T>
  T dummySource_momLami(const ArrayQ<T>& var) const { return getdeltaLami(var) - deltaLami_dummy_;}

  template<class T>
  T dummySource_keLami(const ArrayQ<T>& var) const { return getALami(var) - ALami_dummy_;}

  template<class T>
  T dummySource_momTurb(const ArrayQ<T>& var) const { return getdeltaTurb(var) - deltaTurb_dummy_;}

  template<class T>
  T dummySource_keTurb(const ArrayQ<T>& var) const { return getATurb(var) - ATurb_dummy_;}

  template<class T>
  T dummySource_amp(const ArrayQ<T>& var) const { return getnt(var) - nt_dummy_;}

  template<class T>
  T dummySource_lag(const ArrayQ<T>& var) const { return getctau(var) - ctauLami_dummy_;}

  template<class T>
  void updateFraction(const IBLProfileCategory& profileCat,
                      const ArrayQ<T>& var, const ArrayQ<T>& dvar,
                      const Real& maxChangeFraction, Real& updateFraction) const
  {
    // Assuming the plain Newton update is var_new = var - dvar
    const ArrayQ<T> pdvar = -dvar; // so that the plain Newton update is var_new = var + pdvar

    Real rlx = 1.0; // under-relaxation factor
    // Adjust rlx so that
    //     rlx * pdvar <= dnvar_max * scale_ref
    // and rlx * pdvar >= dnvar_min * scale_ref

    // Currently, the following thresholds are used instead of maxChangeFraction
    const Real dnvar_max = 2.0; // should be > 0
    const Real dnvar_min = -0.9; // should be  < 0

    // compute dnvar, i.e. dvar normalized (non-dimensionalized) by some scale
    if (profileCat == IBLProfileCategory::LaminarBL ||
        profileCat == IBLProfileCategory::TurbulentBL)
    {
      const T dnvar_nt = getnt(pdvar) / 10.0;
      const T rdnvar_nt = rlx * dnvar_nt; // under-relaxed dnvar
      if (rdnvar_nt > dnvar_max) rlx = dnvar_max / dnvar_nt;
      if (rdnvar_nt < dnvar_min) rlx = dnvar_min / dnvar_nt;
    }

    if (profileCat == IBLProfileCategory::TurbulentBL ||
        profileCat == IBLProfileCategory::TurbulentWake)
    {
      const T dnvar_ctau = getctau(pdvar) / fabs(getctau(var));
      const T rdnvar_ctau = rlx * dnvar_ctau; // under-relaxed dnvar
      if (rdnvar_ctau > dnvar_max) rlx = dnvar_max / dnvar_ctau;
      if (rdnvar_ctau < dnvar_min) rlx = dnvar_min / dnvar_ctau;
    }

    const T dnvar_delta = getdelta(profileCat, pdvar) / fabs(getdelta(profileCat, var));
    const T rdnvar_delta = rlx * dnvar_delta; // under-relaxed dnvar
    if (rdnvar_delta > dnvar_max) rlx = dnvar_max / dnvar_delta;
    if (rdnvar_delta < dnvar_min) rlx = dnvar_min / dnvar_delta;

    const T dnvar_A = getA(profileCat, pdvar) / fabs(getA(profileCat, var)); //TODO: what is A = 0?
    const T rdnvar_A = rlx * dnvar_A; // under-relaxed dnvar
    if (rdnvar_A > dnvar_max) rlx = dnvar_max / dnvar_A;
    if (rdnvar_A < dnvar_min) rlx = dnvar_min / dnvar_A;

#if 0 // turn on for debugging
  SANS_ASSERT_MSG(rlx > 0.0, "rlx = %f is required to be strictly positive!", rlx);
  SANS_ASSERT_MSG(updateFraction > 0.0, "updateFraction = %f is required to be strictly positive!", updateFraction);

  if (rlx < 1.0) std::cout << "Relaxation factor rlx = " << rlx << std::endl;
#endif

    updateFraction = MIN(rlx, updateFraction);
  }

  // The following values are used for setting dummy variables
#if 0 // TODO: closer to realistic solution
  const Real deltaLami_dummy_ = 0.0148449864100258025;
  const Real ALami_dummy_ = 1.65;

  const Real deltaTurb_dummy_ = 0.01303;
  const Real ATurb_dummy_ = 1.365;
  const Real ctauLami_dummy_ = 3.8e-4;
#else
  const Real deltaLami_dummy_ = 0.01;
  const Real ALami_dummy_ = 1.0;
  const Real deltaTurb_dummy_ = 0.02;
  const Real ATurb_dummy_ = 2.0;
  const Real nt_dummy_ = 1.6;
  const Real ctauLami_dummy_ = 1e-3;
#endif
};


//----------------------------------------------------------------------------//
// VarInterpret2D<VarTypeDAG>
//   {BL shape parameters} = (delta, A, G)
//----------------------------------------------------------------------------//
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//   VarType                state vector type (e.g. VarTypeDAG)
//
// IBL solution unknowns (var):
//  [0] delta   boundary layer thickness delta
//  [1] A       boundary layer profile shape parameter A
//  [2] G       =ln(ctau^0.5), where ctau is shear-stress coefficient (turbulent); equivalently, a measure of velocity disturbance in laminar flows.
//
//----------------------------------------------------------------------------//
template <>
class VarInterpret2D<VarTypeDAG>
{
public:
  typedef PhysD2 PhysDim;

  typedef IBLUniField IBLFormulationType;
  typedef IBLTraits<PhysDim, IBLFormulationType> IBLTraitsType;

  static const int D = PhysDim::D;
  static const int N = IBLTraitsType::N;

  // unknown variable indexing
  static const int idelta = 0;
  static const int iA = 1;
  static const int iG = 2;

  static_assert(iG == N-1, "Primary unknown index overflows."); // only checks the largest index

  template <class T>
  using ArrayQ = IBLTraitsType::template ArrayQ<T>;    // solution/residual arrays

  //-----------------------Constructors-----------------------//
  VarInterpret2D() {}

  //-----------------------Methods-----------------------//
  // accessors: get individual unknown
//  template <class T>
//  void getdelta(const ArrayQ<T>& var, T& delta) const { delta = getdelta(var); }

  template <class T>
  T getdelta(const ArrayQ<T>& var) const { return var[idelta]; }

//  template <class T>
//  void getA(const ArrayQ<T>& var, T& A) const { A = getA(var); }

  template <class T>
  T getA(const ArrayQ<T>& var) const { return var[iA]; }

#if 1
  template <class T>
  T getdelta(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const { return getdelta(var); }

  template <class T>
  T getA(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const { return getA(var); }
#else // TODO: hacky; to be cleaned up
  template <class T>
  T getdelta(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const
  {
    if (profileCat == IBLProfileCategory::LaminarBL)
      return getdelta(var)*1.137;
    else
      return getdelta(var);
  }

  template <class T>
  T getA(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const
  {
    if (profileCat == IBLProfileCategory::LaminarBL)
      return getA(var)*1.288;
    else
      return getA(var);
  }
#endif

  template <class T>
  void getG(const ArrayQ<T>& var, T& G) const { G = getG(var); }

  template <class T>
  T getG(const ArrayQ<T>& var) const { return var[iG]; }

//  template <class T>
//  void getctau(const ArrayQ<T>& var, T& ctau) const { ctau = getctau(var); }

  template <class T>
  T getctau(const ArrayQ<T>& var) const { return exp(2.0*getG(var)); }

  template <class T>
  T getG_from_ctau(const T& ctau) const { return 0.5*log(ctau); }

  // check state validity
  bool isValidState(const IBLProfileCategory& profileCatDefault, const ArrayQ<Real>& var) const
  {
    return isValidState(var);
  }

  bool isValidState(const ArrayQ<Real>& var) const
  {
    return (getdelta(var) > 0);
  }

  // set DOF from data
  template<class T>
  void setDOFFrom(const VarData2DDAG& data, ArrayQ<T>& var) const
  {
    var[idelta] = data.delta_;
    var[iA] = data.A_;
    var[iG] = data.G_;
  }

  template<class T>
  ArrayQ<T> setDOFFrom(const VarData2DDAG& data) const
  {
    ArrayQ<T> var;
    setDOFFrom(data, var);
    return var;
  }

  template<class T>
  void updateFraction(const IBLProfileCategory& profileCat,
                      const ArrayQ<T>& var, const ArrayQ<T>& dvar,
                      const Real& maxChangeFraction, Real& updateFraction) const
  {
    SANS_DEVELOPER_EXCEPTION("Not implemented yet!");
  }
};

//----------------------------------------------------------------------------//
// VarInterpret2D<VarTypeDsThG>
//   {BL shape parameters} = (delta_1^*, theta_11, G)
//----------------------------------------------------------------------------//
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//   VarType                state vector type (e.g. VarTypeDAG)
//
// IBL solution unknowns (var):
//  [0] delta_1^*: displacement thickness
//  [1] theta_11:  momentum thickness
//  [2] G       =ln(ctau^0.5), where ctau is shear-stress coefficient (turbulent); equivalently, a measure of velocity disturbance in laminar flows.
//
//----------------------------------------------------------------------------//
template <>
class VarInterpret2D<VarTypeDsThG>
{
public:
  typedef PhysD2 PhysDim;

  typedef IBLUniField IBLFormulationType;
  typedef IBLTraits<PhysDim, IBLFormulationType> IBLTraitsType;

  static const int D = PhysDim::D;
  static const int N = IBLTraitsType::N;

  // unknown variable indexing
  static const int ideltas = 0;
  static const int itheta = 1;
  static const int iG = 2;

  static_assert(iG == N-1, "Primary unknown index overflows."); // only checks the largest index

  template <class T>
  using ArrayQ = IBLTraitsType::template ArrayQ<T>;    // solution/residual arrays

  //-----------------------Constructors-----------------------//
  VarInterpret2D() {}

  //-----------------------Methods-----------------------//
  // accessors: get individual unknown
//  template<class T>
//  void getdelta1s(const ArrayQ<T>& var, T& delta1s) const { delta1s = getdelta1s(var); }

  template<class T>
  T getdelta1s(const ArrayQ<T>& var) const { return var[ideltas]; }

  template <class T>
  T getdelta1s(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const { return getdelta1s(var); }

//  template<class T>
//  void gettheta11(const ArrayQ<T>& var, T& theta11) const { theta11 = gettheta11(var); }

  template<class T>
  T gettheta11(const ArrayQ<T>& var) const { return var[itheta]; }

  template <class T>
  T gettheta11(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const { return gettheta11(var); }

//  template<class T>
//  void getG(const ArrayQ<T>& var, T& G) const { G = getG(var); }

  template<class T>
  T getG(const ArrayQ<T>& var) const { return var[iG]; }

//  template<class T>
//  void getctau(const ArrayQ<T>& var, T& ctau) const { ctau = getctau(var); }

  template<class T>
  T getctau(const ArrayQ<T>& var) const { return exp(2.0*getG(var)); }

  template<class T>
  T getG_from_ctau(const T& ctau) const { return 0.5*log(ctau); }

  template<class T>
  T getH(const ArrayQ<T>& var) const { return getdelta1s(var)/gettheta11(var); }

  // check state validity
  bool isValidState(const IBLProfileCategory& profileCatDefault, const ArrayQ<Real>& var) const
  {
    bool isValidStateFlag = (getdelta1s(var) > 0.0) && (gettheta11(var) > 0.0);

//    isValidStateFlag = isValidStateFlag && (getG(var) < 0.0); // TODO: this is to avoid absurdly large G (aka ctau)

    const Real Hk = getH(var); //TODO: Hk = H; this only works for incompressible flow

    // TODO: should reduce copy-paste in other pde classes
    if (profileCatDefault == IBLProfileCategory::LaminarBL ||
        profileCatDefault == IBLProfileCategory::TurbulentBL)
      isValidStateFlag = isValidStateFlag && (Hk >= 1.02); // BL limit
    else if (profileCatDefault == IBLProfileCategory::TurbulentWake)
      isValidStateFlag = isValidStateFlag && (Hk >= 1.00005); // wake limit

    const Real Hkmax = 30.0;
  //    if (Hk >= Hkmax)
  //      SANS_DEVELOPER_EXCEPTION("H is too large!");
    isValidStateFlag = isValidStateFlag && (Hk < Hkmax);

    return isValidStateFlag;
  }

  // set DOF from data
  template<class T>
  void setDOFFrom(const VarData2DDsThG& data, ArrayQ<T>& var) const
  {
    var[ideltas] = data.delta1s_;
    var[itheta] = data.theta11_;
    var[iG] = data.G_;
  }

  template<class T>
  ArrayQ<T> setDOFFrom(const VarData2DDsThG& data) const
  {
    ArrayQ<T> var = 0.0;
    setDOFFrom(data, var);
    return var;
  }

  template<class T>
  void updateFraction(const IBLProfileCategory& profileCat,
                      const ArrayQ<T>& var, const ArrayQ<T>& dvar,
                      const Real& maxChangeFraction, Real& updateFraction) const
  {
    // Assuming the plain Newton update is var_new = var - dvar
    const ArrayQ<T> pdvar = -dvar; // so that the plain Newton update is var_new = var + pdvar

    Real rlx = 1.0; // under-relaxation factor
    // Adjust rlx so that
    //     rlx * pdvar <= dnvar_max * scale_ref
    // and rlx * pdvar >= dnvar_min * scale_ref

    // Currently, the following thresholds are used instead of maxChangeFraction
#if 1
    const Real dnvar_max = 1.5; // should be > 0
    const Real dnvar_min = -0.5; // should be  < 0
#else
    const Real dnvar_max = 3.0; // should be > 0
    const Real dnvar_min = -0.8; // should be  < 0
#endif

    // compute dnvar, i.e. dvar normalized (non-dimensionalized) by some scale
//    const T dnvar_G = getG(pdvar) / max(fabs(getG(var)), 1.0);
//    const T rdnvar_G = rlx * dnvar_G; // under-relaxed dnvar
//    if (rdnvar_G > dnvar_max) rlx = dnvar_max / dnvar_G;
//    if (rdnvar_G < dnvar_min) rlx = dnvar_min / dnvar_G;

    const T dnvar_theta11 = gettheta11(profileCat, pdvar) / fabs(gettheta11(profileCat, var));
    const T rdnvar_theta11 = rlx * dnvar_theta11; // under-relaxed dnvar
    if (rdnvar_theta11 > dnvar_max) rlx = dnvar_max / dnvar_theta11;
    if (rdnvar_theta11 < dnvar_min) rlx = dnvar_min / dnvar_theta11;

    const T dnvar_delta1s = getdelta1s(profileCat, pdvar) / fabs(getdelta1s(profileCat, var));
    const T rdnvar_delta1s = rlx * dnvar_delta1s; // under-relaxed dnvar
    if (rdnvar_delta1s > dnvar_max) rlx = dnvar_max / dnvar_delta1s;
    if (rdnvar_delta1s < dnvar_min) rlx = dnvar_min / dnvar_delta1s;

#if 0 // turn on for debugging
    SANS_ASSERT_MSG(rlx > 0.0, "rlx = %f is required to be strictly positive!", rlx);
    SANS_ASSERT_MSG(updateFraction > 0.0, "updateFraction = %f is required to be strictly positive!", updateFraction);

    if (rlx < 1.0) std::cout << "Relaxation factor rlx = " << rlx << std::endl;
#endif

    updateFraction = MIN(rlx, updateFraction);

    //TODO: trying to limit Hk by tweaking update in delta1* as in XFOIL? How?
  }
};

//----------------------------------------------------------------------------//
// VarInterpret2D<VarTypeDsThNCt>
//   {BL shape parameters} = (delta1sLami, theta11Lami, delta1sTurb, theta11Turb, nt, ctau)
//----------------------------------------------------------------------------//
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//   VarType                state vector type (e.g. VarTypeDAG)
//
// IBL solution unknowns (var):
//  [0] delta1sLami: displacement thickness
//  [1] theta11Lami: momentum thickness
//  [2] delta1sTurb: displacement thickness
//  [3] theta11Turb: momentum thickness
//  [4] nt:          amplification factor
//  [5] ctau:        ctau is shear-stress coefficient (turbulent)
//
//----------------------------------------------------------------------------//
template <>
class VarInterpret2D<VarTypeDsThNCt>
{
public:
  typedef PhysD2 PhysDim;

  typedef IBLTwoField IBLFormulationType;
  typedef IBLTraits<PhysDim, IBLFormulationType> IBLTraitsType;

  static const int D = PhysDim::D;
  static const int N = IBLTraitsType::N;

  // unknown variable indexing
  static const int ideltasLami = 0;
  static const int ithetaLami = 1;
  static const int ideltasTurb = 2;
  static const int ithetaTurb = 3;
  static const int i_nt = 4;
  static const int ictau = 5;

  static_assert(ictau == N-1, "Primary unknown index overflows."); // only checks the largest index

  template <class T>
  using ArrayQ = IBLTraitsType::template ArrayQ<T>;    // solution/residual arrays

  //-----------------------Constructors-----------------------//
  VarInterpret2D() {}

  //-----------------------Methods-----------------------//
  // accessors: get individual unknown
  template<class T>
  T getdelta1sLami(const ArrayQ<T>& var) const { return var[ideltasLami]; }

  template<class T>
  T gettheta11Lami(const ArrayQ<T>& var) const { return var[ithetaLami]; }

  template<class T>
  T getdelta1sTurb(const ArrayQ<T>& var) const { return var[ideltasTurb]; }

  template<class T>
  T gettheta11Turb(const ArrayQ<T>& var) const { return var[ithetaTurb]; }

  template <class T>
  T getdelta1s(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const
  {
    T delta1s = 0.0;
    if (profileCat==IBLProfileCategory::LaminarBL || profileCat==IBLProfileCategory::LaminarWake)
      delta1s = getdelta1sLami(var);
    else if (profileCat==IBLProfileCategory::TurbulentBL || profileCat==IBLProfileCategory::TurbulentWake)
      delta1s = getdelta1sTurb(var);
    else
      SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");

    return delta1s;
  }

  template <class T>
  T gettheta11(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const
  {
    T theta11 = 0.0;
    if (profileCat==IBLProfileCategory::LaminarBL || profileCat==IBLProfileCategory::LaminarWake)
      theta11 = gettheta11Lami(var);
    else if (profileCat==IBLProfileCategory::TurbulentBL || profileCat==IBLProfileCategory::TurbulentWake)
      theta11 = gettheta11Turb(var);
    else
      SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");

    return theta11;
  }

  template<class T>
  T getnt(const ArrayQ<T>& var) const { return var[i_nt]; }

  template<class T>
  T getctau(const ArrayQ<T>& var) const { return var[ictau]; }

  template<class T>
  T getG(const ArrayQ<T>& var) const { return getG_from_ctau(getctau(var)); }

  template<class T>
  T getG_from_ctau(const T& ctau) const { return 0.5*log(ctau); }

  template<class T>
  T getHLami(const ArrayQ<T>& var) const { return getdelta1sLami(var)/gettheta11Lami(var); }

  template<class T>
  T getHTurb(const ArrayQ<T>& var) const { return getdelta1sTurb(var)/gettheta11Turb(var); }

  // check state validity
  bool isValidState(const ArrayQ<Real>& var) const
  {
    const bool isValid_Lami = (getdelta1sLami(var) > 0.0) && (gettheta11Lami(var) > 0.0) && (getHLami(var) > 1.0);
    const bool isValid_Turb = (getdelta1sTurb(var) > 0.0) && (gettheta11Turb(var) > 0.0) && (getHTurb(var) > 1.0);
    const bool isValid_ctau = (getctau(var) > 0.0);
    return (isValid_Lami && isValid_Turb && isValid_ctau);
  }

  // set DOF from data
  template<class T>
  void setDOFFrom(const VarData2DDsThNCt& data, ArrayQ<T>& var) const
  {
    var[ideltasLami] = data.delta1sLami_;
    var[ithetaLami] = data.theta11Lami_;
    var[ideltasTurb] = data.delta1sTurb_;
    var[ithetaTurb] = data.theta11Turb_;
    var[i_nt] = data.nt_;
    var[ictau] = data.ctau_;
  }

  template<class T>
  ArrayQ<T> setDOFFrom(const VarData2DDsThNCt& data) const
  {
    ArrayQ<T> var = 0.0;
    setDOFFrom(data, var);
    return var;
  }

  template<class T>
   T dummySource_momLami(const ArrayQ<T>& var) const { return getdelta1sLami(var) - delta1sLami_dummy_;}

   template<class T>
   T dummySource_keLami(const ArrayQ<T>& var) const { return gettheta11Lami(var) - theta11Lami_dummy_;}

   template<class T>
   T dummySource_momTurb(const ArrayQ<T>& var) const { return getdelta1sTurb(var) - delta1sTurb_dummy_;}

   template<class T>
   T dummySource_keTurb(const ArrayQ<T>& var) const { return gettheta11Turb(var) - theta11Turb_dummy_;}

   template<class T>
   T dummySource_amp(const ArrayQ<T>& var) const { return getnt(var) - nt_dummy_;}

   template<class T>
   T dummySource_lag(const ArrayQ<T>& var) const { return getctau(var) - ctauLami_dummy_;}

   template<class T>
   void updateFraction(const IBLProfileCategory& profileCat,
                       const ArrayQ<T>& var, const ArrayQ<T>& dvar,
                       const Real& maxChangeFraction, Real& updateFraction) const
   {
     // Assuming the plain Newton update is var_new = var - dvar
     const ArrayQ<T> pdvar = -dvar; // so that the plain Newton update is var_new = var + pdvar

     Real rlx = 1.0; // under-relaxation factor
     // Adjust rlx so that
     //     rlx * pdvar <= dnvar_max * scale_ref
     // and rlx * pdvar >= dnvar_min * scale_ref

     // Currently, the following thresholds are used instead of maxChangeFraction
     const Real dnvar_max = 1.5; // should be > 0
     const Real dnvar_min = -0.5; // should be  < 0

     // compute dnvar, i.e. dvar normalized (non-dimensionalized) by some scale
     if (profileCat == IBLProfileCategory::LaminarBL ||
         profileCat == IBLProfileCategory::TurbulentBL)
     {
       const T dnvar_nt = getnt(pdvar) / 10.0;
       const T rdnvar_nt = rlx * dnvar_nt; // under-relaxed dnvar
       if (rdnvar_nt > dnvar_max) rlx = dnvar_max / dnvar_nt;
       if (rdnvar_nt < dnvar_min) rlx = dnvar_min / dnvar_nt;
     }

     if (profileCat == IBLProfileCategory::TurbulentBL ||
         profileCat == IBLProfileCategory::TurbulentWake)
     {
       const T dnvar_ctau = getctau(pdvar) / fabs(getctau(var));
       const T rdnvar_ctau = rlx * dnvar_ctau; // under-relaxed dnvar
       if (rdnvar_ctau > dnvar_max) rlx = dnvar_max / dnvar_ctau;
       if (rdnvar_ctau < dnvar_min) rlx = dnvar_min / dnvar_ctau;
     }

     const T dnvar_theta11 = gettheta11(profileCat, pdvar) / fabs(gettheta11(profileCat, var));
     const T rdnvar_theta11 = rlx * dnvar_theta11; // under-relaxed dnvar
     if (rdnvar_theta11 > dnvar_max) rlx = dnvar_max / dnvar_theta11;
     if (rdnvar_theta11 < dnvar_min) rlx = dnvar_min / dnvar_theta11;

     const T dnvar_delta1s = getdelta1s(profileCat, pdvar) / fabs(getdelta1s(profileCat, var));
     const T rdnvar_delta1s = rlx * dnvar_delta1s; // under-relaxed dnvar
     if (rdnvar_delta1s > dnvar_max) rlx = dnvar_max / dnvar_delta1s;
     if (rdnvar_delta1s < dnvar_min) rlx = dnvar_min / dnvar_delta1s;

 #if 0 // turn on for debugging
   SANS_ASSERT_MSG(rlx > 0.0, "rlx = %f is required to be strictly positive!", rlx);
   SANS_ASSERT_MSG(updateFraction > 0.0, "updateFraction = %f is required to be strictly positive!", updateFraction);

   if (rlx < 1.0) std::cout << "Relaxation factor rlx = " << rlx << std::endl;
 #endif

     updateFraction = MIN(rlx, updateFraction);

     //TODO: trying to limit Hk by tweaking update in delta1* as in XFOIL? How?
   }

   // The following values are used for setting dummy variables
//   const Real delta1sLami_dummy_ = 0.02;
//   const Real theta11Lami_dummy_ = 0.01;
//   const Real delta1sTurb_dummy_ = 0.06;
//   const Real theta11Turb_dummy_ = 0.03;
   const Real delta1sLami_dummy_ = 0.0025;
   const Real theta11Lami_dummy_ = 0.001;
   const Real delta1sTurb_dummy_ = 0.004;
   const Real theta11Turb_dummy_ = 0.002;
   const Real nt_dummy_ = 1.6;
   const Real ctauLami_dummy_ = 1e-3;
};

//----------------------------------------------------------------------------//
// VarInterpret2D<VarTypeDsThNG>
//   {BL shape parameters} = (delta1sLami, theta11Lami, delta1sTurb, theta11Turb, nt, G)
//----------------------------------------------------------------------------//
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//   VarType                state vector type (e.g. VarTypeDAG)
//
// IBL solution unknowns (var):
//  [0] delta1sLami: displacement thickness
//  [1] theta11Lami: momentum thickness
//  [2] delta1sTurb: displacement thickness
//  [3] theta11Turb: momentum thickness
//  [4] nt:          amplification factor
//  [5] G            ln(ctau^0.5), where ctau is shear-stress coefficient (turbulent);
//                   equivalently, a measure of velocity disturbance in laminar flows.
//
//----------------------------------------------------------------------------//
template <>
class VarInterpret2D<VarTypeDsThNG>
{
public:
  typedef PhysD2 PhysDim;

  typedef IBLTwoField IBLFormulationType;
  typedef IBLTraits<PhysDim, IBLFormulationType> IBLTraitsType;

  static const int D = PhysDim::D;
  static const int N = IBLTraitsType::N;

  // unknown variable indexing
  static const int ideltasLami = 0;
  static const int ithetaLami = 1;
  static const int ideltasTurb = 2;
  static const int ithetaTurb = 3;
  static const int i_nt = 4;
  static const int iG = 5;

  static_assert(iG == N-1, "Primary unknown index overflows."); // only checks the largest index

  template <class T>
  using ArrayQ = IBLTraitsType::template ArrayQ<T>;    // solution/residual arrays

  //-----------------------Constructors-----------------------//
  VarInterpret2D() {}

  //-----------------------Methods-----------------------//
  // accessors: get individual unknown
  template<class T>
  T getdelta1sLami(const ArrayQ<T>& var) const { return var[ideltasLami]; }

  template<class T>
  T gettheta11Lami(const ArrayQ<T>& var) const { return var[ithetaLami]; }

  template<class T>
  T getdelta1sTurb(const ArrayQ<T>& var) const { return var[ideltasTurb]; }

  template<class T>
  T gettheta11Turb(const ArrayQ<T>& var) const { return var[ithetaTurb]; }

  template <class T>
  T getdelta1s(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const
  {
    T delta1s = 0.0;
    if (profileCat==IBLProfileCategory::LaminarBL || profileCat==IBLProfileCategory::LaminarWake)
      delta1s = getdelta1sLami(var);
    else if (profileCat==IBLProfileCategory::TurbulentBL || profileCat==IBLProfileCategory::TurbulentWake)
      delta1s = getdelta1sTurb(var);
    else
      SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");

    return delta1s;
  }

  template <class T>
  T gettheta11(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const
  {
    T theta11 = 0.0;
    if (profileCat==IBLProfileCategory::LaminarBL || profileCat==IBLProfileCategory::LaminarWake)
      theta11 = gettheta11Lami(var);
    else if (profileCat==IBLProfileCategory::TurbulentBL || profileCat==IBLProfileCategory::TurbulentWake)
      theta11 = gettheta11Turb(var);
    else
      SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");

    return theta11;
  }

  template<class T>
  T getnt(const ArrayQ<T>& var) const { return var[i_nt]; }

  template<class T>
  T getG(const ArrayQ<T>& var) const { return var[iG]; }

  template<class T>
  T getG_from_ctau(const T& ctau) const { return 0.5*log(ctau); }

  template<class T>
  T getctau(const ArrayQ<T>& var) const { return exp(2.0*getG(var)); }

  template<class T>
  T getHLami(const ArrayQ<T>& var) const { return getdelta1sLami(var)/gettheta11Lami(var); }

  template<class T>
  T getHTurb(const ArrayQ<T>& var) const { return getdelta1sTurb(var)/gettheta11Turb(var); }

  // check state validity
  bool isValidState(const ArrayQ<Real>& var) const
  {
    const bool isValid_Lami = (getdelta1sLami(var) > 0.0) && (gettheta11Lami(var) > 0.0) && (getHLami(var) >= 1.02);
    const bool isValid_Turb = (getdelta1sTurb(var) > 0.0) && (gettheta11Turb(var) > 0.0) && (getHTurb(var) >= 1.00005);
//    const bool isValid_ctau = (getctau(var) > 0.0); // will automatically be satisfied since ctau = exp(2.0*G)
    return (isValid_Lami && isValid_Turb && (getG(var)<-0.5));
  }

  // set DOF from data
  template<class T>
  void setDOFFrom(const VarData2DDsThNCt& data, ArrayQ<T>& var) const
  {
    var[ideltasLami] = data.delta1sLami_;
    var[ithetaLami] = data.theta11Lami_;
    var[ideltasTurb] = data.delta1sTurb_;
    var[ithetaTurb] = data.theta11Turb_;
    var[i_nt] = data.nt_;
    var[iG] = 0.5*log(data.ctau_);
  }

  template<class T>
  ArrayQ<T> setDOFFrom(const VarData2DDsThNCt& data) const
  {
    ArrayQ<T> var = 0.0;
    setDOFFrom(data, var);
    return var;
  }

  template<class T>
   T dummySource_momLami(const ArrayQ<T>& var) const { return getdelta1sLami(var) - delta1sLami_dummy_;}

   template<class T>
   T dummySource_keLami(const ArrayQ<T>& var) const { return gettheta11Lami(var) - theta11Lami_dummy_;}

   template<class T>
   T dummySource_momTurb(const ArrayQ<T>& var) const { return getdelta1sTurb(var) - delta1sTurb_dummy_;}

   template<class T>
   T dummySource_keTurb(const ArrayQ<T>& var) const { return gettheta11Turb(var) - theta11Turb_dummy_;}

   template<class T>
   T dummySource_amp(const ArrayQ<T>& var) const { return getnt(var) - nt_dummy_;}

   template<class T>
   T dummySource_lag(const ArrayQ<T>& var) const { return getG(var) - GLami_dummy_;}

   template<class T>
   void updateFraction(const IBLProfileCategory& profileCat,
                       const ArrayQ<T>& var, const ArrayQ<T>& dvar,
                       const Real& maxChangeFraction, Real& updateFraction) const
   {
     // Assuming the plain Newton update is var_new = var - dvar
     const ArrayQ<T> pdvar = -dvar; // so that the plain Newton update is var_new = var + pdvar

     Real rlx = 1.0; // under-relaxation factor
     // Adjust rlx so that
     //     rlx * pdvar <= dnvar_max * scale_ref
     // and rlx * pdvar >= dnvar_min * scale_ref

     // Currently, the following thresholds are used instead of maxChangeFraction
     const Real dnvar_max = 1.5; // should be > 0
     const Real dnvar_min = -0.5; // should be  < 0

     // compute dnvar, i.e. dvar normalized (non-dimensionalized) by some scale
     if (profileCat == IBLProfileCategory::LaminarBL ||
         profileCat == IBLProfileCategory::TurbulentBL)
     {
       const T dnvar_nt = getnt(pdvar) / 10.0;
       const T rdnvar_nt = rlx * dnvar_nt; // under-relaxed dnvar
       if (rdnvar_nt > dnvar_max) rlx = dnvar_max / dnvar_nt;
       if (rdnvar_nt < dnvar_min) rlx = dnvar_min / dnvar_nt;
     }

//     if (profileCat == IBLProfileCategory::TurbulentBL ||
//         profileCat == IBLProfileCategory::TurbulentWake)
//     {
//       const T dnvar_G = getG(pdvar) / max(fabs(getG(var)), 1.0);
//       const T rdnvar_G = rlx * dnvar_G; // under-relaxed dnvar
//       if (rdnvar_G > dnvar_max) rlx = dnvar_max / dnvar_G;
//       if (rdnvar_G < dnvar_min) rlx = dnvar_min / dnvar_G;
//     }

     const T dnvar_theta11 = gettheta11(profileCat, pdvar) / fabs(gettheta11(profileCat, var));
     const T rdnvar_theta11 = rlx * dnvar_theta11; // under-relaxed dnvar
     if (rdnvar_theta11 > dnvar_max) rlx = dnvar_max / dnvar_theta11;
     if (rdnvar_theta11 < dnvar_min) rlx = dnvar_min / dnvar_theta11;

     const T dnvar_delta1s = getdelta1s(profileCat, pdvar) / fabs(getdelta1s(profileCat, var));
     const T rdnvar_delta1s = rlx * dnvar_delta1s; // under-relaxed dnvar
     if (rdnvar_delta1s > dnvar_max) rlx = dnvar_max / dnvar_delta1s;
     if (rdnvar_delta1s < dnvar_min) rlx = dnvar_min / dnvar_delta1s;

 #if 0 // turn on for debugging
   SANS_ASSERT_MSG(rlx > 0.0, "rlx = %f is required to be strictly positive!", rlx);
   SANS_ASSERT_MSG(updateFraction > 0.0, "updateFraction = %f is required to be strictly positive!", updateFraction);

   if (rlx < 1.0) std::cout << "Relaxation factor rlx = " << rlx << std::endl;
 #endif

     updateFraction = MIN(rlx, updateFraction);

     //TODO: trying to limit Hk by tweaking update in delta1* as in XFOIL? How?
   }

   // The following values are used for setting dummy variables
   const Real delta1sLami_dummy_ = 0.0025;
   const Real theta11Lami_dummy_ = 0.001;
   const Real delta1sTurb_dummy_ = 0.004;
   const Real theta11Turb_dummy_ = 0.002;
   const Real nt_dummy_ = 1.6;
   const Real GLami_dummy_ = -3.0; // np.exp(-3*2) ~ 2.479e-3
   const Real ctauLami_dummy_ = exp(2.0*GLami_dummy_);
//   const Real ctauLami_dummy_ = 8e-3;
//   const Real GLami_dummy_ = 0.5*log(ctauLami_dummy_);
};

//----------------------------------------------------------------------------//
//
// template parameters:
//   T                      solution DOF data type (e.g. Surreal)
//   VarType                state vector type (e.g. VarTypeDAG)
//
// IBL solution unknowns (var):
//  [0] delta_1^*: displacement thickness
//  [1] theta_11:  momentum thickness
//  [2] nt:        amplification factor
//  [3] G          ln(ctau^0.5), where ctau is shear-stress coefficient (turbulent);
//                 equivalently, a measure of velocity disturbance in laminar flows.
//
//----------------------------------------------------------------------------//
template <>
class VarInterpret2D<VarTypeDsThNGsplit>
{
public:
  typedef PhysD2 PhysDim;

  typedef IBLSplitAmpLagField IBLFormulationType;
  typedef IBLTraits<PhysDim, IBLFormulationType> IBLTraitsType;

  static const int D = PhysDim::D;
  static const int N = IBLTraitsType::N;

  // unknown variable indexing
  static const int ideltas = 0;
  static const int itheta = 1;
  static const int i_nt = 2;
  static const int iG = 3;

  static_assert(iG == N-1, "Primary unknown index overflows."); // only checks the largest index

  template <class T>
  using ArrayQ = IBLTraitsType::template ArrayQ<T>;    // solution/residual arrays

  //-----------------------Constructors-----------------------//
  VarInterpret2D() {}

  //-----------------------Methods-----------------------//
  // accessors: get individual unknown

  template<class T>
  T getdelta1s(const ArrayQ<T>& var) const { return var[ideltas]; }

  template<class T>
  T gettheta11(const ArrayQ<T>& var) const { return var[itheta]; }

  template <class T>
  T getdelta1s(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const { return getdelta1s(var); }

  template <class T>
  T gettheta11(const IBLProfileCategory& profileCat, const ArrayQ<T>& var) const { return gettheta11(var); }

  template<class T>
  T getnt(const ArrayQ<T>& var) const { return var[i_nt]; }

  template<class T>
  T getG(const ArrayQ<T>& var) const { return var[iG]; }

  template<class T>
  T getctau(const ArrayQ<T>& var) const { return exp(2.0*getG(var)); }

  template<class T>
  T getG_from_ctau(const T& ctau) const { return 0.5*log(ctau); }

  template<class T>
  T getH(const ArrayQ<T>& var) const { return getdelta1s(var)/gettheta11(var); }

  // check state validity
  template<class T>
  bool isValidState(const ArrayQ<T>& var) const
  {
    bool isValidStateFlag = (getdelta1s(var) > 0.0) && (gettheta11(var) > 0.0);

    return isValidStateFlag;
  }

  // set DOF from data
  template<class T>
  void setDOFFrom(const VarData2DDsThNGsplit& data, ArrayQ<T>& var) const
  {
    var[ideltas] = data.delta1s_;
    var[itheta] = data.theta11_;
    var[i_nt] = data.nt_;
    var[iG] = data.G_;
  }

  template<class T>
  ArrayQ<T> setDOFFrom(const VarData2DDsThNGsplit& data) const
  {
    ArrayQ<T> var = 0.0;
    setDOFFrom(data, var);
    return var;
  }

  template<class T>
  void updateFraction(const IBLProfileCategory& profileCat,
                      const ArrayQ<T>& var, const ArrayQ<T>& dvar,
                      const Real& maxChangeFraction, Real& updateFraction) const
  {
    // Assuming the plain Newton update is var_new = var - dvar
    const ArrayQ<T> pdvar = -dvar; // so that the plain Newton update is var_new = var + pdvar

    Real rlx = 1.0; // under-relaxation factor in (0,1]
    // Adjust rlx so that
    //     rlx * pdvar <= dnvar_max * scale_ref
    // and rlx * pdvar >= dnvar_min * scale_ref

    // Currently, the following thresholds are used instead of maxChangeFraction
#if 1
    const Real dnvar_max = 1.5; // should be > 0
    const Real dnvar_min = -0.5; // should be  < 0
#else
    const Real dnvar_max = 3.0; // should be > 0
    const Real dnvar_min = -0.8; // should be  < 0
#endif

    // compute dnvar, i.e. dvar normalized (non-dimensionalized) by some scale
    const T dnvar_G = getG(pdvar) / max(fabs(getG(var)), 1.0);
    const T rdnvar_G = rlx * dnvar_G; // under-relaxed dnvar
    if (rdnvar_G > dnvar_max) rlx = dnvar_max / dnvar_G;
    if (rdnvar_G < dnvar_min) rlx = dnvar_min / dnvar_G;

    const T dnvar_theta11 = gettheta11(profileCat, pdvar) / gettheta11(profileCat, var);
    const T rdnvar_theta11 = rlx * dnvar_theta11; // under-relaxed dnvar
    if (rdnvar_theta11 > dnvar_max) rlx = dnvar_max / dnvar_theta11;
    if (rdnvar_theta11 < dnvar_min) rlx = dnvar_min / dnvar_theta11;

    const T dnvar_delta1s = getdelta1s(profileCat, pdvar) / getdelta1s(profileCat, var);
    const T rdnvar_delta1s = rlx * dnvar_delta1s; // under-relaxed dnvar
    if (rdnvar_delta1s > dnvar_max) rlx = dnvar_max / dnvar_delta1s;
    if (rdnvar_delta1s < dnvar_min) rlx = dnvar_min / dnvar_delta1s;

#if 0 // turn on for debugging
    SANS_ASSERT_MSG(rlx > 0.0, "rlx = %f is required to be strictly positive!", rlx);
    SANS_ASSERT_MSG(updateFraction > 0.0, "updateFraction = %f is required to be strictly positive!", updateFraction);

    if (rlx < 1.0) std::cout << "Relaxation factor rlx = " << rlx << std::endl;
#endif

    updateFraction = MIN(rlx, updateFraction);

    //TODO: trying to limit Hk by tweaking update in delta1* as in XFOIL? How?
  }
};

} // namespace SANS

#endif /* SRC_PDE_IBL_VARINTERPRET2D_H_ */
