// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCIBLUniField.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

template ParameterOption<BCIBL2DParamsUniField<BCTypeWakeMatchUniField>::MatchingOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<BCIBL2DParamsUniField<BCTypeWakeMatchUniField>::MatchingOptions> > const&) const;

//===========================================================================//
//
// Parameter classes
// cppcheck-suppress passedByValue
void BCIBL2DParamsUniField<BCTypeWakeMatchUniField>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.matchingType));
  d.checkUnknownInputs(allParams);
}
BCIBL2DParamsUniField<BCTypeWakeMatchUniField> BCIBL2DParamsUniField<BCTypeWakeMatchUniField>::params;

//===========================================================================//
// Instantiate BC parameters
BCPARAMETER_INSTANTIATE( BCIBLUniField2DVector<VarTypeDAG> )
BCPARAMETER_INSTANTIATE( BCIBLUniField2DVector<VarTypeDsThG> )

} //namespace SANS
