// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define SPALDINGLAWMODIFIED_INSTANTIATE
#include "SpaldingLawModified_impl.h"

#include "IBL_Traits.h"

#include "Surreal/SurrealS.h"

namespace SANS
{

// Real
template
Real
SpaldingLawModified::wallprofile<Real>(const Real& yplus, const Real etabar) const;

template
void
SpaldingLawModified::residual<Real>(const Real& yplus, const Real etabar, const Real& uplus,
                                    Real& R, Real& dRduplus) const;

// SurrealQ in 2D
typedef SurrealS<IBLTraits<PhysD2,IBLTwoField>::N> SurrealQ2D;
template
SurrealQ2D
SpaldingLawModified::wallprofile<SurrealQ2D>(const SurrealQ2D& yplus, const Real etabar) const;

template
void
SpaldingLawModified::residual<SurrealQ2D>(const SurrealQ2D& yplus, const Real etabar, const SurrealQ2D& uplus,
                                          SurrealQ2D& R, SurrealQ2D& dRduplus) const;

// Single-field SurrealQ in 2D
typedef SurrealS<IBLTraits<PhysD2,IBLUniField>::N> SurrealUniFieldQ2D;
template
SurrealUniFieldQ2D
SpaldingLawModified::wallprofile<SurrealUniFieldQ2D>(const SurrealUniFieldQ2D& yplus, const Real etabar) const;

template
void
SpaldingLawModified::residual<SurrealUniFieldQ2D>(const SurrealUniFieldQ2D& yplus, const Real etabar, const SurrealUniFieldQ2D& uplus,
                                                  SurrealUniFieldQ2D& R, SurrealUniFieldQ2D& dRduplus) const;


// SurrealParam in 2D
typedef SurrealS<IBLTraits<PhysD2>::Nparam> SurrealParam2D;
template
SurrealParam2D
SpaldingLawModified::wallprofile<SurrealParam2D>(const SurrealParam2D& yplus, const Real etabar) const;

template
void
SpaldingLawModified::residual<SurrealParam2D>(const SurrealParam2D& yplus, const Real etabar, const SurrealParam2D& uplus,
                                              SurrealParam2D& R, SurrealParam2D& dRduplus) const;
}
