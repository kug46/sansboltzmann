// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCIBL2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

template ParameterOption<BCIBL2DParams<BCTypeWakeMatch>::MatchingOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<BCIBL2DParams<BCTypeWakeMatch>::MatchingOptions> > const&) const;

//===========================================================================//
//
// Parameter classes

// cppcheck-suppress passedByValue
void BCIBL2DParams<BCTypeFullState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.isNumericalFlux));
  allParams.push_back(d.checkInputs(params.deltaLami));
  allParams.push_back(d.checkInputs(params.ALami));
  allParams.push_back(d.checkInputs(params.deltaTurb));
  allParams.push_back(d.checkInputs(params.ATurb));
  allParams.push_back(d.checkInputs(params.nt));
  allParams.push_back(d.checkInputs(params.ctau));
  d.checkUnknownInputs(allParams);
}
BCIBL2DParams<BCTypeFullState> BCIBL2DParams<BCTypeFullState>::params;


// cppcheck-suppress passedByValue
void BCIBL2DParams<BCTypeWakeMatch>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.matchingType));
  d.checkUnknownInputs(allParams);
}
BCIBL2DParams<BCTypeWakeMatch> BCIBL2DParams<BCTypeWakeMatch>::params;

//===========================================================================//
// Instantiate BC parameters
BCPARAMETER_INSTANTIATE( BCIBL2DVector<VarTypeDANCt> )
BCPARAMETER_INSTANTIATE( BCIBL2DVector_AirfoilAndWake<VarTypeDANCt> )
BCPARAMETER_INSTANTIATE( BCIBL2DVector_NoWake<VarTypeDANCt> )

BCPARAMETER_INSTANTIATE( BCIBL2DVector<VarTypeDsThNCt> )
BCPARAMETER_INSTANTIATE( BCIBL2DVector<VarTypeDsThNG> )
} //namespace SANS
