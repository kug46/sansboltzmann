// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_VELOCITYPROFILE2D_H_
#define SRC_PDE_IBL_VELOCITYPROFILE2D_H_

#include <cmath>
#include <map>
#include <vector>
#include <iostream>

#include "Surreal/PromoteSurreal.h"

#include "tools/SANSException.h"
#include "tools/SANSnumerics.h"
#include "tools/stringify.h"

#include "IBL_Traits.h"
#include "SpaldingLawModified.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2D velocity profile
//----------------------------------------------------------------------------//

class VelocityProfile2D
{
public:
  // Type of velocity profile information to collect
  enum VelocityInfoType
  {
    U,
    dUdeta
  };

  // no explicit constructor here; use the implicitly-defined constructor

  // generic interface for velocity profile calculations
  template <class T, class Tr>
  void velocityProfile(const IBLProfileCategory profileCat,
                       const T& delta, const T& A, const Tr& Redelta,
                       const std::vector<Real>& eta,
                       std::map<VelocityInfoType, std::vector<typename promote_Surreal<T,Tr>::type>>& velocityInfo) const;

  // laminar profile calculations
  template <class T, class Tmix>
  void LaminarVelocityProfile(const IBLProfileCategory profileCat, const T& A,
                              const std::vector<Real>& eta,
                              std::vector<Tmix>& U, std::vector<Tmix>& dUdeta) const;

  // turbulent profile calculations
  template <class T, class Tr>
  void TurbulentVelocityProfile(const IBLProfileCategory profileCat,
                                const Tr& Redelta, const T& A,
                                const std::vector<Real>& eta,
                                std::vector<typename promote_Surreal<T,Tr>::type>& U) const;

  // regularize/limit BL shape parameter A
  template <class T>
  T regularizeA(const IBLProfileCategory profileCat, const T& A) const;

protected:
  const SpaldingLawModified spalding_;
}; // class VelocityProfile2D


// generic interface for velocity profile calculations
template <class T, class Tr>
void VelocityProfile2D::
velocityProfile(const IBLProfileCategory profileCat,
                const T& delta, const T& A, const Tr& Redelta,
                const std::vector<Real>& eta,
                std::map<VelocityInfoType, std::vector<typename promote_Surreal<T,Tr>::type>>& velocityInfo) const
{
  // invoke calculations based on profile type
  switch (profileCat)
  {
    case IBLProfileCategory::LaminarBL:
    case IBLProfileCategory::LaminarWake:
    {
      LaminarVelocityProfile(profileCat, A, eta, velocityInfo.at(U), velocityInfo.at(dUdeta));
      // Note: use at() to access keys in a map instead of [] operator which add new keys if previously non-existent
      break;
    }
    case IBLProfileCategory::TurbulentBL:
    case IBLProfileCategory::TurbulentWake:
    {
      TurbulentVelocityProfile(profileCat, Redelta, A, eta, velocityInfo.at(U));
      break;
    }
    default:
      SANS_DEVELOPER_EXCEPTION("Unknown Profile Category = %d", profileCat);
  }
}


// laminar profile calculations
template <class T, class Tmix>
void VelocityProfile2D::
LaminarVelocityProfile(const IBLProfileCategory profileCat, const T& A,
                       const std::vector<Real>& eta,
                       std::vector<Tmix>& U, std::vector<Tmix>& dUdeta) const
{
  // check matching input/out vector sizes
  SANS_ASSERT( eta.size() == U.size() );
  SANS_ASSERT( eta.size() == dUdeta.size() );

  // compute U and dUdeta
  for (int i = 0; i < static_cast<int>(eta.size()); i++)
  {
    const T f0 = 6 * pow(eta[i],2) - 8 * pow(eta[i],3) + 3 * pow(eta[i],4);
    const T df0deta = 6 * 2*pow(eta[i],1)  - 8 * 3*pow(eta[i],2)  + 3 * 4*pow(eta[i],3);

    switch (profileCat)
    {
      case IBLProfileCategory::LaminarBL:
      {
#if 1 // IBL3 src
        const Real UK = 0.2;
        const Real ACON = 7.0;

        const T AM = ACON * tanh(A/ACON);

        const T f1 = eta[i] - 3 * pow(eta[i],2) + 3 * pow(eta[i],3) - pow(eta[i],4);
        const T df1deta = 1 - 3 * 2*eta[i] + 3 * 3*pow(eta[i],2) - 4*pow(eta[i],3);

        const T f1m  = -2.0*pow(eta[i],2.0) + 7.0*pow(eta[i],3.0) - 9.0*pow(eta[i],4.0)
                      + 5.0*pow(eta[i],5.0) - pow(eta[i],6.0);
        const T df1mdeta = -2.0 * 2.0*eta[i] + 7.0 * 3.0*pow(eta[i],2.0) - 9.0 * 4.0*pow(eta[i],3.0)
                          + 5.0 * 5.0*pow(eta[i],4.0) - 6.0 * pow(eta[i],5.0);

        U.at(i) = f0 + f1*AM + f1m*UK*pow(AM-2.0,2.0);
        dUdeta.at(i) = df0deta + df1deta*AM + df1mdeta*UK*pow(AM-2.0,2.0);
#else // IBL3 writeup
        const T Abar = regularizeA(profileCat, A);

        const T f1 = eta[i] - 3 * pow(eta[i],2) + 3 * pow(eta[i],3) - pow(eta[i],4);
        const T df1deta = 1 - 3 * 2*eta[i] + 3 * 3*pow(eta[i],2) - 4*pow(eta[i],3);

        U.at(i) = f0 + f1*Abar;
        dUdeta.at(i) = df0deta + df1deta*Abar;
#endif
        break;
      }
      case IBLProfileCategory::LaminarWake:
      {
        const Real ai = 0.125;

        const T Abar = regularizeA(profileCat,A);

        U.at(i) = ai*Abar + (1-ai*Abar)*f0;
        dUdeta.at(i) = (1-ai*Abar)*df0deta;

        break;
      }
      default:
      {
        SANS_DEVELOPER_EXCEPTION("Invalid 2D incompressible laminar velocity profile type = %d", profileCat);
        break;
      }
    }
  }
}


// turbulent profile calculations
template <class T, class Tr>
void VelocityProfile2D::
TurbulentVelocityProfile(const IBLProfileCategory profileCat,
                         const Tr& Redelta, const T& A,
                         const std::vector<Real>& eta,
                         std::vector<typename promote_Surreal<T,Tr>::type>& U) const
{
  typedef typename promote_Surreal<T,Tr>::type Tmix;
  // check matching input/out vector sizes
  SANS_ASSERT( eta.size() == U.size() );

  if (!(Redelta>0))
  {
    const std::string exceptionMsg = "Re_delta = " + stringify(Redelta) + " but is required to be strictly positive!";
    SANS_DEVELOPER_EXCEPTION(exceptionMsg);
  }

  switch (profileCat)
  {
    case IBLProfileCategory::TurbulentBL:
    {
      // parameters
      const T Amod = regularizeA(profileCat,A);

      const Real DviDvw = 1.0;
      const Tmix SAB = fabs(Amod);
      const Tmix DP = pow(DviDvw*Redelta*SAB, 0.5); // delta^plus
      const Tmix UT = Amod/DP; // A/delta^plus

      const Real etaedge = 1.0;                            // eta at edge
      const Tmix UP1 = spalding_.wallprofile(DP, etaedge); // u^plus at edge

#if ISIBL3SRCTURB
      // get UIE
      Tmix UPX = DP/SAB;
      Tmix TUP = pow(tanh(pow(UP1/UPX, 4.0)), 0.25);
      Tmix UPE = UPX*TUP; // Note that here UIE is guaranteed to be in [-1,1]
      Tmix UIE = UT*UPE;
#else
      // get UIE
      Tmix UIE = UT*UP1;

      // assumed polynomial coefficients
      const Real b = 1.0;
      const Real b1 =  3 * (b + 2) * (b + 3) / (b + 7),
                 b2 = -5 * (b + 1) * (b + 3) / (b + 7),
                 b3 =  2 * (b + 1) * (b + 2) / (b + 7);
#endif

      const int n_eta = static_cast<int>(eta.size()); // # of evaluation points; stored to speed up the following for loop
      for (int i = 0; i < n_eta; ++i)
      {
        Tmix yplus = eta[i] * DP;
        Tmix uplus = spalding_.wallprofile(yplus, eta[i]); // solve for u^plus
#if ISIBL3SRCTURB
        Real GO = 3*pow(eta[i], 2) - 2*pow(eta[i], 3);
        Tmix UP = UPX*pow(tanh(pow(uplus/UPX, 4.0)), 0.25);
#else
        Real GO = pow(eta[i], b) * ( b1 * eta[i] + b2 * pow(eta[i], 2) + b3 * pow(eta[i], 3) );
        Tmix UP = uplus;
#endif
        U.at(i) = UT * UP + (1-UIE) * GO;
      }

      break;
    }
    case IBLProfileCategory::TurbulentWake:
    {
      const Tmix Abar = regularizeA(profileCat,A);

      const Real ai = 0.125;
      const Tmix UIE = ai*Abar;

#if ISIBL3SRCTURB
      // nothing
#else
      const Real b = 1.0;
      const Real b1 =  3 * (b + 2) * (b + 3) / (b + 7),
                 b2 = -5 * (b + 1) * (b + 3) / (b + 7),
                 b3 =  2 * (b + 1) * (b + 2) / (b + 7);
#endif

      const int n_eta = static_cast<int>(eta.size()); // # of evaluation points
      for (int i = 0; i < n_eta; ++i)
      {
#if ISIBL3SRCTURB
        Real GO = 3*pow(eta[i], 2) - 2*pow(eta[i], 3);
#else
        Real GO = pow(eta[i], b) * ( b1 * eta[i] + b2 * pow(eta[i], 2) + b3 * pow(eta[i], 3) );
#endif
        U.at(i) = UIE + (1-UIE) * GO;
      }

      break;
    }
    default:
    {
      SANS_DEVELOPER_EXCEPTION("Invalid 2D incompressible turbulent velocity profile type = %d", profileCat);
      break;
    }
  }
}

// regularize/limit BL shape parameter A
template <class T>
T
VelocityProfile2D::
regularizeA(const IBLProfileCategory profileCat, const T& A) const
{
  T Amod = 0;
  switch (profileCat)
  {
    case IBLProfileCategory::LaminarBL:
    case IBLProfileCategory::LaminarWake:
    case IBLProfileCategory::TurbulentWake:
    {
      Amod = (A >= 0)          // \bar{A}: scaled A in case of excessive reverse flow
             ? A
             : A / (1.0 - A);
      break;
    }
    case IBLProfileCategory::TurbulentBL:
    {
      Amod = A;
//      Amod = A / (1.0 + A);
    }
  }

  return Amod;
}

//----------------------------------------------------------------------------//
// 2D density and viscosity profiles: forward declaration
//----------------------------------------------------------------------------//
template<class CompressiblityType>
class DensityProfile2D;

template<class CompressiblityType>
class ViscosityProfile2D;

//----------------------------------------------------------------------------//
// 2D density and viscosity profiles: incompressible BL
//----------------------------------------------------------------------------//
template<>
class DensityProfile2D<IncompressibleBL>
{
public:
  DensityProfile2D() : R_(1.0) {}

  template <class T>
  Real densityProfile(const T& U) const { return R_; }

private:
  const Real R_;
};


template<>
class ViscosityProfile2D<IncompressibleBL> : private DensityProfile2D<IncompressibleBL>
{
public:
  ViscosityProfile2D() : V_(1.0) {}

  template <class T>
  Real viscosityProfile(const T& U) const { return V_; }

private:
  const Real V_;
};

} // namespace SANS

#endif /* SRC_PDE_IBL_VELOCITYPROFILE2D_H_ */
