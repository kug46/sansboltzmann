// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_IBLMISCCLOSURES_H_
#define SRC_PDE_IBL_IBLMISCCLOSURES_H_

#include <cmath>

#define IBLMiscClosures_Debug 0
#if IBLMiscClosures_Debug
#include <iomanip>
#endif

// Miscellaneous closure models for IBL
#include "IBL_Traits.h"

namespace SANS
{

namespace IBLMiscClosures
{

//----------------------------------------------------------------------------//
// TS amplification correlations
namespace AmplificationClosure_IBL
{

template<class T>
T ampGrowthOnsetRamp(const T& Hk, const T& ReTheta)
{
  T lnReThetaInit = 5.738/pow(Hk-1.0,0.43) + 1.612*(tanh(14.0/(Hk-1.0) - 9.24) + 1.0);
#if not IBLMiscClosures_Debug
  return 0.5*( 1.0 + tanh(10.0*(log(ReTheta)-lnReThetaInit)) );
#else
  const Real Ret_min = 1e-2;
  if (ReTheta > Ret_min)
    return 0.5*( 1.0 + tanh(10.0*(log(ReTheta)-lnReThetaInit)) );
  else
  {
    T ramp = 0.5*( 1.0 + tanh(10.0*(            -lnReThetaInit)) );

    std::cout << std::setprecision(16) << std::scientific;
    std::cout << "1.0 >= ReTheta = " << ReTheta << std::endl;
    std::cout << "lnReThetaInit = " << lnReThetaInit << std::endl;
    std::cout << "ramp = " << ramp << std::endl << std::endl;

    return ramp;
  }
#endif
}

template<class T>
T ampGrowthRateUnramped(const T& Hk, const T& theta11)
{
  const T tmp = 1.0/(Hk-1.0);
  T dntdReTheta = 0.028*(Hk-1.0) - 0.0345*exp(-pow(3.87*tmp - 2.52, 2.0));
  T ThetadReThetads = -0.05 + 2.7*tmp - 5.5*pow(tmp, 2.0) + 3.0*pow(tmp, 3.0) + 0.1*exp(-20.0*tmp);

  return (dntdReTheta * ThetadReThetads / theta11);
}

template <class T>
T ampGrowthRate(const T& Hk, const T& ReTheta, const T& theta11)
{
  return (ampGrowthOnsetRamp(Hk,ReTheta) * ampGrowthRateUnramped(Hk, theta11));
}

} // namespace AmplificationClosure_IBL

namespace AmplificationClosure_XfoilOriginal // XFOIL v6.99, DAMPL function (not DAMPL2)
{

template<class T>
T ampGrowthOnsetRamp(const T& Hk, const T& ReTheta)
{
  const T GR = log10(ReTheta);

  const T tmp = Hk-1.0;
  // log10(ReTheta_init) - H correlation. This formula is equivalent of IBL closure up to a scaling factor ln(10)
  T GRInit = 2.492/pow(tmp,0.43) + 0.7*(tanh(14.0/tmp - 9.24) + 1.0);

  const Real DGR = 0.08;
  const T RNORM = (GR - (GRInit-DGR)) / (2.0*DGR);
  if (RNORM < 0.0)
    return 0.0;
  else if (RNORM > 1.0)
    return 1.0;
  else
    return 3.0*pow(RNORM,2.0) - 2.0*pow(RNORM,3.0);
}

template<class T>
T ampGrowthRateUnramped(const T& Hk, const T& theta11)
{
  const T HMI = 1.0/(Hk-1.0);
  const T arg = 3.87*HMI - 2.52;

  T dntdReTheta = 0.028*(Hk-1.0) - 0.0345*exp(-arg*arg); // equivalent of IBL closure
  T thetadRetds = -0.05 + 2.7*HMI - 5.5*pow(HMI, 2.0) + 3.0*pow(HMI, 3.0); // IBL closure has one more term
  return (dntdReTheta * thetadRetds / theta11);
}

template <class T>
T ampGrowthRate(const T& Hk, const T& ReTheta, const T& theta11)
{
  return (ampGrowthOnsetRamp(Hk,ReTheta) * ampGrowthRateUnramped(Hk, theta11));
}

} // namespace AmplificationClosure_XfoilOriginal

template<class T>
T getdeltaNominal(const T& Hk, const T& delta1s, const T& theta11)
{
  T delta_nom = theta11*(3.15 + 1.72/(Hk-1.0)) + delta1s; // nominal boundary layer thickness; MSES
  // clamping delta_nom is needed for Hk really close to 1 or very large Hk
  const Real HDMAX = 12.0; // as in XFOIL v6.99, xblsys.f, ~Line 1111
  if (delta_nom > HDMAX*theta11) // TODO: maybe smoothen this switch (i.e. min function) later?
    delta_nom = HDMAX*theta11;
  return delta_nom;
}

template<class T>
T getctauTurbInit_XFOIL(const T& Hk, const T& ctaueq)
{
  // Post-transition ctau fitting (as in XFOIL v6.99, xblsys.f, ~Line 1394)
  const T CTR = 1.8*exp(-3.3/(Hk-1.0));
  const T ctauTurbInit = pow(CTR,2.0)*ctaueq;
  return ctauTurbInit;
}

} // namespace IBLMiscClosures

} // namespace SANS



#endif /* SRC_PDE_IBL_IBLMISCCLOSURES_H_ */
