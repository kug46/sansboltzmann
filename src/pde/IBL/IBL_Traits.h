// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDE_IBL_IBL_TRAITS_H_
#define PDE_IBL_IBL_TRAITS_H_

#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDEIBL forward declaration
template<class PhysDim, class VarType>
class PDEIBL;

template<class PhysDim, class VarType>
class PDEIBLUniField;

template<class PhysDim, class VarType>
class PDEIBLSplitAmpLag;

//----------------------------------------------------------------------------//
// Primitive variable types
class VarTypeDANCt; // duplicate field
class VarTypeDAG; // unified field
class VarTypeDsThG; // unified field
class VarTypeDsThNCt; // duplicate field
class VarTypeDsThNG; // duplicate field
class VarTypeDsThNGsplit; // for captured transition

template<class PhysDim, class VarType>
struct VarInterpretIBL;

//----------------------------------------------------------------------------//
// Closure type
class ProfileClosure;
class CorrelationClosure;

template<class VarType>
struct IBLClosureType;
template<>
struct IBLClosureType<VarTypeDANCt> { typedef ProfileClosure type; };
template<>
struct IBLClosureType<VarTypeDAG> { typedef ProfileClosure type; };
template<>
struct IBLClosureType<VarTypeDsThG> { typedef CorrelationClosure type; };
template<>
struct IBLClosureType<VarTypeDsThNCt> { typedef CorrelationClosure type; };
template<>
struct IBLClosureType<VarTypeDsThNG> { typedef CorrelationClosure type; };
template<>
struct IBLClosureType<VarTypeDsThNGsplit> { typedef CorrelationClosure type; };

//----------------------------------------------------------------------------//
// struct for indexing IBL unknowns and parameters
class IBLTwoField;
class IBLUniField;
class IBLSplitAmpLagField;

template<class VarType>
struct IBLFormulationType;
template<>
struct IBLFormulationType<VarTypeDANCt> { typedef IBLTwoField type; };
template<>
struct IBLFormulationType<VarTypeDAG> { typedef IBLUniField type; };
template<>
struct IBLFormulationType<VarTypeDsThG> { typedef IBLUniField type; };
template<>
struct IBLFormulationType<VarTypeDsThNCt> { typedef IBLTwoField type; };
template<>
struct IBLFormulationType<VarTypeDsThNG> { typedef IBLTwoField type; };
template<>
struct IBLFormulationType<VarTypeDsThNGsplit> { typedef IBLSplitAmpLagField type; };

template<class PhysDim, class IBLtype>
struct IBLIndexer;

template<>
struct IBLIndexer<PhysD2, IBLTwoField>
{
  static const int N = 6; // unknown and parameter counts
};

template<>
struct IBLIndexer<PhysD2, IBLUniField>
{
  static const int N = 3; // unknown and parameter counts
};

template<>
struct IBLIndexer<PhysD2, IBLSplitAmpLagField>
{
  static const int N = 4; // unknown and parameter counts
};

//----------------------------------------------------------------------------//
template<class PhysDim>
struct IBLParamIndexer;

template<>
struct IBLParamIndexer<PhysD2>
{
  static const int Nparam = 8;
};

//----------------------------------------------------------------------------//
// IBL traits class
template<class PhysDim, class IBLtype = IBLTwoField> //TODO: this default might need to be refactored later
class IBLTraits
{
public:
  static const int D = PhysDim::D; // physical dimension

  static const int N = IBLIndexer<PhysDim, IBLtype>::N;
  static const int Nparam = IBLParamIndexer<PhysDim>::Nparam;

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>; // solution/residual array type

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>; // solution jacobian/mass matrix matrix type

  template <class T>
  using ArrayParam = DLA::VectorS<Nparam,T>; // parameter array type

  template <class T>
  using MatrixParam = DLA::MatrixS<N,Nparam,T>; // e.g. jacobian of IBL PDEs w.r.t. parameters

  template <class T>
  using VectorX = DLA::VectorS<D,T>; // size-D Cartesian physical vector (e.g. basis unit vector) type

  template <class T>
  using TensorX = DLA::MatrixS<D,D,T>; // size-D Cartesian physical tensor (e.g. velocity gradient) type
};

//----------------------------------------------------------------------------//
// Compressibility type identifier
class IncompressibleBL;
#if 0 //TODO: not yet used
class CompressibleBL;
#endif

//----------------------------------------------------------------------------//
// Categorize profiles
enum struct IBLProfileCategory : unsigned int // scoped enumeration
{
  LaminarBL,
  LaminarWake,
  TurbulentBL,
  TurbulentWake
};

}

#endif /* PDE_IBL_IBL_TRAITS_H_ */
