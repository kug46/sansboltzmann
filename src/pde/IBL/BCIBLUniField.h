// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_BCIBLUNIFIELD_H_
#define SRC_PDE_IBL_BCIBLUNIFIELD_H_

#define IsCtauWakeTinitXfoil_UniField 1

#define IS_MATCH_CTAU_INSTEADOF_SQRTCTAU 1 // XFOIL 1989 paper says ctau, but source code v6.99 does sqrt(ctau)

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <string>

#include "pde/BCCategory.h"
#include "pde/BCNone.h"

#include <boost/mpl/vector.hpp>

#include "PDEIBLUniField.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2-D IBL BC class
//
// template parameters:
//   BCType           BC type (e.g. BCTypeInflowSupercritical)
//   VarType          solution variable type (e.g. primitive variable set)
//----------------------------------------------------------------------------//
template <class BCType, class VarType>
class BCIBLUniField2D;

// BCParam: BC parameters
template <class BCType>
struct BCIBL2DParamsUniField;

// BCType: BC types
class BCTypeWakeMatchUniField;

//----------------------------------------------------------------------------//

template<class VarType>
using BCIBLUniField2DVector
    = boost::mpl::vector2< BCIBLUniField2D< BCTypeWakeMatchUniField, VarType >,
                           BCNone<PhysD2,PDEIBLUniField<PhysD2,VarType>::N>
                         >;

//----------------------------------------------------------------------------//
// Matching condition: Trailing edge to wake inlet (singled-field IBL)
//----------------------------------------------------------------------------//
template<>
struct BCIBL2DParamsUniField<BCTypeWakeMatchUniField> : noncopyable
{
  static void checkInputs(PyDict d);

  struct MatchingOptions
  {
    typedef std::string ExtractType;
    const ExtractType trailingEdge = "trailingEdge";
    const ExtractType wakeInflow = "wakeInflow";

    const std::vector<ExtractType> options{trailingEdge,wakeInflow};
  };
  const ParameterOption<MatchingOptions> matchingType{"matchingType", NO_DEFAULT, "Used to specify trailing edge or wake inflow BC"};

  static constexpr const char* BCName{"MatchingUniField"};
  struct Option
  {
    const DictOption MatchingUniField{BCIBL2DParamsUniField::BCName, BCIBL2DParamsUniField::checkInputs};
  };

  static BCIBL2DParamsUniField params;
};

template <class VarType>
class BCIBLUniField2D< BCTypeWakeMatchUniField, VarType > :
    public BCType< BCIBLUniField2D<BCTypeWakeMatchUniField, VarType> >
{
public:
  typedef typename BCCategory::HubTrace Category;
  typedef BCIBL2DParamsUniField<BCTypeWakeMatchUniField> ParamsType;

  typedef PhysD2 PhysDim;
  typedef PDEIBLUniField<PhysDim,VarType> PDEClass;

  static const int D = PDEClass::D;   // physical dimensions
  static const int N = PDEClass::N;   // total solution variables

  static const int NBC = 0; // total BCs

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDEClass::template MatrixQ<T>; // matrices

  template <class T>
  using ArrayParam = typename PDEClass::template ArrayParam<T>;     // solution/residual array type

  template <class TX>
  using VectorX = typename PDEClass::template VectorX<TX>; // size-D vector (e.g. basis unit vector) type

  template <class TX>
  using TensorX = typename PDEClass::template TensorX<TX>; // size-D vector (e.g. basis unit vector) type

  typedef typename PDEClass::ProfileCategory ProfileCategory;

  // cppcheck-suppress noExplicitConstructor
  BCIBLUniField2D( const PDEClass& pde, const std::string& matching ) :
    pde_(pde),
    matching_(matching),
    varInterpret_(pde_.getVarInterpreter()),
    paramInterpret_(pde_.getParamInterpreter())
  {
    SANS_ASSERT_MSG( (matching_ == ParamsType::params.matchingType.trailingEdge) ||
                     (matching_ == ParamsType::params.matchingType.wakeInflow),
                     "matching_ = %s is an unknown matching condition identifier",
                     matching_.c_str());
  }

  BCIBLUniField2D( const PDEClass& pde, const PyDict& d ) :
    BCIBLUniField2D(pde, d.get(ParamsType::params.matchingType)) {}

  virtual ~BCIBLUniField2D() {}
  BCIBLUniField2D& operator=( const BCIBLUniField2D& ) = delete;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const { return pde_.isValidState(qI); }

  template <class T, class Tp, class Tout>
  void fluxMatchingSourceHubtrace(
      const ArrayParam<Tp>& params, const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time,
      const Real& nx, const Real& nz,
      const ArrayQ<T>& var, const ArrayQ<T>& varhb,
      ArrayQ<Tout>& f, ArrayQ<Tout>& hubsource ) const;

  const Real hb_dummy_ = 0.0;

protected:
  const PDEClass& pde_;
  const std::string matching_; // cannot be a reference; otherwise, a derived class may cast it into a different type
  const typename PDEClass::VarInterpType varInterpret_;
  const typename PDEClass::ParamInterpType paramInterpret_;
};

//----------------------------------------------------------------------------//
template <class VarType>
template <class T, class Tp, class Tout>
inline void
BCIBLUniField2D<BCTypeWakeMatchUniField,VarType>::fluxMatchingSourceHubtrace(
    const ArrayParam<Tp>& params, const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time,
    const Real& nx, const Real& nz,
    const ArrayQ<T>& var, const ArrayQ<T>& varhb,
    ArrayQ<Tout>& f, ArrayQ<Tout>& sourcehub ) const
{
  ProfileCategory profileCat = pde_.getProfileCategory(var,x);

  const T G = varInterpret_.getG(var);
  const T ctau = varInterpret_.getctau(var);

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);

  // derived variables
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = (pde_.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
  const Tp nue = (pde_.getViscosityModel()).dynamicViscosity() / rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const auto& thicknessesCoefficientsFunctor
    = (pde_.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tout delta1s  = thicknessesCoefficientsFunctor.getdelta1s();
  const Tout theta11  = thicknessesCoefficientsFunctor.gettheta11();

  if (matching_ == ParamsType::params.matchingType.trailingEdge)
  {
    // ------ hubtrace flux ------ //
    const Tout theta1s  = thicknessesCoefficientsFunctor.gettheta1s();

    // P happens to be symmetric in 2D IBL. Note "*" in () is an outer product
    TensorX<Tout> P = rhoe*theta11*(q1*Transpose(q1)); // [tensor] P: momentum defect flux
    VectorX<Tout> PTdote = Transpose(P)*e1; // P^T dot e

    VectorX<Tout> K = rhoe*pow(qe,2)*theta1s*q1; // [vector] K: kinetic energy defect flux

    const VectorX<Real> nrm = {nx, nz};
    const Tp qn = dot(nrm, q1);

    // compute advective fluxes
    f[PDEClass::ir_mom] += dot(nrm, PTdote);
    f[PDEClass::ir_ke]  += dot(nrm, K);
#if IS_ADJUST_G_FLUX_FOR_CTAU_JUMP_AT_TRANSITION
    if (profileCat == IBLProfileCategory::LaminarBL)
    {
      f[PDEClass::ir_amplag] += G*qn;
    }
    else if (profileCat == IBLProfileCategory::TurbulentBL || profileCat == IBLProfileCategory::TurbulentWake)
    {
      f[PDEClass::ir_amplag] += (G - (pde_.getGTurbInitConst() - pde_.getGcrit()))*qn;
    }
    else
      SANS_DEVELOPER_EXCEPTION("Profile category not supported");
#else
    f[PDEClass::ir_amplag] += G*qn;
#endif

    // ------ hubtrace source ------ //
    sourcehub[PDEClass::ir_mom] += delta1s + fabs(z);
    sourcehub[PDEClass::ir_ke] += theta11;

    if ( profileCat == ProfileCategory::LaminarBL )
    {
#if IsCtauWakeTinitXfoil_UniField
      Tout ctaueq = thicknessesCoefficientsFunctor.getctaueq();
      Tout Hk = thicknessesCoefficientsFunctor.getHk();
      const Tout ctauTinit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk, ctaueq);
#else
      const Real ctauTinit = pow(pde_.getQcrit(), 2.0);
#endif

#if IS_MATCH_CTAU_INSTEADOF_SQRTCTAU
      sourcehub[PDEClass::ir_amplag] += theta11 * ctauTinit; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#else
      sourcehub[PDEClass::ir_amplag] += theta11 * sqrt(ctauTinit); // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#endif
    }
    else if ( profileCat == ProfileCategory::TurbulentBL )
#if IS_MATCH_CTAU_INSTEADOF_SQRTCTAU
      sourcehub[PDEClass::ir_amplag] += theta11 * ctau; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#else
      sourcehub[PDEClass::ir_amplag] += theta11 * sqrt(ctau); // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#endif
    else
      SANS_DEVELOPER_EXCEPTION("Invalid profileCat at trailing edge hubtrace!");
  }
  else if (matching_ == ParamsType::params.matchingType.wakeInflow)
  {
    // ------ hubtrace flux ------ //
    f[PDEClass::ir_mom] += varhb[PDEClass::ir_mom];
    f[PDEClass::ir_ke]  += varhb[PDEClass::ir_ke];
    f[PDEClass::ir_amplag] += varhb[PDEClass::ir_amplag];

    // ------ hubtrace source ------ //
    sourcehub[PDEClass::ir_mom] += - delta1s;
    sourcehub[PDEClass::ir_ke] += - theta11;

    if (profileCat == ProfileCategory::LaminarWake)
      SANS_DEVELOPER_EXCEPTION("Laminar wake not suppported at wake inflow hubtrace!");
    else if ( profileCat == ProfileCategory::TurbulentWake )
#if IS_MATCH_CTAU_INSTEADOF_SQRTCTAU
      sourcehub[PDEClass::ir_amplag] += - theta11 * ctau; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#else
      sourcehub[PDEClass::ir_amplag] += - theta11 * sqrt(ctau); // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#endif
    else
      SANS_DEVELOPER_EXCEPTION("Invalid profileCat at wake inflow hubtrace!");
  }
}

} // namespace SANS


#endif /* SRC_PDE_IBL_BCIBLUNIFIELD_H_ */
