// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(THICKNESSESCOEFFICIENTS2D_CORRELATIONCLOSURE_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "ThicknessesCoefficients2D.h"

namespace SANS
{

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::
getCalculatorThicknessesCoefficients(
    const IBLProfileCategory& profileCat, const ArrayParam<Tparam>& params,
    const ArrayQ<Tibl>& var, const Tparam& nue) const
{
  return Functor<Tparam, Tibl>(profileCat, params, var, nue,
                               varInterpret_, paramInterpret_, nameCDclosure_);
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getdelta1s() const
{
  return varInterpret_.getdelta1s(profileCat_, var_);
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
gettheta11() const
{
  return varInterpret_.gettheta11(profileCat_, var_);
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetheta() const
{
  return paramInterpret_.getqe(params_)*gettheta11()/nue_;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getH() const
{
  return getdelta1s() / gettheta11();
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getHk() const
{
  //TODO: compressibility to be accounted for later
  SANS_ASSERT_MSG((std::is_same<CompressibilityType, IncompressibleBL>::value),
                  "CompressibilityType = %s is required to be IncompressibleBL");
  Tout Hk = getH();

#if 1 //TODO: manual limiting
  if (profileCat_ == IBLProfileCategory::LaminarBL || profileCat_ == IBLProfileCategory::TurbulentBL)
  {
    const Real Hklim = 1.02;
    if (Hk < Hklim)
    {
      std::cout << "Hk = " << Hk << " is too small, and thus clamped to " << Hklim << std::endl;
      Hk = Hklim;
    }
  }
  else if (profileCat_ == IBLProfileCategory::TurbulentWake)
  {
    const Real Hklim = 1.00005;
    if (Hk < Hklim)
    {
      std::cout << "Hk = " << Hk << " is too small, thus clamped to " << Hklim << std::endl;
      Hk = Hklim;
    }
  }
#endif

  return Hk;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getHs() const
{
  Tout Hs = 0.0;
  const Tout Hk = getHk();

#if 1
  if (profileCat_ == IBLProfileCategory::LaminarBL)
    Hs = getHs_Lami(Hk);
  else if (profileCat_ == IBLProfileCategory::TurbulentBL ||
           profileCat_ == IBLProfileCategory::TurbulentWake)
    Hs = getHs_Turb(Hk);

//  std::cout << "different laminar and turbulent H* correlation" << std::endl;
#else // Todo: just hacking to see the effect of discontinuity in Hs flow transition occurs
  Hs = getHs_Lami(Hk);
#endif

  return Hs;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline void
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getHs_derivatives(Tout& Hs_Hk, Tout& Hs_Rt) const
{
  Hs_Hk = 0.0;
  Hs_Rt = 0.0;

  const Tout Hk = getHk();

  if (profileCat_ == IBLProfileCategory::LaminarBL)
  {
    getHs_derivatives_Lami(Hk,Hs_Hk);
    Hs_Rt = 0.0;
  }
  else if (profileCat_ == IBLProfileCategory::TurbulentBL ||
           profileCat_ == IBLProfileCategory::TurbulentWake)
  {
    const Tout Ret = getRetheta();
    getHs_derivatives_Turb(Hk,Ret,Hs_Hk,Hs_Rt);
  }
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
gettheta1s() const
{
  return gettheta11()*getHs();
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline void
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
gettheta1s_derivatives(Tout& theta1s_delta1s, Tout& theta1s_theta11) const
{
  const Tout Hk = getHk();
  const Tout Ret = getRetheta();

  Tout Hs_Hk = 0.0, Hs_Rt = 0.0;
  getHs_derivatives(Hs_Hk, Hs_Rt);

  theta1s_delta1s = Hs_Hk;
  theta1s_theta11 = -Hk*Hs_Hk + Ret*Hs_Rt + getHs();
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getdelta1p() const
{
  //TODO: compressibility to be accounted for later
  SANS_ASSERT_MSG((std::is_same<CompressibilityType, IncompressibleBL>::value),
                  "CompressibilityType = %s is required to be IncompressibleBL");
  return getdelta1s();
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getdeltarho() const
{
  //TODO: compressibility to be accounted for later
  SANS_ASSERT_MSG((std::is_same<CompressibilityType, IncompressibleBL>::value),
                  "CompressibilityType = %s is required to be IncompressibleBL");
  return 0.0;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
gettheta0s() const
{
  return gettheta11() + getdelta1s() - getdeltarho();
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetCf1() const
{
  Tout RetCf1 = 0.0;

  const Tout Hk = getHk();

  if (profileCat_ == IBLProfileCategory::LaminarBL)
    RetCf1 = getRetCf_LamiBL(Hk);
  else if (profileCat_ == IBLProfileCategory::TurbulentBL)
  {
#if 1
    RetCf1 = getRetCf_TurbBL(Hk);

    const Tout RetCf1_Lami = getRetCf_LamiBL(Hk);
    RetCf1 = max(RetCf1, RetCf1_Lami); // use laminar Cf if greater than turbulent Cf, which only occurs for unreasonably small Ret
#else
    RetCf1 = getRetCf_LamiBL(Hk);
#endif
  }
  else if (profileCat_ == IBLProfileCategory::TurbulentWake)
    RetCf1 = 0.0;

  return RetCf1;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetCD() const
{
  Tout RetCD = 0.0;

  const Tout Hk = getHk();

  if (profileCat_ == IBLProfileCategory::LaminarBL)
    RetCD = getRetCD_LamiBL(Hk, getHs());
  else if (profileCat_ == IBLProfileCategory::TurbulentBL || profileCat_ == IBLProfileCategory::TurbulentWake)
  {
    RetCD = getRetCD_Turb(Hk, getH(), getHs());

    if (profileCat_ == IBLProfileCategory::TurbulentWake)
    {
      // laminar-turbulent wake blending
      const Tout RetCD_LamiWake = getRetCD_LamiWake(Hk);
      RetCD = max(RetCD, RetCD_LamiWake); //TODO: to be smoothened

      // double dissipation for wake (aka two wake halves)
      RetCD *= 2.0;
    }
    else
    {
#if 1
//      RetCD = getRetheta() * 2.5e-3; //TODO: hacking for debugging
//      RetCD = getRetheta() * 2e-3; //TODO: hacking for debugging
//      RetCD = getRetheta() * 1.5e-3; //TODO: hacking for debugging
//      RetCD = getRetheta() * 1.2e-3; //TODO: hacking for debugging
//      RetCD = getRetheta() * 1e-3; //TODO: hacking for debugging

      // laminar-turbulent BL blending
      const Tout RetCD_LamiBL = getRetCD_LamiBL(Hk, getHs());
      RetCD = max(RetCD, RetCD_LamiBL); //TODO: to be smoothened
#else
      RetCD = getRetCD_LamiBL(Hk, getHs());
#endif
    }
  }

  return RetCD;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getctaueq() const
{
  const Tout Hk = getHk();
  const Tout H = getH();
  const Tout Hs = getHs();

  // TODO: the following quantity can be pre-computed
  const Real CTCON = 0.5 / (pow(A_GbetaLocus_,2.0) * B_GbetaLocus_); // ~0.015 for A = 6.7 and B = 0.75; XFOIL v6.99, xbl.f ~Line 1614

  const Tout HKB = Hk-1.0;
  Tout HKC = HKB;
#if ISUSEXFOIL_CORRELATIONCLOSURE
  if (profileCat_ == IBLProfileCategory::TurbulentBL)
  {
    const Tout Ret = getRetheta();
    HKC = Hk - 1.0 - 18.0/Ret;
    HKC = max(HKC, Tout(0.01));
  }
#endif

  const Tout Us = getUslip(Hk, H, Hs);

  return CTCON*Hs*HKB*pow(HKC,2.0) / ((1.0-Us)*H*pow(Hk,2.0)); // Ref: equation (24) in (MSES,1987);
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getUslip(const Tout& Hk, const Tout& H, const Tout& Hs) const
{
  Tout Us = 0.5*Hs*(1.0-(Hk-1.0)/(H*B_GbetaLocus_)); // XFOIL v6.99, xblsys.f ~L828

#if ISUSEXFOIL_CORRELATIONCLOSURE // The following are tweaks in XFOIL v6.99
  // set upper limit
  if (profileCat_ == IBLProfileCategory::LaminarBL ||
      profileCat_ == IBLProfileCategory::TurbulentBL)
    Us = min(Us, 0.98); // TODO: to be smoothened
  else if (profileCat_ == IBLProfileCategory::TurbulentWake)
    Us = min(Us, 0.99995); // TODO: to be smoothened
#endif

  return Us;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getHs_Lami(const Tout& Hk) const
{
  Tout Hs = 0.0;
#if ISUSEXFOIL_CORRELATIONCLOSURE
  const Tout tmp = Hk-4.35;
  if (Hk < 4.35)
    Hs = 1.528 + 0.0111*pow(tmp,2.0)/(Hk+1.0)
               - 0.0278*pow(tmp,3.0)/(Hk+1.0)
               - 0.0002*pow(tmp*Hk,2.0);
  else
    Hs = 1.528 + 0.015*pow(tmp,2.0)/Hk;
#else
  if (Hk < 4.0)
    Hs = 1.515 + 0.076*pow(4.0-Hk, 2.0)/Hk;
  else
    Hs = 1.515 + 0.040*pow(Hk-4.0, 2.0)/Hk;
#endif
  return Hs;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline void
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getHs_derivatives_Lami(const Tout& Hk, Tout& Hs_Hk) const
{
  Hs_Hk = 0.0;

#if ISUSEXFOIL_CORRELATIONCLOSURE
  const Tout tmp = Hk-4.35;
  if (Hk < 4.35)
  {
    //    Hs = 1.528 + 0.0111*pow(tmp,2.0)/(Hk+1.0)
    //               - 0.0278*pow(tmp,3.0)/(Hk+1.0)
    //               - 0.0002*pow(tmp*Hk,2.0);
    Hs_Hk = 0.0111*(2.0*tmp - pow(tmp,2.0)/(Hk+1.0))/(Hk+1.0)
          - 0.0278*(3.0*pow(tmp,2.0)-pow(tmp,3.0)/(Hk+1.0))/(Hk+1.0)
          - 0.0004*tmp*Hk*(Hk+tmp);
  }
  else
  {
    const Real Hs2 = 0.015;
    //    Hs = 1.528 + 0.015*pow(tmp,2.0)/Hk;
    Hs_Hk = Hs2*(1.0 - pow(4.35/Hk,2.0));
  }
#else
  SANS_DEVELOPER_EXCEPTION("Not implemented!");
#endif
}


template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getHs_Turb(const Tout& Hk) const
{
  Tout Hs = 0.0;
  const Tout Ret = getRetheta();

  const Tout H0 = (Ret<400) ? Tout(4.0) : 3.0 + 400.0/Ret; //TODO: C0 continuous

#if ISUSEXFOIL_CORRELATIONCLOSURE
  const Tout Retz = (Ret<200.0) ? Tout(200.0) : Ret; //TODO: C0 continuous

  const Real Hs_min = 1.500, dHs = 0.015;

  //TODO: C0 continuous
  if (Hk < H0)
  {
    const Tout Hr = (H0-Hk)/(H0-1.0);
    Hs = Hs_min + 4.0/Retz + (2.0 - Hs_min - 4.0/Retz) * pow(Hr, 2.0) * 1.5/(Hk+0.5);
  }
  else
  {
    const Tout GRT = log(Retz);
    const Tout Hd = Hk-H0;
    const Tout R = Hk - H0 + 4.0/GRT;
    const Tout Ht = 0.007*GRT/pow(R,2.0) + dHs/Hk;

    //    Hs = Hs_min + 4.0/Retz + pow(Hk-H0, 2.0)*(0.007*log(Retz)/pow(Hk-H0+4.0/log(Retz),2.0) + dHs/Hk);
    Hs = Hs_min + 4.0/Retz + pow(Hd,2.0)*Ht;
  }
#else
  if (Hk < H0)
    Hs = 1.505 + 4.0/Ret + (0.165 - 1.6/pow(Ret,0.5))*pow(H0-Hk,1.6)/Hk;
  else
    Hs = 1.505 + 4.0/Ret + pow(Hk-H0, 2.0)*(0.04/Hk + 0.007*log(Ret)/pow(Hk-H0+4.0/log(Ret), 2.0));
#endif
  return Hs;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline void
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getHs_derivatives_Turb(const Tout& Hk, const Tout& Ret, Tout& Hs_Hk, Tout& Hs_Rt) const
{
  Hs_Hk = 0.0;
  Hs_Rt = 0.0;

  const Tout H0 = (Ret<400) ? Tout(4.0) : 3.0 + 400.0/Ret; //TODO: C0 continuous
  const Tout H0_Rt = (Ret<400) ? Tout(0.0) : -400.0/pow(Ret,2.0);

#if ISUSEXFOIL_CORRELATIONCLOSURE
  const Tout Retz = (Ret<200.0) ? Tout(200.0) : Ret; //TODO: C0 continuous
  const Tout Retz_Rt = (Ret<200.0) ? Tout(0.0) : 1.0;

  const Real Hs_min = 1.500, dHs = 0.015;

  //TODO: C0 continuous
  if (Hk < H0)
  {
    const Tout Hr = (H0-Hk)/(H0-1.0);
    const Tout Hr_Hk = -1.0/(H0-1.0);
    const Tout Hr_Rt = (1.0-Hr)/(H0-1.0) * H0_Rt;

//    Hs = Hs_min + 4.0/Retz + (2.0 - Hs_min - 4.0/Retz) * pow(Hr, 2.0) * 1.5/(Hk+0.5);
    Hs_Hk = (2.0 - Hs_min - 4.0/Retz) * 1.5*Hr/(Hk+0.5) * (2.0*Hr_Hk - Hr/(Hk+0.5));
    Hs_Rt = (1.0 - pow(Hr,2.0)*1.5/(Hk+0.5))*(-4.0/pow(Retz,2.0))*Retz_Rt
          + (2.0 - Hs_min - 4.0/Retz) * 1.5/(Hk+0.5) * 2.0*Hr * Hr_Rt;
  }
  else
  {
    const Tout GRT = log(Retz);
    const Tout Hd = Hk-H0;
    const Tout R = Hk - H0 + 4.0/GRT;
    const Tout Ht = 0.007*GRT/pow(R,2.0) + dHs/Hk;
    const Tout Ht_Hk = -0.014*GRT/pow(R,3.0) - dHs/pow(Hk,2.0);
    const Tout Ht_Rt = 0.007/pow(R,2.0)/Retz*Retz_Rt - 0.014*GRT/pow(R,3.0)*(-H0_Rt - 4.0/pow(GRT,2.0)/Retz*Retz_Rt);

    //    Hs = Hs_min + 4.0/Retz + pow(Hk-H0, 2.0)*(0.007*log(Retz)/pow(Hk-H0+4.0/log(Retz),2.0) + dHs/Hk);
    //    Hs = Hs_min + 4.0/Retz + pow(Hd,2.0)*Ht;
    Hs_Hk = 2.0*Hd*Ht + pow(Hd,2.0)*Ht_Hk;
    Hs_Rt = 2.0*Hd*Ht*(-H0_Rt) + pow(Hd,2.0)*Ht_Rt - 4.0/pow(Retz,2.0)*Retz_Rt;
  }
#else
  SANS_DEVELOPER_EXCEPTION("Not implemented!");
#endif
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetCf_LamiBL(const Tout& Hk) const
{
  Tout RetCf1 = 0.0;
#if ISUSEXFOIL_CORRELATIONCLOSURE
  if (Hk < 5.5)
    RetCf1 = 0.0727*pow(5.5-Hk, 3.0)/(Hk+1.0) - 0.07;
  else
    RetCf1 = 0.015*pow(1.0 - 1.0/(Hk-4.5), 2.0) - 0.07;
#else
  if (Hk < 7.4)
    RetCf1 = -0.067 + 0.01977*pow(7.4-Hk, 2.0)/(Hk-1.0);
  else
    RetCf1 = -0.067 + 0.022*pow(1.0-1.4/(Hk-6.0), 2.0);

  RetCf1 *= 2.0;
#endif

  return RetCf1;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetCf_TurbBL(const Tout& Hk) const
{
  Tout RetCf1 = 0.0;

  //TODO: compressibility to be accounted for later
  SANS_ASSERT_MSG((std::is_same<CompressibilityType, IncompressibleBL>::value),
                  "CompressibilityType = %s is required to be IncompressibleBL");
  const Real Fc = 1.0; // compressibility correction factor

  const Tout Ret = getRetheta();

  Tout GRT = log(Ret/Fc);
#if ISUSEXFOIL_CORRELATIONCLOSURE
  GRT = max(GRT, Tout(3.0)); //TODO: smoothing?
#endif
  const Tout GEX = -1.74 - 0.31*Hk;

  Tout ARG = -1.33*Hk;
#if ISUSEXFOIL_CORRELATIONCLOSURE
  ARG = max(ARG, -20.0); //TODO: smoothing?
#endif

  const Tout THK = tanh(4.0 - Hk/0.875);

  RetCf1 = 0.3*exp(ARG)*pow(GRT/log(10.0), GEX) + 1.1e-4 * (THK - 1.0);
  RetCf1 *= Ret/Fc;

  return RetCf1;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetCD_LamiBL(const Tout& Hk, const Tout& Hs) const
{
  Tout RetCD = 0.0;
  if (Hk < 4.0)
    RetCD = 0.207 + 0.00205*pow(4.0-Hk, 5.5);
  else
  {
    const Tout HKBSQ = pow(Hk-4.0, 2.0);
#if ISUSEXFOIL_CORRELATIONCLOSURE
    RetCD = 0.207 - 0.0016*HKBSQ/(1.0+0.02*HKBSQ);
#else
    RetCD = 0.207 - 0.003*HKBSQ/(1.0+0.02*HKBSQ);
#endif
  }

  RetCD *= 0.5*Hs;

  return RetCD;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetCD_LamiWake(const Tout& Hk) const
{
  return 1.10*pow(1.0 - 1.0/Hk, 2.0)/Hk;
}

template<class VarType, class CompressibilityType>
template<class Tparam, class Tibl>
inline typename ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::template Functor<Tparam, Tibl>::Tout
ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>::Functor<Tparam,Tibl>::
getRetCD_Turb(const Tout& Hk, const Tout& H, const Tout& Hs) const
{
  const Tout Ret = getRetheta();
  const Tout RetCf1 = getRetCf1();

  const Tout Us = getUslip(Hk, H, Hs);
  const Tibl ctau = varInterpret_.getctau(var_);

  Tout RetCD = 0.0;

#if ISUSEXFOIL_CORRELATIONCLOSURE
  const Tout Hkmin = 1.0 + 2.1/log(Ret); // minimum Hk for wake layer to still exist
  const Tout dfac = 0.5 + 0.5*tanh((Hk-1.0)/(Hkmin-1.0));

  RetCD = 0.5*RetCf1*Us*dfac + Ret*ctau*(0.995-Us);
  // Add laminar stress contribution to outer layer CD.  XFOIL v6.99, xblsys.f ~Line 1027
  RetCD += 0.15*pow(0.995-Us, 2.0);
#else
  RetCD = 0.5*RetCf1*Us + Ret*ctau*(1.0-Us); // MSES paper Eqn (20)
#endif

  return RetCD;
}

} // namespace SANS
