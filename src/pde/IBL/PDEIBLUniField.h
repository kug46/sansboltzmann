// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_PDEIBLUNIFIELD_H_
#define SRC_PDE_IBL_PDEIBLUNIFIELD_H_

#define ISIBLLFFLUX_PDEIBLUniField 1 // true if LF flux scheme is used; false if full upwinding is used

#define IsExplicitCheckingUniFieldIBL 1

#define IS_BINARY_INTERMITTENCY_PDEIBLUNIFIELD 1 // true if using binary intermittency; otherwise, blended intermittency is used

#define IS_LAGFACTOR_XFOILPAPER_NOT_XFOILV699_IBLUniField 1
// determines which lag factor to use: true if using the XFOIL paper; false if using XFOIL v6.99 source code

#define IS_ADJUST_G_FLUX_FOR_CTAU_JUMP_AT_TRANSITION 0

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"   // required for matrix transpose
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"                      // required for dot product
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Surreal/PromoteSurreal.h"

#include "tools/smoothmath.h"

#include "IBL_Traits.h"

#include "IBLMiscClosures.h"
#include "FluidModels.h"
#include "VarInterpret2D.h"
#include "QauxvInterpret2D.h"
#include "ThicknessesCoefficients2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D IBL
//
// Reference: Shun's Master thesis and the IBL2 working document
//
// template parameters:
//   T                        solution DOF data type (e.g. double, SurrealS)
//   VarType                  solution variable type set (e.g. primitive variables, conservative variables)
//   ProfileType              boundary layer profile type (e.g. laminar vs turbulent, incompressible vs compressible)
//
// Nodal solution variable vector (var):
//  Depends on the definition in solution interpreter class (VarType).
//
// Residual vector R
//  [0] R^(e)                 momentum equation
//  [1] R^(*)                 kinetic energy equation
//  [2] R^(n) or R^(ctau)     amplification factor growth equation (laminar), or shear stress transport equation (turbulent)
//
// member functions:
//   .hasFluxConservative        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//   .hasSource                  T/F: PDE has source term
//   .hasForcingFunction         T/F: PDE has forcing function term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .source                     solution-dependent sources: S(X, Q, QX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

// PDEIBL2D class definition
template <class VarType>   // required for compiler to understand that a template is used
class PDEIBLUniField<PhysD2, VarType>
{
public:

  enum IBLResidualInterpCategory
  {
    IBL_ResidInterp_Raw
  };

  typedef PhysD2 PhysDim;
  typedef IBLUniField IBLFormulationType;
  typedef IBLTraits<PhysDim, IBLFormulationType> IBLTraitsType;

  static const int D = IBLTraitsType::D;           // physical dimensions
  static const int N = IBLTraitsType::N;           // total solution variables
  static const int Nparam = IBLTraitsType::Nparam; // total solution variables

  // equation indexing
  static const int ir_mom = 0;
  static const int ir_ke = 1;
  static const int ir_amplag = 2;

  static_assert(ir_mom < N, "Residual equation index overflows.");
  static_assert(ir_ke < N, "Residual equation index overflows.");
  static_assert(ir_amplag < N, "Residual equation index overflows.");

  template <class T>
  using ArrayQ = IBLTraitsType::template ArrayQ<T>;     // solution/residual array type

  template <class T>
  using MatrixQ = IBLTraitsType::template MatrixQ<T>;   // matrix type

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class TP>
  using ArrayParam = IBLTraitsType::template ArrayParam<TP>;     // solution/residual array type

  template <class TP>
  using MatrixParam = IBLTraitsType::template MatrixParam<TP>;   // matrix type

  template <class TX>
  using VectorX = IBLTraitsType::VectorX<TX>; // size-D vector (e.g. basis unit vector) type

  template <class TX>
  using TensorX = IBLTraitsType::TensorX<TX>; // size-D vector (e.g. basis unit vector) type

  typedef VarInterpret2D<VarType> VarInterpType; // solution variable interpreter type
  typedef QauxvInterpret2D ParamInterpType;

  typedef VelocityProfile2D VelocityProfileType;
  typedef IBLProfileCategory ProfileCategory;
  typedef GasModelIBL GasModelType;
  typedef ViscosityModel<ViscosityConstant> ViscosityModelType;
  typedef TransitionModel TransitionModelType;

  typedef typename IBLClosureType<VarType>::type ClosureType;
  typedef IncompressibleBL CompressibilityType; // TODO: currently assume incompressible viscous layer
  typedef ThicknessesCoefficients2D<ClosureType, VarType, CompressibilityType> ThicknessesCoefficientsType;

  template<class Tparam, class Tibl>
  using ThicknessesCoefficientsFunctor = typename ThicknessesCoefficientsType::template Functor<Tparam, Tibl>;

  //-----------------------Constructors-----------------------//
  PDEIBLUniField( const GasModelType& gasModel,
                  const ViscosityModelType& viscosityModel,
                  const TransitionModelType& transitionModel,
                  const PyDict d_thicknessesCoefficients = PyDict(),
                  const IBLResidualInterpCategory rsdCat=IBL_ResidInterp_Raw ) :
    gasModel_(gasModel),
    viscosityModel_(viscosityModel),
    transitionModel_(transitionModel),
    varInterpret_(),
    paramInterpret_(),
    thicknessesCoefficientsObj_(varInterpret_, paramInterpret_, d_thicknessesCoefficients),
    rsdCat_(rsdCat)
  {
    SANS_ASSERT( rsdCat_ == IBL_ResidInterp_Raw );
  }

  //-----------------------Default class methods-----------------------//
  ~PDEIBLUniField() {}
  PDEIBLUniField( const PDEIBLUniField& ) = delete;
  PDEIBLUniField& operator=( const PDEIBLUniField& )  = delete;

  //-----------------------Members-----------------------//
  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  bool needsSolutionGradientforSource() const { return (hasSource() && false); }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tp, class Tout>
  void fluxAdvectiveTime(const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                         const Real& x, const Real& z, const ArrayQ<T>& var, ArrayQ<Tout>& ft ) const
  {
    Tout ftLocal = 0;
    masterState(params, e1, x, z, var, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const;

  // master state: U(Q)
  template <class T, class Tp, class Tout>
  void masterState( const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                    const Real& x, const Real& z, const ArrayQ<T>& var,
                    ArrayQ<Tout>& uCons ) const;

  template <class T, class Tp, class Tout>
  void masterState( const ProfileCategory profileCat, const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                    const Real& x, const Real& z, const ArrayQ<T>& var,
                    ArrayQ<Tout>& uCons ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState( const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime( const ArrayQ<T>& var, const ArrayQ<T>& vart, ArrayQ<T>& strongPDE ) const;

  // advective flux: F(Q)
  template <class T, class Tp, class Tout>
  void fluxAdvective(
      const ArrayParam<Tp>& params, const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time, const ArrayQ<T>& var,
      ArrayQ<Tout>& fx, ArrayQ<Tout>& fz ) const;

  template <class T, class Tp, class Tout>
  void fluxAdvective(
      const ProfileCategory profileCat, const ArrayParam<Tp>& params, const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time, const ArrayQ<T>& var,
      ArrayQ<Tout>& fx, ArrayQ<Tout>& fz ) const;

  template <class TL, class TR, class Tp, class Tout>
  void fluxAdvectiveUpwind(
      const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
      const VectorX<Real>& e1L, const VectorX<Real>& e1R,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
      const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
      ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const;

  template <class T>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<T>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var,
      MatrixQ<T>& ax, MatrixQ<T>& ay ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var, const Real& nx, const Real& ny,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class T, class Tg, class Tp, class Tout>
  void fluxViscous(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      ArrayQ<Tout>& f, ArrayQ<Tout>& g ) const;

  template <class T, class Tg, class Tp, class Tout>
  void fluxViscous(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& varL, const ArrayQ<Tg>& varxL, const ArrayQ<Tg>& varyL,
      const ArrayQ<T>& varR, const ArrayQ<Tg>& varxR, const ArrayQ<Tg>& varyR,
      const Real& nx, const Real& ny,
      ArrayQ<Tout>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class T, class Tg, class Tp, class Tk>
  void diffusionViscous(
      const ArrayParam<Tp>& params, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, MatrixQ<T>& ax, MatrixQ<T>& ay ) const;

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      const ArrayQ<T>& varxx,
      const ArrayQ<T>& varxy, const ArrayQ<T>& varyy,
      ArrayQ<T>& strongPDE ) const;


  // solution-dependent source: S(X, Q, QX)
  template <class T, class Tg, class Tp, class Tout>
  void source(
      const ArrayParam<Tp>& params,
      const VectorX<Real>& e1, const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& var_x, const ArrayQ<Tg>& var_z,
      ArrayQ<Tout>& src) const;

  template <class T, class Tg, class Tp, class Tout>
  void source(
      const ProfileCategory profileCat, const ArrayParam<Tp>& params,
      const VectorX<Real>& e1, const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& var_x, const ArrayQ<Tg>& var_z,
      ArrayQ<Tout>& src) const;

  // dual-consistent source
  template <class T>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<T>& varL, const ArrayQ<T>& varxL, const ArrayQ<T>& varyL,
      const ArrayQ<T>& varR, const ArrayQ<T>& varxR, const ArrayQ<T>& varyR,
      ArrayQ<T>& sourceL, ArrayQ<T>& sourceR ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tg, class Tp>
  void jacobianSource(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ< typename promote_Surreal<T,Tg,Tp>::type >& dsdu ) const {}
  //TODO: Empty now since the analytic jacobian is not readily available.
  //      But may need to fill this up later for better robustness using pseudo time continuation


  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tg, class Tp>
  void jacobianSourceAbsoluteValue(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ< typename promote_Surreal<T,Tg,Tp>::type >& dsdu ) const {}

  // right-hand-side forcing function
  template <class T, class Tp>
  void forcingFunction( const ArrayParam<Tp>& params, const Real& x, const Real& y, const Real& time,
                        ArrayQ<T>& forcing ) const {}

  // characteristic speed (needed for timestep)
  template <class T, class Tp>
  void speedCharacteristic(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, Tp& speed ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& var ) const;

  // update fraction needed for physically valid state
  template <class T>
  void updateFraction(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& dq,
      Real maxChangeFraction, Real& updateFraction ) const;

  template <class T, class Tp>
  void updateFraction(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& dq,
      const Real& maxChangeFraction, Real& updateFraction ) const;

  // set from variable array
  template<class T, class IBLVarData>
  void setDOFFrom( const IBLVarDataType<IBLVarData>& data, ArrayQ<T>& var ) const;

  // set from variable array
  template<class IBLVarData>
  ArrayQ<Real> setDOFFrom( const IBLVarDataType<IBLVarData>& data ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the interp type
  IBLResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // print out parameters
  void dump( int indentSize, std::ostream& out = std::cout ) const;

  // access members
  const GasModelType& getGasModel() const { return gasModel_; }
  const ViscosityModelType& getViscosityModel() const { return viscosityModel_; }
  const TransitionModelType& getTransitionModel() const { return transitionModel_; }
  const ThicknessesCoefficientsType& getSecondaryVarObj() const { return thicknessesCoefficientsObj_; }
  const VarInterpType& getVarInterpreter() const { return varInterpret_; }
  const ParamInterpType& getParamInterpreter() const { return paramInterpret_; }

  // evaluate parameters
  void evalParam( Real& gam, Real& R, Real& mue) const
  {
    gam = gasModel_.gamma();
    R = gasModel_.R();
    mue = viscosityModel_.dynamicViscosity();
  }

  // compute intermittency
  template <class T, class Tp>
  typename promote_Surreal<T,Tp>::type
  calcIntermittency(const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                    const Real& x, const Real& z, const ArrayQ<T>& var) const
  {
    const T nt = getnt(var);

#if IS_BINARY_INTERMITTENCY_PDEIBLUNIFIELD && 0
    return transitionModel_.binaryIntermittency(nt,x);
#else
    const ProfileCategory profileCat = getProfileCategory(var,x);
    const Real intermittencyLength = transitionModel_.getIntermittencyLength();

    if ( (profileCat == ProfileCategory::LaminarBL || profileCat == ProfileCategory::TurbulentBL)
         && transitionModel_.isTransitionActive() && intermittencyLength > 0.0 )
    {
      // intermittency attributed to natural transition
      const Real dnt = 7.0;
      T frac = (nt - transitionModel_.ntcrit()) / dnt; // fractional perturbation from equal laminar/turbulent mix

      // intermittency attributed to forced transition
      const Real frac_trip = (x-transitionModel_.xtr()) / (0.5*intermittencyLength);
      // TODO: a more accurate formulation should be based on the distance to transition front along curved surface rather that just x

      // overall intermittency
      if (frac < frac_trip)
        frac = frac_trip; // activates blending whichever mechanism triggers the first (natural vs forced transition)

      if (frac < -1.0) // fully laminar
        return 0.0;
      else if (frac > 1.0) // fully turbulent
        return 1.0;
      else // blended
        return 0.5*(1.0 + sin(0.5*PI*frac));
  //        return 3.0*pow(0.5*frac+0.5,2.0) - 2.0*pow(0.5*frac+0.5,3.0); // the choice does not seem to matter much as long as it's continuous
    }
    else
      return transitionModel_.binaryIntermittency(nt,x);
#endif
  }

  // determine profile/closure category/type
  template <class T>
  ProfileCategory getProfileCategory(const ArrayQ<T>& var, const Real& x) const
  {
    const T& nt = getnt(var);
    return transitionModel_.getProfileCategory(nt,x);
  }

#if 1 //TODO: to be refactored
  template <class T>
  T getnt(const T& ctau) const { return 0.5*log(ctau) - (log(Qcrit_) - transitionModel_.ntcrit()); }

  template <class T>
  T getnt(const ArrayQ<T>& var) const { return getnt(varInterpret_.getctau(var)); }

  template <class T>
  T getG_from_nt(const T& nt) const { return nt + log(Qcrit_) - transitionModel_.ntcrit(); }
#endif

  Real getQcrit() const { return Qcrit_; }
  Real getGcrit() const { return log(Qcrit_); }

  Real getGTurbInitConst() const { return varInterpret_.getG_from_ctau(ctauTurbInitConst_); }

protected:
  const GasModelType& gasModel_;
  const ViscosityModelType& viscosityModel_;
  const TransitionModelType& transitionModel_;

  const VarInterpType varInterpret_; // solution variable interpreter object
  const ParamInterpType paramInterpret_;// parameter interpreter object
  const ThicknessesCoefficientsType thicknessesCoefficientsObj_; // secondary variable object
  const IBLResidualInterpCategory rsdCat_; // ResidInterp type identifier

//  const Real ctauTurbInitConst_ = 1e-4;
//  const Real ctauTurbInitConst_ = 5e-4;
//  const Real ctauTurbInitConst_ = 1e-3;
//  const Real ctauTurbInitConst_ = 3e-3;
//  const Real ctauTurbInitConst_ = 4e-3;
//  const Real ctauTurbInitConst_ = 5e-3;
  const Real ctauTurbInitConst_ = 8e-3;
//  const Real ctauTurbInitConst_ = 10.e-3;

  const Real Qcrit_ = 1.e-2; // = (q'/q_e)_crit = ctau_crit^0.5 (at transition)
//  const Real Qcrit_ = 3.e-2; // = (q'/q_e)_crit = ctau_crit^0.5 (at transition)
#if 1 //TODO: the following are tried for debugging and failed. To be removed
//  const Real Qcrit_ = 3.5e-2; // = (q'/q_e)_crit = ctau_crit^0.5 (at transition)
//  const Real Qcrit_ = 4.e-2; // = (q'/q_e)_crit = ctau_crit^0.5 (at transition)
  //  const Real Qcrit_ = 5.e-2; // = (q'/q_e)_crit = ctau_crit^0.5 (at transition)
//  const Real Qcrit_ = 8.9e-2; // = (q'/q_e)_crit = ctau_crit^0.5 (at transition)
//  const Real Qcrit_ = 0.1; // = (q'/q_e)_crit = ctau_crit^0.5 (at transition)
#endif
};

//---------------------Member function definitions of PDEIBL2D---------------------//

// unsteady conservative flux: U(Q) (aka conservative variables/quantities)
template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBLUniField<PhysD2, VarType>::
masterState( const ArrayParam<Tp>& params, const VectorX<Real>& e1,
             const Real& x, const Real& z, const ArrayQ<T>& var,
             ArrayQ<Tout>& uCons ) const
{
  const Tout gamma = calcIntermittency(params, e1, x, z, var);
  const ProfileCategory profileCat = getProfileCategory(var,x);

  if (0.0 < gamma &&  gamma < 1.0) // mixture of laminar and turbulent
  {
    ArrayQ<Tout> uConsLami = 0.0, uConsTurb = 0.0;

    if (profileCat == ProfileCategory::LaminarBL ||
        profileCat == ProfileCategory::TurbulentBL)
    {
      masterState(ProfileCategory::LaminarBL, params, e1, x, z, var, uConsLami);
      masterState(ProfileCategory::TurbulentBL, params, e1, x, z, var, uConsTurb);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Blending should be used for BL, not wake");

    uCons = (1.0-gamma)*uConsLami + gamma*uConsTurb;
  }
  else // either laminar or turbulent
    masterState(profileCat, params, e1, x, z, var, uCons);
}

template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBLUniField<PhysD2, VarType>::
masterState(const ProfileCategory profileCat, const ArrayParam<Tp>& params, const VectorX<Real>& e1,
            const Real& x, const Real& z, const ArrayQ<T>& var,
            ArrayQ<Tout>& uCons) const
{
  typedef typename ThicknessesCoefficientsFunctor<Tp, T>::Tout Tpi;

  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Tp nue = viscosityModel_.dynamicViscosity() / rhoe;

  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tpi delta1s  = thicknessesCoefficientsFunctor.getdelta1s();
  const Tpi deltarho = thicknessesCoefficientsFunctor.getdeltarho();
  const Tpi theta0s   = thicknessesCoefficientsFunctor.gettheta0s();

  const VectorX<Tout> p = rhoe*(delta1s-deltarho)*paramInterpret_.getq1(params); // momentum defect
  const Tout k = rhoe*pow(qe,2)*theta0s; //kinetic energy defect

  // master state of momentum and kinetic energy equations
  uCons[ir_mom] = dot(p,e1);
  uCons[ir_ke] = k;
#if IS_ADJUST_G_FLUX_FOR_CTAU_JUMP_AT_TRANSITION
  if (profileCat == IBLProfileCategory::LaminarBL)
    uCons[ir_amplag] = varInterpret_.getG(var);
  else if (profileCat == IBLProfileCategory::TurbulentBL || profileCat == IBLProfileCategory::TurbulentWake)
    uCons[ir_amplag] = varInterpret_.getG(var) - (getGTurbInitConst() - getGcrit());
  else
    SANS_DEVELOPER_EXCEPTION("Profile category not supported");
#else
  uCons[ir_amplag] = varInterpret_.getG(var);
#endif
}

//----------------------------------------------------------------------------//
template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBLUniField<PhysD2, VarType>::fluxAdvective(
    const ArrayParam<Tp>& params, const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time, const ArrayQ<T>& var,
    ArrayQ<Tout>& fx, ArrayQ<Tout>& fz ) const
{
  const Tout gamma = calcIntermittency(params, e1, x, z, var);
  const ProfileCategory profileCat = getProfileCategory(var,x);

  if (0.0 < gamma &&  gamma < 1.0) // mixture of laminar and turbulent
  {
    ArrayQ<Tout> fxLami = 0.0, fxTurb = 0.0;
    ArrayQ<Tout> fzLami = 0.0, fzTurb = 0.0;

    if (profileCat == ProfileCategory::LaminarBL ||
        profileCat == ProfileCategory::TurbulentBL)
    {
      fluxAdvective(ProfileCategory::LaminarBL, params, e1, x, z, time, var, fxLami, fzLami);
      fluxAdvective(ProfileCategory::TurbulentBL, params, e1, x, z, time, var, fxTurb, fzTurb);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Blending should be used for BL, not wake");

    fx += (1.0-gamma)*fxLami + gamma*fxTurb;
    fz += (1.0-gamma)*fzLami + gamma*fzTurb;
  }
  else // either laminar or turbulent
    fluxAdvective(profileCat, params, e1, x, z, time, var, fx, fz);
}

template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBLUniField<PhysD2, VarType>::fluxAdvective(
    const ProfileCategory profileCat, const ArrayParam<Tp>& params, const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time, const ArrayQ<T>& var,
    ArrayQ<Tout>& fx, ArrayQ<Tout>& fz ) const
{
  const T G = varInterpret_.getG(var);

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Tp nue = viscosityModel_.dynamicViscosity() / rhoe;

  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tout theta11 = thicknessesCoefficientsFunctor.gettheta11();
  const Tout theta1s = thicknessesCoefficientsFunctor.gettheta1s();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  // P happens to be symmetric in 2D IBL. Note "*" in () is an outer product
  const TensorX<Tout> P = rhoe*theta11*(q1*Transpose(q1)); // [tensor] P: momentum defect flux
  const VectorX<Tout> PTdote = Transpose(P)*e1; // P^T dot e

  const VectorX<Tout> K = rhoe*pow(qe,2)*theta1s*q1; // [vector] K: kinetic energy defect flux

  // UPDATE/OUTPUT ADVECTIVE FLUX TERMS -----------------------------------------------------------
  fx[ir_mom] += PTdote(0);
  fz[ir_mom] += PTdote(1);

  fx[ir_ke] += K(0);
  fz[ir_ke] += K(1);

#if IS_ADJUST_G_FLUX_FOR_CTAU_JUMP_AT_TRANSITION
  if (profileCat == IBLProfileCategory::LaminarBL)
  {
    fx[ir_amplag] += G * q1[0];
    fz[ir_amplag] += G * q1[1];
  }
  else if (profileCat == IBLProfileCategory::TurbulentBL || profileCat == IBLProfileCategory::TurbulentWake)
  {
    fx[ir_amplag] += (G - (getGTurbInitConst() - getGcrit())) * q1[0];
    fz[ir_amplag] += (G - (getGTurbInitConst() - getGcrit())) * q1[1];
  }
  else
    SANS_DEVELOPER_EXCEPTION("Profile category not supported");
#else
  fx[ir_amplag] += G * q1[0];
  fz[ir_amplag] += G * q1[1];
#endif
}

//----------------------------------------------------------------------------//
template <class VarType>
template <class TL, class TR, class Tp, class Tout>
inline void
PDEIBLUniField<PhysD2,VarType>::fluxAdvectiveUpwind(
    const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
    const VectorX<Real>& e1L, const VectorX<Real>& e1R,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
    const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
    ArrayQ<Tout>& fL, ArrayQ<Tout>& fR ) const
{
  // left contributions
  ArrayQ<Tout> fxL = 0.0, fzL = 0.0;
#if ISIBLLFFLUX_PDEIBLUniField
  ArrayQ<Tout> uConsL = 0.0, uConsR = 0.0;
#endif
  {
    const Tout gammaL = calcIntermittency(paramsL, e1L, x, z, varL);
    const ProfileCategory profileCatL = getProfileCategory(varL,x);

    if (0.0 < gammaL &&  gammaL < 1.0) // mixture of laminar and turbulent
    {
      ArrayQ<Tout> fxL_Lami = 0.0, fzL_Lami = 0.0;
      ArrayQ<Tout> fxL_Turb = 0.0, fzL_Turb = 0.0;
#if ISIBLLFFLUX_PDEIBLUniField
      ArrayQ<Tout> uConsL_Lami = 0.0, uConsL_Turb = 0.0;
#endif

      if (profileCatL == ProfileCategory::LaminarBL ||
          profileCatL == ProfileCategory::TurbulentBL)
      {
        fluxAdvective(ProfileCategory::LaminarBL, paramsL, e1L, x, z, time, varL, fxL_Lami, fzL_Lami);
        fluxAdvective(ProfileCategory::TurbulentBL, paramsL, e1L, x, z, time, varL, fxL_Turb, fzL_Turb);
#if ISIBLLFFLUX_PDEIBLUniField
        masterState(ProfileCategory::LaminarBL, paramsL, e1L, x, z, varL, uConsL_Lami);
        masterState(ProfileCategory::TurbulentBL, paramsL, e1L, x, z, varL, uConsL_Turb);
#endif
      }
      else
        SANS_DEVELOPER_EXCEPTION("Blending should be used for BL, not wake");

      fxL += (1.0-gammaL)*fxL_Lami + gammaL*fxL_Turb;
      fzL += (1.0-gammaL)*fzL_Lami + gammaL*fzL_Turb;
#if ISIBLLFFLUX_PDEIBLUniField
      uConsL += (1.0-gammaL)*uConsL_Lami + gammaL*uConsL_Turb;
#endif
    }
    else // either laminar or turbulent
    {
      fluxAdvective(profileCatL, paramsL, e1L, x, z, time, varL, fxL, fzL);
#if ISIBLLFFLUX_PDEIBLUniField
      masterState(profileCatL, paramsL, e1L, x, z, varL, uConsL);
#endif
    }
  }

  // right contributions
  ArrayQ<Tout> fxR = 0.0, fzR = 0.0;
  {
    const Tout gammaR = calcIntermittency(paramsR, e1R, x, z, varR);
    const ProfileCategory profileCatR = getProfileCategory(varR,x);

    if (0.0 < gammaR &&  gammaR < 1.0) // mixture of laminar and turbulent
    {
      ArrayQ<Tout> fxR_Lami = 0.0, fzR_Lami = 0.0;
      ArrayQ<Tout> fxR_Turb = 0.0, fzR_Turb = 0.0;
#if ISIBLLFFLUX_PDEIBLUniField
      ArrayQ<Tout> uConsR_Lami = 0.0, uConsR_Turb = 0.0;
#endif

      if (profileCatR == ProfileCategory::LaminarBL ||
          profileCatR == ProfileCategory::TurbulentBL)
      {
        fluxAdvective(ProfileCategory::LaminarBL, paramsR, e1R, x, z, time, varR, fxR_Lami, fzR_Lami);
        fluxAdvective(ProfileCategory::TurbulentBL, paramsR, e1R, x, z, time, varR, fxR_Turb, fzR_Turb);
#if ISIBLLFFLUX_PDEIBLUniField
        masterState(ProfileCategory::LaminarBL, paramsR, e1R, x, z, varR, uConsR_Lami);
        masterState(ProfileCategory::TurbulentBL, paramsR, e1R, x, z, varR, uConsR_Turb);
#endif
      }
      else
        SANS_DEVELOPER_EXCEPTION("Blending should be used for BL, not wake");

      fxR += (1.0-gammaR)*fxR_Lami + gammaR*fxR_Turb;
      fzR += (1.0-gammaR)*fzR_Lami + gammaR*fzR_Turb;
    }
    else // either laminar or turbulent
    {
      fluxAdvective(profileCatR, paramsR, e1R, x, z, time, varR, fxR, fzR);
#if ISIBLLFFLUX_PDEIBLUniField
      masterState(profileCatR, paramsR, e1R, x, z, varR, uConsR);
#endif
    }
  }

  // assemble final numerical flux
#if ISIBLLFFLUX_PDEIBLUniField
  Tp qeL = paramInterpret_.getqe(paramsL);
  Tp qeR = paramInterpret_.getqe(paramsR);
  Tp alpha = std::max( fabs(qeL), fabs(qeR) );  // dissipation coefficient

  const ArrayQ<Tout> f_unique = 0.5*(fxL*nxL + fzL*nzL + fxR*nxR + fzR*nzR) + 0.5*alpha*(uConsL - uConsR);
  fL += f_unique;
  fR += f_unique;
#else
  const VectorX<Real> nrmL = {nxL, nzL}; // trace normal vector
  const VectorX<Tp> q1L = paramInterpret_.getq1(paramsL);
  const Tp qnL = dot(q1L, nrmL);

  const ArrayQ<Tout> f_unique = (qnL > 0) ? fxL*nxL + fzL*nzL : fxR*nxR + fzR*nzR;

  fL += f_unique;
  fR += f_unique;
#endif
}

//----------------------------------------------------------------------------//
// solution-dependent source: S(X, Q, QX)
template<class VarType>
template <class T, class Tg, class Tp, class Tout>
inline void
PDEIBLUniField<PhysD2,VarType>::source(
    const ArrayParam<Tp>& params,
    const VectorX<Real>& e1, const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<T>& var, const ArrayQ<Tg>& var_x, const ArrayQ<Tg>& var_z,
    ArrayQ<Tout>& src) const
{
  const Tout gamma = calcIntermittency(params, e1, x, z, var);
  const ProfileCategory profileCat = getProfileCategory(var,x);

  if (0.0 < gamma &&  gamma < 1.0) // mixture of laminar and turbulent
  {
    ArrayQ<Tout> srcLami = 0.0, srcTurb = 0.0;

    if (profileCat == ProfileCategory::LaminarBL ||
        profileCat == ProfileCategory::TurbulentBL)
    {
      source(ProfileCategory::LaminarBL, params, e1, e1_x, e1_z, x, z, time, var, var_x, var_z, srcLami);
      source(ProfileCategory::TurbulentBL, params, e1, e1_x, e1_z, x, z, time, var, var_x, var_z, srcTurb);
    }
    else
      SANS_DEVELOPER_EXCEPTION("Blending should be used for BL, not wake");

    src += (1.0-gamma)*srcLami + gamma*srcTurb;
  }
  else // either laminar or turbulent
    source(profileCat, params, e1, e1_x, e1_z, x, z, time, var, var_x, var_z, src);
}

template<class VarType>
template <class T, class Tg, class Tp, class Tout>
inline void
PDEIBLUniField<PhysD2,VarType>::source(
    const ProfileCategory profileCat, const ArrayParam<Tp>& params,
    const VectorX<Real>& e1, const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<T>& var, const ArrayQ<Tg>& var_x, const ArrayQ<Tg>& var_z,
    ArrayQ<Tout>& src) const
{
  typedef typename ThicknessesCoefficientsFunctor<Tp, T>::Tout Tpi;

  const T nt = getnt(var);
  const T G = varInterpret_.getG(var);
  const T ctau = varInterpret_.getctau(var);

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);
  const TensorX<Tp> nablasq1 = paramInterpret_.getgradq1(params);
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Real mue = viscosityModel_.dynamicViscosity();
  const Tp nue = mue / rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tpi delta1s = thicknessesCoefficientsFunctor.getdelta1s();
  const Tpi theta11 = thicknessesCoefficientsFunctor.gettheta11();
  const Tpi RetCf1 = thicknessesCoefficientsFunctor.getRetCf1();
  const Tpi RetCDiss = thicknessesCoefficientsFunctor.getRetCD();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  // P is symmetric in 2D IBL. Note "*" in () is an outer product
  const TensorX<Tout> P = rhoe*theta11*(q1*Transpose(q1)); // [tensor] P: momentum defect flux

  // (P  dot  nablas)  dot  e1 (as in IBL3) or trace(P^T  (nablas e1)) as in Shun's Master thesis
  const Tout Pdotnablasdote1 = P(0,0)*e1_x[0] + P(1,0)*e1_x[1]
                             + P(0,1)*e1_z[0] + P(1,1)*e1_z[1];

  const VectorX<Tout> tauw = (0.5*mue/theta11*RetCf1)*q1; // tau_w: wall shear stress

  const VectorX<Tout> M = rhoe*delta1s*q1; // mass flux defect

  const Tout Diss = mue*pow(qe,2.0)*RetCDiss/theta11; // {\cal D}: dissipation integral

  // -- momentum and kinetic energy equation source terms --
  src[ir_mom] += -Pdotnablasdote1 + dot(e1, nablasq1*M) - dot(tauw,e1); // source corresponding to R^e in momentum residual
#if 0 // TODO: turn on if flow is compressible
  const Tout delta1p = secondaryVarFcn.getdelta1p();
  const VectorX<Tout> Q = delta1p*q1; // Q: velocity defect
  const VectorX<Tout> D = M - rhoe*Q; // D: density defect flux

#if IsExplicitCheckingUniFieldIBL
  if ( dot(D,D) > 100*std::numeric_limits<Real>::epsilon() )
    SANS_DEVELOPER_EXCEPTION("D should be zero for incompressible flow.");
#endif

  const VectorX<Tp> nablasqsq = 2.0*Transpose(nablasq1)*q1; // [vector] nablas(q^2): surface gradient of q^2
  // note that nablas(q^2) = nablas(q1 . q1) = 2 nablasq1^T . q1 = 2 q1 . nablasq1

  src[ir_ke]  += dot(D, nablasqsq) - 2.0*Diss; // source corresponding to R^* in kinetic energy residual
#else
  src[ir_ke] += -2.0*Diss; // source corresponding to R^* in kinetic energy residual
#endif

  // -- amplification or lag equations source terms --
  const Tp divq1 = paramInterpret_.getdivq1(params);
  const Tpi Hk = thicknessesCoefficientsFunctor.getHk(); // kinematic shape factor

  if (profileCat == ProfileCategory::LaminarBL)
  {
    const Tpi Ret = qe*theta11/nue;

    // source term intended to impose the inflow (at stagnation point) boundary condition on n_tilde
    // TODO: this correction along with the diffusion time scale works pretty well with ninit_=0 but a bit worse when ninit is nonzero
    // reference time scale: diffusion time scale: small near stagnation point and larger as BL thickens
#if 1
    Tout t_ref = pow(theta11,2.0)/nue;
#else // TODO: hack for old pyrite checks
    Tout t_ref = pow(varInterpret_.getdelta(profileCat,var),2.0)/nue; // TODO: only works when delta is a primary unknown;  to be removed
#endif
    Tout q_ref = 1.0; // TODO: hard-coded for now

#if 1 // The two correlations are almost identical
    // cut off inflow-BC source after amplification factor growth source term starts to ramp up
    const Tout Samp_infowbc = (1.0 - IBLMiscClosures::AmplificationClosure_IBL::ampGrowthOnsetRamp(Hk,Ret))
        *1.0/t_ref * exp(-pow(qe/q_ref,2.0)) * (nt-transitionModel_.ntinit()); // TODO: the sign is currently positive; to be examined

    const Tout amprate = IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRate(Hk,Ret,theta11);
#else
    // cut off inflow-BC source after amplification factor growth source term starts to ramp up
    const Tout Samp_infowbc = (1.0 - IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthOnsetRamp(Hk,Ret))
        *1.0/t_ref * exp(-pow(qe/q_ref,2.0)) * (nt-transitionModel_.ntinit()); // TODO: the sign is currently positive; to be examined

    const Tout amprate = IBLMiscClosures::AmplificationClosure_XfoilOriginal::ampGrowthRate(Hk,Ret,theta11);
#endif

    src[ir_amplag] += -G*divq1 - qe*amprate + Samp_infowbc;
  }
  else if (profileCat == ProfileCategory::TurbulentBL ||
           profileCat == ProfileCategory::TurbulentWake)
  {
    // -- lag equation source terms --
    const Tout ctau_eq = thicknessesCoefficientsFunctor.getctaueq();
    const Tout delta_nom = IBLMiscClosures::getdeltaNominal(Hk, delta1s, theta11);

#if 1
    // dissipation length ratio wall/wake (XFOIL v6.99)
    const Real ALD = (profileCat == ProfileCategory::TurbulentWake) ? 0.9 : 1.0;

    const Real Gacon = thicknessesCoefficientsFunctor.getA_GbetaLocus();
    const Real Gbcon = thicknessesCoefficientsFunctor.getB_GbetaLocus();
    const Real duxcon = 1.0;

#if IS_LAGFACTOR_XFOILPAPER_NOT_XFOILV699_IBLUniField
    const Tout scc = 5.6;
    const Tout Hkc = Hk - 1.0;
#else
    const Tpi H = thicknessesCoefficientsFunctor.getH();
    const Tpi Hs = thicknessesCoefficientsFunctor.getHs();
    const Tout scc = 5.6*1.333/(1.0+thicknessesCoefficientsFunctor.getUslip(Hk, H, Hs));

    Tout Hkc = 0.0;
    if (profileCat == IBLProfileCategory::TurbulentBL)
    {
      const Tpi Ret = qe*theta11/nue;
      Hkc = Hk - 1.0 - 18.0/Ret;
      if (Hkc < 0.01)
        Hkc = 0.01; //TODO: to be smoothened
    }
    else
      Hkc = Hk - 1.0;
#endif

    const Tout HR = Hkc/(Gacon*ALD*Hk);

    // XFOIL, xblsys.f, BLDIF, ~L1783
#if 0 //TODO: playing with forced fully turbulent BL
    Tout Slag_turb =  - 0.5*qe/delta_nom * scc*(pow(ctau_eq,0.5) - pow(ctau,0.5)*ALD);
#else
    Tout Slag_turb =  - 0.5*qe/delta_nom * scc*(pow(ctau_eq,0.5) - pow(ctau,0.5)*ALD)
                      - duxcon*(0.5*RetCf1*nue/theta11 - qe*pow(HR,2.0)) / (Gbcon*delta1s)
                      + duxcon*divq1;
#endif
#else
    // Ref: (MSES,1987)
    Tout Slag_turb = - 0.5*qe/delta_nom * 4.2*(pow(ctau_eq,0.5) - pow(ctau,0.5));
#endif

//    if (profileCat == IBLProfileCategory::TurbulentBL)
//      Slag_turb *= (1.0-f_switchoff_ctauTurbInit);

    src[ir_amplag] += -G*divq1 + Slag_turb;
  }
  else if (profileCat == ProfileCategory::LaminarWake)
    SANS_DEVELOPER_EXCEPTION("Laminar wake is not supported!");
#if IsExplicitCheckingUniFieldIBL
  else
    SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");
#endif

  // forced transition occurs on airfoil surface TODO: might want to make this more rigorous
#if 0 // add regularized (aka blended/smeared) point source to reach initial turbulent
  if (transitionModel_.xtr() < 1.0)
//  if (transitionModel_.xtr() < 1.0 && profileCat == ProfileCategory::LaminarBL)
  {
    // TODO: note that using streamwise coordinate s is more rigorous than using x, but s is not readily available here
    const Real dx_inv = 1./0.1; // 1/dx, where the point source is spread over a length scale of 2*dx
//    const Real dx_inv = 1./0.075; // 1/dx, where the point source is spread over a length scale of 2*dx
    const Real frac = (x-transitionModel_.xtr())*dx_inv;
    Tout deltaFcn = 0.0;
    if (-1.0 <= frac && frac <= 1.0)
      deltaFcn = (getGTurbInitConst() - G) * 0.5*dx_inv*(1.0 + cos(PI*frac));

    const Tout src_ctauTurbInit = 5.0*deltaFcn;

    src[ir_amplag] += -qe*src_ctauTurbInit;
  }
#endif

  // free transition
#if 0 // add regularized (aka blended/smeared) point source to reach initial turbulent
  if (profileCat == ProfileCategory::LaminarBL)
//  if (profileCat == ProfileCategory::LaminarBL || profileCat == ProfileCategory::TurbulentBL)
  {
    const Real dnt = 2.0;
    const Real dx = 0.1; // ~ ds = dnt/(dnt_ds)
    const T frac = (nt - transitionModel_.ntcrit())/dnt; // fractional perturbation from equal laminar/turbulent mix

    Tout deltaFcn = 0.0;
    const Real scale_const = dx/dnt * 20.0; // C s.t. C*dnt/ds / (dnt_ds) ~ 1
    if (-1.0 <= frac && frac <= 1.0)
      deltaFcn = 0.5/dx*(1.0 + cos(PI*frac)) * scale_const;

    const Tout src_ctauTurbInit = (getGTurbInitConst() - getGcrit())*deltaFcn;
    //    const Tout src_ctauTurbInit = (getGTurbInitConst() - G)*deltaFcn;
//    const Tout src_ctauTurbInit = (getGTurbInitConst() - G)*deltaFcn * 2.0;

    src[ir_amplag] += -qe*src_ctauTurbInit;
  }
#endif


#if 1
  if (profileCat == ProfileCategory::LaminarBL || profileCat == ProfileCategory::TurbulentBL)
  {
    const Real dG = 0.25;
#if 0
    const Tout G_Tinit = getGTurbInitConst();
#else
    const Tout ctaueq = thicknessesCoefficientsFunctor.getctaueq();
    const Tout Hk = thicknessesCoefficientsFunctor.getHk();
    const Tout ctauTinit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk, ctaueq);
    const Tout G_Tinit = varInterpret_.getG_from_ctau(ctauTinit);
#endif

#if 1
    const T G_crit = G;
#else
    const T G_crit = getGcrit();
#endif

    const Tout f_grow = 24.0*max(G_Tinit - G_crit, 0.4);
    const Tout f_switchoff_ctauTurbInit = 0.5 - 0.5*tanh((G-G_Tinit-1.0*dG)/dG * 1.5);

    const Real dnt = 0.25;
//    const Real dnt = 0.25;
    const Real dx = 0.05;
    const T f_switchon_nt = 0.5 + 0.5*tanh((nt-transitionModel_.ntcrit()+dnt)/dnt * 1.5);
    const T f_switchon_x = 0.5 + 0.5*tanh((x-transitionModel_.xtr())/dx * 1.5);
//    const T f_switchon = smoothmax(f_switchon_nt, f_switchon_x, 20.0);
    const T f_switchon = max(f_switchon_nt, f_switchon_x);

    src[ir_amplag] += -qe*f_switchon*f_grow*f_switchoff_ctauTurbInit;
  }
#endif
}

//----------------------------------------------------------------------------//
// Pseudo transient continuation (PTC) uses this function
template <class VarType>
template <class T, class Tp>
void
PDEIBLUniField<PhysD2,VarType>::
speedCharacteristic(
    const ArrayParam<Tp>& params,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& var, Tp& speed ) const
{
  // velocity vector q_1, defined as q_1 = q_i
  VectorX<Tp> q1 = paramInterpret_.getq1(params);

  speed = paramInterpret_.getqe(params); // q_e: edge speed. Equal to magnitude of q_1.

#if 1
  speed = max(speed, 1.0); //TODO: hard-coded so that speed is larger than zero even at stagnation point
#endif
}

//----------------------------------------------------------------------------//
// check state validity
template <class VarType>
bool
PDEIBLUniField<PhysD2,VarType>::isValidState( const ArrayQ<Real>& var ) const
{
  static_assert(std::is_same<CompressibilityType, IncompressibleBL>::value, "Assumed incompressible shear layer for H=Hk!");
  const IBLProfileCategory profileCatDefault = transitionModel_.getProfileCategoryDefault();
  return varInterpret_.isValidState(profileCatDefault, var);
}

template <class VarType>
template <class T, class Tp>
inline void
PDEIBLUniField<PhysD2,VarType>::updateFraction(
    const ArrayParam<Tp>& params,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& var, const ArrayQ<T>& dvar,
    const Real& maxChangeFraction, Real& updateFraction) const
{
  // analogous implementation can be found in XFOIL, xbl.f SUBROUTINE UPDATE
  // The purpose of this function is to ensure state validity since plain Newton line update does not check validity
  // TODO: can be extended to ensure positive delta*, theta etc.
  const ProfileCategory profileCat = getProfileCategory(var,x);
  varInterpret_.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
}

// set from variable array
template <class VarType>
template <class T, class IBLVarData>
inline void
PDEIBLUniField<PhysD2,VarType>::setDOFFrom( const IBLVarDataType<IBLVarData>& data,
                                    ArrayQ<T>& var ) const
{
  varInterpret_.setDOFFrom( data.cast(), var);
}

// set from variable array
template <class VarType>
template <class IBLVarData>
inline typename PDEIBLUniField<PhysD2,VarType>::template ArrayQ<Real>
PDEIBLUniField<PhysD2,VarType>::setDOFFrom( const IBLVarDataType<IBLVarData>& data ) const
{
  ArrayQ<Real> var;
  setDOFFrom( data.cast(), var );
  return var;
}

// interpret residuals of the solution variable
template <class VarType>
template<class T>
inline void
PDEIBLUniField<PhysD2,VarType>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT( rsdPDEIn.M == N );

  int nOut = rsdPDEOut.m();

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT( nOut == N );
    for ( int i = 0; i < N; i++ )
      rsdPDEOut[i] = rsdPDEIn[i];
  }
}

// interpret residuals of the gradients in the solution variable
template <class VarType>
template<class T>
inline void
PDEIBLUniField<PhysD2,VarType>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT( rsdAuxIn.M == N );

  int nOut = rsdAuxOut.m();

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT( nOut == N );
    for ( int i = 0; i < N; i++ )
      rsdAuxOut[i] = rsdAuxIn[i];
  }
}

// interpret residuals at the boundary (should forward to BCs)
template <class VarType>
template<class T>
inline void
PDEIBLUniField<PhysD2,VarType>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT( rsdBCIn.M == N );

  int nOut = rsdBCOut.m();

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT( nOut == N );
    for ( int i = 0; i < N; i++ )
      rsdBCOut[i] = rsdBCIn[i];
  }
}

// return the interp type
template <class VarType>
inline typename PDEIBLUniField<PhysD2,VarType>::IBLResidualInterpCategory
PDEIBLUniField<PhysD2,VarType>::category() const
{
  return rsdCat_;
}

// how many residual equations are we dealing with
template <class VarType>
inline int
PDEIBLUniField<PhysD2,VarType>::nMonitor() const
{
  int nMon = -1;

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
    nMon = N;

  return nMon;
}

} // namespace SANS

#endif /* SRC_PDE_IBL_PDEIBLUNIFIELD_H_ */
