// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_ELEMENTPROJECTIONIBLOUTPUT_L2_H_
#define SRC_PDE_IBL_ELEMENTPROJECTIONIBLOUTPUT_L2_H_

#include <vector>

#include "Field/Element/ElementProjection_L2.h"
#include "Field/Element/ElementLine.h"
#include "Field/Element/ElementXFieldLine.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "PDEIBL2D.h"
#include "PDEIBLUniField.h"
#include "PDEIBLSplitAmpLag2D.h"

namespace SANS
{

template <class VarType>
class ElementProjectionIBLoutput_L2;

//----------------------------------------------------------------------------//
// Elemental projection onto IBL output from IBL solution field and parameter field
//
template <>
class ElementProjectionIBLoutput_L2<VarTypeDANCt> : public ElementProjectionBase<TopoD1, Line>
{
public:
  enum class outputID : int
  {
    deltaLami = 0,
    ALami = 1,
    delta1s = 2,
    theta11 = 3,
    theta1s = 4,
    Cf1 = 5,
    CD = 6,
    H = 7,
    Hs = 8,
    Retheta = 9,
    UeVinf = 10,
    cp = 11,
    m = 12, // mass defect
    qx = 13,
    qz = 14,
    nt = 15,
    ctau = 16,
    gam = 17, // intermittency
    beta = 18, // pressure gradient parameter in Clauser's equilibrium turbulent BL formulation
    ctaueq = 19, // equilibrium ctau
    deltaTurb = 20,
    ATurb = 21
  };

  static const int nOutput = 22;

  typedef PhysD2 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line TopologyType;

  typedef ElementProjectionBase<TopoDim, TopologyType> BaseType;
  typedef typename BaseType::BasisType BasisType;
  typedef typename BaseType::RefCoordType RefCoordType;

  typedef VarTypeDANCt VarType;
  typedef PDEIBL<PhysDim,VarType> PDEClass;

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>;

  template <class T>
  using ArrayParam = typename PDEClass::template ArrayParam<T>;

  template<class T>
  using ArrayOutput = typename DLA::VectorS<nOutput,T>;

  template<class T>
  using VectorX = typename PDEClass::template VectorX<T>;

  typedef ElementXField<PhysDim, TopoDim, TopologyType> ElementXFieldType;

  // ctor
  ElementProjectionIBLoutput_L2( const BasisType* basis,
                                 const int quadratureOrderRHS ) :
    BaseType(basis, 2*basis->order()),
    quadratureRHS_(quadratureOrderRHS)
  {
    if (basis_->order() == 1)
    {
      SANS_ASSERT_MSG( (basis_->category() == BasisFunctionCategory_Hierarchical),
                       "Assumess Hierarchical basis function which is a nodal basis for order =1." );
    }
  }

  // get output names
  static std::vector<std::string> outputNames()
  {
    std::vector<std::string> output_names(nOutput);

    output_names[(int) outputID::deltaLami] = "deltaLami";
    output_names[(int) outputID::ALami] = "ALami";
    output_names[(int) outputID::delta1s] = "delta1*";
    output_names[(int) outputID::theta11] = "theta11";
    output_names[(int) outputID::theta1s] = "theta1*";
    output_names[(int) outputID::Cf1] = "C_f";
    output_names[(int) outputID::CD] = "C_Diss";
    output_names[(int) outputID::H] = "H";
    output_names[(int) outputID::Hs] = "H*";
    output_names[(int) outputID::Retheta] = "Re_theta";
    output_names[(int) outputID::UeVinf] = "|Ue/Vinf|";
    output_names[(int) outputID::cp] = "c_p";
    output_names[(int) outputID::m] = "m";
    output_names[(int) outputID::qx] = "qx";
    output_names[(int) outputID::qz] = "qz";
    output_names[(int) outputID::nt] = "nt";
    output_names[(int) outputID::ctau] = "ctau";
    output_names[(int) outputID::gam] = "gam";
    output_names[(int) outputID::beta] = "beta";
    output_names[(int) outputID::ctaueq] = "ctau_eq";
    output_names[(int) outputID::deltaTurb] = "deltaTurb";
    output_names[(int) outputID::ATurb] = "ATurb";

    return output_names;
  }

  // project onto output field element from qfldElem and paramfldElem
  template <class T, class Tp, class To>
  void project( const PDEClass& pde,
                const Real& Vinf,
                const Element<ArrayQ<T>, TopoDim, TopologyType>& qfldElem,
                const Element<ArrayParam<Tp>, TopoDim, TopologyType>& paramfldElem,
                const ElementXFieldType& xfldElem,
                Element<ArrayOutput<To>, TopoDim, TopologyType>& outputfldElem ) const
  {
    const int nBasis = outputfldElem.basis()->nBasis();
    SANS_ASSERT(nDOF_ == nBasis);

    DLA::VectorD< ArrayOutput<To> > OutputDOFs(nBasis);

    const auto& varInterpret_ = pde.getVarInterpreter();
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const auto& gasModel_ = pde.getGasModel();

    if (outputfldElem.order()==1) // linear nodal interpolation is used p=1
    {
      std::vector<Real> sRefArray = {0.0, 1.0};

      for (int i = 0; i < nBasis; ++i)
      {
        RefCoordType sRef = sRefArray[i];

        VectorX<Real> X = xfldElem.eval(sRef);
        Real x = X[0], z = X[1];

        VectorX<Real> e1;                // basis direction vector
        xfldElem.unitTangent(sRef, e1);

        ArrayQ<T> var = qfldElem.eval(sRef);
        ArrayParam<Tp> params = paramfldElem.eval(sRef);

        VectorX<Tp> q1 = paramInterpret_.getq1(params);
        Tp qe = paramInterpret_.getqe(params);
        Tp UeVinf = qe/Vinf;
        Tp cp = 1 - pow(UeVinf,2.0);

        Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
        Tp nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

        const auto& profileCat = pde.getProfileCategory(var,x);

        const auto& thicknessCoefficientsFunctor
          = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

        T gam = pde.calcIntermittency(params,e1,x,z,var);

        To delta1s = thicknessCoefficientsFunctor.getdelta1s();
        To theta11 = thicknessCoefficientsFunctor.gettheta11();
        To theta1s = thicknessCoefficientsFunctor.gettheta1s();

        To Retheta = qe * theta11 / nue;

        To Cf1 = thicknessCoefficientsFunctor.getRetCf1() / Retheta;
        To CDiss = thicknessCoefficientsFunctor.getRetCD() / Retheta;
        To ctaueq = thicknessCoefficientsFunctor.getctaueq();

        To H = thicknessCoefficientsFunctor.getH();
        To Hs = thicknessCoefficientsFunctor.getHs();

        To m = rhoe*qe*delta1s;

        To beta = 0.0; // only needed for BL flows; set to zero in wake
        if (profileCat == IBLProfileCategory::LaminarBL || profileCat == IBLProfileCategory::TurbulentBL)
          beta = - delta1s/(0.5*thicknessCoefficientsFunctor.getRetCf1()*nue/theta11)* paramInterpret_.getdivq1(params);

        ArrayOutput<To> output;
        output((int) outputID::deltaLami) = varInterpret_.getdeltaLami(var);
        output((int) outputID::ALami) = varInterpret_.getALami(var);
        output((int) outputID::delta1s) = delta1s;
        output((int) outputID::theta11) = theta11;
        output((int) outputID::theta1s) = theta1s;
        output((int) outputID::Cf1) = Cf1;
        output((int) outputID::CD) = CDiss;
        output((int) outputID::H) = H;
        output((int) outputID::Hs) = Hs;
        output((int) outputID::Retheta) = Retheta;
        output((int) outputID::UeVinf) = UeVinf;
        output((int) outputID::cp) = cp;
        output((int) outputID::m) = m;
        output((int) outputID::qx) = q1[0];
        output((int) outputID::qz) = q1[1];
        output((int) outputID::nt) = varInterpret_.getnt(var);
        output((int) outputID::ctau) = varInterpret_.getctau(var);
        output((int) outputID::gam) = gam;
        output((int) outputID::beta) = beta;
        output((int) outputID::ctaueq) = ctaueq;
        output((int) outputID::deltaTurb) = varInterpret_.getdeltaTurb(var);
        output((int) outputID::ATurb) = varInterpret_.getATurb(var);

        OutputDOFs[i] = output;
      }
    }
    else // otherwise, L2 projection onto output element
    {
      const int nquad = quadratureRHS_.nQuadrature();

      Real weight;                // quadrature weight
      RefCoordType sRef;          // reference-element coordinate

      DLA::VectorD< ArrayOutput<Tp> > rsdOutput(nBasis);
      rsdOutput = 0.0; // initialization is important!

      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadratureRHS_.weight( iquad, weight );
        quadratureRHS_.coordinates( iquad, sRef );

        VectorX<Real> X = xfldElem.eval(sRef);
        Real x = X[0], z = X[1];

        VectorX<Real> e1;                // basis direction vector
        xfldElem.unitTangent(sRef, e1);

        ArrayQ<T> var = qfldElem.eval(sRef);
        ArrayParam<Tp> params = paramfldElem.eval(sRef);

        VectorX<Tp> q1 = paramInterpret_.getq1(params);
        Tp qe = paramInterpret_.getqe(params);
        Tp UeVinf = qe/Vinf;
        Tp cp = 1 - pow(UeVinf,2.0);

        Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
        Tp nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

        const auto& profileCat = pde.getProfileCategory(var,x);

        const auto& thicknessCoefficientsFunctor
        = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

        T gam = pde.calcIntermittency(params,e1,x,z,var);

        To delta1s = thicknessCoefficientsFunctor.getdelta1s();
        To theta11 = thicknessCoefficientsFunctor.gettheta11();
        To theta1s = thicknessCoefficientsFunctor.gettheta1s();

        To Retheta = qe * theta11 / nue;

        To Cf1 = thicknessCoefficientsFunctor.getRetCf1() / Retheta;
        To CDiss = thicknessCoefficientsFunctor.getRetCD() / Retheta;
        To ctaueq = thicknessCoefficientsFunctor.getctaueq();

        To H = thicknessCoefficientsFunctor.getH();
        To Hs = thicknessCoefficientsFunctor.getHs();

        To m = rhoe*qe*delta1s;

        To beta = 0.0; // only needed for BL flows; set to zero in wake
        if (profileCat == IBLProfileCategory::LaminarBL || profileCat == IBLProfileCategory::TurbulentBL)
          beta = - delta1s/(0.5*thicknessCoefficientsFunctor.getRetCf1()*nue/theta11)* paramInterpret_.getdivq1(params);

        ArrayOutput<To> output;
        output((int) outputID::deltaLami) = varInterpret_.getdeltaLami(var);
        output((int) outputID::ALami) = varInterpret_.getALami(var);
        output((int) outputID::delta1s) = delta1s;
        output((int) outputID::theta11) = theta11;
        output((int) outputID::theta1s) = theta1s;
        output((int) outputID::Cf1) = Cf1;
        output((int) outputID::CD) = CDiss;
        output((int) outputID::H) = H;
        output((int) outputID::Hs) = Hs;
        output((int) outputID::Retheta) = Retheta;
        output((int) outputID::UeVinf) = UeVinf;
        output((int) outputID::cp) = cp;
        output((int) outputID::m) = m;
        output((int) outputID::qx) = q1[0];
        output((int) outputID::qz) = q1[1];
        output((int) outputID::nt) = varInterpret_.getnt(var);
        output((int) outputID::ctau) = varInterpret_.getctau(var);
        output((int) outputID::gam) = gam;
        output((int) outputID::beta) = beta;
        output((int) outputID::ctaueq) = ctaueq;
        output((int) outputID::deltaTurb) = varInterpret_.getdeltaTurb(var);
        output((int) outputID::ATurb) = varInterpret_.getATurb(var);

        std::vector<Real> phi(nBasis);
        outputfldElem.evalBasis( sRef, phi.data(), nBasis );

        for (int i = 0; i < nBasis; i++)
          rsdOutput[i] += weight * phi[i] * output;
      }
      OutputDOFs = invMassMatrix_ * rsdOutput;
    }

    for (int i = 0; i < nBasis; ++i)
      outputfldElem.DOF(i) = OutputDOFs[i];
  }

protected:
  Quadrature<TopoDim, TopologyType> quadratureRHS_;
};

//----------------------------------------------------------------------------//
// Elemental projection onto IBL output from IBL solution field and parameter field
//
template <>
class ElementProjectionIBLoutput_L2<VarTypeDAG> : public ElementProjectionBase<TopoD1, Line>
{
public:
  enum class outputID : int
  {
    delta = 0,
    A = 1,
    delta1s = 2,
    theta11 = 3,
    theta1s = 4,
    Cf1 = 5,
    CD = 6,
    H = 7,
    Hs = 8,
    Retheta = 9,
    UeVinf = 10,
    cp = 11,
    m = 12, // mass defect
    qx = 13,
    qz = 14,
    nt = 15,
    ctau = 16,
    gam = 17, // intermittency
    beta = 18, // pressure gradient parameter in Clauser's equilibrium turbulent BL formulation
    ctaueq = 19, // equilibrium ctau
    G = 20,
    dummy = 21
  };

  static const int nOutput = 22;

  typedef PhysD2 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line TopologyType;

  typedef ElementProjectionBase<TopoDim, TopologyType> BaseType;
  typedef typename BaseType::BasisType BasisType;
  typedef typename BaseType::RefCoordType RefCoordType;

  typedef VarTypeDAG VarType;
  typedef PDEIBLUniField<PhysDim,VarType> PDEClass;

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>;

  template <class T>
  using ArrayParam = typename PDEClass::template ArrayParam<T>;

  template<class T>
  using ArrayOutput = typename DLA::VectorS<nOutput,T>;

  template<class T>
  using VectorX = typename PDEClass::template VectorX<T>;

  typedef typename PDEClass::ThicknessesCoefficientsType ThicknessesCoefficientsType;
  typedef typename PDEClass::ProfileCategory ProfileCategory;

  template<class Tv, class Tp>
  using SecondaryVarFunctor = typename ThicknessesCoefficientsType::template Functor<Tv,Tp>;

  typedef ElementXField<PhysDim, TopoDim, TopologyType> ElementXFieldType;

  // ctor
  ElementProjectionIBLoutput_L2( const BasisType* basis,
                                 const int quadratureOrderRHS ) :
    BaseType(basis, 2*basis->order()),
    quadratureRHS_(quadratureOrderRHS)
  {
    if (basis_->order() == 1)
    {
      SANS_ASSERT_MSG( (basis_->category() == BasisFunctionCategory_Hierarchical),
                       "Assumess Hierarchical basis function which is a nodal basis for order =1." );
    }
  }

  // get output names
  static std::vector<std::string> outputNames()
  {
    std::vector<std::string> output_names(nOutput);

    output_names[(int) outputID::delta] = "delta";
    output_names[(int) outputID::A] = "A";
    output_names[(int) outputID::delta1s] = "delta1*";
    output_names[(int) outputID::theta11] = "theta11";
    output_names[(int) outputID::theta1s] = "theta1*";
    output_names[(int) outputID::Cf1] = "C_f";
    output_names[(int) outputID::CD] = "C_Diss";
    output_names[(int) outputID::H] = "H";
    output_names[(int) outputID::Hs] = "H*";
    output_names[(int) outputID::Retheta] = "Re_theta";
    output_names[(int) outputID::UeVinf] = "|Ue/Vinf|";
    output_names[(int) outputID::cp] = "c_p";
    output_names[(int) outputID::m] = "m";
    output_names[(int) outputID::qx] = "qx";
    output_names[(int) outputID::qz] = "qz";
    output_names[(int) outputID::nt] = "nt";
    output_names[(int) outputID::ctau] = "ctau";
    output_names[(int) outputID::gam] = "gam";
    output_names[(int) outputID::beta] = "beta";
    output_names[(int) outputID::ctaueq] = "ctau_eq";
    output_names[(int) outputID::G] = "G";
    output_names[(int) outputID::dummy] = "dummy";

    return output_names;
  }

  // project onto output field element from qfldElem and paramfldElem
  template <class T, class Tp, class To>
  void project( const PDEClass& pde,
                const Real& Vinf,
                const Element<ArrayQ<T>, TopoDim, TopologyType>& qfldElem,
                const Element<ArrayParam<Tp>, TopoDim, TopologyType>& paramfldElem,
                const ElementXFieldType& xfldElem,
                Element<ArrayOutput<To>, TopoDim, TopologyType>& outputfldElem ) const
  {
    const int nBasis = outputfldElem.basis()->nBasis();
    SANS_ASSERT(nDOF_ == nBasis);

    DLA::VectorD< ArrayOutput<To> > OutputDOFs(nBasis);

    const auto& varInterpret_ = pde.getVarInterpreter();
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const auto& gasModel_ = pde.getGasModel();

    if (outputfldElem.order()==1) // linear nodal interpolation is used p=1
    {
      std::vector<Real> sRefArray = {0.0, 1.0};

      for (int i = 0; i < nBasis; ++i)
      {
        RefCoordType sRef = sRefArray[i];

        VectorX<Real> X = xfldElem.eval(sRef);
        Real x = X[0], z = X[1];

        VectorX<Real> e1;                // basis direction vector
        xfldElem.unitTangent(sRef, e1);

        ArrayQ<T> var = qfldElem.eval(sRef);
        ArrayParam<Tp> params = paramfldElem.eval(sRef);

        const ProfileCategory profileCat = pde.getProfileCategory(var,x);

        T G = varInterpret_.getG(var);
        T ctau = varInterpret_.getctau(var);

        VectorX<Tp> q1 = paramInterpret_.getq1(params);
        Tp qe = paramInterpret_.getqe(params);
        Tp UeVinf = qe/Vinf;
        Tp cp = 1 - pow(UeVinf,2.0);

        Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
        const Tp nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

        const auto& thicknessesCoefficientsObj_ = pde.getSecondaryVarObj();
        const auto thicknessesCoefficientsFunctor
          = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

        T gam = pde.calcIntermittency(params,e1,x,z,var);

        To delta1s = thicknessesCoefficientsFunctor.getdelta1s();
        To theta11 = thicknessesCoefficientsFunctor.gettheta11();
        To theta1s = thicknessesCoefficientsFunctor.gettheta1s();

        const To Ret = rhoe*qe*theta11/nue;

        To RetCf1 = thicknessesCoefficientsFunctor.getRetCf1();
        To Cf1 = RetCf1 / Ret;
        To CD = thicknessesCoefficientsFunctor.getRetCD() / Ret;
        To ctaueq = thicknessesCoefficientsFunctor.getctaueq();

        To H = delta1s / theta11;
        To Hs = theta1s / theta11;

        To m = rhoe*qe*delta1s;

        To beta = 0.0; // only needed for BL flows; set to zero in wake
        if (profileCat == ProfileCategory::LaminarBL ||
            profileCat == ProfileCategory::TurbulentBL)
        {
          beta = - delta1s/(0.5*RetCf1*nue/theta11) * paramInterpret_.getdivq1(params);
        }

        ArrayOutput<To> output;
        output((int) outputID::delta) = varInterpret_.getdelta(var);
        output((int) outputID::A) = varInterpret_.getA(var);
        output((int) outputID::delta1s) = delta1s;
        output((int) outputID::theta11) = theta11;
        output((int) outputID::theta1s) = theta1s;
        output((int) outputID::Cf1) = Cf1;
        output((int) outputID::CD) = CD;
        output((int) outputID::H) = H;
        output((int) outputID::Hs) = Hs;
        output((int) outputID::Retheta) = Ret;
        output((int) outputID::UeVinf) = UeVinf;
        output((int) outputID::cp) = cp;
        output((int) outputID::m) = m;
        output((int) outputID::qx) = q1[0];
        output((int) outputID::qz) = q1[1];
        output((int) outputID::nt) = pde.getnt(var);
        output((int) outputID::ctau) = ctau;
        output((int) outputID::gam) = gam;
        output((int) outputID::beta) = beta;
        output((int) outputID::ctaueq) = ctaueq;
        output((int) outputID::G) = G;

        OutputDOFs[i] = output;
      }
    }
    else // otherwise, L2 projection onto output element
    {
      const int nquad = quadratureRHS_.nQuadrature();

      Real weight;                // quadrature weight
      RefCoordType sRef;          // reference-element coordinate

      DLA::VectorD< ArrayOutput<Tp> > rsdOutput(nBasis);
      rsdOutput = 0.0; // initialization is important!

      for (int iquad = 0; iquad < nquad; iquad++)
      {
        quadratureRHS_.weight( iquad, weight );
        quadratureRHS_.coordinates( iquad, sRef );

        VectorX<Real> X = xfldElem.eval(sRef);
        Real x = X[0], z = X[1];

        VectorX<Real> e1;                // basis direction vector
        xfldElem.unitTangent(sRef, e1);

        ArrayQ<T> var = qfldElem.eval(sRef);
        ArrayParam<Tp> params = paramfldElem.eval(sRef);

        const ProfileCategory profileCat = pde.getProfileCategory(var,x);

        T G = varInterpret_.getG(var);
        T ctau = varInterpret_.getctau(var);

        VectorX<Tp> q1 = paramInterpret_.getq1(params);
        Tp qe = paramInterpret_.getqe(params);
        Tp UeVinf = qe/Vinf;
        Tp cp = 1 - pow(UeVinf,2.0);

        Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
        const Tp nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

        const auto& thicknessesCoefficientsObj_ = pde.getSecondaryVarObj();
        const auto thicknessesCoefficientsFunctor
          = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

        T gam = pde.calcIntermittency(params,e1,x,z,var);

        To delta1s = thicknessesCoefficientsFunctor.getdelta1s();
        To theta11 = thicknessesCoefficientsFunctor.gettheta11();
        To theta1s = thicknessesCoefficientsFunctor.gettheta1s();

        const To Ret = rhoe*qe*theta11/nue;

        To RetCf1 = thicknessesCoefficientsFunctor.getRetCf1();
        To Cf1 = RetCf1 / Ret;
        To CD = thicknessesCoefficientsFunctor.getRetCD() / Ret;
        To ctaueq = thicknessesCoefficientsFunctor.getctaueq();

        To H = delta1s / theta11;
        To Hs = theta1s / theta11;

        To m = rhoe*qe*delta1s;

        To beta = 0.0; // only needed for BL flows; set to zero in wake
        if (profileCat == ProfileCategory::LaminarBL ||
            profileCat == ProfileCategory::TurbulentBL)
        {
          beta = - delta1s/(0.5*RetCf1*nue*theta11) * paramInterpret_.getdivq1(params);
        }

        ArrayOutput<To> output;
        output((int) outputID::delta) = varInterpret_.getdelta(var);
        output((int) outputID::A) = varInterpret_.getA(var);
        output((int) outputID::delta1s) = delta1s;
        output((int) outputID::theta11) = theta11;
        output((int) outputID::theta1s) = theta1s;
        output((int) outputID::Cf1) = Cf1;
        output((int) outputID::CD) = CD;
        output((int) outputID::H) = H;
        output((int) outputID::Hs) = Hs;
        output((int) outputID::Retheta) = Ret;
        output((int) outputID::UeVinf) = UeVinf;
        output((int) outputID::cp) = cp;
        output((int) outputID::m) = m;
        output((int) outputID::qx) = q1[0];
        output((int) outputID::qz) = q1[1];
        output((int) outputID::nt) = pde.getnt(var);
        output((int) outputID::ctau) = ctau;
        output((int) outputID::gam) = gam;
        output((int) outputID::beta) = beta;
        output((int) outputID::ctaueq) = ctaueq;
        output((int) outputID::G) = G;

        std::vector<Real> phi(nBasis);
        outputfldElem.evalBasis( sRef, phi.data(), nBasis );

        for (int i = 0; i < nBasis; i++)
          rsdOutput[i] += weight * phi[i] * output;
      }
      OutputDOFs = invMassMatrix_ * rsdOutput;
    }

    for (int i = 0; i < nBasis; ++i)
      outputfldElem.DOF(i) = OutputDOFs[i];
  }

protected:
  Quadrature<TopoDim, TopologyType> quadratureRHS_;
};

//------------------------------------------------------------------------------------------//
template <>
class ElementProjectionIBLoutput_L2<VarTypeDsThG> : public ElementProjectionBase<TopoD1, Line>
{
public:
  enum class outputID : int
  {
    delta1s = 0,
    theta11 = 1,
    theta1s = 2,
    H = 3,
    Hs = 4,
    Cf1 = 5,
    CDiss = 6,
    Retheta = 7,
    UeVinf = 8,
    cp = 9,
    m = 10, // mass defect
    qx = 11,
    qz = 12,
    nt = 13,
    ctau = 14,
    gam = 15, // intermittency
    ctaueq = 16, // equilibrium ctau
    G = 17,
    dummy1 = 18,
    dummy2 = 19,
    dummy3 = 20,
    dummy4 = 21
  };

  static const int nOutput = 22;

  typedef PhysD2 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line TopologyType;

  typedef ElementProjectionBase<TopoDim, TopologyType> BaseType;
  typedef typename BaseType::BasisType BasisType;
  typedef typename BaseType::RefCoordType RefCoordType;

  typedef VarTypeDsThG VarType;
  typedef PDEIBLUniField<PhysDim,VarType> PDEClass;

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>;

  template <class T>
  using ArrayParam = typename PDEClass::template ArrayParam<T>;

  template<class T>
  using ArrayOutput = typename DLA::VectorS<nOutput,T>;

  template<class T>
  using VectorX = typename PDEClass::template VectorX<T>;

  typedef ElementXField<PhysDim, TopoDim, TopologyType> ElementXFieldType;

  // ctor
  ElementProjectionIBLoutput_L2( const BasisType* basis,
                                 const int quadratureOrderRHS ) :
    BaseType(basis, 2*basis->order()),
    quadratureRHS_(quadratureOrderRHS)
  {
    if (basis_->order() == 1)
    {
      SANS_ASSERT_MSG( (basis_->category() == BasisFunctionCategory_Hierarchical),
                       "Assumess Hierarchical basis function which is a nodal basis for order =1." );
    }
  }

  // get output names
  static std::vector<std::string> outputNames()
  {
    std::vector<std::string> output_names(nOutput);

    output_names[(int) outputID::delta1s] = "delta1*";
    output_names[(int) outputID::theta11] = "theta11";
    output_names[(int) outputID::theta1s] = "theta1*";
    output_names[(int) outputID::H] = "H";
    output_names[(int) outputID::Hs] = "H*";
    output_names[(int) outputID::Cf1] = "C_f";
    output_names[(int) outputID::CDiss] = "C_Diss";
    output_names[(int) outputID::Retheta] = "Re_theta";
    output_names[(int) outputID::UeVinf] = "|Ue/Vinf|";
    output_names[(int) outputID::cp] = "c_p";
    output_names[(int) outputID::m] = "m";
    output_names[(int) outputID::qx] = "qx";
    output_names[(int) outputID::qz] = "qz";
    output_names[(int) outputID::nt] = "nt";
    output_names[(int) outputID::ctau] = "ctau";
    output_names[(int) outputID::gam] = "gam";
    output_names[(int) outputID::ctaueq] = "ctau_eq";
    output_names[(int) outputID::G] = "G";
    output_names[(int) outputID::dummy1] = "dummy";
    output_names[(int) outputID::dummy2] = "dummy";
    output_names[(int) outputID::dummy3] = "dummy";
    output_names[(int) outputID::dummy4] = "dummy";

    return output_names;
  }

  // project onto output field element from qfldElem and paramfldElem
  template <class T, class Tp, class To>
  void project( const PDEClass& pde,
                const Real& Vinf,
                const Element<ArrayQ<T>, TopoDim, TopologyType>& qfldElem,
                const Element<ArrayParam<Tp>, TopoDim, TopologyType>& paramfldElem,
                const ElementXFieldType& xfldElem,
                Element<ArrayOutput<To>, TopoDim, TopologyType>& outputfldElem ) const
  {
    const int nBasis = outputfldElem.basis()->nBasis();
    SANS_ASSERT(nDOF_ == nBasis);

    DLA::VectorD< ArrayOutput<To> > OutputDOFs(nBasis);

    const auto& varInterpret_ = pde.getVarInterpreter();
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const auto& gasModel_ = pde.getGasModel();

    if (outputfldElem.order()==1) // linear nodal interpolation is used p=1
    {
      std::vector<Real> sRefArray = {0.0, 1.0};

      for (int i = 0; i < nBasis; ++i)
      {
        RefCoordType sRef = sRefArray[i];

        VectorX<Real> X = xfldElem.eval(sRef);
        Real x = X[0], z = X[1];

        VectorX<Real> e1;                // basis direction vector
        xfldElem.unitTangent(sRef, e1);

        ArrayQ<T> var = qfldElem.eval(sRef);
        ArrayParam<Tp> params = paramfldElem.eval(sRef);

        VectorX<Tp> q1 = paramInterpret_.getq1(params);
        Tp qe = paramInterpret_.getqe(params);
        Tp UeVinf = qe/Vinf;
        Tp cp = 1 - pow(UeVinf,2.0);

        Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
        Tp nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

        const auto& profileCat = pde.getProfileCategory(var,x);

        const auto& thicknessCoefficientsFunctor
          = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

        T G = varInterpret_.getG(var);
        T ctau = varInterpret_.getctau(var);

        T nt = pde.getnt(var);
        T gam = pde.calcIntermittency(params,e1,x,z,var);

        To delta1s = thicknessCoefficientsFunctor.getdelta1s();
        To theta11 = thicknessCoefficientsFunctor.gettheta11();
        To theta1s = thicknessCoefficientsFunctor.gettheta1s();

        To Retheta = qe * theta11 / nue;

        To Cf1 = thicknessCoefficientsFunctor.getRetCf1() / Retheta;
        To CDiss = thicknessCoefficientsFunctor.getRetCD() / Retheta;
        To ctaueq = thicknessCoefficientsFunctor.getctaueq();

        To H = thicknessCoefficientsFunctor.getH();
        To Hs = thicknessCoefficientsFunctor.getHs();

        To m = rhoe*qe*delta1s;

        ArrayOutput<To> output;
        output((int) outputID::delta1s) = delta1s;
        output((int) outputID::theta11) = theta11;
        output((int) outputID::theta1s) = theta1s;
        output((int) outputID::Cf1) = Cf1;
        output((int) outputID::CDiss) = CDiss;
        output((int) outputID::H) = H;
        output((int) outputID::Hs) = Hs;
        output((int) outputID::Retheta) = Retheta;
        output((int) outputID::UeVinf) = UeVinf;
        output((int) outputID::cp) = cp;
        output((int) outputID::m) = m;
        output((int) outputID::qx) = q1[0];
        output((int) outputID::qz) = q1[1];
        output((int) outputID::nt) = nt;
        output((int) outputID::ctau) = ctau;
        output((int) outputID::gam) = gam;
        output((int) outputID::ctaueq) = ctaueq;
        output((int) outputID::G) = G;

        OutputDOFs[i] = output;
      }
    }
    else // otherwise, L2 projection onto output element
      SANS_DEVELOPER_EXCEPTION("Not supported yet");

    for (int i = 0; i < nBasis; ++i)
      outputfldElem.DOF(i) = OutputDOFs[i];
  }

protected:
  Quadrature<TopoDim, TopologyType> quadratureRHS_;
};

//------------------------------------------------------------------------------------------//
template <>
class ElementProjectionIBLoutput_L2<VarTypeDsThNCt> : public ElementProjectionBase<TopoD1, Line>
{
public:
  enum class outputID : int
  {
    delta1sLami = 0,
    theta11Lami = 1,
    theta1s = 2,
    H = 3,
    Hs = 4,
    Cf1 = 5,
    CDiss = 6,
    Retheta = 7,
    UeVinf = 8,
    cp = 9,
    m = 10, // mass defect
    qx = 11,
    qz = 12,
    nt = 13,
    ctau = 14,
    gam = 15, // intermittency
    ctaueq = 16, // equilibrium ctau
    delta1sTurb = 17,
    theta11Turb = 18,
    delta1s = 19,
    theta11 = 20,
    dummy1 = 21
  };

  static const int nOutput = 22;

  typedef PhysD2 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line TopologyType;

  typedef ElementProjectionBase<TopoDim, TopologyType> BaseType;
  typedef typename BaseType::BasisType BasisType;
  typedef typename BaseType::RefCoordType RefCoordType;

  typedef VarTypeDsThNCt VarType;
  typedef PDEIBL<PhysDim,VarType> PDEClass;

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>;

  template <class T>
  using ArrayParam = typename PDEClass::template ArrayParam<T>;

  template<class T>
  using ArrayOutput = typename DLA::VectorS<nOutput,T>;

  template<class T>
  using VectorX = typename PDEClass::template VectorX<T>;

  typedef ElementXField<PhysDim, TopoDim, TopologyType> ElementXFieldType;

  // ctor
  ElementProjectionIBLoutput_L2( const BasisType* basis,
                                 const int quadratureOrderRHS ) :
    BaseType(basis, 2*basis->order()),
    quadratureRHS_(quadratureOrderRHS)
  {
    if (basis_->order() == 1)
    {
      SANS_ASSERT_MSG( (basis_->category() == BasisFunctionCategory_Hierarchical),
                       "Assumess Hierarchical basis function which is a nodal basis for order =1." );
    }
  }

  // get output names
  static std::vector<std::string> outputNames()
  {
    std::vector<std::string> output_names(nOutput);

    output_names[(int) outputID::delta1sLami] = "delta1*_lami";
    output_names[(int) outputID::theta11Lami] = "theta11_lami";
    output_names[(int) outputID::theta1s] = "theta1*";
    output_names[(int) outputID::H] = "H";
    output_names[(int) outputID::Hs] = "H*";
    output_names[(int) outputID::Cf1] = "C_f";
    output_names[(int) outputID::CDiss] = "C_Diss";
    output_names[(int) outputID::Retheta] = "Re_theta";
    output_names[(int) outputID::UeVinf] = "|Ue/Vinf|";
    output_names[(int) outputID::cp] = "c_p";
    output_names[(int) outputID::m] = "m";
    output_names[(int) outputID::qx] = "qx";
    output_names[(int) outputID::qz] = "qz";
    output_names[(int) outputID::nt] = "nt";
    output_names[(int) outputID::ctau] = "ctau";
    output_names[(int) outputID::gam] = "gam";
    output_names[(int) outputID::ctaueq] = "ctau_eq";
    output_names[(int) outputID::delta1sTurb] = "delta1*_turb";
    output_names[(int) outputID::theta11Turb] = "theta11_turb";
    output_names[(int) outputID::delta1s] = "delta1*";
    output_names[(int) outputID::theta11] = "theta11";
    output_names[(int) outputID::dummy1] = "dummy";

    return output_names;
  }

  // project onto output field element from qfldElem and paramfldElem
  template <class T, class Tp, class To>
  void project( const PDEClass& pde,
                const Real& Vinf,
                const Element<ArrayQ<T>, TopoDim, TopologyType>& qfldElem,
                const Element<ArrayParam<Tp>, TopoDim, TopologyType>& paramfldElem,
                const ElementXFieldType& xfldElem,
                Element<ArrayOutput<To>, TopoDim, TopologyType>& outputfldElem ) const
  {
    const int nBasis = outputfldElem.basis()->nBasis();
    SANS_ASSERT(nDOF_ == nBasis);

    DLA::VectorD< ArrayOutput<To> > OutputDOFs(nBasis);

    const auto& varInterpret_ = pde.getVarInterpreter();
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const auto& gasModel_ = pde.getGasModel();

    if (outputfldElem.order()==1) // linear nodal interpolation is used p=1
    {
      std::vector<Real> sRefArray = {0.0, 1.0};

      for (int i = 0; i < nBasis; ++i)
      {
        RefCoordType sRef = sRefArray[i];

        VectorX<Real> X = xfldElem.eval(sRef);
        Real x = X[0], z = X[1];

        VectorX<Real> e1;                // basis direction vector
        xfldElem.unitTangent(sRef, e1);

        ArrayQ<T> var = qfldElem.eval(sRef);
        ArrayParam<Tp> params = paramfldElem.eval(sRef);

        VectorX<Tp> q1 = paramInterpret_.getq1(params);
        Tp qe = paramInterpret_.getqe(params);
        Tp UeVinf = qe/Vinf;
        Tp cp = 1 - pow(UeVinf,2.0);

        Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
        Tp nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

        const auto& profileCat = pde.getProfileCategory(var,x);

        const auto& thicknessCoefficientsFunctor
          = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

        T ctau = varInterpret_.getctau(var);

        T gam = pde.calcIntermittency(params,e1,x,z,var);

        To delta1s = thicknessCoefficientsFunctor.getdelta1s();
        To theta11 = thicknessCoefficientsFunctor.gettheta11();
        To theta1s = thicknessCoefficientsFunctor.gettheta1s();

        To Retheta = qe * theta11 / nue;

        To Cf1 = thicknessCoefficientsFunctor.getRetCf1() / Retheta;
        To CDiss = thicknessCoefficientsFunctor.getRetCD() / Retheta;
        To ctaueq = thicknessCoefficientsFunctor.getctaueq();

        To H = thicknessCoefficientsFunctor.getH();
        To Hs = thicknessCoefficientsFunctor.getHs();

        To m = rhoe*qe*delta1s;

        ArrayOutput<To> output;
        output((int) outputID::delta1sLami) = varInterpret_.getdelta1sLami(var);
        output((int) outputID::theta11Lami) = varInterpret_.gettheta11Lami(var);
        output((int) outputID::theta1s) = theta1s;
        output((int) outputID::Cf1) = Cf1;
        output((int) outputID::CDiss) = CDiss;
        output((int) outputID::H) = H;
        output((int) outputID::Hs) = Hs;
        output((int) outputID::Retheta) = Retheta;
        output((int) outputID::UeVinf) = UeVinf;
        output((int) outputID::cp) = cp;
        output((int) outputID::m) = m;
        output((int) outputID::qx) = q1[0];
        output((int) outputID::qz) = q1[1];
        output((int) outputID::nt) = varInterpret_.getnt(var);
        output((int) outputID::ctau) = ctau;
        output((int) outputID::gam) = gam;
        output((int) outputID::ctaueq) = ctaueq;
        output((int) outputID::delta1sTurb) = varInterpret_.getdelta1sTurb(var);
        output((int) outputID::theta11Turb) = varInterpret_.gettheta11Turb(var);
        output((int) outputID::delta1s) = delta1s;
        output((int) outputID::theta11) = theta11;

        OutputDOFs[i] = output;
      }
    }
    else // otherwise, L2 projection onto output element
      SANS_DEVELOPER_EXCEPTION("Not supported yet");

    for (int i = 0; i < nBasis; ++i)
      outputfldElem.DOF(i) = OutputDOFs[i];
  }

protected:
  Quadrature<TopoDim, TopologyType> quadratureRHS_;
};

//------------------------------------------------------------------------------------------//
template <>
class ElementProjectionIBLoutput_L2<VarTypeDsThNG> : public ElementProjectionBase<TopoD1, Line>
{
public:
  enum class outputID : int
  {
    delta1sLami = 0,
    theta11Lami = 1,
    theta1s = 2,
    H = 3,
    Hs = 4,
    Cf1 = 5,
    CDiss = 6,
    Retheta = 7,
    UeVinf = 8,
    cp = 9,
    m = 10, // mass defect
    qx = 11,
    qz = 12,
    nt = 13,
    ctau = 14,
    gam = 15, // intermittency
    ctaueq = 16, // equilibrium ctau
    delta1sTurb = 17,
    theta11Turb = 18,
    delta1s = 19,
    theta11 = 20,
    G = 21
  };

  static const int nOutput = 22;

  typedef PhysD2 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line TopologyType;

  typedef ElementProjectionBase<TopoDim, TopologyType> BaseType;
  typedef typename BaseType::BasisType BasisType;
  typedef typename BaseType::RefCoordType RefCoordType;

  typedef VarTypeDsThNG VarType;
  typedef PDEIBL<PhysDim,VarType> PDEClass;

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>;

  template <class T>
  using ArrayParam = typename PDEClass::template ArrayParam<T>;

  template<class T>
  using ArrayOutput = typename DLA::VectorS<nOutput,T>;

  template<class T>
  using VectorX = typename PDEClass::template VectorX<T>;

  typedef ElementXField<PhysDim, TopoDim, TopologyType> ElementXFieldType;

  // ctor
  ElementProjectionIBLoutput_L2( const BasisType* basis,
                                 const int quadratureOrderRHS ) :
    BaseType(basis, 2*basis->order()),
    quadratureRHS_(quadratureOrderRHS)
  {
    if (basis_->order() == 1)
    {
      SANS_ASSERT_MSG( (basis_->category() == BasisFunctionCategory_Hierarchical),
                       "Assumess Hierarchical basis function which is a nodal basis for order =1." );
    }
  }

  // get output names
  static std::vector<std::string> outputNames()
  {
    std::vector<std::string> output_names(nOutput);

    output_names[(int) outputID::delta1sLami] = "delta1*_lami";
    output_names[(int) outputID::theta11Lami] = "theta11_lami";
    output_names[(int) outputID::theta1s] = "theta1*";
    output_names[(int) outputID::H] = "H";
    output_names[(int) outputID::Hs] = "H*";
    output_names[(int) outputID::Cf1] = "C_f";
    output_names[(int) outputID::CDiss] = "C_Diss";
    output_names[(int) outputID::Retheta] = "Re_theta";
    output_names[(int) outputID::UeVinf] = "|Ue/Vinf|";
    output_names[(int) outputID::cp] = "c_p";
    output_names[(int) outputID::m] = "m";
    output_names[(int) outputID::qx] = "qx";
    output_names[(int) outputID::qz] = "qz";
    output_names[(int) outputID::nt] = "nt";
    output_names[(int) outputID::ctau] = "ctau";
    output_names[(int) outputID::gam] = "gam";
    output_names[(int) outputID::ctaueq] = "ctau_eq";
    output_names[(int) outputID::delta1sTurb] = "delta1*_turb";
    output_names[(int) outputID::theta11Turb] = "theta11_turb";
    output_names[(int) outputID::delta1s] = "delta1*";
    output_names[(int) outputID::theta11] = "theta11";
    output_names[(int) outputID::G] = "G";

    return output_names;
  }

  // project onto output field element from qfldElem and paramfldElem
  template <class T, class Tp, class To>
  void project( const PDEClass& pde,
                const Real& Vinf,
                const Element<ArrayQ<T>, TopoDim, TopologyType>& qfldElem,
                const Element<ArrayParam<Tp>, TopoDim, TopologyType>& paramfldElem,
                const ElementXFieldType& xfldElem,
                Element<ArrayOutput<To>, TopoDim, TopologyType>& outputfldElem ) const
  {
    const int nBasis = outputfldElem.basis()->nBasis();
    SANS_ASSERT(nDOF_ == nBasis);

    DLA::VectorD< ArrayOutput<To> > OutputDOFs(nBasis);

    const auto& varInterpret_ = pde.getVarInterpreter();
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const auto& gasModel_ = pde.getGasModel();

    if (outputfldElem.order()==1) // linear nodal interpolation is used p=1
    {
      std::vector<Real> sRefArray = {0.0, 1.0};

      for (int i = 0; i < nBasis; ++i)
      {
        RefCoordType sRef = sRefArray[i];

        VectorX<Real> X = xfldElem.eval(sRef);
        Real x = X[0], z = X[1];

        VectorX<Real> e1;                // basis direction vector
        xfldElem.unitTangent(sRef, e1);

        ArrayQ<T> var = qfldElem.eval(sRef);
        ArrayParam<Tp> params = paramfldElem.eval(sRef);

        VectorX<Tp> q1 = paramInterpret_.getq1(params);
        Tp qe = paramInterpret_.getqe(params);
        Tp UeVinf = qe/Vinf;
        Tp cp = 1 - pow(UeVinf,2.0);

        Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
        Tp nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

        const auto& profileCat = pde.getProfileCategory(var,x);

        const auto& thicknessCoefficientsFunctor
          = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

        T ctau = varInterpret_.getctau(var);

        T gam = pde.calcIntermittency(params,e1,x,z,var);

        To delta1s = thicknessCoefficientsFunctor.getdelta1s();
        To theta11 = thicknessCoefficientsFunctor.gettheta11();
        To theta1s = thicknessCoefficientsFunctor.gettheta1s();

        To Retheta = qe * theta11 / nue;

        To Cf1 = thicknessCoefficientsFunctor.getRetCf1() / Retheta;
        To CDiss = thicknessCoefficientsFunctor.getRetCD() / Retheta;
        To ctaueq = thicknessCoefficientsFunctor.getctaueq();

        To H = thicknessCoefficientsFunctor.getH();
        To Hs = thicknessCoefficientsFunctor.getHs();

        To m = rhoe*qe*delta1s;

        ArrayOutput<To> output;
        output((int) outputID::delta1sLami) = varInterpret_.getdelta1sLami(var);
        output((int) outputID::theta11Lami) = varInterpret_.gettheta11Lami(var);
        output((int) outputID::theta1s) = theta1s;
        output((int) outputID::Cf1) = Cf1;
        output((int) outputID::CDiss) = CDiss;
        output((int) outputID::H) = H;
        output((int) outputID::Hs) = Hs;
        output((int) outputID::Retheta) = Retheta;
        output((int) outputID::UeVinf) = UeVinf;
        output((int) outputID::cp) = cp;
        output((int) outputID::m) = m;
        output((int) outputID::qx) = q1[0];
        output((int) outputID::qz) = q1[1];
        output((int) outputID::nt) = varInterpret_.getnt(var);
        output((int) outputID::ctau) = ctau;
        output((int) outputID::gam) = gam;
        output((int) outputID::ctaueq) = ctaueq;
        output((int) outputID::delta1sTurb) = varInterpret_.getdelta1sTurb(var);
        output((int) outputID::theta11Turb) = varInterpret_.gettheta11Turb(var);
        output((int) outputID::delta1s) = delta1s;
        output((int) outputID::theta11) = theta11;
        output((int) outputID::G) = varInterpret_.getG(var);

        OutputDOFs[i] = output;
      }
    }
    else // otherwise, L2 projection onto output element
      SANS_DEVELOPER_EXCEPTION("Not supported yet");

    for (int i = 0; i < nBasis; ++i)
      outputfldElem.DOF(i) = OutputDOFs[i];
  }

protected:
  Quadrature<TopoDim, TopologyType> quadratureRHS_;
};

//------------------------------------------------------------------------------------------//
template <>
class ElementProjectionIBLoutput_L2<VarTypeDsThNGsplit> : public ElementProjectionBase<TopoD1, Line>
{
public:
  enum class outputID : int
  {
    delta1s = 0,
    theta11 = 1,
    theta1s = 2,
    H = 3,
    Hs = 4,
    Cf1 = 5,
    CDiss = 6,
    Retheta = 7,
    UeVinf = 8,
    cp = 9,
    m = 10, // mass defect
    qx = 11,
    qz = 12,
    nt = 13,
    ctau = 14,
    gam = 15, // intermittency
    ctaueq = 16, // equilibrium ctau
    G = 17,
    dummy1 = 18,
    HsLami = 19,
    HsTurb = 20,
    dummy4 = 21
  };

  static const int nOutput = 22;

  typedef PhysD2 PhysDim;
  typedef TopoD1 TopoDim;
  typedef Line TopologyType;

  typedef ElementProjectionBase<TopoDim, TopologyType> BaseType;
  typedef typename BaseType::BasisType BasisType;
  typedef typename BaseType::RefCoordType RefCoordType;

  typedef VarTypeDsThNGsplit VarType;
  typedef PDEIBLSplitAmpLag<PhysDim,VarType> PDEClass;

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>;

  template <class T>
  using ArrayParam = typename PDEClass::template ArrayParam<T>;

  template<class T>
  using ArrayOutput = typename DLA::VectorS<nOutput,T>;

  template<class T>
  using VectorX = typename PDEClass::template VectorX<T>;

  typedef ElementXField<PhysDim, TopoDim, TopologyType> ElementXFieldType;

  // ctor
  ElementProjectionIBLoutput_L2( const BasisType* basis,
                                 const int quadratureOrderRHS ) :
    BaseType(basis, 2*basis->order()),
    quadratureRHS_(quadratureOrderRHS)
  {
    if (basis_->order() == 1)
    {
      SANS_ASSERT_MSG( (basis_->category() == BasisFunctionCategory_Hierarchical),
                       "Assumess Hierarchical basis function which is a nodal basis for order =1." );
    }
  }

  // get output names
  static std::vector<std::string> outputNames()
  {
    std::vector<std::string> output_names(nOutput);

    output_names[(int) outputID::delta1s] = "delta1*";
    output_names[(int) outputID::theta11] = "theta11";
    output_names[(int) outputID::theta1s] = "theta1*";
    output_names[(int) outputID::H] = "H";
    output_names[(int) outputID::Hs] = "H*";
    output_names[(int) outputID::Cf1] = "C_f";
    output_names[(int) outputID::CDiss] = "C_Diss";
    output_names[(int) outputID::Retheta] = "Re_theta";
    output_names[(int) outputID::UeVinf] = "|Ue/Vinf|";
    output_names[(int) outputID::cp] = "c_p";
    output_names[(int) outputID::m] = "m";
    output_names[(int) outputID::qx] = "qx";
    output_names[(int) outputID::qz] = "qz";
    output_names[(int) outputID::nt] = "nt";
    output_names[(int) outputID::ctau] = "ctau";
    output_names[(int) outputID::gam] = "gam";
    output_names[(int) outputID::ctaueq] = "ctau_eq";
    output_names[(int) outputID::G] = "G";
    output_names[(int) outputID::dummy1] = "dummySplitAmpLag";
    output_names[(int) outputID::HsLami] = "H*lami";
    output_names[(int) outputID::HsTurb] = "H*turb";
    output_names[(int) outputID::dummy4] = "dummy";

    return output_names;
  }

  // project onto output field element from qfldElem and paramfldElem
  template <class T, class Tp, class To>
  void project( const PDEClass& pde,
                const Real& Vinf,
                const Element<ArrayQ<T>, TopoDim, TopologyType>& qfldElem,
                const Element<ArrayParam<Tp>, TopoDim, TopologyType>& paramfldElem,
                const ElementXFieldType& xfldElem,
                Element<ArrayOutput<To>, TopoDim, TopologyType>& outputfldElem ) const
  {
    const int nBasis = outputfldElem.basis()->nBasis();
    SANS_ASSERT(nDOF_ == nBasis);

    DLA::VectorD< ArrayOutput<To> > OutputDOFs(nBasis);

    const auto& varInterpret_ = pde.getVarInterpreter();
    const auto& paramInterpret_ = pde.getParamInterpreter();
    const auto& gasModel_ = pde.getGasModel();

    if (outputfldElem.order()==1) // linear nodal interpolation is used p=1
    {
      std::vector<Real> sRefArray = {0.0, 1.0};

      for (int i = 0; i < nBasis; ++i)
      {
        RefCoordType sRef = sRefArray[i];

        VectorX<Real> X = xfldElem.eval(sRef);
        Real x = X[0], z = X[1];

        VectorX<Real> e1;                // basis direction vector
        xfldElem.unitTangent(sRef, e1);

        ArrayQ<T> var = qfldElem.eval(sRef);
        ArrayParam<Tp> params = paramfldElem.eval(sRef);

        VectorX<Tp> q1 = paramInterpret_.getq1(params);
        Tp qe = paramInterpret_.getqe(params);
        Tp UeVinf = qe/Vinf;
        Tp cp = 1 - pow(UeVinf,2.0);

        Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
        Tp nue = (pde.getViscosityModel()).dynamicViscosity() / rhoe;

        const auto& profileCat = pde.getProfileCategory(var,x);
        const T gam = pde.calcIntermittency(params,e1,x,z,var);

        To delta1s = 0.0, theta11 = 0.0, theta1s = 0.0;
        To H = 0.0, Hs = 0.0;
        To Retheta = 0.0, m = 0.0;
        To Cf1 = 0.0, CDiss = 0.0;

        if (profileCat == IBLProfileCategory::TurbulentWake)
        {
          const auto& thicknessCoefficientsFunctor
            = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

          delta1s = thicknessCoefficientsFunctor.getdelta1s();
          theta11 = thicknessCoefficientsFunctor.gettheta11();
          theta1s = thicknessCoefficientsFunctor.gettheta1s();

          H = thicknessCoefficientsFunctor.getH();
          Hs = thicknessCoefficientsFunctor.getHs();

          Retheta = qe * theta11 / nue;
          m = rhoe*qe*delta1s;

          Cf1 = thicknessCoefficientsFunctor.getRetCf1() / Retheta;
          CDiss = thicknessCoefficientsFunctor.getRetCD() / Retheta;
        }
        else
        {
          const auto& thicknessCoefficientsFunctorLami
          = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);
          const auto& thicknessCoefficientsFunctorTurb
          = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

          delta1s = (1.0-gam)*thicknessCoefficientsFunctorLami.getdelta1s()
                        + gam*thicknessCoefficientsFunctorTurb.getdelta1s();
          theta11 = (1.0-gam)*thicknessCoefficientsFunctorLami.gettheta11()
                        + gam*thicknessCoefficientsFunctorTurb.gettheta11();
          theta1s = (1.0-gam)*thicknessCoefficientsFunctorLami.gettheta1s()
                        + gam*thicknessCoefficientsFunctorTurb.gettheta1s();

          H = (1.0-gam)*thicknessCoefficientsFunctorLami.getH()
                  + gam*thicknessCoefficientsFunctorTurb.getH();
          Hs = (1.0-gam)*thicknessCoefficientsFunctorLami.getHs()
                   + gam*thicknessCoefficientsFunctorTurb.getHs();

          Retheta = qe * theta11 / nue;
          m = rhoe*qe*delta1s;
          Cf1 = ((1.0-gam)*thicknessCoefficientsFunctorLami.getRetCf1()
                     + gam*thicknessCoefficientsFunctorTurb.getRetCf1()) / Retheta;
          CDiss = ((1.0-gam)*thicknessCoefficientsFunctorLami.getRetCD()
                       + gam*thicknessCoefficientsFunctorTurb.getRetCD()) / Retheta;
        }

        const auto& thicknessCoefficientsFunctor
          = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(profileCat, params, var, nue);
        To ctaueq = thicknessCoefficientsFunctor.getctaueq();

        ArrayOutput<To> output;
        output((int) outputID::delta1s) = delta1s;
        output((int) outputID::theta11) = theta11;
        output((int) outputID::theta1s) = theta1s;
        output((int) outputID::Cf1) = Cf1;
        output((int) outputID::CDiss) = CDiss;
        output((int) outputID::H) = H;
        output((int) outputID::Hs) = Hs;
        output((int) outputID::Retheta) = Retheta;
        output((int) outputID::UeVinf) = UeVinf;
        output((int) outputID::cp) = cp;
        output((int) outputID::m) = m;
        output((int) outputID::qx) = q1[0];
        output((int) outputID::qz) = q1[1];
        output((int) outputID::nt) = varInterpret_.getnt(var);
        output((int) outputID::ctau) = varInterpret_.getctau(var);
        output((int) outputID::gam) = gam;
        output((int) outputID::ctaueq) = ctaueq;
        output((int) outputID::G) = varInterpret_.getG(var);

        if (x <= 1.0)
        {
          const auto& thicknessCoefficientsFunctorLami
          = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(IBLProfileCategory::LaminarBL, params, var, nue);
          const auto& thicknessCoefficientsFunctorTurb
          = pde.getSecondaryVarObj().getCalculatorThicknessesCoefficients(IBLProfileCategory::TurbulentBL, params, var, nue);

          output((int) outputID::HsLami) = thicknessCoefficientsFunctorLami.getHs();
          output((int) outputID::HsTurb) = thicknessCoefficientsFunctorTurb.getHs();
        }

        OutputDOFs[i] = output;
      }
    }
    else // otherwise, L2 projection onto output element
      SANS_DEVELOPER_EXCEPTION("Not supported yet");

    for (int i = 0; i < nBasis; ++i)
      outputfldElem.DOF(i) = OutputDOFs[i];
  }

protected:
  Quadrature<TopoDim, TopologyType> quadratureRHS_;
};

} //namespace SANS

#endif /* SRC_PDE_IBL_ELEMENTPROJECTIONIBLOUTPUT_L2_H_ */
