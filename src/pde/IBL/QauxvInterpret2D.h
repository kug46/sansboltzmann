// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_QAUXVINTERPRET2D_H_
#define SRC_PDE_IBL_QAUXVINTERPRET2D_H_

#include <cmath>

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "IBL_Traits.h"

namespace SANS
{

class QauxvInterpret2D;

template<class PhysDim>
struct QauxvInterpret;

template<>
struct QauxvInterpret<PhysD2> { typedef QauxvInterpret2D type; };

//----------------------------------------------------------------------------//
// Auxiliary viscous variable (Qauxv) (aka IBL parameters) interpreter class
//
class QauxvInterpret2D
{
public:
  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;
  static const int N = IBLParamIndexer<PhysDim>::Nparam;

  // variable indexing
  static const int iqx = 0;
  static const int iqz = 1;
  static const int iqx_x = 2;
  static const int iqx_z = 3;
  static const int iqz_x = 4;
  static const int iqz_z = 5;
  static const int ip0 = 6;
  static const int iT0 = 7;

  static_assert(iT0 == N-1, "Primary unknown index overflows."); // only checks the largest index

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>; // solution/residual arrays

  template <class T>
  using VectorX = DLA::VectorS<D,T>; // size-D Cartesian physical vector (e.g. basis unit vector) type

  template <class T>
  using TensorX = DLA::MatrixS<D,D,T>; // size-D Cartesian physical tensor (e.g. velocity gradient) type

  //-----------------------Constructors-----------------------//
  QauxvInterpret2D() {}

  //-----------------------Methods-----------------------//

  // velocity vector q_1, defined as q_1 = q_e
  template<class T>
  VectorX<T> getq1(const ArrayQ<T>& qauxv) const { return {qauxv[iqx], qauxv[iqz]}; }

  template<class T>
  T getqe(const ArrayQ<T>& qauxv) const
  {
    const VectorX<T> q1 = getq1(qauxv);
    return sqrt(dot(q1, q1)); // edge speed q_e
  }

  // divergence of velocity q1
  template<class T>
  T getdivq1(const ArrayQ<T>& qauxv) const { return (qauxv[iqx_x] + qauxv[iqz_z]); }

  // gradient of velocity q1
  template<class T>
  TensorX<T> getgradq1(const ArrayQ<T>& qauxv) const
  {
    // grad(q1) = [[ -- grad(qx) --]    row 1
    //             [ -- grad(qz) --]]   row 2
    return {{qauxv[iqx_x], qauxv[iqx_z]}, {qauxv[iqz_x], qauxv[iqz_z]}};
  }

  template<class T>
  T getp0(const ArrayQ<T>& qauxv) const { return qauxv[ip0]; } // stagnation pressure

  template<class T>
  T getT0(const ArrayQ<T>& qauxv) const { return qauxv[iT0]; } // stagnation temperature

  // check state validity
  bool isValidState(const ArrayQ<Real>& qauxv) const
  {
    return (getp0(qauxv)>0) && (getT0(qauxv)>0);
  }

  template<class T>
  ArrayQ<T> setDOFFrom(const T& qx, const T& qz, const T& qx_x, const T& qx_z,
                       const T& qz_x, const T& qz_z, const T& p0, const T& T0) const
  {
    ArrayQ<T> var;
    var[iqx] = qx;
    var[iqz] = qz;
    var[iqx_x] = qx_x;
    var[iqx_z] = qx_z;
    var[iqz_x] = qz_x;
    var[iqz_z] = qz_z;
    var[ip0] = p0;
    var[iT0] = T0;

    return var;
  }

  template<class T>
  ArrayQ<T> setDOFFrom(const VectorX<T> q1, const VectorX<VectorX<T>>& gradq1, const T& p0, const T& T0) const
  {
    // gradq1 = {{qx_x, qz_x}, {qx_z, qz_z}}
    // note that this convention is intended to be consistent with what is in evalGradient() of the element class
    return setDOFFrom(q1[0], q1[1], gradq1[0][0], gradq1[1][0], gradq1[0][1], gradq1[1][1], p0, T0);
  }

  template<class T>
  ArrayQ<T> setDOFFrom(const VectorX<T> q1, const TensorX<T>& gradq1, const T& p0, const T& T0) const
  {
    // grad(q1) = [[ -- grad(qx) --]    row 1
    //             [ -- grad(qz) --]]   row 2
    return setDOFFrom(q1[0], q1[1], gradq1(0,0), gradq1(0,1), gradq1(1,0), gradq1(1,1), p0, T0);
  }
};

} // namespace SANS

#endif /* SRC_PDE_IBL_QAUXVINTERPRET2D_H_ */
