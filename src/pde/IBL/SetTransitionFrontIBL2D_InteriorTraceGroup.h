// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_SETTRANSITIONFRONTIBL2D_INTERIORTRACEGROUP_H_
#define SRC_PDE_IBL_SETTRANSITIONFRONTIBL2D_INTERIORTRACEGROUP_H_

#include "Field/XField.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

#include "PDEIBL2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class VarType, class TupleFieldType>
class SetTransitionFrontIBL2D_interiorTrace_impl:
    public GroupFunctorInteriorTraceType< SetTransitionFrontIBL2D_interiorTrace_impl<VarType, TupleFieldType> >
{
public:
  typedef VarInterpret2D<VarType> VarInterpretType;              // solution variable interpreter type

  typedef typename VarInterpretType::PhysDim PhysDim;
  typedef PDEIBL<PhysDim,VarType> PDEClass;

  static const int D = PhysDim::D;
  static const int N = IBLTraits<PhysDim>::N;    // solution dimension

  typedef typename IBLTraits<PhysDim>::template VectorX<Real> VectorX;
  typedef typename IBLTraits<PhysDim>::template ArrayQ<Real> ArrayQ;
  typedef typename IBLTraits<PhysDim>::template ArrayParam<Real> ArrayParam;

  static const int ixfld = 0;
  static const int iqfld = 1;
  static const int iparamfld = 2;

  // ctor
  SetTransitionFrontIBL2D_interiorTrace_impl(const PDEClass& pde,
                               const std::vector<int>& interiorTraceGroups,
//                               const TupleFieldType& tuplefld,
                               Real& xtr) :
    pde_(pde),
    interiorTraceGroups_(interiorTraceGroups),
//    tuplefld_(tuplefld),
    xtr_(xtr) {}

  std::size_t nInteriorTraceGroups() const          { return interiorTraceGroups_.size(); }
  std::size_t interiorTraceGroup(const int n) const { return interiorTraceGroups_.at(n);     }

  //----------------------------------------------------------------------------//
  // generic functor to set cell group DOFs
  template< class TopologyTrace, class TopologyL, class TopologyR>
  void
  apply( const typename TupleFieldType::template FieldCellGroupType<TopologyL>& fldsCellL,
         const int cellGroupGlobalL,
         const typename TupleFieldType::template FieldCellGroupType<TopologyR>& fldsCellR,
         const int cellGroupGlobalR,
         const typename XField<PhysDim, typename TopologyTrace::CellTopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
         const int interiorTraceGroupGlobal )
  {
    typedef typename TopologyL::TopoDim TopoDim;
    SANS_ASSERT_MSG( ( (std::is_same<TopoD1,TopoDim>::value)
                    && (std::is_same<Line,TopologyL>::value)
                    && (std::is_same<Line,TopologyR>::value) ),
                     "Only implemented for line grids");

    typedef typename TupleFieldType::template FieldCellGroupType<TopologyL> TupleFieldCellGroupType;

    typedef typename TupleType<ixfld, TupleFieldCellGroupType>::type XFieldCellGroupType;
    typedef typename TupleType<iqfld, TupleFieldCellGroupType>::type QFieldCellGroupType;
    typedef typename TupleType<iparamfld, TupleFieldCellGroupType>::type ParamFieldCellGroupType;

    typedef typename XField<PhysDim,TopoDim>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;

    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;

    const XFieldCellGroupType& xfldCellL = get<ixfld>(fldsCellL);
    const XFieldCellGroupType& xfldCellR = get<ixfld>(fldsCellR);

    const QFieldCellGroupType& qfldCellL = get<iqfld>(fldsCellL);
    const QFieldCellGroupType& qfldCellR = get<iqfld>(fldsCellR);

    const ParamFieldCellGroupType& paramfldCellL = get<iparamfld>(fldsCellL);
    const ParamFieldCellGroupType& paramfldCellR = get<iparamfld>(fldsCellR);

    // element field variables
    ElementXFieldClass xfldElemL( xfldCellL.basis() );
    ElementXFieldClass xfldElemR( xfldCellR.basis() );

    ElementQFieldClass qfldElemL( qfldCellL.basis() );
    ElementQFieldClass qfldElemR( qfldCellR.basis() );

    ElementParamFieldClass paramfldElemL( paramfldCellL.basis() );
    ElementParamFieldClass paramfldElemR( paramfldCellR.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    // assert some assumptions about polynomial order //TODO
    SANS_ASSERT_MSG( qfldCellL.order() == 0 || qfldCellL.order() == 1, "Solution order does not match" );
    SANS_ASSERT_MSG( qfldCellR.order() == 0 || qfldCellR.order() == 1, "Solution order does not match" );

    SANS_ASSERT_MSG( paramfldCellL.order() == 1 , "Parameter order does not match" );
    SANS_ASSERT_MSG( paramfldCellR.order() == 1 , "Parameter order does not match" );

    SANS_ASSERT_MSG( xfldCellL.order() == 1 , "Assume linear grid element!" );
    SANS_ASSERT_MSG( xfldCellR.order() == 1 , "Assume linear grid element!" );

    // PDE-specific objects
    const auto& transitionModel = pde_.getTransitionModel();
    const auto& varInterpret = pde_.getVarInterpreter();

    std::vector<VectorX> Xtr_vec; // container for recorded transition location

    // loop over elements within the trace group
    const int nelem = xfldTrace.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      const int elemL = xfldTrace.getElementLeft( elem );
      const int elemR = xfldTrace.getElementRight( elem );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      xfldCellR.getElement( xfldElemR, elemR );

      qfldCellL.getElement( qfldElemL, elemL );
      qfldCellR.getElement( qfldElemR, elemR );

      paramfldCellL.getElement( paramfldElemL, elemL );
      paramfldCellR.getElement( paramfldElemR, elemR );

      xfldTrace.getElement( xfldElemTrace, elem );

      // left/right reference-element coords
      typedef DLA::VectorS<TopoDim::D,Real> RefCoordType;

      const RefCoordType sRefTrace = Node::centerRef;

      const CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elem );
      const CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elem );

      RefCoordType sRefL, sRefR;
      TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyL>::eval( canonicalTraceL, sRefTrace, sRefL );
      TraceToCellRefCoord<TopologyTrace, TopoDim, TopologyR>::eval( canonicalTraceR, sRefTrace, sRefR );

      VectorX nL;                      // unit trace normal vector
      traceUnitNormal(xfldElemTrace, sRefTrace, xfldElemL, sRefL, xfldElemR, sRefR, nL);

      const Real ntL = varInterpret.getnt( qfldElemL.eval( sRefL ) );
      const Real ntR = varInterpret.getnt( qfldElemR.eval( sRefR ) );

      const ArrayParam paramL = paramfldElemL.eval( sRefL );
      const ArrayParam paramR = paramfldElemR.eval( sRefR );

      const VectorX q1L = pde_.getParamInterpreter().getq1(paramL);

      const Real qnL = dot(nL, q1L);
      const bool isTrL = transitionModel.isNaturalTransition(ntL);
      const bool isTrR = transitionModel.isNaturalTransition(ntR);

      const VectorX Xtr = xfldElemTrace.coordinates();

      if (isTrL != isTrR)
      {
        if ( ( qnL > 0 && !isTrL && isTrR ) || ( qnL < 0 && isTrL && !isTrR ) )
        { // flows from laminar to turbulent
          Xtr_vec.push_back(Xtr);
        }
        else
        {
          std::cout << "Flow goes from turbulent to laminar at (x,y) = " << std::setprecision(16) << Xtr << std::endl;
        }
      }

    } // loop elem

    const int n_xtr = int(Xtr_vec.size());

    std::cout << std::fixed << std::setprecision(16); // same effects, but using manipulators

    if (n_xtr > 0)
    {
      xtr_ = (Xtr_vec.at(0))[0];
      std::cout << "Computed forced transition location xtr_ = " << xtr_ << std::endl;
    }
    else
    {
      //TODO: hacky now
      std::cout << std::endl;
      std::cout << "Transition front cannot be found at element interfaces." << std::endl;
      std::cout << std::endl;
    }

    std::cout << "# of natural transition points at element interfaces = " << n_xtr << std::endl;
    for (int j = 0; j < n_xtr; ++j)
    {
      std::cout << Xtr_vec.at(j);
      if (fabs(xtr_ - Xtr_vec[j][0]) > 1.e+3*std::numeric_limits<Real>::epsilon())
      {
        std::cout << "  --> a different transition front j = " << j;
      }
      std::cout << std::endl;
    }
  }

protected:
  const PDEClass& pde_;
  const std::vector<int> interiorTraceGroups_;
//  const TupleFieldType& tuplefld_;
  Real& xtr_;
};


// Factory function

template <class VarType, class TupleFieldType>
SetTransitionFrontIBL2D_interiorTrace_impl<VarType,TupleFieldType>
SetTransitionFrontIBL2D_interiorTrace(
    const typename SetTransitionFrontIBL2D_interiorTrace_impl<VarType, TupleFieldType>::PDEClass& pde,
    const std::vector<int>& interiorTraceGroups,
    const TupleFieldType& tuplefld,
    Real& xtr)
{
  return SetTransitionFrontIBL2D_interiorTrace_impl<VarType,TupleFieldType>(pde, interiorTraceGroups, xtr);
}

} // namespace SANS

#endif /* SRC_PDE_IBL_SETTRANSITIONFRONTIBL2D_INTERIORTRACEGROUP_H_ */
