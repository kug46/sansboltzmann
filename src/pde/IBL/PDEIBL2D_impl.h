// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_PDEIBL2D_IMPL_H_
#define SRC_PDE_IBL_PDEIBL2D_IMPL_H_

#include "PDEIBL2D.h"

#define IsContinousInterface_HDG 0 // true if mass conservation is explicitly enforced at transition front; false otherwise

namespace SANS
{

#if IsContinousInterface_HDG
// with explicit mass and momentum conservation at transition front

template<class VarType>
template<class TL, class TI, class Tp, class Tf>
inline void
PDEIBL<PhysD2,VarType>::interfaceFluxAndSourceHDGIBL(
    const ArrayParam<Tp>& paramsL, const VectorX<Real>& e1L,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<TL>& varL, const ArrayQ<TI>& varI,
    const Real& nxL, const Real& nzL,
    ArrayQ<Tf>& fL, ArrayQ<Tf>& sourceL ) const
{
  // L and I denote left and interface quantities respectively

  const ProfileCategory profileCatL = getProfileCategory(varL,x);

  const TL ntL = varInterpret_.getnt( varL );
  const TL ctauL = varInterpret_.getctau( varL );

  const VectorX<Tp> q1L = paramInterpret_.getq1(paramsL);

  const Tp qeL = paramInterpret_.getqe(paramsL);
  const Tp rhoeL = gasModel_.density(qeL, paramInterpret_.getp0(paramsL), paramInterpret_.getT0(paramsL));
  const Tp nueL = viscosityModel_.dynamicViscosity() / rhoeL;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const ThicknessesCoefficientsFunctor<Tp, TL> thicknessesCoefficientsFunctorL
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatL, paramsL, varL, nueL);

  const Tf delta1sL  = thicknessesCoefficientsFunctorL.getdelta1s();
  const Tf theta11L = thicknessesCoefficientsFunctorL.gettheta11();
  const Tf theta1sL = thicknessesCoefficientsFunctorL.gettheta1s();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  // P is symmetric in 2D IBL. Note "*" in () is an outer product
  const TensorX<Tf> PL = rhoeL*theta11L*(q1L*Transpose(q1L)); // [tensor] P: momentum defect flux
  const VectorX<Tf> PTdoteL = Transpose(PL) * e1L; // P^T dot e

  const VectorX<Tf> KL = rhoeL*pow(qeL,2)*theta1sL*q1L; // [vector] K: kinetic energy defect flux

  const VectorX<Real> nrmL = {nxL, nzL}; // trace normal vector

  // Full upwind
  Tf f_mom = 0.0, f_ke = 0.0, f_amp = 0.0;
  Tf f_lag = 0.0;

  const Tp q1nL = dot(q1L, nrmL);

  // Assume q1L and nrm are not orthogonal.
  if (q1nL > 0)
  {
    f_mom = dot(PTdoteL, nrmL);
    f_ke = dot(KL, nrmL);
    f_amp = ntL * q1nL;
    f_lag = ctauL * q1nL;

    sourceL[ir_momLami] += delta1sL;
    sourceL[ir_keLami] += theta11L;

    sourceL[ir_momTurb] += varInterpret_.dummySource_momTurb(varI);
    sourceL[ir_keTurb] += varInterpret_.dummySource_keTurb(varI);

    sourceL[ir_amp] += ntL;

    if (profileCatL == ProfileCategory::LaminarBL ||
        profileCatL == ProfileCategory::LaminarWake)
    {
      // do nothing
    }
    else if (profileCatL == ProfileCategory::TurbulentBL ||
             profileCatL == ProfileCategory::TurbulentWake)
    {
      sourceL[ir_lag] += ctauL;
    }
#if IsExplicitCheckingIBL
    else
      SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");
#endif
  }
  else if (q1nL < 0)
  {
    f_mom = varInterpret_.getdeltaLami(varI); // here, the variable deltaLami is actually a flux magnitude
    f_ke = varInterpret_.getALami(varI);
    f_amp = varInterpret_.getnt(varI);
    f_lag = varInterpret_.getctau(varI);

    sourceL[ir_momLami] += -delta1sL;
    sourceL[ir_keLami] += -theta11L;
    sourceL[ir_amp] += -ntL;

    if (profileCatL == ProfileCategory::LaminarBL ||
        profileCatL == ProfileCategory::LaminarWake)
    {
      sourceL[ir_lag] += varInterpret_.dummySource_lag(varI);
    }
    else if (profileCatL == ProfileCategory::TurbulentBL ||
             profileCatL == ProfileCategory::TurbulentWake)
    {
      sourceL[ir_lag] += -ctauL;
    }
#if IsExplicitCheckingIBL
else
  SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");
#endif
  }
  else //TODO: else?
    SANS_DEVELOPER_EXCEPTION("Stagnation point appears!");

  const Tf gamL = calcIntermittency(paramsL, e1L, x, z, varL);

  fL[ir_momLami] += (1.0-gamL)*f_mom;
  fL[ir_keLami] += (1.0-gamL)*f_ke;
  fL[ir_momTurb] += gamL*f_mom;
  fL[ir_keTurb] += gamL*f_ke;

#if defined(NOAMPEQN_IBL)
  fL[ir_amp] += 0.0*f_amp;
#else
  fL[ir_amp] += f_amp;
#endif

  fL[ir_lag] += gamL*f_lag;
}

#else // match mass and momentum at interfaces weakly

#if 0 // continuous nt

template<class VarType>
template<class TL, class TI, class Tp, class Tf>
inline void
PDEIBL<PhysD2,VarType>::interfaceFluxAndSourceHDGIBL(
    const ArrayParam<Tp>& paramsL, const VectorX<Real>& e1L,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<TL>& varL, const ArrayQ<TI>& varI,
    const Real& nxL, const Real& nzL,
    ArrayQ<Tf>& fL, ArrayQ<Tf>& sourceL ) const
{
  // L and I denote left and interface quantities respectively

  // DETERMINE CLOSURE TYPE -----------------------------
  ProfileCategory profileCatL = getProfileCategory(varL,x);

  const TL deltaL = getdelta(varL, profileCatL);
  const TL AL = getA(varL, profileCatL);
  const TL ntL = varInterpret_.getnt( varL );
  const TL ctauL = varInterpret_.getctau( varL );

  const VectorX<Tp> q1L = paramInterpret_.getq1(paramsL);

  // derived variables
  const Tp mue = viscosityModel_.dynamicViscosity();
  const Tp qeL = sqrt( dot(q1L,q1L) );
  const Tp rhoeL = calcrhoe( paramsL );
  const Tf RedeltaL = rhoeL*qeL*deltaL/mue;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const SecondaryVarFunctor<TL,Tf> secondaryVarFcnL
    = secondaryVarObj_.secondaryVarCalc(profileCatL, deltaL, AL, ctauL, RedeltaL);

  const Tf theta11L  = secondaryVarFcnL.gettheta11();
  const Tf theta1sL  = secondaryVarFcnL.gettheta1s();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  // P is symmetric in 2D IBL. Note "*" in () is an outer product
  const TensorX<Tf> PL = rhoeL*theta11L*(q1L*Transpose(q1L)); // [tensor] P: momentum defect flux
  const VectorX<Tf> PTdoteL = Transpose(PL) * e1L; // P^T dot e

  const VectorX<Tf> KL = rhoeL*pow(qeL,2)*theta1sL*q1L; // [vector] K: kinetic energy defect flux

  const VectorX<Real> nrmL = {nxL, nzL}; // trace normal vector
  const Tp q1nL = dot(q1L, nrmL);

  // Full upwind
  Tf f_mom = 0.0, f_ke = 0.0, f_amp = 0.0, f_lag = 0.0;

  const Tf delta1sL  = secondaryVarFcnL.getdelta1s();

  // Assume q1L and nrm are not orthogonal.
  if (q1nL > 0)
  {
    f_mom = dot(PTdoteL, nrmL);
    f_ke = dot(KL, nrmL);
    f_amp = ntL * q1nL;
    f_lag = ctauL * q1nL;

    sourceL[ir_momLami] += varInterpret_.getdeltaLami(varI) - (f_mom);
    sourceL[ir_keLami] += varInterpret_.getALami(varI) - (f_ke);

    sourceL[ir_momTurb] += varInterpret_.dummySource_momTurb(varI); // By default, take turb delta and A as dummy variables
    sourceL[ir_keTurb] += varInterpret_.dummySource_keTurb(varI);

    sourceL[ir_amp] += ntL;

    sourceL[ir_lag] += varInterpret_.getctau(varI) - (f_lag);
  }
  else if (q1nL < 0)
  {
    f_mom = -varInterpret_.getdeltaLami(varI); // here, the variable deltaLami is acutally a flux magnitude
    f_ke = -varInterpret_.getALami(varI);
    f_amp = -varInterpret_.getnt(varI);
    f_lag = -varInterpret_.getctau(varI);

    sourceL[ir_amp] += -ntL;
  }
  else //TODO: else?
    SANS_DEVELOPER_EXCEPTION("Stagnation point appears!");

  Tf gamL = calcIntermittency(paramsL, e1L, x, z, varL);

  fL[ir_momLami] += (1.0-gamL)*f_mom;
  fL[ir_keLami] += (1.0-gamL)*f_ke;
  fL[ir_momTurb] += gamL*f_mom;
  fL[ir_keTurb] += gamL*f_ke;

#if defined(NOAMPEQN_IBL)
  fL[ir_amp] += 0.0*f_amp;
#else
  fL[ir_amp] += f_amp;
#endif

  fL[ir_lag] += gamL*f_lag;
}

#else // discontinuous nt

template<class VarType>
template<class TL, class TI, class Tp, class Tf>
inline void
PDEIBL<PhysD2,VarType>::interfaceFluxAndSourceHDGIBL(
    const ArrayParam<Tp>& paramsL, const VectorX<Real>& e1L,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<TL>& varL, const ArrayQ<TI>& varI,
    const Real& nxL, const Real& nzL,
    ArrayQ<Tf>& fL, ArrayQ<Tf>& sourceL ) const
{
  //Note that varI here is interpreted in the same manner as varL

  // L and I denote left and interface quantities respectively

  // DETERMINE CLOSURE TYPE -----------------------------
  ProfileCategory profileCatL = getProfileCategory(varL,x);
  ProfileCategory profileCatI = getProfileCategory(varI,x);

  TL ntL = varInterpret_.getnt( varL );
  TI ntI = varInterpret_.getnt( varI );

  TL ctauL = varInterpret_.getctau( varL );
  TI ctauI = varInterpret_.getctau( varI );

  // velocity vector q_1, defined as q_1 = q_e
  VectorX<Tp> q1L = paramInterpret_.getq1(paramsL);

  // derived variables
  Tp qeL = paramInterpret_.getqe(paramsL);
  const Tp rhoeL = gasModel_.density(qeL, paramInterpret_.getp0(paramsL), paramInterpret_.getT0(paramsL));
  const Tp nueL = viscosityModel_.dynamicViscosity() / rhoeL;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const ThicknessesCoefficientsFunctor<Tp, TL> thicknessesCoefficientsFunctorL
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatL, paramsL, varL, nueL);
  const ThicknessesCoefficientsFunctor<Tp, TI> thicknessesCoefficientsFunctorI
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatI, paramsL, varI, nueL);

  Tf theta11L = thicknessesCoefficientsFunctorL.gettheta11();
  Tf theta1sL = thicknessesCoefficientsFunctorL.gettheta1s();

  Tf theta11I = thicknessesCoefficientsFunctorI.gettheta11();
  Tf theta1sI = thicknessesCoefficientsFunctorI.gettheta1s();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  // P is symmetric in 2D IBL. Note "*" in () is an outer product
  TensorX<Tf> PL = rhoeL*theta11L*(q1L*Transpose(q1L)); // [tensor] P: momentum defect flux
  TensorX<Tf> PI = rhoeL*theta11I*(q1L*Transpose(q1L));

  VectorX<Tf> PTdoteL = Transpose(PL) * e1L; // P^T dot e
  VectorX<Tf> PTdoteI = Transpose(PI) * e1L;

  VectorX<Tf> KL = rhoeL*pow(qeL,2)*theta1sL*q1L; // [vector] K: kinetic energy defect flux
  VectorX<Tf> KI = rhoeL*pow(qeL,2)*theta1sI*q1L;

  const VectorX<Real> nrmL = {nxL, nzL}; // trace normal vector
  const Tp q1nL = dot(q1L, nrmL);

  // Full upwind
  Tf f_mom = 0.0, f_ke = 0.0, f_amp = 0.0;

  // Assume q1L and nrm are not orthogonal.
  if (q1nL > 0)
  {
    f_mom = dot(PTdoteL, nrmL);
    f_ke = dot(KL, nrmL);
    f_amp = ntL * q1nL;

    const Tf delta1sL = thicknessesCoefficientsFunctorL.getdelta1s();
    const Tf delta1sI = thicknessesCoefficientsFunctorI.getdelta1s();

    if (profileCatI == ProfileCategory::LaminarBL ||
        profileCatI == ProfileCategory::LaminarWake)
    {
      sourceL[ir_momLami] += delta1sI - delta1sL;
      sourceL[ir_keLami] += theta11I - theta11L;
      sourceL[ir_momTurb] += varInterpret_.dummySource_momTurb(varI);
      sourceL[ir_keTurb] += varInterpret_.dummySource_keTurb(varI);
    }
    else if (profileCatI == ProfileCategory::TurbulentBL ||
             profileCatI == ProfileCategory::TurbulentWake)
    {
      sourceL[ir_momLami] += varInterpret_.dummySource_momLami(varI);
      sourceL[ir_keLami] += varInterpret_.dummySource_keLami(varI);
      sourceL[ir_momTurb] += delta1sI - delta1sL;
      sourceL[ir_keTurb] += theta11I - theta11L;
    }
#if IsExplicitCheckingIBL
    else
      SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");
#endif

    sourceL[ir_amp] += ntI - ntL;

    if ( profileCatL == profileCatI ) // interface without transition
    {
      // Assume q1L and nrm are not orthogonal.
      sourceL[ir_lag] += ctauI - ctauL;
    }
    else if (profileCatL == ProfileCategory::LaminarBL && profileCatI == ProfileCategory::TurbulentBL)
    { // interface coincident with the transition front: flow from laminar into turbulent
      //      SANS_DEVELOPER_EXCEPTION("Not totally sure if this is necessary yet. Worth keeping an eye on it"); //TODO
      //TODO: inflow condition on ctau has been set by the unwind flux
      const Tf ctau_eqLami = thicknessesCoefficientsFunctorL.getctaueq();
      const Tf H_Lami = thicknessesCoefficientsFunctorL.getH();
      const Tf CTR = 1.8*exp(-3.3/(H_Lami-1.0)); // post-transition fitting factor

      // Post-transition ctau fitting (as in XFOIL v6.99, xblsys.f, ~Line 1394)
      const Tf ctau_TurbInit = pow(CTR,2.0)*ctau_eqLami; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)

      sourceL[ir_lag] += ctauI - ctau_TurbInit;
    }
    else if (profileCatL == ProfileCategory::TurbulentBL && profileCatI == ProfileCategory::LaminarBL)
      sourceL[ir_lag] += ctauI - ctauL;
  }
  else if (q1nL < 0)
  {
    f_mom = dot(PTdoteI, nrmL);
    f_ke = dot(KI, nrmL);
    f_amp = ntI * q1nL;
  }
  else //TODO: else?
    SANS_DEVELOPER_EXCEPTION("Stagnation point appears!");

  Tf gamL = calcIntermittency(paramsL, e1L, x, z, varL);

  fL[ir_momLami] += (1.0-gamL)*f_mom;
  fL[ir_keLami] += (1.0-gamL)*f_ke;
  fL[ir_momTurb] += gamL*f_mom;
  fL[ir_keTurb] += gamL*f_ke;

#if defined(NOAMPEQN_IBL)
  fL[ir_amp] += 0.0*f_amp;
#else
  fL[ir_amp] += f_amp;
#endif

  // compute interface flux depending on the type of the interface
  Tf f_lag = 0.0;

  if ( profileCatL == profileCatI ) // interface without transition
  {
    // Full upwind

    // Assume q1L and nrm are not orthogonal.
    if (q1nL > 0)
      f_lag = ctauL * q1nL;
    else
      f_lag = ctauI * q1nL;
  }
  else if ( (q1nL < 0 && profileCatI == ProfileCategory::LaminarBL && profileCatL == ProfileCategory::TurbulentBL) )
  { // interface coincident with the transition front: flow from laminar into turbulent
//    SANS_DEVELOPER_EXCEPTION("Not totally sure if this is necessary yet. Worth keeping an eye on it"); //TODO

    Tf delta1sLami, theta11Lami, ctau_eqLami;

    delta1sLami = thicknessesCoefficientsFunctorI.getdelta1s();
    theta11Lami = theta11I;
    ctau_eqLami = thicknessesCoefficientsFunctorI.getctaueq();

    const Tf H_Lami = delta1sLami/theta11Lami;
    const Tf CTR = 1.8*exp(-3.3/(H_Lami-1.0)); // post-transition fitting factor

    // Post-transition ctau fitting (as in XFOIL v6.99, xblsys.f, ~Line 1394)
    const Tf ctau_TurbInit = pow(CTR,2.0)*ctau_eqLami; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)

    f_lag = ctau_TurbInit * q1nL;
  }
  else if ( (q1nL > 0 && profileCatI == ProfileCategory::LaminarBL && profileCatL == ProfileCategory::TurbulentBL) )
  { // interface coinciding with the transition front: flow from turbulent into laminar
//    SANS_DEVELOPER_EXCEPTION("Not totally sure if this is necessary yet. Worth keeping an eye on it");
    f_lag = ctauL * q1nL;
  }
  else if ( profileCatL == ProfileCategory::LaminarBL )
    f_lag = 0.0;
#if IsExplicitCheckingIBL
  else
    SANS_DEVELOPER_EXCEPTION("profileCatL and profileR combination is not allowed!");
#endif

#if defined(NOLAGEQN_IBL)
    fL[ir_lag] += 0.0;
#else
    fL[ir_lag] += gamL*f_lag;
#endif
}

#endif

#endif

} // namespace SANS

#endif /* SRC_PDE_IBL_PDEIBL2D_IMPL_H_ */
