// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// TODO: hacky includes for now, since vera does now allow length source file
#include "PDEIBL2D.h"
#include "PDEIBL2D_impl.h"

namespace SANS
{

template<class VarType>
void
PDEIBL<PhysD2,VarType>::dump( int indentSize, std::ostream& out ) const
{
  gasModel_.dump(indentSize,out);
  viscosityModel_.dump(indentSize,out);
}

//----------------------------------------------------------------------------//
// Explicitly instantiations
template class PDEIBL<PhysD2,VarTypeDANCt>;
template class PDEIBL<PhysD2,VarTypeDsThNCt>;
template class PDEIBL<PhysD2,VarTypeDsThNG>;
}
