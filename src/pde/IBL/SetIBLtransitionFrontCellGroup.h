// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_SETIBLTRANSITIONFRONTCELLGROUP_H_
#define SRC_PDE_IBL_SETIBLTRANSITIONFRONTCELLGROUP_H_

#include "Field/XField.h"
#include "Field/FieldLine_DG_Cell.h"
#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

#include "PDEIBL2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class VarType>
class SetTransitionFrontIBL2D_impl:
    public GroupFunctorCellType< SetTransitionFrontIBL2D_impl<VarType> >
{
public:
  typedef VarInterpret2D<VarType> VarInterpretType;              // solution variable interpreter type

  typedef typename VarInterpretType::PhysDim PhysDim;
  typedef PDEIBL<PhysDim,VarType> PDEClass;

  static const int D = PhysDim::D;
  static const int N = IBLTraits<PhysDim>::N;    // solution dimension

  typedef typename IBLTraits<PhysDim>::template VectorX<Real> VectorX;
  typedef typename IBLTraits<PhysDim>::template ArrayQ<Real> ArrayQ;
  typedef typename IBLTraits<PhysDim>::template ArrayParam<Real> ArrayParam;

  template <class T>
  using QFieldType = Field_DG_Cell<PhysDim,TopoD1,ArrayQ>;

  template <class T>
  using ParamFieldType = Field_DG_Cell<PhysDim,TopoD1,ArrayParam>;


  // ctor
  SetTransitionFrontIBL2D_impl(const PDEClass& pde,
                               const QFieldType<Real>& qfld,
                               const ParamFieldType<Real>& paramfld,
                               const std::vector<int>& cellGroups,
                               Real& xtr,
                               bool& isTrFoundInCell) :
    pde_(pde),
    qfld_(qfld),
    paramfld_(paramfld),
    cellGroups_(cellGroups),
    xtr_(xtr),
    isTrFoundInCell_(isTrFoundInCell) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // generic functor to set cell group DOFs
  template <class Topology>
  void
  apply(const typename XField<PhysDim, typename Topology::TopoDim>::template FieldCellGroupType<Topology>& xfldCell,
        const int cellGroupGlobal)
  {
    SANS_ASSERT_MSG( ((std::is_same<TopoD1,typename Topology::TopoDim>::value) && (std::is_same<Line,Topology>::value)),
                     "Only implemented for line grids");

    typedef typename XField<PhysDim,typename Topology::TopoDim>                 ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim,typename Topology::TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;
    typedef typename Field<PhysDim,typename Topology::TopoDim, ArrayParam>::template FieldCellGroupType<Topology> ParamFieldCellGroupType;

    typedef typename XFieldCellGroupType    ::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;
    typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;

    const QFieldCellGroupType& qfldCell = qfld_.template getCellGroupGlobal<Line>(cellGroupGlobal);
    const ParamFieldCellGroupType& paramfldCell = paramfld_.template getCellGroupGlobal<Line>(cellGroupGlobal);

    const int order_q = qfldCell.order();
    const int order_param = paramfldCell.order();
    SANS_ASSERT_MSG( order_q == 0 || order_q == 1, "Solution order does not match" );
    SANS_ASSERT_MSG( order_param == 1 , "Parameter order does not match" );
    SANS_ASSERT_MSG( xfldCell.order() == 1, "Assume linear grid element!");

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );
    ElementParamFieldClass paramfldElem( paramfldCell.basis() );

    // PDE-specific objects
    const auto& transitionModel = pde_.getTransitionModel();
    const auto& varInterpret = pde_.getVarInterpreter();

    // loop over elements within group
    const int nelem = xfldCell.nElem();

    std::vector<VectorX> Xtr_vec;

    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/solution DOFs to element
      xfldCell.getElement( xfldElem, elem );
      qfldCell.getElement( qfldElem, elem );
      paramfldCell.getElement( paramfldElem, elem );

      // check if cut-cell "natural" transition treatment is needed for the current element

      const int nnode = Topology::NNode; // number of vertices/nodes; 2 for a line element
      const std::vector<Real> sRefNode_list = {0.0, 1.0};
      SANS_ASSERT_MSG(nnode == sRefNode_list.size(), "Incorrect number of vertices are initialized!");
#if 0 //TODO: can switch to the following method for generalization
      // method 1
      std::vector<Real> sRefNode_list
      getLagrangeNodes_Line(1, getLagrangeNodes_Line(order, s););

      // method 2
      std::vector<DLA::VectorS<typename Topology::TopoDim::D,Real>> sRefNodes;
      LagrangeNodes<Topology>::get(1, sRefNodes);
#endif

      std::vector<VectorX> XNode_list(nnode);
      std::vector<Real> ntNode_list(nnode);
      std::vector<bool> isTrNode_list(nnode); // if transition happens at a node

      bool isCutCellNaturalTransitionElem = false; // whether the current element contains the transition front (which requires a cell cut)
      if ( transitionModel.isTransitionActive() )
      {
        for (int j = 0; j < nnode; ++j)
        {
          Real sRef = sRefNode_list.at(j);
          XNode_list.at(j) = xfldElem.coordinates(sRef);
          ntNode_list.at(j) = varInterpret.getnt(qfldElem.eval(sRef));
          isTrNode_list.at(j) = transitionModel.isNaturalTransition(ntNode_list.at(j));
        }

        // whether the current element is fully laminar or turbulent
        const bool isLamiElem = std::all_of(isTrNode_list.begin(), isTrNode_list.end(), [](bool isTr){return !isTr;});
        const bool isTurbElem = std::all_of(isTrNode_list.begin(), isTrNode_list.end(), [](bool isTr){return isTr;});

        // transition happens on the current element if it is neither fully laminar nor fully turbulent
        isCutCellNaturalTransitionElem = !(isLamiElem || isTurbElem);
      }

      if (isCutCellNaturalTransitionElem) // cut-cell treatment
      {
        // compute natural transition location in terms of element reference coordinate sRef
        const Real sRefTr_natural = ( transitionModel.ntcrit() - ntNode_list.at(0) ) / ( ntNode_list.at(1) - ntNode_list.at(0) );
        const VectorX Xtr = xfldElem.coordinates(sRefTr_natural);

        Xtr_vec.push_back(Xtr);
      }
    }  // loop elem

    const int n_xtr = int(Xtr_vec.size());

    std::cout << std::fixed << std::setprecision(16); // same effects, but using manipulators

    std::cout << "# of natural transition points within elements = " << n_xtr << std::endl;
    for (int j = 0; j < n_xtr; ++j)
    {
      std::cout << Xtr_vec.at(j);
      if (fabs(xtr_ - Xtr_vec[j][0]) > 1.e+3*std::numeric_limits<Real>::epsilon())
      {
        std::cout << "  --> a different transition front j = " << j;
      }
      std::cout << std::endl;
    }

    if (n_xtr > 0)
    {
      xtr_ = (Xtr_vec.at(0))[0];
//      for (int i=1; i<n_xtr; ++i)
//        xtr_ = min(xtr_, (Xtr_vec.at(i))[0]); // pick the smallest x

      isTrFoundInCell_ = true;
      std::cout << "Computed forced transition location xtr_ = " << xtr_ << std::endl;
    }
    else
    {
      isTrFoundInCell_ = false;
      std::cout << std::endl;
      std::cout << "Transition front cannot be found within an element. Need to look into element interfaces." << std::endl;
      std::cout << std::endl;
    }
  }

protected:
  const PDEClass& pde_;
  const QFieldType<Real>& qfld_;
  const ParamFieldType<Real>& paramfld_;
  const std::vector<int> cellGroups_;
  Real& xtr_;
  bool& isTrFoundInCell_;
};


// Factory function

template <class VarType>
SetTransitionFrontIBL2D_impl<VarType>
SetTransitionFrontIBL2D(const typename SetTransitionFrontIBL2D_impl<VarType>::PDEClass& pde,
                               const typename SetTransitionFrontIBL2D_impl<VarType>::template QFieldType<Real>& qfld,
                               const typename SetTransitionFrontIBL2D_impl<VarType>::template ParamFieldType<Real>& paramfld,
                               const std::vector<int>& cellGroups,
                               Real& xtr,
                               bool& isTrFoundInCell)
{
  return {pde, qfld, paramfld, cellGroups, xtr, isTrFoundInCell};
}

} // namespace SANS

#endif /* SRC_PDE_IBL_SETIBLTRANSITIONFRONTCELLGROUP_H_ */
