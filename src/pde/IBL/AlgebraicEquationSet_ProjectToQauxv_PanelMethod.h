// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_ALGEBRAICEQUATIONSET_PROJECTTOQAUXV_PANELMETHOD_H_
#define SRC_PDE_IBL_ALGEBRAICEQUATIONSET_PROJECTTOQAUXV_PANELMETHOD_H_

#include "LinearAlgebra/DenseLinAlg/DynamicSize/MatrixD_Mul.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

#include "Discretization/Galerkin/AlgebraicEquationSet_Project.h"

#include "PanelMethod/FunctionEvalQauxv_IBL.h"

#include "Surreal/SurrealS.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Algebraic equation set specialized for auxiliary viscous equations
// i.e. L^2 projection onto auxiliary viscous variables in coupling with panel method equations
// \int phi (q - f) = 0, where phi is weighting function (basis), q is auxiliary viscous variable, and f(gamma,lambda) is forcing function
//
template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits, class PanelEqnClass>
class AlgebraicEquationSet_ProjectToQauxv_PanelMethod :
    public AlgebraicEquationSet_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits>
{
public:
  typedef AlgebraicEquationSet_Project<ParamFieldType,IntegrandCellClass,TopoDim,Traits> BaseClassType;
  typedef typename IntegrandCellClass::FunctionNDConvertType FunctionNDConvertType;
  typedef typename PanelEqnClass::PanelClass PanelMethodType;
  typedef FunctionEvalQauxv<PanelMethodType,Real,Real> FunctionEvalQauxvType;

  typedef typename FunctionEvalQauxvType::TopologyType Topology;

  typedef Traits TraitsTag;

  using typename BaseClassType::PhysDim;
  using typename BaseClassType::ArrayQ;
  using typename BaseClassType::SystemMatrix;
  using typename BaseClassType::SystemVector;
  using typename BaseClassType::SystemVectorView;

  template <class T>
  using GamFieldType = typename PanelEqnClass::template GamFieldType<T>;

  template <class T>
  using LamFieldType = typename PanelEqnClass::template LamFieldType<T>;

  using BaseClassType::iProj;
  using BaseClassType::iq;

  static const int N = BaseClassType::N;
  static const int NQinv = PanelEqnClass::N;

  template<class T>
  using ArrayQauxv = DLA::VectorS<N,T>;

  using MatrixParam = DLA::MatrixS<N,NQinv,Real>;

  typedef SurrealS<NQinv> SurrealQinvType;

  typedef DLA::MatrixD<typename BaseClassType::MatrixQ> MatrixMassElemClass;
  typedef GalerkinWeightedIntegral_New<TopoDim, Topology, MatrixMassElemClass> MatrixMassElemIntegralType;
  typedef GalerkinWeightedIntegral<TopoDim, Topology, ArrayQ> ResidualElemIntegralType;

  typedef DLA::MatrixD<MatrixParam> MatrixElemClass;

  static const int order_qauxv = FunctionEvalQauxvType::order_qauxv;


  AlgebraicEquationSet_ProjectToQauxv_PanelMethod(const ParamFieldType& paramfld,
                                                  Field<PhysDim, TopoDim, ArrayQ>& qfld,
                                                  const IntegrandCellClass& fcnCell,
                                                  const QuadratureOrder& quadratureOrder,
                                                  const std::array<Real,1> tol,
                                                  const PanelEqnClass& panelEqnSet,
                                                  const std::vector<int>& cellGroups) :
    BaseClassType(paramfld,qfld,fcnCell,quadratureOrder,tol),
    panelEqnSet_(panelEqnSet),
    cellGroups_(cellGroups),
    gamfld_(panelEqnSet_.getgamfld()),
    lamfld_(panelEqnSet_.getlamfld())
  {
    for (int group = 0; group < qfld_.nCellGroups(); ++group)
      SANS_ASSERT_MSG(order_qauxv == (qfld.template getCellGroup<Line>(group) ).order(),
                      "qfld_ (Qauxv) should have order order_qauxv");
  }

  void residual(SystemVectorView& rsd) override;

  // compute jacobian of the auxiliary viscous equation w.r.t. its parameters (inviscid variables Qinv)
  template<class SparseMatrixType>
  void jacobianParam(SparseMatrixType& jac) const;

protected:
  // Evaluate contributions of a cell group of qfld to residual
  template<class SystemVectorType>
  void residualCell(const int cellGroupGlobal,
                    const typename ParamFieldType::template FieldCellGroupType<Line>& paramfldCell,
                    const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Line>& qfldCell,
                    const int quadratureorder,
                    SystemVectorType& rsd) const;

  // Fill contributions of a cell group of qfld to the coupling jacobian
  template <class SparseMatrixType>
  void jacobianAirfoilProjQauxv_Qinv( const int cellGroupGlobal,
                                      const typename ParamFieldType::template FieldCellGroupType<Line>& paramfldCell,
                                      const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Line>& qfldCell,
                                      SparseMatrixType& jacProjQauxv_QinvGam ) const;

  template <class SparseMatrixType>
  void jacobianWakeProjQauxv_Qinv( const int cellGroupGlobal,
                                   const typename ParamFieldType::template FieldCellGroupType<Line>& paramfldCell,
                                   const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Line>& qfldCell,
                                   SparseMatrixType& jacProjQauxv_QinvLam,
                                   SparseMatrixType& jacProjQauxv_QinvGam) const;

  using BaseClassType::fcnCell_;
  using BaseClassType::paramfld_;
  using BaseClassType::qfld_; // auxiliary viscous variable Qauxv field
  using BaseClassType::quadratureOrder_;

  const PanelEqnClass& panelEqnSet_;
  const std::vector<int> cellGroups_;

  const GamFieldType<Real>& gamfld_; // vortex strength (gamma) field
  const LamFieldType<Real>& lamfld_; // source strength (sigma) field
};

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits, class PanelEqnClass>
void
AlgebraicEquationSet_ProjectToQauxv_PanelMethod<ParamFieldType, IntegrandCellClass, TopoDim, Traits, PanelEqnClass>::
residual(SystemVectorView& rsd)
{
  // check residual sizing
  SANS_ASSERT( rsd.m() == 1 );
  SANS_ASSERT( rsd(iProj).m() == qfld_.nDOF() );

  // check cell group validity
  SANS_ASSERT( &paramfld_.getXField() == &qfld_.getXField() );

  const int nCellGroups = (int)cellGroups_.size();

  // loop over cell groups to add their contributions to residual
  for (int group = 0; group < nCellGroups; ++group)
  {
    const int cellGroupGlobal = cellGroups_.at(group);

    SANS_ASSERT_MSG( cellGroupGlobal < paramfld_.nCellGroups(),
                     "cellGroupGlobal = %d, paramfld_.nCellGroups() = %d", cellGroupGlobal, paramfld_.nCellGroups() );

    if ( paramfld_.getXField().getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Topology) )
    {
      if ( (cellGroupGlobal==PanelMethodType::domainName::airfoil) ||
           (cellGroupGlobal==PanelMethodType::domainName::wake) )
      {
        residualCell(cellGroupGlobal,
                     paramfld_.template getCellGroup<Topology>(cellGroupGlobal),
                     qfld_.template getCellGroupGlobal<Topology>(cellGroupGlobal),
                     quadratureOrder_.cellOrders[cellGroupGlobal],
                     rsd(iProj) );
      }
      else
        SANS_DEVELOPER_EXCEPTION("Invalid cell group %d", cellGroupGlobal);
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION("Invalid topology type");
    }
  }
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits, class PanelEqnClass>
template<class SystemVectorType>
void
AlgebraicEquationSet_ProjectToQauxv_PanelMethod<ParamFieldType, IntegrandCellClass, TopoDim, Traits, PanelEqnClass>::
residualCell(const int cellGroupGlobal,
             const typename ParamFieldType::template FieldCellGroupType<Line>& paramfldCell,
             const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Line>& qfldCell,
             const int quadratureorder,
             SystemVectorType& rsd) const
{
  typedef typename ParamFieldType::template FieldCellGroupType<Topology>                  ParamFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;
  typedef typename QFieldCellGroupType    ::template ElementType<> ElementQFieldClass;

  // just to make sure things are consistent
  SANS_ASSERT( paramfldCell.nElem() == qfldCell.nElem() );

  // element field variables
  ElementParamFieldClass paramfldElem( paramfldCell.basis() );
  ElementQFieldClass     qfldElem( qfldCell.basis() );

  // number of integrals evaluated
  const int nIntegrand = qfldElem.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapDOFGlobal( nIntegrand, -1 );
  std::vector<int> mapDOFLocal( nIntegrand, -1 );
  std::vector<int> maprsd( nIntegrand, -1 );
  int nDOFLocal = 0;
  const int nDOFpossessed = rsd.m();

  // residual array
  std::vector<ArrayQ> rsdPDEElem( nIntegrand );

  // element integral
  ResidualElemIntegralType integral(quadratureorder, nIntegrand);

  // loop over elements within group
  const int nelem = paramfldCell.nElem();
  for (int elem = 0; elem < nelem; ++elem)
  {
    // get the mapping for the DOFs
    qfldCell.associativity( elem ).getGlobalMapping( mapDOFGlobal.data(), nIntegrand );

    // copy over the map for each DOF that is possessed by this processor
    nDOFLocal = 0;
    for (int n = 0; n < nIntegrand; n++)
    {
      if (mapDOFGlobal[n] < nDOFpossessed)
      {
        maprsd[nDOFLocal] = n;
        mapDOFLocal[nDOFLocal] = mapDOFGlobal[n];
        nDOFLocal++;
      }
    }

    // no need if all DOFs are possessed by other processors
    if (nDOFLocal == 0) continue;

    // copy global grid/solution DOFs to element
    paramfldCell.getElement( paramfldElem, elem );
    qfldCell.getElement( qfldElem, elem );

    for (int n = 0; n < nIntegrand; n++)
      rsdPDEElem[n] = 0;

    FunctionNDConvertType fcnQauxvEval(panelEqnSet_.getpanel(), get<-1>(paramfldElem),
                                       gamfld_, lamfld_, cellGroupGlobal, elem);

    // cell integration for canonical element
    integral( fcnCell_.integrand( paramfldElem, qfldElem, fcnQauxvEval ),
              get<-1>(paramfldElem), rsdPDEElem.data(), nIntegrand );

    // scatter-add element integral to global residual
    for (int n = 0; n < nDOFLocal; n++)
      rsd[ mapDOFLocal[n] ] += rsdPDEElem[ maprsd[n] ];
  } // elem
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits, class PanelEqnClass>
template<class SparseMatrixType>
void
AlgebraicEquationSet_ProjectToQauxv_PanelMethod<ParamFieldType, IntegrandCellClass, TopoDim, Traits, PanelEqnClass>::
jacobianParam(SparseMatrixType& jac) const
{
  // check jacobian sizing
  SANS_ASSERT(jac.m() == 1);
  SANS_ASSERT(jac.n() == PanelEqnClass::nsubeqn);

  // ------------------------------------------------------------------------------------------ //
  // construct jacobian (d R_auxv)/(d Q_inv)
  // ------------------------------------------------------------------------------------------ //
  // Get the matrix type, this could be a reference or temporary variable depending on SparseMatrixType
  typedef typename std::result_of<SparseMatrixType(const int, const int)>::type Matrix;

  Matrix jacProjQauxv_QinvLam = jac(iProj,PanelEqnClass::ilam);
  Matrix jacProjQauxv_QinvGam = jac(iProj,PanelEqnClass::igam);

  // check jacobian block component sizing
  SANS_ASSERT( jacProjQauxv_QinvLam.m()  == qfld_.nDOF() );
  SANS_ASSERT( jacProjQauxv_QinvGam.m()  == qfld_.nDOF() );

  SANS_ASSERT( jacProjQauxv_QinvLam.n()  == panelEqnSet_.nDOFlambda() );
  SANS_ASSERT( jacProjQauxv_QinvGam.n()  == panelEqnSet_.nDOFgamma() );

  // check cell group validity
  SANS_ASSERT( &paramfld_.getXField() == &qfld_.getXField() );

  // loop over cell groups to add contributions to jacobian
  const int nCellGroups = (int)cellGroups_.size();
  for (int group = 0; group < nCellGroups; group++)
  {
    const int cellGroupGlobal = cellGroups_.at(group);

    if ( get<-1>(paramfld_).getCellGroupBase(cellGroupGlobal).topoTypeID() == typeid(Topology) )
    {
      if ( cellGroupGlobal == PanelMethodType::domainName::airfoil)
      {
        jacobianAirfoilProjQauxv_Qinv(cellGroupGlobal,
                                      paramfld_.template getCellGroup<Line>(cellGroupGlobal),
                                      qfld_.template getCellGroup<Line>(cellGroupGlobal),
                                      jacProjQauxv_QinvGam);
      }
      else if ( cellGroupGlobal == PanelMethodType::domainName::wake)
      {
        jacobianWakeProjQauxv_Qinv(cellGroupGlobal,
                                   paramfld_.template getCellGroup<Line>(cellGroupGlobal),
                                   qfld_.template getCellGroup<Line>(cellGroupGlobal),
                                   jacProjQauxv_QinvLam,
                                   jacProjQauxv_QinvGam);
      }
      else
        SANS_DEVELOPER_EXCEPTION("Invalid cell group %d", cellGroupGlobal);
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION("Invalid topology type");
    }
  }
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits, class PanelEqnClass>
template<class SparseMatrixType>
void
AlgebraicEquationSet_ProjectToQauxv_PanelMethod<ParamFieldType, IntegrandCellClass, TopoDim, Traits, PanelEqnClass>::
jacobianAirfoilProjQauxv_Qinv( const int cellGroupGlobal,
                               const typename ParamFieldType::template FieldCellGroupType<Line>& paramfldCell,
                               const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Line>& qfldCell,
                               SparseMatrixType& jacProjQauxv_QinvGam ) const
{
  typedef typename ParamFieldType::template FieldCellGroupType<Topology>                  ParamFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;
  typedef typename QFieldCellGroupType    ::template ElementType<> ElementQFieldClass;

  // just to make sure things are consistent
  SANS_ASSERT( paramfldCell.nElem() == qfldCell.nElem() );

  // element field variables
  ElementParamFieldClass paramfldElem( paramfldCell.basis() );
  ElementQFieldClass     qfldElem( qfldCell.basis() );

  // DOF counts
  const int nDOF_ElemQ = qfldElem.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapGlobalDOF_ElemEqn( nDOF_ElemQ, -1 );
  std::vector<int> mapLocalDOF_ElemEqn( nDOF_ElemQ, -1 );
  std::vector<int> maprsd( nDOF_ElemQ, -1 );
  int nEqnDOFLocal = 0;
  const int nTotalEqnDOFpossessed = jacProjQauxv_QinvGam.m();

  // element jacobian matrix
  MatrixElemClass mtxPDEElem(nDOF_ElemQ, NQinv);
  MatrixElemClass mtxPDEElemLocal(nDOF_ElemQ, NQinv);

  MatrixElemClass mtxParamElem(nDOF_ElemQ, NQinv);

  MatrixMassElemClass mtxmass(nDOF_ElemQ, nDOF_ElemQ);
  MatrixMassElemClass mtxmassLocal(nDOF_ElemQ, nDOF_ElemQ);

  // element integral
  MatrixMassElemIntegralType integral(quadratureOrder_.cellOrders[cellGroupGlobal]);

  const int nelem = paramfldCell.nElem();

  ////////////////////// Derivatives w.r.t. to gamma DOFs //////////////////////
  // create surrealized Qinv (gamma) fields
  GamFieldType<SurrealQinvType> gamfldSurreal(paramfld_.getXField(), panelEqnSet_.order_gam,
                                              BasisFunctionCategory_Hierarchical, {PanelMethodType::domainName::airfoil});

  const int nDOFgam = panelEqnSet_.nDOFgamma();
  for (int j = 0; j < nDOFgam; j++) // Note that gamfldSurreal only has DOFs on the airfoil
    gamfldSurreal.DOF(j) = gamfld_.DOF(j);

  std::vector<int> mapGlobalDOF_QinvGam( NQinv, -1 );

  // gamfld element DOF global mapping
  const typename GamFieldType<Real>::template FieldCellGroupType<Topology>&
    gamfldCell = gamfld_.template getCellGroup<Topology>(cellGroupGlobal);

  const int nDOF_ElemQinvGam = gamfldCell.nBasis();
  std::vector<int> mapGlobalDOF_ElemQinvGam( nDOF_ElemQinvGam, -1 );

  for (int j = 0; j < nDOFgam; ++j)
  {
    gamfldSurreal.DOF(j).deriv() = 1; // shorthand for .deriv(0) since Surreal<1> has only one derivative
    mapGlobalDOF_QinvGam[0] = j;

    // loop over elements within group
    for (int elem = 0; elem < nelem; elem++)
    {
      ////////////////////// Check if the jacobian is nonzero //////////////////////
      // get the DOF global mapping for the gamma
      gamfldCell.associativity( elem ).getGlobalMapping( mapGlobalDOF_ElemQinvGam.data(),
                                                         mapGlobalDOF_ElemQinvGam.size() );

      // influence of DOF j is only local (to its neighboring elements)
      bool jIsInvolved = false;
      for (int i = 0; i < nDOF_ElemQinvGam; ++i)
        if (mapGlobalDOF_ElemQinvGam[i] == j) jIsInvolved = true;

      if (not jIsInvolved) continue; // only carry out the following jacobian calculation only if it's nonzero

      // get the DOF global mapping for the projection equation
      // note that here the getGlobalMapping provides indexing of DOFs within one processor
      qfldCell.associativity( elem ).getGlobalMapping( mapGlobalDOF_ElemEqn.data(), mapGlobalDOF_ElemEqn.size() );

      ////////////////////// Check if the residuals are possessed by this processor //////////////////////
      // copy over the map for each DOF that is possessed by this processor that is not ghost DOFs
      // Note that on each processor, the actual/primary DOFs are ordered before ghost DOFs
      nEqnDOFLocal = 0;
      for (int n = 0; n < nDOF_ElemQ; n++)
      {
        if (mapGlobalDOF_ElemEqn[n] < nTotalEqnDOFpossessed)
        {
          maprsd[nEqnDOFLocal] = n;
          mapLocalDOF_ElemEqn[nEqnDOFLocal] = mapGlobalDOF_ElemEqn[n];
          nEqnDOFLocal++;
        }
      }

      if (nEqnDOFLocal == 0) continue; // move onto next iteration if no residuals are possessed by this processor

      ////////////////////// Construct jacobian //////////////////////
      // copy global parameter/solution DOFs to element
      paramfldCell.getElement( paramfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // get derivatives d(R_auxv/projection)/d(-forcing_DOF)
      integral( fcnCell_.integrand( paramfldElem, qfldElem ), get<-1>(paramfldElem), mtxmass ); // Just to get the mass matrix

      // get derivatives d(forcing_DOF)/d(QinvGam)
      FunctionEvalQauxv<PanelMethodType,SurrealQinvType,Real>
        fcnQauxvEval(panelEqnSet_.getpanel(), get<-1>(paramfldElem),
                     gamfldSurreal, lamfld_, cellGroupGlobal, elem);

      for (std::size_t ip = 0; ip < fcnQauxvEval.sRefList_.size(); ++ip )
      {
        ArrayQauxv<SurrealQinvType> forcing = fcnQauxvEval( fcnQauxvEval.sRefList_[ip] );
        for (int iq = 0; iq < N; ++iq )
        {
          DLA::index(mtxParamElem(ip,0), iq,0) = DLA::index(forcing, iq).deriv();
        }
      }

      // complete chain rule to get d(R_auxv)/d(QinvGam) = (-1) * d(R_auxv/projection)/d(-forcing_DOF) * d(forcing_DOF) / d(QinvGam)
      // where d(R_auxv/projection)/d(-forcing_DOF) is mass matrix and d(forcing_DOF) / d(QinvLam) corresponds to df/dQinv
      mtxPDEElem = - mtxmass * mtxParamElem;

      ////////////////////// Scatter-add element jacobian to global //////////////////////
      if (nEqnDOFLocal == nDOF_ElemQ)
      {
        jacProjQauxv_QinvGam.scatterAdd( mtxPDEElem,
                                         mapGlobalDOF_ElemEqn.data(), mapGlobalDOF_ElemEqn.size(),
                                         mapGlobalDOF_QinvGam.data(), mapGlobalDOF_QinvGam.size() );
      }
      else
      {
        // compress in only the DOFs possessed by this processor
        for (int i = 0; i < nEqnDOFLocal; i++)
          for (int jp = 0; jp < NQinv; jp++)
            mtxPDEElemLocal(i,jp) = mtxPDEElem(maprsd[i],jp);

        jacProjQauxv_QinvGam.scatterAdd( mtxPDEElemLocal,
                                         mapLocalDOF_ElemEqn.data(), mapLocalDOF_ElemEqn.size(),
                                         mapGlobalDOF_QinvGam.data(), mapGlobalDOF_QinvGam.size() );
      }
    } // elem

    gamfldSurreal.DOF(j).deriv() = 0; // reset Surreal derivative
  } // j
}

template<class ParamFieldType, class IntegrandCellClass, class TopoDim, class Traits, class PanelEqnClass>
template<class SparseMatrixType>
void
AlgebraicEquationSet_ProjectToQauxv_PanelMethod<ParamFieldType, IntegrandCellClass, TopoDim, Traits, PanelEqnClass>::
jacobianWakeProjQauxv_Qinv( const int cellGroupGlobal,
                            const typename ParamFieldType::template FieldCellGroupType<Line>& paramfldCell,
                            const typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Line>& qfldCell,
                            SparseMatrixType& jacProjQauxv_QinvLam,
                            SparseMatrixType& jacProjQauxv_QinvGam ) const
{
  typedef typename ParamFieldType::template FieldCellGroupType<Topology>                  ParamFieldCellGroupType;
  typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

  typedef typename ParamFieldCellGroupType::template ElementType<> ElementParamFieldClass;
  typedef typename QFieldCellGroupType    ::template ElementType<> ElementQFieldClass;

  // just to make sure things are consistent
  SANS_ASSERT( paramfldCell.nElem() == qfldCell.nElem() );

  // element field variables
  ElementParamFieldClass paramfldElem( paramfldCell.basis() );
  ElementQFieldClass     qfldElem( qfldCell.basis() );

  // DOF counts
  const int nDOF_ElemQ = qfldElem.nDOF();

  // element-to-global DOF mapping
  std::vector<int> mapGlobalDOF_ElemEqn( nDOF_ElemQ, -1 );
  std::vector<int> mapLocalDOF_ElemEqn( nDOF_ElemQ, -1 );
  std::vector<int> maprsd( nDOF_ElemQ, -1 );
  int nEqnDOFLocal = 0;
  const int nTotalEqnDOFpossessed = jacProjQauxv_QinvGam.m(); // number of DOFs possessed by the current processor that are not ghost DOFs

  SANS_ASSERT( jacProjQauxv_QinvGam.m() == jacProjQauxv_QinvLam.m() );

  // element jacobian matrix
  MatrixElemClass mtxPDEElem(nDOF_ElemQ, NQinv);
  MatrixElemClass mtxPDEElemLocal(nDOF_ElemQ, NQinv);

  MatrixElemClass mtxParamElem(nDOF_ElemQ, NQinv);

  MatrixMassElemClass mtxmass(nDOF_ElemQ, nDOF_ElemQ);
  MatrixMassElemClass mtxmassLocal(nDOF_ElemQ, nDOF_ElemQ);

  // element integral
  MatrixMassElemIntegralType integral(quadratureOrder_.cellOrders[cellGroupGlobal]);

  const int nelem = paramfldCell.nElem();

  ////////////////////// Derivatives w.r.t. to gamma DOFs //////////////////////
  // create surrealized Qinv fields
  GamFieldType<SurrealQinvType> gamfldSurreal(paramfld_.getXField(), panelEqnSet_.order_gam,
                                              BasisFunctionCategory_Hierarchical, {PanelMethodType::domainName::airfoil});

  const int nDOFgam = panelEqnSet_.nDOFgamma();
  for (int j = 0; j < nDOFgam; j++) // TODO: Note that dummy DOFs on the wake are not touched here
    gamfldSurreal.DOF(j) = gamfld_.DOF(j);

  std::vector<int> mapGlobalDOF_QinvGam( NQinv, -1 );

  for (int j = 0; j < nDOFgam; ++j)
  {
    gamfldSurreal.DOF(j).deriv() = 1; // shorthand for .deriv(0) since Surreal<1> has only one derivative
    mapGlobalDOF_QinvGam[0] = j;

    // loop over elements within group
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the DOF global mapping for the projection equation
      // note that here the getGlobalMapping provides indexing of DOFs within one processor
      qfldCell.associativity( elem ).getGlobalMapping( mapGlobalDOF_ElemEqn.data(), mapGlobalDOF_ElemEqn.size() );

      ////////////////////// Check if the residuals are possessed by this processor //////////////////////
      // copy over the map for each DOF that is possessed by this processor that is not ghost DOFs
      // Note that on each processor, the actual/primary DOFs are ordered before ghost DOFs
      nEqnDOFLocal = 0;
      for (int n = 0; n < nDOF_ElemQ; n++)
        if (mapGlobalDOF_ElemEqn[n] < nTotalEqnDOFpossessed)
        {
          maprsd[nEqnDOFLocal] = n;
          mapLocalDOF_ElemEqn[nEqnDOFLocal] = mapGlobalDOF_ElemEqn[n];
          nEqnDOFLocal++;
        }

      if (nEqnDOFLocal == 0) continue; // move onto next iteration if no residuals are possessed by this processor

      ////////////////////// Construct jacobian //////////////////////
      // copy global parameter/solution DOFs to element
      paramfldCell.getElement( paramfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // get derivatives d(R_auxv/projection)/d(-forcing_DOF)
      integral( fcnCell_.integrand( paramfldElem, qfldElem ), get<-1>(paramfldElem), mtxmass ); // Just to get the mass matrix

      // get derivatives d(forcing_DOF)/d(QinvGam)
      FunctionEvalQauxv<PanelMethodType,SurrealQinvType,Real>
      fcnQauxvEval(panelEqnSet_.getpanel(), get<-1>(paramfldElem),
                   gamfldSurreal, lamfld_, cellGroupGlobal, elem);

      for (std::size_t ip = 0; ip < fcnQauxvEval.sRefList_.size(); ++ip )
      {
        ArrayQauxv<SurrealQinvType> forcing = fcnQauxvEval( fcnQauxvEval.sRefList_[ip] );
        for (int iq = 0; iq < N; ++iq )
        {
          DLA::index(mtxParamElem(ip,0), iq,0) = DLA::index(forcing, iq).deriv();
        }
      }

      // complete chain rule to get d(R_auxv)/d(QinvGam) = (-1) * d(R_auxv/projection)/d(-forcing_DOF) * d(forcing_DOF) / d(QinvGam)
      // where d(R_auxv/projection)/d(-forcing_DOF) is mass matrix and d(forcing_DOF) / d(QinvLam) corresponds to df/dQinv
      mtxPDEElem = - mtxmass * mtxParamElem;

      ////////////////////// Scatter-add element jacobian to global //////////////////////
      if (nEqnDOFLocal == nDOF_ElemQ)
      {
        jacProjQauxv_QinvGam.scatterAdd( mtxPDEElem,
                                         mapGlobalDOF_ElemEqn.data(), mapGlobalDOF_ElemEqn.size(),
                                         mapGlobalDOF_QinvGam.data(), mapGlobalDOF_QinvGam.size() );
      }
      else
      {
        // compress in only the DOFs possessed by this processor
        for (int i = 0; i < nEqnDOFLocal; i++)
          for (int jp = 0; jp < NQinv; jp++)
            mtxPDEElemLocal(i,jp) = mtxPDEElem(maprsd[i],jp);

        jacProjQauxv_QinvGam.scatterAdd( mtxPDEElemLocal,
                                         mapLocalDOF_ElemEqn.data(), mapLocalDOF_ElemEqn.size(),
                                         mapGlobalDOF_QinvGam.data(), mapGlobalDOF_QinvGam.size() );
      }
    } // elem

    gamfldSurreal.DOF(j).deriv() = 0; // reset Surreal derivative
  } // j


  ////////////////////// Derivatives w.r.t. to lambda DOFs //////////////////////
  // create surrealized Qinv fields
  LamFieldType<SurrealQinvType> lamfldSurreal(paramfld_.getXField(), panelEqnSet_.order_lam,
                                              BasisFunctionCategory_Legendre);

  const int nDOFlam = panelEqnSet_.nDOFlambda();
  for (int j = 0; j < nDOFlam; j++)
    lamfldSurreal.DOF(j) = lamfld_.DOF(j);

  std::vector<int> mapGlobalDOF_QinvLam( NQinv, -1 );

  for (int j = 0; j < nDOFlam; ++j)
  {
    lamfldSurreal.DOF(j).deriv() = 1; // shorthand for .deriv(0) since Surreal<1> has only one derivative
    mapGlobalDOF_QinvLam[0] = j;

    // loop over elements within group
    for (int elem = 0; elem < nelem; elem++)
    {
      // get the DOF global mapping for the projection equation
      qfldCell.associativity( elem ).getGlobalMapping( mapGlobalDOF_ElemEqn.data(), mapGlobalDOF_ElemEqn.size() );

      ////////////////////// Check if the residuals are possessed by this processor //////////////////////
      // copy over the map for each DOF that is possessed by this processor
      nEqnDOFLocal = 0;
      for (int n = 0; n < nDOF_ElemQ; n++)
        if (mapGlobalDOF_ElemEqn[n] < nTotalEqnDOFpossessed)
        {
          maprsd[nEqnDOFLocal] = n;
          mapLocalDOF_ElemEqn[nEqnDOFLocal] = mapGlobalDOF_ElemEqn[n];
          nEqnDOFLocal++;
        }

      if (nEqnDOFLocal == 0) continue; // move onto next iteration if no residuals are possessed by this processor

      ////////////////////// Construct jacobian //////////////////////
      // copy global parameter/solution DOFs to element
      paramfldCell.getElement( paramfldElem, elem );
      qfldCell.getElement( qfldElem, elem );

      // get derivatives d(R_auxv/projection)/d(-forcing_DOF)
      integral( fcnCell_.integrand( paramfldElem, qfldElem ), get<-1>(paramfldElem), mtxmass ); // Just to get the mass matrix

      // get derivatives d(forcing_DOF)/d(QinvLam)
      FunctionEvalQauxv<PanelMethodType,Real,SurrealQinvType>
        fcnQauxvEval(panelEqnSet_.getpanel(), get<-1>(paramfldElem),
                     gamfld_, lamfldSurreal, cellGroupGlobal, elem);

      for (std::size_t ip = 0; ip < fcnQauxvEval.sRefList_.size(); ++ip )
      {
        ArrayQauxv<SurrealQinvType> forcing = fcnQauxvEval( fcnQauxvEval.sRefList_[ip] );
        for (int iq = 0; iq < N; ++iq )
        {
          DLA::index(mtxParamElem(ip,0), iq,0) = DLA::index(forcing, iq).deriv();
        }
      }

      // complete chain rule to get d(R_auxv)/d(QinvLam) = (-1) * d(R_auxv/projection)/d(-forcing_DOF) * d(forcing_DOF) / d(QinvLam)
      // where d(R_auxv/projection)/d(-forcing_DOF) is mass matrix and d(forcing_DOF) / d(QinvLam) corresponds to df/dQinv
      mtxPDEElem = - mtxmass * mtxParamElem;

      ////////////////////// Scatter-add element jacobian to global //////////////////////
      if (nEqnDOFLocal == nDOF_ElemQ)
      {
        jacProjQauxv_QinvLam.scatterAdd( mtxPDEElem,
                                         mapGlobalDOF_ElemEqn.data(), mapGlobalDOF_ElemEqn.size(),
                                         mapGlobalDOF_QinvLam.data(), mapGlobalDOF_QinvLam.size() );
      }
      else
      {
        // compress in only the DOFs possessed by this processor
        for (int i = 0; i < nEqnDOFLocal; i++)
          for (int jp = 0; jp < NQinv; jp++)
            mtxPDEElemLocal(i,jp) = mtxPDEElem(maprsd[i],jp);

        jacProjQauxv_QinvLam.scatterAdd( mtxPDEElemLocal,
                                         mapLocalDOF_ElemEqn.data(), mapLocalDOF_ElemEqn.size(),
                                         mapGlobalDOF_QinvLam.data(), mapGlobalDOF_QinvLam.size() );
      }
    } // elem

    lamfldSurreal.DOF(j).deriv() = 0; // reset Surreal derivative
  } // j
}

} // namespace SANS

#endif /* SRC_PDE_IBL_ALGEBRAICEQUATIONSET_PROJECTTOQAUXV_PANELMETHOD_H_ */
