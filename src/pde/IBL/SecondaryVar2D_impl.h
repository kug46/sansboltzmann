// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(SECONDARYVAR2D_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "SecondaryVar2D.h"

namespace SANS
{
template<class CompressibilityType>
template<class T, class Tout>
typename SecondaryVar2D<CompressibilityType>::template Functor<T,Tout>
SecondaryVar2D<CompressibilityType>::
secondaryVarCalc(const ProfileCategory& profileCat,
                 const T& delta, const T& A, const T& ctau, const Tout& Redelta) const
{
  return Functor<T,Tout>(nquad_, w_, eta_,
                         profileCat, delta, A, ctau, Redelta,
                         velocityProfile_, densityProfile_, viscosityProfile_,
                         nameCDclosure_, A_GbetaLocus_, B_GbetaLocus_);
}

template<class CompressibilityType>
template<class T, class Tout>
SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::
Functor(const int& nquad,
        const std::vector<Real>& w,
        const std::vector<Real>& eta,
        const ProfileCategory& profileCat,
        const T& delta, const T& A, const T& ctau, const Tout& Redelta,
        const VelocityProfileType& velocityProfile,
        const DensityProfile2DType& densityProfile,
        const ViscosityProfile2DType& viscosityProfile,
        const std::string& nameCDclosure,
        const Real& A_GbetaLocus,
        const Real& B_GbetaLocus) :
  nquad_(nquad),
  w_(w),
  eta_(eta),
  profileCat_(profileCat),
  delta_(delta),
  A_(A),
  ctau_(ctau),
  Redelta_(Redelta),
  velocityProfile_(velocityProfile),
  densityProfile_(densityProfile),
  viscosityProfile_(viscosityProfile),
  nameCDclosure_(nameCDclosure),
  A_GbetaLocus_(A_GbetaLocus),
  B_GbetaLocus_(B_GbetaLocus)
{
  // compute/cache velocity profile information
  switch (profileCat_)
  {
    case ProfileCategory::LaminarBL:
    case ProfileCategory::LaminarWake:
    {
      velocityInfo_ = std::map<VelocityInfoType, std::vector<Tout>>({{VelocityInfoType::U,std::vector<Tout>(nquad_)},
        {VelocityInfoType::dUdeta,std::vector<Tout>(nquad_)}});
      break;
    }
    case ProfileCategory::TurbulentBL:
    case ProfileCategory::TurbulentWake:
    {
      velocityInfo_ = std::map<VelocityInfoType, std::vector<Tout>>({{VelocityInfoType::U,std::vector<Tout>(nquad_)}});
      break;
    }
    default:
      SANS_DEVELOPER_EXCEPTION("Unknown Profile Category = %d", profileCat_);
  }
  velocityProfile_.velocityProfile(profileCat_, delta_, A_, Redelta_, eta_, velocityInfo_);
}

// subordinate methods for computing each secondary variable
template<class CompressibilityType>
template<class T, class Tout>
inline
Tout SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::getdelta1s() const
{
  Tout delta1s = 0.0;
  Tout U = 0.0, R = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    delta1s += w_[i] * (1 - R*U); // delta_1^* / delta
  }
  delta1s *= delta_;
  return delta1s;
}

template<class CompressibilityType>
template<class T, class Tout>
inline
Tout SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::gettheta11() const
{
  Tout theta11 = 0.0;
  Tout U = 0.0, R = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    theta11 += w_[i] * R*U*(1-U); // theta_11 / delta
  }
  theta11 *= delta_;
  return theta11;
}

template<class CompressibilityType>
template<class T, class Tout>
inline
Tout SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::gettheta1s() const
{
  Tout theta1s = 0.0;
  Tout U = 0.0, R = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    theta1s += w_[i] * R*U*(1-pow(U,2.0)); // theta_1^* / delta
  }
  theta1s *= delta_;
  return theta1s;
}

template<class CompressibilityType>
template<class T, class Tout>
inline
Tout SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::getdelta1p() const
{
  Tout delta1p = 0.0;
  Tout U = 0.0;
  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);

    delta1p += w_[i] * (1-U); // delta_1^' / delta
  }
  delta1p *= delta_;
  return delta1p;
}

template<class CompressibilityType>
template<class T, class Tout>
inline
Tout SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::getdeltarho() const
{
  Tout deltarho = 0.0;
  Tout U = 0.0, R = 0.0;

  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    deltarho += w_[i] * (1 - R); // delta_rho / delta
  }
  deltarho *= delta_;
  return deltarho;
}

template<class CompressibilityType>
template<class T, class Tout>
inline
Tout SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::getdeltaq() const
{
  Tout delta1q = 0.0;
  Tout U = 0.0, R = 0.0;

  for (int i = 0; i < nquad_; i++)
  {
    U = velocityInfo_[VelocityInfoType::U].at(i);
    R = densityProfile_.densityProfile(U);

    delta1q += w_[i] * R*(1 - pow(U,2.0)); // delta_q / delta
  }
  delta1q *= delta_;
  return delta1q;
}

// compute Re_d * C_f1
template<class CompressibilityType>
template<class T, class Tout>
inline
Tout SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::getRedCf1() const
{
  Tout RedCf1 = 0.0;

  switch (profileCat_)
  {
    case ProfileCategory::LaminarWake:
    case ProfileCategory::TurbulentWake:
    {
      RedCf1 = 0.0;
      break;
    }
    case ProfileCategory::LaminarBL:
    {
      std::vector<Real> eta0(1,0.0); // coordinate at wall
      std::map<VelocityInfoType,std::vector<Tout>> velocityInfoWall = {{VelocityInfoType::U,std::vector<Tout>(1)},
                                                                       {VelocityInfoType::dUdeta,std::vector<Tout>(1)}};
      velocityProfile_.velocityProfile(profileCat_, delta_, A_, Redelta_, eta0, velocityInfoWall);

      Tout U_w = velocityInfoWall[VelocityInfoType::U].at(0);
      Tout V_w = viscosityProfile_.viscosityProfile(U_w);

      Tout dUdeta_w = velocityInfoWall[VelocityInfoType::dUdeta].at(0);

      RedCf1 = 2.0*V_w*dUdeta_w;
      break;
    }
    case ProfileCategory::TurbulentBL:
    {
      std::vector<Real> eta0(1,0.0); // coordinate at wall
      std::map<VelocityInfoType,std::vector<Tout>> velocityInfoWall = {{VelocityInfoType::U,std::vector<Tout>(1)}};
      velocityProfile_.velocityProfile(profileCat_, delta_, A_, Redelta_, eta0, velocityInfoWall);

      Tout U_w = velocityInfoWall[VelocityInfoType::U].at(0);
      Tout V_w = viscosityProfile_.viscosityProfile(U_w);

      RedCf1 = 2.0*V_w*A_;
      break;
    }
  }

  return RedCf1;
}

// compute Re_d * C_D
template<class CompressibilityType>
template<class T, class Tout>
inline
Tout SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::getRedCD() const
{
  Tout RedCD = 0.0;

  switch (profileCat_)
  {
    case ProfileCategory::LaminarBL:
    case ProfileCategory::LaminarWake:
    {
      Tout dUdeta = 0.0, U =0.0, V = 0.0;

      for (int i = 0; i < nquad_; ++i)
      {
        dUdeta = velocityInfo_[VelocityInfoType::dUdeta].at(i);
        U = velocityInfo_[VelocityInfoType::U].at(i);
        V = viscosityProfile_.viscosityProfile(U);
        RedCD += w_[i] * (V*pow(dUdeta,2.0));
      }

      if (profileCat_==ProfileCategory::LaminarWake)
        RedCD *= 4.0; // correction factor accounts for two halves of the wake

      break;
    }
    case ProfileCategory::TurbulentBL:
    {
      // TODO: here, the quadrature is carried out again, which is a bit redundant, but seems the optimal as of now
      Tout delta1s = getdelta1s();
      Tout theta11 = gettheta11();
      Tout theta1s = gettheta1s();

      Tout RedCf1 = getRedCf1();

      // compute shape parameters
      Tout H  = delta1s/theta11;
      Tout Hs = theta1s/theta11;

      if (nameCDclosure_ == SecondaryVar2DParams::params.nameCDclosure.ibl3eqm)
      {
        Tout GH = (H-1.0)/H;

        Tout GA = GH/A_GbetaLocus_;
        Tout GB = GH/B_GbetaLocus_;

        RedCD = 0.5*Hs * ( 0.5*RedCf1*(1-GB) + Redelta_ * GB*pow(GA,2.0) ); // equilibrium flow value
      }
      else if (nameCDclosure_ == SecondaryVar2DParams::params.nameCDclosure.ibl3lag)
      {
        Tout Us = 0.5*(-1.0 + 3.0/H);
        RedCD = 0.5*(0.5*RedCf1 + Redelta_*ctau_)*Us + Redelta_*ctau_*(1.0 - Us);
      }
      else if (nameCDclosure_ == SecondaryVar2DParams::params.nameCDclosure.xfoillag)
      {
        Tout GH = (H-1.0)/H;

        Tout Us = 0.5*Hs*(1-GH/B_GbetaLocus_);
        RedCD = 0.5*RedCf1*Us + Redelta_*ctau_*(0.995-Us);

        // Add laminar stress contribution to outer layer CD.  XFOIL v6.99, xblsys.f ~Line 1027
        RedCD += 0.15*pow(0.995-Us, 2.0) * delta_/theta11;
      }
      else
        SANS_DEVELOPER_EXCEPTION("Invalid nameCDclosure_ = %s!", nameCDclosure_.c_str());

      break;
    }
    case ProfileCategory::TurbulentWake:
    {
      // TODO: here, the quadrature is carried out again, which is a bit redundant, but seems the optimal as of now
      Tout delta1s = getdelta1s();
      Tout theta11 = gettheta11();
      Tout theta1s = gettheta1s();

      // compute shape parameters
      Tout H  = delta1s/theta11;
      Tout Hs = theta1s/theta11;

      if (nameCDclosure_ == SecondaryVar2DParams::params.nameCDclosure.ibl3eqm)
      {
        Tout GH = (H-1.0)/H;
        Tout GA = GH/A_GbetaLocus_;
        Tout GB = GH/B_GbetaLocus_;
        RedCD = 0.5*Hs * Redelta_ * GB*pow(GA,2.0);
      }
      else if (nameCDclosure_ == SecondaryVar2DParams::params.nameCDclosure.ibl3lag)
      {
        Tout Us = 0.5*(-1.0 + 3.0/H);
        RedCD = Redelta_*ctau_*(1.0 - Us);
      }
      else if (nameCDclosure_ == SecondaryVar2DParams::params.nameCDclosure.xfoillag)
      {
        Tout GH = (H-1.0)/H;
        Tout Us = 0.5*Hs*(1-GH/B_GbetaLocus_);
        RedCD = Redelta_*ctau_*(0.995-Us);

        // Add laminar stress contribution to outer layer CD
        RedCD += 0.15*pow(0.995-Us, 2.0) * delta_/theta11;
      }
      else
        SANS_DEVELOPER_EXCEPTION("Invalid nameCDclosure_ = %s!", nameCDclosure_.c_str());

      // Note that Cf1 or the inner layer term is omitted here since it vanishes in the wake

      RedCD *= 2.0; // the factor 2.0 accounts for double layers in the wake.
      break;
    }
  }

  return RedCD;
}

// compute (c_tau)_equilibrium
template<class CompressibilityType>
template<class T, class Tout>
inline
Tout SecondaryVar2D<CompressibilityType>::Functor<T,Tout>::getctaueq() const
{
  // TODO: here, the quadrature is carried out again, which is a bit redundant, but seems the optimal as of now
  const Tout delta1s = getdelta1s();
  const Tout theta11 = gettheta11();
  const Tout theta1s = gettheta1s();

  // compute shape parameters
  const Tout H  = delta1s/theta11;
  const Tout Hs = theta1s/theta11;
  const Tout GH = (H-1.0)/H;

  Tout ctaueq = 0.0;

  if (nameCDclosure_ == SecondaryVar2DParams::params.nameCDclosure.ibl3eqm ||
      nameCDclosure_ == SecondaryVar2DParams::params.nameCDclosure.ibl3lag )
  {
    const Tout Us = 0.5*(-1.0 + 3.0/H);

    ctaueq = Hs / (2.0-Us) * pow(GH,3.0) / (B_GbetaLocus_*pow(A_GbetaLocus_,2.0));
  }
  else if (nameCDclosure_ == SecondaryVar2DParams::params.nameCDclosure.xfoillag)
  {
    const Tout Us = 0.5*Hs*(1-GH/B_GbetaLocus_);

    // TODO: the following quantity can be pre-computed
    const Real CTCON = 0.5 / (pow(A_GbetaLocus_,2.0) * B_GbetaLocus_); // ~0.015 for A = 6.7 and B = 0.75
    // Ref: equation (24) in (MSES,1987); and also in XFOIL v6.99, xbl.f ~Line 1592

    ctaueq = Hs * CTCON/(1.0-Us) * pow(GH,3.0);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Invalid nameCDclosure_ = %s!", nameCDclosure_.c_str());

#if 0 // Based on (Drela, 1989, blunt trailing edge IBL) not used by XFOIL though
  if (profileCat_ == VelocityProfileType::TurbulentWake)
    ctaueq *= 4.0;
#endif

  return ctaueq;
}

} // namespace SANS
