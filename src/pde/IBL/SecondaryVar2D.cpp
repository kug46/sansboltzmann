// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define SECONDARYVAR2D_INSTANTIATE
#include "SecondaryVar2D_impl.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

#include "Surreal/SurrealS.h"

namespace SANS
{

PYDICT_PARAMETER_OPTION_INSTANTIATE( SecondaryVar2DParams::DissipationClosureOptions )

void SecondaryVar2DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.quadratureOrder));
  allParams.push_back(d.checkInputs(params.nameCDclosure));
  d.checkUnknownInputs(allParams);
}

// explicit intantiations
SecondaryVar2DParams SecondaryVar2DParams::params;

template class SecondaryVar2D<IncompressibleBL>;
template class SecondaryVar2D<IncompressibleBL>::Functor<Real,Real>;

template class SecondaryVar2D<IncompressibleBL>::Functor<Real,SurrealS<3>>;
template class SecondaryVar2D<IncompressibleBL>::Functor<Real,SurrealS<6>>;
template class SecondaryVar2D<IncompressibleBL>::Functor<Real,SurrealS<8>>;

template class SecondaryVar2D<IncompressibleBL>::Functor<SurrealS<3>,SurrealS<3>>;
template class SecondaryVar2D<IncompressibleBL>::Functor<SurrealS<6>,SurrealS<6>>;

template
SecondaryVar2D<IncompressibleBL>::Functor<Real,Real>
SecondaryVar2D<IncompressibleBL>::secondaryVarCalc<Real,Real>(const ProfileCategory& profileCat,
                 const Real& delta, const Real& A, const Real& ctau, const Real& Redelta) const;

template
SecondaryVar2D<IncompressibleBL>::Functor<Real,SurrealS<3>>
SecondaryVar2D<IncompressibleBL>::secondaryVarCalc<Real,SurrealS<3>>(const ProfileCategory& profileCat,
                 const Real& delta, const Real& A, const Real& ctau, const SurrealS<3>& Redelta) const;

template
SecondaryVar2D<IncompressibleBL>::Functor<Real,SurrealS<6>>
SecondaryVar2D<IncompressibleBL>::secondaryVarCalc<Real,SurrealS<6>>(const ProfileCategory& profileCat,
                 const Real& delta, const Real& A, const Real& ctau, const SurrealS<6>& Redelta) const;

template
SecondaryVar2D<IncompressibleBL>::Functor<SurrealS<3>,SurrealS<3>>
SecondaryVar2D<IncompressibleBL>::secondaryVarCalc<SurrealS<3>,SurrealS<3>>(const ProfileCategory& profileCat,
                 const SurrealS<3>& delta, const SurrealS<3>& A, const SurrealS<3>& ctau, const SurrealS<3>& Redelta) const;

template
SecondaryVar2D<IncompressibleBL>::Functor<SurrealS<6>,SurrealS<6>>
SecondaryVar2D<IncompressibleBL>::secondaryVarCalc<SurrealS<6>,SurrealS<6>>(const ProfileCategory& profileCat,
                 const SurrealS<6>& delta, const SurrealS<6>& A, const SurrealS<6>& ctau, const SurrealS<6>& Redelta) const;

template
SecondaryVar2D<IncompressibleBL>::Functor<Real,SurrealS<8>>
SecondaryVar2D<IncompressibleBL>::secondaryVarCalc<Real,SurrealS<8>>(const ProfileCategory& profileCat,
                 const Real& delta, const Real& A, const Real& ctau, const SurrealS<8>& Redelta) const;

}
