// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_BCIBL2D_H_
#define SRC_PDE_IBL_BCIBL2D_H_

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <string>

#include "pde/BCCategory.h"
#include "pde/BCNone.h"

#include <boost/mpl/vector.hpp>

#include "PDEIBL2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2-D IBL BC class
//
// template parameters:
//   BCType           BC type (e.g. BCTypeInflowSupercritical)
//   VarType          solution variable type (e.g. primitive variable set)
//----------------------------------------------------------------------------//
template <class BCType, class VarType>
class BCIBL2D;

// BCParam: BC parameters
template <class BCType>
struct BCIBL2DParams;

// BCType: BC types
class BCTypeWakeMatch;
class BCTypeFullState;

//----------------------------------------------------------------------------//

template<class VarType>
using BCIBL2DVector
    = boost::mpl::vector3< BCIBL2D< BCTypeWakeMatch, VarType >,
                           BCNone<PhysD2,PDEIBL<PhysD2,VarType>::N>,
                           BCIBL2D< BCTypeFullState, VarType >
                         >;

template<class VarType>
using BCIBL2DVector_AirfoilAndWake
    = boost::mpl::vector2< BCIBL2D< BCTypeWakeMatch, VarType >,
                           BCNone<PhysD2,PDEIBL<PhysD2,VarType>::N> >;

template<class VarType>
using BCIBL2DVector_NoWake
    = boost::mpl::vector1< BCNone<PhysD2,PDEIBL<PhysD2,VarType>::N> >;

//----------------------------------------------------------------------------//
// Full State Inflow
//----------------------------------------------------------------------------//

template<>
struct BCIBL2DParams<BCTypeFullState> : noncopyable
{
  const ParameterBool isNumericalFlux{"isNumericalFlux", NO_DEFAULT, "Use numerical flux based on both full BC state and interior state"};
  const ParameterNumeric<Real> deltaLami{"deltaLami", NO_DEFAULT, 0, NO_LIMIT, "Laminar BL thickness"};
  const ParameterNumeric<Real> ALami{"ALami", NO_DEFAULT, NO_RANGE, "Laminar profile shape parameter"};
  const ParameterNumeric<Real> deltaTurb{"deltaTurb", NO_DEFAULT, 0, NO_LIMIT, "Turbulent BL thickness"};
  const ParameterNumeric<Real> ATurb{"ATurb", NO_DEFAULT, NO_RANGE, "Turbulent profile shape parameter"};
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "e^N envelop amplification factor"};
  const ParameterNumeric<Real> ctau{"ctau", NO_DEFAULT, NO_RANGE, "c_tau, characterizes turbulent shear stress"};

  static constexpr const char* BCName{"FullState"};
  struct Option
  {
    const DictOption FullState{BCIBL2DParams::BCName, BCIBL2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCIBL2DParams params;
};

template <class VarType>
class BCIBL2D< BCTypeFullState, VarType > :
    public BCType< BCIBL2D<BCTypeFullState, VarType> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCIBL2DParams<BCTypeFullState> ParamsType;

  typedef PhysD2 PhysDim;
  typedef PDEIBL<PhysDim,VarType> PDEClass;

  static const int D = PDEClass::D;   // physical dimensions
  static const int N = PDEClass::N;   // total solution variables

  static const int NBC = PDEClass::N; // total BCs

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDEClass::template MatrixQ<T>;  // matrices

  template <class Tp>
  using ArrayParam = typename PDEClass::template ArrayParam<Tp>; // parameter arrays

  BCIBL2D(const PDEIBL<PhysD2,VarType>& pde,
          PyDict& d ) :
    pde_(pde),
    upwind_(d.get(ParamsType::params.isNumericalFlux))
  {
    SANS_DEVELOPER_EXCEPTION("Don't use this until it's fixed!");
#if 0 //TODO: needs to be generalized to allow for different state definition
    qB_ = pde_.setDOFFrom( VarData2DDANCt(d.get(ParamsType::params.deltaLami),
                                          d.get(ParamsType::params.ALami),
                                          d.get(ParamsType::params.deltaTurb),
                                          d.get(ParamsType::params.ATurb),
                                          d.get(ParamsType::params.nt),
                                          d.get(ParamsType::params.ctau)) );

    SANS_ASSERT_MSG(pde_.isValidState(qB_), "BC state is invalid");
#endif
  }

  ~BCIBL2D() {}

  BCIBL2D( const BCIBL2D& ) = delete;
  BCIBL2D& operator=( const BCIBL2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // true, if using fluxAdvectiveUpwind (see Discretization/Galerkin/IntegrandBoundaryTrace_Flux_mitState_Galerkin_manifold.h)
  // false, if using fluxAdvective based on BC full state
  bool useUpwind() const { return upwind_; }

  // BC data
  template <class Tp, class T>
  void state( const ArrayParam<Tp>& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const;

protected:
  const PDEIBL<PhysDim,VarType>& pde_;
  const bool upwind_;
  ArrayQ<Real> qB_;
};


template <class VarType>
template <class Tp, class T>
inline void
BCIBL2D<BCTypeFullState, VarType>::
state( const ArrayParam<Tp>& param,
       const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qB_;
}


// is the boundary state valid
template <class VarType>
bool
BCIBL2D<BCTypeFullState, VarType>::
isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const
{
  return pde_.isValidState(qI);
}

//----------------------------------------------------------------------------//
// Matching condition: Trailing edge to wake inlet
//----------------------------------------------------------------------------//
template<>
struct BCIBL2DParams<BCTypeWakeMatch> : noncopyable
{
  static void checkInputs(PyDict d);

  struct MatchingOptions
  {
    typedef std::string ExtractType;
    const ExtractType trailingEdge = "trailingEdge";
    const ExtractType wakeInflow = "wakeInflow";

    const std::vector<ExtractType> options{trailingEdge,wakeInflow};
  };
  const ParameterOption<MatchingOptions> matchingType{"matchingType", NO_DEFAULT, "Used to specify trailing edge or wake inflow BC"};

  static constexpr const char* BCName{"Matching"};
  struct Option
  {
    const DictOption Matching{BCIBL2DParams::BCName, BCIBL2DParams::checkInputs};
  };

  static BCIBL2DParams params;
};

template <class VarType>
class BCIBL2D< BCTypeWakeMatch, VarType > :
    public BCType< BCIBL2D<BCTypeWakeMatch, VarType> >
{
public:

  typedef typename BCCategory::HubTrace Category;
  typedef BCIBL2DParams<BCTypeWakeMatch> ParamsType;

  typedef PhysD2 PhysDim;
  typedef PDEIBL<PhysDim,VarType> PDEClass;

  static const int D = PDEClass::D;   // physical dimensions
  static const int N = PDEClass::N;   // total solution variables

  static const int NBC = 0; // total BCs

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDEClass::template MatrixQ<T>; // matrices

  template <class T>
  using ArrayParam = typename PDEClass::template ArrayParam<T>;     // solution/residual array type

  template <class TX>
  using VectorX = typename PDEClass::template VectorX<TX>; // size-D vector (e.g. basis unit vector) type

  template <class TX>
  using TensorX = typename PDEClass::template TensorX<TX>; // size-D vector (e.g. basis unit vector) type

  typedef typename PDEClass::ProfileCategory ProfileCategory;

  // cppcheck-suppress noExplicitConstructor
  BCIBL2D( const PDEClass& pde, const std::string& matching ) :
    pde_(pde),
    matching_(matching),
    varInterpret_(pde_.getVarInterpreter()),
    paramInterpret_(pde_.getParamInterpreter())
  {
    SANS_ASSERT_MSG( (matching_ == ParamsType::params.matchingType.trailingEdge) ||
                     (matching_ == ParamsType::params.matchingType.wakeInflow),
                     "matching_ = %d is an unknown matching condition identifier",
                     matching_.c_str());
  }

  BCIBL2D( const PDEClass& pde, PyDict& d ) :
    BCIBL2D(pde, d.get(ParamsType::params.matchingType)) {}


  virtual ~BCIBL2D() {}
  BCIBL2D& operator=( const BCIBL2D& ) = delete;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const { return pde_.isValidState(qI); }

  template <class T, class Tp, class Tout>
  void fluxMatchingSourceHubtrace(
      const ArrayParam<Tp>& params, const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time,
      const Real& nx, const Real& nz,
      const ArrayQ<T>& var, const ArrayQ<T>& varhb,
      ArrayQ<Tout>& f, ArrayQ<Tout>& hubsource ) const;

  const Real hb_dummy_ = 0.0;

protected:
  const PDEClass& pde_;
  const std::string matching_; // cannot be a reference; otherwise, a derived class may cast it into a different type
  const typename PDEClass::VarInterpType varInterpret_;
  const typename PDEClass::ParamInterpType paramInterpret_;
};

//----------------------------------------------------------------------------//
template <class VarType>
template <class T, class Tp, class Tout>
inline void
BCIBL2D<BCTypeWakeMatch,VarType>::fluxMatchingSourceHubtrace(
    const ArrayParam<Tp>& params, const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time,
    const Real& nx, const Real& nz,
    const ArrayQ<T>& var, const ArrayQ<T>& varhb,
    ArrayQ<Tout>& f, ArrayQ<Tout>& sourcehub ) const
{
  // laminar mom./k.e. eqns are dummy placeholders
  sourcehub[PDEClass::ir_momLami] += varhb[PDEClass::ir_momLami] - hb_dummy_;
  sourcehub[PDEClass::ir_keLami] += varhb[PDEClass::ir_keLami] - hb_dummy_;

  const ProfileCategory profileCat = pde_.getProfileCategory(var,x);

  const T nt = varInterpret_.getnt(var);
  const T ctau  = varInterpret_.getctau(var);

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);

  // derived variables
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = (pde_.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
  const Tp nue = (pde_.getViscosityModel()).dynamicViscosity() / rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const auto& thicknessesCoefficientsFunctor
    = (pde_.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tout delta1s  = thicknessesCoefficientsFunctor.getdelta1s();
  const Tout theta11  = thicknessesCoefficientsFunctor.gettheta11();

  Tout gam = pde_.calcIntermittency(params, e1, x, z, var);

  if (matching_ == ParamsType::params.matchingType.trailingEdge)
  {
    // ------ hubtrace flux ------ //
    const Tout theta1s  = thicknessesCoefficientsFunctor.gettheta1s();

    // P happens to be symmetric in 2D IBL. Note "*" in () is an outer product
    TensorX<Tout> P = rhoe*theta11*(q1*Transpose(q1)); // [tensor] P: momentum defect flux
    VectorX<Tout> PTdote = Transpose(P)*e1; // P^T dot e

    VectorX<Tout> K = rhoe*pow(qe,2)*theta1s*q1; // [vector] K: kinetic energy defect flux

    const VectorX<Real> nrm = {nx, nz};
    const Tout f_mom = dot(nrm, PTdote);
    const Tout f_ke = dot(nrm, K);
    const Tp qn = dot(nrm, q1);

    // compute advective fluxes
    f[PDEClass::ir_momLami] += (1.0-gam) * f_mom;
    f[PDEClass::ir_keLami]  += (1.0-gam) * f_ke;
    f[PDEClass::ir_momTurb] += gam * f_mom;
    f[PDEClass::ir_keTurb]  += gam * f_ke;
    f[PDEClass::ir_amp]     += nt * qn;
    f[PDEClass::ir_lag]     += gam * ctau * qn;

    // ------ hubtrace source ------ //
    sourcehub[PDEClass::ir_momTurb] += delta1s + fabs(z);
    sourcehub[PDEClass::ir_keTurb] += theta11;
    sourcehub[PDEClass::ir_amp] += theta11 * nt;

    const bool isTransitionActive = pde_.getTransitionModel().isTransitionActive();
    if ( profileCat == ProfileCategory::LaminarBL )
    {
      if (isTransitionActive)
      {
#if IsCtauTinitXfoil
        Tout ctaueq = thicknessesCoefficientsFunctor.getctaueq();
        Tout Hk = thicknessesCoefficientsFunctor.getHk();
        const Tout ctauTinit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk, ctaueq);
#else
        const Real ctauTinit = 1.e-4;
#endif
        // Post-transition ctau fitting (as in XFOIL v6.99, xblsys.f, ~Line 1394)
        sourcehub[PDEClass::ir_lag] += theta11 * ctauTinit; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
      }
      else
        sourcehub[PDEClass::ir_lag] += varhb[PDEClass::ir_lag] - hb_dummy_;
    }
    else if ( profileCat == ProfileCategory::TurbulentBL )
      sourcehub[PDEClass::ir_lag] += theta11 * ctau; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
    else
      SANS_DEVELOPER_EXCEPTION("Invalid profileCat at trailing edge hubtrace!");
  }
  else if (matching_ == ParamsType::params.matchingType.wakeInflow)
  {
    // ------ hubtrace flux ------ //
    f[PDEClass::ir_momLami] += (1.0-gam)*varhb[PDEClass::ir_momTurb];
    f[PDEClass::ir_keLami]  += (1.0-gam)*varhb[PDEClass::ir_keTurb];

    f[PDEClass::ir_momTurb] += gam*varhb[PDEClass::ir_momTurb];
    f[PDEClass::ir_keTurb]  += gam*varhb[PDEClass::ir_keTurb];

    f[PDEClass::ir_amp] += varhb[PDEClass::ir_amp];
    f[PDEClass::ir_lag] += gam * varhb[PDEClass::ir_lag];

    // ------ hubtrace source ------ //
    sourcehub[PDEClass::ir_momTurb] += - delta1s;
    sourcehub[PDEClass::ir_keTurb] += - theta11;
    sourcehub[PDEClass::ir_amp] += - theta11 * nt;

    if (profileCat == ProfileCategory::LaminarWake)
    {
      SANS_DEVELOPER_EXCEPTION("Laminar wake not suppported at wake inflow hubtrace!");
//      sourcehub[PDEClass::ir_lag] += varhb[PDEClass::ir_lag] - hb_dummy_;
    }
    else if ( profileCat == ProfileCategory::TurbulentWake )
      sourcehub[PDEClass::ir_lag] += - theta11 * ctau; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
    else
      SANS_DEVELOPER_EXCEPTION("Invalid profileCat at wake inflow hubtrace!");
  }
}

} // namespace SANS

#endif /* SRC_PDE_IBL_BCIBL2D_H_ */
