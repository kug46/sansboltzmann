// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_SETSOLNDOFCELL_IBL2D_H_
#define SRC_PDE_IBL_SETSOLNDOFCELL_IBL2D_H_

#include "VarInterpret2D.h"

#include <fstream>
#include <sstream>

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

namespace SANS
{

template<class VarType>
class SetSolnDofCell_IBL2D_impl;

//----------------------------------------------------------------------------//
//  Assign nodal DOFs of an IBL2D solution field

//----------------------------------------------------------------------------//
template<class VarType>
class SetSolnDofCell_IBL2D_impl :
    public GroupFunctorCellType< SetSolnDofCell_IBL2D_impl<VarType> >
{
public:
  typedef PhysD2 PhysDim;
  typedef IBLTraits<PhysDim, typename IBLFormulationType<VarType>::type> IBLTraitsType;

  static const int D = PhysDim::D;
  static const int N = IBLTraitsType::N; // solution dimension

  typedef VarInterpret2D<VarType> VarInterpretType; // solution variable interpreter type

  typedef typename IBLTraitsType::template VectorX<Real> VectorX;
  typedef typename IBLTraitsType::template ArrayQ<Real> ArrayQ;


  SetSolnDofCell_IBL2D_impl( const std::vector<ArrayQ>& data,
                             const std::vector<int>& cellGroups,
                             const int order) :
    data_(data),
    cellGroups_(cellGroups),
    order_(order)
  {
    SANS_ASSERT( order_==0 || order_==1 ); // TODO: only these two orders are currently supported
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Assign nodal DOFs to the solution field
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, typename Topology::TopoDim>,
                                   Field<PhysDim, typename Topology::TopoDim, ArrayQ>, TupleClass<>>
           ::template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename Topology::TopoDim TopoDim;
    typedef typename XField<PhysDim, TopoDim>       ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
          QFieldCellGroupType& qfldCell = const_cast<QFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    SANS_ASSERT_MSG( qfldCell.order() == order_, "Solution order does not match" );

    const int nDOFelem = qfldElem.nDOF();

    // loop over elements within group
    const int nelem = xfldCell.nElem();

    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid/parameter DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // assign DOFs to parameter element field

      if (qfldCell.order() == 0) // piecewise constant solution
      {
        const int nodalIndxGlobal = elem; // Indexing nodal DOFs in the input data
        qfldElem.DOF(0) = 0.5*(data_[nodalIndxGlobal] + data_[nodalIndxGlobal+1]);
      }
      else if (qfldCell.order() == 1) // piecewise linear solution
      {
        SANS_ASSERT_MSG( qfldCell.basis()->category() == BasisFunctionCategory_Hierarchical,
                         "The following method assumes hierarchical basis function");

        for ( int i = 0; i < nDOFelem; i++ )
        {
          const int nodalIndxGlobal = elem + i; // Indexing nodal DOFs in the input data

          for (int n=0; n < N; ++n)
            qfldElem.DOF(i)[n] = data_[nodalIndxGlobal][n];
        }
      }
      else
        SANS_DEVELOPER_EXCEPTION("Not implemented!");

      // set the projected solution
      qfldCell.setElement( qfldElem, elem );
    }
  }

protected:
  const std::vector<ArrayQ>& data_;
  const std::vector<int> cellGroups_;
  const int order_;
  static constexpr VarInterpretType varInterpret_();
};

//----------------------------------------------------------------------------//
// Factory function
template <class VarType>
SetSolnDofCell_IBL2D_impl<VarType>
SetSolnDofCell_IBL2D( const std::vector< typename SetSolnDofCell_IBL2D_impl<VarType>::ArrayQ >& data,
                      const std::vector<int>& cellGroups,
                      const int order )
{
  return SetSolnDofCell_IBL2D_impl<VarType>(data, cellGroups, order);
}

//----------------------------------------------------------------------------//
// Read solution data from a file into an array
template<class VarDataType>
struct QIBLDataReader
{
  template<class VarInterpreterType>
  static std::vector<typename VarInterpreterType::template ArrayQ<Real>>
  getqIBLvec (const VarInterpreterType& varInterpret, const std::string& filename)
  {
    std::vector<typename VarInterpreterType::template ArrayQ<Real>> qIBL_vec;

    std::ifstream datafile(filename);
    int nline = 0;

    // line-by-line data parsing
    if (datafile.is_open())
    {
      std::string line;
      while (std::getline(datafile,line))
      {
        std::istringstream dataline(line);
        qIBL_vec.push_back(varInterpret.template setDOFFrom<Real>(VarDataType(dataline)));
        nline++;
      }
      datafile.close();

      SANS_ASSERT_MSG( nline == (int) qIBL_vec.size(), "nline = %d", "qIBL_vec.size() = %d", nline, (int)qIBL_vec.size() );
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "Error: the data file \"%s\" cannot be opened.", filename.c_str() );
    }

    return qIBL_vec;
  }
};

} //namespace SANS

#endif /* SRC_PDE_IBL_SETSOLNDOFCELL_IBL2D_H_ */
