// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_FLUIDMODELS_H_
#define SRC_PDE_IBL_FLUIDMODELS_H_

// This file contains various fluid models (gas, viscosity) that are used for IBL
//   to interpret EIF fluid states/properties that are evaluated at the edge of the
//   viscous layer.

#include "Python/Parameter.h"

#include <cmath> // sqrt
#include <iostream>
#include <limits>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "VelocityProfile2D.h"

namespace SANS
{

struct GasModelParamsIBL;
class GasModelIBL;

template<class ViscosityModelType>
class ViscosityModel;

class ViscosityConstant;
#if 0 //TODO: not yet used
class ViscositySutherlandLaw;
#endif

struct TransitionModelParams;
class TransitionModel;

//----------------------------------------------------------------------------//
// A class for representing input parameters for the gas model
struct GasModelParamsIBL : noncopyable
{
  const ParameterNumeric<Real> gamma{"gamma", 1.4    , 1.0, 5./3.   , "Specific heat ratio"};
  const ParameterNumeric<Real> R    {"R"    , 287.15 , std::numeric_limits<Real>::epsilon(), NO_LIMIT, "Gas Constant [J/(kg*K)]"};
  // note that the bounds are included as valid

  static void checkInputs(PyDict d); // parameter validity are checked only when this function is called

  static GasModelParamsIBL params;
};

//----------------------------------------------------------------------------//
// GasModelIBL:  ideal gas law with constant specific heat ratio
//----------------------------------------------------------------------------//
class GasModelIBL
{
public:
  explicit GasModelIBL(const PyDict& d) :
    gamma_(d.get(GasModelParamsIBL::params.gamma)),
    R_(d.get(GasModelParamsIBL::params.R))
  {
    GasModelParamsIBL::checkInputs(d); // necessary for checking input parameter validity
  }

  GasModelIBL(const Real& gamma, const Real& R) :
    gamma_(gamma),
    R_(R)
  {
    // check parameter validity using Python dictionary
    PyDict gasModelDict;
    gasModelDict[GasModelParamsIBL::params.gamma] = gamma_;
    gasModelDict[GasModelParamsIBL::params.R] = R_;

    GasModelParamsIBL::checkInputs(gasModelDict); // necessary for checking input parameter validity
  }

  ~GasModelIBL() {}
  GasModelIBL(const GasModelIBL&) = delete;
  GasModelIBL& operator=(const GasModelIBL&) = delete;

  // return model parameters
  Real gamma() const { return gamma_; }
  Real R() const { return R_; }

  // compute static density
  template<class T>
  T density(const T& q, const T& p0, const T& T0) const
  {
    // using isentropic relations
    T Msq = pow(q,2.0) / (gamma_*R_*T0 - 0.5*(gamma_-1.0)*pow(q,2.0)); // Msq = M^2, where M is Mach number
    T rho0 = p0/(R_*T0); // stagnation density
    T rho = rho0 * pow(1.0 + 0.5*(gamma_-1.0)*Msq, -1.0/(gamma_-1.0) ); // static density

    return rho;
  }

#if 0 //TODO: not yet used
  // compute static temperature
  template<class T>
  T temperature(const T& q, const T& T0) const
  {
    // using isentropic relations
    T Msq = pow(q,2.0) / (gamma_*R_*T0 - 0.5*(gamma_-1.0)*pow(q,2.0)); // Msq = M^2, where M is Mach number
    return T0 / (1.0 + 0.5*(gamma_-1.0)*Msq) ;
  }
#endif

  // debug dump of private data
  void dump(const int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real gamma_;                // specific heat ratio
  const Real R_;                    // gas constant [J/(kg*K)]
};

//----------------------------------------------------------------------------//
// ViscosityModel: constant viscosity model
//----------------------------------------------------------------------------//
template<>
class ViscosityModel<ViscosityConstant>
{
public:
  explicit ViscosityModel(const Real& mu) : mu_(mu)
  {
    SANS_ASSERT(mu_>0);
  }

  ~ViscosityModel() {}
  ViscosityModel(const ViscosityModel&) = delete;
  ViscosityModel& operator=( const ViscosityModel& ) = delete;

  // compute dynamic viscosity
  Real dynamicViscosity() const { return mu_; }

  // debug dump of private data
  void dump(const int indentSize=0, std::ostream& out = std::cout) const;

private:
  const Real mu_; // dynamic viscosity [kg/(s*m)]
};

#if 0 //TODO: not yet used
//----------------------------------------------------------------------------//
// ViscosityModel: viscosity model based on Sutherland's law for ideal gas
//----------------------------------------------------------------------------//

template<>
class ViscosityModel<ViscositySutherlandLaw> : private GasModelIBL
{
public:
  explicit ViscosityModel(const GasModelIBL& gasModel,
                          const Real& TSuth, const Real& Tref, const Real& muref) :
    GasModelIBL(gasModel),
    TSuth_(TSuth),
    Tref_(Tref),
    muref_(muref)
  {
    SANS_ASSERT(TSuth_>0);
    SANS_ASSERT(Tref_>0);
    SANS_ASSERT(muref_>0);
  }

  ~ViscosityModel() {}
  ViscosityModel( const ViscosityModel& ) = delete;
  ViscosityModel& operator=( const ViscosityModel& ) = delete;

  // return model parameters
  // TODO

  // compute dynamic viscosity
  template<class T>
  T dynamicViscosity(const T& q, const T& T0) const
  {
    T Temp = temperature(q,T0);
    T mu = muref_ * pow(Temp/Tref_, 1.5) * (Tref_+TSuth_) /(Temp+TSuth_);
    return mu;
  }

  // debug dump of private data
  void dump(const int indentSize=0, std::ostream& out = std::cout) const;

private:
  const Real TSuth_; // Sutherland's law constant [K]
  const Real Tref_; // reference absolute temperature [K]
  const Real muref_; // reference dynamic viscosity [kg/(s*m)]
};
#endif

//----------------------------------------------------------------------------//
// A class for representing input parameters for the transition model
struct TransitionModelParams : noncopyable
{
  const ParameterNumeric<Real> ntinit{"ntinit", 0.0, 0.0, NO_LIMIT, "Initial amplification factor"};
  const ParameterNumeric<Real> ntcrit{"ntcrit", 9.0, 0.0, NO_LIMIT, "Critical amplification factor"};
  const ParameterNumeric<Real> xtr{"xtr", 1.e+5 , 0.0, NO_LIMIT, "Forced transition x coordinate"};
  // xtr = 10.0 by default so that it's outside the domain of interest unless the user wants to use it intentionally
  // note that the bounds are included as valid
  const ParameterBool isTransitionActive{"isTransitionActive", false, "Logical flag: whether transition model is active"};
  const ParameterNumeric<Real> intermittencyLength{"intermittencyLength", 0.05, 0.0, NO_LIMIT, "Length scale of laminar-turbulent intermittency"};

  struct DefaultProfileCategoryOptions
  {
    typedef std::string ExtractType;
    const ExtractType laminarBL = "laminarBL";
    const ExtractType laminarWake = "laminarWake";
    const ExtractType turbulentBL = "turbulentBL";
    const ExtractType turbulentWake = "turbulentWake";

    const std::vector<ExtractType> options{laminarBL,laminarWake,turbulentBL,turbulentWake};
  };
  const ParameterOption<DefaultProfileCategoryOptions>
    profileCatDefault{"profileCatDefault", NO_DEFAULT, "Default viscous layer profile type"};

  static void checkInputs(PyDict d); // parameter validity are checked only when this function is called

  static TransitionModelParams params;
};

//----------------------------------------------------------------------------//
// TransitionModel: laminar-to-turbulent flow transition model
//----------------------------------------------------------------------------//
class TransitionModel
{
public:
  static const int D = PhysD2::D; // TODO: assumes 2D
  typedef DLA::VectorS<D,Real> VectorX;

  typedef IBLProfileCategory ProfileCategory;

  explicit TransitionModel(const PyDict& d) :
    ntinit_(d.get(TransitionModelParams::params.ntinit)),
    ntcrit_(d.get(TransitionModelParams::params.ntcrit)),
    xtr_(d.get(TransitionModelParams::params.xtr)),
    isTransitionActive_(d.get(TransitionModelParams::params.isTransitionActive)),
    intermittencyLength_(d.get(TransitionModelParams::params.intermittencyLength))
  {
    // necessary for checking input parameter validity
    TransitionModelParams::checkInputs(d);

    // set default profile category
    const std::string profileCatDefaultName = d.get(TransitionModelParams::params.profileCatDefault);

    if ( profileCatDefaultName == TransitionModelParams::params.profileCatDefault.laminarBL )
      profileCatDefault_ = ProfileCategory::LaminarBL;
    else if ( profileCatDefaultName == TransitionModelParams::params.profileCatDefault.laminarWake )
      profileCatDefault_ = ProfileCategory::LaminarWake;
    else if ( profileCatDefaultName == TransitionModelParams::params.profileCatDefault.turbulentBL )
      profileCatDefault_ = ProfileCategory::TurbulentBL;
    else if ( profileCatDefaultName == TransitionModelParams::params.profileCatDefault.turbulentWake )
      profileCatDefault_ = ProfileCategory::TurbulentWake;
    else
      SANS_DEVELOPER_EXCEPTION("Unknown profileCatDefaultName = '%s'", profileCatDefaultName.c_str());

    if ( profileCatDefault_ == ProfileCategory::LaminarWake ||
         profileCatDefault_ == ProfileCategory::TurbulentBL ||
         profileCatDefault_ == ProfileCategory::TurbulentWake )
      SANS_ASSERT_MSG(!isTransitionActive_, "isTransitionActive_ is allowed to be true only for laminarBL!");
  }

  TransitionModel(const ProfileCategory& profileCatDefault,
                  const Real& ntinit = TransitionModelParams::params.ntinit.Default,
                  const Real& ntcrit = TransitionModelParams::params.ntcrit.Default,
                  const Real& xtr = TransitionModelParams::params.xtr.Default,
                  const bool& isTransitionActive = TransitionModelParams::params.isTransitionActive.Default,
                  const Real& intermittencyLength = TransitionModelParams::params.intermittencyLength.Default) :
    ntinit_(ntinit),
    ntcrit_(ntcrit),
    xtr_(xtr),
    isTransitionActive_(isTransitionActive),
    intermittencyLength_(intermittencyLength),
    profileCatDefault_(profileCatDefault)
  {
    // check parameter validity using Python dictionary
    PyDict transitionModelDict;
    transitionModelDict[TransitionModelParams::params.ntinit] = ntinit_;
    transitionModelDict[TransitionModelParams::params.ntcrit] = ntcrit_;
    transitionModelDict[TransitionModelParams::params.xtr] = xtr_;
    transitionModelDict[TransitionModelParams::params.isTransitionActive] = isTransitionActive_;
    transitionModelDict[TransitionModelParams::params.profileCatDefault]
      = TransitionModelParams::params.profileCatDefault.laminarBL; // This is dummy; not actually checking profileCatDefault_

    TransitionModelParams::checkInputs(transitionModelDict); // necessary for checking input parameter validity

    if ( profileCatDefault_ == ProfileCategory::LaminarWake ||
         profileCatDefault_ == ProfileCategory::TurbulentBL ||
         profileCatDefault_ == ProfileCategory::TurbulentWake )
      SANS_ASSERT_MSG(!isTransitionActive_, "isTransitionActive_ is allowed to be true only for laminarBL!");
  }

  ~TransitionModel() {}
  TransitionModel(const TransitionModel&) = delete;
  TransitionModel& operator=(const TransitionModel&) = delete;

  // return model parameters
  Real ntinit() const { return ntinit_; }
  Real ntcrit() const { return ntcrit_; }
  Real xtr() const { return xtr_; }

  // reports whether the transition model is active
  bool isTransitionActive() const { return isTransitionActive_; }

  ProfileCategory getProfileCategoryDefault() const { return profileCatDefault_; };

  // determine profile/closure category/type
  template <class T>
  ProfileCategory getProfileCategory(const T& nt, const Real& x) const;

  // determine the intermittency (i.e. the proportion of turbulence). For example:
  // intermittency = 0.0: fully laminar
  // intermittency = 0.5: equal lamimnar/turbulent mix
  // intermittency = 1.0: fully turbulent

  template <class T>
  T binaryIntermittency(const T& nt, const Real& x) const;

  Real getIntermittencyLength() const { return intermittencyLength_; }

  // check if any transition has happened
  template <class T>
  bool isTransition(const T& nt, const VectorX& X) const { return ( isNaturalTransition(nt) || isForcedTransition(X) ); }

  // check if natural transition has happened
  template <class T>
  bool isNaturalTransition(const T& nt) const { return !(nt < ntcrit_); }

  // check if forced transition has happened
  bool isForcedTransition(const Real& x) const { return !(x < xtr_); }

  bool isForcedTransition(const VectorX& X) const { return isForcedTransition(X[0]); }

  // debug dump of private data
  void dump(const int indentSize=0, std::ostream& out = std::cout ) const;

  // set ntcrit
  void set_ntcrit(const Real& ntcrit) const
  {
    std::cout << "Use extreme caution: the parameter ntcrit_ is being changed from " << ntcrit_ << " to " << ntcrit << std::endl;
    ntcrit_ = ntcrit;
  }

  void set_xtr(const Real& xtr) const
  {
    std::cout << "Use extreme caution: the parameter xtr_ is being changed from " << xtr_ << " to " << xtr << std::endl;
    xtr_ = xtr;
  }

private:
  const Real ntinit_; // initial amplification factor
  mutable Real ntcrit_; // critical amplification factor
  mutable Real xtr_; // forced transition location as in coordinate x
  const bool isTransitionActive_; // true if transition model is active; otherwise, false
  const Real intermittencyLength_;
  ProfileCategory profileCatDefault_; // default profile type category
};

// determine profile/closure category/type
template <class T>
inline typename TransitionModel::ProfileCategory
TransitionModel::getProfileCategory(const T& nt, const Real& x) const
{
  if ( !isTransitionActive_ )
  {
    return profileCatDefault_; // these default closures are assumed to be immutable and are strictly enforced as prescribed by the user
  }

  SANS_ASSERT( profileCatDefault_ == ProfileCategory::LaminarBL );
  if ( isTransition(nt,x) )
  {
    return ProfileCategory::TurbulentBL;
  }

  return ProfileCategory::LaminarBL;
}

template <class T>
inline T
TransitionModel::binaryIntermittency(const T& nt, const Real& x) const
{
  const ProfileCategory profileCat = getProfileCategory(nt,x);

  // Binary intermittency: Heaviside function of nt
  if ( profileCat == ProfileCategory::LaminarBL )
    return 0.0;
  else if ( profileCat == ProfileCategory::TurbulentBL )
    return 1.0;
  else if ( profileCat == ProfileCategory::LaminarWake )
    return 0.0; // laminar

  SANS_ASSERT( profileCat == ProfileCategory::TurbulentWake );
  return 1.0; // turbulent
}

}

#endif /* SRC_PDE_IBL_FLUIDMODELS_H_ */
