// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_SECONDARYVAR2D_H_
#define SRC_PDE_IBL_SECONDARYVAR2D_H_

#include "Python/Parameter.h"

#include "Quadrature/QuadratureLine.h"

#include "VelocityProfile2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for representing input parameters for SecondaryVar2D
struct SecondaryVar2DParams : noncopyable
{
  const ParameterNumeric<int> quadratureOrder{"quadratureOrder", 39, 39, NO_LIMIT, "Numerical quadrature order for computing thickness integrals"};
  // note that the bounds are included as valid
  // default quadrature order = 39 is currently the highest supported in SANS

  struct DissipationClosureOptions
  {
    typedef std::string ExtractType;
    const ExtractType ibl3eqm = "ibl3eqm";
    const ExtractType ibl3lag = "ibl3lag";
    const ExtractType xfoillag = "xfoillag";

    const std::vector<ExtractType> options{ibl3eqm,ibl3lag,xfoillag};
  };
  const ParameterOption<DissipationClosureOptions>
    nameCDclosure{"nameCDclosure", "ibl3eqm", "Name (string) of the C_Diss closure model"};

  static void checkInputs(PyDict d); // parameter validity are checked only when this function is called

  static SecondaryVar2DParams params;
};

//----------------------------------------------------------------------------//
// 2D secondary variables
//----------------------------------------------------------------------------//
template<class CompressiblityType>
class SecondaryVar2D
{
public:
  typedef VelocityProfile2D VelocityProfileType;
  typedef IBLProfileCategory ProfileCategory;
  typedef typename VelocityProfileType::VelocityInfoType VelocityInfoType;

  typedef DensityProfile2D<CompressiblityType> DensityProfile2DType;
  typedef ViscosityProfile2D<CompressiblityType> ViscosityProfile2DType;

  // ctor
  explicit SecondaryVar2D(const PyDict& d) : // default quadrature order = 39 is currently the highest supported
    SecondaryVar2D( d.get(SecondaryVar2DParams::params.nameCDclosure),
                    d.get(SecondaryVar2DParams::params.quadratureOrder) ) {}

  explicit SecondaryVar2D(const std::string nameCDclosure = SecondaryVar2DParams::params.nameCDclosure.Default,
                          const int quadratureOrder = SecondaryVar2DParams::params.quadratureOrder.Default) :
    nameCDclosure_(nameCDclosure),
    quadrature_(quadratureOrder),
    nquad_(quadrature_.nQuadrature()),
    w_(nquad_),
    eta_(nquad_),
    velocityProfile_(),
    densityProfile_(),
    viscosityProfile_()
  {
    PyDict d;
    d[SecondaryVar2DParams::params.quadratureOrder] = quadratureOrder;
    d[SecondaryVar2DParams::params.nameCDclosure] = nameCDclosure_;

    SecondaryVar2DParams::checkInputs(d);

    // TODO: Note that it is assumed here that the same quadrature rule (order) is used throughout
    // the secondary variable calculations.  This needs to change if we want to explore adaptive quadrature.

    // cache quadrature weights/coordinates
    for (int i=0; i < nquad_; i++)
    {
      w_[i] = quadrature_.weight(i);
      eta_[i] = quadrature_.coordinate(i);
    }
  }

  // Gets quadrature order for profile integration
  int quadratureOrder() const { return quadrature_.order(); };
  int nQuadrature() const { return nquad_; };

  std::string nameCDclosure() const { return nameCDclosure_; }

  Real getA_GbetaLocus() const { return A_GbetaLocus_; }
  Real getB_GbetaLocus() const { return B_GbetaLocus_; }

  // Functor for computing secondary variables from primary variables
  // Type abbreviations: T = primary var, Tout = derived input var (e.g. Re_delta)
  template<class T, class Tout>
  class Functor
  {
  public:
    Functor(const int& nquad,
            const std::vector<Real>& w,
            const std::vector<Real>& eta,
            const ProfileCategory& profileCat,
            const T& delta, const T& A, const T& ctau, const Tout& Redelta,
            const VelocityProfileType& velocityProfile,
            const DensityProfile2DType& densityProfile,
            const ViscosityProfile2DType& viscosityProfile,
            const std::string& nameCDclosure,
            const Real& A_GbetaLocus,
            const Real& B_GbetaLocus);

    // subordinate methods for computing each secondary variable
    Tout getdelta1s() const;
    Tout gettheta11() const;
    Tout gettheta1s() const;
    Tout getdelta1p() const;
    Tout getdeltarho() const;
    Tout getdeltaq() const;
    Tout getRedCf1() const;
    Tout getRedCD() const;
    Tout getctaueq() const;

  protected:
    const int& nquad_; // this member needs to be declared here because the member of the same name
                       // from the parent class is not visible inside this class
    const std::vector<Real>& w_, eta_; // quadrature weights/coordinates

    const ProfileCategory profileCat_;
    const T& delta_;
    const T& A_;
    const T& ctau_;
    const Tout& Redelta_;

    mutable std::map<VelocityInfoType, std::vector<Tout>> velocityInfo_;

    const VelocityProfileType& velocityProfile_;
    const DensityProfile2DType& densityProfile_;
    const ViscosityProfile2DType& viscosityProfile_;

    const std::string& nameCDclosure_;

    // G-beta locus constants
    const Real& A_GbetaLocus_;
    const Real& B_GbetaLocus_;
  };

  // Returns functor to carry out secondary variable calculations
  template<class T, class Tout>
  Functor<T,Tout>
  secondaryVarCalc(const ProfileCategory& profileCat,
                   const T& delta, const T& A, const T& ctau, const Tout& Redelta) const;

private:
  const std::string nameCDclosure_;
  const QuadratureLine quadrature_;
  const int nquad_;
  std::vector<Real> w_, eta_; // quadrature weights/coordinates

  const VelocityProfileType velocityProfile_;
  const DensityProfile2DType densityProfile_;
  const ViscosityProfile2DType viscosityProfile_;

  // G-beta locus constants
  const Real A_GbetaLocus_ = 6.7;
  const Real B_GbetaLocus_ = 0.75;
};

} // namespace SANS


#endif /* SRC_PDE_IBL_SECONDARYVAR2D_H_ */
