// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PDEIBLSplitAmpLag2D.h"

namespace SANS
{

template<class VarType>
void
PDEIBLSplitAmpLag<PhysD2,VarType>::dump( int indentSize, std::ostream& out ) const
{
  gasModel_.dump(indentSize,out);
  viscosityModel_.dump(indentSize,out);
}

//----------------------------------------------------------------------------//
// Explicitly instantiations
template class PDEIBLSplitAmpLag<PhysD2,VarTypeDsThNGsplit>;
}
