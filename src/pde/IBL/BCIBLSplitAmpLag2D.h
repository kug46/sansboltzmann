// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_BCIBLSPLITAMPLAG2D_H_
#define SRC_PDE_IBL_BCIBLSPLITAMPLAG2D_H_

#define IS_MATCH_CTAU_INSTEADOF_SQRTCTAU_SplitAmpLag 1 // XFOIL 1989 paper says ctau, but source code v6.99 does sqrt(ctau)

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <string>

#include "pde/BCCategory.h"
#include "pde/BCNone.h"

#include <boost/mpl/vector.hpp>

#include "PDEIBLSplitAmpLag2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// 2-D IBL BC class
//
// template parameters:
//   BCType           BC type (e.g. BCTypeInflowSupercritical)
//   VarType          solution variable type (e.g. primitive variable set)
//----------------------------------------------------------------------------//
template <class BCType, class VarType>
class BCIBLSplitAmpLag2D;

// BCParam: BC parameters
template <class BCType>
struct BCIBLSplitAmpLag2DParams;

// BCType: BC types
class BCTypeWakeMatchSplitAmpLag;

//----------------------------------------------------------------------------//

template<class VarType>
using BCIBLSplitAmpLag2DVector
    = boost::mpl::vector2< BCIBLSplitAmpLag2D<BCTypeWakeMatchSplitAmpLag, VarType>,
                           BCNone<PhysD2,PDEIBLSplitAmpLag<PhysD2,VarType>::N>
                         >;

//----------------------------------------------------------------------------//
// Matching condition: Trailing edge to wake inlet (singled-field IBL)
//----------------------------------------------------------------------------//
template<>
struct BCIBLSplitAmpLag2DParams<BCTypeWakeMatchSplitAmpLag> : noncopyable
{
  static void checkInputs(PyDict d);

  struct MatchingOptions
  {
    typedef std::string ExtractType;
    const ExtractType trailingEdge = "trailingEdge";
    const ExtractType wakeInflow = "wakeInflow";

    const std::vector<ExtractType> options{trailingEdge,wakeInflow};
  };
  const ParameterOption<MatchingOptions> matchingType{"matchingType", NO_DEFAULT, "Used to specify trailing edge or wake inflow BC"};

  static constexpr const char* BCName{"MatchingUniField"};
  struct Option
  {
    const DictOption MatchingUniField{BCIBLSplitAmpLag2DParams::BCName, BCIBLSplitAmpLag2DParams::checkInputs};
  };

  static BCIBLSplitAmpLag2DParams params;
};

template <class VarType>
class BCIBLSplitAmpLag2D<BCTypeWakeMatchSplitAmpLag, VarType> :
    public BCType< BCIBLSplitAmpLag2D<BCTypeWakeMatchSplitAmpLag, VarType> >
{
public:
  typedef typename BCCategory::HubTrace Category;
  typedef BCIBLSplitAmpLag2DParams<BCTypeWakeMatchSplitAmpLag> ParamsType;

  typedef PhysD2 PhysDim;
  typedef PDEIBLSplitAmpLag<PhysDim,VarType> PDEClass;

  static const int D = PDEClass::D;   // physical dimensions
  static const int N = PDEClass::N;   // total solution variables

  static const int NBC = 0; // total BCs

  template <class T>
  using ArrayQ = typename PDEClass::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDEClass::template MatrixQ<T>; // matrices

  template <class T>
  using ArrayParam = typename PDEClass::template ArrayParam<T>;     // solution/residual array type

  template <class TX>
  using VectorX = typename PDEClass::template VectorX<TX>; // size-D vector (e.g. basis unit vector) type

  template <class TX>
  using TensorX = typename PDEClass::template TensorX<TX>; // size-D vector (e.g. basis unit vector) type

  typedef typename PDEClass::ProfileCategory ProfileCategory;

  // cppcheck-suppress noExplicitConstructor
  BCIBLSplitAmpLag2D( const PDEClass& pde, const std::string& matching ) :
    pde_(pde),
    matching_(matching),
    varInterpret_(pde_.getVarInterpreter()),
    paramInterpret_(pde_.getParamInterpreter())
  {
    SANS_ASSERT_MSG( (matching_ == ParamsType::params.matchingType.trailingEdge) ||
                     (matching_ == ParamsType::params.matchingType.wakeInflow),
                     "matching_ = %s is an unknown matching condition identifier",
                     matching_.c_str());
  }

  BCIBLSplitAmpLag2D(const PDEClass& pde, const PyDict& d) :
    BCIBLSplitAmpLag2D(pde, d.get(ParamsType::params.matchingType)) {}

  virtual ~BCIBLSplitAmpLag2D() {}
  BCIBLSplitAmpLag2D& operator=( const BCIBLSplitAmpLag2D& ) = delete;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const { return pde_.isValidState(qI); }

  template <class T, class Tp, class Tout>
  void fluxMatchingSourceHubtrace(
      const ArrayParam<Tp>& params, const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time,
      const Real& nx, const Real& nz,
      const ArrayQ<T>& var, const ArrayQ<T>& varhb,
      ArrayQ<Tout>& f, ArrayQ<Tout>& hubsource ) const;

  const Real hb_dummy_ = 0.0;

protected:
  const PDEClass& pde_;
  const std::string matching_; // cannot be a reference; otherwise, a derived class may cast it into a different type
  const typename PDEClass::VarInterpType varInterpret_;
  const typename PDEClass::ParamInterpType paramInterpret_;
};

//----------------------------------------------------------------------------//
template <class VarType>
template <class T, class Tp, class Tout>
inline void
BCIBLSplitAmpLag2D<BCTypeWakeMatchSplitAmpLag,VarType>::fluxMatchingSourceHubtrace(
    const ArrayParam<Tp>& params, const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time,
    const Real& nx, const Real& nz,
    const ArrayQ<T>& var, const ArrayQ<T>& varhb,
    ArrayQ<Tout>& f, ArrayQ<Tout>& sourcehub ) const
{
  ProfileCategory profileCat = pde_.getProfileCategory(var,x);

  const T ctau = varInterpret_.getctau(var);

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);

  // derived variables
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = (pde_.getGasModel()).density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
  const Tp nue = (pde_.getViscosityModel()).dynamicViscosity() / rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const auto& thicknessesCoefficientsFunctor
    = (pde_.getSecondaryVarObj()).getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tout delta1s = thicknessesCoefficientsFunctor.getdelta1s();
  const Tout theta11 = thicknessesCoefficientsFunctor.gettheta11();

  if (matching_ == ParamsType::params.matchingType.trailingEdge)
  {
    ArrayQ<Tout> fx = 0.0, fz = 0.0;
    pde_.fluxAdvective(profileCat, params, e1, x, z, time, var, fx, fz);

    // ------ hubtrace flux ------ //
    f += fx*nx + fz*nz;;

    // ------ hubtrace source ------ //
    sourcehub[PDEClass::ir_mom] += delta1s + fabs(z);
    //TODO: current trailing edge thickness implementation assumes upper and lower TE are on either sides of z=0
    sourcehub[PDEClass::ir_ke] += theta11;
//    sourcehub[PDEClass::ir_amp] += 0.0; // decouples nt of BL from nt of the wake

    if ( profileCat == ProfileCategory::LaminarBL )
    {
      Tout ctaueq = thicknessesCoefficientsFunctor.getctaueq();
      Tout Hk = thicknessesCoefficientsFunctor.getHk();
      const Tout ctauTinit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk, ctaueq);

#if IS_MATCH_CTAU_INSTEADOF_SQRTCTAU_SplitAmpLag
      sourcehub[PDEClass::ir_lag] += theta11 * ctauTinit; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#else
      sourcehub[PDEClass::ir_amplag] += theta11 * sqrt(ctauTinit); // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#endif
    }
    else if ( profileCat == ProfileCategory::TurbulentBL )
#if IS_MATCH_CTAU_INSTEADOF_SQRTCTAU_SplitAmpLag
      sourcehub[PDEClass::ir_lag] += theta11 * ctau; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#else
      sourcehub[PDEClass::ir_lag] += theta11 * sqrt(ctau); // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#endif
    else
      SANS_DEVELOPER_EXCEPTION("Invalid profileCat at trailing edge hubtrace!");
  }
  else if (matching_ == ParamsType::params.matchingType.wakeInflow)
  {
    // ------ hubtrace flux ------ //
//    f += varhb;
    f[PDEClass::ir_mom] += varhb[PDEClass::ir_mom];
    f[PDEClass::ir_ke]  += varhb[PDEClass::ir_ke];
    const VectorX<Real> nrm = {nx, nz};
    f[PDEClass::ir_amp] += pde_.getTransitionModel().ntcrit()*dot(nrm, q1);
    f[PDEClass::ir_lag] += varhb[PDEClass::ir_lag];

    // ------ hubtrace source ------ //
    sourcehub[PDEClass::ir_mom] += - delta1s;
    sourcehub[PDEClass::ir_ke] += - theta11;
    sourcehub[PDEClass::ir_amp] += varhb[PDEClass::ir_amp] - hb_dummy_;

    if (profileCat == ProfileCategory::LaminarWake)
      SANS_DEVELOPER_EXCEPTION("Laminar wake not suppported at wake inflow hubtrace!");
    else if ( profileCat == ProfileCategory::TurbulentWake )
#if IS_MATCH_CTAU_INSTEADOF_SQRTCTAU_SplitAmpLag
      sourcehub[PDEClass::ir_lag] += - theta11 * ctau; // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#else
      sourcehub[PDEClass::ir_lag] += - theta11 * sqrt(ctau); // Matching: sum ctau weighted by momentum defect thicknesses (XFOIL,1989)
#endif
    else
      SANS_DEVELOPER_EXCEPTION("Invalid profileCat at wake inflow hubtrace!");
  }
}

} // namespace SANS

#endif /* SRC_PDE_IBL_BCIBLSPLITAMPLAG2D_H_ */
