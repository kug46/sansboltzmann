// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(SPALDINGLAWMODIFIED_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "SpaldingLawModified.h"

#include <cmath>

#include "tools/SANSException.h"
#include "tools/stringify.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template<class T>
T
SpaldingLawModified::wallprofile(const T& yplus, const Real eta) const
{
  T uplus = 0;                             // u^+
  T uplus_ini = 0;                         // u^+ initial guess

  T resDrop = newtonRelTol_ + 1.0;        // absolute value of the ratio between current residual and initial residual
  T R = 0, dRduplus = 0, R0 = 0;       // residual, jacobian, initial residual
  T duplus = 0;                        // Delta u^+

  // compute initial guess uplus_ini
#if ISIBL3SRCTURB
  T ymplus = yplus * (1.0 - 1.5*eta + pow(eta,2) - 0.25*pow(eta,3));

  if ( 0.0<=ymplus && ymplus<13.8 )
  {
    uplus_ini = ymplus - pow(ymplus,3)/680.0;
  }
  else if ( 13.8<=ymplus )
  {
    uplus_ini = log(ymplus - 7.35) / kappa_ + B_;
  }
  else
    SANS_DEVELOPER_EXCEPTION( "ymplus cannot be negative." );
#else
  if ( unlikely(yplus < 0) )
  {
    const std::string exceptionMsg = "yplus = " + stringify(yplus) + " but it cannot negative.";
    SANS_DEVELOPER_EXCEPTION(exceptionMsg);
  }
  else if ( yplus<7 )
    uplus_ini = yplus;
  else if ( 7<=yplus && yplus<40 )
    uplus_ini = 1.6/kappa_*log(yplus) + B_ - 5.9;
  else
    uplus_ini = 1.0/kappa_*log(yplus) + B_ - 0.4;
#endif

  T ymplus = ( 1 - pow(eta,4.0)/(1+4.0) )*yplus;

  // initial guess and residual
  uplus = uplus_ini;
  residual( ymplus, eta, uplus_ini, R0, dRduplus );

  if ( fabs(R0) > newtonAbsTol_ )  // if initial residual R0 is not good enough
  {
    for ( int iter = 0; iter < maxNewtonIter_; ++iter)
    {
      // compute residual and its jacobian
      residual( ymplus, eta, uplus, R, dRduplus );

      duplus = - R / dRduplus;                  // compute delta
      uplus = uplus + duplus;                   // update uplus

      // residual tolerance
      resDrop = fabs(R/R0);
      if ( !(resDrop > newtonRelTol_ && fabs(R) > newtonAbsTol_) ) { break; } // tolerances satisfied; iteration terminated
    }

    if (resDrop > newtonRelTol_ && fabs(R) > newtonAbsTol_)
      SANS_DEVELOPER_EXCEPTION("Newton solution of the modified Spalding law didn't converge.");
  }

  return uplus;
}

//----------------------------------------------------------------------------//
template<class T>
void
SpaldingLawModified::residual(const T& ymplus, const Real eta, const T& uplus,
                              T& R, T& dRduplus) const
{
  R        = ymplus - uplus
      - exp(-kappa_*B_)*( expm1(kappa_*uplus)      - kappa_*uplus - pow(kappa_*uplus,2)/2.0 - pow(kappa_*uplus,3)/6.0 );

  dRduplus =        - 1.0
      - exp(-kappa_*B_)*( kappa_*exp(kappa_*uplus) - kappa_       - pow(kappa_,2)*uplus     - pow(kappa_,3)*pow(uplus,2)/2.0 );
}

}
