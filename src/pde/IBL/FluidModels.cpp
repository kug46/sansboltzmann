// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "FluidModels.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

void GasModelParamsIBL::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gamma));
  allParams.push_back(d.checkInputs(params.R));
  d.checkUnknownInputs(allParams);
}
GasModelParamsIBL GasModelParamsIBL::params;

void
GasModelIBL::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "GasModelIBL:" << std::endl;
  out << indent << "  gamma_ = " << gamma_ << std::endl;
  out << indent << "  R_ = " << R_ << std::endl;
}


void
ViscosityModel<ViscosityConstant>::dump( int indentSize, std::ostream& out) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ViscosityModel<ViscosityConstant>:" << std::endl;
  out << indent << "  mu_ = " << mu_ << std::endl;
}

#if 0 //TODO: not yet used
void
ViscosityModel<ViscositySutherlandLaw>::dump( int indentSize, std::ostream& out) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ViscosityModel<ViscositySutherlandLaw>:" << std::endl;
  out << indent << "  TSuth_ = " << TSuth_ << std::endl;
  out << indent << "  Tref_ = " << Tref_ << std::endl;
  out << indent << "  muref_ = " << muref_ << std::endl;
}
#endif

PYDICT_PARAMETER_OPTION_INSTANTIATE( TransitionModelParams::DefaultProfileCategoryOptions )

void TransitionModelParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.ntinit));
  allParams.push_back(d.checkInputs(params.ntcrit));
  allParams.push_back(d.checkInputs(params.xtr));
  allParams.push_back(d.checkInputs(params.isTransitionActive));
  allParams.push_back(d.checkInputs(params.intermittencyLength));
  allParams.push_back(d.checkInputs(params.profileCatDefault));
  d.checkUnknownInputs(allParams);
}
TransitionModelParams TransitionModelParams::params;

void
TransitionModel::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "TransitionModel:" << std::endl;
  out << indent << "  profileCatDefault_ = " << static_cast<int>(profileCatDefault_) << std::endl;
  out << indent << "  ntinit_ = " << ntinit_ << std::endl;
  out << indent << "  ntcrit_ = " << ntcrit_ << std::endl;
  out << indent << "  xtr_ = " << xtr_ << std::endl;
  out << indent << "  isTransitionActive_ = " << isTransitionActive_ << std::endl;
  out << indent << "  intermittencyLength_ = " << intermittencyLength_ << std::endl;
}

}
