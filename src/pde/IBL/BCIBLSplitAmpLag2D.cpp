// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCIBLSplitAmpLag2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

template ParameterOption<BCIBLSplitAmpLag2DParams<BCTypeWakeMatchSplitAmpLag>::MatchingOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<BCIBLSplitAmpLag2DParams<BCTypeWakeMatchSplitAmpLag>::MatchingOptions> > const&) const;

//===========================================================================//
//
// Parameter classes
// cppcheck-suppress passedByValue
void BCIBLSplitAmpLag2DParams<BCTypeWakeMatchSplitAmpLag>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.matchingType));
  d.checkUnknownInputs(allParams);
}
BCIBLSplitAmpLag2DParams<BCTypeWakeMatchSplitAmpLag> BCIBLSplitAmpLag2DParams<BCTypeWakeMatchSplitAmpLag>::params;

//===========================================================================//
// Instantiate BC parameters
BCPARAMETER_INSTANTIATE( BCIBLSplitAmpLag2DVector<VarTypeDsThNGsplit> )

} //namespace SANS
