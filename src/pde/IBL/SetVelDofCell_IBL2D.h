// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_SETVELDOFCELL_IBL2D_H_
#define SRC_PDE_IBL_SETVELDOFCELL_IBL2D_H_

#include <fstream>
#include <vector>

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

namespace SANS
{

//----------------------------------------------------------------------------//
//  Assign field DOFs of EIF velocity and its gradient

class SetVelocityDofCell_IBL2D :
    public GroupFunctorCellType< SetVelocityDofCell_IBL2D >
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;

  typedef DLA::VectorS<D,Real> VectorX;
  typedef DLA::VectorS<D,VectorX> VectorVectorX;

  // Save off the boundary trace integrand and the residual vectors
  SetVelocityDofCell_IBL2D( const std::vector<Real>& data,
                            const std::vector<int>& cellGroups ) :
    data_(data),
    cellGroups_(cellGroups) {}

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  // Assign nodal DOFs to the parameter field
  template <class Topology>
  void
  apply( const typename FieldTuple<FieldTuple<FieldTuple<Field<PhysDim, typename Topology::TopoDim, VectorX>,
                                                         Field<PhysDim, typename Topology::TopoDim, VectorX>,
                                                         TupleClass<>>,
                                              Field<PhysDim, typename Topology::TopoDim, VectorX>,
                                              TupleClass<>>,
                                   XField<PhysDim, typename Topology::TopoDim>,
                                   TupleClass<>>
                        ::template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    typedef typename XField<PhysDim, typename Topology::TopoDim>::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, typename Topology::TopoDim, VectorX>::template FieldCellGroupType<Topology> VelocityFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename VelocityFieldCellGroupType::template ElementType<> ElementVelocityFieldClass;

    VelocityFieldCellGroupType& VelfldCell = const_cast<VelocityFieldCellGroupType&>( get<0>(fldsCell) );
    VelocityFieldCellGroupType& VelxXfldCell = const_cast<VelocityFieldCellGroupType&>( get<1>(fldsCell) );
    VelocityFieldCellGroupType& VelzXfldCell = const_cast<VelocityFieldCellGroupType&>( get<2>(fldsCell) );
    const XFieldCellGroupType& xfldCell = get<-1>(fldsCell);

    SANS_ASSERT_MSG( VelfldCell.basis()->category() == BasisFunctionCategory_Hierarchical,
                     "The following method assumes hierarchical basis function");
    SANS_ASSERT_MSG( VelxXfldCell.basis()->category() == BasisFunctionCategory_Hierarchical,
                     "The following method assumes hierarchical basis function");
    SANS_ASSERT_MSG( VelzXfldCell.basis()->category() == BasisFunctionCategory_Hierarchical,
                     "The following method assumes hierarchical basis function");

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementVelocityFieldClass VelfldElem( VelfldCell.basis() );
    ElementVelocityFieldClass VelxXfldElem( VelxXfldCell.basis() );
    ElementVelocityFieldClass VelzXfldElem( VelzXfldCell.basis() );

    const int nDOF = VelfldElem.nDOF();
    SANS_ASSERT_MSG( nDOF == 2, "Currently only implemented for linear grid/param fields" );

    std::vector<VectorX> gradphi(nDOF);

    VectorVectorX gradv = 0;

    const Real sRef[] = { 0.0, 1.0 };

    // loop over elements within group
    const int nelem = xfldCell.nElem();

    for (int elem = 0; elem < nelem; elem++)
    {
      // copy global grid DOFs to element
      xfldCell.getElement( xfldElem, elem );

      // assign DOFs to velocity field element
      for ( int i = 0; i < nDOF; i++ )
      {
        int iDOF = i;
        const int nodalIndxGlobal = elem + i; // Indexing nodal DOFs in the input data

        // basis direction vector
        VectorX e0;
        xfldElem.unitTangent( sRef[i], e0 );

        // velocity components
        Real qxi, qzi;
        qxi = data_[nodalIndxGlobal] * e0[0];
        qzi = data_[nodalIndxGlobal] * e0[1];

        VelfldElem.DOF(iDOF) = {qxi, qzi};
      }

#if 0 // correction for when Xfoil ue output at LE node is negative
      if (elem == nelem/2)
        VelfldElem.DOF(0) = -VelfldElem.DOF(0);
#endif

      // assign DOFs to velocity gradient field element
      for ( int i = 0; i < nDOF; i++ )
      {
        int iDOF = i;

        xfldElem.evalBasisGradient( sRef[i], VelfldElem, gradphi.data(), gradphi.size() );
        VelfldElem.evalFromBasis( gradphi.data(), gradphi.size(), gradv );

        VelxXfldElem.DOF(iDOF) = { gradv[0][0], gradv[1][0] };
        VelzXfldElem.DOF(iDOF) = { gradv[0][1], gradv[1][1] };
      }

      // set fields
      VelfldCell.setElement( VelfldElem, elem );
      VelxXfldCell.setElement( VelxXfldElem, elem );
      VelzXfldCell.setElement( VelzXfldElem, elem );
    }
  }

  //----------------------------------------------------------------------------//
  // Read in velocity data from a file
  static void readVelocity( const std::string& filename, std::vector<Real>& data )
  {
    std::ifstream grid(filename);
    int nline = 0;
    Real VelData;

    if (grid.is_open())
    {
      while ( grid >> VelData )
      {
        data.push_back(VelData);
        nline++;
      }
      grid.close();

      SANS_ASSERT_MSG( nline == (int) data.size(), "nline = %d", "data.size() = %d", nline, (int) data.size() );
    }
    else
    {
      SANS_DEVELOPER_EXCEPTION( "Error: the data file \"%s\" cannot be opened.", filename.c_str() );
    }
  }

protected:
  const std::vector<Real>& data_;
  const std::vector<int> cellGroups_;
};

} //namespace SANS

#endif /* SRC_PDE_IBL_SETVELDOFCELL_IBL2D_H_ */
