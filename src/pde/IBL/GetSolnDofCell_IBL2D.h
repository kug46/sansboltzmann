// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_GETSOLNDOFCELL_IBL2D_H_
#define SRC_PDE_IBL_GETSOLNDOFCELL_IBL2D_H_

#include <algorithm>
#include <fstream>

#include "Field/Field.h"
#include "Field/XField.h"

#include "Field/tools/GroupFunctorType.h"
#include "Field/Tuple/FieldTuple.h"

#include "VarInterpret2D.h"

namespace SANS
{

template<class VarType, class TopoDimOuter>
class GetSolnDofCell_IBL2D_impl;

//----------------------------------------------------------------------------//
// Get point-wise values of field cell groups evaluated at prescribed reference coordinates of within each element
//
// TODO: assume 2D and IBL for now --> can be generalized later

template<class TopoDim>
class GetSolnDofCell_IBL2D_impl<VarTypeDANCt,TopoDim> :
    public GroupFunctorCellType< GetSolnDofCell_IBL2D_impl<VarTypeDANCt,TopoDim> >
{
public:
  typedef PhysD2 PhysDim;
  typedef DLA::VectorS<TopoDim::D,Real> RefCoordType;

  static const int D = PhysDim::D;
  static const int N = IBLTraits<PhysDim>::N;    // solution dimension

  typedef typename IBLTraits<PhysDim>::template VectorX<Real> VectorX;
  typedef typename IBLTraits<PhysDim>::template ArrayQ<Real> ArrayQ;

  typedef VarData2DDANCt VarDataType;
  typedef VarTypeDANCt VarType;
  typedef VarInterpret2D<VarType> VarInterpretType; // solution variable interpreter type

  GetSolnDofCell_IBL2D_impl( const std::vector<RefCoordType>& sRefArr,
                             const std::vector<int>& cellGroups,
                             std::vector<std::vector<ArrayQ> >& data ) :
    sRefArr_(sRefArr),
    cellGroups_(cellGroups),
    varInterpret_(),
    data_(data)
  {
    data_.resize(cellGroups.size());
  }

  std::size_t nCellGroups() const          { return cellGroups_.size(); }
  std::size_t cellGroup(const int n) const { return cellGroups_[n];     }

  //----------------------------------------------------------------------------//
  template <class Topology>
  void
  apply( const typename FieldTuple<XField<PhysDim, TopoDim>, Field<PhysDim, TopoDim, ArrayQ>, TupleClass<>>
             ::template FieldCellGroupType<Topology>& fldsCell,
         const int cellGroupGlobal )
  {
    // Assume fldsCell is a cell from the tuple field (xfield, qfield)
    typedef typename XField<PhysDim, TopoDim>       ::template FieldCellGroupType<Topology> XFieldCellGroupType;
    typedef typename Field<PhysDim, TopoDim, ArrayQ>::template FieldCellGroupType<Topology> QFieldCellGroupType;

    typedef typename XFieldCellGroupType::template ElementType<> ElementXFieldClass;
    typedef typename QFieldCellGroupType::template ElementType<> ElementQFieldClass;

    const XFieldCellGroupType& xfldCell = get<0>(fldsCell);
    const QFieldCellGroupType& qfldCell = const_cast<QFieldCellGroupType&>( get<1>(fldsCell) );

    // element field variables
    ElementXFieldClass xfldElem( xfldCell.basis() );
    ElementQFieldClass qfldElem( qfldCell.basis() );

    const int nelem = xfldCell.nElem(); // number of elements within group
    const int nref = sRefArr_.size(); // number of evaluation points per element

    std::vector<int> cellGroupTmp(cellGroups_); // find() cannot be used for const vector
    std::vector<int>::iterator itCellGroup = std::find(cellGroupTmp.begin(), cellGroupTmp.end(), cellGroupGlobal);
    data_.at(*itCellGroup).resize(nelem*sRefArr_.size()); // resize output data container

    ArrayQ q; // pre-allocate

    for (int elem = 0; elem < nelem; ++elem)
    {
      qfldCell.getElement( qfldElem, elem );

      for (int pt = 0; pt < nref; ++pt)
      {
        qfldElem.eval(sRefArr_.at(pt), data_.at(*itCellGroup).at(elem*nref+pt));
      }
    }
  }

protected:
  const std::vector<RefCoordType>& sRefArr_;
  const std::vector<int> cellGroups_;
  const VarInterpretType varInterpret_;
  std::vector<std::vector<ArrayQ> >& data_;
};


// Factory function
template <class VarType, class TopoDim>
GetSolnDofCell_IBL2D_impl<VarType,TopoDim>
GetSolnDofCell_IBL2D( const std::vector< typename GetSolnDofCell_IBL2D_impl<VarType,TopoDim>::RefCoordType >& sRefArr,
                      const std::vector<int>& cellGroups,
                      std::vector< std::vector< typename GetSolnDofCell_IBL2D_impl<VarType,TopoDim>::ArrayQ > >& data )
{
  return GetSolnDofCell_IBL2D_impl<VarType,TopoDim>(sRefArr, cellGroups, data);
}

} //namespace SANS


#endif /* SRC_PDE_IBL_GETSOLNDOFCELL_IBL2D_H_ */
