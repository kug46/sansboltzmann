// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_JACOBIANPARAM_PROJECTTOQAUXI_QIBL_H_
#define SRC_PDE_IBL_JACOBIANPARAM_PROJECTTOQAUXI_QIBL_H_

#include "Discretization/JacobianParamBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// A class for constructing jacobian of the auxiliary inviscid equations (i.e. L2 projection)
// w.r.t IBL variables QIBL
// This is used for coupling inviscid solver with IBL equations.
//
template<class ProjectToQauxiEqnClass>
class JacobianParam_ProjectToQauxi_QIBL
  : public JacobianParamBase<
      typename AlgebraicEquationSetTraits<typename ProjectToQauxiEqnClass::MatrixParamQIBL,
                                          typename ProjectToQauxiEqnClass::ArrayQ,
                                          typename ProjectToQauxiEqnClass::TraitsTag
                                         >::SystemMatrix
                            >
{
public:
  typedef typename ProjectToQauxiEqnClass::PhysDim PhysDim;
  typedef typename ProjectToQauxiEqnClass::ArrayQ ArrayQ;
  typedef typename ProjectToQauxiEqnClass::MatrixParamQIBL MatrixParam;

  typedef typename ProjectToQauxiEqnClass::TraitsTag TraitsTag;
  typedef AlgebraicEquationSetTraits<MatrixParam, ArrayQ, TraitsTag> TraitsType;

  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef JacobianParamBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemMatrixTranspose SystemMatrixTranspose;
  typedef typename BaseType::SystemNonZeroPatternTranspose SystemNonZeroPatternTranspose;

  explicit JacobianParam_ProjectToQauxi_QIBL(const ProjectToQauxiEqnClass& ProjEqn) :
    ProjEqn_(ProjEqn) {}

  virtual ~JacobianParam_ProjectToQauxi_QIBL() {}

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override { jacobianProjEqn_QIBL<SystemMatrixView>(mtx); }
  virtual void jacobian(SystemNonZeroPatternView& nz) override { jacobianProjEqn_QIBL<SystemNonZeroPatternView>(nz); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobian(SystemMatrixTranspose mtxT       ) override { jacobianProjEqn_QIBL(mtxT); }
  virtual void jacobian(SystemNonZeroPatternTranspose nzT) override { jacobianProjEqn_QIBL(nzT); }

protected:
  template<class SparseMatrixType>
  void jacobianProjEqn_QIBL(SparseMatrixType& jac) const { ProjEqn_.template jacobianParamQIBL<SparseMatrixType>(jac); }

  const ProjectToQauxiEqnClass& ProjEqn_;
};

} // namespace SANS

#endif /* SRC_PDE_IBL_JACOBIANPARAM_PROJECTTOQAUXI_QIBL_H_ */
