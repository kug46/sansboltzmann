// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEIBL_H_
#define PDEIBL_H_

#define ISIBLLFFLUX_PDEIBL 0 // true if LF flux scheme is used; false if full upwinding is used

#define IsmitLGConservationTransitionIBL 0 // true if mass conservation is explicitly enforced at transition front; false otherwise

//#define NOLAGEQN_IBL // if defined, the lag eqn will be replaced with a trivial one
//#define NOAMPEQN_IBL // if defined, the amplification factor eqn will be replaced with a trivial one

#define IsExplicitCheckingIBL 1 // on: for explicit checking that are inconvenient/impossible to routine unit tests; off: improves coverage

#define IsCtauTinitXfoil 1
#define IsCtauTinitBasedOnLamiOutflow (IsCtauTinitXfoil && 1)

#define IS_LAGFACTOR_XFOILPAPER_NOT_XFOILV699_IBLTwoField 1
// determines which lag factor to use: true if using the XFOIL paper; false if using XFOIL v6.99 source code

#define IS_USING_GLAG_INSTEADOF_CTAULAG 0

// PDE class: 2-D IBL

#include "Field/Tuple/ParamTuple.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"   // required for matrix transpose
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"                      // required for dot product
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Surreal/PromoteSurreal.h"

#include "IBLMiscClosures.h"
#include "FluidModels.h"
#include "VarInterpret2D.h"
#include "QauxvInterpret2D.h"
#include "ThicknessesCoefficients2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2-D IBL
//
// Reference: Shun's Master thesis and the IBL2 working document
//
// template parameters:
//   T                        solution DOF data type (e.g. double, SurrealS)
//   VarType                  solution variable type set (e.g. primitive variables, conservative variables)
//   ProfileType              boundary layer profile type (e.g. laminar vs turbulent, incompressible vs compressible)
//
// Nodal solution variable vector (var):
//  Depends on the definition in solution interpreter class (VarType).
//
// Residual vector R
//  [0] R^(e)                 laminar momentum equation
//  [1] R^(*)                 laminar kinetic energy equation
//  [2] R^(e)                 turbulent momentum equation
//  [3] R^(*)                 turbulent kinetic energy equation
//  [4] R^(n)                 amplification factor growth equation
//  [5] R^(t)                 shear stress transport equation
//
// member functions:
//   .hasFluxAdvectiveTime       T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//   .hasSource                  T/F: PDE has source term
//   .hasForcingFunction         T/F: PDE has forcing function term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .source                     solution-dependent sources: S(X, Q, QX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

// PDEIBL2D class definition
template <class VarType>   // required for compiler to understand that a template is used
class PDEIBL<PhysD2, VarType>
{
public:

  enum IBLResidualInterpCategory
  {
    IBL_ResidInterp_Raw
  };

  typedef PhysD2 PhysDim;
  typedef IBLTwoField IBLFormulationType;
  typedef IBLTraits<PhysDim, IBLFormulationType> IBLTraitsType;

  static const int D = IBLTraitsType::D;           // physical dimensions
  static const int N = IBLTraitsType::N;           // total solution variables
  static const int Nparam = IBLTraitsType::Nparam; // total solution variables

  // equation indexing
  static const int ir_momLami = 0;
  static const int ir_keLami = 1;
  static const int ir_momTurb = 2;
  static const int ir_keTurb = 3;
  static const int ir_amp = 4;
  static const int ir_lag = 5;

  static_assert(ir_lag == N-1, "Residual equation index overflows."); // only checks the largest index

  template <class T>
  using ArrayQ = IBLTraitsType::template ArrayQ<T>;     // solution/residual array type

  template <class T>
  using MatrixQ = IBLTraitsType::template MatrixQ<T>;   // matrix type

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class TP>
  using ArrayParam = IBLTraitsType::template ArrayParam<TP>;     // solution/residual array type

  template <class TP>
  using MatrixParam = IBLTraitsType::template MatrixParam<TP>;   // matrix type

  template <class TX>
  using VectorX = IBLTraitsType::VectorX<TX>; // size-D vector (e.g. basis unit vector) type

  template <class TX>
  using TensorX = IBLTraitsType::TensorX<TX>; // size-D vector (e.g. basis unit vector) type

  typedef VarInterpret2D<VarType> VarInterpType; // solution variable interpreter type
  typedef QauxvInterpret2D ParamInterpType;

  typedef VelocityProfile2D VelocityProfileType;
  typedef IBLProfileCategory ProfileCategory;
  typedef GasModelIBL GasModelType;
  typedef ViscosityModel<ViscosityConstant> ViscosityModelType;
  typedef TransitionModel TransitionModelType;

  typedef typename IBLClosureType<VarType>::type ClosureType;
  typedef IncompressibleBL CompressibilityType; // TODO: currently assume incompressible viscous layer
  typedef ThicknessesCoefficients2D<ClosureType, VarType, CompressibilityType> ThicknessesCoefficientsType;

  template<class Tparam, class Tibl>
  using ThicknessesCoefficientsFunctor = typename ThicknessesCoefficientsType::template Functor<Tparam, Tibl>;

  //-----------------------Constructors-----------------------//
  PDEIBL(const GasModelType& gasModel,
         const ViscosityModelType& viscosityModel,
         const TransitionModelType& transitionModel,
         const PyDict d_thicknessesCoefficients = PyDict(),
         const IBLResidualInterpCategory rsdCat = IBL_ResidInterp_Raw) :
    gasModel_(gasModel),
    viscosityModel_(viscosityModel),
    transitionModel_(transitionModel),
    varInterpret_(),
    paramInterpret_(),
    thicknessesCoefficientsObj_(varInterpret_, paramInterpret_, d_thicknessesCoefficients),
    rsdCat_(rsdCat)
  {
    SANS_ASSERT(rsdCat_ == IBL_ResidInterp_Raw);
  }

  //-----------------------Default class methods-----------------------//
  ~PDEIBL() {}
  PDEIBL(const PDEIBL&) = delete;
  PDEIBL& operator=(const PDEIBL&)  = delete;

  //-----------------------Members-----------------------//
  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  bool needsSolutionGradientforSource() const { return false; }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tp, class Tout>
  void fluxAdvectiveTime(const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                         const Real& x, const Real& z, const ArrayQ<T>& var, ArrayQ<Tout>& ft ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not used!");
//    Tout ftLocal = 0;
//    masterState(params, e1, x, z, var, ftLocal);
//    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tp, class Tout>
  void jacobianFluxAdvectiveTime(const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                                 const Real& x, const Real& z, const ArrayQ<T>& var, MatrixQ<Tout>& J ) const
  {
    SANS_DEVELOPER_EXCEPTION("Not used!");
//    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T, class Tp, class Tout>
  void masterState(const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                    const Real& x, const Real& z, const ArrayQ<T>& var,
                    ArrayQ<Tout>& uCons) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(const ArrayQ<T>& q, MatrixQ<T>& dudq) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(const ArrayQ<T>& var, const ArrayQ<T>& vart, ArrayQ<T>& strongPDE) const;

  // advective flux: F(Q)
  template <class T, class Tp, class Tf>
  void fluxAdvective(
      const ArrayParam<Tp>& params,
      const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<T>& var,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fz) const;

  template <class T, class Tp, class Tf>
  void fluxAdvectiveUpwind(
      const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
      const VectorX<Real>& e1L, const VectorX<Real>& e1R,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<T>& varL, const ArrayQ<T>& varR,
      const Real& nx, const Real& nz,
      ArrayQ<Tf>& f) const;

//  template <class TL, class TR, class Tp, class Tf>
//  void fluxAdvectiveUpwind(
//      const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
//      const VectorX<Real>& e1L, const VectorX<Real>& e1R,
//      const Real& x, const Real& z, const Real& time,
//      const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
//      const Real& nx, const Real& nz,
//      ArrayQ<Tf>& fL, ArrayQ<Tf>& fR) const;

  template <class TL, class TR, class Tp, class Tf>
  void fluxAdvectiveUpwind(
      const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
      const VectorX<Real>& e1L, const VectorX<Real>& e1R,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
      const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
      ArrayQ<Tf>& fL, ArrayQ<Tf>& fR) const;

  template <class T>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<T>& f) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var,
      MatrixQ<T>& ax, MatrixQ<T>& ay) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& var, const Real& nx, const Real& ny,
      MatrixQ<T>& a) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<T>& a) const;

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      ArrayQ<T>& strongPDE) const;

  // viscous flux: Fv(Q, QX)
  template <class T, class Tg, class Tp, class Tf>
  void fluxViscous(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g) const;

  template <class T, class Tg, class Tp, class Tf>
  void fluxViscous(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& varL, const ArrayQ<Tg>& varxL, const ArrayQ<Tg>& varyL,
      const ArrayQ<T>& varR, const ArrayQ<Tg>& varxR, const ArrayQ<Tg>& varyR,
      const Real& nx, const Real& ny,
      ArrayQ<Tf>& f) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class T, class Tg, class Tp, class Tk>
  void diffusionViscous(
      const ArrayParam<Tp>& params, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, MatrixQ<T>& ax, MatrixQ<T>& ay) const;

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& varx, const ArrayQ<T>& vary,
      const ArrayQ<T>& varxx,
      const ArrayQ<T>& varxy, const ArrayQ<T>& varyy,
      ArrayQ<T>& strongPDE) const;


  // solution-dependent source: S(X, Q, QX)
  template <class T, class Tg, class Tp, class Tout>
  void source(
      const ArrayParam<Tp>& params,
      const VectorX<Real>& e1,
      const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<T>& var,
      const ArrayQ<Tg>& varx, const ArrayQ<Tg>& varz,
      ArrayQ<Tout>& source) const;

  // dual-consistent source
  template <class T>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<T>& varL, const ArrayQ<T>& varxL, const ArrayQ<T>& varyL,
      const ArrayQ<T>& varR, const ArrayQ<T>& varxR, const ArrayQ<T>& varyR,
      ArrayQ<T>& sourceL, ArrayQ<T>& sourceR) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tg, class Tp>
  void jacobianSource(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ< typename promote_Surreal<T,Tg,Tp>::type >& dsdu) const {}
  //TODO: Empty now since the analytic jacobian is not readily available.
  //      But may need to fill this up later for better robustness using pseudo time continuation

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tg, class Tp>
  void jacobianSourceAbsoluteValue(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<Tg>& varx, const ArrayQ<Tg>& vary,
      MatrixQ< typename promote_Surreal<T,Tg,Tp>::type >& dsdu) const {}
  //TODO: Empty now since the analytic jacobian is not readily available.
  //      But may need to fill this up later for better robustness using pseudo time continuation

  // right-hand-side forcing function
  template <class T, class Tp>
  void forcingFunction(const ArrayParam<Tp>& params, const Real& x, const Real& y, const Real& time,
                        ArrayQ<T>& forcing) const {}

  // advective flux and matching conditions at the transition front
  template <class Tm, class T, class Tp, class Tout>
  void cutCellTransitionFluxAndSource(
      const ArrayParam<Tp>& params,
      const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<Tm>& varMatch, const ArrayQ<T>& var,
      const Real& nx, const Real& nz,
      ArrayQ<Tout>& f, ArrayQ<Tout>& sourceMatching) const;

  // dummy equation (cast in source terms) for the matching variable in non-transition elements
  template <class Tm, class Tout>
  void sourceMatchNonTransitionDummy(
      const ArrayQ<Tm>& varMatch,
      ArrayQ<Tout>& sourceMatching) const;

  template <class T, class Tp, class Tout>
  void sourceTurbAmp(
      const ArrayParam<Tp>& params,
      const VectorX<Real>& e1,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<T>& var,
      Tout& source_turbAmp) const;

  // characteristic speed (needed for timestep)
  template <class T, class Tp>
  void speedCharacteristic(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, Tp& speed) const;

  // is state physically valid
  bool isValidState(const ArrayQ<Real>& q) const;

  // update fraction needed for physically valid state
  template <class T, class Tp>
  void updateFraction(
      const ArrayParam<Tp>& params,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& var, const ArrayQ<T>& dvar,
      const Real& maxChangeFraction, Real& updateFraction) const;

  // set from variable array
  template<class T, class IBLVarData>
  void setDOFFrom(const IBLVarDataType<IBLVarData>& data, ArrayQ<T>& var) const;

  // set from variable array
  template<class IBLVarData>
  ArrayQ<Real> setDOFFrom(const IBLVarDataType<IBLVarData>& data) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable(const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient(const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC(const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut) const;

  // return the interp type
  IBLResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // print out parameters
  void dump(int indentSize, std::ostream& out = std::cout) const;

  // access members
  const GasModelType& getGasModel() const { return gasModel_; }
  const ViscosityModelType& getViscosityModel() const { return viscosityModel_; }
  const TransitionModelType& getTransitionModel() const { return transitionModel_; }
  const ThicknessesCoefficientsType& getSecondaryVarObj() const { return thicknessesCoefficientsObj_; }
  const VarInterpType& getVarInterpreter() const { return varInterpret_; }
  const ParamInterpType& getParamInterpreter() const { return paramInterpret_; }

  // evaluate parameters
  void evalParam(Real& gam, Real& R, Real& mue) const
  {
    gam = gasModel_.gamma();
    R = gasModel_.R();
    mue = viscosityModel_.dynamicViscosity();
  }

  // determine profile/closure category/type
  template <class T, class Tp>
  typename promote_Surreal<T,Tp>::type
  calcIntermittency(const ArrayParam<Tp>& params, const VectorX<Real>& e1,
                    const Real& x, const Real& z, const ArrayQ<T>& var) const
  {
    const T& nt = varInterpret_.getnt(var);
    return transitionModel_.binaryIntermittency(nt,x);
  }

  template <class T>
  ProfileCategory getProfileCategory(const ArrayQ<T>& var, const Real& x) const
  {
    const T& nt = varInterpret_.getnt(var);
    return transitionModel_.getProfileCategory(nt,x);
  }

  template <class TL, class TI, class Tp, class Tf>
  void interfaceFluxAndSourceHDGIBL(
      const ArrayParam<Tp>& paramsL, const VectorX<Real>& e1L,
      const Real& x, const Real& z, const Real& time,
      const ArrayQ<TL>& varL, const ArrayQ<TI>& varI,
      const Real& nxL, const Real& nzL,
      ArrayQ<Tf>& fL, ArrayQ<Tf>& sourceL) const;

protected:
  const GasModelType& gasModel_;
  const ViscosityModelType& viscosityModel_;
  const TransitionModelType& transitionModel_;

  const VarInterpType varInterpret_; // solution variable interpreter object
  const ParamInterpType paramInterpret_;// parameter interpreter object
  const ThicknessesCoefficientsType thicknessesCoefficientsObj_; // secondary variable object
  const IBLResidualInterpCategory rsdCat_; // ResidInterp type identifier

public:
  const Real varMatchDummy_ = 0.01;
}; // class PDEIBL<PhysD2, VarType, ProfileType>


//---------------------Member function definitions of PDEIBL2D---------------------//

// unsteady conservative flux: U(Q) (aka conservative variables/quantities)
template <class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBL<PhysD2, VarType>::
masterState(const ArrayParam<Tp>& params, const VectorX<Real>& e1,
             const Real& x, const Real& z, const ArrayQ<T>& var,
             ArrayQ<Tout>& uCons) const
{
//  typedef typename ThicknessesCoefficientsFunctor<Tp, T>::Tout Tpi;

  ProfileCategory profileCat = getProfileCategory(var,x);

  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
  const Tp nue = viscosityModel_.dynamicViscosity() / rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  T delta1s  = thicknessesCoefficientsFunctor.getdelta1s();
  T deltarho = thicknessesCoefficientsFunctor.getdeltarho();
  T theta0s   = thicknessesCoefficientsFunctor.gettheta0s();

  VectorX<Tout> p = rhoe*(delta1s-deltarho)*paramInterpret_.getq1(params); // momentum defect

  Tout k = rhoe*pow(qe,2)*theta0s; //kinetic energy defect

  // master state of momentum and kinetic energy equations
  if (profileCat == ProfileCategory::LaminarBL ||
      profileCat == ProfileCategory::LaminarWake)
  {
    uCons[ir_momLami] = dot(p,e1);
    uCons[ir_keLami] = k;

    uCons[ir_momTurb] = 0.0;
    uCons[ir_keTurb] = 0.0;
  }
  else if (profileCat == ProfileCategory::TurbulentBL ||
           profileCat == ProfileCategory::TurbulentWake)
  {

    uCons[ir_momLami] = 0.0;
    uCons[ir_keLami] = 0.0;

    uCons[ir_momTurb] = dot(p,e1);
    uCons[ir_keTurb] = k;
  }
#if IsExplicitCheckingIBL
  else
    SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");
#endif

  uCons[ir_amp] = varInterpret_.getnt(var);
#if IS_USING_GLAG_INSTEADOF_CTAULAG
  uCons[ir_lag] = varInterpret_.getG(var);
#else
  uCons[ir_lag] = varInterpret_.getctau(var);
#endif
}

//----------------------------------------------------------------------------//
template <class VarType>
template <class T, class Tp, class Tf>
inline void
PDEIBL<PhysD2, VarType>::fluxAdvective(
    const ArrayParam<Tp>& params,
    const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<T>& var,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fz) const
{
  // DETERMINE CLOSURE TYPE -----------------------------
  const ProfileCategory profileCat = getProfileCategory(var,x);

#if !defined(NOAMPEQN_IBL)
  const T nt = varInterpret_.getnt(var);
#endif

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);

  // derived variables
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));
  const Tp nue = viscosityModel_.dynamicViscosity() / rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tf theta11 = thicknessesCoefficientsFunctor.gettheta11();
  const Tf theta1s = thicknessesCoefficientsFunctor.gettheta1s();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  // P happens to be symmetric in 2D IBL. Note "*" in () is an outer product
//  const TensorX<Tf> P = rhoe*theta11*(q1*Transpose(q1)); // [tensor] P: momentum defect flux
  const VectorX<Tf> PTdote = rhoe*theta11*q1*dot(q1,e1); // P^T dot e. This simplified calculation is only valid in IBL2D where P is symmetric

  const VectorX<Tf> K = rhoe*pow(qe,2)*theta1s*q1; // [vector] K: kinetic energy defect flux

  // UPDATE/OUTPUT ADVECTIVE FLUX TERMS -----------------------------------------------------------
  const Tf gam = calcIntermittency(params, e1, x, z, var);

  // compute advective fluxes
  fx[ir_momLami] += (1.0-gam) * PTdote(0);
  fz[ir_momLami] += (1.0-gam) * PTdote(1);

  fx[ir_keLami]  += (1.0-gam) * K(0);
  fz[ir_keLami]  += (1.0-gam) * K(1);

  fx[ir_momTurb] += gam * PTdote(0);
  fz[ir_momTurb] += gam * PTdote(1);

  fx[ir_keTurb]  += gam * K(0);
  fz[ir_keTurb]  += gam * K(1);

  // advective flux terms in amplification factor residual
#if defined(NOAMPEQN_IBL)
  fx[ir_amp] += 0.0;
  fz[ir_amp] += 0.0;
#else
  fx[ir_amp] += nt * q1[0];
  fz[ir_amp] += nt * q1[1];
#endif

#if defined(NOLAGEQN_IBL)
  fx[ir_lag] += 0.0;
  fz[ir_lag] += 0.0;
#else

#if IS_USING_GLAG_INSTEADOF_CTAULAG
  const T G = varInterpret_.getG(var);
  fx[ir_lag] += gam * G * q1[0];
  fz[ir_lag] += gam * G * q1[1];
#else
  const T ctau  = varInterpret_.getctau(var);
  fx[ir_lag] += gam * ctau * q1[0];
  fz[ir_lag] += gam * ctau * q1[1];
#endif
#endif
}

//----------------------------------------------------------------------------//
template <class VarType>
template <class T, class Tp, class Tf>
inline void
PDEIBL<PhysD2,VarType>::fluxAdvectiveUpwind(
    const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
    const VectorX<Real>& e1L, const VectorX<Real>& e1R,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<T>& varL, const ArrayQ<T>& varR,
    const Real& nx, const Real& nz,
    ArrayQ<Tf>& f) const
{
  SANS_DEVELOPER_EXCEPTION("Should not use this?"); //TODO: TBD

//  const VectorX<Tp> q1L = paramInterpret_.getq1(paramsL);
//  VectorX<Real> nrm = {nx, nz};
//
//  ArrayQ<Tf> fL = 0.0, fR = 0.0;
//  fluxAdvectiveUpwind(paramsL, paramsR, e1L, e1R, x, z, time, varL, varR, nx, nz,
//                      fL, fR);
//
//  SANS_ASSERT_MSG(dot(q1L,nrm) < 0,
//                  "This fluxAdvectiveUpwind method is currently only intended for mit-state Dirichlet BC (i.e. full state inflow)");
//
//  f += fL;
}

//----------------------------------------------------------------------------//
// TODO: unit test to be extended
template <class VarType>
template <class TL, class TR, class Tp, class Tf>
inline void
PDEIBL<PhysD2,VarType>::fluxAdvectiveUpwind(
    const ArrayParam<Tp>& paramsL, const ArrayParam<Tp>& paramsR,
    const VectorX<Real>& e1L, const VectorX<Real>& e1R,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<TL>& varL, const ArrayQ<TR>& varR,
    const Real& nxL, const Real& nzL, const Real& nxR, const Real& nzR,
    ArrayQ<Tf>& fL, ArrayQ<Tf>& fR) const
{
  // DETERMINE CLOSURE TYPE -----------------------------
  const ProfileCategory profileCatL = getProfileCategory(varL,x);
  const ProfileCategory profileCatR = getProfileCategory(varR,x);

  const TL ntL = varInterpret_.getnt(varL);
  const TR ntR = varInterpret_.getnt(varR);

  // velocity vector q_1, defined as q_1 = q_e
  const VectorX<Tp> q1L = paramInterpret_.getq1(paramsL);
  const VectorX<Tp> q1R = paramInterpret_.getq1(paramsR);

  // derived variables
  const Tp qeL = paramInterpret_.getqe(paramsL);
  const Tp qeR = paramInterpret_.getqe(paramsR);

  const Tp rhoeL = gasModel_.density(qeL, paramInterpret_.getp0(paramsL), paramInterpret_.getT0(paramsL));
  const Tp rhoeR = gasModel_.density(qeR, paramInterpret_.getp0(paramsR), paramInterpret_.getT0(paramsR));

  const Tp nueL = viscosityModel_.dynamicViscosity() / rhoeL;
  const Tp nueR = viscosityModel_.dynamicViscosity() / rhoeR;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const ThicknessesCoefficientsFunctor<Tp, TL> thicknessesCoefficientsFunctorL
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatL, paramsL, varL, nueL);
  const ThicknessesCoefficientsFunctor<Tp, TR> thicknessesCoefficientsFunctorR
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatR, paramsR, varR, nueR);

  Tf theta11L  = thicknessesCoefficientsFunctorL.gettheta11();
  Tf theta1sL  = thicknessesCoefficientsFunctorL.gettheta1s();

  Tf theta11R  = thicknessesCoefficientsFunctorR.gettheta11();
  Tf theta1sR  = thicknessesCoefficientsFunctorR.gettheta1s();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  // P is symmetric in 2D IBL. Note "*" in () is an outer product
//  TensorX<Tf> PL = rhoeL*theta11L*(q1L*Transpose(q1L)); // [tensor] P: momentum defect flux
//  TensorX<Tf> PR = rhoeR*theta11R*(q1R*Transpose(q1R));
//
//  VectorX<Tf> PTdoteL = Transpose(PL) * e1L; // P^T dot e
//  VectorX<Tf> PTdoteR = Transpose(PR) * e1R;

  const VectorX<Tf> PTdoteL = rhoeL*theta11L*dot(q1L,e1L)*q1L; // P^T dot e. This simplified calculation is only valid in IBL2D where P is symmetric
  const VectorX<Tf> PTdoteR = rhoeR*theta11R*dot(q1R,e1R)*q1R;

  const VectorX<Tf> KL = rhoeL*pow(qeL,2)*theta1sL*q1L; // [vector] K: kinetic energy defect flux
  const VectorX<Tf> KR = rhoeR*pow(qeR,2)*theta1sR*q1R;

  const VectorX<Real> nrmL = {nxL, nzL}; // trace normal vector
  const VectorX<Real> nrmR = {nxR, nzR}; // trace normal vector

  const Tp qnL = dot(q1L, nrmL);
  const Tp qnR = dot(q1R, nrmR);

  Tf gamL = calcIntermittency(paramsL, e1L, x, z, varL);
  Tf gamR = calcIntermittency(paramsR, e1R, x, z, varR);

#if ISIBLLFFLUX_PDEIBL // Lax-Friedrichs flux
  Tf delta1sL  = thicknessesCoefficientsFunctorL.getdelta1s();
  Tf deltarhoL = thicknessesCoefficientsFunctorL.getdeltarho();
  Tf theta0sL   = thicknessesCoefficientsFunctorL.gettheta0s();

  Tf delta1sR  = thicknessesCoefficientsFunctorR.getdelta1s();
  Tf deltarhoR = thicknessesCoefficientsFunctorR.getdeltarho();
  Tf theta0sR   = thicknessesCoefficientsFunctorR.gettheta0s();

  VectorX<Tf> pL = rhoeL*(delta1sL-deltarhoL)*q1L;
  VectorX<Tf> pR = rhoeR*(delta1sR-deltarhoR)*q1R;

  Tf kL = rhoeL*pow(qeL,2)*theta0sL; // k: kinetic energy defect
  Tf kR = rhoeR*pow(qeR,2)*theta0sR;

#if 1 // TODO: this is okay for 2D IBL
  Tp alpha = std::max(qeL, qeR);  // dissipation coefficient
#else // TODO: this should be used for 3D IBL
  Tp alpha = std::max(fabs(dot(q1L,nrm)), fabs(dot(q1R,nrm)));  // dissipation coefficient
#endif

  Tf f_mom = 0.5 * (dot(PTdoteL, nrmL) + dot(PTdoteR, nrmR)) + 0.5 * alpha * (dot(pL,e1L) - dot(pR,e1R));
  Tf f_ke  = 0.5 * (dot(KL, nrmL) + dot(KR, nrmR)) + 0.5 * alpha * (kL - kR);

#else // Full upwind
  Tf f_mom = 0.0, f_ke = 0.0;

  // Assume q1L and nrm are not orthogonal.
  if (qnL > 0)
  {
    f_mom = dot(PTdoteL, nrmL);
    f_ke = dot(KL, nrmL);
  }
  else
  {
    f_mom = dot(PTdoteR, nrmR);
    f_ke = dot(KR, nrmR);
  }
#endif

  // compute interface flux depending on the type of the interface
  Tf f_lag = 0.0;

#if IS_USING_GLAG_INSTEADOF_CTAULAG

  const TL GL = varInterpret_.getG(varL);
  const TR GR = varInterpret_.getG(varR);

  if (profileCatL == profileCatR) // interface without transition
  {
#if ISIBLLFFLUX_PDEIBL // Lax-Friedrichs flux
    f_lag = 0.5 * (GL*qnL + GR*qnR) + 0.5 * alpha * (GL - GR);
#else // Full upwind
    // Assume q1L and nrm are not orthogonal.
    if (qnL > 0)
      f_lag = GL*qnL;
    else
      f_lag = GR*qnR;

#endif
  }
  else if ((qnL > 0 && profileCatL == ProfileCategory::LaminarBL && profileCatR == ProfileCategory::TurbulentBL) ||
           (qnL < 0 && profileCatR == ProfileCategory::LaminarBL && profileCatL == ProfileCategory::TurbulentBL))
  { // interface coincident with the transition front: flow from laminar into turbulent
//    SANS_DEVELOPER_EXCEPTION("Not totally sure if this is necessary yet. Worth keeping an eye on it"); //TODO
    // TODO: should switch to numerical flux, instead of full upwinding

#if ISIBLLFFLUX_PDEIBL // use LF flux for ctau inflow condition
    Tf G_TurbInit, GTurb;
    Tp qnLami, qnTurb;

    if (profileCatL == ProfileCategory::LaminarBL)
    {
      const Tf Hk_Lami = thicknessesCoefficientsFunctorL.getHk();
      const Tf ctau_eqLami = thicknessesCoefficientsFunctorL.getctaueq();

      G_TurbInit = varInterpret_.getG_from_ctau(IBLMiscClosures::getctauTurbInit_XFOIL(Hk_Lami, ctau_eqLami));
      GTurb = GR;

      qnLami = qnL;
      qnTurb = qnR;
    }
    else
    {
      const Tf Hk_Lami = thicknessesCoefficientsFunctorR.getHk();
      const Tf ctau_eqLami = thicknessesCoefficientsFunctorR.getctaueq();

      G_TurbInit =IBLMiscClosures::getctauTurbInit_XFOIL(Hk_Lami, ctau_eqLami);
      GTurb = GL;

      qnLami = qnR;
      qnTurb = qnL;
    }


    f_lag = 0.5 * (G_TurbInit*qnLami + GTurb*qnTurb)
          + 0.5 * alpha * (G_TurbInit - GTurb);
#else

    if (profileCatL == ProfileCategory::LaminarBL)
    {
      const Tf ctaueq_Lami = thicknessesCoefficientsFunctorL.getctaueq();
      const Tf Hk_Lami = thicknessesCoefficientsFunctorL.getHk();
      const Tf G_TurbInit = varInterpret_.getG_from_ctau(IBLMiscClosures::getctauTurbInit_XFOIL(Hk_Lami, ctaueq_Lami));
      f_lag = G_TurbInit*qnL;
    }
    else
    {
      const Tf ctaueq_Lami = thicknessesCoefficientsFunctorR.getctaueq();
      const Tf Hk_Lami = thicknessesCoefficientsFunctorR.getHk();
      const Tf G_TurbInit = varInterpret_.getG_from_ctau(IBLMiscClosures::getctauTurbInit_XFOIL(Hk_Lami, ctaueq_Lami));
      f_lag = G_TurbInit*qnR;
    }

#endif
  }
  else if ((qnL > 0 && profileCatR == ProfileCategory::LaminarBL && profileCatL == ProfileCategory::TurbulentBL) ||
           (qnL < 0 && profileCatL == ProfileCategory::LaminarBL && profileCatR == ProfileCategory::TurbulentBL))
  { // interface coinciding with the transition front: flow from turbulent into laminar
//    SANS_DEVELOPER_EXCEPTION("Not totally sure if this is necessary yet. Worth keeping an eye on it");
    //TODO: this case is currently underserved
    if (profileCatL == ProfileCategory::LaminarBL)
      f_lag = GR*qnR;
    else
      f_lag = GL*qnL;
  }
#if IsExplicitCheckingIBL
  else
    SANS_DEVELOPER_EXCEPTION("profileCatL and profileR combination is not allowed!");
#endif

#else

  const TL ctauL = varInterpret_.getctau(varL);
  const TR ctauR = varInterpret_.getctau(varR);

  if (profileCatL == profileCatR) // interface without transition
  {
#if ISIBLLFFLUX_PDEIBL // Lax-Friedrichs flux
    f_lag = 0.5 * (ctauL*qnL + ctauR*qnR) + 0.5 * alpha * (ctauL - ctauR);
#else // Full upwind
    // Assume q1L and nrm are not orthogonal.
    if (qnL > 0)
      f_lag = ctauL*qnL;
    else
      f_lag = ctauR*qnR;

#endif
  }
  else if ((qnL > 0 && profileCatL == ProfileCategory::LaminarBL && profileCatR == ProfileCategory::TurbulentBL) ||
           (qnL < 0 && profileCatR == ProfileCategory::LaminarBL && profileCatL == ProfileCategory::TurbulentBL))
  { // interface coincident with the transition front: flow from laminar into turbulent
//    SANS_DEVELOPER_EXCEPTION("Not totally sure if this is necessary yet. Worth keeping an eye on it"); //TODO
    // TODO: should switch to numerical flux, instead of full upwinding

#if ISIBLLFFLUX_PDEIBL // use LF flux for ctau inflow condition
    Tf ctau_TurbInit, ctauTurb;
    Tp qnLami, qnTurb;

    if (profileCatL == ProfileCategory::LaminarBL)
    {
      const Tf Hk_Lami = thicknessesCoefficientsFunctorL.getHk();
      const Tf ctau_eqLami = thicknessesCoefficientsFunctorL.getctaueq();

      ctau_TurbInit =IBLMiscClosures::getctauTurbInit_XFOIL(Hk_Lami, ctau_eqLami);
      ctauTurb = ctauR;

      qnLami = qnL;
      qnTurb = qnR;
    }
    else
    {
      const Tf Hk_Lami = thicknessesCoefficientsFunctorR.getHk();
      const Tf ctau_eqLami = thicknessesCoefficientsFunctorR.getctaueq();

      ctau_TurbInit =IBLMiscClosures::getctauTurbInit_XFOIL(Hk_Lami, ctau_eqLami);
      ctauTurb = ctauL;

      qnLami = qnR;
      qnTurb = qnL;
    }


    f_lag = 0.5 * (ctau_TurbInit*qnLami + ctauTurb*qnTurb)
          + 0.5 * alpha * (ctau_TurbInit - ctauTurb);
#else

    if (profileCatL == ProfileCategory::LaminarBL)
    {
      const Tf ctaueq_Lami = thicknessesCoefficientsFunctorL.getctaueq();
      const Tf Hk_Lami = thicknessesCoefficientsFunctorL.getHk();
      const Tf ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_Lami, ctaueq_Lami);
      f_lag = ctau_TurbInit*qnL;
    }
    else
    {
      const Tf ctaueq_Lami = thicknessesCoefficientsFunctorR.getctaueq();
      const Tf Hk_Lami = thicknessesCoefficientsFunctorR.getHk();
      const Tf ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_Lami, ctaueq_Lami);
      f_lag = ctau_TurbInit*qnR;
    }

#endif
  }
  else if ((qnL > 0 && profileCatR == ProfileCategory::LaminarBL && profileCatL == ProfileCategory::TurbulentBL) ||
           (qnL < 0 && profileCatL == ProfileCategory::LaminarBL && profileCatR == ProfileCategory::TurbulentBL))
  { // interface coinciding with the transition front: flow from turbulent into laminar
//    SANS_DEVELOPER_EXCEPTION("Not totally sure if this is necessary yet. Worth keeping an eye on it");
    //TODO: this case is currently underserved
    if (profileCatL == ProfileCategory::LaminarBL)
      f_lag = ctauR*qnR;
    else
      f_lag = ctauL*qnL;
  }
#if IsExplicitCheckingIBL
  else
    SANS_DEVELOPER_EXCEPTION("profileCatL and profileR combination is not allowed!");
#endif

#endif

  fL[ir_momLami] += (1.0-gamL)*f_mom;
  fR[ir_momLami] += (1.0-gamR)*f_mom;

  fL[ir_keLami] += (1.0-gamL)*f_ke;
  fR[ir_keLami] += (1.0-gamR)*f_ke;

  fL[ir_momTurb] += gamL*f_mom;
  fR[ir_momTurb] += gamR*f_mom;

  fL[ir_keTurb] += gamL*f_ke;
  fR[ir_keTurb] += gamR*f_ke;

  Tf f_amp = 0.0;
#if defined(NOAMPEQN_IBL)
  fL[ir_amp] += 0.0*f_amp;
  fR[ir_amp] += 0.0*f_amp;
#else

#if ISIBLLFFLUX_PDEIBL // Lax-Friedrichs flux
  f_amp = 0.5 * (ntL*qnL + ntR*qnR) + 0.5 * alpha * (ntL - ntR);
#else
  // TODO: nt flux may need to be always upwinded; to avoid contaminating laminar region by turbulent results
  if (qnL > 0)
    f_amp = ntL*qnL;
  else
    f_amp = ntR*qnR;
#endif

  fL[ir_amp] += f_amp;
  fR[ir_amp] += f_amp;
#endif

#if defined(NOLAGEQN_IBL)
    fL[ir_lag] += 0.0;
    fR[ir_lag] += 0.0;
#else
    fL[ir_lag] += gamL*f_lag;
    fR[ir_lag] += gamR*f_lag;
#endif

}

//----------------------------------------------------------------------------//
// solution-dependent source: S(X, Q, QX)
template<class VarType>
template <class T, class Tg, class Tp, class Tout>
inline void
PDEIBL<PhysD2,VarType>::source(
    const ArrayParam<Tp>& params,
    const VectorX<Real>& e1,
    const VectorX<Real>& e1_x, const VectorX<Real>& e1_z,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<T>& var,
    const ArrayQ<Tg>& varx, const ArrayQ<Tg>& varz,
    ArrayQ<Tout>& source) const
{
  typedef typename ThicknessesCoefficientsFunctor<Tp, T>::Tout Tpi;

  const ProfileCategory profileCat = getProfileCategory(var,x);

  const VectorX<Tp> q1 = paramInterpret_.getq1(params);
  const TensorX<Tp> gradsq1 = paramInterpret_.getgradq1(params);

  // derived variables
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Tp mue = viscosityModel_.dynamicViscosity();
  const Tp nue = mue/rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  const Tpi delta1s = thicknessesCoefficientsFunctor.getdelta1s();
  const Tpi theta11 = thicknessesCoefficientsFunctor.gettheta11();
  const Tpi delta1p = thicknessesCoefficientsFunctor.getdelta1p();
  const Tpi RetCf1 = thicknessesCoefficientsFunctor.getRetCf1();
  const Tpi RetCDiss = thicknessesCoefficientsFunctor.getRetCD();

  // COMPUTE DEFECT INTEGRALS AND SURFACE GRADIENTS ---------------------------------------
  // P is symmetric in 2D IBL. Note "*" in () is an outer product
  const TensorX<Tout> P = rhoe*theta11*(q1*Transpose(q1)); // [tensor] P: momentum defect flux

  // (P  dot  nablas)  dot  e1 (as in IBL3) or trace(P^T  (nablas e1)) as in Shun's Master thesis
  const Tout Pdotnablasdote1 = P(0,0)*e1_x[0] + P(1,0)*e1_x[1]
                             + P(0,1)*e1_z[0] + P(1,1)*e1_z[1];

  const VectorX<Tout> tauw = (0.5*mue/theta11*RetCf1)*q1; // tau_w: wall shear stress

  const VectorX<Tout> M = rhoe*delta1s*q1; // mass flux defect
  const VectorX<Tout> Q = delta1p*q1; // Q: velocity defect
  const VectorX<Tout> D = M - rhoe*Q; // D: density defect flux

#if IsExplicitCheckingIBL
  if (dot(D,D) > 100*std::numeric_limits<Real>::epsilon())
    SANS_DEVELOPER_EXCEPTION("D should be zero for incompressible flow.");
#endif

  VectorX<Tp> gradsqsq = 2*Transpose(gradsq1)*q1; // [vector] nablas(q^2): surface gradient of q^2
  // note that nablas(q^2) = nablas(q1 . q1) = 2 nablasq1^T . q1 = 2 q1 . nablasq1

  const Tout Diss = mue*pow(qe,2.0)*RetCDiss/theta11; // {\cal D}: dissipation integral

  const Tp divq1 = paramInterpret_.getdivq1(params);

  const Tpi Hk = thicknessesCoefficientsFunctor.getHk(); // kinematic shape factor

  // laminar and turbulent source terms
  if (profileCat == ProfileCategory::LaminarBL ||
      profileCat == ProfileCategory::LaminarWake)
  {
    source[ir_momLami] += -Pdotnablasdote1 + dot(e1, gradsq1*M) - dot(tauw,e1); // source corresponding to R^e in momentum residual
    source[ir_keLami]  += dot(D, gradsqsq) - 2*Diss; // source corresponding to R^* in kinetic energy residual

    source[ir_momTurb] += varInterpret_.dummySource_momTurb(var);
    source[ir_keTurb]  += varInterpret_.dummySource_keTurb(var);

    source[ir_lag]     += varInterpret_.dummySource_lag(var);
  }
  else if (profileCat == ProfileCategory::TurbulentBL ||
           profileCat == ProfileCategory::TurbulentWake)
  {
    source[ir_momLami] += varInterpret_.dummySource_momLami(var);
    source[ir_keLami]  += varInterpret_.dummySource_keLami(var);

    source[ir_momTurb] += -Pdotnablasdote1 + dot(e1, gradsq1*M) - dot(tauw,e1); // source corresponding to R^e in momentum residual
    source[ir_keTurb]  += dot(D, gradsqsq) - 2*Diss; // source corresponding to R^* in kinetic energy residual

#if defined(NOLAGEQN_IBL)
    source[ir_lag]     += varInterpret_.dummySource_lag(var);
#else
    // -- lag equation source terms --
    const Tout ctau_eq = thicknessesCoefficientsFunctor.getctaueq();
    const Tout delta_nom = IBLMiscClosures::getdeltaNominal(Hk, delta1s, theta11);

    // dissipation length ratio wall/wake (XFOIL v6.99)
    const Real ALD = (profileCat == ProfileCategory::TurbulentWake) ? 0.9 : 1.0;

    const Real Gacon = thicknessesCoefficientsFunctor.getA_GbetaLocus();
    const Real Gbcon = thicknessesCoefficientsFunctor.getB_GbetaLocus();
    const Real duxcon = 1.0;

#if IS_LAGFACTOR_XFOILPAPER_NOT_XFOILV699_IBLTwoField
    const Tout scc = 5.6;
    const Tout HR = (Hk-1.0)/(Gacon*ALD*Hk);
#else
    const Tpi H = thicknessesCoefficientsFunctor.getH();
    const Tpi Hs = thicknessesCoefficientsFunctor.getHs();
    const Tout scc = 5.6*1.333/(1.0+thicknessesCoefficientsFunctor.getUslip(Hk, H, Hs));

    const Tpi Ret = qe*theta11/nue;

    Tout Hkc = 0.0;
    if (profileCat == IBLProfileCategory::TurbulentBL)
    {
      Hkc = Hk - 1.0 - 18.0/Ret;
      if (Hkc < 0.01)
        Hkc = 0.01; //TODO: to be smoothened
    }
    else
      Hkc = Hk - 1.0;

    const Tout HR = Hkc/(Gacon*ALD*Hk);
#endif

    const T ctau = varInterpret_.getctau(var);

    // XFOIL, xblsys.f, BLDIF, ~L1783
#if IS_USING_GLAG_INSTEADOF_CTAULAG
    Tout Slag_turb = - 0.5*qe/delta_nom * scc*(pow(ctau_eq,0.5) - pow(ctau,0.5)*ALD)
                     - duxcon*(0.5*RetCf1*nue/theta11 - qe*pow(HR,2.0)) / (Gbcon*delta1s)
                     + duxcon*divq1;

    const T G = varInterpret_.getG(var);
    source[ir_lag] += -G*divq1 + Slag_turb;
#else
    Tout Slag_turb = 2.0*ctau*(- 0.5*qe/delta_nom * scc*(pow(ctau_eq,0.5) - pow(ctau,0.5)*ALD)
                               - duxcon*(0.5*RetCf1*nue/theta11 - qe*pow(HR,2.0)) / (Gbcon*delta1s)
                               + duxcon*divq1);

    source[ir_lag] += -ctau*divq1 + Slag_turb;
#endif

#endif
  }
#if IsExplicitCheckingIBL
  else
    SANS_DEVELOPER_EXCEPTION("profileCat is not recognizable!");
#endif

  // -- amplification factor source terms --
  const T nt = varInterpret_.getnt(var);

  if (profileCat == ProfileCategory::LaminarBL)
  {
    const Tpi Ret = qe*theta11/nue;

    // source term intended to impose the inflow (at stagnation point) boundary condition on n_tilde
    // TODO: this correction along with the diffusion time scale works pretty well with ninit_=0 but a bit worse when ninit is nonzero
#if 1
    Tout t_ref = pow(theta11,2.0)/nue; // reference time scale: diffusion time scale: small near stagnation point and larger as BL thickens
#else // TODO: hack for old pyrite checks
    Tout t_ref = pow(varInterpret_.getdelta(profileCat,var),2.0)/nue; // TODO: only works when delta is a primary unknown;  to be removed
#endif
    Tout q_ref = 1.0; // TODO: hard-coded for now
    // cut off inflow-BC source after amplification factor growth source term starts to ramp up
    const Tout Samp_infowbc = (1.0 - IBLMiscClosures::AmplificationClosure_IBL::ampGrowthOnsetRamp(Hk,Ret))
        *1.0/t_ref * exp(-pow(qe/q_ref,2.0)) * (nt-transitionModel_.ntinit()); // TODO: the sign is currently positive; to be examined

    const Tout amprate = IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRate(Hk, Ret, theta11);

#if IsExplicitCheckingIBL
    if (amprate < 0.0)
      SANS_DEVELOPER_EXCEPTION("We would like amp growth to stay non-negative so that nt never decreases");
#endif

#if defined(NOAMPEQN_IBL)
    source[ir_amp] += varInterpret_.dummySource_amp(var) + 0.0*amprate + 0.0*Samp_infowbc;
#else
    source[ir_amp] += -nt*divq1 - qe*amprate + Samp_infowbc;// source terms in amplification factor residual
#endif
  }
  else
    source[ir_amp] += -nt*divq1;
}

//----------------------------------------------------------------------------//
#if IsmitLGConservationTransitionIBL
// mit-LG formulation of the matching conditions at transition front
// Strong imposition of matching condition at turbulent sub-element inlet
//
// or just plain conservation of PDE flux
template<class VarType>
template <class Tm, class T, class Tp, class Tout>
inline void
PDEIBL<PhysD2,VarType>::
cutCellTransitionFluxAndSource(
    const ArrayParam<Tp>& params,
    const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<Tm>& varMatch, const ArrayQ<T>& var,
    const Real& nx, const Real& nz,
    ArrayQ<Tout>& f, ArrayQ<Tout>& sourceMatching) const
{
  ProfileCategory profileCatL = ProfileCategory::LaminarBL;
  ProfileCategory profileCatT = ProfileCategory::TurbulentBL;

  // advective fluxes at the transition front

  T deltaL = varInterpret_.getdeltaLami(var);
  T AL = varInterpret_.getALami(var);
  T ctauL = ctauLami_dummy_;

  const VectorX<Tp> q1 = paramInterpret_.getq1(params); // velocity vector q_1, defined as q_1 = q_e

  // derived variables
  const Tp qe = paramInterpret_.getqe(params);      // q_e: edge speed. Equal to magnitude of q_1.
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));

  Tp mue = viscosityModel_.dynamicViscosity();

  Tout RedeltaL = rhoe*qe*deltaL/mue;

  SecondaryVarFunctor<T,Tout> thicknessesCoefficientsFunctorL
  = secondaryVarObj_.secondaryVarCalc(profileCatL, deltaL, AL, ctauL, RedeltaL);

  Tout theta11_L = thicknessesCoefficientsFunctorL.gettheta11();
  Tout theta1s_L = thicknessesCoefficientsFunctorL.gettheta1s();

  // P happens to be symmetric in 2D IBL. Note "*" in () is an outer product
  TensorX<Tout> P_L = rhoe*theta11_L*(q1*Transpose(q1)); // [tensor] P: momentum defect flux
  VectorX<Tout> PTdote_L = Transpose(P_L)*e1; // P^T dot e

  VectorX<Tout> K_L = rhoe*pow(qe,2)*theta1s_L*q1; // [vector] K: kinetic energy defect flux

  VectorX<Real> nrm = {nx, nz}; // trace normal vector

  f[ir_momLami] += dot(PTdote_L,nrm);
  f[ir_keLami] += dot(K_L, nrm);

  //  f[ir_amp] += 0.0; // do nothing

#if 1 // with explicit mass conservation at transition front
  f[ir_momTurb] += varMatch[ir_momTurb];
  f[ir_keTurb] += varMatch[ir_keTurb];
  f[ir_lag] += varMatch[ir_lag];

  // source in the matching conditions

  T deltaT = varInterpret_.getdeltaTurb(var);
  T AT = varInterpret_.getATurb(var);

  T ctauT = varInterpret_.getctau(var);

  Tout RedeltaT = rhoe*qe*deltaT/mue;

  SecondaryVarFunctor<T,Tout> thicknessesCoefficientsFunctorT
  = secondaryVarObj_.secondaryVarCalc(profileCatT, deltaT, AT, ctauT, RedeltaT);

  // matching conditions corresponding to the momentum and kinetic energy equation
  Tout delta1s_L = thicknessesCoefficientsFunctorL.getdelta1s();

  Tout delta1s_T = thicknessesCoefficientsFunctorT.getdelta1s();
  Tout theta11_T = thicknessesCoefficientsFunctorT.gettheta11();

  Tout ctaueq_L = thicknessesCoefficientsFunctorL.getctaueq();
  Tout Hk_L = thicknessesCoefficientsFunctorL.getHk();
  const Tout ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_Lami, ctaueq_L);

  sourceMatching[ir_momLami] += varMatch[ir_momLami] - varMatchDummy_;
  sourceMatching[ir_keLami] += varMatch[ir_keLami] - varMatchDummy_;
  sourceMatching[ir_momTurb] += delta1s_L - delta1s_T;
  sourceMatching[ir_keTurb] += theta11_L - theta11_T;
  sourceMatching[ir_amp] += varMatch[ir_amp] - varMatchDummy_;
  sourceMatching[ir_lag] += ctauT - ctau_TurbInit;

#else // no explicit mass conservation at transition front
  // the same as laminar sub-element outflux
  f[ir_momTurb] += dot(PTdote_L,nrm);
  f[ir_keTurb] += dot(K_L, nrm);

  // source in the matching conditions

  // matching conditions corresponding to the momentum and kinetic energy equation
  Tout delta1s_L = thicknessesCoefficientsFunctorL.getdelta1s();

  // remaining advective flux terms: in shear stress transport residual

#if defined(NOLAGEQN_IBL)
  fx[ir_lag] += 0.0;
  fz[ir_lag] += 0.0;
#else

#if IsCtauTinitXfoil
  // matching conditions corresponding to the momentum and kinetic energy equation
  Tout ctau_eq_L = thicknessesCoefficientsFunctorL.getctaueq();
  Tout Hk_L = thicknessesCoefficientsFunctorL.getHk();
  const Tout ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_L, ctaueq_L);
#else
  const Real ctau_TurbInit = 1e-4;
#endif

  f[ir_lag] += ctau_TurbInit * dot(q1, nrm);

#endif

  // all the matching variables are dummy
  for (int n = 0; n < N; ++n)
    sourceMatching[n] += varMatch[n] - varMatchDummy_;

#endif
}

#elif 0

// plain consistent flux with interface upwinding
template<class VarType>
template <class Tm, class T, class Tp, class Tout>
inline void
PDEIBL<PhysD2,VarType>::
cutCellTransitionFluxAndSource(
    const ArrayParam<Tp>& params,
    const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<Tm>& varMatch, const ArrayQ<T>& var,
    const Real& nx, const Real& nz,
    ArrayQ<Tout>& f, ArrayQ<Tout>& sourceMatching) const
{
  // The transitional element K is decomposed into three components as follows
  //   L      R
  // -----> ----->
  // L: laminar sub-element
  // R: turbulent sub-element

  // f contains the outflux from L, and the influx to R
  // sourceMatch defines the matching conditions at T, i.e. sourceMatch = 0
  // varMatch is the additional variable that is introduced on T to facilitate the imposition of matching conditions

  ProfileCategory profileCatL = ProfileCategory::LaminarBL;
  ProfileCategory profileCatR = ProfileCategory::TurbulentBL;

  // advective fluxes at the transition front

  T deltaL = varInterpret_.getdeltaLami(var);
  T deltaR = varInterpret_.getdeltaTurb(var);

  T AL = varInterpret_.getALami(var);
  T AR = varInterpret_.getATurb(var);

  T ctauL = ctauLami_dummy_;
  T ctauR = varInterpret_.getctau(var);

  const VectorX<Tp> q1 = paramInterpret_.getq1(params); // velocity vector q_1, defined as q_1 = q_e

  const Tp qe = paramInterpret_.getqe(params);      // q_e: edge speed. Equal to magnitude of q_1.
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params));      // rho_e: edge static density
  const Tp mue = viscosityModel_.dynamicViscosity();

  Tout RedeltaL = rhoe*qe*deltaL/mue;
  Tout RedeltaR = rhoe*qe*deltaR/mue;

  const SecondaryVarFunctor<T, Tout> thicknessesCoefficientsFunctorL
    = secondaryVarObj_.secondaryVarCalc(profileCatL, deltaL, AL, ctauL, RedeltaL);
  const SecondaryVarFunctor<T, Tout> thicknessesCoefficientsFunctorR
    = secondaryVarObj_.secondaryVarCalc(profileCatR, deltaR, AR, ctauR, RedeltaR);

  Tout theta11_L = thicknessesCoefficientsFunctorL.gettheta11();
  Tout theta11_R = thicknessesCoefficientsFunctorR.gettheta11();

  Tout theta1s_L = thicknessesCoefficientsFunctorL.gettheta1s();
  Tout theta1s_R = thicknessesCoefficientsFunctorR.gettheta1s();

  // P happens to be symmetric in 2D IBL. Note "*" in () is an outer product
  TensorX<Tout> P_L = rhoe*theta11_L*(q1*Transpose(q1)); // [tensor] P: momentum defect flux
  TensorX<Tout> P_R = rhoe*theta11_R*(q1*Transpose(q1)); // [tensor] P: momentum defect flux

  VectorX<Tout> PTdote_L = Transpose(P_L)*e1; // P^T dot e
  VectorX<Tout> PTdote_R = Transpose(P_R)*e1; // P^T dot e

  VectorX<Tout> K_L = rhoe*pow(qe,2)*theta1s_L*q1; // [vector] K: kinetic energy defect flux
  VectorX<Tout> K_R = rhoe*pow(qe,2)*theta1s_R*q1; // [vector] K: kinetic energy defect flux

  const VectorX<Real> nrm = {nx, nz}; // trace normal vector
  const Tp q1n = dot(q1, nrm);

#if ISIBLLFFLUX_PDEIBL
  Tout delta1s_L  = thicknessesCoefficientsFunctorL.getdelta1s();
  Tout delta1s_R  = thicknessesCoefficientsFunctorR.getdelta1s();

#if 1 // TODO: just for testing
  std::cout << std::endl;
  std::cout << "delta* L = " << delta1s_L << ", delta* R = " << delta1s_R << std::endl;
  std::cout << "theta  L = " << theta11_L << ", theta  R = " << theta11_R << std::endl;
  std::cout << "theta* L = " << theta1s_L << ", theta* R = " << theta1s_R << std::endl;
  std::cout << std::endl;
#endif

  Tout deltarho_L = thicknessesCoefficientsFunctorL.getdeltarho();
  Tout deltarho_R = thicknessesCoefficientsFunctorR.getdeltarho();

  Tout theta0s_L   = thicknessesCoefficientsFunctorL.gettheta0s();
  Tout theta0s_R   = thicknessesCoefficientsFunctorR.gettheta0s();

  VectorX<Tout> p_L = rhoe*(delta1s_L-deltarho_L)*q1;
  VectorX<Tout> p_R = rhoe*(delta1s_R-deltarho_R)*q1;

  Tout k_L = rhoe*pow(qe,2)*theta0s_L; // k: kinetic energy defect
  Tout k_R = rhoe*pow(qe,2)*theta0s_R; // k: kinetic energy defect

#if 1 // TODO: this is okay for 2D IBL
  Tp alpha = qe;  // dissipation coefficient
#else // TODO: this should be used for 3D IBL
  Tp alpha = std::max(fabs(dot(q1L,nrm)), fabs(dot(q1R,nrm)));  // dissipation coefficient
#endif

  Tout f_mom = 0.5 * dot(PTdote_L + PTdote_R, nrm) + 0.5 * alpha * dot(p_L-p_R, e1);
  Tout f_ke  = 0.5 * dot(K_L + K_R, nrm) + 0.5 * alpha * (k_L - k_R);

#if IsCtauTinitXfoil
  Tout ctaueq_L = thicknessesCoefficientsFunctorL.getctaueq();
  Tout Hk_L = thicknessesCoefficientsFunctorL.getHk();
  const Tout ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_L, ctaueq_L);
#else
  const Real ctau_TurbInit = 1e-4;
#endif

  Tout f_lag = 0.5 * (ctau_TurbInit + ctauR) * dot(q1, nrm) + 0.5 * alpha * (ctau_TurbInit - ctauR);
#else

  // Assume q1L and nrm are not orthogonal.
  if (q1n > 0) // when flow goes from laminar to turbulent
  {
    const Tout f_mom = dot(PTdote_L, nrm);
    const Tout f_ke = dot(K_L, nrm);

    f[ir_momLami] += f_mom; // flux out of laminar sub-element
    f[ir_keLami] += f_ke;

    f[ir_momTurb] += f_mom;
    f[ir_keTurb] += f_ke;

#if IsCtauTinitXfoil
    Tout ctaueq_L = thicknessesCoefficientsFunctorL.getctaueq();
    Tout Hk_L = thicknessesCoefficientsFunctorL.getHk();
    const Tout ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_L, ctaueq_L);
#else
    const Real ctau_TurbInit = 1e-4;
#endif

    f[ir_lag] += ctau_TurbInit*q1n;
  }
  else if (q1n < 0) // relaminarization: flow goes from turbulent to laminar
  {
//    std::cout << std::endl;
//    std::cout << "Relaminarization at (x,z) = " << x << ", " << z << std::endl;
//    std::cout << "q1  = " << q1 << std::endl;
//    std::cout << "nrm = " << nrm << std::endl;
//    std::cout << std::endl;
    //    SANS_DEVELOPER_EXCEPTION("Flow must be from laminar to turbulent at transition front!");

    const Tout f_mom = dot(PTdote_R, nrm);
    const Tout f_ke = dot(K_R, nrm);

    f[ir_momLami] += f_mom; // flux into laminar sub-element
    f[ir_keLami] += f_ke;

    f[ir_momTurb] += f_mom;
    f[ir_keTurb] += f_ke;

    f[ir_lag] += ctauR*q1n;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Transition front should not be a stagnation point");

  //  f[ir_amp] += 0.0;
#endif

  // source in the matching conditions
  // sourceMatching index does not matter at present
  sourceMatching[ir_momLami] += varInterpret_.getdeltaLami(varMatch) - varMatchDummy_;
  sourceMatching[ir_keLami] += varInterpret_.getALami(varMatch) - varMatchDummy_;
  sourceMatching[ir_momTurb] += varInterpret_.getdeltaTurb(varMatch) - varMatchDummy_;
  sourceMatching[ir_keTurb] += varInterpret_.getATurb(varMatch) - varMatchDummy_;
  sourceMatching[ir_amp] += varInterpret_.getnt(varMatch) - varMatchDummy_;
  sourceMatching[ir_lag] += varInterpret_.getctau(varMatch) - varMatchDummy_;
}

#else

// mit-State formulation of matching conditions at transition front
template<class VarType>
template <class Tm, class T, class Tp, class Tout>
inline void
PDEIBL<PhysD2,VarType>::
cutCellTransitionFluxAndSource(
    const ArrayParam<Tp>& params,
    const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<Tm>& varMatch, const ArrayQ<T>& var,
    const Real& nx, const Real& nz,
    ArrayQ<Tout>& f, ArrayQ<Tout>& sourceMatching) const
{
  // The transitional element K is decomposed into three components as follows
  //   L    T   R
  // -----> * ----->
  // L: laminar sub-element
  // T: turbulent transition point, coinciding with the cell cut
  // R: turbulent sub-element

  // f contains the outflux from L, and the influx to R
  // sourceMatch defines the matching conditions at T, i.e. sourceMatch = 0
  // varMatch is the additional variable that is introduced on T to facilitate the imposition of matching conditions

  ProfileCategory profileCatL = ProfileCategory::LaminarBL;
  ProfileCategory profileCatT = ProfileCategory::TurbulentBL;
  ProfileCategory profileCatR = ProfileCategory::TurbulentBL;

  const VectorX<Tp> q1 = paramInterpret_.getq1(params); // velocity vector q_1, defined as q_1 = q_e

  const Tp qe = paramInterpret_.getqe(params); // q_e: edge speed. Equal to magnitude of q_1.
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Tp nue = viscosityModel_.dynamicViscosity()/rhoe;

  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctorL
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatL, params, var, nue);
  const ThicknessesCoefficientsFunctor<Tp, Tm> thicknessesCoefficientsFunctorT
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatT, params, varMatch, nue);
  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctorR
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCatR, params, var, nue);

  Tout theta11_L = thicknessesCoefficientsFunctorL.gettheta11();
  Tout theta1s_L = thicknessesCoefficientsFunctorL.gettheta1s();

  Tout theta11_T = thicknessesCoefficientsFunctorT.gettheta11();
  Tout theta1s_T = thicknessesCoefficientsFunctorT.gettheta1s();

  Tout theta11_R = thicknessesCoefficientsFunctorR.gettheta11();
  Tout theta1s_R = thicknessesCoefficientsFunctorR.gettheta1s();

  // P happens to be symmetric in 2D IBL. Note "*" in () is an outer product
  TensorX<Tout> P_L = rhoe*theta11_L*(q1*Transpose(q1)); // [tensor] P: momentum defect flux
  TensorX<Tout> P_T = rhoe*theta11_T*(q1*Transpose(q1)); // [tensor] P: momentum defect flux
  TensorX<Tout> P_R = rhoe*theta11_R*(q1*Transpose(q1)); // [tensor] P: momentum defect flux

  VectorX<Tout> PTdote_L = Transpose(P_L)*e1; // P^T dot e
  VectorX<Tout> PTdote_T = Transpose(P_T)*e1; // P^T dot e
  VectorX<Tout> PTdote_R = Transpose(P_R)*e1; // P^T dot e

  VectorX<Tout> K_L = rhoe*pow(qe,2)*theta1s_L*q1; // [vector] K: kinetic energy defect flux
  VectorX<Tout> K_T = rhoe*pow(qe,2)*theta1s_T*q1; // [vector] K: kinetic energy defect flux
  VectorX<Tout> K_R = rhoe*pow(qe,2)*theta1s_R*q1; // [vector] K: kinetic energy defect flux

  const VectorX<Real> nrm = {nx, nz}; // trace normal vector
  const Tp q1n = dot(q1, nrm);

#if ISIBLLFFLUX_PDEIBL // LF numerical flux into sub-element R
  SANS_ASSERT_MSG(q1n > 0, "currently does not handle relaminarization"); //TODO

  // flux out of element L
  Tout delta1s_L = thicknessesCoefficientsFunctorL.getdelta1s();
  Tout delta1s_T = thicknessesCoefficientsFunctorT.getdelta1s();
  Tout delta1s_R = thicknessesCoefficientsFunctorR.getdelta1s();

  Tout deltarho_L = thicknessesCoefficientsFunctorL.getdeltarho();
  Tout deltarho_T = thicknessesCoefficientsFunctorT.getdeltarho();
  Tout deltarho_R = thicknessesCoefficientsFunctorR.getdeltarho();

  Tout theta0s_T   = thicknessesCoefficientsFunctorT.gettheta0s();
  Tout theta0s_R   = thicknessesCoefficientsFunctorR.gettheta0s();

  VectorX<Tout> p_L = rhoe*(delta1s_L-deltarho_L)*q1;
  VectorX<Tout> p_T = rhoe*(delta1s_T-deltarho_T)*q1;
  VectorX<Tout> p_R = rhoe*(delta1s_R-deltarho_R)*q1;

  Tout k_T = rhoe*pow(qe,2)*theta0s_T; // k: kinetic energy defect
  Tout k_R = rhoe*pow(qe,2)*theta0s_R; // k: kinetic energy defect

  Tp alpha = qe;  // dissipation coefficient

#if 0
  Tout theta0s_L   = thicknessesCoefficientsFunctorL.gettheta0s();

  Tout k_L = rhoe*pow(qe,2)*theta0s_L; // k: kinetic energy defect

  // TODO this contaminates laminar with turbulent stuff and leads to oscillatory H.  To be examined further. For now, just use full upwinding
  f[ir_momLami] += 0.5 * dot(PTdote_L + PTdote_T, nrm) + 0.5 * alpha * dot(p_L-p_T, e1);
  f[ir_keLami] += 0.5 * dot(K_L + K_T, nrm) + 0.5 * alpha * (k_L - k_T);
#else
  f[ir_momLami] += dot(PTdote_L,nrm); // flux out of laminar sub-element
  f[ir_keLami] += dot(K_L, nrm);
#endif

  f[ir_momTurb] += 0.5 * dot(PTdote_T + PTdote_R, nrm) + 0.5 * alpha * dot(p_T-p_R, e1);
  f[ir_keTurb] += 0.5 * dot(K_T + K_R, nrm) + 0.5 * alpha * (k_T - k_R);

  sourceMatching[ir_momTurb] += delta1s_T - delta1s_L;
  sourceMatching[ir_keTurb] += theta11_T - theta11_L;

#if IsCtauTinitXfoil
#if IsCtauTinitBasedOnLamiOutflow // based on variables at laminar outflow boundary
    const Tout ctau_eq_L = thicknessesCoefficientsFunctorL.getctaueq();
    const Tout Hk_L = thicknessesCoefficientsFunctorL.getHk();
    const Tout ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_L, ctau_eq_L);
#else // based on variables at turbulent inflow boundary
    const Tout ctau_eq_R = thicknessesCoefficientsFunctorR.getctaueq();
    const Tout Hk_R = thicknessesCoefficientsFunctorR.getHk();
    const Tout ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_R, ctau_eq_R);
#endif
#else
  const Real ctau_TurbInit = 1e-4;
#endif

  Tm ctauT = varInterpret_.getctau(varMatch);
  sourceMatching[ir_lag] += ctauT - ctau_TurbInit;

#if IS_USING_GLAG_INSTEADOF_CTAULAG
  Tm GT = varInterpret_.getG(varMatch);
  T GR = varInterpret_.getG(var);
  f[ir_lag] += 0.5 * (GT + GR) * q1n + 0.5 * alpha * (GT - GR);
#else
  T ctauR = varInterpret_.getctau(var);
  f[ir_lag] += 0.5 * (ctauT + ctauR) * q1n + 0.5 * alpha * (ctauT - ctauR);
#endif

#else // fully upwinded flux into sub-element R
  Tout delta1s_T = thicknessesCoefficientsFunctorT.getdelta1s();
  const Tm ctauT = varInterpret_.getctau(varMatch);
  const T ctauR = varInterpret_.getctau(var);

  if (q1n > 0) // when flow goes from laminar to turbulent
  {
    // matching conditions corresponding to the momentum and kinetic energy equation
    f[ir_momLami] += dot(PTdote_L,nrm); // flux out of laminar sub-element
    f[ir_keLami] += dot(K_L, nrm);

    f[ir_momTurb] += dot(PTdote_T, nrm);
    f[ir_keTurb] += dot(K_T, nrm);

    Tout delta1s_L = thicknessesCoefficientsFunctorL.getdelta1s();

    // sourceMatching index does not matter at present
    sourceMatching[ir_momTurb] += delta1s_T - delta1s_L;
    sourceMatching[ir_keTurb] += theta11_T - theta11_L;

#if IsCtauTinitXfoil
#if IsCtauTinitBasedOnLamiOutflow // based on variables at laminar outflow boundary
    const Tout ctau_eq_L = thicknessesCoefficientsFunctorL.getctaueq();
    const Tout Hk_L = thicknessesCoefficientsFunctorL.getHk();
    const Tout ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_L, ctau_eq_L);
#else // based on variables at turbulent inflow boundary
    const Tout ctau_eq_R = thicknessesCoefficientsFunctorR.getctaueq();
    const Tout Hk_R = thicknessesCoefficientsFunctorR.getHk();
    const Tout ctau_TurbInit = IBLMiscClosures::getctauTurbInit_XFOIL(Hk_R, ctau_eq_R);
#endif
#else
    const Real ctau_TurbInit = 1e-4;
#endif

    f[ir_lag] += ctauT * q1n;
    sourceMatching[ir_lag] += ctauT - ctau_TurbInit;
  }
  else if (q1n < 0) // relaminarization: flow goes from turbulent to laminar
  {
    //TODO: worth rethinking how to do relaminarization
//    std::cout << std::endl;
//    std::cout << "Relaminarization at (x,z) = " << x << ", " << z << std::endl;
//    std::cout << "q1  = " << q1 << std::endl;
//    std::cout << "nrm = " << nrm << std::endl;
//    std::cout << std::endl;
    //    SANS_DEVELOPER_EXCEPTION("Flow must be from laminar to turbulent at transition front!");

    f[ir_momLami] += dot(PTdote_T, nrm); // flux into laminar sub-element
    f[ir_keLami] += dot(K_T, nrm);

    f[ir_momTurb] += dot(PTdote_R,nrm);
    f[ir_keTurb] += dot(K_R, nrm);
    f[ir_lag] += ctauR * q1n;

    Tout delta1s_R = thicknessesCoefficientsFunctorR.getdelta1s();

    sourceMatching[ir_momTurb] += delta1s_T - delta1s_R;
    sourceMatching[ir_keTurb] += theta11_T - theta11_R;
//    sourceMatching[ir_lag] += ctauT - ctauR;
    sourceMatching[ir_lag] += ctauT - varMatchDummy_;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Transition front should not be a stagnation point");

  f[ir_amp] += 0.0; // do nothing
#endif

  // source in the matching conditions //TODO: the following explicit indexing is dangerous!!! to be fixed later
  sourceMatching[ir_momLami] += varInterpret_.dummySource_momLami(varMatch);
  sourceMatching[ir_keLami] += varInterpret_.dummySource_keLami(varMatch);
  sourceMatching[ir_amp] += varInterpret_.dummySource_amp(varMatch);
//
//  sourceMatching[ir_momLami] += varMatch[0] - varMatchDummy_;
//  sourceMatching[ir_keLami] += varMatch[1] - varMatchDummy_;
//  sourceMatching[ir_amp] += varInterpret_.getnt(varMatch) - varMatchDummy_;

#if 0 // TODO: just for testing
  std::cout << std::endl;
  std::cout << "delta* L = " << delta1s_L << ", delta* R = " << delta1s_R << std::endl;
  std::cout << "theta  L = " << theta11_L << ", theta  R = " << theta11_R << std::endl;
  std::cout << "theta* L = " << theta1s_L << ", theta* R = " << theta1s_R << std::endl;
  std::cout << "H L = " << delta1s_L/theta11_L << ", H R = " << delta1s_R/theta11_R << std::endl;
  std::cout << "H L - H R = " << delta1s_L/theta11_L - delta1s_R/theta11_R << std::endl;
  std::cout << std::endl;
#endif
}

#endif

//----------------------------------------------------------------------------//
// dummy equation (cast in source terms) for the matching variable in non-transition elements
template<class VarType>
template <class Tm, class Tout>
inline void
PDEIBL<PhysD2,VarType>::sourceMatchNonTransitionDummy(
    const ArrayQ<Tm>& varMatch,
    ArrayQ<Tout>& sourceMatching) const
{
//  for (int n  = 0; n < N; ++n)
//    sourceMatching[n] += varMatch[n] - varMatchDummy_;
  sourceMatching[ir_momLami] += varInterpret_.dummySource_momLami(varMatch);
  sourceMatching[ir_keLami] += varInterpret_.dummySource_keLami(varMatch);
  sourceMatching[ir_momTurb] += varInterpret_.dummySource_momTurb(varMatch);
  sourceMatching[ir_keTurb] += varInterpret_.dummySource_keTurb(varMatch);
  sourceMatching[ir_amp] += varInterpret_.dummySource_amp(varMatch);
  sourceMatching[ir_lag] += varInterpret_.dummySource_lag(varMatch);
}

//----------------------------------------------------------------------------//
// solution-dependent source: S(X, Q, QX)
template<class VarType>
template <class T, class Tp, class Tout>
inline void
PDEIBL<PhysD2,VarType>::sourceTurbAmp(
    const ArrayParam<Tp>& params,
    const VectorX<Real>& e1,
    const Real& x, const Real& z, const Real& time,
    const ArrayQ<T>& var,
    Tout& source_turbAmp) const
{
  const ProfileCategory profileCat = ProfileCategory::LaminarBL;

  // derived variables
  const Tp qe = paramInterpret_.getqe(params);
  const Tp rhoe = gasModel_.density(qe, paramInterpret_.getp0(params), paramInterpret_.getT0(params)); // rho_e: edge static density
  const Tp nue = viscosityModel_.dynamicViscosity() / rhoe;

  // COMPUTE SECONDARY VARIABLES -----------------------------
  const ThicknessesCoefficientsFunctor<Tp, T> thicknessesCoefficientsFunctor
    = thicknessesCoefficientsObj_.getCalculatorThicknessesCoefficients(profileCat, params, var, nue);

  Tout theta11 = thicknessesCoefficientsFunctor.gettheta11();

  const Tout Hk = thicknessesCoefficientsFunctor.getHk(); // kinematic shape factor
  const Tout Ret = qe*theta11/nue;

  source_turbAmp += -qe*IBLMiscClosures::AmplificationClosure_IBL::ampGrowthRate(Hk, Ret, theta11);
}


//----------------------------------------------------------------------------//
// Pseudo transient continuation (PTC) uses this function
template <class VarType>
template <class T, class Tp>
void
PDEIBL<PhysD2,VarType>::
speedCharacteristic(
    const ArrayParam<Tp>& params,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& var, Tp& speed) const
{
  speed = paramInterpret_.getqe(params); // q_e: edge speed. Equal to magnitude of q_1.

#if 1
  speed = max(speed, 1.0); //TODO: hard-coded so that speed is larger than zero even at stagnation point
#endif
}

//----------------------------------------------------------------------------//
// check state validity
template <class VarType>
bool
PDEIBL<PhysD2,VarType>::isValidState(const ArrayQ<Real>& q) const
{
  bool isStateValid = varInterpret_.isValidState(q);

#if 0 //IsExplicitCheckingIBL
  // TODO: not sure if this requirement is necessary
  isStateValid = isStateValid && (varInterpret_.getctau(q) <= 0.0625); // The upper bound on ctau follows XFOIL v6.99, xbl.f, ~Line 1520

  SANS_ASSERT_MSG(varInterpret_.getctau(q) <= 0.0625, "Check if violation ever happens");

  if (!(varInterpret_.getctau(q) <= 0.0625))
  {
    //
  }
#endif

  return isStateValid;
}

template <class VarType>
template <class T, class Tp>
void PDEIBL<PhysD2,VarType>::updateFraction(
    const ArrayParam<Tp>& params,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& var, const ArrayQ<T>& dvar,
    const Real& maxChangeFraction, Real& updateFraction) const
{
  // analogous implementation can be found in XFOIL, xbl.f SUBROUTINE UPDATE
  // The purpose of this function is to ensure state validity since plain Newton line update does not check validity
  // TODO: can be extended to ensure positive delta*, theta etc.
  const ProfileCategory profileCat = getProfileCategory(var,x);
  varInterpret_.updateFraction(profileCat, var, dvar, maxChangeFraction, updateFraction);
}

// set from variable array
template <class VarType>
template <class T, class IBLVarData>
inline void
PDEIBL<PhysD2,VarType>::setDOFFrom(const IBLVarDataType<IBLVarData>& data,
                                    ArrayQ<T>& var) const
{
  varInterpret_.setDOFFrom(data.cast(), var);
}

// set from variable array
template <class VarType>
template <class IBLVarData>
inline typename PDEIBL<PhysD2,VarType>::template ArrayQ<Real>
PDEIBL<PhysD2,VarType>::setDOFFrom(const IBLVarDataType<IBLVarData>& data) const
{
  ArrayQ<Real> var;
  setDOFFrom(data.cast(), var);
  return var;
}

// interpret residuals of the solution variable
template <class VarType>
template<class T>
inline void
PDEIBL<PhysD2,VarType>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut) const
{
  SANS_ASSERT(rsdPDEIn.M == N);

  int nOut = rsdPDEOut.m();

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N);
    for (int i = 0; i < N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
}

// interpret residuals of the gradients in the solution variable
template <class VarType>
template<class T>
inline void
PDEIBL<PhysD2,VarType>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut) const
{
  SANS_ASSERT(rsdAuxIn.M == N);

  int nOut = rsdAuxOut.m();

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N);
    for (int i = 0; i < N; i++)
      rsdAuxOut[i] = rsdAuxIn[i];
  }
}

// interpret residuals at the boundary (should forward to BCs)
template <class VarType>
template<class T>
inline void
PDEIBL<PhysD2,VarType>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut) const
{
  SANS_ASSERT(rsdBCIn.M == N);

  int nOut = rsdBCOut.m();

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N);
    for (int i = 0; i < N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
}

// return the interp type
template <class VarType>
inline typename PDEIBL<PhysD2,VarType>::IBLResidualInterpCategory
PDEIBL<PhysD2,VarType>::category() const
{
  return rsdCat_;
}

// how many residual equations are we dealing with
template <class VarType>
inline int
PDEIBL<PhysD2,VarType>::nMonitor() const
{
  int nMon = -1;

  if (rsdCat_==IBL_ResidInterp_Raw) // raw residual
    nMon = N;

  return nMon;
}

} //namespace SANS

#endif  // PDEIBL_H
