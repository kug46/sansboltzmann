// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_JACOBIANPARAM_PROJECTTOQAUXV_QINV_H_
#define SRC_PDE_IBL_JACOBIANPARAM_PROJECTTOQAUXV_QINV_H_

#include "Discretization/JacobianParamBase.h"
#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// A class for constructing jacobian of the auxiliary viscous equations (i.e. L2 projection)
// w.r.t inviscid variables Qinv (e.g. inviscid solver may be panel method)
//
template<class ProjectToQauxvEqnClass>
class JacobianParam_ProjectToQauxv_Qinv
  : public JacobianParamBase<
      typename AlgebraicEquationSetTraits<typename ProjectToQauxvEqnClass::MatrixParam,
                                          typename ProjectToQauxvEqnClass::ArrayQ,
                                          typename ProjectToQauxvEqnClass::TraitsTag
                                         >::SystemMatrix
                            >
{
public:
  typedef typename ProjectToQauxvEqnClass::PhysDim PhysDim;
  typedef typename ProjectToQauxvEqnClass::ArrayQ ArrayQ;
  typedef typename ProjectToQauxvEqnClass::MatrixParam MatrixParam;

  typedef typename ProjectToQauxvEqnClass::TraitsTag TraitsTag;
  typedef AlgebraicEquationSetTraits<MatrixParam, ArrayQ, TraitsTag> TraitsType;

  typedef typename TraitsType::MatrixSizeClass MatrixSizeClass;

  typedef typename TraitsType::SystemMatrix SystemMatrix;
  typedef typename TraitsType::SystemNonZeroPattern SystemNonZeroPattern;

  typedef typename TraitsType::SystemMatrixView SystemMatrixView;
  typedef typename TraitsType::SystemNonZeroPatternView SystemNonZeroPatternView;

  typedef JacobianParamBase<SystemMatrix> BaseType;

  typedef typename BaseType::SystemMatrixTranspose SystemMatrixTranspose;
  typedef typename BaseType::SystemNonZeroPatternTranspose SystemNonZeroPatternTranspose;

  explicit JacobianParam_ProjectToQauxv_Qinv(const ProjectToQauxvEqnClass& ProjEqn) :
    ProjEqn_(ProjEqn) {}

  virtual ~JacobianParam_ProjectToQauxv_Qinv() {}

  //Fills jacobian or the non-zero pattern of a jacobian
  virtual void jacobian(SystemMatrixView& mtx       ) override { jacobianProjEqn_Qinv<SystemMatrixView&>(mtx); }
  virtual void jacobian(SystemNonZeroPatternView& nz) override { jacobianProjEqn_Qinv<SystemNonZeroPatternView&>(nz); }

  //Fills jacobian transpose or the non-zero pattern transpose of a jacobian transpose
  virtual void jacobian(SystemMatrixTranspose mtxT       ) override { jacobianProjEqn_Qinv(mtxT); }
  virtual void jacobian(SystemNonZeroPatternTranspose nzT) override { jacobianProjEqn_Qinv(nzT); }

protected:
  template<class SparseMatrixType>
  void jacobianProjEqn_Qinv(SparseMatrixType& jac) const { ProjEqn_.template jacobianParam<SparseMatrixType&>(jac); }

  const ProjectToQauxvEqnClass& ProjEqn_;
};

} // namespace SANS

#endif /* SRC_PDE_IBL_JacobianParam_ProjectToQauxv_Qinv_H_ */
