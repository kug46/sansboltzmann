// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PDEIBLUniField.h"

namespace SANS
{

template<class VarType>
void
PDEIBLUniField<PhysD2,VarType>::dump( int indentSize, std::ostream& out ) const
{
  gasModel_.dump(indentSize,out);
  viscosityModel_.dump(indentSize,out);
}

//----------------------------------------------------------------------------//
// Explicitly instantiations
template class PDEIBLUniField<PhysD2,VarTypeDAG>;
template class PDEIBLUniField<PhysD2,VarTypeDsThG>;
}
