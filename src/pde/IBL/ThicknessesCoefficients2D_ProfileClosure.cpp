// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define THICKNESSESCOEFFICIENTS2D_PROFILECLOSURE_INSTANTIATE
#include "ThicknessesCoefficients2D_ProfileClosure_impl.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

#include "Surreal/SurrealS.h"

namespace SANS
{
PYDICT_PARAMETER_OPTION_INSTANTIATE( typename ThicknessesCoefficientsParams2D_ProfileClosure::DissipationClosureOptions )

void ThicknessesCoefficientsParams2D_ProfileClosure::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.quadratureOrder));
  allParams.push_back(d.checkInputs(params.nameCDclosure));
  d.checkUnknownInputs(allParams);
}
ThicknessesCoefficientsParams2D_ProfileClosure ThicknessesCoefficientsParams2D_ProfileClosure::params;

// ----- Profile closure, VarTypeDANCt, IncompressibleBL ----- //

template class ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>;
template class ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::Functor<Real, Real>;

template
ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::Functor<Real, Real>
ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<Real>& var,
    const Real& nue) const;

typedef SurrealS<6> SurrealS6;
template class ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::Functor<Real, SurrealS6>;

template
ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::Functor<Real, SurrealS6>
ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, SurrealS6>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<SurrealS6>& var,
    const Real& nue) const;

template class ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::Functor<SurrealS6, SurrealS6>;

template
ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::Functor<SurrealS6, SurrealS6>
ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS6, SurrealS6>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS6>& params, const ArrayQ<SurrealS6>& var,
    const SurrealS6& nue) const;


typedef SurrealS<8> SurrealS8;
template class ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::Functor<SurrealS8, Real>;

template
ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::Functor<SurrealS8, Real>
ThicknessesCoefficients2D<ProfileClosure, VarTypeDANCt, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS8, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS8>& params, const ArrayQ<Real>& var,
    const SurrealS8& nue) const;


// ----- Profile closure, VarTypeDAG, IncompressibleBL ----- //

template class ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>;


template class ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>::Functor<Real, Real>;

template
ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>::Functor<Real, Real>
ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<Real>& var,
    const Real& nue) const;


typedef SurrealS<3> SurrealS3;
template class ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>::Functor<Real, SurrealS3>;

template
ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>::Functor<Real, SurrealS3>
ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<Real, SurrealS3>(
    const IBLProfileCategory& profileCat, const ArrayParam<Real>& params, const ArrayQ<SurrealS3>& var,
    const Real& nue) const;

typedef SurrealS<8> SurrealS8;
template class ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>::Functor<SurrealS8, Real>;

template
ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>::Functor<SurrealS8, Real>
ThicknessesCoefficients2D<ProfileClosure, VarTypeDAG, IncompressibleBL>::
getCalculatorThicknessesCoefficients<SurrealS8, Real>(
    const IBLProfileCategory& profileCat, const ArrayParam<SurrealS8>& params, const ArrayQ<Real>& var,
    const SurrealS8& nue) const;

}
