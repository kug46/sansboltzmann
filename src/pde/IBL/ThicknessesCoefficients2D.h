// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SRC_PDE_IBL_THICKNESSESCOEFFICIENTS2D_H_
#define SRC_PDE_IBL_THICKNESSESCOEFFICIENTS2D_H_

#define ISUSEXFOIL_CORRELATIONCLOSURE 1 // true for XFOIL; false for MSES paper

#include "Python/Parameter.h"

#include "Quadrature/QuadratureLine.h"

#include "IBL_Traits.h"
#include "VarInterpret2D.h"
#include "QauxvInterpret2D.h"
#include "VelocityProfile2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// forward declaration of the class that manages/computes thicknesses and coefficients
template<class ClosureType, class VarType, class CompressiblityType>
class ThicknessesCoefficients2D;

template<class VarType, class CompressibilityType>
ThicknessesCoefficients2D<typename IBLClosureType<VarType>::type, VarType, CompressibilityType>
getThicknessesCoeffcients2DObj(const VarInterpret2D<VarType>& varInterpret, const QauxvInterpret2D& qauxvInterpret, const PyDict& d)
{
  return ThicknessesCoefficients2D<typename IBLClosureType<VarType>::type, VarType, CompressibilityType>(
    varInterpret, qauxvInterpret, d);
}

//----------------------------------------------------------------------------//
struct ThicknessesCoefficientsParams2D_CorrelationClosure : noncopyable
{
  struct DissipationClosureOptions
  {
    typedef std::string ExtractType;
    const ExtractType ibl3eqm = "ibl3eqm";
    const ExtractType ibl3lag = "ibl3lag";
    const ExtractType xfoillag = "xfoillag";

    const std::vector<ExtractType> options{ibl3eqm,ibl3lag,xfoillag};
  };
  const ParameterOption<DissipationClosureOptions>
    nameCDclosure{"nameCDclosure", "xfoillag", "Name (string) of the C_Diss closure model"};

  static void checkInputs(PyDict d); // parameter validity are checked only when this function is called

  static ThicknessesCoefficientsParams2D_CorrelationClosure params;
};

template<class VarType, class CompressibilityType>
class ThicknessesCoefficients2D<CorrelationClosure, VarType, CompressibilityType>
{
public:
  typedef PhysD2 PhysDim;
  typedef CorrelationClosure ClosureType;

  typedef ThicknessesCoefficientsParams2D_CorrelationClosure ClassParamsType;

  typedef VarInterpret2D<VarType> VarInterpType;
  typedef QauxvInterpret2D ParamInterpType;

  template<class T>
  using ArrayQ = typename VarInterpType::template ArrayQ<T>;

  template<class T>
  using ArrayParam = typename ParamInterpType::ArrayQ<T>;

  ThicknessesCoefficients2D(const VarInterpType& varInterpret,
                            const ParamInterpType& paramInterpret,
                            const PyDict& d=PyDict()) :
    ThicknessesCoefficients2D(varInterpret, paramInterpret, d.get(ClassParamsType::params.nameCDclosure)) {}

  std::string nameCDclosure() const { return nameCDclosure_; }

  // Functor for computing secondary variables from primary variables
  template<class Tparam, class Tibl>
  class Functor
  {
  public:
    typedef typename promote_Surreal<Tparam, Tibl>::type Tout;

    Functor(const IBLProfileCategory& profileCat,
            const ArrayParam<Tparam>& params,
            const ArrayQ<Tibl>& var,
            const Tparam& nue,
            const VarInterpType& varInterpret,
            const ParamInterpType& paramInterpret,
            const std::string& nameCDclosure) :
      profileCat_(profileCat),
      params_(params),
      var_(var),
      nue_(nue),
      varInterpret_(varInterpret),
      paramInterpret_(paramInterpret),
      nameCDclosure_(nameCDclosure)
    {
      // compute/cache velocity profile information
      switch (profileCat_)
      {
        case IBLProfileCategory::LaminarBL:
        case IBLProfileCategory::TurbulentBL:
        case IBLProfileCategory::TurbulentWake:
        {
          break;
        }
        case IBLProfileCategory::LaminarWake:
        {
          SANS_DEVELOPER_EXCEPTION("Laminar wake is not allowed");
          break;
        }
        default:
          SANS_DEVELOPER_EXCEPTION("Unknown Profile Category = %d", profileCat_);
      }
    }

    Real getA_GbetaLocus() const { return A_GbetaLocus_; }
    Real getB_GbetaLocus() const { return B_GbetaLocus_; }

    // subordinate methods for computing each secondary variable
    Tout getdelta1s() const;
    Tout gettheta11() const;
    Tout getRetheta() const;

    Tout getH() const;
    Tout getHk() const;
    Tout getHs() const;
    void getHs_derivatives(Tout& Hs_Hk, Tout& Hs_Rt) const;
    void getHs_derivatives_Lami(const Tout& Hk, Tout& Hs_Hk) const;
    void getHs_derivatives_Turb(const Tout& Hk, const Tout& Ret, Tout& Hs_Hk, Tout& Hs_Rt) const;

    Tout gettheta1s() const;
    void gettheta1s_derivatives(Tout& theta1s_delta1s, Tout& theta1s_theta11) const;

    Tout getdelta1p() const;
    Tout getdeltarho() const;
    Tout gettheta0s() const;
    Tout getRetCf1() const;
    Tout getRetCD() const;
    Tout getctaueq() const;

    Tout getUslip(const Tout& Hk, const Tout& H, const Tout& Hs) const;
  private:
    Tout getHs_Lami(const Tout& Hk) const;
    Tout getHs_Turb(const Tout& Hk) const;

    Tout getRetCf_LamiBL(const Tout& Hk) const;
    Tout getRetCf_TurbBL(const Tout& Hk) const;

    Tout getRetCD_LamiBL(const Tout& Hk, const Tout& Hs) const;
    Tout getRetCD_LamiWake(const Tout& Hk) const;
    Tout getRetCD_Turb(const Tout& Hk, const Tout& H, const Tout& Hs) const;

    const IBLProfileCategory profileCat_;
    const ArrayParam<Tparam>& params_;
    const ArrayQ<Tibl>& var_;
    const Tparam& nue_; // nu_e = mu_e/rho_e: edge kinematic viscosity

    const VarInterpType& varInterpret_;
    const ParamInterpType& paramInterpret_;
    const std::string nameCDclosure_;

    // G-beta locus constants
    const Real A_GbetaLocus_ = 6.7;
    const Real B_GbetaLocus_ = 0.75;
  };

  // Returns functor to carry out secondary variable calculations
  template<class Tparam, class Tibl>
  Functor<Tparam, Tibl>
  getCalculatorThicknessesCoefficients(const IBLProfileCategory& profileCat, const ArrayParam<Tparam>& params,
                                       const ArrayQ<Tibl>& var, const Tparam& nue) const;

private:
  ThicknessesCoefficients2D(const VarInterpType& varInterpret,
                            const ParamInterpType& paramInterpret,
                            const std::string& nameCDclosure) :
    varInterpret_(varInterpret),
    paramInterpret_(paramInterpret),
    nameCDclosure_(nameCDclosure)
  {
    PyDict d;
    d[ClassParamsType::params.nameCDclosure] = nameCDclosure_;

    ClassParamsType::checkInputs(d);
  }

  const VarInterpType& varInterpret_;
  const ParamInterpType& paramInterpret_;
  const std::string nameCDclosure_;
};

//----------------------------------------------------------------------------//
struct ThicknessesCoefficientsParams2D_ProfileClosure: noncopyable
{
  const ParameterNumeric<int> quadratureOrder{"quadratureOrder", 39, 39, NO_LIMIT, "Numerical quadrature order for computing thickness integrals"};
  // note that the bounds are included as valid
  // default quadrature order = 39 is currently the highest supported in SANS

  struct DissipationClosureOptions
  {
    typedef std::string ExtractType;
    const ExtractType ibl3eqm = "ibl3eqm";
    const ExtractType ibl3lag = "ibl3lag";
    const ExtractType xfoillag = "xfoillag";

    const std::vector<ExtractType> options{ibl3eqm,ibl3lag,xfoillag};
  };
  const ParameterOption<DissipationClosureOptions>
    nameCDclosure{"nameCDclosure", "xfoillag", "Name (string) of the C_Diss closure model"};

  static void checkInputs(PyDict d); // parameter validity are checked only when this function is called

  static ThicknessesCoefficientsParams2D_ProfileClosure params;
};

template<class VarType, class CompressibilityType>
class ThicknessesCoefficients2D<ProfileClosure, VarType, CompressibilityType>
{
public:
  typedef PhysD2 PhysDim;
  typedef ProfileClosure ClosureType;

  typedef ThicknessesCoefficientsParams2D_ProfileClosure ClassParamsType;

  typedef VarInterpret2D<VarType> VarInterpType;
  typedef QauxvInterpret2D ParamInterpType;

  template<class T>
  using ArrayQ = typename VarInterpType::template ArrayQ<T>;

  template<class T>
  using ArrayParam = typename ParamInterpType::ArrayQ<T>;

  typedef VelocityProfile2D VelocityProfileType;
  typedef typename VelocityProfileType::VelocityInfoType VelocityInfoType;

  typedef DensityProfile2D<CompressibilityType> DensityProfile2DType;
  typedef ViscosityProfile2D<CompressibilityType> ViscosityProfile2DType;

  ThicknessesCoefficients2D(const VarInterpType& varInterpret,
                            const ParamInterpType& paramInterpret,
                            const PyDict& d) :
    ThicknessesCoefficients2D(varInterpret, paramInterpret,
                              d.get(ClassParamsType::params.nameCDclosure), d.get(ClassParamsType::params.quadratureOrder)) {}

  int quadratureOrder() const { return quadrature_.order();}
  std::string nameCDclosure() const { return nameCDclosure_; }

  // Functor for computing secondary variables from primary variables
  template<class Tparam, class Tibl>
  class Functor
  {
  public:
    typedef typename promote_Surreal<Tparam, Tibl>::type Tout;

    Functor(const IBLProfileCategory& profileCat,
            const ArrayParam<Tparam>& params,
            const ArrayQ<Tibl>& var,
            const Tparam& nue,
            const std::string& nameCDclosure,
            const int& nquad,
            const std::vector<Real>& w,
            const std::vector<Real>& eta,
            const VarInterpType& varInterpret,
            const ParamInterpType& paramInterpret,
            const VelocityProfileType& velocityProfile,
            const DensityProfile2DType& densityProfile,
            const ViscosityProfile2DType& viscosityProfile);

    Real getA_GbetaLocus() const { return A_GbetaLocus_; }
    Real getB_GbetaLocus() const { return B_GbetaLocus_; }

    // subordinate methods for computing each secondary variable
    Tout getdelta1s() const;
    Tout gettheta11() const;
    Tout gettheta1s() const;
    Tout getH() const;
    Tout getHk() const;
    Tout getHs() const;
    Tout getdelta1p() const;
    Tout getdeltarho() const;
    Tout gettheta0s() const;
    Tout getRetCf1() const;
    Tout getRetCD() const;
    Tout getctaueq() const;

    Tout getUslip(const Tout& Hk, const Tout& H, const Tout& Hs) const;

  private:
    const IBLProfileCategory profileCat_;
    const ArrayParam<Tparam>& params_;
    const ArrayQ<Tibl>& var_;
    const Tparam& nue_; // nu_e = mu_e/rho_e: edge kinematic viscosity
    const std::string nameCDclosure_;

    const int& nquad_; // this member needs to be declared here because the member of the same name
                       // from the parent class is not visible inside this class
    const std::vector<Real>& w_, eta_; // quadrature weights/coordinates

    const VarInterpType& varInterpret_;
    const ParamInterpType& paramInterpret_;

    const VelocityProfile2D velocityProfile_;
    const DensityProfile2DType densityProfile_;
    const ViscosityProfile2DType viscosityProfile_;

    const Tibl delta_;
    const Tibl A_;
    const Tibl ctau_;
    const Tout Redelta_;

    // G-beta locus constants
    const Real A_GbetaLocus_ = 6.7;
    const Real B_GbetaLocus_ = 0.75;

    mutable std::map<VelocityInfoType, std::vector<Tout>> velocityInfo_;
  };

  // Returns functor to carry out secondary variable calculations
  template<class Tparam, class Tibl>
  Functor<Tparam, Tibl>
  getCalculatorThicknessesCoefficients(const IBLProfileCategory& profileCat, const ArrayParam<Tparam>& params, const ArrayQ<Tibl>& var,
                                       const Tparam& nue) const;

private:
  ThicknessesCoefficients2D(const VarInterpType& varInterpret,
                            const ParamInterpType& paramInterpret,
                            const std::string& nameCDclosure,
                            const int quadratureOrder) :
    varInterpret_(varInterpret),
    paramInterpret_(paramInterpret),
    nameCDclosure_(nameCDclosure),
    quadrature_(quadratureOrder),
    nquad_(quadrature_.nQuadrature()),
    w_(nquad_),
    eta_(nquad_),
    velocityProfile_(),
    densityProfile_(),
    viscosityProfile_()
  {
    PyDict d;
    d[ClassParamsType::params.quadratureOrder] = quadratureOrder;
    d[ClassParamsType::params.nameCDclosure] = nameCDclosure_;

    ClassParamsType::checkInputs(d);

    // TODO: Note that it is assumed here that the same quadrature rule (order) is used throughout
    // the secondary variable calculations.  This needs to change if we want to explore adaptive quadrature.

    // cache quadrature weights/coordinates
    for (int i=0; i < nquad_; i++)
    {
      w_[i] = quadrature_.weight(i);
      eta_[i] = quadrature_.coordinate(i);
    }
  }

  const VarInterpType& varInterpret_;
  const ParamInterpType& paramInterpret_;
  const std::string nameCDclosure_;

  const QuadratureLine quadrature_;
  const int nquad_;
  std::vector<Real> w_, eta_; // quadrature weights/coordinates

  const VelocityProfile2D velocityProfile_;
  const DensityProfile2DType densityProfile_;
  const ViscosityProfile2DType viscosityProfile_;
};

} // namespace SANS

#endif /* SRC_PDE_IBL_THICKNESSESCOEFFICIENTS2D_H_ */
