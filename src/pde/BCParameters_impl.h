// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#if !defined(BCPARAMETERS_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include <boost/mpl/next.hpp>

#include "BCParameters.h"
#include "BCCategory.h"

// PyDict instantiate needed here for PyDict checkInputs below
#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

namespace BCParameters_detail
{

// Looping class. Get the current BC type and add a DictOption to the options vector
template < class iter, class end>
struct initOptions
{
  static void init( std::vector<DictOption>& options )
  {
    typedef typename boost::mpl::deref<iter>::type currentBCType;

    // Add the current option
    options.push_back( DictOption(currentBCType::ParamsType::BCName, currentBCType::ParamsType::checkInputs) );

    // Iterate recursively to the next option
    initOptions< typename boost::mpl::next<iter>::type, end >::init(options);
  }
};

// Recursive loop terminates when 'iter' is the same as 'end'
template < class end >
struct initOptions< end, end >
{
  static void init( std::vector<DictOption>& options )
  {
  }
};

} // namespace BCParameters_detail

template<class BCVector>
BCParameters<BCVector>::BCOptions::BCOptions()
{
  // Get the start and end iterators from the BCVector
  typedef typename boost::mpl::begin< BCVector >::type begin;
  typedef typename boost::mpl::end< BCVector >::type   end;

  BCParameters_detail::initOptions< begin, end >::init( const_cast<std::vector<DictOption>&>(options) );
}

template<class BCVector> // cppcheck-suppress passedByValue
void BCParameters<BCVector>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;

  // Extract the keys from the dictionary
  std::vector<std::string> keys = d.stringKeys();

  for (std::size_t i = 0; i < keys.size(); i++)
  {
    // Create a BC parameter for the given key
    const ParameterOption<BCOptions> BCParam{keys[i], NO_DEFAULT, "Boundary Condition Dictionary"};

    // Check the parameter
    allParams.push_back(d.checkInputs(BCParam));
  }

  // Check for unexpected inputs (though the above should remove them all)
  d.checkUnknownInputs(allParams);
}


namespace BCParameters_detail
{

// Looping class. Get the current BC type check if it matches the BC parameter
template < class iter, class end>
struct needsFieldTrace
{
  static bool check( const DictKeyPair& BC )
  {
    typedef typename boost::mpl::deref<iter>::type currentBCType;

    // Check if the BC matches the name of the current BC type
    if ( BC == currentBCType::ParamsType::BCName )
      return CategoryNeedsFieldTrace<typename currentBCType::Category>::value;

    // Iterate recursively to the next option
    return needsFieldTrace< typename boost::mpl::next<iter>::type, end >::check(BC);
  }
};

// Recursive loop terminates when 'iter' is the same as 'end'
template < class end >
struct needsFieldTrace< end, end >
{
  static bool check( const DictKeyPair& BC )
  {
    SANS_DEVELOPER_EXCEPTION("Should not be possible to get here. The BC dictionary must not have been checked with checkInputs.");
    return false;
  }
};

} // namespace BCParameters_detail

template<class BCVector>
std::vector<int> BCParameters<BCVector>::getLGBoundaryGroups(const PyDict& PyBCList,
                                                             const std::map< std::string, std::vector<int> >& BoundaryGroups )
{
  // Get the start and end iterators from the BCVector
  typedef typename boost::mpl::begin< BCVector >::type begin;
  typedef typename boost::mpl::end< BCVector >::type   end;

  // The vector of boundary groups that need lagrange multipliers
  std::vector<int> boundaryGroupsLG;

  // Extract the keys from the dictionary
  std::vector<std::string> keys = PyBCList.stringKeys();

  for (std::size_t i = 0; i < keys.size(); i++)
  {
    // Create a BC parameter for the given key
    const ParameterOption<BCOptions> BCParam{keys[i], NO_DEFAULT, "Boundary Condition Dictionary"};

    DictKeyPair BC = PyBCList.get(BCParam);

    // Check if Lagrange multipliers are needed and store off the vector of group indexes if so
    if ( BCParameters_detail::needsFieldTrace<begin, end>::check(BC) )
    {
      const std::vector<int>& group = BoundaryGroups.at(keys[i]);
      boundaryGroupsLG.insert( boundaryGroupsLG.end(), group.begin(), group.end() );
    }
  }

  boundaryGroupsLG.shrink_to_fit();

  return boundaryGroupsLG;
}


//Instantiate the singleton
template<class BCVector>
BCParameters<BCVector> BCParameters<BCVector>::params;

#define BCPARAMETER_INSTANTIATE( BCVECTOR ) \
PYDICT_PARAMETER_OPTION_INSTANTIATE(BCParameters<BCVECTOR>::BCOptions) \
template struct BCParameters<BCVECTOR>;

} // namespace SANS
