// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GETEULERTRAITSMODEL_H
#define GETEULERTRAITSMODEL_H

#include "TraitsEuler.h"
#include "TraitsNavierStokes.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"

namespace SANS
{

template<class TraitsModel> struct getEulerTraitsModel;

template<class QType_, class GasModel_>
struct getEulerTraitsModel< TraitsModelEuler<QType_,GasModel_> >
{
  typedef TraitsModelEuler<QType_,GasModel_> type;
};

template<class QType_, class GasModel_, class ViscosityModel_, class ThermalConductivityModel_>
struct getEulerTraitsModel<TraitsModelNavierStokes<QType_, GasModel_, ViscosityModel_, ThermalConductivityModel_>>
{
  typedef TraitsModelNavierStokes<QType_, GasModel_, ViscosityModel_, ThermalConductivityModel_> type;
};


template<class PDEBase, class SensorAdvectiveFlux, class SensorViscousFlux, class SensorSource>
struct getEulerTraitsModel<TraitsModelArtificialViscosity<PDEBase, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> >
{
  typedef typename getEulerTraitsModel<typename PDEBase::TraitsModel>::type type;
};


}

#endif // GETEULERTRAITSMODEL_H
