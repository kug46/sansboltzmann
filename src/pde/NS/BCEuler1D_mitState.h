// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER1D_MITFLUX_H
#define BCEULER1D_MITFLUX_H

// 1-D Euler BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "TraitsEuler.h"
#include "PDEEuler1D.h"
#include "PDEEulermitAVDiffusion1D.h"
#include "SolutionFunction_Euler1D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeFunction_mitState;
class BCTypeFullState_mitState;
class BCTypeOutflowSubsonic_Pressure_mitState;
class BCTypeOutflowSupersonic_Pressure_mitState;
class BCTypeOutflowSupersonic_PressureFunction_mitState;
class BCTypeReflect_mitState;
class BCTypeInflowSubsonic_PtTt_mitState;

template <class BCType, class PDEEuler1D>
class BCEuler1D;

template <class BCType>
struct BCEuler1DParams;


//----------------------------------------------------------------------------//
// Struct for creating a BC solution function
//
struct BCEulerSolution1DParams : noncopyable
{

  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Solution function used on the BC" };
    const ParameterString& key = Name;

    const DictOption Const{"Const", SolutionFunction_Euler1D_Const_Params::checkInputs};
    const DictOption Riemann{"Riemann", SolutionFunction_Euler1D_Riemann_Params::checkInputs};
    const DictOption DensityPulse{"DensityPulse", SolutionFunction_Euler1D_DensityPulse_Params::checkInputs};
    const DictOption ArtShock{"ArtShock", SolutionFunction_Euler1D_ArtShock_Params::checkInputs};
    const DictOption SineBackPressure{"SineBackPressure", SolutionFunction_Euler1D_SineBackPressure_Params::checkInputs};
    const DictOption ShuOsher{"ShuOsher", SolutionFunction_Euler1D_ShuOsher_Params::checkInputs};

    const std::vector<DictOption> options{ Const, Riemann, DensityPulse, ArtShock, SineBackPressure, ShuOsher };
  };
  const ParameterOption<SolutionOptions> Function{"Function", NO_DEFAULT, "The BC is a solution function"};

};

// Struct for creating the solution pointer
template<template <class> class PDETraitsSize, class PDETraitsModel>
struct BCEulerSolution1D
{
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  static Function_ptr getSolution(const PyDict& d);
};

//----------------------------------------------------------------------------//
// Full state with Solution Function
//----------------------------------------------------------------------------//
template<>
struct BCEuler1DParams< BCTypeFunction_mitState > : BCEulerSolution1DParams
{
  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};

  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler1D<BCTypeFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler1D<BCTypeFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeFunction_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCType> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  static const int D = PDETraitsSize<PhysDim>::D;   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;   // total solution variables

  static const int NBC = 3;                       // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde, const Function_ptr& solution, const bool& upwind ) :
      pde_(pde), solution_(solution), upwind_(upwind) {}
  ~BCEuler1D() {}

  BCEuler1D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    solution_( BCEulerSolution1D<PDETraitsSize, PDETraitsModel>::getSolution(d) ),
    upwind_(d.get(ParamsType::params.Characteristic)) {}

  BCEuler1D( const BCEuler1D& ) = delete;
  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  bool useUpwind() const { return upwind_; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  { state(x, time, nx, qI, qB); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  { state(x, time, nx, qI, qB); }

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  { state(x, time, nx, qI, qB); }


  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return isValidState(nx, q); }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const Function_ptr solution_;
  const bool upwind_;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
state( const Real& x, const Real& time,
       const Real& nx,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = (*solution_)(x,time);
}


// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( x, time, qI, qB, nx, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( x, time, qB, fx );

    Fn += fx*nx;
  }

  // No viscous flux
}


// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tp, class Tf>
inline void
BCEuler1D<BCTypeFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
fluxNormal( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( param, x, time, qI, qB, nx, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( param, x, time, qB, fx );

    Fn += fx*nx;
  }

  // No viscous flux
}


// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
fluxNormalSpaceTime( const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwindSpaceTime( x, time, qI, qB, nx, nt, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( x, time, qB, fx );

    ArrayQ<T> ft = 0;
    pde_.fluxAdvectiveTime( x, time, qB, ft);

    Fn += fx*nx + ft*nt;
  }

  // No viscous flux
}


// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tp, class Tf>
inline void
BCEuler1D<BCTypeFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
fluxNormalSpaceTime( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwindSpaceTime( param, x, time, qI, qB, nx, nt, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( param, x, time, qB, fx );

    ArrayQ<T> ft = 0;
    pde_.fluxAdvectiveTime( param, x, time, qB, ft);

    Fn += fx*nx + ft*nt;
  }

  // No viscous flux
}

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
void
BCEuler1D<BCTypeFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler1D<BCTypeFunction_mitState, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// Conventional Full State Imposed (possibly with characteristics)
//
//----------------------------------------------------------------------------//

template<class VariableType1DParams>
struct BCEuler1DFullStateParams : noncopyable
{
  BCEuler1DFullStateParams();

  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};
  const ParameterOption<typename VariableType1DParams::VariableOptions>& StateVector;

  static constexpr const char* BCName{"FullState"};
  struct Option
  {
    const DictOption FullState{BCEuler1DFullStateParams::BCName, BCEuler1DFullStateParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DFullStateParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeFullState_mitState, PDE_> :
    public BCType< BCEuler1D<BCTypeFullState_mitState, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeFullState_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DFullStateParams<typename PDE_::VariableType1DParams> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Varibles>
  BCEuler1D( const PDE& pde, const NSVariableType1D<Varibles>& data, const bool& upwind ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(data)),
    upwind_(upwind)
  {
    qInterpret_.evalVelocity( qB_, uB_ );
  }

  BCEuler1D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(d)),
    upwind_(d.get(ParamsType::params.Characteristic))
  {
    qInterpret_.evalVelocity( qB_, uB_ );
  }


  ~BCEuler1D() {}

  BCEuler1D( const BCEuler1D& ) = delete;
  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> qB_;
  const bool upwind_;
  Real uB_;
};

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeFullState_mitState, PDE>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( x, time, qI, qB, nx, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( x, time, qB, fx );

    Fn += fx*nx;
  }

  // No viscous flux
}

// normal BC flux
template <class PDE>
template <class T, class Tp, class Tf>
inline void
BCEuler1D<BCTypeFullState_mitState, PDE>::
fluxNormal( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( param, x, time, qI, qB, nx, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( param, x, time, qB, fx );

    Fn += fx*nx;
  }

  // No viscous flux
}

// is the boundary state valid
template <class PDE>
bool
BCEuler1D<BCTypeFullState_mitState, PDE>::isValidState( const Real& nx, const ArrayQ<Real>& qI) const
{
  if (upwind_)
  {
    // Characteristic can be either inflow or outflow
    return true;
  }
  else
  {
    Real uI;
    qInterpret_.evalVelocity( qI, uI );
    // Full state requires inflow, so of the dot product between the BC an interior must be positive
    return (uI*uB_) > 0;
  }

}

//----------------------------------------------------------------------------//
// Conventional PX Style subsonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSubsonic_Pressure_mitState"};
  struct Option
  {
    const DictOption OutflowSubsonic_Pressure_mitState{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE_> :
    public BCType< BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeOutflowSubsonic_Pressure_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 2;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde, const Real pSpec ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    pSpec_(pSpec)
  {
    // Nothing
  }
  ~BCEuler1D() {}

  BCEuler1D( const PDE& pde, const PyDict& d ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    pSpec_(d.get(ParamsType::params.pSpec) )
  {
    // Nothing
  }

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  //const ArrayQ<Real> bcdata_;
  const Real pSpec_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
state( const Real& x, const Real& time,
       const Real& nx,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rho, u, t, tnew, pnew = 0;

  // set new boundary state

  //compute Riemann Invariant Jm(q,n) - after Fidkowski 2004
  T gamma = gas_.gamma();
  T R = gas_.R();
  qInterpret_.eval( qI, rho, u, t );
  T p = gas_.pressure(rho, t);
  T s = p / pow(rho, gamma);
  T c = gas_.speedofSound(t);
  T Jp = u*nx + 2*c/(gamma-1);

  pnew = pSpec_;
  T rhonew = pow( (pnew/s), (1./gamma));
  T cnew = sqrt(gamma * pnew / rhonew);
  T vnnew = Jp - 2*cnew / (gamma-1);

  T unew = nx*vnnew;

  tnew = pnew / (rhonew*R);

  qB = 0;
  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature1D<T>( rhonew, unew, tnew ) );
}


template <class PDE>
template <class T, class L, class R>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
state(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const Real& nx,
    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, time, nx, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0;
  pde_.fluxAdvective(x, time, qB, fx);

  Fn += fx*nx;

  // No viscous flux
}

// normal BC flux
template <class PDE>
template <class T, class Tp, class Tf>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
fluxNormal( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0;
  pde_.fluxAdvective(param, x, time, qB, fx);

  Fn += fx*nx;

  // No viscous flux
}


template <class PDE>
void
BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: pSpec_ = " << pSpec_ << std::endl;
}

//----------------------------------------------------------------------------//
// Conventional PX Style supersonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_mitState> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSupersonic_Pressure_mitState"};
  struct Option
  {
    const DictOption OutflowSupersonic_Pressure_mitState{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE_> :
    public BCType< BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeOutflowSupersonic_Pressure_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 2;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde, const Real pSpec ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    pSpec_(pSpec)
  {
    // Nothing
  }
  ~BCEuler1D() {}

  BCEuler1D( const PDE& pde, const PyDict& d ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    pSpec_(d.get(ParamsType::params.pSpec) )
  {
    // Nothing
  }

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const { state(x, time, nx, qI, qB); }

  // BC state
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const { state(x, time, nx, qI, qB); }

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const { state(x, time, nx, qI, qB); }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return isValidState(nx, q); }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  //const ArrayQ<Real> bcdata_;
  const Real pSpec_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
state( const Real& x, const Real& time,
       const Real& nx,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rho, u, t, tnew, pnew = 0;

  // set new boundary state

  //compute Riemann Invariant Jm(q,n) - after Fidkowski 2004
  T gamma = gas_.gamma();
  T R = gas_.R();
  qInterpret_.eval( qI, rho, u, t );
  T p = gas_.pressure(rho, t);
  T s = p / pow(rho, gamma);
  T c = gas_.speedofSound(t);
  T Jp = u*nx + 2*c/(gamma-1);

  pnew = pSpec_;
  T rhonew = pow( (pnew/s), (1./gamma));
  T cnew = sqrt(gamma * pnew / rhonew);
  T vnnew = Jp - 2*cnew / (gamma-1);

  T unew = nx*vnnew;

  tnew = pnew / (rhonew*R);

  qB = 0;
  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature1D<T>( rhonew, unew, tnew ) );
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0;
  pde_.fluxAdvectiveUpwind(param, x, time, qI, qB, nx, fx);
  Fn += fx;

  // No viscous flux
}


// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0;
  pde_.fluxAdvectiveUpwind(x, time, qI, qB, nx, fx);
  Fn += fx;

  // No viscous flux
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
fluxNormalSpaceTime( const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  ArrayQ<T> f = 0;
  // Compute the advective flux based on the boundary state
  pde_.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, f);

  Fn += f;
  // No viscous flux
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  ArrayQ<T> f = 0;
  // Compute the advective flux based on the boundary state
  pde_.fluxAdvectiveUpwindSpaceTime(param, x, time, qI, qB, nx, nt, f);

  Fn += f;
  // No viscous flux
}



template <class PDE>
void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>: pSpec_ = " << pSpec_ << std::endl;
}



//----------------------------------------------------------------------------//
// Conventional PX Style supersonic outflow BC: varying backpressure
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_mitState> : BCEulerSolution1DParams
{

  static constexpr const char* BCName{"OutflowSupersonic_PressureFunction_mitState"};
  struct Option
  {
    const DictOption OutflowSupersonic_PressureFunction_mitState{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeOutflowSupersonic_PressureFunction_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_mitState> ParamsType;
  typedef PDE_<PDETraitsSize, PDETraitsModel> PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 2;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde, const Function_ptr& solution ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    solution_(solution)
  {
    // Nothing
  }
  ~BCEuler1D() {}

  BCEuler1D( const PDE& pde, const PyDict& d ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    solution_( BCEulerSolution1D<PDETraitsSize, PDETraitsModel>::getSolution(d) )
  {
    // Nothing
  }

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Function_ptr solution_;
};


// BC state
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
state( const Real& x, const Real& time,
       const Real& nx,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{

  ArrayQ<T> qS = (*solution_)(x,time);
  T rhoS, uS, tS, pS;
  qInterpret_.eval( qS, rhoS, uS, tS );
  pS = gas_.pressure(rhoS, tS);

  T rho, u, t, tnew = 0;

  // set new boundary state

  //compute Riemann Invariant Jm(q,n) - after Fidkowski 2004
  T gamma = gas_.gamma();
  T R = gas_.R();
  qInterpret_.eval( qI, rho, u, t );
  T p = gas_.pressure(rho, t);
  T s = p / pow(rho, gamma);
  T c = gas_.speedofSound(t);
  T Jp = u*nx + 2*c/(gamma-1);

  T rhonew = pow( (pS/s), (1./gamma));
  T cnew = sqrt(gamma * pS / rhonew);
  T vnnew = Jp - 2*cnew / (gamma-1);

  T unew = nx*vnnew;

  tnew = pS / (rhonew*R);

  qB = 0;
  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature1D<T>( rhonew, unew, tnew ) );
}


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class L, class R>
inline void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
state(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const Real& nx,
    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, time, nx, qI, qB);
}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0;
  pde_.fluxAdvectiveUpwind(x, time, qI, qB, nx, fx);
  Fn += fx;
}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tp, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
fluxNormal( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0;
  pde_.fluxAdvectiveUpwind(param, x, time, qI, qB, nx, fx);
  Fn += fx;
}


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Solution reflected about a symmetry plane
//
// specify: normal velocity
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeReflect_mitState> : noncopyable
{
  static constexpr const char* BCName{"Reflect_mitState"};
  struct Option
  {
    const DictOption Reflect_mitState{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeReflect_mitState, PDE_> :
    public BCType< BCEuler1D<BCTypeReflect_mitState, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeReflect_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}
  ~BCEuler1D() {}

  BCEuler1D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, qI, qB);
  }

  // BC state gradient
  template <class T>
  void stateGradient( const Real& nx,
                      const ArrayQ<T>& qIx,
                            ArrayQ<T>& qBx ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class PDE>
template <class T>
inline void
BCEuler1D<BCTypeReflect_mitState, PDE>::state(
    const Real& x, const Real& time,
    const Real& nx,
    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rhoI=0, uI=0, tI=0;

  // modify pressure so that it is computed only from parallel velocity
  qInterpret_.eval( qI, rhoI, uI, tI );
  T pI = gas_.pressure(rhoI, tI);

  T uB = uI - nx*(uI*nx);

  qInterpret_.setFromPrimitive( qB, DensityVelocityPressure1D<T>(rhoI, uB, pI) );
}


template <class PDE>
template <class T>
void
BCEuler1D<BCTypeReflect_mitState, PDE>::
stateGradient( const Real& nx,
               const ArrayQ<T>& qIx,
                     ArrayQ<T>& qBx ) const
{
  typedef DLA::VectorS<D,T> VectorX;
  typedef DLA::MatrixS<D,D,Real> MatrixX;

  // mirror scalar components
  ArrayQ<T> qIn = qIx*nx;
  qBx = qIx - 2*nx*qIn;

  // Matrix that performs the mirroring operation
  MatrixX Jr = { {1 -2*nx*nx} };

  VectorX gradIu = { qIx[QInterpret::ix] };

  // Vector gradient in the normal direction
  VectorX gradIVn = gradIu*nx;

  VectorX gradBu = Jr*(gradIu - 2*nx*gradIVn);

  // set the vector gradients
  qBx[QInterpret::ix] = gradBu[0];
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeReflect_mitState, PDE>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on interior and reflected boundary state
  pde_.fluxAdvectiveUpwind( x, time, qI, qB, nx, Fn );

  // Don't bother with viscous flux if it does not exist
  if (!pde_.hasFluxViscous()) return;

  // compute the mirrored gradients
  ArrayQ<T> qBx, qBy, qBz;

  stateGradient( nx,
                 qIx,
                 qBx );

  // Compute the viscous flux
  pde_.fluxViscous(x, time, qI, qIx, qB, qBx, nx, Fn);
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler1D<BCTypeReflect_mitState, PDE>::
fluxNormal( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on interior and reflected boundary state
  pde_.fluxAdvectiveUpwind( param, x, time, qI, qB, nx, Fn );

  // Don't bother with viscous flux if it does not exist
  if (!pde_.hasFluxViscous()) return;

  // compute the mirrored gradients
  ArrayQ<T> qBx, qBy, qBz;

  stateGradient( nx,
                 qIx,
                 qBx );

  // Compute the viscous flux
//  pde_.fluxViscous(param, x, time, qI, qIx, qB, qBx, nx, Fn);
}


template <class PDE>
void
BCEuler1D<BCTypeReflect_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler1D<BCTypeReflect_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeReflect_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeReflect_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// Conventional PX style subsonic inflow BC:
//
// specify: Stagnation Temperature and stagnation pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState> : noncopyable
{
  const ParameterNumeric<Real> TtSpec{"TtSpec", NO_DEFAULT, NO_RANGE, "Stagnation Temperature"};
  const ParameterNumeric<Real> PtSpec{"PtSpec", NO_DEFAULT, NO_RANGE, "Stagnation Pressure"};

  static constexpr const char* BCName{"InflowSubsonic_PtTt_mitState"};
  struct Option
  {
    const DictOption InflowSubsonic_PtTt_mitState{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE_> :
    public BCType< BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeInflowSubsonic_PtTt_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 2;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde, const Real& TtSpec, const Real& PtSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      TtSpec_(TtSpec), PtSpec_(PtSpec)  {}
  ~BCEuler1D() {}

  BCEuler1D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    TtSpec_(d.get(ParamsType::params.TtSpec)),
    PtSpec_(d.get(ParamsType::params.PtSpec)) {}

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  { state(x, time, nx, qI, qB); }

  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  { state(x, time, nx, qI, qB); }

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  { state(x, time, nx, qI, qB); }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const
    { return isValidState(nx, q);  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:

  // Computes the boundary mach number
  template<class T>
  bool MachB(const Real& nx, const ArrayQ<T>& qI, T& M) const;

  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real TtSpec_;
  const Real PtSpec_;

};

template <class PDE>
template <class T>
inline bool
BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>::
MachB( const Real& nx,
       const ArrayQ<T>& qI, T& M ) const
{

  T rho=0, u=0, t=0;

  //preserve outgoing Riemann Invariant Jp(q,n) - after Fidkowski 2004
  qInterpret_.eval( qI, rho, u, t );

  Real R     = gas_.R();
  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  T c = gas_.speedofSound(t);
  T Jp = u*nx + 2*c/gmi;

  // u*n[0] = c*M*( cos(alpha)*n[0] + sin(alpha)*n[1] ) = c*M*ndir

  Real dn = nx;
  // maximum possible J_plus for real M roots
  Real Jpmax = sqrt(2*(2+gmi*dn*dn)*gamma*R*TtSpec_)/gmi;

  if ( fabs(Jp) > Jpmax )
  {
    // Mach from quadratic equation with Jpmax instead of Jp; note only one root
    M = -dn; // added now, may need to delete
    return true;
  }

  T A =    gamma*R*TtSpec_*dn*dn - gmi/2*Jp*Jp;
  T B = 4.*gamma*R*TtSpec_*dn / gmi;
  T C = 4.*gamma*R*TtSpec_/(gmi*gmi) - Jp*Jp;
  T D = B*B - 4*A*C;

  // Check that we have a valid state
  if ( D < 0 ) return false;

  T Mplus  = (-B + sqrt(D))/(2.*A);
  T Mminus = (-B - sqrt(D))/(2.*A);

  // Pick physically relevant solution
  if ((Mplus >= 0.0) && (Mminus <= 0.0))
  {
    M = Mplus;
  }
  else if ((Mplus <= 0.0) && (Mminus >= 0.0))
  {
    M = Mminus;
  }
  else if ((Mplus > 0.0) && (Mminus > 0.0))
  {
    // not clear which is physical...
    // return invalid state
    return false;
#if 0
    // pick smaller for now
    if ( Mminus <= Mplus )
      M = Mminus;
    else
      M = Mplus;
#endif
  }
  else
  {
    // negative Mach means reverse flow and is OK; choose smallest Mach
    if (fabs(Mminus) < fabs(Mplus))
      M = Mminus;
    else
      M = Mplus;
  }

  // the state is valid
  return true;
}


template <class PDE>
template <class T>
inline void
BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>::
state( const Real& x, const Real& time,
       const Real& nx,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T M = 0;
  MachB( nx, qI, M );

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  // set boundary state
  T tB   = TtSpec_/(1.+0.5*gmi*M*M);
  T pB   = PtSpec_/pow( 1.+0.5*gmi*M*M, gamma/gmi );
  T rhoB = gas_.density(pB, tB);

  T cB = gas_.speedofSound(tB);
  T VB = cB*M;
  T uB = VB;

  qInterpret_.setFromPrimitive( qB, DensityVelocityPressure1D<T>( rhoB, uB, pB ) );

}


// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0;
  pde_.fluxAdvective(param, x, time, qB, fx);

  Fn += fx*nx;
}


// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0;
  pde_.fluxAdvective(x, time, qB, fx);

  Fn += fx*nx;
}


// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>::
fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  pde_.fluxAdvectiveUpwindSpaceTime( param, x, time, qI, qB, nx, nt, Fn );
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>::
fluxNormalSpaceTime( const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  pde_.fluxAdvectiveUpwindSpaceTime( x, time, qI, qB, nx, nt, Fn );
}


// is the boundary state valid
template <class PDE>
bool
BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>::
isValidState( const Real& nx, const ArrayQ<Real>& q) const
{
  Real M;
  return MachB( nx, q, M );
}

template <class PDE>
void
BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


} //namespace SANS

#endif  // BCEULER1D_MITFLUX_H
