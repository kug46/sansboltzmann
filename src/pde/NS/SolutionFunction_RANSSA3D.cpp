// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/NS/SolutionFunction_RANSSA3D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_RANSSA3D_OjedaMMS_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.ntRef));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_RANSSA3D_OjedaMMS_Params SolutionFunction_RANSSA3D_OjedaMMS_Params::params;


}
