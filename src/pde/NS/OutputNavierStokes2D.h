// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTNAVIERSTOKES2D_H
#define OUTPUTNAVIERSTOKES2D_H

#include "PDERANSSA2D.h"

#include "pde/OutputCategory.h"
#include "pde/AnalyticFunction/Function2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Provides weights to compute a total heat flux from a weighted residual

template <class PDENDConvert>
class OutputNavierStokes2D_TotalHeatFlux : public OutputType< OutputNavierStokes2D_TotalHeatFlux<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputNavierStokes2D_TotalHeatFlux( const PDENDConvert& pde, const Real scale = 1.0 ) :
    pde_(pde), scale_(scale)
  {
    // Nothing
  }

  void operator()(const Real& x, const Real& y, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = 0;
    weight[pde_.iEngy] = 1.0*scale_; // scale allows non-dimensionalization
  }

private:
  const PDENDConvert& pde_;
  const Real scale_;
};

//----------------------------------------------------------------------------//
// Provides weights to compute a Gaussian heat flux from a weighted residual

template <class PDENDConvert>
class OutputNavierStokes2D_GaussianWeightedHeatFlux : public OutputType< OutputNavierStokes2D_GaussianWeightedHeatFlux<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputNavierStokes2D_GaussianWeightedHeatFlux( const PDENDConvert& pde, const Real s = 0, const Real x0 = 0, const Real y0 = 0 ) :
    pde_(pde),
    // s - standard deviations
    // s2 - variance
    s2_(s*s),
    x0_(x0), y0_(y0) // centroid location
  {
    // Nothing
  }

  void operator()(const Real& x, const Real& y, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = 0;

    Real Gauss = 1;
    if (s2_ > 0)
      Gauss = exp( -0.5*(pow(x-x0_,2) + pow(y-y0_,2))/s2_ )/sqrt( 2*s2_*M_PI);

    weight[pde_.iEngy] = Gauss;
  }

private:
  const PDENDConvert& pde_;
  const Real s2_;      //variance
  const Real x0_, y0_; //center location
};


//----------------------------------------------------------------------------//
// Provides weights to compute a Gaussian heat flux from a weighted residual

template <class PDENDConvert>
class OutputNavierStokes2D_Drag : public OutputType< OutputNavierStokes2D_Drag<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using MatrixQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputNavierStokes2D_Drag( const PDENDConvert& pde ) :
    pde_(pde)
  {
    // Nothing
  }

  bool needsSolutionGradient() const { return true; }

  // Boundary output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    ArrayQ<To> Fx = 0, Fy=0;
    pde_.fluxAdvective(x, y, time, q, Fx, Fy);
    pde_.fluxViscous(x, y, time, q, qx, qy, Fx, Fy);

    ArrayQ<To> F = Fx*nx + Fy*ny;

    output = F[pde_.ixMom];

  }

  // Boundary output
  template<class Tq, class Tg, class To>
  void operator()(const Real& dist, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, nx, ny, q, qx, qy, output);
  }

private:
  const PDENDConvert& pde_;
};

}

#endif //OUTPUTNAVIERSTOKES2D_H
