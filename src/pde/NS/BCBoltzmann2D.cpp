// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCBoltzmann2D.h"
#include "Q2DPrimitiveDistributionFunctions.h"

// TODO: Rework so this is not needed here
#include "TraitsBoltzmann2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"


namespace SANS
{

// cppcheck-suppress passedByValue
void BCBoltzmann2DParams<BCTypeNone>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCBoltzmann2DParams<BCTypeNone> BCBoltzmann2DParams<BCTypeNone>::params;

// cppcheck-suppress passedByValue
void BCBoltzmann2DParams<BCTypeDiffuseKinetic>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Uw));
  allParams.push_back(d.checkInputs(params.Vw));
  allParams.push_back(d.checkInputs(params.Rhow));
  d.checkUnknownInputs(allParams);
}
BCBoltzmann2DParams<BCTypeDiffuseKinetic> BCBoltzmann2DParams<BCTypeDiffuseKinetic>::params;

// cppcheck-suppress passedByValue
void BCBoltzmann2DParams<BCTypeDiffuseKineticDensityOutlet>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Rhow));
  allParams.push_back(d.checkInputs(params.applyUw));
  allParams.push_back(d.checkInputs(params.valUw));
  allParams.push_back(d.checkInputs(params.applyVw));
  allParams.push_back(d.checkInputs(params.valVw));
  d.checkUnknownInputs(allParams);
}
BCBoltzmann2DParams<BCTypeDiffuseKineticDensityOutlet> BCBoltzmann2DParams<BCTypeDiffuseKineticDensityOutlet>::params;

//===========================================================================//
// Instantiate BC parameters

// Pressure-Primitive Variables
typedef BCBoltzmann2DVector< TraitsSizeBoltzmann2D, TraitsModelBoltzmann2D<QTypePrimitiveDistributionFunctions, GasModel> > BCVector_PDFs;
BCPARAMETER_INSTANTIATE( BCVector_PDFs )

} //namespace SANS
