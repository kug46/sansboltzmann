// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ROEBOLTZMANN2D_H
#define ROEBOLTZMANN2D_H

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "tools/minmax.h"
#include "UserVariables/BoltzmannNVar.h"
#include "UserVariables/BoltzmannVelocity.h"

namespace SANS
{

enum RoeEntropyFix
{
  none,
  eVanLeer,
  eHarten
};


template<class QType, template <class> class PDETraitsSize>
class Q2D;



template <class QType, template <class> class PDETraitsSize>
class RoeBoltzmann2D
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef Q2D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  explicit RoeBoltzmann2D(const GasModel& gas0, const RoeEntropyFix entropyFix) :
        gas_(gas0),
        qInterpret_(gas0),
        entropyFix_(entropyFix)
  {
    // Nothing
  }

  RoeBoltzmann2D( const RoeBoltzmann2D& roe0 ) = delete;
  RoeBoltzmann2D& operator=( const RoeBoltzmann2D& ) = delete;

  RoeEntropyFix entropyFix() const { return entropyFix_; }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    T C1, eigu;
    fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f, C1, eigu);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, Tq& C1, Tq& eigu ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const;

private:
  const GasModel& gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  const RoeEntropyFix entropyFix_;
};


template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
RoeBoltzmann2D<QType, PDETraitsSize>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f, Tq& C1, Tq& eigu ) const
{
#if 0
  Tq  pdf0L = 0, pdf1L = 0, pdf2L = 0,
      pdf3L = 0, pdf4L = 0, pdf5L = 0,
      pdf6L = 0, pdf7L = 0, pdf8L = 0;
  Tq  pdf0R = 0, pdf1R = 0, pdf2R = 0,
      pdf3R = 0, pdf4R = 0, pdf5R = 0,
      pdf6R = 0, pdf7R = 0, pdf8R = 0;

  qInterpret_.eval( qL,  pdf0L, pdf1L, pdf2L,
                         pdf3L, pdf4L, pdf5L,
                         pdf6L, pdf7L, pdf8L );


  qInterpret_.eval( qR,  pdf0R, pdf1R, pdf2R,
                         pdf3R, pdf4R, pdf5R,
                         pdf6R, pdf7R, pdf8R );


  const int e[9][2] = {   {  0,  0 },
                          {  1,  0 },
                          {  0,  1 },
                          { -1,  0 },
                          {  0, -1 },
                          {  1,  1 },
                          { -1,  1 },
                          { -1, -1 },
                          {  1, -1 }    };

  f(if0) += 0.5*(nx*e[0][0] + ny*e[0][1])*(pdf0R + pdf0L) - 0.5*fabs(nx*e[0][0] + ny*e[0][0])*(pdf0R - pdf0L);
  f(if1) += 0.5*(nx*e[1][0] + ny*e[1][1])*(pdf1R + pdf1L) - 0.5*fabs(nx*e[1][0] + ny*e[1][0])*(pdf1R - pdf1L);
  f(if2) += 0.5*(nx*e[2][0] + ny*e[2][1])*(pdf2R + pdf2L) - 0.5*fabs(nx*e[2][0] + ny*e[2][0])*(pdf2R - pdf2L);
  f(if3) += 0.5*(nx*e[3][0] + ny*e[3][1])*(pdf3R + pdf3L) - 0.5*fabs(nx*e[3][0] + ny*e[3][0])*(pdf3R - pdf3L);
  f(if4) += 0.5*(nx*e[4][0] + ny*e[4][1])*(pdf4R + pdf4L) - 0.5*fabs(nx*e[4][0] + ny*e[4][0])*(pdf4R - pdf4L);
  f(if5) += 0.5*(nx*e[5][0] + ny*e[5][1])*(pdf5R + pdf5L) - 0.5*fabs(nx*e[5][0] + ny*e[5][0])*(pdf5R - pdf5L);
  f(if6) += 0.5*(nx*e[6][0] + ny*e[6][1])*(pdf6R + pdf6L) - 0.5*fabs(nx*e[6][0] + ny*e[6][0])*(pdf6R - pdf6L);
  f(if7) += 0.5*(nx*e[7][0] + ny*e[7][1])*(pdf7R + pdf7L) - 0.5*fabs(nx*e[7][0] + ny*e[7][0])*(pdf7R - pdf7L);
  f(if8) += 0.5*(nx*e[8][0] + ny*e[8][1])*(pdf8R + pdf8L) - 0.5*fabs(nx*e[8][0] + ny*e[8][0])*(pdf8R - pdf8L);
#endif


  Real Beta = 1.;
  for (int i=0; i<NVar; i++)
    f(i) += 0.5*(nx*ew[i][0] + ny*ew[i][1])*(qR(i) + qL(i)) - Beta*fabs(nx*ew[i][0] + ny*ew[i][0])*(qR(i) - qL(i));



#if 0
  // TODO: Ideal gas is assumed in the following formulation
  Tq w1L = sqrt(rhoL);
  Tq w1R = sqrt(rhoR);

  // Roe averaged quantities
  Tq u = (w1L*uL + w1R*uR) / (w1L + w1R);
  Tq H = (w1L*HL + w1R*HR) / (w1L + w1R) ;

  // This assumes calorically perfect gas
  Tq c = sqrt( (gamma - 1.0)*(H - 0.5*(u*u)) ) ;

  Tq qn = nx*u;

  // jump conditions and Roe coefficients
  Tq delrho  = rhoR    - rhoL;
  Tq delrhou = rhoR*uR - rhoL*uL;
  Tq delrhoE = rhoR*ER - rhoL*EL;

  Tq eigumc = fabs(qn - c);
  Tq eigupc = fabs(qn + c);
     eigu   = fabs(qn    );

  Real eps0 = 1e-2;
  Tq eps    = c*eps0;
#endif
  switch (entropyFix_)
  {
  case none:
  {
    // Do nothing
    break;
  }
  case eVanLeer:
  {
#if 0
    // 'entropy fix' of van Leer, Lee and Powell (Sonic Point Capturing)
    //
    // The term
    //
    // deigmin = max(eps, deig);
    //
    // was suggested by Prof Darmofal so that the dissipation does not disappear
    // for eigu == 0
    static const int  FIX = 4; // entropy fix fudge factor

    Tq qnL = nx*uL;
    Tq qnR = nx*uR;
    Tq cL  = gas_.speedofSound(tL);
    Tq cR  = gas_.speedofSound(tR);

    Tq eigL, eigR, deig, deigmin;

    eigL = qnL - cL;
    eigR = qnR - cR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigumc < 0.5*deigmin)
      eigumc = (eigumc*eigumc)/deigmin + 0.25*deigmin;

    eigL = qnL + cL;
    eigR = qnR + cR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigupc < 0.5*deigmin)
      eigupc = (eigupc*eigupc)/deigmin + 0.25*deigmin;

    eigL = qnL;
    eigR = qnR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigu < 0.5*deigmin)
      eigu = (eigu*eigu)/deigmin + 0.25*deigmin;
#endif
    SANS_DEVELOPER_EXCEPTION("eVanLeer entropy fix not yet developed for Boltzmann Eq.");
    break;
  }
  case eHarten:
  {
#if 0
    /*--------------------------*/
    /*  Harten (PX) Entropy Fix */
    /*--------------------------*/

    if (eigumc < eps)
      eigumc = 0.5*(eps + eigumc*eigumc/eps);
    //eigumc = (eigumc*eigumc + eps*eps) / (2*eps);

    if (eigupc < eps)
      eigupc = 0.5*(eps + eigupc*eigupc/eps);
    //eigupc = (eigupc*eigupc + eps*eps) / (2*eps);

    if (eigu < eps)
      eigu = 0.5*(eps + eigu*eigu/eps);
    //eigu = (eigu*eigu + eps*eps) / (2*eps);
#endif
    SANS_DEVELOPER_EXCEPTION("eHarten entropy fix not yet developed for Boltzmann Eq.");
    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown entropy fix.");
  }
#if 0
  // Roe dissipation flux following Fidkowski
  Tq qq = 0.5*(u*u) ;
  Tq G1 = (gamma-1.0)/(c*c)*(delrho*qq - delrhou*u + delrhoE);
  Tq G2 = (delrhou*nx - delrho*qn);
  Tq s1 = 0.5*(eigupc + eigumc);
  Tq s2 = 0.5*(eigupc - eigumc);
     C1 = G2*s2/c + G1*(s1 - eigu);
  Tq C2 = c*G1*s2 + G2*(s1 - eigu);

  f(if0) -= 0.5*(           C1 + delrho *eigu );
  f(if1) -= 0.5*( nx*C2 + u*C1 + delrhou*eigu );
  f(if2) -= 0.5*( qn*C2 + H*C1 + delrhoE*eigu );
#endif
#if 0 //Steve's alternate formulation
  T alpha1   = 0.5*(delp   - delqn) * eigumc;
  T alpha2   = 0.5*(delp   + delqn) * eigupc;
  T alpha3   =     (delrho - delp ) * eigu  ;
  T alpha4   =     (delqs         ) * eigu  ;

  /*  Roe dissipation flux  */
  T qq     = 0.5*(u*u + v*v) ;
  flx1_dsp = al1            + al2            + al3                ;
  flxs_dsp = al1*(qs      ) + al2*(qs      ) + al3*(qs) + al4     ;
  flxn_dsp = al1*(qn - c  ) + al2*(qn + c  ) + al3*(qn)           ;
  flx4_dsp = al1*(H - c*qn) + al2*(H + c*qn) + al3*(qq) + al4*(qs);

  f(0) -= 0.5*(ds*flx1_dsp              );
  f(1) -= 0.5*(dx*flxs_dsp + dy*flxn_dsp);
  f(2) -= 0.5*(dy*flxs_dsp - dx*flxn_dsp);
  f(3) -= 0.5*(ds*flx4_dsp              );
#endif
}

template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
RoeBoltzmann2D<QType, PDETraitsSize>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{

  SANS_DEVELOPER_EXCEPTION( "Spacetime Roe Scheme Not Yet Implemented" );

}

} // namespace SANS

#endif // ROEBOLTZMANN_H
