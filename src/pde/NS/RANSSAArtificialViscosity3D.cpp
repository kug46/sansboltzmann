// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define AVSENSOR3D_INSTANTIATE

#include "TraitsRANSSAArtificialViscosity.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D_impl.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D_impl.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D_impl.h"

namespace SANS
{

//Instantiations for the Artificial Viscosity PDE with RANS-SA

template class AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity>;

template class AVSensor_DiffusionMatrix3D_Uniform<TraitsSizeRANSSAArtificialViscosity>;
template class AVSensor_DiffusionMatrix3D_GenHScale<TraitsSizeRANSSAArtificialViscosity>;

template class AVSensor_Source3D_Uniform<TraitsSizeRANSSAArtificialViscosity>;

}
