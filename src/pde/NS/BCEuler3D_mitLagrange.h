// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER3D_MITLAGRANGE_H
#define BCEULER3D_MITLAGRANGE_H

// 3-D Euler BC class with mitLagrange formulations

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "NSVariable3D.h"
#include "GasModel.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>
#include <memory> //shared_ptr

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 3-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//


template <class BCType, class PDEEuler3D>
class BCEuler3D;

template <class BCType>
struct BCEuler3DParams;

} //namespace SANS

#endif  // BCEULER3D_MITLAGRANGE_H
