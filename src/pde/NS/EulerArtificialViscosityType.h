// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EULERARTIFICIALVISCOSITYTYPE_H
#define EULERARTIFICIALVISCOSITYTYPE_H

//  type defining the artificial viscosity type

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // sqrt
#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/PromoteSurreal.h"

namespace SANS
{

enum class EulerArtViscosity
{
  eLaplaceViscosityEnthalpy,
  eLaplaceViscosityEnergy,
  eShearViscosity,
  eBrennerViscosity,
  eHolst2020
};

} //namespace SANS

#endif  // EULERARTIFICIALVISCOSITYTYPE_H
