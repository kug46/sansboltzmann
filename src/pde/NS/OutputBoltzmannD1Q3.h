// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTBOLTZMANND1Q3_H
#define OUTPUTBOLTZMANND1Q3_H

#include "PDEBoltzmannD1Q3.h"

#include "pde/OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Provides weights to compute a force from a weighted residual

template <class PDENDConvert>
class OutputBoltzmannD1Q3_Force : public OutputType< OutputBoltzmannD1Q3_Force<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputBoltzmannD1Q3_Force( const PDENDConvert& pde, const Real& wx = 1 ) :
    pde_(pde), wx_(wx) {}

  void operator()(const Real& x, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = 0;
    weight[pde_.ixMom] = wx_;
  }

private:
  const PDENDConvert& pde_;
  Real wx_;
};
//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputBoltzmannD1Q3_Density : public OutputType< OutputBoltzmannD1Q3_Density<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputBoltzmannD1Q3_Density( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    GasModel gas = pde_.gasModel();
    QInterpret qInterp = pde_.variableInterpreter();

    T rho = 0;
    T gamma = gas.gamma();

    qInterp.evalDensity( q, rho);
    output = rho;
  }

  template<class Tp, class T>
  void operator()(const  Tp& param,
                  const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    operator()(x, time, q, qx, output);
  }

private:
  const PDENDConvert& pde_;
};

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputBoltzmannD1Q3_Momentum : public OutputType< OutputBoltzmannD1Q3_Momentum<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputBoltzmannD1Q3_Momentum( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    GasModel gas = pde_.gasModel();
    QInterpret qInterp = pde_.variableInterpreter();

    T rho = 0, U = 0;
    T gamma = gas.gamma();

    qInterp.evalDensity( q, rho);
    qInterp.evalVelocity(q, U);
    output = rho * U;
  }

  template<class Tp, class T>
  void operator()(const  Tp& param,
                  const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    operator()(x, time, q, qx, output);
  }

private:
  const PDENDConvert& pde_;
};

#if 0
//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputBoltzmannD1Q3_WindowedEntropy : public OutputType< OutputBoltzmannD1Q3_WindowedEntropy<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputBoltzmannD1Q3_WindowedEntropy( const PDENDConvert& pde, const Real& t1, const Real& t2 ) :
    pde_(pde), t1_(t1), t2_(t2)
    { if (t2 < t1) SANS_DEVELOPER_EXCEPTION("Window Function error: t2 must be greater than t1"); }

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    GasModel gas = pde_.gasModel();
    QInterpret qInterp = pde_.variableInterpreter();

    T rho = 0, u = 0, t = 0;
    T gamma = gas.gamma();

    qInterp.eval( q, rho, u, t );
    T p = gas.pressure(rho, t);
    T s = log(p / pow(rho, gamma));

    Real tau = (time - t1_)/(t2_-t1_);

    T window = 0;
    if (time > t1_ && time < t2_)
      window = 2./3.*(1 - cos(2*PI*tau) )*(1 - cos(2*PI*tau) );

    output = window*s;
  }

  template<class Tp, class T>
  void operator()(const  Tp& param,
                  const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    operator()(x, time, q, qx, output);
  }

private:
  const PDENDConvert& pde_;
  const Real& t1_;
  const Real& t2_;
};

template <class PDENDConvert>
class OutputBoltzmannD1Q3_Density : public OutputType< OutputBoltzmannD1Q3_Density<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputBoltzmannD1Q3_Density( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
      GasModel gas = pde_.gasModel();
      QInterpret qInterp = pde_.variableInterpreter();

      T rho = 0, u = 0, t = 0;

      qInterp.eval( q, rho, u, t );
      output = rho;
  }

private:
  const PDENDConvert& pde_;
};

template <class PDENDConvert>
class OutputBoltzmannD1Q3_U : public OutputType< OutputBoltzmannD1Q3_U<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputBoltzmannD1Q3_U( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
      GasModel gas = pde_.gasModel();
      QInterpret qInterp = pde_.variableInterpreter();

      T rho = 0, u = 0, t = 0;

      qInterp.eval( q, rho, u, t );
      output = u;
  }

private:
  const PDENDConvert& pde_;
};


// Pressure output
template <class PDENDConvert>
class OutputBoltzmannD1Q3_Pressure : public OutputType< OutputBoltzmannD1Q3_Pressure<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputBoltzmannD1Q3_Pressure( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    T rho = 0, u = 0, t = 0;

    qInterp.eval( q, rho, u, t );
    T p = gas.pressure(rho, t);
    output = p;
  }

  template<class Tp, class T>
  void operator()(const  Tp& param,
                  const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    operator()(x, time, q, qx, output);
  }
private:
  const PDENDConvert& pde_;
};

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputBoltzmannD1Q3_WindowedPressureSquared : public OutputType< OutputBoltzmannD1Q3_WindowedPressureSquared<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;

  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputBoltzmannD1Q3_WindowedPressureSquared( const PDENDConvert& pde, const Real& t1, const Real& t2, const Real& pbar ) :
    pde_(pde), t1_(t1), t2_(t2), pbar_(pbar)
    { if (t2 < t1) SANS_DEVELOPER_EXCEPTION("Window Function error: t2 must be greater than t1"); }

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    GasModel gas = pde_.gasModel();
    QInterpret qInterp = pde_.variableInterpreter();

    T rho = 0, u = 0, t = 0;
//    T gamma = gas.gamma();

    qInterp.eval( q, rho, u, t );
    T p = gas.pressure(rho, t);

    Real tau = (time - t1_)/(t2_-t1_);

    T window = 0;
    if (time > t1_ && time < t2_)
      window = 2./3.*(1 - cos(2*PI*tau) )*(1 - cos(2*PI*tau) );

    output = window*(p-pbar_)*(p-pbar_);

  }

  template<class Tp, class T>
  void operator()(const  Tp& param,
                  const Real& x, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayJ<T>& output ) const
  {
    operator()(x, time, q, qx, output);
  }

private:
  const PDENDConvert& pde_;
  const Real& t1_;
  const Real& t2_;
  const Real& pbar_;

};
#endif

}

#endif //OUTPUTBOLTZMANND1Q3_H
