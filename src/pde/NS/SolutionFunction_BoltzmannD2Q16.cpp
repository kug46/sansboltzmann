// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/NS/SolutionFunction_BoltzmannD2Q16.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_BoltzmannD2Q16_Gaussian_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.sigma_x));
  allParams.push_back(d.checkInputs(params.sigma_y));
  d.checkUnknownInputs(allParams);
}

SolutionFunction_BoltzmannD2Q16_Gaussian_Params SolutionFunction_BoltzmannD2Q16_Gaussian_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_BoltzmannD2Q16_WeightedDensity_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.Rho));
  d.checkUnknownInputs(allParams);
}

SolutionFunction_BoltzmannD2Q16_WeightedDensity_Params SolutionFunction_BoltzmannD2Q16_WeightedDensity_Params::params;

}
