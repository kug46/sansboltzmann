// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QD1Q3PRIMITIVEFUNCTIONS_H
#define QD1Q3PRIMITIVEFUNCTIONS_H

// solution interpreter class
// Euler/N-S conservation-eqns: primitive variables (rho, u, v, p)

#include <vector>
#include <string>

#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "GasModel.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "pde/NS/BoltzmannVariableD1Q3.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 1-D Euler/N-S
// primitive variables (rho, u, p)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (rho, u, p)
class QTypePrimitiveDistributionFunctions;


// primary template
template <class QType, template <class> class PDETraitsSize>
class QD1Q3;


template <template <class> class PDETraitsSize>
class QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>
{
public:
  typedef PhysD1 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int if0 = 0;
  static const int if1 = 1;
  static const int if2 = 2;

  // The components of the state vector that make up the vector
  static const int ix = if1;

  static std::vector<std::string> stateNames() { return {"pdf0", "pdf1", "pdf2"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit QD1Q3( const GasModel& gas0 ) : gas_(gas0) {}
  QD1Q3( const QD1Q3& q0 ) : gas_(q0.gas_) {}
  ~QD1Q3() {}

  QD1Q3& operator=( const QD1Q3& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& pdf0, T& pdf1, T& pdf2 ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& pdf0_q, ArrayQ<T>& pdf1_q, ArrayQ<T>& pdf2_q ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& pdf0x,
    typename promote_Surreal<Tq,Tg>::type& pdf1x,
    typename promote_Surreal<Tq,Tg>::type& pdf2x ) const;

  template<class T>
  void evalSecondGradient(
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
      T& pdf0xx, T& pdf1xx, T& pdf2xx ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from primitive variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const PrimitiveDistributionFunctionsD1Q3<T>& data ) const;

  // set from primitive variable
 // template<class T>
 // void setFromPrimitive(
 //     ArrayQ<T>& q, const DensityVelocityTemperature1D<T>& data ) const;

  // set from variables
 // template<class T>
 // void setFromPrimitive(
 //     ArrayQ<T>& q, const Conservative1D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& pdf0, T& pdf1, T& pdf2 ) const
{
  pdf0 = q(if0);
  pdf1 = q(if1);
  pdf2 = q(if2);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  rho = q(if0) + q(if1) + q(if2);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u ) const
{
  Real e1 = -1.;
  Real e2 =  0.;
  Real e3 = +1.;
  T rho = q(if0) + q(if1) +q(if2);

  u = (e1*q(if0) + e2*q(if1) + e3*q(if2))/rho;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  const Real csq = 1/3.;
  const T& rho = q(if0) + q(if1) + q(if2);
  const T& p   = csq*rho;  // Krueger, (Book) LBM principals and practice eq. 7.15, Springer '17

  t = p/(rho * gas_.R());
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& pdf0_q, ArrayQ<T>& pdf1_q, ArrayQ<T>& pdf2_q  ) const
{
//  const T& pdf0 = q(if0);
//  const T& pdf1 = q(if1);
//  const T& pdf2 = q(if2);

  pdf0_q = 0.; pdf1_q = 0.;  pdf2_q = 0.;

  pdf0_q[if0] = 1;
  pdf1_q[if1] = 1;
  pdf2_q[if2] = 1;
}


template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& pdf0x,
    typename promote_Surreal<Tq,Tg>::type& pdf1x,
    typename promote_Surreal<Tq,Tg>::type& pdf2x ) const
{
  pdf0x   = qx(if0);
  pdf1x   = qx(if1);
  pdf2x   = qx(if2);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
    T& pdf0xx, T& pdf1xx, T& pdf2xx ) const
{
  pdf0xx = qxx(if0);
  pdf1xx = qxx(if1);
  pdf2xx = qxx(if2);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 3);
  SANS_ASSERT((name[0] == "DistributionFunction0") &&
              (name[1] == "DistributionFunction1") &&
              (name[2] == "DistributionFunction2"));

//  T pdf0, pdf1, pdf2;

  q(if0) = data[0];
  q(if1) = data[1];
  q(if2) = data[2];
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const PrimitiveDistributionFunctionsD1Q3<T>& data ) const
{
  q(if0) = data.DistributionFunction0;
  q(if1) = data.DistributionFunction1;
  q(if2) = data.DistributionFunction2;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  const Real csq = 1/3.;
  const Real rho = q(if0) + q(if1) + q(if2);
  const Real p   = csq*rho;  // Krueger, (Book) LBM principals and practice eq. 7.15, Springer '17

  const Real drho = dq(if0) + dq(if1) + dq(if2);
  const Real dp   = csq*drho;

  Real wr = 1, wp = 1;

  // density check
  Real nrho = rho - drho;

  // Compute wr such that:

  //rho - wr*drho >= (1-maxChangeFraction)*rho
  if (nrho < (1-maxChangeFraction)*rho)
    wr =  maxChangeFraction*rho/drho;

  //rho - wr*drho <= (1+maxChangeFraction)*rho
  if (nrho > (1+maxChangeFraction)*rho)
    wr = -maxChangeFraction*rho/drho;

  // pressure check
  Real np = p - dp;

  // Compute wp such that:

  //p - wp*dp >= (1-maxChangeFraction)*p
  if (np < (1-maxChangeFraction)*p)
    wp =  maxChangeFraction*p/dp;

  //p - wp*dp <= (1+maxChangeFraction)*p
  if (np > (1+maxChangeFraction)*p)
    wp = -maxChangeFraction*p/dp;

  updateFraction = MIN(wr, wp);
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  bool isPositive;
  isPositive = true;
  //const Real& rho = q(if0) + q(if1) + q(if2);
  // Stronger condition: if all pdf's > 0

#if 0
  printf( "%s: rho = %e  p = %e\n", __func__, rho, p );


  if (q(if0) > 0 && q(if1) > 0 && q(if2) > 0)
    isPositive = true;
  else
    isPositive = false;
#endif
  return isPositive;
}


template <template <class> class PDETraitsSize>
void
QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "QD1Q3<QTypePrimitiveDistributionFunctions, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

}

#endif  // QD1Q3PRIMITIVEFUNCTIONS_H
