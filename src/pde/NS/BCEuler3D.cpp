// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCEuler3D.h"
#include "Q3DPrimitiveRhoPressure.h"
#include "Q3DPrimitiveSurrogate.h"
#include "Q3DConservative.h"
//#include "Q3DEntropy.h"
#include "TraitsEuler.h"
#include "TraitsEulerArtificialViscosity.h"
#include "TraitsNavierStokes.h"
#include "TraitsRANSSA.h"
#include "BCRANSSA3D_Solution.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
// Instantiate the paramter options
PYDICT_PARAMETER_OPTION_INSTANTIATE(BCEulerSolution3DOptions::SolutionOptions)

#if 0
// cppcheck-suppress passedByValue
void BCEuler3DParams<BCTypeSymmetry>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qn));
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeSymmetry> BCEuler3DParams<BCTypeSymmetry>::params;
#endif

// cppcheck-suppress passedByValue
void BCEuler3DParams<BCTypeSymmetry_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeSymmetry_mitState> BCEuler3DParams<BCTypeSymmetry_mitState>::params;

// cppcheck-suppress passedByValue
void BCEuler3DParams<BCTypeReflect_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeReflect_mitState> BCEuler3DParams<BCTypeReflect_mitState>::params;


template<class VariableType3DParams>
BCEuler3DFullStateParams<VariableType3DParams>::BCEuler3DFullStateParams()
  : StateVector(VariableType3DParams::params.StateVector)
{}

template<class VariableType3DParams>// cppcheck-suppress passedByValue
void BCEuler3DFullStateParams<VariableType3DParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Characteristic));
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
template<class VariableType3DParams>
BCEuler3DFullStateParams<VariableType3DParams> BCEuler3DFullStateParams<VariableType3DParams>::params;

template struct BCEuler3DFullStateParams<NSVariableType3DParams>;
//template struct BCEuler3DFullStateParams<SAVariableType3DParams>;

// cppcheck-suppress passedByValue
void BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState> BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params;

#if 0
// cppcheck-suppress passedByValue
void BCEuler3DParams<BCTypeInflowSupersonic>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.rhou));
  allParams.push_back(d.checkInputs(params.rhov));
  allParams.push_back(d.checkInputs(params.rhoE));
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeInflowSupersonic> BCEuler3DParams<BCTypeInflowSupersonic>::params;

// cppcheck-suppress passedByValue
void BCEuler3DParams<BCTypeInflowSubsonic_sHqt>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.sSpec));
  allParams.push_back(d.checkInputs(params.HSpec));
  allParams.push_back(d.checkInputs(params.qtSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeInflowSubsonic_sHqt> BCEuler3DParams<BCTypeInflowSubsonic_sHqt>::params;

// cppcheck-suppress passedByValue
void BCEuler3DParams<BCTypeOutflowSubsonic_Pressure>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeOutflowSubsonic_Pressure> BCEuler3DParams<BCTypeOutflowSubsonic_Pressure>::params;
#endif
// cppcheck-suppress passedByValue
void BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.PtSpec));
  allParams.push_back(d.checkInputs(params.TtSpec));
  allParams.push_back(d.checkInputs(params.aSpec));
  allParams.push_back(d.checkInputs(params.bSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState> BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState>::params;
#if 0
void BCEuler3DParams<BCTypeInflowSubsonic_sHqt_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.sSpec));
  allParams.push_back(d.checkInputs(params.HSpec));
  allParams.push_back(d.checkInputs(params.VxSpec));
  allParams.push_back(d.checkInputs(params.VySpec));
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeInflowSubsonic_sHqt_mitState> BCEuler3DParams<BCTypeInflowSubsonic_sHqt_mitState>::params;

void BCEuler3DParams<BCTypeInflowSubsonic_sHqt_BN>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.sSpec));
  allParams.push_back(d.checkInputs(params.HSpec));
  allParams.push_back(d.checkInputs(params.aSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeInflowSubsonic_sHqt_BN> BCEuler3DParams<BCTypeInflowSubsonic_sHqt_BN>::params;
#endif

// cppcheck-suppress passedByValue
void BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN> BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN>::params;

// cppcheck-suppress passedByValue
template<template <class> class PDETraitsSize>
void BCEulerFunction_mitState3DParams< PDETraitsSize >::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.Characteristic));
  d.checkUnknownInputs(allParams);
}
template<template <class> class PDETraitsSize>
BCEulerFunction_mitState3DParams< PDETraitsSize > BCEulerFunction_mitState3DParams< PDETraitsSize >::params;

template struct BCEulerFunction_mitState3DParams< TraitsSizeEuler >;
template struct BCEulerFunction_mitState3DParams< TraitsSizeNavierStokes >;
template struct BCEulerFunction_mitState3DParams< TraitsSizeRANSSA >;
template struct BCEulerFunction_mitState3DParams< TraitsSizeEulerArtificialViscosity >;

//===========================================================================//
// Instantiate BC parameters

// Pressure-Primitive Variables
typedef BCEuler3DVector< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> > BCVector_RhoP;
BCPARAMETER_INSTANTIATE( BCVector_RhoP )

// Conservative Variables
typedef BCEuler3DVector< TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel> > BCVector_Cons;
BCPARAMETER_INSTANTIATE( BCVector_Cons )

// Entropy Variables
//typedef BCEuler3DVector< TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel> > BCVector_Entropy;
//BCPARAMETER_INSTANTIATE( BCVector_Entropy )

// Surrogate Variables
typedef BCEuler3DVector< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> > BCVector_Surrogate;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate )

} //namespace SANS
