// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Q2DPrimitiveRhoPressure.h"
#include "TraitsEuler.h"
//#include "TraitsNavierStokes.h"

namespace SANS
{

template class Q2D<QTypePrimitiveRhoPressure, TraitsSizeEuler>;
//template class Q2D<QTypePrimitiveRhoPressure, TraitsSizeNavierStokes>;

}
