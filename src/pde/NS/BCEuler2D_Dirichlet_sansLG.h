// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER2D_DIRICHLET_SANSLG_H
#define BCEULER2D_DIRICHLET_SANSLG_H

// 2-D Euler BC class for Dirichlet sansLG formulation

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsEuler.h"
#include "PDEEuler2D.h"
#include "pde/NS/SolutionFunction_Euler2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeSymmetry;
class BCTypeInflowSupersonic;
class BCTypeInflowSubsonic_sHqt;

class BCTypeFullState_mitState;

template <class SolutionType>
class BCTypeInflowSubsonic_sHqt_Profile;

class BCTypeOutflowSubsonic_Pressure;

template <class BCType, class PDEEuler2D>
class BCEuler2D;

template <class BCType>
struct BCEuler2DParams;

//----------------------------------------------------------------------------//
// slip wall BC:
//
// specify: normal velocity
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeSymmetry> : noncopyable
{
  const ParameterNumeric<Real> qn{"qn", 0, NO_RANGE, "VelocityNormal"};

  static constexpr const char* BCName{"Symmetry"};
  struct Option
  {
    const DictOption Symmetry{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeSymmetry BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCEuler2DParams<BCTypeSymmetry> ParamsType;

  static const int D = PDETraitsSize<PhysDim>::D;   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDETraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;     // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const ArrayQ<Real>& bcdata ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()), bcdata_(bcdata), qn_(bcdata(0))  {}
  ~BCEuler2D() {}

  BCEuler2D(const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    bcdata_({d.get(ParamsType::params.qn),0,0,0}),
    qn_(d.get(ParamsType::params.qn)) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  // BC residual: a(u) - b
  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T>
  void weightBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wght ) const;

  template <class T, class TL, class TR, class W>
  void weightBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wght ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const;

  template <class T, class TL, class TR, class W>
  void weightLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                       const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtLG ) const;

  // Lagrange multiplier rhs
  // ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const;

  template <class T, class TL, class TR, class R>
  void rhsLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rhsLG ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> bcdata_;
  const Real qn_;

  // BC jacobian: A = d(a)/d(uCons), where uCons is conservation variables
  template <class T>
  void jacobianBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                   const ArrayQ<T>& q, MatrixQ<T>& A ) const;

  // BC jacobian nullspace: A.N = 0
  template <class T>
  void nullspaceBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, MatrixQ<T>& N ) const;

  // units consistency matrix: M
  template <class T>
  void unitsBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                const ArrayQ<T>& q, MatrixQ<T>& M ) const;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const
{
  T rho=0, u=0, v=0, t = 0;

  qInterpret_.eval( q, rho, u, v, t );

  rsdBC(0) = (nx*u + ny*v) - qn_;
  rsdBC(1) = 0;
  rsdBC(2) = 0;
  rsdBC(3) = 0;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
{
  T rho=0, u=0, v=0, t=0, e=0, E = 0;

  qInterpret_.eval( q, rho, u, v, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v);

  rsdBC(0) = (nx*u + ny*v) - qn_;
  rsdBC(1) = lg(1) - rho*E;
  rsdBC(2) = lg(2);
  rsdBC(3) = lg(3) - rho;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param,
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeSymmetry, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param,
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeSymmetry, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtBC ) const
{
  T rho=0, u=0, v=0, t=0, h=0, H=0, qn = 0;

  qInterpret_.eval( q, rho, u, v, t );
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;

  // B^t = Fn M A^t ( A M A^t )^{-1}
  wghtBC = 0;
  wghtBC(0,0) = rho;
  wghtBC(1,0) = rho*(u + nx*qn);
  wghtBC(2,0) = rho*(v + ny*qn);
  wghtBC(3,0) = rho*(H + qn*qn);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class W>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeSymmetry, PDEEuler2D<>>::weightBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const
{
  Real gamma = gas_.gamma();
  T rho=0, u=0, v=0, t=0, qn=0, qs = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qn = nx*u + ny*v;
  qs = nx*v - ny*u;

  // \bar{A}^t = Fn N
  wghtLG = 0;
  wghtLG(0,0) = 0;
  wghtLG(1,0) = (gamma - 1)*nx;
  wghtLG(2,0) = (gamma - 1)*ny;
  wghtLG(3,0) = gamma*qn;
  wghtLG(0,1) = 0;
  wghtLG(1,1) = (gamma - 1)*nx*qs + ny*qn;
  wghtLG(2,1) = (gamma - 1)*ny*qs - nx*qn;
  wghtLG(3,1) = (gamma - 1)*qn*qs;
  wghtLG(0,2) = qn;
  wghtLG(1,2) = -0.5*(gamma - 1)*nx*(u*u + v*v) + u*qn;
  wghtLG(2,2) = -0.5*(gamma - 1)*ny*(u*u + v*v) + v*qn;
  wghtLG(3,2) = -0.5*(gamma - 1)*qn*(u*u + v*v);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class W>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeSymmetry, PDEEuler2D<>>::weightBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const
{
#if 0
  const char msg[] = "Error in rhsLagrange: not yet implemented for nonlinear\n";
  SANS_DEVELOPER_EXCEPTION( msg );
#else
  // NOTE: this is not completely correct, but serves as place holder

  T rho=0, u=0, v=0, t=0, e=0, E = 0;

  qInterpret_.eval( q, rho, u, v, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v);

  rhsLG(0) = rho*E;
  rhsLG(1) = 0;
  rhsLG(2) = rho;
  rhsLG(3) = 0;     // dummy data
#endif
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rhsLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeSymmetry, PDEEuler2D<>>::rhsLagrange not implemented with params");
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::jacobianBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& A ) const
{
  T rho=0, u=0, v=0, t = 0;

  qInterpret_.eval( q, rho, u, v, t );

  A = 0;
  A(0,0) = -(nx*u + ny*v)/rho;
  A(0,1) = nx/rho;
  A(0,2) = ny/rho;
  A(0,3) = 0;
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::nullspaceBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& N ) const
{
  T rho=0, u=0, v=0, t = 0;

  qInterpret_.eval( q, rho, u, v, t );

  N = 0;
  N(0,0) = 0;
  N(1,0) = 0;
  N(2,0) = 0;
  N(3,0) = 1;
  N(0,1) = 0;
  N(1,1) =  ny;
  N(2,1) = -nx;
  N(3,1) = 0;
  N(0,2) = 1;
  N(1,2) = u;
  N(2,2) = v;
  N(3,2) = 0;
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::unitsBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& M ) const
{
  T rho=0, u=0, v=0, t=0, p=0, e=0, h=0, E=0, H = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  E = e + 0.5*(u*u + v*v);
  H = h + 0.5*(u*u + v*v);

  M(0,0) = rho;
  M(0,1) = rho*u;
  M(0,2) = rho*v;
  M(0,3) = rho*E;
  M(1,1) = rho*u*u + p;
  M(1,2) = rho*u*v;
  M(1,3) = rho*u*H;
  M(2,2) = rho*v*v + p;
  M(2,3) = rho*v*H;
  M(3,3) = rho*H*H - h*p;

  M(1,0) = M(0,1);
  M(2,0) = M(0,2);
  M(2,1) = M(1,2);
  M(3,0) = M(0,3);
  M(3,1) = M(1,3);
  M(3,2) = M(2,3);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeSymmetry, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: bcdata_ = " << std::endl;
  bcdata_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// supersonic inflow BC:
//
// specify: conservation variables
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeInflowSupersonic> : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, NO_RANGE, "Density"};
  const ParameterNumeric<Real> rhou{"rhou", NO_DEFAULT, NO_RANGE, "MomentumX"};
  const ParameterNumeric<Real> rhov{"rhov", NO_DEFAULT, NO_RANGE, "MomentumY"};
  const ParameterNumeric<Real> rhoE{"rhoE", NO_DEFAULT, NO_RANGE, "TotalEnergy"};

  static constexpr const char* BCName{"InflowSupersonic"};
  struct Option
  {
    const DictOption InflowSupersonic{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeInflowSupersonic BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCEuler2DParams<BCTypeInflowSupersonic> ParamsType;

  static const int D = PDETraitsSize<PhysDim>::D;   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;   // total solution variables

  static const int NBC = 4;                         // total BCs

  typedef typename PDETraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;     // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const ArrayQ<Real>& bcdata ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()), bcdata_(bcdata),
      rho_(bcdata(0)), rhou_(bcdata(1)), rhov_(bcdata(2)), rhoE_(bcdata(3))  {}
  ~BCEuler2D() {}

  BCEuler2D(const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    bcdata_({d.get(ParamsType::params.rho), d.get(ParamsType::params.rhou), d.get(ParamsType::params.rhov), d.get(ParamsType::params.rhoE)} ),
    rho_(d.get(ParamsType::params.rho)),
    rhou_(d.get(ParamsType::params.rhou)),
    rhov_(d.get(ParamsType::params.rhov)),
    rhoE_(d.get(ParamsType::params.rhoE)) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T>
  void weightBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtBC ) const;

  template <class T, class TL, class TR, class W>
  void weightBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtBC ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const;

  template <class T, class TL, class TR, class W>
  void weightLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                       const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtLG ) const;

  // Lagrange multiplier rhs
  // B = ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const;

  template <class T, class TL, class TR, class R>
  void rhsLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rhsLG ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> bcdata_;
  const Real rho_;
  const Real rhou_;
  const Real rhov_;
  const Real rhoE_;

  // BC jacobian: A = d(a)/d(uCons), where uCons is conservation variables
  template <class T>
  void jacobianBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                   const ArrayQ<T>& q, MatrixQ<T>& A ) const;

  // units consistency matrix: M
  template <class T>
  void unitsBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                const ArrayQ<T>& q, MatrixQ<T>& M ) const;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const
{
  T rho, u, v, t, e, E = 0;

  qInterpret_.eval( q, rho, u, v, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v);

  rsdBC(0) = rho   - rho_;
  rsdBC(1) = rho*u - rhou_;
  rsdBC(2) = rho*v - rhov_;
  rsdBC(3) = rho*E - rhoE_;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
{
  T rho, u, v, t, e, E = 0;

  qInterpret_.eval( q, rho, u, v, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v);

  rsdBC(0) = rho   - rho_;
  rsdBC(1) = rho*u - rhou_;
  rsdBC(2) = rho*v - rhov_;
  rsdBC(3) = rho*E - rhoE_;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param,
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param,
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtBC ) const
{
  MatrixQ<T> F, G;

  F = 0;
  G = 0;
  pde_.jacobianFluxAdvective( x, y, time, q, F, G );

  wghtBC = nx*F + ny*G;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class W>
inline void
BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const ParamTuple<TL, TR, TupleClass<>>& param,
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<>>::weightBC not implemented with params");
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::jacobianBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& A ) const
{
  A = 0;
  A(0,0) = 1;
  A(1,1) = 1;
  A(2,2) = 1;
  A(3,3) = 1;
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::unitsBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& M ) const
{
  T rho, u, v, t, p, e, h, E, H;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  E = e + 0.5*(u*u + v*v);
  H = h + 0.5*(u*u + v*v);

  M(0,0) = rho;
  M(0,1) = rho*u;
  M(0,2) = rho*v;
  M(0,3) = rho*E;
  M(1,1) = rho*u*u + p;
  M(1,2) = rho*u*v;
  M(1,3) = rho*u*H;
  M(2,2) = rho*v*v + p;
  M(2,3) = rho*v*H;
  M(3,3) = rho*H*H - h*p;

  M(1,0) = M(0,1);
  M(2,0) = M(0,2);
  M(2,1) = M(1,2);
  M(3,0) = M(0,3);
  M(3,1) = M(1,3);
  M(3,2) = M(2,3);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSupersonic, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: bcdata_ = " << std::endl;
  bcdata_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// subsonic inflow BC:
//
// specify: entropy, stagnation enthalpy, tangential velocity
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeInflowSubsonic_sHqt> : noncopyable
{
  const ParameterNumeric<Real> sSpec{"sSpec", NO_DEFAULT, NO_RANGE, "Entropy"};
  const ParameterNumeric<Real> HSpec{"HSpec", NO_DEFAULT, NO_RANGE, "Stagnation Enthalpy"};
  const ParameterNumeric<Real> qtSpec{"qtSpec", NO_DEFAULT, NO_RANGE, "Tangential Velocity"};

  static constexpr const char* BCName{"InflowSubsonic_sHqt"};
  struct Option
  {
    const DictOption InflowSubsonic_sHqt{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeInflowSubsonic_sHqt BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCEuler2DParams<BCTypeInflowSubsonic_sHqt> ParamsType;

  static const int D = PDETraitsSize<PhysDim>::D;   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDETraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;     // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const ArrayQ<Real>& bcdata ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      bcdata_(bcdata), sSpec_(bcdata(0)), HSpec_(bcdata(1)), qtSpec_(bcdata(2)) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    bcdata_({d.get(ParamsType::params.sSpec), d.get(ParamsType::params.HSpec), d.get(ParamsType::params.qtSpec),0} ),
    sSpec_(d.get(ParamsType::params.sSpec)),
    HSpec_(d.get(ParamsType::params.HSpec)),
    qtSpec_(d.get(ParamsType::params.qtSpec)) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T>
  void weightBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wght ) const;

  template <class T, class TL, class TR, class W>
  void weightBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wght ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const;

  template <class T, class TL, class TR, class W>
  void weightLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                       const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtLG ) const;

  // Lagrange multiplier rhs
  // ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const;

  template <class T, class TL, class TR, class R>
  void rhsLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rhsLG ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> bcdata_;
  const Real sSpec_;
  const Real HSpec_;
  const Real qtSpec_;

  // BC jacobian: A = d(a)/d(uCons), where uCons is conservation variables
  template <class T>
  void jacobianBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                   const ArrayQ<T>& q, MatrixQ<T>& A ) const;

  // BC jacobian nullspace: A.N = 0
  template <class T>
  void nullspaceBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, MatrixQ<T>& N ) const;

  // units consistency matrix: M
  template <class T>
  void unitsBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                const ArrayQ<T>& q, MatrixQ<T>& M ) const;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const
{
  Real gamma = gas_.gamma();
  T rho, u, v, t, p, h, H, s, qt = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  s = log(p / pow(rho, gamma));
  qt = nx*v - ny*u;

  rsdBC(0) = s  - sSpec_;
  rsdBC(1) = H  - HSpec_;
  rsdBC(2) = qt - qtSpec_;
  rsdBC(3) = 0;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
{
  Real gamma = gas_.gamma();
  T rho, u, v, t, p, h, H, s, qt = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  s = log(p / pow(rho, gamma));
  qt = nx*v - ny*u;

  rsdBC(0) = s  - sSpec_;
  rsdBC(1) = H  - HSpec_;
  rsdBC(2) = qt - qtSpec_;
  rsdBC(3) = 0;   // should be LG residuals, but zeroed out for now
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtBC ) const
{
  Real gamma = gas_.gamma();
  T rho, u, v, t, c, e, h, H, qn, qt, sig, mult = 0;

  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;
  qt = nx*v - ny*u;
  sig = ((gamma + 1)*h + qn*qn)/gamma;
  mult = rho*qn/(qn*qn + c*c);

  // B^t = Fn M A^t ( A M A^t )^{-1}
  wghtBC = 0;
  wghtBC(0,0) =               mult*(-sig);
  wghtBC(1,0) = rho*(-nx*e) + mult*(-sig*u);
  wghtBC(2,0) = rho*(-ny*e) + mult*(-sig*v);
  wghtBC(3,0) =               mult*(-sig*H);
  wghtBC(0,1) =               mult*(2);
  wghtBC(1,1) = rho*(nx)    + mult*(2*u);
  wghtBC(2,1) = rho*(ny)    + mult*(2*v);
  wghtBC(3,1) =               mult*(gamma*sig + (u*u + v*v));
  wghtBC(0,2) =               mult*(-2*qt);
  wghtBC(1,2) = rho*(-v)    + mult*(-2*qt*u);
  wghtBC(2,2) = rho*( u)    + mult*(-2*qt*v);
  wghtBC(3,2) =               mult*(-2*qt*H);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class W>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<>>::weightBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const
{
  T rho, u, v, t, c, h, H, qn = 0;

  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;

  // \bar{A}^t = Fn N
  wghtLG = 0;
  wghtLG(0,0) = (qn*qn - c*c);
  wghtLG(1,0) = (qn*qn - c*c)*u;
  wghtLG(2,0) = (qn*qn - c*c)*v;
  wghtLG(3,0) = (qn*qn - c*c)*H;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class W>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<>>::weightLagrange not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const
{
  const char msg[] = "Error in rhsLagrange: not yet implemented for nonlinear\n";
  SANS_DEVELOPER_EXCEPTION( msg );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x,  const Real& y,const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rhsLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<>>::rhsLagrange not implemented with params");
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::jacobianBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& A ) const
{
  Real gamma = gas_.gamma();
  T rho, u, v, t, e, h, H, qn, qt = 0;

  qInterpret_.eval( q, rho, u, v, t );
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;
  qt = nx*v - ny*v;

  A = 0;
  A(0,0) = -(H - (u*u + v*v))/(rho*e);
  A(0,1) = -u/(rho*e);
  A(0,2) = -v/(rho*e);
  A(0,3) =  1/(rho*e);
  A(1,0) = -(H - 0.5*(gamma - 1)*(u*u + v*v))/rho;
  A(1,1) = -(gamma - 1)*u/rho;
  A(1,2) = -(gamma - 1)*v/rho;
  A(1,3) = gamma/rho;
  A(2,0) = -qt/rho;
  A(2,1) = -ny/rho;
  A(2,2) =  nx/rho;
  A(2,3) = 0;
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::nullspaceBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& N ) const
{
  T rho, u, v, t, c, h, H, qn = 0;

  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;

  N = 0;
  N(0,0) = qn;
  N(1,0) = qn*u - nx*c*c;
  N(2,0) = qn*v - ny*c*c;
  N(3,0) = qn*(H - c*c);
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::unitsBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& M ) const
{
  T rho, u, v, t, p, e, h, E, H = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  E = e + 0.5*(u*u + v*v);
  H = h + 0.5*(u*u + v*v);

  M(0,0) = rho;
  M(0,1) = rho*u;
  M(0,2) = rho*v;
  M(0,3) = rho*E;
  M(1,1) = rho*u*u + p;
  M(1,2) = rho*u*v;
  M(1,3) = rho*u*H;
  M(2,2) = rho*v*v + p;
  M(2,3) = rho*v*H;
  M(3,3) = rho*H*H - h*p;

  M(1,0) = M(0,1);
  M(2,0) = M(0,2);
  M(2,1) = M(1,2);
  M(3,0) = M(0,3);
  M(3,1) = M(1,3);
  M(3,2) = M(2,3);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: bcdata_ = " << std::endl;
  bcdata_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// subsonic inflow Profle BC:
//
// specify: entropy, stagnation enthalpy, tangential velocity
//----------------------------------------------------------------------------//

template<class SolutionFunction>
struct BCEuler2DParams< BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction> > : noncopyable
{
  const ParameterDict SolutionArgs{"SolutionArgs", EMPTY_DICT, SolutionFunction::ParamsType::checkInputs, "Solution function arguments"};

  static constexpr const char* BCName{"InflowSubsonic_sHqt_Profile"};
  struct Option
  {
    const DictOption InflowSubsonic_sHqt_Profile{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.SolutionArgs));
    d.checkUnknownInputs(allParams);
  }

  static BCEuler2DParams params;

};

// Instantiate the singleton
template<class SolutionFunction>
BCEuler2DParams< BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction> > BCEuler2DParams< BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction> >::params;


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
class BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction> BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCEuler2DParams< BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction> > ParamsType;

  static const int D = PDETraitsSize<PhysDim>::D;   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDETraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;     // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const SolutionFunction& solution ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      solution_(solution) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    solution_( d.get(ParamsType::params.SolutionArgs) ) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T>
  void weightBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wght ) const;

  template <class T, class TL, class TR, class W>
  void weightBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wght ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const;

  template <class T, class TL, class TR, class W>
  void weightLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                       const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtLG ) const;

  // Lagrange multiplier rhs
  // ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const;

  template <class T, class TL, class TR, class R>
  void rhsLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rhsLG ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const SolutionFunction solution_;

  // BC jacobian: A = d(a)/d(uCons), where uCons is conservation variables
  template <class T>
  void jacobianBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                   const ArrayQ<T>& q, MatrixQ<T>& A ) const;

  // BC jacobian nullspace: A.N = 0
  template <class T>
  void nullspaceBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, MatrixQ<T>& N ) const;

  // units consistency matrix: M
  template <class T>
  void unitsBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                const ArrayQ<T>& q, MatrixQ<T>& M ) const;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const
{
  Real gamma = gas_.gamma();
  T rho, u, v, t, p, h, H, s, qt = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  s = log(p / pow(rho, gamma));
  qt = nx*v - ny*u;

  ArrayQ<T> qSpec = solution_(x,y,time);

  T rhoSpec, uSpec, vSpec, tSpec, pSpec, hSpec, HSpec, sSpec, qtSpec = 0;
  qInterpret_.eval( qSpec, rhoSpec, uSpec, vSpec, tSpec );
  pSpec = gas_.pressure(rhoSpec, tSpec);
  hSpec = gas_.enthalpy(rhoSpec, tSpec);
  HSpec = hSpec + 0.5*(uSpec*uSpec + vSpec*vSpec);
  sSpec = log(pSpec / pow(rhoSpec, gamma));
  qtSpec = nx*vSpec - ny*uSpec;


  rsdBC(0) = s  - sSpec;
  rsdBC(1) = H  - HSpec;
  rsdBC(2) = qt - qtSpec;
  rsdBC(3) = 0;
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
{
  Real gamma = gas_.gamma();
  T rho, u, v, t, p, h, H, s, qt = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  s = log(p / pow(rho, gamma));
  qt = nx*v - ny*u;

  ArrayQ<T> qSpec = solution_(x,y,time);

  T rhoSpec, uSpec, vSpec, tSpec, pSpec, hSpec, HSpec, sSpec, qtSpec = 0;
  qInterpret_.eval( qSpec, rhoSpec, uSpec, vSpec, tSpec );
  pSpec = gas_.pressure(rhoSpec, tSpec);
  hSpec = gas_.enthalpy(rhoSpec, tSpec);
  HSpec = hSpec + 0.5*(uSpec*uSpec + vSpec*vSpec);
  sSpec = log(pSpec / pow(rhoSpec, gamma));
  qtSpec = nx*vSpec - ny*uSpec;

  rsdBC(0) = s  - sSpec;
  rsdBC(1) = H  - HSpec;
  rsdBC(2) = qt - qtSpec;
  rsdBC(3) = 0;   // should be LG residuals, but zeroed out for now
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtBC ) const
{
  Real gamma = gas_.gamma();
  T rho, u, v, t, c, e, h, H, qn, qt, sig, mult = 0;

  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;
  qt = nx*v - ny*u;
  sig = ((gamma + 1)*h + qn*qn)/gamma;
  mult = rho*qn/(qn*qn + c*c);

  // B^t = Fn M A^t ( A M A^t )^{-1}
  wghtBC = 0;
  wghtBC(0,0) =               mult*(-sig);
  wghtBC(1,0) = rho*(-nx*e) + mult*(-sig*u);
  wghtBC(2,0) = rho*(-ny*e) + mult*(-sig*v);
  wghtBC(3,0) =               mult*(-sig*H);
  wghtBC(0,1) =               mult*(2);
  wghtBC(1,1) = rho*(nx)    + mult*(2*u);
  wghtBC(2,1) = rho*(ny)    + mult*(2*v);
  wghtBC(3,1) =               mult*(gamma*sig + (u*u + v*v));
  wghtBC(0,2) =               mult*(-2*qt);
  wghtBC(1,2) = rho*(-v)    + mult*(-2*qt*u);
  wghtBC(2,2) = rho*( u)    + mult*(-2*qt*v);
  wghtBC(3,2) =               mult*(-2*qt*H);
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T, class TL, class TR, class W>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile, PDEEuler2D<>>::weightBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const
{
  T rho, u, v, t, c, h, H, qn = 0;

  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;

  // \bar{A}^t = Fn N
  wghtLG = 0;
  wghtLG(0,0) = (qn*qn - c*c);
  wghtLG(1,0) = (qn*qn - c*c)*u;
  wghtLG(2,0) = (qn*qn - c*c)*v;
  wghtLG(3,0) = (qn*qn - c*c)*H;
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T, class TL, class TR, class W>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<>, PDEEuler2D<>>::weightLagrange not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const
{
  const char msg[] = "Error in rhsLagrange: not yet implemented for nonlinear\n";
  SANS_DEVELOPER_EXCEPTION( msg );
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rhsLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<>, PDEEuler2D<>>::rhsLagrange not implemented with params");
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::jacobianBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& A ) const
{
  Real gamma = gas_.gamma();
  T rho, u, v, t, e, h, H, qn, qt = 0;

  qInterpret_.eval( q, rho, u, v, t );
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;
  qt = nx*v - ny*v;

  A = 0;
  A(0,0) = -(H - (u*u + v*v))/(rho*e);
  A(0,1) = -u/(rho*e);
  A(0,2) = -v/(rho*e);
  A(0,3) =  1/(rho*e);
  A(1,0) = -(H - 0.5*(gamma - 1)*(u*u + v*v))/rho;
  A(1,1) = -(gamma - 1)*u/rho;
  A(1,2) = -(gamma - 1)*v/rho;
  A(1,3) = gamma/rho;
  A(2,0) = -qt/rho;
  A(2,1) = -ny/rho;
  A(2,2) =  nx/rho;
  A(2,3) = 0;
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::nullspaceBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& N ) const
{
  T rho, u, v, t, c, h, H, qn = 0;

  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;

  N = 0;
  N(0,0) = qn;
  N(1,0) = qn*u - nx*c*c;
  N(2,0) = qn*v - ny*c*c;
  N(3,0) = qn*(H - c*c);
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::unitsBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& M ) const
{
  T rho, u, v, t, p, e, h, E, H = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  E = e + 0.5*(u*u + v*v);
  H = h + 0.5*(u*u + v*v);

  M(0,0) = rho;
  M(0,1) = rho*u;
  M(0,2) = rho*v;
  M(0,3) = rho*E;
  M(1,1) = rho*u*u + p;
  M(1,2) = rho*u*v;
  M(1,3) = rho*u*H;
  M(2,2) = rho*v*v + p;
  M(2,3) = rho*v*H;
  M(3,3) = rho*H*H - h*p;

  M(1,0) = M(0,1);
  M(2,0) = M(0,2);
  M(2,1) = M(1,2);
  M(3,0) = M(0,3);
  M(3,1) = M(1,3);
  M(3,2) = M(2,3);
}


template <template <class> class PDETraitsSize, class PDETraitsModel, class SolutionFunction>
void
BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile<SolutionFunction>, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::dump(
    int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_Profile, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// subsonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeOutflowSubsonic_Pressure> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSubsonic_Pressure"};
  struct Option
  {
    const DictOption OutflowSubsonic_Pressure{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeOutflowSubsonic_Pressure BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCEuler2DParams<BCTypeOutflowSubsonic_Pressure> ParamsType;

  static const int D = PDETraitsSize<PhysDim>::D;   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDETraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;     // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const ArrayQ<Real>& bcdata ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      bcdata_(bcdata), pSpec_(bcdata(0)) {}
  ~BCEuler2D() {}

  BCEuler2D( const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    bcdata_({d.get(ParamsType::params.pSpec), 0,0,0} ),
    pSpec_(d.get(ParamsType::params.pSpec) ) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T>
  void weightBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wght ) const;

  template <class T, class TL, class TR, class W>
  void weightBC( const ParamTuple<TL, TR, TupleClass<>>& param,
                 const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wght ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const;

  template <class T, class TL, class TR, class W>
  void weightLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                       const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtLG ) const;

  // Lagrange multiplier rhs
  // ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const;

  template <class T, class TL, class TR, class R>
  void rhsLagrange( const ParamTuple<TL, TR, TupleClass<>>& param,
                    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rhsLG ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDEEuler2D<PDETraitsSize, PDETraitsModel>& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> bcdata_;
  const Real pSpec_;

  // BC jacobian: A = d(a)/d(uCons), where uCons is conservation variables
  template <class T>
  void jacobianBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                   const ArrayQ<T>& q, MatrixQ<T>& A ) const;

  // BC jacobian nullspace: A.N = 0
  template <class T>
  void nullspaceBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    const ArrayQ<T>& q, MatrixQ<T>& N ) const;

  // units consistency matrix: M
  template <class T>
  void unitsBC( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                const ArrayQ<T>& q, MatrixQ<T>& M ) const;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rsdBC ) const
{
  T rho, u, v, t, p = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);

  rsdBC(0) = p - pSpec_;
  rsdBC(1) = 0;
  rsdBC(2) = 0;
  rsdBC(3) = 0;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
{
  T rho, u, v, t, p = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);

  rsdBC(0) = p - pSpec_;
  rsdBC(1) = 0;   // should be LG residuals, but zeroed out for now
  rsdBC(2) = 0;
  rsdBC(3) = 0;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtBC ) const
{
  T rho, u, v, t, h, H, qn, c = 0;

  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;

  // B^t = Fn M A^t ( A M A^t )^{-1}
  wghtBC = 0;
  wghtBC(0,0) =      qn  /(c*c);
  wghtBC(1,0) = nx + qn*u/(c*c);
  wghtBC(2,0) = ny + qn*v/(c*c);
  wghtBC(3,0) = qn + qn*H/(c*c);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class W>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<>>::weightBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<T>& wghtLG ) const
{
  T rho, u, v, t, h, H, qn = 0;

  qInterpret_.eval( q, rho, u, v, t );
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);
  qn = nx*u + ny*v;

  // \bar{A}^t = Fn N
  wghtLG = 0;
  wghtLG(0,0) = 0;
  wghtLG(1,0) = qn*u;
  wghtLG(2,0) = qn*v;
  wghtLG(3,0) = qn*(H + 0.5*(u*u + v*v));
  wghtLG(0,1) = nx;
  wghtLG(1,1) = nx*u + qn;
  wghtLG(2,1) = nx*v;
  wghtLG(3,1) = nx*H + qn*u;
  wghtLG(0,2) = ny;
  wghtLG(1,2) = ny*u;
  wghtLG(2,2) = ny*v + qn;
  wghtLG(3,2) = ny*H + qn*v;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class W>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixQ<W>& wghtLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<>>::weightLagrange not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<T>& rhsLG ) const
{
  const char msg[] = "Error in rhsLagrange: not yet implemented for nonlinear\n";
  SANS_DEVELOPER_EXCEPTION( msg );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayQ<R>& rhsLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<>>::rhsLagrange not implemented with params");
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::jacobianBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& A ) const
{
  Real gamma = gas_.gamma();
  T rho, u, v, t = 0;

  qInterpret_.eval( q, rho, u, v, t );

  A = 0;
  A(0,0) = (gamma - 1)*(0.5*(u*u + v*v));
  A(0,1) = (gamma - 1)*(-u);
  A(0,2) = (gamma - 1)*(-v);
  A(0,3) = (gamma - 1);
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::nullspaceBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& N ) const
{
  T rho, u, v, t = 0;

  qInterpret_.eval( q, rho, u, v, t );

  N = 0;
  N(0,0) = -1;
  N(1,0) = 0;
  N(2,0) = 0;
  N(3,0) = 0.5*(u*u + v*v);
  N(0,1) = 0;
  N(1,1) = 1;
  N(2,1) = 0;
  N(3,1) = u;
  N(0,2) = 0;
  N(1,2) = 0;
  N(2,2) = 1;
  N(3,2) = v;
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::unitsBC(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const ArrayQ<T>& q, MatrixQ<T>& M ) const
{
  T rho, u, v, t, p, e, h, E, H = 0;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  E = e + 0.5*(u*u + v*v);
  H = h + 0.5*(u*u + v*v);

  M(0,0) = rho;
  M(0,1) = rho*u;
  M(0,2) = rho*v;
  M(0,3) = rho*E;
  M(1,1) = rho*u*u + p;
  M(1,2) = rho*u*v;
  M(1,3) = rho*u*H;
  M(2,2) = rho*v*v + p;
  M(2,3) = rho*v*H;
  M(3,3) = rho*H*H - h*p;

  M(1,0) = M(0,1);
  M(2,0) = M(0,2);
  M(2,1) = M(1,2);
  M(3,0) = M(0,3);
  M(3,1) = M(1,3);
  M(3,2) = M(2,3);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeOutflowSubsonic_Pressure, PDEEuler2D<PDETraitsSize, PDETraitsModel>>: bcdata_ = " << std::endl;
  bcdata_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // BCEULER2D_DIRICHLET_SANSLG_H
