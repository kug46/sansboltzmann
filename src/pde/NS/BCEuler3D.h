// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER3D_H
#define BCEULER3D_H

// 3-D Euler BC class

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "tools/Tuple.h"
#include "Field/Tuple/ParamTuple.h"
#include "Topology/Dimension.h"
#include "PDEEuler3D.h"

#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "BCEuler3D_mitState.h"
#include "BCEuler3D_mitLagrange.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 3-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

//class BCTypeSymmetry;
//class BCTypeInflowSupersonic;
//class BCTypeInflowSubsonic_sHqt;

//class BCTypeFullState_mitState;

//template <class SolutionType>
//class BCTypeInflowSubsonic_sHqt_Profile;

//class BCTypeOutflowSubsonic_Pressure;

template <class BCType, class PDEEuler3D>
class BCEuler3D;

template <class BCType>
struct BCEuler3DParams;

template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCEuler3DVector = boost::mpl::vector8<
    BCNone<PhysD3,PDETraitsSize<PhysD3>::N>,
    //BCEuler3D<BCTypeSymmetry,                          PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    BCEuler3D<BCTypeSymmetry_mitState,                 PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    BCEuler3D<BCTypeReflect_mitState,                  PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    //BCEuler3D<BCTypeInflowSupersonic,                  PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    //BCEuler3D<BCTypeInflowSubsonic_sHqt,               PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    //BCEuler3D<BCTypeOutflowSubsonic_Pressure,          PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN,       PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    BCEuler3D<BCTypeFullState_mitState,                PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState,     PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    //BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState,      PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    //BCEuler3D<BCTypeInflowSubsonic_sHqt_BN,            PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    BCEuler3D<BCTypeFunction_mitState,                 PDEEuler3D<PDETraitsSize, PDETraitsModel>>
    //BCEuler3D<BCTypeFunctionInflow_sHqt_mitState,      PDEEuler3D<PDETraitsSize, PDETraitsModel>>,
    //BCEuler3D<BCTypeFunctionInflow_sHqt_BN,            PDEEuler3D<PDETraitsSize, PDETraitsModel>>
                                          >;
}

#endif // BCEULER3D_H
