// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCEuler2D.h"
#include "Q2DPrimitiveRhoPressure.h"
#include "Q2DPrimitiveSurrogate.h"
#include "Q2DConservative.h"
#include "Q2DEntropy.h"

// TODO: Rework so this is not needed here
#include "TraitsEulerArtificialViscosity.h"
#include "TraitsNavierStokes.h"
#include "TraitsRANSSA.h"
#include "SAVariable2D.h"
#include "BCRANSSA2D_Solution.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
// Instantiate the paramter options
PYDICT_PARAMETER_OPTION_INSTANTIATE(BCEulerSolution2DOptions::SolutionOptions)

// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeTimeOut>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeTimeOut> BCEuler2DParams<BCTypeTimeOut>::params;

template<class VariableType2DParams>
BCEuler2DTimeICParams<VariableType2DParams>::BCEuler2DTimeICParams()
  : StateVector(VariableType2DParams::params.StateVector)
{}

template<class VariableType2DParams>// cppcheck-suppress passedByValue
void BCEuler2DTimeICParams<VariableType2DParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
template<class VariableType2DParams>
BCEuler2DTimeICParams<VariableType2DParams> BCEuler2DTimeICParams<VariableType2DParams>::params;

template struct BCEuler2DTimeICParams<NSVariableType2DParams>;
template struct BCEuler2DTimeICParams<SAVariableType2DParams>;

// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeSymmetry>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.qn));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeSymmetry> BCEuler2DParams<BCTypeSymmetry>::params;

// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeSymmetry_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeSymmetry_mitState> BCEuler2DParams<BCTypeSymmetry_mitState>::params;

// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeInflowSupersonic>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.rhou));
  allParams.push_back(d.checkInputs(params.rhov));
  allParams.push_back(d.checkInputs(params.rhoE));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeInflowSupersonic> BCEuler2DParams<BCTypeInflowSupersonic>::params;

// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.sSpec));
  allParams.push_back(d.checkInputs(params.HSpec));
  allParams.push_back(d.checkInputs(params.qtSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeInflowSubsonic_sHqt> BCEuler2DParams<BCTypeInflowSubsonic_sHqt>::params;

// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeOutflowSubsonic_Pressure> BCEuler2DParams<BCTypeOutflowSubsonic_Pressure>::params;


template<class VariableType2DParams>
BCEuler2DFullStateParams<VariableType2DParams>::BCEuler2DFullStateParams()
  : StateVector(VariableType2DParams::params.StateVector)
{}

template<class VariableType2DParams>// cppcheck-suppress passedByValue
void BCEuler2DFullStateParams<VariableType2DParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Characteristic));
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
template<class VariableType2DParams>
BCEuler2DFullStateParams<VariableType2DParams> BCEuler2DFullStateParams<VariableType2DParams>::params;

template struct BCEuler2DFullStateParams<NSVariableType2DParams>;
template struct BCEuler2DFullStateParams<SAVariableType2DParams>;

// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.PtSpec));
  allParams.push_back(d.checkInputs(params.TtSpec));
  allParams.push_back(d.checkInputs(params.aSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState> BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState>::params;

void BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.sSpec));
  allParams.push_back(d.checkInputs(params.HSpec));
  allParams.push_back(d.checkInputs(params.VxSpec));
  allParams.push_back(d.checkInputs(params.VySpec));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState> BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState>::params;

// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState> BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params;

void BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState> BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params;

void BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.sSpec));
  allParams.push_back(d.checkInputs(params.HSpec));
  allParams.push_back(d.checkInputs(params.aSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN> BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN>::params;

void BCEuler2DParams<BCTypeInflowSubsonic_PtHa_BN>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.ptSpec));
  allParams.push_back(d.checkInputs(params.HSpec));
  allParams.push_back(d.checkInputs(params.aSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeInflowSubsonic_PtHa_BN> BCEuler2DParams<BCTypeInflowSubsonic_PtHa_BN>::params;


// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN> BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN>::params;

// cppcheck-suppress passedByValue
template<template <class> class PDETraitsSize>
void BCEulerFunction_mitState2DParams< PDETraitsSize >::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.Characteristic));
  d.checkUnknownInputs(allParams);
}
template<template <class> class PDETraitsSize>
BCEulerFunction_mitState2DParams< PDETraitsSize > BCEulerFunction_mitState2DParams< PDETraitsSize >::params;

template struct BCEulerFunction_mitState2DParams< TraitsSizeEuler >;
template struct BCEulerFunction_mitState2DParams< TraitsSizeNavierStokes >;
template struct BCEulerFunction_mitState2DParams< TraitsSizeRANSSA >;
template struct BCEulerFunction_mitState2DParams< TraitsSizeEulerArtificialViscosity >;

// cppcheck-suppress passedByValue
template<template <class> class PDETraitsSize>
void BCEuler2DTimeIC_FunctionParams< PDETraitsSize >::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
template<template <class> class PDETraitsSize>
BCEuler2DTimeIC_FunctionParams< PDETraitsSize > BCEuler2DTimeIC_FunctionParams< PDETraitsSize >::params;

template struct BCEuler2DTimeIC_FunctionParams< TraitsSizeEuler >;
template struct BCEuler2DTimeIC_FunctionParams< TraitsSizeEulerArtificialViscosity >;
template struct BCEuler2DTimeIC_FunctionParams< TraitsSizeNavierStokes >;
template struct BCEuler2DTimeIC_FunctionParams< TraitsSizeRANSSA >;

template <template <class> class PDETraitsSize>
void BCEulerSolutionInflow_sHqt_mitState2DParams< PDETraitsSize >::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
template <template <class> class PDETraitsSize>
BCEulerSolutionInflow_sHqt_mitState2DParams< PDETraitsSize > BCEulerSolutionInflow_sHqt_mitState2DParams< PDETraitsSize >::params;

template struct BCEulerSolutionInflow_sHqt_mitState2DParams< TraitsSizeEuler >;
template struct BCEulerSolutionInflow_sHqt_mitState2DParams< TraitsSizeNavierStokes >;
template struct BCEulerSolutionInflow_sHqt_mitState2DParams< TraitsSizeRANSSA >;

template <template <class> class PDETraitsSize>
void BCEulerFunctionInflow_sHqt_BN2DParams< PDETraitsSize >::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
template <template <class> class PDETraitsSize>
BCEulerFunctionInflow_sHqt_BN2DParams< PDETraitsSize > BCEulerFunctionInflow_sHqt_BN2DParams< PDETraitsSize >::params;

template struct BCEulerFunctionInflow_sHqt_BN2DParams< TraitsSizeEuler >;
template struct BCEulerFunctionInflow_sHqt_BN2DParams< TraitsSizeNavierStokes >;
template struct BCEulerFunctionInflow_sHqt_BN2DParams< TraitsSizeRANSSA >;

template <template <class> class PDETraitsSize>
void BCEulerFunctionInflow_PtHa_BN2DParams< PDETraitsSize >::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
template <template <class> class PDETraitsSize>
BCEulerFunctionInflow_PtHa_BN2DParams< PDETraitsSize > BCEulerFunctionInflow_PtHa_BN2DParams< PDETraitsSize >::params;

template struct BCEulerFunctionInflow_PtHa_BN2DParams< TraitsSizeEuler >;
template struct BCEulerFunctionInflow_PtHa_BN2DParams< TraitsSizeNavierStokes >;
template struct BCEulerFunctionInflow_PtHa_BN2DParams< TraitsSizeRANSSA >;

// cppcheck-suppress passedByValue
void BCEuler2DParams<BCTypeReflect_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeReflect_mitState> BCEuler2DParams<BCTypeReflect_mitState>::params;

void BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Characteristic));
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.V));
  allParams.push_back(d.checkInputs(params.T));
  d.checkUnknownInputs(allParams);
}
BCEuler2DParams<BCTypeFullStateRadialInflow_mitState> BCEuler2DParams<BCTypeFullStateRadialInflow_mitState>::params;


//===========================================================================//
// Instantiate BC parameters

// Pressure-Primitive Variables
typedef BCEuler2DVector< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> > BCVector_RhoP;
BCPARAMETER_INSTANTIATE( BCVector_RhoP )

// Conservative Variables
typedef BCEuler2DVector< TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel> > BCVector_Cons;
BCPARAMETER_INSTANTIATE( BCVector_Cons )

// Entropy Variables
typedef BCEuler2DVector< TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel> > BCVector_Entropy;
BCPARAMETER_INSTANTIATE( BCVector_Entropy )

// Surrogate Variables
typedef BCEuler2DVector< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> > BCVector_Surrogate;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate )

} //namespace SANS
