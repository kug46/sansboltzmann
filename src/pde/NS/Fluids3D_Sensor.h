// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FLUIDS3D_SENSOR_H
#define FLUIDS3D_SENSOR_H

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Fluids_Sensor_fwd.h"

#include <iostream>
#include <string>

namespace SANS
{

template<class PDE>
class Fluids_Sensor<PhysD3, PDE>
{
public:
  static_assert( std::is_same< PhysD3, typename PDE::PhysDim >::value, "These should be the same.");

  typedef PhysD3 PhysDim;
  static const int D = PhysDim::D;
  static const int N = PDE::N;   // total solution variables

  typedef DLA::VectorS<D,Real> VectorX;                                        // physical coordinate vector, i.e. cartesian or cylindrical

  template<class T> using ArrayQ          = typename PDE::template ArrayQ<T>;  // solution/flux arrays
  template<class T> using MatrixQ         = typename PDE::template MatrixQ<T>; // diffusion matrix/flux jacobians

  explicit Fluids_Sensor(const PDE& pde) :
    pde_(pde)
  {
    // Nothing
  }

  ~Fluids_Sensor() {}

  // variable used in calculating jump switch for artificial viscosity
  template<class T, class Tg>
  void jumpQuantity( const ArrayQ<T>& q, Tg& g ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  const PDE& pde_;
};

// Hard coded for pressure for now...
template<class PDE>
template <class T, class Tg>
void inline
Fluids_Sensor<PhysD3, PDE>::jumpQuantity(
    const ArrayQ<T>& q, Tg& g ) const
{
  T rho = 0;
  T u = 0;
  T v = 0;
  T w = 0;
  T t = 0;
  T p = 0;

  pde_.variableInterpreter().eval( q, rho, u, v, w, t );
  p  = pde_.gasModel().pressure(rho, t);

  g = p;
}


template<class PDE>
void inline
Fluids_Sensor<PhysD3, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Fluids_Sensor: pde_ =" << std::endl;
  pde_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // FLUIDS3D_SENSOR_H
