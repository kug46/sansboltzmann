// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBOLTZMANN2D_H
#define PDEBOLTZMANN2D_H

// 2-D compressible Euler

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "pde/ForcingFunctionBase.h"
#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "EulerResidCategory.h"
#include "GasModel.h"
#include "RoeBoltzmann2D.h"
//#include "HLLC2D.h"
#include "BoltzmannVariable2D.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr
#include <math.h>             // isnan

#include "UserVariables/BoltzmannNVar.h"
#include "UserVariables/BoltzmannVelocity.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q2D;


//----------------------------------------------------------------------------//
// PDE class: 2-D Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. Real)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEBoltzmann2D
{
public:

  enum EulerResidualInterpCategory
  {
    Euler_ResidInterp_Raw,
    Euler_ResidInterp_Momentum,
    Euler_ResidInterp_Entropy
  };

  enum BoltzmannEquationMode
  {
    AdvectionDiffusion,
    Hydrodynamics
  };

  enum CollisionOperatorCategory
  {
    BGK,
    BGK_feq3,
    BGK_exp
  };

  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef PDETraitsModel TraitsModel;

  typedef typename PDETraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                     // gas model

  typedef RoeBoltzmann2D<QType, PDETraitsSize> FluxType;

  typedef BoltzmannVariableType2DParams VariableType2DParams;

  typedef ForcingFunctionBase2D< PDEBoltzmann2D<PDETraitsSize, PDETraitsModel> > ForcingFunctionType;

  explicit PDEBoltzmann2D( const GasModel& gas0,
                       EulerResidualInterpCategory cat,
                       CollisionOperatorCategory omega = BGK,
                       const Real tau = 1.,
                       BoltzmannEquationMode eqMode = AdvectionDiffusion,
                       const Real Uadv = 0.,
                       const Real Vadv = 0.,
                       const RoeEntropyFix entropyFix = none ): //,
                       // const std::shared_ptr<ForcingFunctionType>& forcing = NULL) :
    gas_(gas0),
    qInterpret_(gas0),
    cat_(cat),
    omega_(omega),
    tau_(tau),
    eqMode_(eqMode),
    Uadv_(Uadv),
    Vadv_(Vadv),
    flux_(gas0, entropyFix) //,
//  forcing_(forcing)
  {
    // Nothing
  }


  PDEBoltzmann2D( const PDEBoltzmann2D& pde0 ) = delete;
  PDEBoltzmann2D& operator=( const PDEBoltzmann2D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // unsteady temporal flux: Ft(Q)
  template<class Tq, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template<class Tq, class Tf>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const;

  // master state: U(Q)
  template<class Tf, class Tq>
  void workingState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tf>& uCons, ArrayQ<Tq>& q ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    fluxAdvective(x, y, time, q, f, g);
  }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwind(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const;


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const {}

  template <class Tq, class Tg, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,
      MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const {}

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, time, q, qx, qy, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const {}

  // right-hand-side forcing function (Is technically not needed, Boltzmann uses SOURCE
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const;

  // characteristic speed (needed for timestep)
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T, class Varibles>
  void setDOFFrom(
      ArrayQ<T>& q, const BoltzmannVariableType2D<Varibles>& data ) const;

  // set from variable
  template<class Varibles>
  ArrayQ<Real> setDOFFrom( const BoltzmannVariableType2D<Varibles>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the residual-interpreter type
  EulerResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // accessors for gas model and Q interpreter
  const GasModel& gasModel() const { return gas_; }
  const QInterpret& variableInterpreter() const { return qInterpret_; }

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const GasModel gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  EulerResidualInterpCategory cat_; // Residual interp type
  CollisionOperatorCategory omega_;
  const Real tau_;
  BoltzmannEquationMode eqMode_;
  const Real Uadv_;
  const Real Vadv_;
  const FluxType flux_;
//   const std::shared_ptr<ForcingFunctionType> forcing_; 
};


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const
{

  for (int i=0; i<NVar; i++)
    uCons(i) = q(i);

}

// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tf, class Tq>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::workingState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tf>& uCons, ArrayQ<Tq>& q ) const
{
  //SANS_DEVELOPER_EXCEPTION("workingState is being used in its functional form. ERROR");
#if 0
  PrimitiveDistributionFunctions2D<Tf> data(uCons(if0), uCons(if1), uCons(if2), uCons(if3),
                                               uCons(if4), uCons(if5), uCons(if6), uCons(if7),
                                               uCons(if8), uCons(if9), uCons(if10), uCons(if11),
                                               uCons(if12), uCons(if13), uCons(if14), uCons(if15));

  qInterpret_.setFromPrimitive(q, data);
#endif
  std::cout << uCons(1) << "Ucons\n";
  std::cout << q(1) << "q\n" ;
  SANS_DEVELOPER_EXCEPTION("Used working state");
}


// unsteady conservative flux Jacobian: dU(Q)/dQ
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  dudq = 1.;
  SANS_DEVELOPER_EXCEPTION("jacobianMasterState is being used...");
#if 0
  ArrayQ<T> pdf0_q, pdf1_q, pdf2_q, pdf3_q, pdf4_q, pdf5_q, pdf6_q, pdf7_q, pdf8_q;
  ArrayQ<T> pdf9_q, pdf10_q, pdf11_q, pdf12_q, pdf13_q, pdf14_q, pdf15_q;



  //qInterpret_.eval( q, pdf0, pdf1, pdf2, pdf3, pdf4, pdf5, pdf6, pdf7, pdf8);
  qInterpret_.evalJacobian( q, pdf0_q, pdf1_q, pdf2_q, pdf3_q,
                               pdf4_q, pdf5_q, pdf6_q, pdf7_q,
                               pdf8_q, pdf9_q, pdf10_q, pdf11_q,
                               pdf12_q, pdf13_q, pdf14_q, pdf15_q);

  for (int i=0; i<16; i++)
  {
    dudq(if0,i) = pdf0_q[i];
    dudq(if1,i) = pdf1_q[i];
    dudq(if2,i) = pdf2_q[i];
    dudq(if3,i) = pdf3_q[i];
    dudq(if4,i) = pdf4_q[i];
    dudq(if5,i) = pdf5_q[i];
    dudq(if6,i) = pdf6_q[i];
    dudq(if7,i) = pdf7_q[i];
    dudq(if8,i) = pdf8_q[i];
    dudq(if9,i) = pdf9_q[i];
    dudq(if10,i) = pdf10_q[i];
    dudq(if11,i) = pdf11_q[i];
    dudq(if12,i) = pdf12_q[i];
    dudq(if13,i) = pdf13_q[i];
    dudq(if14,i) = pdf14_q[i];
    dudq(if15,i) = pdf15_q[i];
  }
#endif

}


// strong form of unsteady conservative flux: dU(Q)/dt
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& utCons ) const
{

  for (int i=0; i<NVar; i++)
    utCons(i) += qt(i);

}


// advective flux: F(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{

  for (int i = 0; i<NVar; i++)
  {
    f(i) += ew[i][0]*q(i);
    g(i) += ew[i][1]*q(i);
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  flux_.fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{

  SANS_DEVELOPER_EXCEPTION(" SpaceTime is not implemented in Boltzmann Equation Solver.");
#if 0
//  flux_.fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tq> fnx = 0;
  fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fnx );

  //Upwind temporal flux
  ArrayQ<Tq> ft = 0;
  if (nt >= 0)
    fluxAdvectiveTime( x, y, time, qL, ft );
  else
    fluxAdvectiveTime( x, y, time, qR, ft );

  f += ft*nt + fnx;
#endif
}


// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{/*
 const int e[16][2] = {   {  1,  1 }, {  -1, 1  }, {  -1,  -1 }, { 1, -1 },
                           {  4,  4 }, {  -4, 4  }, {  -4,  -4 }, { 4, -4 },
                           {  1,  4 }, {  -1, 4  }, {  -1,  -4 }, { 1, -4 },
                           {  4,  1 }, {  -4, 1  }, {  -4,  -1 }, { 4, -1 }   };*/

  dfdu = 1.;
  dgdu = 1.;
  SANS_DEVELOPER_EXCEPTION("jacobianFluxAdvective is being used");
#if 0
  // See eq. 6,7 in  Kim&Pitsch2008 10.1103/PhysRevE.78.016702
  const Real r = std::sqrt(3 + std::sqrt(6));
  const Real s = std::sqrt(3 - std::sqrt(6));
  const Real e[16][2] = {  { r, r }, { -r, r }, { r, -r }, { -r, -r }, // { r, 0 }, { -r, 0 }, { 0, r }, { 0, -r },
                           { s, s }, { -s, s }, { s, -s }, { -s, -s }, // { s, 0 }, { -s, 0 }, { 0, s }, { 0, -s },
                           { r, s }, { -r, s }, { r, -s }, { -r, -s },
                           { s, r }, { -s, r }, { s, -r }, { -s, -r }
                        };

  dfdu = 0.;
  dgdu = 0.;

  for (int i=0; i<16; i++)
  {
    dfdu(i,i) = e[i][0];
    dgdu(i,i) = e[i][1];
  }
#endif
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& Nx, const Real& Ny, MatrixQ<Tf>& mtx ) const
{
 
  SANS_DEVELOPER_EXCEPTION("JacobianFluxAdvectiveAbsVal is needed...");
#if 0
  Real nMag = sqrt(Nx*Nx + Ny*Ny);
  Real nx = Nx/nMag;
  Real ny = Ny/nMag;

  if ( std::isnan(nx) || std::isnan(ny)  ) {}
  else
  {

    Real gamma = gas_.gamma();
    Tq rho, u, v, t, h, H;

    qInterpret_.eval( q, rho, u, v, t );

    Real gm1 = gamma - 1.0;

    Tq qs  = -ny*u + nx*v;
    Tq qn  =  nx*u + ny*v;
    Tq q2  =  u*u + v*v;

    h = gas_.enthalpy(rho, t);
    H = h + 0.5*q2;

    Tq c  = gas_.speedofSound( t ) ;
    Tq c2 = c*c;

    //pre-multiply the eigenvalue by the normal magnitude to save time
    Tq eigumc = fabs(qn - c)*nMag;
    Tq eigupc = fabs(qn + c)*nMag;
    Tq eigu   = fabs(qn    )*nMag;

    mtx(0,0) += (1 - q2*gm1*0.5/c2)*eigu + (2*c*qn + q2*gm1)*eigumc*0.25/c2 + (-2*c*qn + q2*gm1)*eigupc*0.25/c2;
    mtx(1,0) += ny*qs*eigu + u*(1 - 0.5*q2*gm1/c2)*eigu + (-c*nx + u)*(2*c*qn + q2*gm1)*eigumc*0.25/c2
        + (c*nx + u)*(-2*c*qn + q2*gm1)*eigupc*0.25/c2;
    mtx(2,0) += -nx*qs*eigu + v*(1 - 0.5*q2*gm1/c2)*eigu + (-c*ny + v)*(2*c*qn + q2*gm1)*eigumc*0.25/c2
        + (c*ny + v)*(-2*c*qn + q2*gm1)*eigupc*0.25/c2;
    mtx(3,0) += -qs*qs*eigu + 0.5*q2*(1 - 0.5*q2*gm1/c2)*eigu + (H - c*qn)*(2*c*qn + q2*gm1)*eigumc*0.25/c2
        + (H+c*qn)*(-2*c*qn + q2*gm1)*eigupc*0.25/c2;

    mtx(0,1) += u*gm1*eigu/c2 - (c*nx + u*gm1)*eigumc*0.5/c2 + (c*nx - u*gm1)*eigupc*0.5/c2;
    mtx(1,1) += ny*ny*eigu + u*u*gm1*eigu/c2 - (-c*nx + u)*(c*nx + u*gm1)*eigumc*0.5/c2 + (c*nx + u)*(c*nx - u*gm1)*eigupc*0.5/c2;
    mtx(2,1) += -nx*ny*eigu + u*v*gm1*eigu/c2 - (-c*ny + v)*(c*nx + u*gm1)*eigumc*0.5/c2 + (c*ny + v)*(c*nx - u*gm1)*eigupc*0.5/c2;
    mtx(3,1) += -ny*qs*eigu + u*q2*gm1*eigu*0.5/c2 - (H - c*qn)*(c*nx + u*gm1)*eigumc*0.5/c2 + (H + c*qn)*(c*nx - u*gm1)*eigupc*0.5/c2;

    mtx(0,2) += v*gm1*eigu/c2 - (c*ny + v*gm1)*eigumc*0.5/c2 + (c*ny - v*gm1)*eigupc*0.5/c2;
    mtx(1,2) += -nx*ny*eigu + u*v*gm1*eigu/c2 - (-c*nx+u)*(c*ny + v*gm1)*eigumc*0.5/c2 + (c*nx+u)*(c*ny - v*gm1)*eigupc*0.5/c2;
    mtx(2,2) += nx*nx*eigu + v*v*gm1*eigu/c2 - (-c*ny+v)*(c*ny + v*gm1)*eigumc*0.5/c2 + (c*ny+v)*(c*ny - v*gm1)*eigupc*0.5/c2;
    mtx(3,2) += nx*qs*eigu + v*q2*gm1*eigu*0.5/c2 - (H-c*qn)*(c*ny + v*gm1)*eigumc*0.5/c2 + (H+c*qn)*(c*ny - v*gm1)*eigupc*0.5/c2;

    mtx(0,3) += -gm1*eigu/c2 + gm1*eigumc*0.5/c2 + gm1*eigupc*0.5/c2;
    mtx(1,3) += -u*gm1*eigu/c2 + (-c*nx + u)*gm1*eigumc*0.5/c2 + (c*nx+u)*gm1*eigupc*0.5/c2;
    mtx(2,3) += -v*gm1*eigu/c2 + (-c*ny + v)*gm1*eigumc*0.5/c2 + (c*ny+v)*gm1*eigupc*0.5/c2;
    mtx(3,3) += -q2*gm1*eigu*0.5/c2 + (H - c*qn)*gm1*eigumc*0.5/c2 + (H + c*qn)*gm1*eigupc*0.5/c2;
  }
#endif
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION("SpaceTime has not been implemented in Boltzmann Eqation Solver.");
//  flux_.jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, time, q, nx, ny, nt, mtx);
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& strongPDE ) const
{
  for (int i=0; i<NVar; i++)
    strongPDE(i) += ew[i][0]*qx(i) + ew[i][1]*qy(i);

}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::source(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  Tq rho = 0., U = 0., V = 0.;
  Tq e_dot_u = 0., u_dot_u = 0., feq = 0;        // terms in feq


  const Real csq = 1.;

  qInterpret_.evalDensity(q,rho);

  switch (eqMode_)
  {
    case AdvectionDiffusion:
    {
      U = Uadv_;
      V = Vadv_;
      break;
    }
    case Hydrodynamics:
    {
      qInterpret_.evalVelocity(q, U, V);
      break;
    }
    default: SANS_DEVELOPER_EXCEPTION("Unknown Equation mode (only AdvectionDiffusion and Hydrodynamics permitted).");
  }

  u_dot_u = U*U + V*V;

  switch (omega_)
  {
    case BGK:
      for (int i=0; i<NVar; i++)
      {
        e_dot_u = ew[i][0]*U + ew[i][1]*V;
        feq = ew[i][2]*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));
        source(i) -= (1./tau_)*(feq - q(i));
      }
      break;
    case BGK_feq3:
    for (int i=0; i<NVar; i++)
    {
      e_dot_u = ew[i][0]*U + ew[i][1]*V;
      feq = ew[i][2]*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq)
                            + (e_dot_u/6.)*(e_dot_u*e_dot_u - 3*u_dot_u*u_dot_u));
      source(i) -= (1./tau_)*(feq - q(i));
    }
    break;
    case BGK_exp:
    {
      Tq exp_param = 0.;
      for (int i=0; i<NVar; i++)
      {
        e_dot_u = ew[i][0]*U + ew[i][1]*V;
#ifdef NON_LB_GH
        exp_param = 2*e_dot_u - u_dot_u*u_dot_u;
#else
        exp_param = e_dot_u - u_dot_u*u_dot_u/2.;
#endif
        feq = rho*ew[i][2]*exp(exp_param);
      source(i) -= (1./tau_)*(feq - q(i));
      }
    break;
    }
    default: SANS_DEVELOPER_EXCEPTION("Unknown collision operator.");
  }

}


// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const
{
  SANS_DEVELOPER_EXCEPTION("speedCharacteristic being used...");
#if 0
  Tq t = 0, c = 0;
  Tq u = 0, v = 0;

  if (Uadv_ == 0 && Vadv_ == 0)
    qInterpret_.evalVelocity(q,u,v);
  else
  {
     u = Uadv_;
     v = Vadv_;
  }
  //qInterpret_.evalTemperature( q, t );
  c = 1;//gas_.speedofSound(t);

  speed = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;
#endif
}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const
{
  SANS_DEVELOPER_EXCEPTION("speedCharacteristic being used...");
#if 0
  Tq t = 0, c = 0;
  Tq u = 0, v = 0;

  if (Uadv_ == 0 && Vadv_ == 0)
    qInterpret_.evalVelocity(q,u,v);
  else
  {
     u = Uadv_;
     v = Vadv_;
  }
  //qInterpret_.evalTemperature( q, t );
  c = 1;//gas_.speedofSound(t);

  speed = sqrt(u*u + v*v) + c;
#endif
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::
updateFraction( const Real& x, const Real& y, const Real& time,
                const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("updateFraction being used...");
  //qInterpret_.updateFraction(q, dq, maxChangeFraction, updateFraction);
}


// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return qInterpret_.isValidState(q);
}


// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_DEVELOPER_EXCEPTION("unidentified setDOFFrom 1. But shouyld be OK. ERROR");
  SANS_ASSERT(nn >= N);
  qInterpret_.setFromPrimitive( q, data, name, nn );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Varibles>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const BoltzmannVariableType2D<Varibles>& data ) const
{
  SANS_DEVELOPER_EXCEPTION("unidentified setDOFFrom 2. But should NOT be OK. ERROR");
  qInterpret_.setFromPrimitive( q, data.cast() );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Varibles>
inline typename PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const BoltzmannVariableType2D<Varibles>& data ) const
{
  SANS_DEVELOPER_EXCEPTION("unidentified setDOFFrom 3. But shouyld be OK. ERROR");
  ArrayQ<Real> q;
  qInterpret_.setFromPrimitive( q, data.cast() );
  return q;
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const PyDict& d ) const
{
  SANS_DEVELOPER_EXCEPTION("unidentified setDOFFrom 4. But shouyld be OK. ERROR");
#if 0
  ArrayQ<Real> q;
  DictKeyPair data = d.get(BoltzmannVariableType2DParams::params.StateVector);

  if ( data == BoltzmannVariableType2DParams::params.StateVector.PrimitiveDistributionFunctions16 )
    qInterpret_.setFromPrimitive( q, PrimitiveDistributionFunctions2D<Real>(data) );
  else
    SANS_DEVELOPER_EXCEPTION(" Missing state vector option ");

  return q;
#endif
}

// --------------------- HERE! TODO TODO TODO
// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{

  int nOut = rsdPDEOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Momentum not implemented in 2D");
/*    SANS_ASSERT(nOut == N-1);
    rsdPDEOut[0] = rsdPDEIn[iCont];
    rsdPDEOut[1] = sqrt( rsdPDEIn[ixMom]*rsdPDEIn[ixMom] + rsdPDEIn[iyMom]*rsdPDEIn[iyMom] );
    rsdPDEOut[2] = rsdPDEIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdPDEOut[2+i] = rsdPDEIn[iEngy+i];*/
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 2D");
    SANS_ASSERT(nOut == 1);
//    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 2D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    for (int i = 0; i < N; i++)
      rsdPDEOut[0] += rsdPDEIn[i]*rsdPDEIn[i];

    rsdPDEOut[0] = sqrt(rsdPDEOut[0]);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{

  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
    {
      rsdAuxOut[i] = rsdAuxIn[i];
    }
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Momentum not implemented in 2D");
/*    SANS_ASSERT(nOut == N-1);
    rsdAuxOut[0] = rsdAuxIn[iCont];
    rsdAuxOut[1] = sqrt(rsdAuxIn[ixMom]*rsdAuxIn[ixMom] + rsdAuxIn[iyMom]*rsdAuxIn[iyMom]);
    rsdAuxOut[2] = rsdAuxIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdAuxOut[2+i] = rsdAuxIn[iEngy+i]; */
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 2D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    for (int i = 0; i < N; i++)
      rsdAuxOut[0] += rsdAuxIn[i]*rsdAuxIn[i];

    rsdAuxOut[0] = sqrt(rsdAuxOut[0]);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{

  int nOut = rsdBCOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Momentum not implemented in 2D");
/*    SANS_ASSERT(nOut == N-1);
    rsdBCOut[0] = rsdBCIn[iCont];
    rsdBCOut[1] = sqrt( rsdBCIn[ixMom]*rsdBCIn[ixMom] + rsdBCIn[iyMom]*rsdBCIn[iyMom] );
    rsdBCOut[2] = rsdBCIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdBCOut[2+i] = rsdBCIn[iEngy+i];*/
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 2D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    for (int i = 0; i < N; i++)
      rsdBCOut[0] += rsdBCIn[i]*rsdBCIn[i];

    rsdBCOut[0] = sqrt(rsdBCOut[0]);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}

#if 0
// return the interp type
template <template <class> class PDETraitsSize, class PDETraitsModel>
EulerResidualInterpCategory
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::category() const
{
  return cat_;
}
#endif

// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::nMonitor() const
{
  int nMon;

  if (cat_==Euler_ResidInterp_Raw) // raw residual
    nMon = N;
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
    nMon = N-1;
  else if (cat_ == Euler_ResidInterp_Entropy)
    nMon = 1;
  else
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );

  return nMon;
}

// Huh
#if 1
// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  // do nothing.
  //(*forcing_)(*this, x, y, time, forcing );
}
#endif

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  return {
          // Nothing
         };
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  switch (index)
  {
  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEBoltzmann2D::derivedQuantity - Invalid index!");
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // PDEBOLTZMANN2D_H
