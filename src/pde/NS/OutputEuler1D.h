// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTEULER1D_H
#define OUTPUTEULER1D_H

#include "PDEEuler1D.h"

#include "pde/OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Provides weights to compute a force from a weighted residual

template <class PDENDConvert>
class OutputEuler1D_Force : public OutputType< OutputEuler1D_Force<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputEuler1D_Force( const PDENDConvert& pde, const Real& wx = 1 ) :
    pde_(pde), wx_(wx) {}

  void operator()(const Real& x, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = 0;
    weight[pde_.ixMom] = wx_;
  }

private:
  const PDENDConvert& pde_;
  Real wx_;
};
//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputEuler1D_Entropy : public OutputType< OutputEuler1D_Entropy<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler1D_Entropy( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    GasModel gas = pde_.gasModel();
    QInterpret qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, t = 0;
    Real gamma = gas.gamma();

    qInterp.eval( q, rho, u, t );
    Tq p = gas.pressure(rho, t);
    Tq s = log(p / pow(rho, gamma));
    output = s;
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const  Tp& param,
                  const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    operator()(x, time, q, qx, output);
  }

private:
  const PDENDConvert& pde_;
};

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputEuler1D_WindowedEntropy : public OutputType< OutputEuler1D_WindowedEntropy<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler1D_WindowedEntropy( const PDENDConvert& pde, const Real& t1, const Real& t2 ) :
    pde_(pde), t1_(t1), t2_(t2)
    { if (t2 < t1) SANS_DEVELOPER_EXCEPTION("Window Function error: t2 must be greater than t1"); }

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    GasModel gas = pde_.gasModel();
    QInterpret qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, t = 0;
    Real gamma = gas.gamma();

    qInterp.eval( q, rho, u, t );
    Tq p = gas.pressure(rho, t);
    Tq s = log(p / pow(rho, gamma));

    Real tau = (time - t1_)/(t2_-t1_);

    Real window = 0;
    if (time > t1_ && time < t2_)
      window = 2./3.*(1 - cos(2*PI*tau) )*(1 - cos(2*PI*tau) );

    output = window*s;
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const  Tp& param,
                  const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    operator()(x, time, q, qx, output);
  }

private:
  const PDENDConvert& pde_;
  const Real& t1_;
  const Real& t2_;
};

template <class PDENDConvert>
class OutputEuler1D_Density : public OutputType< OutputEuler1D_Density<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler1D_Density( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
      GasModel gas = pde_.gasModel();
      QInterpret qInterp = pde_.variableInterpreter();

      Tq rho = 0, u = 0, t = 0;

      qInterp.eval( q, rho, u, t );
      output = rho;
  }

private:
  const PDENDConvert& pde_;
};

template <class PDENDConvert>
class OutputEuler1D_U : public OutputType< OutputEuler1D_U<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler1D_U( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
      GasModel gas = pde_.gasModel();
      QInterpret qInterp = pde_.variableInterpreter();

      Tq rho = 0, u = 0, t = 0;

      qInterp.eval( q, rho, u, t );
      output = u;
  }

private:
  const PDENDConvert& pde_;
};


// Pressure output
template <class PDENDConvert>
class OutputEuler1D_Pressure : public OutputType< OutputEuler1D_Pressure<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler1D_Pressure( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, t = 0;

    qInterp.eval( q, rho, u, t );
    Tq p = gas.pressure(rho, t);
    output = p;
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const  Tp& param,
                  const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    operator()(x, time, q, qx, output);
  }
private:
  const PDENDConvert& pde_;
};

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputEuler1D_WindowedPressureSquared : public OutputType< OutputEuler1D_WindowedPressureSquared<PDENDConvert> >
{
public:
  typedef PhysD1 PhysDim;

  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler1D_WindowedPressureSquared( const PDENDConvert& pde, const Real& t1, const Real& t2, const Real& pbar ) :
    pde_(pde), t1_(t1), t2_(t2), pbar_(pbar)
    { if (t2 < t1) SANS_DEVELOPER_EXCEPTION("Window Function error: t2 must be greater than t1"); }

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    GasModel gas = pde_.gasModel();
    QInterpret qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, t = 0;
//    T gamma = gas.gamma();

    qInterp.eval( q, rho, u, t );
    Tq p = gas.pressure(rho, t);

    Real tau = (time - t1_)/(t2_-t1_);

    Real window = 0;
    if (time > t1_ && time < t2_)
      window = 2./3.*(1 - cos(2*PI*tau) )*(1 - cos(2*PI*tau) );

    output = window*(p-pbar_)*(p-pbar_);

  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const  Tp& param,
                  const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& output ) const
  {
    operator()(x, time, q, qx, output);
  }

private:
  const PDENDConvert& pde_;
  const Real& t1_;
  const Real& t2_;
  const Real& pbar_;

};


}

#endif //OUTPUTEULER1D_H
