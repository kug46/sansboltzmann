// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER3D_MITSTATE_H
#define BCEULER3D_MITSTATE_H

// 3-D Euler BC class with mitState formulations

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "NSVariable3D.h"
#include "GasModel.h"
#include "BCEuler3D_Solution.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>
#include <memory> //shared_ptr

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 3-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeFullState_mitState;
class BCTypeSymmetry_mitState;
class BCTypeReflect_mitState;
class BCTypeOutflowSubsonic_Pressure_mitState;
class BCTypeInflowSubsonic_PtTta_mitState;
//class BCTypeInflowSubsonic_sHqt_mitState;
//class BCTypeInflowSubsonic_sHqt_BN;
class BCTypeOutflowSubsonic_Pressure_BN;
class BCTypeFunction_mitState;
//class BCTypeFunctionInflow_sHqt_mitState;
//class BCTypeFunctionInflow_sHqt_BN;

template <class BCType, class PDEEuler3D>
class BCEuler3D;

template <class BCType>
struct BCEuler3DParams;


//----------------------------------------------------------------------------//
// Conventional Full State Imposed (possibly with characteristics)
//----------------------------------------------------------------------------//

template<class VariableType3DParams>
struct BCEuler3DFullStateParams : noncopyable
{
  BCEuler3DFullStateParams();

  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};
  const ParameterOption<typename VariableType3DParams::VariableOptions>& StateVector;

  static constexpr const char* BCName{"FullState"};
  struct Option
  {
    const DictOption FullState{BCEuler3DFullStateParams::BCName, BCEuler3DFullStateParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler3DFullStateParams params;
};


template <class PDE_>
class BCEuler3D<BCTypeFullState_mitState, PDE_> :
    public BCType< BCEuler3D<BCTypeFullState_mitState, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeFullState_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler3DFullStateParams<typename PDE_::VariableType3DParams> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Variables>
  BCEuler3D( const PDE& pde, const Variables& data, const bool& upwind ) :
      pde_(pde), qInterpret_(pde_.variableInterpreter()),
      qB_(pde.setDOFFrom(data)),
      upwind_(upwind)
  {
    qInterpret_.evalVelocity( qB_, uB_, vB_, wB_ );
  }

  BCEuler3D(const PDE& pde, const PyDict& d ) :
    pde_(pde), qInterpret_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(d)),
    upwind_(d.get(ParamsType::params.Characteristic))
  {
    qInterpret_.evalVelocity( qB_, uB_, vB_, wB_ );
  }

  ~BCEuler3D() {}

  BCEuler3D( const BCEuler3D& ) = delete;
  BCEuler3D& operator=( const BCEuler3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  bool useUpwind() const { return upwind_; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // BC state
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if (upwind_)
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind( x, y, z, time, qI, qB, nx, ny, nz, Fn );
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<T> fx = 0, fy = 0, fz = 0;
      pde_.fluxAdvective(x, y, z, time, qB, fx, fy, fz);

      Fn += fx*nx + fy*ny + fz*nz;
    }
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if (upwind_)
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind( param, x, y, z, time, qI, qB, nx, ny, nz, Fn );
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<T> fx = 0, fy = 0, fz = 0;
      pde_.fluxAdvective( param, x, y, z, time, qB, fx, fy, fz );

      Fn += fx*nx + fy*ny + fz*nz;
    }
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> qB_;
  const bool upwind_;
  Real uB_, vB_, wB_;
};

// is the boundary state valid
template <class PDE>
bool
BCEuler3D<BCTypeFullState_mitState, PDE>::
isValidState(const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& qI) const
{
  if (upwind_)
  {
    // Characteristic can be either inflow or outflow
    return true;
  }
  else
  {
    Real uI, vI, wI;
    qInterpret_.evalVelocity( qI, uI, vI, wI );
    // Full state requires inflow, so of the dot product between the BC an interior must be positive
    return (uI*uB_ + vI*vB_ + wI*wB_) > 0;
  }
}


template <class PDE>
void
BCEuler3D<BCTypeFullState_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler3D<BCTypeFullState_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeFullState_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX-style symmetry/slipwall BC:
//
// specify: normal velocity
//----------------------------------------------------------------------------//

template<>
struct BCEuler3DParams<BCTypeSymmetry_mitState> : noncopyable
{
  static constexpr const char* BCName{"Symmetry_mitState"};
  struct Option
  {
    const DictOption Symmetry_mitState{BCEuler3DParams::BCName, BCEuler3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler3DParams params;
};


template <class PDE_>
class BCEuler3D<BCTypeSymmetry_mitState, PDE_> :
    public BCType< BCEuler3D<BCTypeSymmetry_mitState, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeSymmetry_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler3DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler3D( const PDE& pde ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}
  ~BCEuler3D() {}

  BCEuler3D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}

  BCEuler3D& operator=( const BCEuler3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, z, time, nx, ny, nz, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0, fy = 0, fz = 0;
    pde_.fluxAdvective(x, y, z, time, qB, fx, fy, fz);

    Fn += fx*nx + fy*ny + fz*nz;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0, fy = 0, fz = 0;
    pde_.fluxAdvective( param, x, y, z, time, qB, fx, fy, fz );

    Fn += fx*nx + fy*ny + fz*nz;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class PDE>
template <class T>
inline void
BCEuler3D<BCTypeSymmetry_mitState, PDE>::state(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rhoI=0, uI=0, vI=0, wI=0, tI=0;

  qInterpret_.eval( qI, rhoI, uI, vI, wI, tI );
  T pI = gas_.pressure(rhoI, tI);

  // subtract off the normal velocity component such that U.n == 0
  T uB = uI - nx*(uI*nx + vI*ny + wI*nz);
  T vB = vI - ny*(uI*nx + vI*ny + wI*nz);
  T wB = wI - nz*(uI*nx + vI*ny + wI*nz);

  qInterpret_.setFromPrimitive( qB, DensityVelocityPressure3D<T>(rhoI, uB, vB, wB, pI) );
}

template <class PDE>
void
BCEuler3D<BCTypeSymmetry_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler3D<BCTypeSymmetry_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeSymmetry_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeSymmetry_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Reflects state and gradient to mimic a discrete symmetry plane
//----------------------------------------------------------------------------//

template<>
struct BCEuler3DParams<BCTypeReflect_mitState> : noncopyable
{
  static constexpr const char* BCName{"Reflect_mitState"};
  struct Option
  {
    const DictOption Reflect_mitState{BCEuler3DParams::BCName, BCEuler3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler3DParams params;
};


template <class PDE_>
class BCEuler3D<BCTypeReflect_mitState, PDE_> :
    public BCType< BCEuler3D<BCTypeReflect_mitState, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeReflect_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler3DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler3D( const PDE& pde ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}
  ~BCEuler3D() {}

  BCEuler3D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}

  BCEuler3D& operator=( const BCEuler3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state gradient
  template <class T>
  void stateGradient( const Real& nx, const Real& ny, const Real& nz,
                      const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                            ArrayQ<T>& qBx,       ArrayQ<T>& qBy,       ArrayQ<T>& qBz ) const;

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, z, time, nx, ny, nz, qI, qB);
  }

  // BC state for RANS
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, z, time, nx, ny, nz, qI, qB);
  }


  // BC state for artificial viscosity
  template <int Dim, class T>
  void state( const DLA::MatrixSymS<Dim,Real>& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, z, time, nx, ny, nz, qI, qB);
  }
  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class L, class R, class T, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
  }

  // normal BC flux for RANS
  template <class T, class Tf>
  void fluxNormal( const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
  }

  // normal BC flux for artificial viscosity
  template <int Dim, class T, class Tf>
  void fluxNormal( const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class PDE>
template <class T>
inline void
BCEuler3D<BCTypeReflect_mitState, PDE>::state(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  // these might be a velocity or momentum vector
  const T& uI = qI[QInterpret::ix];
  const T& vI = qI[QInterpret::iy];
  const T& wI = qI[QInterpret::iz];

  // scalar quantities are identical in the reflection
  qB = qI;

  // normal component of the vector
  T Vn = (uI*nx + vI*ny + wI*nz);

  // subtract off twice the normal vector component to reflect it
  qB[QInterpret::ix] = uI - 2*nx*Vn;
  qB[QInterpret::iy] = vI - 2*ny*Vn;
  qB[QInterpret::iz] = wI - 2*nz*Vn;
}

template <class PDE>
template <class T>
void
BCEuler3D<BCTypeReflect_mitState, PDE>::
stateGradient( const Real& nx, const Real& ny, const Real& nz,
               const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                     ArrayQ<T>& qBx,       ArrayQ<T>& qBy,       ArrayQ<T>& qBz ) const
{
  typedef DLA::VectorS<D,T> VectorX;
  typedef DLA::MatrixS<D,D,Real> MatrixX;

  // mirror scalar components
  ArrayQ<T> qIn = (qIx*nx + qIy*ny + qIz*nz);
  qBx = qIx - 2*nx*qIn;
  qBy = qIy - 2*ny*qIn;
  qBz = qIz - 2*nz*qIn;

  // Matrix that performs the mirroring operation
  MatrixX Jr = { {1 -2*nx*nx,  -2*nx*ny,  -2*nx*nz},
                 {  -2*ny*nx, 1-2*ny*ny,  -2*ny*nz},
                 {  -2*nz*nx,  -2*nz*ny, 1-2*nz*nz} };

  VectorX gradIu = { qIx[QInterpret::ix], qIy[QInterpret::ix], qIz[QInterpret::ix] };
  VectorX gradIv = { qIx[QInterpret::iy], qIy[QInterpret::iy], qIz[QInterpret::iy] };
  VectorX gradIw = { qIx[QInterpret::iz], qIy[QInterpret::iz], qIz[QInterpret::iz] };

  // Vector gradient in the normal direction
  VectorX gradIVn = gradIu*nx + gradIv*ny + gradIw*nz;

  VectorX gradBu = Jr*(gradIu - 2*nx*gradIVn);
  VectorX gradBv = Jr*(gradIv - 2*ny*gradIVn);
  VectorX gradBw = Jr*(gradIw - 2*nz*gradIVn);

  // set the vector gradients
  qBx[QInterpret::ix] = gradBu[0]; qBy[QInterpret::ix] = gradBu[1]; qBz[QInterpret::ix] = gradBu[2];
  qBx[QInterpret::iy] = gradBv[0]; qBy[QInterpret::iy] = gradBv[1]; qBz[QInterpret::iy] = gradBv[2];
  qBx[QInterpret::iz] = gradBw[0]; qBy[QInterpret::iz] = gradBw[1]; qBz[QInterpret::iz] = gradBw[2];
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler3D<BCTypeReflect_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on interior and reflected boundary state
  pde_.fluxAdvectiveUpwind( x, y, z, time, qI, qB, nx, ny, nz, Fn );

  // Don't bother with viscous flux if it does not exist
  if (!pde_.hasFluxViscous()) return;

  // compute the mirrored gradients
  ArrayQ<T> qBx, qBy, qBz;

  stateGradient( nx, ny, nz,
                 qIx, qIy, qIz,
                 qBx, qBy, qBz );

  // Compute the viscous flux
  pde_.fluxViscous(x, y, z, time, qI, qIx, qIy, qIz, qB, qBx, qBy, qBz, nx, ny, nz, Fn);
}

// normal BC flux
template <class PDE>
template <int Dim, class T, class Tf>
inline void
BCEuler3D<BCTypeReflect_mitState, PDE>::
fluxNormal( const DLA::MatrixSymS<Dim,Real>& param,
            const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on interior and reflected boundary state
  pde_.fluxAdvectiveUpwind( param, x, y, z, time, qI, qB, nx, ny, nz, Fn );

  // Don't bother with viscous flux if it does not exist
  if (!pde_.hasFluxViscous()) return;

  // compute the mirrored gradients
  ArrayQ<T> qBx, qBy, qBz;

  stateGradient( nx, ny, nz,
                 qIx, qIy, qIz,
                 qBx, qBy, qBz );

  // Compute the viscous flux
  pde_.fluxViscous(param, x, y, z, time, qI, qIx, qIy, qIz, qB, qBx, qBy, qBz, nx, ny, nz, Fn);
}

template <class PDE>
void
BCEuler3D<BCTypeReflect_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler3D<BCTypeSymmetry_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeSymmetry_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeSymmetry_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX Style subsonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSubsonic_Pressure_mitState"};
  struct Option
  {
    const DictOption OutflowSubsonic_Pressure_mitState{BCEuler3DParams::BCName, BCEuler3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler3DParams params;
};


template <class PDE_>
class BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE_> :
    public BCType< BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeOutflowSubsonic_Pressure_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler3D( const PDE& pde, const Real pSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      pSpec_(pSpec) {}
  ~BCEuler3D() {}

  BCEuler3D( const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    pSpec_(d.get(ParamsType::params.pSpec) ) {}

  BCEuler3D& operator=( const BCEuler3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <int Dim, class T>
  void state( const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux for artificial viscosity
  template <int Dim, class T, class Tf>
  void fluxNormal( const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real pSpec_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  // compute Riemann Invariant Jm(q,n) - after Fidkowski 2004

  Real gamma = gas_.gamma();
  Real R     = gas_.R();

  T rhoI=0, uI=0, vI=0, wI=0, tI=0;
  qInterpret_.eval( qI, rhoI, uI, vI, wI, tI );
  T pI = gas_.pressure(rhoI, tI);
  T sI = pI / pow(rhoI, gamma);
  T cI = gas_.speedofSound(tI);
  T Jp = (uI*nx + vI*ny + wI*nz) + 2*cI/(gamma - 1);

  T rhoB = pow( (pSpec_/sI), (1./gamma));

  // TODO: This assumes ideal gas
  T tB = pSpec_ / (rhoB*R);
  T cB = gas_.speedofSound(tB);

  // normal velocity
  T qnB = Jp - 2*cB / (gamma - 1);

  // Cartesian velocities
  T uB = uI - nx*(uI*nx + vI*ny + wI*nz) + nx*qnB;
  T vB = vI - ny*(uI*nx + vI*ny + wI*nz) + ny*qnB;
  T wB = wI - nz*(uI*nx + vI*ny + wI*nz) + nz*qnB;

  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature3D<T>( rhoB, uB, vB, wB, tB ) );
}


template <class PDE>
template <class T, class TL, class TR>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, z, time, nx, ny, nz, qI, qB);
}


template <class PDE>
template <int Dim, class T>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
state( const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, z, time, nx, ny, nz, qI, qB);
}


// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;
}


// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
}


// normal BC flux for artificial viscosity
template <class PDE>
template <int Dim, class T, class Tf>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
fluxNormal( const DLA::MatrixSymS<Dim,Real>& param,
            const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(param, x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;
}


// is the boundary state valid
template <class PDE>
bool
BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& qI ) const
{
  bool isOK = true;

  Real rho, u, v, w, t, c;
  qInterpret_.eval( qI, rho, u, v, w, t );

  // density, temperature must be positive
  if ((rho <= 0) || (t <= 0))  return false;

  // velocity must be subsonic and outflow
  c = gas_.speedofSound(t);
  if ((u*u + v*v + w*w)/(c*c) >= 1)
  {
    std::cout << "Supersonic flow on subsonic outflow boundary" << std::endl;
    std::cout << "nx, ny, nz = " << nx << ", " << ny << ", " << nz << std::endl;
    std::cout << "u, v, w = " << u << ", " << v << ", " << w << std::endl;
    std::cout << "c = " << c << std::endl;
    std::cout << "M = " << sqrt((u*u + v*v + w*w)/(c*c)) << std::endl;
    isOK = false;
  }
  if (nx*u + ny*v + nz*w < 0)
  {
    std::cout << "Reversed flow on outflow boundary" << std::endl;
    std::cout << "nx, ny, nz = " << nx << ", " << ny << ", " << nz << std::endl;
    std::cout << "u, v, w = " << u << ", " << v << ", " << w << std::endl;
    std::cout << "V.n = " << nx*u + ny*v + nz*w << std::endl;
    isOK = false;
  }

  return isOK;
}


template <class PDE>
void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: pSpec_ = " << pSpec_ << std::endl;
}


//----------------------------------------------------------------------------//
// Conventional PX style subsonic inflow BC:
//
// specify: Stagnation Temperature, stagnation pressure, inlet flow angle to normal
//----------------------------------------------------------------------------//

template<>
struct BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState> : noncopyable
{
  const ParameterNumeric<Real> TtSpec{"TtSpec", NO_DEFAULT, NO_RANGE, "Stagnation Temperature"};
  const ParameterNumeric<Real> PtSpec{"PtSpec", NO_DEFAULT, NO_RANGE, "Stagnation Pressure"};
  const ParameterNumeric<Real> aSpec{"aSpec", NO_DEFAULT, NO_RANGE, "Flow Angle relative to positive xy-plane, evaluated CCW"};
  const ParameterNumeric<Real> bSpec{"bSpec", NO_DEFAULT, NO_RANGE, "Flow Angle relative to positive xz-plane, evaluated CCW"};

  static constexpr const char* BCName{"InflowSubsonic_PtTta_mitState"};
  struct Option
  {
    const DictOption InflowSubsonic_PtTta_mitState{BCEuler3DParams::BCName, BCEuler3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler3DParams params;
};


template <class PDE_>
class BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE_> :
    public BCType< BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeInflowSubsonic_PtTta_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler3DParams<BCTypeInflowSubsonic_PtTta_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 4;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  BCEuler3D( const PDE& pde, const Real& TtSpec, const Real& PtSpec, const Real& aSpec, const Real& bSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      TtSpec_(TtSpec), PtSpec_(PtSpec), aSpec_(aSpec), bSpec_(bSpec),
      cosa_(cos(aSpec_)), sina_(sin(aSpec_)),
      cosb_(cos(bSpec_)), sinb_(sin(bSpec_)) {}
  ~BCEuler3D() {}

  BCEuler3D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    TtSpec_(d.get(ParamsType::params.TtSpec)),
    PtSpec_(d.get(ParamsType::params.PtSpec)),
    aSpec_(d.get(ParamsType::params.aSpec)),
    bSpec_(d.get(ParamsType::params.bSpec)),
    cosa_(cos(aSpec_)), sina_(sin(aSpec_)),
    cosb_(cos(bSpec_)), sinb_(sin(bSpec_)) {}

  BCEuler3D& operator=( const BCEuler3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <int Dim, class T>
  void state( const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux for artificial viscosity
  template <int Dim, class T, class Tf>
  void fluxNormal( const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:

  // Computes the boundary mach number
  template<class T>
  bool MachB(const Real& nx, const Real& ny, const Real& nz, const ArrayQ<T>& qI, T& M) const;

  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real TtSpec_;
  const Real PtSpec_;
  const Real aSpec_;
  const Real bSpec_;
  const Real cosa_, sina_;
  const Real cosb_, sinb_;
};

template <class PDE>
template <class T>
inline bool
BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
MachB( const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, T& M ) const
{
  T rho=0, u=0, v=0, w=0, t=0;

  //preserve outgoing Riemann Invariant Jp(q,n) - after Fidkowski 2004
  qInterpret_.eval( qI, rho, u, v, w, t );

  Real R     = gas_.R();
  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  T c = gas_.speedofSound(t);
  T Jp = u*nx + v*ny + w*nz + 2*c/gmi;

  Real uN = cosa_*cosb_;
  Real vN =       sinb_;
  Real wN = sina_*cosb_;

  // u*n[0] + v*n[1] + w*n[2] = c*M*( uN*n[0] + vN*n[1] + wN*n[2] ) = c*M*ndir
  Real dn = uN*nx + vN*ny + wN*nz;

  // maximum possible J_plus for real M roots
  Real Jpmax = sqrt(2*(2+gmi*dn*dn)*gamma*R*TtSpec_)/gmi;

  if ( fabs(Jp) > Jpmax )
  {
    // Mach from quadratic equation with Jpmax instead of Jp; note only one root
    M = -dn;
    return true;
  }

  T A =    gamma*R*TtSpec_*dn*dn - gmi/2*Jp*Jp;
  T B = 4.*gamma*R*TtSpec_*dn / gmi;
  T C = 4.*gamma*R*TtSpec_/(gmi*gmi) - Jp*Jp;
  T D = B*B - 4*A*C;

  // Check that we have a valid state
  if ( D < 0 ) return false;

  T Mplus  = (-B + sqrt(D))/(2.*A);
  T Mminus = (-B - sqrt(D))/(2.*A);

  // Pick physically relevant solution
  if ((Mplus >= 0.0) && (Mminus <= 0.0))
  {
    M = Mplus;
  }
  else if ((Mplus <= 0.0) && (Mminus >= 0.0))
  {
    M = Mminus;
  }
  else if ((Mplus > 0.0) && (Mminus > 0.0))
  {
    // not clear which is physical...
    // return invalid state
    return false;
#if 0
    // pick smaller for now
    if ( Mminus <= Mplus )
      M = Mminus;
    else
      M = Mplus;
#endif
  }
  else
  {
    // negative Mach means reverse flow and is OK; choose smallest Mach
    if (fabs(Mminus) < fabs(Mplus))
      M = Mminus;
    else
      M = Mplus;
  }

  // the state is valid
  return true;
}


template <class PDE>
template <class T>
inline void
BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T M = 0;
  MachB( nx, ny, nz, qI, M );

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  // set boundary state
  T tB   = TtSpec_/(1.+0.5*gmi*M*M);
  T pB   = PtSpec_/pow( 1.+0.5*gmi*M*M, gamma/gmi );
  T rhoB = gas_.density(pB, tB);

  T cB = gas_.speedofSound(tB);
  T VB = cB*M;
  T uB = VB*cosa_*cosb_;
  T vB = VB*      sinb_;
  T wB = VB*sina_*cosb_;

  qInterpret_.setFromPrimitive( qB, DensityVelocityPressure3D<T>( rhoB, uB, vB, wB, pB ) );
}


template <class PDE>
template <class T, class TL, class TR>
inline void
BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, z, time, nx, ny, nz, qI, qB);
}


template <class PDE>
template <int Dim, class T>
inline void
BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
state( const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, z, time, nx, ny, nz, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;
}


// normal BC flux
template <class PDE>
template <class T, class L, class R, class Tf>
inline void
BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
}


// normal BC flux for artificial viscosity
template <class PDE>
template <int Dim, class T, class Tf>
inline void
BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
fluxNormal( const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(param, x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;
}

// is the boundary state valid
template <class PDE>
bool
BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q) const
{
  Real M;
  return MachB( nx, ny, nz, q, M );
}


template <class PDE>
void
BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


#if 0   // not implemented for 3D
//----------------------------------------------------------------------------//
// Conventional PX style subsonic inflow BC:
//
// specify: entropy, stagnation enthalpy, vertical velocity
//----------------------------------------------------------------------------//

template<>
struct BCEuler3DParams<BCTypeInflowSubsonic_sHqt_mitState> : noncopyable
{
  const ParameterNumeric<Real> sSpec{"sSpec", NO_DEFAULT, 0, NO_LIMIT, "Entropy"};
  const ParameterNumeric<Real> HSpec{"HSpec", NO_DEFAULT, 0, NO_LIMIT, "Stagnation Enthalpy"};
  const ParameterNumeric<Real> VxSpec{"VxSpec", NO_DEFAULT, NO_RANGE, "Reference x-velocity"};
  const ParameterNumeric<Real> VySpec{"VySpec", NO_DEFAULT, NO_RANGE, "Reference y-velocity"};

  static constexpr const char* BCName{"InflowSubsonic_sHqt_mitState"};
  struct Option
  {
    const DictOption InflowSubsonic_sHqt_mitState{BCEuler3DParams::BCName, BCEuler3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler3DParams params;
};


template <class PDE_>
class BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE_> :
    public BCType< BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeInflowSubsonic_sHqt_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler3DParams<BCTypeInflowSubsonic_sHqt_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler3D( const PDE& pde, const Real& sSpec, const Real& HSpec,
             const Real& VxSpec, const Real& VySpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      sSpec_(sSpec), HSpec_(HSpec), VxSpec_(VxSpec), VySpec_(VySpec),
      exps_( exp( sSpec_ ) ) {}
  ~BCEuler3D() {}

  BCEuler3D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    sSpec_(d.get(ParamsType::params.sSpec)),
    HSpec_(d.get(ParamsType::params.HSpec)),
    VxSpec_(d.get(ParamsType::params.VxSpec)),
    VySpec_(d.get(ParamsType::params.VySpec)),
    exps_( exp( sSpec_ ) ) {}

  BCEuler3D& operator=( const BCEuler3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:

  // Computes the boundary mach number
  template<class T>
  bool VelB(const Real& nx, const Real& ny, const Real& nz, const ArrayQ<T>& qI, T& M) const;

  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real sSpec_;
  const Real HSpec_;
  const Real VxSpec_;
  const Real VySpec_;
  const Real exps_;

};

template <class PDE>
template <class T>
inline bool
BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
VelB( const Real& nx, const Real& ny, const Real& nz,
      const ArrayQ<T>& qI, T& Vn ) const
{
  T rho=0, u=0, v=0, t=0;

  //preserve outgoing Riemann Invariant Jp(q,n) - after Fidkowski 2004
  qInterpret_.eval( qI, rho, u, v, t );

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // Specified tangential velocity component
  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

  T c = gas_.speedofSound(t);
  T Jp = u*nx + v*ny + 2*c/gmi;

  Real A = 1 + 2.0/gmi;
  T    B = -2*Jp;
  T    C = Jp*Jp - 4*HSpec_/gmi + 2/gmi*vtSpec*vtSpec;
  T    D = B*B-4*A*C;

  // Check that we have a valid state
  if ( D < 0 ) return false;

  T Vnplus  = (-B + sqrt(D))/(2.*A);
  T Vnminus = (-B - sqrt(D))/(2.*A);

  // Pick physically relevant solution
  if ((Vnplus <= 0.0) && (Vnminus >= 0.0))
  {
    Vn = Vnplus;
  }
  else if ((Vnplus >= 0.0) && (Vnminus <= 0.0))
  {
    Vn = Vnminus;
  }
  else if ((Vnplus < 0.0) && (Vnminus < 0.0))
  {
    // not clear which is physical...
    // return invalid state
    return false;
#if 0
    // pick smaller for now
    if ( fabs(Vnminus) <= fabs(Vnplus) )
      Vn = Vnminus;
    else
      Vn = Vnplus;
#endif
  }
  else
  {
    // positive Vn means reverse flow and is OK; choose smallest Vn
    if (Vnminus < Vnplus)
      Vn = Vnminus;
    else
      Vn = Vnplus;
  }

  // the state is valid
  return true;
}


template <class PDE>
template <class T>
inline void
BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T Vn = 0;
  VelB( nx, ny, nz, qI, Vn );

  Real R     = gas_.R();
  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // Specified tangential velocity component
  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

  // set boundary state
  T uB   = Vn*nx + vtSpec*tx;
  T vB   = Vn*ny + vtSpec*ty;
  T tB   = (HSpec_ - 0.5*(uB*uB + vB*vB))/gas_.Cp();
  T rhoB = pow(R*tB/exps_, 1./gmi);

  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature3D<T>( rhoB, uB, vB, tB ) );
}


template <class PDE>
template <class T, class TL, class TR>
inline void
BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, z, time, nx, ny, nz, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, z, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;
}


// normal BC flux
template <class PDE>
template <class T, class L, class R, class Tf>
inline void
BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
}

// is the boundary state valid
template <class PDE>
bool
BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q) const
{
  Real Vn = 0;
  return VelB( nx, ny, nz, q, Vn );
}


template <class PDE>
void
BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeInflowSubsonic_sHqt_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}
#endif


#if 0   // not implemented for 3D
//----------------------------------------------------------------------------//
// Berg Nordstrom Style subsonic inflow BC:
//
// specify: entropy, Stagnation enthalpy, vertical velocity
//----------------------------------------------------------------------------//

template<>
struct BCEuler3DParams<BCTypeInflowSubsonic_sHqt_BN> : noncopyable
{
  const ParameterNumeric<Real> sSpec{"sSpec", NO_DEFAULT, 0, NO_LIMIT, "Entropy"};
  const ParameterNumeric<Real> HSpec{"HSpec", NO_DEFAULT, 0, NO_LIMIT, "Stagnation Enthalpy"};
  const ParameterNumeric<Real> aSpec{"aSpec", NO_DEFAULT, NO_RANGE, "Angle of attack = arctan(v/u)"};

  static constexpr const char* BCName{"InflowSubsonic_sHqt_BN"};
  struct Option
  {
    const DictOption InflowSubsonic_sHqt_BN{BCEuler3DParams::BCName, BCEuler3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler3DParams params;
};


template <class PDE_>
class BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE_> :
    public BCType< BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeInflowSubsonic_sHqt_BN BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler3DParams<BCTypeInflowSubsonic_sHqt_BN> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler3D( const PDE& pde, const Real& sSpec, const Real& HSpec, const Real& aSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      sSpec_(sSpec), HSpec_(HSpec), aSpec_(aSpec) {}
  ~BCEuler3D() {}

  BCEuler3D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    sSpec_(d.get(ParamsType::params.sSpec)),
    HSpec_(d.get(ParamsType::params.HSpec)),
    aSpec_(d.get(ParamsType::params.aSpec)) {}

  BCEuler3D& operator=( const BCEuler3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real sSpec_;
  const Real HSpec_;
  const Real aSpec_;
};



template <class PDE>
template <class T>
inline void
BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}


template <class PDE>
template <class T, class TL, class TR>
inline void
BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, z, time, nx, ny, nz, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, z, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  T rho=0, u=0, v=0, t=0;

  //compute BN Flux
  qInterpret_.eval( qI, rho, u, v, t );
  T p = gas_.pressure(rho,t);

  T H = gas_.enthalpy(rho, t) + 0.5*(u*u + v*v + w*w);
  T c = gas_.speedofSound(t);
  T s = log( p /pow(rho,gamma) );

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // normal and tangential velocities
  T vn = u*nx + v*ny;
  T vt = u*tx + v*ty;
  T V2 = u*u + v*v + w*w;
  T V = sqrt(V2);

  // Specified tangential velocity component
//  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

  // Strong BC's that are imposed
  T ds  = s  - sSpec_;
  T dH  = H  - HSpec_;
  T dvt = v/sqrt(u*u + v*v + w*w) - sin(aSpec_);

  //COMPUTE NORMAL FLUX: DERIVED FROM ENTROPY VARS
  Fn[pde_.iCont] -= -rho*vn/gmi*ds;
  Fn[pde_.iCont] -= vn*gamma*rho/(c*c + V2*gmi)*dH;
  Fn[pde_.iCont] -=  -vt*V2*V*rho*gamma/u/(c*c+V2*gmi)*dvt;

  Fn[pde_.ixMom] -= -rho*(c*c*nx + u*vn*gamma)/(gamma*gmi)*ds;
  Fn[pde_.ixMom] -= rho*(c*c*nx + u*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
  Fn[pde_.ixMom] -= -(rho*V2*V*(c*c*v + vn*(v*vn*gmi+u*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;

  Fn[pde_.iyMom] -=  -rho*(c*c*ny+v*vn*gamma*rho)/(gamma*gmi)*ds;
  Fn[pde_.iyMom] -= rho*(c*c*ny + v*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
  Fn[pde_.iyMom] -= (rho*V2*V*(c*c*u + vn*(u*vn*gmi-v*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;

  Fn[pde_.iEngy] -= -rho*vn*( 2*c*c + V2*gmi)/(2*gmi*gmi)*ds;
  Fn[pde_.iEngy] -= (rho*vn*(c*c*(4*gamma-2) + V2*(2-5*gamma+3*gamma*gamma)))/(2*gmi*(c*c + V2*gmi) )*dH;
  Fn[pde_.iEngy] -=  -(vt*V2*V*rho*gamma*(2*c*c+V2*gmi))/(2*u*gmi*(c*c+V2*gmi))*dvt;

}


// normal BC flux
template <class PDE>
template <class T, class L, class R, class Tf>
inline void
BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE>::
fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
}

template <class PDE>
bool
BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE>::isValidState(
    const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const
{
  Real rho=0, u=0, v=0, t=0;
  qInterpret_.eval( q, rho, u, v, t );

  // Check that the normal velocity is negative (inflow)
  return (u*nx + v*ny) < 0;
}


template <class PDE>
void
BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler3D<BCTypeInflowSubsonic_sHqt_BN, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}
#endif


//----------------------------------------------------------------------------//
// Berg-Nordstrom style subsonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSubsonic_Pressure_BN"};
  struct Option
  {
    const DictOption OutflowSubsonic_Pressure_BN{BCEuler3DParams::BCName, BCEuler3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler3DParams params;
};


template <class PDE_>
class BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE_> :
    public BCType< BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeOutflowSubsonic_Pressure_BN BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler3DParams<BCTypeOutflowSubsonic_Pressure_BN> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler3D( const PDE& pde, const Real pSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      pSpec_(pSpec) {}
  ~BCEuler3D() {}

  BCEuler3D( const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    pSpec_(d.get(ParamsType::params.pSpec) ) {}

  BCEuler3D& operator=( const BCEuler3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real pSpec_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}


template <class PDE>
template <class T, class TL, class TR>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, z, time, nx, ny, nz, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;

  //BERG NORDSTROM BC
  Real gamma = gas_.gamma();

  T rho, u, v, w, t, p, vn;
  qInterpret_.eval(qI, rho, u, v, w, t);
  p = gas_.pressure(rho,t);
  vn = u*nx + v*ny + w*nz;

  Fn[pde_.ixMom] -= ( (p-pSpec_) - rho*vn*vn/gamma*log(p/pSpec_) )*nx;
  Fn[pde_.iyMom] -= ( (p-pSpec_) - rho*vn*vn/gamma*log(p/pSpec_) )*ny;
  Fn[pde_.izMom] -= ( (p-pSpec_) - rho*vn*vn/gamma*log(p/pSpec_) )*nz;
  Fn[pde_.iEngy] -= ( (p-pSpec_) - rho*vn*vn/gamma*log(p/pSpec_) )*vn;
}


// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
}


template <class PDE>
bool
BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::isValidState(
    const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const
{
  Real rho=0, u=0, v=0, w=0, t=0;
  qInterpret_.eval( q, rho, u, v, w, t );

  // Check that the normal velocity is positive (outflow)
  return (u*nx + v*ny + w*nz) > 0;
}


template <class PDE>
void
BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler3D<BCTypeOutflowSubsonic_Pressure_BN, PDE>: pSpec_ = " << pSpec_ << std::endl;
}


//----------------------------------------------------------------------------//
// Full state with Solution Function
//----------------------------------------------------------------------------//
template<template <class> class PDETraitsSize>
struct BCEulerFunction_mitState3DParams : BCEulerSolution3DParams<PDETraitsSize>
{
  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};

  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{BCEulerFunction_mitState3DParams::BCName, BCEulerFunction_mitState3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEulerFunction_mitState3DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler3D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCEuler3D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeFunction_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEulerFunction_mitState3DParams<PDETraitsSize> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 5;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function3DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCEuler3D( const PDE& pde, const Function_ptr& solution, const bool& upwind ) :
      pde_(pde), solution_(solution), upwind_(upwind) {}
  ~BCEuler3D() {}

  BCEuler3D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    solution_( BCEulerSolution3D<PDETraitsSize, PDETraitsModel>::getSolution(d) ),
    upwind_(d.get(ParamsType::params.Characteristic)) {}

  BCEuler3D( const BCEuler3D& ) = delete;
  BCEuler3D& operator=( const BCEuler3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*solution_)(x,y,z,time);
  }

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, z, time, nx, ny, nz, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const
  {
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCEuler3D<BCTypeFunction_mitState, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
  }

protected:
  const PDE& pde_;
  const Function_ptr solution_;
  const bool upwind_;
};

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tf>
inline void
BCEuler3D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::
fluxNormal( const Real& x, const Real& y,  const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  if (upwind_)
  {
    // Upwind advective flux based on the boundary state
    pde_.fluxAdvectiveUpwind(x, y, z, time, qI, qB, nx, ny, nz, Fn);
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0, fy = 0, fz = 0;
    pde_.fluxAdvective(x, y, z, time, qB, fx, fy, fz);
    Fn += fx*nx + fy*ny + fz*nz;
  }

  //Evaluate viscous flux using boundary state qB and interior state gradient qIx, qIy
  ArrayQ<T> fv = 0, gv = 0, hv = 0;
  pde_.fluxViscous( x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv );

  Fn += fv*nx + gv*ny + hv*nz; //add normal component of viscous flux
}

} //namespace SANS

#endif  // BCEULER3D_MITSTATE_H
