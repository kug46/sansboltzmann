// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_RANSSA2D_H
#define SOLUTIONFUNCTION_RANSSA2D_H

// 2-D RANSSA PDE: exact and MMS solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // sin, cos

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "NSVariable2D.h"
#include "SAVariable2D.h"
#include "QRANSSA2D.h"

#include "pde/AnalyticFunction/Function2D.h"

#include <boost/preprocessor/cat.hpp>

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q2D;

//----------------------------------------------------------------------------//
// solution: const

struct SolutionFunction_RANSSA2D_Const_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, NO_RANGE, "Constant of the density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Constant of the x-velocity"};
  const ParameterNumeric<Real> v{"v", NO_DEFAULT, NO_RANGE, "Constant of the y-velocity"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, NO_RANGE, "Constant of the pressure"};
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "Constant SA Nu tilde"};

  static void checkInputs(PyDict d);

  static SolutionFunction_RANSSA2D_Const_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_RANSSA2D_Const :
    public Function2DVirtualInterface<SolutionFunction_RANSSA2D_Const<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_RANSSA2D_Const_Params ParamsType;

  typedef typename PDETraitsModel::QType QType;
  typedef QRANSSA2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_RANSSA2D_Const( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
    rho_(d.get(ParamsType::params.rho)),
    u_(d.get(ParamsType::params.u)),
    v_(d.get(ParamsType::params.v)),
    p_(d.get(ParamsType::params.p)),
    nt_(d.get(ParamsType::params.nt)) {}
  explicit SolutionFunction_RANSSA2D_Const( const GasModel& gas, const Real& rho, const Real& u, const Real& v, const Real& p, const Real& nt  ) :
      qinterp_(gas), rho_(rho), u_(u), v_(v), p_(p), nt_(nt) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> q = 0;
    SAnt2D<DensityVelocityPressure2D<T>> dvp( {rho_, u_, v_, p_, nt_} );
    qinterp_.setFromPrimitive(q, dvp);
    return q;
  }

private:
  const QInterpret qinterp_;
  const Real rho_;
  const Real u_;
  const Real v_;
  const Real p_;
  const Real nt_;
};

//----------------------------------------------------------------------------//
struct SolutionFunction_RANSSA2D_Wake_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> wakedepth{"Wakedepth", NO_DEFAULT, NO_RANGE, "Constant wake depth"};
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, NO_RANGE, "Constant of the density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Constant of the x-velocity"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, NO_RANGE, "Constant of the pressure"};
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "Constant of SA nu tilde"};

  static void checkInputs(PyDict d);

  static SolutionFunction_RANSSA2D_Wake_Params params;
};


//----------------------------------------------------------------------------//
// solution: gaussian velocity wake centered at y = 0.5
template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_RANSSA2D_Wake :
    public Function2DVirtualInterface<SolutionFunction_RANSSA2D_Wake<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDETraitsModel::QType QType;
  typedef QRANSSA2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_RANSSA2D_Wake_Params ParamsType;

  explicit SolutionFunction_RANSSA2D_Wake( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )), wakeDepth_(d.get(ParamsType::params.wakedepth)),
      rho_(d.get(ParamsType::params.rho)), u_(d.get(ParamsType::params.u)), p_(d.get(ParamsType::params.p)), nt_(d.get(ParamsType::params.nt))  {}
  explicit SolutionFunction_RANSSA2D_Wake(  const GasModel& gas, const Real wakeDepth, const Real rho, const Real u, const Real p, const Real nt ) :
      qinterp_(gas), wakeDepth_(wakeDepth), rho_(rho), u_(u), p_(p), nt_(nt) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T rho = rho_;
    T u = u_*( 1.0 - wakeDepth_*exp( -(y-0.5)*(y-0.5)*1000.0 ) );
    T v = 0.0;
    T p = p_;
    T nt = nt_*( 1.0 + 5.0*exp( -(y-0.5)*(y-0.5)*1000.0 ) );

    ArrayQ<T> q = 0;
    SAnt2D<DensityVelocityPressure2D<T>> dvp( {rho, u, v, p, nt} );
    qinterp_.setFromPrimitive(q, dvp);

    return q;
  }

private:
  const QInterpret qinterp_;
  const Real wakeDepth_;
  const Real rho_;
  const Real u_;
  const Real p_;
  const Real nt_;
};


//----------------------------------------------------------------------------//
struct SolutionFunction_RANSSA2D_OjedaMMS_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> ntRef{"ntRef", 1, 0, NO_LIMIT, "reference value for nt"};

  static void checkInputs(PyDict d);

  static SolutionFunction_RANSSA2D_OjedaMMS_Params params;
};


//----------------------------------------------------------------------------//
// solution: gaussian velocity wake centered at y = 0.5
template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_RANSSA2D_OjedaMMS :
    public Function2DVirtualInterface<SolutionFunction_RANSSA2D_OjedaMMS<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDETraitsModel::QType QType;
  typedef QRANSSA2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_RANSSA2D_OjedaMMS_Params ParamsType;

  explicit SolutionFunction_RANSSA2D_OjedaMMS( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) ), d.get(ParamsType::params.ntRef)),
      ntRef_(d.get(ParamsType::params.ntRef))  {}
  explicit SolutionFunction_RANSSA2D_OjedaMMS(  const GasModel& gas, const Real& ntRef = 1 ) :
      qinterp_(gas, ntRef), ntRef_(ntRef) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T rho, u, v, p, nt;

    //POSITIVE BUMP
    rho = 1.0 + 0.1*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    u = 0.2 + 0.3*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    v =  0.1 + 0.15*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    p = pow(rho, 1.4);
    nt = (0.001/ntRef_)*( 1 + 10.*exp(-(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200 ) );

    ArrayQ<T> q = 0;
    SAnt2D<DensityVelocityPressure2D<T>> dvp( {rho, u, v, p, nt} );
    qinterp_.setFromPrimitive(q, dvp);

    return q;
  }

private:
  const QInterpret qinterp_;
  const Real ntRef_;
};

//----------------------------------------------------------------------------//
// solution: - Trig function MMS Solution Function
struct SolutionFunction_RANSSA2D_TrigMMS_Params : noncopyable
{
  const ParameterDict gasModel{"gasModel", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> L{"L", NO_DEFAULT, NO_RANGE, "Reference lenght"};

  const ParameterNumeric<Real>   r0{  "r0", NO_DEFAULT, NO_RANGE, "Density shift"};
  const ParameterNumeric<Real>   rx{  "rx", NO_DEFAULT, NO_RANGE, "Density x-amplitude"};
  const ParameterNumeric<Real>   ry{  "ry", NO_DEFAULT, NO_RANGE, "Density y-amplitude"};
  const ParameterNumeric<Real>  rxy{ "rxy", NO_DEFAULT, NO_RANGE, "Density xy-amplitude"};
  const ParameterNumeric<Real>  arx{ "arx", NO_DEFAULT, NO_RANGE, "Density x-frequency"};
  const ParameterNumeric<Real>  ary{ "ary", NO_DEFAULT, NO_RANGE, "Density y-frequency"};
  const ParameterNumeric<Real> arxy{"arxy", NO_DEFAULT, NO_RANGE, "Density xy-frequency"};

  const ParameterNumeric<Real>   u0{  "u0", NO_DEFAULT, NO_RANGE, "u-Velocity shift"};
  const ParameterNumeric<Real>   ux{  "ux", NO_DEFAULT, NO_RANGE, "u-Velocity x-amplitude"};
  const ParameterNumeric<Real>   uy{  "uy", NO_DEFAULT, NO_RANGE, "u-Velocity y-amplitude"};
  const ParameterNumeric<Real>  uxy{ "uxy", NO_DEFAULT, NO_RANGE, "u-Velocity xy-amplitude"};
  const ParameterNumeric<Real>  aux{ "aux", NO_DEFAULT, NO_RANGE, "u-Velocity x-frequency"};
  const ParameterNumeric<Real>  auy{ "auy", NO_DEFAULT, NO_RANGE, "u-Velocity y-frequency"};
  const ParameterNumeric<Real> auxy{"auxy", NO_DEFAULT, NO_RANGE, "u-Velocity xy-frequency"};

  const ParameterNumeric<Real>   v0{  "v0", NO_DEFAULT, NO_RANGE, "v-Velocity shift"};
  const ParameterNumeric<Real>   vx{  "vx", NO_DEFAULT, NO_RANGE, "v-Velocity x-amplitude"};
  const ParameterNumeric<Real>   vy{  "vy", NO_DEFAULT, NO_RANGE, "v-Velocity y-amplitude"};
  const ParameterNumeric<Real>  vxy{ "vxy", NO_DEFAULT, NO_RANGE, "v-Velocity xy-amplitude"};
  const ParameterNumeric<Real>  avx{ "avx", NO_DEFAULT, NO_RANGE, "v-Velocity x-frequency"};
  const ParameterNumeric<Real>  avy{ "avy", NO_DEFAULT, NO_RANGE, "v-Velocity y-frequency"};
  const ParameterNumeric<Real> avxy{"avxy", NO_DEFAULT, NO_RANGE, "v-Velocity xy-frequency"};

  const ParameterNumeric<Real>   p0{  "p0", NO_DEFAULT, NO_RANGE, "p-Velocity shift"};
  const ParameterNumeric<Real>   px{  "px", NO_DEFAULT, NO_RANGE, "p-Velocity x-amplitude"};
  const ParameterNumeric<Real>   py{  "py", NO_DEFAULT, NO_RANGE, "p-Velocity y-amplitude"};
  const ParameterNumeric<Real>  pxy{ "pxy", NO_DEFAULT, NO_RANGE, "p-Velocity xy-amplitude"};
  const ParameterNumeric<Real>  apx{ "apx", NO_DEFAULT, NO_RANGE, "p-Velocity x-frequency"};
  const ParameterNumeric<Real>  apy{ "apy", NO_DEFAULT, NO_RANGE, "p-Velocity y-frequency"};
  const ParameterNumeric<Real> apxy{"apxy", NO_DEFAULT, NO_RANGE, "p-Velocity xy-frequency"};

  const ParameterNumeric<Real>   nt0{  "nt0", NO_DEFAULT, NO_RANGE, "nt-Velocity shift"};
  const ParameterNumeric<Real>   ntx{  "ntx", NO_DEFAULT, NO_RANGE, "nt-Velocity x-amplitude"};
  const ParameterNumeric<Real>   nty{  "nty", NO_DEFAULT, NO_RANGE, "nt-Velocity y-amplitude"};
  const ParameterNumeric<Real>  ntxy{ "ntxy", NO_DEFAULT, NO_RANGE, "nt-Velocity xy-amplitude"};
  const ParameterNumeric<Real>  antx{ "antx", NO_DEFAULT, NO_RANGE, "nt-Velocity x-frequency"};
  const ParameterNumeric<Real>  anty{ "anty", NO_DEFAULT, NO_RANGE, "nt-Velocity y-frequency"};
  const ParameterNumeric<Real> antxy{"antxy", NO_DEFAULT, NO_RANGE, "nt-Velocity xy-frequency"};

  static void checkInputs(PyDict d);

  static SolutionFunction_RANSSA2D_TrigMMS_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_RANSSA2D_TrigMMS :
    public Function2DVirtualInterface< SolutionFunction_RANSSA2D_TrigMMS<PDETraitsSize, PDETraitsModel>,
                                       PDETraitsSize<PhysD2> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDETraitsModel::QType QType;
  typedef QRANSSA2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_RANSSA2D_TrigMMS_Params ParamsType;

#define PRM_r0   d.get(ParamsType::params.r0  )
#define PRM_rx   d.get(ParamsType::params.rx  )
#define PRM_ry   d.get(ParamsType::params.ry  )
#define PRM_rxy  d.get(ParamsType::params.rxy )
#define PRM_arx  d.get(ParamsType::params.arx )
#define PRM_ary  d.get(ParamsType::params.ary )
#define PRM_arxy d.get(ParamsType::params.arxy)

#define PRM_u0   d.get(ParamsType::params.u0  )
#define PRM_ux   d.get(ParamsType::params.ux  )
#define PRM_uy   d.get(ParamsType::params.uy  )
#define PRM_uxy  d.get(ParamsType::params.uxy )
#define PRM_aux  d.get(ParamsType::params.aux )
#define PRM_auy  d.get(ParamsType::params.auy )
#define PRM_auxy d.get(ParamsType::params.auxy)

#define PRM_v0   d.get(ParamsType::params.v0  )
#define PRM_vx   d.get(ParamsType::params.vx  )
#define PRM_vy   d.get(ParamsType::params.vy  )
#define PRM_vxy  d.get(ParamsType::params.vxy )
#define PRM_avx  d.get(ParamsType::params.avx )
#define PRM_avy  d.get(ParamsType::params.avy )
#define PRM_avxy d.get(ParamsType::params.avxy)

#define PRM_p0   d.get(ParamsType::params.p0  )
#define PRM_px   d.get(ParamsType::params.px  )
#define PRM_py   d.get(ParamsType::params.py  )
#define PRM_pxy  d.get(ParamsType::params.pxy )
#define PRM_apx  d.get(ParamsType::params.apx )
#define PRM_apy  d.get(ParamsType::params.apy )
#define PRM_apxy d.get(ParamsType::params.apxy)

#define PRM_nt0   d.get(ParamsType::params.nt0  )
#define PRM_ntx   d.get(ParamsType::params.ntx  )
#define PRM_nty   d.get(ParamsType::params.nty  )
#define PRM_ntxy  d.get(ParamsType::params.ntxy )
#define PRM_antx  d.get(ParamsType::params.antx )
#define PRM_anty  d.get(ParamsType::params.anty )
#define PRM_antxy d.get(ParamsType::params.antxy)

  explicit SolutionFunction_RANSSA2D_TrigMMS( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
      L_(d.get(ParamsType::params.L)),
      r0_( PRM_r0),  rx_( PRM_rx),  ry_( PRM_ry),  rxy_( PRM_rxy),  arx_( PRM_arx),  ary_( PRM_ary),  arxy_( PRM_arxy),
      u0_( PRM_u0),  ux_( PRM_ux),  uy_( PRM_uy),  uxy_( PRM_uxy),  aux_( PRM_aux),  auy_( PRM_auy),  auxy_( PRM_auxy),
      v0_( PRM_v0),  vx_( PRM_vx),  vy_( PRM_vy),  vxy_( PRM_vxy),  avx_( PRM_avx),  avy_( PRM_avy),  avxy_( PRM_avxy),
      p0_( PRM_p0),  px_( PRM_px),  py_( PRM_py),  pxy_( PRM_pxy),  apx_( PRM_apx),  apy_( PRM_apy),  apxy_( PRM_apxy),
     nt0_(PRM_nt0), ntx_(PRM_ntx), nty_(PRM_nty), ntxy_(PRM_ntxy), antx_(PRM_antx), anty_(PRM_anty), antxy_(PRM_antxy)
  {}

#undef PRM_r0
#undef PRM_rx
#undef PRM_ry
#undef PRM_rxy
#undef PRM_arx
#undef PRM_ary
#undef PRM_arxy

#undef PRM_u0
#undef PRM_ux
#undef PRM_uy
#undef PRM_uxy
#undef PRM_aux
#undef PRM_auy
#undef PRM_auxy

#undef PRM_v0
#undef PRM_vx
#undef PRM_vy
#undef PRM_vxy
#undef PRM_avx
#undef PRM_avy
#undef PRM_avxy

#undef PRM_p0
#undef PRM_px
#undef PRM_py
#undef PRM_pxy
#undef PRM_apx
#undef PRM_apy
#undef PRM_apxy

#undef PRM_nt0
#undef PRM_ntx
#undef PRM_nty
#undef PRM_ntxy
#undef PRM_antx
#undef PRM_anty
#undef PRM_antxy

  explicit SolutionFunction_RANSSA2D_TrigMMS(
      const GasModel& gas, const Real L,
      const Real  r0, const Real  rx, const Real  ry, const Real  rxy, const Real  arx, const Real  ary, const Real  arxy,
      const Real  u0, const Real  ux, const Real  uy, const Real  uxy, const Real  aux, const Real  auy, const Real  auxy,
      const Real  v0, const Real  vx, const Real  vy, const Real  vxy, const Real  avx, const Real  avy, const Real  avxy,
      const Real  p0, const Real  px, const Real  py, const Real  pxy, const Real  apx, const Real  apy, const Real  apxy,
      const Real nt0, const Real ntx, const Real nty, const Real ntxy, const Real antx, const Real anty, const Real antxy
    ) : qinterp_(gas), L_(L),
         r0_( r0),  rx_( rx),  ry_( ry),  rxy_( rxy),  arx_( arx),  ary_( ary),  arxy_( arxy),
         u0_( u0),  ux_( ux),  uy_( uy),  uxy_( uxy),  aux_( aux),  auy_( auy),  auxy_( auxy),
         v0_( v0),  vx_( vx),  vy_( vy),  vxy_( vxy),  avx_( avx),  avy_( avy),  avxy_( avxy),
         p0_( p0),  px_( px),  py_( py),  pxy_( pxy),  apx_( apx),  apy_( apy),  apxy_( apxy),
        nt0_(nt0), ntx_(ntx), nty_(nty), ntxy_(ntxy), antx_(antx), anty_(anty), antxy_(antxy)
    {}

#if 0
  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    SANS_ASSERT(false);
  }

  ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& time ) const
  {
    Real rho, u, v, p, nt;

    int MS_no = 4;
    int qnt_type = 0; // Quantity type, q, gradq, or forcing (10)
    int qnt_id;
    rho = MS_qnt_2D(MS_no, qnt_type, qnt_id=0, x, y);
    u   = MS_qnt_2D(MS_no, qnt_type, qnt_id=1, x, y);
    v   = MS_qnt_2D(MS_no, qnt_type, qnt_id=2, x, y);
    p   = MS_qnt_2D(MS_no, qnt_type, qnt_id=9, x, y);
    nt  = MS_qnt_2D(MS_no, qnt_type, qnt_id=4, x, y);

    ArrayQ<Real> q = 0;
    qinterp_.setFromPrimitive(q, SAnt2D<DensityVelocityPressure2D<Real>>( {rho, u, v, p, nt} ) );

    return q;
  }

#else
  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T rho, u, v, p, nt;

    rho =  r0_ +  rx_*sin( arx_*PI*x/L_) +  ry_*cos( ary_*PI*y/L_) +  rxy_*cos( arxy_*PI*x/L_)*cos( arxy_*PI*y/L_);
    u   =  u0_ +  ux_*sin( aux_*PI*x/L_) +  uy_*cos( auy_*PI*y/L_) +  uxy_*cos( auxy_*PI*x/L_)*cos( auxy_*PI*y/L_);
    v   =  v0_ +  vx_*cos( avx_*PI*x/L_) +  vy_*sin( avy_*PI*y/L_) +  vxy_*cos( avxy_*PI*x/L_)*cos( avxy_*PI*y/L_);
    p   =  p0_ +  px_*cos( apx_*PI*x/L_) +  py_*sin( apy_*PI*y/L_) +  pxy_*cos( apxy_*PI*x/L_)*cos( apxy_*PI*y/L_);

    nt  = nt0_ + ntx_*cos(antx_*PI*x/L_) + nty_*cos(anty_*PI*y/L_) + ntxy_*cos(antxy_*PI*x/L_)*cos(antxy_*PI*y/L_);

    ArrayQ<T> q = 0;
    qinterp_.setFromPrimitive(q, SAnt2D<DensityVelocityPressure2D<T>>( {rho, u, v, p, nt} ) );

    return q;
  }
#endif

#if 0
  ArrayQ<Real> q0() const
  {
    ArrayQ<Real> q = 0;
#if 0
    Real rho, u, v, p, nt;

    Real x = 0.525;
    Real y = 0.034;

    int MS_no = 1;
    int qnt_type = 0; // Quantity type, q, gradq, or forcing (10)
    int qnt_id;
    rho = MS_qnt_2D(MS_no, qnt_type, qnt_id=0, x, y);
    u   = MS_qnt_2D(MS_no, qnt_type, qnt_id=1, x, y);
    v   = MS_qnt_2D(MS_no, qnt_type, qnt_id=2, x, y);
    p   = MS_qnt_2D(MS_no, qnt_type, qnt_id=9, x, y);
    nt  = MS_qnt_2D(MS_no, qnt_type, qnt_id=4, x, y);

    qinterp_.setFromPrimitive(q, SAnt2D<DensityVelocityPressure2D<Real>>( {rho, u, v, p, nt} ) );
#else
    qinterp_.setFromPrimitive(q, SAnt2D<DensityVelocityPressure2D<Real>>( {r0_, u0_, v0_, p0_, nt0_} ) );
#endif
    return q;
  }
#endif

private:
  const QInterpret qinterp_;
  const Real L_;

  const Real  r0_,  rx_,  ry_,  rxy_,  arx_,  ary_,  arxy_;
  const Real  u0_,  ux_,  uy_,  uxy_,  aux_,  auy_,  auxy_;
  const Real  v0_,  vx_,  vy_,  vxy_,  avx_,  avy_,  avxy_;
  const Real  p0_,  px_,  py_,  pxy_,  apx_,  apy_,  apxy_;
  const Real nt0_, ntx_, nty_, ntxy_, antx_, anty_, antxy_;
};

} // namespace SANS

#endif  // SOLUTIONFUNCTION_RANSSA2D_H
