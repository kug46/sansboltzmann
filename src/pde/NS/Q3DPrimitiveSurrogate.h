// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q3DPRIMITIVESURROGATE_H
#define Q3DPRIMITIVESURROGATE_H

// solution interpreter class
// Euler/N-S conservation-eqns: primitive surrogate variables

#include <vector>
#include <string>

#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "GasModel.h"
#include "pde/NS/NSVariable3D.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 3-D Euler/N-S
// primitive variables (\hat{rho}, \hat{u}, \hat{v}, \hat{w}, \hat{T})
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, w, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v, w)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (\hat{rho}, \hat{u}, \hat{T})
class QTypePrimitiveSurrogate;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q3D;


template <template <class> class PDETraitsSize>
class Q3D<QTypePrimitiveSurrogate, PDETraitsSize>
{
public:
  typedef PhysD3 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int ir = 0;
  static const int iu = 1;
  static const int iv = 2;
  static const int iw = 3;
  static const int it = 4;

  // The three components of the state vector that make up the velocity vector
  static const int ix = iu;
  static const int iy = iv;
  static const int iz = iw;

  static std::vector<std::string> stateNames() { return {"rho", "u", "v", "w", "t"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q3D( const GasModel& gas0, Real rhoc = 0.01, Real Tc = 0.001 ) :
    gas_(gas0),
    rhoc_(rhoc),
    Tc_(Tc)
  {
    // Nothing
  }
  Q3D( const Q3D& q0, Real rhoc = 0.01, Real Tc = 0.001 ) :
    gas_(q0.gas_),
    rhoc_(rhoc),
    Tc_(Tc)
  {
    // Nothing
  }
  ~Q3D() {}

  Q3D& operator=( const Q3D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& v, T& w, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u, T& v, T& w ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalDensityJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                                         ArrayQ<T>& u_q, ArrayQ<T>& v_q, ArrayQ<T>& w_q,
                                         ArrayQ<T>& t_q ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type& ux,
    typename promote_Surreal<Tq,Tg>::type& vx,
    typename promote_Surreal<Tq,Tg>::type& wx,
    typename promote_Surreal<Tq,Tg>::type& tx ) const;

  template<class Tq, class Tg, class Th>
  void evalSecondGradient(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,

      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& rhoH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& uH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& vH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& wH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& tH ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure3D<T>& data ) const;

  // set from variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature3D<T>& data ) const;

  // set from variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative3D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  template<class T>
  void limit(const T& q0, const Real c, T& q) const;

  template<class Tq, class Tg>
  typename promote_Surreal<Tq, Tg>::type
  limitGradient(const Tq& q0, const Tg& q0x, const Real c) const;

  template<class T>
  void limitJacobian(const T& q0, const Real c, T& q) const;

  const GasModel gas_;
  const Real rhoc_;
  const Real Tc_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& v, T& w, T& t ) const
{
  limit(q(ir), rhoc_, rho);
  u   = q(iu);
  v   = q(iv);
  w   = q(iw);
  limit(q(it), Tc_, t);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  limit(q(ir), rhoc_, rho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u, T& v, T& w ) const
{
  u = q(iu);
  v = q(iv);
  w = q(iw);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  limit(q(it), Tc_, t);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::evalDensityJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const
{
  rho_q = 0;

  limitJacobian(q(ir), rhoc_, rho_q(ir)); // drho/drho
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                        ArrayQ<T>& u_q, ArrayQ<T>& v_q, ArrayQ<T>& w_q,
                        ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; v_q = 0; w_q = 0; t_q = 0;

  limitJacobian(q(ir), rhoc_, rho_q(ir)); // drho/drho
  u_q[iu] = 1;                            // du/du
  v_q[iv] = 1;                            // dv/dv
  w_q[iw] = 1;                            // dv/dv
  limitJacobian(q(it), Tc_, t_q(it));     // dt/dt
}


// NOTE: Non limited gradient is included so diffusion terms drive negative values back towards zero
template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type&   ux,
    typename promote_Surreal<Tq,Tg>::type&   vx,
    typename promote_Surreal<Tq,Tg>::type&   wx,
    typename promote_Surreal<Tq,Tg>::type&   tx ) const
{
  rhox = limitGradient(q(ir), qx(ir), rhoc_);
  ux   = qx(iu);
  vx   = qx(iv);
  wx   = qx(iw);
  tx   = limitGradient(q(it), qx(it), Tc_);
}


template <template <class> class PDETraitsSize>
template<class Tq, class Tg, class Th>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,

    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& rhoH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& uH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& vH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& wH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& tH  ) const
{
  SANS_DEVELOPER_EXCEPTION("Second derivatives for Q3DPrimitiveSurrogate not yet implemented");
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::limit(
    const T& q0, const Real c, T& q ) const
{
#if 1
  if (q0 >= c)
    q = q0;
  else
    q = c / ( 3.0 - 3.0*(q0/c) + (q0/c)*(q0/c) );
#else
  q = exp(q0);
#endif
}

template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline typename promote_Surreal<Tq, Tg>::type
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::limitGradient(
    const Tq& q0, const Tg& q0x, const Real c ) const
{
#if 1
  if (q0 >= c)
    return q0x;
  else
  {
    Tq denom = ( 3.0 - 3.0*(q0/c) + (q0/c)*(q0/c) );
    return -c * ( -3.0*(q0x/c) + 2*(q0x/c)*(q0/c) ) / ( denom*denom );
  }
#else
  return exp(q0)*q0x;
#endif
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::limitJacobian(
    const T& q0, const Real c, T& q_q0 ) const
{
#if 1
  if (q0 >= c)
    q_q0 = 1;
  else
  {
    T denom = ( 3.0 - 3.0*(q0/c) + (q0/c)*(q0/c) );
    q_q0 = -c * (-3.0/c + 2*q0/(c*c)) / ( denom*denom );
  }
#else
  q_q0 = exp(q0);
#endif
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 5);
  SANS_ASSERT((name[ir] == "Density") &&
              (name[iu] == "VelocityX") &&
              (name[iv] == "VelocityY") &&
              (name[iw] == "VelocityZ") &&
              (name[it] == "Temperature"));

  const T& rho = data[ir];
  const T& u   = data[iu];
  const T& v   = data[iv];
  const T& w   = data[iw];
  const T& t   = data[it];

  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(iw) = w;
  q(it) = t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure3D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.VelocityX;
  const T& v = data.VelocityY;
  const T& w = data.VelocityZ;
  const T& p = data.Pressure;

  T t = p/(rho * gas_.R());

  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(iw) = w;
  q(it) = t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature3D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.VelocityX;
  const T& v = data.VelocityY;
  const T& w = data.VelocityZ;
  const T& t = data.Temperature;

  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(iw) = w;
  q(it) = t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative3D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.MomentumX/rho;
  const T& v = data.MomentumY/rho;
  const T& w = data.MomentumZ/rho;

  T e = data.TotalEnergy/data.Density - 0.5*(u*u + v*v + w*w);
  T t = gas_.temperature(rho, e);

  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(iw) = w;
  q(it) = t;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  const Real rho = q(ir);
  const Real t   = q(it);

  const Real drho = dq(ir);
  const Real dt   = dq(it);


  // density check
  Real wr = 1;
  Real nrho = rho - drho;

  // Compute wr such that:

  //rho - wr*drho >= (1-maxChangeFraction)*rho
  if (nrho < (1-maxChangeFraction)*rho)
    wr =  maxChangeFraction*rho/drho;

  //rho - wr*drho <= (1+maxChangeFraction)*rho
  if (nrho > (1+maxChangeFraction)*rho)
    wr = -maxChangeFraction*rho/drho;


  // temperature check
  Real wt = 1;
  Real nt = t - dt;

  // Compute wp such that:

  //t - wt*dt >= (1-maxChangeFraction)*t
  if (nt < (1-maxChangeFraction)*t)
    wt =  maxChangeFraction*t/dt;

  //t - wt*dt <= (1+maxChangeFraction)*t
  if (nt > (1+maxChangeFraction)*t)
    wt = -maxChangeFraction*t/dt;

  updateFraction = MIN(wr, wt);
}


// Surrogates always give physically valid primitives
template <template <class> class PDETraitsSize>
inline bool
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  return true;
}


template <template <class> class PDETraitsSize>
void
Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q3D<QTypePrimitiveSurrogate, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q3D<QTypePrimitiveSurrogate, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // Q3DPRIMITIVESURROGATE_H
