// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBOLTZMANND2Q13_H
#define BCBOLTZMANND2Q13_H

// 1-D Euler BC class

//PyDict must be included first
#include <boost/mpl/vector.hpp>
#include <boost/mpl/vector/vector30.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "TraitsBoltzmannD2Q13.h"
#include "PDEBoltzmannD2Q13.h"

#include "BCBoltzmannD2Q13_BounceBackDirichlet.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

/*class BCTypeNone;
class BCTypeTimeOut;
class BCTypeReflect;
class BCTypeInflowSupersonic;
class BCTypeOutflowSubsonic_Pressure;*/

template <class BCType, class PDEBoltzmannD2Q13>
class BCBoltzmannD2Q13;

template <class BCType>
struct BCBoltzmannD2Q13Params;


template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCBoltzmannD2Q13Vector = boost::mpl::vector2<
    BCBoltzmannD2Q13<BCTypeNone, PDEBoltzmannD2Q13<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDEBoltzmannD2Q13<PDETraitsSize, PDETraitsModel>>
                                                 >;

} //namespace SANS

#endif  // BCBOLTZMANND2Q13_H
