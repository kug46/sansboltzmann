// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNAVIERSTOKES3D_H
#define BCNAVIERSTOKES3D_H

// 3-D compressible Navier-Stokes BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "PDENavierStokes3D.h"
#include "PDEEuler3D.h"

#include "pde/BCCategory.h"
#include "BCNavierStokes3D_mitState.h"
#include "BCEuler3D.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 3-D compressible Navier-Stokes
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

//class BCTypeWallNoSlipAdiabatic_mitState;
//class BCTypeSymmetry_mitState;

template <class BCType, class PDENavierStokes3D>
class BCNavierStokes3D;

template <class BCType>
struct BCNavierStokes3DParams;


template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCNavierStokes3DVector = boost::mpl::vector9<
    BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState,  PDENavierStokes3D<PDETraitsSize, PDETraitsModel> >,
    BCNavierStokes3D< BCTypeWallNoSlipIsothermal_mitState, PDENavierStokes3D<PDETraitsSize, PDETraitsModel> >,
    BCNavierStokes3D< BCTypeSymmetry_mitState,             PDENavierStokes3D<PDETraitsSize, PDETraitsModel> >,
    BCEuler3D< BCTypeReflect_mitState,                     PDENavierStokes3D<PDETraitsSize, PDETraitsModel> >,
    BCEuler3D< BCTypeFullState_mitState,                   PDEEuler3D<PDETraitsSize, PDETraitsModel> >,
    BCEuler3D< BCTypeOutflowSubsonic_Pressure_mitState,    PDEEuler3D<PDETraitsSize, PDETraitsModel> >,
    BCEuler3D< BCTypeInflowSubsonic_PtTta_mitState,        PDEEuler3D<PDETraitsSize, PDETraitsModel> >,
    //BCEuler3D< BCTypeInflowSubsonic_sHqt_mitState,        PDEEuler3D<PDETraitsSize, PDETraitsModel> >,
    //BCEuler3D< BCTypeInflowSubsonic_sHqt_BN,              PDEEuler3D<PDETraitsSize, PDETraitsModel> >,
    BCEuler3D< BCTypeOutflowSubsonic_Pressure_BN,          PDEEuler3D<PDETraitsSize, PDETraitsModel> >,
    BCEuler3D< BCTypeFunction_mitState,                   PDEEuler3D<PDETraitsSize, PDETraitsModel> >
    //BCEuler3D< BCTypeFunctionInflow_sHqt_mitState,        PDEEuler3D<PDETraitsSize, PDETraitsModel> >,
    //BCEuler3D< BCTypeFunctionInflow_sHqt_BN,              PDEEuler3D<PDETraitsSize, PDETraitsModel> >
                                                  >;

} //namespace SANS

#endif  // BCNAVIERSTOKES3D_H
