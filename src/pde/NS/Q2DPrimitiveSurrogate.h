// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q2DPRIMITIVESURROGATE_H
#define Q2DPRIMITIVESURROGATE_H

// solution interpreter class
// Euler/N-S conservation-eqns: primitive surrogate variables

#include <vector>
#include <string>

#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "GasModel.h"
#include "pde/NS/NSVariable2D.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D Euler/N-S
// primitive variables (\hat{rho}, u, v, \hat{T})
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (\hat{rho}, \hat{u}, \hat{T})
class QTypePrimitiveSurrogate;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q2D;


template <template <class> class PDETraitsSize>
class Q2D<QTypePrimitiveSurrogate, PDETraitsSize>
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int ir = 0;
  static const int iu = 1;
  static const int iv = 2;
  static const int it = 3;

  static std::vector<std::string> stateNames() { return {"rho", "u", "v", "t"}; }

  // The three components of the state vector that make up the velocity vector
  static const int ix = iu;
  static const int iy = iv;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q2D( const GasModel& gas0, Real rhoc = 0.01, Real Tc = 0.001 ) :
    gas_(gas0),
    rhoc_(rhoc),
    Tc_(Tc)
  {
    // Nothing
  }
  Q2D( const Q2D& q0, Real rhoc = 0.01, Real Tc = 0.001 ) :
    gas_(q0.gas_),
    rhoc_(rhoc),
    Tc_(Tc)
  {
    // Nothing
  }
  ~Q2D() {}

  Q2D& operator=( const Q2D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& v, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u, T& v ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;
  template<class T>
  void evalPressure( const ArrayQ<T>& q, T& p ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                                         ArrayQ<T>& u_q, ArrayQ<T>& v_q,
                                         ArrayQ<T>& t_q ) const;

  template<class Tq, class Tg>
  void evalPressureGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                             typename promote_Surreal<Tq,Tg>::type& p_x ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type& ux,
    typename promote_Surreal<Tq,Tg>::type& vx,
    typename promote_Surreal<Tq,Tg>::type& tx ) const;

  template<class T>
  void evalSecondGradient(
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      const ArrayQ<T>& qxx, const ArrayQ<T>& qxy, const ArrayQ<T>& qyy,
      T& rhoxx, T& rhoxy, T& rhoyy,
      T& uxx, T& uyy, T& uxy,
      T& vxx, T& vxy, T& vyy,
      T& txx, T& txy, T& tyy ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure2D<T>& data ) const;

  // set from variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const;

  // set from variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative2D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  template<class T>
  void limit(const T& q0, const Real c, T& q) const;

  template<class T>
  void limitJacobian(const T& q0, const Real c, T& q) const;

  const GasModel gas_;
  const Real rhoc_;
  const Real Tc_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& v, T& t ) const
{
  limit(q(ir), rhoc_, rho);
  u   = q(iu);
  v   = q(iv);
  limit(q(it), Tc_, t);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  limit(q(ir), rhoc_, rho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u, T& v ) const
{
  u = q(iu);
  v = q(iv);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  limit(q(it), Tc_, t);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::evalPressure(
    const ArrayQ<T>& q, T& p ) const
{
  T rho = 0;
  limit(q(ir), rhoc_, rho);
  T t = 0;
  limit(q(it), Tc_, t);

  p = rho*gas_.R()*t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                        ArrayQ<T>& u_q, ArrayQ<T>& v_q,
                        ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; v_q = 0; t_q = 0;

  limitJacobian(q(ir), rhoc_, rho_q(0)); // drho/drho
  u_q[1] = 1;                            // du/du
  v_q[2] = 1;                            // dv/dv
  limitJacobian(q(it), Tc_, t_q(3));     // dt/dt
}


// NOTE: I am leaving the gradient calculation in terms f surrogates for the
//       time being because the high gradient is needed
template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type&   ux,
    typename promote_Surreal<Tq,Tg>::type&   vx,
    typename promote_Surreal<Tq,Tg>::type&   tx ) const
{
  rhox = qx(ir);
  ux   = qx(iu);
  vx   = qx(iv);
  tx   = qx(it);
}


template <template <class> class PDETraitsSize>template<class Tq, class Tg>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::
evalPressureGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                      typename promote_Surreal<Tq,Tg>::type& p_x ) const
{
  Tq rho = 0;
  limit(q(ir), rhoc_, rho);
  Tg rhox = qx(ir);

  Tq t = 0;
  limit(q(it), Tc_, t);
  Tg tx   = qx(it);

  p_x = rhox*gas_.R()*t + rho*gas_.R()*tx;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
    const ArrayQ<T>& qxx, const ArrayQ<T>& qxy, const ArrayQ<T>& qyy,
    T& rhoxx, T& rhoxy, T& rhoyy,
    T& uxx, T& uxy, T& uyy,
    T& vxx, T& vxy, T& vyy,
    T& txx, T& txy, T& tyy ) const
{
  SANS_DEVELOPER_EXCEPTION("Second derivatives for Q2DPrimitiveSurrogate not yet implemented");
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::limit(
    const T& q0, const Real c, T& q ) const
{
  if (q0 >= c)
    q = q0;
  else
    q = c / ( 3.0 - 3.0*(q0/c) + (q0/c)*(q0/c) );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::limitJacobian(
    const T& q0, const Real c, T& q_q0 ) const
{
  if (q0 >= c)
    q_q0 = 1;
  else
  {
    q_q0 = -c * (-3.0/c + 2*q0/(c*c)) / pow( 3.0 - 3.0*(q0/c) + (q0/c)*(q0/c), 2 );
  }
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 4);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "VelocityY") &&
              (name[3] == "Temperature"));

  const T& rho = data[0];
  const T& u   = data[1];
  const T& v   = data[2];
  const T& t   = data[3];

  // What is the problem with setting a low density of the primary unknown?
#if 0
  if (rho <= rhoc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Densities lower that critical density not implemented in surrogates" );
  }
  if (t <= Tc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Temperatures lower that critical temperature not implemented in surrogates" );
  }
#endif

  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(it) = t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure2D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.VelocityX;
  const T& v = data.VelocityY;
  const T& p = data.Pressure;

  T t = p/(rho * gas_.R());

  // What is the problem with setting a low density of the primary unknown?
#if 0
  if (rho <= rhoc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Densities lower that critical density not implemented in surrogates" );
  }
  if (t <= Tc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Temperatures lower that critical temperature not implemented in surrogates" );
  }
#endif

  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(it) = t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.VelocityX;
  const T& v = data.VelocityY;
  const T& t = data.Temperature;

  // What is the problem with setting a low density of the primary unknown?
#if 0
  if (rho <= rhoc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Densities lower that critical density not implemented in surrogates" );
  }
  if (t <= Tc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Temperatures lower that critical temperature not implemented in surrogates" );
  }
#endif

  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(it) = t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative2D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.MomentumX/rho;
  const T& v = data.MomentumY/rho;

  T e = data.TotalEnergy/data.Density - 0.5*(u*u + v*v);
  T t = gas_.temperature(rho, e);


  // What is the problem with setting a low density of the primary unknown?
#if 0
  if (rho <= rhoc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Densities lower that critical density not implemented in surrogates" );
  }
  if (t <= Tc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Temperatures lower that critical temperature not implemented in surrogates" );
  }
#endif

  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(it) = t;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  const Real rho = q(ir);
  const Real t   = q(it);

  const Real drho = dq(ir);
  const Real dt   = dq(it);

  Real wr = 1, wt = 1;

  // density check
  Real nrho = rho - drho;

  // Compute wr such that:

  //rho - wr*drho >= (1-maxChangeFraction)*rho
  if (nrho < (1-maxChangeFraction)*rho)
    wr =  maxChangeFraction*rho/drho;

  //rho - wr*drho <= (1+maxChangeFraction)*rho
  if (nrho > (1+maxChangeFraction)*rho)
    wr = -maxChangeFraction*rho/drho;

  // temperature check
  Real np = t - dt;

  // Compute wt such that:

  //t - wt*dt >= (1-maxChangeFraction)*p
  if (np < (1-maxChangeFraction)*t)
    wt =  maxChangeFraction*t/dt;

  //t - wt*dt <= (1+maxChangeFraction)*p
  if (np > (1+maxChangeFraction)*t)
    wt = -maxChangeFraction*t/dt;

  updateFraction = MIN(wr, wt);
}


// Surrogates always give physically valid primitives
template <template <class> class PDETraitsSize>
inline bool
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  bool isPositive;
  Real rho, t;

  evalDensity(q, rho);
  evalTemperature(q, t);

  if ((rho > 0) && (t > 0))
    isPositive = true;
  else
    isPositive = false;

  return isPositive;
}


template <template <class> class PDETraitsSize>
void
Q2D<QTypePrimitiveSurrogate, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q2D<QTypePrimitiveSurrogate, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q2D<QTypePrimitiveSurrogate, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // Q2DPRIMITIVESURROGATE_H
