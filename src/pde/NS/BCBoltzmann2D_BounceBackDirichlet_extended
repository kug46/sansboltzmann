// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBOLTZMANN2D_BOUNCEBACKDIRICHLET_H
#define BCBOLTZMANN2D_BOUNCEBACKDIRICHLET_H

// 1-D Euler BC class

//PyDict must be included first

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsBoltzmann2D.h"
#include "PDEBoltzmann2D.h"
//#include "BCEuler2D_Solution.h"
//#include "pde/NS/SolutionFunction_Euler2D.h"
//#include "pde/NS/SolutionFunction_NavierStokes2D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include "UserVariables/BoltzmannNVar.h"
#include "UserVariables/BoltzmannVelocity.h"

#include <iostream>
#include <string>
#include <memory> //shared_ptr
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeNone;
class BCTypeDiffuseKinetic;
class BCTypeDiffuseKineticDensityOutlet;

template <class BCType, class PDEBoltzmann2D>
class BCBoltzmann2D;

template <class BCType>
struct BCBoltzmann2DParams;


//----------------------------------------------------------------------------//
// BCNone
//----------------------------------------------------------------------------//

template<>
struct BCBoltzmann2DParams<BCTypeNone> : noncopyable
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"None"};
  struct Option
  {
    const DictOption None{BCBoltzmann2DParams::BCName, BCBoltzmann2DParams::checkInputs};
  };

  static BCBoltzmann2DParams params;
};


template <class PDE_>
class BCBoltzmann2D<BCTypeNone, PDE_> :
    public BCType< BCBoltzmann2D<BCTypeNone, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeNone BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBoltzmann2DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 0;                       // total BCs

  typedef typename PDE::QType QType;
  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  //typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCBoltzmann2D( const PDE& pde ) :
      pde_(pde),
      qInterpret_(pde_.variableInterpreter())
  {
    // Nothing
  }
  ~BCBoltzmann2D() {}

  BCBoltzmann2D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter())
  {
    // Nothing
  }

  BCBoltzmann2D( const BCBoltzmann2D& ) = delete;
  BCBoltzmann2D& operator=( const BCBoltzmann2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const
  {
    Real rho = 0.;
    qInterpret_.evalDensity( qI, rho );
    // None requires outflow
    return (rho) > 0;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class PDE_>
template <class T>
inline void
BCBoltzmann2D<BCTypeNone, PDE_>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}


// normal BC flux
template <class PDE_>
template <class T, class Tf>
inline void
BCBoltzmann2D<BCTypeNone, PDE_>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on the interior state
  ArrayQ<Tf> f = 0, g = 0;
  pde_.fluxAdvective(x, y, time, qI, f, g);

  Fn += f*nx + g*ny;
}


// normal BC flux
template <class PDE_>
template <class T, class Tp, class Tf>
inline void
BCBoltzmann2D<BCTypeNone, PDE_>::
fluxNormal( const Tp& param,
            const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on the interior state
  ArrayQ<Tf> f = 0, g = 0;
  pde_.fluxAdvective(x, y, time, qI, f, g);

  Fn += f*nx + g*ny;
}

template <class PDE_>
void
BCBoltzmann2D<BCTypeNone, PDE_>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCBoltzmann2D<BCTypeNone, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCBoltzmann2D<BCTypeNone, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// BCDiffuseKinetic
//----------------------------------------------------------------------------//

template<>
struct BCBoltzmann2DParams<BCTypeDiffuseKinetic> : noncopyable
{
  static void checkInputs(PyDict d);

  const ParameterNumeric<Real> Uw{"Uw", 0, NO_RANGE, "Wall velocity-X"};
  const ParameterNumeric<Real> Vw{"Vw", 0, NO_RANGE, "Wall velocity-Y"};
  const ParameterNumeric<Real> Rhow{"Rhow", -1, NO_RANGE, "Wall density"};

  static constexpr const char* BCName{"DiffuseKinetic"};
  struct Option
  {
    const DictOption DiffuseKinetic{BCBoltzmann2DParams::BCName, BCBoltzmann2DParams::checkInputs};
  };

  static BCBoltzmann2DParams params;
};


template <class PDE_>
class BCBoltzmann2D<BCTypeDiffuseKinetic, PDE_> :
    public BCType< BCBoltzmann2D<BCTypeDiffuseKinetic, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeDiffuseKinetic BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBoltzmann2DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                       // total BCs

  typedef typename PDE::QType QType;
  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  BCBoltzmann2D( const PDE& pde, const Real& Uw, const Real& Vw, const Real& Rhow ):
    pde_(pde), qInterpret_(pde_.variableInterpreter()), Uw_(Uw),
    Vw_(Vw), Rhow_(Rhow) {}

  ~BCBoltzmann2D() {}

  BCBoltzmann2D(const PDE& pde, const PyDict& d) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    Uw_(d.get(ParamsType::params.Uw)),
    Vw_(d.get(ParamsType::params.Vw)),
    Rhow_(d.get(ParamsType::params.Rhow))
  {

  }

  BCBoltzmann2D( const BCBoltzmann2D& ) = delete;
  BCBoltzmann2D& operator=( const BCBoltzmann2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // Apply BC Data
  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;
  const Real Uw_;
  const Real Vw_;
  const Real Rhow_;
  const bool upwind_ = true;
};

// BC data
template <class PDE>
template <class T>
void
BCBoltzmann2D<BCTypeDiffuseKinetic, PDE>::state( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rho = 0;
  T numerator = 0.;
  T denominator = 0.;
  Real u_dot_u = Uw_*Uw_ + Vw_*Vw_;

  if (Rhow_ < 0) SANS_DEVELOPER_EXCEPTION("Rhow_ < 0.; CALL THIS....................");
#if 0
  else
  {
    for (int i = 0; i<NVar; i++)
    rho = rho + qI(i);
  }
#endif 


  for (int i=0; i<NVar; i++)
  {
    if ( (nx*ew[i][0] + ny*ew[i][1]) < 0)
    {
// ADVANCED BC START
//
    //rho = Rhow_;
    for (int j=0; j<NVar; j++)
    {
      if (( (nx*(ew[j][0]-Uw_)) + (ny*(ew[j][1]-Vw_)) ) > 0 )
      {
        numerator = numerator + qI(j)*sqrt(pow((nx*(ew[j][0]-Uw_)),2) + pow((ny*(ew[j][1]-Vw_)),2));
      } // if num
      else if (( (nx*(ew[j][0]-Uw_)) + (ny*(ew[j][1]-Vw_)) ) < 0 )
      {
        Real e_dot_u1 = ew[j][0]*Uw_ + ew[j][1]*Vw_;
#ifdef NON_LB_GH
      Real exp_param1 = 2*e_dot_u1 - u_dot_u*u_dot_u;
#else
      Real exp_param1 = e_dot_u1 - u_dot_u*u_dot_u/2.;
#endif
//        Real exp_param1  = e_dot_u1 - u_dot_u*u_dot_u/2.;
        T feqj = Rhow_*ew[j][2]*exp(exp_param1);

        denominator = denominator + feqj*sqrt(pow((nx*(ew[j][0]-Uw_)),2) + pow((ny*(ew[j][1]-Vw_)),2));
      } // if den
    } // for
    rho = numerator/denominator; 
//
// ADVANCED BC End
      Real e_dot_u = ew[i][0]*Uw_ + ew[i][1]*Vw_;
#ifdef NON_LB_GH
      Real exp_param = 2*e_dot_u - u_dot_u*u_dot_u;
#else
      Real exp_param = e_dot_u - u_dot_u*u_dot_u/2.;
#endif
      T feq = rho*ew[i][2]*exp(exp_param);
      //T feq = weight[i]*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq)
      //                       + (e_dot_u/6.)*(e_dot_u*e_dot_u - 3*u_dot_u*u_dot_u));
      //T feq = weight[i]*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));
      //std::cout<<"Direction"<<e[i][0]<<"and"<<e[i][1]<<std::endl<<"Vel"<<Uw_<<","<<Vw_<<std::endl;
      qB(i) = feq;
    }
    else
      qB(i) = qI(i);
  }
}

// BC data
template <class PDE>
template <class Tp, class T>
void
BCBoltzmann2D<BCTypeDiffuseKinetic, PDE>::state( const Tp& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
void
BCBoltzmann2D<BCTypeDiffuseKinetic, PDE>::fluxNormal( const Real& x, const Real& y, const Real& time,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& qI,
                 const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                 const ArrayQ<T>& qB,
                 ArrayQ<Tf>& Fn) const
{
  if ( upwind_ )
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( x, y, time, qI, qB, nx, ny, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<Tf> fx = 0, fy = 0;
    pde_.fluxAdvective(x, y, time, qB, fx, fy);

    Fn += fx*nx + fy*ny;
  }
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
void
BCBoltzmann2D<BCTypeDiffuseKinetic, PDE>::fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& qI,
                 const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                 const ArrayQ<T>& qB,
                 ArrayQ<Tf>& Fn) const
{
  if ( upwind_ )
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<Tf> fx = 0, fy = 0;
    pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

    Fn += fx*nx + fy*ny;
  }
}

// is the boundary state valid
template <class PDE>
bool
BCBoltzmann2D<BCTypeDiffuseKinetic, PDE>::isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const
{
  return true;
}


template <class PDE>
void
BCBoltzmann2D<BCTypeDiffuseKinetic, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCBoltzmann2D<BCTypeDiffuseKinetic, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCBoltzmann2D<BCTypeDiffuseKinetic, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//
// BCDiffuseKineticDensityOutlet
//----------------------------------------------------------------------------//

template<>
struct BCBoltzmann2DParams<BCTypeDiffuseKineticDensityOutlet> : noncopyable
{
  static void checkInputs(PyDict d);

  const ParameterNumeric<Real> Rhow{"Rhow", -1, NO_RANGE, "Wall density"};
  const ParameterNumeric<Real> applyUw{"applyUw", -1, NO_RANGE, "check if Uw needs to be appllied"};
  const ParameterNumeric<Real> applyVw{"applyVw", -1, NO_RANGE, "check if Vw needs to be appllied"};
  const ParameterNumeric<Real> valUw{"valUw", 0., NO_RANGE, "Uvelocity"};
  const ParameterNumeric<Real> valVw{"valVw", 0., NO_RANGE, "Vvelocity"};

  static constexpr const char* BCName{"DiffuseKineticDensityOutlet"};
  struct Option
  {
    const DictOption DiffuseKineticDensityOutlet{BCBoltzmann2DParams::BCName, BCBoltzmann2DParams::checkInputs};
  };

  static BCBoltzmann2DParams params;
};


template <class PDE_>
class BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE_> :
    public BCType< BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeDiffuseKineticDensityOutlet BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBoltzmann2DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                       // total BCs

  typedef typename PDE::QType QType;
  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  BCBoltzmann2D( const PDE& pde, const Real& Rhow, const Real& applyUw, const Real& valUw, const Real& applyVw, const Real& valVw ):
    pde_(pde), qInterpret_(pde_.variableInterpreter()), Rhow_(Rhow), applyUw_(applyUw), valUw_(valUw), applyVw_(applyVw), valVw_(valVw) {}

  ~BCBoltzmann2D() {}

  BCBoltzmann2D(const PDE& pde, const PyDict& d) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    Rhow_(d.get(ParamsType::params.Rhow)),
    applyUw_(d.get(ParamsType::params.applyUw)),
    valUw_(d.get(ParamsType::params.valUw)),
    applyVw_(d.get(ParamsType::params.applyVw)),
    valVw_(d.get(ParamsType::params.valVw))
  {

  }

  BCBoltzmann2D( const BCBoltzmann2D& ) = delete;
  BCBoltzmann2D& operator=( const BCBoltzmann2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // Apply BC Data
  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;
  const Real Rhow_;
  const bool upwind_ = true;
  const Real applyUw_;
  const Real valUw_;
  const Real applyVw_;
  const Real valVw_;
};

// BC data
template <class PDE>
template <class T>
void
BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE>::state( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rho = 0;
  if (Rhow_ < 0)
  {
    //SANS_DEVELOPER_EXCEPTION("Rhow_ in BC is unphysical");
    for (int i = 0;i<NVar;i++)
    {
      if ( (nx*ew[i][0] + ny*ew[i][1]) < 0)
        qB(i) = qI(i);
    }
  }
  else
  {
    rho = Rhow_;
    T Uw = valUw_;
    T Vw = valVw_;
    if (applyUw_ < 0.5 or applyVw_ < 0.5)
    {
      T tmpRho = 0.;           // TODO: Check.
      for (int i = 0 ; i < NVar ; i++ )
        tmpRho = tmpRho + qI(i);
      if (applyUw_ < 0.5)
      {
        Uw = 0.;
        for (int i=0;i<NVar;i++)
          Uw = Uw + qI(i)*ew[i][0];
        Uw = Uw/tmpRho;
      }
      if (applyVw_ < 0.5)
      {
        Vw = 0.;
        for (int i=0;i<NVar;i++)
          Vw = Vw + qI(i)*ew[i][1];
        Vw = Vw/tmpRho;
      }
    }
    //..........................................................

    T u_dot_u = Uw*Uw + Vw*Vw;
    //SANS_DEVELOPER_EXCEPTION("Do not use DiffuseOutlet.");
    for (int i=0; i<NVar; i++)
    {
      if (true)// (nx*e[i][0] + ny*e[i][1]) < 0) // OUTFLOW ONLY
      {
        T e_dot_u = ew[i][0]*Uw + ew[i][1]*Vw;
#ifdef NON_LB_GH
        T exp_param = 2*e_dot_u - u_dot_u*u_dot_u;
#else
        T exp_param = e_dot_u - u_dot_u*u_dot_u/2.;
#endif
        T feq = rho*ew[i][2]*exp(exp_param);
       // T feq = weight[i]*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq)
       //                         + (e_dot_u/6.)*(e_dot_u*e_dot_u - 3*u_dot_u*u_dot_u));
       // T feq = weight[i]*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq)
       //                         + (e_dot_u/6.)*(e_dot_u*e_dot_u - 3*u_dot_u*u_dot_u));

        //std::cout<<"Direction"<<e[i][0]<<"and"<<e[i][1]<<std::endl<<"Vel"<<Uw_<<","<<Vw_<<std::endl;
        qB(i) = feq;
      }
      else
        qB(i) = qI(i);
    }
  }
}

// BC data
template <class PDE>
template <class Tp, class T>
void
BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE>::state( const Tp& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
void
BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE>::fluxNormal( const Real& x, const Real& y, const Real& time,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& qI,
                 const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                 const ArrayQ<T>& qB,
                 ArrayQ<Tf>& Fn) const
{
  if ( upwind_ )
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( x, y, time, qI, qB, nx, ny, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<Tf> fx = 0, fy = 0;
    pde_.fluxAdvective(x, y, time, qB, fx, fy);

    Fn += fx*nx + fy*ny;
  }
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
void
BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE>::fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& qI,
                 const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                 const ArrayQ<T>& qB,
                 ArrayQ<Tf>& Fn) const
{
  if ( upwind_ )
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<Tf> fx = 0, fy = 0;
    pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

    Fn += fx*nx + fy*ny;
  }
}

// is the boundary state valid
template <class PDE>
bool
BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE>::isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const
{
  return true;
}


template <class PDE>
void
BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // BCBOLTZMANN2D_H
