// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FORCINGFUNCTIONEULER2D_H
#define FORCINGFUNCTIONEULER2D_H

#include "tools/SANSnumerics.h"     // Real

#include "pde/ForcingFunctionBase.h"
#include "pde/AnalyticFunction/Function2D.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// forcing function given a 2D analytical solution

template<class PDE>
class ForcingFunction2D_HeatSource : public ForcingFunctionBase2D<PDE>
{
public:
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  static const int D = 2;                     // physical dimensions

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iyMom = 2;
  static const int iEngy = 3; // PDE index for the energy equation

  typedef Function2DBase<ArrayQ<Real>> FunctionBase;

  virtual bool hasForcingFunction() const override { return true; }

  // cppcheck-suppress noExplicitConstructor
  ForcingFunction2D_HeatSource( const Real x0, const Real y0,
                                const Real str, const Real width ) : x0_(x0), y0_(y0), str_(str), width_(width) {}
  virtual ~ForcingFunction2D_HeatSource() {}

  void operator()( const PDE& pde,
                   const Real &x, const Real &y, const Real &time,
                   ArrayQ<Real>& forcing ) const override
  {
      // Add some energy forcing at location x0, y0, with strength str, and isotropic width
      forcing(iEngy) += (str_/sqrt(2*PI*width_)) * exp( -( (x-x0_)*(x-x0_) + (y-y0_)*(y-y0_) )/(2*width_)  );

      // std::cout << "forcing = " << forcing <<std::endl;
  }

  void set( const Real x0, const Real y0, const Real str, const Real width )
  {
    x0_ = x0; y0_ = y0; str_ = str; width_ = width;
    // std::cout << "x0_ = " << x0_ << "y0_ = " << y0_  <<std::endl;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const override
  {
    std::string indent(indentSize, ' ');
    out << indent << "ForcingFunction2D_HeatSource: x0_ = "<< x0_ << "y0_ = "<< y0_
        << "str_ = "<< str_ << "width_ = "<< width_ << std::endl;

  }

private:
  Real x0_, y0_; // focal point coordinates
  Real str_; // strength
  Real width_; // width
};

} //namespace SANS

#endif //FORCINGFUNCTIONEULER2D_H
