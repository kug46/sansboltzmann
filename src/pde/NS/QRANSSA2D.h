// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QRANSSA2D_H
#define QRANSSA2D_H

// solution interpreter class
// RANS with SA conservation-eqns: NS variables with SA nu~

#include <vector>
#include <string>

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "pde/NS/SAVariable2D.h"
#include "Surreal/PromoteSurreal.h"

#include <string>


namespace SANS
{

// forward declare: Euler/NS variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q2D;

class QTypeConservative;

//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D RANS with SA
// NS variables combined with SA nu~
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QEulerType           solution variable set for Euler/NS (e.g. primitive)
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//

template <class QEulerType, template <class> class PDETraitsSize>
class QRANSSA2D : public Q2D<QEulerType, PDETraitsSize>
{
public:
  typedef PhysD2 PhysDim;

  typedef Q2D<QEulerType, PDETraitsSize> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  static const int iSA = 4;
  static std::vector<std::string> stateNames()
  {
    std::vector<std::string> statenames = BaseType::stateNames();
    statenames.push_back("nut");
    return statenames;
  }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit QRANSSA2D( const GasModel& gas0, const Real ntref = 1 ) : Q2D<QEulerType, PDETraitsSize>(gas0), ntref_(ntref) {}
  QRANSSA2D( const QRANSSA2D& q0, const Real ntref = 1 ) : Q2D<QEulerType, PDETraitsSize>(q0), ntref_(ntref) {}
  ~QRANSSA2D() {}

  QRANSSA2D& operator=( const QRANSSA2D& );

  // evaluate SA nu~ variable
  template<class T>
  void evalSA( const ArrayQ<T>& q, T& nt ) const;

  template<class T>
  void evalSAJacobian( const ArrayQ<T>& q, ArrayQ<T>& nt_q ) const;

  template<class Tq, class Tg>
  void evalSAGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& ntx ) const;

  template<class Tq, class Tg, class Th>
  void evalSAHessian(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Th>& qxy,
      typename promote_Surreal<Tq,Tg,Th>::type& ntxy) const;

  template<class Tq, class Tqp, class T>
  void conservativePerturbation(const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<T>& dU) const
  {
    Tq nt = 0;
    evalSA(q, nt);
    Tq rho = 0;

    BaseType::evalDensity( q, rho );

    dU[iSA] = nt/ntref_*dq[0] + rho*dq[4];

    Q2D<QEulerType, PDETraitsSize>::conservativePerturbation(q, dq, dU);
  }

  template<class Tq, class Tqp, class T>
  void conservativeSAPerturbation(const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, T& drhont) const
  {
    Tq nt = 0;
    evalSA(q, nt);
    Tq rho = 0;

    BaseType::evalDensity( q, rho );

    drhont = nt/ntref_*dq[0] + rho*dq[4];
  }


  using BaseType::setFromPrimitive;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variables
  template <template<class> class NSVariables, class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const SAnt2D<NSVariables<T>>& data ) const;

  template <class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const SAnt2D<Conservative2D<T>>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real ntref_;

};

template <class QEulerType, template <class> class PDETraitsSize>
template <class T>
inline void
QRANSSA2D<QEulerType, PDETraitsSize>::evalSA( const ArrayQ<T>& q, T& nt ) const
{
  nt = q(iSA) * ntref_;
}


template <class QEulerType, template <class> class PDETraitsSize>
template <class T>
inline void
QRANSSA2D<QEulerType, PDETraitsSize>::evalSAJacobian( const ArrayQ<T>& q, ArrayQ<T>& nt_q ) const
{
  nt_q = 0;

//  nt_q(iSA) = 1;
  nt_q(iSA) = ntref_;
}


template <class QEulerType, template <class> class PDETraitsSize>
template<class Tq, class Tg>
inline void
QRANSSA2D<QEulerType, PDETraitsSize>::evalSAGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& ntx ) const
{
  ntx = qx(iSA) * ntref_;
}


template <class QEulerType, template <class> class PDETraitsSize>
template <class Tq, class Tg, class Th>
inline void
QRANSSA2D<QEulerType, PDETraitsSize>::evalSAHessian(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Th>& qxy, typename promote_Surreal<Tq,Tg,Th>::type& ntxy ) const
{
  ntxy = qxy(iSA) * ntref_;
}



template <class QEulerType, template <class> class PDETraitsSize>
template <class T>
inline void
QRANSSA2D<QEulerType, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 5);
  SANS_ASSERT(name[4] == "SANutilde");

  q(iSA) = data[iSA];

  Q2D<QEulerType, PDETraitsSize>::setFromPrimitive( q, data, name, nn-1 );
}


template <class QEulerType, template <class> class PDETraitsSize>
template <template<class> class NSVariables, class T>
inline void
QRANSSA2D<QEulerType, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const SAnt2D<NSVariables<T>>& data ) const
{
  Q2D<QEulerType, PDETraitsSize>::setFromPrimitive( q, data );
  q(iSA) = data.SANutilde;
}

template <class QEulerType, template <class> class PDETraitsSize>
template <class T>
inline void
QRANSSA2D<QEulerType, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const SAnt2D<Conservative2D<T>>& data ) const
{
  Q2D<QEulerType, PDETraitsSize>::setFromPrimitive( q, data );
  q(iSA) = (data.rhoSANutilde)/(data.Density);
}

template <class QEulerType, template <class> class PDETraitsSize>
void
QRANSSA2D<QEulerType, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "QRANSSA2D<QType, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "QRANSSA2D<QType, PDETraitsSize>: Q2D<QType, PDETraitsSize> = " << std::endl;
  Q2D<QEulerType, PDETraitsSize>::dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D RANS with SA specialization for conservative variables
// NS variables combined with SA nu~
//
// template parameters:
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class QRANSSA2D<QTypeConservative, PDETraitsSize> : public Q2D<QTypeConservative, PDETraitsSize>
{
public:
  typedef PhysD2 PhysDim;

  typedef Q2D<QTypeConservative, PDETraitsSize> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  using BaseType::irho;
  static const int iSA = N-1;
  static std::vector<std::string> stateNames()
  {
    std::vector<std::string> statenames = BaseType::stateNames();
    statenames.push_back("rhonut");
    return statenames;
  }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit QRANSSA2D( const GasModel& gas0, const Real ntref = 1 ) : Q2D<QTypeConservative, PDETraitsSize>(gas0), ntref_(ntref) {}
  QRANSSA2D( const QRANSSA2D& q0, const Real ntref = 1 ) : Q2D<QTypeConservative, PDETraitsSize>(q0), ntref_(ntref) {}
  ~QRANSSA2D() {}

  QRANSSA2D& operator=( const QRANSSA2D& );

  // evaluate SA nu~ variable
  template<class T>
  void evalSA( const ArrayQ<T>& q, T& nt ) const;

  template<class T>
  void evalSAJacobian( const ArrayQ<T>& q, ArrayQ<T>& nt_q ) const;

  template<class Tq, class Tg>
  void evalSAGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& ntx ) const;

  template<class Tq, class Tg, class Th>
  void evalSAHessian(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Th>& qxy,
      typename promote_Surreal<Tq,Tg,Th>::type& ntxy) const;

  using BaseType::setFromPrimitive;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variables
  template <template<class> class NSVariables, class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const SAnt2D<NSVariables<T>>& data ) const;

  template <class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const SAnt2D<Conservative2D<T>>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real ntref_;

};

template <template <class> class PDETraitsSize>
template <class T>
inline void
QRANSSA2D<QTypeConservative, PDETraitsSize>::evalSA( const ArrayQ<T>& q, T& nt ) const
{
  nt = q(iSA)/q(irho) * ntref_;
}

//dnt / dU
template <template <class> class PDETraitsSize>
template <class T>
inline void
QRANSSA2D<QTypeConservative, PDETraitsSize>::evalSAJacobian( const ArrayQ<T>& q, ArrayQ<T>& nt_q ) const
{
  nt_q = 0;

  nt_q(irho) = -ntref_*q(iSA)/q(irho)/q(irho);
  nt_q(iSA)  = ntref_/q(irho);
}


template <template <class> class PDETraitsSize>
template<class Tq, class Tg>
inline void
QRANSSA2D<QTypeConservative, PDETraitsSize>::evalSAGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& ntx ) const
{
  const Tq& rho  = q(irho);
  const Tg& rhox = qx(irho);

  const Tq& rhont  = q(iSA)  * ntref_;
  const Tg& rhontx = qx(iSA)  * ntref_;

  ntx = rhontx/rho - rhont*rhox/(rho*rho);
}


template <template <class> class PDETraitsSize>
template<class Tq, class Tg, class Th>
inline void
QRANSSA2D<QTypeConservative, PDETraitsSize>::evalSAHessian(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Th>& qxy,
    typename promote_Surreal<Tq,Tg,Th>::type& ntxy ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  const Tq& rho   = q(irho);
  const Tq& rhont   = q(iSA)* ntref_;

  const Tg& rhox  = qx(irho);
  const Tg& rhoy  = qy(irho);

  T ntx =0, nty=0;
  evalSAGradient(q, qx, ntx);
  evalSAGradient(q, qy, nty);

  const Th& rhoxy = qxy(irho);
  const Th& rhontxy = qxy(iSA)* ntref_;

  Tq nt = rhont/rho;

  ntxy = (  rhontxy - rhoxy*nt - rhox*nty - rhoy*ntx )/rho;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QRANSSA2D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 5);
  SANS_ASSERT(name[4] == "rhoSANutilde");

  q(iSA) = data[0]*data[iSA];

  Q2D<QTypeConservative, PDETraitsSize>::setFromPrimitive( q, data, name, nn-1 );
}


template <template <class> class PDETraitsSize>
template <template<class> class NSVariables, class T>
inline void
QRANSSA2D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const SAnt2D<NSVariables<T>>& data ) const
{
  Q2D<QTypeConservative, PDETraitsSize>::setFromPrimitive( q, data );
  q(iSA) = data.Density*data.SANutilde;
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
QRANSSA2D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const SAnt2D<Conservative2D<T>>& data ) const
{
  Q2D<QTypeConservative, PDETraitsSize>::setFromPrimitive( q, data );
  q(iSA) = data.rhoSANutilde;
}

template <template <class> class PDETraitsSize>
void
QRANSSA2D<QTypeConservative, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "QRANSSA2D<QTypeConservative, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "QRANSSA2D<QTypeConservative, PDETraitsSize>: Q2D<QType, PDETraitsSize> = " << std::endl;
  Q2D<QTypeConservative, PDETraitsSize>::dump(indentSize+2, out);
}


} // namespace SANS

#endif  // QRANSSA2D_H
