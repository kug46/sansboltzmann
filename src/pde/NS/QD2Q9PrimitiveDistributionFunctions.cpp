// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "QD2Q9PrimitiveDistributionFunctions.h"
#include "TraitsBoltzmannD2Q9.h"

namespace SANS
{

template class QD2Q9<QTypePrimitiveDistributionFunctions, TraitsSizeBoltzmannD2Q9>;

}
