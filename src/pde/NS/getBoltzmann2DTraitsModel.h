// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GETBOLTZMANN2DTRAITSMODEL_H
#define GETBOLTZMANN2DTRAITSMODEL_H

#include "TraitsBoltzmann2D.h"
//#include "TraitsNavierStokes.h"
//#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"

namespace SANS
{

template<class TraitsModel> struct getBoltzmann2DTraitsModel;

template<class QType_, class GasModel_>
struct getBoltzmann2DTraitsModel< TraitsModelBoltzmann2D<QType_,GasModel_> >
{
  typedef TraitsModelBoltzmann2D<QType_,GasModel_> type;
};

}

#endif
