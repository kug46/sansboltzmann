// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NAVIERSTOKESSOLUTIONFUNCTION2D_H
#define NAVIERSTOKESSOLUTIONFUNCTION2D_H

// 2-D NavierStokes PDE: exact and MMS solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // pow

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "NSVariable2D.h"
#include "getEulerTraitsModel.h"

#include "pde/AnalyticFunction/Function2D.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q2D;


//----------------------------------------------------------------------------//
// solution: - Ojeda MMS Solution Function
struct SolutionFunction_NavierStokes2D_OjedaMMS_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  static void checkInputs(PyDict d);

  static SolutionFunction_NavierStokes2D_OjedaMMS_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_NavierStokes2D_OjedaMMS :
    public Function2DVirtualInterface< SolutionFunction_NavierStokes2D_OjedaMMS<PDETraitsSize, PDETraitsModel>,
                                       PDETraitsSize<PhysD2> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_NavierStokes2D_OjedaMMS_Params ParamsType;

  explicit SolutionFunction_NavierStokes2D_OjedaMMS( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )) {}

  explicit SolutionFunction_NavierStokes2D_OjedaMMS( const GasModel& gas ) :
      qinterp_(gas) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T rho, u, v, p;

#if 0
    //NEGATIVE BUMP
    rho = 1.0 + 0.1*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    u = 0.2 - 0.3*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    v =  0.1 - 0.15*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    p = pow(rho, 1.4);
#else
    //POSITIVE BUMP
    rho = 1.0 + 0.1*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    u = 0.2 + 0.3*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    v =  0.1 + 0.15*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    p = pow(rho, 1.4);
#endif

    ArrayQ<T> q = 0;
    qinterp_.setFromPrimitive(q, DensityVelocityPressure2D<T>( rho, u, v, p ) );

    return q;
  }

private:
  const QInterpret qinterp_;
};


//----------------------------------------------------------------------------//
// solution: - Ojeda MMS Solution Function
struct SolutionFunction_NavierStokes2D_Hartmann_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  static void checkInputs(PyDict d);

  static SolutionFunction_NavierStokes2D_Hartmann_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_NavierStokes2D_Hartmann :
    public Function2DVirtualInterface< SolutionFunction_NavierStokes2D_Hartmann<PDETraitsSize, PDETraitsModel>,
                                       PDETraitsSize<PhysD2> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_NavierStokes2D_Hartmann_Params ParamsType;

  explicit SolutionFunction_NavierStokes2D_Hartmann( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )) {}

  explicit SolutionFunction_NavierStokes2D_Hartmann( const GasModel& gas ) :
      qinterp_(gas) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {

   T rho = sin(2.*PI*(x+y))+4.0;
   T rhou = sin(2.*PI*(x+y))*0.2+ 4.0;
   T rhov = sin(2.*PI*(x+y))*0.2 + 4.0;
   T rhoE = (sin(2.*PI*(x+y))+4.)*(sin(2.*PI*(x+y))+4.);

   ArrayQ<T> q = 0;
   qinterp_.setFromPrimitive(q, Conservative2D<T>( rho, rhou, rhov, rhoE) );

   return q;

  }

private:
  const QInterpret qinterp_;
};


//----------------------------------------------------------------------------//
// solution: Quadratic MMS

struct SolutionFunction_NavierStokes2D_QuadraticMMS_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  static void checkInputs(PyDict d);

  static SolutionFunction_NavierStokes2D_QuadraticMMS_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_NavierStokes2D_QuadraticMMS :
    public Function2DVirtualInterface< SolutionFunction_NavierStokes2D_QuadraticMMS<PDETraitsSize, PDETraitsModel>,
                                       PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_NavierStokes2D_QuadraticMMS_Params ParamsType;

  explicit SolutionFunction_NavierStokes2D_QuadraticMMS( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )) {}
  explicit SolutionFunction_NavierStokes2D_QuadraticMMS(const GasModel& gas ) :
      qinterp_(gas) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T rho = 1.0 + x*y*0.035;
    T u = 0.3 + x*y*0.015;
    T v = 0.1 + x*y*0.012;
    T p = 1.0;

    ArrayQ<T> q = 0;
    qinterp_.setFromPrimitive(q, DensityVelocityPressure2D<T>( rho, u, v, p ) );

    return q;
  }

private:
  const QInterpret qinterp_;
};


} // namespace SANS

#endif  // NAVIERSTOKESSOLUTIONFUNCTION2D_H
