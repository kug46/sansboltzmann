// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef EULERRESIDCATEGORY_H
#define EULERRESIDCATEGORY_H

namespace SANS
{

enum EulerResidualInterpCategory
{
  Euler_ResidInterp_Raw,
  Euler_ResidInterp_Momentum,
  Euler_ResidInterp_Entropy
};

}

#endif //
