// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/NS/SolutionFunction_Euler1D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_Euler1D_Const_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}

// cppcheck-suppress passedByValue
void SolutionFunction_Euler1D_Riemann_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.interface));
  allParams.push_back(d.checkInputs(params.rhoL));
  allParams.push_back(d.checkInputs(params.uL));
  allParams.push_back(d.checkInputs(params.pL));
  allParams.push_back(d.checkInputs(params.rhoR));
  allParams.push_back(d.checkInputs(params.uR));
  allParams.push_back(d.checkInputs(params.pR));
  d.checkUnknownInputs(allParams);
}

// cppcheck-suppress passedByValue
void SolutionFunction_Euler1D_DensityPulse_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.center));
  allParams.push_back(d.checkInputs(params.width));
  allParams.push_back(d.checkInputs(params.height));
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}

// cppcheck-suppress passedByValue
void SolutionFunction_Euler1D_ArtShock_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.xShock));
  allParams.push_back(d.checkInputs(params.xThroat));
  allParams.push_back(d.checkInputs(params.pt1));
  allParams.push_back(d.checkInputs(params.pt2));
  allParams.push_back(d.checkInputs(params.Tt));
  allParams.push_back(d.checkInputs(params.pe));
  d.checkUnknownInputs(allParams);
}

// cppcheck-suppress passedByValue
void SolutionFunction_Euler1D_SineBackPressure_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.p));
  allParams.push_back(d.checkInputs(params.dp));
  allParams.push_back(d.checkInputs(params.dt));
  d.checkUnknownInputs(allParams);
}

// cppcheck-suppress passedByValue
void SolutionFunction_Euler1D_ShuOsher_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.interface));
  allParams.push_back(d.checkInputs(params.rhoL));
  allParams.push_back(d.checkInputs(params.uL));
  allParams.push_back(d.checkInputs(params.pL));
  allParams.push_back(d.checkInputs(params.epsilon));
  allParams.push_back(d.checkInputs(params.lambda));
  allParams.push_back(d.checkInputs(params.rhoR));
  allParams.push_back(d.checkInputs(params.uR));
  allParams.push_back(d.checkInputs(params.pR));
  d.checkUnknownInputs(allParams);
}

SolutionFunction_Euler1D_Const_Params SolutionFunction_Euler1D_Const_Params::params;
SolutionFunction_Euler1D_Riemann_Params SolutionFunction_Euler1D_Riemann_Params::params;
SolutionFunction_Euler1D_DensityPulse_Params SolutionFunction_Euler1D_DensityPulse_Params::params;
SolutionFunction_Euler1D_ArtShock_Params SolutionFunction_Euler1D_ArtShock_Params::params;
SolutionFunction_Euler1D_SineBackPressure_Params SolutionFunction_Euler1D_SineBackPressure_Params::params;
SolutionFunction_Euler1D_ShuOsher_Params SolutionFunction_Euler1D_ShuOsher_Params::params;

}
