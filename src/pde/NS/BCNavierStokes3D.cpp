// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCNavierStokes3D.h"
#include "Q3DPrimitiveRhoPressure.h"
#include "Q3DPrimitiveSurrogate.h"
//#include "Q3DEntropy.h"
#include "Q3DConservative.h"
#include "TraitsNavierStokes.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
// cppcheck-suppress passedByValue
void BCNavierStokes3DParams<BCTypeWallNoSlipAdiabatic_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCNavierStokes3DParams<BCTypeWallNoSlipAdiabatic_mitState> BCNavierStokes3DParams<BCTypeWallNoSlipAdiabatic_mitState>::params;

// cppcheck-suppress passedByValue
void BCNavierStokes3DParams<BCTypeWallNoSlipIsothermal_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Twall));
  d.checkUnknownInputs(allParams);
}
BCNavierStokes3DParams<BCTypeWallNoSlipIsothermal_mitState> BCNavierStokes3DParams<BCTypeWallNoSlipIsothermal_mitState>::params;

// cppcheck-suppress passedByValue
void BCNavierStokes3DParams<BCTypeSymmetry_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCNavierStokes3DParams<BCTypeSymmetry_mitState> BCNavierStokes3DParams<BCTypeSymmetry_mitState>::params;


//===========================================================================//
// Instantiate BC parameters

// Pressure-primitive variables
typedef BCNavierStokes3DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_S )

// Conservative Variables
typedef BCNavierStokes3DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Cons_S;
BCPARAMETER_INSTANTIATE( BCVector_Cons_S )

#if 0   // not implemented
// Entropy Variables
typedef BCNavierStokes3DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeEntropy, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Entropy_S;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_S )
#endif

// Surrogate Variables
typedef BCNavierStokes3DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Surrogate_S;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_S )


//===========================================================================//
// Repeat for constant viscosity

// Pressure-primitive variables
typedef BCNavierStokes3DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_RhoP_C;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_C )

// Conservative Variables
typedef BCNavierStokes3DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Cons_C;
BCPARAMETER_INSTANTIATE( BCVector_Cons_C )

#if 0   // not implemented
// Entropy Variables
typedef BCNavierStokes3DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Entropy_C;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_C )
#endif

// Surrogate Variables
typedef BCNavierStokes3DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Surrogate_C;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_C )

} //namespace SANS
