// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define AVSENSOR1D_INSTANTIATE

#include "TraitsEulerArtificialViscosity.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D_impl.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D_impl.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D_impl.h"

namespace SANS
{

//Instantiations for the Artificial Viscosity PDE with Euler

template class AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity>;

template class AVSensor_DiffusionMatrix1D_Uniform<TraitsSizeEulerArtificialViscosity>;
template class AVSensor_DiffusionMatrix1D_GenHScale<TraitsSizeEulerArtificialViscosity>;

template class AVSensor_Source1D_Uniform<TraitsSizeEulerArtificialViscosity>;

}
