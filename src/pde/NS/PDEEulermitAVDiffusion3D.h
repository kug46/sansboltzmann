// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEEULER3D_ARTIFICIALVISCOSITY_H
#define PDEEULER3D_ARTIFICIALVISCOSITY_H

// 3-D compressible Euler with artificial viscosity

#include <cmath>              // sqrt
#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/smoothmath.h"
#include "tools/Tuple.h"

#include "Field/Tuple/ParamTuple.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"

#include "PDEEuler3D.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q3D;


//----------------------------------------------------------------------------//
// PDE class: 3-D Artificial Viscosity wrapper for Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//


template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase = PDEEuler3D>
class PDEEulermitAVDiffusion3D : public PDEBase<PDETraitsSize, PDETraitsModel>
{
public:
  typedef PhysD3 PhysDim;

  typedef PDEBase<PDETraitsSize, PDETraitsModel> BaseType;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables
  static const int Nparam = 1;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  template <class TP>
  using ArrayParam = DLA::VectorS<Nparam,TP>;  // parameter array type

  template <class T>
  using MatrixParam = DLA::MatrixS<N,Nparam,T>;         // e.g. jacobian of PDEs w.r.t. parameters

  template< class... PDEArgs >
  PDEEulermitAVDiffusion3D(const int order, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    order_(order)
  {
    //Nothing
  }

  ~PDEEulermitAVDiffusion3D() {}

  PDEEulermitAVDiffusion3D& operator=( const PDEEulermitAVDiffusion3D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return BaseType::hasFluxAdvective(); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return BaseType::hasSource(); }
  bool hasSourceTrace() const { return BaseType::hasSourceTrace(); }
  bool hasForcingFunction() const { return BaseType::hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return BaseType::needsSolutionGradientforSource(); }

  using BaseType::fluxAdvective;
  using BaseType::speedCharacteristic;

  // unsteady temporal flux: Ft(Q)
  template <class Tp, class T, class Tf>
  void fluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(x, y, z, time, q, ft);
  }

//  // jacobian of unsteady temporal flux: d(Ft)/dU
//  template <class Tp, class T, class Tf>
//  void jacobianFluxAdvectiveTime(
//      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
//      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
//  {
//    BaseType::jacobianFluxAdvectiveTime(x, y, z, time, q, J);
//  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <int Dim, class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(x, y, z, time, q, J);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <int Dim, class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(param.right(), x, y, z, time, q, J);
  }

  // master state: U(Q)
  template <class Tp, class T, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
  {
    BaseType::masterState(x, y, z, time, q, uCons);
  }


  // master state Gradient: Ux(Q)
  template<class Tp, class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy, ArrayQ<Tf>& Vz  ) const
  {
    BaseType::masterStateGradient(x, y, z, time, q, qx, qy, qz, Vx, Vy, Vz);
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tp, class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
      const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vxz,
      ArrayQ<Tf>& Vyy, ArrayQ<Tf>& Vyz, ArrayQ<Tf>& Vzz  ) const
  {
    BaseType::masterStateHessian(x, y, z, time, q, qx, qy, qz,
                                 qxx, qxy, qxz, qyy, qyz, qzz,
                                 Vxx, Vxy, Vxz, Vyy, Vyz, Vzz );
  }

  // jacobian of master state wrt q: dU/dQ
  template<class Tp, class T>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
     BaseType::jacobianMasterState(x, y, z, time, q, dudq);
  }

  // unsteady conservative flux: d(U)/d(t)
  template <class Tp, class T>
  void strongFluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
  {
    BaseType::strongFluxAdvectiveTime(x, y, z, time, q, qt, dudt);
  }

  // advective flux: F(Q)
  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
  {
    BaseType::fluxAdvective(x, y, z, time, q, fx, fy, fz);
  }

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<Tf>& fn ) const
  {
    BaseType::fluxAdvectiveUpwind(x, y, z, time, qL, qR, nx, ny, nz, fn);
  }

  template <class Tp, class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt,
      ArrayQ<Tf>& fn ) const
  {
    BaseType::fluxAdvectiveUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, fn);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfxdu, MatrixQ<Tf>& dfydu, MatrixQ<Tf>& dfzdu ) const
  {
    BaseType::jacobianFluxAdvective( x, y, z, time, q, dfxdu, dfydu, dfzdu );
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<Tf>& a ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValue( x, y, z, time, q, nx, ny, nz, a );
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt,
      MatrixQ<Tf>& a ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, z, time, q, nx, ny, nz, nt, a);
  }

  // strong form advective fluxes
  template <class Tp, class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& strongPDE ) const
  {
    BaseType::strongFluxAdvective( x, y, z, time, q, qx, qy, qz, strongPDE );
  }

  // viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& fn ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz) const;

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <int Dim, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const;

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <int Dim, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <int Dim, class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <int Dim, class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const;

  // strong form viscous fluxes
  template <int Dim, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& strongPDE ) const;

  // strong form viscous fluxes
  template <int Dim, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& strongPDE ) const;

protected:

  // Hide the base class flux functions to avoid missing the artificial viscosity flux
  using BaseType::fluxViscous;
  using BaseType::diffusionViscous;
  using BaseType::jacobianFluxViscous;
  using BaseType::strongFluxViscous;

public:

  // solution-dependent source: S(X, Q, QX) - param: H-tensor
  template <class Tq, class Tg, class Ts>
  void source(
      const DLA::MatrixSymS<D,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const
  {
    // assuming the base class is NS or Euler
    BaseType::source( x, y, z, time, q, qx, qy, qz, source );
  }

  // solution-dependent source: S(X, Q, QX) - param: H-tensor + distance
  template <class Tq, class Tg, class Ts>
  void source(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const
  {
    // assuming the base class is RANS
    BaseType::source( param.right(), x, y, z, time, q, qx, qy, qz, source );
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX) - param: H-tensor
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const DLA::MatrixSymS<D,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const
  {
    // assuming the base class is NS or Euler
    BaseType::source( x, y, z, time, lifted_quantity, q, qx, qy, qz, source );
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX) - param: H-tensor + distance
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const
  {
    // assuming the base class is RANS
    BaseType::source( param.right(), x, y, z, time, lifted_quantity, q, qx, qy, qz, source );
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const DLA::MatrixSymS<D,Real>& paramL, const Real& xL, const Real& yL, const Real& zL,
      const DLA::MatrixSymS<D,Real>& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    BaseType::sourceTrace(xL, yL, zL, xR, yR, zR, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sourceL, sourceR);
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& paramL, const Real& xL, const Real& yL, const Real& zL,
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    BaseType::sourceTrace(paramL.right(), xL, yL, zL,
                          paramR.right(), xR, yR, zR, time,
                          qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, sourceL, sourceR);
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const DLA::MatrixSymS<D,Real>& paramL, const Real& xL, const Real& yL, const Real& zL,
      const DLA::MatrixSymS<D,Real>& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(xL, yL, zL, xR, yR, zR, time, qL, qR, s);
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& paramL, const Real& xL, const Real& yL, const Real& zL,
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(paramL.right(), xL, yL, zL, paramR.right(), xR, yR, zR, time, qL, qR, s);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const DLA::MatrixSymS<D,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSource(x, y, z, time, q, qx, qy, qz, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U) - param: H-tensor + distance
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSource(param.right(), x, y, z, time, q, qx, qy, qz, dsdu);
  }


  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const DLA::MatrixSymS<D,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSourceHACK(x, y, z, time, q, qx, qy, qz, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U) - param: H-tensor + distance
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSourceHACK(param.right(), x, y, z, time, q, qx, qy, qz, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSourceAbsoluteValue(x, y, z, time, q, qx, qy, qz, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSourceAbsoluteValue(param.right(), x, y, z, time, q, qx, qy, qz, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const DLA::MatrixSymS<D,Real>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
  {
    BaseType::jacobianGradientSource(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
  {
    BaseType::jacobianGradientSource(param.right(), x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
  }

  template <int Dim, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradient(
        const DLA::MatrixSymS<Dim,Real>& param,
        const Real& x, const Real& y, const Real& z, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
        MatrixQ<Ts>& div_dsgradu ) const
  {
    BaseType::jacobianGradientSourceGradient(x, y, z, time,
                                             q, qx, qy, qz,
                                             qxx, qxy, qyy,
                                             qxz, qyz, qzz, div_dsgradu);
  }

  template <int Dim, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradient(
        const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
        const Real& x, const Real& y, const Real& z, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
        MatrixQ<Ts>& div_dsgradu ) const
  {
    BaseType::jacobianGradientSourceGradient(param.right(), x, y, z, time,
                                             q, qx, qy, qz,
                                             qxx, qxy, qyy,
                                             qxz, qyz, qzz, div_dsgradu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const DLA::MatrixSymS<D,Real>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
  {
    BaseType::jacobianGradientSourceHACK(x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
  {
    BaseType::jacobianGradientSourceHACK(param.right(), x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
  }

  // right-hand-side forcing function - NS and Euler
  template<class T>
  void forcingFunction( const DLA::MatrixSymS<D,Real>& param, const Real& x, const Real& y, const Real& z, const Real& time,
                        ArrayQ<T>& forcing ) const
  {
    BaseType::forcingFunction(x, y, z, time, forcing );
  }

  // right-hand-side forcing function - RANS
  template<class T>
  void forcingFunction( const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
                        const Real& x, const Real& y, const Real& z, const Real& time,
                        ArrayQ<T>& forcing ) const
  {
    BaseType::forcingFunction(param.right(), x, y, z, time, forcing );
  }

  // characteristic speed (needed for timestep)
  template<class Tp, class Tq, class Tc>
  void speedCharacteristic( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                            const Real& dx, const Real& dy, const Real& dz, const ArrayQ<Tq>& q,
                            Tc& speed ) const
  {
    BaseType::speedCharacteristic(x, y, z, time, dx, dy, dz, q, speed);
  }

  // characteristic speed
  template<class Tp, class Tq, class Tc>
  void speedCharacteristic( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                            const ArrayQ<Tq>& q, Tc& speed ) const
  {
    BaseType::speedCharacteristic(x, y, z, time, q, speed);
  }

  // update fraction needed for physically valid state
  template <class Tp>
  void updateFraction( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                       const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction,
                       Real& updateFraction ) const
  {
    BaseType::updateFraction(x, y, z, time, q, dq, maxChangeFraction, updateFraction);
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int getOrder() const { return order_; };

protected:

  // Artificial viscosity - generalized H-tensor
  template <int Dim, class Tq, class Te>
  void artificialViscosity( const DLA::MatrixSymS<Dim,Real>& logH,
                            const Real& x, const Real& y, const Real& z, const Real& time,
                            const ArrayQ<Tq>& q,
                            DLA::MatrixSymS<D,Te>& eps ) const;

  template <int Dim, class Tq, class Te>
  void artificialViscosity( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
                            const Real& x, const Real& y, const Real &z, const Real& time,
                            const ArrayQ<Tq>& q,
                            DLA::MatrixSymS<D,Te>& eps ) const;

  // Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
  template <class Tp, class Tq, class Tk>
  void artificialViscosity( const Tp& param,
                            const Real& x, const Real& y, const Real& z, const Real& time,
                            const ArrayQ<Tq>& q,
                            MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
                            MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
                            MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const;
  // Artificial viscosity flux
  template <class Tp, class Tq, class Tg, class Tf>
  void artificialViscosityFlux( const Tp& param,
                                const Real& x, const Real& y, const Real& z,const Real& time,
                                const ArrayQ<Tq>& q,
                                const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                                ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const;

  const int order_;

  using BaseType::gas_;
  using BaseType::qInterpret_;
};

// Artificial viscosity - generalized H-tensor
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Te>
void inline
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscosity(
    const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
    const Real& x, const Real& y,const  Real& z,
    const Real& time, const ArrayQ<Tq>& q,
    DLA::MatrixSymS<D,Te>& eps ) const
{
  DLA::MatrixSymS<Dim,Real> logH = param.left();
  Real dist = param.right();
  DLA::MatrixSymS<Dim,Real> H = exp(logH); //generalized h-tensor

  Tq lambda;
  BaseType::speedCharacteristic( dist, x, y, z, time, q, lambda );

  Real C = 2.0;
  Tq sensor = q(N-1);
  Tq zz = 0.0;
  sensor = max(sensor, zz);

  Tq factor = C/(Real(max(order_,1))) * lambda * sensor;

  for (int i = 0; i < D; i++)
    for (int j = 0; j <= i; j++)
        eps(i,j) = factor*H(i,j); //Artificial viscosity
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Te>
void inline
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscosity(
    const DLA::MatrixSymS<Dim,Real>& logH,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q,
    DLA::MatrixSymS<D,Te>& eps ) const
{

  DLA::MatrixSymS<Dim,Real> H = exp(logH); //generalized h-tensor

  Tq lambda;
  BaseType::speedCharacteristic( x, y, z, time, q, lambda );

  Real C = 2.0;
  Tq sensor = q(N-1);
  Tq zz = 0.0;
  sensor = max(sensor, zz);

  Tq factor = C/(Real(max(order_,1))) * lambda * sensor;

  for (int i = 0; i < D; i++)
    for (int j = 0; j <= i; j++)
        eps(i,j) = factor*H(i,j); //Artificial viscosity
}

// Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tk>
void inline
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscosity(
    const Tp& param,
    const Real& x, const Real& y, const Real& z,
    const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
    MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz  ) const
{
  DLA::MatrixSymS<D,Tq> eps; //Artificial viscosity
  artificialViscosity( param, x, y, z, time, q, eps );

  // Energy to Enthalpy jacobian
  ArrayQ<Tq> dutdu = 0;

  Real gamma = BaseType::gas_.gamma();
  Tq rho = 0, u = 0, v = 0, w = 0, t = 0;
  BaseType::qInterpret_.eval( q, rho, u, v, w, t );

  dutdu(BaseType::iCont) = 0.5*(gamma - 1.0)*(u*u + v*v + w*w);
  dutdu(BaseType::ixMom) = -(gamma - 1.0)*u;
  dutdu(BaseType::iyMom) = -(gamma - 1.0)*v;
  dutdu(BaseType::izMom) = -(gamma - 1.0)*w;
  dutdu(BaseType::iEngy) = gamma;

  static_assert( BaseType::iCont == 0 &&
                 BaseType::iEngy > BaseType::iCont &&
                 BaseType::iEngy > BaseType::ixMom &&
                 BaseType::iEngy > BaseType::iyMom &&
                 BaseType::iEngy > BaseType::izMom, "Below loops assume energy is last");

  // Don't add diffusion to the sensor equation
  for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
  {
    kxx(i,i) += eps(0,0); // [length^2 / time]
    kxy(i,i) += eps(0,1); // [length^2 / time]
    kxz(i,i) += eps(0,2); // [length^2 / time]
    kyx(i,i) += eps(1,0); // [length^2 / time]
    kyy(i,i) += eps(1,1); // [length^2 / time]
    kyz(i,i) += eps(1,2); // [length^2 / time]
    kzx(i,i) += eps(2,0); // [length^2 / time]
    kzy(i,i) += eps(2,1); // [length^2 / time]
    kzz(i,i) += eps(2,2); // [length^2 / time]
  }

  for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
  {
    // This changes the gradient to be on Enthalpy rather than Energy
    kxx(BaseType::iEngy,i) += eps(0,0)*dutdu(i);
    kxy(BaseType::iEngy,i) += eps(0,1)*dutdu(i);
    kxz(BaseType::iEngy,i) += eps(0,2)*dutdu(i);
    kyx(BaseType::iEngy,i) += eps(1,0)*dutdu(i);
    kyy(BaseType::iEngy,i) += eps(1,1)*dutdu(i);
    kyz(BaseType::iEngy,i) += eps(1,2)*dutdu(i);
    kzx(BaseType::iEngy,i) += eps(2,0)*dutdu(i);
    kzy(BaseType::iEngy,i) += eps(2,1)*dutdu(i);
    kzz(BaseType::iEngy,i) += eps(2,2)*dutdu(i);
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::artificialViscosityFlux(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type Tqg;

  MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0, nu_xz = 0.0;
  MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0, nu_yz = 0.0;
  MatrixQ<Tf> nu_zx = 0.0, nu_zy = 0.0, nu_zz = 0.0;
  MatrixQ<Tq> dudq = 0;

  artificialViscosity( param, x, y, z, time, q, nu_xx, nu_xy, nu_xz,
                                                nu_yx, nu_yy, nu_yz,
                                                nu_zx, nu_zy, nu_zz );

  BaseType::jacobianMasterState( x, y, z, time, q, dudq );
  ArrayQ<Tqg> ux = dudq*qx;
  ArrayQ<Tqg> uy = dudq*qy;
  ArrayQ<Tqg> uz = dudq*qz;

  static_assert( BaseType::iCont == 0 &&
                 BaseType::iEngy > BaseType::iCont &&
                 BaseType::iEngy > BaseType::ixMom &&
                 BaseType::iEngy > BaseType::iyMom &&
                 BaseType::iEngy > BaseType::izMom, "Below loops assume energy is last");

  // Expand out the matmul to reduce computational time
  for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
  {
    fx(i) -= nu_xx(i,i)*ux(i) + nu_xy(i,i)*uy(i) + nu_xz(i,i)*uz(i);
    fy(i) -= nu_yx(i,i)*ux(i) + nu_yy(i,i)*uy(i) + nu_yz(i,i)*uz(i);
    fz(i) -= nu_zx(i,i)*ux(i) + nu_zy(i,i)*uy(i) + nu_zz(i,i)*uz(i);
  }

  for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
  {
    fx(BaseType::iEngy) -= nu_xx(BaseType::iEngy,i)*ux(i) + nu_xy(BaseType::iEngy,i)*uy(i) + nu_xz(BaseType::iEngy,i)*uz(i);
    fy(BaseType::iEngy) -= nu_yx(BaseType::iEngy,i)*ux(i) + nu_yy(BaseType::iEngy,i)*uy(i) + nu_yz(BaseType::iEngy,i)*uz(i);
    fz(BaseType::iEngy) -= nu_zx(BaseType::iEngy,i)*ux(i) + nu_zy(BaseType::iEngy,i)*uy(i) + nu_zz(BaseType::iEngy,i)*uz(i);
  }

}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::fluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
{
  artificialViscosityFlux( param, x, y, z, time, q, qx, qy, qz, fx, fy, fz );
  BaseType::fluxViscous( x, y, z, time, q, qx, qy, qz, fx, fy, fz );
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::fluxViscous(
    const Tp& paramL, const Tp& paramR,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
    const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& fn ) const
{
  ArrayQ<Tf> fxL = 0, fxR = 0;
  ArrayQ<Tf> fyL = 0, fyR = 0;
  ArrayQ<Tf> fzL = 0, fzR = 0;

  artificialViscosityFlux(paramL, x, y, z, time, qL, qxL, qyL, qzL, fxL, fyL, fzL);
  artificialViscosityFlux(paramR, x, y, z, time, qR, qxR, qyR, qzR, fxR, fyR, fzR);

  //Compute the average artificial viscosity flux from both sides
  fn += 0.5*(fxL + fxR)*nx + 0.5*(fyL + fyR)*ny + 0.5*(fzL + fzR)*nz;

  //Add on any interface viscous fluxes from the base class
  BaseType::fluxViscous( x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::diffusionViscous(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
    MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
{
  artificialViscosity( param, x, y, z, time, q,
                       kxx, kxy, kxz,
                       kyx, kyy, kyz,
                       kzx, kzy, kzz );

  BaseType::diffusionViscous( x, y, z, time,
                              q, qx, qy, qz,
                              kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Tg, class Th, class Tk>
inline void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::
diffusionViscousGradient(
    const DLA::MatrixSymS<Dim,Real>& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
    MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const
{
//  artificialViscosity( param, x, y, z, time, q,
//                       kxx, kxy, kxz,
//                       kyx, kyy, kyz,
//                       kzx, kzy, kzz );

  BaseType::diffusionViscousGradient( x, y, z, time,
                                      q, qx, qy, qz,
                                      qxx,
                                      qxy, qyy,
                                      qxz, qyz, qzz,
                                      kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                      kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                      kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Tg, class Th, class Tk>
inline void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::
diffusionViscousGradient(
    const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
    MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const
{
//  artificialViscosity( param, x, y, z, time, q,
//                       kxx, kxy, kxz,
//                       kyx, kyy, kyz,
//                       kzx, kzy, kzz );

  BaseType::diffusionViscousGradient( param.right(),
                                      x, y, z, time,
                                      q, qx, qy, qz,
                                      qxx,
                                      qxy, qyy,
                                      qxz, qyz, qzz,
                                      kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                      kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                      kxz_z, kyz_z, kzx_z, kzy_z, kzz_z  );
}

// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::
jacobianFluxViscous(
    const DLA::MatrixSymS<Dim,Real>& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
{
  BaseType::jacobianFluxViscous( x, y, z, time,
                                 q, qx, qy, qz,
                                 ax, ay, az);
}

// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::
jacobianFluxViscous(
    const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
{
  BaseType::jacobianFluxViscous( param.right(),
                                 x, y, z, time,
                                 q, qx, qy, qz,
                                 ax, ay, az);
}



// strong form viscous fluxes - Euler / NS
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Tg, class Th, class Tf>
void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::
strongFluxViscous(
    const DLA::MatrixSymS<Dim,Real>& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& strongPDE ) const
{
  BaseType::strongFluxViscous( x, y, z, time,
                               q, qx, qy, qz,
                               qxx, qxy, qyy, qxz, qyz, qzz,
                               strongPDE );
}

// strong form viscous fluxes - RANSSA
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Tg, class Th, class Tf>
void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::
strongFluxViscous(
    const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& strongPDE ) const
{
  BaseType::strongFluxViscous( param.right(), x, y, z, time,
                               q, qx, qy, qz,
                               qxx, qxy, qyy, qxz, qyz, qzz,
                               strongPDE );
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
void
PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEBase>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEEulermitAVDiffusion3D: order = " << order_ << std::endl;
  out << indent << "PDEEulermitAVDiffusion3D: pde_ = " << std::endl;
  BaseType::dump(indentSize+2, out);
}


} // namespace SANS

#endif  // PDEEULER3D_ARTIFICIALVISCOSITY_H
