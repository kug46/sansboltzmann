// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSRANSSAARTIFICIALVISCOSITY_H
#define TRAITSRANSSAARTIFICIALVISCOSITY_H

#include "TraitsRANSSA.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"

namespace SANS
{

template <class PhysDim_>
using TraitsSizeRANSSAArtificialViscosity = TraitsSizeArtificialViscosity<TraitsSizeRANSSA,PhysDim_>;

}

#endif // TRAITSRANSSAARTIFICIALVISCOSITY_H
