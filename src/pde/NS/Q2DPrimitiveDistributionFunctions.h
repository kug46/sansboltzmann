// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q2DPRIMITIVEDISTRIBUTIONFUNCTIONS_H
#define Q2DPRIMITIVEDISTRIBUTIONFUNCTIONS_H

// solution interpreter class
// Euler/N-S conservation-eqns: primitive variables (rho, u, v, p)

#include <vector>
#include <string>
#include <cmath>

#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "GasModel.h"
#include "BoltzmannVariable2D.h"
#include "Surreal/PromoteSurreal.h"

#include <string>

#include "UserVariables/BoltzmannNVar.h"
#include "UserVariables/BoltzmannVelocity.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D Euler/N-S
// primitive variables (rho, u, v, p)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (rho, u, v, p)
class QTypePrimitiveDistributionFunctions;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q2D;


template <template <class> class PDETraitsSize>
class Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  // The three components of the state vector that make up the velocity vector
  //static const int ix = if1;
  //static const int iy = if2;

  static std::vector<std::string> stateNames()
  {
    SANS_DEVELOPER_EXCEPTION("StateNames not defined");
    return {"pdf0","pdf2"};
    /*
    return { "pdf0", "pdf1", "pdf2", "pdf3",
             "pdf4", "pdf5", "pdf6", "pdf7",
             "pdf8", "pdf9", "pdf10","pdf11",
             "pdf12", "pdf13","pdf14", "pdf15"};
             */
  }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q2D( const GasModel& gas0 ) : gas_(gas0) {}
  Q2D( const Q2D& q0 ) : gas_(q0.gas_) {}
  ~Q2D() {}

  Q2D& operator=( const Q2D& );


  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u, T& v ) const;




  template<class Tq, class Tg>
  void evalDensityGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                            typename promote_Surreal<Tq,Tg>::type& rho_x ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[]) const;//, const std::string name[], int nn ) const;

  // set from variables
#if 0
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const PrimitiveDistributionFunctions2D<T>& data ) const;
#endif

/*  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative2D<T>& data ) const; */

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  rho = 0.;           // TODO: Check.
  for (int i = 0 ; i < NVar ; i++ )
    rho = rho + q(i);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u, T& v ) const
{
  T rho = 0.;
  for (int i = 0 ; i < NVar ; i++ )
    rho = rho + q(i);

  u = 0.;
  v = 0.;
  for (int i = 0; i < NVar; i++)
  {
    u = u + q(i)*ew[i][0];
    v = v + q(i)*ew[i][1];
  }

  u = u/rho;
  v = v/rho;
}


template <template <class> class PDETraitsSize>
template<class Tq, class Tg>
void
Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalDensityGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rho_x ) const
{
  rho_x = 0.;
  for (int i = 0; i < NVar ; i++)
    rho_x = rho_x + qx(i);
}



template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[]) const //, const std::string name[], int nn ) const
{
#if 0
  SANS_ASSERT(nn == 16);
  SANS_ASSERT((name[0] == "DistributionFunction0") &&
              (name[1] == "DistributionFunction1") &&
              (name[2] == "DistributionFunction2") &&
              (name[3] == "DistributionFunction3") &&
              (name[4] == "DistributionFunction4") &&
              (name[5] == "DistributionFunction5") &&
              (name[6] == "DistributionFunction6") &&
              (name[7] == "DistributionFunction7") &&
              (name[8] == "DistributionFunction8") &&
              (name[9] == "DistributionFunction9") &&
              (name[10] == "DistributionFunction10") &&
              (name[11] == "DistributionFunction11") &&
              (name[12] == "DistributionFunction12") &&
              (name[13] == "DistributionFunction13") &&
              (name[14] == "DistributionFunction14") &&
              (name[15] == "DistributionFunction15") );
#endif
  for (int i = 0; i < NVar; i++)
    q(i) = data[i];


}

// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const // TODO: Is this valid?
{
  SANS_DEVELOPER_EXCEPTION("updateFraction is being used...... might not be correct.");
#if 0
  const Real csq = 1.;
  Real rho = 0.;
  for (int i=0; i<16; i++)
    rho = rho + q(i);
  const Real p   = csq*rho;  // Krueger, (Book) LBM principals and practice eq. 7.15, Springer '17

  Real drho = 0.;
  for (int i=0; i<16; i++)
    drho = drho + q(i);
  const Real dp   = csq*drho;

  Real wr = 1, wp = 1;

  // density check
  Real nrho = rho - drho;

  // Compute wr such that:

  //rho - wr*drho >= (1-maxChangeFraction)*rho
  if (nrho < (1-maxChangeFraction)*rho)
    wr =  maxChangeFraction*rho/drho;

  //rho - wr*drho <= (1+maxChangeFraction)*rho
  if (nrho > (1+maxChangeFraction)*rho)
    wr = -maxChangeFraction*rho/drho;

  // pressure check
  Real np = p - dp;

  // Compute wp such that:

  //p - wp*dp >= (1-maxChangeFraction)*p
  if (np < (1-maxChangeFraction)*p)
    wp =  maxChangeFraction*p/dp;

  //p - wp*dp <= (1+maxChangeFraction)*p
  if (np > (1+maxChangeFraction)*p)
    wp = -maxChangeFraction*p/dp;

  updateFraction = MIN(wr, wp);
#endif
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  return true;
}


template <template <class> class PDETraitsSize>
void
Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q2D<QTypePrimitiveDistributionFunctions, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // Q2DPRIMITIVEDISTRIBUTIONFUNCTIONS_H
