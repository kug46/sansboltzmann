// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULERMITAVDIFFUSION3D_H
#define BCEULERMITAVDIFFUSION3D_H

// 3-D artificial viscosity BC class

#include <iostream>
#include <limits>
#include <string>

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "BCEuler3D.h"
#include "BCRANSSA3D.h"
#include "PDEEulermitAVDiffusion3D.h"

namespace SANS
{

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEmitAVSensor3D;


//----------------------------------------------------------------------------//
// BC class: 3-D artificial viscosity
//
// template parameters:
//   BCType               BC type (e.g. BCTypeDirichlet_mitState)
//----------------------------------------------------------------------------//
class BCTypeSymmetry_mitState;  //sensor value is set from interior
class BCTypeReflect_mitState;  //sensor value is set from interior
class BCTypeDirichlet_mitState; //sensor value is set from specified boundary value
class BCTypeFlux_mitState; //sensor value is set from specified boundary value
class BCTypeNeumann_mitState; //sensor value is set from interior


template <class BCType, class BCBase>
class BCmitAVSensor3D;

template <class BCType, class BCBaseParams>
struct BCmitAVSensor3DParams;

template <template <class> class PDETraitsSize, class PDETraitsModel>

using BCEulermitAVSensor3DVector = boost::mpl::vector5<
    BCNone<PhysD3,PDETraitsSize<PhysD3>::N>,
//    BCmitAVSensor3D<BCTypeSymmetry_mitState,
//                            BCEuler3D<BCTypeSymmetry_mitState, PDEEulermitAVDiffusion3D<PDETraitsSize, PDETraitsModel, PDEEuler3D>> >,
    BCmitAVSensor3D<BCTypeReflect_mitState,
                            BCEuler3D<BCTypeReflect_mitState, PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor3D<BCTypeFlux_mitState,
                            BCEuler3D<BCTypeFullState_mitState, PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor3D<BCTypeFlux_mitState,
                            BCEuler3D<BCTypeInflowSubsonic_PtTta_mitState,
                            PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor3D<BCTypeFlux_mitState,
                            BCEuler3D<BCTypeOutflowSubsonic_Pressure_mitState,
                            PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>> >
  >;

template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCRANSSAmitAVDiffusion3DVector = boost::mpl::vector4<
    BCmitAVSensor3D<BCTypeWall_mitState,
                            BCRANSSA3D< BCTypeWall_mitState,
                                        BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState,
                                                          PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>>> >,
    BCmitAVSensor3D<BCTypeSymmetry_mitState,
                            BCRANSSA3D< BCTypeNeumann_mitState,
                                        BCNavierStokes3D< BCTypeSymmetry_mitState,
                                                          PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>>> >,
    BCmitAVSensor3D<BCTypeReflect_mitState,
                            BCEuler3D<BCTypeReflect_mitState, PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor3D<BCTypeFlux_mitState,
                            BCRANSSA3D< BCTypeFullState_mitState,
                                        BCEuler3D< BCTypeFullState_mitState,
                                                   PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>>> >
  >;

//----------------------------------------------------------------------------//
// Symmetry BC : Sensor value is taken from interior
//----------------------------------------------------------------------------//

template <class BCBase>
class BCmitAVSensor3D<BCTypeSymmetry_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCBase::Category>::value, "BC category must match that of base class!");
  typedef typename BCBase::ParamsType ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor3D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  ~BCmitAVSensor3D() {}

  BCmitAVSensor3D( const BCmitAVSensor3D& ) = delete;
  BCmitAVSensor3D& operator=( const BCmitAVSensor3D& ) = delete;

  // BC state - NS and Euler
  template <class T>
  void state( const DLA::MatrixSymS<D,Real>& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, z, time, nx, ny, nz, qI, qB);
    qB(iSens) = qI(iSens); //evaluate sensor variable from interior
  }

  // BC state - RANS
  template <class T>
  void state( const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(param.right(), x, y, z, time, nx, ny, nz, qI, qB);
    qB(iSens) = qI(iSens); //evaluate sensor variable from interior
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Zero out any possible stabilization
    Fn[pde_.iEngy] = 0;

    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0, fy = 0, fz = 0;
    pde_.fluxAdvective(param, x, y, z, time, qB, fx, fy, fz);

    Fn += fx*nx + fy*ny + fz*nz;

    // Compute the viscous flux
    ArrayQ<T> fv = 0, gv = 0, hv = 0;
    pde_.fluxViscous(param, x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);

    // Compute normal x-, y- and z-momentum equations
    T xMomN = fv[pde_.ixMom]*nx + gv[pde_.ixMom]*ny + hv[pde_.ixMom]*nz;
    T yMomN = fv[pde_.iyMom]*nx + gv[pde_.iyMom]*ny + hv[pde_.iyMom]*nz;
    T zMomN = fv[pde_.izMom]*nx + gv[pde_.izMom]*ny + hv[pde_.izMom]*nz;

    // add only momentum viscous flux with zero tau_ns (n-normal, s-tangential coordinates)
    Fn[pde_.ixMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nx;
    Fn[pde_.iyMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*ny;
    Fn[pde_.izMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nz;
  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BCBase::pde_;
};


//----------------------------------------------------------------------------//
// Wall_mitState BC : sensor value is taken from interior
//----------------------------------------------------------------------------//

template <class BCBase>
class BCmitAVSensor3D<BCTypeWall_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCBase::Category>::value, "BC category must match that of base class!");

  typedef typename BCBase::ParamsType ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D, Real> VectorX;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor3D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  BCmitAVSensor3D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d) {}

  ~BCmitAVSensor3D() {}

  BCmitAVSensor3D( const BCmitAVSensor3D& ) = delete;
  BCmitAVSensor3D& operator=( const BCmitAVSensor3D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(param, x, y, z, time, nx, ny, nz, qI, qB);
    qB(iSens) = 0; //qI(iSens); //this should be *after* the call to the base class's state function
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const ParamTuple<DLA::MatrixSymS<D,Tp>, Real, TupleClass<>>& param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0, fy = 0, fz = 0;
    pde_.fluxAdvective(param, x, y, z, time, qB, fx, fy, fz);

    Fn += fx*nx + fy*ny + fz*nz;

    // Compute the viscous flux
    ArrayQ<T> fv = 0, gv = 0, hv = 0;
    pde_.fluxViscous(param, x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);

    Fn += fv*nx + gv*ny + hv*nz;

    // No energy flux at wall
    Fn[pde_.iEngy] = 0;

#if 0
    // SENSOR
    DLA::MatrixSymS<D,Tp> H = exp(get<0>(param)); //generalized h-tensor
    SANS::DLA::VectorS<D,Tp> hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = min(hPrincipal[0], min(hPrincipal[1], hPrincipal[2]));

    VectorX n = {nx, ny, nz};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    T lambda = 0;
    pde_.speedCharacteristic( param.right(), x, y, z, time, qI, lambda );

    Real lenfact = sqrt(C1_*C2_*Real(pde_.getOrder())/hmin)*hn;

    Fn[pde_.iSens] = lenfact*lambda*qI[pde_.iSens];
#endif
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;
protected:
  const Real C1_ = 3.0;
  const Real C2_ = 5.0;
  using BCBase::pde_;
};

//----------------------------------------------------------------------------//
// Reflect BC : Sensor value is taken from interior
//----------------------------------------------------------------------------//

template <class BCBase>
class BCmitAVSensor3D<BCTypeReflect_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCBase::Category>::value, "BC category must match that of base class!");
  typedef typename BCBase::ParamsType ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor3D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  ~BCmitAVSensor3D() {}

  BCmitAVSensor3D( const BCmitAVSensor3D& ) = delete;
  BCmitAVSensor3D& operator=( const BCmitAVSensor3D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(param, x, y, z, time, nx, ny, nz, qI, qB); //Base class takes care of everything
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Compute the advective flux based on interior and reflected boundary state
    pde_.fluxAdvectiveUpwind( param, x, y, z, time, qI, qB, nx, ny, nz, Fn );

    // Don't bother with viscous flux if it does not exist
    if (!pde_.hasFluxViscous()) return;

    // compute the mirrored gradients
    ArrayQ<T> qBx, qBy, qBz;

    stateGradient( nx, ny, nz,
                   qIx, qIy, qIz,
                   qBx, qBy, qBz );

    // Compute the viscous flux
    pde_.fluxViscous( param,  param, x, y, z, time, qI, qIx, qIy, qIz, qB, qBx, qBy, qBz, nx, ny, nz, Fn );
  }

  using BCBase::stateGradient;

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BCBase::pde_;
};

//----------------------------------------------------------------------------//
// Dirichlet_mitState BC : sensor value is taken from specified boundary value
//----------------------------------------------------------------------------//
template<class BCBaseParams>
struct BCmitAVSensor3DParams<BCTypeDirichlet_mitState, BCBaseParams> : public BCBaseParams
{
  const ParameterNumeric<Real> sensor{"sensor", NO_DEFAULT, NO_RANGE, "Specified sensor variable"};

  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.sensor));
    BCBaseParams::checkInputs(d);
//    d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor3DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor3DParams<BCTypeDirichlet_mitState, BCBaseParams> BCmitAVSensor3DParams<BCTypeDirichlet_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor3D<BCTypeDirichlet_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCBase::Category>::value, "BC category must match that of base class!");

  typedef BCmitAVSensor3DParams<BCTypeDirichlet_mitState, typename BCBase::ParamsType> ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor3D( const Real& sensor, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...), sensor_(sensor) {}

  BCmitAVSensor3D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d), sensor_(d.get(ParamsType::params.sensor)) {}

  ~BCmitAVSensor3D() {}

  BCmitAVSensor3D( const BCmitAVSensor3D& ) = delete;
  BCmitAVSensor3D& operator=( const BCmitAVSensor3D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(param, x, y, z, time, nx, ny, nz, qI, qB);
    qB(iSens) = sensor_; //this should be *after* the call to the base class's state function
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real sensor_;
};


//----------------------------------------------------------------------------//
// Flux_mitState BC : sensor value is taken from specified boundary value
//----------------------------------------------------------------------------//
template<class BCBaseParams>
struct BCmitAVSensor3DParams<BCTypeFlux_mitState, BCBaseParams> : public BCBaseParams
{
  const ParameterNumeric<Real> sensor{"sensor", 0.0, NO_RANGE, "Specified sensor variable"};

  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.sensor));
    BCBaseParams::checkInputs(d);
//    d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor3DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor3DParams<BCTypeFlux_mitState, BCBaseParams> BCmitAVSensor3DParams<BCTypeFlux_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor3D<BCTypeFlux_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCBase::Category>::value, "BC category must match that of base class!");

  typedef BCmitAVSensor3DParams<BCTypeFlux_mitState, typename BCBase::ParamsType> ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D, Real> VectorX;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor3D( const Real& sensor, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...), sensor_(sensor) {}

  BCmitAVSensor3D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d), sensor_(d.get(ParamsType::params.sensor)) {}

  ~BCmitAVSensor3D() {}

  BCmitAVSensor3D( const BCmitAVSensor3D& ) = delete;
  BCmitAVSensor3D& operator=( const BCmitAVSensor3D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(param, x, y, z, time, nx, ny, nz, qI, qB);
    qB(pde_.iSens) = qI(pde_.iSens); //this should be *after* the call to the base class's state function
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    BCBase::fluxNormal(param, x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);

    // SENSOR

    // Compute the viscous flux
    //ArrayQ<T> fv = 0, gv = 0, hv = 0;
    //pde_.fluxViscous(param, x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);
    //Fn[pde_.iSens] = fv[pde_.iSens]*nx + gv[pde_.iSens]*ny + hv[pde_.iSens]*nz;

    DLA::MatrixSymS<D,Real> H = exp(get<0>(param)); //generalized h-tensor

    VectorX n = {nx, ny, nz};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    //Get minimum length scale
//    SANS::DLA::VectorS<D,Real> hPrincipal;
//    DLA::EigenValues(H, hPrincipal);
//    Real hmin = min(hPrincipal[0], min(hPrincipal[1], hPrincipal[2]));
//
//    T lambda = 0;
//    pde_.speedCharacteristic( param.right(), x, y, z, time, qI, lambda );
//
//    Real lenfact = sqrt(C1_*C2_*Real(pde_.getOrder())/hmin)*hn;

    Fn(pde_.iSens) = sqrt(C2_)*hn*qI(pde_.iSens);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const DLA::MatrixSymS<D,Real>& param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    BCBase::fluxNormal(param, x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);

    // SENSOR

    // Compute the viscous flux
    //ArrayQ<T> fv = 0, gv = 0, hv = 0;
    //pde_.fluxViscous(param, x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);
    //Fn[pde_.iSens] = fv[pde_.iSens]*nx + gv[pde_.iSens]*ny + hv[pde_.iSens]*nz;

    DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor

    VectorX n = {nx, ny, nz};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(pde_.iSens) = sqrt(C2_)*hn*qI(pde_.iSens);
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real C1_ = 3.0;
  const Real C2_ = 5.0;
  const Real sensor_;
  using BCBase::pde_;
};

//----------------------------------------------------------------------------//
// Neumann_mitState BC : sensor value is taken from interior
//----------------------------------------------------------------------------//

template <class BCBase>
class BCmitAVSensor3D<BCTypeNeumann_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCBase::Category>::value, "BC category must match that of base class!");

  typedef typename BCBase::ParamsType ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor3D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  BCmitAVSensor3D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d) {}

  ~BCmitAVSensor3D() {}

  BCmitAVSensor3D( const BCmitAVSensor3D& ) = delete;
  BCmitAVSensor3D& operator=( const BCmitAVSensor3D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(param, x, y, z, time, nx, ny, nz, qI, qB);
    qB(iSens) = qI(iSens); //this should be *after* the call to the base class's state function
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    BCBase::fluxNormal(param, x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;
};

} //namespace SANS

#endif  // BCEULERMITAVDIFFUSION3D_H
