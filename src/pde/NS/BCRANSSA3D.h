// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCRANSSA3D_H
#define BCRANSSA3D_H

// 3-D compressible RANS-SA BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "PDERANSSA3D.h"

#include "pde/BCCategory.h"
#include "BCRANSSA3D_mitState.h"
#include "BCNavierStokes3D.h"
#include "BCEuler3D.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 3-D compressible RANS SA
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeWall_mitState;
class BCTypeDirichlet_mitState;
class BCTypeNeumann_mitState;

template <class BCType, class BCNS3D>
class BCRANSSA3D;

template <class BCType, class BCNS3DParams>
struct BCRANSA3DParams;


//RANS Slip walls are generally not appropriate. Please do not add a RANS slip wall, use symmetry instead

template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCRANSSA3DVector = boost::mpl::vector8<
  BCRANSSA3D<BCTypeWall_mitState,           BCNavierStokes3D< BCTypeWallNoSlipAdiabatic_mitState, PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA3D<BCTypeNeumann_mitState,        BCNavierStokes3D< BCTypeSymmetry_mitState,            PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >,
                                            BCEuler3D< BCTypeReflect_mitState,                    PDERANSSA3D<PDETraitsSize, PDETraitsModel>>,
  BCRANSSA3D<BCTypeFullState_mitState,      BCEuler3D< BCTypeFullState_mitState,                  PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA3D<BCTypeNeumann_mitState,        BCEuler3D< BCTypeOutflowSubsonic_Pressure_mitState,   PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA3D<BCTypeNeumann_mitState,        BCEuler3D< BCTypeOutflowSubsonic_Pressure_BN,         PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA3D<BCTypeDirichlet_mitState,      BCEuler3D< BCTypeInflowSubsonic_PtTta_mitState,       PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA3D<BCTypeFullState_mitState,      BCEuler3D< BCTypeFunction_mitState,                   PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >
  //BCRANSSA3D<BCTypeDirichlet_mitState,       BCEuler3D< BCTypeFunctionInflow_sHqt_mitState,        PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >,
  //BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN,  BCEuler3D< BCTypeInflowSubsonic_sHqt_BN,              PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >,
  //BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN,  BCEuler3D< BCTypeFunctionInflow_sHqt_BN,              PDERANSSA3D<PDETraitsSize, PDETraitsModel>> >
                                            >;

} //namespace SANS

#endif  // BCRANSSA3D_H
