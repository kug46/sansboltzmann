// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BoltzmannVariableD1Q3.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<BoltzmannVariableTypeD1Q3Params::VariableOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<BoltzmannVariableTypeD1Q3Params::VariableOptions> > const&) const;

void PrimitiveDistributionFunctionsD1Q3Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pdf0));
  allParams.push_back(d.checkInputs(params.pdf1));
  allParams.push_back(d.checkInputs(params.pdf2));
  d.checkUnknownInputs(allParams);
}
PrimitiveDistributionFunctionsD1Q3Params PrimitiveDistributionFunctionsD1Q3Params::params;



void BoltzmannVariableTypeD1Q3Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
BoltzmannVariableTypeD1Q3Params BoltzmannVariableTypeD1Q3Params::params;

}
