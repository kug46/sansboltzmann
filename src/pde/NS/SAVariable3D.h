// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SAVARIALBE3D_H
#define SAVARIALBE3D_H

#include "NSVariable3D.h"

namespace SANS
{

//===========================================================================//
template<class Derived>
struct SAVariableType3D
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//===========================================================================//
template<class NSVariableParams>
struct SAnt3DParams : public NSVariableParams
{
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "S-A Working Variable"};

  static void checkInputs(PyDict d);

  // cppcheck-suppress duplInheritedMember
  static SAnt3DParams params;
};

//---------------------------------------------------------------------------//
template<class NSVariables>
struct SAnt3D;

template<template<class> class NSVariables, class T>
struct SAnt3D<NSVariables<T>> : public SAVariableType3D<SAnt3D<NSVariables<T>>>,
                                public NSVariables<T>
{
  typedef SAnt3DParams<typename NSVariables<T>::ParamsType> ParamsType;

  SAnt3D() {}

  explicit SAnt3D( const PyDict& d )
    : NSVariables<T>(d), SANutilde( d.get(ParamsType::params.nt) ) {}

  // cppcheck-suppress noExplicitConstructor
  SAnt3D( const std::initializer_list<T>& s ) { operator=(s); }

  SAnt3D( const NSVariables<T>& var, const T& nt )
    : NSVariables<T>(var), SANutilde(nt) {}

  SAnt3D& operator=( const std::initializer_list<T>& s )
  {
    SANS_ASSERT(s.size() == 6);
    auto q = s.begin();
    NSVariables<T>::operator=({*(q+0),*(q+1),*(q+2),*(q+3),*(q+4)});
    SANutilde = *(q+5);

    return *this;
  }

  T SANutilde;
};

//===========================================================================//
template<>
struct SAnt3DParams<Conservative3DParams> : public Conservative3DParams
{
  const ParameterNumeric<Real> rhont{"rhont", NO_DEFAULT, NO_RANGE, "S-A Working Variable"};

  static void checkInputs(PyDict d);

  // cppcheck-suppress duplInheritedMember
  static SAnt3DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct SAnt3D<Conservative3D<T>> : public SAVariableType3D<SAnt3D<Conservative3D<T>>>,
                                   public Conservative3D<T>
{
  typedef SAnt3DParams<typename Conservative3D<T>::ParamsType> ParamsType;

  SAnt3D() {}

  explicit SAnt3D( const PyDict& d )
    : Conservative3D<T>(d), rhoSANutilde( d.get(ParamsType::params.rhont) ) {}

  // cppcheck-suppress noExplicitConstructor
  SAnt3D( const std::initializer_list<T>& s ) { operator=(s); }

  SAnt3D( const Conservative3D<T>& var, const T& rhont )
    : Conservative3D<T>(var), rhoSANutilde(rhont) {}

  SAnt3D& operator=( const std::initializer_list<T>& s )
  {
    SANS_ASSERT(s.size() == 6);
    auto q = s.begin();
    Conservative3D<T>::operator=({*(q+0),*(q+1),*(q+2),*(q+3),*(q+4)});
    rhoSANutilde = *(q+5);

    return *this;
  }

  T rhoSANutilde;
};

//===========================================================================//
struct SAVariableType3DParams : noncopyable
{
  struct VariableOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Variables{"Variables", NO_DEFAULT, "Name of the variable set" };
    const ParameterString& key = Variables;

    const DictOption Conservative{"Conservative", SAnt3DParams<Conservative3DParams>::checkInputs};
    const DictOption PrimitiveTemperature{"Primitive Temperature", SAnt3DParams<DensityVelocityTemperature3DParams>::checkInputs};
    const DictOption PrimitivePressure{"Primitive Pressure", SAnt3DParams<DensityVelocityPressure3DParams>::checkInputs};

    const std::vector<DictOption> options{Conservative,PrimitiveTemperature,PrimitivePressure};
  };
  const ParameterOption<VariableOptions> StateVector{"StateVector", NO_DEFAULT, "The state vector of variables"};

  static void checkInputs(PyDict d);

  static SAVariableType3DParams params;
};


}

#endif //SAVARIALBE3D_H
