// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEuler1D_H
#define BCEuler1D_H

// 1-D Euler BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "TraitsEuler.h"
#include "PDEEuler1D.h"
#include "PDEEuler1D_Source.h"
#include "PDEEulermitAVDiffusion1D.h"
#include "SolutionFunction_Euler1D.h"

#include "BCEuler1D_mitState.h"
#include "BCEuler1D_mitState_ST.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeTimeOut;
class BCTypeReflect;
class BCTypeInflowSupersonic;
class BCTypeOutflowSubsonic_Pressure;

template <class BCType, class PDEEuler1D>
class BCEuler1D;

template <class BCType>
struct BCEuler1DParams;


template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCEuler1DVector = boost::mpl::vector15<
    BCNone<PhysD1,PDETraitsSize<PhysD1>::N>,
    SpaceTimeBC< BCEuler1D<BCTypeTimeOut             , PDEEuler1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCEuler1D<BCTypeTimeIC_Function     , PDEEuler1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCEuler1D<BCTypeTimeIC,               PDEEuler1D<PDETraitsSize, PDETraitsModel>> >,
//    BCEuler1D<BCTypeReflect                          , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
//    BCEuler1D<BCTypeInflowSupersonic                 , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
//    BCEuler1D<BCTypeOutflowSubsonic_Pressure         , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC< BCEuler1D<BCTypeFunction_mitState                , PDEEuler1D<PDETraitsSize, PDETraitsModel>> >,
    BCEuler1D<BCTypeFullState_mitState               , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC< BCEuler1D<BCTypeFullStateSpaceTime_mitState , PDEEuler1D<PDETraitsSize, PDETraitsModel>> >,
    BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
    BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
    BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC< BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDEEuler1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDEEuler1D<PDETraitsSize, PDETraitsModel>> >,
    BCEuler1D<BCTypeReflect_mitState                 , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
    BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState     , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC<BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState     , PDEEuler1D<PDETraitsSize, PDETraitsModel>>>
                                          >;
template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCEuler1D_ArtificialViscosityVector = boost::mpl::vector14<
    SpaceTimeBC< BCNone<PhysD1,PDETraitsSize<PhysD1>::N> >,
    SpaceTimeBC< BCEuler1D<BCTypeTimeIC,               PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCEuler1D<BCTypeTimeIC_Function     , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
//    BCEuler1D<BCTypeReflect                          , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
//    BCEuler1D<BCTypeInflowSupersonic                 , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
//    BCEuler1D<BCTypeOutflowSubsonic_Pressure         , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC< BCEuler1D<BCTypeFunction_mitState     , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    BCEuler1D<BCTypeFullState_mitState               , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> ,
    SpaceTimeBC< BCEuler1D<BCTypeFullStateSpaceTime_mitState , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC<BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC<BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    BCEuler1D<BCTypeReflect_mitState                 , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState     , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC<BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState     , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>>
                                          >;

//----------------------------------------------------------------------------//
// reflection BC:
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeReflect> : noncopyable
{
  static constexpr const char* BCName{"Reflect"};
  struct Option
  {
    const DictOption Reflect{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeReflect, PDE_> :
    public BCType< BCEuler1D<BCTypeReflect, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeReflect BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCEuler1DParams<BCTypeReflect> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDE::QType QType;
  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde, const ArrayQ<Real>& bcdata ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    bcdata_(bcdata)
  {
    // Nothing
  }
  ~BCEuler1D() {}

  BCEuler1D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    bcdata_({0,0,0})
  {
    // Nothing
  }

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T>
  void weightBC( const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wght ) const;

  template <class T, class L, class R, class W>
  void weightBC( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wght ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& time, const Real& nx,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wghtLG ) const;

  template <class T, class L, class R, class W>
  void weightLagrange( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wghtLG ) const;

  // Lagrange multiplier rhs
  // ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& time, const Real& nx,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rhsLG ) const;

  template <class T, class L, class R>
  void rhsLagrange( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rhsLG ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> bcdata_;

  // BC jacobian: A = d(a)/d(uCons), where uCons is conservation variables
  template <class T>
  void jacobianBC( const Real& x, const Real& time, const Real& nx,
                   const ArrayQ<T>& q, MatrixQ<T>& A ) const;

  // BC jacobian nullspace: A.N = 0
  template <class T>
  void nullspaceBC( const Real& x, const Real& time, const Real& nx,
                    const ArrayQ<T>& q, MatrixQ<T>& N ) const;

  // units consistency matrix: M
  template <class T>
  void unitsBC( const Real& x, const Real& time, const Real& nx,
                const ArrayQ<T>& q, MatrixQ<T>& M ) const;
};


template <class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeReflect, PDE_>::strongBC(
    const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rsdBC ) const
{
  T rho, u, t = 0;

  qInterpret_.eval( q, rho, u, t );

  rsdBC(0) = nx*u;
  rsdBC(1) = 0;
  rsdBC(2) = 0;
}


template <class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeReflect, PDE_>::strongBC(
    const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
{
  T rho, u, t, e, E = 0;

  qInterpret_.eval( q, rho, u, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u);

  rsdBC(0) = nx*u;
  rsdBC(1) = lg(1) - rho*E;
  rsdBC(2) = lg(2) - rho;
}


template <class PDE_>
template <class T, class TL, class TR, class R>
inline void
BCEuler1D<BCTypeReflect, PDE_>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeReflect, PDE_>::strongBC not implemented with params");
}


template <class PDE_>
template <class T, class TL, class TR, class R>
inline void
BCEuler1D<BCTypeReflect, PDE_>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeReflect, PDE_>::strongBC not implemented with params");
}


template <class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeReflect, PDE_>::weightBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wghtBC ) const
{
  T rho, u, t, h, H, qn = 0;

  qInterpret_.eval( q, rho, u, t );
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u);
  qn = nx*u;

  // B^t = Fn M A^t ( A M A^t )^{-1}
  wghtBC = 0;
  wghtBC(0,0) = rho;
  wghtBC(1,0) = rho*(u + nx*qn);
  wghtBC(2,0) = rho*(H + qn*qn);
}


template <class PDE_>
template <class T, class L, class R, class W>
inline void
BCEuler1D<BCTypeReflect, PDE_>::weightBC(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wghtBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeReflect, PDE_>::weightBC not implemented with params");
}


template <class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeReflect, PDE_>::weightLagrange(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wghtLG ) const
{
  Real gamma = gas_.gamma();
  T rho, u, t, qn = 0;

  qInterpret_.eval( q, rho, u, t );
  qn = nx*u;

  // \bar{A}^t = Fn N
  wghtLG = 0;

  wghtLG(0,0) = 0;
  wghtLG(1,0) = (gamma - 1)*nx;
  wghtLG(2,0) = gamma*qn;

  wghtLG(0,1) = 0;
  wghtLG(1,1) = 0;
  wghtLG(2,1) = 0;
}


template <class PDE_>
template <class T, class L, class R, class W>
inline void
BCEuler1D<BCTypeReflect, PDE_>::weightLagrange(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wghtLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeReflect, PDE_>::weightLagrange not implemented with params");
}


template <class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeReflect, PDE_>::rhsLagrange(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rhsLG ) const
{
#if 0
  const char msg[] = "Error in rhsLagrange: not yet implemented for nonlinear\n";
  SANS_DEVELOPER_EXCEPTION( msg );
#else
  // NOTE: this is not completely correct, but serves as place holder

  T rho, u, t, e, E = 0;

  qInterpret_.eval( q, rho, u, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u);

  rhsLG(0) = rho*E;
  rhsLG(1) = 0;
  rhsLG(2) = 0;     // dummy data
#endif
}


template <class PDE_>
template <class T, class L, class R>
inline void
BCEuler1D<BCTypeReflect, PDE_>::rhsLagrange(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rhsLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeReflect, PDE_>::rhsLagrange not implemented with params");
}


// NOTE: not actually used, but here for reference/documentation
template <class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeReflect, PDE_>::jacobianBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, MatrixQ<T>& A ) const
{
  T rho, u, t = 0;

  qInterpret_.eval( q, rho, u, t );

  A = 0;
  A(0,0) = -(nx*u)/rho;
  A(0,1) = nx/rho;
  A(0,2) = 0;
}


// NOTE: not actually used, but here for reference/documentation
template <class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeReflect, PDE_>::nullspaceBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, MatrixQ<T>& N ) const
{
  T rho, u, t = 0;

  qInterpret_.eval( q, rho, u, t );

  N = 0;

  N(0,0) = 0;
  N(1,0) = 0;
  N(2,0) = 1;

  N(0,1) = 0;
  N(1,1) = 0;
  N(2,1) = 0;
}


// NOTE: not actually used, but here for reference/documentation
template <class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeReflect, PDE_>::unitsBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, MatrixQ<T>& M ) const
{
  T rho, u, t, p, e, h, E, H = 0;

  qInterpret_.eval( q, rho, u, t );
  p = gas_.pressure(rho, t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  E = e + 0.5*(u*u);
  H = h + 0.5*(u*u);

  M(0,0) = rho;
  M(0,1) = rho*u;
  M(0,2) = rho*E;

  M(1,1) = rho*u*u + p;
  M(1,2) = rho*u*H;

  M(2,2) = rho*H*H - h*p;

  M(1,0) = M(0,1);

  M(2,0) = M(0,2);
  M(2,1) = M(1,2);
}


template <class PDE_>
void
BCEuler1D<BCTypeReflect, PDE_>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler1D<BCTypeReflect, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeReflect, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeReflect, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeReflect, PDE>: bcdata_ = " << std::endl;
  bcdata_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// supersonic inflow BC:
//
// specify: conservation variables
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeInflowSupersonic> : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, NO_RANGE, "Density"};
  const ParameterNumeric<Real> rhou{"rhou", NO_DEFAULT, NO_RANGE, "MomentumX"};
  const ParameterNumeric<Real> rhoE{"rhoE", NO_DEFAULT, NO_RANGE, "TotalEnergy"};

  static constexpr const char* BCName{"InflowSupersonic"};
  struct Option
  {
    const DictOption InflowSupersonic{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeInflowSupersonic BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCEuler1DParams<BCTypeInflowSupersonic> ParamsType;

  static const int D = PDETraitsSize<PhysDim>::D;   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDETraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;     // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDEEuler1D<PDETraitsSize, PDETraitsModel>& pde, const ArrayQ<Real>& bcdata ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    bcdata_(bcdata),
    rho_(bcdata(0)),
    rhou_(bcdata(1)),
    rhoE_(bcdata(2))
  {
    // Nothing
  }

  ~BCEuler1D() {}

  BCEuler1D(const PDEEuler1D<PDETraitsSize, PDETraitsModel>& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    bcdata_({d.get(ParamsType::params.rho), d.get(ParamsType::params.rhou), d.get(ParamsType::params.rhoE)} ),
    rho_(d.get(ParamsType::params.rho)),
    rhou_(d.get(ParamsType::params.rhou)),
    rhoE_(d.get(ParamsType::params.rhoE))
  {
    // Nothing
  }

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T>
  void weightBC( const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wght ) const;

  template <class T, class L, class R, class W>
  void weightBC( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wght ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& time, const Real& nx,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wghtLG ) const;

  template <class T, class L, class R, class W>
  void weightLagrange( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wghtLG ) const;

  // Lagrange multiplier rhs
  // ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& time, const Real& nx,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rhsLG ) const;

  template <class T, class L, class R>
  void rhsLagrange( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rhsLG ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDEEuler1D<PDETraitsSize, PDETraitsModel>& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> bcdata_;
  const Real rho_;
  const Real rhou_;
  const Real rhoE_;

  // BC jacobian: A = d(a)/d(uCons), where uCons is conservation variables
  template <class T>
  void jacobianBC( const Real& x, const Real& time, const Real& nx,
                   const ArrayQ<T>& q, MatrixQ<T>& A ) const;

  // units consistency matrix: M
  template <class T>
  void unitsBC( const Real& x, const Real& time, const Real& nx,
                const ArrayQ<T>& q, MatrixQ<T>& M ) const;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rsdBC ) const
{
  T rho, u, t, e, E = 0;

  qInterpret_.eval( q, rho, u, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u);

  rsdBC(0) = rho   - rho_;
  rsdBC(1) = rho*u - rhou_;
  rsdBC(2) = rho*E - rhoE_;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
{
  T rho, u, t, e, E = 0;

  qInterpret_.eval( q, rho, u, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u);

  rsdBC(0) = rho   - rho_;
  rsdBC(1) = rho*u - rhou_;
  rsdBC(2) = rho*E - rhoE_;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wghtBC ) const
{
  MatrixQ<T> F;

  F = 0;
  pde_.jacobianFluxAdvective( x, time, q, F );

  wghtBC = nx*F;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class L, class R, class W>
inline void
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wghtBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<>>::weightBC not implemented with params");
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::jacobianBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, MatrixQ<T>& A ) const
{
  A = 0;
  A(0,0) = 1;
  A(1,1) = 1;
  A(2,2) = 1;
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::unitsBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, MatrixQ<T>& M ) const
{
  T rho, u, t, p, e, h, E, H;

  qInterpret_.eval( q, rho, u, t );
  p = gas_.pressure(rho, t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  E = e + 0.5*(u*u);
  H = h + 0.5*(u*u);

  M(0,0) = rho;
  M(0,1) = rho*u;
  M(0,2) = rho*E;

  M(1,1) = rho*u*u + p;
  M(1,2) = rho*u*H;

  M(2,2) = rho*H*H - h*p;

  M(1,0) = M(0,1);

  M(2,0) = M(0,2);
  M(2,1) = M(1,2);
}


// is the boundary state valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
bool
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::isValidState(const Real& nx, const ArrayQ<Real>& q) const
{
  Real rho, u, t = 0;

  qInterpret_.eval( q, rho, u, t );

  return ((rho*u * rhou_) >= 0);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeInflowSupersonic, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: bcdata_ = " << std::endl;
  bcdata_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// subsonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeOutflowSubsonic_Pressure> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSubsonic_Pressure"};
  struct Option
  {
    const DictOption OutflowSubsonic_Pressure{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeOutflowSubsonic_Pressure BCType;
  typedef typename BCCategory::Dirichlet_sansLG Category;
  typedef BCEuler1DParams<BCType> ParamsType;

  static const int D = PDETraitsSize<PhysDim>::D;   // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDETraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;     // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDEEuler1D<PDETraitsSize, PDETraitsModel>& pde, const ArrayQ<Real>& bcdata ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    bcdata_(bcdata), pSpec_(bcdata(0))
  {
    // Nothing
  }
  ~BCEuler1D() {}

  BCEuler1D( const PDEEuler1D<PDETraitsSize, PDETraitsModel>& pde, const PyDict& d ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    bcdata_({d.get(ParamsType::params.pSpec), 0, 0} ),
    pSpec_(d.get(ParamsType::params.pSpec) )
  {
    // Nothing
  }

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // BC residual: a(u) - b
  template <class T>
  void strongBC( const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rsdBC ) const;

  template <class T>
  void strongBC( const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rsdBC ) const;

  template <class T, class TL, class TR, class R>
  void strongBC( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const;

  // conventional formulation BC weighting function
  // B^t = Fn M A^t ( A M A^t )^{-1}
  template <class T>
  void weightBC( const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wght ) const;

  template <class T, class L, class R, class W>
  void weightBC( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wght ) const;

  // Lagrange multiplier weighting function
  // \bar{A}^t = Fn N
  template <class T>
  void weightLagrange( const Real& x, const Real& time, const Real& nx,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wghtLG ) const;

  template <class T, class L, class R, class W>
  void weightLagrange( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                       const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wghtLG ) const;

  // Lagrange multiplier rhs
  // ( N^t M^{-1} N )^{-1} N^t M^{-1}
  template <class T>
  void rhsLagrange( const Real& x, const Real& time, const Real& nx,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rhsLG ) const;

  template <class T, class L, class R>
  void rhsLagrange( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
                    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rhsLG ) const;
  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDEEuler1D<PDETraitsSize, PDETraitsModel>& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> bcdata_;
  const Real pSpec_;

  // BC jacobian: A = d(a)/d(uCons), where uCons is conservation variables
  template <class T>
  void jacobianBC( const Real& x, const Real& time, const Real& nx,
                   const ArrayQ<T>& q, MatrixQ<T>& A ) const;

  // BC jacobian nullspace: A.N = 0
  template <class T>
  void nullspaceBC( const Real& x, const Real& time, const Real& nx,
                    const ArrayQ<T>& q, MatrixQ<T>& N ) const;

  // units consistency matrix: M
  template <class T>
  void unitsBC( const Real& x, const Real& time, const Real& nx,
                const ArrayQ<T>& q, MatrixQ<T>& M ) const;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rsdBC ) const
{
  T rho=0, u=0, t=0, p = 0;

  qInterpret_.eval( q, rho, u, t );
  p = gas_.pressure(rho, t);

  rsdBC(0) = p - pSpec_;
  rsdBC(1) = 0;
  rsdBC(2) = 0;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<T>& rsdBC ) const
{
  T rho=0, u=0, t=0, p = 0;

  qInterpret_.eval( q, rho, u, t );
  p = gas_.pressure(rho, t);

  rsdBC(0) = p - pSpec_;
  rsdBC(1) = 0;   // should be LG residuals, but zeroed out for now
  rsdBC(2) = 0;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<>>::strongBC not implemented with params");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class TL, class TR, class R>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::strongBC(
    const ParamTuple<TL, TR, TupleClass<>>& param, const Real&, const Real&, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& lg, ArrayQ<R>& rsdBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<>>::strongBC not implemented with params");
}


// conventional formulation BC weighting function
// B^t = Fn M A^t ( A M A^t )^{-1}
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wghtBC ) const
{
  T rho=0, u=0, t=0, h=0, H=0, qn=0, c = 0;
  Real area=0, dAdx = 0.0;

  pde_.area(x, time, area, dAdx);

  qInterpret_.eval( q, rho, u, t );
  c = gas_.speedofSound(t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u);
  qn = nx*u;

  // B^t = Fn M A^t ( A M A^t )^{-1}
  wghtBC = 0;
  wghtBC(0,0) =      qn  /(c*c);
  wghtBC(1,0) = nx + qn*u/(c*c);
  wghtBC(2,0) = qn + qn*H/(c*c);

  wghtBC *= area;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class L, class R, class W>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::weightBC(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wghtBC ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<>>::weightBC not implemented with params");
}


// Lagrange multiplier weighting function
// \bar{A}^t = Fn N
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& wghtLG ) const
{
  T rho=0, u=0, t=0, h=0, H=0, qn = 0;
  Real area=0, dAdx = 0.0;

  pde_.area(x, time, area, dAdx);

  qInterpret_.eval( q, rho, u, t );
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u);
  qn = nx*u;

  // \bar{A}^t = Fn N
  wghtLG = 0;

  wghtLG(0,0) = 0;
  wghtLG(1,0) = qn*u;
  wghtLG(2,0) = qn*(H + 0.5*(u*u));

  wghtLG(0,1) = nx;
  wghtLG(1,1) = nx*u + qn;
  wghtLG(2,1) = nx*H + qn*u;

  wghtLG *= area;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class L, class R, class W>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::weightLagrange(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<W>& wghtLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<>>::weightLagrange not implemented with params");
}


// Lagrange multiplier rhs
// B = ( N^t M^{-1} N )^{-1} N^t M^{-1}
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<T>& rhsLG ) const
{
  const char msg[] = "Error in rhsLagrange: not yet implemented for nonlinear\n";
  SANS_DEVELOPER_EXCEPTION( msg );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class L, class R>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::rhsLagrange(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, ArrayQ<R>& rhsLG ) const
{
  SANS_DEVELOPER_EXCEPTION("BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<>>::rhsLagrange not implemented with params");
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::jacobianBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, MatrixQ<T>& A ) const
{
  Real gamma = gas_.gamma();
  T rho=0, u=0, t = 0;

  qInterpret_.eval( q, rho, u, t );

  A = 0;
  A(0,0) = (gamma - 1)*(0.5*(u*u));
  A(0,1) = (gamma - 1)*(-u);
  A(0,2) = (gamma - 1);
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::nullspaceBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, MatrixQ<T>& N ) const
{
  T rho=0, u=0, t = 0;

  qInterpret_.eval( q, rho, u, t );

  N = 0;

  N(0,0) = -1;
  N(1,0) = 0;
  N(2,0) = 0.5*(u*u);

  N(0,1) = 0;
  N(1,1) = 1;
  N(2,1) = u;
}


// NOTE: not actually used, but here for reference/documentation
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::unitsBC(
    const Real& x, const Real& time, const Real& nx,
    const ArrayQ<T>& q, MatrixQ<T>& M ) const
{
  T rho=0, u=0, t=0, p=0, e=0, h=0, E=0, H = 0;

  qInterpret_.eval( q, rho, u, t );
  p = gas_.pressure(rho, t);
  e = gas_.energy(rho, t);
  h = gas_.enthalpy(rho, t);
  E = e + 0.5*(u*u);
  H = h + 0.5*(u*u);

  M(0,0) = rho;
  M(0,1) = rho*u;
  M(0,2) = rho*E;

  M(1,1) = rho*u*u + p;
  M(1,2) = rho*u*H;

  M(2,2) = rho*H*H - h*p;

  M(1,0) = M(0,1);
  M(2,0) = M(0,2);
  M(2,1) = M(1,2);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeOutflowSubsonic_Pressure, PDEEuler1D<PDETraitsSize, PDETraitsModel>>: bcdata_ = " << std::endl;
  bcdata_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // BCEuler1D_H
