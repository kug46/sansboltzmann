// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDERANSSA_BRENNER2D_H
#define PDERANSSA_BRENNER2D_H

// 2-D compressible RANS with Spalart-Allmaras PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Topology/Dimension.h"
#include "pde/AnalyticFunction/Function2D.h"

#include "GasModel.h"
#include "SAVariable2D.h"
#include "PDENavierStokes2D.h"
#include "PDENavierStokes_Brenner2D.h"
#include "PDERANSSA2D.h"

#include <iostream>
#include <string>
#include <cmath> // sqrt

namespace SANS
{

// power<N,T>(x):  power template
// reference: Walter Brown, C++ Meta<Programming> Concepts and Results
// e.g. power<3>(x) = x*x*x;

//template <int N, class T>
//inline T power( const T& x )
//{
//  return ((N == 0) ? 1 : ((N == 1) ? x : power<N%2,T>(x) * power<N/2,T>(x*x)));
//}


// forward declare: RANS with SA variables interpreter
template<class QEulerType, template <class> class PDETraitsSize>
class QRANSSA2D;

//----------------------------------------------------------------------------//
// PDE class: 2-D compressible RANS with Spalart-Allmaras turbulence model
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//   QType                    solution variable set (e.g. primitive)
//   ViscosityModel           molecular viscosity model type
//   PDETraitsSize            define PDE size-related features
//     N, D                   PDE size, physical dimensions
//     ArrayQ                 solution/residual arrays
//     MatrixQ                matrices (e.g. flux jacobian)
//   PDETraitsModel           define PDE model-related features
//     QType                  solution variable set (e.g. primitive)
//     QInterpret             solution variable interpreter
//     GasModel               gas model (e.g. ideal gas law)
//     ViscosityModel         molecular viscosity model (e.g. Sutherland)
//     ThermalConductivityModel   thermal conductivity model (e.g. const Prandtl number)
//
// member functions:
//   .hasFluxConservative     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//   .masterState        unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDERANSSA_Brenner2D : public PDERANSSA2D<PDETraitsSize, PDETraitsModel>
{
  friend class PDERANSSA2D<PDETraitsSize, PDETraitsModel>;
public:
  typedef PhysD2 PhysDim;

  typedef PDERANSSA2D<PDETraitsSize, PDETraitsModel> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;                                        // cartesian vectors

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef typename PDETraitsModel::QType QType;
  typedef QRANSSA2D<QType, PDETraitsSize> QInterpret;                                 // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                                 // gas model

  typedef typename PDETraitsModel::ViscosityModel ViscosityModel;                     // molecular viscosity model

  typedef typename PDETraitsModel::ThermalConductivityModel ThermalConductivityModel; // thermal conductivity model

  typedef SAVariableType2DParams VariableType2DParams;

  // Solution pointer rather than MMS pointer due to distance function
  typedef Function2DBase<ArrayQ<Real>> FunctionBase;

  PDERANSSA_Brenner2D( const GasModel& gas, const ViscosityModel& visc,
         const ThermalConductivityModel& tcond,
         const EulerResidualInterpCategory cat = Euler_ResidInterp_Raw,
         const RoeEntropyFix entropyFix = eVanLeer,
         const Real rhodiff = 4./3.,
         const std::shared_ptr<FunctionBase>& soln = nullptr) :
      BaseType( gas, visc, tcond, cat, entropyFix, nullptr ),
      soln_(soln),
      qInterpret_(gas),
      rhodiff_(rhodiff)
  {
    BaseType::setCoeffients();
  }

  using BaseType::iCont;
  using BaseType::ixMom;
  using BaseType::iyMom;
  using BaseType::iEngy;
  using BaseType::iSA;

  PDERANSSA_Brenner2D& operator=( const PDERANSSA_Brenner2D& ) = delete;

  // flux components
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return true; }
  bool needsSolutionGradientforSource() const { return true; }

  bool hasForcingFunction() const { return soln_ != nullptr; }


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    fluxViscous(x, y, time, q, qx, qy, f, g);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& distL, const Real& distR,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {
    diffusionViscous(x, y, time, q, qx ,qy, kxx, kxy, kyx, kyy);
  }

  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,  MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }


protected:

  const std::shared_ptr<FunctionBase> soln_;
  const QInterpret qInterpret_;         // solution variable interpreter class
  const Real rhodiff_;

  using BaseType::gas_;
  using BaseType::visc_;
  using BaseType::tcond_;

  using BaseType::cb1_;
  using BaseType::cb2_;
  using BaseType::sigma_;
  using BaseType::vk_;
  using BaseType::cw1_;
  using BaseType::cw2_;
  using BaseType::cw3_;
  using BaseType::cv1_;
  using BaseType::ct1_;
  using BaseType::ct2_;
  using BaseType::ct3_;
  using BaseType::ct4_;
  using BaseType::prandtlEddy_;
  using BaseType::rlim_;
  using BaseType::cv2_;
  using BaseType::cv3_;
  using BaseType::cn1_;
};


// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA_Brenner2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  // SA fluxes

  Tq rho=0, u=0, v=0, t=0, nt=0;
  T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;
//  T  ntx=0, nty=0;
  Tq mu, k;

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalTemperature( q, t );
  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  // SA model viscous terms

  qInterpret_.evalSA( q, nt );

  // Reynolds stresses

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  Tq chi  = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Tq fv1 = chi3 / (chi3 + power<3,Tq>(cv1_));
    Tq muEddy = rho*nt*fv1;
    Real Cp = this->gas_.Cp();

    // Augment the molecular viscosity and thermal conductivity
    mu += muEddy;
    k  += muEddy*Cp/prandtlEddy_;
  }

  Tq delta = rhodiff_*mu/rho;

  Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  f(iCont) -= delta*rhox;
  f(ixMom) -= delta*u*rhox;
  f(iyMom) -= delta*v*rhox;
  f(iEngy) -= delta*e0*rhox;
  f(iSA)   -= delta*nt*rhox;

  g(iCont) -= delta*rhoy;
  g(ixMom) -= delta*u*rhoy;
  g(iyMom) -= delta*v*rhoy;
  g(iEngy) -= delta*e0*rhoy;
  g(iSA)   -= delta*nt*rhoy;

  // Compute the viscous flux
  BaseType::fluxViscous(x, y, time, q, qx, qy, f, g );
}


// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA_Brenner2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscous(x, y, time, qL, qxL, qyL, fL, gL);
  fluxViscous(x, y, time, qR, qxR, qyR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;
#else
  // SA fluxes

  T rhoL, uL, vL, tL, ntL;
  T rhoR, uR, vR, tR, ntR;
  T ntxL, ntyL;
  T ntxR, ntyR;
  T rho, t, nt;
  T ntx, nty;
  T mu;

  qInterpret_.evalDensity( qL, rhoL );
  qInterpret_.evalDensity( qR, rhoR );
  qInterpret_.evalTemperature( qL, tL );
  qInterpret_.evalTemperature( qR, tR );

  rho = 0.5*(rhoL + rhoR);
  t   = 0.5*(tL + tR);
  mu = visc_.viscosity( t );

  qInterpret_.evalSA( qL, ntL );
  qInterpret_.evalSA( qR, ntR );
  qInterpret_.evalSAGradient( qL, qxL, ntxL );
  qInterpret_.evalSAGradient( qL, qyL, ntyL );
  qInterpret_.evalSAGradient( qR, qxR, ntxR );
  qInterpret_.evalSAGradient( qR, qyR, ntyR );

  nt  = 0.5*(ntL + ntR);
  ntx = 0.5*(ntxL + ntxR);
  nty = 0.5*(ntyL + ntyR);

  if (nt < 0)
  {
    T chi = rho*nt/mu;
    T fn  = (cn1_ + power<3,T>(chi)) / (cn1_ - power<3,T>(chi));
    f(iSA) -= (mu + rho*nt*fn)*(nx*ntx + ny*nty)/sigma_;
  }
  else
  {
    f(iSA) -= (mu + rho*nt)*(nx*ntx + ny*nty)/sigma_;
  }

  // Reynolds stresses

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    T rhoxL, rhoyL, uxL, uyL, vxL, vyL, txL, tyL;
    T rhoxR, rhoyR, uxR, uyR, vxR, vyR, txR, tyR;
    T u, v;
    T ux, uy, vx, vy, tx, ty;
    T tauxx, tauxy, tauyy;
    T muEddy;
    T Cp, qheat;

    qInterpret_.eval( qL, rhoL, uL, vL, tL );
    qInterpret_.eval( qR, rhoR, uR, vR, tR );
    qInterpret_.evalGradient( qL, qxL, rhoxL, uxL, vxL, txL );
    qInterpret_.evalGradient( qL, qyL, rhoyL, uyL, vyL, tyL );
    qInterpret_.evalGradient( qR, qxR, rhoxR, uxR, vxR, txR );
    qInterpret_.evalGradient( qR, qyR, rhoyR, uyR, vyR, tyR );

    u = 0.5*(uL + uR);
    v = 0.5*(vL + vR);
    t = 0.5*(tL + tR);

    ux = 0.5*(uxL + uxR);
    uy = 0.5*(uyL + uyR);
    vx = 0.5*(vxL + vxR);
    vy = 0.5*(vyL + vyR);
    tx = 0.5*(txL + txR);
    ty = 0.5*(tyL + tyR);

    T chi = rho*nt/mu;
    T fv1 = power<3,T>(chi) / (power<3,T>(chi) + power<3>(cv1_));
    muEddy = rho*nt*fv1;
    //std::cout << "muEddy = " << muEddy << std::endl;
    //std::cout << "fv1 = " << fv1 << std::endl;

    tauxx = muEddy*(2*ux - (2/3.)*(ux + vy));
    tauxy = muEddy*(uy + vx);
    tauyy = muEddy*(2*vy - (2/3.)*(ux + vy));

    Cp = this->gas_.Cp();
    qheat = (muEddy*Cp/prandtlEddy_)*(nx*tx + ny*ty);

    f(ixMom) -= nx*tauxx + ny*tauxy;
    f(iyMom) -= nx*tauxy + ny*tauyy;
    f(iEngy) -= qheat + u*(nx*tauxx + ny*tauxy) + v*(nx*tauxy + ny*tauyy);
  }

  BaseType::fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f );
#endif
}


// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDERANSSA_Brenner2D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  // SA diffusion matrix

  Tq rho=0, u=0, v=0, t=0, nt=0;
  Tq mu, k;

  qInterpret_.eval( q, rho, u, v, t );

  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  qInterpret_.evalSA( q, nt );

  Tq chi = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);

  // Reynolds stresses diffusion matrix

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Real Cp = this->gas_.Cp();

    Tq fv1 = chi3 / (chi3 + power<3>(cv1_));
    Tq muEddy = rho*nt*fv1;

    // Augment the molecular viscosity and thermal conductivity
    mu += muEddy;
    k  += muEddy*Cp/prandtlEddy_;
  }

  Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  Tq delta = rhodiff_* mu/rho;

  // d(Fv)/d(Ux)
  kxx(iCont,0) += delta;
  kxx(ixMom,0) += delta*u;
  kxx(iyMom,0) += delta*v;
  kxx(iEngy,0) += delta*e0;
  kxx(iSA,0)   += delta*nt;

  // d(Gv)/d(Uy)
  kyy(iCont,0) += delta;
  kyy(ixMom,0) += delta*u;
  kyy(iyMom,0) += delta*v;
  kyy(iEngy,0) += delta*e0;
  kyy(iSA,0)   += delta*nt;

  // compute the diffusion matrix
  BaseType::diffusionViscous( x, y, time, q, qx, qy,
                              kxx, kxy,
                              kyx, kyy );
}


// gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tk>
inline void
PDERANSSA_Brenner2D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,  MatrixQ<Tk>& kyx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
{
  SANS_DEVELOPER_EXCEPTION("RANS BRENNER DIFFUSIONVISCOUSGRADIENT NOT IMPLEMENTED");
}


// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA_Brenner2D<PDETraitsSize, PDETraitsModel>::jacobianFluxViscous(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  SANS_DEVELOPER_EXCEPTION("RANS BRENNER JACOBIANFLUXVISCOUS NOT IMPLEMENTED");
}


// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDERANSSA_Brenner2D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{

  SANS_DEVELOPER_EXCEPTION("RANS BRENNER STRONGFLUXVISCOUS NOT IMPLEMENTED");
}



} // namespace SANS

#endif  // PDERANSSA_Brenner2D_H
