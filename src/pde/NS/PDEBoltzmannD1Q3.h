// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBOLTZMANND1Q3_H
#define PDEBOLTZMANND1Q3_H

// quasi 1-D compressible Euler

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "EulerResidCategory.h"
#include "GasModel.h"
#include "RoeBoltzmann.h"
#include "BoltzmannVariableD1Q3.h"

#include "pde/AnalyticFunction/Function1D.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr

namespace SANS
{

// Forward declare
template <class QType, template <class> class PDETraitsSize>
class QD1Q3;


//----------------------------------------------------------------------------//
// PDE class: quasi 1-D Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE eqModel-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas eqModel (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEBoltzmannD1Q3
{
public:

  enum EulerResidualInterpCategory
  {
    Euler_ResidInterp_Raw,
    Euler_ResidInterp_Momentum,
    Euler_ResidInterp_Entropy
  };

  enum BoltzmannEquationMode
  {
    AdvectionDiffusion,
    Hydrodynamics
  };

  enum CollisionOperatorCategory
  {
    BGK
  };

  typedef PhysD1 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef PDETraitsModel TraitsModel;

  typedef typename PDETraitsModel::QType QType;
  typedef QD1Q3<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                     // gas eqModel

  typedef RoeBoltzmannD1Q3<QType, PDETraitsSize> FluxType;

  typedef BoltzmannVariableTypeD1Q3Params VariableTypeD1Q3Params;

  typedef std::shared_ptr<Function1DBase<Real>> AreaFunction_ptr;

  explicit PDEBoltzmannD1Q3( const GasModel& gas0,
                       EulerResidualInterpCategory cat,
                       CollisionOperatorCategory omega = BGK,
                       const Real tau = 1.,
                       BoltzmannEquationMode eqMode = AdvectionDiffusion,
                       const Real Uadv = 0.,
                       const AreaFunction_ptr& area = nullptr,
                       const RoeEntropyFix entropyFix = none) :
    gas_(gas0),
    qInterpret_(gas0),
    cat_(cat),
    omega_(omega),
    tau_(tau),
    eqMode_(eqMode),
    Uadv_(Uadv),
    area_(area),
    flux_(gas0, entropyFix)
  {
    // Nothing
  }

  static const int if0 = 0;
  static const int if1 = 1;
  static const int if2 = 2;

  static_assert( if0 == RoeBoltzmannD1Q3<QType, PDETraitsSize>::if0, "Must match" );
  static_assert( if1 == RoeBoltzmannD1Q3<QType, PDETraitsSize>::if1, "Must match" );
  static_assert( if2 == RoeBoltzmannD1Q3<QType, PDETraitsSize>::if2, "Must match" );

  // Non-copyable
  PDEBoltzmannD1Q3( const PDEBoltzmannD1Q3& pde0 ) = delete;
  PDEBoltzmannD1Q3& operator=( const PDEBoltzmannD1Q3& pde0 ) = delete;

  ~PDEBoltzmannD1Q3() {}

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }//(area_ != nullptr); }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // unsteady temporal flux: Ft(Q)
  template<class Tp, class T, class Tf>
  void fluxAdvectiveTime(
      const Tp& param,
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(param, x, time, q, ftLocal);

    Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
    ft += A*ftLocal;
  }

  // unsteady temporal flux: Ft(Q)              //***************************************************************************
  template<class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, time, q, ftLocal);

    Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
    ft += A*ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template<class Tp, class T, class Tu>             //***************************************************************************
  void masterState(
      const Tp& param,
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
  {const std::string
    masterState(x, time, q, uCons);
  }

  // master state: U(Q)
  template<class T, class Tu>
  void masterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of master state wrt q: dU/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param,
      const Real& x, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f ) const
  {
    fluxAdvective(x, time, q, f);
  }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx,  ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
   void fluxAdvectiveUpwind(
       const Tp& param,
       const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
       const Real& nx, ArrayQ<Tf>& f ) const
   {
     fluxAdvectiveUpwind(x, time, qL, qR, nx, f);
   }

  template <class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
  {
    fluxAdvectiveUpwindSpaceTime( x, time, qL, qR, nx, nt, f);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T, class Ta>
  void jacobianFluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& q,
      MatrixQ<Ta>& ax ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt,
      MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const {}

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const {}

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const {}

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx ) const {}

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax ) const {}

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      const ArrayQ<T>& qxx,
      ArrayQ<T>& strongPDE ) const {}

  // space-time strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qtx, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& s ) const
  {
    source(x, time, q, qx, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL,
      const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const {}

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& time, ArrayQ<T>& forcing ) const {}

  // characteristic speed (needed for timestep)
  template<class T>
  void speedCharacteristic( Real, Real, Real dx, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template<class T>
  void speedCharacteristic( Real, Real, const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from primitive variable
  template<class T, class PrimitiveVarible>
  void setDOFFrom(
      ArrayQ<T>& q, const BoltzmannVariableTypeD1Q3<PrimitiveVarible>& data ) const;

  // set from primitive variable
  template<class PrimitiveVarible>
  ArrayQ<Real> setDOFFrom( const BoltzmannVariableTypeD1Q3<PrimitiveVarible>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // accessors for gas eqModel and Q interpreter
  const GasModel& gasModel() const { return gas_; }
  const QInterpret& variableInterpreter() const { return qInterpret_; }

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the interp type
  EulerResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const;

  // Exact area and area change
  void area( const Real& x, const Real& time,
             Real& A, Real& dAdx ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const GasModel gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  EulerResidualInterpCategory cat_; // Residual interp type
  CollisionOperatorCategory omega_;
  const Real tau_;
  BoltzmannEquationMode eqMode_;
  const Real Uadv_;
  const AreaFunction_ptr area_;
  const FluxType flux_;                       // Roe upwinding
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tu>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  T pdf0 = 0, pdf1 = 0, pdf2 = 0;

  qInterpret_.eval( q, pdf0, pdf1, pdf2 );

  uCons(if0) = pdf0;
  uCons(if1) = pdf1;
  uCons(if2) = pdf2;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  ArrayQ<T> pdf0_q, pdf1_q, pdf2_q;
  T pdf0 = 0, pdf1 = 0, pdf2 = 0;

  qInterpret_.eval( q, pdf0, pdf1, pdf2 );
  qInterpret_.evalJacobian( q, pdf0_q, pdf1_q, pdf2_q );

  dudq(if0,if0) = pdf0_q[0];
  dudq(if0,if1) = pdf0_q[1];
  dudq(if0,if2) = pdf0_q[2];

  dudq(if1,if0) = pdf1_q[0];
  dudq(if1,if1) = pdf1_q[1];
  dudq(if1,if2) = pdf1_q[2];

  dudq(if2,if0) = pdf2_q[0];
  dudq(if2,if1) = pdf2_q[1];
  dudq(if2,if2) = pdf2_q[2];

}


// strong form of unsteady conservative flux: dU(Q)/dt
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& utCons ) const
{

  T pdf0t, pdf1t, pdf2t = 0;

  qInterpret_.evalGradient( qt, pdf0t, pdf1t, pdf2t );

  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;

  utCons(if0) += A*pdf0t;
  utCons(if1) += A*pdf1t;
  utCons(if2) += A*pdf2t;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f ) const
{
  T pdf0, pdf1, pdf2 = 0;
  const Real e1 = -1.;
  const Real e2 =  0.;
  const Real e3 = +1.;
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;

  qInterpret_.eval( q, pdf0, pdf1, pdf2 );

  f(if0) += A *  e1*pdf0;
  f(if1) += A *  e2*pdf1;
  f(if2) += A *  e3*pdf2;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, ArrayQ<Tf>& fx ) const
{
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
  ArrayQ<Tf> f = 0;
  flux_.fluxAdvectiveUpwind(x, time, qL, qR, nx, f);

  fx += A*f;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<T> fnx = 0;
  fluxAdvectiveUpwind( x, time, qL, qR, nx, fnx );

  //Upwind temporal flux
  ArrayQ<T> ft = 0;
  if (nt >= 0)
    fluxAdvectiveTime( x, time, qL, ft );
  else
    fluxAdvectiveTime( x, time, qR, ft );

  f += ft*nt + fnx;

}

// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Ta>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& time, const ArrayQ<T>& q,
    MatrixQ<Ta>& ax ) const
{

  // d(F)/d(U): // TODO: check if this is correct ...
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;

  const Real e1 = -1.;
  const Real e2 =  0.;
  const Real e3 = +1.;

  ax = 0;

  ax(if0,if0) = e1;
  ax(if1,if1) = e2;
  ax(if2,if2) = e3;

  ax *= A;
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const
{
#if 0
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
  SANS_ASSERT( false );
#endif
#if 0
  MatrixQ<T> ax = 0, ay = 0;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  T an = nx*ax + ny*ay;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
#endif
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const
{
#if 0
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
  SANS_ASSERT( false );
#endif
#if 0
  MatrixQ<T> ax = 0, ay = 0;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  T an = nx*ax + ny*ay + nt*1;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
#endif
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    ArrayQ<T>& strongPDE ) const
{
  T pdf0, pdf1, pdf2 = 0;
  T pdf0x, pdf1x, pdf2x = 0;

  const Real e1 = -1.;
  const Real e2 =  0.;
  const Real e3 = +1.;
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;

  qInterpret_.eval( q, pdf0, pdf1, pdf2 );

  qInterpret_.evalGradient( qx, pdf0x, pdf1x, pdf2x );

  strongPDE(if0) += A*(e1*pdf0x);        // TODO: Check if this is correct.
  strongPDE(if1) += A*(e2*pdf1x);
  strongPDE(if2) += A*(e3*pdf2x);

}


template <template <class> class PDETraitsSize, class PDETraitsModel> // **************************************************************
template <class Tq, class Tg, class Ts>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::source(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{
  //
  Tq pdf0 = 0, pdf1 = 0, pdf2 = 0;
  Tq feq1 = 0, feq2 = 0, feq3 = 0;
  Tq rho = 0., U = 0.;

  const Real csq = 1./3.;
  const Real e1  = -1.;
  const Real e2  =  0.;
  const Real e3  = +1.;
  const Real w1  = 1./6.;
  const Real w2  = 4./6.;
  const Real w3  = 1./6.;

  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;

  //qInterpret_.eval( q, pdf0, pdf1, pdf2 );
  //qInterpret_.evalDensity(q, rho);
  pdf0 = q(if0);
  pdf1 = q(if1);
  pdf2 = q(if2);
  rho = pdf0 + pdf1 + pdf2;

  switch (eqMode_)
  {
    case AdvectionDiffusion:
    {
      U = Uadv_;
      break;
    }
    case Hydrodynamics:
    {
    //if ( Uadv_ != 0. )
        SANS_DEVELOPER_EXCEPTION("Uadv MUST be 0 for 'Hydrodynamics' simulation as the velocity will not be affected by Uadv");
    //  qInterpret_.evalVelocity(q, U);
      break;
    }
    default: SANS_DEVELOPER_EXCEPTION("Unknown equation mode for the Boltzmann Equation. Available modes: AdvectionDiffusion, Hydrodynamics");
  }

#define FULLEQ 1
#define REDEQ 0
#define CONST 0


#if FULLEQ
  feq1 = w1*rho*( 1 + (e1*U)/csq + (e1*U)*(e1*U)/(2*(csq*csq)) - (U*U)/(2*csq) );
  feq2 = w2*rho*( 1 + (e2*U)/csq + (e2*U)*(e2*U)/(2*(csq*csq)) - (U*U)/(2*csq) );
  feq3 = w3*rho*( 1 + (e3*U)/csq + (e3*U)*(e3*U)/(2*(csq*csq)) - (U*U)/(2*csq) );
#endif

#if FULLEQ
  switch (omega_)
  {
    case BGK:
      A = 1.;
      source(if0) -= A*((1./tau_)*(feq1 - pdf0) + w1*0.0223);
      source(if1) -= A*((1./tau_)*(feq2 - pdf1) + w2*0.0223);
      source(if2) -= A*((1./tau_)*(feq3 - pdf2) + w3*0.0223);
      break;
    default: SANS_DEVELOPER_EXCEPTION("Unknown collision operator. Available options: BGK");
  }
#elif REDEQ
  source(if0) -= (1/tau_)*(feq1_ - q(if0));
  source(if1) -= (1/tau_)*(feq2_ - q(if1));
  source(if2) -= (1/tau_)*(feq3_ - q(if2));
#elif CONST
  source(if0) -= 10.;
  source(if1) -= 10.;
  source(if2) -= 10.;
#endif

#define DEBUG_ 0
#if DEBUG_
  std::cout << "************************************************" << "X: " << x << " ";
  std::cout << "feq1: " << feq1 << " feq2: " << feq2 << " feq3: " << feq3
      << " source1: " << source(if0) << " source2: " << source(if1) << " source3: " << source(if2) << std::endl;
#endif

}


// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real dx, const ArrayQ<T>& q, T& speed ) const
{
  T c = 0, t =0 , u = 0;

  //qInterpret_.evalVelocity( q, uEvaluated );

  qInterpret_.evalTemperature(q, t);
  c = gas_.speedofSound(t);

  u = max(c, 1.);     // e1 = 1.

  speed = fabs(dx*u)/sqrt(dx*dx) + c;

}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, const ArrayQ<T>& q, T& speed ) const
{
  T c, t, u = 0;

  //qInterpret_.evalVelocity( q, u );

  qInterpret_.evalTemperature(q, t);
  c = gas_.speedofSound(t);

  u = max(c, 1.);     // e1 = 1.

  speed = fabs(u) + c;

}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::
updateFraction( const Real& x, const Real& time,
                const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  qInterpret_.updateFraction(q, dq, maxChangeFraction, updateFraction);
}


// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return qInterpret_.isValidState(q);
}


// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpret_.setFromPrimitive( q, data, name, nn );
}


// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class PrimitiveVarible>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const BoltzmannVariableTypeD1Q3<PrimitiveVarible>& data ) const
{
  qInterpret_.setFromPrimitive( q, data.cast() );
}


// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class PrimitiveVarible>
inline typename PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::setDOFFrom( const BoltzmannVariableTypeD1Q3<PrimitiveVarible>& data ) const
{
  ArrayQ<Real> q;
  qInterpret_.setFromPrimitive( q, data.cast() );
  return q;
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q;
  DictKeyPair data = d.get(BoltzmannVariableTypeD1Q3Params::params.StateVector);

  if ( data == BoltzmannVariableTypeD1Q3Params::params.StateVector.PrimitiveDistributionFunctions3 )
    qInterpret_.setFromPrimitive( q, PrimitiveDistributionFunctionsD1Q3<Real>(data) );
  else
    SANS_DEVELOPER_EXCEPTION(" Missing state vector option ");

  return q;
}


// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  int nOut = rsdPDEOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N);
    rsdPDEOut[0] = rsdPDEIn[if0];
    rsdPDEOut[1] = rsdPDEIn[if1];
    rsdPDEOut[2] = rsdPDEIn[if2];
    for (int i = 1; i < N-if2; i++)
      rsdPDEOut[2+i] = rsdPDEIn[if2+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1 + (N-3));
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 1D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    rsdPDEOut[0] = rsdPDEIn[0] + rsdPDEIn[1] + rsdPDEIn[2];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
    {
      rsdAuxOut[i] = sqrt(rsdAuxIn[i]*rsdAuxIn[i]);
    }
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N);
    rsdAuxOut[0] = rsdAuxIn[if0];
    rsdAuxOut[1] = rsdAuxIn[if1];
    rsdAuxOut[2] = rsdAuxIn[if2];
    for (int i = 1; i < N-if2; i++)
      rsdAuxOut[2+i] = rsdAuxIn[if2+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1 + (N-3));
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 1D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    rsdAuxOut[0] = rsdAuxIn[0] + rsdAuxIn[1] + rsdAuxIn[2];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N);
    rsdBCOut[0] = rsdBCIn[if0];
    rsdBCOut[1] = rsdBCIn[if1];
    rsdBCOut[2] = rsdBCIn[if2];
    for (int i = 1; i < N-if2; i++)
      rsdBCOut[2+i] = rsdBCIn[if2+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1 + (N-3));
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 1D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    rsdBCOut[0] = rsdBCIn[0] + rsdBCIn[1] + rsdBCIn[2];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}

// return the interp type
template <template <class> class PDETraitsSize, class PDETraitsModel>
typename PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::EulerResidualInterpCategory
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::category() const
{
  return cat_;
}


// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::nMonitor() const
{
  int nMon;

  if (cat_==Euler_ResidInterp_Raw) // raw residual
    nMon = N;
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
    nMon = N;
  else if (cat_ == Euler_ResidInterp_Entropy)
    nMon = 1 + (N - 3);
  else
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );

  return nMon;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  return {
          // Nothing
         };
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Real& x, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const
{
  switch (index)
  {
  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEBoltzmannD1Q3::derivedQuantity - Invalid index!");
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::area( const Real& x, const Real& time, Real& A, Real& dAdx ) const
{
  A = 1.0;
  dAdx = 0.0;

  if (area_ != nullptr)
  {
    Real dAdt;
    (*area_).gradient(x, time, A, dAdx, dAdt);
//    A = (*area_)(x, time);
//    dAdx = (*area_).gradient(x, time);
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>: qInterpret_ = "  << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEBOLTZMANND1Q3_H
