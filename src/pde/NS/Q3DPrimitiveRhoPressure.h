// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q3DPRIMITIVERHOPRESSURE_H
#define Q3DPRIMITIVERHOPRESSURE_H

// solution interpreter class
// Euler/N-S conservation-eqns: primitive variables (rho, u, v, p)

#include <vector>
#include <string>

#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "GasModel.h"
#include "NSVariable3D.h"
#include "Surreal/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 3-D Euler/N-S
// primitive variables (rho, u, v, w, p)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, w, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v, w)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (rho, u, v, w, p)
class QTypePrimitiveRhoPressure;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q3D;


template <template <class> class PDETraitsSize>
class Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>
{
public:
  typedef PhysD3 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int ir = 0;
  static const int iu = 1;
  static const int iv = 2;
  static const int iw = 3;
  static const int ip = 4;

  // The three components of the state vector that make up the velocity vector
  static const int ix = iu;
  static const int iy = iv;
  static const int iz = iw;

  static std::vector<std::string> stateNames() { return {"rho", "u", "v", "w", "p"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q3D( const GasModel& gas0 ) : gas_(gas0) {}
  Q3D( const Q3D& q0 ) : gas_(q0.gas_) {}
  ~Q3D() {}

  Q3D& operator=( const Q3D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& v, T& w, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u, T& v, T& w ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalDensityJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                                         ArrayQ<T>& u_q, ArrayQ<T>& v_q, ArrayQ<T>& w_q,
                                         ArrayQ<T>& t_q ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type& ux,
    typename promote_Surreal<Tq,Tg>::type& vx,
    typename promote_Surreal<Tq,Tg>::type& wx,
    typename promote_Surreal<Tq,Tg>::type& tx ) const;

  template<class Tq, class Tg>
  void evalPressureGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& px ) const;

  template<class Tq, class Tg, class Th>
  void evalSecondGradient(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,

      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& rhoH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& uH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& vH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& wH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& tH ) const;

  template<class Tq, class Tqp, class T>
  void conservativePerturbation(const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<T>& dU) const
  {
    Tq rho = q(ir);
    Tq u   = q(iu);
    Tq v   = q(iv);
    Tq w   = q(iw);
//    Tq p   = q(ip);

    Tqp drho = dq(ir);
    Tqp du   = dq(iu);
    Tqp dv   = dq(iv);
    Tqp dw   = dq(iw);
    Tqp dp   = dq(ip);

    dU[0] = drho;
    dU[1] = u*drho + rho*du;
    dU[2] = v*drho + rho*dv;
    dU[3] = w*drho + rho*dw;
    dU[4] = 0.5*(u*u+v*v+w*w)*drho + rho*u*du + rho*v*dv + rho*w*dw + dp/(gas_.gamma()-1.);
  }


  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure3D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature3D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative3D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& v, T& w, T& t ) const
{
  rho = q(ir);
  u   = q(iu);
  v   = q(iv);
  w   = q(iw);
  t   = q(ip)/(rho * gas_.R());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  rho = q(ir);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u, T& v, T& w ) const
{
  u = q(iu);
  v = q(iv);
  w = q(iw);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  t = q(ip)/(q(ir) * gas_.R());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalDensityJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const
{
  rho_q = 0;
  rho_q[0] = 1;  // drho/drho
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                        ArrayQ<T>& u_q, ArrayQ<T>& v_q, ArrayQ<T>& w_q,
                        ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; v_q = 0;  w_q = 0; t_q = 0;

  const T& rho = q(ir);
  const T& p   = q(ip);

  rho_q[ir] = 1;  // drho/drho

  u_q[iu]   = 1;  // du/du

  v_q[iv]   = 1;  // dv/dv

  w_q[iw]   = 1;  // dw/dw

  t_q[ir]   = -p/(rho*rho * gas_.R()); // dt/drho
  t_q[ip]   =  1/(rho * gas_.R());     // dt/dp
}


template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type&   ux,
    typename promote_Surreal<Tq,Tg>::type&   vx,
    typename promote_Surreal<Tq,Tg>::type&   wx,
    typename promote_Surreal<Tq,Tg>::type&   tx ) const
{
  const Tq& rho = q(ir);
  const Tq& p   = q(ip);
        Tq  t   = p/(rho * gas_.R());

            rhox = qx(ir);
            ux   = qx(iu);
            vx   = qx(iv);
            wx   = qx(iw);
  const Tg& px   = qx(ip);
            tx   = (t/p)*px - (t/rho)*(rhox);
}

template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalPressureGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& px ) const
{
  // no need to use gas.pressureGradient here
  px = qx(ip);
}

template <template <class> class PDETraitsSize>
template<class Tq, class Tg, class Th>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,

    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& rhoH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& uH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& vH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& wH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& tH  ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;

  Tq  rho=0,  u=0,  v=0,  w=0,  t=0;
  Tqg rhox=0, ux=0, vx=0, wx=0, tx=0;
  Tqg rhoy=0, uy=0, vy=0, wy=0, ty=0;
  Tqg rhoz=0, uz=0, vz=0, wz=0, tz=0;

  const Tq& p = q(ip);
  const Tg &px = qx(ip), &py = qy(ip), &pz = qz(ip);

  eval(q, rho, u, v, w, t);

  evalGradient(q, qx, rhox, ux, vx, wx, tx);
  evalGradient(q, qy, rhoy, uy, vy, wy, ty);
  evalGradient(q, qz, rhoz, uz, vz, wz, tz);

  rhoH(0,0) = qxx(ir);
  rhoH(1,0) = qxy(ir); rhoH(1,1) = qyy(ir);
  rhoH(2,0) = qxz(ir); rhoH(2,1) = qyz(ir); rhoH(2,2) = qzz(ir);

  uH(0,0) = qxx(iu);
  uH(1,0) = qxy(iu); uH(1,1) = qyy(iu);
  uH(2,0) = qxz(iu); uH(2,1) = qyz(iu); uH(2,2) = qzz(iu);

  vH(0,0) = qxx(iv);
  vH(1,0) = qxy(iv); vH(1,1) = qyy(iv);
  vH(2,0) = qxz(iv); vH(2,1) = qyz(iv); vH(2,2) = qzz(iv);

  wH(0,0) = qxx(iw);
  wH(1,0) = qxy(iw); wH(1,1) = qyy(iw);
  wH(2,0) = qxz(iw); wH(2,1) = qyz(iw); wH(2,2) = qzz(iw);

  const Th &pxx = qxx(ip);
  const Th &pxy = qxy(ip), &pyy = qyy(ip);
  const Th &pxz = qxz(ip), &pyz = qyz(ip), &pzz = qzz(ip);

  Tq rho2 = rho*rho;
  Tq rho3 = rho*rho*rho;
  Real Ri = 1/(gas_.R());

  // Diagonal terms
  tH(0,0) = Ri * ( (-2*px*rhox)/rho2 + (2*p*rhox*rhox)/rho3 + pxx/rho - p*rhoH(0,0)/rho2 );
  tH(1,1) = Ri * ( (-2*py*rhoy)/rho2 + (2*p*rhoy*rhoy)/rho3 + pyy/rho - p*rhoH(1,1)/rho2 );
  tH(2,2) = Ri * ( (-2*pz*rhoz)/rho2 + (2*p*rhoz*rhoz)/rho3 + pzz/rho - p*rhoH(2,2)/rho2 );

  // Lower triangular
  tH(1,0) = Ri * ( -(rhoy*px)/rho2 - (py*rhox)/rho2 + 2*p*rhox*rhoy/rho3 + pxy/rho - p*rhoH(1,0)/rho2 );
  tH(2,0) = Ri * ( -(rhoz*px)/rho2 - (pz*rhox)/rho2 + 2*p*rhox*rhoz/rho3 + pxz/rho - p*rhoH(2,0)/rho2 );
  tH(2,1) = Ri * ( -(rhoz*py)/rho2 - (pz*rhoy)/rho2 + 2*p*rhoy*rhoz/rho3 + pyz/rho - p*rhoH(2,1)/rho2 );

}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 5);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "VelocityY") &&
              (name[3] == "VelocityZ") &&
              (name[4] == "Temperature"));

  T rho, u, v, w, t;

  rho = data[0];
  u   = data[1];
  v   = data[2];
  w   = data[3];
  t   = data[4];
  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(iw) = w;
  q(ip) = gas_.pressure(rho, t);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure3D<T>& data ) const
{
  q(ir) = data.Density;
  q(iu) = data.VelocityX;
  q(iv) = data.VelocityY;
  q(iw) = data.VelocityZ;
  q(ip) = data.Pressure;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature3D<T>& data ) const
{
  q(ir) = data.Density;
  q(iu) = data.VelocityX;
  q(iv) = data.VelocityY;
  q(iw) = data.VelocityZ;
  q(ip) = gas_.pressure(data.Density, data.Temperature);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative3D<T>& data ) const
{
  q(ir) = data.Density;
  q(iu) = data.MomentumX/data.Density;
  q(iv) = data.MomentumY/data.Density;
  q(iw) = data.MomentumZ/data.Density;

  T e = data.TotalEnergy/data.Density - 0.5*(q(iu)*q(iu) + q(iv)*q(iv) + q(iw)*q(iw));
  T t = gas_.temperature(data.Density, e);

  q(ip) = gas_.pressure(data.Density, t);
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  const Real rho = q(ir);
  const Real p   = q(ip);

  const Real drho = dq(ir);
  const Real dp   = dq(ip);


  // density check
  Real wr = 1;
  Real nrho = rho - drho;

  // Compute wr such that:

  //rho - wr*drho >= (1-maxChangeFraction)*rho
  if (nrho < (1-maxChangeFraction)*rho)
    wr =  maxChangeFraction*rho/drho;

  //rho - wr*drho <= (1+maxChangeFraction)*rho
  if (nrho > (1+maxChangeFraction)*rho)
    wr = -maxChangeFraction*rho/drho;


  // pressure check
  Real wp = 1;
  Real np = p - dp;

  // Compute wp such that:

  //p - wp*dp >= (1-maxChangeFraction)*p
  if (np < (1-maxChangeFraction)*p)
    wp =  maxChangeFraction*p/dp;

  //p - wp*dp <= (1+maxChangeFraction)*p
  if (np > (1+maxChangeFraction)*p)
    wp = -maxChangeFraction*p/dp;

  updateFraction = MIN(wr, wp);


#if 0
  if (updateFraction < 1)
  {
    std::cout << "updateFraction = " << updateFraction << std::endl;
    std::cout << "nrhon, rho, -drho, newton = " << nrho << " " << rho << " " << -drho << " " << -drho/rho << std::endl;
    std::cout << "np   , p  , -dp,   newton = " << np << " " << p << " " << -dp << " " << -dp/p << std::endl;
  }
#endif
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  bool isPositive;
  Real rho, p;

  rho = q(ir);
  p   = q(ip);

  //printf( "%s: rho = %e  p = %e\n", __func__, rho, p );

  if ((rho > 0) && (p > 0))
    isPositive = true;
  else
    isPositive = false;

  return isPositive;
}


template <template <class> class PDETraitsSize>
void
Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q3D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // Q3DPRIMITIVERHOPRESSURE_H
