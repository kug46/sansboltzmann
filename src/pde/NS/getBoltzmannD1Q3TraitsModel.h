// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GETBOLTZMANND1Q3TRAITSMODEL_H
#define GETBOLTZMANND1Q3TRAITSMODEL_H

#include "TraitsBoltzmannD1Q3.h"
//#include "TraitsNavierStokes.h"
//#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"

namespace SANS
{

template<class TraitsModel> struct getBoltzmannD1Q3TraitsModel;

template<class QType_, class GasModel_>
struct getBoltzmannD1Q3TraitsModel< TraitsModelBoltzmannD1Q3<QType_,GasModel_> >
{
  typedef TraitsModelBoltzmannD1Q3<QType_,GasModel_> type;
};
/*
template<class QType_, class GasModel_, class ViscosityModel_, class ThermalConductivityModel_>
struct getBoltzmannD1Q3TraitsModel<TraitsModelBoltzmannD1Q3<QType_, GasModel_, ViscosityModel_, ThermalConductivityModel_>>
{
  typedef TraitsModelBoltzmannD1Q3<QType_, GasModel_, ViscosityModel_, ThermalConductivityModel_> type;
};


template<class PDEBase, class SensorAdvectiveFlux, class SensorViscousFlux, class SensorSource>
struct getBoltzmannD1Q3TraitsModel<TraitsModelArtificialViscosity<PDEBase, SensorAdvectiveFlux, SensorViscousFlux, SensorSource> >
{
  typedef typename getBoltzmannD1Q3TraitsModel<typename PDEBase::TraitsModel>::type type;
};
*/

}

#endif // GETEULERTRAITSMODEL_H
