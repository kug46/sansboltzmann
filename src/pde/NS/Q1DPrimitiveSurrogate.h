// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q1DPRIMITIVESURROGATE_H
#define Q1DPRIMITIVESURROGATE_H

// solution interpreter class
// Euler/N-S conservation-eqns: primitive surrogate variables

#include <vector>
#include <string>

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "pde/NS/NSVariable1D.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 1-D Euler/N-S
// primitive variables (\hat{rho}, \hat{u}, \hat{T})
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (\hat{rho}, \hat{u}, \hat{T})
class QTypePrimitiveSurrogate;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q1D;


template <template <class> class PDETraitsSize>
class Q1D<QTypePrimitiveSurrogate, PDETraitsSize>
{
public:
  typedef PhysD1 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int ir = 0;
  static const int iu = 1;
  static const int it = 2;

  // The components of the state vector that make up the vector
  static const int ix = iu;

  static std::vector<std::string> stateNames() { return {"rho", "u", "t"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q1D( const GasModel& gas0, Real rhoc = 0.01, Real Tc = 0.001 ) :
    gas_(gas0),
    rhoc_(rhoc),
    Tc_(Tc)
  {
    // Nothing
  }
  Q1D( const Q1D& q0, Real rhoc = 0.01, Real Tc = 0.001 ) :
    gas_(q0.gas_),
    rhoc_(rhoc),
    Tc_(Tc)
  {
    // Nothing
  }
  ~Q1D() {}

  Q1D& operator=( const Q1D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q, ArrayQ<T>& u_q, ArrayQ<T>& t_q ) const;

  template<class Tq, class Tg, class T>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    T& rhox, T& ux, T& tx ) const;

  template<class T>
  void evalSecondGradient(
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
      T& rhoxx, T& uxx, T& txx ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from primitive variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure1D<T>& data ) const;

  // set from primitive variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature1D<T>& data ) const;

  // set from variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative1D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  template<class T>
  void limit(const T& q0, const Real c, T& q) const;

  template<class T>
  void limitJacobian(const T& q0, const Real c, T& q) const;

  const GasModel gas_;
  const Real rhoc_;
  const Real Tc_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& t ) const
{
  limit(q(ir), rhoc_, rho);
  u = q(iu);
  limit(q(it), Tc_, t);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  limit(q(ir), rhoc_, rho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u ) const
{
  u = q(iu);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  limit(q(it), Tc_, t);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q, ArrayQ<T>& u_q, ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; t_q = 0;

  limitJacobian(q(ir), rhoc_, rho_q(ir)); // drho/drho
  u_q[1] = 1;                           // du/du
  limitJacobian(q(it), Tc_, t_q(it));     // dt/dt
//  rho_q[0] = 1;                           // drho/drho
//  u_q[1]   = 1;                           // du/du
//  t_q[2]   = 1;                           // dt/dt
}


// NOTE: I am leaving the gradient calculation in terms f surrogates for the
//       time being because the high gradient is needed
template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    T& rhox, T& ux, T& tx ) const
{
  rhox = qx(0);
  ux   = qx(1);
  tx   = qx(2);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
    T& rhoxx, T& uxx, T& txx ) const
{
  rhoxx = qxx(0);
  uxx   = qxx(1);
  txx   = qxx(2);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::limit(
    const T& q0, const Real c, T& q ) const
{
  if (q0 >= c)
    q = q0;
  else
    q = c / ( 3.0 - 3.0*(q0/c) + (q0/c)*(q0/c) );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::limitJacobian(
    const T& q0, const Real c, T& q_q0 ) const
{
  if (q0 >= c)
    q_q0 = 1;
  else
  {
    q_q0 = -c * (-3.0/c + 2*q0/(c*c)) / pow( 3.0 - 3.0*(q0/c) + (q0/c)*(q0/c), 2 );
  }
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 3);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "Temperature"));

  T rho, u, t;

  rho = data[0];
  u   = data[1];
  t   = data[2];

  // What is the problem with setting a low density of the primary unknown?
#if 0
  if (rho <= rhoc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Densities lower that critical density not implemented in surrogates" );
  }
  if (t <= Tc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Temperatures lower that critical temperature not implemented in surrogates" );
  }
#endif

  q(ir) = rho;
  q(iu) = u;
  q(it) = t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure1D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.VelocityX;
  const T& p = data.Pressure;

  T t   = p/(rho * gas_.R());

  // What is the problem with setting a low density of the primary unknown?
#if 0
  if (rho <= rhoc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Densities lower that critical density not implemented in surrogates" );
  }
  if (t <= Tc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Temperatures lower that critical temperature not implemented in surrogates" );
  }
#endif

  q(ir) = rho;
  q(iu) = u;
  q(it) = t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature1D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u   = data.VelocityX;
  const T& t   = data.Temperature;

  // What is the problem with setting a low density of the primary unknown?
#if 0
  if (rho <= rhoc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Densities lower that critical density not implemented in surrogates" );
  }
  if (t <= Tc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Temperatures lower that critical temperature not implemented in surrogates" );
  }
#endif

  q(ir) = rho;
  q(iu) = u;
  q(it) = t;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative1D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.MomentumX/rho;

  T e = data.TotalEnergy/data.Density - 0.5*(u*u);
  T t = gas_.temperature(rho, e);


  // What is the problem with setting a low density of the primary unknown?
#if 0
  if (rho <= rhoc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Densities lower that critical density not implemented in surrogates" );
  }
  if (t <= Tc_)
  {
    SANS_DEVELOPER_EXCEPTION( "Error: Temperatures lower that critical temperature not implemented in surrogates" );
  }
#endif

  q(ir) = rho;
  q(iu) = u;
  q(it) = t;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
}


// Surrogates always give physically valid primitives
template <template <class> class PDETraitsSize>
inline bool
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  bool isPositive;
  Real rho, t;

  evalDensity(q, rho);
  evalTemperature(q, t);

  if ((rho > 0) && (t > 0))
    isPositive = true;
  else
    isPositive = false;

  return isPositive;
}


template <template <class> class PDETraitsSize>
void
Q1D<QTypePrimitiveSurrogate, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q1D<QTypePrimitiveSurrogate, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q1D<QTypePrimitiveSurrogate, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

}

#endif  // Q1DPRIMITIVESURROGATE_H
