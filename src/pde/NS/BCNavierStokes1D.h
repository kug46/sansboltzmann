// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNAVIERSTOKES1D_H
#define BCNAVIERSTOKES1D_H

// 2-D compressible Navier-Stokes BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "TraitsNavierStokes.h"
#include "PDENavierStokes1D.h"
#include "BCEuler1D.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D compressible Navier-Stokes
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeWallNoSlipAdiabatic_mitState;
class BCTypeSymmetry_mitState;

template <class BCType, class PDENavierStokes1D>
class BCNavierStokes1D;

template <class BCType>
struct BCNavierStokes1DParams;


template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCNavierStokes1DVector = boost::mpl::vector7<
        BCNone<PhysD1,PDETraitsSize<PhysD1>::N>,
        BCEuler1D<BCTypeReflect                          , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
        BCEuler1D<BCTypeInflowSupersonic                 , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
        BCEuler1D<BCTypeOutflowSubsonic_Pressure         , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
        BCEuler1D<BCTypeFunction_mitState                , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
        BCEuler1D<BCTypeFullState_mitState               , PDEEuler1D<PDETraitsSize, PDETraitsModel>>,
        BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState, PDEEuler1D<PDETraitsSize, PDETraitsModel>>
                                              >;


} //namespace SANS

#endif  // BCNAVIERSTOKES1D_H
