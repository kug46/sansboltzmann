// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q1DPRIMITIVERHOPRESSURE_H
#define Q1DPRIMITIVERHOPRESSURE_H

// solution interpreter class
// Euler/N-S conservation-eqns: primitive variables (rho, u, v, p)

#include <vector>
#include <string>

#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "GasModel.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "pde/NS/NSVariable1D.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 1-D Euler/N-S
// primitive variables (rho, u, p)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (rho, u, p)
class QTypePrimitiveRhoPressure;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q1D;


template <template <class> class PDETraitsSize>
class Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>
{
public:
  typedef PhysD1 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int ir = 0;
  static const int iu = 1;
  static const int ip = 2;

  // The components of the state vector that make up the vector
  static const int ix = iu;

  static std::vector<std::string> stateNames() { return {"rho", "u", "p"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q1D( const GasModel& gas0 ) : gas_(gas0) {}
  Q1D( const Q1D& q0 ) : gas_(q0.gas_) {}
  ~Q1D() {}

  Q1D& operator=( const Q1D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q, ArrayQ<T>& u_q, ArrayQ<T>& t_q ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type& ux,
    typename promote_Surreal<Tq,Tg>::type& tx ) const;

  template<class T>
  void evalSecondGradient(
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
      T& rhoxx, T& uxx, T& txx ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from primitive variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure1D<T>& data ) const;

  // set from primitive variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature1D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative1D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& t ) const
{
  rho = q(ir);
  u   = q(iu);
  t   = q(ip)/(rho * gas_.R());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  rho = q(ir);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u ) const
{
  u = q(iu);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  const T& rho = q(ir);
  const T& p   = q(ip);

  t = p/(rho * gas_.R());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q, ArrayQ<T>& u_q, ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; t_q = 0;

  const T& rho = q(0);
  const T& p   = q(2);

  rho_q[0] = 1;  // drho/drho

  u_q[1]   = 1;  // du/du

  t_q[0]   = -p/(rho*rho * gas_.R()); // dt/drho
  t_q[2]   =  1/(rho * gas_.R());     // dt/dp
}


template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type& ux,
    typename promote_Surreal<Tq,Tg>::type& tx ) const
{
  Tq rho, t, p;
  Tg px;

  rho = q(0);
  p   = q(2);
  t   = p/(rho * gas_.R());

  rhox = qx(0);
  ux   = qx(1);
  px   = qx(2);
  tx   = (t/p)*px - (t/rho)*(rhox);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
    T& rhoxx, T& uxx, T& txx ) const
{
  T rho, rhox = 0;
  T t, tx = 0;
  T p, px, pxx = 0;

  rho = q(ir);
  p   = q(ip);
  t   = p/(rho * gas_.R());

  rhox = q(ir);

  rhoxx = qxx(ir);
  uxx   = qxx(iu);
  pxx   = qxx(ip);
  tx   = (t/p)*px - (t/rho)*(rhox);

  txx = tx( px/p - rhox/rho) + t*( pxx/p - px*px/(p*p) - rhoxx/rho + rhox*rhox/(rho*rho)  ); //NOT UNIT TESTED!
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 3);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "Temperature"));

  T rho, u, t;

  rho = data[0];
  u   = data[1];
  t   = data[2];
  q(ir) = rho;
  q(iu) = u;
  q(ip) = gas_.pressure(rho, t);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure1D<T>& data ) const
{
  q(ir) = data.Density;
  q(iu) = data.VelocityX;
  q(ip) = data.Pressure;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature1D<T>& data ) const
{
  q(ir) = data.Density;
  q(iu) = data.VelocityX;
  q(ip) = gas_.pressure(data.Density, data.Temperature);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative1D<T>& data ) const
{
  q(ir) = data.Density;
  q(iu) = data.MomentumX/data.Density;

  T e = data.TotalEnergy/data.Density - 0.5*(q(1)*q(1));
  T t = gas_.temperature(data.Density, e);

  q(ip) = gas_.pressure(data.Density, t);
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  const Real rho = q(ir);
  const Real p   = q(ip);

  const Real drho = dq(ir);
  const Real dp   = dq(ip);

  Real wr = 1, wp = 1;

  // density check
  Real nrho = rho - drho;

  // Compute wr such that:

  //rho - wr*drho >= (1-maxChangeFraction)*rho
  if (nrho < (1-maxChangeFraction)*rho)
    wr =  maxChangeFraction*rho/drho;

  //rho - wr*drho <= (1+maxChangeFraction)*rho
  if (nrho > (1+maxChangeFraction)*rho)
    wr = -maxChangeFraction*rho/drho;

  // pressure check
  Real np = p - dp;

  // Compute wp such that:

  //p - wp*dp >= (1-maxChangeFraction)*p
  if (np < (1-maxChangeFraction)*p)
    wp =  maxChangeFraction*p/dp;

  //p - wp*dp <= (1+maxChangeFraction)*p
  if (np > (1+maxChangeFraction)*p)
    wp = -maxChangeFraction*p/dp;

  updateFraction = MIN(wr, wp);
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  bool isPositive;

  const Real& rho = q(ir);
  const Real& p   = q(ip);

#if 0
  printf( "%s: rho = %e  p = %e\n", __func__, rho, p );
#endif

  if ((rho > 0) && (p > 0))
    isPositive = true;
  else
    isPositive = false;

  return isPositive;
}


template <template <class> class PDETraitsSize>
void
Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q1D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

}

#endif  // Q1DPRIMITIVERHOPRESSURE_H
