// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_EULER2D_H
#define SOLUTIONFUNCTION_EULER2D_H

// 2-D Euler PDE: exact and MMS solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // exp

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"

#include "GasModel.h"
#include "NSVariable2D.h"
#include "getEulerTraitsModel.h"
#include "pde/AnalyticFunction/Function2D.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q2D;


//----------------------------------------------------------------------------//
// solution: Linear Combination
// template for combining two solution functions to make another
// TODO: not quite sure how this works at the moment...

//template <class SOLN1, class SOLN2>
//class SolutionFunction_Euler2D_Combination
//{
//public:
//  typedef PhysD2 PhysDim;
//
//  template<class T>
//  using ArrayQ = EulerTraits<PhysD2>::ArrayQ<T>;
//
//  struct ParamsType : noncopyable
//  {
//    const ParameterNumeric<Real> a{"a", 1, NO_RANGE, "Coefficient for Function 1"};
//    const ParameterNumeric<Real> b{"b", 1, NO_RANGE, "Coefficient for Function 2"};
//    const ParameterDict SolutionArgs1{"SolutionArgs1", EMPTY_DICT, SOLN1::ParamsType::checkInputs, "Solution function arguments 1"};
//    const ParameterDict SolutionArgs2{"SolutionArgs2", EMPTY_DICT, SOLN2::ParamsType::checkInputs, "Solution function arguments 2"};
//
//    static void checkInputs(PyDict d);
//
//    static ParamsType params;
//  };
//
//  explicit SolutionFunction_Euler2D_Combination( PyDict d ) :
//      a_(d.get(ParamsType::params.a)),
//      f1_(d.get(ParamsType::params.SolutionArgs1)),
//      b_(d.get(ParamsType::params.b)),
//      f2_(d.get(ParamsType::params.SolutionArgs1)) {}
//  explicit SolutionFunction_Euler2D_Combination( const Real& a, const SOLN1& f1, const Real& b, const SOLN2& f2 ) :
//      a_(a), f1_(f1), b_(b), f2_(f2) {}
//
//  template<class T>
//  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
//  {
//    return a_*f1_(x,y,time) + b_*f2_(x,y,time);
//  }
//
//  void dump( int indentSize=0, std::ostream& out = std::cout ) const
//  {
//    std::string indent(indentSize, ' ');
//    out << indent << "SolutionFunction_Euler2D_Combination:  a_, b_ = " << a_ << ", " << b_ << std::endl;
//  }
//
//private:
//  Real a_;
//  SOLN1 f1_;
//  Real b_;
//  SOLN2 f2_;
//};


//----------------------------------------------------------------------------//
// solution: const

struct SolutionFunction_Euler2D_Const_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, NO_RANGE, "Density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "X-velocity"};
  const ParameterNumeric<Real> v{"v", NO_DEFAULT, NO_RANGE, "Y-velocity"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, NO_RANGE, "Pressure"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler2D_Const_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler2D_Const :
    public Function2DVirtualInterface<SolutionFunction_Euler2D_Const<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_Euler2D_Const_Params ParamsType;

  explicit SolutionFunction_Euler2D_Const( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )), rho_(d.get(ParamsType::params.rho)),
    u_(d.get(ParamsType::params.u)), v_(d.get(ParamsType::params.v)), p_(d.get(ParamsType::params.p))  {}
  explicit SolutionFunction_Euler2D_Const( const GasModel& gas, const Real& rho, const Real& u, const Real& v, const Real& p  ) :
      qinterp_(gas), rho_(rho), u_(u), v_(v), p_(p) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> q = 0;
    DensityVelocityPressure2D<T> dvp( rho_, u_, v_, p_ );
    qinterp_.setFromPrimitive(q, dvp);
    return q;
  }

private:
  const QInterpret qinterp_;
  const Real rho_;
  const Real u_;
  const Real v_;
  const Real p_;
};

//template <class QType>
//typename SolutionFunction_Euler2D_Const<QType>::ParamsType SolutionFunction_Euler2D_Const<QType>::ParamsType::params;


struct SolutionFunction_Euler2D_Wake_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> wakedepth{"Wakedepth", NO_DEFAULT, NO_RANGE, "Constant wake depth"};
  const ParameterNumeric<Real> Pt{"rho", NO_DEFAULT, NO_RANGE, "Constant of the Stagnation Pressure"};
  const ParameterNumeric<Real> Tt{"u", NO_DEFAULT, NO_RANGE, "Constant of the Stagnation Temperature"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, NO_RANGE, "Approximate inlet pressure"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler2D_Wake_Params params;
};


//----------------------------------------------------------------------------//
// solution: gaussian velocity wake centered at y = 0.5
template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler2D_Wake :
    public Function2DVirtualInterface<SolutionFunction_Euler2D_Wake<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_Euler2D_Wake_Params ParamsType;

  explicit SolutionFunction_Euler2D_Wake( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )), gas_( d.get(ParamsType::params.gasModel) ),
      wakeDepth_(d.get(ParamsType::params.wakedepth)),Pt_(d.get(ParamsType::params.Pt)),
      Tt_(d.get(ParamsType::params.Tt)), p_(d.get(ParamsType::params.p))  {}
  explicit SolutionFunction_Euler2D_Wake(  const GasModel& gas, const Real wakeDepth, const Real Pt, const Real Tt, const Real p ) :
      qinterp_(gas), gas_(gas), wakeDepth_(wakeDepth), Pt_(Pt), Tt_(Tt), p_(p) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    Real gamma = gas_.gamma();
    Real R = gas_.R();
//    Real cp = gas_.Cp();

    T p = p_;

    T Mref = sqrt(2./(gamma-1)*(pow(Pt_/p_, (gamma-1.)/gamma)-1));
    T M = Mref*( 1.0 - exp( -y*y*10000.0 )*wakeDepth_ );

    T t = Tt_ / (1 + (gamma-1.)/2*M*M);
    T c = sqrt(gamma*R*t);
    T u = M*c;
    T v = 0.0;
    T rho = p/R/t;

    ArrayQ<T> q = 0;
    DensityVelocityPressure2D<T> dvp( rho, u, v, p );
    qinterp_.setFromPrimitive(q, dvp);

//    std::cout << "x: " << x << " y: " << y << " rho: "<< rho << " u: " << u  << " v: " << v << " p: " << p <<  "\n" ;

    return q;
  }

private:
  const QInterpret qinterp_;
  const GasModel gas_;
  const Real wakeDepth_;
  const Real Pt_;
  const Real Tt_;
  const Real p_;
};


//template <class QType>
//typename SolutionFunction_Euler2D_Wake<QType>::ParamsType SolutionFunction_Euler2D_Wake<QType>::ParamsType::params;


//----------------------------------------------------------------------------//
// solution: Riemann
// NOTE: This is really only to create 2D Riemann problem initial conditions

struct SolutionFunction_Euler2D_Riemann_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> interface{"interface", NO_DEFAULT, NO_RANGE, "Location of the discontinuity interface"};

  const ParameterNumeric<Real> rhoL{"rhoL", NO_DEFAULT, NO_RANGE, "Left side density"};
  const ParameterNumeric<Real> uL{"uL", NO_DEFAULT, NO_RANGE, "Left side x-velocity"};
  const ParameterNumeric<Real> vL{"vL", NO_DEFAULT, NO_RANGE, "Left side y-velocity"};
  const ParameterNumeric<Real> pL{"pL", NO_DEFAULT, NO_RANGE, "Left side pressure"};

  const ParameterNumeric<Real> rhoR{"rhoR", NO_DEFAULT, NO_RANGE, "Right side density"};
  const ParameterNumeric<Real> uR{"uR", NO_DEFAULT, NO_RANGE, "Right side x-velocity"};
  const ParameterNumeric<Real> vR{"vR", NO_DEFAULT, NO_RANGE, "Right side y-velocity"};
  const ParameterNumeric<Real> pR{"pR", NO_DEFAULT, NO_RANGE, "Right side pressure"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler2D_Riemann_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler2D_Riemann :
    public Function2DVirtualInterface<SolutionFunction_Euler2D_Riemann<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_Euler2D_Riemann_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler2D_Riemann( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
    x_(d.get(ParamsType::params.interface))
  {
    qinterp_.setFromPrimitive(qL_, DensityVelocityPressure2D<Real>( d.get(ParamsType::params.rhoL),
                                                                    d.get(ParamsType::params.uL),
                                                                    d.get(ParamsType::params.vL),
                                                                    d.get(ParamsType::params.pL) ));
    qinterp_.setFromPrimitive(qR_, DensityVelocityPressure2D<Real>( d.get(ParamsType::params.rhoR),
                                                                    d.get(ParamsType::params.uR),
                                                                    d.get(ParamsType::params.vR),
                                                                    d.get(ParamsType::params.pR) ));
  }

  explicit SolutionFunction_Euler2D_Riemann( const GasModel& gas, const Real& interface,
                                            const Real& rhoL, const Real& uL, const Real& vL, const Real& pL,
                                            const Real& rhoR, const Real& uR, const Real& vR, const Real& pR  ) :
    qinterp_(gas),
    x_(interface)
  {
    qinterp_.setFromPrimitive(qL_, DensityVelocityPressure2D<Real>( rhoL, uL, vL, pL ));
    qinterp_.setFromPrimitive(qR_, DensityVelocityPressure2D<Real>( rhoR, uR, vR, pR ));
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if (x < x_)
    {
      return qL_;
    }
    else
    {
      return qR_;
    }
    // We should not get here
  }

private:
  const QInterpret qinterp_;
  const Real x_;
  ArrayQ<Real> qL_;
  ArrayQ<Real> qR_;
};


//----------------------------------------------------------------------------//
// solution: Riemann From Dumbser's Paper
// NOTE: This is really only to create 2D Riemann_Dumbser problem initial conditions

struct SolutionFunction_Euler2D_Riemann_Dumbser_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> interfaceX{"interfaceX", NO_DEFAULT, NO_RANGE, " X Location of the discontinuity interface"};
  const ParameterNumeric<Real> interfaceY{"interfaceY", NO_DEFAULT, NO_RANGE, " Y Location of the discontinuity interface"};


  const ParameterNumeric<Real> rhoTL{"rhoTL", NO_DEFAULT, NO_RANGE, "Top Left side density"};
  const ParameterNumeric<Real> uTL{"uTL", NO_DEFAULT, NO_RANGE, "Top Left side x-velocity"};
  const ParameterNumeric<Real> vTL{"vTL", NO_DEFAULT, NO_RANGE, "Top Left side y-velocity"};
  const ParameterNumeric<Real> pTL{"pTL", NO_DEFAULT, NO_RANGE, "Top Left side pressure"};

  const ParameterNumeric<Real> rhoTR{"rhoTR", NO_DEFAULT, NO_RANGE, "Top Right side density"};
  const ParameterNumeric<Real> uTR{"uTR", NO_DEFAULT, NO_RANGE, "Top Right side x-velocity"};
  const ParameterNumeric<Real> vTR{"vTR", NO_DEFAULT, NO_RANGE, "Top Right side y-velocity"};
  const ParameterNumeric<Real> pTR{"pTR", NO_DEFAULT, NO_RANGE, "Top Right side pressure"};

  const ParameterNumeric<Real> rhoBL{"rhoBL", NO_DEFAULT, NO_RANGE, "Bottom Left side density"};
  const ParameterNumeric<Real> uBL{"uBL", NO_DEFAULT, NO_RANGE, "Bottom Left side x-velocity"};
  const ParameterNumeric<Real> vBL{"vBL", NO_DEFAULT, NO_RANGE, "Bottom Left side y-velocity"};
  const ParameterNumeric<Real> pBL{"pBL", NO_DEFAULT, NO_RANGE, "Bottom Left side pressure"};

  const ParameterNumeric<Real> rhoBR{"rhoBR", NO_DEFAULT, NO_RANGE, "Bottom Right side density"};
  const ParameterNumeric<Real> uBR{"uBR", NO_DEFAULT, NO_RANGE, "Bottom Right side x-velocity"};
  const ParameterNumeric<Real> vBR{"vBR", NO_DEFAULT, NO_RANGE, "Bottom Right side y-velocity"};
  const ParameterNumeric<Real> pBR{"pBR", NO_DEFAULT, NO_RANGE, "Bottom Right side pressure"};


  static void checkInputs(PyDict d);

  static SolutionFunction_Euler2D_Riemann_Dumbser_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler2D_Riemann_Dumbser :
    public Function2DVirtualInterface<SolutionFunction_Euler2D_Riemann_Dumbser<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_Euler2D_Riemann_Dumbser_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler2D_Riemann_Dumbser( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
    x_(d.get(ParamsType::params.interfaceX)),
    y_(d.get(ParamsType::params.interfaceY))
  {
    qinterp_.setFromPrimitive(qTL_, DensityVelocityPressure2D<Real>( d.get(ParamsType::params.rhoTL),
                                                                    d.get(ParamsType::params.uTL),
                                                                    d.get(ParamsType::params.vTL),
                                                                    d.get(ParamsType::params.pTL) ));
    qinterp_.setFromPrimitive(qTR_, DensityVelocityPressure2D<Real>( d.get(ParamsType::params.rhoTR),
                                                                    d.get(ParamsType::params.uTR),
                                                                    d.get(ParamsType::params.vTR),
                                                                    d.get(ParamsType::params.pTR) ));
    qinterp_.setFromPrimitive(qBL_, DensityVelocityPressure2D<Real>( d.get(ParamsType::params.rhoBL),
                                                                    d.get(ParamsType::params.uBL),
                                                                    d.get(ParamsType::params.vBL),
                                                                    d.get(ParamsType::params.pBL) ));
    qinterp_.setFromPrimitive(qBR_, DensityVelocityPressure2D<Real>( d.get(ParamsType::params.rhoBR),
                                                                    d.get(ParamsType::params.uBR),
                                                                    d.get(ParamsType::params.vBR),
                                                                    d.get(ParamsType::params.pBR) ));
  }

  explicit SolutionFunction_Euler2D_Riemann_Dumbser( const GasModel& gas, const Real& interfaceX, const Real& interfaceY,
                                            const Real& rhoTL, const Real& uTL, const Real& vTL, const Real& pTL,
                                            const Real& rhoTR, const Real& uTR, const Real& vTR, const Real& pTR,
                                            const Real& rhoBL, const Real& uBL, const Real& vBL, const Real& pBL,
                                            const Real& rhoBR, const Real& uBR, const Real& vBR, const Real& pBR ) :
    qinterp_(gas),
    x_(interfaceX),
    y_(interfaceY)
  {
    qinterp_.setFromPrimitive(qTL_, DensityVelocityPressure2D<Real>( rhoTL, uTL, vTL, pTL ));
    qinterp_.setFromPrimitive(qTR_, DensityVelocityPressure2D<Real>( rhoTR, uTR, vTR, pTR ));
    qinterp_.setFromPrimitive(qBL_, DensityVelocityPressure2D<Real>( rhoBL, uBL, vBL, pBL ));
    qinterp_.setFromPrimitive(qBR_, DensityVelocityPressure2D<Real>( rhoBR, uBR, vBR, pBR ));
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    if ((x < x_) && ( y > y_) )
    {
      return qTL_;
    }
    if ( (x > x_) && ( y > y_) )
    {
      return qTR_;
    }
    else if ( (x < x_) && ( y < y_) )
    {
      return qBL_;
    }
    else
    {
      return qBR_;
    }

    // We should not get here
  }

private:
  const QInterpret qinterp_;
  const Real x_;
  const Real y_;
  ArrayQ<Real> qTL_;
  ArrayQ<Real> qTR_;
  ArrayQ<Real> qBL_;
  ArrayQ<Real> qBR_;
};

//----------------------------------------------------------------------------//
// solution: vortex centered at x0, y0


struct SolutionFunction_Euler2D_Vortex_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};
  const ParameterNumeric<Real> xzero{"xzero", NO_DEFAULT, NO_RANGE, "x-coordinate of vortex center"};
  const ParameterNumeric<Real> yzero{"yzero", NO_DEFAULT, NO_RANGE, "y-coordinate of vortex center"};
  const ParameterNumeric<Real> beta{"beta", NO_DEFAULT, NO_RANGE, "measure of vortex strength"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler2D_Vortex_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler2D_Vortex :
    public Function2DVirtualInterface<SolutionFunction_Euler2D_Vortex<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_Euler2D_Vortex_Params ParamsType;

  explicit SolutionFunction_Euler2D_Vortex( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )), xzero_(d.get(ParamsType::params.xzero)),
      yzero_(d.get(ParamsType::params.yzero)), beta_(d.get(ParamsType::params.beta)) {}
  explicit SolutionFunction_Euler2D_Vortex( const GasModel& gas, const Real xzero, const Real yzero, const Real beta ) :
      qinterp_(gas), xzero_(xzero), yzero_(yzero), beta_(beta) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T gam      = 1.4; //HACK (get this from qinterp_)
    T pi_const = 3.141592653589793;

    T rad = sqrt(pow((x - time - xzero_), 2.) + pow((y - yzero_), 2.));
    T u   = 1. - beta_*exp(1.-rad*rad)*(y - yzero_)*(1./(2.*pi_const));
    T v   = beta_*exp(1.-rad*rad)*(x - xzero_)*(1./(2.*pi_const));
    T rho = pow((1.-((gam-1)/(16.*gam*pi_const*pi_const))*beta_*beta_*exp(2.*(1.-rad*rad))), (1./(gam-1.)));
    T p   = pow(rho, gam);

    ArrayQ<T> q = 0;
    DensityVelocityPressure2D<T> dvp( rho, u, v, p );
    qinterp_.setFromPrimitive(q, dvp);

    return q;
  }

private:
  const QInterpret qinterp_;
  const Real xzero_;
  const Real yzero_;
  const Real beta_;
};


struct SolutionFunction_Euler2D_Temperature_Spot_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};
  const ParameterNumeric<Real> xzero{"xzero", NO_DEFAULT, NO_RANGE, "x-coordinate of spot center"};
  const ParameterNumeric<Real> yzero{"yzero", NO_DEFAULT, NO_RANGE, "y-coordinate of spot center"};
  const ParameterNumeric<Real> vzero{"vzero", NO_DEFAULT, NO_RANGE, "speed of spot propagation"};
  const ParameterNumeric<Real> theta{"theta", NO_DEFAULT, NO_RANGE, "angle of spot propagation"};
  const ParameterNumeric<Real> beta{"beta", NO_DEFAULT, NO_RANGE, "measure of spot strength"};
  const ParameterNumeric<Real> xi{"xi", NO_DEFAULT, NO_RANGE, "measure of spot width"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler2D_Temperature_Spot_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler2D_Temperature_Spot :
    public Function2DVirtualInterface<SolutionFunction_Euler2D_Temperature_Spot<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_Euler2D_Temperature_Spot_Params ParamsType;

  explicit SolutionFunction_Euler2D_Temperature_Spot( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )), xzero_(d.get(ParamsType::params.xzero)),
      yzero_(d.get(ParamsType::params.yzero)), vzero_(d.get(ParamsType::params.vzero)), theta_(d.get(ParamsType::params.theta)),
      beta_(d.get(ParamsType::params.beta)), xi_(d.get(ParamsType::params.xi)) {}
  explicit SolutionFunction_Euler2D_Temperature_Spot( const GasModel& gas, const Real xzero, const Real yzero, const Real vzero, const Real theta,
                                            const Real beta, const Real xi) :
      qinterp_(gas), xzero_(xzero), yzero_(yzero), vzero_(vzero), theta_(theta), beta_(beta), xi_(xi) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    //Real R = qinterp_.R();
    T R   = 0.4; //HACK (get this from qinterp_)

    T u   = vzero_*cos(theta_);
    T v   = vzero_*sin(theta_);
    T p   = 1.0;

    T rad  = sqrt(pow((x - u*time - xzero_), 2.) + pow((y - v*time - yzero_), 2.));
    T temp = 0.5*((1+beta_) + (1-beta_)*tanh(rad-xi_));

    T rho = p/(temp*R);

    ArrayQ<T> q = 0;
    DensityVelocityPressure2D<T> dvp( rho, u, v, p );
    qinterp_.setFromPrimitive(q, dvp);

    return q;
  }

private:
  const QInterpret qinterp_;
  const Real xzero_;
  const Real yzero_;
  const Real vzero_;
  const Real theta_;
  const Real beta_;
  const Real xi_;
};



//----------------------------------------------------------------------------//
// solution: Taylor Green :: vortex centered at x0, y0

struct SolutionFunction_Euler2D_Taylor_Green_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};
  const ParameterNumeric<Real> rhozero{"rhozero", NO_DEFAULT, NO_RANGE, "reference density"};
  const ParameterNumeric<Real> pzero{"pzero", NO_DEFAULT, NO_RANGE, "reference pressure"};
  const ParameterNumeric<Real> knum{"knum", NO_DEFAULT, NO_RANGE, "wave number of vortices"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler2D_Taylor_Green_Params params;
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler2D_Taylor_Green :
    public Function2DVirtualInterface<SolutionFunction_Euler2D_Taylor_Green<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_Euler2D_Taylor_Green_Params ParamsType;

  explicit SolutionFunction_Euler2D_Taylor_Green( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )), rhozero_(d.get(ParamsType::params.rhozero)),
      pzero_(d.get(ParamsType::params.pzero)), knum_(d.get(ParamsType::params.knum)) {}
  explicit SolutionFunction_Euler2D_Taylor_Green( const GasModel& gas, const Real rhozero, const Real pzero, const Real knum ) :
      qinterp_(gas), rhozero_(rhozero), pzero_(pzero), knum_(knum) {}

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T u   = sin(knum_*x)*cos(knum_*y);
    T v   = -cos(knum_*x)*sin(knum_*y);
    T rho = rhozero_;
    T p   = pzero_ + (rhozero_/2.)*(pow(cos(knum_*x), 2.) + pow(cos(knum_*y), 2.));

    ArrayQ<T> q = 0;
    DensityVelocityPressure2D<T> dvp( rho, u, v, p );
    qinterp_.setFromPrimitive(q, dvp);

    return q;
  }

private:
  const QInterpret qinterp_;
  const Real rhozero_;
  const Real pzero_;
  const Real knum_;
};


//----------------------------------------------------------------------------//
// solution: - Trig function MMS Solution Function
struct SolutionFunction_Euler2D_TrigMMS_Params : noncopyable
{
  const ParameterDict gasModel{"gasModel", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> L{"L", NO_DEFAULT, NO_RANGE, "Reference lenght"};

  const ParameterNumeric<Real>   r0{  "r0", NO_DEFAULT, NO_RANGE, "Density shift"};
  const ParameterNumeric<Real>   rx{  "rx", NO_DEFAULT, NO_RANGE, "Density x-amplitude"};
  const ParameterNumeric<Real>   ry{  "ry", NO_DEFAULT, NO_RANGE, "Density y-amplitude"};
  const ParameterNumeric<Real>  rxy{ "rxy", NO_DEFAULT, NO_RANGE, "Density xy-amplitude"};
  const ParameterNumeric<Real>  arx{ "arx", NO_DEFAULT, NO_RANGE, "Density x-frequency"};
  const ParameterNumeric<Real>  ary{ "ary", NO_DEFAULT, NO_RANGE, "Density y-frequency"};
  const ParameterNumeric<Real> arxy{"arxy", NO_DEFAULT, NO_RANGE, "Density xy-frequency"};

  const ParameterNumeric<Real>   u0{  "u0", NO_DEFAULT, NO_RANGE, "u-Velocity shift"};
  const ParameterNumeric<Real>   ux{  "ux", NO_DEFAULT, NO_RANGE, "u-Velocity x-amplitude"};
  const ParameterNumeric<Real>   uy{  "uy", NO_DEFAULT, NO_RANGE, "u-Velocity y-amplitude"};
  const ParameterNumeric<Real>  uxy{ "uxy", NO_DEFAULT, NO_RANGE, "u-Velocity xy-amplitude"};
  const ParameterNumeric<Real>  aux{ "aux", NO_DEFAULT, NO_RANGE, "u-Velocity x-frequency"};
  const ParameterNumeric<Real>  auy{ "auy", NO_DEFAULT, NO_RANGE, "u-Velocity y-frequency"};
  const ParameterNumeric<Real> auxy{"auxy", NO_DEFAULT, NO_RANGE, "u-Velocity xy-frequency"};

  const ParameterNumeric<Real>   v0{  "v0", NO_DEFAULT, NO_RANGE, "v-Velocity shift"};
  const ParameterNumeric<Real>   vx{  "vx", NO_DEFAULT, NO_RANGE, "v-Velocity x-amplitude"};
  const ParameterNumeric<Real>   vy{  "vy", NO_DEFAULT, NO_RANGE, "v-Velocity y-amplitude"};
  const ParameterNumeric<Real>  vxy{ "vxy", NO_DEFAULT, NO_RANGE, "v-Velocity xy-amplitude"};
  const ParameterNumeric<Real>  avx{ "avx", NO_DEFAULT, NO_RANGE, "v-Velocity x-frequency"};
  const ParameterNumeric<Real>  avy{ "avy", NO_DEFAULT, NO_RANGE, "v-Velocity y-frequency"};
  const ParameterNumeric<Real> avxy{"avxy", NO_DEFAULT, NO_RANGE, "v-Velocity xy-frequency"};

  const ParameterNumeric<Real>   p0{  "p0", NO_DEFAULT, NO_RANGE, "p-Velocity shift"};
  const ParameterNumeric<Real>   px{  "px", NO_DEFAULT, NO_RANGE, "p-Velocity x-amplitude"};
  const ParameterNumeric<Real>   py{  "py", NO_DEFAULT, NO_RANGE, "p-Velocity y-amplitude"};
  const ParameterNumeric<Real>  pxy{ "pxy", NO_DEFAULT, NO_RANGE, "p-Velocity xy-amplitude"};
  const ParameterNumeric<Real>  apx{ "apx", NO_DEFAULT, NO_RANGE, "p-Velocity x-frequency"};
  const ParameterNumeric<Real>  apy{ "apy", NO_DEFAULT, NO_RANGE, "p-Velocity y-frequency"};
  const ParameterNumeric<Real> apxy{"apxy", NO_DEFAULT, NO_RANGE, "p-Velocity xy-frequency"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler2D_TrigMMS_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler2D_TrigMMS :
    public Function2DVirtualInterface< SolutionFunction_Euler2D_TrigMMS<PDETraitsSize, PDETraitsModel>,
                                       PDETraitsSize<PhysD2> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_Euler2D_TrigMMS_Params ParamsType;

#define PRM_r0   d.get(ParamsType::params.r0  )
#define PRM_rx   d.get(ParamsType::params.rx  )
#define PRM_ry   d.get(ParamsType::params.ry  )
#define PRM_rxy  d.get(ParamsType::params.rxy )
#define PRM_arx  d.get(ParamsType::params.arx )
#define PRM_ary  d.get(ParamsType::params.ary )
#define PRM_arxy d.get(ParamsType::params.arxy)

#define PRM_u0   d.get(ParamsType::params.u0  )
#define PRM_ux   d.get(ParamsType::params.ux  )
#define PRM_uy   d.get(ParamsType::params.uy  )
#define PRM_uxy  d.get(ParamsType::params.uxy )
#define PRM_aux  d.get(ParamsType::params.aux )
#define PRM_auy  d.get(ParamsType::params.auy )
#define PRM_auxy d.get(ParamsType::params.auxy)

#define PRM_v0   d.get(ParamsType::params.v0  )
#define PRM_vx   d.get(ParamsType::params.vx  )
#define PRM_vy   d.get(ParamsType::params.vy  )
#define PRM_vxy  d.get(ParamsType::params.vxy )
#define PRM_avx  d.get(ParamsType::params.avx )
#define PRM_avy  d.get(ParamsType::params.avy )
#define PRM_avxy d.get(ParamsType::params.avxy)

#define PRM_p0   d.get(ParamsType::params.p0  )
#define PRM_px   d.get(ParamsType::params.px  )
#define PRM_py   d.get(ParamsType::params.py  )
#define PRM_pxy  d.get(ParamsType::params.pxy )
#define PRM_apx  d.get(ParamsType::params.apx )
#define PRM_apy  d.get(ParamsType::params.apy )
#define PRM_apxy d.get(ParamsType::params.apxy)

  explicit SolutionFunction_Euler2D_TrigMMS( const PyDict& d ) :
      qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
      L_(d.get(ParamsType::params.L)),
      r0_( PRM_r0),  rx_( PRM_rx),  ry_( PRM_ry),  rxy_( PRM_rxy),  arx_( PRM_arx),  ary_( PRM_ary),  arxy_( PRM_arxy),
      u0_( PRM_u0),  ux_( PRM_ux),  uy_( PRM_uy),  uxy_( PRM_uxy),  aux_( PRM_aux),  auy_( PRM_auy),  auxy_( PRM_auxy),
      v0_( PRM_v0),  vx_( PRM_vx),  vy_( PRM_vy),  vxy_( PRM_vxy),  avx_( PRM_avx),  avy_( PRM_avy),  avxy_( PRM_avxy),
      p0_( PRM_p0),  px_( PRM_px),  py_( PRM_py),  pxy_( PRM_pxy),  apx_( PRM_apx),  apy_( PRM_apy),  apxy_( PRM_apxy)
  {}

#undef PRM_r0
#undef PRM_rx
#undef PRM_ry
#undef PRM_rxy
#undef PRM_arx
#undef PRM_ary
#undef PRM_arxy

#undef PRM_u0
#undef PRM_ux
#undef PRM_uy
#undef PRM_uxy
#undef PRM_aux
#undef PRM_auy
#undef PRM_auxy

#undef PRM_v0
#undef PRM_vx
#undef PRM_vy
#undef PRM_vxy
#undef PRM_avx
#undef PRM_avy
#undef PRM_avxy

#undef PRM_p0
#undef PRM_px
#undef PRM_py
#undef PRM_pxy
#undef PRM_apx
#undef PRM_apy
#undef PRM_apxy

  explicit SolutionFunction_Euler2D_TrigMMS(
      const GasModel& gas, const Real L,
      const Real  r0, const Real  rx, const Real  ry, const Real  rxy, const Real  arx, const Real  ary, const Real  arxy,
      const Real  u0, const Real  ux, const Real  uy, const Real  uxy, const Real  aux, const Real  auy, const Real  auxy,
      const Real  v0, const Real  vx, const Real  vy, const Real  vxy, const Real  avx, const Real  avy, const Real  avxy,
      const Real  p0, const Real  px, const Real  py, const Real  pxy, const Real  apx, const Real  apy, const Real  apxy
    ) : qinterp_(gas), L_(L),
         r0_( r0),  rx_( rx),  ry_( ry),  rxy_( rxy),  arx_( arx),  ary_( ary),  arxy_( arxy),
         u0_( u0),  ux_( ux),  uy_( uy),  uxy_( uxy),  aux_( aux),  auy_( auy),  auxy_( auxy),
         v0_( v0),  vx_( vx),  vy_( vy),  vxy_( vxy),  avx_( avx),  avy_( avy),  avxy_( avxy),
         p0_( p0),  px_( px),  py_( py),  pxy_( pxy),  apx_( apx),  apy_( apy),  apxy_( apxy)
    {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T rho, u, v, p;

    rho =  r0_ +  rx_*sin( arx_*PI*x/L_) +  ry_*cos( ary_*PI*y/L_) +  rxy_*cos( arxy_*PI*x/L_)*cos( arxy_*PI*y/L_);
    u   =  u0_ +  ux_*sin( aux_*PI*x/L_) +  uy_*cos( auy_*PI*y/L_) +  uxy_*cos( auxy_*PI*x/L_)*cos( auxy_*PI*y/L_);
    v   =  v0_ +  vx_*cos( avx_*PI*x/L_) +  vy_*sin( avy_*PI*y/L_) +  vxy_*cos( avxy_*PI*x/L_)*cos( avxy_*PI*y/L_);
    p   =  p0_ +  px_*cos( apx_*PI*x/L_) +  py_*sin( apy_*PI*y/L_) +  pxy_*cos( apxy_*PI*x/L_)*cos( apxy_*PI*y/L_);

    ArrayQ<T> q = 0;
    qinterp_.setFromPrimitive(q, DensityVelocityPressure2D<T>( {rho, u, v, p} ) );

    return q;
  }

  ArrayQ<Real> q0() const
  {
    ArrayQ<Real> q = 0;
    qinterp_.setFromPrimitive(q, DensityVelocityPressure2D<Real>( {r0_, u0_, v0_, p0_} ) );
    return q;
  }

private:
  const QInterpret qinterp_;
  const Real L_;

  const Real  r0_,  rx_,  ry_,  rxy_,  arx_,  ary_,  arxy_;
  const Real  u0_,  ux_,  uy_,  uxy_,  aux_,  auy_,  auxy_;
  const Real  v0_,  vx_,  vy_,  vxy_,  avx_,  avy_,  avxy_;
  const Real  p0_,  px_,  py_,  pxy_,  apx_,  apy_,  apxy_;
};

//----------------------------------------------------------------------------//
// solution: radial shock

struct SolutionFunction_Euler2D_RadialShock_Params: noncopyable
{
  const ParameterDict gasModel { "Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> rho{ "rho", NO_DEFAULT, NO_RANGE, "Pre-shock stagnation density"};
  const ParameterNumeric<Real> M{ "M", NO_DEFAULT, NO_RANGE, "Shock Mach number"};
  const ParameterNumeric<Real> T{ "T", NO_DEFAULT, NO_RANGE, "Pre-shock stagnation temperature"};
  const ParameterNumeric<Real> r{ "r", NO_DEFAULT, NO_RANGE, "Shock location"};

static void checkInputs(PyDict d);

static SolutionFunction_Euler2D_RadialShock_Params params;
};


template<template<class > class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler2D_RadialShock
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_Euler2D_RadialShock_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter


  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler2D_RadialShock(const GasModel& gas,
                                                const Real& rhoc,
                                                const Real& Mstar,
                                                const Real& Tc,
                                                const Real& rs) :
      qinterp_( gas ),
      gas_(gas),
      gamma_( gas.gamma() ),
      R_( gas.R() ),
      rhoc_( rhoc ),
      Mc_(Mstar),
      Tc_( Tc ),
      rs_(rs)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& time) const
  {
    Real r = sqrt(x*x + y*y);

    Real Astar = rs_ / (pow((gamma_+1)/2,(-(gamma_+1)/(2*(gamma_-1))))
                            * pow(1+(gamma_-1)/2*Mc_*Mc_,((gamma_+1)/(2*(gamma_-1)))) / Mc_);

    if (r <= rs_)
    {
      Real areaRatio = r / Astar;

      SurrealS<1> M = 3;


      M.deriv() = 1;

      SurrealS<1> rsd = 1;
      Real gm1 = gamma_ - 1;
      Real gp1 = gamma_ + 1;
      do
      {
        rsd = pow( ((gp1) / 2.), -(gp1) / (2. * (gm1)) ) * pow( (1. + (gm1) / 2. * M * M), (gp1) / (2. * (gm1)) ) / M
                    - areaRatio;
        Real dM = -rsd.value()/rsd.deriv();
        M += dM;
      } while (fabs(rsd.value()) > 1.e-11);

      Real Mach_IC = M.value();

      Real rho = rhoc_ * pow( 1. + (gm1) / 2. * Mach_IC * Mach_IC, -1 / (gm1) );
      Real T_IC = Tc_ / (1. + (gm1) / 2. * Mach_IC * Mach_IC);
      Real q3 = Mach_IC * sqrt( gamma_ * R_ * T_IC );
      Real u3 = q3 * x /r;
      Real v3 = q3 * y /r;

      ArrayQ<T> q = 0;
      DensityVelocityTemperature2D<T> dvt( rho, u3, v3, T_IC );
      qinterp_.setFromPrimitive( q, dvt );

      return q;
    }
    else
    {
      Real areaRatio = rs_ / Astar;

      SurrealS<1> M = 3;


      M.deriv() = 1;

      SurrealS<1> rsd = 1;
      Real gm1 = gamma_ - 1;
      Real gp1 = gamma_ + 1;
      do
      {
        rsd = pow( ((gp1) / 2.), -(gp1) / (2. * (gm1)) ) * pow( (1. + (gm1) / 2. * M * M), (gp1) / (2. * (gm1)) ) / M
                    - areaRatio;
        Real dM = -rsd.value()/rsd.deriv();
        M += dM;
      } while (fabs(rsd.value()) > 1.e-11);

      Real Mach_IC = M.value();
      // shock relations
      Real M2 = std::sqrt((gm1*Mach_IC*Mach_IC + 2.0)/(2.0*gamma_*Mach_IC*Mach_IC - gm1));
      Real T02 = Tc_;
      Real p02 = rhoc_*R_*Tc_ * std::pow(gp1*Mach_IC*Mach_IC / (gm1*Mach_IC*Mach_IC+2), gamma_/gm1)
                              * std::pow(gp1 / (2*gamma_*Mach_IC*Mach_IC - gm1), 1/gm1);
      Real rho02 = p02 / (R_*T02);
      // new sonic area ratio
      Real Astar2 = rs_ / (pow((gamma_+1)/2,(-(gamma_+1)/(2*(gamma_-1))))
                              * pow(1+(gamma_-1)/2*M2*M2,((gamma_+1)/(2*(gamma_-1)))) / M2);
      // New mach number
      M = 0.3; // subsonic
      M.deriv() = 1;
      Real areaRatio2 = r / Astar2;
      do
      {
        rsd = pow( ((gp1) / 2.), -(gp1) / (2. * (gm1)) ) * pow( (1. + (gm1) / 2. * M * M), (gp1) / (2. * (gm1)) ) / M
                    - areaRatio2;
        Real dM = -rsd.value()/rsd.deriv();
        M += dM;
      } while (fabs(rsd.value()) > 1.e-11);

      Real M3 = M.value();
      // isentropic
      Real rho3 = rho02 * pow( 1. + (gm1) / 2. * M3 * M3, -1 / (gm1) );
      Real T3 = T02 / (1. + (gm1) / 2. * M3 * M3);
      Real q3 = M3 * sqrt( gamma_ * R_ * T3 );
      Real u3 = q3 * x / r;
      Real v3 = q3 * y / r;

      ArrayQ<T> q = 0;
      DensityVelocityTemperature2D<T> dvt( rho3, u3, v3, T3 );
      qinterp_.setFromPrimitive( q, dvt );

      return q;
    }
  }
private:
  const QInterpret qinterp_;
  const GasModel gas_;
  const Real gamma_;
  const Real R_;
  const Real rhoc_;
  const Real Mc_;
  const Real Tc_;
  const Real rs_;

};


} // namespace SANS

#endif  // SOLUTIONFUNCTION_EULER2D_H
