// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCSANSSA2D_SOLUTION_H
#define BCSANSSA2D_SOLUTION_H

#include <memory> // shared_ptr

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "SolutionFunction_RANSSA2D.h"
#include "TraitsRANSSA.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Struct for creating a BC solution function

template<template <class> class PDETraitsSize>
struct BCEulerSolution2DParams;

template<>
struct BCEulerSolution2DParams<TraitsSizeRANSSA> : noncopyable
{
  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Solution function used on the BC" };
    const ParameterString& key = Name;

    const DictOption Const{"Const", SolutionFunction_RANSSA2D_Const_Params::checkInputs};
    const DictOption SAWake{"SAWake", SolutionFunction_RANSSA2D_Wake_Params::checkInputs};
    const DictOption OjedaMMS{"OjedaMMS", SolutionFunction_RANSSA2D_OjedaMMS_Params::checkInputs};
    const DictOption TrigMMS{"TrigMMS", SolutionFunction_RANSSA2D_TrigMMS_Params::checkInputs};

    const std::vector<DictOption> options{ Const, SAWake, OjedaMMS, TrigMMS };
  };
  const ParameterOption<SolutionOptions> Function{"Function", NO_DEFAULT, "The BC is a solution function"};
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
struct BCEulerSolution2D;

// Struct for creating the solution pointer
template <class PDETraitsModel>
struct BCEulerSolution2D<TraitsSizeRANSSA, PDETraitsModel>
{
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = typename TraitsSizeRANSSA<PhysDim>::template ArrayQ<T>;     // solution arrays

  typedef Function2DBase<ArrayQ<Real>> Function2DBaseType;
  typedef std::shared_ptr<Function2DBaseType> Function_ptr;

  static Function_ptr getSolution(const PyDict& d);
};

} // namespace SANS

#endif // BCSANSSA2D_SOLUTION_H
