// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCSANSSA3D_SOLUTION_H
#define BCSANSSA3D_SOLUTION_H

#include <memory> // shared_ptr

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "SolutionFunction_RANSSA3D.h"
#include "TraitsRANSSA.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Struct for creating a BC solution function

template<template <class> class PDETraitsSize>
struct BCEulerSolution3DParams;

template<>
struct BCEulerSolution3DParams<TraitsSizeRANSSA> : noncopyable
{
  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Solution function used on the BC" };
    const ParameterString& key = Name;

    const DictOption OjedaRANSMMS{"OjedaRANSMMS", SolutionFunction_RANSSA3D_OjedaMMS_Params::checkInputs};

    const std::vector<DictOption> options{ OjedaRANSMMS };
  };
  const ParameterOption<SolutionOptions> Function{"Function", NO_DEFAULT, "The BC is a solution function"};
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
struct BCEulerSolution3D;

// Struct for creating the solution pointer
template <class PDETraitsModel>
struct BCEulerSolution3D<TraitsSizeRANSSA, PDETraitsModel>
{
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ = typename TraitsSizeRANSSA<PhysDim>::template ArrayQ<T>;     // solution arrays

  typedef Function3DBase<ArrayQ<Real>> Function3DBaseType;
  typedef std::shared_ptr<Function3DBaseType> Function_ptr;

  static Function_ptr getSolution(const PyDict& d);
};

} // namespace SANS

#endif // BCSANSSA3D_SOLUTION_H
