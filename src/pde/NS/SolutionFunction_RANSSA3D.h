// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef RANSSASOLUTIONFUNCTION3D_H
#define RANSSASOLUTIONFUNCTION3D_H

// 3-D RANSSA PDE: exact and MMS solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // pow

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "NSVariable3D.h"
#include "SAVariable3D.h"
#include "QRANSSA3D.h"

#include "pde/AnalyticFunction/Function3D.h"

namespace SANS
{

// forward declare: solution variables interpreter
template<class QType, template<class > class PDETraitsSize>
class Q3D;

struct SolutionFunction_RANSSA3D_OjedaMMS_Params: noncopyable
{
  const ParameterDict gasModel { "Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};
  const ParameterNumeric<Real> ntRef{"ntRef", 1, 0, NO_LIMIT, "reference value for nt"};

  static void checkInputs(PyDict d);

  static SolutionFunction_RANSSA3D_OjedaMMS_Params params;
};

template<template<class > class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_RANSSA3D_OjedaMMS:
    public Function3DVirtualInterface<SolutionFunction_RANSSA3D_OjedaMMS<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD3>>
{
public:
  typedef PhysD3 PhysDim;

  typedef typename PDETraitsModel::QType QType;
  typedef QRANSSA3D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_RANSSA3D_OjedaMMS_Params ParamsType;

  explicit SolutionFunction_RANSSA3D_OjedaMMS(const PyDict& d) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) ), d.get(ParamsType::params.ntRef)),
    ntRef_(d.get(ParamsType::params.ntRef))  {}
  explicit SolutionFunction_RANSSA3D_OjedaMMS(const GasModel& gas, const Real ntRef) :
      qinterp_( gas, ntRef ), ntRef_(ntRef) {}

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    T rho, u, v, w, p, nt;

    //POSITIVE BUMP
    rho = 1.0 + 0.1*exp( -(x-0.5)*(x-0.5)*75. -(y-0.5)*(y-0.5)*75.  -(z-0.5)*(z-0.5)*75. );
    u = 0.2 + 0.3*exp( -(x-0.5)*(x-0.5)*75. -(y-0.5)*(y-0.5)*75.  -(z-0.5)*(z-0.5)*75. );
    v =  0.1 + 0.1*exp(-(x-0.5)*(x-0.5)*75. -(y-0.5)*(y-0.5)*75.  -(z-0.5)*(z-0.5)*75. );
    w =  0.15 - 0.05*exp( -(x-0.5)*(x-0.5)*75. -(y-0.5)*(y-0.5)*75.  -(z-0.5)*(z-0.5)*75. );
    p = pow(rho, 1.4);
    nt = (0.0001/ntRef_)*( 1 + 10.*exp( -(x-0.5)*(x-0.5)*75. -(y-0.5)*(y-0.5)*75. -(z-0.5)*(z-0.5)*75. ) );

    ArrayQ<T> q = 0;
    SAnt3D<DensityVelocityPressure3D<T>> dvp( {rho, u, v, w, p, nt} );
    qinterp_.setFromPrimitive(q, dvp);

    return q;
  }

private:
  const QInterpret qinterp_;
  const Real ntRef_;
};

} // namespace SANS

#endif  // RANSSASOLUTIONFUNCTION3D_H
