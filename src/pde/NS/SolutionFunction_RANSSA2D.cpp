// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/NS/SolutionFunction_RANSSA2D.h"

namespace SANS
{
// cppcheck-suppress passedByValue
void SolutionFunction_RANSSA2D_Const_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.v));
  allParams.push_back(d.checkInputs(params.p));
  allParams.push_back(d.checkInputs(params.nt));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_RANSSA2D_Const_Params SolutionFunction_RANSSA2D_Const_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_RANSSA2D_Wake_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.wakedepth));
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.p));
  allParams.push_back(d.checkInputs(params.nt));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_RANSSA2D_Wake_Params SolutionFunction_RANSSA2D_Wake_Params::params;


// cppcheck-suppress passedByValue
void SolutionFunction_RANSSA2D_OjedaMMS_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.ntRef));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_RANSSA2D_OjedaMMS_Params SolutionFunction_RANSSA2D_OjedaMMS_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_RANSSA2D_TrigMMS_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.L));

  allParams.push_back(d.checkInputs(params.r0));
  allParams.push_back(d.checkInputs(params.rx));
  allParams.push_back(d.checkInputs(params.ry));
  allParams.push_back(d.checkInputs(params.rxy));
  allParams.push_back(d.checkInputs(params.arx));
  allParams.push_back(d.checkInputs(params.ary));
  allParams.push_back(d.checkInputs(params.arxy));

  allParams.push_back(d.checkInputs(params.u0));
  allParams.push_back(d.checkInputs(params.ux));
  allParams.push_back(d.checkInputs(params.uy));
  allParams.push_back(d.checkInputs(params.uxy));
  allParams.push_back(d.checkInputs(params.aux));
  allParams.push_back(d.checkInputs(params.auy));
  allParams.push_back(d.checkInputs(params.auxy));

  allParams.push_back(d.checkInputs(params.v0));
  allParams.push_back(d.checkInputs(params.vx));
  allParams.push_back(d.checkInputs(params.vy));
  allParams.push_back(d.checkInputs(params.vxy));
  allParams.push_back(d.checkInputs(params.avx));
  allParams.push_back(d.checkInputs(params.avy));
  allParams.push_back(d.checkInputs(params.avxy));

  allParams.push_back(d.checkInputs(params.p0));
  allParams.push_back(d.checkInputs(params.px));
  allParams.push_back(d.checkInputs(params.py));
  allParams.push_back(d.checkInputs(params.pxy));
  allParams.push_back(d.checkInputs(params.apx));
  allParams.push_back(d.checkInputs(params.apy));
  allParams.push_back(d.checkInputs(params.apxy));

  allParams.push_back(d.checkInputs(params.nt0));
  allParams.push_back(d.checkInputs(params.ntx));
  allParams.push_back(d.checkInputs(params.nty));
  allParams.push_back(d.checkInputs(params.ntxy));
  allParams.push_back(d.checkInputs(params.antx));
  allParams.push_back(d.checkInputs(params.anty));
  allParams.push_back(d.checkInputs(params.antxy));

  d.checkUnknownInputs(allParams);
}
SolutionFunction_RANSSA2D_TrigMMS_Params SolutionFunction_RANSSA2D_TrigMMS_Params::params;

}
