// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_EULER1D_H
#define SOLUTIONFUNCTION_EULER1D_H

// quasi 1-D Euler PDE: exact and MMS solutions

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // exp

#include "tools/SANSnumerics.h"     // Real

#include "PDEEuler1D.h"
#include "pde/AnalyticFunction/Function1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "getEulerTraitsModel.h"
//#include "LinearAlgebra/AlgebraicEquationSetBase.h"
//#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q1D;


//----------------------------------------------------------------------------//
// solution: Linear Combination
// template for combining two solution functions to make another
// TODO: not quite sure how this works at the moment...

//template <class SOLN1, class SOLN2>
//class SolutionFunction_Euler1D_Combination
//{
//public:
//  typedef PhysD1 PhysDim;
//
//  template<class T>
//  using ArrayQ = EulerTraits<PhysD1>::ArrayQ<T>;
//
//  struct ParamsType : noncopyable
//  {
//    const ParameterNumeric<Real> a{"a", 1, NO_RANGE, "Coefficient for Function 1"};
//    const ParameterNumeric<Real> b{"b", 1, NO_RANGE, "Coefficient for Function 2"};
//    const ParameterDict SolutionArgs1{"SolutionArgs1", EMPTY_DICT, SOLN1::ParamsType::checkInputs, "Solution function arguments 1"};
//    const ParameterDict SolutionArgs2{"SolutionArgs2", EMPTY_DICT, SOLN2::ParamsType::checkInputs, "Solution function arguments 2"};
//
//    static void checkInputs(PyDict d);
//
//    static ParamsType params;
//  };
//
//  explicit SolutionFunction_Euler1D_Combination( PyDict d ) :
//      a_(d.get(ParamsType::params.a)),
//      f1_(d.get(ParamsType::params.SolutionArgs1)),
//      b_(d.get(ParamsType::params.b)),
//      f2_(d.get(ParamsType::params.SolutionArgs1)) {}
//  explicit SolutionFunction_Euler1D_Combination( const Real& a, const SOLN1& f1, const Real& b, const SOLN2& f2 ) :
//      a_(a), f1_(f1), b_(b), f2_(f2) {}
//
//  template<class T>
//  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
//  {
//    return a_*f1_(x,y,time) + b_*f2_(x,y,time);
//  }
//
//  void dump( int indentSize=0, std::ostream& out = std::cout ) const
//  {
//    std::string indent(indentSize, ' ');
//    out << indent << "SolutionFunction_Euler1D_Combination:  a_, b_ = " << a_ << ", " << b_ << std::endl;
//  }
//
//private:
//  Real a_;
//  SOLN1 f1_;
//  Real b_;
//  SOLN2 f2_;
//};


//----------------------------------------------------------------------------//
// solution: const

struct SolutionFunction_Euler1D_Const_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, NO_RANGE, "Constant of the density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Constant of the x-velocity"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, NO_RANGE, "Constant of the pressure"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler1D_Const_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler1D_Const :
    public Function1DVirtualInterface<SolutionFunction_Euler1D_Const<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD1>>
{
public:
  typedef PhysD1 PhysDim;

  typedef SolutionFunction_Euler1D_Const_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler1D_Const( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
    rho_(d.get(ParamsType::params.rho)),
    u_(d.get(ParamsType::params.u)),
    p_(d.get(ParamsType::params.p))
  {
    // Nothing
  }

  explicit SolutionFunction_Euler1D_Const( const GasModel& gas, const Real& rho, const Real& u, const Real& p  ) :
    qinterp_(gas),
    rho_(rho),
    u_(u),
    p_(p)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    ArrayQ<T> q = 0;
    DensityVelocityPressure1D<T> dvp( rho_, u_, p_ );
    qinterp_.setFromPrimitive(q, dvp);
    return q;
  }

private:
  const QInterpret qinterp_;
  const Real rho_;
  const Real u_;
  const Real p_;
};


//----------------------------------------------------------------------------//
// solution: Riemann
// NOTE: This is really only to create 1D Riemann problem initial conditions

struct SolutionFunction_Euler1D_Riemann_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> interface{"interface", NO_DEFAULT, NO_RANGE, "Location of the discontinuity interface"};

  const ParameterNumeric<Real> rhoL{"rhoL", NO_DEFAULT, NO_RANGE, "Left side density"};
  const ParameterNumeric<Real> uL{"uL", NO_DEFAULT, NO_RANGE, "Left side x-velocity"};
  const ParameterNumeric<Real> pL{"pL", NO_DEFAULT, NO_RANGE, "Left side pressure"};

  const ParameterNumeric<Real> rhoR{"rhoR", NO_DEFAULT, NO_RANGE, "Right side density"};
  const ParameterNumeric<Real> uR{"uR", NO_DEFAULT, NO_RANGE, "Right side x-velocity"};
  const ParameterNumeric<Real> pR{"pR", NO_DEFAULT, NO_RANGE, "Right side pressure"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler1D_Riemann_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler1D_Riemann :
    public Function1DVirtualInterface<SolutionFunction_Euler1D_Riemann<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD1>>
{
public:
  typedef PhysD1 PhysDim;

  typedef SolutionFunction_Euler1D_Riemann_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler1D_Riemann( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
    x_(d.get(ParamsType::params.interface))
  {
    qinterp_.setFromPrimitive(qL_, DensityVelocityPressure1D<Real>( d.get(ParamsType::params.rhoL),
                                                                    d.get(ParamsType::params.uL),
                                                                    d.get(ParamsType::params.pL) ));
    qinterp_.setFromPrimitive(qR_, DensityVelocityPressure1D<Real>( d.get(ParamsType::params.rhoR),
                                                                    d.get(ParamsType::params.uR),
                                                                    d.get(ParamsType::params.pR) ));
  }

  explicit SolutionFunction_Euler1D_Riemann( const GasModel& gas, const Real& interface,
                                            const Real& rhoL, const Real& uL, const Real& pL,
                                            const Real& rhoR, const Real& uR, const Real& pR  ) :
    qinterp_(gas),
    x_(interface)
  {
    qinterp_.setFromPrimitive(qL_, DensityVelocityPressure1D<Real>( rhoL, uL, pL ));
    qinterp_.setFromPrimitive(qR_, DensityVelocityPressure1D<Real>( rhoR, uR, pR ));
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    if (x < x_)
    {
      return qL_;
    }
    else
    {
      return qR_;
    }
    // We should not get here
  }

private:
  const QInterpret qinterp_;
  const Real x_;
  ArrayQ<Real> qL_;
  ArrayQ<Real> qR_;
};


//----------------------------------------------------------------------------//
// solution: DensityPulse
// NOTE: This is really only to create 1D DensityPulse problem initial conditions

struct SolutionFunction_Euler1D_DensityPulse_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> center{"center", NO_DEFAULT, NO_RANGE, "Center of pulse"};
  const ParameterNumeric<Real> width{"width", NO_DEFAULT, NO_RANGE, "Gaussian characteristic width of pulse"};
  const ParameterNumeric<Real> height{"height", NO_DEFAULT, NO_RANGE, "Height of the pulse"};

  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, NO_RANGE, "Ambient density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Ambient x-velocity"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, NO_RANGE, "Ambient pressure"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler1D_DensityPulse_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler1D_DensityPulse :
    public Function1DVirtualInterface<SolutionFunction_Euler1D_DensityPulse<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD1>>
{
public:
  typedef PhysD1 PhysDim;

  typedef SolutionFunction_Euler1D_DensityPulse_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;
  typedef typename TraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler1D_DensityPulse( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
    xc_(d.get(ParamsType::params.center)),
    s_(d.get(ParamsType::params.width)),
    h_(d.get(ParamsType::params.height)),
    rho_(d.get(ParamsType::params.rho)),
    u_(d.get(ParamsType::params.u)),
    p_(d.get(ParamsType::params.p))
  {
    // Nothing
  }

  explicit SolutionFunction_Euler1D_DensityPulse( const GasModel& gas, const Real& center,
                                                 const Real& width, const Real& height,
                                                 const Real& rho, const Real& u, const Real& p  ) :
    qinterp_(gas),
    xc_(center),
    s_(width),
    h_(height),
    rho_(rho),
    u_(u),
    p_(p)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    ArrayQ<T> q = 0;
    T rho = rho_ + h_*exp(-(x-xc_)*(x-xc_) / 2.0 / (s_*s_));

    DensityVelocityPressure1D<T> dvp( rho, u_, p_ );
    qinterp_.setFromPrimitive(q, dvp);

    return q;
  }

private:
  const QInterpret qinterp_;
  const Real xc_;
  const Real s_;
  const Real h_;
  const Real rho_;
  const Real u_;
  const Real p_;
};

//----------------------------------------------------------------------------//
// solution: DensityPulse
// NOTE: This is really only to create initial conditions for Arthur's 1D shock problem

struct SolutionFunction_Euler1D_ArtShock_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> xShock{"xShock", NO_DEFAULT, NO_RANGE, "Shock x location"};
  const ParameterNumeric<Real> xThroat{"xThroat", NO_DEFAULT, NO_RANGE, "Throat Location"};
  const ParameterNumeric<Real> pt1{"pt1", NO_DEFAULT, NO_RANGE, "Inlet stagnation pressure"};
  const ParameterNumeric<Real> pt2{"pt2", NO_DEFAULT, NO_RANGE, "Stagnation pressure after shock"};
  const ParameterNumeric<Real> Tt{"Tt", NO_DEFAULT, NO_RANGE, "Stagnation temperature"};
  const ParameterNumeric<Real> pe{"pe", NO_DEFAULT, NO_RANGE, "exit pressure"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler1D_ArtShock_Params params;
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler1D_ArtShock :
    public Function1DVirtualInterface<SolutionFunction_Euler1D_ArtShock<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD1>>
{
public:
  typedef PhysD1 PhysDim;

  typedef SolutionFunction_Euler1D_ArtShock_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;
  typedef typename TraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler1D_ArtShock( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
    gas_( d.get(ParamsType::params.gasModel) ),
    xthroat_( d.get(ParamsType::params.xThroat) ),
    xshock_( d.get(ParamsType::params.xShock) ),
    pt1_( d.get(ParamsType::params.pt1) ),
    pt2_( d.get(ParamsType::params.pt2) ),
    Tt_( d.get(ParamsType::params.Tt) ),
    pe_( d.get(ParamsType::params.pe) ),
    area_(),
    mdot_(0.0)
  {
    Real gamma = gas_.gamma();
    Real Me = sqrt( 2.0/(gamma-1.0)*(pow(pt2_/pe_, (gamma-1.0)/gamma ) - 1.0) );

    Real De = sqrt(gamma)*Me / pow( 1 + 0.5*(gamma-1)*Me*Me , 0.5*(gamma+1.)/(gamma-1.)  );//exit corrected flow per unit area
    Real Ae =  area_(1.0, 0.0);

    mdot_ = De*pt2_*Ae / sqrt(Tt_);
  }

  explicit SolutionFunction_Euler1D_ArtShock( const GasModel& gas, const Real& xthroat, const Real& xshock,
                                              const Real& pt1, const Real& pt2, const Real& Tt, const Real& pe) :
    qinterp_(gas),
    gas_( gas ),
    xthroat_( xthroat ),
    xshock_( xshock ),
    pt1_( pt1 ),
    pt2_( pt2 ),
    Tt_( Tt ),
    pe_( pe ),
    area_(),
    mdot_(0.0)
  {
      Real gamma = gas_.gamma();
      Real Me = sqrt( 2.0/(gamma-1.0)*(pow(pt2_/pe_, (gamma-1.0)/gamma ) - 1.0) );

      Real De = sqrt(gamma)*Me / pow(1 + 0.5*(gamma-1)*Me*Me , 0.5*(gamma+1.)/(gamma-1.)  );//exit corrected flow per unit area
      Real Ae =  area_(1.0, 0.0);

      mdot_ = De*pt2_*Ae / sqrt(Tt_);
  }


  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
//    std::shared_ptr<ScalarFunction1D_ArtNozzle> area( new ScalarFunction1D_ArtNozzle() );

    Real gamma = gas_.gamma();
    Real R = gas_.R();

    Real Pt = pt1_;
    if (x > xshock_)
      Pt = pt2_; // apply stagnation pressure shock jump

    T A =  area_(x, time);
    T D = mdot_*sqrt(Tt_)/Pt/A; // corrected flow per unit area at x location

    //solve for Mach number D = sqrt(gamma)* M / (1+0.5*(gamma-1)*M^2)^ (0.5*(gamma+1.)/(gamma-1.))

    T M = 0.5; //default to subsonic
    if ( x > xthroat_ && x < xshock_) //supersonic branch
      M = 2.0;

    T D2 = sqrt(gamma)* M / pow(1 + 0.5*(gamma-1)*M*M , 0.5*(gamma+1.)/(gamma-1.) );
    T resid = D2 - D;
    T drdm;
    int i = 0;

    //mini Newton solver to find M(A)
    while ( fabs(resid) > 1e-10 && i<10)
    {
//      drdm = -6.0*sqrt(1.4)*M*M / (5.0*pow(1*0.2*M*M, 4.0)) + sqrt(1.4)/pow(1*0.2*M*M, 3.0);
      drdm = -2*(M*M-1)*sqrt(gamma) *pow(1 + 0.5*M*M*(gamma-1), (gamma+1)/(2-2*gamma)) / (2 + M*M*(gamma-1));

      M -= resid/drdm;
      D2 = sqrt(gamma)* M / pow(1 + 0.5*(gamma-1)*M*M , 0.5*(gamma+1.)/(gamma-1.) );
      resid = (D2 - D);
      i++;
    }

    if (resid > 1e-10)
      std::cout << "\nWARNING, INLET CONDITION NOT CONVERGED: x = "
      << x << ", time= " << time << ", resid= " << resid << ", Mach= " << M << "\n";

    T p = Pt / pow(1 + 0.5*(gamma-1)*M*M, 3.5);
    T t = Tt_ / (1 + 0.5*(gamma-1)*M*M);
    T rho = p/t;
    T c = sqrt(gamma*R*t);
    T u = M*c;

    ArrayQ<T> q = 0;

    DensityVelocityPressure1D<T> dvp( rho, u, p );
    qinterp_.setFromPrimitive(q, dvp);

    return q;

  }

private:
  const QInterpret qinterp_;
  const GasModel gas_;
  const Real xthroat_;
  const Real xshock_;
  const Real pt1_;
  const Real pt2_;
  const Real Tt_;
  const Real pe_;
  const ScalarFunction1D_ArtNozzle area_;
  mutable Real mdot_;
};

//----------------------------------------------------------------------------//
// solution: SineBackpressure
// NOTE: This is to create 1D Sine wave backpressure

struct SolutionFunction_Euler1D_SineBackPressure_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, NO_RANGE, "Ambient density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Ambient x-velocity"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, NO_RANGE, "Average pressure"};
  const ParameterNumeric<Real> dp{"dp", NO_DEFAULT, NO_RANGE, "Pressure fluctuation"};
  const ParameterNumeric<Real> dt{"dt", NO_DEFAULT, NO_RANGE, "Period of the fluctuation"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler1D_SineBackPressure_Params params;
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler1D_SineBackPressure :
    public Function1DVirtualInterface<SolutionFunction_Euler1D_SineBackPressure<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD1>>
{
public:
  typedef PhysD1 PhysDim;

  typedef SolutionFunction_Euler1D_SineBackPressure_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;
  typedef typename TraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler1D_SineBackPressure( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
    rho_(d.get(ParamsType::params.rho)),
    u_(d.get(ParamsType::params.u)),
    p_(d.get(ParamsType::params.p)),
    dp_(d.get(ParamsType::params.dp)),
    dt_(d.get(ParamsType::params.dt))
  {
    // Nothing
  }

  explicit SolutionFunction_Euler1D_SineBackPressure( const GasModel& gas, const Real& rho, const Real& u, const Real& p,
                                                      const Real& dp, const Real& dt) :
    qinterp_(gas),
    rho_(rho),
    u_(u),
    p_(p),
    dp_(dp),
    dt_(dt)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    T p = p_ - dp_*sin( 2*PI*time / dt_ );

    ArrayQ<T> q = 0;

    DensityVelocityPressure1D<T> dvp( rho_, u_, p );
    qinterp_.setFromPrimitive(q, dvp);

    return q;

  }

private:
  const QInterpret qinterp_;
  const Real rho_;
  const Real u_;
  const Real p_;
  const Real dp_;
  const Real dt_;
};

//----------------------------------------------------------------------------//
// solution: SSME density, velocity, and temperature
// NOTE: This is really only to create 1D SSME problem initial conditions

struct SolutionFunction_Euler1D_SSME_Params: noncopyable
{
  const ParameterDict gasModel { "Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

const ParameterNumeric<Real> center
{ "center", NO_DEFAULT, NO_RANGE, "Center of pulse"};
const ParameterNumeric<Real> width
{ "width", NO_DEFAULT, NO_RANGE, "Gaussian characteristic width of pulse"};
const ParameterNumeric<Real> height
{ "height", NO_DEFAULT, NO_RANGE, "Height of the pulse"};

const ParameterNumeric<Real> rho
{ "rho", NO_DEFAULT, NO_RANGE, "Ambient density"};
const ParameterNumeric<Real> u
{ "u", NO_DEFAULT, NO_RANGE, "Ambient x-velocity"};
const ParameterNumeric<Real> T
{ "T", NO_DEFAULT, NO_RANGE, "Ambient temperature"};

static void checkInputs(PyDict d);

static SolutionFunction_Euler1D_SSME_Params params;
};
#define TESTSSME 0


template<template<class > class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler1D_SSME
{
public:
  typedef PhysD1 PhysDim;

  typedef SolutionFunction_Euler1D_SSME_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter


  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler1D_SSME(const GasModel& gas, const Real& gamma, const Real& R, const Real& rhoc, const Real& Tc)
      : qinterp_( gas ), gamma_( gamma ), R_( R ), rhoc_( rhoc ), Tc_( Tc )
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& time) const
  {
    std::shared_ptr<ScalarFunction1D_SSME> area( new ScalarFunction1D_SSME() );

    Real ThroatArea = (*area)( 0.0, 0.3 );
    Real areaRatio = (*area)( x, 0.3 ) / ThroatArea;

    SurrealS<1> M;
    if (x< 0)
      M = 0.1;
    else
      M = 3;


    M.deriv() = 1;

    SurrealS<1> rsd = 1;
    Real gm1 = gamma_ - 1;
    Real gp1 = gamma_ + 1;
    do
    {
      rsd = pow( ((gp1) / 2), -(gp1) / (2 * (gm1)) ) * pow( (1 + (gm1) / 2 * M * M), (gp1) / (2 * (gm1)) ) / M
                  - areaRatio;
      Real dM = -rsd.value()/rsd.deriv();
      M += dM;
    } while (fabs(rsd.value()) > 1.e-11);

    Real Mach_IC = M.value();

    Real rho = rhoc_ * pow( 1 + (gm1) / 2 * Mach_IC * Mach_IC, -1 / (gm1) );
    Real T_IC = Tc_ / (1 + (gm1) / 2 * Mach_IC * Mach_IC);
    Real u = Mach_IC * sqrt( gamma_ * R_ * T_IC );

    ArrayQ<T> q = 0;
    DensityVelocityTemperature1D<T> dvt( rho, u, T_IC );
    qinterp_.setFromPrimitive( q, dvt );

    return q;
  }
private:
  const QInterpret qinterp_;
  const Real gamma_;
  const Real R_;
  const Real rhoc_;
  const Real Tc_;

};


//----------------------------------------------------------------------------//
// solution: ShuOsher
// NOTE: This is really only to create 1D ShuOsher problem initial conditions

struct SolutionFunction_Euler1D_ShuOsher_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> interface{"interface", NO_DEFAULT, NO_RANGE, "Location of the discontinuity interface"};

  const ParameterNumeric<Real> rhoL{"rhoL", NO_DEFAULT, NO_RANGE, "Left side density"};
  const ParameterNumeric<Real> uL{"uL", NO_DEFAULT, NO_RANGE, "Left side x-velocity"};
  const ParameterNumeric<Real> pL{"pL", NO_DEFAULT, NO_RANGE, "Left side pressure"};

  const ParameterNumeric<Real> epsilon{"epsilon", NO_DEFAULT, NO_RANGE, "Density perturbation magnitude"};
  const ParameterNumeric<Real> lambda{"lambda", NO_DEFAULT, NO_RANGE, "Density perturbation frequency"};
  const ParameterNumeric<Real> rhoR{"rhoR", NO_DEFAULT, NO_RANGE, "Right side density unperturbed"};
  const ParameterNumeric<Real> uR{"uR", NO_DEFAULT, NO_RANGE, "Right side x-velocity"};
  const ParameterNumeric<Real> pR{"pR", NO_DEFAULT, NO_RANGE, "Right side pressure"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Euler1D_ShuOsher_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Euler1D_ShuOsher :
    public Function1DVirtualInterface<SolutionFunction_Euler1D_ShuOsher<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD1>>
{
public:
  typedef PhysD1 PhysDim;

  typedef SolutionFunction_Euler1D_ShuOsher_Params ParamsType;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Euler1D_ShuOsher( const PyDict& d ) :
    qinterp_(GasModel( d.get(ParamsType::params.gasModel) )),
    x_(d.get(ParamsType::params.interface)),
    epsilon_(d.get(ParamsType::params.epsilon)),
    lambda_(d.get(ParamsType::params.lambda)),
    rhoR_(d.get(ParamsType::params.rhoR)),
    uR_(d.get(ParamsType::params.uR)),
    pR_(d.get(ParamsType::params.pR))
  {
    qinterp_.setFromPrimitive(qL_, DensityVelocityPressure1D<Real>( d.get(ParamsType::params.rhoL),
                                                                    d.get(ParamsType::params.uL),
                                                                    d.get(ParamsType::params.pL) ));
  }

  explicit SolutionFunction_Euler1D_ShuOsher( const GasModel& gas, const Real& interface,
                                              const Real& rhoL, const Real& uL, const Real& pL,
                                              const Real& epsilon, const Real& lambda,
                                              const Real& rhoR, const Real& uR, const Real& pR  ) :
    qinterp_(gas),
    x_(interface),
    epsilon_(epsilon),
    lambda_(lambda),
    rhoR_(rhoR),
    uR_(uR),
    pR_(pR)
  {
    qinterp_.setFromPrimitive(qL_, DensityVelocityPressure1D<Real>( rhoL, uL, pL ));
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    if (x < x_)
    {
      return qL_;
    }
    else
    {
      ArrayQ<T> qR = 0;
      T rho = rhoR_ + epsilon_*sin(lambda_*x);
      DensityVelocityPressure1D<T> dvp( rho, uR_, pR_ );
      qinterp_.setFromPrimitive(qR, dvp);
      return qR;
    }
    // We should not get here
  }

private:
  const QInterpret qinterp_;
  const Real x_;
  Real epsilon_, lambda_;
  Real rhoR_, uR_, pR_;
  ArrayQ<Real> qL_;
};


} // namespace SANS

#endif  // SOLUTIONFUNCTION_EULER1D_H
