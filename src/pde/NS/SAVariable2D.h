// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SAVARIALBE2D_H
#define SAVARIALBE2D_H

#include "NSVariable2D.h"

namespace SANS
{

//===========================================================================//
template<class Derived>
struct SAVariableType2D
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//===========================================================================//
template<class NSVariableParams>
struct SAnt2DParams : public NSVariableParams
{
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "S-A Working Variable"};

  static void checkInputs(PyDict d);

  // cppcheck-suppress duplInheritedMember
  static SAnt2DParams params;
};

//---------------------------------------------------------------------------//
template<class NSVariables>
struct SAnt2D;

template<template<class> class NSVariables, class T>
struct SAnt2D<NSVariables<T>> : public SAVariableType2D<SAnt2D<NSVariables<T>>>,
                                public NSVariables<T>
{
  typedef SAnt2DParams<typename NSVariables<T>::ParamsType> ParamsType;

  SAnt2D() {}

  explicit SAnt2D( const PyDict& d )
    : NSVariables<T>(d), SANutilde( d.get(ParamsType::params.nt) ) {}

  // cppcheck-suppress noExplicitConstructor
  SAnt2D( const std::initializer_list<T>& s ) { operator=(s); }

  SAnt2D( const NSVariables<T>& var, const T& nt )
    : NSVariables<T>(var), SANutilde(nt) {}

  SAnt2D& operator=( const std::initializer_list<T>& s )
  {
    SANS_ASSERT(s.size() == 5);
    auto q = s.begin();
    NSVariables<T>::operator=({*(q+0),*(q+1),*(q+2),*(q+3)});
    SANutilde = *(q+4);

    return *this;
  }

  T SANutilde;
};

//===========================================================================//
template<>
struct SAnt2DParams<Conservative2DParams> : public Conservative2DParams
{
  const ParameterNumeric<Real> rhont{"rhont", NO_DEFAULT, NO_RANGE, "S-A Working Variable"};

  static void checkInputs(PyDict d);

  // cppcheck-suppress duplInheritedMember
  static SAnt2DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct SAnt2D<Conservative2D<T>> : public SAVariableType2D<SAnt2D<Conservative2D<T>>>,
                                   public Conservative2D<T>
{
  typedef SAnt2DParams<typename Conservative2D<T>::ParamsType> ParamsType;

  SAnt2D() {}

  explicit SAnt2D( const PyDict& d )
    : Conservative2D<T>(d), rhoSANutilde( d.get(ParamsType::params.rhont) ) {}

  // cppcheck-suppress noExplicitConstructor
  SAnt2D( const std::initializer_list<T>& s ) { operator=(s); }

  SAnt2D( const Conservative2D<T>& var, const T& rhont )
    : Conservative2D<T>(var), rhoSANutilde(rhont) {}

  SAnt2D& operator=( const std::initializer_list<T>& s )
  {
    SANS_ASSERT(s.size() == 5);
    auto q = s.begin();
    Conservative2D<T>::operator=({*(q+0),*(q+1),*(q+2),*(q+3)});
    rhoSANutilde = *(q+4);

    return *this;
  }

  T rhoSANutilde;
};

//===========================================================================//
struct SAVariableType2DParams : noncopyable
{
  struct VariableOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Variables{"Variables", NO_DEFAULT, "Name of the variable set" };
    const ParameterString& key = Variables;

    const DictOption Conservative{"Conservative", SAnt2DParams<Conservative2DParams>::checkInputs};
    const DictOption PrimitiveTemperature{"Primitive Temperature", SAnt2DParams<DensityVelocityTemperature2DParams>::checkInputs};
    const DictOption PrimitivePressure{"Primitive Pressure", SAnt2DParams<DensityVelocityPressure2DParams>::checkInputs};

    const std::vector<DictOption> options{Conservative,PrimitiveTemperature,PrimitivePressure};
  };
  const ParameterOption<VariableOptions> StateVector{"StateVector", NO_DEFAULT, "The state vector of variables"};

  static void checkInputs(PyDict d);

  static SAVariableType2DParams params;
};


}

#endif //SAVARIALBE2D_H
