// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDENAVIERSTOKES1D_H
#define PDENAVIERSTOKES1D_H

// 2-D compressible Navier-Stokes PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Topology/Dimension.h"
#include "pde/ForcingFunctionBase.h"

#include "GasModel.h"
#include "NSVariable1D.h"
#include "PDEEuler1D.h"

#include <iostream>
#include <string>

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q1D;


//----------------------------------------------------------------------------//
// PDE class: 2-D compressible Navier-Stokes
//
// template parameters:
//   T                        solution DOF data type (e.g. Real)
//   PDETraitsSize            define PDE size-related features
//     N, D                   PDE size, physical dimensions
//     ArrayQ                 solution/residual arrays
//     MatrixQ                matrices (e.g. flux jacobian)
//   PDETraitsModel           define PDE model-related features
//     QType                  solution variable set (e.g. primitive)
//     QInterpret             solution variable interpreter
//     GasModel               gas model (e.g. ideal gas law)
//     ViscosityModel         molecular viscosity model (e.g. Sutherland)
//     ThermalConductivityModel   thermal conductivity model (e.g. const Prandtl number)
//
// member functions:
//   .hasFluxAdvectiveTime     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//INHERITS CONSERVATIVE, ADVECTIVE FLUXES FROM EULER1D
//   .masterState        unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDENavierStokes1D : public PDEEuler1D<PDETraitsSize, PDETraitsModel>
{
public:
  typedef PhysD1 PhysDim;

  typedef PDEEuler1D<PDETraitsSize, PDETraitsModel> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  using EulerResidualInterpCategory = typename BaseType::EulerResidualInterpCategory;

  typedef typename PDETraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;                                       // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                                 // gas model

  typedef typename PDETraitsModel::ViscosityModel ViscosityModel;                     // molecular viscosity model

  typedef typename PDETraitsModel::ThermalConductivityModel ThermalConductivityModel; // thermal conductivity model

  typedef ForcingFunctionBase1D< PDENavierStokes1D<PDETraitsSize, PDETraitsModel> > ForcingFunctionType;

  typedef std::shared_ptr<Function1DBase<Real>> AreaFunction_ptr;

  explicit PDENavierStokes1D( const GasModel& gas, const ViscosityModel& visc,
         const ThermalConductivityModel& tcond,
         const EulerResidualInterpCategory& cat,
         const AreaFunction_ptr& area = NULL ) :
      PDEEuler1D<PDETraitsSize, PDETraitsModel>( gas, cat, area ),
      visc_(visc),
      tcond_(tcond)
  {
    // Nothing
  }

  PDENavierStokes1D& operator=( const PDENavierStokes1D& ) = delete;

  // flux components
  bool hasFluxViscous() const { return true; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx,ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax ) const;

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      const ArrayQ<T>& qxx,
      ArrayQ<T>& strongPDE ) const;

  const ViscosityModel& viscosityModel() const {return visc_;}
  const ThermalConductivityModel& thermalConductivityModel() const {return tcond_;}

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BaseType::gas_;
  using BaseType::qInterpret_;
  using BaseType::area_;
  const ViscosityModel& visc_;
  const ThermalConductivityModel& tcond_;
};


// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& f ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq rho, u, t;
  T rhox, ux, tx;
  Tq mu, k, lambda;
  T tauxx;
  Real A = (area_ != NULL) ? (*area_)(x, time) : 1.0;

  qInterpret_.eval( q, rho, u, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, tx );

  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );
  lambda = -2./3. * mu;

  tauxx = mu*(2*ux   ) + lambda*(ux);

  f(1) -= A *  tauxx;
  f(2) -= A * (k*tx + u*tauxx);
}


// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    const Real& nx,ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;

  fluxViscous(x, time, qL, qxL, fL);
  fluxViscous(x, time, qR, qxR, fR);

  f += 0.5*(fL + fR)*nx;

#else
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq rhoL, uL, tL;
  T rhoxL, uxL, txL;
  Tq rhoR, uR, tR;
  T rhoxR, uxR, txR;
  Tq u, t;
  T ux, tx;
  Tq mu, k, lambda;
  T tauxx;
  Real A = (area_ != NULL) ? (*area_)(x, time) : 1.0;

  qInterpret_.eval( qL, rhoL, uL, tL );
  qInterpret_.eval( qR, rhoR, uR, tR );
  qInterpret_.evalGradient( qL, qxL, rhoxL, uxL, txL );
  qInterpret_.evalGradient( qR, qxR, rhoxR, uxR, txR );

  u = 0.5*(uL + uR);
  t = 0.5*(tL + tR);

  ux = 0.5*(uxL + uxR);
  tx = 0.5*(txL + txR);

  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );
  lambda = -2./3. * mu;

  tauxx = mu*(2*ux   ) + lambda*(ux);

  f(1) -= A * nx*(         tauxx);
  f(2) -= A * nx*(k*tx + u*tauxx);
#endif
}


// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDENavierStokes1D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tk>& kxx ) const
{
  Tq rho = 0.0;
  Tq u = 0.0;
  Tq t = 0.0;
  Tq e0 = 0.0;
  Tq mu, k, lambda;
  Real Cv;

  qInterpret_.eval( q, rho, u, t );
  e0 = gas_.energy(rho, t) + 0.5*(u*u);
  Cv = gas_.Cv();
  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );
  lambda = -2./3. * mu;

  // d(Fv)/d(Ux)
  kxx(1,0) += -u*(2*mu + lambda)/rho;
  kxx(1,1) +=    (2*mu + lambda)/rho;
  kxx(2,0) += -u*u*((2*mu + lambda) - k/Cv)/rho - e0*k/(Cv*rho);
  kxx(2,1) +=    u*((2*mu + lambda) - k/Cv)/rho;
  kxx(2,2) +=                                        k/(Cv*rho);

}

// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDENavierStokes1D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    const ArrayQ<T>& qxx,
    ArrayQ<T>& strongPDE ) const
{
  T rho, u, t = 0;
  T rhox, rhoy, ux, tx = 0;
  T mu, k, lambda = 0;
  T tauxx = 0;

  qInterpret_.eval( q, rho, u, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, tx );

  //EVALUATE SECOND DERIVATIVES
  T rhoxx;
  T uxx;
  T vxx;
  T txx;

  qInterpret_.evalSecondGradient( q, qx, qxx, rhoxx, uxx, txx);

  mu = visc_.viscosity( t );
  T mut = 0;
  visc_.viscosityJacobian( t, mut );
  T mux = mut*tx;
  lambda = -2./3. * mu;
  T lambdax = -2./3. * mux;

  k  = tcond_.conductivity( mu );
  T kmu = 0.0;
  tcond_.conductivityJacobian( mu, kmu );
  T kx = kmu*mux;

  //From ViscousFlux...
  tauxx = mu*(2*ux   ) + lambda*(ux);

  T tauxx_x = mux*(2*ux) + mu*(2*uxx) + lambdax*(ux) + lambda*(uxx);

  strongPDE(1) -= tauxx_x;
  strongPDE(2) -= kx*tx + k*txx + ux*tauxx + u*tauxx_x;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDENavierStokes1D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDENavierStokes1D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDENavierStokes1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes1D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  this->gas_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes1D<PDETraitsSize, PDETraitsModel>: visc_ = " << std::endl;
  this->visc_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes1D<PDETraitsSize, PDETraitsModel>: tcond_ = " << std::endl;
  this->tcond_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDENAVIERSTOKES1D_H
