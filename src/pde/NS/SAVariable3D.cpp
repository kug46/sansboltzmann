// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SAVariable3D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<SAVariableType3DParams::VariableOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<SAVariableType3DParams::VariableOptions> > const&) const;

template<class NSVariableParams>
void SAnt3DParams<NSVariableParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.nt));
  NSVariableParams::checkInputs(d);
  // checkUnkownInputs is called int the base class
  //d.checkUnknownInputs(allParams);
}
template<class NSVariableParams>
SAnt3DParams<NSVariableParams> SAnt3DParams<NSVariableParams>::params;

void SAnt3DParams<Conservative3DParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rhont));
  Conservative3DParams::checkInputs(d);
  // checkUnkownInputs is called int the base class
  //d.checkUnknownInputs(allParams);
}
SAnt3DParams<Conservative3DParams> SAnt3DParams<Conservative3DParams>::params;

template struct SAnt3DParams<DensityVelocityPressure3DParams>;
template struct SAnt3DParams<DensityVelocityTemperature3DParams>;

void SAVariableType3DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
SAVariableType3DParams SAVariableType3DParams::params;

}
