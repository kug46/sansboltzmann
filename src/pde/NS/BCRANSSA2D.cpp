// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCRANSSA2D.h"
#include "Q2DPrimitiveRhoPressure.h"
#include "Q2DPrimitiveSurrogate.h"
#include "Q2DEntropy.h"
#include "Q2DConservative.h"
#include "QRANSSA2D.h"
#include "BCRANSSA2D_Solution.h"

#include "TraitsRANSSA.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
// Instantiate BC parameters

// Pressure-primitive variables
typedef BCRANSSA2DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_S )

// Conservative Variables
typedef BCRANSSA2DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Cons_S;
BCPARAMETER_INSTANTIATE( BCVector_Cons_S )

// Entropy Variables
typedef BCRANSSA2DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypeEntropy, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Entropy_S;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_S )

// Surrogate Variables
typedef BCRANSSA2DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Surrogate_S;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_S )


//===========================================================================//
// Repeat for constant viscosity

// Pressure-primitive variables
typedef BCRANSSA2DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_RhoP_C;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_C )

// Conservative Variables
typedef BCRANSSA2DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Cons_C;
BCPARAMETER_INSTANTIATE( BCVector_Cons_C )

// Entropy Variables
typedef BCRANSSA2DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Entropy_C;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_C )

// Surrogate Variables
typedef BCRANSSA2DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Surrogate_C;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_C )

} //namespace SANS
