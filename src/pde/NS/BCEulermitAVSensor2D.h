// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULERARTIFICIALVISCOSITY2D_H
#define BCEULERARTIFICIALVISCOSITY2D_H

// 2-D artificial viscosity BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"

#include "BCEuler2D.h"
#include "BCNavierStokes2D.h"
#include "BCRANSSA2D.h"
#include "PDEEulermitAVDiffusion2D.h"

#include <iostream>
#include <limits>
#include <string>

namespace SANS
{


template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEmitAVSensor2D;

//----------------------------------------------------------------------------//
// BC class: 2-D artificial viscosity
//
// template parameters:
//   BCType               BC type (e.g. BCTypeDirichlet_mitState)
//----------------------------------------------------------------------------//
class BCNone_AV;
class BCTypeSymmetry_mitState;
class BCTypeFullState_mitState;
class BCTypeDirichlet_mitState;
class BCTypeFlux_mitState;
class BCTypeWall_mitState;


template <class BCType, class BCBase>
class BCmitAVSensor2D;

template <class BCType, class BCBaseParams>
struct BCmitAVSensor2DParams;

template <template <class> class PDETraitsSize, class PDETraitsModel>

using BCEulermitAVSensor2DVector = boost::mpl::vector10<
    BCNone<PhysD2,PDETraitsSize<PhysD2>::N>,
    SpaceTimeBC< BCmitAVSensor2D<BCNone_AV, BCEuler2D<BCTypeTimeOut,
                                                  PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> > >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeSymmetry_mitState,
                                                  PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeFullState_mitState,
                                                  PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState,
                                                  PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState,
                                                  PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState,
                                                  PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeReflect_mitState,
                                                  PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState,
                                                  PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeTimeIC_Function,
      PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> > >
  >;

template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCNavierStokesmitAVDiffusion2DVector = boost::mpl::vector7<
    BCNone<PhysD2,PDETraitsSize<PhysD2>::N>,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeSymmetry_mitState,
                                                  PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeFullState_mitState,
                                                   PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState,
                                                          PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState,
                                                          PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState,
                                                          PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState, BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState,
                                                          PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>> >

  >;

template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCRANSSAmitAVDiffusion2DVector = boost::mpl::vector6<
    BCNone<PhysD2,PDETraitsSize<PhysD2>::N>,
    BCmitAVSensor2D<BCTypeFlux_mitState,
                            BCRANSSA2D< BCTypeWall_mitState,
                                        BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState,
                                                          PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState,
                            BCRANSSA2D< BCTypeNeumann_mitState,
                                        BCNavierStokes2D< BCTypeSymmetry_mitState,
                                                          PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState,
                            BCRANSSA2D< BCTypeFullState_mitState,
                                        BCEuler2D< BCTypeFullState_mitState,
                                                   PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState,
                            BCRANSSA2D< BCTypeNeumann_mitState,
                                        BCEuler2D< BCTypeInflowSubsonic_sHqt_mitState,
                                                   PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>>> >,
    BCmitAVSensor2D<BCTypeFlux_mitState,
                            BCRANSSA2D< BCTypeNeumann_mitState,
                                        BCEuler2D< BCTypeOutflowSubsonic_Pressure_mitState,
                                                   PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>>> >
  >;

//----------------------------------------------------------------------------//
// None BC
//----------------------------------------------------------------------------//

template <class BCBase>
class BCmitAVSensor2D<BCNone_AV, BCBase> : public BCBase
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCBase::Category Category;
  typedef typename BCBase::ParamsType ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  using BCBase::pde_;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D, Real> VectorX;
  typedef DLA::VectorS<D+1, Real> VectorXT;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor2D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  ~BCmitAVSensor2D() {}

  BCmitAVSensor2D( const BCmitAVSensor2D& ) = delete;
  BCmitAVSensor2D& operator=( const BCmitAVSensor2D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, qI, qB);
    qB(pde_.iSens) = qI(pde_.iSens);
  }

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, qI, qB);
    qB(pde_.iSens) = qI(pde_.iSens);
  }


  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {

    // Compute the advective flux based on the interior state
    pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn);

    // Compute the viscous flux based on the interior state
    ArrayQ<Tf> f = 0, g = 0;
    pde_.fluxViscous(param, x, y, time, qI, qIx, qIy, f, g);
    Fn += f*nx + g*ny;
#if 1
    Tp H = exp(param); //generalized h-tensor

    VectorX n = {nx, ny};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(pde_.iSens) = sqrt(C2_)*hn*qI(pde_.iSens);
#endif
  }


  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {

    // Compute the advective flux based on the interior state
    pde_.fluxAdvectiveUpwindSpaceTime(param, x, y, time, qI, qB, nx, ny, nt, Fn);

    // Compute the viscous flux based on the interior state
    ArrayQ<Tf> f = 0, g = 0, h = 0;
    pde_.fluxViscousSpaceTime(param, x, y, time, qI, qIx, qIy, qIt, f, g, h);
    Fn += f*nx + g*ny + h*nt;
#if 1
    Tp H = exp(param); //generalized h-tensor

    VectorXT n = {nx, ny, nt};
    Real hn = 0;
    for (int i = 0; i < D+1; i++)
      for (int j = 0; j < D+1; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(pde_.iSens) = sqrt(C2_)*hn*qI(pde_.iSens);
#endif
  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real C1_ = 3.0;
  const Real C2_ = 5.0;
};

//----------------------------------------------------------------------------//
// Conventional PX-style slipwall BC:
//
// specify: normal velocity
//----------------------------------------------------------------------------//


template <class BCBase>
class BCmitAVSensor2D<BCTypeSymmetry_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCBase::Category Category;
  typedef typename BCBase::ParamsType ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor2D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  ~BCmitAVSensor2D() {}

  BCmitAVSensor2D( const BCmitAVSensor2D& ) = delete;
  BCmitAVSensor2D& operator=( const BCmitAVSensor2D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, qI, qB);
    qB(iSens) = qI(iSens); //evaluate sensor variable from interior
  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
};

//----------------------------------------------------------------------------//
// Conventional PX-style Full State
//----------------------------------------------------------------------------//
template<class BCBaseParams>
struct BCmitAVSensor2DParams<BCTypeFullState_mitState, BCBaseParams> : public BCBaseParams
{
  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    BCBaseParams::checkInputs(d);
//    d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor2DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor2DParams<BCTypeFullState_mitState, BCBaseParams> BCmitAVSensor2DParams<BCTypeFullState_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor2D<BCTypeFullState_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCBase::Category Category;
  typedef BCmitAVSensor2DParams<BCTypeFullState_mitState, typename BCBase::ParamsType> ParamsType;
//  typedef typename BCBase::PDE PDE;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  using BCBase::pde_;
  using BCBase::upwind_;
  using BCBase::hasFluxViscous;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor2D( const Real& sensor, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  BCmitAVSensor2D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d) {}

  ~BCmitAVSensor2D() {}

  BCmitAVSensor2D( const BCmitAVSensor2D& ) = delete;
  BCmitAVSensor2D& operator=( const BCmitAVSensor2D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, qI, qB);
    qB(iSens) = qI(iSens);
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if ( upwind_ )
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn);
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<Tf> fx = 0, fy = 0;
      pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

      Fn += fx*nx + fy*ny;
    }
  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;
};


//----------------------------------------------------------------------------//
// Conventional PX-style Robin BC for sensor
//----------------------------------------------------------------------------//

template<class BCBaseParams>
struct BCmitAVSensor2DParams<BCTypeFlux_mitState, BCBaseParams> : public BCBaseParams
{
  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    BCBaseParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor2DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor2DParams<BCTypeFlux_mitState, BCBaseParams> BCmitAVSensor2DParams<BCTypeFlux_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor2D<BCTypeFlux_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCBase::Category>::value, "BC category must match that of base class!");

  typedef BCmitAVSensor2DParams<BCTypeFlux_mitState, typename BCBase::ParamsType> ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  static const int NBC = BCBase::NBC+1; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D, Real> VectorX;
  typedef DLA::VectorS<D+1, Real> VectorXT;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor2D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  BCmitAVSensor2D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d) {}

  ~BCmitAVSensor2D() {}

  BCmitAVSensor2D( const BCmitAVSensor2D& ) = delete;
  BCmitAVSensor2D& operator=( const BCmitAVSensor2D& ) = delete;

  // BC state
  template <int Dim, class T>
  void state( const DLA::MatrixSymS<Dim,Real>& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, qI, qB);
    qB(iSens) = qI(iSens);
  }

  // BC state
  template <int Dim, class T>
  void state( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(param.right(), x, y, time, nx, ny, qI, qB);
    qB(iSens) = qI(iSens);
  }

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, nt, qI, qB);
    qB(iSens) = qI(iSens);
  }

  // normal BC flux
  template <int Dim, class Tq, class Tg, class Tf>
  void fluxNormal( const DLA::MatrixSymS<Dim,Real>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<Tq>& qI,
                   const ArrayQ<Tg>& qIx, const ArrayQ<Tg>& qIy,
                   const ArrayQ<Tq>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    BCBase::fluxNormal(param, x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);

    // SENSOR

    // Compute the viscous flux
    //ArrayQ<T> fv = 0, gv = 0;
    //pde_.fluxViscous(param, x, y, time, qI, qIx, qIy, fv, gv);
    //Fn(pde_.iSens) = fv(pde_.iSens)*nx + gv(pde_.iSens)*ny;

    DLA::MatrixSymS<Dim,Real> H = exp(param); //generalized h-tensor

    VectorX n = {nx, ny};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(iSens) = sqrt(C2_)*hn*qI(iSens);
  }

  // normal BC flux
  template <int Dim, class Tq, class Tg, class Tf>
  void fluxNormal( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<Tq>& qI,
                   const ArrayQ<Tg>& qIx, const ArrayQ<Tg>& qIy,
                   const ArrayQ<Tq>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    BCBase::fluxNormal(param, x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);

    // SENSOR

    // Compute the viscous flux
    //ArrayQ<T> fv = 0, gv = 0;
    //pde_.fluxViscous(param, x, y, time, qI, qIx, qIy, fv, gv);
    //Fn(pde_.iSens) = fv(pde_.iSens)*nx + gv(pde_.iSens)*ny;
    DLA::MatrixSymS<Dim,Real> logH = param.left();
    DLA::MatrixSymS<Dim,Real> H = exp(logH); //generalized h-tensor

    VectorX n = {nx, ny};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(iSens) = sqrt(C2_)*hn*qI(iSens);
  }

  // normal BC flux
  template <class Tp, class T>
  void fluxNormalSpaceTime( const Tp& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCBase::fluxNormalSpaceTime(param, x, y, time, nx, ny, nt, qI, qIx, qIy, qIt, qB, Fn);

    // SENSOR

    // Compute the viscous flux
    //ArrayQ<T> fv = 0, gv = 0;
    //pde_.fluxViscous(param, x, y, time, qI, qIx, qIy, fv, gv);
    //Fn(pde_.iSens) = fv(pde_.iSens)*nx + gv(pde_.iSens)*ny;

    Tp H = exp(param); //generalized h-tensor

    VectorXT n = {nx, ny, nt};
    Real hn = 0;
    for (int i = 0; i < D+1; i++)
      for (int j = 0; j < D+1; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(iSens) = sqrt(C2_)*hn*qI(iSens);
  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real C1_ = 3.0;
  const Real C2_ = 5.0;
  using BCBase::pde_;
};

template <class BCBase>
void
BCmitAVSensor2D<BCTypeFlux_mitState, BCBase>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCmitAVSensor2D<BCTypeFlux_mitState, BCBase>" << std::endl;
}


//----------------------------------------------------------------------------//
// Wall_mitState BC : sensor value is taken from interior
//----------------------------------------------------------------------------//

template <class BCBase>
class BCmitAVSensor2D<BCTypeWall_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCBase::Category>::value, "BC category must match that of base class!");

  typedef typename BCBase::ParamsType ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D, Real> VectorX;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor2D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  BCmitAVSensor2D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d) {}

  ~BCmitAVSensor2D() {}

  BCmitAVSensor2D( const BCmitAVSensor2D& ) = delete;
  BCmitAVSensor2D& operator=( const BCmitAVSensor2D& ) = delete;

  // BC state
  template <int Dim, class T>
  void state( const DLA::MatrixSymS<Dim,Real>& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, qI, qB);
    qB(iSens) = 0; //qI(iSens); //this should be *after* the call to the base class's state function
  }

  // BC state
  template <int Dim, class T>
  void state( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(param.right(), x, y, time, nx, ny, qI, qB);
    qB(iSens) = 0; //qI(iSens); //this should be *after* the call to the base class's state function
  }

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, y, time, nx, ny, nt, qI, qB);
    qB(iSens) = 0; //qI(iSens); //this should be *after* the call to the base class's state function
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const ParamTuple<DLA::MatrixSymS<D,Tp>, Real, TupleClass<>>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0, fy = 0, fz = 0;
    pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

    Fn += fx*nx + fy*ny;

    // Compute the viscous flux
    ArrayQ<T> fv = 0, gv = 0;
    pde_.fluxViscous(param, x, y, time, qB, qIx, qIy, fv, gv);

    Fn += fv*nx + gv*ny;

    // No energy flux at wall
    Fn[pde_.iEngy] = 0;

#if 0
    // SENSOR
    DLA::MatrixSymS<D,Tp> H = exp(get<0>(param)); //generalized h-tensor
    SANS::DLA::VectorS<D,Tp> hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = min(hPrincipal[0], min(hPrincipal[1], hPrincipal[2]));

    VectorX n = {nx, ny, nz};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    T lambda = 0;
    pde_.speedCharacteristic( param.right(), x, y, z, time, qI, lambda );

    Real lenfact = sqrt(C1_*C2_*Real(pde_.getOrder())/hmin)*hn;

    Fn[pde_.iSens] = lenfact*lambda*qI[pde_.iSens];
#endif
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;
protected:
  const Real C1_ = 3.0;
  const Real C2_ = 5.0;
  using BCBase::pde_;
};


//----------------------------------------------------------------------------//
// Conventional PX-style Dirichlet BC for sensor
//----------------------------------------------------------------------------//

template<class BCBaseParams>
struct BCmitAVSensor2DParams<BCTypeDirichlet_mitState, BCBaseParams> : public BCBaseParams
{
  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    BCBaseParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor2DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor2DParams<BCTypeDirichlet_mitState, BCBaseParams> BCmitAVSensor2DParams<BCTypeDirichlet_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor2D<BCTypeDirichlet_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCBase::Category Category;
  typedef BCmitAVSensor2DParams<BCTypeDirichlet_mitState, typename BCBase::ParamsType> ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  static const int NBC = BCBase::NBC+1; // total BCs

  static const int iSens = N - 1;

  using BCBase::pde_;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor2D( const Real& sensor, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  BCmitAVSensor2D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d) {}

  ~BCmitAVSensor2D() {}

  BCmitAVSensor2D( const BCmitAVSensor2D& ) = delete;
  BCmitAVSensor2D& operator=( const BCmitAVSensor2D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    // Zero out any possible stabilization
    Fn = 0;

    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0, fy = 0;
    pde_.fluxAdvective(param, x, y, time, qB, fx, fy);
//    pde_.fluxViscous(param, x, y, time, qB, qIx, qIy, fx, fy); //Not sure if viscous flux should be included here

    Fn += fx*nx + fy*ny;
  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
};

// BC state
template <class BCBase>
template <class Tp, class T>
inline void
BCmitAVSensor2D<BCTypeDirichlet_mitState, BCBase>::
state( const Tp& param,
       const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  BCBase::state(x, y, time, nx, ny, qI, qB);
  qB(iSens) = qI(iSens);
}


template <class BCBase>
void
BCmitAVSensor2D<BCTypeDirichlet_mitState, BCBase>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCmitAVSensor2D<BCTypeDirichlet_mitState, BCBase>: " << std::endl;
}

} //namespace SANS

#endif  // BCEULERARTIFICIALVISCOSITY2D_H
