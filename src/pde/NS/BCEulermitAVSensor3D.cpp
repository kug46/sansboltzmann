// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Q3DPrimitiveRhoPressure.h"
#include "Q3DPrimitiveSurrogate.h"
//#include "Q3DEntropy.h"
#include "Q3DConservative.h"

#include "QRANSSA3D.h"

#include "TraitsEulerArtificialViscosity.h"
#include "TraitsRANSSAArtificialViscosity.h"
#include "BCEulermitAVSensor3D.h"

#include "Fluids3D_Sensor.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux3D.h"
#include "pde/ArtificialViscosity/AVSensor_Source3D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor3D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
// Instantiate BC parameters

typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFluxEuler;
typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFluxEuler;


// Pressure-primitive variables
typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> PDEBaseRhoPressure;
typedef AVSensor_Source3D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD3, PDEBaseRhoPressure>> SensorSourceRhoPressure;
typedef TraitsModelArtificialViscosity<PDEBaseRhoPressure,
                                       SensorAdvectiveFluxEuler,
                                       SensorViscousFluxEuler,
                                       SensorSourceRhoPressure> TraitsModelAVRhoPressure;

typedef BCEulermitAVSensor3DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelAVRhoPressure > BCVector_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_S )

// Pressure gradient sensor
//typedef PDEEulermitAVDiffusion3D<TraitsSizeEulerArtificialViscosity,
//                                       TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> PDEBaseRhoPressure;
typedef AVSensor_Source3D_PressureGrad<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD3, PDEBaseRhoPressure>> SensorGradPSourceRhoPressure;
typedef TraitsModelArtificialViscosity<PDEBaseRhoPressure,
                                       SensorAdvectiveFluxEuler,
                                       SensorViscousFluxEuler,
                                       SensorGradPSourceRhoPressure> TraitsModelGradPAVRhoPressure;

typedef BCEulermitAVSensor3DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelGradPAVRhoPressure > BCVector_GradP_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_GradP_RhoP_S )

/// RANS-SA --------------------------
typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFluxRANSSA;
typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFluxRANSSA;

typedef QTypePrimitiveRhoPressure QType;

// Constant viscosity model and gradP sensor
typedef ViscosityModel_Const ViscosityModelType_Const;
typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType_Const, ThermalConductivityModel> TraitsModelRANSSAClass_Const;
typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass_Const, PDERANSSA3D> PDEBaseClass_Const;


typedef Fluids_Sensor<PhysD3, PDEBaseClass_Const> Sensor_Const;
typedef AVSensor_Source3D_PressureGrad<TraitsSizeRANSSAArtificialViscosity, Sensor_Const> SensorSourceGradP_Const;
//typedef AVSensor_Source3D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass_Const,
                                       SensorAdvectiveFluxRANSSA,
                                       SensorViscousFluxRANSSA,
                                       SensorSourceGradP_Const> TraitsModelAV_GradP_Const;

// Pressure-primitive variables
typedef BCRANSSAmitAVDiffusion3DVector<
            TraitsSizeRANSSAArtificialViscosity, TraitsModelAV_GradP_Const> BCVector_RANSSA_RhoP_GradP_Const_S;
BCPARAMETER_INSTANTIATE( BCVector_RANSSA_RhoP_GradP_Const_S )

// Sutherland viscosity model and jump sensor
typedef ViscosityModel_Sutherland ViscosityModelType;
typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType, ThermalConductivityModel> TraitsModelRANSSAClass;
typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA3D> PDEBaseClass;


typedef Fluids_Sensor<PhysD3, PDEBaseClass> Sensor;
//typedef AVSensor_Source3D_PressureGrad<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;
typedef AVSensor_Source3D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass, SensorAdvectiveFluxRANSSA, SensorViscousFluxRANSSA, SensorSource> TraitsModelAV;

// Pressure-primitive variables
typedef BCRANSSAmitAVDiffusion3DVector<
            TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> BCVector_RANSSA_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RANSSA_RhoP_S )

// Sutherland Viscosity model
typedef AVSensor_AdvectiveFlux3D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFluxRANSSA;
typedef AVSensor_ViscousFlux3D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFluxRANSSA;

typedef QTypePrimitiveRhoPressure QType;

// Constant viscosity model and gradP sensor
typedef ViscosityModel_Sutherland ViscosityModelType_Suth;
typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType_Suth, ThermalConductivityModel> TraitsModelRANSSAClass_Suth;
typedef PDEEulermitAVDiffusion3D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass_Suth, PDERANSSA3D> PDEBaseClass_Suth;


typedef Fluids_Sensor<PhysD3, PDEBaseClass_Suth> Sensor_Suth;
typedef AVSensor_Source3D_PressureGrad<TraitsSizeRANSSAArtificialViscosity, Sensor_Suth> SensorSourceGradP_Suth;
//typedef AVSensor_Source3D_Jump<TraitsSizeRANSSAArtificialViscosity, Sensor> SensorSource;

typedef TraitsModelArtificialViscosity<PDEBaseClass_Suth,
                                       SensorAdvectiveFluxRANSSA,
                                       SensorViscousFluxRANSSA,
                                       SensorSourceGradP_Suth> TraitsModelAV_GradP_Suth;

typedef BCRANSSAmitAVDiffusion3DVector<
            TraitsSizeRANSSAArtificialViscosity, TraitsModelAV_GradP_Suth> BCVector_RANSSA_RhoP_GradP_Suth_S;
BCPARAMETER_INSTANTIATE( BCVector_RANSSA_RhoP_GradP_Suth_S )

} //namespace SANS
