// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "QRANSSA3D.h"
#include "Q3DPrimitiveRhoPressure.h"
//#include "Q3DPrimitiveSurrogate.h"
#include "Q3DConservative.h"
//#include "Q3DEntropy.h"
#include "TraitsRANSSA.h"

namespace SANS
{
template class QRANSSA3D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA>;
//template class QRANSSA3D<QTypePrimitiveSurrogate, TraitsSizeRANSSA>;
template class QRANSSA3D<QTypeConservative, TraitsSizeRANSSA>;
//template class QRANSSA3D<QTypeEntropy, TraitsSizeRANSSA>;
}
