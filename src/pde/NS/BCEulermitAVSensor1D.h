// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULERARTIFICIALVISCOSITY1D_H
#define BCEULERARTIFICIALVISCOSITY1D_H

// 2-D artificial viscosity BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"

#include "BCEuler1D.h"
#include "PDEEulermitAVDiffusion1D.h"

#include <iostream>
#include <limits>
#include <string>

namespace SANS
{


template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEmitAVSensor1D;

//----------------------------------------------------------------------------//
// BC class: 2-D artificial viscosity
//
// template parameters:
//   BCType               BC type (e.g. BCTypeDirichlet_mitState)
//----------------------------------------------------------------------------//
class BCNone_AV;
class BCTypeReflect_mitState;
class BCTypeFullState_mitState;
class BCTypeDirichlet_mitState;
class BCTypeFlux_mitState;

template <class BCType, class BCBase>
class BCmitAVSensor1D;

template <class BCType, class BCBaseParams>
struct BCmitAVSensor1DParams;

template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCEulermitAVSensor1DVector = boost::mpl::vector15<
    BCNone<PhysD1,PDETraitsSize<PhysD1>::N>,
    SpaceTimeBC< BCmitAVSensor1D<BCNone_AV, BCEuler1D<BCTypeTimeOut,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> > >,
    BCmitAVSensor1D<BCTypeReflect_mitState, BCEuler1D<BCTypeReflect_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeFullState_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC<  BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeFullStateSpaceTime_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> > >,
    SpaceTimeBC< BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeFunction_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> > >,
    BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeInflowSubsonic_PtTt_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC<  BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> > >,
    BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeOutflowSubsonic_Pressure_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeOutflowSupersonic_Pressure_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> >,
    BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCmitAVSensor1D<BCTypeFlux_mitState,  BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> > >,
    SpaceTimeBC< BCmitAVSensor1D<BCTypeFlux_mitState,  BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> > >,
    SpaceTimeBC< BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeTimeIC,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> > >,
    SpaceTimeBC< BCmitAVSensor1D<BCTypeFlux_mitState, BCEuler1D<BCTypeTimeIC_Function,
      PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>> > >
  >;

//----------------------------------------------------------------------------//
// None BC
//----------------------------------------------------------------------------//


template <class BCBase>
class BCmitAVSensor1D<BCNone_AV, BCBase> : public BCBase
{
public:
  typedef PhysD1 PhysDim;
  typedef typename BCBase::Category Category;
  typedef typename BCBase::ParamsType ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  using BCBase::pde_;

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D, Real> VectorX;
  typedef DLA::VectorS<D+1, Real> VectorXT;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor1D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  ~BCmitAVSensor1D() {}

  BCmitAVSensor1D( const BCmitAVSensor1D& ) = delete;
  BCmitAVSensor1D& operator=( const BCmitAVSensor1D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qI;
  }

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qI;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                   const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {

    // Compute the advective flux based on the interior state
    pde_.fluxAdvectiveUpwindSpaceTime(param, x, time, qI, qB, nx, nt, Fn);
    ArrayQ<T> fv = 0; ArrayQ<T> gv = 0;
    pde_.fluxViscousSpaceTime(param, x, time, qB, qIx, qIt, fv, gv );

    Fn += fv*nx + gv*nt;

    Tp H = exp(param); //generalized h-tensor

    VectorXT n = {nx, nt};
    Real hn = 0;
    for (int i = 0; i < D+1; i++)
      for (int j = 0; j < D+1; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(pde_.iSens) = sqrt(C2_)*hn*qI(pde_.iSens);

  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {

    // Compute the advective flux based on the interior state
    pde_.fluxAdvectiveUpwind(param, x, time, qI, qB, nx, Fn);
    ArrayQ<T> fv = 0;
    pde_.fluxViscous(param, x, time, qB, qIx, fv );

    Fn += fv*nx;

    Tp H = exp(param); //generalized h-tensor

    VectorX n = {nx};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(pde_.iSens) = sqrt(C2_)*hn*qI(pde_.iSens);

  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real C1_ = 3.0;
  const Real C2_ = 5.0;
};

//----------------------------------------------------------------------------//
// Conventional PX-style slipwall BC:
//
// specify: normal velocity
//----------------------------------------------------------------------------//


template <class BCBase>
class BCmitAVSensor1D<BCTypeReflect_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD1 PhysDim;
  typedef typename BCBase::Category Category;
  typedef typename BCBase::ParamsType ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor1D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  ~BCmitAVSensor1D() {}

  BCmitAVSensor1D( const BCmitAVSensor1D& ) = delete;
  BCmitAVSensor1D& operator=( const BCmitAVSensor1D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, time, nx, qI, qB);
    qB(iSens) = qI(iSens); //evaluate sensor variable from interior
  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
};

//----------------------------------------------------------------------------//
// Conventional PX-style Full State
//----------------------------------------------------------------------------//
template<class BCBaseParams>
struct BCmitAVSensor1DParams<BCTypeFullState_mitState, BCBaseParams> : public BCBaseParams
{
  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    BCBaseParams::checkInputs(d);
//    d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor1DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor1DParams<BCTypeFullState_mitState, BCBaseParams> BCmitAVSensor1DParams<BCTypeFullState_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor1D<BCTypeFullState_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD1 PhysDim;
  typedef typename BCBase::Category Category;
  typedef BCmitAVSensor1DParams<BCTypeFullState_mitState, typename BCBase::ParamsType> ParamsType;
//  typedef typename BCBase::PDE PDE;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  using BCBase::NBC; // total BCs

  static const int iSens = N - 1;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  using BCBase::pde_;
  using BCBase::upwind_;
  using BCBase::hasFluxViscous;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor1D( const Real& sensor, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  BCmitAVSensor1D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d) {}

  ~BCmitAVSensor1D() {}

  BCmitAVSensor1D( const BCmitAVSensor1D& ) = delete;
  BCmitAVSensor1D& operator=( const BCmitAVSensor1D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, time, nx, qI, qB);
    qB(iSens) = qI(iSens);
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if ( upwind_ )
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind(param, x, time, qI, qB, nx, Fn);
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<Tf> fx = 0;
      pde_.fluxAdvective(param, x,time, qB, fx);

      Fn += fx*nx;
    }
  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;
};


//----------------------------------------------------------------------------//
// Conventional PX-style Robin BC for sensor
//----------------------------------------------------------------------------//

template<class BCBaseParams>
struct BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBaseParams> : public BCBaseParams
{
  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    BCBaseParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor1DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBaseParams> BCmitAVSensor1DParams<BCTypeFlux_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor1D<BCTypeFlux_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD1 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCBase::Category>::value, "BC category must match that of base class!");

  typedef BCmitAVSensor1DParams<BCTypeFlux_mitState, typename BCBase::ParamsType> ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  static const int NBC = BCBase::NBC+1; // total BCs

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  typedef DLA::VectorS<D, Real> VectorX;
  typedef DLA::VectorS<D+1, Real> VectorXT;

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor1D( BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  BCmitAVSensor1D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d) {}

  ~BCmitAVSensor1D() {}

  BCmitAVSensor1D( const BCmitAVSensor1D& ) = delete;
  BCmitAVSensor1D& operator=( const BCmitAVSensor1D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, time, nx, qI, qB);
    qB(pde_.iSens) = qI(pde_.iSens);
  }

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCBase::state(x, time, nx, nt, qI, qB);
    qB(pde_.iSens) = qI(pde_.iSens);
  }

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCBase::fluxNormal(param, x, time, nx, qI, qIx, qB, Fn);

    Tp H = exp(param); //generalized h-tensor

    VectorX n = {nx};
    Real hn = 0;
    for (int i = 0; i < D; i++)
      for (int j = 0; j < D; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(pde_.iSens) = sqrt(C2_)*hn*qI(pde_.iSens);
  }

  // normal BC flux
  template <class Tp, class T>
  void fluxNormalSpaceTime( const Tp& param,
                   const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCBase::fluxNormalSpaceTime(param, x, time, nx, nt, qI, qIx, qIt, qB, Fn);

//    ArrayQ<T> fv = 0; ArrayQ<T> gv = 0;
//    pde_.fluxViscousSpaceTime(param, x, time, qB, qIx, qIt, fv, gv );

//    Fn += fv*nx + gv*nt;

    Tp H = exp(param); //generalized h-tensor

    VectorXT n = {nx, nt};
    Real hn = 0;
    for (int i = 0; i < D+1; i++)
      for (int j = 0; j < D+1; j++)
        hn += n[i]*H(i,j)*n[j];

    Fn(pde_.iSens) = sqrt(C2_)*hn*qI(pde_.iSens);
  }


  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const Real C1_ = 3.0;
  const Real C2_ = 5.0;
  using BCBase::pde_;
};

template <class BCBase>
void
BCmitAVSensor1D<BCTypeFlux_mitState, BCBase>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCmitAVSensor1D<BCTypeFlux_mitState, BCBase>" << std::endl;
}


//----------------------------------------------------------------------------//
// Conventional PX-style Dirichlet BC for sensor
//----------------------------------------------------------------------------//

template<class BCBaseParams>
struct BCmitAVSensor1DParams<BCTypeDirichlet_mitState, BCBaseParams> : public BCBaseParams
{
  using BCBaseParams::BCName;
  typedef typename BCBaseParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    BCBaseParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCmitAVSensor1DParams params;
};
// Instantiate the singleton
template<class BCBaseParams>
BCmitAVSensor1DParams<BCTypeDirichlet_mitState, BCBaseParams> BCmitAVSensor1DParams<BCTypeDirichlet_mitState, BCBaseParams>::params;


template <class BCBase>
class BCmitAVSensor1D<BCTypeDirichlet_mitState, BCBase> : public BCBase
{
public:
  typedef PhysD1 PhysDim;
  typedef typename BCBase::Category Category;
  typedef BCmitAVSensor1DParams<BCTypeDirichlet_mitState, typename BCBase::ParamsType> ParamsType;

  using BCBase::D;   // physical dimensions
  using BCBase::N;   // total solution variables

  static const int NBC = BCBase::NBC+1; // total BCs

  static const int iSens = N - 1;

  using BCBase::pde_;

  template <class T>
  using ArrayQ = typename BCBase::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename BCBase::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCmitAVSensor1D( const Real& sensor, BCArgs&&... args ) :
    BCBase(std::forward<BCArgs>(args)...) {}

  BCmitAVSensor1D( const typename BCBase::PDE& pde, const PyDict& d ) :
    BCBase(pde, d) {}

  ~BCmitAVSensor1D() {}

  BCmitAVSensor1D( const BCmitAVSensor1D& ) = delete;
  BCmitAVSensor1D& operator=( const BCmitAVSensor1D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    // Zero out any possible stabilization
    Fn = 0;

    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective(param, x, time, qB, fx);

    Fn += fx*nx;
  }

  // is the boundary state valid
  using BCBase::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
};

// BC state
template <class BCBase>
template <class Tp, class T>
inline void
BCmitAVSensor1D<BCTypeDirichlet_mitState, BCBase>::
state( const Tp& param,
       const Real& x, const Real& time,
       const Real& nx,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  BCBase::state(x, time, nx, qI, qB);
  qB(iSens) = qI(iSens);
}


template <class BCBase>
void
BCmitAVSensor1D<BCTypeDirichlet_mitState, BCBase>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCmitAVSensor1D<BCTypeDirichlet_mitState, BCBase>: " << std::endl;
}

} //namespace SANS

#endif  // BCEULERARTIFICIALVISCOSITY1D_H
