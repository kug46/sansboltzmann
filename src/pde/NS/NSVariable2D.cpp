// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "NSVariable2D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<NSVariableType2DParams::VariableOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<NSVariableType2DParams::VariableOptions> > const&) const;

void DensityVelocityPressure2DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.v));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
DensityVelocityPressure2DParams DensityVelocityPressure2DParams::params;

void DensityVelocityTemperature2DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.v));
  allParams.push_back(d.checkInputs(params.t));
  d.checkUnknownInputs(allParams);
}
DensityVelocityTemperature2DParams DensityVelocityTemperature2DParams::params;

void Conservative2DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.rhou));
  allParams.push_back(d.checkInputs(params.rhov));
  allParams.push_back(d.checkInputs(params.rhoE));
  d.checkUnknownInputs(allParams);
}
Conservative2DParams Conservative2DParams::params;


void NSVariableType2DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
NSVariableType2DParams NSVariableType2DParams::params;

}
