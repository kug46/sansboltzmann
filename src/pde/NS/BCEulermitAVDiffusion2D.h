// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEEULERMITAVDIFFUSION2D_H
#define PDEEULERMITAVDIFFUSION2D_H

// 2-D Euler BC class
#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "TraitsEuler.h"
#include "PDEEulermitAVDiffusion2D.h"
#include "SolutionFunction_Euler2D.h"
#include "SolutionFunction_NavierStokes2D.h"

#include "BCEuler2D.h"
#include "BCEuler2D_mitState.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeTimeOut;
class BCTypeTimeIC;

class BCTypeFullState_mitState;

template <class BCType, class PDEEulermitAVDiffusion2D>
class BCEuler2D;

template <class BCType>
struct BCEuler2DParams;

template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCEulermitAVDiffusion2DVector = boost::mpl::vector8<
    BCNone<PhysD2,PDETraitsSize<PhysD2>::N>,
    BCEuler2D<BCTypeSymmetry_mitState,                   PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState,   PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeFullState_mitState,                  PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState,       PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState,        PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeReflect_mitState,                    PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel>>
                                          >;

} //namespace SANS

#endif  // PDEEULERMITAVDIFFUSION2D_H
