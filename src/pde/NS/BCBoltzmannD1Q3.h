// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBOLTZMANND1Q3_H
#define BCBOLTZMANND1Q3_H

// 1-D Euler BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsBoltzmannD1Q3.h"
#include "PDEBoltzmannD1Q3.h"
//#include "PDEBoltzmannD1Q3_Source.h"
//#include "PDEEulermitAVDiffusion1D.h"
//#include "SolutionFunction_Euler1D.h"

#include "BCBoltzmannD1Q3_BounceBackDirichlet.h"
//#include "BCBoltzmannD1Q3_mitState_ST.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

//class BCTypeNone;
class BCTypeTimeOut;
class BCTypeReflect;
class BCTypeInflowSupersonic;
class BCTypeOutflowSubsonic_Pressure;

template <class BCType, class PDEBoltzmannD1Q3>
class BCBoltzmannD1Q3;

template <class BCType>
struct BCBoltzmannD1Q3Params;


template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCBoltzmannD1Q3Vector = boost::mpl::vector3<
    BCBoltzmannD1Q3<BCTypeNone                             , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
//    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeTimeOut             , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>> >,
//    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeTimeIC_Function     , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>> >,
//    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeTimeIC,               PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>> >,
//    BCBoltzmannD1Q3<BCTypeReflect                          , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
//    BCBoltzmannD1Q3<BCTypeInflowSupersonic                 , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
//    BCBoltzmannD1Q3<BCTypeOutflowSubsonic_Pressure         , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
//    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeFunction_mitState                , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>> >,
    BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf0              , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf2              , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>
//    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeFullStateSpaceTime_mitState , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>> >,
//    BCBoltzmannD1Q3<BCTypeOutflowSubsonic_Pressure_mitState, PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
//    BCBoltzmannD1Q3<BCTypeOutflowSupersonic_Pressure_mitState, PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
//    BCBoltzmannD1Q3<BCTypeOutflowSupersonic_PressureFunction_mitState, PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
//    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>> >,
//    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>> >,
//    BCBoltzmannD1Q3<BCTypeReflect_mitState                 , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
//    BCBoltzmannD1Q3<BCTypeInflowSubsonic_PtTt_mitState     , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>,
//    SpaceTimeBC<BCBoltzmannD1Q3<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState     , PDEBoltzmannD1Q3<PDETraitsSize, PDETraitsModel>>>
                                                 >;

#if 0
template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCBoltzmannD1Q3_ArtificialViscosityVector = boost::mpl::vector14<
    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeNone                , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeTimeIC,               PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeTimeIC_Function     , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    BCBoltzmannD1Q3<BCTypeReflect                          , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmannD1Q3<BCTypeInflowSupersonic                 , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmannD1Q3<BCTypeOutflowSubsonic_Pressure         , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeFunction_mitState     , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    BCBoltzmannD1Q3<BCTypeFullState_mitState               , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> ,
    SpaceTimeBC< BCBoltzmannD1Q3<BCTypeFullStateSpaceTime_mitState , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    BCBoltzmannD1Q3<BCTypeOutflowSubsonic_Pressure_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmannD1Q3<BCTypeOutflowSupersonic_Pressure_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmannD1Q3<BCTypeOutflowSupersonic_PressureFunction_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC<BCBoltzmannD1Q3<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC<BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>> >,
    BCBoltzmannD1Q3<BCTypeReflect_mitState                 , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmannD1Q3<BCTypeInflowSubsonic_PtTt_mitState     , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>,
    SpaceTimeBC<BCBoltzmannD1Q3<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState     , PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>>>
                                          >;
#endif


} //namespace SANS

#endif  // BCBOLTZMANND1Q3_H
