// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GASMODEL_H
#define GASMODEL_H

//  ideal gas law, molecular and thermal conductivity laws

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // sqrt
#include <iostream>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Surreal/PromoteSurreal.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// A class for representing input parameters for the GasModel
struct GasModelParams : noncopyable
{
  const ParameterNumeric<Real> gamma{"gamma", 1.4    , 1, 5./3.   , "Specific heat ratio"};
  const ParameterNumeric<Real> R    {"R"    , 287.04 , 0, NO_LIMIT, "Gas Constant"};

  static void checkInputs(PyDict d);

  static GasModelParams params;
};

//----------------------------------------------------------------------------//
// GasModel:  ideal gas law with constant specific heat ratio
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .gamma                   specific heat ratio
//   .R                       gas constant
//   .Cv                      specific heat at constant volume
//   .Cp                      specific heat at constant pressure
//
//   .pressure                static pressure: p(rho, t)
//   .pressureJacobian        jacobian: d(p)/d(rho, t)
//   .energy                  static internal energy: e(rho, t)
//   .energyJacobian          jacobian: d(e)/d(rho, t)
//   .enthalpy                static enthalpy: h(rho, t)
//   .enthalpyJacobian        jacobian: d(h)/d(rho, t)
//   .speedofSound            speed of sound: c
//   .speedofSoundJacobian    jacobian: d(c)/d(t)
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//

class GasModel
{
public:
  GasModelParams& params;

  explicit GasModel( const PyDict& d ) :
      params(GasModelParams::params),
      gamma_(d.get(params.gamma)),
      R_(d.get(params.R)) {}

  GasModel( Real gamma, Real R ) :
      params(GasModelParams::params),
      gamma_(gamma),
      R_(R)
  {
    SANS_ASSERT( gamma >= 1 && gamma <= 5./3. );
    SANS_ASSERT( R > 0 );
  }
  GasModel( const GasModel& gas ) :
      params( GasModelParams::params ),
      gamma_(gas.gamma_),
      R_(gas.R_) {}
  ~GasModel() {}

  Real gamma() const { return gamma_; }
  Real R() const { return R_; }
  Real Cv() const { return R_/(gamma_ - 1); }
  Real Cp() const { return gamma_*R_/(gamma_ - 1); }

  // density
  template<class T>
  T density( const T& p, const T& t ) const;
  template<class T>
  void densityJacobian( const T& p, const T& t, T& rho_p, T& rho_t ) const;
  template<class T>
  void densityGradient( const T& p, const T& t, const T& px, const T& tx, T& rhox ) const;

  // thermodynamic equation of state: static pressure
  template<class T>
  T pressure( const T& rho, const T& t ) const;
  template<class T>
  void pressureJacobian( const T& rho, const T& t, T& p_rho, T& p_t ) const;
  template<class Tq, class Tg>
  void pressureGradient( const Tq& rho, const Tq& t,
                         const Tg& rhox, const Tg& tx,
                         typename promote_Surreal<Tq,Tg>::type& px ) const;

  // caloric equation of state: static internal energy
  template<class T>
  T energy( const T& rho, const T& t ) const;
  template<class T>
  void energyJacobian( const T& rho, const T& t, T& e_rho, T& e_t ) const;
  template<class T>
  void energyGradient( const T& rho, const T& t, const T& rhox, const T& tx, T& ex ) const;

  // static enthalpy
  template<class T>
  T enthalpy( const T& rho, const T& t ) const;
  template<class T>
  void enthalpyJacobian( const T& rho, const T& t, T& h_rho, T& h_t ) const;
  template<class Tq, class Tg>
  void enthalpyGradient( const Tq& rho, const Tq& t,
                         const Tg& rhox, const Tg& tx,
                         typename promote_Surreal<Tq,Tg>::type& hx ) const;

  // caloric equation of state: static internal energy
  template<class T>
  T temperature( const T& rho, const T& e ) const;
  template<class T>
  void temperatureJacobian( const T& rho, const T& e, T& t_rho, T& t_e ) const;
  template<class T>
  void temperatureGradient( const T& rho, const T& e, const T& rhox, const T& ex, T& tx ) const;

  // speed of sound
  template<class T>
  T speedofSound( const T& t ) const;
  template<class T>
  void speedofSoundJacobian( const T& t, T& c_t ) const;

  void dump( std::string indent, std::ostream& out = std::cout ) const;
  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  Real gamma_;                // specific heat ratio
  Real R_;                    // gas constant
};


template <class T>
inline T
GasModel::density( const T& p, const T& t ) const
{
  T rho = p/(R_*t);
  return rho;
}


template <class T>
inline void
GasModel::densityJacobian( const T& p, const T& t, T& rho_p, T& rho_t ) const
{
  rho_p = 1/(R_*t);
  rho_t = -p/(R_*t*t);
}


template <class T>
inline void
GasModel::densityGradient( const T& p, const T& t,
                           const T& px, const T& tx, T& rhox ) const
{
  rhox = px/(R_*t) - tx*p/(R_*t*t);
}


template <class T>
inline T
GasModel::pressure( const T& rho, const T& t ) const
{
  T p = R_*rho*t;
  return p;
}


template <class T>
inline void
GasModel::pressureJacobian( const T& rho, const T& t, T& p_rho, T& p_t ) const
{
  p_rho = R_*t;
  p_t   = R_*rho;
}


template <class Tq, class Tg>
inline void
GasModel::pressureGradient( const Tq& rho, const Tq& t,
                            const Tg& rhox, const Tg& tx,
                            typename promote_Surreal<Tq,Tg>::type& px ) const
{
  px = R_*rhox*t + R_*rho*tx;
}


template <class T>
inline T
GasModel::energy( const T& rho, const T& t ) const
{
  Real Cv = R_/(gamma_ - 1);
  T e = Cv*t;
  return e;
}


template <class T>
inline void
GasModel::energyJacobian( const T& rho, const T& t, T& e_rho, T& e_t ) const
{
  Real Cv = R_/(gamma_ - 1);
  e_rho = 0;
  e_t   = Cv;
}

template <class T>
inline void
GasModel::energyGradient( const T& rho, const T& t,
                          const T& rhox, const T& tx, T& ex ) const
{
  Real Cv = R_/(gamma_-1);
  ex = Cv*tx;
}


template <class T>
inline T
GasModel::enthalpy( const T& rho, const T& t ) const
{
  Real Cp = gamma_*R_/(gamma_ - 1);
  T h = Cp*t;
  return h;
}


template <class T>
inline void
GasModel::enthalpyJacobian( const T& rho, const T& t, T& h_rho, T& h_t ) const
{
  Real Cp = gamma_*R_/(gamma_ - 1);
  h_rho = 0;
  h_t   = Cp;
}


template <class Tq, class Tg>
inline void
GasModel::enthalpyGradient( const Tq& rho, const Tq& t,
                            const Tg& rhox, const Tg& tx,
                            typename promote_Surreal<Tq,Tg>::type& hx ) const
{
  Real Cp = gamma_*R_/(gamma_ - 1);
  hx = Cp*tx;
}


template <class T>
inline T
GasModel::temperature( const T& rho, const T& e ) const
{
  return e/Cv();
}


template <class T>
inline void
GasModel::temperatureJacobian( const T& rho, const T& e, T& t_rho, T& t_e ) const
{
  t_rho = 0;
  t_e   = 1/Cv();
}

template <class T>
inline void
GasModel::temperatureGradient( const T& rho, const T& e,
                               const T& rhox, const T& ex, T& tx ) const
{
  tx = ex/Cv();
}


template <class T>
inline T
GasModel::speedofSound( const T& t ) const
{
  T c = sqrt(gamma_*R_*t);
  return c;
}


template <class T>
inline void
GasModel::speedofSoundJacobian( const T& t, T& c_t ) const
{
  c_t = 0.5*speedofSound(t)/t;
}

//----------------------------------------------------------------------------//
// A class for representing input parameters for the ViscosityModel_Sutherland
struct ViscosityModel_SutherlandParams : noncopyable
{
  const ParameterNumeric<Real> tSuth{"tSuth", 198.6     , 0, NO_LIMIT, "Sutherland's law constant"};
  const ParameterNumeric<Real> tRef {"tRef" , NO_DEFAULT, 0, NO_LIMIT, "Reference temperature"};
  const ParameterNumeric<Real> muRef{"muRef", NO_DEFAULT, 0, NO_LIMIT, "Molecular viscosity"};

  static void checkInputs(PyDict d);

  static ViscosityModel_SutherlandParams params;
};

//----------------------------------------------------------------------------//
// ViscosityModel<T>:  Sutherland's law for molecular viscosity
//
// template parameters:
//   T            solution DOF data type (e.g. Real)
//
// member functions:
// .viscosity                 molecular viscosity
// .viscocityJacobian         jacobian wrt temperature
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//

class ViscosityModel_Sutherland
{
public:
  explicit ViscosityModel_Sutherland( const PyDict& d ) :
    tSuth_(d.get(ViscosityModel_SutherlandParams::params.tSuth)),
    tRef_(d.get(ViscosityModel_SutherlandParams::params.tRef)),
    muRef_(d.get(ViscosityModel_SutherlandParams::params.muRef)) {}
  ViscosityModel_Sutherland( Real muRef, Real tRef, Real tSuth ) :
      tSuth_(tSuth),
      tRef_(tRef),
      muRef_(muRef) {}
  ViscosityModel_Sutherland( const ViscosityModel_Sutherland& visc ) :
      tSuth_(visc.tSuth_),
      tRef_(visc.tRef_),
      muRef_(visc.muRef_) {}

  template <class T>
  T viscosity( const T& t ) const;
  template <class T>
  void viscosityJacobian( const T& t, T& mu_t ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real tSuth_;                // Sutherland's law constant
  Real tRef_;
  Real muRef_;                // reference molecular viscosity
};


template <class T>
inline T
ViscosityModel_Sutherland::viscosity( const T& t ) const
{
  T mu = muRef_ * t*sqrt(t) * (tRef_ + tSuth_)/(t + tSuth_) / (tRef_*sqrt(tRef_));
  return mu;
}


template <class T>
void
ViscosityModel_Sutherland::viscosityJacobian( const T& t, T& mu_t ) const
{
  mu_t = muRef_*( 1.5*sqrt(t)*(tRef_ + tSuth_)/(t + tSuth_) / (tRef_*sqrt(tRef_))
                + t*sqrt(t)*(tRef_ + tSuth_)*(-1/((t + tSuth_)*(t + tSuth_))) / (tRef_*sqrt(tRef_)) );
}


//----------------------------------------------------------------------------//
// ViscosityModel<T>:  Sutherland's law for molecular viscosity
//                     (modified formulation)
//
// template parameters:
//   T            solution DOF data type (e.g. Real)
//
// member functions:
// .viscosity                 molecular viscosity
// .viscocityJacobian         jacobian wrt temperature
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//

class ViscosityModel_SutherlandModified
{
public:
  ViscosityModel_SutherlandModified( Real muRef, Real tSuth ) :
      tSuth_(tSuth),
      muRef_(muRef) {}
  ViscosityModel_SutherlandModified( const ViscosityModel_SutherlandModified& visc ) :
      tSuth_(visc.tSuth_),
      muRef_(visc.muRef_) {}

  template <class T>
  T viscosity( const T& t ) const;
  template <class T>
  void viscosityJacobian( const T& t, T& mu_t ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real tSuth_;                // Sutherland's law constant
  Real muRef_;                // reference molecular viscosity
};


template <class T>
inline T
ViscosityModel_SutherlandModified::
viscosity( const T& t ) const
{
  T mu = muRef_ * t*sqrt(t) /(t + tSuth_);
  return mu;
}


template <class T>
void
ViscosityModel_SutherlandModified::
viscosityJacobian( const T& t, T& mu_t ) const
{
  mu_t = muRef_*( 1.5*sqrt(t)/(t + tSuth_)
                + t*sqrt(t)*(-1/((t + tSuth_)*(t + tSuth_))));
}

//----------------------------------------------------------------------------//
// A class for representing input parameters for the ViscosityModel_Const
struct ViscosityModel_ConstParams : noncopyable
{
  const ParameterNumeric<Real> muRef{"muRef", NO_DEFAULT, 0, NO_LIMIT, "Molecular viscosity"};

  static void checkInputs(PyDict d);

  static ViscosityModel_ConstParams params;
};

///////////////////////////
//CONSTANT VISCOSITY MODEL
///////////////////////////

class ViscosityModel_Const
{
public:
  explicit ViscosityModel_Const( const PyDict& d ) :
    muRef_(d.get(ViscosityModel_ConstParams::params.muRef)) {}
  explicit ViscosityModel_Const( Real muRef ) :
    muRef_(muRef) {}
  explicit ViscosityModel_Const( const ViscosityModel_Const& visc ) :
    muRef_(visc.muRef_) {}

  template <class T>
  T viscosity( const T& t ) const;
  template <class T>
  void viscosityJacobian( const T& t, T& mu_t ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real muRef_;                // reference molecular viscosity
};


template <class T>
inline T
ViscosityModel_Const::viscosity( const T& t ) const
{
  T mu = muRef_;
  return mu;
}


template <class T>
void
ViscosityModel_Const::viscosityJacobian( const T& t, T& mu_t ) const
{
  mu_t = 0.0;
}


//----------------------------------------------------------------------------//
// ThermalConductivityModel<T>:  constant Prandtl number model
//
// template parameters:
//   T            solution DOF data type (e.g. Real)
//
// member functions:
//   .conductivity            thermal conductivity
//   .conductivityJacobian    jacobian wrt temperature
//
//   .dump                    debug dump of private data
//----------------------------------------------------------------------------//

class ThermalConductivityModel
{
public:
  ThermalConductivityModel( Real Prandtl, Real Cp ) :
      Prandtl_(Prandtl),
      Cp_(Cp) {}
  ThermalConductivityModel( const ThermalConductivityModel& tcond0 ) :
      Prandtl_(tcond0.Prandtl_),
      Cp_(tcond0.Cp_) {}

  template <class T>
  T conductivity( const T& mu ) const;
  template <class T>
  void conductivityJacobian( const T& mu, T& k_mu ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real Prandtl_;              // Prandtl number (mu Cp/k)
  Real Cp_;                   // specific heat at const pressure
};


template <class T>
inline T
ThermalConductivityModel::conductivity( const T& mu ) const
{
  T k = mu*Cp_/Prandtl_;
  return k;
}


template <class T>
void
ThermalConductivityModel::conductivityJacobian( const T& mu, T& k_mu ) const
{
  k_mu = Cp_/Prandtl_;
}


} //namespace SANS

#endif  // GASMODEL_H
