// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/NS/SolutionFunction_BoltzmannD1Q3.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_BoltzmannD1Q3_SinX_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.pdf0));
  allParams.push_back(d.checkInputs(params.pdf1));
  allParams.push_back(d.checkInputs(params.pdf2));
  d.checkUnknownInputs(allParams);
}

SolutionFunction_BoltzmannD1Q3_SinX_Params SolutionFunction_BoltzmannD1Q3_SinX_Params::params;


}
