// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBOLTZMANND2Q9_BOUCEBACKINTERACTION_H
#define BCBOLTZMANND2Q9_BOUCEBACKINTERACTION_H

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "PDEBoltzmannD2Q9.h"


#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

  const Real csq = 1./3.;
  const int e[9][2] = {   {  0,  0 }, {  1,  0 }, {  0,  1 },
                          { -1,  0 }, {  0, -1 }, {  1,  1 },
                          { -1,  1 }, { -1, -1 }, {  1, -1 }    };

  const Real weight[9] = { 16./36., 4./36., 4./36., 4./36., 4./36.,
                                    1./36., 1./36., 1./36., 1./36.};

  const int if0 = 0;
  const int if1 = 1;
  const int if2 = 2;
  const int if3 = 3;
  const int if4 = 4;
  const int if5 = 5;
  const int if6 = 6;
  const int if7 = 7;
  const int if8 = 8;



  //class BCTypeBouceBackInteraction_pdf0; // No BC needed when advection velocity is 0
  class BCTypeBounceBackInteraction;
  class BCTypeEquilibriumInteraction;

  template <class BCType, class PDEBoltzmannD1Q3>
  class BCBoltzmannD2Q9;

  template <class BCType>
  struct BCBoltzmannD2Q9Params;

//----------------------------------------------------------------------------//
// PDF1, e(1) = {1,0}
//----------------------------------------------------------------------------//
#if 0
  // TODO: Complete this.
template<class VariableTypeD2Q9Params>
struct BCBoltzmannD2Q9BounceBackInteractionParams : noncopyable
{
  BCBoltzmannD2Q9BounceBackInteractionParams();

  const ParameterBool UseInteractionPotentials{"UseInteractionPotentials", NO_DEFAULT, "Use species interactions at boundary"};
  const ParameterNumeric<Real> rho0{"rho0", NO_DEFAULT, NO_RANGE, "Reference density"};
  const ParameterNumeric<Real> setU{"setU", NO_DEFAULT, NO_RANGE, "Set moving surface U velocity"};
  const ParameterNumeric<Real> setV{"setV", NO_DEFAULT, NO_RANGE, "Set moving surface V velocity"};
  const ParameterNumeric<Real> coulombic{"coulombic", NO_DEFAULT, NO_RANGE, "Coefficient for Coulombic interaction"};
  const ParameterNumeric<Real> vdw{"vdw", NO_DEFAULT, NO_RANGE, "Coefficient for van der Waals interaction"};
  const ParameterNumeric<Real> gaussian{"gaussian", NO_DEFAULT, NO_RANGE, "Coefficient for gaussian correction interaction"};

  static constexpr const char* BCName{"BounceBackInteraction"};
  struct Option
  {
    const DictOption BounceBackDirichletPDF0{
      BCBoltzmannD2Q9BounceBackInteractionParams::BCName, BCBoltzmannD2Q9BounceBackInteractionParams::checkInputs
                                            };
  };

  static void checkInputs(PyDict d);
  static BCBoltzmannD2Q9BounceBackInteractionParams params;
};

template <class PDE_>
class BCBoltzmannD2Q9<BCTypeBounceBackInteraction, PDE_> :
    public BCType< BCBoltzmannD2Q9<BCTypeBounceBackInteraction, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeBounceBackInteraction BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBoltzmannD2Q9BounceBackInteractionParams<typename PDE_::VariableTypeD2Q9Params> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N-1;//N;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  BCBoltzmannD2Q9(const PDE& pde, const PyDic& d) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    rho0_(d.get(ParamsType::params.rho0)),
    dt_(d.get(ParamsType::params.dt)),
    setU_(d.get(ParamsType::params.setU)),
    setV_(d.get(ParamsType::params.setV)),
    interaction_(d.get(ParamsType::params.UseInteractionPotentials)),
    coulombic_(d.get(ParamsType::params.coulombic)),
    vdw_(d.get(ParamsType::params.vdw)),
    gaussian_(d.get(ParamsType::params.gaussian))
    {}

  ~BCBoltzmannD2Q9() {}

  BCBoltzmannD2Q9(const BCBoltzmannD2Q9& ) = delete;
  BCBoltzmannD2Q9& operator=( const BCBoltzmannD2Q9& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    Real Ueff = setU_;
    Real Veff = setV_;
    if (interaction_)
    {
      // TODO: add interaction potential to set effective U and V
    }

    // Standard bounce back for outflow velocities
    //*********** NOTE: Suited for orthogonal BC's for the time being *********
    for (int i=1; i<9; i++)
    {
      Real e_dot_n = nx*e[i][0] + ny*e[i][1];
      if (e_dot_n > 0)
        qB(i) = qI(i);
      else
      {
        if (i<=4)
        {
          int shift = 2*e[i][0] + 2*e[i][1];
          qB(i) = qI(i+shift) + 2*weight[i]*rho0_*(e[i][0]*Ueff+e[i][1]*Veff)/csq;
        }
        else
        {
          int shift = 2*e[i][1];
          qB(i) = qI(i+shift) + 2*weight[i]*rho0_*(e[i][0]*Ueff+e[i][1]*Veff)/csq;
        }
      }
    }

  }

protected:
  const PDE& pde_;
  const Real rho0_;
  const Real dt_;
  const Real setU;
  const Real setV;
  const Real coulombic_;
  const Real vdw_;
  const Real gaussian_;


}; // class BCBoltzmannD2Q9<BCTypeBounceBackInteraction, PDE_>
#endif

} // namespace SANS


#endif // BCBOLTZMANND2Q9_BOUNCEBACKINTERACTION_H
