// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GETBOLTZMANND2Q9TRAITSMODEL_H
#define GETBOLTZMANND2Q9TRAITSMODEL_H

#include "TraitsBoltzmannD2Q9.h"
//#include "TraitsNavierStokes.h"
//#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"

namespace SANS
{

template<class TraitsModel> struct getBoltzmannD2Q9TraitsModel;

template<class QType_, class GasModel_>
struct getBoltzmannD2Q9TraitsModel< TraitsModelBoltzmannD2Q9<QType_,GasModel_> >
{
  typedef TraitsModelBoltzmannD2Q9<QType_,GasModel_> type;
};

}

#endif
