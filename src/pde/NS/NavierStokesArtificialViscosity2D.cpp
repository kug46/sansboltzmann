// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#define AVSENSOR2D_INSTANTIATE

#include "TraitsNavierStokesArtificialViscosity.h"

#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D_impl.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D_impl.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D_impl.h"

namespace SANS
{

//Instantiations for the Artificial Viscosity PDE with Navier Stokes

template class AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeNavierStokesArtificialViscosity>;

template class AVSensor_DiffusionMatrix2D_Uniform<TraitsSizeNavierStokesArtificialViscosity>;
template class AVSensor_DiffusionMatrix2D_GenHScale<TraitsSizeNavierStokesArtificialViscosity>;

template class AVSensor_Source2D_Uniform<TraitsSizeNavierStokesArtificialViscosity>;

}
