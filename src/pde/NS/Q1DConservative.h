// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q1DCONSERVATIVE_H
#define Q1DCONSERVATIVE_H

// solution interpreter class
// Euler/N-S conservation-eqns: conservation variables (rho, rho*u, rho*v, rho*E)

#include <vector>
#include <string>

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "pde/NS/NSVariable1D.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D Euler/N-S
// conservative variables (rho, rhou, rhov, rhoE)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (rho, u, v, p)
class QTypeConservative;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q1D;


template <template <class> class PDETraitsSize>
class Q1D<QTypeConservative, PDETraitsSize>
{
public:
  typedef PhysD1 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int irho = 0;
  static const int irhou = 1;
  static const int irhoE = 2;

  // The components of the state vector that make up the momentum vector
  static const int ix = irhou;

  static std::vector<std::string> stateNames() { return {"rho", "rhou", "rhoE"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q1D( const GasModel& gas0 ) : gas_(gas0) {}
  Q1D( const Q1D& q0 ) : gas_(q0.gas_) {}
  ~Q1D() {}

  Q1D& operator=( const Q1D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q, ArrayQ<T>& u_q, ArrayQ<T>& t_q ) const;

  template<class Tq, class Tg, class T>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    T& rhox,
    T&   ux,
    T&   tx ) const;

  template<class T>
  void evalSecondGradient(
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
      T& rhoxx, T& uxx, T& txx ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from primitive variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure1D<T>& data ) const;

  // set from primitive variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature1D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative1D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& t ) const
{
  rho = q(irho);
  u   = q(irhou)/q(irho);
  t   = (q(irhoE)/q(irho) - 0.5*(u*u) ) / ( gas_.Cv() );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  rho = q(irho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u ) const
{
  u = q(irhou)/q(irho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  const T& rho  = q(irho);
  const T& rhou = q(irhou);
  const T& rhoE = q(irhoE);

  t   = ( rhoE/rho - 0.5*( rhou*rhou )/(rho*rho) ) / ( gas_.Cv() );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q, ArrayQ<T>& u_q, ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; t_q = 0;

  const T& rho  = q(irho);
  const T& rhou = q(irhou);
  const T& rhoE = q(irhoE);

  rho_q[irho] = 1;  // drho/drho

  u_q[irho ] = -rhou/(rho*rho); // du/drho
  u_q[irhou] = 1/rho;           // du/drhou

  t_q[irho ] = ( -rhoE/(rho*rho) + ( rhou*rhou )/(rho*rho*rho) ) / ( gas_.Cv() ); // dt/drho
  t_q[irhou] = ( -( rhou )/(rho*rho) ) / ( gas_.Cv() );                           // dt/drhou
  t_q[irhoE] = ( 1/rho ) / ( gas_.Cv() );                                         // dt/drhoE
}


template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    T& rhox,
    T&   ux,
    T&   tx ) const
{
  rhox = qx(0);

  ux   = qx(1)/q(irho) - q(irhou)*qx(0)/(q(irho)*q(irho));

  tx = (q(irhou)*q(irhou))*qx(0)/(q(irho)*q(irho)*q(irho))
      - (q(irhou)*qx(1))/(q(irho)*q(irho)) + qx(2)/q(irho) - q(irhoE)*qx(0)/(q(irho)*q(irho));
  tx /= ( gas_.Cv() );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
    T& rhoxx, T& uxx, T& txx ) const
{
  SANS_DEVELOPER_EXCEPTION("Second derivatives for Q1DConservative not yet implemented");
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 3);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "Temperature"));

  const T& rho = data[0];
  const T& u   = data[1];
  const T& t   = data[2];

  q(irho) = rho;
  q(irhou) = rho*u;
  q(irhoE) = rho*( gas_.energy(rho,t) + 0.5*(u*u) );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure1D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.VelocityX;
  const T& p = data.Pressure;
  T t = p / (gas_.R()*rho);
  T E = gas_.energy(rho, t) + 0.5*(u*u );

  q(irho) = rho;
  q(irhou) = rho*u;
  q(irhoE) = rho*E;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature1D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u   = data.VelocityX;
  const T& t   = data.Temperature;
  T E = gas_.energy(rho, t) + 0.5*(u*u);

  q(irho) = rho;
  q(irhou) = rho*u;
  q(irhoE) = rho*E;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative1D<T>& data ) const
{
  q(irho) = data.Density;
  q(irhou) = data.MomentumX;
  q(irhoE) = data.TotalEnergy;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q1D<QTypeConservative, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
Q1D<QTypeConservative, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  Real rho = 0, t = 0;

  evalDensity(q, rho);
  if (rho <= 0)
    return false;

  evalTemperature(q, t);
  if (t <= 0)
    return false;

  return true;
}


template <template <class> class PDETraitsSize>
void
Q1D<QTypeConservative, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q1D<QTypeConservative, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q1D<QTypeConservative, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

}

#endif  // Q1DCONSERVATIVE_H
