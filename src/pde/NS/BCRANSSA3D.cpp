// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCRANSSA3D.h"
#include "BCEuler3D.h"
#include "Q3DPrimitiveRhoPressure.h"
#include "Q3DPrimitiveSurrogate.h"
//#include "Q3DEntropy.h"
#include "Q3DConservative.h"
#include "QRANSSA3D.h"
#include "BCRANSSA3D_Solution.h"
#include "TraitsRANSSA.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

template<class VariableType3DParams>
BCEuler3DFullStateParams<VariableType3DParams>::BCEuler3DFullStateParams()
  : StateVector(VariableType3DParams::params.StateVector)
{}

template<class VariableType3DParams>// cppcheck-suppress passedByValue
void BCEuler3DFullStateParams<VariableType3DParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Characteristic));
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
template<class VariableType3DParams>
BCEuler3DFullStateParams<VariableType3DParams> BCEuler3DFullStateParams<VariableType3DParams>::params;

template struct BCEuler3DFullStateParams<SAVariableType3DParams>;


//===========================================================================//
// Instantiate BC parameters

// Pressure-primitive variables
typedef BCRANSSA3DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_S )

// Conservative Variables
typedef BCRANSSA3DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Cons_S;
BCPARAMETER_INSTANTIATE( BCVector_Cons_S )

#if 0   // not implemented
// Entropy Variables
typedef BCRANSSA3DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypeEntropy, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Entropy_S;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_S )
#endif

// Surrogate Variables
typedef BCRANSSA3DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Surrogate_S;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_S )



//===========================================================================//
// Repeat for constant viscosity

// Pressure-primitive variables
typedef BCRANSSA3DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_RhoP_C;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_C )

// Conservative Variables
typedef BCRANSSA3DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Cons_C;
BCPARAMETER_INSTANTIATE( BCVector_Cons_C )

#if 0   // not implemented
// Entropy Variables
typedef BCRANSSA3DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Entropy_C;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_C )
#endif

// Surrogate Variables
typedef BCRANSSA3DVector<
              TraitsSizeRANSSA,
              TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Surrogate_C;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_C )
} //namespace SANS
