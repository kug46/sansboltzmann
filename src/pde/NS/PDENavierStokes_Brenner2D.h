// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDENAVIERSTOKES_BRENNER2D_H
#define PDENAVIERSTOKES_BRENNER2D_H

// 2-D compressible Navier-Stokes PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "pde/ForcingFunctionBase.h"
#include "Topology/Dimension.h"

#include "GasModel.h"
#include "NSVariable2D.h"
#include "PDENavierStokes2D.h"

#include <iostream>
#include <string>
#include <memory> // shared_ptr

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q2D;


//----------------------------------------------------------------------------//
// PDE class: 2-D compressible Navier-Stokes
//
// template parameters:
//   T                        solution DOF data type (e.g. Real)
//   PDETraitsSize            define PDE size-related features
//     N, D                   PDE size, physical dimensions
//     ArrayQ                 solution/residual arrays
//     MatrixQ                matrices (e.g. flux jacobian)
//   PDETraitsModel           define PDE model-related features
//     QType                  solution variable set (e.g. primitive)
//     QInterpret             solution variable interpreter
//     GasModel               gas model (e.g. ideal gas law)
//     ViscosityModel         molecular viscosity model (e.g. Sutherland)
//     ThermalConductivityModel   thermal conductivity model (e.g. const Prandtl number)
//
// member functions:
//   .hasFluxConservative     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//INHERITS CONSERVATIVE, ADVECTIVE FLUXES FROM EULER2D
//   .masterState        unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDENavierStokes_Brenner2D : public PDENavierStokes2D<PDETraitsSize, PDETraitsModel>
{
  friend class PDENavierStokes2D<PDETraitsSize, PDETraitsModel>;
public:
  typedef PhysD2 PhysDim;

  typedef PDENavierStokes2D<PDETraitsSize, PDETraitsModel> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef typename PDETraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;                                       // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                                 // gas model

  typedef typename PDETraitsModel::ViscosityModel ViscosityModel;                     // molecular viscosity model

  typedef typename PDETraitsModel::ThermalConductivityModel ThermalConductivityModel; // thermal conductivity model

  typedef ForcingFunctionBase2D< PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel> > ForcingFunctionType;

  explicit PDENavierStokes_Brenner2D( const GasModel& gas, const ViscosityModel& visc,
         const ThermalConductivityModel& tcond,
         const EulerResidualInterpCategory cat = Euler_ResidInterp_Raw,
         const RoeEntropyFix entropyFix = eVanLeer,
         const Real rhodiff = 4./3.,
         const std::shared_ptr<ForcingFunctionType>& forcing = NULL) :
      PDENavierStokes2D<PDETraitsSize, PDETraitsModel>( gas, visc, tcond, cat, entropyFix ),
      rhodiff_(rhodiff), forcing_(forcing) {}

  using BaseType::iCont;
  using BaseType::ixMom;
  using BaseType::iyMom;
  using BaseType::iEngy;

  PDENavierStokes_Brenner2D& operator=( const PDENavierStokes_Brenner2D& ) = delete;

  // flux components
  bool hasFluxViscous() const { return true; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool hasForcingFunction() const { return forcing_ != NULL && (*forcing_).hasForcingFunction(); }

  bool needsSolutionGradientforSource() const { return false; }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

protected:
  // internal function for computing the actual viscous flux
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tq& mu, const Tq& k,
      const Tg& rhox, const Tg& ux, const Tg& vx, const Tg& tx,
      const Tg& rhoy, const Tg& uy, const Tg& vy, const Tg& ty,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

public:
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

protected:
  template <class Tq, class Tk>
  void diffusionViscous(
      const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tq& mu, const Tq& kn,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

public:

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,  MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const;

protected:
  template <class Tq,  class Tg, class Tk, class Tm>
  void diffusionViscousGradient(
      const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tm& mu, const Tm& k,
      const Tg& rhox, const Tg& ux, const Tg& vx, const Tg& tx, const Tm& mux, const Tm& kx,
      const Tg& rhoy, const Tg& uy, const Tg& vy, const Tg& ty, const Tm& muy, const Tm& ky,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,  MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const;

public:

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

protected:
  template <class Tm, class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Tq& mu, const Tq& k,
      const ArrayQ<Tm>& dmu, const ArrayQ<Tm> dk,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

public:

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

protected:
  // strong form viscous fluxes
  template <class Tm, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Tm& mu,
      const typename promote_Surreal<Tq, Tg>::type& mux,
      const typename promote_Surreal<Tq, Tg>::type& muy,
      const Tm& k,
      const typename promote_Surreal<Tq, Tg>::type& kx,
      const typename promote_Surreal<Tq, Tg>::type& ky,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

public:

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

#if 0   // should inherit empties from Euler
  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const {}

  // dual-consistent source
  template <class T>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL,
      const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR,
      ArrayQ<T>& sourceL, ArrayQ<T>& sourceR ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<sT>& dsdu ) const {}
#endif

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const;

  const ViscosityModel& viscosityModel() const {return visc_;}
  const ThermalConductivityModel& thermalConductivityModel() const {return tcond_;}

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BaseType::qInterpret_;
  using BaseType::gas_;
  using BaseType::visc_;
  using BaseType::tcond_;
  const Real rhodiff_;
  const std::shared_ptr<ForcingFunctionType> forcing_;
};


// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
    {
      typedef typename promote_Surreal<Tq,Tg>::type T;

      Tq rho=0, u=0, v=0, t=0;
      T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;
      Tq mu, k;

      qInterpret_.eval( q, rho, u, v, t );
      qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
      qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

      mu = visc_.viscosity( t );
      k  = tcond_.conductivity( mu );

      // Compute the viscous flux
      fluxViscous(rho, u, v, t, mu, k,
                  rhox, ux, vx, tx,
                  rhoy, uy, vy, ty,
                  f, g );
    }

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tq& mu, const Tq& k,
    const Tg& rhox, const Tg& ux, const Tg& vx, const Tg& tx,
    const Tg& rhoy, const Tg& uy, const Tg& vy, const Tg& ty,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  Tq delta = rhodiff_*mu/rho;

  Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  f(iCont) -= delta*rhox;
  f(ixMom) -= delta*u*rhox;
  f(iyMom) -= delta*v*rhox;
  f(iEngy) -= delta*e0*rhox;

  g(iCont) -= delta*rhoy;
  g(ixMom) -= delta*u*rhoy;
  g(iyMom) -= delta*v*rhoy;
  g(iEngy) -= delta*e0*rhoy;

  BaseType::fluxViscous(u, v, mu, k,
                        ux, vx, tx,
                        uy, vy, ty,
                        f, g );
}


// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscous(x, y, time, qL, qxL, qyL, fL, gL);
  fluxViscous(x, y, time, qR, qxR, qyR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;

}


// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{

  Tq rho=0, u=0, v=0, t=0;
  Tq mu, k;

  qInterpret_.eval( q, rho, u, v, t );
  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  diffusionViscous( rho, u, v, t, mu, k,
                    kxx, kxy,
                    kyx, kyy );

}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tk>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tq& mu, const Tq& k,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{

  Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  Tq delta = rhodiff_* mu/rho;

  // d(Fv)/d(Ux)
  kxx(iCont,0) += delta;
  kxx(ixMom,0) += delta*u;
  kxx(iyMom,0) += delta*v;
  kxx(iEngy,0) += delta*e0;

  // d(Gv)/d(Uy)
  kyy(iCont,0) += delta;
  kyy(ixMom,0) += delta*u;
  kyy(iyMom,0) += delta*v;
  kyy(iEngy,0) += delta*e0;

  BaseType::diffusionViscous(rho, u, v, t, mu, k, kxx, kxy, kyx, kyy);

}


// gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tk>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,  MatrixQ<Tk>& kyx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y  ) const
{
  SANS_DEVELOPER_EXCEPTION("NS BRENNER DIFFUSIONVISCOUSGRADIENT NOT IMPLEMENTED");
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk, class Tm>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tm& mu, const Tm& k,
    const Tg& rhox, const Tg& ux, const Tg& vx, const Tg& tx, const Tm& mux, const Tm& kx,
    const Tg& rhoy, const Tg& uy, const Tg& vy, const Tg& ty, const Tm& muy, const Tm& ky,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,  MatrixQ<Tk>& kyx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
{
  SANS_DEVELOPER_EXCEPTION("NS BRENNER DIFFUSIONVISCOUSGRADIENT NOT IMPLEMENTED");
}


// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::jacobianFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  Tq rho=0, u=0, v=0, t=0;
  Tq mu, k;
  Tq mu_t = 0;
  Tq k_mu = 0;

  typedef typename promote_Surreal<Tq,Tg>::type T;
  T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  mu = visc_.viscosity( t );
  visc_.viscosityJacobian(t, mu_t);
  k  = tcond_.conductivity( mu );
  tcond_.conductivityJacobian( mu, k_mu);
  Tq E = gas_.energy(rho, t) + 0.5*(u*u + v*v);
  Real Cv = gas_.Cv();

  Tq t_rho  = ( -E/rho + ( u*u + v*v )/rho ) / Cv;
  Tq t_rhou = (        - (   u       )/rho ) / Cv;
  Tq t_rhov = (        - (         v )/rho ) / Cv;
  Tq t_rhoE = (  1/rho                     ) / Cv;

  ArrayQ<Tq> dmu = 0;
  dmu(0) = mu_t*t_rho;
  dmu(1) = mu_t*t_rhou;
  dmu(2) = mu_t*t_rhov;
  dmu(3) = mu_t*t_rhoE;

  ArrayQ<Tq> dk = 0;
  dk(0) = k_mu*dmu(0);
  dk(1) = k_mu*dmu(1);
  dk(2) = k_mu*dmu(2);
  dk(3) = k_mu*dmu(3);

  jacobianFluxViscous(mu, k, dmu, dk, q, qx, qy, dfdu, dgdu);

  BaseType::jacobianFluxViscous(x, y, time, q, qx, qy, dfdu, dgdu);

}

// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tm, class Tq, class Tg, class Tf>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::jacobianFluxViscous(
    const Tq& mu, const Tq& k,
    const ArrayQ<Tm>& dmu, const ArrayQ<Tm> dk,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
//  Real R = gas_.R();
//  Real g = gas_.gamma();
//  Real Cv = gas_.Cv();
  Tq rho=0, u=0, v=0, t=0;

  typedef typename promote_Surreal<Tq,Tg>::type T;
  T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  Tq E = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  Tq delta = rhodiff_*mu/rho;

  ArrayQ<Tq> ddelta;
  ddelta(0) = rhodiff_*(dmu(0)/rho - mu/rho/rho);
  ddelta(1) = rhodiff_*dmu(1)/rho;
  ddelta(2) = rhodiff_*dmu(2)/rho;
  ddelta(3) = rhodiff_*dmu(3)/rho;

//  f(iCont) -= delta*rhox;
//  f(ixMom) -= delta*u*rhox;
//  f(iyMom) -= delta*v*rhox;
//  f(iEngy) -= delta*E*rhox;
//
//  g(iCont) -= delta*rhoy;
//  g(ixMom) -= delta*u*rhoy;
//  g(iyMom) -= delta*v*rhoy;
//  g(iEngy) -= delta*E*rhoy;

  dfdu(iCont,0) -= ddelta(0)*rhox;
  dfdu(iCont,1) -= ddelta(1)*rhox;
  dfdu(iCont,2) -= ddelta(2)*rhox;
  dfdu(iCont,3) -= ddelta(3)*rhox;

  dfdu(ixMom,0) -= ddelta(0)*u*rhox - delta*(u/rho)*rhox;
  dfdu(ixMom,1) -= ddelta(1)*u*rhox + delta/rho*rhox ;
  dfdu(ixMom,2) -= ddelta(2)*u*rhox ;
  dfdu(ixMom,3) -= ddelta(3)*u*rhox ;

  dfdu(iyMom,0) -= ddelta(0)*v*rhox  - delta*(v/rho)*rhox ;
  dfdu(iyMom,1) -= ddelta(1)*v*rhox ;
  dfdu(iyMom,2) -= ddelta(2)*v*rhox  + delta/rho*rhox ;
  dfdu(iyMom,3) -= ddelta(3)*v*rhox ;

  dfdu(iEngy,0) -= ddelta(0)*E*rhox  - delta*(E/rho)*rhox;
  dfdu(iEngy,1) -= ddelta(1)*E*rhox ;
  dfdu(iEngy,2) -= ddelta(2)*E*rhox ;
  dfdu(iEngy,3) -= ddelta(3)*E*rhox  + delta/rho*rhox ;

  dgdu(iCont,0) -= ddelta(0)*rhoy;
  dgdu(iCont,1) -= ddelta(1)*rhoy;
  dgdu(iCont,2) -= ddelta(2)*rhoy;
  dgdu(iCont,3) -= ddelta(3)*rhoy;

  dgdu(ixMom,0) -= ddelta(0)*u*rhoy - delta*(u/rho)*rhoy;
  dgdu(ixMom,1) -= ddelta(1)*u*rhoy + delta/rho*rhoy;
  dgdu(ixMom,2) -= ddelta(2)*u*rhoy;
  dgdu(ixMom,3) -= ddelta(3)*u*rhoy;

  dgdu(iyMom,0) -= ddelta(0)*v*rhoy - delta*(v/rho)*rhoy;
  dgdu(iyMom,1) -= ddelta(1)*v*rhoy;
  dgdu(iyMom,2) -= ddelta(2)*v*rhoy + delta/rho*rhoy;
  dgdu(iyMom,3) -= ddelta(3)*v*rhoy;

  dgdu(iEngy,0) -= ddelta(0)*E*rhoy - delta*(E/rho)*rhoy;
  dgdu(iEngy,1) -= ddelta(1)*E*rhoy;
  dgdu(iEngy,2) -= ddelta(2)*E*rhoy;
  dgdu(iEngy,3) -= ddelta(3)*E*rhoy + delta/rho*rhoy;


}


// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  //typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho, u, v, t = 0;
  Tqg rhox, rhoy, ux, uy, vx, vy, tx, ty = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  Tq mu = visc_.viscosity( t );
  Tq mu_t = 0;
  visc_.viscosityJacobian( t, mu_t );
  Tqg mux = mu_t*tx;
  Tqg muy = mu_t*ty;

  Tq k = tcond_.conductivity( mu );
  Tq k_mu = 0;
  tcond_.conductivityJacobian( mu, k_mu );
  Tqg kx = k_mu*mux;
  Tqg ky = k_mu*muy;

  strongFluxViscous(mu, mux, muy, k, kx, ky, q, qx, qy, qxx, qxy, qyy, strongPDE);

  BaseType::strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
}


// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tm, class Tq, class Tg, class Th, class Tf>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Tm& mu,
    const typename promote_Surreal<Tq, Tg>::type& mux,
    const typename promote_Surreal<Tq, Tg>::type& muy,
    const Tm& k,
    const typename promote_Surreal<Tq, Tg>::type& kx,
    const typename promote_Surreal<Tq, Tg>::type& ky,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho, u, v, t = 0;
  Tqg rhox, rhoy, ux, uy, vx, vy, tx, ty = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  Tq e_rho = 0; Tq e_t = 0;
  Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  gas_.energyJacobian(rho, t, e_rho, e_t);
  T e0x = e_rho*rhox + e_t*tx + (u*ux + v*vx);
  T e0y = e_rho*rhoy + e_t*ty + (u*uy + v*vy);

  // evaluate second derivatives
  T rhoxx = 0, rhoxy = 0, rhoyy = 0;
  T uxx = 0, uxy = 0, uyy = 0;
  T vxx = 0, vxy = 0, vyy = 0;
  T txx = 0, txy = 0, tyy = 0;

  qInterpret_.evalSecondGradient( q, qx, qy,
                                  qxx, qxy, qyy,
                                  rhoxx, rhoxy, rhoyy,
                                  uxx, uxy, uyy,
                                  vxx, vxy, vyy,
                                  txx, txy, tyy );

  Tq delta = rhodiff_*mu/rho;
  Tqg deltax = rhodiff_*(mux/rho - mu*rhox/rho/rho);
  Tqg deltay = rhodiff_*(muy/rho - mu*rhoy/rho/rho);

//  f(iCont) -= delta*rhox;
//  f(ixMom) -= delta*u*rhox;
//  f(iyMom) -= delta*v*rhox;
//  f(iEngy) -= delta*e0*rhox;
//
//  g(iCont) -= delta*rhoy;
//  g(ixMom) -= delta*u*rhoy;
//  g(iyMom) -= delta*v*rhoy;
//  g(iEngy) -= delta*e0*rhoy;

  strongPDE(iCont) -= deltax*rhox + delta*rhoxx;
  strongPDE(ixMom) -= deltax*u*rhox + delta*ux*rhox + delta*u*rhoxx;
  strongPDE(iyMom) -= deltax*v*rhox + delta*vx*rhox + delta*v*rhoxx;
  strongPDE(iEngy) -= deltax*e0*rhox + delta*e0x*rhox + delta*e0*rhoxx;

  strongPDE(iCont) -= deltay*rhoy + delta*rhoyy;
  strongPDE(ixMom) -= deltay*u*rhoy + delta*uy*rhoy + delta*u*rhoyy;
  strongPDE(iyMom) -= deltay*v*rhoy + delta*vy*rhoy + delta*v*rhoyy;
  strongPDE(iEngy) -= deltay*e0*rhoy + delta*e0y*rhoy + delta*e0*rhoyy;
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  (*forcing_)(*this, x, y, time, forcing );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  this->gas_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: visc_ = " << std::endl;
  this->visc_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: tcond_ = " << std::endl;
  this->tcond_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDENAVIERSTOKES2D_H
