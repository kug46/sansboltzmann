// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef HLLC2D_H
#define HLLC2D_H

#include <cmath>  // sqrt

#include "Topology/Dimension.h"
#include "Surreal/SurrealS.h"

#include "GasModel.h"

#include "tools/smoothmath.h"
#include "tools/minmax.h"

namespace SANS
{

// Forward declare
template<class QType, template <class> class PDETraitsSize>
class Q2D;


template <class QType, template <class> class PDETraitsSize>
class HLLC2D
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef Q2D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  explicit HLLC2D(const GasModel& gas0) :
        gas_(gas0),
        qInterpret_(gas0)
  {
    // Nothing
  }

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iyMom = 2;
  static const int iEngy = 3;

  HLLC2D( const HLLC2D& ausm0 ) = delete;
  HLLC2D& operator=( const HLLC2D& ) = delete;

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, T& C1, T& eigu ) const;

  template <class T>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<T>& f ) const;

  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const;

private:
  const GasModel& gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
};

template <class QType, template <class> class PDETraitsSize>
template <class T, class Tf>
inline void
HLLC2D<QType, PDETraitsSize>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f) const
{
  T rhoL, uL, vL, tL, pL, hL, HL = 0;
  T rhoR, uR, vR, tR, pR, hR, HR = 0;

  qInterpret_.eval( qL, rhoL, uL, vL, tL );
  pL = gas_.pressure(rhoL, tL);
  hL = gas_.enthalpy(rhoL, tL);
  HL = hL + 0.5*(uL*uL + vL*vL);

  qInterpret_.eval( qR, rhoR, uR, vR, tR );
  pR = gas_.pressure(rhoR, tR);
  hR = gas_.enthalpy(rhoR, tR);
  HR = hR + 0.5*(uR*uR + vR*vR);

  T eL, EL;
  T eR, ER;

  eL = gas_.energy(rhoL, tL);
  EL = eL + 0.5*(uL*uL + vL*vL);

  eR = gas_.energy(rhoR, tR);
  ER = eR + 0.5*(uR*uR + vR*vR);

  Real gamma = gas_.gamma();

  // We need to resolve the velocity components and flux in that frame...
  T unL =  uL*nx + vL*ny;
  T unR =  uR*nx + vR*ny;
  // Get the left and right sound speeds and normal Mach numbers
  T aL = gas_.speedofSound(tL);
  T aR = gas_.speedofSound(tR);

  T rhobar = 0.5*(rhoL + rhoR);
  T abar   = 0.5*(aL + aR);
  T p_pvrs = 0.5*(pL + pR) - 0.5*(unR - unL)*rhobar*abar;
  T p_star = max(0.,p_pvrs);

  T qnL = 0.0;
  if (p_star <= pL)
  {
    qnL = 1.0;
  }
  else
  {
    qnL = sqrt(1. + (gamma+1.)/2./gamma*(p_star/pL - 1.));
  }
  T qnR = 0.0;
  if (p_star <= pR)
  {
    qnR = 1.0;
  }
  else
  {
    qnR = sqrt(1. + (gamma+1.)/2./gamma*(p_star/pR - 1.));
  }
  T SL = unL - qnL*aL;
  T SR = unR + qnR*aR;
  T S_star = (pR - pL + rhoL*unL*(SL-unL) - rhoR*unR*(SR-unR))
               / (rhoL*(SL-unL) - rhoR*(SR-unR));

  if (0. <= SL)
  {
    f(iCont) += nx*(rhoL*uL        ) + ny*(rhoL*vL        );
    f(ixMom) += nx*(rhoL*uL*uL + pL) + ny*(rhoL*vL*uL     );
    f(iyMom) += nx*(rhoL*uL*vL     ) + ny*(rhoL*vL*vL + pL);
    f(iEngy) += nx*(rhoL*uL*HL     ) + ny*(rhoL*vL*HL     );
  }
  else if ((SL <= 0.) && (0. <= S_star))
  {
    T cL = (SL-unL)/(SL-S_star);
    T C1 = rhoL*cL*(S_star - unL);
    T C2 = rhoL*cL*(S_star - unL)*(S_star + pL/(rhoL*(SL-unL)));

    f(iCont) += nx*(rhoL*uL        ) + ny*(rhoL*vL        );
    f(ixMom) += nx*(rhoL*uL*uL + pL) + ny*(rhoL*vL*uL     );
    f(iyMom) += nx*(rhoL*uL*vL     ) + ny*(rhoL*vL*vL + pL);
    f(iEngy) += nx*(rhoL*uL*HL     ) + ny*(rhoL*vL*HL     );

    f(iCont) += SL* rhoL*   (cL-1.);
    f(ixMom) += SL*(rhoL*uL*(cL-1.) + C1*nx);
    f(iyMom) += SL*(rhoL*vL*(cL-1.) + C1*ny);
    f(iEngy) += SL*(rhoL*EL*(cL-1.) + C2   );
  }
  else if ((S_star <= 0.) && (0. <= SR))
  {
    T cR = (SR-unR)/(SR-S_star);
    T C1 = rhoR*cR*(S_star - unR);
    T C2 = rhoR*cR*(S_star - unR)*(S_star + pR/(rhoR*(SR-unR)));

    f(iCont) += nx*(rhoR*uR        ) + ny*(rhoR*vR        );
    f(ixMom) += nx*(rhoR*uR*uR + pR) + ny*(rhoR*vR*uR     );
    f(iyMom) += nx*(rhoR*uR*vR     ) + ny*(rhoR*vR*vR + pR);
    f(iEngy) += nx*(rhoR*uR*HR     ) + ny*(rhoR*vR*HR     );

    f(iCont) += SR* rhoR*   (cR-1.);
    f(ixMom) += SR*(rhoR*uR*(cR-1.) + C1*nx);
    f(iyMom) += SR*(rhoR*vR*(cR-1.) + C1*ny);
    f(iEngy) += SR*(rhoR*ER*(cR-1.) + C2   );
  }
  else if (0. >= SR)
  {
    f(iCont) += nx*(rhoR*uR        ) + ny*(rhoR*vR        );
    f(ixMom) += nx*(rhoR*uR*uR + pR) + ny*(rhoR*vR*uR     );
    f(iyMom) += nx*(rhoR*uR*vR     ) + ny*(rhoR*vR*vR + pR);
    f(iEngy) += nx*(rhoR*uR*HR     ) + ny*(rhoR*vR*HR     );
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Unknown wave state occurred in HLLC2D, SL = %f, SR = %f, S_star = %f", SL, SR, S_star );
  }
}

template <class QType, template <class> class PDETraitsSize>
template <class T>
inline void
HLLC2D<QType, PDETraitsSize>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<T>& f ) const
{

  SANS_DEVELOPER_EXCEPTION( "Spacetime HLLC2D Scheme Not Yet Implemented" );
}

}
#endif // HLLC2D_H
