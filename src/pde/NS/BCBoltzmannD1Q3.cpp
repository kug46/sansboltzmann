// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCBoltzmannD1Q3.h"
#include "QD1Q3PrimitiveDistributionFunctions.h"
//#include "Q1DPrimitiveSurrogate.h"
//#include "Q1DConservative.h"
//#include "Q1DEntropy.h"

// TODO: Rework so this is not needed here
#include "TraitsBoltzmannD1Q3.h"
//#include "TraitsEulerArtificialViscosity.h"
//#include "Fluids1D_Sensor.h"
//#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
//#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
//#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
//#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"


namespace SANS
{

// cppcheck-suppress passedByValue
void BCBoltzmannD1Q3Params<BCTypeNone>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCBoltzmannD1Q3Params<BCTypeNone> BCBoltzmannD1Q3Params<BCTypeNone>::params;

// cppcheck-suppress passedByValue
//void BCBoltzmannD1Q3Params<BCTypeTimeOut>::checkInputs(PyDict d)
//{
//  std::vector<const ParameterBase*> allParams;
//  d.checkUnknownInputs(allParams);
//}
//BCBoltzmannD1Q3Params<BCTypeTimeOut> BCBoltzmannD1Q3Params<BCTypeTimeOut>::params;

template<class VariableTypeD1Q3Params>
BCBoltzmannD1Q3BounceBackDirichletPDF0Params<VariableTypeD1Q3Params>::BCBoltzmannD1Q3BounceBackDirichletPDF0Params()
  : StateVector(VariableTypeD1Q3Params::params.StateVector)
{}

#if 1
template<class VariableTypeD1Q3Params>// cppcheck-suppress passedByValue
void BCBoltzmannD1Q3BounceBackDirichletPDF0Params<VariableTypeD1Q3Params>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Characteristic));
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
template<class VariableTypeD1Q3Params>
BCBoltzmannD1Q3BounceBackDirichletPDF0Params<VariableTypeD1Q3Params> BCBoltzmannD1Q3BounceBackDirichletPDF0Params<VariableTypeD1Q3Params>::params;

template struct BCBoltzmannD1Q3BounceBackDirichletPDF0Params<BoltzmannVariableTypeD1Q3Params>;
#endif
// PDF2
template<class VariableTypeD1Q3Params>
BCBoltzmannD1Q3BounceBackDirichletPDF2Params<VariableTypeD1Q3Params>::BCBoltzmannD1Q3BounceBackDirichletPDF2Params()
  : StateVector(VariableTypeD1Q3Params::params.StateVector)
{}
#if 1
template<class VariableTypeD1Q3Params>// cppcheck-suppress passedByValue
void BCBoltzmannD1Q3BounceBackDirichletPDF2Params<VariableTypeD1Q3Params>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Characteristic));
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
template<class VariableTypeD1Q3Params>
BCBoltzmannD1Q3BounceBackDirichletPDF2Params<VariableTypeD1Q3Params> BCBoltzmannD1Q3BounceBackDirichletPDF2Params<VariableTypeD1Q3Params>::params;

template struct BCBoltzmannD1Q3BounceBackDirichletPDF2Params<BoltzmannVariableTypeD1Q3Params>;
#endif


//===========================================================================//
// Instantiate BC parameters

// Pressure-Primitive Variables
typedef BCBoltzmannD1Q3Vector< TraitsSizeBoltzmannD1Q3, TraitsModelBoltzmannD1Q3<QTypePrimitiveDistributionFunctions, GasModel> > BCVector_PDFs;
BCPARAMETER_INSTANTIATE( BCVector_PDFs )


} //namespace SANS
