// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSRANSSA_H
#define TRAITSRANSSA_H

//#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

template <class PhysDim_>
class TraitsSizeRANSSA
{
public:
  typedef PhysDim_ PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = D+3;                   // total solution variables

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;           // solution/residual arrays

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>;        // matrices
};


template <class QType_, class GasModel_, class ViscosityModel_, class ThermalConductivityModel_>
class TraitsModelRANSSA
{
public:
  typedef QType_ QType;                                         // solution variables interpreter

  typedef GasModel_ GasModel;                                   // gas model

  typedef ViscosityModel_ ViscosityModel;                       // molecular viscosity model

  typedef ThermalConductivityModel_ ThermalConductivityModel;   // thermal conductivity model
};

}

#endif // TRAITSRANSSA_H
