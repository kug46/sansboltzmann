// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef GETBOLTZMANND2Q13TRAITSMODEL_H
#define GETBOLTZMANND2Q13TRAITSMODEL_H

#include "TraitsBoltzmannD2Q13.h"
//#include "TraitsNavierStokes.h"
//#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"

namespace SANS
{

template<class TraitsModel> struct getBoltzmannD2Q13TraitsModel;

template<class QType_, class GasModel_>
struct getBoltzmannD2Q13TraitsModel< TraitsModelBoltzmannD2Q13<QType_,GasModel_> >
{
  typedef TraitsModelBoltzmannD2Q13<QType_,GasModel_> type;
};

}

#endif
