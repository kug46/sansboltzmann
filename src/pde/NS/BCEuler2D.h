// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER2D_H
#define BCEULER2D_H

// 2-D Euler BC class
#undef BOOST_MPL_LIMIT_VECTOR_SIZE
#define BOOST_MPL_LIMIT_VECTOR_SIZE 30
#include <boost/mpl/vector.hpp>
#include <boost/mpl/vector/vector30.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "TraitsEuler.h"
#include "PDEEuler2D.h"
#include "SolutionFunction_Euler2D.h"
#include "SolutionFunction_NavierStokes2D.h"

#include "BCEuler2D_mitState.h"
#include "BCEuler2D_BN.h"
#include "BCEuler2D_Dirichlet_sansLG.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeTimeOut;
class BCTypeTimeIC;
class BCTypeSymmetry;
class BCTypeInflowSupersonic;
class BCTypeInflowSubsonic_sHqt;

class BCTypeFullState_mitState;

template <class SolutionType>
class BCTypeInflowSubsonic_sHqt_Profile;

class BCTypeOutflowSubsonic_Pressure;

template <class BCType, class PDEEuler2D>
class BCEuler2D;

template <class BCType>
struct BCEuler2DParams;


template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCEuler2DVector = boost::mpl::vector23<
    BCNone<PhysD2,PDETraitsSize<PhysD2>::N>,
    SpaceTimeBC< BCEuler2D<BCTypeTimeOut,                PDEEuler2D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCEuler2D<BCTypeTimeIC,                 PDEEuler2D<PDETraitsSize, PDETraitsModel>> >,
    SpaceTimeBC< BCEuler2D<BCTypeTimeIC_Function,        PDEEuler2D<PDETraitsSize, PDETraitsModel>> >,
    BCEuler2D<BCTypeSymmetry,                            PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeSymmetry_mitState,                   PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeInflowSupersonic,                    PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeInflowSubsonic_sHqt,                 PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeOutflowSubsonic_Pressure,            PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState,   PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN,         PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeFullState_mitState,                  PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState,       PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState,        PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeInflowSubsonic_sHqt_BN,              PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeInflowSubsonic_PtHa_BN,              PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeFunction_mitState,                   PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeFunctionInflow_sHqt_mitState,        PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeFunctionInflow_sHqt_BN,              PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeFunctionInflow_PtHa_BN,              PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeReflect_mitState,                    PDEEuler2D<PDETraitsSize, PDETraitsModel>>,
    BCEuler2D<BCTypeFullStateRadialInflow_mitState,      PDEEuler2D<PDETraitsSize, PDETraitsModel>>
                                          >;

} //namespace SANS

#endif  // BCEULER2D_H
