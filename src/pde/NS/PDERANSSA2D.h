// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDERANSSA2D_H
#define PDERANSSA2D_H

//#define SMOOTHR

// 2-D compressible RANS with Spalart-Allmaras PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/smoothmath.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Topology/Dimension.h"
#include "pde/AnalyticFunction/Function2D.h"

#include "GasModel.h"
#include "SAVariable2D.h"
#include "PDENavierStokes2D.h"

#include <iostream>
#include <string>
#include <cmath> // sqrt

//#define SANS_SA_QCR

namespace SANS
{

// power<N,T>(x):  power template
// reference: Walter Brown, C++ Meta<Programming> Concepts and Results
// e.g. power<3>(x) = x*x*x;

template <int N, class T>
inline T power( const T& x )
{
  return ((N == 0) ? 1 : ((N == 1) ? x : power<N%2,T>(x) * power<N/2,T>(x*x)));
}


// forward declare: RANS with SA variables interpreter
template<class QEulerType, template <class> class PDETraitsSize>
class QRANSSA2D;

//----------------------------------------------------------------------------//
// PDE class: 2-D compressible RANS with Spalart-Allmaras turbulence model
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//   QType                    solution variable set (e.g. primitive)
//   ViscosityModel           molecular viscosity model type
//   PDETraitsSize            define PDE size-related features
//     N, D                   PDE size, physical dimensions
//     ArrayQ                 solution/residual arrays
//     MatrixQ                matrices (e.g. flux jacobian)
//   PDETraitsModel           define PDE model-related features
//     QType                  solution variable set (e.g. primitive)
//     QInterpret             solution variable interpreter
//     GasModel               gas model (e.g. ideal gas law)
//     ViscosityModel         molecular viscosity model (e.g. Sutherland)
//     ThermalConductivityModel   thermal conductivity model (e.g. const Prandtl number)
//
// member functions:
//   .hasFluxAdvectiveTime     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//   .masterState        unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDERANSSA2D : public PDENavierStokes2D<PDETraitsSize, PDETraitsModel>
{
public:
  typedef PhysD2 PhysDim;

  typedef PDENavierStokes2D<PDETraitsSize, PDETraitsModel> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;                                        // cartesian vectors

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef typename PDETraitsModel::QType QType;
  typedef QRANSSA2D<QType, PDETraitsSize> QInterpret;                                 // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                                 // gas model

  typedef typename PDETraitsModel::ViscosityModel ViscosityModel;                     // molecular viscosity model

  typedef typename PDETraitsModel::ThermalConductivityModel ThermalConductivityModel; // thermal conductivity model

  typedef SAVariableType2DParams VariableType2DParams;

  // Solution pointer rather than MMS pointer due to distance function
  typedef Function2DBase<ArrayQ<Real>> FunctionBase;

  PDERANSSA2D( const GasModel& gas, const ViscosityModel& visc,
         const ThermalConductivityModel& tcond,
         const EulerResidualInterpCategory cat = Euler_ResidInterp_Raw,
         const RoeEntropyFix entropyFix = eVanLeer,
         const Real ntref = 1,
         const std::shared_ptr<FunctionBase>& soln = nullptr) :
      BaseType( gas, visc, tcond, cat, entropyFix, nullptr ),
      soln_(soln),
      ntref_(ntref),
      qInterpret_(gas, ntref)
  {
    setCoeffients();
  }

  static const int iCont = PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::iCont;
  static const int ixMom = PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::ixMom;
  static const int iyMom = PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::iyMom;
  static const int iEngy = PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::iEngy;
  static const int iSA   = iEngy + 1;

  PDERANSSA2D& operator=( const PDERANSSA2D& ) = delete;

  // flux components
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return true; }
  bool needsSolutionGradientforSource() const { return true; }

  bool hasForcingFunction() const { return soln_ != nullptr; }

  // unsteady conservative flux: U(Q)
  template<class T>
  void fluxAdvectiveTime(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& f ) const
  {
    fluxAdvectiveTime(x, y, time, q, f);
  }

  // unsteady conservative flux: U(Q)
  template<class T>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& f ) const
  {
    ArrayQ<T> ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    f += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(x, y, time, q, J);
  }

  template<class T>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const;


  template<class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& dU ) const
  {
    qInterpret_.conservativePerturbation(q, dq, dU);
  }

  template<class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& dU ) const
  {
    perturbedMasterState(x, y, time, q, dq, dU);
  }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& uConsx, ArrayQ<Tf>& uConsy  ) const;

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& uConsxx, ArrayQ<Tf>& uConsxy, ArrayQ<Tf>& uConsyy ) const;

  template<class T>
  void masterState(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
  {
    masterState(x, y, time, q, uCons);
  }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& uConsx, ArrayQ<Tf>& uConsy  ) const
  {
    masterStateGradient(x, y, time, q, qx, qy, uConsx, uConsy);
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& uConsxx, ArrayQ<Tf>& uConsxy, ArrayQ<Tf>& uConsyy ) const

  {
    masterStateHessian(x, y, time, q, qx, qy, qxx, qxy, qyy, uConsxx, uConsxy, uConsyy);
  }

  template<class T>
  void adjointState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const;

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void adjointStateGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& uConsx, ArrayQ<Tf>& uConsy  ) const;

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void adjointStateHessian(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& uConsxx, ArrayQ<Tf>& uConsxy, ArrayQ<Tf>& uConsyy ) const;

  template<class T>
  void adjointState(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
  {
    masterState(x, y, time, q, uCons);
  }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void adjointStateGradient(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& uConsx, ArrayQ<Tf>& uConsy  ) const
  {
    adjointStateGradient(x, y, time, q, qx, qy, uConsx, uConsy);
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void adjointStateHessian(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& uConsxx, ArrayQ<Tf>& uConsxy, ArrayQ<Tf>& uConsyy ) const

  {
    adjointStateHessian(x, y, time, q, qx, qy, qxx, qxy, qyy, uConsxx, uConsxy, uConsyy);
  }

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    jacobianMasterState(x, y, time, q, dudq);
  }


  // conversion from entropy variables dudv
  template<class Tq>
  void adjointVariableJacobian(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const
  {
    adjointVariableJacobian(x, y, time, q, dudv);
  }


  // conversion to entropy variables dvdu
  template<class Tq>
  void adjointVariableJacobianInverse(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const
  {
    adjointVariableJacobianInverse(x, y, time, q, dvdu);
  }


  // conversion from entropy variables dudv
  template<class Tq>
  void adjointVariableJacobian(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const;


  template<class Tq>
  void adjointVariableJacobianGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tq>& qx, const ArrayQ<Tq>& qy, MatrixQ<Tq>& dudvx, MatrixQ<Tq>& dudvy ) const;

  // conversion to entropy variables dvdu
  template<class Tq>
  void adjointVariableJacobianInverse(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& dist,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
     fluxAdvective(x, y, time, q, f, g);
  }

  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;


  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor = 1 ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& dist,
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor = 1  ) const
  {
    fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f, scaleFactor);
  }



  template <class Tq, class Tqp, class Tf>
  void fluxAdvectiveUpwindLinear(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& dq,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class Tq, class Tqp, class Tf>
  void fluxAdvectiveUpwindLinear(
      const Real& dist,
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& dq,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    fluxAdvectiveUpwindLinear(x, y, time, qL, dq, nx, ny, f);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime( const Real& dist,
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
  {
    fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tu, class Tf>
  void perturbedFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tu>& du,
      ArrayQ<Tf>& dF, ArrayQ<Tf>& dG ) const;
  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tu, class Tf>
  void perturbedFluxAdvective(
      const Real& dist, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const ArrayQ<Tu>& du,
      ArrayQ<Tf>& dF, ArrayQ<Tf>& dG ) const
  {
    perturbedFluxAdvective(x, y, time, q, du, dF, dG);
  }


  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;
  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& dist, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
  {
    jacobianFluxAdvective(x, y, time, q, dfdu, dgdu);
  }


  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const;
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& dist, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const
  {
    jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, nx, ny, a);
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Tf>& a ) const;
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& dist, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Tf>& a ) const
  {
    jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, time, q, nx, ny, nt, a);
  }

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const;

  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective( const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const
  {
    strongFluxAdvective(x, y, time, q, qx, qy, strongPDE);
  }


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    fluxViscous(x, y, time, q, qx, qy, f, g);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& distL, const Real& distR,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
  {
    perturbedGradFluxViscous(x, y, time, q, qx, qy, dux, duy, df, dg);
  }

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;


  // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
  {
    perturbedGradFluxViscous(x, y, time, Kxx, Kxy, Kyx, Kyy, dux, duy, df, dg);
  }

  // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {
    diffusionViscous(x, y, time, q, qx ,qy, kxx, kxy, kyx, kyy);
  }

  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const
  {
    strongFluxViscous(x,y,time,q,qx,qy,qxx,qxy,qyy,strongPDE);
  }

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    fluxViscousSpaceTime(x, y, time, q, qx, qy, qt, fx, fy, ft);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& dist,const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscousSpaceTime(x, y, time, qL, qxL, qyL, qtL, qR, qxR, qyR, qtR, nx, ny, nt, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ< Ts >& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("Calling wrong source");
  }

  template <class Tq, class Tg, class Ts>
  void source(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ< Ts >& source ) const;

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;
    ArrayQ<Tgg> qqy = qy + qpy;

    source(dist, x, y, time, qq, qqx, qqy, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    source(dist, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Real& dist,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    source(dist, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(dist, x, y, time, q, qx, qy, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& distL, const Real& xL, const Real& yL,
      const Real& distR, const Real& xR, const Real& yR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& distL, const Real& xL, const Real& yL,
      const Real& distR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(xL, yL, xR, yR, time, qL, qR, s);
  }

  // linear change in source in response to linear perturbations du, dux, duy
  template <class Tq, class Tg, class Tu, class Tgu, class Ts>
  void perturbedSource(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tu>& du, const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, ArrayQ<Ts>& dS ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variable gradients: d(S)/d(Ux), d(S)/d(Uy)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    SANS_DEVELOPER_EXCEPTION("Calling wrong jacobianGradientSource");
  }

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const;

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const;
//
//  template <class Tq, class Tg, class Th, class Ts>
//  void jacobianGradientSourceGradient(
//      const Real& dist, const Real& distx, const Real& disty, const Real& x, const Real& y, const Real& time,
//      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
//      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
//      MatrixQ<Ts>& div_dsdgradu ) const;

  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Ts>& div_dsdgradu ) const;

  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradientHACK(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Ts>& div_dsdgradu ) const;

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue(
      const Real& dist, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& divSdotN ) const;

  // right-hand-side forcing function
  void forcingFunction( const Real& dist, const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const;

  // characteristic speed (needed for timestep)
  template<class T>
  void speedCharacteristic( const Real& dist, Real, Real, Real, Real dx, Real dy, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template<class T>
  void speedCharacteristic( const Real& dist, Real, Real, Real, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template<class T>
  void speedCharacteristic( Real, Real, Real, const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& dist, const Real& x, const Real& y, const Real& time,
                       const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(x, y, time, q, dq, maxChangeFraction, updateFraction);
  }


  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // set from variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  template<class T, class Varibles>
  void setDOFFrom( ArrayQ<T>& q, const SAVariableType2D<Varibles>& data ) const;

  template<class Varibles>
  ArrayQ<Real> setDOFFrom( const SAVariableType2D<Varibles>& data ) const;

  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  const QInterpret& variableInterpreter() const { return qInterpret_; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceRow(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& dsdu ) const;

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceRow(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& dsdux, ArrayQ<Ts>& dsduy ) const;
  //
  template<class Tq, class Ts> //HELPER FUNCTION CONSTRUCTS dv/du*mat*du/dv
  void jacobianHelperHACK(const Real& x, const Real& y, const Real& time,
                          const ArrayQ<Tq>& q,  const MatrixQ<Ts>& mat, MatrixQ<Ts>& matHACK) const;

  void setCoeffients();                 // default coefficients (initially hard-coded)

  const std::shared_ptr<FunctionBase> soln_;
  const Real ntref_;
  const QInterpret qInterpret_;         // solution variable interpreter class

  using BaseType::gas_;
  using BaseType::visc_;
  using BaseType::tcond_;
  using BaseType::roe_;
  using BaseType::ismailRoe_;
  using BaseType::entropyFix_;
  using BaseType::cat_;

  Real cb1_, cb2_, sigma_, vk_, cw1_, cw2_, cw3_, cv1_, ct1_, ct2_, ct3_, ct4_;
  Real prandtlEddy_;
  Real rlim_;
  Real cv2_, cv3_;
  Real cn1_;
#ifdef SANS_SA_QCR
  Real cr1_;
#endif
};


// SA2012 constants
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::setCoeffients()
{
  cb1_   = 0.1355;
  cb2_   = 0.622;
  sigma_ = 2./3.;
  vk_    = 0.41;
  cw1_   = cb1_/(vk_*vk_) + (1 + cb2_)/sigma_;
  cw2_   = 0.3;
  cw3_   = 2;
  cv1_   = 7.1;
  ct1_   = 1;
  ct2_   = 2;
  ct3_   = 1.2;
  ct4_   = 0.5;
  prandtlEddy_ = 0.9;
  rlim_  = 10;
  cv2_   = 1 - 0.3;
  cv3_   = 1 - 0.1;
  cn1_   = 16;

#ifdef SANS_SA_QCR
  cr1_   = 0.3;
#endif
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  T rho = 0, nt = 0;

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalSA( q, nt );

  uCons(iSA) = rho*nt/ntref_;

  BaseType::masterState( x, y, time, q, uCons );
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::masterStateGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& uConsx, ArrayQ<Tf>& uConsy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq rho = 0, u = 0, v = 0, t = 0, nt = 0;
  T rhox = 0, ux = 0, vx = 0, tx = 0;
  T rhoy = 0, uy = 0, vy = 0, ty = 0;
  T ntx = 0, nty = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalSA( q, nt );
  qInterpret_.evalGradient(q, qx, rhox, ux, vx, tx);
  qInterpret_.evalGradient(q, qy, rhoy, uy, vy, ty);

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  uConsx(iSA) = (rhox*nt + rho*ntx)/ntref_;
  uConsy(iSA) = (rhoy*nt + rho*nty)/ntref_;

  BaseType::masterStateGradient(x, y, time, q, qx, qy, uConsx, uConsy);
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::masterStateHessian(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& uConsxx, ArrayQ<Tf>& uConsxy, ArrayQ<Tf>& uConsyy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho =0, u=0, v=0, t = 0, nt=0;
  Tqg rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0, ntx=0, nty = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  // evaluate second derivatives
  T rhoxx = 0, rhoxy = 0, rhoyy = 0;
  T uxx = 0, uxy = 0, uyy = 0;
  T vxx = 0, vxy = 0, vyy = 0;
  T txx = 0, txy = 0, tyy = 0;
  T ntxx = 0, ntxy = 0, ntyy = 0;

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  qInterpret_.evalSecondGradient( q, qx, qy,
                                  qxx, qxy, qyy,
                                  rhoxx, rhoxy, rhoyy,
                                  uxx, uxy, uyy,
                                  vxx, vxy, vyy,
                                  txx, txy, tyy );

  qInterpret_.evalSAHessian( q, qx, qx, qxx, ntxx);
  qInterpret_.evalSAHessian( q, qx, qy, qxy, ntxy);
  qInterpret_.evalSAHessian( q, qy, qy, qyy, ntyy);

  uConsxx(iSA) = (rhoxx*nt + 2.*rhox*ntx + rho*ntxx)/ntref_;
  uConsxy(iSA) = (rhoxy*nt + rhox*nty + rhoy*ntx + rho*ntxy)/ntref_;
  uConsyy(iSA) = (rhoyy*nt + 2.*rhoy*nty + rho*ntyy)/ntref_;

  BaseType::masterStateHessian(x, y, time, q, qx, qy, qxx, qxy, qyy, uConsxx, uConsxy, uConsyy);
}

// unsteady conservative flux Jacobian: dU(Q)/dQ
// NOTE: assumes SA variable independent of NS variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  ArrayQ<T> rho_q, nt_q;
  T rho = 0, nt = 0;

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalSA( q, nt );

  qInterpret_.evalDensityJacobian( q, rho_q );
  qInterpret_.evalSAJacobian( q, nt_q );

  dudq(iCont,4) = 0;
  dudq(ixMom,4) = 0;
  dudq(iyMom,4) = 0;
  dudq(iEngy,4) = 0;

  dudq(iSA,0) = rho_q[0]*(nt/ntref_) + rho*(nt_q[0]/ntref_);
  dudq(iSA,1) = rho_q[1]*(nt/ntref_) + rho*(nt_q[1]/ntref_);
  dudq(iSA,2) = rho_q[2]*(nt/ntref_) + rho*(nt_q[2]/ntref_);
  dudq(iSA,3) = rho_q[3]*(nt/ntref_) + rho*(nt_q[3]/ntref_);
  dudq(iSA,4) = rho_q[4]*(nt/ntref_) + rho*(nt_q[4]/ntref_);

  BaseType::jacobianMasterState( x, y, time, q, dudq );
}



// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::adjointState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& V ) const
{
  T nt = 0;

//  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalSA( q, nt );

//  V(iSA) = k_*nt;
  V(iSA) = nt/ntref_;

  BaseType::adjointState( x, y, time, q, V );

//  V(iCont) -= 0.5*k_*nt*nt;

  V(iCont) -= 0.5*(nt/ntref_)*(nt/ntref_);

}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::adjointStateGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq nt = 0;
//  T rhox = 0, ux = 0, vx = 0, tx = 0;
//  T rhoy = 0, uy = 0, vy = 0, ty = 0;
  T ntx = 0, nty = 0;

//  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalSA( q, nt );
//  qInterpret_.evalGradient(q, qx, rhox, ux, vx, tx);
//  qInterpret_.evalGradient(q, qy, rhoy, uy, vy, ty);

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  Vx(iSA) = ntx/ntref_;
  Vy(iSA) = nty/ntref_;

  BaseType::adjointStateGradient(x, y, time, q, qx, qy, Vx, Vy);

  Vx(iCont) -= nt*ntx/(ntref_*ntref_);
  Vy(iCont) -= nt*nty/(ntref_*ntref_);

}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::adjointStateHessian(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vyy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq nt=0;
  Tqg ntx=0, nty = 0;
//
//  qInterpret_.eval( q, rho, u, v, t );
//  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
//  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  // evaluate second derivatives
  T ntxx = 0, ntxy = 0, ntyy = 0;

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  qInterpret_.evalSAHessian( q, qx, qx, qxx, ntxx);
  qInterpret_.evalSAHessian( q, qx, qy, qxy, ntxy);
  qInterpret_.evalSAHessian( q, qy, qy, qyy, ntyy);

  Vxx(iSA) = ntxx/ntref_;
  Vxy(iSA) = ntxy/ntref_;
  Vyy(iSA) = ntyy/ntref_;

  BaseType::adjointStateHessian(x, y, time, q, qx, qy, qxx, qxy, qyy, Vxx, Vxy, Vyy);
//
  Vxx(iCont) -= (ntx*ntx + nt*ntxx)/(ntref_*ntref_);
  Vxy(iCont) -= (ntx*nty + nt*ntxy)/(ntref_*ntref_);
  Vyy(iCont) -= (nty*nty + nt*ntyy)/(ntref_*ntref_);
}




// entropy variable jacobian du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::adjointVariableJacobian(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const
{
  //entropy variable interpreter for adjoint
  Tq rho = 0, u = 0, v = 0, t = 0, e = 0, E = 0;

//  Real gamma = gas_.gamma();

  qInterpret_.eval( q, rho, u, v, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v);

//  Tq p = gas_.pressure(rho, t);

  Tq nt = 0;
  qInterpret_.evalSA( q, nt );

  dudv(iCont,4) = rho*nt/ntref_;
  dudv(ixMom,4) = rho*u*nt/ntref_;
  dudv(iyMom,4) = rho*v*nt/ntref_;
  dudv(iEngy,4) = rho*E*nt/ntref_;

  dudv(iSA,0) = dudv(iCont,4);
  dudv(iSA,1) = dudv(ixMom,4);
  dudv(iSA,2) = dudv(iyMom,4);
  dudv(iSA,3) = dudv(iEngy,4);
  dudv(iSA,4) = rho*(1. + nt*nt/(ntref_*ntref_));

  BaseType::adjointVariableJacobian(x, y, time, q, dudv);

}



// entropy variable jacobian du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::adjointVariableJacobianGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tq>& qx, const ArrayQ<Tq>& qy, MatrixQ<Tq>& dudvx, MatrixQ<Tq>& dudvy ) const
{

  Tq rho, u, v, t, nt;
  Tq rhox, ux, vx, tx, ntx;
  Tq rhoy, uy, vy, ty, nty;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

//  Tq p = gas_.pressure(rho, t);

  qInterpret_.evalSA( q, nt );

  Tq e = gas_.energy(rho, t);
  Tq ex = 0, ey = 0;
  gas_.energyGradient( rho, t, rhox, tx, ex );
  gas_.energyGradient( rho, t, rhoy, ty, ey );

  Tq E = e + 0.5*(u*u + v*v);

  Tq Ex = ex + (u*ux + v*vx);
  Tq Ey = ey + (u*uy + v*vy);

  dudvx(iCont,4) = (rhox*nt + rho*ntx)/ntref_;
  dudvx(ixMom,4) = (rhox*u*nt + rho*ux*nt + rho*u*ntx)/ntref_;
  dudvx(iyMom,4) = (rhox*v*nt + rho*vx*nt + rho*v*ntx)/ntref_;
  dudvx(iEngy,4) = (rhox*E*nt + rho*Ex*nt + rho*E*ntx)/ntref_;

  dudvx(iSA,0) = dudvx(iCont,4);
  dudvx(iSA,1) = dudvx(ixMom,4);
  dudvx(iSA,2) = dudvx(iyMom,4);
  dudvx(iSA,3) = dudvx(iEngy,4);
  dudvx(iSA,4) = rhox*(1. + nt*nt/(ntref_*ntref_)) + 2*rho*nt*ntx/(ntref_*ntref_);


  dudvy(iCont,4) = (rhoy*nt + rho*nty)/ntref_;
  dudvy(ixMom,4) = (rhoy*u*nt + rho*uy*nt + rho*u*nty)/ntref_;
  dudvy(iyMom,4) = (rhoy*v*nt + rho*vy*nt + rho*v*nty)/ntref_;
  dudvy(iEngy,4) = (rhoy*E*nt + rho*Ey*nt + rho*E*nty)/ntref_;

  dudvy(iSA,0) = dudvy(iCont,4);
  dudvy(iSA,1) = dudvy(ixMom,4);
  dudvy(iSA,2) = dudvy(iyMom,4);
  dudvy(iSA,3) = dudvy(iEngy,4);
  dudvy(iSA,4) = rhoy*(1. + nt*nt/(ntref_*ntref_)) + 2*rho*nt*nty/(ntref_*ntref_);


  BaseType::adjointVariableJacobianGradient(x, y, time, q, qx, qy, dudvx, dudvy);

}


// entropy variable jacobian du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::adjointVariableJacobianInverse(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const
{
  //entropy variable interpreter for adjoint
  Tq rho = 0, u = 0, v = 0, t = 0;

  qInterpret_.eval( q, rho, u, v, t );

  Tq nt = 0;
  qInterpret_.evalSA( q, nt );

  dvdu(iCont,4) =  -(nt/ntref_)/rho;
  dvdu(iSA,0) = dvdu(iCont,4);
  dvdu(iSA,4) = 1./rho;

  BaseType::adjointVariableJacobianInverse(x,y,time,q, dvdu);

  dvdu(iCont, 0) += (nt/ntref_)*(nt/ntref_)/rho;

}


// strong form of unsteady conservative flux: dU(Q)/dt
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& dist,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& utCons ) const
{
  T rho, nt;
  T rhot, ntt;

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalSA( q, nt );
  qInterpret_.evalDensityGradient( q, qt, rhot );
  qInterpret_.evalSAGradient( q, qt, ntt );

  utCons(iSA) += (rho*ntt + nt*rhot)/ntref_;

  BaseType::strongFluxAdvectiveTime( x, y, time, q, qt, utCons );
}


// advective flux: F(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  T rho = 0, u = 0, v = 0, t = 0, nt = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalSA( q, nt );

  f(iSA) += rho*u*nt/ntref_;
  g(iSA) += rho*v*nt/ntref_;

  BaseType::fluxAdvective( x, y, time, q, f, g );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor ) const
{
  Tq C1, eigu;
  if ((entropyFix_ == eVanLeer) or (entropyFix_ == eHarten))
  {
    roe_.fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f, C1, eigu, scaleFactor);
  }
  else if ((entropyFix_ == eIsmailRoeEntropyNeutral) or (entropyFix_ == eIsmailRoeHartenFix))
  {
    SANS_DEVELOPER_EXCEPTION("PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind - IsmailRoe not implemented");
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind - Unknown entropy fix.");
  }

  Tq rhoL, uL, vL, tL, ntL;
  Tq rhoR, uR, vR, tR, ntR;

  qInterpret_.eval( qL, rhoL, uL, vL, tL );
  qInterpret_.evalSA( qL, ntL );
  qInterpret_.eval( qR, rhoR, uR, vR, tR );
  qInterpret_.evalSA( qR, ntR );

  // Average convective flux
  f(iSA) += 0.5*(nx*(rhoL*uL*ntL) + ny*(rhoL*vL*ntL))/ntref_;
  f(iSA) += 0.5*(nx*(rhoR*uR*ntR) + ny*(rhoR*vR*ntR))/ntref_;

  // Roe stabilization
  Tq w1L = sqrt(rhoL);
  Tq w1R = sqrt(rhoR);

  Tq nt = (w1L*ntL + w1R*ntR) / (w1L + w1R);

  Tq delrhont = rhoR*ntR - rhoL*ntL;

  f(iSA) -= 0.5*( nt*C1 + delrhont*eigu )/ntref_*scaleFactor;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tqp, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindLinear(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& qp,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  Tf C1; Tq eigu;
  ArrayQ<Tf> du = 0;

  if ((entropyFix_ == eVanLeer) or (entropyFix_ == eHarten))
  {
    roe_.fluxAdvectiveUpwindLinear(x, y, time, qL, qp, nx, ny, f, C1, eigu, du);
  }
  else if ((entropyFix_ == eIsmailRoeEntropyNeutral) or (entropyFix_ == eIsmailRoeHartenFix))
  {
    SANS_DEVELOPER_EXCEPTION("PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindLinear - IsmailRoe not implemented");
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindLinear - Unknown entropy fix.");
  }

  Tq rhoL, uL, vL, tL, ntL;

  qInterpret_.eval( qL, rhoL, uL, vL, tL );
  qInterpret_.evalSA( qL, ntL );

  Tf delrhont;
  qInterpret_.conservativeSAPerturbation(qL, qp, delrhont);

  // Average convective flux
  f(iSA) += (nx*(rhoL*uL*ntL) + ny*(rhoL*vL*ntL))/ntref_;
  f(iSA) += ( ntL*C1 + delrhont*eigu )/ntref_;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{
  SANS_DEVELOPER_EXCEPTION( "Spacetime Roe Scheme Not Yet Implemented" );
}

// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  Tq u, v, nt = 0;

  qInterpret_.evalVelocity( q, u, v );
  qInterpret_.evalSA( q, nt );

//  dfdu(iCont,4) = 0;
//  dfdu(ixMom,4) = 0;
//  dfdu(iyMom,4) = 0;
//  dfdu(iEngy,4) = 0;
//
//  dgdu(iCont,4) = 0;
//  dgdu(ixMom,4) = 0;
//  dgdu(iyMom,4) = 0;
//  dgdu(iEngy,4) = 0;

  dfdu(iSA,0) += -u*nt/ntref_;
  dfdu(iSA,1) +=    nt/ntref_;
//  dfdu(iSA,2) += 0;
//  dfdu(iSA,3) += 0;
  dfdu(iSA,4) +=  u;

  dgdu(iSA,0) += -v*nt/ntref_;
//  dgdu(iSA,1) += 0;
  dgdu(iSA,2) += nt/ntref_;
//  dgdu(iSA,3) += 0;
  dgdu(iSA,4) +=  v;

  BaseType::jacobianFluxAdvective( x, y, time, q, dfdu, dgdu );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& Nx, const Real& Ny, MatrixQ<Tf>& mtx ) const
{
  BaseType::jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, Nx, Ny, mtx);

  Real nMag = sqrt(Nx*Nx + Ny*Ny );
  Real eps0 = 1.e-9;
  Real nMag2 = smoothabsP(nMag, eps0);

  Real nx = Nx/nMag2;
  Real ny = Ny/nMag2;

  Real gamma = gas_.gamma();
  Tq rho, u, v, t, nt;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalSA( q, nt );

  Real gm1 = gamma - 1.0;

  //  Tq qs  = -ny*u + nx*v;
  Tq qn  =  nx*u + ny*v;
  Tq q2  =  u*u + v*v;

  Tq c  = gas_.speedofSound( t ) ;
  Tq c2 = c*c;

  //pre-multiply the eigenvalue by the normal magnitude to save time
  Tq eigm = fabs(qn - c);
  Tq eigp = fabs(qn + c);
  Tq eigu   = fabs(qn);

  //HARTEN ENTROPY FIX
  Real eps1 = 1e-2;
  Tq eps    = c*eps1;

  if (eigm < eps)
    eigm = 0.5*(eps + eigm*eigm/eps);

  if (eigp < eps)
    eigp = 0.5*(eps + eigp*eigp/eps);

  if (eigu < eps)
    eigu = 0.5*(eps + eigu*eigu/eps);

  eigm *= nMag;
  eigp *= nMag;
  eigu *= nMag;

  mtx(4,0) += nt/ntref_* (2*c*qn*(eigm - eigp) + q2*(gm1)*(eigp+eigm - 2*eigu) )*0.25/c2;
  mtx(4,1) += nt/ntref_* ( c*nx*(eigp - eigm)  - u*gm1*(eigp+eigm - 2*eigu) )*0.5/c2;
  mtx(4,2) += nt/ntref_* ( c*ny*(eigp - eigm)  - v*gm1*(eigp+eigm - 2*eigu) )*0.5/c2;
  mtx(4,3) += nt/ntref_* ( gm1*(eigp+eigm - 2*eigu) )*0.5/c2;
  mtx(4,4) += eigu;

}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION( "jacobianFluxAdvectiveAbsoluteValueSpaceTime Not Yet Implemented" );
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq rho, u, v, t, nt;
  T rhox, ux, vx, tx, ntx;
  T rhoy, uy, vy, ty, nty;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  strongPDE(iSA) += (rhox*u*nt + rho*ux*nt + rho*u*ntx
                  + rhoy*v*nt + rho*vy*nt + rho*v*nty)/ntref_;

  BaseType::strongFluxAdvective( x, y, time, q, qx, qy, strongPDE );
//  SANS_DEVELOPER_EXCEPTION( "RANS 2D strongFluxAdvective: not tested" );
}


// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  // SA fluxes

  Tq rho=0, u=0, v=0, t=0, nt=0;
  T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;
  T  ntx=0, nty=0;
  Tq mu, k;

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalTemperature( q, t );
  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  // SA model viscous terms

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  Tq chi  = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);

  if (nt < 0)
  {
    Tq fn  = (cn1_ + chi3) / (cn1_ - chi3);
    f(iSA) -= (mu + rho*nt*fn)*ntx/sigma_/ntref_;
    g(iSA) -= (mu + rho*nt*fn)*nty/sigma_/ntref_;
  }
  else
  {
    f(iSA) -= (mu + rho*nt)*ntx/sigma_/ntref_;
    g(iSA) -= (mu + rho*nt)*nty/sigma_/ntref_;
  }

  // Reynolds stresses

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Tq fv1 = chi3 / (chi3 + power<3,Tq>(cv1_));
    Tq muEddy = rho*nt*fv1;
    Real Cp = this->gas_.Cp();

    // Augment the molecular viscosity and thermal conductivity
    mu += muEddy;
    k  += muEddy*Cp/prandtlEddy_;

#ifdef SANS_SA_QCR
    Tq lambdaEddy = -2./3. * muEddy;

    T tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy);
    T tauRxy = muEddy*(uy + vx);
    T tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy);

    T magGradV = sqrt(ux*ux + uy*uy + vx*vx + vy*vy);

    T Wxy = (uy - vx);

    T Oxy = magGradV == 0.0 ? T(0) : Wxy/magGradV;
    T Oyx = -Oxy;

    // tauR_ij,QCR = tauR_ij - cr1*(O_ik*tauR_jk + O_jk*tauR_ik)

    // qcrxx = cr1*(Oxx*tauRxx + Oxy*tauRxy + Oxx*tauRxx + Oxy*tauRxy)
    // qcrxy = cr1*(Oxx*tauRyx + Oxy*tauRyy + Oyx*tauRxx + Oyy*tauRxy)
    // qcryx = cr1*(Oyx*tauRxx + Oyy*tauRxy + Oxx*tauRyx + Oxy*tauRyy)
    // qcryy = cr1*(Oyx*tauRyx + Oyy*tauRyy + Oyx*tauRyx + Oyy*tauRyy)

    T qcrxx = cr1_*2*(Oxy*tauRxy);
    T qcrxy = cr1_*  (Oxy*tauRyy + Oyx*tauRxx);
    T qcryy = cr1_*2*(Oyx*tauRxy);

    f(ixMom) += qcrxx;
    f(iyMom) += qcrxy;
    f(iEngy) += u*qcrxx + v*qcrxy;

    g(ixMom) += qcrxy;
    g(iyMom) += qcryy;
    g(iEngy) += u*qcrxy + v*qcryy;
#endif
  }

  // Compute the viscous flux
  BaseType::fluxViscous(u, v, mu, k,
                        ux, vx, tx,
                        uy, vy, ty,
                        f, g );
}


// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscous(x, y, time, qL, qxL, qyL, fL, gL);
  fluxViscous(x, y, time, qR, qxR, qyR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;
#else
  // SA fluxes

  T rhoL, uL, vL, tL, ntL;
  T rhoR, uR, vR, tR, ntR;
  T ntxL, ntyL;
  T ntxR, ntyR;
  T rho, t, nt;
  T ntx, nty;
  T mu;

  qInterpret_.evalDensity( qL, rhoL );
  qInterpret_.evalDensity( qR, rhoR );
  qInterpret_.evalTemperature( qL, tL );
  qInterpret_.evalTemperature( qR, tR );

  rho = 0.5*(rhoL + rhoR);
  t   = 0.5*(tL + tR);
  mu = visc_.viscosity( t );

  qInterpret_.evalSA( qL, ntL );
  qInterpret_.evalSA( qR, ntR );
  qInterpret_.evalSAGradient( qL, qxL, ntxL );
  qInterpret_.evalSAGradient( qL, qyL, ntyL );
  qInterpret_.evalSAGradient( qR, qxR, ntxR );
  qInterpret_.evalSAGradient( qR, qyR, ntyR );

  nt  = 0.5*(ntL + ntR);
  ntx = 0.5*(ntxL + ntxR);
  nty = 0.5*(ntyL + ntyR);

  if (nt < 0)
  {
    T chi = rho*nt/mu;
    T fn  = (cn1_ + power<3,T>(chi)) / (cn1_ - power<3,T>(chi));
    f(iSA) -= (mu + rho*nt*fn)*(nx*ntx + ny*nty)/sigma_;
  }
  else
  {
    f(iSA) -= (mu + rho*nt)*(nx*ntx + ny*nty)/sigma_;
  }

  // Reynolds stresses

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    T rhoxL, rhoyL, uxL, uyL, vxL, vyL, txL, tyL;
    T rhoxR, rhoyR, uxR, uyR, vxR, vyR, txR, tyR;
    T u, v;
    T ux, uy, vx, vy, tx, ty;
    T tauxx, tauxy, tauyy;
    T muEddy;
    T Cp, qheat;

    qInterpret_.eval( qL, rhoL, uL, vL, tL );
    qInterpret_.eval( qR, rhoR, uR, vR, tR );
    qInterpret_.evalGradient( qL, qxL, rhoxL, uxL, vxL, txL );
    qInterpret_.evalGradient( qL, qyL, rhoyL, uyL, vyL, tyL );
    qInterpret_.evalGradient( qR, qxR, rhoxR, uxR, vxR, txR );
    qInterpret_.evalGradient( qR, qyR, rhoyR, uyR, vyR, tyR );

    u = 0.5*(uL + uR);
    v = 0.5*(vL + vR);
    t = 0.5*(tL + tR);

    ux = 0.5*(uxL + uxR);
    uy = 0.5*(uyL + uyR);
    vx = 0.5*(vxL + vxR);
    vy = 0.5*(vyL + vyR);
    tx = 0.5*(txL + txR);
    ty = 0.5*(tyL + tyR);

    T chi = rho*nt/mu;
    T fv1 = power<3,T>(chi) / (power<3,T>(chi) + power<3>(cv1_));
    muEddy = rho*nt*fv1;
    //std::cout << "muEddy = " << muEddy << std::endl;
    //std::cout << "fv1 = " << fv1 << std::endl;

    tauxx = muEddy*(2*ux - (2/3.)*(ux + vy));
    tauxy = muEddy*(uy + vx);
    tauyy = muEddy*(2*vy - (2/3.)*(ux + vy));

    Cp = this->gas_.Cp();
    qheat = (muEddy*Cp/prandtlEddy_)*(nx*tx + ny*ty);

    f(ixMom) -= nx*tauxx + ny*tauxy;
    f(iyMom) -= nx*tauxy + ny*tauyy;
    f(iEngy) -= qheat + u*(nx*tauxx + ny*tauxy) + v*(nx*tauxy + ny*tauyy);
  }

  BaseType::fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f );
#endif
}



// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tgu, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::perturbedGradFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
    ArrayQ<Tf>& dF, ArrayQ<Tf>& dG ) const
{
#ifdef SANS_SA_QCR //QCR HAS DIFFERENT SPARSITY PATTERN AND IS NOT LINEAR IN GRADIENT

  typedef typename promote_Surreal<Tq,Tg>::type T;

  MatrixQ<T> kxx = 0;
  MatrixQ<T> kxy = 0;
  MatrixQ<T> kyx = 0;
  MatrixQ<T> kyy = 0;
  diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);

  //kxx
  dF[ixMom] -= kxx(ixMom,0)*dux[0];
  dF[ixMom] -= kxx(ixMom,1)*dux[1];
  dF[ixMom] -= kxx(ixMom,2)*dux[2];

  dF[iyMom] -= kxx(iyMom,0)*dux[0];
  dF[iyMom] -= kxx(iyMom,1)*dux[1];
  dF[iyMom] -= kxx(iyMom,2)*dux[2];

  dF[iEngy] -= kxx(iEngy,0)*dux[0];
  dF[iEngy] -= kxx(iEngy,1)*dux[1];
  dF[iEngy] -= kxx(iEngy,2)*dux[2];
  dF[iEngy] -= kxx(iEngy,3)*dux[3];

  //kxy
  dF[ixMom] -= kxy(ixMom,0)*duy[0];
  dF[ixMom] -= kxy(ixMom,1)*duy[1];
  dF[ixMom] -= kxy(ixMom,2)*duy[2];

  dF[iyMom] -= kxy(iyMom,0)*duy[0];
  dF[iyMom] -= kxy(iyMom,1)*duy[1];
  dF[iyMom] -= kxy(iyMom,2)*duy[2];

  dF[iEngy] -= kxy(iEngy,0)*duy[0];
  dF[iEngy] -= kxy(iEngy,1)*duy[1];
  dF[iEngy] -= kxy(iEngy,2)*duy[2];

  //kyx
  dG[ixMom] -= kyx(ixMom,0)*dux[0];
  dG[ixMom] -= kyx(ixMom,1)*dux[1];
  dG[ixMom] -= kyx(ixMom,2)*dux[2];

  dG[iyMom] -= kyx(iyMom,0)*dux[0];
  dG[iyMom] -= kyx(iyMom,1)*dux[1];
  dG[iyMom] -= kyx(iyMom,2)*dux[2];

  dG[iEngy] -= kyx(iEngy,0)*dux[0];
  dG[iEngy] -= kyx(iEngy,1)*dux[1];
  dG[iEngy] -= kyx(iEngy,2)*dux[2];

  //kyy
  dG[ixMom] -= kyy(ixMom,0)*duy[0];
  dG[ixMom] -= kyy(ixMom,1)*duy[1];
  dG[ixMom] -= kyy(ixMom,2)*duy[2];

  dG[iyMom] -= kyy(iyMom,0)*duy[0];
  dG[iyMom] -= kyy(iyMom,1)*duy[1];
  dG[iyMom] -= kyy(iyMom,2)*duy[2];

  dG[iEngy] -= kyy(iEngy,0)*duy[0];
  dG[iEngy] -= kyy(iEngy,1)*duy[1];
  dG[iEngy] -= kyy(iEngy,2)*duy[2];
  dG[iEngy] -= kyy(iEngy,3)*duy[3];

  dF[iSA] -= kxx(iSA,0)*dux[0];
  dF[iSA] -= kxx(iSA,4)*dux[4];

  dG[iSA] -= kyy(iSA,0)*duy[0];
  dG[iSA] -= kyy(iSA,4)*duy[4];
#else
  // this RANSSA is linear in the gradient so the following works:
  fluxViscous(x, y, time, q, dqx, dqy, dF, dG);
#endif


}


// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tk, class Tgu, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::perturbedGradFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const MatrixQ<Tk>& kxx, const MatrixQ<Tk>& kxy, const MatrixQ<Tk>& kyx, const MatrixQ<Tk>& kyy,
    const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
    ArrayQ<Tf>& dF, ArrayQ<Tf>& dG ) const
{
#ifdef SANS_SA_QCR //QCR HAS DIFFERENT SPARSITY PATTERN AND IS NOT LINEAR IN GRADIENT

  typedef typename promote_Surreal<Tq,Tg>::type T;

  MatrixQ<T> kxx = 0;
  MatrixQ<T> kxy = 0;
  MatrixQ<T> kyx = 0;
  MatrixQ<T> kyy = 0;
  diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);

  //kxx
  dF[ixMom] -= kxx(ixMom,0)*dux[0];
  dF[ixMom] -= kxx(ixMom,1)*dux[1];
  dF[ixMom] -= kxx(ixMom,2)*dux[2];

  dF[iyMom] -= kxx(iyMom,0)*dux[0];
  dF[iyMom] -= kxx(iyMom,1)*dux[1];
  dF[iyMom] -= kxx(iyMom,2)*dux[2];

  dF[iEngy] -= kxx(iEngy,0)*dux[0];
  dF[iEngy] -= kxx(iEngy,1)*dux[1];
  dF[iEngy] -= kxx(iEngy,2)*dux[2];
  dF[iEngy] -= kxx(iEngy,3)*dux[3];

  //kxy
  dF[ixMom] -= kxy(ixMom,0)*duy[0];
  dF[ixMom] -= kxy(ixMom,1)*duy[1];
  dF[ixMom] -= kxy(ixMom,2)*duy[2];

  dF[iyMom] -= kxy(iyMom,0)*duy[0];
  dF[iyMom] -= kxy(iyMom,1)*duy[1];
  dF[iyMom] -= kxy(iyMom,2)*duy[2];

  dF[iEngy] -= kxy(iEngy,0)*duy[0];
  dF[iEngy] -= kxy(iEngy,1)*duy[1];
  dF[iEngy] -= kxy(iEngy,2)*duy[2];

  //kyx
  dG[ixMom] -= kyx(ixMom,0)*dux[0];
  dG[ixMom] -= kyx(ixMom,1)*dux[1];
  dG[ixMom] -= kyx(ixMom,2)*dux[2];

  dG[iyMom] -= kyx(iyMom,0)*dux[0];
  dG[iyMom] -= kyx(iyMom,1)*dux[1];
  dG[iyMom] -= kyx(iyMom,2)*dux[2];

  dG[iEngy] -= kyx(iEngy,0)*dux[0];
  dG[iEngy] -= kyx(iEngy,1)*dux[1];
  dG[iEngy] -= kyx(iEngy,2)*dux[2];

  //kyy
  dG[ixMom] -= kyy(ixMom,0)*duy[0];
  dG[ixMom] -= kyy(ixMom,1)*duy[1];
  dG[ixMom] -= kyy(ixMom,2)*duy[2];

  dG[iyMom] -= kyy(iyMom,0)*duy[0];
  dG[iyMom] -= kyy(iyMom,1)*duy[1];
  dG[iyMom] -= kyy(iyMom,2)*duy[2];

  dG[iEngy] -= kyy(iEngy,0)*duy[0];
  dG[iEngy] -= kyy(iEngy,1)*duy[1];
  dG[iEngy] -= kyy(iEngy,2)*duy[2];
  dG[iEngy] -= kyy(iEngy,3)*duy[3];

  dF[iSA] -= kxx(iSA,0)*dux[0];
  dF[iSA] -= kxx(iSA,4)*dux[4];

  dG[iSA] -= kyy(iSA,0)*duy[0];
  dG[iSA] -= kyy(iSA,4)*duy[4];
#else
  //using what we know about the sparsity of K

  dF[iSA] -= kxx(iSA,0)*dux[0];
  dF[iSA] -= kxx(iSA,4)*dux[4];

  dG[iSA] -= kyy(iSA,0)*duy[0];
  dG[iSA] -= kyy(iSA,4)*duy[4];

  BaseType::perturbedGradFluxViscous(x, y, time, kxx, kxy, kyx, kyy, dux, duy, dF, dG);
#endif


}



// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  // SA diffusion matrix

  Tq rho=0, u=0, v=0, t=0, nt=0;
  Tq mu, nu, k;

  qInterpret_.eval( q, rho, u, v, t );

  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  nu = mu/rho;

  qInterpret_.evalSA( q, nt );

  Tq ntr = nt/ntref_;

  Tq chi = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);

  if (nt < 0)
  {
    Tq fn  = (cn1_ + chi3) / (cn1_ - chi3);
    kxx(iSA,0) += -ntr*(nu + nt*fn)/sigma_;
    kxx(iSA,4) +=     (nu + nt*fn)/sigma_;

    kyy(iSA,0) += -ntr*(nu + nt*fn)/sigma_;
    kyy(iSA,4) +=     (nu + nt*fn)/sigma_;
  }
  else
  {
    kxx(iSA,0) += -ntr*(nu + nt)/sigma_;
    kxx(iSA,4) +=     (nu + nt)/sigma_;

    kyy(iSA,0) += -ntr*(nu + nt)/sigma_;
    kyy(iSA,4) +=     (nu + nt)/sigma_;
  }

  // Reynolds stresses diffusion matrix

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Real Cp = this->gas_.Cp();

    Tq fv1 = chi3 / (chi3 + power<3>(cv1_));
    Tq muEddy = rho*nt*fv1;

    // Augment the molecular viscosity and thermal conductivity
    mu += muEddy;
    k  += muEddy*Cp/prandtlEddy_;

#ifdef SANS_SA_QCR
    typedef typename promote_Surreal<Tq,Tg>::type T;

    Tq lambdaEddy = -2./3. * muEddy;

    T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;
    qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
    qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

    Tq rhoi = 1/rho;

    // velocity gradient Jacobian wrt conservative variables
    Tq  ux_rhox  = -u*rhoi;
    Tq& ux_rhoux = rhoi;

    Tq& uy_rhoy  = ux_rhox;
    Tq& uy_rhouy = rhoi;

    Tq  vx_rhox  = -v*rhoi;
    Tq& vx_rhovx = rhoi;

    Tq& vy_rhoy  = vx_rhox;
    Tq& vy_rhovy = rhoi;

    T tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy);
    T tauRxy = muEddy*(uy + vx);
    T tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy);

    Tq  tauRxx_ux = muEddy*2 + lambdaEddy;
    Tq& tauRxx_vy = lambdaEddy;
    Tq& tauRxy_uy = muEddy;
    Tq& tauRxy_vx = muEddy;
    Tq& tauRyy_ux = lambdaEddy;
    Tq  tauRyy_vy = muEddy*2 + lambdaEddy;

    T magGradV    = sqrt(ux*ux + uy*uy + vx*vx + vy*vy);
    T magGradV_ux = (magGradV == 0.0) ? T(0) : ux/magGradV;
    T magGradV_uy = (magGradV == 0.0) ? T(0) : uy/magGradV;
    T magGradV_vx = (magGradV == 0.0) ? T(0) : vx/magGradV;
    T magGradV_vy = (magGradV == 0.0) ? T(0) : vy/magGradV;
    T magGradV2   = magGradV*magGradV;

    T    Wxy    = (uy - vx);
    Real Wxy_uy = (1      );
    Real Wxy_vx = (   -  1);

    T Oxy    = (magGradV == 0.0) ? T(0) : Wxy/magGradV;
    T Oxy_ux = (magGradV == 0.0) ? T(0) :                 - Wxy*magGradV_ux/magGradV2;
    T Oxy_uy = (magGradV == 0.0) ? T(0) : Wxy_uy/magGradV - Wxy*magGradV_uy/magGradV2;
    T Oxy_vx = (magGradV == 0.0) ? T(0) : Wxy_vx/magGradV - Wxy*magGradV_vx/magGradV2;
    T Oxy_vy = (magGradV == 0.0) ? T(0) :                 - Wxy*magGradV_vy/magGradV2;

    T Oyx    = -Oxy;
    T Oyx_ux = -Oxy_ux;
    T Oyx_uy = -Oxy_uy;
    T Oyx_vx = -Oxy_vx;
    T Oyx_vy = -Oxy_vy;

    //qcrxx    = cr1_*2*(Oxy*tauRxy);
    T qcrxx_ux = cr1_*2*(Oxy_ux*tauRxy                );
    T qcrxx_uy = cr1_*2*(Oxy_uy*tauRxy + Oxy*tauRxy_uy);
    T qcrxx_vx = cr1_*2*(Oxy_vx*tauRxy + Oxy*tauRxy_vx);
    T qcrxx_vy = cr1_*2*(Oxy_vy*tauRxy                );

    //qcrxy    = cr1_*(Oxy*tauRyy    + Oyx*tauRxx);
    T qcrxy_ux = cr1_*(Oxy_ux*tauRyy + Oxy*tauRyy_ux + Oyx_ux*tauRxx + Oyx*tauRxx_ux);
    T qcrxy_uy = cr1_*(Oxy_uy*tauRyy                 + Oyx_uy*tauRxx                );
    T qcrxy_vx = cr1_*(Oxy_vx*tauRyy                 + Oyx_vx*tauRxx                );
    T qcrxy_vy = cr1_*(Oxy_vy*tauRyy + Oxy*tauRyy_vy + Oyx_vy*tauRxx + Oyx*tauRxx_vy);

    //qcryy    = cr1_*2*(Oyx*tauRxy);
    T qcryy_ux = cr1_*2*(Oyx_ux*tauRxy                );
    T qcryy_uy = cr1_*2*(Oyx_uy*tauRxy + Oyx*tauRxy_uy);
    T qcryy_vx = cr1_*2*(Oyx_vx*tauRxy + Oyx*tauRxy_vx);
    T qcryy_vy = cr1_*2*(Oyx_vy*tauRxy                );

    // Convert to derivatives w.r.t. master state
    T qcrxx_rhox  = qcrxx_ux*ux_rhox + qcrxx_vx*vx_rhox;
    T qcrxx_rhoy  = qcrxx_uy*uy_rhoy + qcrxx_vy*vy_rhoy;
    T qcrxx_rhoux = qcrxx_ux*ux_rhoux;
    T qcrxx_rhouy = qcrxx_uy*uy_rhouy;
    T qcrxx_rhovx = qcrxx_vx*vx_rhovx;
    T qcrxx_rhovy = qcrxx_vy*vy_rhovy;

    T qcrxy_rhox  = qcrxy_ux*ux_rhox + qcrxy_vx*vx_rhox;
    T qcrxy_rhoy  = qcrxy_uy*uy_rhoy + qcrxy_vy*vy_rhoy;
    T qcrxy_rhoux = qcrxy_ux*ux_rhoux;
    T qcrxy_rhouy = qcrxy_uy*uy_rhouy;
    T qcrxy_rhovx = qcrxy_vx*vx_rhovx;
    T qcrxy_rhovy = qcrxy_vy*vy_rhovy;

    T qcryy_rhox  = qcryy_ux*ux_rhox + qcryy_vx*vx_rhox;
    T qcryy_rhoy  = qcryy_uy*uy_rhoy + qcryy_vy*vy_rhoy;
    T qcryy_rhoux = qcryy_ux*ux_rhoux;
    T qcryy_rhouy = qcryy_uy*uy_rhouy;
    T qcryy_rhovx = qcryy_vx*vx_rhovx;
    T qcryy_rhovy = qcryy_vy*vy_rhovy;

    // d(Fv)/d(Ux)
    kxx(ixMom,0) -= qcrxx_rhox ;
    kxx(ixMom,1) -= qcrxx_rhoux;
    kxx(ixMom,2) -= qcrxx_rhovx;

    kxx(iyMom,0) -= qcrxy_rhox ;
    kxx(iyMom,1) -= qcrxy_rhoux;
    kxx(iyMom,2) -= qcrxy_rhovx;

    kxx(iEngy,0) -= u*qcrxx_rhox  + v*qcrxy_rhox ;
    kxx(iEngy,1) -= u*qcrxx_rhoux + v*qcrxy_rhoux;
    kxx(iEngy,2) -= u*qcrxx_rhovx + v*qcrxy_rhovx;

    // d(Fv)/d(Uy)
    kxy(ixMom,0) -= qcrxx_rhoy ;
    kxy(ixMom,1) -= qcrxx_rhouy;
    kxy(ixMom,2) -= qcrxx_rhovy;

    kxy(iyMom,0) -= qcrxy_rhoy ;
    kxy(iyMom,1) -= qcrxy_rhouy;
    kxy(iyMom,2) -= qcrxy_rhovy;

    kxy(iEngy,0) -= u*qcrxx_rhoy  + v*qcrxy_rhoy ;
    kxy(iEngy,1) -= u*qcrxx_rhouy + v*qcrxy_rhouy;
    kxy(iEngy,2) -= u*qcrxx_rhovy + v*qcrxy_rhovy;

    // d(Gv)/d(Ux)
    kyx(ixMom,0) -= qcrxy_rhox ;
    kyx(ixMom,1) -= qcrxy_rhoux;
    kyx(ixMom,2) -= qcrxy_rhovx;

    kyx(iyMom,0) -= qcryy_rhox ;
    kyx(iyMom,1) -= qcryy_rhoux;
    kyx(iyMom,2) -= qcryy_rhovx;

    kyx(iEngy,0) -= u*qcrxy_rhox  + v*qcryy_rhox ;
    kyx(iEngy,1) -= u*qcrxy_rhoux + v*qcryy_rhoux;
    kyx(iEngy,2) -= u*qcrxy_rhovx + v*qcryy_rhovx;

    // d(Gv)/d(Uy)
    kyy(ixMom,0) -= qcrxy_rhoy ;
    kyy(ixMom,1) -= qcrxy_rhouy;
    kyy(ixMom,2) -= qcrxy_rhovy;

    kyy(iyMom,0) -= qcryy_rhoy ;
    kyy(iyMom,1) -= qcryy_rhouy;
    kyy(iyMom,2) -= qcryy_rhovy;

    kyy(iEngy,0) -= u*qcrxy_rhoy  + v*qcryy_rhoy ;
    kyy(iEngy,1) -= u*qcrxy_rhouy + v*qcryy_rhouy;
    kyy(iEngy,2) -= u*qcrxy_rhovy + v*qcryy_rhovy;
#endif
  }

  // compute the diffusion matrix
  BaseType::diffusionViscous( rho, u, v, t, mu, k,
                              kxx, kxy,
                              kyx, kyy );

}


// gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tk>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  // SA diffusion matrix

  Tq rho=0, u=0, v=0, t=0, nt=0;
  T rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;
  T ntx = 0; T nty = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  Tq mu = visc_.viscosity( t );

  Tq mut = 0;

  visc_.viscosityJacobian(t, mut);
  T mux = mut*tx;
  T muy = mut*ty;

  Tq k = tcond_.conductivity( mu );

  Tq k_mu = 0;
  tcond_.conductivityJacobian(mu, k_mu);
  T kx = k_mu*mux;
  T ky = k_mu*muy;

  T nu  = mu/rho;
  T nux = mux/rho - mu*rhox/rho/rho;
  T nuy = muy/rho - mu*rhoy/rho/rho;

  Tq chi  = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);

  T  chix = rhox*nt/mu + rho*ntx/mu - rho*nt*mux/(mu*mu);
  T  chiy = rhoy*nt/mu + rho*nty/mu - rho*nt*muy/(mu*mu);


  if (nt < 0)
  {
    T fn  = (cn1_ + chi3) / (cn1_ - chi3);
    T fn_chi = 3*power<2,T>(chi)/(cn1_ - chi3) + 3*power<2,T>(chi)*(cn1_ + chi3)/power<2,T>(cn1_ - chi3);

    T fnx = fn_chi*chix;
    T fny = fn_chi*chiy;

//    kxx(iSA,0) += -nt/ntref_*(nu + nt*fn)/sigma_;
//    kxx(iSA,4) +=            (nu + nt*fn)/sigma_;
//
//    kyy(iSA,0) += -nt/ntref_*(nu + nt*fn)/sigma_;
//    kyy(iSA,4) +=            (nu + nt*fn)/sigma_;


    kxx_x(iSA,0) += (-ntx*(nu + nt*fn) - nt*(nux + ntx*fn + nt*fnx))/sigma_/ntref_;
    kxx_x(iSA,4) +=                         (nux + ntx*fn + nt*fnx)/sigma_;

    kyy_y(iSA,0) += (-nty*(nu + nt*fn) - nt*(nuy + nty*fn + nt*fny))/sigma_/ntref_;
    kyy_y(iSA,4) +=                         (nuy + nty*fn + nt*fny)/sigma_;
  }
  else
  {
//    kxx(iSA,0) += -nt/ntref_*(nu + nt)/sigma_;
//    kxx(iSA,4) +=            (nu + nt)/sigma_;
//
//    kyy(iSA,0) += -nt/ntref_*(nu + nt)/sigma_;
//    kyy(iSA,4) +=            (nu + nt)/sigma_;

    kxx_x(iSA,0) += (-ntx*(nu + nt) - nt*(nux + ntx))/sigma_/ntref_;
    kxx_x(iSA,4) +=                      (nux + ntx)/sigma_;

    kyy_y(iSA,0) += (-nty*(nu + nt) - nt*(nuy + nty))/sigma_/ntref_;
    kyy_y(iSA,4) +=                      (nuy + nty)/sigma_;
  }

  // Reynolds stresses diffusion matrix
  T muT = mu;
  T kT = k;

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Real cv13 = power<3>(cv1_);
    T fv1 = chi3 / (chi3 + cv13);
    T muEddy = rho*nt*fv1;

    T dfv1dchi = 3.0*cv13*chi*chi / power<2,T>(cv13 + chi3);

    T fv1x = dfv1dchi*chix;
    T fv1y = dfv1dchi*chiy;

    T muEddy_x = rhox*nt*fv1 + rho*ntx*fv1 + rho*nt*fv1x;
    T muEddy_y = rhoy*nt*fv1 + rho*nty*fv1 + rho*nt*fv1y;

    muT  += muEddy;
    mux += muEddy_x;
    muy += muEddy_y;


    Real Cp = this->gas_.Cp();
    Tq kmuEddy = Cp/prandtlEddy_;

    kT  += muEddy*Cp/prandtlEddy_;
    kx += kmuEddy*muEddy_x;
    ky += kmuEddy*muEddy_y;

#ifdef SANS_SA_QCR

    // evaluate second derivatives
    T rhoxx = 0, rhoxy = 0, rhoyy = 0;
    T uxx = 0, uxy = 0, uyy = 0;
    T vxx = 0, vxy = 0, vyy = 0;
    T txx = 0, txy = 0, tyy = 0;

    qInterpret_.evalSecondGradient( q, qx, qy,
                                    qxx, qxy, qyy,
                                    rhoxx, rhoxy, rhoyy,
                                    uxx, uxy, uyy,
                                    vxx, vxy, vyy,
                                    txx, txy, tyy );

    Tq rhoi   = 1/rho;
    Tq rhoi_x = -rhox*rhoi*rhoi;
    Tq rhoi_y = -rhoy*rhoi*rhoi;

    // velocity gradient Jacobian wrt conservative variables
    Tq  ux_rhox    = -u*rhoi;
    Tq  ux_rhox_x  = -ux*rhoi - u*rhoi_x;
    Tq  ux_rhox_y  = -uy*rhoi - u*rhoi_y;

    Tq& ux_rhoux   = rhoi;
    Tq& ux_rhoux_x = rhoi_x;
    Tq& ux_rhoux_y = rhoi_y;

    Tq& uy_rhoy    = ux_rhox;
    Tq& uy_rhoy_x  = ux_rhox_x;
    Tq& uy_rhoy_y  = ux_rhox_y;

    Tq& uy_rhouy   = rhoi;
    Tq& uy_rhouy_x = rhoi_x;
    Tq& uy_rhouy_y = rhoi_y;

    Tq  vx_rhox    = -v*rhoi;
    Tq  vx_rhox_x  = -vx*rhoi - v*rhoi_x;
    Tq  vx_rhox_y  = -vy*rhoi - v*rhoi_y;

    Tq& vx_rhovx   = rhoi;
    Tq& vx_rhovx_x = rhoi_x;
    Tq& vx_rhovx_y = rhoi_y;

    Tq& vy_rhoy    = vx_rhox;
    Tq& vy_rhoy_x  = vx_rhox_x;
    Tq& vy_rhoy_y  = vx_rhox_y;

    Tq& vy_rhovy   = rhoi;
    Tq& vy_rhovy_x = rhoi_x;
    Tq& vy_rhovy_y = rhoi_y;

    Tq lambdaEddy   = -2./3. * muEddy;
    Tq lambdaEddy_x = -2./3. * muEddy_x;
    Tq lambdaEddy_y = -2./3. * muEddy_y;

    T tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy);
    T tauRxy = muEddy*(uy + vx);
    T tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy);

    Tq  tauRxx_ux   = muEddy*2   + lambdaEddy;
    Tq  tauRxx_ux_x = muEddy_x*2 + lambdaEddy_x;
    Tq  tauRxx_ux_y = muEddy_y*2 + lambdaEddy_y;

    Tq& tauRxx_vy   = lambdaEddy;
    Tq& tauRxx_vy_x = lambdaEddy_x;
    Tq& tauRxx_vy_y = lambdaEddy_y;

    Tq& tauRxy_uy   = muEddy;
    Tq& tauRxy_uy_x = muEddy_x;
    Tq& tauRxy_uy_y = muEddy_y;

    Tq& tauRxy_vx   = muEddy;
    Tq& tauRxy_vx_x = muEddy_x;
    Tq& tauRxy_vx_y = muEddy_y;

    Tq& tauRyy_ux   = lambdaEddy;
    Tq& tauRyy_ux_x = lambdaEddy_x;
    Tq& tauRyy_ux_y = lambdaEddy_y;

    Tq  tauRyy_vy   = muEddy*2   + lambdaEddy;
    Tq  tauRyy_vy_x = muEddy_x*2 + lambdaEddy_x;
    Tq  tauRyy_vy_y = muEddy_y*2 + lambdaEddy_y;


    T tauRxx_x = muEddy_x*(2*ux) + muEddy*(2*uxx) + lambdaEddy_x*(ux + vy) + lambdaEddy*(uxx + vxy);
    T tauRxx_y = muEddy_y*(2*ux) + muEddy*(2*uxy) + lambdaEddy_y*(ux + vy) + lambdaEddy*(uxy + vyy);

    T tauRxy_x = muEddy_x*(uy + vx) + muEddy*(uxy + vxx);
    T tauRxy_y = muEddy_y*(uy + vx) + muEddy*(uyy + vxy);

    T tauRyy_x = muEddy_x*(2*vy) + muEddy*(2*vxy) + lambdaEddy_x*(ux + vy) + lambdaEddy*(uxx + vxy);
    T tauRyy_y = muEddy_y*(2*vy) + muEddy*(2*vyy) + lambdaEddy_y*(ux + vy) + lambdaEddy*(uxy + vyy);

    T magGradV    = sqrt(ux*ux + uy*uy + vx*vx + vy*vy);

    T magGradV_x  = (magGradV == 0.0) ? T(0) : (ux*uxx + uy*uxy + vx*vxx + vy*vxy)/magGradV;
    T magGradV_y  = (magGradV == 0.0) ? T(0) : (ux*uxy + uy*uyy + vx*vxy + vy*vyy)/magGradV;

    T magGradVi   = (magGradV == 0.0) ? T(0) : 1/magGradV;
    T magGradVi2  =  magGradVi*magGradVi;
    T magGradVi_x = -magGradV_x*magGradVi2;
    T magGradVi_y = -magGradV_y*magGradVi2;

    T magGradVi2_x = -2*magGradV_x*magGradVi2*magGradVi;
    T magGradVi2_y = -2*magGradV_y*magGradVi2*magGradVi;

    T magGradV_ux   = ux*magGradVi;
    T magGradV_ux_x = uxx*magGradVi + ux*magGradVi_x;
    T magGradV_ux_y = uxy*magGradVi + ux*magGradVi_y;

    T magGradV_uy   = uy*magGradVi;
    T magGradV_uy_x = uxy*magGradVi + uy*magGradVi_x;
    T magGradV_uy_y = uyy*magGradVi + uy*magGradVi_y;

    T magGradV_vx   = vx*magGradVi;
    T magGradV_vx_x = vxx*magGradVi + vx*magGradVi_x;
    T magGradV_vx_y = vxy*magGradVi + vx*magGradVi_y;

    T magGradV_vy   = vy*magGradVi;
    T magGradV_vy_x = vxy*magGradVi + vy*magGradVi_x;
    T magGradV_vy_y = vyy*magGradVi + vy*magGradVi_y;

    T    Wxy    = (uy  - vx );
    T    Wxy_x  = (uxy - vxx);
    T    Wxy_y  = (uyy - vxy);
    Real Wxy_uy = (1        );
    Real Wxy_vx = (    - 1  );

    T Oxy      =  Wxy*magGradVi;
    T Oxy_x    =  Wxy_x*magGradVi + Wxy*magGradVi_x;
    T Oxy_y    =  Wxy_y*magGradVi + Wxy*magGradVi_y;

    T Oxy_ux   = -Wxy*magGradV_ux*magGradVi2;
    T Oxy_ux_x = -Wxy_x*magGradV_ux*magGradVi2 - Wxy*magGradV_ux_x*magGradVi2 - Wxy*magGradV_ux*magGradVi2_x;
    T Oxy_ux_y = -Wxy_y*magGradV_ux*magGradVi2 - Wxy*magGradV_ux_y*magGradVi2 - Wxy*magGradV_ux*magGradVi2_y;

    T Oxy_uy   =  Wxy_uy*magGradVi   - Wxy*magGradV_uy*magGradVi2;
    T Oxy_uy_x =  Wxy_uy*magGradVi_x - Wxy_x*magGradV_uy*magGradVi2 - Wxy*magGradV_uy_x*magGradVi2 - Wxy*magGradV_uy*magGradVi2_x;
    T Oxy_uy_y =  Wxy_uy*magGradVi_y - Wxy_y*magGradV_uy*magGradVi2 - Wxy*magGradV_uy_y*magGradVi2 - Wxy*magGradV_uy*magGradVi2_y;

    T Oxy_vx   =  Wxy_vx*magGradVi   - Wxy*magGradV_vx*magGradVi2;
    T Oxy_vx_x =  Wxy_vx*magGradVi_x - Wxy_x*magGradV_vx*magGradVi2 - Wxy*magGradV_vx_x*magGradVi2 - Wxy*magGradV_vx*magGradVi2_x;
    T Oxy_vx_y =  Wxy_vx*magGradVi_y - Wxy_y*magGradV_vx*magGradVi2 - Wxy*magGradV_vx_y*magGradVi2 - Wxy*magGradV_vx*magGradVi2_y;

    T Oxy_vy   = -Wxy*magGradV_vy*magGradVi2;
    T Oxy_vy_x = -Wxy_x*magGradV_vy*magGradVi2 - Wxy*magGradV_vy_x*magGradVi2 - Wxy*magGradV_vy*magGradVi2_x;
    T Oxy_vy_y = -Wxy_y*magGradV_vy*magGradVi2 - Wxy*magGradV_vy_y*magGradVi2 - Wxy*magGradV_vy*magGradVi2_y;

    T Oyx      = -Oxy;
    T Oyx_x    = -Oxy_x;
    T Oyx_y    = -Oxy_y;

    T Oyx_ux   = -Oxy_ux;
    T Oyx_ux_x = -Oxy_ux_x;
    T Oyx_ux_y = -Oxy_ux_y;

    T Oyx_uy   = -Oxy_uy;
    T Oyx_uy_x = -Oxy_uy_x;
    T Oyx_uy_y = -Oxy_uy_y;

    T Oyx_vx   = -Oxy_vx;
    T Oyx_vx_x = -Oxy_vx_x;
    T Oyx_vx_y = -Oxy_vx_y;

    T Oyx_vy   = -Oxy_vy;
    T Oyx_vy_x = -Oxy_vy_x;
    T Oyx_vy_y = -Oxy_vy_y;

    //qcrxx   = cr1_*2*(Oxy*tauRxy);
    T qcrxx_ux   = cr1_*2*(Oxy_ux*tauRxy                     );
    T qcrxx_ux_x = cr1_*2*(Oxy_ux_x*tauRxy + Oxy_ux*tauRxy_x );
  //T qcrxx_ux_y = cr1_*2*(Oxy_ux_y*tauRxy + Oxy_ux*tauRxy_y );

    T qcrxx_uy   = cr1_*2*(Oxy_uy*tauRxy                     + Oxy*tauRxy_uy);
    T qcrxx_uy_x = cr1_*2*(Oxy_uy_x*tauRxy + Oxy_uy*tauRxy_x + Oxy_x*tauRxy_uy + Oxy*tauRxy_uy_x);
    T qcrxx_uy_y = cr1_*2*(Oxy_uy_y*tauRxy + Oxy_uy*tauRxy_y + Oxy_y*tauRxy_uy + Oxy*tauRxy_uy_y);

    T qcrxx_vx   = cr1_*2*(Oxy_vx*tauRxy                     + Oxy*tauRxy_vx);
    T qcrxx_vx_x = cr1_*2*(Oxy_vx_x*tauRxy + Oxy_vx*tauRxy_x + Oxy_x*tauRxy_vx + Oxy*tauRxy_vx_x);
  //T qcrxx_vx_y = cr1_*2*(Oxy_vx_y*tauRxy + Oxy_vx*tauRxy_y + Oxy_y*tauRxy_vx + Oxy*tauRxy_vx_y);

    T qcrxx_vy   = cr1_*2*(Oxy_vy*tauRxy                );
    T qcrxx_vy_x = cr1_*2*(Oxy_vy_x*tauRxy + Oxy_vy*tauRxy_x );
    T qcrxx_vy_y = cr1_*2*(Oxy_vy_y*tauRxy + Oxy_vy*tauRxy_y );

    //qcrxy   = cr1_*(Oxy*tauRyy    + Oyx*tauRxx);
    T qcrxy_ux   = cr1_*(Oxy_ux*tauRyy + Oxy*tauRyy_ux + Oyx_ux*tauRxx + Oyx*tauRxx_ux);
    T qcrxy_ux_x = cr1_*(Oxy_ux_x*tauRyy + Oxy_ux*tauRyy_x +
                         Oxy_x*tauRyy_ux + Oxy*tauRyy_ux_x +
                         Oyx_ux_x*tauRxx + Oyx_ux*tauRxx_x +
                         Oyx_x*tauRxx_ux + Oyx*tauRxx_ux_x);
    T qcrxy_ux_y = cr1_*(Oxy_ux_y*tauRyy + Oxy_ux*tauRyy_y +
                         Oxy_y*tauRyy_ux + Oxy*tauRyy_ux_y +
                         Oyx_ux_y*tauRxx + Oyx_ux*tauRxx_y +
                         Oyx_y*tauRxx_ux + Oyx*tauRxx_ux_y);

    T qcrxy_uy   = cr1_*(Oxy_uy*tauRyy                     + Oyx_uy*tauRxx                     );
    T qcrxy_uy_x = cr1_*(Oxy_uy_x*tauRyy + Oxy_uy*tauRyy_x + Oyx_uy_x*tauRxx + Oyx_uy*tauRxx_x );
    T qcrxy_uy_y = cr1_*(Oxy_uy_y*tauRyy + Oxy_uy*tauRyy_y + Oyx_uy_y*tauRxx + Oyx_uy*tauRxx_y );

    T qcrxy_vx   = cr1_*(Oxy_vx*tauRyy                     + Oyx_vx*tauRxx                     );
    T qcrxy_vx_x = cr1_*(Oxy_vx_x*tauRyy + Oxy_vx*tauRyy_x + Oyx_vx_x*tauRxx + Oyx_vx*tauRxx_x );
    T qcrxy_vx_y = cr1_*(Oxy_vx_y*tauRyy + Oxy_vx*tauRyy_y + Oyx_vx_y*tauRxx + Oyx_vx*tauRxx_y );

    T qcrxy_vy   = cr1_*(Oxy_vy*tauRyy + Oxy*tauRyy_vy + Oyx_vy*tauRxx + Oyx*tauRxx_vy);
    T qcrxy_vy_x = cr1_*(Oxy_vy_x*tauRyy + Oxy_vy*tauRyy_x +
                         Oxy_x*tauRyy_vy + Oxy*tauRyy_vy_x +
                         Oyx_vy_x*tauRxx + Oyx_vy*tauRxx_x +
                         Oyx_x*tauRxx_vy + Oyx*tauRxx_vy_x);
    T qcrxy_vy_y = cr1_*(Oxy_vy_y*tauRyy + Oxy_vy*tauRyy_y +
                         Oxy_y*tauRyy_vy + Oxy*tauRyy_vy_y +
                         Oyx_vy_y*tauRxx + Oyx_vy*tauRxx_y +
                         Oyx_y*tauRxx_vy + Oyx*tauRxx_vy_y);

    //qcryy   = cr1_*2*(Oyx*tauRxy);
    T qcryy_ux   = cr1_*2*(Oyx_ux*tauRxy                    );
    T qcryy_ux_x = cr1_*2*(Oyx_ux_x*tauRxy + Oyx_ux*tauRxy_x);
    T qcryy_ux_y = cr1_*2*(Oyx_ux_y*tauRxy + Oyx_ux*tauRxy_y);

    T qcryy_uy   = cr1_*2*(Oyx_uy*tauRxy                     + Oyx*tauRxy_uy                    );
  //T qcryy_uy_x = cr1_*2*(Oyx_uy_x*tauRxy + Oyx_uy*tauRxy_x + Oyx_x*tauRxy_uy + Oyx*tauRxy_uy_x);
    T qcryy_uy_y = cr1_*2*(Oyx_uy_y*tauRxy + Oyx_uy*tauRxy_y + Oyx_y*tauRxy_uy + Oyx*tauRxy_uy_y);

    T qcryy_vx   = cr1_*2*(Oyx_vx*tauRxy                     + Oyx*tauRxy_vx);
    T qcryy_vx_x = cr1_*2*(Oyx_vx_x*tauRxy + Oyx_vx*tauRxy_x + Oyx_x*tauRxy_vx + Oyx*tauRxy_vx_x);
    T qcryy_vx_y = cr1_*2*(Oyx_vx_y*tauRxy + Oyx_vx*tauRxy_y + Oyx_y*tauRxy_vx + Oyx*tauRxy_vx_y);

    T qcryy_vy   = cr1_*2*(Oyx_vy*tauRxy                    );
  //T qcryy_vy_x = cr1_*2*(Oyx_vy_x*tauRxy + Oyx_vy*tauRxy_x);
    T qcryy_vy_y = cr1_*2*(Oyx_vy_y*tauRxy + Oyx_vy*tauRxy_y);

    // Convert to derivatives w.r.t. master state
    //qcrxx
    T qcrxx_rhox    = qcrxx_ux*ux_rhox                        + qcrxx_vx*vx_rhox;
    T qcrxx_rhox_x  = qcrxx_ux_x*ux_rhox + qcrxx_ux*ux_rhox_x + qcrxx_vx_x*vx_rhox + qcrxx_vx*vx_rhox_x;
  //T qcrxx_rhox_y  = qcrxx_ux_y*ux_rhox + qcrxx_ux*ux_rhox_y + qcrxx_vx_y*vx_rhox + qcrxx_vx*vx_rhox_y;

    T qcrxx_rhoy    = qcrxx_uy*uy_rhoy                        + qcrxx_vy*vy_rhoy;
    T qcrxx_rhoy_x  = qcrxx_uy_x*uy_rhoy + qcrxx_uy*uy_rhoy_x + qcrxx_vy_x*vy_rhoy + qcrxx_vy*vy_rhoy_x;
    T qcrxx_rhoy_y  = qcrxx_uy_y*uy_rhoy + qcrxx_uy*uy_rhoy_y + qcrxx_vy_y*vy_rhoy + qcrxx_vy*vy_rhoy_y;

    T qcrxx_rhoux   = qcrxx_ux*ux_rhoux;
    T qcrxx_rhoux_x = qcrxx_ux_x*ux_rhoux + qcrxx_ux*ux_rhoux_x;
  //T qcrxx_rhoux_y = qcrxx_ux_y*ux_rhoux + qcrxx_ux*ux_rhoux_y;

    T qcrxx_rhouy   = qcrxx_uy*uy_rhouy;
    T qcrxx_rhouy_x = qcrxx_uy_x*uy_rhouy + qcrxx_uy*uy_rhouy_x;
    T qcrxx_rhouy_y = qcrxx_uy_y*uy_rhouy + qcrxx_uy*uy_rhouy_y;

    T qcrxx_rhovx   = qcrxx_vx*vx_rhovx;
    T qcrxx_rhovx_x = qcrxx_vx_x*vx_rhovx + qcrxx_vx*vx_rhovx_x;
  //T qcrxx_rhovx_y = qcrxx_vx_y*vx_rhovx + qcrxx_vx*vx_rhovx_y;

    T qcrxx_rhovy   = qcrxx_vy*vy_rhovy;
    T qcrxx_rhovy_x = qcrxx_vy_x*vy_rhovy + qcrxx_vy*vy_rhovy_x;
    T qcrxx_rhovy_y = qcrxx_vy_y*vy_rhovy + qcrxx_vy*vy_rhovy_y;

    //qcrxy
    T qcrxy_rhox    = qcrxy_ux*ux_rhox                        + qcrxy_vx*vx_rhox;
    T qcrxy_rhox_x  = qcrxy_ux_x*ux_rhox + qcrxy_ux*ux_rhox_x + qcrxy_vx_x*vx_rhox + qcrxy_vx*vx_rhox_x;
    T qcrxy_rhox_y  = qcrxy_ux_y*ux_rhox + qcrxy_ux*ux_rhox_y + qcrxy_vx_y*vx_rhox + qcrxy_vx*vx_rhox_y;

    T qcrxy_rhoy    = qcrxy_uy*uy_rhoy                        + qcrxy_vy*vy_rhoy;
    T qcrxy_rhoy_x  = qcrxy_uy_x*uy_rhoy + qcrxy_uy*uy_rhoy_x + qcrxy_vy_x*vy_rhoy + qcrxy_vy*vy_rhoy_x;
    T qcrxy_rhoy_y  = qcrxy_uy_y*uy_rhoy + qcrxy_uy*uy_rhoy_y + qcrxy_vy_y*vy_rhoy + qcrxy_vy*vy_rhoy_y;

    T qcrxy_rhoux   = qcrxy_ux*ux_rhoux;
    T qcrxy_rhoux_x = qcrxy_ux_x*ux_rhoux + qcrxy_ux*ux_rhoux_x;
    T qcrxy_rhoux_y = qcrxy_ux_y*ux_rhoux + qcrxy_ux*ux_rhoux_y;

    T qcrxy_rhouy   = qcrxy_uy*uy_rhouy;
    T qcrxy_rhouy_x = qcrxy_uy_x*uy_rhouy + qcrxy_uy*uy_rhouy_x;
    T qcrxy_rhouy_y = qcrxy_uy_y*uy_rhouy + qcrxy_uy*uy_rhouy_y;

    T qcrxy_rhovx   = qcrxy_vx*vx_rhovx;
    T qcrxy_rhovx_x = qcrxy_vx_x*vx_rhovx + qcrxy_vx*vx_rhovx_x;
    T qcrxy_rhovx_y = qcrxy_vx_y*vx_rhovx + qcrxy_vx*vx_rhovx_y;

    T qcrxy_rhovy   = qcrxy_vy*vy_rhovy;
    T qcrxy_rhovy_x = qcrxy_vy_x*vy_rhovy + qcrxy_vy*vy_rhovy_x;
    T qcrxy_rhovy_y = qcrxy_vy_y*vy_rhovy + qcrxy_vy*vy_rhovy_y;

    //qcryy
    T qcryy_rhox   = qcryy_ux*ux_rhox                        + qcryy_vx*vx_rhox;
    T qcryy_rhox_x = qcryy_ux_x*ux_rhox + qcryy_ux*ux_rhox_x + qcryy_vx_x*vx_rhox + qcryy_vx*vx_rhox_x;
    T qcryy_rhox_y = qcryy_ux_y*ux_rhox + qcryy_ux*ux_rhox_y + qcryy_vx_y*vx_rhox + qcryy_vx*vx_rhox_y;

    T qcryy_rhoy   = qcryy_uy*uy_rhoy                        + qcryy_vy*vy_rhoy;
  //T qcryy_rhoy_x = qcryy_uy_x*uy_rhoy + qcryy_uy*uy_rhoy_x + qcryy_vy_x*vy_rhoy + qcryy_vy*vy_rhoy_x;
    T qcryy_rhoy_y = qcryy_uy_y*uy_rhoy + qcryy_uy*uy_rhoy_y + qcryy_vy_y*vy_rhoy + qcryy_vy*vy_rhoy_y;

    T qcryy_rhoux   = qcryy_ux*ux_rhoux;
    T qcryy_rhoux_x = qcryy_ux_x*ux_rhoux + qcryy_ux*ux_rhoux_x;
    T qcryy_rhoux_y = qcryy_ux_y*ux_rhoux + qcryy_ux*ux_rhoux_y;

    T qcryy_rhouy   = qcryy_uy*uy_rhouy;
  //T qcryy_rhouy_x = qcryy_uy_x*uy_rhouy + qcryy_uy*uy_rhouy_x;
    T qcryy_rhouy_y = qcryy_uy_y*uy_rhouy + qcryy_uy*uy_rhouy_y;

    T qcryy_rhovx   = qcryy_vx*vx_rhovx;
    T qcryy_rhovx_x = qcryy_vx_x*vx_rhovx + qcryy_vx*vx_rhovx_x;
    T qcryy_rhovx_y = qcryy_vx_y*vx_rhovx + qcryy_vx*vx_rhovx_y;

    T qcryy_rhovy   = qcryy_vy*vy_rhovy;
  //T qcryy_rhovy_x = qcryy_vy_x*vy_rhovy + qcryy_vy*vy_rhovy_x;
    T qcryy_rhovy_y = qcryy_vy_y*vy_rhovy + qcryy_vy*vy_rhovy_y;


    // d(Fv)/d(Ux)
    kxx_x(ixMom,0) -= qcrxx_rhox_x ;
    kxx_x(ixMom,1) -= qcrxx_rhoux_x;
    kxx_x(ixMom,2) -= qcrxx_rhovx_x;

//    kxx_y(ixMom,0) -= qcrxx_rhox_y ;
//    kxx_y(ixMom,1) -= qcrxx_rhoux_y;
//    kxx_y(ixMom,2) -= qcrxx_rhovx_y;

    kxx_x(iyMom,0) -= qcrxy_rhox_x ;
    kxx_x(iyMom,1) -= qcrxy_rhoux_x;
    kxx_x(iyMom,2) -= qcrxy_rhovx_x;

//    kxx_y(iyMom,0) -= qcrxy_rhox_y ;
//    kxx_y(iyMom,1) -= qcrxy_rhoux_y;
//    kxx_y(iyMom,2) -= qcrxy_rhovx_y;

    kxx_x(iEngy,0) -= ux*qcrxx_rhox  + u*qcrxx_rhox_x  + vx*qcrxy_rhox  + v*qcrxy_rhox_x ;
    kxx_x(iEngy,1) -= ux*qcrxx_rhoux + u*qcrxx_rhoux_x + vx*qcrxy_rhoux + v*qcrxy_rhoux_x;
    kxx_x(iEngy,2) -= ux*qcrxx_rhovx + u*qcrxx_rhovx_x + vx*qcrxy_rhovx + v*qcrxy_rhovx_x;

//    kxx_y(iEngy,0) -= uy*qcrxx_rhox  + u*qcrxx_rhox_y  + vy*qcrxy_rhox  + v*qcrxy_rhox_y ;
//    kxx_y(iEngy,1) -= uy*qcrxx_rhoux + u*qcrxx_rhoux_y + vy*qcrxy_rhoux + v*qcrxy_rhoux_y;
//    kxx_y(iEngy,2) -= uy*qcrxx_rhovx + u*qcrxx_rhovx_y + vy*qcrxy_rhovx + v*qcrxy_rhovx_y;

    // d(Fv)/d(Uy)
    kxy_x(ixMom,0) -= qcrxx_rhoy_x ;
    kxy_x(ixMom,1) -= qcrxx_rhouy_x;
    kxy_x(ixMom,2) -= qcrxx_rhovy_x;

    kxy_y(ixMom,0) -= qcrxx_rhoy_y ;
    kxy_y(ixMom,1) -= qcrxx_rhouy_y;
    kxy_y(ixMom,2) -= qcrxx_rhovy_y;

    kxy_x(iyMom,0) -= qcrxy_rhoy_x ;
    kxy_x(iyMom,1) -= qcrxy_rhouy_x;
    kxy_x(iyMom,2) -= qcrxy_rhovy_x;

    kxy_y(iyMom,0) -= qcrxy_rhoy_y ;
    kxy_y(iyMom,1) -= qcrxy_rhouy_y;
    kxy_y(iyMom,2) -= qcrxy_rhovy_y;

    kxy_x(iEngy,0) -= ux*qcrxx_rhoy  + u*qcrxx_rhoy_x  + vx*qcrxy_rhoy  + v*qcrxy_rhoy_x ;
    kxy_x(iEngy,1) -= ux*qcrxx_rhouy + u*qcrxx_rhouy_x + vx*qcrxy_rhouy + v*qcrxy_rhouy_x;
    kxy_x(iEngy,2) -= ux*qcrxx_rhovy + u*qcrxx_rhovy_x + vx*qcrxy_rhovy + v*qcrxy_rhovy_x;

    kxy_y(iEngy,0) -= uy*qcrxx_rhoy  + u*qcrxx_rhoy_y  + vy*qcrxy_rhoy  + v*qcrxy_rhoy_y ;
    kxy_y(iEngy,1) -= uy*qcrxx_rhouy + u*qcrxx_rhouy_y + vy*qcrxy_rhouy + v*qcrxy_rhouy_y;
    kxy_y(iEngy,2) -= uy*qcrxx_rhovy + u*qcrxx_rhovy_y + vy*qcrxy_rhovy + v*qcrxy_rhovy_y;

    // d(Gv)/d(Ux)
    kyx_x(ixMom,0) -= qcrxy_rhox_x ;
    kyx_x(ixMom,1) -= qcrxy_rhoux_x;
    kyx_x(ixMom,2) -= qcrxy_rhovx_x;

    kyx_y(ixMom,0) -= qcrxy_rhox_y ;
    kyx_y(ixMom,1) -= qcrxy_rhoux_y;
    kyx_y(ixMom,2) -= qcrxy_rhovx_y;

    kyx_x(iyMom,0) -= qcryy_rhox_x ;
    kyx_x(iyMom,1) -= qcryy_rhoux_x;
    kyx_x(iyMom,2) -= qcryy_rhovx_x;

    kyx_y(iyMom,0) -= qcryy_rhox_y ;
    kyx_y(iyMom,1) -= qcryy_rhoux_y;
    kyx_y(iyMom,2) -= qcryy_rhovx_y;

    kyx_x(iEngy,0) -= ux*qcrxy_rhox  + u*qcrxy_rhox_x  + vx*qcryy_rhox  + v*qcryy_rhox_x ;
    kyx_x(iEngy,1) -= ux*qcrxy_rhoux + u*qcrxy_rhoux_x + vx*qcryy_rhoux + v*qcryy_rhoux_x;
    kyx_x(iEngy,2) -= ux*qcrxy_rhovx + u*qcrxy_rhovx_x + vx*qcryy_rhovx + v*qcryy_rhovx_x;

    kyx_y(iEngy,0) -= uy*qcrxy_rhox  + u*qcrxy_rhox_y  + vy*qcryy_rhox  + v*qcryy_rhox_y ;
    kyx_y(iEngy,1) -= uy*qcrxy_rhoux + u*qcrxy_rhoux_y + vy*qcryy_rhoux + v*qcryy_rhoux_y;
    kyx_y(iEngy,2) -= uy*qcrxy_rhovx + u*qcrxy_rhovx_y + vy*qcryy_rhovx + v*qcryy_rhovx_y;

    // d(Gv)/d(Uy)
//    kyy_x(ixMom,0) -= qcrxy_rhoy_x ;
//    kyy_x(ixMom,1) -= qcrxy_rhouy_x;
//    kyy_x(ixMom,2) -= qcrxy_rhovy_x;

    kyy_y(ixMom,0) -= qcrxy_rhoy_y ;
    kyy_y(ixMom,1) -= qcrxy_rhouy_y;
    kyy_y(ixMom,2) -= qcrxy_rhovy_y;

//    kyy_x(iyMom,0) -= qcryy_rhoy_x ;
//    kyy_x(iyMom,1) -= qcryy_rhouy_x;
//    kyy_x(iyMom,2) -= qcryy_rhovy_x;

    kyy_y(iyMom,0) -= qcryy_rhoy_y ;
    kyy_y(iyMom,1) -= qcryy_rhouy_y;
    kyy_y(iyMom,2) -= qcryy_rhovy_y;

//    kyy_x(iEngy,0) -= ux*qcrxy_rhoy  + u*qcrxy_rhoy_x  + vx*qcryy_rhoy  + v*qcryy_rhoy_x ;
//    kyy_x(iEngy,1) -= ux*qcrxy_rhouy + u*qcrxy_rhouy_x + vx*qcryy_rhouy + v*qcryy_rhouy_x;
//    kyy_x(iEngy,2) -= ux*qcrxy_rhovy + u*qcrxy_rhovy_x + vx*qcryy_rhovy + v*qcryy_rhovy_x;

    kyy_y(iEngy,0) -= uy*qcrxy_rhoy  + u*qcrxy_rhoy_y  + vy*qcryy_rhoy  + v*qcryy_rhoy_y ;
    kyy_y(iEngy,1) -= uy*qcrxy_rhouy + u*qcrxy_rhouy_y + vy*qcryy_rhouy + v*qcryy_rhouy_y;
    kyy_y(iEngy,2) -= uy*qcrxy_rhovy + u*qcrxy_rhovy_y + vy*qcryy_rhovy + v*qcryy_rhovy_y;

#endif
  }

  // compute the diffusion matrix
  BaseType::diffusionViscousGradient( rho, u, v, t, muT, kT,
                                      rhox, ux, vx, tx, mux, kx,
                                      rhoy, uy, vy, ty, muy, ky,
                                      kxx_x, kxy_x, kyx_x,
                                      kxy_y, kyx_y, kyy_y );
}


// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianFluxViscous(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  // SA fluxes

  Tq rho=0, u=0, v=0, t=0, nt=0;
  T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;
  T  ntx=0, nty=0;
  Tq mu, k;
  Tq mu_t = 0;
  Tq k_mu = 0;

  qInterpret_.eval( q, rho, u, v, t );
  mu = visc_.viscosity( t );
  visc_.viscosityJacobian(t, mu_t);
  k  = tcond_.conductivity( mu );
  tcond_.conductivityJacobian( mu, k_mu);

  Real Cv = gas_.Cv();

  Tq e = gas_.energy(rho, t);
  Tq E = e + 0.5*(u*u + v*v);

  Tq t_rho  = ( -E/rho + ( u*u + v*v )/rho ) / Cv;
  Tq t_rhou = (        - (   u       )/rho ) / Cv;
  Tq t_rhov = (        - (         v )/rho ) / Cv;
  Tq t_rhoE = (  1/rho                     ) / Cv;

  ArrayQ<T> dmu = 0.0;
  dmu(0) = mu_t*t_rho;
  dmu(1) = mu_t*t_rhou;
  dmu(2) = mu_t*t_rhov;
  dmu(3) = mu_t*t_rhoE;

  ArrayQ<T> dk = 0.0;
  dk(0) = k_mu*dmu(0);
  dk(1) = k_mu*dmu(1);
  dk(2) = k_mu*dmu(2);
  dk(3) = k_mu*dmu(3);

  // SA model viscous terms

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  Tq chi  = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);

  T chi_rhont = 1./mu;

  T chi_mu = -rho*nt/power<2,T>(mu);
  T chi_rho = chi_mu*dmu(0);
  T chi_rhou = chi_mu*dmu(1);
  T chi_rhov = chi_mu*dmu(2);
  T chi_rhoE = chi_mu*dmu(3);

  if (nt < 0)
  {
    T fn  = (cn1_ + chi3) / (cn1_ - chi3);
    T fn_chi = 3*power<2,T>(chi)/(cn1_ - chi3) + 3*power<2,T>(chi)*(cn1_ + chi3)/power<2,T>(cn1_ - chi3);

    T fn_rho = fn_chi*chi_rho;
    T fn_rhou = fn_chi*chi_rhou;
    T fn_rhov = fn_chi*chi_rhov;
    T fn_rhoE = fn_chi*chi_rhoE;
    T fn_rhont = fn_chi*chi_rhont;

//    dfdu(iSA) -= (mu + rho*nt*fn)*ntx/sigma_;
    dfdu(iSA,0) -= ((dmu(0) + rho*nt*fn_rho)*ntx  + (mu + rho*nt*fn)*(nt*rhox - rho*ntx)/power<2>(rho))/sigma_/ntref_;
    dfdu(iSA,1) -= (dmu(1) + rho*nt*fn_rhou)*ntx/sigma_/ntref_;
    dfdu(iSA,2) -= (dmu(2) + rho*nt*fn_rhov)*ntx/sigma_/ntref_;
    dfdu(iSA,3) -= (dmu(3) + rho*nt*fn_rhoE)*ntx/sigma_/ntref_;
    dfdu(iSA,4) -= (fn + rho*nt*fn_rhont)*ntx/sigma_ - (mu + rho*nt*fn)*rhox/power<2>(rho)/sigma_;

//    dgdu(iSA) -= (mu + rho*nt*fn)*nty/sigma_;
    dgdu(iSA,0) -= ((dmu(0) + rho*nt*fn_rho)*nty + (mu + rho*nt*fn)*(nt*rhoy - rho*nty)/power<2>(rho))/sigma_/ntref_;
    dgdu(iSA,1) -= (dmu(1) + rho*nt*fn_rhou)*nty/sigma_/ntref_;
    dgdu(iSA,2) -= (dmu(2) + rho*nt*fn_rhov)*nty/sigma_/ntref_;
    dgdu(iSA,3) -= (dmu(3) + rho*nt*fn_rhoE)*nty/sigma_/ntref_;
    dgdu(iSA,4) -= (fn + rho*nt*fn_rhont)*nty/sigma_ - (mu + rho*nt*fn)*rhoy/power<2>(rho)/sigma_;

  }
  else
  {
//    f(iSA) -= (mu + rho*nt)*ntx/sigma_;
//    g(iSA) -= (mu + rho*nt)*nty/sigma_;
    dfdu(iSA,0) -= (dmu(0)*ntx + (mu + rho*nt)*(nt*rhox - rho*ntx)/power<2>(rho))/sigma_/ntref_;
    dfdu(iSA,1) -= dmu(1)*ntx/sigma_/ntref_;
    dfdu(iSA,2) -= dmu(2)*ntx/sigma_/ntref_;
    dfdu(iSA,3) -= dmu(3)*ntx/sigma_/ntref_;
    dfdu(iSA,4) -= ntx/sigma_ - (mu + rho*nt)*rhox/power<2>(rho)/sigma_;

    dgdu(iSA,0) -= (dmu(0)*nty + (mu + rho*nt)*(nt*rhoy - rho*nty)/power<2>(rho))/sigma_/ntref_;
    dgdu(iSA,1) -= dmu(1)*nty/sigma_/ntref_;
    dgdu(iSA,2) -= dmu(2)*nty/sigma_/ntref_;
    dgdu(iSA,3) -= dmu(3)*nty/sigma_/ntref_;
    dgdu(iSA,4) -= nty/sigma_ - (mu + rho*nt)*rhoy/power<2>(rho)/sigma_;
  }


  // Reynolds stresses

  T tauxx_mu = (2*ux   ) - 2./3.*(ux + vy);
  T tauxy_mu = (uy + vx);
  T tauyy_mu = (2*vy   ) - 2./3.*(ux + vy);

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Tq fv1 = chi3 / (chi3 + power<3>(cv1_));
    Tq muEddy = rho*nt*fv1;
    Real Cp = this->gas_.Cp();

    // Augment the molecular viscosity and thermal conductivity
    mu += muEddy;
    k  += muEddy*Cp/prandtlEddy_;

    T fv1_chi = -3*power<5,T>(chi)/power<2,T>(power<3>(cv1_) + chi3) + 3*power<2,T>(chi)/(power<3>(cv1_) + chi3);

    T fv1_rho   = fv1_chi*chi_rho;
    T fv1_rhou  = fv1_chi*chi_rhou;
    T fv1_rhov  = fv1_chi*chi_rhov;
    T fv1_rhoE  = fv1_chi*chi_rhoE;
    T fv1_rhont = fv1_chi*chi_rhont;

    ArrayQ<T> dmuEddy;
    dmuEddy(0) = rho*nt*fv1_rho;
    dmuEddy(1) = rho*nt*fv1_rhou;
    dmuEddy(2) = rho*nt*fv1_rhov;
    dmuEddy(3) = rho*nt*fv1_rhoE;
    dmuEddy(4) = rho*nt*fv1_rhont + fv1;

    dk  += dmuEddy*Cp/prandtlEddy_;
    dmu += dmuEddy;

    // add QCR terms
#ifdef SANS_SA_QCR
    Tq u_rho  = -u/rho;
    Tq u_rhou =  1/rho;
    Tq v_rho  = -v/rho;
    Tq v_rhov =  1/rho;

    Tq lambdaEddy = -2./3. * muEddy;

    T tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy);
    T tauRxy = muEddy*(uy + vx);
    T tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy);

    Tq rho2 = rho*rho;

    T ux_rho  = (rhox * u - rho*ux)/rho2;
    T ux_rhou = -rhox              /rho2;
    T uy_rho  = (rhoy * u - rho*uy)/rho2;
    T uy_rhou = -rhoy              /rho2;

    T vx_rho  = (rhox * v - rho*vx)/rho2;
    T vx_rhov = -rhox              /rho2;
    T vy_rho  = (rhoy * v - rho*vy)/rho2;
    T vy_rhov = -rhoy              /rho2;

    T magGradV = sqrt(ux*ux + uy*uy + vx*vx + vy*vy);
    T magGradV_rho  = magGradV == 0.0 ? T(0) : (ux*ux_rho + uy*uy_rho + vx*vx_rho  + vy*vy_rho )/magGradV;
    T magGradV_rhou = magGradV == 0.0 ? T(0) : (ux*ux_rhou + uy*uy_rhou                        )/magGradV;
    T magGradV_rhov = magGradV == 0.0 ? T(0) : (                        vx*vx_rhov + vy*vy_rhov)/magGradV;
    T magGradV2   = magGradV*magGradV;

    T Wxy      = (uy      - vx     );
    T Wxy_rho  = (uy_rho  - vx_rho );
    T Wxy_rhou = (uy_rhou          );
    T Wxy_rhov = (        - vx_rhov);

    T Oxy      = magGradV == 0.0 ? T(0) : Wxy/magGradV;
    T Oxy_rho  = magGradV == 0.0 ? T(0) : Wxy_rho/magGradV  - Wxy*magGradV_rho/magGradV2;
    T Oxy_rhou = magGradV == 0.0 ? T(0) : Wxy_rhou/magGradV - Wxy*magGradV_rhou/magGradV2;
    T Oxy_rhov = magGradV == 0.0 ? T(0) : Wxy_rhov/magGradV - Wxy*magGradV_rhov/magGradV2;

    T Oyx      = -Oxy;
    T Oyx_rho  = -Oxy_rho;
    T Oyx_rhou = -Oxy_rhou;
    T Oyx_rhov = -Oxy_rhov;

    T tauRxx_rho   = tauxx_mu*dmuEddy(0) + muEddy*(4./3.*ux_rho  - 2./3.*vy_rho  );
    T tauRxx_rhou  = tauxx_mu*dmuEddy(1) + muEddy*(4./3.*ux_rhou                 );
    T tauRxx_rhov  = tauxx_mu*dmuEddy(2) + muEddy*(              - 2./3.*vy_rhov );
    T tauRxx_rhoE  = tauxx_mu*dmuEddy(3);
    T tauRxx_rhont = tauxx_mu*dmuEddy(4);

    T tauRxy_rho   = tauxy_mu*dmuEddy(0) + muEddy*(uy_rho + vx_rho);
    T tauRxy_rhou  = tauxy_mu*dmuEddy(1) + muEddy*(uy_rhou          );
    T tauRxy_rhov  = tauxy_mu*dmuEddy(2) + muEddy*(vx_rhov          );
    T tauRxy_rhoE  = tauxy_mu*dmuEddy(3);
    T tauRxy_rhont = tauxy_mu*dmuEddy(4);

    T tauRyy_rho   = tauyy_mu*dmuEddy(0) + muEddy*(4./3.*vy_rho  - 2./3.*ux_rho  );
    T tauRyy_rhou  = tauyy_mu*dmuEddy(1) + muEddy*(              - 2./3.*ux_rhou );
    T tauRyy_rhov  = tauyy_mu*dmuEddy(2) + muEddy*(4./3.*vy_rhov                 );
    T tauRyy_rhoE  = tauyy_mu*dmuEddy(3);
    T tauRyy_rhont = tauyy_mu*dmuEddy(4);

    T qcrxx       = cr1_*2*(Oxy*tauRxy);
    T qcrxx_rho   = cr1_*2*(Oxy_rho *tauRxy + Oxy*tauRxy_rho  );
    T qcrxx_rhou  = cr1_*2*(Oxy_rhou*tauRxy + Oxy*tauRxy_rhou );
    T qcrxx_rhov  = cr1_*2*(Oxy_rhov*tauRxy + Oxy*tauRxy_rhov );
    T qcrxx_rhoE  = cr1_*2*(                  Oxy*tauRxy_rhoE );
    T qcrxx_rhont = cr1_*2*(                  Oxy*tauRxy_rhont);

    T qcrxy       = cr1_*(Oxy*tauRyy + Oyx*tauRxx);
    T qcrxy_rho   = cr1_*(Oxy_rho *tauRyy + Oxy*tauRyy_rho  + Oyx_rho *tauRxx + Oyx*tauRxx_rho  );
    T qcrxy_rhou  = cr1_*(Oxy_rhou*tauRyy + Oxy*tauRyy_rhou + Oyx_rhou*tauRxx + Oyx*tauRxx_rhou );
    T qcrxy_rhov  = cr1_*(Oxy_rhov*tauRyy + Oxy*tauRyy_rhov + Oyx_rhov*tauRxx + Oyx*tauRxx_rhov );
    T qcrxy_rhoE  = cr1_*(                  Oxy*tauRyy_rhoE                   + Oyx*tauRxx_rhoE );
    T qcrxy_rhont = cr1_*(                  Oxy*tauRyy_rhont                  + Oyx*tauRxx_rhont);

    T qcryy       = cr1_*2*(Oyx*tauRxy);
    T qcryy_rho   = cr1_*2*(Oyx_rho *tauRxy + Oyx*tauRxy_rho  );
    T qcryy_rhou  = cr1_*2*(Oyx_rhou*tauRxy + Oyx*tauRxy_rhou );
    T qcryy_rhov  = cr1_*2*(Oyx_rhov*tauRxy + Oyx*tauRxy_rhov );
    T qcryy_rhoE  = cr1_*2*(                  Oyx*tauRxy_rhoE );
    T qcryy_rhont = cr1_*2*(                  Oyx*tauRxy_rhont);

//    f(ixMom) += qcrxx;
//    f(iyMom) += qcrxy;
//    f(iEngy) += u*qcrxx + v*qcrxy;
//
//    g(ixMom) += qcrxy;
//    g(iyMom) += qcryy;
//    g(iEngy) += u*qcrxy + v*qcryy;

    dfdu(ixMom,0) += qcrxx_rho  ;
    dfdu(ixMom,1) += qcrxx_rhou ;
    dfdu(ixMom,2) += qcrxx_rhov ;
    dfdu(ixMom,3) += qcrxx_rhoE ;
    dfdu(ixMom,4) += qcrxx_rhont;

    dfdu(iyMom,0) += qcrxy_rho  ;
    dfdu(iyMom,1) += qcrxy_rhou ;
    dfdu(iyMom,2) += qcrxy_rhov ;
    dfdu(iyMom,3) += qcrxy_rhoE ;
    dfdu(iyMom,4) += qcrxy_rhont;

    dfdu(iEngy,0) += u_rho*qcrxx  + u*qcrxx_rho   + v_rho*qcrxy  + v*qcrxy_rho  ;
    dfdu(iEngy,1) += u_rhou*qcrxx + u*qcrxx_rhou                 + v*qcrxy_rhou ;
    dfdu(iEngy,2) +=                u*qcrxx_rhov  + v_rhov*qcrxy + v*qcrxy_rhov ;
    dfdu(iEngy,3) +=                u*qcrxx_rhoE                 + v*qcrxy_rhoE ;
    dfdu(iEngy,4) +=                u*qcrxx_rhont                + v*qcrxy_rhont;

    dgdu(ixMom,0) += qcrxy_rho  ;
    dgdu(ixMom,1) += qcrxy_rhou ;
    dgdu(ixMom,2) += qcrxy_rhov ;
    dgdu(ixMom,3) += qcrxy_rhoE ;
    dgdu(ixMom,4) += qcrxy_rhont;

    dgdu(iyMom,0) += qcryy_rho  ;
    dgdu(iyMom,1) += qcryy_rhou ;
    dgdu(iyMom,2) += qcryy_rhov ;
    dgdu(iyMom,3) += qcryy_rhoE ;
    dgdu(iyMom,4) += qcryy_rhont;

    dgdu(iEngy,0) += u_rho*qcrxy  + u*qcrxy_rho   + v_rho*qcryy  + v*qcryy_rho  ;
    dgdu(iEngy,1) += u_rhou*qcrxy + u*qcrxy_rhou                 + v*qcryy_rhou ;
    dgdu(iEngy,2) +=                u*qcrxy_rhov  + v_rhov*qcryy + v*qcryy_rhov ;
    dgdu(iEngy,3) +=                u*qcrxy_rhoE                 + v*qcryy_rhoE ;
    dgdu(iEngy,4) +=                u*qcrxy_rhont                + v*qcryy_rhont;
#endif
  }

  // Compute the jacobian viscous flux for NS

  BaseType::jacobianFluxViscous(mu, k, dmu, dk, q, qx, qy, dfdu, dgdu);

  // Finish jacobian

  //  f(ixMom) -= tauxx;
  //  f(iyMom) -= tauxy;
  //  f(iEngy) -= k*tx + u*tauxx + v*tauxy;
  //
  //  g(ixMom) -= tauxy;
  //  g(iyMom) -= tauyy;
  //  g(iEngy) -= k*ty + u*tauxy + v*tauyy;
  Tf tauxx_rhont = tauxx_mu*dmu(4);
  Tf tauxy_rhont = tauxy_mu*dmu(4);
  Tf tauyy_rhont = tauyy_mu*dmu(4);

  dfdu(ixMom,4) -= tauxx_rhont*ntref_;
  dfdu(iyMom,4) -= tauxy_rhont*ntref_;
  dfdu(iEngy,4) -= (tx*dk(4) + u*tauxx_rhont + v*tauxy_rhont)*ntref_;

  dgdu(ixMom,4) -= tauxy_rhont*ntref_;
  dgdu(iyMom,4) -= tauyy_rhont*ntref_;
  dgdu(iEngy,4) -= (ty*dk(4) + u*tauxy_rhont + v*tauyy_rhont)*ntref_;
}


// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho = 0; Tq u = 0; Tq v = 0; Tq t = 0; Tq nt = 0;
  Tqg rhox, ux, vx, tx, ntx = 0;
  Tqg rhoy, uy, vy, ty, nty = 0;
  T ntxx = 0, ntxy = 0, ntyy = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalSA( q, nt );

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  qInterpret_.evalSAHessian( q, qx, qx, qxx, ntxx);
  qInterpret_.evalSAHessian( q, qx, qy, qxy, ntxy);
  qInterpret_.evalSAHessian( q, qy, qy, qyy, ntyy);

  Tq mu_t = 0;
  visc_.viscosityJacobian( t, mu_t );

  Tq  mu  = visc_.viscosity( t );
  Tqg mux = mu_t*tx;
  Tqg muy = mu_t*ty;

  Tq k_mu = 0;
  tcond_.conductivityJacobian( mu, k_mu );

  Tq  k  = tcond_.conductivity( mu );
  Tqg kx = k_mu*mux;
  Tqg ky = k_mu*muy;

  Tq chi = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);
  Tqg chix = rhox*nt/mu + rho*ntx/mu - rho*nt*mux/(mu*mu);
  Tqg chiy = rhoy*nt/mu + rho*nty/mu - rho*nt*muy/(mu*mu);

  if (nt < 0)
  {
    Tq  fn = (cn1_ + chi3) / (cn1_ - chi3);
    Tq  fn_chi = 6*cn1_*(chi*chi) / power<2,Tq>(cn1_ - chi3);
    Tqg fnx = fn_chi*chix;
    Tqg fny = fn_chi*chiy;
    strongPDE(iSA) -= ((mu + rho*nt*fn)*(ntxx + ntyy)
                    + (mux + rhox*nt*fn + rho*ntx*fn + rho*nt*fnx)*ntx
                    + (muy + rhoy*nt*fn + rho*nty*fn + rho*nt*fny)*nty)/sigma_/ntref_;
  }
  else
  {
    strongPDE(iSA) -= ((mu + rho*nt)*(ntxx + ntyy)
                    + (mux + rhox*nt + rho*ntx)*ntx
                    + (muy + rhoy*nt + rho*nty)*nty)/sigma_/ntref_;
  }

  // molecular and Reynolds stresses

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Real cv13 = power<3>(cv1_);
    Tq fv1 = chi3 / (chi3 + cv13);
    Tq muEddy = rho*nt*fv1;

    Tq fv1_chi = 3*cv13*chi*chi / power<2,Tq>(cv13 + chi3);

    Tqg fv1x = fv1_chi*chix;
    Tqg fv1y = fv1_chi*chiy;

    Tqg muEddy_x = rhox*nt*fv1 + rho*ntx*fv1 + rho*nt*fv1x;
    Tqg muEddy_y = rhoy*nt*fv1 + rho*nty*fv1 + rho*nt*fv1y;

    mu  += muEddy;
    mux += muEddy_x;
    muy += muEddy_y;

    Real Cp = this->gas_.Cp();
    Tq k_muEddy = Cp/prandtlEddy_;

    k  += muEddy*Cp/prandtlEddy_;
    kx += k_muEddy*muEddy_x;
    ky += k_muEddy*muEddy_y;

#ifdef SANS_SA_QCR

    // evaluate second derivatives
    T rhoxx = 0, rhoxy = 0, rhoyy = 0;
    T uxx = 0, uxy = 0, uyy = 0;
    T vxx = 0, vxy = 0, vyy = 0;
    T txx = 0, txy = 0, tyy = 0;

    qInterpret_.evalSecondGradient( q, qx, qy,
                                    qxx, qxy, qyy,
                                    rhoxx, rhoxy, rhoyy,
                                    uxx, uxy, uyy,
                                    vxx, vxy, vyy,
                                    txx, txy, tyy );

    Tq lambdaEddy   = -2./3. * muEddy;
    Tq lambdaEddy_x = -2./3. * muEddy_x;
    Tq lambdaEddy_y = -2./3. * muEddy_y;

    T tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy);
    T tauRxy = muEddy*(uy + vx);
    T tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy);

    T tauRxx_x = muEddy_x*(2*ux) + muEddy*(2*uxx) + lambdaEddy_x*(ux + vy) + lambdaEddy*(uxx + vxy);
    T tauRxx_y = muEddy_y*(2*ux) + muEddy*(2*uxy) + lambdaEddy_y*(ux + vy) + lambdaEddy*(uxy + vyy);

    T tauRxy_x = muEddy_x*(uy + vx) + muEddy*(uxy + vxx);
    T tauRxy_y = muEddy_y*(uy + vx) + muEddy*(uyy + vxy);

    T tauRyy_x = muEddy_x*(2*vy) + muEddy*(2*vxy) + lambdaEddy_x*(ux + vy) + lambdaEddy*(uxx + vxy);
    T tauRyy_y = muEddy_y*(2*vy) + muEddy*(2*vyy) + lambdaEddy_y*(ux + vy) + lambdaEddy*(uxy + vyy);

    T magGradV   = sqrt(ux*ux + uy*uy + vx*vx + vy*vy);
    T magGradV_x = (ux*uxx + uy*uxy + vx*vxx + vy*vxy)/magGradV;
    T magGradV_y = (ux*uxy + uy*uyy + vx*vxy + vy*vyy)/magGradV;
    T magGradV2  = magGradV*magGradV;

    T Wxy   = (uy  - vx );
    T Wxy_x = (uxy - vxx);
    T Wxy_y = (uyy - vxy);

    T Oxy   = (magGradV == 0.0) ? T(0) : Wxy/magGradV;
    T Oxy_x = (magGradV == 0.0) ? T(0) : Wxy_x/magGradV - Wxy*magGradV_x/magGradV2;
    T Oxy_y = (magGradV == 0.0) ? T(0) : Wxy_y/magGradV - Wxy*magGradV_y/magGradV2;

    T Oyx   = -Oxy;
    T Oyx_x = -Oxy_x;
    T Oyx_y = -Oxy_y;

    T qcrxx   = cr1_*2*(Oxy*tauRxy);
    T qcrxx_x = cr1_*2*(Oxy_x*tauRxy + Oxy*tauRxy_x);

    T qcrxy   = cr1_*(Oxy*tauRyy   + Oyx*tauRxx);
    T qcrxy_x = cr1_*(Oxy_x*tauRyy + Oxy*tauRyy_x + Oyx_x*tauRxx + Oyx*tauRxx_x);
    T qcrxy_y = cr1_*(Oxy_y*tauRyy + Oxy*tauRyy_y + Oyx_y*tauRxx + Oyx*tauRxx_y);

    T qcryy   = cr1_*2*(Oyx*tauRxy);
    T qcryy_y = cr1_*2*(Oyx_y*tauRxy + Oyx*tauRxy_y);

    strongPDE(ixMom) += qcrxx_x;
    strongPDE(iyMom) += qcrxy_x;
    strongPDE(iEngy) += ux*qcrxx + u*qcrxx_x + vx*qcrxy + v*qcrxy_x;

    strongPDE(ixMom) += qcrxy_y;
    strongPDE(iyMom) += qcryy_y;
    strongPDE(iEngy) += uy*qcrxy + u*qcrxy_y + vy*qcryy + v*qcryy_y;
#endif
  }

  BaseType::strongFluxViscous( mu, mux, muy,
                               k, kx, ky,
                               q, qx, qy,
                               qxx,
                               qxy, qyy, strongPDE );
}


// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::source(
    const Real& dist,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ< Ts >& source ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  const bool verbose = false;

  Tq rho = 0, t = 0;
  T  rhox, rhoy, ux, uy, vx, vy, tx, ty;
  Tq nt;                                // nu_tilde
  T  ntx, nty;
  Tq mu;                                // molecular viscosity
  Tq chi, chi3;
  T  fv2, ft2;
  T  shr0;                              // shear rate: vorticity
  T  shrmod;                            // shear rate: modified
  T  r, g, fw;                          // wall terms
  T  prod, wall, nonc;                  // production, destruction, non-conservative diffusion terms

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalTemperature( q, t );
  qInterpret_.evalSA( q, nt );
  mu  = visc_.viscosity( t );
  chi = rho*nt/mu;

  chi3 = power<3,Tq>(chi);

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  // production and near-wall destruction terms

  // shear rate: vorticity
  shr0 = fabs(uy - vx);

  // production term (vorticity component only)
  if (nt < 0)
  {
    prod = cb1_*(1 - ct3_)*shr0*nt;
    if (verbose) std::cout << "prodN = " << rho*prod << std::endl;
  }
  else
  {
    prod = cb1_*shr0*nt;
    if (verbose) std::cout << "prod1 = " << rho*prod << std::endl;
  }
  source(iSA) += -rho*prod/ntref_;

  // near-wall destruction term (includes modified production component)

  if (nt < 0)
  {
    wall = -cw1_*power<2,Tq>(nt/dist);
    if (verbose) std::cout << "wallN = " << -rho*wall << std::endl;
  }
  else
  {
    // modified shear rate in viscous sublayer
    //  NOTE: fv2 rewritten for better accuracy
    fv2    = (chi3 + (1 - chi)*power<3>(cv1_)) / ((1 + chi)*chi3 + power<3>(cv1_));
    shrmod = fv2 * nt/(power<2>(vk_*dist));

    if (shrmod < -cv2_*shr0)
    {
      shrmod = shr0 * (cv2_*cv2_*shr0 + cv3_*shrmod)/((cv3_ - 2*cv2_)*shr0 - shrmod);
      if (verbose) std::cout << "source: S~ branch 2" << std::endl;
    }

    T shrsum = shr0 + shrmod;

    if (shrsum == 0)
      r = rlim_;
    else
    {
#ifndef SMOOTHR
      r = min( rlim_, nt / ((shr0 + shrmod) * power<2>(vk_*dist)) );
#else
      T rtmp =  power<2,T>(shrsum*power<2>(vk_*dist)/nt);
      r = pow( 1./(rlim_*rlim_) + rtmp , -0.5  );
#endif
    }

    g  = r + cw2_*(power<6,T>(r) - r);
    fw = g * pow((1 + power<6>(cw3_)) / (power<6,T>(g) + power<6>(cw3_)), 1./6.);

    ft2  = ct3_*exp( -ct4_*chi*chi );
    wall = (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*power<2,Tq>(nt/dist)
           + cb1_*ft2*shr0*nt - cb1_*(1 - ft2)*shrmod*nt;

    if (verbose) std::cout << "prod2 = " << -rho*cb1_*ft2*shr0*nt << std::endl;
    if (verbose) std::cout << "prod3 = " <<  rho*cb1_*(1 - ft2)*shrmod*nt << std::endl;
    if (verbose) std::cout << "wall1 = " << -rho*cw1_*fw*power<2,Tq>(nt/dist) << std::endl;
    if (verbose) std::cout << "wall2 = " <<  rho*(cb1_/(vk_*vk_))*ft2*power<2,Tq>(nt/dist) << std::endl;
  }
  source(iSA) += rho*wall/ntref_;

  // non-conservative diffusion terms

  nonc = cb2_*rho*(ntx*ntx + nty*nty)/sigma_;

  if (verbose) std::cout << "nonc1 = " << nonc << std::endl;

  if (nt < 0)
  {
    Tq fn = (cn1_ + chi3) / (cn1_ - chi3);
    nonc -= (mu/rho + nt*fn)*(rhox*ntx + rhoy*nty)/sigma_;
    if (verbose) std::cout << "nonc2 = " << - (mu/rho + nt*fn)*(rhox*ntx + rhoy*nty)/sigma_ << std::endl;
  }
  else
  {
    nonc -= (mu/rho + nt)*(rhox*ntx + rhoy*nty)/sigma_;
    if (verbose) std::cout << "nonc2 = " << - (mu/rho + nt)*(rhox*ntx + rhoy*nty)/sigma_ << std::endl;
  }
  source(iSA) += -nonc/ntref_;

  // NS source terms (nominally empty function)
  BaseType::source( x, y, time, q, qx, qy, source );
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianSourceRow(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& dsdu ) const
{

  typedef typename promote_Surreal<Tq,Tg>::type T;
  Tq rho, u, v, t;
  T t_rho, t_rhou, t_rhov, t_rhoE;
  T rhox, rhoy, ux, uy, vx, vy, tx, ty;
  Tq nt;                                // nu_tilde
  T ntx, nty;
  Tq mu, mu_t;                          // molecular viscosity
  T mu_rho, mu_rhou, mu_rhov, mu_rhoE;
  T chi, chi_mu, chi_rho, chi_rhou, chi_rhov, chi_rhoE, chi_rhont;
  T fv2, ft2;
  T shr0;                              // shear rate: vorticity
  T shr0_rho, shr0_rhou, shr0_rhov;
  T shrmod;                            // shear rate: modified
  T r, g, fw;                          // wall terms
  T prod, wall, nonc;                  // production, destruction, non-conservative diffusion terms
  T prod_rho, prod_rhou, prod_rhov, prod_rhont;
  T wall_rho, wall_rhou=0, wall_rhov=0, wall_rhoE=0, wall_rhont;
  T nonc_rho, nonc_rhou=0, nonc_rhov=0, nonc_rhoE=0, nonc_rhont;

  Real Cv = gas_.Cv();

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalSA( q, nt );

  T E = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  // Temperature from conservative variables
  //t = ( rhoE/rho - 0.5*( rhou*rhou + rhov*rhov )/(rho*rho) ) / Cv
  t_rho  = ( -E/rho + ( u*u + v*v )/rho ) / Cv;
  t_rhou = (        - (   u       )/rho ) / Cv;
  t_rhov = (        - (         v )/rho ) / Cv;
  t_rhoE = (  1/rho                     ) / Cv;

  mu  = visc_.viscosity( t );
  visc_.viscosityJacobian( t, mu_t );

  // viscosity jacobian wrt conservative variables
  mu_rho  = mu_t*t_rho;
  mu_rhou = mu_t*t_rhou;
  mu_rhov = mu_t*t_rhov;
  mu_rhoE = mu_t*t_rhoE;

  chi       = rho*nt/mu;

  chi_mu    = -rho*nt/(mu*mu);
  chi_rho   = chi_mu*mu_rho;
  chi_rhou  = chi_mu*mu_rhou;
  chi_rhov  = chi_mu*mu_rhov;
  chi_rhoE  = chi_mu*mu_rhoE;
  chi_rhont = 1./mu;

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  // velocity gradient Jacobian wrt conservative variables
  // ux = rhoux/rho - rhou*rhox/(rho*rho);
  // rhoux = rho*ux + rhou*rhox/rho;
  T uy_rho  = -uy/rho + u*rhoy/(rho*rho);
  T uy_rhou =         -   rhoy/(rho*rho);

  T vx_rho  = -vx/rho + v*rhox/(rho*rho);
  T vx_rhov =         -   rhox/(rho*rho);

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  // SA working variable gradient Jacobian wrt conservative variables
  // ntx = rhontx/rho - rhont*rhox/(rho*rho);
  T ntx_rho   = -ntx/rho + nt*rhox/(rho*rho);
  T ntx_rhont =          -    rhox/(rho*rho);

  T nty_rho   = -nty/rho + nt*rhoy/(rho*rho);
  T nty_rhont =          -    rhoy/(rho*rho);

  // production and near-wall destruction terms

  // shear rate: vorticity
  shr0 = fabs(uy - vx);

  Real sgn = (uy - vx) > 0 ? 1 : -1;
  shr0_rho  = sgn*(uy_rho  - vx_rho );
  shr0_rhou = sgn*(uy_rhou          );
  shr0_rhov = sgn*(        - vx_rhov);


  // production term (vorticity component only)

  if (nt < 0)
  {
    prod       = cb1_*(1 - ct3_)*shr0*nt;
    prod_rho   = cb1_*(1 - ct3_)*shr0_rho*nt - cb1_*(1 - ct3_)*shr0*nt/rho;
    prod_rhou  = cb1_*(1 - ct3_)*shr0_rhou*nt;
    prod_rhov  = cb1_*(1 - ct3_)*shr0_rhov*nt;
    prod_rhont = cb1_*(1 - ct3_)*shr0/rho;
  }
  else
  {
    prod       = cb1_*shr0*nt;
    prod_rho   = cb1_*shr0_rho*nt - cb1_*shr0*nt/rho;
    prod_rhou  = cb1_*shr0_rhou*nt;
    prod_rhov  = cb1_*shr0_rhov*nt;
    prod_rhont = cb1_*shr0/rho;
  }
  //source(iSA) += -rho*prod;
  dsdu[0] += (-prod + -rho*prod_rho)/ntref_;
  dsdu[1] += -rho*prod_rhou/ntref_;
  dsdu[2] += -rho*prod_rhov/ntref_;
  dsdu[4] += -rho*prod_rhont;

  // near-wall destruction term (includes modified production component)

  T chi2 = power<2,T>(chi);
  T chi3 = power<3,T>(chi);


  if (dist < 1e-12)
  {
    wall       = -cw1_;
    wall_rho   = 0;
    wall_rhont = 0;
  }
  else if (nt < 0)
  {
    wall       = -cw1_*(nt/dist)*(nt/dist);
    wall_rho   =  cw1_*2*(nt/dist)*(nt/dist)/rho;
    wall_rhont = -cw1_*2*nt/rho/power<2>(dist);
  }
  else
  {
    Real cv13 = power<3>(cv1_);

    // modified shear rate in viscous sublayer
    //  NOTE: fv2 rewritten for better accuracy
    fv2     = (chi3 + (1 - chi)*cv13) / ((1 + chi)*chi3 + cv13);

    T fv2_chi = (3*chi2 - cv13) / ((1 + chi)*chi3 + cv13)
              - (chi3 + 3*chi2*(1 + chi))*(chi3 + (1 - chi)*cv13) / ( power<2,T>((1 + chi)*chi3 + cv13) );

    T fv2_rho   = fv2_chi*chi_rho;
    T fv2_rhou  = fv2_chi*chi_rhou;
    T fv2_rhov  = fv2_chi*chi_rhov;
    T fv2_rhoE  = fv2_chi*chi_rhoE;
    T fv2_rhont = fv2_chi*chi_rhont;

    shrmod = fv2 * nt/(power<2>(vk_*dist));

    T tmp = nt/(power<2>(vk_*dist));

    T shrmod_rho   = fv2_rho * tmp - fv2/rho * tmp;
    T shrmod_rhou  = fv2_rhou * tmp;
    T shrmod_rhov  = fv2_rhov * tmp;
    T shrmod_rhoE  = fv2_rhoE * tmp;
    T shrmod_rhont = fv2_rhont * tmp + fv2/rho/(power<2>(vk_*dist));

    if (shrmod < -cv2_*shr0)
    {
      T denom = ((cv3_ - 2*cv2_)*shr0 - shrmod);
      tmp = (cv2_*cv2_*shr0 + cv3_*shrmod)/denom;

      T tmp_rho   = (cv2_*cv2_*shr0_rho  + cv3_*shrmod_rho  )/denom - ((cv3_ - 2*cv2_)*shr0_rho  - shrmod_rho  )*tmp/denom;
      T tmp_rhou  = (cv2_*cv2_*shr0_rhou + cv3_*shrmod_rhou )/denom - ((cv3_ - 2*cv2_)*shr0_rhou - shrmod_rhou )*tmp/denom;
      T tmp_rhov  = (cv2_*cv2_*shr0_rhov + cv3_*shrmod_rhov )/denom - ((cv3_ - 2*cv2_)*shr0_rhov - shrmod_rhov )*tmp/denom;
      T tmp_rhoE  = (                      cv3_*shrmod_rhoE )/denom - (                          - shrmod_rhoE )*tmp/denom;
      T tmp_rhont = (                      cv3_*shrmod_rhont)/denom - (                          - shrmod_rhont)*tmp/denom;

      shrmod       = shr0 * tmp;

      shrmod_rho   = shr0_rho   * tmp + shr0 * tmp_rho;
      shrmod_rhou  = shr0_rhou  * tmp + shr0 * tmp_rhou;
      shrmod_rhov  = shr0_rhov  * tmp + shr0 * tmp_rhov;
      shrmod_rhoE  =                    shr0 * tmp_rhoE;
      shrmod_rhont =                    shr0 * tmp_rhont;
    }

    T r_rho   = 0;
    T r_rhou  = 0;
    T r_rhov  = 0;
    T r_rhoE  = 0;
    T r_rhont = 0;

    T shrsum = shr0 + shrmod;

    if (shrsum == 0)
      r = rlim_;
    else
    {
#ifndef SMOOTHR

      r = nt / ((shr0 + shrmod) * power<2>(vk_*dist));

      if ( r >= rlim_ )
        r = rlim_;
      else
      {
        tmp = -nt / (power<2,T>((shr0 + shrmod)*vk_*dist));
        r_rho   = (shr0_rho   + shrmod_rho  ) * tmp - nt/rho/((shr0 + shrmod) * (vk_*dist)*(vk_*dist));
        r_rhou  = (shr0_rhou  + shrmod_rhou ) * tmp;
        r_rhov  = (shr0_rhov  + shrmod_rhov ) * tmp;
        r_rhoE  = (             shrmod_rhoE ) * tmp;
        r_rhont = (             shrmod_rhont) * tmp + 1./rho/((shr0 + shrmod) * (vk_*dist)*(vk_*dist));
      }
#else

      T rtmp =  nt / ((shr0 + shrmod) * power<2>(vk_*dist));
      r = pow( 1./(rlim_*rlim_) + 1./(rtmp*rtmp) , -0.5  );

      T r_rtmp = pow( 1./(rlim_*rlim_) + 1./(rtmp*rtmp) , -1.5  )*(1./(rtmp*rtmp*rtmp));

      tmp = -nt / (power<2,T>((shr0 + shrmod)*vk_*dist));

      r_rho   = r_rtmp*((shr0_rho   + shrmod_rho  ) * tmp - nt/rho/((shr0 + shrmod) * (vk_*dist)*(vk_*dist)));
      r_rhou  = r_rtmp*(shr0_rhou  + shrmod_rhou ) * tmp;
      r_rhov  = r_rtmp*(shr0_rhov  + shrmod_rhov ) * tmp;
      r_rhoE  = r_rtmp*(             shrmod_rhoE ) * tmp;
      r_rhont = r_rtmp*((             shrmod_rhont) * tmp + 1./rho/((shr0 + shrmod) * (vk_*dist)*(vk_*dist)));

#endif
    }

    g  = r + cw2_*(power<6,T>(r) - r);

    T g_r     = 1 + cw2_*(6*power<5,T>(r) - 1);
    T g_rho   = g_r*r_rho;
    T g_rhou  = g_r*r_rhou;
    T g_rhov  = g_r*r_rhov;
    T g_rhoE  = g_r*r_rhoE;
    T g_rhont = g_r*r_rhont;

    Real cw36 = power<6>(cw3_);
    fw = g * pow((1 + cw36) / (power<6,T>(g) + cw36), 1./6.);

    T fw_g     = cw36/(1+cw36) * pow((1 + cw36) / (power<6,T>(g) + cw36), 7./6.);
    T fw_rho   = fw_g*g_rho;
    T fw_rhou  = fw_g*g_rhou;
    T fw_rhov  = fw_g*g_rhov;
    T fw_rhoE  = fw_g*g_rhoE;
    T fw_rhont = fw_g*g_rhont;

    ft2  = ct3_*exp( -ct4_*chi*chi );

    tmp = -ct4_*2*chi*ft2;
    T ft2_rho   = tmp*chi_rho;
    T ft2_rhou  = tmp*chi_rhou;
    T ft2_rhov  = tmp*chi_rhov;
    T ft2_rhoE  = tmp*chi_rhoE;
    T ft2_rhont = tmp*chi_rhont;

    wall = (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*(nt/dist)*(nt/dist)
           + cb1_*ft2*shr0*nt
           - cb1_*(1 - ft2)*shrmod*nt;

    wall_rho = (cw1_*fw_rho - (cb1_/(vk_*vk_))*ft2_rho)*(nt/dist)*(nt/dist)
             - (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*2*(nt/dist)*(nt/dist)/rho
             + cb1_*ft2_rho*shr0*nt + cb1_*ft2*shr0_rho*nt - cb1_*ft2*shr0*nt/rho
             + cb1_*ft2_rho*shrmod*nt - cb1_*(1 - ft2)*shrmod_rho*nt + cb1_*(1 - ft2)*shrmod*nt/rho;

    wall_rhou = (cw1_*fw_rhou - (cb1_/(vk_*vk_))*ft2_rhou)*(nt/dist)*(nt/dist)
              + cb1_*ft2_rhou*shr0*nt + cb1_*ft2*shr0_rhou*nt
              + cb1_*ft2_rhou*shrmod*nt - cb1_*(1 - ft2)*shrmod_rhou*nt;

    wall_rhov = (cw1_*fw_rhov - (cb1_/(vk_*vk_))*ft2_rhov)*(nt/dist)*(nt/dist)
              + cb1_*ft2_rhov*shr0*nt + cb1_*ft2*shr0_rhov*nt
              + cb1_*ft2_rhov*shrmod*nt - cb1_*(1 - ft2)*shrmod_rhov*nt;

    wall_rhoE = (cw1_*fw_rhoE - (cb1_/(vk_*vk_))*ft2_rhoE)*(nt/dist)*(nt/dist)
              + cb1_*ft2_rhoE*shr0*nt
              + cb1_*ft2_rhoE*shrmod*nt - cb1_*(1 - ft2)*shrmod_rhoE*nt;

    wall_rhont = (cw1_*fw_rhont - (cb1_/(vk_*vk_))*ft2_rhont)*(nt/dist)*(nt/dist)
               + (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*2*nt/power<2>(dist)/rho
               + cb1_*ft2_rhont*shr0*nt + cb1_*ft2*shr0/rho
               + cb1_*ft2_rhont*shrmod*nt - cb1_*(1 - ft2)*shrmod_rhont*nt - cb1_*(1 - ft2)*shrmod/rho;
  }
  //source(iSA) += rho*wall;
  dsdu[0] += (wall + rho*wall_rho)/ntref_;
  dsdu[1] += rho*wall_rhou/ntref_;
  dsdu[2] += rho*wall_rhov/ntref_;
  dsdu[3] += rho*wall_rhoE/ntref_;
  dsdu[4] += rho*wall_rhont;

  // non-conservative diffusion terms
  nonc       = cb2_*rho*(ntx*ntx + nty*nty)/sigma_;
  nonc_rho   = cb2_*(ntx*ntx + nty*nty)/sigma_ + cb2_*rho*2*(ntx_rho*ntx + nty_rho*nty)/sigma_;
  nonc_rhont = cb2_*rho*2*(ntx_rhont*ntx + nty_rhont*nty)/sigma_;

  if (nt < 0)
  {
    T fn = (cn1_ + chi3) / (cn1_ - chi3);

    T fn_chi = 3*chi2 / (cn1_ - chi3) + 3*chi2*(cn1_ + chi3) / power<2,T>(cn1_ - chi3);

    T fn_rho   = fn_chi*chi_rho;
    T fn_rhou  = fn_chi*chi_rhou;
    T fn_rhov  = fn_chi*chi_rhov;
    T fn_rhoE  = fn_chi*chi_rhoE;
    T fn_rhont = fn_chi*chi_rhont;

    T tmp = (rhox*ntx + rhoy*nty)/sigma_;

    nonc += - (mu/rho + nt*fn)*tmp;

    nonc_rho   += - (mu_rho/rho - mu/(rho*rho) - nt*fn/rho + nt*fn_rho  )*tmp - (mu/rho + nt*fn)*(rhox*ntx_rho + rhoy*nty_rho)/sigma_;
    nonc_rhou  += - (mu_rhou/rho                           + nt*fn_rhou )*tmp;
    nonc_rhov  += - (mu_rhov/rho                           + nt*fn_rhov )*tmp;
    nonc_rhoE  += - (mu_rhoE/rho                           + nt*fn_rhoE )*tmp;
    nonc_rhont += - (                               fn/rho + nt*fn_rhont)*tmp - (mu/rho + nt*fn)*(rhox*ntx_rhont + rhoy*nty_rhont)/sigma_;
  }
  else
  {
    T tmp = (rhox*ntx + rhoy*nty)/sigma_;

    nonc       += - (mu/rho + nt)*tmp;

    nonc_rho   += - (mu_rho/rho - mu/(rho*rho) - nt/rho)*tmp - (mu/rho + nt)*(rhox*ntx_rho + rhoy*nty_rho)/sigma_;
    nonc_rhou  += - (mu_rhou/rho                       )*tmp;
    nonc_rhov  += - (mu_rhov/rho                       )*tmp;
    nonc_rhoE  += - (mu_rhoE/rho                       )*tmp;
    nonc_rhont += - (                             1/rho)*tmp - (mu/rho + nt)*(rhox*ntx_rhont + rhoy*nty_rhont)/sigma_;
  }
  //source(iSA) += -nonc;
  dsdu[0] += -nonc_rho/ntref_;
  dsdu[1] += -nonc_rhou/ntref_;
  dsdu[2] += -nonc_rhov/ntref_;
  dsdu[3] += -nonc_rhoE/ntref_;
  dsdu[4] += -nonc_rhont;

  // NS source terms (nominally empty function)
//  BaseType::jacobianSource( x, y, time, q, qx, qy, dsdu );
}



// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianSource(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdu ) const
{
  ArrayQ<Ts> dsdu_row = 0;

  jacobianSourceRow(dist, x, y, time, q, qx, qy, dsdu_row);

  dsdu(iSA,0) += dsdu_row[0];
  dsdu(iSA,1) += dsdu_row[1];
  dsdu(iSA,2) += dsdu_row[2];
  dsdu(iSA,3) += dsdu_row[3];
  dsdu(iSA,4) += dsdu_row[4];

  // NS source terms (nominally empty function)
  BaseType::jacobianSource( x, y, time, q, qx, qy, dsdu );
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tu, class Tgu, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::perturbedSource(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tu>& du, const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
    ArrayQ<Ts>& dS ) const
{
  ArrayQ<Ts> dsdu_row = 0;
  jacobianSourceRow(dist, x, y, time, q, qx, qy, dsdu_row);

  dS(iSA) += dot(dsdu_row, du);

  ArrayQ<Ts> dsdux_row = 0;
  ArrayQ<Ts> dsduy_row = 0;
  jacobianGradientSourceRow(dist, x, y, time, q, qx, qy, dsdux_row, dsduy_row);

  dS(iSA) += dsdux_row[0]*dux[0] + dsduy_row[0]*duy[0];
  dS(iSA) +=                       dsduy_row[1]*duy[1];
  dS(iSA) += dsdux_row[2]*dux[2];
  dS(iSA) += dsdux_row[4]*dux[4] + dsduy_row[4]*duy[4];

  // NS source terms (nominally empty function)
  BaseType::perturbedSource( x, y, time, q, qx, qy, du, dux, duy, dS );
}



template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianHelperHACK(const Real& x, const Real& y, const Real& time,
                        const ArrayQ<Tq>& q, const MatrixQ<Ts>& mat, MatrixQ<Ts>& matHACK) const
{
  Ts s0 = mat(iSA,0);
  Ts s1 = mat(iSA,1);
  Ts s2 = mat(iSA,2);
  Ts s3 = mat(iSA,3);
  Ts s4 = mat(iSA,4);

  Tq rho, u, v, t;
  Tq nt;

  Real gamma = gas_.gamma();
  Real gm1 = gamma-1;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalSA( q, nt );
  Tq p = gas_.pressure(rho, t);
  Tq h = gas_.enthalpy(rho, t);
  Tq H = h + 0.5*(u*u + v*v);

  //precomputed  dv/du*mat*du/dv, assuming mat is zero outside of last row
  matHACK(iCont,0) += -nt/ntref_*s0 - nt*u/ntref_*s1 -nt*v/ntref_*s2 - nt*(H-p/rho)/ntref_*s3 - nt*nt/(ntref_*ntref_)*s4;

  matHACK(iCont,1) += -nt*u/ntref_*s0 - nt*(p/rho+u*u)/ntref_*s1 -nt*u*v/ntref_*s2 - u*H*nt/ntref_*s3 - u*nt*nt/(ntref_*ntref_)*s4;
  matHACK(iCont,2) += -nt*v/ntref_*s0 - nt*u*v/ntref_*s1 -nt*(p/rho+v*v)/ntref_*s2 - v*H*nt/ntref_*s3 - v*nt*nt/(ntref_*ntref_)*s4;
  matHACK(iCont,3) += -nt*(H -p/rho)/ntref_*s0 - nt*u*H/ntref_*s1 -nt*v*H/ntref_*s2
                    - nt*(H*H - p*p*gamma/gm1/rho/rho)/ntref_*s3 - nt*nt*(H - p/rho)/(ntref_*ntref_)*s4;

  matHACK(iCont,4) += -nt*nt/ntref_/ntref_*s0 -nt*nt*u/ntref_/ntref_*s1 -nt*nt*v/ntref_/ntref_*s2
                    - nt*nt*(H - p/rho)/ntref_/ntref_*s3 - nt*(nt*nt + ntref_*ntref_)/(ntref_*ntref_*ntref_)*s4;

  matHACK(iSA,0) += s0 + u*s1 + v*s2 + (H-p/rho)*s3 + nt/ntref_*s4;
  matHACK(iSA,1) += u*s0 + (u*u + p/rho)*s1 + u*v*s2 + u*H*s3 + u*nt/ntref_*s4;
  matHACK(iSA,2) += v*s0 + u*v*s1 + (v*v + p/rho)*s2 + v*H*s3 + v*nt/ntref_*s4;
  matHACK(iSA,3) += (H - p/rho)*s0 + u*H*s1 + v*H*s2 + (H*H - p*p*gamma/gm1/rho/rho)*s3 + nt*(H - p/rho)/ntref_*s4;
  matHACK(iSA,4) += nt/ntref_*s0 + nt*u/ntref_*s1 + nt*v/ntref_*s2 + nt*(H-p/rho)/ntref_*s3 + (1 + nt*nt/ntref_/ntref_)*s4;

}


// jacobian of source wrt conservation variables, hacked to give entropy variable transformation for GLS/AGLS
// dv/du*d(S)/d(U)*du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianSourceHACK(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdu ) const
{
  MatrixQ<Ts> tmp = 0;
  jacobianSource(dist, x, y, time, q, qx, qy, tmp);

  jacobianHelperHACK(x, y, time, q, tmp, dsdu);
}



// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianSourceAbsoluteValue(
    const Real& dist, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdu ) const
{
  MatrixQ<Ts> S = 0;

  jacobianSource(dist, x, y, time, q, qx, qy, S);

#if 1

  Ts tmp = S(iSA,4);
  Real eps0 = 1.e-12;
  Ts tmpabs = smoothabsP(tmp, eps0);

  dsdu += tmp/tmpabs*S;
#else
  Ts atmp = fabs(S(iSA,4));
  dsdu(iCont,0) += atmp;
  dsdu(ixMom,1) += atmp;
  dsdu(iyMom, 2) += atmp;
  dsdu(iEngy, 3) += atmp;
  dsdu(iSA,4) += atmp;
#endif

  // NS source terms (nominally empty function)
  BaseType::jacobianSourceAbsoluteValue( x, y, time, q, qx, qy, dsdu );
}

// jacobian of source wrt gradients of conservation variables: d(S)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceRow(
    const Real& dist,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& dsdux, ArrayQ<Ts>& dsduy ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq rho=0, u=0, v=0, t=0;
  T rhox, rhoy, ux, uy, vx, vy, tx, ty;
  Tq nt;                                // nu_tilde
  T  ntx, nty;
  Tq mu;                                // molecular viscosity
  Tq chi, chi3;
  T  ft2;
  T  shr0;                              // shear rate: vorticity
  T  r, g;                              // wall terms

  qInterpret_.eval( q, rho, u, v, t );

//  if (dist > 1e-8)
  qInterpret_.evalSA( q, nt );
//  else
//    nt = 0;

  mu  = visc_.viscosity( t );
  chi = rho*nt/mu;

  chi3 = power<3>(chi);

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  // velocity gradient Jacobian wrt conservative variables
  T uy_rhoy  = -u/rho;
  T uy_rhouy =  1/rho;

  T vx_rhox  = -v/rho;
  T vx_rhovx =  1/rho;

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  // production and near-wall destruction terms

  // shear rate: vorticity
  shr0 = sqrt((uy - vx)*(uy - vx));

  T shr0_uy =  (uy - vx)/max(shr0, T(1e-16));
  T shr0_vx = -(uy - vx)/max(shr0, T(1e-16));

  T shr0_rhox = shr0_vx*vx_rhox;
  T shr0_rhoy = shr0_uy*uy_rhoy;

  T shr0_rhouy = shr0_uy*uy_rhouy;

  T shr0_rhovx = shr0_vx*vx_rhovx;

  // production term (vorticity component only)

  T prod_rhox=0, prod_rhoy=0;
  T prod_rhouy=0;
  T prod_rhovx=0;

  if (nt < 0)
  {
    //prod       = cb1_*(1 - ct3_)*shr0*nt;
    prod_rhox  = cb1_*(1 - ct3_)*shr0_rhox*nt;
    prod_rhoy  = cb1_*(1 - ct3_)*shr0_rhoy*nt;

    prod_rhouy = cb1_*(1 - ct3_)*shr0_rhouy*nt;
    prod_rhovx = cb1_*(1 - ct3_)*shr0_rhovx*nt;

  }
  else
  {
    //prod       = cb1_*shr0*nt;
    prod_rhox  = cb1_*shr0_rhox*nt;
    prod_rhoy  = cb1_*shr0_rhoy*nt;

    prod_rhouy = cb1_*shr0_rhouy*nt;

    prod_rhovx = cb1_*shr0_rhovx*nt;

  }
  dsdux[0] += -rho*prod_rhox/ntref_;
  dsdux[2] += -rho*prod_rhovx/ntref_;

  dsduy[0] += -rho*prod_rhoy/ntref_;
  dsduy[1] += -rho*prod_rhouy/ntref_;

  // near-wall destruction term (includes modified production component)

  if (nt <= 0)
  {
    // wall = -cw1_*power<2,Tq>(nt/dist);
    // no dependency on gradient
  }
  else
  {
    Real distmod = max(dist, 1e-8);

    // modified shear rate in viscous sublayer
    //  NOTE: fv2 rewritten for better accuracy
    Tq fv2    = (chi3 + (1 - chi)*power<3>(cv1_)) / ((1 + chi)*chi3 + power<3>(cv1_));
    T  shrmod = fv2 * nt/(power<2>(vk_*distmod));

    T shrmod_rhox=0, shrmod_rhoy=0;
    T shrmod_rhouy=0;
    T shrmod_rhovx=0;

    if (shrmod < -cv2_*shr0)
    {
      // shrmod = shr0 * (cv2_*cv2_*shr0 + cv3_*shrmod)/((cv3_ - 2*cv2_)*shr0 - shrmod);
      T num   = (cv2_*cv2_*shr0 + cv3_*shrmod)*shr0;
      T denom = (cv3_ - 2*cv2_)*shr0 - shrmod;
      T tmp = num*(cv3_ - 2*cv2_)/power<2>(denom);

      shrmod_rhox = (2*cv2_*cv2_*shr0*shr0_rhox + shr0_rhox*cv3_*shrmod)/denom - tmp*shr0_rhox;
      shrmod_rhoy = (2*cv2_*cv2_*shr0*shr0_rhoy + shr0_rhoy*cv3_*shrmod)/denom - tmp*shr0_rhoy;

      shrmod_rhouy = (2*cv2_*cv2_*shr0*shr0_rhouy + shr0_rhouy*cv3_*shrmod)/denom - tmp*shr0_rhouy;

      shrmod_rhovx = (2*cv2_*cv2_*shr0*shr0_rhovx + shr0_rhovx*cv3_*shrmod)/denom - tmp*shr0_rhovx;

      shrmod = num/denom;
    }

    T r_rhox=0, r_rhoy=0;
    T r_rhouy=0;
    T r_rhovx=0;

    if (shr0 + shrmod == 0)
      r = rlim_;
    else
    {
#ifndef SMOOTHR
      r = nt / ((shr0 + shrmod) * power<2>(vk_*distmod));

      if ( r > rlim_ )
        r = rlim_;
      else
      {
        T denom = (power<2,T>(shr0 + shrmod) * power<2>(vk_*distmod));

        r_rhox = -nt*(shr0_rhox + shrmod_rhox) / denom;
        r_rhoy = -nt*(shr0_rhoy + shrmod_rhoy) / denom;

        r_rhouy = -nt*(shr0_rhouy + shrmod_rhouy) / denom;

        r_rhovx = -nt*(shr0_rhovx + shrmod_rhovx) / denom;

      }
#else
      T rtmp =  nt / ((shr0 + shrmod) * power<2>(vk_*distmod));
      r = pow( 1./(rlim_*rlim_) + 1./(rtmp*rtmp) , -0.5  );

      T r_rtmp = pow( 1./(rlim_*rlim_) + 1./(rtmp*rtmp) , -1.5  )*(1./(rtmp*rtmp*rtmp));


      T denom = (power<2,T>(shr0 + shrmod) * power<2>(vk_*distmod));

      r_rhox = -r_rtmp*nt*(shr0_rhox + shrmod_rhox) / denom;
      r_rhoy = -r_rtmp*nt*(shr0_rhoy + shrmod_rhoy) / denom;

      r_rhouy = -r_rtmp*nt*(shr0_rhouy + shrmod_rhouy) / denom;

      r_rhovx = -r_rtmp*nt*(shr0_rhovx + shrmod_rhovx) / denom;

#endif
    }

    g      = r + cw2_*(power<6>(r) - r);

    T r5 = power<5>(r);
    T g_rhox = r_rhox + cw2_*(6*r5*r_rhox - r_rhox);
    T g_rhoy = r_rhoy + cw2_*(6*r5*r_rhoy - r_rhoy);

    T g_rhouy = r_rhouy + cw2_*(6*r5*r_rhouy - r_rhouy);

    T g_rhovx = r_rhovx + cw2_*(6*r5*r_rhovx - r_rhovx);

    //fw = g * pow((1 + power<6>(cw3_)) / (power<6>(g) + power<6>(cw3_)), 1./6.);

    Real cw36 = power<6>(cw3_);
    T g6 = power<6>(g);
//    T tmp = pow((1 + cw36) / (g6 + cw36), 1./6.);
//    T fw_g = tmp - g6 * (1 + cw36) / (power<5>(tmp)*power<2,T>( g6 + cw36 ));
    T fw_g = cw36*pow((1 + cw36) / (g6 + cw36), 7./6.)/(1. + cw36);

    T fw_rhox = fw_g*g_rhox;
    T fw_rhoy = fw_g*g_rhoy;

    T fw_rhouy = fw_g*g_rhouy;

    T fw_rhovx = fw_g*g_rhovx;

    ft2  = ct3_*exp( -ct4_*chi*chi );

    //wall = (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*power<2,Tq>(nt/dist) + cb1_*ft2*shr0*nt - cb1_*(1 - ft2)*shrmod*nt;
    T wall_rhox = cw1_*fw_rhox*power<2,Tq>(nt/distmod) + cb1_*ft2*shr0_rhox*nt - cb1_*(1 - ft2)*shrmod_rhox*nt;
    T wall_rhoy = cw1_*fw_rhoy*power<2,Tq>(nt/distmod) + cb1_*ft2*shr0_rhoy*nt - cb1_*(1 - ft2)*shrmod_rhoy*nt;

    T wall_rhouy = cw1_*fw_rhouy*power<2,Tq>(nt/distmod) + cb1_*ft2*shr0_rhouy*nt - cb1_*(1 - ft2)*shrmod_rhouy*nt;

    T wall_rhovx = cw1_*fw_rhovx*power<2,Tq>(nt/distmod) + cb1_*ft2*shr0_rhovx*nt - cb1_*(1 - ft2)*shrmod_rhovx*nt;

    dsdux[0] += rho*wall_rhox/ntref_;
    dsdux[2] += rho*wall_rhovx/ntref_;

    dsduy[0] += rho*wall_rhoy/ntref_;
    dsduy[1] += rho*wall_rhouy/ntref_;
  }

  // non-conservative diffusion terms

  //nonc = cb2_*rho*(ntx*ntx + nty*nty + ntz*ntz)/sigma_;
  T nonc_rhox   = -2*cb2_*nt*ntx/sigma_;
  T nonc_rhoy   = -2*cb2_*nt*nty/sigma_;
  T nonc_rhontx = 2*cb2_*ntx/sigma_;
  T nonc_rhonty = 2*cb2_*nty/sigma_;

  if (nt < 0)
  {
    Tq fn = (cn1_ + chi3) / (cn1_ - chi3);
    //nonc += - (mu/rho + nt*fn)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

    nonc_rhox -= (mu/rho + nt*fn)*(rho*ntx - nt*rhox)/rho/sigma_;
    nonc_rhoy -= (mu/rho + nt*fn)*(rho*nty - nt*rhoy)/rho/sigma_;
    nonc_rhontx -= (mu/rho + nt*fn)*(rhox/rho)/sigma_;
    nonc_rhonty -= (mu/rho + nt*fn)*(rhoy/rho)/sigma_;
  }
  else
  {
    //nonc += - (mu/rho + nt)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

    nonc_rhox += - (mu/rho + nt)*(rho*ntx - nt*rhox)/rho/sigma_;
    nonc_rhoy += - (mu/rho + nt)*(rho*nty - nt*rhoy)/rho/sigma_;
    nonc_rhontx += - (mu/rho + nt)*(rhox/rho)/sigma_;
    nonc_rhonty += - (mu/rho + nt)*(rhoy/rho)/sigma_;
  }

  dsdux[0] += -nonc_rhox/ntref_;
  dsdux[4] += -nonc_rhontx;

  dsduy[0] += -nonc_rhoy/ntref_;
  dsduy[4] += -nonc_rhonty;
}


// jacobian of source wrt gradients of conservation variables: d(S)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianGradientSource(
    const Real& dist,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
{
  ArrayQ<Ts> dsdux_row = 0;
  ArrayQ<Ts> dsduy_row = 0;

  jacobianGradientSourceRow(dist, x, y, time, q, qx, qy, dsdux_row, dsduy_row);

  dsdux(iSA, 0) += dsdux_row[0];
  dsdux(iSA, 2) += dsdux_row[2];
  dsdux(iSA, 4) += dsdux_row[4];

  dsduy(iSA, 0) += dsduy_row[0];
  dsduy(iSA, 1) += dsduy_row[1];
  dsduy(iSA, 4) += dsduy_row[4];

  // NS source terms (nominally empty function)
  BaseType::jacobianGradientSource( x, y, time, q, qx, qy, dsdux, dsduy );
}


// jacobian of source wrt gradients of conservation variables with entropy variable transformations:
// JUST FOR GLS AND AGLS
//  dv/du*d(S)/d(Ux)*dudv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceHACK(
    const Real& dist,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
{
  MatrixQ<Ts> tmpx = 0, tmpy=0;
  jacobianGradientSource(dist, x, y, time, q, qx, qy, tmpx, tmpy);

  jacobianHelperHACK(x, y, time, q, tmpx, dsdux);
  jacobianHelperHACK(x, y, time, q, tmpy, dsduy);

}


// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceAbsoluteValue(
    const Real& dist,
    const Real& x, const Real& y, const Real& time, const Real& Nx, const Real& Ny,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& divSdotN ) const
{

  Real nMag = sqrt(Nx*Nx + Ny*Ny );
  Real eps0 = 1.e-9;
  Real nMag2 = smoothabsP(nMag, eps0);

  Real nx = Nx/nMag2;
  Real ny = Ny/nMag2;

  MatrixQ<Ts> dsdux = 0;
  MatrixQ<Ts> dsduy = 0;

  jacobianGradientSource(dist, x, y, time, q, qx, qy, dsdux, dsduy);

  Ts tmp = dsdux(iSA,4)*nx + dsduy(iSA,4)*ny;

  Ts sign = 0;
  if (tmp >0)
    sign = 1;
  else if (tmp < 0)
    sign = -1;
  else
  {}

  divSdotN += (dsdux*nx + dsduy*ny)*sign;

  // NS source terms (nominally empty function)
  BaseType::jacobianGradientSourceAbsoluteValue( x, y, time, Nx, Ny, q, qx, qy, divSdotN );
}


// jacobian of source wrt gradients of conservation variables: d(S)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceGradient(
    const Real& dist, //const Real& distx, const Real& disty,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Ts>& div_dsdgradu ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type Tpq;

  typedef typename promote_Surreal<Tq, Tg, Th>::type T;
  Tq rho=0, u=0, v=0, t=0;
  Tpq rhox, rhoy, ux, uy, vx, vy, tx, ty = 0;
  Tq nt;                                // nu_tilde
  Tpq  ntx, nty;
  Tq mu;                                // molecular viscosity
  Tq chi, chi3;
  T  ft2;
  T  shr0;                              // shear rate: vorticity
  T  r, g;                              // wall terms

  Real distx = 0, disty = 0; //don't use distance function gradients for now

  Real dist2 = max(dist, 1e-10);

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );

  // evaluate second derivatives
  T rhoxx = 0, rhoxy = 0, rhoyy = 0;
  T uxx = 0, uxy = 0, uyy = 0;
  T vxx = 0, vxy = 0, vyy = 0;
  T txx = 0, txy = 0, tyy = 0;
  T ntxx = 0, ntxy = 0, ntyy = 0;

  qInterpret_.evalSecondGradient( q, qx, qy,
                                  qxx, qxy, qyy,
                                  rhoxx, rhoxy, rhoyy,
                                  uxx, uxy, uyy,
                                  vxx, vxy, vyy,
                                  txx, txy, tyy );

  qInterpret_.evalSAHessian( q, qx, qx, qxx, ntxx);
  qInterpret_.evalSAHessian( q, qx, qy, qxy, ntxy);
  qInterpret_.evalSAHessian( q, qy, qy, qyy, ntyy);

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalSA( q, nt );
  mu  = visc_.viscosity( t );
  chi = rho*nt/mu;

  Tq mu_t = 0;
  visc_.viscosityJacobian( t, mu_t );
  T mux = mu_t*tx;
  T muy = mu_t*ty;

  T chix = rhox*nt/mu + rho*ntx/mu - rho*nt/(mu*mu)*mux;
  T chiy = rhoy*nt/mu + rho*nty/mu - rho*nt/(mu*mu)*muy;

  chi3 = power<3>(chi);
  T chi3x = 3*power<2>(chi)*chix;
  T chi3y = 3*power<2>(chi)*chiy;


  // velocity gradient Jacobian wrt conservative variables
  T uy_rhoy  = -u/rho;
  T uy_rhouy =  1/rho;

  T vx_rhox  = -v/rho;
  T vx_rhovx =  1/rho;

  T uy_rhoyy = -uy/rho + u*rhoy/(rho*rho);
  T uy_rhouyy = -rhoy/(rho*rho);

  T vx_rhoxx = -vx/rho + v*rhox/(rho*rho);
  T vx_rhovxx = -rhox/(rho*rho);



  // production and near-wall destruction terms

  // shear rate: vorticity
  shr0 = sqrt((uy - vx)*(uy - vx));

  T shr0x = (uy - vx)/max(shr0, T(1e-16))*(uxy - vxx);
  T shr0y = (uy - vx)/max(shr0, T(1e-16))*(uyy - vxy);

  T shr0_uy =  (uy - vx)/max(shr0, T(1e-16));
  T shr0_vx = -(uy - vx)/max(shr0, T(1e-16));

//  T shr0_vxx = -(uxy - vxx)/max(shr0, T(1e-16)) + (uy - vx)*shr0x/max( power<2,T>(shr0), T(1e-16));
//  T shr0_uyy = (uyy - vxy)/max(shr0, T(1e-16)) - (uy - vx)*shr0y/max( power<2,T>(shr0), T(1e-16));
  T shr0_vxx = 0; //assume shr0_uy will not flip sign; shr0_vx = shr0_uy = +/-1
  T shr0_uyy = 0;

  T shr0_rhox = shr0_vx*vx_rhox;
  T shr0_rhoy = shr0_uy*uy_rhoy;

  T shr0_rhouy = shr0_uy*uy_rhouy;
  T shr0_rhovx = shr0_vx*vx_rhovx;

  T shr0_rhoxx =  shr0_vxx*vx_rhox + shr0_vx*vx_rhoxx;
  T shr0_rhoyy =  shr0_uyy*uy_rhoy + shr0_uy*uy_rhoyy;

  T shr0_rhovxx =  shr0_vxx*vx_rhovx + shr0_vx*vx_rhovxx;
  T shr0_rhouyy =  shr0_uyy*uy_rhouy + shr0_uy*uy_rhouyy;

  // production term (vorticity component only)

  T prod_rhox, prod_rhoy;
  T prod_rhouy;
  T prod_rhovx;
  T prod_rhoxx, prod_rhoyy, prod_rhouyy, prod_rhovxx;
  if (nt < 0)
  {
    //prod       = cb1_*(1 - ct3_)*shr0*nt;
    prod_rhox  = cb1_*(1 - ct3_)*shr0_rhox*nt;
    prod_rhoy  = cb1_*(1 - ct3_)*shr0_rhoy*nt;

    prod_rhouy = cb1_*(1 - ct3_)*shr0_rhouy*nt;
    prod_rhovx = cb1_*(1 - ct3_)*shr0_rhovx*nt;

    //shr0 doesn't depend on second derivatives
    prod_rhoxx = cb1_*(1 - ct3_)*(shr0_rhoxx*nt + shr0_rhox*ntx);
    prod_rhoyy = cb1_*(1 - ct3_)*(shr0_rhoyy*nt +shr0_rhoy*nty);
    prod_rhouyy = cb1_*(1 - ct3_)*(shr0_rhouyy*nt + shr0_rhouy*nty);
    prod_rhovxx = cb1_*(1 - ct3_)*(shr0_rhovxx*nt +shr0_rhovx*ntx);

  }
  else
  {
    //prod       = cb1_*shr0*nt;
    prod_rhox  = cb1_*shr0_rhox*nt;
    prod_rhoy  = cb1_*shr0_rhoy*nt;

    prod_rhouy = cb1_*shr0_rhouy*nt;
    prod_rhovx = cb1_*shr0_rhovx*nt;

    prod_rhoxx = cb1_*(shr0_rhoxx*nt + shr0_rhox*ntx);
    prod_rhoyy = cb1_*(shr0_rhoyy*nt +shr0_rhoy*nty);
    prod_rhouyy = cb1_*(shr0_rhouyy*nt + shr0_rhouy*nty);
    prod_rhovxx = cb1_*(shr0_rhovxx*nt +shr0_rhovx*ntx);

  }

//  dsdux(iSA, 0) += -rho*prod_rhox;
//  dsdux(iSA, 2) += -rho*prod_rhovx;
//
//  dsduy(iSA, 0) += -rho*prod_rhoy;
//  dsduy(iSA, 1) += -rho*prod_rhouy;
//
  div_dsdgradu(iSA, 0) += (-rhox*prod_rhox - rho*prod_rhoxx)/ntref_;
  div_dsdgradu(iSA, 2) += (-rhox*prod_rhovx - rho*prod_rhovxx)/ntref_;

  div_dsdgradu(iSA, 0) += (-rhoy*prod_rhoy - rho*prod_rhoyy)/ntref_;
  div_dsdgradu(iSA, 1) += (-rhoy*prod_rhouy - rho*prod_rhouyy)/ntref_;


  // near-wall destruction term (includes modified production component)

  if (nt < 0)
  {
    // wall = -cw1_*power<2,Tq>(nt/dist);
    // no dependency on gradient
  }
  else
  {
    // modified shear rate in viscous sublayer
    //  NOTE: fv2 rewritten for better accuracy
    Real cv13 = power<3>(cv1_);
    Tq fv2    = (chi3 + (1 - chi)*cv13) / ((1 + chi)*chi3 + cv13);
    T  shrmod = fv2 * nt/(power<2>(vk_*dist2));

    T shrmod_rhox=0, shrmod_rhoy=0;
    T shrmod_rhouy=0;
    T shrmod_rhovx=0;

    T chi4 = power<4>(chi);
    T chi6 = power<6>(chi);
    Real cv16 = power<6>(cv1_);

    T fv2x = - (cv16 + cv13*(2. - 3.*chi)*chi3 + chi6)*chix
             /( cv13 + chi3 + chi4  )/( cv13 + chi3 + chi4  );

    T fv2y = -  (cv16 + cv13*(2. - 3.*chi)*chi3 + chi6)*chiy
             /( cv13 + chi3 + chi4  )/( cv13 + chi3 + chi4  );

    T shrmodx = (fv2x *nt + fv2*ntx)/(power<2>(vk_*dist2)) - 2*shrmod/dist2*distx;
    T shrmody = (fv2y *nt + fv2*nty)/(power<2>(vk_*dist2)) - 2*shrmod/dist2*disty;

    T shrmod_rhoxx = 0, shrmod_rhoyy = 0;
    T shrmod_rhovxx = 0, shrmod_rhouyy = 0;

    if (shrmod < -cv2_*shr0)
    {
      // shrmod = shr0 * (cv2_*cv2_*shr0 + cv3_*shrmod)/((cv3_ - 2*cv2_)*shr0 - shrmod);
      T num   = (cv2_*cv2_*shr0*shr0 + shr0*cv3_*shrmod);
      T denom = (cv3_ - 2*cv2_)*shr0 - shrmod;
      T tmp = num*(cv3_ - 2*cv2_)/power<2>(denom);

      T numx =  (2.*cv2_*cv2_*shr0*shr0x + cv3_*(shr0x*shrmod + shr0*shrmodx));
      T numy =  (2.*cv2_*cv2_*shr0*shr0y + cv3_*(shr0y*shrmod + shr0*shrmody));

      T denomx = (cv3_ - 2*cv2_)*shr0x - shrmodx;
      T denomy = (cv3_ - 2*cv2_)*shr0y - shrmody;

      T tmpx = numx*(cv3_ - 2*cv2_)/power<2>(denom) - 2*num*(cv3_ - 2*cv2_)*denomx/power<3>(denom);
      T tmpy = numy*(cv3_ - 2*cv2_)/power<2>(denom) - 2*num*(cv3_ - 2*cv2_)*denomy/power<3>(denom);

      shrmod_rhox = (2.*cv2_*cv2_*shr0*shr0_rhox + shr0_rhox*cv3_*shrmod)/denom - tmp*shr0_rhox;
      shrmod_rhoy = (2.*cv2_*cv2_*shr0*shr0_rhoy + shr0_rhoy*cv3_*shrmod)/denom - tmp*shr0_rhoy;

      shrmod_rhouy = (2.*cv2_*cv2_*shr0*shr0_rhouy + shr0_rhouy*cv3_*shrmod)/denom - tmp*shr0_rhouy;

      shrmod_rhovx = (2.*cv2_*cv2_*shr0*shr0_rhovx + shr0_rhovx*cv3_*shrmod)/denom - tmp*shr0_rhovx;

      shrmod_rhoxx = 2.*cv2_*cv2_*(shr0x*shr0_rhox + shr0*shr0_rhoxx) /denom
                      + cv3_*(shr0_rhoxx*shrmod + shr0_rhox*shrmodx)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhox + shr0_rhox*cv3_*shrmod)*denomx/power<2,T>(denom)
                     - tmpx*shr0_rhox - tmp*shr0_rhoxx;

      shrmod_rhoyy = 2.*cv2_*cv2_*(shr0y*shr0_rhoy + shr0*shr0_rhoyy) /denom
                      + cv3_*(shr0_rhoyy*shrmod + shr0_rhoy*shrmody)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhoy + shr0_rhoy*cv3_*shrmod)*denomy/power<2,T>(denom)
                     - tmpy*shr0_rhoy - tmp*shr0_rhoyy;

      shrmod_rhouyy = 2.*cv2_*cv2_*(shr0y*shr0_rhouy + shr0*shr0_rhouyy) /denom
                      + cv3_*(shr0_rhouyy*shrmod + shr0_rhouy*shrmody)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhouy + shr0_rhouy*cv3_*shrmod)*denomy/power<2,T>(denom)
                     - tmpy*shr0_rhouy - tmp*shr0_rhouyy;

      shrmod_rhovxx = 2.*cv2_*cv2_*(shr0x*shr0_rhovx + shr0*shr0_rhovxx) /denom
                      + cv3_*(shr0_rhovxx*shrmod + shr0_rhovx*shrmodx)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhovx + shr0_rhovx*cv3_*shrmod)*denomx/power<2,T>(denom)
                     - tmpx*shr0_rhovx - tmp*shr0_rhovxx;

      //new values

      shrmod = num/denom;

      shrmodx = numx/denom - num/(denom*denom)*denomx;
      shrmody = numy/denom - num/(denom*denom)*denomy;

    }

    T r_rhox=0, r_rhoy=0;
    T r_rhouy=0;
    T r_rhovx=0;

    T rx = 0; T ry = 0;
    T r_rhoxx = 0; T r_rhoyy = 0;
    T r_rhouyy = 0; T r_rhovxx = 0;

    T shrsum = shr0 + shrmod;
    if (shrsum == 0)
    {
      r = rlim_;
    }
    else
    {
#ifndef SMOOTHR
      r = nt / (shrsum * power<2>(vk_*dist));

      if ( r > rlim_ )
        r = rlim_;
      else
      {
        T denom = power<2,T>(shr0 + shrmod) * power<2>(vk_*dist);
        T denomx = 2*(shr0 + shrmod)*(shr0x + shrmodx)*power<2>(vk_*dist)
                   + 2.*power<2,T>(shr0 + shrmod)*vk_*vk_*dist*distx;
        T denomy = 2*(shr0 + shrmod)*(shr0y + shrmody)*power<2>(vk_*dist)
                   + 2.*power<2,T>(shr0 + shrmod)*vk_*vk_*dist*disty;

        r_rhox = -nt*(shr0_rhox + shrmod_rhox) / denom;
        r_rhoy = -nt*(shr0_rhoy + shrmod_rhoy) / denom;

        r_rhouy = -nt*(shr0_rhouy + shrmod_rhouy) / denom;

        r_rhovx = -nt*(shr0_rhovx + shrmod_rhovx) / denom;

        rx = ntx / (shrsum * power<2>(vk_*dist))
               - nt*(shr0x + shrmodx) / (power<2,T>(shrsum) * power<2>(vk_*dist)) - 2./dist*r*distx;

        ry = nty / (shrsum * power<2>(vk_*dist))
               - nt*(shr0y + shrmody) / (power<2,T>(shrsum) * power<2>(vk_*dist)) - 2./dist*r*disty;

        r_rhoxx = -ntx*(shr0_rhox + shrmod_rhox) / denom - nt*(shr0_rhoxx + shrmod_rhoxx)/denom
                   + nt*(shr0_rhox + shrmod_rhox)*denomx /power<2,T>(denom);
        r_rhoyy = -nty*(shr0_rhoy + shrmod_rhoy) / denom - nt*(shr0_rhoyy + shrmod_rhoyy)/denom
                   + nt*(shr0_rhoy + shrmod_rhoy)*denomy /power<2,T>(denom);
        r_rhouyy = -nty*(shr0_rhouy + shrmod_rhouy) / denom - nt*(shr0_rhouyy + shrmod_rhouyy)/denom
                   + nt*(shr0_rhouy + shrmod_rhouy)*denomy /power<2,T>(denom);
        r_rhovxx = -ntx*(shr0_rhovx + shrmod_rhovx) / denom - nt*(shr0_rhovxx + shrmod_rhovxx)/denom
                   + nt*(shr0_rhovx + shrmod_rhovx)*denomx /power<2,T>(denom);

      }
#else
      T rtmp =  nt / ((shr0 + shrmod) * power<2>(vk_*dist2));
      r = pow( 1./(rlim_*rlim_) + 1./(rtmp*rtmp) , -0.5  );

      T r_rtmp = pow( 1./(rlim_*rlim_) + 1./(rtmp*rtmp) , -1.5  )*(1./(rtmp*rtmp*rtmp));

      T denom = power<2,T>(shr0 + shrmod) * power<2>(vk_*dist2);
      T denomx = 2*(shr0 + shrmod)*(shr0x + shrmodx)*power<2>(vk_*dist2)
                 + 2.*power<2,T>(shr0 + shrmod)*vk_*vk_*dist2*distx;
      T denomy = 2*(shr0 + shrmod)*(shr0y + shrmody)*power<2>(vk_*dist2)
                 + 2.*power<2,T>(shr0 + shrmod)*vk_*vk_*dist2*disty;

      r_rhox = -r_rtmp*nt*(shr0_rhox + shrmod_rhox) / denom;
      r_rhoy = -r_rtmp*nt*(shr0_rhoy + shrmod_rhoy) / denom;

      r_rhouy = -r_rtmp*nt*(shr0_rhouy + shrmod_rhouy) / denom;

      r_rhovx = -r_rtmp*nt*(shr0_rhovx + shrmod_rhovx) / denom;

      rx = r_rtmp*(ntx / (shrsum * power<2>(vk_*dist2))
             - nt*(shr0x + shrmodx) / (power<2,T>(shrsum) * power<2>(vk_*dist2)) - 2./dist2*rtmp*distx);

      ry = r_rtmp*(nty / (shrsum * power<2>(vk_*dist2))
             - nt*(shr0y + shrmody) / (power<2,T>(shrsum) * power<2>(vk_*dist2)) - 2./dist2*rtmp*disty);

      r_rhoxx = r_rtmp*(-ntx*(shr0_rhox + shrmod_rhox) / denom - nt*(shr0_rhoxx + shrmod_rhoxx)/denom
                 + nt*(shr0_rhox + shrmod_rhox)*denomx /power<2,T>(denom));
      r_rhoyy = r_rtmp*(-nty*(shr0_rhoy + shrmod_rhoy) / denom - nt*(shr0_rhoyy + shrmod_rhoyy)/denom
                 + nt*(shr0_rhoy + shrmod_rhoy)*denomy /power<2,T>(denom));
      r_rhouyy = r_rtmp*(-nty*(shr0_rhouy + shrmod_rhouy) / denom - nt*(shr0_rhouyy + shrmod_rhouyy)/denom
                 + nt*(shr0_rhouy + shrmod_rhouy)*denomy /power<2,T>(denom));
      r_rhovxx = r_rtmp*(-ntx*(shr0_rhovx + shrmod_rhovx) / denom - nt*(shr0_rhovxx + shrmod_rhovxx)/denom
                 + nt*(shr0_rhovx + shrmod_rhovx)*denomx /power<2,T>(denom));

#endif
    }

    T r5 = power<5>(r);
    T r4 = power<4>(r);

    g      = r + cw2_*(power<6>(r) - r);
    T gx = rx + cw2_*(6.*r5*rx - rx);
    T gy = ry + cw2_*(6.*r5*ry - ry);

    T g_rhox = r_rhox + cw2_*(6.*r5*r_rhox - r_rhox);
    T g_rhoy = r_rhoy + cw2_*(6.*r5*r_rhoy - r_rhoy);

    T g_rhouy = r_rhouy + cw2_*(6.*r5*r_rhouy - r_rhouy);
    T g_rhovx = r_rhovx + cw2_*(6.*r5*r_rhovx - r_rhovx);

    T g_rhoxx = r_rhoxx + cw2_*(30.*r4*r_rhox*rx + 6.*r5*r_rhoxx - r_rhoxx);
    T g_rhoyy = r_rhoyy + cw2_*(30.*r4*r_rhoy*ry + 6.*r5*r_rhoyy - r_rhoyy);
    T g_rhouyy = r_rhouyy + cw2_*(30.*r4*r_rhouy*ry + 6.*r5*r_rhouyy - r_rhouyy);
    T g_rhovxx = r_rhovxx + cw2_*(30.*r4*r_rhovx*rx + 6.*r5*r_rhovxx - r_rhovxx);

    //fw = g * pow((1 + power<6>(cw3_)) / (power<6>(g) + power<6>(cw3_)), 1./6.);

    Real cw36 = power<6>(cw3_);
    T g6 = power<6>(g);
    T g5 = power<5>(g);
    T tmp = pow((1 + cw36) / (g6 + cw36), 1./6.);

    T tmpx = -(1 + cw36)*g5*gx/power<2,T>(g6 + cw36) / power<5>(tmp);
    T tmpy = -(1 + cw36)*g5*gy/power<2,T>(g6 + cw36) / power<5>(tmp);

    T fw_g = tmp - g6 * (1 + cw36) / (power<5>(tmp)*power<2,T>( g6 + cw36 ));

    T fw_gx = tmpx - 6.*g5 * gx* (1 + cw36) / (power<5>(tmp)*power<2,T>( g6 + cw36 ))
                     +5*g6 * (1 + cw36)*tmpx / (power<6>(tmp)*power<2,T>( g6 + cw36 ))
                     +12*g6*(1+cw36)*g5*gx / (power<5>(tmp)*power<3,T>( g6 + cw36 ));

    T fw_gy = tmpy - 6*g5 * gy* (1 + cw36) / (power<5>(tmp)*power<2,T>( g6 + cw36 ))
                         +5*g6 * (1 + cw36)*tmpy / (power<6>(tmp)*power<2,T>( g6 + cw36 ))
                         +12*g6*(1+cw36)*g5*gy / (power<5>(tmp)*power<3,T>( g6 + cw36 ));

    T fw_rhox = fw_g*g_rhox;
    T fw_rhoy = fw_g*g_rhoy;

    T fw_rhouy = fw_g*g_rhouy;
    T fw_rhovx = fw_g*g_rhovx;

    T fw_rhoxx = fw_gx*g_rhox + fw_g*g_rhoxx;
    T fw_rhoyy = fw_gy*g_rhoy + fw_g*g_rhoyy;

    T fw_rhouyy = fw_gy*g_rhouy + fw_g*g_rhouyy;
    T fw_rhovxx = fw_gx*g_rhovx + fw_g*g_rhovxx;

    ft2  = ct3_*exp( -ct4_*chi*chi );
    T ft2x = ct3_*exp( -ct4_*chi*chi )*(-ct4_*2*chi*chix);
    T ft2y = ct3_*exp( -ct4_*chi*chi )*(-ct4_*2*chi*chiy);

    T wall_rhox = cw1_*fw_rhox*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhox*nt - cb1_*(1 - ft2)*shrmod_rhox*nt;
    T wall_rhoy = cw1_*fw_rhoy*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhoy*nt - cb1_*(1 - ft2)*shrmod_rhoy*nt;

    T wall_rhouy = cw1_*fw_rhouy*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhouy*nt - cb1_*(1 - ft2)*shrmod_rhouy*nt;
    T wall_rhovx = cw1_*fw_rhovx*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhovx*nt - cb1_*(1 - ft2)*shrmod_rhovx*nt;

    T wall_rhoxx = cw1_*fw_rhoxx*power<2,Tq>(nt/dist2)+ cw1_*fw_rhox*2*(nt/dist2)*(ntx/dist2 -nt/dist2/dist2*distx);
    wall_rhoxx += cb1_*ft2x*shr0_rhox*nt + cb1_*ft2*shr0_rhoxx*nt + cb1_*ft2*shr0_rhox*ntx;
    wall_rhoxx += cb1_*ft2x*shrmod_rhox*nt - cb1_*(1 - ft2)*(shrmod_rhoxx*nt + shrmod_rhox*ntx);

    T wall_rhoyy = cw1_*fw_rhoyy*power<2,Tq>(nt/dist2)+ cw1_*fw_rhoy*2*(nt/dist2)*(nty/dist2 - nt/dist2/dist2*disty);
    wall_rhoyy += cb1_*ft2y*shr0_rhoy*nt + cb1_*ft2*shr0_rhoyy*nt + cb1_*ft2*shr0_rhoy*nty;
    wall_rhoyy += cb1_*ft2y*shrmod_rhoy*nt - cb1_*(1 - ft2)*(shrmod_rhoyy*nt + shrmod_rhoy*nty);

    T wall_rhouyy = cw1_*fw_rhouyy*power<2,Tq>(nt/dist2)+ cw1_*fw_rhouy*2*(nt/dist2)*(nty/dist2 - nt/dist2/dist2*disty);
    wall_rhouyy += cb1_*ft2y*shr0_rhouy*nt + cb1_*ft2*shr0_rhouyy*nt + cb1_*ft2*shr0_rhouy*nty;
    wall_rhouyy += cb1_*ft2y*shrmod_rhouy*nt - cb1_*(1 - ft2)*(shrmod_rhouyy*nt  + shrmod_rhouy*nty);

    T wall_rhovxx = cw1_*fw_rhovxx*power<2,Tq>(nt/dist2)+ cw1_*fw_rhovx*2*(nt/dist2)*(ntx/dist2 -nt/dist2/dist2*distx);
    wall_rhovxx += cb1_*ft2x*shr0_rhovx*nt + cb1_*ft2*shr0_rhovxx*nt + cb1_*ft2*shr0_rhovx*ntx;
    wall_rhovxx += cb1_*ft2x*shrmod_rhovx*nt - cb1_*(1 - ft2)*(shrmod_rhovxx*nt + shrmod_rhovx*ntx);

//    dsdux(iSA, 0) += rho*wall_rhox;
//    dsdux(iSA, 2) += rho*wall_rhovx;
//
//    dsduy(iSA, 0) += rho*wall_rhoy;
//    dsduy(iSA, 1) += rho*wall_rhouy;

    div_dsdgradu(iSA, 0) += (rhox*wall_rhox + rho*wall_rhoxx)/ntref_;
    div_dsdgradu(iSA, 2) += (rhox*wall_rhovx + rho*wall_rhovxx)/ntref_;

    div_dsdgradu(iSA, 0) += (rhoy*wall_rhoy + rho*wall_rhoyy)/ntref_;
    div_dsdgradu(iSA, 1) += (rhoy*wall_rhouy + rho*wall_rhouyy)/ntref_;

  }

  // non-conservative diffusion terms

  //nonc = cb2_*rho*(ntx*ntx + nty*nty + ntz*ntz)/sigma_;
//  T nonc_rhox   = -cb2_*2*nt*ntx/sigma_;
//  T nonc_rhoy   = -cb2_*2*nt*nty/sigma_;
//  T nonc_rhontx = cb2_*2*ntx/sigma_;
//  T nonc_rhonty = cb2_*2*nty/sigma_;

  T nonc_rhoxx = -cb2_*2.*(ntx*ntx + nt*ntxx)/sigma_;
  T nonc_rhoyy = -cb2_*2.*(nty*nty + nt*ntyy)/sigma_;
  T nonc_rhontxx = cb2_*2.*ntxx/sigma_;
  T nonc_rhontyy = cb2_*2.*ntyy/sigma_;

  if (nt < 0)
  {
    Tq fn = (cn1_ + chi3) / (cn1_ - chi3);
    T fnx = (chi3x) /(cn1_ - chi3) + (cn1_+chi3)/(cn1_ - chi3)/(cn1_ - chi3)*(chi3x);
    T fny = (chi3y) /(cn1_ - chi3) + (cn1_+chi3)/(cn1_ - chi3)/(cn1_ - chi3)*(chi3y);
    //nonc += - (mu/rho + nt*fn)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

//    nonc_rhox += - (mu/rho + nt*fn)*(rhontx - 2*nt*rhox)/rho/sigma_;
//    nonc_rhoy += - (mu/rho + nt*fn)*(rhonty - 2*nt*rhoy)/rho/sigma_;
//    nonc_rhontx += - (mu/rho + nt*fn)*(rhox/rho)/sigma_;
//    nonc_rhonty += - (mu/rho + nt*fn)*(rhoy/rho)/sigma_;

    nonc_rhoxx += - (mux/rho -mu/(rho*rho)*rhox + ntx*fn  + nt*fnx)*(rho*ntx - nt*rhox)/rho/sigma_
                  - (mu/rho + nt*fn)*(rho*ntxx - nt*rhoxx)/rho/sigma_
                  + (mu/rho + nt*fn)*(rho*ntx - nt*rhox)/(rho*rho)*rhox/sigma_;

    nonc_rhoyy += - (muy/rho -mu/(rho*rho)*rhoy + nty*fn + nt*fny)*(rho*nty - nt*rhoy)/rho/sigma_
                  - (mu/rho + nt*fn)*(rho*ntyy - nt*rhoyy)/rho/sigma_
                  + (mu/rho + nt*fn)*(rho*nty - nt*rhoy)/(rho*rho)*rhoy/sigma_;

    nonc_rhontxx += - (mux/rho  -mu/(rho*rho)*rhox + ntx*fn + nt*fnx)*(rhox/rho)/sigma_
                    - (mu/rho + nt*fn)*(rhoxx/rho)/sigma_
                    + (mu/rho + nt*fn)*(rhox*rhox/(rho*rho))/sigma_;

    nonc_rhontyy += - (muy/rho -mu/(rho*rho)*rhoy + nty*fn + nt*fny)*(rhoy/rho)/sigma_
                    - (mu/rho + nt*fn)*(rhoyy/rho)/sigma_
                    + (mu/rho + nt*fn)*(rhoy*rhoy/(rho*rho))/sigma_;

  }
  else
  {
    //nonc += - (mu/rho + nt)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

//    nonc_rhox += - (mu/rho + nt)*(rhontx - 2*nt*rhox)/rho/sigma_;
//    nonc_rhoy += - (mu/rho + nt)*(rhonty - 2*nt*rhoy)/rho/sigma_;
//    nonc_rhontx += - (mu/rho + nt)*(rhox/rho)/sigma_;
//    nonc_rhonty += - (mu/rho + nt)*(rhoy/rho)/sigma_;


    nonc_rhoxx += - (mux/rho - mu/(rho*rho)*rhox + ntx)*(rho*ntx - nt*rhox)/rho/sigma_
                  - (mu/rho + nt)*(rho*ntxx - nt*rhoxx)/rho/sigma_
                  + (mu/rho + nt)*(rho*ntx - nt*rhox)/(rho*rho)*rhox/sigma_;

    nonc_rhoyy += - (muy/rho - mu/(rho*rho)*rhoy + nty)*(rho*nty - nt*rhoy)/rho/sigma_
                  - (mu/rho + nt)*(rho*ntyy - nt*rhoyy)/rho/sigma_
                  + (mu/rho + nt)*(rho*nty - nt*rhoy)/(rho*rho)*rhoy/sigma_;

    nonc_rhontxx += - (mux/rho - mu/(rho*rho)*rhox + ntx)*(rhox/rho)/sigma_
                    - (mu/rho + nt)*(rhoxx/rho - rhox*rhox/(rho*rho))/sigma_ ;
    nonc_rhontyy += - (muy/rho - mu/(rho*rho)*rhoy + nty)*(rhoy/rho)/sigma_
                    - (mu/rho + nt)*(rhoyy/rho - rhoy*rhoy/(rho*rho))/sigma_;

  }

//  dsdux(iSA, 0) += -nonc_rhox;
//  dsdux(iSA, 4) += -nonc_rhontx;
//
//  dsduy(iSA, 0) += -nonc_rhoy;
//  dsduy(iSA, 4) += -nonc_rhonty;

  div_dsdgradu(iSA, 0) -= nonc_rhoxx/ntref_;
  div_dsdgradu(iSA, 4) -= nonc_rhontxx;

  div_dsdgradu(iSA, 0) -= nonc_rhoyy/ntref_;
  div_dsdgradu(iSA, 4) -= nonc_rhontyy;

  // NS source terms (nominally empty function)
  BaseType::jacobianGradientSourceGradient( x, y, time, q, qx, qy, qxx, qxy, qyy, div_dsdgradu );
}


// jacobian of source wrt gradients of conservation variables: d(S)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceGradientHACK(
    const Real& dist,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Ts>& div_dsdgradu ) const
{
  MatrixQ<Ts> tmp = 0;
//  jacobianGradientSourceGradient(dist, 0, 0, x, y, time, q, qx, qy, qxx, qxy, qyy, tmp);
  jacobianGradientSourceGradient(dist, x, y, time, q, qx, qy, qxx, qxy, qyy, tmp);

  jacobianHelperHACK(x, y, time, q, tmp, div_dsdgradu);

}



// dual-consistent source
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::sourceTrace(
    const Real& distL, const Real& xL, const Real& yL,
    const Real& distR, const Real& xR, const Real& yR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
//  SANS_DEVELOPER_EXCEPTION( "RANS sourceTrace Not Yet Implemented" );
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& dist,
    const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  SANS_ASSERT( soln_ != nullptr );

  ArrayQ<Real> q = 0;
  ArrayQ<Real> qx = 0, qy = 0, qt = 0;
  ArrayQ<Real> qxx = 0,
               qyx = 0, qyy = 0;
#if 1
  soln_->secondGradient( x, y, time,
                         q,
                         qx, qy, qt,
                         qxx,
                         qyx, qyy);

  //strongFluxAdvectiveTime(q, qt, forcing);
  strongFluxAdvective(dist, x, y, time, q, qx, qy, forcing);
  strongFluxViscous(dist, x, y, time, q, qx, qy, qxx, qyx, qyy, forcing);
  source(dist, x, y, time, q, qx, qy, forcing);
#endif

#if 0
  ArrayQ<Real> forcingMMS=0;

  int MS_no = 4;
  int qnt_type = 10; // Quantity type, q, gradq, or forcing (10)
  int qnt_id;   // Equation ID (cont, momentum, eneergy etc. for qnt_type >=10)
  for (int i = 0; i < N; i++)
    forcingMMS[i] += MS_qnt_2D(MS_no, qnt_type, qnt_id=i, x, y);
#endif
#if 0
  for (int i = 4; i < N; i++)
    forcingMMS[i] += MS_qnt_2D(MS_no, qnt_type=13, qnt_id=i, x, y);
  for (int i = 4; i < N; i++)
    forcingMMS[i] += MS_qnt_2D(MS_no, qnt_type=14, qnt_id=i, x, y);
  for (int i = 4; i < N; i++)
    forcingMMS[i] += MS_qnt_2D(MS_no, qnt_type=15, qnt_id=i, x, y);
  for (int i = 4; i < N; i++)
    forcingMMS[i] += MS_qnt_2D(MS_no, qnt_type=16, qnt_id=i, x, y);
#endif

//  Real tol = 1e-10;
//  for (int i = 0; i < N; i++)
//    if (fabs(forcing[i] - forcingMMS[i]) > tol)
//      std::cout << x << ", "<< y << " : ["<<i<<"] " << forcing[i] << ", " << forcingMMS[i] << std::endl;

  //forcing = forcingMMS;
}


// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    const Real& dist,
    Real x, Real y, Real time, Real dx, Real dy, const ArrayQ<T>& q, T& speed ) const
{
  BaseType::speedCharacteristic(x,y,time,dx,dy,q,speed);
}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    const Real& dist,
    Real x, Real y, Real time, const ArrayQ<T>& q, T& speed ) const
{
  BaseType::speedCharacteristic(x,y,time,q,speed);
}



// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real x, Real y, Real time, const ArrayQ<T>& q, T& speed ) const
{
  BaseType::speedCharacteristic(x,y,time,q,speed);
}


// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  BaseType::interpResidVariable(rsdPDEIn, rsdPDEOut);

  if (cat_==Euler_ResidInterp_Raw)
  {
    rsdPDEOut[iSA] = rsdPDEIn[iSA];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    rsdPDEOut[iSA-1] = rsdPDEIn[iSA];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {

    Real tmp = rsdPDEOut[0]*rsdPDEOut[0];
    rsdPDEOut[0] = sqrt(tmp + rsdPDEIn[iSA]*rsdPDEIn[iSA] );
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{

  BaseType::interpResidGradient(rsdAuxIn, rsdAuxOut);

  //TEMPORARY SOLUTION FOR AUX VARIABLE
  if (cat_==Euler_ResidInterp_Raw)
  {
    rsdAuxOut[iSA] = rsdAuxIn[iSA];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    rsdAuxOut[iSA-1] = rsdAuxIn[iSA];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
//    rsdAuxOut[0] = sqrt(rsdAuxOut[0]*rsdAuxOut[0] + ntref_*ntref_*rsdAuxIn[iSA]*rsdAuxOut[iSA] );
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  BaseType::interpResidBC(rsdBCIn, rsdBCOut);

  if (cat_==Euler_ResidInterp_Raw)
  {
    rsdBCOut[iSA] = rsdBCIn[iSA];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    rsdBCOut[iSA-1] = rsdBCIn[iSA];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    Real tmp = rsdBCOut[0]*rsdBCOut[0];
    rsdBCOut[0] = sqrt(tmp + rsdBCIn[iSA]*rsdBCIn[iSA] );
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}



// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpret_.setFromPrimitive( q, data, name, nn );
}

// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class T, class Varibles>
inline void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::setDOFFrom( ArrayQ<T>& q, const SAVariableType2D<Varibles>& data ) const
{
  qInterpret_.setFromPrimitive( q, data.cast() );
}

// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Varibles>
inline typename PDERANSSA2D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const SAVariableType2D<Varibles>& data ) const
{
  ArrayQ<Real> q;
  qInterpret_.setFromPrimitive( q, data.cast() );
  return q;
}

// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDERANSSA2D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q;
  DictKeyPair data = d.get(SAVariableType2DParams::params.StateVector);

  if ( data == SAVariableType2DParams::params.StateVector.Conservative )
    qInterpret_.setFromPrimitive( q, SAnt2D<Conservative2D<Real>>(data) );
  else if ( data == SAVariableType2DParams::params.StateVector.PrimitivePressure )
    qInterpret_.setFromPrimitive( q, SAnt2D<DensityVelocityPressure2D<Real>>(data) );
  else if ( data == SAVariableType2DParams::params.StateVector.PrimitiveTemperature )
    qInterpret_.setFromPrimitive( q, SAnt2D<DensityVelocityTemperature2D<Real>>(data) );
  else
    SANS_DEVELOPER_EXCEPTION(" Missing state vector option ");

  return q;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDERANSSA2D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDERANSSA2D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDERANSSA2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  this->qInterpret_.dump(indentSize+2, out);
  out << indent << "PDERANSSA2D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  this->gas_.dump(indentSize+2, out);
  out << indent << "PDERANSSA2D<PDETraitsSize, PDETraitsModel>: visc_ = " << std::endl;
  this->visc_.dump(indentSize+2, out);
  out << indent << "PDERANSSA2D<PDETraitsSize, PDETraitsModel>: tcond_ = " << std::endl;
  this->tcond_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // PDERANSSA2D_H
