// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDENAVIERSTOKES2D_H
#define PDENAVIERSTOKES2D_H

// 2-D compressible Navier-Stokes PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "pde/ForcingFunctionBase.h"
#include "Topology/Dimension.h"

#include "GasModel.h"
#include "NSVariable2D.h"
#include "PDEEuler2D.h"

#include <iostream>
#include <string>
#include <memory> // shared_ptr

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q2D;


//----------------------------------------------------------------------------//
// PDE class: 2-D compressible Navier-Stokes
//
// template parameters:
//   T                        solution DOF data type (e.g. Real)
//   PDETraitsSize            define PDE size-related features
//     N, D                   PDE size, physical dimensions
//     ArrayQ                 solution/residual arrays
//     MatrixQ                matrices (e.g. flux jacobian)
//   PDETraitsModel           define PDE model-related features
//     QType                  solution variable set (e.g. primitive)
//     QInterpret             solution variable interpreter
//     GasModel               gas model (e.g. ideal gas law)
//     ViscosityModel         molecular viscosity model (e.g. Sutherland)
//     ThermalConductivityModel   thermal conductivity model (e.g. const Prandtl number)
//
// member functions:
//   .hasFluxAdvectiveTime     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//INHERITS CONSERVATIVE, ADVECTIVE FLUXES FROM EULER2D
//   .masterState        unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDENavierStokes2D : public PDEEuler2D<PDETraitsSize, PDETraitsModel>
{
public:
  typedef PhysD2 PhysDim;

  typedef PDEEuler2D<PDETraitsSize, PDETraitsModel> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef typename PDETraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;                                       // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                                 // gas model

  typedef typename PDETraitsModel::ViscosityModel ViscosityModel;                     // molecular viscosity model

  typedef typename PDETraitsModel::ThermalConductivityModel ThermalConductivityModel; // thermal conductivity model

  typedef ForcingFunctionBase2D< PDENavierStokes2D<PDETraitsSize, PDETraitsModel> > ForcingFunctionType;

  explicit PDENavierStokes2D( const GasModel& gas, const ViscosityModel& visc,
         const ThermalConductivityModel& tcond,
         const EulerResidualInterpCategory cat = Euler_ResidInterp_Raw,
         const RoeEntropyFix entropyFix = eVanLeer,
         const std::shared_ptr<ForcingFunctionType>& forcing = NULL) :
      PDEEuler2D<PDETraitsSize, PDETraitsModel>( gas, cat, entropyFix ),
      visc_(visc),
      tcond_(tcond),
      forcing_(forcing) {}

  using BaseType::iCont;
  using BaseType::ixMom;
  using BaseType::iyMom;
  using BaseType::iEngy;

  PDENavierStokes2D& operator=( const PDENavierStokes2D& ) = delete;

  // flux components
  bool hasFluxViscous() const { return true; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool hasForcingFunction() const { return forcing_ != NULL && (*forcing_).hasForcingFunction(); }

  bool needsSolutionGradientforSource() const { return false; }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

protected:
  // internal function for computing the actual viscous flux
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tq& u, const Tq& v, const Tq& mu, const Tq& k,
      const Tg& ux, const Tg& vx, const Tg& tx,
      const Tg& uy, const Tg& vy, const Tg& ty,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

public:
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

protected:
  template <class Tq, class Tk>
  void diffusionViscous(
      const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tq& mu, const Tq& kn,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

public:

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const;


  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
  {
    diffusionViscousGradient(x, y, time, q, qx, qy, qxx, qxy, qyy,
                             kxx_x, kxy_x, kyx_x, kxy_y, kyx_y, kyy_y);
  }

protected:
  template <class Tq,  class Tg, class Tk, class Tm>
  void diffusionViscousGradient(
      const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tm& mu, const Tm& k,
      const Tg& rhox, const Tg& ux, const Tg& vx, const Tg& tx, const Tm& mux, const Tm& kx,
      const Tg& rhoy, const Tg& uy, const Tg& vy, const Tg& ty, const Tm& muy, const Tm& ky,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const;

public:

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

protected:
  template <class Tm, class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Tq& mu, const Tq& k,
      const ArrayQ<Tm>& dmu, const ArrayQ<Tm> dk,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

public:

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

protected:
  // strong form viscous fluxes
  template <class Tm, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Tm& mu,
      const typename promote_Surreal<Tq, Tg>::type& mux,
      const typename promote_Surreal<Tq, Tg>::type& muy,
      const Tm& k,
      const typename promote_Surreal<Tq, Tg>::type& kx,
      const typename promote_Surreal<Tq, Tg>::type& ky,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

public:

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

#if 0   // should inherit empties from Euler
  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& dist, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // dual-consistent source
  template <class T>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL,
      const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR,
      ArrayQ<T>& sourceL, ArrayQ<T>& sourceR ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<sT>& dsdu ) const {}
#endif

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const;

  const ViscosityModel& viscosityModel() const {return visc_;}
  const ThermalConductivityModel& thermalConductivityModel() const {return tcond_;}

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BaseType::qInterpret_;
  using BaseType::gas_;
  const ViscosityModel& visc_;
  const ThermalConductivityModel& tcond_;
  const std::shared_ptr<ForcingFunctionType> forcing_;
};


// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq rho=0, u=0, v=0, t=0;
  T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;
  Tq mu, k;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  // Compute the viscous flux
  fluxViscous(u, v, mu, k,
              ux, vx, tx,
              uy, vy, ty,
              f, g );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tq& u, const Tq& v, const Tq& mu, const Tq& k,
    const Tg& ux, const Tg& vx, const Tg& tx,
    const Tg& uy, const Tg& vy, const Tg& ty,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  Tq lambda = -2./3. * mu;

  Tg tauxx = mu*(2*ux   ) + lambda*(ux + vy);
  Tg tauxy = mu*(uy + vx);
  Tg tauyy = mu*(2*vy   ) + lambda*(ux + vy);

  f(ixMom) -= tauxx;
  f(iyMom) -= tauxy;
  f(iEngy) -= k*tx + u*tauxx + v*tauxy;

  g(ixMom) -= tauxy;
  g(iyMom) -= tauyy;
  g(iEngy) -= k*ty + u*tauxy + v*tauyy;
}


// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscous(x, y, time, qL, qxL, qyL, fL, gL);
  fluxViscous(x, y, time, qR, qxR, qyR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;

#else
  T rhoL, uL, vL, tL;
  T rhoxL, rhoyL, uxL, uyL, vxL, vyL, txL, tyL;
  T rhoR, uR, vR, tR;
  T rhoxR, rhoyR, uxR, uyR, vxR, vyR, txR, tyR;
  T u, v, t;
  T ux, uy, vx, vy, tx, ty;
  T mu, k, lambda;
  T tauxx, tauxy, tauyy;

  qInterpret_.eval( qL, rhoL, uL, vL, tL );
  qInterpret_.eval( qR, rhoR, uR, vR, tR );
  qInterpret_.evalGradient( qL, qxL, rhoxL, uxL, vxL, txL );
  qInterpret_.evalGradient( qL, qyL, rhoyL, uyL, vyL, tyL );
  qInterpret_.evalGradient( qR, qxR, rhoxR, uxR, vxR, txR );
  qInterpret_.evalGradient( qR, qyR, rhoyR, uyR, vyR, tyR );

  u = 0.5*(uL + uR);
  v = 0.5*(vL + vR);
  t = 0.5*(tL + tR);

  ux = 0.5*(uxL + uxR);
  uy = 0.5*(uyL + uyR);
  vx = 0.5*(vxL + vxR);
  vy = 0.5*(vyL + vyR);
  tx = 0.5*(txL + txR);
  ty = 0.5*(tyL + tyR);

  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );
  lambda = -2./3. * mu;

  tauxx = mu*(2*ux   ) + lambda*(ux + vy);
  tauxy = mu*(uy + vx);
  tauyy = mu*(2*vy   ) + lambda*(ux + vy);

  f(ixMom) -= nx*(         tauxx          ) + ny*(         tauxy          );
  f(iyMom) -= nx*(                   tauxy) + ny*(                   tauyy);
  f(iEngy) -= nx*(k*tx + u*tauxx + v*tauxy) + ny*(k*ty + u*tauxy + v*tauyy);
#endif
}

// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tgu, class Tf>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::perturbedGradFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
    ArrayQ<Tf>& dF, ArrayQ<Tf>& dG ) const
{
  // this thing is linear in the gradient so the following works:
  fluxViscous(x, y, time, q, dqx, dqy, dF, dG);
}


// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tk, class Tgu, class Tf>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::perturbedGradFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const MatrixQ<Tk>& kxx,  const MatrixQ<Tk>& kxy,  const MatrixQ<Tk>& kyx,  const MatrixQ<Tk>& kyy,
    const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
    ArrayQ<Tf>& dF, ArrayQ<Tf>& dG ) const
{
  //using what we know about the sparsity of K

  // d(Fv)/d(Ux)
  dF[ixMom] -= kxx(ixMom,0)*dux[0];
  dF[ixMom] -= kxx(ixMom,1)*dux[1];

  dF[iyMom] -= kxx(iyMom,0)*dux[0];
  dF[iyMom] -= kxx(iyMom,2)*dux[2];

  dF[iEngy] -= kxx(iEngy,0)*dux[0];
  dF[iEngy] -= kxx(iEngy,1)*dux[1];
  dF[iEngy] -= kxx(iEngy,2)*dux[2];
  dF[iEngy] -= kxx(iEngy,3)*dux[3];

  // d(Fv)/d(Uy)
  dF[ixMom] -=kxy(ixMom,0)*duy[0];
  dF[ixMom] -=kxy(ixMom,2)*duy[2];

  dF[iyMom] -= kxy(iyMom,0)*duy[0];
  dF[iyMom] -= kxy(iyMom,1)*duy[1];

  dF[iEngy] -= kxy(iEngy,0)*duy[0];
  dF[iEngy] -= kxy(iEngy,1)*duy[1];
  dF[iEngy] -= kxy(iEngy,2)*duy[2];

  // d(Gv)/d(Ux)

  dG[ixMom] -=kyx(ixMom,0)*dux[0];
  dG[ixMom] -=kyx(ixMom,2)*dux[2];

  dG[iyMom] -= kyx(iyMom,0)*dux[0];
  dG[iyMom] -= kyx(iyMom,1)*dux[1];

  dG[iEngy] -= kyx(iEngy,0)*dux[0];
  dG[iEngy] -= kyx(iEngy,1)*dux[1];
  dG[iEngy] -= kyx(iEngy,2)*dux[2];

  // d(Gv)/d(Uy)
  dG[ixMom] -= kyy(ixMom,0)*duy[0];
  dG[ixMom] -= kyy(ixMom,1)*duy[1];

  dG[iyMom] -= kyy(iyMom,0)*duy[0];
  dG[iyMom] -= kyy(iyMom,2)*duy[2];

  dG[iEngy] -= kyy(iEngy,0)*duy[0];
  dG[iEngy] -= kyy(iEngy,1)*duy[1];
  dG[iEngy] -= kyy(iEngy,2)*duy[2];
  dG[iEngy] -= kyy(iEngy,3)*duy[3];

}




// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  Tq rho=0, u=0, v=0, t=0;
  Tq mu, k;

  qInterpret_.eval( q, rho, u, v, t );
  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  diffusionViscous( rho, u, v, t, mu, k,
                    kxx, kxy,
                    kyx, kyy );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tk>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tq& mu, const Tq& k,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  Real Cv = gas_.Cv();

  Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  // defining nu here as mu/rho (and hence lambda/rho as well as k/(Cv*rho)) to remove rho division throughout the K matrix calculation
  Tq nu = mu/rho;
  Tq lambda = -2./3. * nu;
  Tq kn = k/(Cv*rho);

  // d(Fv)/d(Ux)
  kxx(ixMom,0) += -u*(2*nu + lambda);
  kxx(ixMom,1) +=    (2*nu + lambda);

  kxx(iyMom,0) += -v*nu;
  kxx(iyMom,2) +=    nu;

  kxx(iEngy,0) += -u*u*((2*nu + lambda) - kn) - v*v*(nu - kn) - e0*kn;
  kxx(iEngy,1) +=    u*((2*nu + lambda) - kn);
  kxx(iEngy,2) +=                                 v*(nu - kn);
  kxx(iEngy,3) +=                                                  kn;

  // d(Fv)/d(Uy)
  kxy(ixMom,0) += -v*lambda;
  kxy(ixMom,2) +=    lambda;

  kxy(iyMom,0) += -u*nu;
  kxy(iyMom,1) +=    nu;

  kxy(iEngy,0) += -u*v*(nu + lambda);
  kxy(iEngy,1) +=  v*nu;
  kxy(iEngy,2) +=  u*lambda;

  // d(Gv)/d(Ux)
  kyx(ixMom,0) += -v*nu;
  kyx(ixMom,2) +=    nu;

  kyx(iyMom,0) += -u*lambda;
  kyx(iyMom,1) +=    lambda;

  kyx(iEngy,0) += -u*v*(nu + lambda);
  kyx(iEngy,1) +=  v*lambda;
  kyx(iEngy,2) +=  u*nu;

  // d(Gv)/d(Uy)
  kyy(ixMom,0) += -u*nu;
  kyy(ixMom,1) +=    nu;

  kyy(iyMom,0) += -v*(2*nu + lambda);
  kyy(iyMom,2) +=    (2*nu + lambda);

  kyy(iEngy,0) += -u*u*(nu - kn) - v*v*((2*nu + lambda) - kn) - e0*kn;
  kyy(iEngy,1) +=    u*(nu - kn);
  kyy(iEngy,2) +=                    v*((2*nu + lambda) - kn);
  kyy(iEngy,3) +=                                                  kn;
}


// gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tk>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,  MatrixQ<Tk>& kyx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq rho=0, u=0, v=0, t=0;
  T mu, k;
  T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  T tT = t; // promote to surreal

  mu = visc_.viscosity( tT );

  T mut = 0;

  visc_.viscosityJacobian(tT, mut);
  T mux = mut*tx;
  T muy = mut*ty;

  k  = tcond_.conductivity( mu );

  T k_mu = 0;
  tcond_.conductivityJacobian(mu, k_mu);
  T kx = k_mu*mux;
  T ky = k_mu*muy;

  diffusionViscousGradient( rho, u, v, t, mu, k,
                            rhox, ux, vx, tx, mux, kx,
                            rhoy, uy, vy, ty, muy, ky,
                            kxx_x, kxy_x, kyx_x,
                            kxy_y, kyx_y, kyy_y );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk, class Tm>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Tq& rho, const Tq& u, const Tq& v, const Tq& t, const Tm& mu, const Tm& k,
    const Tg& rhox, const Tg& ux, const Tg& vx, const Tg& tx, const Tm& mux, const Tm& kx,
    const Tg& rhoy, const Tg& uy, const Tg& vy, const Tg& ty, const Tm& muy, const Tm& ky,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;
  Real Cv = gas_.Cv();
  Tq e_rho = 0; Tq e_t = 0;

  Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  gas_.energyJacobian(rho, t, e_rho, e_t);
  T e0x = e_rho*rhox + e_t*tx + (u*ux + v*vx);
  T e0y = e_rho*rhoy + e_t*ty + (u*uy + v*vy);

  Tq rho2 = rho*rho;

  // defining nu here as mu/rho (and hence lambda/rho as well as k/(Cv*rho)) to remove rho division throughout the K matrix calculation
  T nu  = mu/rho;
  T nux = mux/rho - mu*rhox/rho2;
  T nuy = muy/rho - mu*rhoy/rho2;

  T lambda  = -2./3. * nu;
  T lambdax = -2./3. * nux;
  T lambday = -2./3. * nuy;

  T kn  = k/(Cv*rho);
  T knx = kx/(Cv*rho) - k*rhox/Cv/rho2;
  T kny = ky/(Cv*rho) - k*rhoy/Cv/rho2;

  kxx_x(ixMom,0) += -ux*(2*nu + lambda) - u*(2*nux + lambdax);
  kxx_x(ixMom,1) +=                         (2*nux + lambdax);

  kxx_x(iyMom,0) += -vx*nu - v*nux;
  kxx_x(iyMom,2) +=            nux;

  kxx_x(iEngy,0) += -2*u*ux*((2*nu + lambda) - kn) - u*u*(2*nux + lambdax - knx)
                   - 2*v*vx*(nu - kn) - v*v*(nux - knx) - e0*knx - e0x*kn;
  kxx_x(iEngy,1) +=    ux*((2*nu + lambda) - kn) + u*((2*nux + lambdax) - knx);
  kxx_x(iEngy,2) +=   vx*(nu - kn) +  v*(nux - knx);
  kxx_x(iEngy,3) +=                                                  knx;

  // d(Fv)/d(Uy)
//  kxy(ixMom,0) += -v*lambda;
//  kxy(ixMom,2) +=    lambda;
//
//  kxy(iyMom,0) += -u*nu;
//  kxy(iyMom,1) +=    nu;
//
//  kxy(iEngy,0) += -u*v*(nu + lambda);
//  kxy(iEngy,1) +=  v*nu;
//  kxy(iEngy,2) +=  u*lambda;

  // d/dx d(Fv)/d(Uy)
  kxy_x(ixMom,0) += -vx*lambda - v*lambdax;
  kxy_x(ixMom,2) +=                lambdax;

  kxy_x(iyMom,0) += -ux*nu - u*nux;
  kxy_x(iyMom,1) +=            nux;

  kxy_x(iEngy,0) += -ux*v*(nu + lambda) -u*vx*(nu + lambda) -u*v*(nux + lambdax);
  kxy_x(iEngy,1) +=  vx*nu + v*nux;
  kxy_x(iEngy,2) +=  ux*lambda + u*lambdax;


  // d/dx d(Gv)/d(Ux)
  kyx_x(ixMom,0) += -vx*nu - v*nux;
  kyx_x(ixMom,2) +=    nux;

  kyx_x(iyMom,0) += -ux*lambda - u*lambdax;
  kyx_x(iyMom,1) +=    lambdax;

  kyx_x(iEngy,0) += -ux*v*(nu + lambda) -u*vx*(nu + lambda) -u*v*(nux + lambdax);
  kyx_x(iEngy,1) +=  vx*lambda + v*lambdax;
  kyx_x(iEngy,2) +=  ux*nu + u*nux;


//
//  // d(Fv)/d(Uy)
//  kxy(ixMom,0) += -v*lambda;
//  kxy(ixMom,2) +=    lambda;
//
//  kxy(iyMom,0) += -u*nu;
//  kxy(iyMom,1) +=    nu;
//
//  kxy(iEngy,0) += -u*v*(nu + lambda);
//  kxy(iEngy,1) +=  v*nu;
//  kxy(iEngy,2) +=  u*lambda;
//

  //d/dy  d(Fv)/d(Uy)
  kxy_y(ixMom,0) += -vy*lambda-v*lambday;
  kxy_y(ixMom,2) +=              lambday;

  kxy_y(iyMom,0) += -uy*nu - u*nuy;
  kxy_y(iyMom,1) +=            nuy;

  kxy_y(iEngy,0) += -uy*v*(nu + lambda)-u*vy*(nu + lambda)-u*v*(nuy + lambday);
  kxy_y(iEngy,1) +=  vy*nu + v*nuy;
  kxy_y(iEngy,2) +=  uy*lambda + u*lambday;

  // d/dy (d(Gv)/d(Ux))
  kyx_y(ixMom,0) += -vy*nu - v*nuy;
  kyx_y(ixMom,2) +=    nuy;

  kyx_y(iyMom,0) += -uy*lambda -u*lambday;
  kyx_y(iyMom,1) +=    lambday;

  kyx_y(iEngy,0) += -uy*v*(nu + lambda)  -u*vy*(nu + lambda)  -u*v*(nuy + lambday);
  kyx_y(iEngy,1) +=  vy*lambda + v*lambday;
  kyx_y(iEngy,2) +=  uy*nu + u*nuy;

  // d/dy (d(Gv)/d(Uy)
  kyy_y(ixMom,0) += -uy*nu - u*nuy;
  kyy_y(ixMom,1) +=    nuy;

  kyy_y(iyMom,0) += -vy*(2*nu + lambda) -v*(2*nuy + lambday);
  kyy_y(iyMom,2) +=    (2*nuy + lambday);

  kyy_y(iEngy,0) += -2*u*uy*(nu - kn) - u*u*(nuy - kny)
                     -2*v*vy*((2*nu + lambda) - kn) -v*v*(2*nuy + lambday - kny) - e0y*kn - e0*kny;
  kyy_y(iEngy,1) +=    uy*(nu - kn) +  u*(nuy - kny);
  kyy_y(iEngy,2) +=    vy*((2*nu + lambda) - kn) +   v*((2*nuy + lambday) - kny);
  kyy_y(iEngy,3) +=                                                  kny;
}


// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::jacobianFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  Tq rho=0, u=0, v=0, t=0;
  Tq mu, k;
  Tq mu_t = 0;
  Tq k_mu = 0;

  qInterpret_.eval( q, rho, u, v, t );

  mu = visc_.viscosity( t );
  visc_.viscosityJacobian(t, mu_t);
  k  = tcond_.conductivity( mu );
  tcond_.conductivityJacobian( mu, k_mu);
  Tq E = gas_.energy(rho, t) + 0.5*(u*u + v*v);
  Real Cv = gas_.Cv();

  Tq t_rho  = ( -E/rho + ( u*u + v*v )/rho ) / Cv;
  Tq t_rhou = (        - (   u       )/rho ) / Cv;
  Tq t_rhov = (        - (         v )/rho ) / Cv;
  Tq t_rhoE = (  1/rho                     ) / Cv;

  ArrayQ<Tq> dmu = 0;
  dmu(0) = mu_t*t_rho;
  dmu(1) = mu_t*t_rhou;
  dmu(2) = mu_t*t_rhov;
  dmu(3) = mu_t*t_rhoE;

  ArrayQ<Tq> dk = 0;
  dk(0) = k_mu*dmu(0);
  dk(1) = k_mu*dmu(1);
  dk(2) = k_mu*dmu(2);
  dk(3) = k_mu*dmu(3);

  jacobianFluxViscous(mu, k, dmu, dk, q, qx, qy, dfdu, dgdu);

}

// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tm, class Tq, class Tg, class Tf>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::jacobianFluxViscous(
    const Tq& mu, const Tq& k,
    const ArrayQ<Tm>& dmu, const ArrayQ<Tm> dk,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  Real R = gas_.R();
  Real g = gas_.gamma();
  Real Cv = gas_.Cv();
  Tq rho=0, u=0, v=0, t=0;

  typedef typename promote_Surreal<Tq,Tg>::type T;
  T  rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;

  Tf tauxx_rho  = 0; Tf tauxy_rho = 0; Tf tauyy_rho = 0;
  Tf tauxx_rhou = 0; Tf tauxy_rhou = 0; Tf tauyy_rhou = 0;
  Tf tauxx_rhov = 0; Tf tauxy_rhov = 0; Tf tauyy_rhov = 0;
  Tf tauxx_rhoE = 0; Tf tauxy_rhoE = 0; Tf tauyy_rhoE = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  Tq E = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  T tauxx_mu = (2*ux   ) - 2./3.*(ux + vy);
  T tauxy_mu = (uy + vx);
  T tauyy_mu = (2*vy   ) - 2./3.*(ux + vy);

  Tq rho2 = rho*rho;
  Tq rho3 = rho2*rho;

  T ux_rho  = (rhox * u - rho*ux)/rho2;
  T ux_rhou = -rhox              /rho2;
  T uy_rho  = (rhoy * u - rho*uy)/rho2;
  T uy_rhou = -rhoy              /rho2;

  T vx_rho  = (rhox * v - rho*vx)/rho2;
  T vx_rhov = -rhox              /rho2;
  T vy_rho  = (rhoy * v - rho*vy)/rho2;
  T vy_rhov = -rhoy              /rho2;

  T rhoEx = (rhox*R*t + rho*R*tx)/(g-1) + 0.5*rhox*(u*u + v*v) + rho*(u*ux + v*vx);
  T rhoEy = (rhoy*R*t + rho*R*ty)/(g-1) + 0.5*rhoy*(u*u + v*v) + rho*(u*uy + v*vy);

  T tx_rho  = (2.*u*ux*rho*rho + 2.*v*vx*rho*rho - rho*rhoEx - rho*u*u*rhox - rho*v*v*rhox + 2*rho*E*rhox)/(Cv*rho3);
  T tx_rhou = (u*rhox - rho*ux)/(Cv*rho2);
  T tx_rhov = (v*rhox - rho*vx)/(Cv*rho2);
  T tx_rhoE = -rhox            /(Cv*rho2);

  T ty_rho  = (2.*u*uy*rho*rho + 2.*v*vy*rho*rho - rho*rhoEy - rho*u*u*rhoy - rho*v*v*rhoy + 2*rho*E*rhoy)/(Cv*rho3);
  T ty_rhou = (u*rhoy - rho*uy)/(Cv*rho2);
  T ty_rhov = (v*rhoy - rho*vy)/(Cv*rho2);
  T ty_rhoE = -rhoy            /(Cv*rho2);

  Tq u_rho  = -u/rho;
  Tq u_rhou =  1/rho;
  Tq v_rho  = -v/rho;
  Tq v_rhov =  1/rho;

  T tauxx = mu*(2*ux   ) - 2./3.*mu*(ux + vy);
  T tauxy = mu*(uy + vx);
  T tauyy = mu*(2*vy   ) - 2./3.*mu*(ux + vy);

  //  f(ixMom) -= tauxx;
  //  f(iyMom) -= tauxy;
  //  f(iEngy) -= k*tx + u*tauxx + v*tauxy;
  //
  //  g(ixMom) -= tauxy;
  //  g(iyMom) -= tauyy;
  //  g(iEngy) -= k*ty + u*tauxy + v*tauyy;

  tauxx_rho  = tauxx_mu*dmu(0) + mu*(4./3.*ux_rho  - 2./3.*vy_rho  );
  tauxx_rhou = tauxx_mu*dmu(1) + mu*(4./3.*ux_rhou                 );
  tauxx_rhov = tauxx_mu*dmu(2) + mu*(              - 2./3.*vy_rhov );
  tauxx_rhoE = tauxx_mu*dmu(3);

  tauxy_rho  = tauxy_mu*dmu(0) + mu*(uy_rho + vx_rho);
  tauxy_rhou = tauxy_mu*dmu(1) + mu*(uy_rhou          );
  tauxy_rhov = tauxy_mu*dmu(2) + mu*(vx_rhov          );
  tauxy_rhoE = tauxy_mu*dmu(3);

  tauyy_rho  = tauyy_mu*dmu(0) + mu*(4./3.*vy_rho  - 2./3.*ux_rho  );
  tauyy_rhou = tauyy_mu*dmu(1) + mu*(              - 2./3.*ux_rhou );
  tauyy_rhov = tauyy_mu*dmu(2) + mu*(4./3.*vy_rhov                 );
  tauyy_rhoE = tauyy_mu*dmu(3);

  dfdu(ixMom,0) -= tauxx_rho;
  dfdu(ixMom,1) -= tauxx_rhou;
  dfdu(ixMom,2) -= tauxx_rhov;
  dfdu(ixMom,3) -= tauxx_rhoE;

  dfdu(iyMom,0) -= tauxy_rho;
  dfdu(iyMom,1) -= tauxy_rhou;
  dfdu(iyMom,2) -= tauxy_rhov;
  dfdu(iyMom,3) -= tauxy_rhoE;

  dfdu(iEngy,0) -= k*tx_rho  + tx*dk(0) + u_rho*tauxx  + u*tauxx_rho  + v_rho*tauxy  + v*tauxy_rho;
  dfdu(iEngy,1) -= k*tx_rhou + tx*dk(1) + u_rhou*tauxx + u*tauxx_rhou                + v*tauxy_rhou;
  dfdu(iEngy,2) -= k*tx_rhov + tx*dk(2) +                u*tauxx_rhov + v_rhov*tauxy + v*tauxy_rhov;
  dfdu(iEngy,3) -= k*tx_rhoE + tx*dk(3) +                u*tauxx_rhoE                + v*tauxy_rhoE;

  dgdu(ixMom,0) -= tauxy_rho;
  dgdu(ixMom,1) -= tauxy_rhou;
  dgdu(ixMom,2) -= tauxy_rhov;
  dgdu(ixMom,3) -= tauxy_rhoE;

  dgdu(iyMom,0) -= tauyy_rho;
  dgdu(iyMom,1) -= tauyy_rhou;
  dgdu(iyMom,2) -= tauyy_rhov;
  dgdu(iyMom,3) -= tauyy_rhoE;

  dgdu(iEngy,0) -= k*ty_rho  + ty*dk(0) + u_rho*tauxy  + u*tauxy_rho  + v_rho*tauyy  + v*tauyy_rho;
  dgdu(iEngy,1) -= k*ty_rhou + ty*dk(1) + u_rhou*tauxy + u*tauxy_rhou                + v*tauyy_rhou;
  dgdu(iEngy,2) -= k*ty_rhov + ty*dk(2) +                u*tauxy_rhov + v_rhov*tauyy + v*tauyy_rhov;
  dgdu(iEngy,3) -= k*ty_rhoE + ty*dk(3) +                u*tauxy_rhoE                + v*tauyy_rhoE;
}


// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  //typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho, u, v, t = 0;
  Tqg rhox, rhoy, ux, uy, vx, vy, tx, ty = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  Tq mu = visc_.viscosity( t );
  Tq mu_t = 0;
  visc_.viscosityJacobian( t, mu_t );
  Tqg mux = mu_t*tx;
  Tqg muy = mu_t*ty;

  Tq k = tcond_.conductivity( mu );
  Tq k_mu = 0;
  tcond_.conductivityJacobian( mu, k_mu );
  Tqg kx = k_mu*mux;
  Tqg ky = k_mu*muy;

  strongFluxViscous(mu, mux, muy, k, kx, ky, q, qx, qy, qxx, qxy, qyy, strongPDE);
}


// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tm, class Tq, class Tg, class Th, class Tf>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Tm& mu,
    const typename promote_Surreal<Tq, Tg>::type& mux,
    const typename promote_Surreal<Tq, Tg>::type& muy,
    const Tm& k,
    const typename promote_Surreal<Tq, Tg>::type& kx,
    const typename promote_Surreal<Tq, Tg>::type& ky,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho, u, v, t = 0;
  Tqg rhox, rhoy, ux, uy, vx, vy, tx, ty = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  // evaluate second derivatives
  T rhoxx = 0, rhoxy = 0, rhoyy = 0;
  T uxx = 0, uxy = 0, uyy = 0;
  T vxx = 0, vxy = 0, vyy = 0;
  T txx = 0, txy = 0, tyy = 0;

  qInterpret_.evalSecondGradient( q, qx, qy,
                                  qxx, qxy, qyy,
                                  rhoxx, rhoxy, rhoyy,
                                  uxx, uxy, uyy,
                                  vxx, vxy, vyy,
                                  txx, txy, tyy );

  Tm  lambda  = -2./3. * mu;
  Tqg lambdax = -2./3. * mux;
  Tqg lambday = -2./3. * muy;

  T tauxx = mu*(2*ux   ) + lambda*(ux + vy);
  T tauxy = mu*(uy + vx);
  T tauyy = mu*(2*vy   ) + lambda*(ux + vy);

  T tauxx_x = mux*(2*ux) + mu*(2*uxx) + lambdax*(ux + vy) + lambda*(uxx + vxy);
  T tauxy_y = muy*(uy + vx) + mu*(uyy + vxy);

  T tauxy_x = mux*(uy + vx) + mu*(uxy + vxx);
  T tauyy_y = muy*(2*vy) + mu*(2*vyy) + lambday*(ux + vy) + lambda*(uxy + vyy);

  //      (         tauxx          )        (         tauxy          );
  //  F = (                   tauxy) ;  G = (                   tauyy);
  //      (k*tx + u*tauxx + v*tauxy) +      (k*ty + u*tauxy + v*tauyy);

  strongPDE(ixMom) -= tauxx_x + tauxy_y;
  strongPDE(iyMom) -= tauxy_x + tauyy_y;
  strongPDE(iEngy) -= kx*tx + k*txx + ux*tauxx + u*tauxx_x + vx*tauxy + v*tauxy_x;
  strongPDE(iEngy) -= ky*ty + k*tyy + uy*tauxy + u*tauxy_y + vy*tauyy + v*tauyy_y;
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  (*forcing_)(*this, x, y, time, forcing );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDENavierStokes2D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  this->gas_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: visc_ = " << std::endl;
  this->visc_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes2D<PDETraitsSize, PDETraitsModel>: tcond_ = " << std::endl;
  this->tcond_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDENAVIERSTOKES2D_H
