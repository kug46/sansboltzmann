// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SAVariable2D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<SAVariableType2DParams::VariableOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<SAVariableType2DParams::VariableOptions> > const&) const;

template<class NSVariableParams>
void SAnt2DParams<NSVariableParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.nt));
  NSVariableParams::checkInputs(d);
  // checkUnkownInputs is called int the base class
  //d.checkUnknownInputs(allParams);
}
template<class NSVariableParams>
SAnt2DParams<NSVariableParams> SAnt2DParams<NSVariableParams>::params;

void SAnt2DParams<Conservative2DParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rhont));
  Conservative2DParams::checkInputs(d);
  // checkUnkownInputs is called int the base class
  //d.checkUnknownInputs(allParams);
}
SAnt2DParams<Conservative2DParams> SAnt2DParams<Conservative2DParams>::params;

template struct SAnt2DParams<DensityVelocityPressure2DParams>;
template struct SAnt2DParams<DensityVelocityTemperature2DParams>;

void SAVariableType2DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
SAVariableType2DParams SAVariableType2DParams::params;

}
