// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCEuler1D.h"
#include "Q1DPrimitiveRhoPressure.h"
#include "Q1DPrimitiveSurrogate.h"
#include "Q1DConservative.h"
#include "Q1DEntropy.h"

// TODO: Rework so this is not needed here
#include "TraitsNavierStokes.h"
#include "TraitsEulerArtificialViscosity.h"
#include "Fluids1D_Sensor.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"


namespace SANS
{

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeTimeOut>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeTimeOut> BCEuler1DParams<BCTypeTimeOut>::params;

template<class VariableType1DParams>
BCEuler1DTimeICParams<VariableType1DParams>::BCEuler1DTimeICParams()
  : StateVector(VariableType1DParams::params.StateVector)
{}

template<class VariableType1DParams>// cppcheck-suppress passedByValue
void BCEuler1DTimeICParams<VariableType1DParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
template<class VariableType1DParams>
BCEuler1DTimeICParams<VariableType1DParams> BCEuler1DTimeICParams<VariableType1DParams>::params;

template struct BCEuler1DTimeICParams<NSVariableType1DParams>;

// cppcheck-suppress passedByValue
void BCEuler1DTimeIC_FunctionParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCEuler1DTimeIC_FunctionParams BCEuler1DTimeIC_FunctionParams::params;

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeReflect>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeReflect> BCEuler1DParams<BCTypeReflect>::params;

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeInflowSupersonic>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.rhou));
  allParams.push_back(d.checkInputs(params.rhoE));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeInflowSupersonic> BCEuler1DParams<BCTypeInflowSupersonic>::params;

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeOutflowSubsonic_Pressure>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeOutflowSubsonic_Pressure> BCEuler1DParams<BCTypeOutflowSubsonic_Pressure>::params;


template<template <class> class PDETraitsSize, class PDETraitsModel>
typename BCEulerSolution1D<PDETraitsSize, PDETraitsModel>::Function_ptr
BCEulerSolution1D<PDETraitsSize, PDETraitsModel>::getSolution(const PyDict& d)
{
  BCEulerSolution1DParams params;

  DictKeyPair SolutionParam = d.get(params.Function);

  if ( SolutionParam == params.Function.Const )
    return Function_ptr( new SolutionFunction_Euler1D_Const<PDETraitsSize, PDETraitsModel>( SolutionParam ) );
  else if ( SolutionParam == params.Function.Riemann )
    return Function_ptr( new SolutionFunction_Euler1D_Riemann<PDETraitsSize, PDETraitsModel>( SolutionParam ) );
  else if ( SolutionParam == params.Function.DensityPulse )
    return Function_ptr( new SolutionFunction_Euler1D_DensityPulse<PDETraitsSize, PDETraitsModel>( SolutionParam ) );
  else if ( SolutionParam == params.Function.ArtShock )
    return Function_ptr( new SolutionFunction_Euler1D_ArtShock<PDETraitsSize, PDETraitsModel>( SolutionParam ) );
  else if ( SolutionParam == params.Function.SineBackPressure )
    return Function_ptr( new SolutionFunction_Euler1D_SineBackPressure<PDETraitsSize, PDETraitsModel>( SolutionParam ) );
  else if ( SolutionParam == params.Function.ShuOsher )
    return Function_ptr( new SolutionFunction_Euler1D_ShuOsher<PDETraitsSize, PDETraitsModel>( SolutionParam ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized solution function specified");
  return NULL; // suppress compiler warnings
}

template struct BCEulerSolution1D< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> >;
template struct BCEulerSolution1D< TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel> >;
template struct BCEulerSolution1D< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> >;
template struct BCEulerSolution1D< TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel> >;

template struct BCEulerSolution1D< TraitsSizeNavierStokes,
                                    TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution1D< TraitsSizeNavierStokes,
                                    TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution1D< TraitsSizeNavierStokes,
                                    TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution1D< TraitsSizeNavierStokes,
                                    TraitsModelNavierStokes<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;

typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;


// Pressure-primitive variables
typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> PDEBaseRhoPressure;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDEBaseRhoPressure>> SensorSourceRhoPressure;
typedef TraitsModelArtificialViscosity<PDEBaseRhoPressure, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceRhoPressure> TraitsModelAVRhoPressure;
template struct BCEulerSolution1D< TraitsSizeEulerArtificialViscosity, TraitsModelAVRhoPressure >;

// Pressure-primitive variables
typedef PDEEuler1D_Source<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> PDEBaseRhoPressure2;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDEBaseRhoPressure2>> SensorSourceRhoPressure2;
typedef TraitsModelArtificialViscosity<PDEBaseRhoPressure2, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceRhoPressure2>
          TraitsModelAVRhoPressure2;
template struct BCEulerSolution1D< TraitsSizeEulerArtificialViscosity, TraitsModelAVRhoPressure2 >;

typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypeConservative, GasModel>> PDEBaseCons;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDEBaseCons>> SensorSourceCons;
typedef TraitsModelArtificialViscosity<PDEBaseCons, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceCons> TraitsModelAVCons;
template struct BCEulerSolution1D< TraitsSizeEulerArtificialViscosity, TraitsModelAVCons >;

typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>> PDEBaseSurrogate;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDEBaseSurrogate>> SensorSourceSurrogate;
typedef TraitsModelArtificialViscosity<PDEBaseSurrogate, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceSurrogate> TraitsModelAVSurrogate;
template struct BCEulerSolution1D< TraitsSizeEulerArtificialViscosity, TraitsModelAVSurrogate >;

typedef PDEEuler1D_Source<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>> PDESrcBaseSurrogate;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDESrcBaseSurrogate>> SensorSourceSurrogate2;
typedef TraitsModelArtificialViscosity<PDESrcBaseSurrogate, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceSurrogate2> TraitsModelAVSurrogate2;
template struct BCEulerSolution1D< TraitsSizeEulerArtificialViscosity, TraitsModelAVSurrogate2 >;

// cppcheck-suppress passedByValue
void BCEuler1DParams< BCTypeFunction_mitState >::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  allParams.push_back(d.checkInputs(params.Characteristic));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams< BCTypeFunction_mitState > BCEuler1DParams< BCTypeFunction_mitState >::params;

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState> BCEuler1DParams<BCTypeOutflowSubsonic_Pressure_mitState>::params;

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_mitState> BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_mitState>::params;

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_mitState>
  BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_mitState>::params;

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState> BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState>::params;

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState>
  BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState>::params;


template<class VariableType1DParams>
BCEuler1DFullStateParams<VariableType1DParams>::BCEuler1DFullStateParams()
  : StateVector(VariableType1DParams::params.StateVector)
{}

template<class VariableType1DParams>// cppcheck-suppress passedByValue
void BCEuler1DFullStateParams<VariableType1DParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Characteristic));
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
template<class VariableType1DParams>
BCEuler1DFullStateParams<VariableType1DParams> BCEuler1DFullStateParams<VariableType1DParams>::params;

template struct BCEuler1DFullStateParams<NSVariableType1DParams>;

template<class VariableType1DParams>
BCEuler1DFullStateSpaceTimeParams<VariableType1DParams>::BCEuler1DFullStateSpaceTimeParams()
  : StateVector(VariableType1DParams::params.StateVector)
{}

template<class VariableType1DParams>// cppcheck-suppress passedByValue
void BCEuler1DFullStateSpaceTimeParams<VariableType1DParams>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Characteristic));
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
template<class VariableType1DParams>
BCEuler1DFullStateSpaceTimeParams<VariableType1DParams> BCEuler1DFullStateSpaceTimeParams<VariableType1DParams>::params;

template struct BCEuler1DFullStateSpaceTimeParams<NSVariableType1DParams>;

// cppcheck-suppress passedByValue
void BCEuler1DParams<BCTypeReflect_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeReflect_mitState> BCEuler1DParams<BCTypeReflect_mitState>::params;

void BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.TtSpec));
  allParams.push_back(d.checkInputs(params.PtSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState> BCEuler1DParams<BCTypeInflowSubsonic_PtTt_mitState>::params;

void BCEuler1DParams<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.TtSpec));
  allParams.push_back(d.checkInputs(params.PtSpec));
  d.checkUnknownInputs(allParams);
}
BCEuler1DParams<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState> BCEuler1DParams<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState>::params;


//===========================================================================//
// Instantiate BC parameters

// Pressure-Primitive Variables
typedef BCEuler1DVector< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> > BCVector_RhoP;
BCPARAMETER_INSTANTIATE( BCVector_RhoP )

// Conservative Variables
typedef BCEuler1DVector< TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel> > BCVector_Cons;
BCPARAMETER_INSTANTIATE( BCVector_Cons )

// Entropy Variables
typedef BCEuler1DVector< TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel> > BCVector_Entropy;
BCPARAMETER_INSTANTIATE( BCVector_Entropy )

// Surrogate Variables
typedef BCEuler1DVector< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> > BCVector_Surrogate;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate )


} //namespace SANS
