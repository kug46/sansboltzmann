// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NSVARIALBE1D_H
#define NSVARIALBE1D_H

#include "Python/PyDict.h" // python must be included first
#include "Python/Parameter.h"

#include <initializer_list>

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

namespace SANS
{

//===========================================================================//
template<class Derived>
struct NSVariableType1D
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//===========================================================================//
struct DensityVelocityPressure1DParams : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Velocity-X"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, 0, NO_LIMIT, "Pressure"};

  static void checkInputs(PyDict d);

  static DensityVelocityPressure1DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct DensityVelocityPressure1D : public NSVariableType1D<DensityVelocityPressure1D<T>>
{
  typedef DensityVelocityPressure1DParams ParamsType;

  DensityVelocityPressure1D() {}

  explicit DensityVelocityPressure1D( const PyDict& d )
    : Density( d.get(DensityVelocityPressure1DParams::params.rho) ),
      VelocityX( d.get(DensityVelocityPressure1DParams::params.u) ),
      Pressure( d.get(DensityVelocityPressure1DParams::params.p) ) {}

  DensityVelocityPressure1D( const T& rho, const T& u, const T& p )
    : Density(rho), VelocityX(u), Pressure(p) {}

  DensityVelocityPressure1D& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == 3);
    auto q = s.begin();
    Density = *q; q++;
    VelocityX = *q; q++;
    Pressure = *q;

    return *this;
  }

  T Density;
  T VelocityX;
  T Pressure;
};

//===========================================================================//
struct DensityVelocityTemperature1DParams : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Velocity-X"};
  const ParameterNumeric<Real> t{"t", NO_DEFAULT, 0, NO_LIMIT, "Temperature"};

  static void checkInputs(PyDict d);

  static DensityVelocityTemperature1DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct DensityVelocityTemperature1D : public NSVariableType1D<DensityVelocityTemperature1D<T>>
{
  typedef DensityVelocityTemperature1DParams ParamsType;

  DensityVelocityTemperature1D() {}

  explicit DensityVelocityTemperature1D( const PyDict& d )
    : Density( d.get(DensityVelocityTemperature1DParams::params.rho) ),
      VelocityX( d.get(DensityVelocityTemperature1DParams::params.u) ),
      Temperature( d.get(DensityVelocityTemperature1DParams::params.t) ) {}

  DensityVelocityTemperature1D( const T& rho, const T& u, const T& t )
    : Density(rho), VelocityX(u), Temperature(t) {}

  DensityVelocityTemperature1D& operator=(const std::initializer_list<Real>& s)
  {
    SANS_ASSERT(s.size() == 3);
    auto q = s.begin();
    Density = *q; q++;
    VelocityX = *q; q++;
    Temperature = *q;

    return *this;
  }

  T Density;
  T VelocityX;
  T Temperature;
};

//===========================================================================//
struct Conservative1DParams : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> rhou{"rhou", NO_DEFAULT, NO_RANGE, "Momentum-X"};
  const ParameterNumeric<Real> rhoE{"rhoE", NO_DEFAULT, 0, NO_LIMIT, "Total Energy"};

  static void checkInputs(PyDict d);

  static Conservative1DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct Conservative1D : public NSVariableType1D<Conservative1D<T>>
{
  typedef Conservative1DParams ParamsType;

  Conservative1D() {}

  explicit Conservative1D( const PyDict& d )
    : Density( d.get(Conservative1DParams::params.rho) ),
      MomentumX( d.get(Conservative1DParams::params.rhou) ),
      TotalEnergy( d.get(Conservative1DParams::params.rhoE) ) {}

  Conservative1D( const T& rho, const T& rhou, const T& rhoE )
    : Density(rho), MomentumX(rhou), TotalEnergy(rhoE) {}
  Conservative1D& operator=(const std::initializer_list<Real>& s)
  {
    SANS_ASSERT(s.size() == 3);
    auto q = s.begin();
    Density = *q; q++;
    MomentumX = *q; q++;
    TotalEnergy = *q;

    return *this;
  }

  T Density;
  T MomentumX;
  T TotalEnergy;
};


//===========================================================================//
struct NSVariableType1DParams : noncopyable
{
  struct VariableOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Variables{"Variables", NO_DEFAULT, "Name of the variable set" };
    const ParameterString& key = Variables;

    const DictOption Conservative{"Conservative", Conservative1DParams::checkInputs};
    const DictOption PrimitiveTemperature{"Primitive Temperature", DensityVelocityTemperature1DParams::checkInputs};
    const DictOption PrimitivePressure{"Primitive Pressure", DensityVelocityPressure1DParams::checkInputs};

    const std::vector<DictOption> options{Conservative,PrimitiveTemperature,PrimitivePressure};
  };
  const ParameterOption<VariableOptions> StateVector{"StateVector", NO_DEFAULT, "The state vector of variables"};

  static void checkInputs(PyDict d);

  static NSVariableType1DParams params;
};


}

#endif //NSVARIALBE1D_H
