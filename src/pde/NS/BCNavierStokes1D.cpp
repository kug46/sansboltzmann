// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCNavierStokes1D.h"
#include "pde/NS/Q1DPrimitiveRhoPressure.h"
#include "pde/NS/Q1DPrimitiveSurrogate.h"
#include "pde/NS/Q1DEntropy.h"
#include "pde/NS/Q1DConservative.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
// Instantiate BC parameters

// Pressure-primitive variables
typedef BCNavierStokes1DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_S )

// Conservative Variables
typedef BCNavierStokes1DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Cons_S;
BCPARAMETER_INSTANTIATE( BCVector_Cons_S )

// Entropy Variables
typedef BCNavierStokes1DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeEntropy, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Entropy_S;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_S )


// Surrogate Variables
typedef BCNavierStokes1DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Surrogate_S;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_S )


//===========================================================================//
// Repeat for constant viscosity

// Pressure-primitive variables
typedef BCNavierStokes1DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_RhoP_C;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_C )

// Conservative Variables
typedef BCNavierStokes1DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Cons_C;
BCPARAMETER_INSTANTIATE( BCVector_Cons_C )

// Entropy Variables
typedef BCNavierStokes1DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Entropy_C;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_C )

// Surrogate Variables
typedef BCNavierStokes1DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Surrogate_C;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_C )


} //namespace SANS
