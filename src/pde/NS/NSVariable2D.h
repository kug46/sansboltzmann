// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NSVARIALBE2D_H
#define NSVARIALBE2D_H

#include "Python/PyDict.h" // python must be included first
#include "Python/Parameter.h"

#include <initializer_list>

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

namespace SANS
{

//===========================================================================//
template<class Derived>
struct NSVariableType2D
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//===========================================================================//
struct DensityVelocityPressure2DParams : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Velocity-X"};
  const ParameterNumeric<Real> v{"v", NO_DEFAULT, NO_RANGE, "Velocity-Y"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, 0, NO_LIMIT, "Pressure"};

  static void checkInputs(PyDict d);

  static DensityVelocityPressure2DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct DensityVelocityPressure2D : public NSVariableType2D<DensityVelocityPressure2D<T>>
{
  typedef DensityVelocityPressure2DParams ParamsType;

  DensityVelocityPressure2D() {}

  explicit DensityVelocityPressure2D( const PyDict& d )
    : Density( d.get(DensityVelocityPressure2DParams::params.rho) ),
      VelocityX( d.get(DensityVelocityPressure2DParams::params.u) ),
      VelocityY( d.get(DensityVelocityPressure2DParams::params.v) ),
      Pressure( d.get(DensityVelocityPressure2DParams::params.p) ) {}

  DensityVelocityPressure2D( const T& rho, const T& u, const T& v, const T& p )
    : Density(rho), VelocityX(u), VelocityY(v), Pressure(p) {}

  DensityVelocityPressure2D& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == 4);
    auto q = s.begin();
    Density = *q; q++;
    VelocityX = *q; q++;
    VelocityY = *q; q++;
    Pressure = *q;

    return *this;
  }

  T Density;
  T VelocityX;
  T VelocityY;
  T Pressure;
};

//===========================================================================//
struct DensityVelocityTemperature2DParams : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Velocity-X"};
  const ParameterNumeric<Real> v{"v", NO_DEFAULT, NO_RANGE, "Velocity-Y"};
  const ParameterNumeric<Real> t{"t", NO_DEFAULT, 0, NO_LIMIT, "Temperature"};

  static void checkInputs(PyDict d);

  static DensityVelocityTemperature2DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct DensityVelocityTemperature2D : public NSVariableType2D<DensityVelocityTemperature2D<T>>
{
  typedef DensityVelocityTemperature2DParams ParamsType;

  DensityVelocityTemperature2D() {}

  explicit DensityVelocityTemperature2D( const PyDict& d )
    : Density( d.get(DensityVelocityTemperature2DParams::params.rho) ),
      VelocityX( d.get(DensityVelocityTemperature2DParams::params.u) ),
      VelocityY( d.get(DensityVelocityTemperature2DParams::params.v) ),
      Temperature( d.get(DensityVelocityTemperature2DParams::params.t) ) {}

  DensityVelocityTemperature2D( const T& rho, const T& u, const T& v, const T& t )
    : Density(rho), VelocityX(u), VelocityY(v), Temperature(t) {}

  DensityVelocityTemperature2D& operator=(const std::initializer_list<Real>& s)
  {
    SANS_ASSERT(s.size() == 4);
    auto q = s.begin();
    Density = *q; q++;
    VelocityX = *q; q++;
    VelocityY = *q; q++;
    Temperature = *q;

    return *this;
  }

  T Density;
  T VelocityX;
  T VelocityY;
  T Temperature;
};

//===========================================================================//
struct Conservative2DParams : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> rhou{"rhou", NO_DEFAULT, NO_RANGE, "Momentum-X"};
  const ParameterNumeric<Real> rhov{"rhov", NO_DEFAULT, NO_RANGE, "Momentum-Y"};
  const ParameterNumeric<Real> rhoE{"rhoE", NO_DEFAULT, 0, NO_LIMIT, "Total Energy"};

  static void checkInputs(PyDict d);

  static Conservative2DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct Conservative2D : public NSVariableType2D<Conservative2D<T>>
{
  typedef Conservative2DParams ParamsType;

  Conservative2D() {}

  explicit Conservative2D( const PyDict& d )
    : Density( d.get(Conservative2DParams::params.rho) ),
      MomentumX( d.get(Conservative2DParams::params.rhou) ),
      MomentumY( d.get(Conservative2DParams::params.rhov) ),
      TotalEnergy( d.get(Conservative2DParams::params.rhoE) ) {}

  Conservative2D( const T& rho, const T& rhou, const T& rhov, const T& rhoE )
    : Density(rho), MomentumX(rhou), MomentumY(rhov), TotalEnergy(rhoE) {}
  Conservative2D& operator=(const std::initializer_list<Real>& s)
  {
    SANS_ASSERT(s.size() == 4);
    auto q = s.begin();
    Density = *q; q++;
    MomentumX = *q; q++;
    MomentumY = *q; q++;
    TotalEnergy = *q;

    return *this;
  }

  T Density;
  T MomentumX;
  T MomentumY;
  T TotalEnergy;
};


//===========================================================================//
struct NSVariableType2DParams : noncopyable
{
  struct VariableOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Variables{"Variables", NO_DEFAULT, "Name of the variable set" };
    const ParameterString& key = Variables;

    const DictOption Conservative{"Conservative", Conservative2DParams::checkInputs};
    const DictOption PrimitiveTemperature{"Primitive Temperature", DensityVelocityTemperature2DParams::checkInputs};
    const DictOption PrimitivePressure{"Primitive Pressure", DensityVelocityPressure2DParams::checkInputs};

    const std::vector<DictOption> options{Conservative,PrimitiveTemperature,PrimitivePressure};
  };
  const ParameterOption<VariableOptions> StateVector{"StateVector", NO_DEFAULT, "The state vector of variables"};

  static void checkInputs(PyDict d);

  static NSVariableType2DParams params;
};


}

#endif //NSVARIALBE2D_H
