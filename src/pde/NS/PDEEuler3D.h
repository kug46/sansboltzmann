// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEEULER3D_H
#define PDEEULER3D_H

// 3-D compressible Euler

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "EulerResidCategory.h"
#include "GasModel.h"
#include "Roe.h"
#include "NSVariable3D.h"
#include "Q3DPrimitiveSurrogate.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q3D;


//----------------------------------------------------------------------------//
// PDE class: 3-D Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEEuler3D
{
public:
  typedef PhysD3 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef typename PDETraitsModel::QType QType;
  typedef Q3D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                     // gas model

  typedef Roe3D<QType, PDETraitsSize> FluxType;

  typedef NSVariableType3DParams VariableType3DParams;

  explicit PDEEuler3D( const GasModel& gas0,
                       const EulerResidualInterpCategory cat = Euler_ResidInterp_Raw,
                       const RoeEntropyFix entropyFix = eVanLeer ) :
    gas_(gas0),
    qInterpret_(gas0),
    cat_(cat),
    flux_(gas0, entropyFix)
  {
    // Nothing
  }

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iyMom = 2;
  static const int izMom = 3;
  static const int iEngy = 4;

  static_assert( iCont == Roe3D<QType, PDETraitsSize>::iCont, "Must match" );
  static_assert( ixMom == Roe3D<QType, PDETraitsSize>::ixMom, "Must match" );
  static_assert( iyMom == Roe3D<QType, PDETraitsSize>::iyMom, "Must match" );
  static_assert( izMom == Roe3D<QType, PDETraitsSize>::izMom, "Must match" );
  static_assert( iEngy == Roe3D<QType, PDETraitsSize>::iEngy, "Must match" );

  PDEEuler3D( const PDEEuler3D& pde0 ) = delete;
  PDEEuler3D& operator=( const PDEEuler3D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  //bool hasSource() const { return std::is_same<QType, QTypePrimitiveSurrogate>::value; }
  bool hasSource() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // unsteady conservative flux: U(Q)
  template<class Tq, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, y, z, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template<class Tq, class Tf>
  void masterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const;

  // master state: U(Q)
  template<class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& dU ) const
  {
    qInterpret_.conservativePerturbation(q, dq, dU);
  }

  // jacobian of master state wrt q: dU/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& Ux, ArrayQ<Tf>& Uy, ArrayQ<Tf>& Uz  ) const;

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
      const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& Uxx, ArrayQ<Tf>& Uxy, ArrayQ<Tf>& Uxz,
      ArrayQ<Tf>& Uyy, ArrayQ<Tf>& Uyz, ArrayQ<Tf>& Uzz  ) const;

  // master state: U(Q)
  template<class Tq, class Tf>
  void adjointState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& V ) const;

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void adjointStateGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy, ArrayQ<Tf>& Vz  ) const;

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void adjointStateHessian(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
      const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vxz,
      ArrayQ<Tf>& Vyy, ArrayQ<Tf>& Vyz, ArrayQ<Tf>& Vzz  ) const;

  // conversion from entropy variables dudv
  template<class Tq>
  void adjointVariableJacobian(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const;

  // conversion to entropy variables dvdu
  template<class Tq>
  void adjointVariableJacobianInverse(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const;

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& fn, const Real& scale = 1 ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfxdu, MatrixQ<Tf>& dfydu, MatrixQ<Tf>& dfzdu ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<Tf>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& strongPDE ) const;


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const {}

  template <class T, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL, const ArrayQ<T>& qzL,
      const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR, const ArrayQ<T>& qzR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& fn ) const {}

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y,  const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy, const ArrayQ<Tgu>& dqz,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg, ArrayQ<Tf>& dh ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz) const {}

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const
  {
    // Nothing here
  }

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const {}

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& strongPDE ) const
  {
    // Nothing here
  }


  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const;

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;
    ArrayQ<Tgg> qqy = qy + qpy;
    ArrayQ<Tgg> qqz = qz + qpz;

    source(x, y, z, time, qq, qqx, qqy, qqz, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, z, time, q, qx, qy, qz, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& zL,
      const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& zL,
      const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const {}


  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue( const Tp& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts, class Th>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Ts>& div_dsdgradu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts, class Th>
  void jacobianGradientSourceGradientHACK(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Ts>& div_dsdgradu ) const {}

  // linear change in source in response to linear perturbations du, dux, duy, duz
  template <class Tq, class Tg, class Tu, class Tug, class Ts>
  void perturbedSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,  const ArrayQ<Tg>& qx,  const ArrayQ<Tg>& qy,  const ArrayQ<Tg>& qz,
      const ArrayQ<Tu>& du, const ArrayQ<Tug>& dux, const ArrayQ<Tug>& duy, const ArrayQ<Tug>& duz,
      ArrayQ<Ts>& dS ) const {}

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& forcing ) const {}

  // characteristic speed (needed for timestep)
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, Real, Real dx, Real dy, Real dz, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const;


  // PDE specific stabilization matrix
  template <class Tq, class Tf>
  void stabMatrix(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      const Tq& cn1, const Tq& cn2, const Tq& cn3, MatrixQ<Tf>& dfdu ) const;

  // PDE specific function to accumulate basis functions for stabilization matrix
  template <class Tq>
  void getCN( const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
              const Real& nx, const Real& ny, const Real& nz, Tq& cn1, Tq& cn2, Tq& cn3 ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& y, const Real& z, const Real& time,
                       const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // set from variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T, class Variables>
  void setDOFFrom(
      ArrayQ<T>& q, const NSVariableType3D<Variables>& data ) const;

  // set from variable
  template<class Variables>
  ArrayQ<Real> setDOFFrom( const NSVariableType3D<Variables>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the residual-interpreter type
  EulerResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // accessors for gas model and Q interpreter
  const GasModel& gasModel() const { return gas_; }
  const QInterpret& variableInterpreter() const { return qInterpret_; }

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const GasModel& gas_;
  const QInterpret qInterpret_;               // solution variable interpreter class
  EulerResidualInterpCategory cat_;           // Residual interp type
  const FluxType flux_;                       // Roe upwinding
};


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const
{
  Tq rho = 0, u = 0, v = 0, w = 0, t = 0, e = 0, E = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v + w*w);

  uCons(iCont) = rho;
  uCons(ixMom) = rho*u;
  uCons(iyMom) = rho*v;
  uCons(izMom) = rho*w;
  uCons(iEngy) = rho*E;
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::masterStateGradient(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& Ux, ArrayQ<Tf>& Uy, ArrayQ<Tf>& Uz ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq rho = 0, u = 0, v = 0, w=0, t = 0;
  T rhox = 0, ux = 0, vx = 0, wx=0, tx = 0;
  T rhoy = 0, uy = 0, vy = 0, wy=0, ty = 0;
  T rhoz = 0, uz = 0, vz = 0, wz=0, tz = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient(q, qx, rhox, ux, vx, wx, tx);
  qInterpret_.evalGradient(q, qy, rhoy, uy, vy, wy, ty);
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  Tq e = gas_.energy(rho, t);
  Tq E = e + 0.5*(u*u + v*v + w*w);

  Real Cv = gas_.Cv();
  T Ex = Cv*tx + u*ux + v*vx + w*wx;
  T Ey = Cv*ty + u*uy + v*vy + w*wy;
  T Ez = Cv*tz + u*uz + v*vz + w*wz;

  Ux(iCont) = rhox;
  Ux(ixMom) = rhox*u + rho*ux;
  Ux(iyMom) = rhox*v + rho*vx;
  Ux(izMom) = rhox*w + rho*wx;
  Ux(iEngy) = rhox*E + rho*Ex;

  Uy(iCont) = rhoy;
  Uy(ixMom) = rhoy*u + rho*uy;
  Uy(iyMom) = rhoy*v + rho*vy;
  Uy(izMom) = rhoy*w + rho*wy;
  Uy(iEngy) = rhoy*E + rho*Ey;

  Uz(iCont) = rhoz;
  Uz(ixMom) = rhoz*u + rho*uz;
  Uz(iyMom) = rhoz*v + rho*vz;
  Uz(izMom) = rhoz*w + rho*wz;
  Uz(iEngy) = rhoz*E + rho*Ez;
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::masterStateHessian(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
    const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& Uxx, ArrayQ<Tf>& Uxy, ArrayQ<Tf>& Uxz,
    ArrayQ<Tf>& Uyy, ArrayQ<Tf>& Uyz, ArrayQ<Tf>& Uzz ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho = 0, u = 0, v = 0, w=0, t = 0;
  Tqg rhox = 0, ux = 0, vx = 0, wx=0, tx = 0;
  Tqg rhoy = 0, uy = 0, vy = 0, wy=0, ty = 0;
  Tqg rhoz = 0, uz = 0, vz = 0, wz=0, tz = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  // evaluate second derivatives
  T rhoxx = 0, rhoxy = 0, rhoxz = 0, rhoyy = 0, rhoyz = 0, rhozz = 0;
  T uxx = 0, uxy = 0, uxz = 0, uyy=0, uyz = 0, uzz = 0;
  T vxx = 0, vxy = 0, vxz = 0, vyy=0, vyz = 0, vzz = 0;
  T wxx = 0, wxy = 0, wxz = 0, wyy=0, wyz = 0, wzz = 0;
  T txx = 0, txy = 0, txz = 0, tyy=0, tyz = 0, tzz = 0;

  DLA::MatrixSymS<D,T> rhoH, uH, vH, wH, tH;
  qInterpret_.evalSecondGradient( q, qx, qy, qz,
                                  qxx, qxy, qyy, qxz, qyz, qzz,
                                  rhoH, uH, vH, wH, tH );
  rhoxx = rhoH(0,0); uxx = uH(0,0);  vxx = vH(0,0);  wxx = wH(0,0);  txx = tH(0,0);
  rhoxy = rhoH(1,0); uxy = uH(1,0);  vxy = vH(1,0);  wxy = wH(1,0);  txy = tH(1,0);
  rhoyy = rhoH(1,1); uyy = uH(1,1);  vyy = vH(1,1);  wyy = wH(1,1);  tyy = tH(1,1);
  rhoxz = rhoH(2,0); uxz = uH(2,0);  vxz = vH(2,0);  wxz = wH(2,0);  txz = tH(2,0);
  rhoyz = rhoH(2,1); uyz = uH(2,1);  vyz = vH(2,1);  wyz = wH(2,1);  tyz = tH(2,1);
  rhozz = rhoH(2,2); uzz = uH(2,2);  vzz = vH(2,2);  wzz = wH(2,2);  tzz = tH(2,2);

  Tq e = gas_.energy(rho, t);
  Tq E = e + 0.5*(u*u + v*v + w*w);

  Real Cv = gas_.Cv();
  T Ex = Cv*tx + u*ux + v*vx + w*wx;
  T Ey = Cv*ty + u*uy + v*vy + w*wy;
  T Ez = Cv*tz + u*uz + v*vz + w*wz;

  T Exx = Cv*txx + ux*ux + u*uxx + vx*vx + v*vxx + wx*wx + w*wxx;
  T Exy = Cv*txy + ux*uy + u*uxy + vx*vy + v*vxy + wx*wy + w*wxy;
  T Exz = Cv*txz + ux*uz + u*uxz + vx*vz + v*vxz + wx*wz + w*wxz;
  T Eyy = Cv*tyy + uy*uy + u*uyy + vy*vy + v*vyy + wy*wy + w*wyy;
  T Eyz = Cv*tyz + uy*uz + u*uyz + vy*vz + v*vyz + wy*wz + w*wyz;
  T Ezz = Cv*tzz + uz*uz + u*uzz + vz*vz + v*vzz + wz*wz + w*wzz;

  Uxx(iCont) = rhoxx;
  Uxx(ixMom) = rhoxx*u + 2*rhox*ux + rho*uxx;
  Uxx(iyMom) = rhoxx*v + 2*rhox*vx + rho*vxx;
  Uxx(izMom) = rhoxx*w + 2*rhox*wx + rho*wxx;
  Uxx(iEngy) = rhoxx*E + 2*rhox*Ex + rho*Exx;

  Uxy(iCont) = rhoxy;
  Uxy(ixMom) = rhoxy*u + rhox*uy + rhoy*ux + rho*uxy;
  Uxy(iyMom) = rhoxy*v + rhox*vy + rhoy*vx + rho*vxy;
  Uxy(izMom) = rhoxy*w + rhox*wy + rhoy*wx + rho*wxy;
  Uxy(iEngy) = rhoxy*E + rhox*Ey + rhoy*Ex + rho*Exy;

  Uxz(iCont) = rhoxz;
  Uxz(ixMom) = rhoxz*u + rhox*uz + rhoz*ux + rho*uxz;
  Uxz(iyMom) = rhoxz*v + rhox*vz + rhoz*vx + rho*vxz;
  Uxz(izMom) = rhoxz*w + rhox*wz + rhoz*wx + rho*wxz;
  Uxz(iEngy) = rhoxz*E + rhox*Ez + rhoz*Ex + rho*Exz;

  Uyy(iCont) = rhoyy;
  Uyy(ixMom) = rhoyy*u + 2*rhoy*uy + rho*uyy;
  Uyy(iyMom) = rhoyy*v + 2*rhoy*vy + rho*vyy;
  Uyy(izMom) = rhoyy*w + 2*rhoy*wy + rho*wyy;
  Uyy(iEngy) = rhoyy*E + 2*rhoy*Ey + rho*Eyy;

  Uyz(iCont) = rhoyz;
  Uyz(ixMom) = rhoyz*u + rhoy*uz + rhoz*uy + rho*uyz;
  Uyz(iyMom) = rhoyz*v + rhoy*vz + rhoz*vy + rho*vyz;
  Uyz(izMom) = rhoyz*w + rhoy*wz + rhoz*wy + rho*wyz;
  Uyz(iEngy) = rhoyz*E + rhoy*Ez + rhoz*Ey + rho*Eyz;

  Uzz(iCont) = rhozz;
  Uzz(ixMom) = rhozz*u + 2*rhoz*uz + rho*uzz;
  Uzz(iyMom) = rhozz*v + 2*rhoz*vz + rho*vzz;
  Uzz(izMom) = rhozz*w + 2*rhoz*wz + rho*wzz;
  Uzz(iEngy) = rhozz*E + 2*rhoz*Ez + rho*Ezz;
}


// unsteady conservative flux Jacobian: dU(Q)/dQ
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  ArrayQ<T> rho_q, u_q, v_q, w_q, t_q, E_q;
  T rho = 0, u = 0, v = 0, w = 0, t = 0, e = 0, E = 0;
  T e_rho, e_t;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalJacobian( q, rho_q, u_q, v_q, w_q, t_q );
  e = gas_.energy(rho, t);
  gas_.energyJacobian(rho, t, e_rho, e_t);

  E = e + 0.5*(u*u + v*v + w*w);
  E_q = e_rho*rho_q + e_t*t_q + u*u_q + v*v_q + w*w_q;

  dudq(iCont,0) = rho_q[0];
  dudq(iCont,1) = rho_q[1];
  dudq(iCont,2) = rho_q[2];
  dudq(iCont,3) = rho_q[3];
  dudq(iCont,4) = rho_q[4];

  dudq(ixMom,0) = rho_q[0]*u + rho*u_q[0];
  dudq(ixMom,1) = rho_q[1]*u + rho*u_q[1];
  dudq(ixMom,2) = rho_q[2]*u + rho*u_q[2];
  dudq(ixMom,3) = rho_q[3]*u + rho*u_q[3];
  dudq(ixMom,4) = rho_q[4]*u + rho*u_q[4];

  dudq(iyMom,0) = rho_q[0]*v + rho*v_q[0];
  dudq(iyMom,1) = rho_q[1]*v + rho*v_q[1];
  dudq(iyMom,2) = rho_q[2]*v + rho*v_q[2];
  dudq(iyMom,3) = rho_q[3]*v + rho*v_q[3];
  dudq(iyMom,4) = rho_q[4]*v + rho*v_q[4];

  dudq(izMom,0) = rho_q[0]*w + rho*w_q[0];
  dudq(izMom,1) = rho_q[1]*w + rho*w_q[1];
  dudq(izMom,2) = rho_q[2]*w + rho*w_q[2];
  dudq(izMom,3) = rho_q[3]*w + rho*w_q[3];
  dudq(izMom,4) = rho_q[4]*w + rho*w_q[4];

  dudq(iEngy,0) = rho_q[0]*E + rho*E_q[0];
  dudq(iEngy,1) = rho_q[1]*E + rho*E_q[1];
  dudq(iEngy,2) = rho_q[2]*E + rho*E_q[2];
  dudq(iEngy,3) = rho_q[3]*E + rho*E_q[3];
  dudq(iEngy,4) = rho_q[4]*E + rho*E_q[4];
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::adjointState(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, ArrayQ<Tf>& V ) const
{
  Tq rho = 0, u = 0, v = 0, w =0, t = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  Tq p = gas_.pressure(rho, t);
  Real gamma = gas_.gamma();
  Real gm1 = gamma-1.;
  Real R = gas_.R();

  Tq s = log( p/pow(rho,gamma));
  Real s0 = 0;

  V(iCont) = -(s - s0)/gm1 + (gamma)/gm1 - 0.5*(u*u + v*v + w*w)/(R*t);
  V(ixMom) = u/(R*t);
  V(iyMom) = v/(R*t);
  V(izMom) = w/(R*t);
  V(iEngy) = -1./(R*t);
}



// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::adjointStateGradient(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy, ArrayQ<Tf>& Vz ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq rho = 0, u = 0, v = 0, w=0, t = 0;
  T rhox = 0, ux = 0, vx = 0, wx=0, tx = 0;
  T rhoy = 0, uy = 0, vy = 0, wy=0, ty = 0;
  T rhoz = 0, uz = 0, vz = 0, wz=0, tz = 0;

  Real R = gas_.R();
  Real gamma = gas_.gamma();
  Real gm1 = gamma-1.;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient(q, qx, rhox, ux, vx, wx, tx);
  qInterpret_.evalGradient(q, qy, rhoy, uy, vy, wy, ty);
  qInterpret_.evalGradient( q, qz, rhoz, uz, vy, wz, tz );

  Tq p = gas_.pressure(rho, t);
  T px = 0, py = 0, pz = 0;
  gas_.pressureGradient(rho, t, rhox, tx, px);
  gas_.pressureGradient(rho, t, rhoy, ty, py);
  gas_.pressureGradient(rho, t, rhoz, tz, pz);

  T sx = px / p - gamma*rhox/rho;
  T sy = py / p - gamma*rhoy/rho;
  T sz = pz / p - gamma*rhoz/rho;

  Vx(iCont) = -sx/gm1 - (u*ux + v*vx + w*wx)/(R*t) + 0.5*(u*u + v*v + w*w)*tx/(R*t*t);
  Vx(ixMom) = ux/(R*t)-u*tx/(R*t*t);
  Vx(iyMom) = vx/(R*t)-v*tx/(R*t*t);
  Vx(izMom) = wx/(R*t)-w*tx/(R*t*t);
  Vx(iEngy) = tx/(R*t*t);

  Vy(iCont) = -sy/gm1 - (u*uy + v*vy + w*wy)/(R*t) + 0.5*(u*u + v*v  + w*w)*ty/(R*t*t);
  Vy(ixMom) = uy/(R*t)-u*ty/(R*t*t);
  Vy(iyMom) = vy/(R*t)-v*ty/(R*t*t);
  Vy(izMom) = wy/(R*t)-w*ty/(R*t*t);
  Vy(iEngy) = ty/(R*t*t);

  Vz(iCont) = -sz/gm1 - (u*uz + v*vz + w*wz)/(R*t) + 0.5*(u*u + v*v  + w*w)*tz/(R*t*t);
  Vz(ixMom) = uz/(R*t)-u*tz/(R*t*t);
  Vz(iyMom) = vz/(R*t)-v*tz/(R*t*t);
  Vz(izMom) = wz/(R*t)-w*tz/(R*t*t);
  Vz(iEngy) = tz/(R*t*t);
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::adjointStateHessian(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
    const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vxz,
    ArrayQ<Tf>& Vyy, ArrayQ<Tf>& Vyz, ArrayQ<Tf>& Vzz ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho = 0, u = 0, v = 0, w=0, t = 0;
  Tqg rhox = 0, ux = 0, vx = 0, wx=0, tx = 0;
  Tqg rhoy = 0, uy = 0, vy = 0, wy=0, ty = 0;
  Tqg rhoz = 0, uz = 0, vz = 0, wz=0, tz = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vy, wz, tz );

  // evaluate second derivatives
  T rhoxx = 0, rhoxy = 0, rhoxz = 0, rhoyy = 0, rhoyz = 0, rhozz = 0;
  T uxx = 0, uxy = 0, uxz = 0, uyy=0, uyz = 0, uzz = 0;
  T vxx = 0, vxy = 0, vxz = 0, vyy=0, vyz = 0, vzz = 0;
  T wxx = 0, wxy = 0, wxz = 0, wyy=0, wyz = 0, wzz = 0;
  T txx = 0, txy = 0, txz = 0, tyy=0, tyz = 0, tzz = 0;

  DLA::MatrixSymS<D,T> rhoH, uH, vH, wH, tH;
  qInterpret_.evalSecondGradient( q, qx, qy, qz,
                                  qxx, qxy, qyy, qxz, qyz, qzz,
                                  rhoH, uH, vH, wH, tH );
  rhoxx = rhoH(0,0); uxx = uH(0,0);  vxx = vH(0,0);  wxx = wH(0,0);  txx = tH(0,0);
  rhoxy = rhoH(1,0); uxy = uH(1,0);  vxy = vH(1,0);  wxy = wH(1,0);  txy = tH(1,0);
  rhoyy = rhoH(1,1); uyy = uH(1,1);  vyy = vH(1,1);  wyy = wH(1,1);  tyy = tH(1,1);
  rhoxz = rhoH(2,0); uxz = uH(2,0);  vxz = vH(2,0);  wxz = wH(2,0);  txz = tH(2,0);
  rhoyz = rhoH(2,1); uyz = uH(2,1);  vyz = vH(2,1);  wyz = wH(2,1);  tyz = tH(2,1);
  rhoyz = rhoH(2,2); uzz = uH(2,2);  vzz = vH(2,2);  wzz = wH(2,2);  tzz = tH(2,2);


  Real R = gas_.R();
  Real gamma = gas_.gamma();
  Real gm1 = gamma-1.;

  Tq p = gas_.pressure(rho, t);
  Tqg px = 0, py = 0, pz = 0;
  gas_.pressureGradient(rho, t, rhox, tx, px);
  gas_.pressureGradient(rho, t, rhoy, ty, py);
  gas_.pressureGradient(rho, t, rhoz, tz, pz);

  T pxx = R*(rhoxx*t + 2*rhox*tx + rho*txx);
  T pxy = R*(rhoxy*t + rhox*ty + rhoy*tx + rho*txy);
  T pxz = R*(rhoxz*t + rhox*tz + rhoz*tx + rho*txz);
  T pyy = R*(rhoyy*t + 2*rhoy*ty + rho*tyy);
  T pyz = R*(rhoyz*t + rhoy*tz + rhoz*ty + rho*tyz);
  T pzz = R*(rhozz*t + 2*rhoz*tz + rho*tzz);

//  T sx = px / p - gamma*rhox/rho;
//  T sy = py / p - gamma*rhoy/rho;

  T sxx = (pxx*p - px*px)/(p*p) - gamma/(rho*rho)*(rhoxx*rho - rhox*rhox );
  T sxy = (pxy*p - px*py)/(p*p) - gamma/(rho*rho)*(rhoxy*rho - rhox*rhoy );
  T sxz = (pxz*p - px*pz)/(p*p) - gamma/(rho*rho)*(rhoxz*rho - rhox*rhoz );
  T syy = (pyy*p - py*py)/(p*p) - gamma/(rho*rho)*(rhoyy*rho - rhoy*rhoy );
  T syz = (pyz*p - py*pz)/(p*p) - gamma/(rho*rho)*(rhoyz*rho - rhoy*rhoz );
  T szz = (pzz*p - pz*pz)/(p*p) - gamma/(rho*rho)*(rhozz*rho - rhoz*rhoz );

//  Vx(iCont) = sx/gm1 - (u*ux + v*vx)/(R*t) + 0.5*(u*u + v*v)/(R*t*t)*tx;  //rhoE/p = 0.5(u^2 + v^2)/(2*R*t)
//  Vx(ixMom) = ux/(R*t)-u/(R*t*t)*tx;
//  Vx(iyMom) = vx/(R*t)-v/(R*t*t)*tx;
//  Vx(iEngy) = 1./(R*t*t)*tx;
//
//  Vy(iCont) = sy/gm1 - (u*uy + v*vy)/(R*t) + 0.5*(u*u + v*v)/(R*t*t)*ty;
//  Vy(ixMom) = uy/(R*t)-u/(R*t*t)*ty;
//  Vy(iyMom) = vy/(R*t)-v/(R*t*t)*ty;
//  Vy(iEngy) = 1./(R*t*t)*ty;

  Vxx(iCont) = -sxx/gm1 - (u*uxx + ux*ux + vx*vx + v*vxx + wx*wx + w*wxx)/(R*t) + 2*(u*ux + v*vx + w*wx)*tx/(R*t*t)
                 - (u*u + v*v + w*w)*tx*tx/(R*t*t*t)+ 0.5*(u*u + v*v + w*w)/(R*t*t)*txx;
  Vxx(ixMom) = uxx/(R*t) - 2*ux*tx/(R*t*t) + 2*u*tx*tx/(R*t*t*t) - u*txx/(R*t*t);
  Vxx(iyMom) = vxx/(R*t) - 2*vx*tx/(R*t*t) + 2*v*tx*tx/(R*t*t*t) - v*txx/(R*t*t);
  Vxx(izMom) = wxx/(R*t) - 2*wx*tx/(R*t*t) + 2*w*tx*tx/(R*t*t*t) - w*txx/(R*t*t);
  Vxx(iEngy) = -2*tx*tx/(R*t*t*t) + txx/(R*t*t);

  Vxy(iCont) = -sxy/gm1 - (u*uxy + ux*uy + vx*vy + v*vxy + wx*wy + w*wxy)/(R*t) + (u*ux + v*vx + w*wx)*(tx + ty)/(R*t*t)
      - (u*u + v*v + w*w)/(R*t*t*t)*tx*ty + 0.5*(u*u + v*v + w*w)/(R*t*t)*txy;
  Vxy(ixMom) = uxy/(R*t) - ux*ty/(R*t*t) - uy*tx/(R*t*t) + 2*u*tx*ty/(R*t*t*t) - u*txy/(R*t*t);
  Vxy(iyMom) = vxy/(R*t) - vx*ty/(R*t*t) - vy*tx/(R*t*t) + 2*v*tx*ty/(R*t*t*t) - v*txy/(R*t*t);
  Vxy(izMom) = wxy/(R*t) - wx*ty/(R*t*t) - wy*tx/(R*t*t) + 2*w*tx*ty/(R*t*t*t) - w*txy/(R*t*t);
  Vxy(iEngy) = -2*tx*ty/(R*t*t*t) + txy/(R*t*t);

  Vxz(iCont) = -sxz/gm1 - (u*uxz + ux*uz + vx*vz + v*vxz + wx*wz + w*wxz)/(R*t) + (u*ux + v*vx + w*wx)*(tx + tz)/(R*t*t)
      - (u*u + v*v + w*w)/(R*t*t*t)*tx*tz + 0.5*(u*u + v*v + w*w)/(R*t*t)*txz;
  Vxz(ixMom) = uxz/(R*t) - ux*tz/(R*t*t) - uz*tx/(R*t*t) + 2*u*tx*tz/(R*t*t*t) - u*txz/(R*t*t);
  Vxz(izMom) = vxz/(R*t) - vx*tz/(R*t*t) - vz*tx/(R*t*t) + 2*v*tx*tz/(R*t*t*t) - v*txz/(R*t*t);
  Vxz(izMom) = wxz/(R*t) - wx*tz/(R*t*t) - wz*tx/(R*t*t) + 2*w*tx*tz/(R*t*t*t) - w*txz/(R*t*t);
  Vxz(iEngy) = -2*tx*tz/(R*t*t*t) + txz/(R*t*t);

  Vyy(iCont) = -syy/gm1 - (u*uyy + uy*uy + vy*vy + v*vyy + wy*wy + w*wyy)/(R*t) + 2*(u*uy + v*vy + w*wy)*ty/(R*t*t)
      - (u*u + v*v + w*w)*ty*ty/(R*t*t*t) + 0.5*(u*u + v*v + w*w)/(R*t*t)*tyy;
  Vyy(ixMom) = uyy/(R*t) - 2*uy*ty/(R*t*t) + 2*u*ty*ty/(R*t*t*t) - u*tyy/(R*t*t);
  Vyy(iyMom) = vyy/(R*t) - 2*vy*ty/(R*t*t) + 2*v*ty*ty/(R*t*t*t) - v*tyy/(R*t*t);
  Vyy(izMom) = wyy/(R*t) - 2*wy*ty/(R*t*t) + 2*w*ty*ty/(R*t*t*t) - w*tyy/(R*t*t);
  Vyy(iEngy) = -2*ty*ty/(R*t*t*t) + tyy/(R*t*t);

  Vyz(iCont) = -syz/gm1 - (u*uyz + uy*uz + vy*vz + v*vyz + wy*wz + w*wyz)/(R*t) + (u*uy + v*vy + w*wy)*(ty + tz)/(R*t*t)
      - (u*u + v*v + w*w)/(R*t*t*t)*ty*tz + 0.5*(u*u + v*v + w*w)/(R*t*t)*tyz;
  Vyz(iyMom) = uyz/(R*t) - uy*tz/(R*t*t) - uz*ty/(R*t*t) + 2*u*ty*tz/(R*t*t*t) - u*tyz/(R*t*t);
  Vyz(izMom) = vyz/(R*t) - vy*tz/(R*t*t) - vz*ty/(R*t*t) + 2*v*ty*tz/(R*t*t*t) - v*tyz/(R*t*t);
  Vyz(izMom) = wyz/(R*t) - wy*tz/(R*t*t) - wz*ty/(R*t*t) + 2*w*ty*tz/(R*t*t*t) - w*tyz/(R*t*t);
  Vyz(iEngy) = -2*ty*tz/(R*t*t*t) + tyz/(R*t*t);

  Vzz(iCont) = -szz/gm1 - (u*uzz + uz*uz + vz*vz + v*vzz + wz*wz + w*wzz)/(R*t) + 2*(u*uz + v*vz + w*wz)*tz/(R*t*t)
                 - (u*u + v*v + w*w)*tz*tz/(R*t*t*t)+ 0.5*(u*u + v*v + w*w)/(R*t*t)*tzz;
  Vzz(izMom) = uzz/(R*t) - 2*uz*tz/(R*t*t) + 2*u*tz*tz/(R*t*t*t) - u*tzz/(R*t*t);
  Vzz(iyMom) = vzz/(R*t) - 2*vz*tz/(R*t*t) + 2*v*tz*tz/(R*t*t*t) - v*tzz/(R*t*t);
  Vzz(izMom) = wzz/(R*t) - 2*wz*tz/(R*t*t) + 2*w*tz*tz/(R*t*t*t) - w*tzz/(R*t*t);
  Vzz(iEngy) = -2*tz*tz/(R*t*t*t) + tzz/(R*t*t);
}



// entropy variable jacobian du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::adjointVariableJacobian(
    const Real& x, const Real& y,  const Real& z, const Real& time,
    const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const
{
  //entropy variable interpreter for adjoint
  Tq rho = 0, u = 0, v = 0, w=0, t = 0;

  Real gamma = gas_.gamma();
//  Real  s0 = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  Tq p  = gas_.pressure(rho, t);

  Tq h = gas_.enthalpy(rho, t);
  Tq H = h + 0.5*(u*u + v*v + w*w);

  dudv(0,0) = rho;
  dudv(0,1) = rho*u;
  dudv(0,2) = rho*v;
  dudv(0,3) = rho*w;
  dudv(0,4) = rho*H - p;

  dudv(1,0) = dudv(0,1);
  dudv(1,1) = p + rho*u*u;
  dudv(1,2) = rho*u*v;
  dudv(1,3) = rho*u*w;
  dudv(1,4) = rho*u*H;

  dudv(2,0) = dudv(0,2);
  dudv(2,1) = dudv(1,2);
  dudv(2,2) = p+rho*v*v;
  dudv(2,3) = rho*v*w;
  dudv(2,4) = rho*v*H;

  dudv(3,0) = dudv(0,3);
  dudv(3,1) = dudv(1,3);
  dudv(3,2) = dudv(2,3);
  dudv(3,3) = p+rho*w*w;
  dudv(3,4) = rho*w*H;

  dudv(4,0) = dudv(0,4);
  dudv(4,1) = dudv(1,4);
  dudv(4,2) = dudv(2,4);
  dudv(4,3) = dudv(3,4);
  dudv(4,4) = rho*H*H - p*p/rho * (gamma)/(gamma-1);

}


// entropy variable jacobian du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::adjointVariableJacobianInverse(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const
{
  //entropy variable interpreter for adjoint
  Tq rho = 0, u = 0, v = 0, w=0, t = 0;

  Real gamma = gas_.gamma();
//  Real  s0 = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
//  e = gas_.energy(rho, t);
//  E = e + 0.5*(u*u + v*v);
  Tq p  = gas_.pressure(rho, t);

//  Tq h = gas_.enthalpy(rho, t);
//  Tq H = h + 0.5*(u*u + v*v);

//  Tq s = log( p / pow(rho,gamma));

  Tq q2 = u*u + v*v + w*w;
  Real gm1 = gamma-1.;
  Tq p2 = p*p;

  dvdu(0,0) = (p2*gamma + 0.25*q2*q2*gm1*gm1*rho*rho )/(p2*gm1*rho);
  dvdu(0,1) = -0.5*u*q2*gm1*rho/p2;
  dvdu(0,2) = -0.5*v*q2*gm1*rho/p2;
  dvdu(0,3) = -0.5*w*q2*gm1*rho/p2;
  dvdu(0,4) = (-p + 0.5*q2*gm1*rho)/p2;

  dvdu(1,0) =  dvdu(0,1);
  dvdu(1,1) = (p + u*u*gm1*rho)/p2;
  dvdu(1,2) = u*v*gm1*rho/p2;
  dvdu(1,3) = u*w*gm1*rho/p2;
  dvdu(1,4) = -u*gm1*rho/p2;

  dvdu(2,0) = dvdu(0,2);
  dvdu(2,1) = dvdu(1,2);
  dvdu(2,2) = (p + v*v*gm1*rho)/p2;
  dvdu(2,3) = v*w*gm1*rho/p2;
  dvdu(2,4) = -v*(gm1)*rho/p2;

  dvdu(3,0) = dvdu(0,3);
  dvdu(3,1) = dvdu(1,3);
  dvdu(3,2) = dvdu(2,3);
  dvdu(3,3) = (p + w*w*gm1*rho)/p2;
  dvdu(3,4) = -w*(gm1)*rho/p2;

  dvdu(4,0) = dvdu(0,4);
  dvdu(4,1) = dvdu(1,4);
  dvdu(4,2) = dvdu(2,4);
  dvdu(4,3) = dvdu(3,4);
  dvdu(4,4) = rho*gm1/p2;

}

// strong form of unsteady conservative flux: dU(Q)/dt
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& utCons ) const
{
  T rho = 0, u = 0, v = 0, w = 0, t = 0, e = 0, E = 0;
  T rhot, ut, vt, wt, tt, et, Et;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qt, rhot, ut, vt, wt, tt);

  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v + w*w);

  gas_.energyGradient(rho, t, rhot, tt, et);
  Et = et + u*ut + v*vt + w*wt;

  utCons(iCont) += rhot;
  utCons(ixMom) += rho*ut + u*rhot;
  utCons(iyMom) += rho*vt + v*rhot;
  utCons(izMom) += rho*wt + w*rhot;
  utCons(iEngy) += rho*Et + E*rhot;
}


// advective flux: F(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
{
  T rho = 0, u = 0, v = 0, w = 0, t = 0;
  T p, h, H;

  qInterpret_.eval( q, rho, u, v, w, t );
  p = gas_.pressure(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v + w*w);

  fx(iCont) += rho*u;
  fx(ixMom) += rho*u*u + p;
  fx(iyMom) += rho*u*v;
  fx(izMom) += rho*u*w;
  fx(iEngy) += rho*u*H;

  fy(iCont) += rho*v;
  fy(ixMom) += rho*v*u;
  fy(iyMom) += rho*v*v + p;
  fy(izMom) += rho*v*w;
  fy(iEngy) += rho*v*H;

  fz(iCont) += rho*w;
  fz(ixMom) += rho*w*u;
  fz(iyMom) += rho*w*v;
  fz(izMom) += rho*w*w + p;
  fz(iEngy) += rho*w*H;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& fn, const Real& scale ) const
{
  flux_.fluxAdvectiveUpwind(x, y, z, time, qL, qR, nx, ny, nz, fn, scale);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nz, const Real& nt, ArrayQ<Tf>& fn ) const
{
//  flux_.fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tq> fnx = 0;
  fluxAdvectiveUpwind( x, y, z, time, qL, qR, nx, ny, nz, fnx );

  //Upwind temporal flux
  ArrayQ<Tq> ft = 0;
  if (nt >= 0)
    fluxAdvectiveTime( x, y, z, time, qL, ft );
  else
    fluxAdvectiveTime( x, y, z, time, qR, ft );

  fn += ft*nt + fnx;
}


// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q,
    MatrixQ<Tf>& dfxdu, MatrixQ<Tf>& dfydu, MatrixQ<Tf>& dfzdu ) const
{
  Real gamma = gas_.gamma();
  Tq rho = 0, u = 0, v = 0, w = 0, t = 0, h = 0, H = 0, V2 = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  h  = gas_.enthalpy(rho, t);
  V2 = u*u + v*v + w*w;
  H  = h + 0.5*V2;

  const int ir  = 0;
  const int iru = 1;
  const int irv = 2;
  const int irw = 3;
  const int irE = 4;

  dfxdu(iCont,iru) += 1;

  dfxdu(ixMom,ir ) += -u*u + 0.5*(gamma - 1)*V2;
  dfxdu(ixMom,iru) += -(gamma - 3)*u;
  dfxdu(ixMom,irv) += -(gamma - 1)*v;
  dfxdu(ixMom,irw) += -(gamma - 1)*w;
  dfxdu(ixMom,irE) += gamma - 1;

  dfxdu(iyMom,ir ) += -u*v;
  dfxdu(iyMom,iru) += v;
  dfxdu(iyMom,irv) += u;

  dfxdu(izMom,ir ) += -u*w;
  dfxdu(izMom,iru) += w;
  dfxdu(izMom,irw) += u;

  dfxdu(iEngy,ir ) += -u*(H - 0.5*(gamma - 1)*V2);
  dfxdu(iEngy,iru) += H - (gamma - 1)*u*u;
  dfxdu(iEngy,irv) +=   - (gamma - 1)*u*v;
  dfxdu(iEngy,irw) +=   - (gamma - 1)*u*w;
  dfxdu(iEngy,irE) += gamma*u;


  dfydu(iCont,irv) += 1;

  dfydu(ixMom,ir ) += -u*v;
  dfydu(ixMom,iru) += v;
  dfydu(ixMom,irv) += u;

  dfydu(iyMom,ir ) += -v*v + 0.5*(gamma - 1)*V2;
  dfydu(iyMom,iru) += -(gamma - 1)*u;
  dfydu(iyMom,irv) += -(gamma - 3)*v;
  dfydu(iyMom,irw) += -(gamma - 1)*w;
  dfydu(iyMom,irE) += gamma - 1;

  dfydu(izMom,ir ) += -v*w;
  dfydu(izMom,irv) += w;
  dfydu(izMom,irw) += v;

  dfydu(iEngy,ir ) += -v*(H - 0.5*(gamma - 1)*V2);
  dfydu(iEngy,iru) +=   - (gamma - 1)*v*u;
  dfydu(iEngy,irv) += H - (gamma - 1)*v*v;
  dfydu(iEngy,irw) +=   - (gamma - 1)*v*w;
  dfydu(iEngy,irE) += gamma*v;


  dfzdu(iCont,irw) += 1;

  dfzdu(ixMom,ir ) += -u*w;
  dfzdu(ixMom,iru) += w;
  dfzdu(ixMom,irw) += u;

  dfzdu(iyMom,ir ) += -v*w;
  dfzdu(iyMom,irv) += w;
  dfzdu(iyMom,irw) += v;

  dfzdu(izMom,ir ) += -w*w + 0.5*(gamma - 1)*V2;
  dfzdu(izMom,iru) += -(gamma - 1)*u;
  dfzdu(izMom,irv) += -(gamma - 1)*v;
  dfzdu(izMom,irw) += -(gamma - 3)*w;
  dfzdu(izMom,irE) += gamma - 1;

  dfzdu(iEngy,ir ) += -w*(H - 0.5*(gamma - 1)*V2);
  dfzdu(iEngy,iru) +=   - (gamma - 1)*w*u;
  dfzdu(iEngy,irv) +=   - (gamma - 1)*w*v;
  dfzdu(iEngy,irw) += H - (gamma - 1)*w*w;
  dfzdu(iEngy,irE) += gamma*w;
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
//
// result evaluated using only acoustic eigenvectors and fact that left/right
// eigenvectors are orthogonal
//
// n.d(F)/d(U) = qn Sum_3 R_k^t L_k + (qn - c) R_m^t L_m + (qn + c) R_p^t L_p
//             = qn I + ((qn - c) - qn) R_m^t L_m + ((qn + c) - qn) R_p^t L_p
//
// where R_i are right eigenvectors and L_i are left eigenvectors (L = R^{-1})
// and R_i L_j = {1 if i=j, 0 else}
//
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& z, const Real& time,
  const ArrayQ<Tq>& q, const Real& Nx, const Real& Ny, const Real& Nz,
  MatrixQ<Tf>& mtx ) const
{
  Real nMag = sqrt(Nx*Nx + Ny*Ny + Nz*Nz);
  Real eps0 = 1.e-9;
  Real nMag2 = smoothabsP(nMag, eps0);

  Real nx = Nx/nMag2;
  Real ny = Ny/nMag2;
  Real nz = Nz/nMag2;

  Tq rho=0, u=0, v=0, w=0, t=0;

  qInterpret_.eval( q, rho, u, v, w, t );
  Tq c = gas_.speedofSound( t );
  Tq h = gas_.enthalpy( rho, t );
  Tq qsq = u*u + v*v + w*w;
  Tq H = h + 0.5*qsq;
  Tq qn = nx*u + ny*v + nz*w;

  Tq eigm = fabs(qn - c)*nMag;
  Tq eigp = fabs(qn + c)*nMag;
  Tq eigu = fabs(qn    )*nMag;

  // right acoustic eigenvectors

  ArrayQ<Tq> rvecm=0, rvecp=0;
  rvecm[0] = 1;
  rvecm[1] = u - nx*c;
  rvecm[2] = v - ny*c;
  rvecm[3] = w - nz*c;
  rvecm[4] = H - qn*c;
  rvecp[0] = 1;
  rvecp[1] = u + nx*c;
  rvecp[2] = v + ny*c;
  rvecp[3] = w + nz*c;
  rvecp[4] = H + qn*c;

  // left acoustic eigenvectors

  ArrayQ<Tq> lvecm=0, lvecp=0;
  lvecm[0] = qsq/(4*h) + qn/(2*c);
  lvecm[1] = -u/(2*h) - nx/(2*c);
  lvecm[2] = -v/(2*h) - ny/(2*c);
  lvecm[3] = -w/(2*h) - nz/(2*c);
  lvecm[4] = 1./(2*h);
  lvecp[0] = qsq/(4*h) - qn/(2*c);
  lvecp[1] = -u/(2*h) + nx/(2*c);
  lvecp[2] = -v/(2*h) + ny/(2*c);
  lvecp[3] = -w/(2*h) + nz/(2*c);
  lvecp[4] = 1./(2*h);

  // absolute value jacobian

  // premultiply delta for performance
  Tq deigm = eigm - eigu;
  Tq deigp = eigp - eigu;

  rvecm *= deigm;
  rvecp *= deigp;

  mtx(0,0) += eigu;
  mtx(1,1) += eigu;
  mtx(2,2) += eigu;
  mtx(3,3) += eigu;
  mtx(4,4) += eigu;
  for (int i = 0; i < 5; i++)
    for (int j = 0; j < 5; j++)
      mtx(i,j) += rvecm[i]*lvecm[j] + rvecp[i]*lvecp[j];
    //mtx(i,j) += (eigm - eigu)*rvecm[i]*lvecm[j] + (eigp - eigu)*rvecp[i]*lvecp[j];

#if 0
  // check eigenvectors orthogonal
  Tq mm = 0, mp = 0, pm = 0, pp = 0;
  for (int i = 0; i < 5; i++)
  {
    mm += rvecm[i]*lvecm[i];
    mp += rvecm[i]*lvecp[i];
    pm += rvecp[i]*lvecm[i];
    pp += rvecp[i]*lvecp[i];
  }
  std::cout << __func__ << ": mm = " << mm << "  mp = " << mp << "  pm = " << pm << "  pp = " << pp << std::endl;
#endif

#if 0
  printf( "rvecm * lvecm\n" );
  for (int i=0; i<5; i++)
  {
    for (int j = 0; j < 5; j++)
      printf( " %13.5e", rvecm[i]*lvecm[j] );
    printf( "\n" );
  }
  printf( "\n" );
  printf( "rvecp * lvecp\n" );
  for (int i=0; i<5; i++)
  {
    for (int j = 0; j < 5; j++)
      printf( " %13.5e", rvecp[i]*lvecp[j] );
    printf( "\n" );
  }
  printf( "\n" );
  printf( "eigu = %13.5e  eigm = %13.5e  eigp = %13.5e\n", eigu, eigm, eigp );
#endif
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& z, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz, const Real& nt,
  MatrixQ<Tf>& mtx ) const
{
  flux_.jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, z, time, q, nx, ny, nz, nt, mtx);
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;
  Tq rho = 0, u = 0, v = 0, w = 0, t = 0, h = 0, H = 0;
  T rhox=0, ux=0, vx=0, wx=0, tx=0, px=0, hx=0, Hx=0;
  T rhoy=0, uy=0, vy=0, wy=0, ty=0, py=0, hy=0, Hy=0;
  T rhoz=0, uz=0, vz=0, wz=0, tz=0, pz=0, hz=0, Hz=0;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx);
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty);
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz);

//  p  = gas_.pressure(rho, t);
  gas_.pressureGradient(rho, t, rhox, tx, px);
  gas_.pressureGradient(rho, t, rhoy, ty, py);
  gas_.pressureGradient(rho, t, rhoz, tz, pz);

  h  = gas_.enthalpy(rho, t);
  gas_.enthalpyGradient(rho, t, rhox, tx, hx);
  gas_.enthalpyGradient(rho, t, rhoy, ty, hy);
  gas_.enthalpyGradient(rho, t, rhoz, tz, hz);

  H  = h  + 0.5*(u*u + v*v + w*w);
  Hx = hx + (ux*u + vx*v + wx*w);
  Hy = hy + (uy*u + vy*v + wy*w);
  Hz = hz + (uz*u + vz*v + wz*w);

  strongPDE(iCont) += rhox*u   +   rho*ux;
  strongPDE(ixMom) += rhox*u*u + 2*rho*ux*u + px;
  strongPDE(iyMom) += rhox*u*v +   rho*ux*v + rho*u*vx;
  strongPDE(izMom) += rhox*u*w +   rho*ux*w + rho*u*wx;
  strongPDE(iEngy) += rhox*u*H +   rho*ux*H + rho*u*Hx;

  strongPDE(iCont) += rhoy*v   +   rho*vy;
  strongPDE(ixMom) += rhoy*v*u +   rho*vy*u + rho*v*uy;
  strongPDE(iyMom) += rhoy*v*v + 2*rho*vy*v + py;
  strongPDE(izMom) += rhoy*v*w +   rho*vy*w + rho*v*wy;
  strongPDE(iEngy) += rhoy*v*H +   rho*vy*H + rho*v*Hy;

  strongPDE(iCont) += rhoz*w   +   rho*wz;
  strongPDE(ixMom) += rhoz*w*u +   rho*wz*u + rho*w*uz;
  strongPDE(iyMom) += rhoz*w*v +   rho*wz*v + rho*w*vz;
  strongPDE(izMom) += rhoz*w*w + 2*rho*wz*w + pz;
  strongPDE(iEngy) += rhoz*w*H +   rho*wz*H + rho*w*Hz;
}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::source(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Ts>& source ) const
{
#if 0
  if (std::is_same<QType, QTypePrimitiveSurrogate>::value)
  {
    const int ir = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::ir;
    const int it = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::it;

    if (q[ir] < 0 || q[it] < 0)
    {
      Tq rho = 0, u = 0, v = 0, w = 0, t = 0;

      qInterpret_.eval( q, rho, u, v, w, t );

      Tq e = gas_.energy(rho, q[it]);
      Tq E = e + 0.5*(u*u + v*v + w*w);

      Tq zero = 0;

      Real alpha = 40;

      Tq rhou = rho*u;
      Tq rhov = rho*v;
      Tq rhow = rho*w;
      Tq rhoE = rho*E;

      source[iCont] += smoothmin(q[ir], zero, alpha);
      if (q[ir] > 0) source[iCont] -= smoothmax(q[ir], zero, alpha);
      else           source[iCont] -= smoothmin(q[ir], zero, alpha);

      if (rhou > 0) source[ixMom] += smoothmax(rhou, zero, alpha);
      else          source[ixMom] += smoothmin(rhou, zero, alpha);
      if (rhov > 0) source[iyMom] += smoothmax(rhov, zero, alpha);
      else          source[iyMom] += smoothmin(rhov, zero, alpha);
      if (rhow > 0) source[izMom] += smoothmax(rhow, zero, alpha);
      else          source[izMom] += smoothmin(rhow, zero, alpha);

      if (rhoE > 0) source[iEngy] -= smoothmax(rhoE, zero, alpha);
      else          source[iEngy] -= smoothmin(rhoE, zero, alpha);
    }
  }
#endif
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::jacobianSource(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdu ) const
{
#if 0
  if (std::is_same<QType, QTypePrimitiveSurrogate>::value)
  {
    const int ir = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::ir;
    const int it = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::it;

    if (q[ir] < 0 || q[it] < 0)
    {
      dsdu(iCont,0) += 1;
      dsdu(ixMom,1) += 1;
      dsdu(iyMom,2) += 1;
      dsdu(izMom,3) += 1;
      dsdu(iEngy,4) += 1;
    }
  }
#endif
}


// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, Real, Real dx, Real dy, Real dz, const ArrayQ<Tq>& q, Tc& speed ) const
{
  Tq rho = 0, u = 0, v = 0, w = 0, t = 0, c = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  c = gas_.speedofSound(t);

  speed = fabs(dx*u + dy*v + dz*w)/sqrt(dx*dx + dy*dy + dz*dz) + c;
}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const
{
  Tq rho = 0, u = 0, v = 0, w = 0, t = 0, c = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  c = gas_.speedofSound(t);

  speed = sqrt(u*u + v*v + w*w) + c;
}


// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::stabMatrix(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
    const Tq& cn1, const Tq& cn2, const Tq& cn3, MatrixQ<Tf>& dfdu) const
{
  //Real gamma = gas_.gamma();
  Tq rho, u, v, w, t = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  Tq e = gas_.energy(rho, t);
  Tq E = e + 0.5*(u*u + v*v + w*w);

  Tq cn = smoothmax(cn1, cn2, 1.);

  dfdu(iCont,0) += rho/cn;
  dfdu(ixMom,0) += rho*u/cn;
  dfdu(iyMom,0) += rho*v/cn;
  dfdu(iyMom,0) += rho*w/cn;
  dfdu(iEngy,0) += rho*E/cn;

}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::getCN(
    const Real& x, const Real& y, const Real&z, const Real& time, const ArrayQ<Tq>& q,
    const Real& nx, const Real& ny, const Real& nz, Tq& cn1, Tq& cn2, Tq& cn3) const
{
  //Real gamma = gas_.gamma();
  Tq rho, u, v, w, t = 0;

  qInterpret_.eval( q, rho, u, v, w, t );

  Tq c = gas_.speedofSound(t);

  Real eps0 = 1.e-2;
  Tq umin = eps0*c;

//  Tq speed = 0;
//  speedCharacteristic(x,y,time,q,speed);

//  Tq eps = 0.01*rho*c*fabs(nx + ny);

//  Tq tmp = fabs(rho*u*nx + rho*v*ny);

  cn1 += fabs(rho*u*nx + rho*v*ny + rho*w*nz);
  cn2 += fabs(rho*sqrt(nx*nx + ny*ny+nz*nz)*umin);
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::
updateFraction( const Real& x, const Real& y, const Real& z, const Real& time,
                const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  qInterpret_.updateFraction(q, dq, maxChangeFraction, updateFraction);
}

// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEEuler3D<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return qInterpret_.isValidState(q);
}

// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpret_.setFromPrimitive( q, data, name, nn );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Variables>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const NSVariableType3D<Variables>& data ) const
{
  qInterpret_.setFromPrimitive( q, data.cast() );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Variables>
inline typename PDEEuler3D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEEuler3D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const NSVariableType3D<Variables>& data ) const
{
  ArrayQ<Real> q;
  qInterpret_.setFromPrimitive( q, data.cast() );
  return q;
}




// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEEuler3D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEEuler3D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q;
  DictKeyPair data = d.get(NSVariableType3DParams::params.StateVector);

  if ( data == NSVariableType3DParams::params.StateVector.Conservative )
    qInterpret_.setFromPrimitive( q, Conservative3D<Real>(data) );
  else if ( data == NSVariableType3DParams::params.StateVector.PrimitivePressure )
    qInterpret_.setFromPrimitive( q, DensityVelocityPressure3D<Real>(data) );
  else if ( data == NSVariableType3DParams::params.StateVector.PrimitiveTemperature )
    qInterpret_.setFromPrimitive( q, DensityVelocityTemperature3D<Real>(data) );
  else
    SANS_DEVELOPER_EXCEPTION(" Missing state vector option ");

  return q;
}


// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{

  int nOut = rsdPDEOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N-2);
    rsdPDEOut[0] = rsdPDEIn[iCont];
    rsdPDEOut[1] = sqrt( rsdPDEIn[ixMom]*rsdPDEIn[ixMom] + rsdPDEIn[iyMom]*rsdPDEIn[iyMom] + rsdPDEIn[izMom]*rsdPDEIn[izMom] );
    rsdPDEOut[2] = rsdPDEIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdPDEOut[2+i] = rsdPDEIn[iEngy+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
//    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 3D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now

    Real g = gas_.gamma();
    Real pRef = 1; Real rhoRef = 1; Real uRef = sqrt(g);
    Real rhoE = pRef/(g-1) + 0.5*rhoRef*(uRef*uRef);

    ArrayQ<Real> Vref = 0;
    Vref[0] = (g+1)/(g-1) - rhoE/pRef;
    Vref[1] = rhoRef*uRef/pRef;
    Vref[2] = rhoRef*uRef/pRef;
    Vref[3] = rhoRef*uRef/pRef;
    Vref[4] = -rhoRef/pRef;

    T tmp = 0;
    tmp += Vref[0]*Vref[0]*rsdPDEIn[0]*rsdPDEIn[0];
    tmp += Vref[1]*Vref[1]*rsdPDEIn[1]*rsdPDEIn[1];
    tmp += Vref[2]*Vref[2]*rsdPDEIn[2]*rsdPDEIn[2];
    tmp += Vref[3]*Vref[3]*rsdPDEIn[3]*rsdPDEIn[3];
    tmp += Vref[4]*Vref[4]*rsdPDEIn[4]*rsdPDEIn[4];

    rsdPDEOut[0] = sqrt(tmp);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
    {
      rsdAuxOut[i] = rsdAuxIn[i];
    }
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N-2);
    rsdAuxOut[0] = rsdAuxIn[iCont];
    rsdAuxOut[1] = sqrt(rsdAuxIn[ixMom]*rsdAuxIn[ixMom] + rsdAuxIn[iyMom]*rsdAuxIn[iyMom] + rsdAuxIn[izMom]*rsdAuxIn[izMom]);
    rsdAuxOut[2] = rsdAuxIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdAuxOut[2+i] = rsdAuxIn[iEngy+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);

    Real g = gas_.gamma();
    Real pRef = 1; Real rhoRef = 1; Real uRef = sqrt(g);
    Real rhoE = pRef/(g-1) + 0.5*rhoRef*(uRef*uRef);

    ArrayQ<Real> Vref = 0;
    Vref[0] = (g+1)/(g-1) - rhoE/pRef;
    Vref[1] = rhoRef*uRef/pRef;
    Vref[2] = rhoRef*uRef/pRef;
    Vref[3] = rhoRef*uRef/pRef;
    Vref[4] = -rhoRef/pRef;

    T tmp = 0;
    tmp += Vref[0]*Vref[0]*rsdAuxIn[0]*rsdAuxIn[0];
    tmp += Vref[1]*Vref[1]*rsdAuxIn[1]*rsdAuxIn[1];
    tmp += Vref[2]*Vref[2]*rsdAuxIn[2]*rsdAuxIn[2];
    tmp += Vref[3]*Vref[3]*rsdAuxIn[3]*rsdAuxIn[3];
    tmp += Vref[4]*Vref[4]*rsdAuxIn[4]*rsdAuxIn[4];

    rsdAuxOut[0] = sqrt(tmp);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N-2);
    rsdBCOut[0] = rsdBCIn[iCont];
    rsdBCOut[1] = sqrt( rsdBCIn[ixMom]*rsdBCIn[ixMom] + rsdBCIn[iyMom]*rsdBCIn[iyMom]  + rsdBCIn[izMom]*rsdBCIn[izMom] );
    rsdBCOut[2] = rsdBCIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdBCOut[2+i] = rsdBCIn[iEngy+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);

    Real g = gas_.gamma();
    Real pRef = 1; Real rhoRef = 1; Real uRef = sqrt(g);
    Real rhoE = pRef/(g-1) + 0.5*rhoRef*(uRef*uRef);

    ArrayQ<Real> Vref = 0;
    Vref[0] = (g+1)/(g-1) - rhoE/pRef;
    Vref[1] = rhoRef*uRef/pRef;
    Vref[2] = rhoRef*uRef/pRef;
    Vref[3] = rhoRef*uRef/pRef;
    Vref[4] = -rhoRef/pRef;

    T tmp = 0;
    tmp += Vref[0]*Vref[0]*rsdBCIn[0]*rsdBCIn[0];
    tmp += Vref[1]*Vref[1]*rsdBCIn[1]*rsdBCIn[1];
    tmp += Vref[2]*Vref[2]*rsdBCIn[2]*rsdBCIn[2];
    tmp += Vref[3]*Vref[3]*rsdBCIn[3]*rsdBCIn[3];
    tmp += Vref[4]*Vref[4]*rsdBCIn[4]*rsdBCIn[4];

    rsdBCOut[0] = sqrt(tmp);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// return the interp type
template <template <class> class PDETraitsSize, class PDETraitsModel>
EulerResidualInterpCategory
PDEEuler3D<PDETraitsSize, PDETraitsModel>::category() const
{
  return cat_;
}


// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDEEuler3D<PDETraitsSize, PDETraitsModel>::nMonitor() const
{
  int nMon;

  if (cat_==Euler_ResidInterp_Raw) // raw residual
    nMon = N;
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
    nMon = N-2;
  else if (cat_ == Euler_ResidInterp_Entropy)
    nMon = 1;
  else
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );

  return nMon;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDEEuler3D<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  return {
          "mach",
          "streamwiseVorticity"
         };
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Real& x, const Real& y, const Real& z, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, Tf& J ) const
{
  switch (index)
  {
    case 0:
    {
      // std::cout << "mach number" << std::endl;

      Tq rho, u, v, w, t, c, V;
      qInterpret_.eval( q, rho, u, v, w, t );

      c = gas_.speedofSound( t );
      V = sqrt( u*u + v*v + w*w );
      J = V/c;
      break;
    }

    case 1:
    {
      Tq rho, u, v, w, t;
      Tq rhox, ux, vx, wx, tx;
      Tq rhoy, uy, vy, wy, ty;
      Tq rhoz, uz, vz, wz, tz;
      qInterpret_.eval( q, rho, u, v, w, t );
      qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
      qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
      qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

      Tq vortx, vorty, vortz;
      vortx = (wy-vz);
      vorty = (uz-wx);
      vortz = (vx-uy);

      // Velocity dot vorticity
      J = u*vortx + v*vorty + z*vortz;

      break;
    }

    default:
    {
      std::cout << "index: " << index << std::endl;
      SANS_DEVELOPER_EXCEPTION("PDEEuler3D::derivedQuantity - Invalid index!");
    }
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEEuler3D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEEuler3D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDEEuler3D<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDEEuler3D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // PDEEULER3D_H
