// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER3D_SOLUTION_H
#define BCEULER3D_SOLUTION_H

#include <memory> // shared_ptr
#include <vector>

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "pde/NS/SolutionFunction_NavierStokes3D.h"
#include "pde/NS/SolutionFunction_RANSSA3D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Struct for creating a BC solution function

struct BCEulerSolution3DOptions : noncopyable
{
  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Solution function used on the BC" };
    const ParameterString& key = Name;

    const DictOption TaylorGreen{"TaylorGreen", SolutionFunction_NavierStokes3D_TaylorGreen_Params::checkInputs};
    const DictOption OjedaMMS{"OjedaMMS", SolutionFunction_NavierStokes3D_OjedaMMS_Params::checkInputs};

    const std::vector<DictOption> options{ TaylorGreen, OjedaMMS };
  };
  const ParameterOption<SolutionOptions> Function{"Function", NO_DEFAULT, "The BC is a solution function"};

};

template<template <class> class PDETraitsSize>
struct BCEulerSolution3DParams : public BCEulerSolution3DOptions
{
};


// Struct for creating the solution pointer
template <template <class> class PDETraitsSize, class PDETraitsModel>
struct BCEulerSolution3D
{
  typedef PhysD3 PhysDim;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  typedef Function3DBase<ArrayQ<Real>> Function3DBaseType;
  typedef std::shared_ptr<Function3DBaseType> Function_ptr;

  static Function_ptr getSolution(const PyDict& d);
};

} // namespace SANS

#endif // BCEULER2D_SOLUTION_H
