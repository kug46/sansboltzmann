// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDERANSSA_SOURCE3D_H
#define PDERANSSA_SOURCE3D_H

#include "PDERANSSA3D.h"

namespace SANS
{

#if 1
template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDERANSSA_Source3D : public PDERANSSA3D<PDETraitsSize, PDETraitsModel>
{
  friend class PDERANSSA3D<PDETraitsSize, PDETraitsModel>;
public:
  typedef PhysD3 PhysDim;

  typedef PDERANSSA3D<PDETraitsSize, PDETraitsModel> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;                                        // cartesian vectors

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef typename PDETraitsModel::QType QType;
  typedef QRANSSA3D<QType, PDETraitsSize> QInterpret;                                 // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                                 // gas model

  typedef typename PDETraitsModel::ViscosityModel ViscosityModel;                     // molecular viscosity model

  typedef typename PDETraitsModel::ThermalConductivityModel ThermalConductivityModel; // thermal conductivity model

  // Solution pointer rather than MMS pointer due to distance function
  typedef ForcingFunctionBase3D< PDENavierStokes3D<PDETraitsSize, PDETraitsModel> > ForcingFunctionType;

  using BaseType::ixMom;
  using BaseType::iyMom;
  using BaseType::izMom;
  using BaseType::iSA;

  PDERANSSA_Source3D( const GasModel& gas, const ViscosityModel& visc,
         const ThermalConductivityModel& tcond,
         const bool userSource = false,
         const EulerResidualInterpCategory cat = Euler_ResidInterp_Raw,
         const RoeEntropyFix entropyFix = eVanLeer,
         const std::shared_ptr<ForcingFunctionType>& soln = nullptr) :
      BaseType( gas, visc, tcond, cat, entropyFix, nullptr ),
      userSource_(userSource),
      soln_(soln),
      qInterpret_(gas)
  {
    BaseType::setCoeffients();
  }

  template <class Tq, class Tg, class Ts>
  void source(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ< Ts >& source ) const
  {
    typedef typename promote_Surreal<Tq,Tg>::type T;

    if (userSource_ == true)
    {

      Tq rho=0, u=0, v=0, w=0, t=0, nt=0;

      qInterpret_.eval( q, rho, u, v, w, t );
      qInterpret_.evalSA( q, nt );

      Real dx = 0.161478224451580;
      Real dy = 0.118003317868462;

      Real x0 = 0.79;
      Real y0 = 0.6407355 - 0.005;

      if (x < x0) {}
      else if (x < (x0+dx) ) {y0 -= dy;}
      else if (x < (x0+2*dx) ) {y0 -= 2*dy;}
      else if (x < (x0+3*dx) ) {y0 -= 3*dy;}
      else if (x < (x0+4*dx) ) {y0 -= 4*dy;}
      else if (x < (x0+5*dx) ) {y0 -= 5*dy;}
      else                     {y0 -= 6*dy;}

      Real alpha = 200000.0;

      Real Gauss = sqrt(alpha/PI)*exp( -(y-y0)*(y-y0)*alpha );

      Real Kn = 0.3;
      Real eta = sqrt(Kn*Kn*0.25*0.25+1) - Kn*0.25;
      Real Kt = 1./eta - 1.;

      T dxMom = Kt*0.5*rho*u*fabs(v)*Gauss;
      T dyMom = Kn*rho*v*fabs(v)*Gauss;
      T dzMom = Kt*0.5*rho*w*fabs(v)*Gauss;
      T dSA = 0.99*Gauss*rho*fabs(v)*nt;

      // Momentum Sink Here
      source(ixMom) += dxMom;
      source(iyMom) += dyMom;
      source(izMom) += dzMom;
      source(iSA) += dSA;

    }

    BaseType::source(dist, x, y, z, time, q, qx, qy, qz, source);


  }

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;
    ArrayQ<Tgg> qqy = qy + qpy;
    ArrayQ<Tgg> qqz = qz + qpz;

    source(dist, x, y, z, time, qq, qqx, qqy, qqz, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& s ) const
  {
    source(dist, x, y, z, time, q, qx, qy, qz, s); //forward to regular source
  }


protected:
  bool userSource_;

  const std::shared_ptr<ForcingFunctionType> soln_;
  const QInterpret qInterpret_;         // solution variable interpreter class

  using BaseType::gas_;
  using BaseType::visc_;
  using BaseType::tcond_;
  using BaseType::flux_;




};
#endif

} //SANS
#endif /* PDERANSSA_SOURCE3D_H */
