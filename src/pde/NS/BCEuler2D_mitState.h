// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER2D_MITSTATE_H
#define BCEULER2D_MITSTATE_H

// 2-D Euler BC class with mitState formulations

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsEuler.h"
#include "PDEEuler2D.h"
#include "BCEuler2D_Solution.h"
#include "pde/NS/SolutionFunction_Euler2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>
#include <memory> //shared_ptr

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeTimeOut;
class BCTypeTimeIC;
class BCTypeTimeIC_Function;
class BCTypeFullState_mitState;
class BCTypeSymmetry_mitState;
class BCTypeInflowSubsonic_PtTta_mitState;
class BCTypeInflowSubsonic_sHqt_mitState;
class BCTypeOutflowSubsonic_Pressure_mitState;
class BCTypeOutflowSupersonic_Pressure_mitState;
class BCTypeFunction_mitState;
class BCTypeFunctionInflow_sHqt_mitState;
class BCTypeFunctionInflow_sHqt_BN;
class BCTypeReflect_mitState;
class BCTypeFullStateRadialInflow_mitState;

template <class BCType, class PDEEuler2D>
class BCEuler2D;

template <class BCType>
struct BCEuler2DParams;

//----------------------------------------------------------------------------//
// Complete the integration by parts on the time boundary only
//
//----------------------------------------------------------------------------//
template<>
struct BCEuler2DParams<BCTypeTimeOut> : noncopyable
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"TimeOut"};
  struct Option
  {
    const DictOption TimeOut{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static BCEuler2DParams params;
};

template <class PDE_>
class BCEuler2D<BCTypeTimeOut, PDE_> :
    public BCType< BCEuler2D<BCTypeTimeOut, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeTimeOut BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCTypeTimeOut>  ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 0;                         // No BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde ) :
      pde_(pde), qInterpret_(pde_.variableInterpreter())
  {
  }

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), qInterpret_(pde_.variableInterpreter())
  {
  }

  ~BCEuler2D() {}

  BCEuler2D( const BCEuler2D& ) = delete;
  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qI;
  }

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    ArrayQ<Tf> ft = 0;
    pde_.fluxAdvectiveTime(x, y, time, qI, ft);

    Fn += ft*nt;
  }

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    ArrayQ<Tf> ft = 0;
    pde_.fluxAdvectiveTime(x, y, time, qI, ft);

    Fn += ft*nt;
  }

  bool isValidState( const Real& nx, const Real& ny, const Real& nt, const ArrayQ<Real>& qI ) const
  {
    Real uI, vI;
    qInterpret_.evalVelocity( qI, uI, vI );
    // None requires outflow
    //return (uI*nx + vI*ny + nt) > 0;
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class PDE>
void
BCEuler2D<BCTypeTimeOut, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeTimeOut, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeTimeOut, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// SpaceTime IC BC:
//----------------------------------------------------------------------------//
template<class VariableType2DParams>
struct BCEuler2DTimeICParams : noncopyable
{
  BCEuler2DTimeICParams();

  const ParameterOption<typename VariableType2DParams::VariableOptions>& StateVector;

  static constexpr const char* BCName{"TimeIC"};
  struct Option
  {
    const DictOption TimeIC{BCEuler2DTimeICParams::BCName, BCEuler2DTimeICParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DTimeICParams params;
};



template <class PDE_>
class BCEuler2D<BCTypeTimeIC, PDE_> :
    public BCType< BCEuler2D<BCTypeTimeIC, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCTypeTimeIC BCType;
  typedef BCEuler2DTimeICParams<typename PDE_::VariableType2DParams> ParamsType;
  typedef PDE_ PDE;

  typedef PhysD2 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = N;         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Varibles>
  BCEuler2D( const PDE& pde, const NSVariableType2D<Varibles>& data ) :
      pde_(pde),
      qB_(pde.setDOFFrom(data))
  {
    // Nothing
  }

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qB_(pde.setDOFFrom(d))
  {
    // Nothing
  }

  virtual ~BCEuler2D() {}

  BCEuler2D( const BCEuler2D& ) = delete;
  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& y, const Real& time,
                            const Real& nx, const Real& ny, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, y, time, qB, uB);
    Fn += nt*uB;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& y, const Real& time,
                            const Real& nx, const Real& ny, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, y, time, qB, uB);
    Fn += nt*uB;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nt, const ArrayQ<Real>& q ) const
  {
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout )
  {
    std::string indent(indentSize, ' ');
    out << indent <<
        "BCEuler2D<BCTypeTimeIC, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
    out << indent <<
        "BCEuler2D<BCTypeTimeIC, PDE>: qB_ = " << std::endl;
    qB_.dump(indentSize+2, out);
  }

protected:
  const PDE& pde_;
  const ArrayQ<Real> qB_;
};

//----------------------------------------------------------------------------//
// SpaceTime IC BC with function:
//----------------------------------------------------------------------------//
template<template <class> class PDETraitsSize>
struct BCEuler2DTimeIC_FunctionParams : BCEulerSolution2DParams<PDETraitsSize>
{
  static constexpr const char* BCName{"TimeIC_Function"};
  struct Option
  {
    const DictOption TimeIC_Function{BCEuler2DTimeIC_FunctionParams::BCName, BCEuler2DTimeIC_FunctionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DTimeIC_FunctionParams params;
};



template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler2D<BCTypeTimeIC_Function, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeTimeIC_Function, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCEuler2DTimeIC_FunctionParams<PDETraitsSize> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  typedef PhysD2 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = 4;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    uexact_( BCEulerSolution2D<PDETraitsSize, PDETraitsModel>::getSolution(d) ),
    pde_(pde)
  {
    // Nothing
  }

  BCEuler2D(const PDE& pde, const Function_ptr& uexact ) :
    uexact_(uexact),
    pde_(pde)
  {
    // Nothing
  }

  virtual ~BCEuler2D() {}

  BCEuler2D& operator=( const BCEuler2D& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx,const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*uexact_)(x, y, time);
  }

  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& y, const Real& time,
                            const Real& nx,const Real& ny, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, y, time, qB, uB);
    Fn += nt*uB;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& y, const Real& time,
                            const Real& nx, const Real& ny, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(param, x, y, time, qB, uB);
    Fn += nt*uB;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nt, const ArrayQ<Real>& q ) const
  {
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout )
  {
    std::string indent(indentSize, ' ');
    out << indent <<
        "BCEuler2D<BCTypeTimeIC_Function, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
  }

protected:
  const Function_ptr uexact_;
  const PDE& pde_;
};


//----------------------------------------------------------------------------//
// Conventional Full State Imposed (possibly with characteristics)
//
//----------------------------------------------------------------------------//

template<class VariableType2DParams>
struct BCEuler2DFullStateParams : noncopyable
{
  BCEuler2DFullStateParams();

  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};
  const ParameterOption<typename VariableType2DParams::VariableOptions>& StateVector;

  static constexpr const char* BCName{"FullState"};
  struct Option
  {
    const DictOption FullState{BCEuler2DFullStateParams::BCName, BCEuler2DFullStateParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DFullStateParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeFullState_mitState, PDE_> :
    public BCType< BCEuler2D<BCTypeFullState_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeFullState_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DFullStateParams<typename PDE_::VariableType2DParams> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Varibles>
  BCEuler2D( const PDE& pde, const NSVariableType2D<Varibles>& data, const bool& upwind ) :
      pde_(pde), qInterpret_(pde_.variableInterpreter()),
      qB_(pde.setDOFFrom(data)),
      upwind_(upwind)
  {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), qInterpret_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(d)),
    upwind_(d.get(ParamsType::params.Characteristic))
  {}

  ~BCEuler2D() {}

  BCEuler2D( const BCEuler2D& ) = delete;
  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if ( upwind_ )
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind( x, y, time, qI, qB, nx, ny, Fn );
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<Tf> fx = 0, fy = 0;
      pde_.fluxAdvective(x, y, time, qB, fx, fy);

      Fn += fx*nx + fy*ny;
    }
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if ( upwind_ )
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn );
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<Tf> fx = 0, fy = 0;
      pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

      Fn += fx*nx + fy*ny;
    }
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const
  {
    if (upwind_)
    {
      // Characteristic can be either inflow or outflow
      return true;
    }
    else
    {
      Real uI, vI;
      qInterpret_.evalVelocity( qI, uI, vI );

      // Full state requires inflow, so the dot product between the normal and velocity should be negative
      return (uI*nx + vI*ny) < 0;
    }
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCEuler2D<BCTypeFullState_mitState, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
    out << indent << "BCEuler2D<BCTypeFullState_mitState, PDE>: qInterpret_ = " << std::endl;
    qInterpret_.dump(indentSize+2, out);
  }

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> qB_;
  const bool upwind_;
};


//----------------------------------------------------------------------------//
// Conventional PX-style slipwall BC:
//
// specify: normal velocity
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeSymmetry_mitState> : noncopyable
{
  static constexpr const char* BCName{"Symmetry_mitState"};
  struct Option
  {
    const DictOption Symmetry_mitState{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeSymmetry_mitState, PDE_> :
    public BCType< BCEuler2D<BCTypeSymmetry_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeSymmetry_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    T rhoI=0, uI=0, vI=0, tI=0;

    qInterpret_.eval( qI, rhoI, uI, vI, tI );
    T pI = gas_.pressure(rhoI, tI);

    // subtract off the normal velocity component such that U.n == 0
    T uB = uI - nx*(uI*nx + vI*ny);
    T vB = vI - ny*(uI*nx + vI*ny);

    qInterpret_.setFromPrimitive( qB, DensityVelocityPressure2D<T>(rhoI, uB, vB, pI) );
  }

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    ArrayQ<Tf> fx = 0, fy = 0;
    pde_.fluxAdvective(x, y, time, qB, fx, fy);

    Fn += fx*nx + fy*ny;
  }

  // normal BC flux
  template <class T, class L, class Tf>
  void fluxNormal( const L& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    ArrayQ<Tf> fx = 0, fy = 0;
    pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

    Fn += fx*nx + fy*ny;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const
  {
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCEuler2D<BCTypeSymmetry_mitState, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
    out << indent << "BCEuler2D<BCTypeSymmetry_mitState, PDE>: qInterpret_ = " << std::endl;
    qInterpret_.dump(indentSize+2, out);
    out << indent << "BCEuler2D<BCTypeSymmetry_mitState, PDE>: gas_ = " << std::endl;
    gas_.dump(indentSize+2, out);
  }

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


//----------------------------------------------------------------------------//
// Conventional PX style subsonic inflow BC:
//
// specify: Stagnation Temperature, stagnation pressure, inlet flow angle to normal
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState> : noncopyable
{
  const ParameterNumeric<Real> TtSpec{"TtSpec", NO_DEFAULT, NO_RANGE, "Stagnation Temperature"};
  const ParameterNumeric<Real> PtSpec{"PtSpec", NO_DEFAULT, NO_RANGE, "Stagnation Pressure"};
  const ParameterNumeric<Real> aSpec{"aSpec", NO_DEFAULT, NO_RANGE, "Flow Angle relative to positive x-axis, evaluated CCW"};

  static constexpr const char* BCName{"InflowSubsonic_PtTta_mitState"};
  struct Option
  {
    const DictOption InflowSubsonic_PtTta_mitState{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE_> :
    public BCType< BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeInflowSubsonic_PtTta_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCTypeInflowSubsonic_PtTta_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Real& TtSpec, const Real& PtSpec, const Real& aSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      TtSpec_(TtSpec), PtSpec_(PtSpec), aSpec_(aSpec),
      cosa_(cos(aSpec_)), sina_(sin(aSpec_)) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    TtSpec_(d.get(ParamsType::params.TtSpec)),
    PtSpec_(d.get(ParamsType::params.PtSpec)),
    aSpec_(d.get(ParamsType::params.aSpec)),
    cosa_(cos(aSpec_)), sina_(sin(aSpec_)) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:

  // Computes the boundary mach number
  template<class T>
  bool MachB(const Real& nx, const Real& ny, const ArrayQ<T>& qI, T& M) const;

  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real TtSpec_;
  const Real PtSpec_;
  const Real aSpec_;
  const Real cosa_;
  const Real sina_;
};

template <class PDE>
template <class T>
inline bool
BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
MachB( const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, T& M ) const
{

  T rho=0, u=0, v=0, t=0;

  //preserve outgoing Riemann Invariant Jp(q,n) - after Fidkowski 2004
  qInterpret_.eval( qI, rho, u, v, t );

  Real R     = gas_.R();
  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  T c = gas_.speedofSound(t);
  T Jp = u*nx + v*ny + 2*c/gmi;

  // u*n[0] + v*n[1] = c*M*( cos(alpha)*n[0] + sin(alpha)*n[1] ) = c*M*ndir
  Real dn = cosa_*nx + sina_*ny;

  // maximum possible J_plus for real M roots
  Real Jpmax = sqrt(2*(2+gmi*dn*dn)*gamma*R*TtSpec_)/gmi;

  if ( fabs(Jp) > Jpmax )
  {
    // Mach from quadratic equation with Jpmax instead of Jp; note only one root
    M = -dn;
    return true;
  }

  T A =    gamma*R*TtSpec_*dn*dn - gmi/2*Jp*Jp;
  T B = 4.*gamma*R*TtSpec_*dn / gmi;
  T C = 4.*gamma*R*TtSpec_/(gmi*gmi) - Jp*Jp;
  T D = B*B - 4*A*C;

  // Check that we have a valid state
  if ( D < 0 ) return false;

  T Mplus  = (-B + sqrt(D))/(2.*A);
  T Mminus = (-B - sqrt(D))/(2.*A);

  // Pick physically relevant solution
  if ((Mplus >= 0.0) && (Mminus <= 0.0))
  {
    M = Mplus;
  }
  else if ((Mplus <= 0.0) && (Mminus >= 0.0))
  {
    M = Mminus;
  }
  else if ((Mplus > 0.0) && (Mminus > 0.0))
  {
    // not clear which is physical...
    // return invalid state
    return false;
#if 0
    // pick smaller for now
    if ( Mminus <= Mplus )
      M = Mminus;
    else
      M = Mplus;
#endif
  }
  else
  {
    // negative Mach means reverse flow and is OK; choose smallest Mach
    if (fabs(Mminus) < fabs(Mplus))
      M = Mminus;
    else
      M = Mplus;
  }

  // the state is valid
  return true;
}


template <class PDE>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T M = 0;
  MachB( nx, ny, qI, M );

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  // set boundary state
  T tB   = TtSpec_/(1.+0.5*gmi*M*M);
  T pB   = PtSpec_/pow( 1.+0.5*gmi*M*M, gamma/gmi );
  T rhoB = gas_.density(pB, tB);

  T cB = gas_.speedofSound(tB);
  T VB = cB*M;
  T uB = VB*cosa_;
  T vB = VB*sina_;

  qInterpret_.setFromPrimitive( qB, DensityVelocityPressure2D<T>( rhoB, uB, vB, pB ) );
}


template <class PDE>
template <class Tp, class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
state( const Tp& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;
}

// is the boundary state valid
template <class PDE>
bool
BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::
isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q) const
{
  Real M;
  return MachB( nx, ny, q, M );
}


template <class PDE>
void
BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_PtTta_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// Conventional PX style subsonic inflow BC:
//
// specify: entropy, stagnation enthalpy, vertical velocity
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState> : noncopyable
{
  const ParameterNumeric<Real> sSpec{"sSpec", NO_DEFAULT, NO_RANGE, "Entropy"};
  const ParameterNumeric<Real> HSpec{"HSpec", NO_DEFAULT, 0, NO_LIMIT, "Stagnation Enthalpy"};
  const ParameterNumeric<Real> VxSpec{"VxSpec", NO_DEFAULT, NO_RANGE, "Reference x-velocity"};
  const ParameterNumeric<Real> VySpec{"VySpec", NO_DEFAULT, NO_RANGE, "Reference y-velocity"};

  static constexpr const char* BCName{"InflowSubsonic_sHqt_mitState"};
  struct Option
  {
    const DictOption InflowSubsonic_sHqt_mitState{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE_> :
    public BCType< BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeInflowSubsonic_sHqt_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCTypeInflowSubsonic_sHqt_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Real& sSpec, const Real& HSpec,
             const Real& VxSpec, const Real& VySpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      sSpec_(sSpec), HSpec_(HSpec), VxSpec_(VxSpec), VySpec_(VySpec),
      exps_( exp( sSpec_ ) ) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    sSpec_(d.get(ParamsType::params.sSpec)),
    HSpec_(d.get(ParamsType::params.HSpec)),
    VxSpec_(d.get(ParamsType::params.VxSpec)),
    VySpec_(d.get(ParamsType::params.VySpec)),
    exps_( exp( sSpec_ ) ) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:

  // Computes the boundary mach number
  template<class T>
  bool VelB(const Real& nx, const Real& ny, const ArrayQ<T>& qI, T& M) const;

  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real sSpec_;
  const Real HSpec_;
  const Real VxSpec_;
  const Real VySpec_;
  const Real exps_;

};

template <class PDE>
template <class T>
inline bool
BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
VelB( const Real& nx, const Real& ny,
      const ArrayQ<T>& qI, T& Vn ) const
{
  T rho=0, u=0, v=0, t=0;

  //preserve outgoing Riemann Invariant Jp(q,n) - after Fidkowski 2004
  qInterpret_.eval( qI, rho, u, v, t );

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // Specified tangential velocity component
  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

  T c = gas_.speedofSound(t);
  T Jp = u*nx + v*ny + 2*c/gmi;

  Real A = 1 + 2.0/gmi;
  T    B = -2*Jp;
  T    C = Jp*Jp - 4*HSpec_/gmi + 2/gmi*vtSpec*vtSpec;
  T    D = B*B-4*A*C;

  // Check that we have a valid state
  if ( D < 0 ) return false;

  T Vnplus  = (-B + sqrt(D))/(2.*A);
  T Vnminus = (-B - sqrt(D))/(2.*A);

  // Pick physically relevant solution
  if ((Vnplus <= 0.0) && (Vnminus >= 0.0))
  {
    Vn = Vnplus;
  }
  else if ((Vnplus >= 0.0) && (Vnminus <= 0.0))
  {
    Vn = Vnminus;
  }
  else if ((Vnplus < 0.0) && (Vnminus < 0.0))
  {
    // not clear which is physical...
    // return invalid state
    return false;
#if 0
    // pick smaller for now
    if ( fabs(Vnminus) <= fabs(Vnplus) )
      Vn = Vnminus;
    else
      Vn = Vnplus;
#endif
  }
  else
  {
    // positive Vn means reverse flow and is OK; choose smallest Vn
    if (Vnminus < Vnplus)
      Vn = Vnminus;
    else
      Vn = Vnplus;
  }

  // the state is valid
  return true;
}


template <class PDE>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T Vn = 0;
  VelB( nx, ny, qI, Vn );

  Real R     = gas_.R();
  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // Specified tangential velocity component
  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

  // set boundary state
  T uB   = Vn*nx + vtSpec*tx;
  T vB   = Vn*ny + vtSpec*ty;
  T tB   = (HSpec_ - 0.5*(uB*uB + vB*vB))/gas_.Cp();
  T rhoB = pow(R*tB/exps_, 1./gmi);

  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature2D<T>( rhoB, uB, vB, tB ) );
}


template <class PDE>
template <class Tp, class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
state( const Tp& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;
}


// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;
}

// is the boundary state valid
template <class PDE>
bool
BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q) const
{
  Real Vn = 0;
  return VelB( nx, ny, q, Vn );
}


template <class PDE>
void
BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}



//----------------------------------------------------------------------------//
// Conventional PX Style subsonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSubsonic_Pressure_mitState"};
  struct Option
  {
    const DictOption OutflowSubsonic_Pressure_mitState{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE_> :
    public BCType< BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeOutflowSubsonic_Pressure_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Real pSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      pSpec_(pSpec) {}
  ~BCEuler2D() {}

  BCEuler2D( const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    pSpec_(d.get(ParamsType::params.pSpec) ) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class Tp>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <int Dim, class T, class Tf>
  void fluxNormal( const DLA::MatrixSymS<Dim,Real>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <int Dim, class T, class Tf>
  void fluxNormal( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real pSpec_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{

  // set new boundary state

  // compute Riemann Invariant Jm(q,n) - after Fidkowski 2004

  Real gamma = gas_.gamma();
  Real R     = gas_.R();
  Real gmi   = gamma - 1;

  T rhoI=0, uI=0, vI=0, tI=0;
  qInterpret_.eval( qI, rhoI, uI, vI, tI );
  T pI = gas_.pressure(rhoI, tI);
  T sI = pI / pow(rhoI, gamma);
  T cI = gas_.speedofSound(tI);
  T Jp = uI*nx + vI*ny + 2*cI/gmi;

  T rhoB = pow( (pSpec_/sI), (1./gamma));

  // TODO: This assumies ideal gas
  T tB = pSpec_ / (rhoB*R);

  T cB = gas_.speedofSound(tB);

  // Normal and tangential velocities
  T vnB = Jp - 2*cB / gmi;
  T vsB = vI*nx - uI*ny;

  // Cartesian velocities
  T uB = nx*vnB - ny*vsB;
  T vB = ny*vnB + nx*vsB;

  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature2D<T>( rhoB, uB, vB, tB ) );
}


template <class PDE>
template <class T, class Tp>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
state( const Tp& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;
}


// normal BC flux
template <class PDE>
template <int Dim, class T, class Tf>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
fluxNormal( const DLA::MatrixSymS<Dim,Real>& param,
            const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;
}


// normal BC flux
template <class PDE>
template <int Dim, class T, class Tf>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
fluxNormal( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
            const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(param.left(), x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;
}

template <class PDE>
void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler2D<BCTypeOutflowSubsonic_Pressure_mitState, PDE>: pSpec_ = " << pSpec_ << std::endl;
}

//----------------------------------------------------------------------------//
// Conventional PX Style supersonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSupersonic_Pressure_mitState"};
  struct Option
  {
    const DictOption OutflowSupersonic_Pressure_mitState{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE_> :
    public BCType< BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeOutflowSupersonic_Pressure_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCTypeOutflowSupersonic_Pressure_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Real pSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      pSpec_(pSpec) {}
  ~BCEuler2D() {}

  BCEuler2D( const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    pSpec_(d.get(ParamsType::params.pSpec) ) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real pSpec_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{

  // set new boundary state

  // compute Riemann Invariant Jm(q,n) - after Fidkowski 2004

  Real gamma = gas_.gamma();
  Real R     = gas_.R();
  Real gmi   = gamma - 1;

  T rhoI=0, uI=0, vI=0, tI=0;
  qInterpret_.eval( qI, rhoI, uI, vI, tI );
  T pI = gas_.pressure(rhoI, tI);
  T sI = pI / pow(rhoI, gamma);
  T cI = gas_.speedofSound(tI);
  T Jp = uI*nx + vI*ny + 2*cI/gmi;

  T rhoB = pow( (pSpec_/sI), (1./gamma));

  // TODO: This assumies ideal gas
  T tB = pSpec_ / (rhoB*R);

  T cB = gas_.speedofSound(tB);

  // Normal and tangential velocities
  T vnB = Jp - 2*cB / gmi;
  T vsB = vI*nx - uI*ny;

  // Cartesian velocities
  T uB = nx*vnB - ny*vsB;
  T vB = ny*vnB + nx*vsB;

  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature2D<T>( rhoB, uB, vB, tB ) );
}


template <class PDE>
template <class Tp, class T>
inline void
BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
state( const Tp& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> f = 0;
  pde_.fluxAdvectiveUpwind(x, y, time, qI, qB, nx, ny, f);

  Fn += f;

}


// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> f = 0;
  pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, f);

  Fn += f;
}

template <class PDE>
void
BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler2D<BCTypeOutflowSupersonic_Pressure_mitState, PDE>: pSpec_ = " << pSpec_ << std::endl;
}

//----------------------------------------------------------------------------//
// Full state with Solution Function
//----------------------------------------------------------------------------//
template<template <class> class PDETraitsSize>
struct BCEulerFunction_mitState2DParams : BCEulerSolution2DParams<PDETraitsSize>
{
  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};

  static constexpr const char* BCName{"Function_mitState"};
  struct Option
  {
    const DictOption Function_mitState{BCEulerFunction_mitState2DParams::BCName, BCEulerFunction_mitState2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEulerFunction_mitState2DParams params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler2D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeFunction_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEulerFunction_mitState2DParams<PDETraitsSize> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 4;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Function_ptr& solution, const bool& upwind ) :
      pde_(pde), solution_(solution), upwind_(upwind) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    solution_( BCEulerSolution2D<PDETraitsSize, PDETraitsModel>::getSolution(d) ),
    upwind_(d.get(ParamsType::params.Characteristic)) {}

  BCEuler2D( const BCEuler2D& ) = delete;
  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*solution_)(x,y,time);
  }

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const
  {
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCEuler2D<BCTypeFunction_mitState, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
  }

protected:
  const PDE& pde_;
  const Function_ptr solution_;
  const bool upwind_;
};

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeFunction_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  if (upwind_)
  {
    // Upwind advective flux based on the boundary state
    pde_.fluxAdvectiveUpwind(x, y, time, qI, qB, nx, ny, Fn);
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0, fy = 0;
    pde_.fluxAdvective(x, y, time, qB, fx, fy);
    Fn += fx*nx + fy*ny;
  }

  //Evaluate viscous flux using boundary state qB and interior state gradient qIx, qIy
  ArrayQ<T> fv = 0, gv = 0;
  pde_.fluxViscous( x, y, time, qB, qIx, qIy, fv, gv );

  Fn += fv*nx + gv*ny; //add normal component of viscous flux
}


//----------------------------------------------------------------------------//
// Subsonic Inflow mitState with Solution Function
//----------------------------------------------------------------------------//
template<template <class> class PDETraitsSize>
struct BCEulerSolutionInflow_sHqt_mitState2DParams : BCEulerSolution2DParams<PDETraitsSize>
{
  typedef BCEulerSolutionInflow_sHqt_mitState2DParams This;

  static constexpr const char* BCName{"SolutionInflow_sHqt_mitState"};
  struct Option
  {
    const DictOption SolutionInflow_sHqt_mitState{This::BCName, This::checkInputs};
  };

  static void checkInputs(PyDict d);

  static This params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeFunctionInflow_sHqt_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEulerSolutionInflow_sHqt_mitState2DParams<PDETraitsSize> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Function_ptr& solution ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()), solution_(solution) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    solution_( BCEulerSolution2D<PDETraitsSize, PDETraitsModel>::getSolution(d) ) {}

  BCEuler2D( const BCEuler2D& ) = delete;
  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  bool useUpwind() const { return false; }

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  // Computes the boundary mach number
  template<class T>
  bool VelB( const Real& nx, const Real& ny, const ArrayQ<T>& qI,
             const Real& HSpec, const Real& vtSpec, T& Vn) const;
  // Computes the specified BC data - s, H, qt
  bool BCQuantities(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    Real& expsSpec, Real& HSpec, Real& vtSpec) const;

  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Function_ptr solution_;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
inline bool
BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::
BCQuantities( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
              Real& expsSpec, Real& HSpec, Real& vtSpec ) const
{
  Real gamma = gas_.gamma();

  ArrayQ<Real> qBspec = 0;
  qBspec = (*solution_)(x,y,time);
  Real rhoSpec, uSpec, vSpec, tSpec, pSpec = 0;
  qInterpret_.eval( qBspec, rhoSpec, uSpec, vSpec, tSpec );
  pSpec = gas_.pressure(rhoSpec, tSpec);
  HSpec = gas_.enthalpy(rhoSpec, tSpec) + 0.5*(uSpec*uSpec + vSpec*vSpec);
  expsSpec = pSpec / pow(rhoSpec, gamma);

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;
  // Specified tangential velocity component
  vtSpec = uSpec*tx + vSpec*ty;

  // the state is valid
  return true;
}

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T>
inline bool
BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::
VelB( const Real& nx, const Real& ny, const ArrayQ<T>& qI,
      const Real& HSpec, const Real& vtSpec, T& Vn ) const
{

  T rho=0, u=0, v=0, t=0;

  //preserve outgoing Riemann Invariant Jp(q,n) - after Fidkowski 2004
  qInterpret_.eval( qI, rho, u, v, t );

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  // tangential vector
//  const Real tx = -ny;
//  const Real ty =  nx;

  T c = gas_.speedofSound(t);
  T Jp = u*nx + v*ny + 2*c/gmi;

  Real A = 1 + 2.0/gmi;
  T    B = -2*Jp;
  T    C = Jp*Jp - 4*HSpec/gmi + 2/gmi*vtSpec*vtSpec;
  T    D = B*B-4*A*C;

  // Check that we have a valid state
  if ( D < 0 ) return false;

  T Vnplus  = (-B + sqrt(D))/(2.*A);
  T Vnminus = (-B - sqrt(D))/(2.*A);

  // Pick physically relevant solution
  if ((Vnplus <= 0.0) && (Vnminus >= 0.0))
  {
    Vn = Vnplus;
  }
  else if ((Vnplus >= 0.0) && (Vnminus <= 0.0))
  {
    Vn = Vnminus;
  }
  else if ((Vnplus < 0.0) && (Vnminus < 0.0))
  {
    // not clear which is physical...
    // return invalid state
    return false;
#if 0
    // pick smaller for now
    if ( fabs(Vnminus) <= fabs(Vnplus) )
      Vn = Vnminus;
    else
      Vn = Vnplus;
#endif
  }
  else
  {
    // positive Vn means reverse flow and is OK; choose smallest Vn
    if (Vnminus < Vnplus)
      Vn = Vnminus;
    else
      Vn = Vnplus;
  }

  // the state is valid
  return true;
}


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T>
inline void
BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::state(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  Real R     = gas_.R();
  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  Real expsSpec, HSpec, vtSpec = 0;
  BCQuantities(x,y,time,nx,ny, expsSpec, HSpec, vtSpec);

  T Vn = 0;
  VelB( nx, ny, qI, HSpec, vtSpec, Vn );

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // set boundary state
  T uB   = Vn*nx + vtSpec*tx;
  T vB   = Vn*ny + vtSpec*ty;
  T tB   = (HSpec - 0.5*(uB*uB + vB*vB))/gas_.Cp();
  T rhoB = pow(R*tB/expsSpec, 1./gmi);

  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature2D<T>( rhoB, uB, vB, tB ) );

}

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class TL, class TR>
inline void
BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;
}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class TL, class TR, class Tf>
inline void
BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::
fluxNormal( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
}

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
bool
BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::isValidState(
    const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const
{
  Real rho=0, u=0, v=0, t=0;
  qInterpret_.eval( q, rho, u, v, t );

  // Check that the normal velocity is negative (inflow)
//  return (u*nx + v*ny) < 0;
  return true;
}

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
void
BCEuler2D<BCTypeFunctionInflow_sHqt_mitState, PDE_<PDETraitsSize,PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeFunction_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// Reflects state and gradient
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeReflect_mitState> : noncopyable
{
  static constexpr const char* BCName{"Reflect_mitState"};
  struct Option
  {
    const DictOption Reflect_mitState{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeReflect_mitState, PDE_> :
    public BCType< BCEuler2D<BCTypeReflect_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeReflect_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return pde_.hasFluxViscous(); }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state gradient
  template <class T>
  void stateGradient( const Real& nx, const Real& ny,
                      const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                            ArrayQ<T>& qBx,       ArrayQ<T>& qBy ) const;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, qI, qB);
  }

  // BC state for RANS
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class L, class R, class T, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  }

  // normal BC flux for RANS
  template <class T, class Tf>
  void fluxNormal( const Real& dist, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class PDE>
template <class T>
inline void
BCEuler2D<BCTypeReflect_mitState, PDE>::state(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  // these might be a velocity or momentum vector
  const T& uI = qI[QInterpret::ix];
  const T& vI = qI[QInterpret::iy];

  // scalar quantities are identical in the reflection
  qB = qI;

  // normal component of the vector
  T Vn = (uI*nx + vI*ny);

  // subtract off twice the normal vector component to reflect it
  qB[QInterpret::ix] = uI - 2*nx*Vn;
  qB[QInterpret::iy] = vI - 2*ny*Vn;
}

template <class PDE>
template <class T>
void
BCEuler2D<BCTypeReflect_mitState, PDE>::
stateGradient( const Real& nx, const Real& ny,
               const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                     ArrayQ<T>& qBx,       ArrayQ<T>& qBy ) const
{
  typedef DLA::VectorS<D,T> VectorX;
  typedef DLA::MatrixS<D,D,Real> MatrixX;
  // mirror scalar components
  ArrayQ<T> qIn = (qIx*nx + qIy*ny);
  qBx = qIx - 2*nx*qIn;
  qBy = qIy - 2*ny*qIn;
  // Matrix that performs the mirroring operation
  MatrixX Jr = { {1 -2*nx*nx,  -2*nx*ny},
                 {  -2*ny*nx, 1-2*ny*ny} };
  VectorX gradIu = { qIx[QInterpret::ix], qIy[QInterpret::ix] };
  VectorX gradIv = { qIx[QInterpret::iy], qIy[QInterpret::iy] };
  // Vector gradient in the normal direction
  VectorX gradIVn = gradIu*nx + gradIv*ny ;
  VectorX gradBu = Jr*(gradIu - 2*nx*gradIVn);
  VectorX gradBv = Jr*(gradIv - 2*ny*gradIVn);
  // set the vector gradients
  qBx[QInterpret::ix] = gradBu[0]; qBy[QInterpret::ix] = gradBu[1];
  qBx[QInterpret::iy] = gradBv[0]; qBy[QInterpret::iy] = gradBv[1];
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeReflect_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on interior and reflected boundary state
  pde_.fluxAdvectiveUpwind( x, y, time, qI, qB, nx, ny, Fn );
  // Don't bother with viscous flux if it does not exist
  if (!pde_.hasFluxViscous()) return;
  // compute the mirrored gradients
  ArrayQ<T> qBx, qBy;
  stateGradient( nx, ny,
                 qIx, qIy,
                 qBx, qBy);
  // Compute the viscous flux
  pde_.fluxViscous(x, y, time, qI, qIx, qIy, qB, qBx, qBy, nx, ny, Fn);
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler2D<BCTypeReflect_mitState, PDE>::
fluxNormal( const Tp& param,
            const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on interior and reflected boundary state
  pde_.fluxAdvectiveUpwind( param, x, y, time, qI, qB, nx, ny, Fn );

  // Don't bother with viscous flux if it does not exist
  if (!pde_.hasFluxViscous()) return;

  // compute the mirrored gradients
  ArrayQ<T> qBx, qBy, qBz;

  stateGradient( nx, ny,
                 qIx, qIy,
                 qBx, qBy );

  // Compute the viscous flux
//  pde_.fluxViscous(param, x, y, time, qI, qIx, qB, qBx, nx, Fn);
}

template <class PDE>
void
BCEuler2D<BCTypeReflect_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeReflect_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeReflect_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeReflect_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional Full State Imposed (possibly with characteristics) for a radial
// inflow located at (0,0)
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeFullStateRadialInflow_mitState> : noncopyable
{

  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};
  const ParameterNumeric<Real> rho{"Density", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> V{"Speed", NO_DEFAULT, 0, NO_LIMIT, "Speed (Velocity Magnitude)"};
  const ParameterNumeric<Real> T{"Temperature", NO_DEFAULT, 0, NO_LIMIT, "Temperature"};

  static constexpr const char* BCName{"FullStateRadialInflow_mitState"};
  struct Option
  {
    const DictOption FullStateRadialInflow_mitState{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeFullStateRadialInflow_mitState, PDE_> :
    public BCType< BCEuler2D<BCTypeFullStateRadialInflow_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeFullStateRadialInflow_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCTypeFullStateRadialInflow_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const bool& upwind,
             const Real& rho, const Real& V, const Real& T ) :
      pde_(pde),
      qInterpret_(pde_.variableInterpreter()),
      upwind_(upwind),
      rho_(rho),
      V_(V),
      T_(T)
  {
    // Nothing
  }

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    upwind_(d.get(ParamsType::params.Characteristic)),
    rho_(d.get(ParamsType::params.rho)),
    V_(d.get(ParamsType::params.V)),
    T_(d.get(ParamsType::params.T))
  {
    // Nothing
  }

  ~BCEuler2D() {}

  BCEuler2D( const BCEuler2D& ) = delete;
  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    Real r = sqrt(x*x + y*y);
    Real Nx = x / r;
    Real Ny = y / r;

    Real rhoB = rho_;
    Real uB = V_ * Nx;
    Real vB = V_ * Ny;
    Real tB = T_;
    qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature2D<T>( rhoB, uB, vB, tB ) );
  }

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if ( upwind_ )
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind( x, y, time, qI, qB, nx, ny, Fn );
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<Tf> fx = 0, fy = 0;
      pde_.fluxAdvective(x, y, time, qB, fx, fy);

      Fn += fx*nx + fy*ny;
    }
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if ( upwind_ )
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn );
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<Tf> fx = 0, fy = 0;
      pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

      Fn += fx*nx + fy*ny;
    }
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const
  {
    if (upwind_)
    {
      // Characteristic can be either inflow or outflow
      return true;
    }
    else
    {
      Real uI, vI;
      qInterpret_.evalVelocity( qI, uI, vI );

      // Full state requires inflow, so the dot product between the normal and velocity should be negative
      return (uI*nx + vI*ny) < 0;
    }
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCEuler2D<BCTypeFullStateRadialInflow_mitState, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
    out << indent << "BCEuler2D<BCTypeFullStateRadialInflow_mitState, PDE>: qInterpret_ = " << std::endl;
    qInterpret_.dump(indentSize+2, out);
  }

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const bool upwind_;              // Upwind the flux or no?
  const Real rho_;                 // Inflow density
  const Real V_;                   // Inflow speed
  const Real T_;                   // Inflow temperature
};

} //namespace SANS

#endif  // BCEULER2D_MITSTATE_H
