// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "QD1Q3PrimitiveDistributionFunctions.h"
#include "TraitsBoltzmannD1Q3.h"

namespace SANS
{

template class QD1Q3<QTypePrimitiveDistributionFunctions, TraitsSizeBoltzmannD1Q3>;

}
