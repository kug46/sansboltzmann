// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q2DCONSERVATIVE_H
#define Q2DCONSERVATIVE_H

// solution interpreter class
// Euler/N-S conservation-eqns: conservation variables (rho, rho*u, rho*v, rho*E)

#include <vector>
#include <string>

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "pde/NS/NSVariable2D.h"
#include "Surreal/PromoteSurreal.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D Euler/N-S
// conservative variables (rho, rhou, rhov, rhoE)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//     MatrixQ            solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// conservative variables (rho, rhou, rhov, rhoE)
class QTypeConservative;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q2D;


template <template <class> class PDETraitsSize>
class Q2D<QTypeConservative, PDETraitsSize>
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int irho = 0;
  static const int irhou = 1;
  static const int irhov = 2;
  static const int irhoE = 3;

  // The three components of the state vector that make up the momentum vector
  static const int ix = irhou;
  static const int iy = irhov;

  static std::vector<std::string> stateNames() { return {"rho", "rhou", "rhov", "rhoE"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  explicit Q2D( const GasModel& gas0 ) : gas_(gas0) {}
  Q2D( const Q2D& q0 ) : gas_(q0.gas_) {}
  ~Q2D() {}

  Q2D& operator=( const Q2D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& v, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u, T& v ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalDensityJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                                         ArrayQ<T>& u_q, ArrayQ<T>& v_q,
                                         ArrayQ<T>& t_q ) const;

  template <class T>
  void conservativeJacobian( const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& duxdqx ) const;

  template<class Tq, class Tg>
  void evalDensityGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                            typename promote_Surreal<Tq,Tg>::type& rho_x ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type& ux,
    typename promote_Surreal<Tq,Tg>::type& vx,
    typename promote_Surreal<Tq,Tg>::type& tx ) const;

  template<class Tq, class Tg, class Th>
  void evalSecondGradient(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      typename promote_Surreal<Tq, Tg, Th>::type& rhoxx,
      typename promote_Surreal<Tq, Tg, Th>::type& rhoxy,
      typename promote_Surreal<Tq, Tg, Th>::type& rhoyy,
      typename promote_Surreal<Tq, Tg, Th>::type& uxx,
      typename promote_Surreal<Tq, Tg, Th>::type& uxy,
      typename promote_Surreal<Tq, Tg, Th>::type& uyy,
      typename promote_Surreal<Tq, Tg, Th>::type& vxx,
      typename promote_Surreal<Tq, Tg, Th>::type& vxy,
      typename promote_Surreal<Tq, Tg, Th>::type& vyy,
      typename promote_Surreal<Tq, Tg, Th>::type& txx,
      typename promote_Surreal<Tq, Tg, Th>::type& txy,
      typename promote_Surreal<Tq, Tg, Th>::type& tyy ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure2D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative2D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& v, T& t ) const
{
  rho = q(irho);
  u   = q(irhou)/rho;
  v   = q(irhov)/rho;
  t   = (q(irhoE)/rho - 0.5*(u*u+v*v) ) / ( gas_.Cv() );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  rho = q(irho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u, T& v ) const
{
  u = q(irhou)/q(irho);
  v = q(irhov)/q(irho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  const T& rho  = q(irho);
  const T& rhou = q(irhou);
  const T& rhov = q(irhov);
  const T& rhoE = q(irhoE);

  t   = ( rhoE/rho - 0.5*( rhou*rhou + rhov*rhov )/(rho*rho) ) / ( gas_.Cv() );
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::evalDensityJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const
{
  rho_q = 0;
  rho_q[0] = 1;  // drho/drho
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                        ArrayQ<T>& u_q, ArrayQ<T>& v_q,
                        ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; v_q = 0; t_q = 0;

  const T& rho  = q(irho);
  const T& rhou = q(irhou);
  const T& rhov = q(irhov);
  const T& rhoE = q(irhoE);

  rho_q[0] = 1;  // drho/drho

  u_q[0] = -rhou/(rho*rho); // du/drho
  u_q[1] = 1/rho;           // du/drhou

  v_q[0] = -rhov/(rho*rho); // dv/drho
  v_q[2] = 1/rho;           // dv/drhov

  t_q[0] = ( -rhoE/(rho*rho) + ( rhou*rhou + rhov*rhov )/(rho*rho*rho) ) / ( gas_.Cv() ); // dt/drho
  t_q[1] = ( -( rhou )/(rho*rho) ) / ( gas_.Cv() );                                       // dt/drhou
  t_q[2] = ( -( rhov )/(rho*rho) ) / ( gas_.Cv() );                                       // dt/drhov
  t_q[3] = ( 1/rho ) / ( gas_.Cv() );                                                     // dt/drhoE
}

template <template <class> class PDETraitsSize>
template<class Tq, class Tg>
void
Q2D<QTypeConservative, PDETraitsSize>::evalDensityGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rho_x ) const
{
  rho_x = qx(irho);
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::conservativeJacobian(
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& duxdqx ) const
{
  //dUX/dQX
  duxdqx = 0;
  duxdqx(irho ,irho ) = 1;
  duxdqx(irhou,irhou) = 1;
  duxdqx(irhov,irhov) = 1;
  duxdqx(irhoE,irhoE) = 1;
}


template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
Q2D<QTypeConservative, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type&   ux,
    typename promote_Surreal<Tq,Tg>::type&   vx,
    typename promote_Surreal<Tq,Tg>::type&   tx ) const
{
  const Tq& rho  = q(irho);
            rhox = qx(irho);

  const Tq& rhou  = q(irhou);
  const Tg& rhoux = qx(irhou);

  const Tq& rhov  = q(irhov);
  const Tg& rhovx = qx(irhov);

  const Tq& rhoE  = q(irhoE);
  const Tg& rhoEx = qx(irhoE);

  ux   = rhoux/rho - rhou*rhox/(rho*rho);
  vx   = rhovx/rho - rhov*rhox/(rho*rho);

  tx = (rhou*rhou + rhov*rhov)*rhox/(rho*rho*rho)
            - (rhou*rhoux + rhov*rhovx)/(rho*rho) + rhoEx/rho - rhoE*rhox/(rho*rho);
  tx /= ( gas_.Cv() );
}


template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class Th>
inline void
Q2D<QTypeConservative, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    typename promote_Surreal<Tq, Tg, Th>::type& rhoxx,
    typename promote_Surreal<Tq, Tg, Th>::type& rhoxy,
    typename promote_Surreal<Tq, Tg, Th>::type& rhoyy,
    typename promote_Surreal<Tq, Tg, Th>::type& uxx,
    typename promote_Surreal<Tq, Tg, Th>::type& uxy,
    typename promote_Surreal<Tq, Tg, Th>::type& uyy,
    typename promote_Surreal<Tq, Tg, Th>::type& vxx,
    typename promote_Surreal<Tq, Tg, Th>::type& vxy,
    typename promote_Surreal<Tq, Tg, Th>::type& vyy,
    typename promote_Surreal<Tq, Tg, Th>::type& txx,
    typename promote_Surreal<Tq, Tg, Th>::type& txy,
    typename promote_Surreal<Tq, Tg, Th>::type& tyy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
//  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho, u, v, t;
  Tqg rhox, ux, vx, tx;
  Tqg rhoy, uy, vy, ty;

//  T rhou = q(irhou); //NOT USED
                            const Tg& rhoux = qx(irhou); const Tg& rhouy = qy(irhou);
//  T rhov = q(irhov); //NOT USED
                            const Tg& rhovx = qx(irhov); const Tg& rhovy = qy(irhov);
  const Tq& rhoE = q(irhoE); const Tg& rhoEx = qx(irhoE); const Tg& rhoEy = qy(irhoE);

  rhoxx = qxx(irho); rhoxy = qxy(irho); rhoyy = qyy(irho);
  Th rhouxx = qxx(irhou); Th rhouxy = qxy(irhou); Th rhouyy = qyy(irhou);
  Th rhovxx = qxx(irhov); Th rhovxy = qxy(irhov); Th rhovyy = qyy(irhov);
  Th rhoExx = qxx(irhoE); Th rhoExy = qxy(irhoE); Th rhoEyy = qyy(irhoE);

  eval(q, rho, u, v, t);
  evalGradient(q, qx, rhox, ux, vx, tx);
  evalGradient(q, qy, rhoy, uy, vy, ty);

  uxx = 2.0*u*rhox*rhox/(rho*rho) - 2.0*rhox * rhoux/(rho*rho) - u*rhoxx/rho + rhouxx/rho;
  uxy = 2.0*u*rhoy*rhox/(rho*rho) - rhouy*rhox/(rho*rho) - rhoy*rhoux/(rho*rho) - u*rhoxy/rho + rhouxy/rho;
  uyy = 2.0*u*rhoy*rhoy/(rho*rho) - 2.0*rhoy * rhouy/(rho*rho) - u*rhoyy/rho + rhouyy/rho;

  vxx = 2.0*v*rhox*rhox/(rho*rho) - 2.0*rhox * rhovx/(rho*rho) - v*rhoxx/rho + rhovxx/rho;
  vxy = 2.0*v*rhoy*rhox/(rho*rho) - rhovy*rhox/(rho*rho) - rhoy*rhovx/(rho*rho) - v*rhoxy/rho + rhovxy/rho;
  vyy = 2.0*v*rhoy*rhoy/(rho*rho) - 2.0*rhoy*rhovy/(rho*rho) - v*rhoyy/rho + rhovyy/rho;

  txx = 2.0*rhoE*rhox*rhox/(rho*rho*rho) - 2.0*rhox*rhoEx/(rho*rho) + (-ux*ux - vx*vx -u*uxx -v*vxx)
      - rhoE*rhoxx/(rho*rho) + rhoExx/rho ;

  txy = 2.0*rhoE*rhox*rhoy/(rho*rho*rho) - rhoEy*rhox/(rho*rho) - rhoy*rhoEx/(rho*rho) +
      (-uy*ux - vy*vx - u*uxy - v*vxy ) - rhoE*rhoxy/(rho*rho) + rhoExy/rho;

  tyy = 2.0*rhoE*rhoy*rhoy/(rho*rho*rho) - 2.0*rhoy*rhoEy/(rho*rho) + (-uy*uy - vy*vy -u*uyy -v*vyy)
      - rhoE*rhoyy/(rho*rho) + rhoEyy/rho ;

  txx /= ( gas_.Cv() );
  txy /= ( gas_.Cv() );
  tyy /= ( gas_.Cv() );

}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 4);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "VelocityY") &&
              (name[3] == "Temperature"));

  T rho, u, v, t = 0;

  rho = data[0];
  u   = data[1];
  v   = data[2];
  t   = data[3];

  q(irho)  = rho;
  q(irhou) = rho*u;
  q(irhov) = rho*v;
  q(irhoE) = rho*( gas_.energy(rho,t) + 0.5*(u*u + v*v) );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure2D<T>& data ) const
{

  T rho, u, v, t, p, E = 0;

  rho = data.Density;
  u = data.VelocityX;
  v = data.VelocityY;
  p = data.Pressure;
  t = p / (gas_.R()*rho);
  E = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  q(irho)  = rho;
  q(irhou) = rho*u;
  q(irhov) = rho*v;
  q(irhoE) = rho*E;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const
{
  T rho, u, v, t, E = 0;

  rho = data.Density;
  u = data.VelocityX;
  v = data.VelocityY;
  t = data.Temperature;
  E = gas_.energy(rho, t) + 0.5*(u*u + v*v);

  q(irho)  = rho;
  q(irhou) = rho*u;
  q(irhov) = rho*v;
  q(irhoE) = rho*E;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative2D<T>& data ) const
{
  q(irho)  = data.Density;
  q(irhou) = data.MomentumX;
  q(irhov) = data.MomentumY;
  q(irhoE) = data.TotalEnergy;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q2D<QTypeConservative, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
Q2D<QTypeConservative, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  Real rho = 0, t = 0;

  evalDensity(q, rho);
  if (rho <= 0)
    return false;

  evalTemperature(q, t);
  if (t <= 0)
    return false;

  return true;
}


template <template <class> class PDETraitsSize>
void
Q2D<QTypeConservative, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q2D<QTypeConservative, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q2D<QTypeConservative, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // Q2DCONSERVATIVE_H
