// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEEULER2D_ARTIFICIALVISCOSITY2D_H
#define PDEEULER2D_ARTIFICIALVISCOSITY2D_H

// 2-D compressible Euler with artificial viscosity

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/smoothmath.h"
#include "tools/Tuple.h"

#include "Field/Tuple/ParamTuple.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/InverseLU.h"

#include "PDEEuler2D.h"
#include "EulerArtificialViscosityType.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2D Artificial Viscosity wrapper for Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase = PDEEuler2D>
class PDEEulermitAVDiffusion2D : public PDEBase<PDETraitsSize, PDETraitsModel>
{
public:

  typedef PhysD2 PhysDim;

  typedef PDEBase<PDETraitsSize, PDETraitsModel> BaseType;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables
  static const int Nparam = 1;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  template <class TP>
  using ArrayParam = DLA::VectorS<Nparam,TP>;  // parameter array type

  template <class T>
  using MatrixParam = DLA::MatrixS<N,Nparam,T>;         // e.g. jacobian of PDEs w.r.t. parameters

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs >
  PDEEulermitAVDiffusion2D(const int order, const bool hasSpaceTimeDiffusion, const EulerArtViscosity& visc, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    order_(order), hasSpaceTimeDiffusion_(hasSpaceTimeDiffusion), visc_(visc)
  {
    //Nothing
  }

  ~PDEEulermitAVDiffusion2D() {}

  PDEEulermitAVDiffusion2D& operator=( const PDEEulermitAVDiffusion2D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return BaseType::hasFluxAdvective(); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return BaseType::hasSource(); }
  bool hasSourceTrace() const { return BaseType::hasSourceTrace(); }
  bool hasForcingFunction() const { return BaseType::hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return BaseType::needsSolutionGradientforSource(); }

  // unsteady temporal flux: Ft(Q)
  template <class Tp, class T, class Tf>
  void fluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(x, y, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <int Dim, class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(x, y, time, q, J);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <int Dim, class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(param.right(), x, y, time, q, J);
  }

  //  master state: U(Q)
  template <class Tp, class T, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
  {
    BaseType::masterState(x, y, time, q, uCons);
  }

  // master state Gradient: Ux(Q)
  template<class Tp, class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& Ux, ArrayQ<Tf>& Uy  ) const
  {
    BaseType::masterStateGradient(x, y, time, q, qx, qy, Ux, Uy);
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tp, class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& Uxx, ArrayQ<Tf>& Uxy, ArrayQ<Tf>& Uyy ) const
  {
    BaseType::masterStateHessian(x, y, time, q, qx, qy, qxx, qxy, qyy, Uxx, Uxy, Uyy);
  }

  template<class Tp, class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& dU ) const
  {
    BaseType::perturbedMasterState(x, y, time, q, dq, dU);
  }


  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class Tp, class T>
  void jacobianMasterState(
      const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    BaseType::jacobianMasterState(x, y, time, q, dudq);
  }

  // unsteady conservative flux: d(U)/d(t)
  template <class T>
  void strongFluxAdvectiveTime(
      const DLA::MatrixSymS<D,Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
  {
    BaseType::strongFluxAdvectiveTime(x, y, time, q, qt, dudt);
  }

  // unsteady conservative flux: d(U)/d(t)
  template <class T>
  void strongFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
  {
    BaseType::strongFluxAdvectiveTime(param.right(), x, y, time, q, qt, dudt);
  }

  // advective flux: F(Q)
  template <class Tp, class T, class Ts>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Ts>& f, ArrayQ<Ts>& g ) const
  {
    BaseType::fluxAdvective(x, y, time, q, f, g);
  }

  template <class Tp, class T, class Ts>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Ts>& f, const Real& scaleFactor = 1 ) const
  {
    BaseType::fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f, scaleFactor);
  }

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class T, class Ta>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x, const Real& y,  const Real& time, const ArrayQ<T>& q,
      MatrixQ<Ta>& ax, MatrixQ<Ta>& ay ) const
  {
    BaseType::jacobianFluxAdvective( x, y, time, q, ax, ay );
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Ta>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Ta>& mtx ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValue( x, y, time, q, nx, ny, mtx );
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Tf>& mtx ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValueSpaceTime( x, y, time, q, nx, ny, nt, mtx );
  }

  // strong form advective fluxes
  template <class Tp, class T, class Tg, class Tf>
  void strongFluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const
  {
    BaseType::strongFluxAdvective( x, y, time, q, qx, qy, strongPDE );
  }

  // viscous flux: Fv(Q, Qx)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tp, class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;


  // perturbation to viscous flux from dux, duy
  template <class Tp, class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const;

  template <int Dim, class Tq, class Tg, class Ts>
    void jacobianFluxViscous(
        const DLA::MatrixSymS<Dim,Real>& param,
        const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& ax, MatrixQ<Ts>& ay ) const
  {
    BaseType::jacobianFluxViscous( x, y, time, q, qx, qy, ax, ay );
  }

  template <int Dim, class Tq, class Tg, class Ts>
    void jacobianFluxViscous(
        const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
        const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& ax, MatrixQ<Ts>& ay ) const
  {
    BaseType::jacobianFluxViscous( param.right(), x, y, time, q, qx, qy, ax, ay );
  }

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const DLA::MatrixSymS<D,Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
  {
    BaseType::diffusionViscousGradient( x, y, time,
                                        q, qx, qy,
                                        qxx, qxy, qyy,
                                        kxx_x, kxy_x, kyx_x,
                                        kxy_y, kyx_y, kyy_y );
  }

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const
  {
    BaseType::diffusionViscousGradient( param.right(), x, y, time,
                                        q, qx, qy,
                                        qxx, qxy, qyy,
                                        kxx_x, kxy_x, kyx_x,
                                        kxy_y, kyx_y, kyy_y );
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSourceAbsoluteValue(x, y, time, q, qx, qy, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSourceAbsoluteValue(param.right(), x, y, time, q, qx, qy, dsdu);
  }

  template <int Dim, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradient(
        const DLA::MatrixSymS<Dim,Real>& param, const
        Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        MatrixQ<Ts>& div_dsgradu ) const
  {
    BaseType::jacobianGradientSourceGradient(x, y, time, q, qx, qy, qxx, qxy, qyy, div_dsgradu);
  }

  template <int Dim, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradient(
        const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
        const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        MatrixQ<Ts>& div_dsgradu ) const
  {
    BaseType::jacobianGradientSourceGradient(param.right(), x, y, time, q, qx, qy, qxx, qxy, qyy, div_dsgradu);
  }

  // strong form viscous fluxes
  template <class Tp, class T, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tqg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy, const ArrayQ<Tqg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const;

  template <class Tp, class Tq, class Tqg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qyL, const ArrayQ<Tqg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qyR, const ArrayQ<Tqg>& qtR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // solution-dependent source: S(x, Q, Qx) - param: H-tensor + distance
  template <int Dim, class Tq, class Tg, class Ts>
  void source(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( param.right(), x, y, time, q, qx, qy, source );
  }

  // solution-dependent source: S(x, Q, Qx) - param: H-tensor
  template <int Dim, class Tq, class Tg, class Ts>
  void source(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, y, time, q, qx, qy, source );
  }

  // solution-dependent source: S(x, Q, Qx) - param: H-tensor
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    BaseType::source( x, y, time, q, qp, qx, qy, qpx, qpy, src );
  }

  // solution-dependent source: S(x, Q, Qx) - param: H-tensor
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    BaseType::sourceCoarse( x, y, time, q, qp, qx, qy, qpx, qpy, src );
  }

  // solution-dependent source: S(x, Q, Qx) - param: H-tensor
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    BaseType::sourceFine( x, y, time, q, qp, qx, qy, qpx, qpy, src );
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, y, time, lifted_quantity, q, qx, qy, source );
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX) - param: H-tensor
  template <int Dim, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const
  {
    // assuming the base class is NS or Euler
    BaseType::source( x, y, time, lifted_quantity, q, qx, qy, source );
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX) - param: H-tensor + distance
  template <int Dim, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y,const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const
  {
    // assuming the base class is RANS
    BaseType::source( param.right(), x, y, time, lifted_quantity, q, qx, qy, source );
  }

  // dual-consistent source
  template <class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    BaseType::sourceTrace(xL, yL, xR, yR, time, qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR);
  }

  template <int Dim, class Tq, class Tg, class Ts>
  void sourceTrace(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& paramL, const Real& xL, const Real& yL,
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    BaseType::sourceTrace(paramL.right(), xL, yL,
                          paramR.right(), xR, yR, time,
                          qL, qxL, qyL, qR, qxR, qyR, sourceL, sourceR);
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <int Dim, class Tq, class Ts>
  void sourceLiftedQuantity(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& paramL, const Real& xL, const Real& yL,
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(paramL.right(), xL, yL, paramR.right(), xR, yR, time, qL, qR, s);
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <int Dim, class Tq, class Ts>
  void sourceLiftedQuantity(
      const DLA::MatrixSymS<Dim,Real>& paramL, const Real& xL, const Real& yL,
      const DLA::MatrixSymS<Dim,Real>& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(xL, yL, xR, yR, time, qL, qR, s);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class T, class Tg, class Ts>
  void jacobianSource(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSource(x, y, time, q, qx, qy, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class T, class Tg, class Ts>
  void jacobianSource(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSource(param.right(), x, y, time, q, qx, qy, dsdu);
  }


  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class T, class Tg, class Ts>
  void jacobianSourceHACK(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSourceHACK(x, y, time, q, qx, qy, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class T, class Tg, class Ts>
  void jacobianSourceHACK(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    BaseType::jacobianSourceHACK(param.right(), x, y, time, q, qx, qy, dsdu);
  }

  template <int Dim, class Tq, class Tg, class Ts>
    void jacobianGradientSource(
        const DLA::MatrixSymS<Dim,Real>& param,
        const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    BaseType::jacobianGradientSource( x, y, time, q, qx, qy, dsdux, dsduy );
  }

  template <int Dim, class Tq, class Tg, class Ts>
    void jacobianGradientSource(
        const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
        const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    BaseType::jacobianGradientSource( param.right(), x, y, time, q, qx, qy, dsdux, dsduy );
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class T, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    BaseType::jacobianGradientSourceHACK(x, y, time, q, qx, qy, dsdux, dsduy);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <int Dim, class T, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
  {
    BaseType::jacobianGradientSourceHACK(param.right(), x, y, time, q, qx, qy, dsdux, dsduy);
  }

  // right-hand-side forcing function
  template <class Tp, class T>
  void forcingFunction( const Tp& param,
                        const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const
  {
    BaseType::forcingFunction(x, y, time, forcing );
  }

  // right-hand-side forcing function - NS and Euler
  template<int Dim, class T>
  void forcingFunction( const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& time,
                        ArrayQ<T>& forcing ) const
  {
    BaseType::forcingFunction(x, y, time, forcing );
  }

  // right-hand-side forcing function - RANS
  template<int Dim, class T>
  void forcingFunction( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
                        const Real& x, const Real& y, const Real& time,
                        ArrayQ<T>& forcing ) const
  {
    BaseType::forcingFunction(param.right(), x, y, time, forcing );
  }


  // characteristic speed (needed for timestep)
  template <int Dim, class T>
  void speedCharacteristic(
      const DLA::MatrixSymS<Dim,Real>& param,
      const Real& x, const Real& y, const Real& time,
      const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, y, time, dx, dy, q, speed);
  }


  // characteristic speed (needed for timestep)
  template <int Dim, class T>
  void speedCharacteristic(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(param.right(), x, y, time, dx, dy, q, speed);
  }

  // characteristic speed
  template <int Dim, class T>
  void speedCharacteristic(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(param.right(), x, y, time, q, speed);
  }

  // characteristic speed
  template <int Dim, class T>
  void speedCharacteristic(
      const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, y, time, q, speed);
  }

  // update fraction needed for physically valid state
  template <int Dim>
  void updateFraction(
      const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(x, y, time, q, dq, maxChangeFraction, updateFraction);
  }

  // update fraction needed for physically valid state
  template <int Dim>
  void updateFraction(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(param.right(), x, y, time, q, dq, maxChangeFraction, updateFraction);
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int getOrder() const { return order_; };

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const DLA::MatrixSymS<2, Real>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index,
      const ParamTuple<DLA::MatrixSymS<2,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const DLA::MatrixSymS<3, Real>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

protected:

  // Artificial viscosity - generalized H-tensor
  template <int Dim, class Tq, class Te>
  void artificialViscosity( const DLA::MatrixSymS<Dim,Real>& logH,
                            const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
                            DLA::MatrixSymS<D,Te>& eps ) const;

  template <int Dim, class Tq, class Te>
  void artificialViscosity( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
                            const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
                            DLA::MatrixSymS<D,Te>& eps ) const;

  // Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
  template <class Tp, class Tq, class Tk>
  void artificialViscosity( const Tp& param,
                            const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
                            MatrixQ<Tk>& Kxx, MatrixQ<Tk>& Kxy,
                            MatrixQ<Tk>& Kyx, MatrixQ<Tk>& Kyy ) const;


  template <int Dim, class Tq, class Tqg, class Te>
  void artificialViscosity( const DLA::MatrixSymS<Dim,Real>& logH,
                            const Real& x, const Real& y, const Real& time,
                            const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy,
                            DLA::MatrixSymS<D,Te>& eps ) const;

  template <int Dim, class Tq, class Tqg, class Te>
  void artificialViscosity( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
                            const Real& x, const Real& y, const Real& time,
                            const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy,
                            DLA::MatrixSymS<D,Te>& eps ) const;

  // Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
  template <class Tp, class Tq, class Tqg, class Tk>
  void artificialViscosity( const Tp& param,
                            const Real& x, const Real& y, const Real& time,
                            const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy,
                            MatrixQ<Tk>& Kxx, MatrixQ<Tk>& Kxy,
                            MatrixQ<Tk>& Kyx, MatrixQ<Tk>& Kyy ) const;

  template <class T, class Th, class Tg>
  void artificialViscositySpaceTime( const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q,
                                     const DLA::MatrixSymS<D+1,Th>& logH,
                                     MatrixQ<Tg>& Kxx, MatrixQ<Tg>& Kxy, MatrixQ<Tg>& Kxt,
                                     MatrixQ<Tg>& Kyx, MatrixQ<Tg>& Kyy, MatrixQ<Tg>& Kyt,
                                     MatrixQ<Tg>& Ktx, MatrixQ<Tg>& Kty, MatrixQ<Tg>& Ktt ) const;
  // Artificial viscosity flux
  template <class Tp, class Tq, class Tg, class Tf>
  void artificialViscosityFlux( const Tp& param,
                                const Real& x, const Real& y, const Real& time,
                                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                                ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  const int order_;
  const bool hasSpaceTimeDiffusion_;
  const EulerArtViscosity visc_;
  const Real Prandtl_ = 0.72;
  const Real rhodiff_ = 4.0/3.0;
  const Real C_ = 2.0;

  using BaseType::gas_;
  using BaseType::qInterpret_;

  const bool usePositivityTerm_ = true;
};

// Artificial viscosity - generalized H-tensor
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Te>
void inline
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscosity(
    const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    DLA::MatrixSymS<D,Te>& eps ) const
{
  DLA::MatrixSymS<Dim,Real> logH = param.left();
  Real dist = param.right();
  DLA::MatrixSymS<Dim,Real> H = exp(logH); //generalized h-tensor

  Tq lambda;
  BaseType::speedCharacteristic( dist, x, y, time, q, lambda );

  Tq sensor = q(N-1);
  Tq zz = 0.0;
  sensor = max(sensor, zz);

  Tq factor = C_/(Real(max(order_,1))) * lambda * sensor;

  for (int i = 0; i < D; i++)
    for (int j = 0; j <= i; j++)
        eps(i,j) = factor*H(i,j); //Artificial viscosity
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Te>
void inline
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscosity(
    const DLA::MatrixSymS<Dim,Real>& logH,
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    DLA::MatrixSymS<D,Te>& eps ) const
{

  DLA::MatrixSymS<Dim,Real> H = exp(logH); //generalized h-tensor

  Tq lambda;
  BaseType::speedCharacteristic( x, y, time, q, lambda );

  Tq sensor = q(N-1);
  Tq zz = 0.0;
  sensor = max(sensor, zz);

  Tq factor = C_/(Real(max(order_,1))) * lambda * sensor;

  for (int i = 0; i < D; i++)
    for (int j = 0; j <= i; j++)
        eps(i,j) = factor*H(i,j); //Artificial viscosity
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Tqg, class Te>
void inline
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscosity(
    const DLA::MatrixSymS<Dim,Real>& logH,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy,
    DLA::MatrixSymS<D,Te>& eps ) const
{
  typedef typename promote_Surreal<Tq, Tqg>::type Tp;
  Tp kappa = 3.0;

  DLA::MatrixSymS<Dim,Real> H = exp(logH); //generalized h-tensor
  DLA::MatrixSymS<Dim,Real> M = H*H;

  Tq rho = 0, u = 0, v = 0, t = 0, p = 0;
  qInterpret_.eval( q, rho, u, v, t );
  p  = gas_.pressure(rho, t);

  Tp rhox = 0, ux = 0, vx = 0, tx = 0;
  Tp rhoy = 0, uy = 0, vy = 0, ty = 0;
  Tp px = 0, py = 0;
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );
  gas_.pressureGradient( rho, t, rhox, tx, px );
  gas_.pressureGradient( rho, t, rhoy, ty, py );
  DLA::VectorS<D, Tp> gradp = {px, py};
  Tp sensor = Transpose(gradp)*M*gradp;
  sensor = sqrt(sensor);
  sensor = sensor / (sensor + kappa * p);
  sensor = sensor * tanh(10.0*sensor);

  Tq lambda;
  BaseType::speedCharacteristic( x, y, time, q, lambda );

  Tp factor = 1.0/(Real(max(order_,1))) * lambda * sensor;

//  for (int i = 0; i < D; i++)
//    for (int j = 0; j <= i; j++)
//        eps(i,j) = factor*H(i,j); //Artificial viscosity

  DLA::VectorS<D, Real> ex = {1, 0};
  DLA::VectorS<D, Real> ey = {0, 1};
  Real hx = Transpose(ex) * H * ex;
  Real hy = Transpose(ey) * H * ey;
  eps(0,0) = factor * hx;
  eps(1,1) = factor * hy;
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <int Dim, class Tq, class Tqg, class Te>
void inline
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscosity(
    const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy,
    DLA::MatrixSymS<D,Te>& eps ) const
{
  artificialViscosity(param.left(), x, y, time, q, qx, qy, eps);
}


// Space-time artificial viscosity calculation with generalized h-tensor
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tq, class Th, class Tg>
void inline
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscositySpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    const DLA::MatrixSymS<D+1,Th>& logH,
    MatrixQ<Tg>& Kxx, MatrixQ<Tg>& Kxy, MatrixQ<Tg>& Kxt,
    MatrixQ<Tg>& Kyx, MatrixQ<Tg>& Kyy, MatrixQ<Tg>& Kyt,
    MatrixQ<Tg>& Ktx, MatrixQ<Tg>& Kty, MatrixQ<Tg>& Ktt ) const
{

  DLA::MatrixSymS<D+1,Th> H = exp(logH); //generalized h-tensor

  Tq lambda;
  BaseType::speedCharacteristic( x, y, time, q, lambda );

  Tq z = 0;

//  sensor = smoothmax(sensor, z, 40.0);
  Tq sensor = q(N-1);
  sensor = max(sensor, z);

  Tq factor = C_/(Real(max(order_,1))) * sensor;

  for (int i = 0; i < N; i++)
  {
    Kxx(i,i) += factor*lambda*H(0,0);
    Kxy(i,i) += factor*lambda*H(0,1);
    Kyx(i,i) += factor*lambda*H(1,0);
    Kyy(i,i) += factor*lambda*H(1,1);
  }


  if (hasSpaceTimeDiffusion_)
  {
    for (int i = 0; i < N; i++)
    {
      Kxt(i,i) += factor*H(0,2);
      Kyt(i,i) += factor*H(1,2);
      Ktx(i,i) += factor*H(2,0);
      Kty(i,i) += factor*H(2,1);
      Ktt(i,i) += factor*H(2,2);
    }
  }
}

// Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tqg, class Tk>
void inline
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscosity(
    const Tp& param,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  SANS_ASSERT_MSG(visc_ == EulerArtViscosity::eHolst2020, "Must be using Holst2020 model to get here");

  DLA::MatrixSymS<D,Tk> eps = 0.0; //Artificial viscosity
  artificialViscosity( param, x, y, time, q, qx, qy, eps );

  // Energy to Enthalpy jacobian
  ArrayQ<Tq> dutdu = 0;

  Real gamma = BaseType::gas_.gamma();
  Tq rho = 0, u = 0, v = 0, t = 0;
  BaseType::qInterpret_.eval( q, rho, u, v, t );

  dutdu(BaseType::iCont) = 0.5*(gamma - 1.0)*(u*u + v*v);
  dutdu(BaseType::ixMom) = -(gamma - 1.0)*u;
  dutdu(BaseType::iyMom) = -(gamma - 1.0)*v;
  dutdu(BaseType::iEngy) = gamma;

  static_assert( BaseType::iCont == 0 &&
                 BaseType::iEngy > BaseType::iCont &&
                 BaseType::iEngy > BaseType::ixMom &&
                 BaseType::iEngy > BaseType::iyMom, "Below loops assume energy is last");

  // Don't add diffusion to the sensor equation
  for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
  {
    kxx(i,i) += eps(0,0); // [length^2 / time]
    kxy(i,i) += eps(0,1); // [length^2 / time]
    kyx(i,i) += eps(1,0); // [length^2 / time]
    kyy(i,i) += eps(1,1); // [length^2 / time]
  }

  for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
  {
    // This changes the gradient to be on Enthalpy rather than Energy
    kxx(BaseType::iEngy,i) += eps(0,0)*dutdu(i);
    kxy(BaseType::iEngy,i) += eps(0,1)*dutdu(i);
    kyx(BaseType::iEngy,i) += eps(1,0)*dutdu(i);
    kyy(BaseType::iEngy,i) += eps(1,1)*dutdu(i);
  }

  // Holst adds diffusion to SA equation...
  for (int i = BaseType::iEngy+1; i < N; i++)
  {
    kxx(i,i) += eps(0,0); // [length^2 / time]
    kxy(i,i) += eps(0,1); // [length^2 / time]
    kyx(i,i) += eps(1,0); // [length^2 / time]
    kyy(i,i) += eps(1,1); // [length^2 / time]
  }
}

// Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tk>
void inline
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
artificialViscosity(
    const Tp& param,
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  DLA::MatrixSymS<D,Tq> eps; //Artificial viscosity
  artificialViscosity( param, x, y, time, q, eps );

  if (visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy)
  {

    // Energy to Enthalpy jacobian
    ArrayQ<Tq> dutdu = 0;

    Real gamma = BaseType::gas_.gamma();
    Tq rho = 0, u = 0, v = 0, t = 0;
    BaseType::qInterpret_.eval( q, rho, u, v, t );

    dutdu(BaseType::iCont) = 0.5*(gamma - 1.0)*(u*u + v*v);
    dutdu(BaseType::ixMom) = -(gamma - 1.0)*u;
    dutdu(BaseType::iyMom) = -(gamma - 1.0)*v;
    dutdu(BaseType::iEngy) = gamma;

    static_assert( BaseType::iCont == 0 &&
                   BaseType::iEngy > BaseType::iCont &&
                   BaseType::iEngy > BaseType::ixMom &&
                   BaseType::iEngy > BaseType::iyMom, "Below loops assume energy is last");

    // Don't add diffusion to the sensor equation
    for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
    {
      kxx(i,i) += eps(0,0); // [length^2 / time]
      kxy(i,i) += eps(0,1); // [length^2 / time]
      kyx(i,i) += eps(1,0); // [length^2 / time]
      kyy(i,i) += eps(1,1); // [length^2 / time]
    }

    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      // This changes the gradient to be on Enthalpy rather than Energy
      kxx(BaseType::iEngy,i) += eps(0,0)*dutdu(i);
      kxy(BaseType::iEngy,i) += eps(0,1)*dutdu(i);
      kyx(BaseType::iEngy,i) += eps(1,0)*dutdu(i);
      kyy(BaseType::iEngy,i) += eps(1,1)*dutdu(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eLaplaceViscosityEnergy)
  {
    // Don't add diffusion to the sensor equation
    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      kxx(i,i) += eps(0,0); // [length^2 / time]
      kxy(i,i) += eps(0,1); // [length^2 / time]
      kyx(i,i) += eps(1,0); // [length^2 / time]
      kyy(i,i) += eps(1,1); // [length^2 / time]
    }
  }
  else if (visc_ == EulerArtViscosity::eShearViscosity)
  {
    Tq rho=0, u=0, v=0, t=0;

    qInterpret_.eval( q, rho, u, v, t );

    Real Cv = gas_.Cv();
    Real Cp = gas_.Cp();

    DLA::MatrixSymS<D,Tq> k=0;
    for (int d = 0; d < D; d++)
      k(d,d) = eps(d,d)*Cp/Prandtl_;

    Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

    // defining nu here as mu/rho (and hence lambda/rho as well as k/(Cv*rho)) to remove rho division throughout the K matrix calculation
    DLA::MatrixSymS<D,Tq> nu = eps/rho;
    DLA::MatrixSymS<D,Tq> lambda = -2./3. * eps;
    DLA::MatrixSymS<D,Tq> kn = k/Tq(Cv*rho);

    // d(Fv)/d(Ux)
    kxx(BaseType::ixMom,0) += -u*(2*nu(0,0) + lambda(0,0));
    kxx(BaseType::ixMom,1) +=    (2*nu(0,0) + lambda(0,0));

    kxx(BaseType::iyMom,0) += -v*nu(0,0);
    kxx(BaseType::iyMom,2) +=    nu(0,0);

    kxx(BaseType::iEngy,0) += -u*u*((2*nu(0,0) + lambda(0,0)) - kn(0,0)) - v*v*(nu(0,0) - kn(0,0)) - e0*kn(0,0);
    kxx(BaseType::iEngy,1) +=    u*((2*nu(0,0) + lambda(0,0)) - kn(0,0));
    kxx(BaseType::iEngy,2) +=                                                v*(nu(0,0) - kn(0,0));
    kxx(BaseType::iEngy,3) +=                                                                           kn(0,0);

    // d(Fv)/d(Uy)
    kxy(BaseType::ixMom,0) += -v*lambda(0,1);
    kxy(BaseType::ixMom,2) +=    lambda(0,1);

    kxy(BaseType::iyMom,0) += -u*nu(0,1);
    kxy(BaseType::iyMom,1) +=    nu(0,1);

    kxy(BaseType::iEngy,0) += -u*v*(nu(0,1) + lambda(0,1));
    kxy(BaseType::iEngy,1) +=  v*nu(0,1);
    kxy(BaseType::iEngy,2) +=  u*lambda(0,1);

    // d(Gv)/d(Ux)
    kyx(BaseType::ixMom,0) += -v*nu(1,0);
    kyx(BaseType::ixMom,2) +=    nu(1,0);

    kyx(BaseType::iyMom,0) += -u*lambda(1,0);
    kyx(BaseType::iyMom,1) +=    lambda(1,0);

    kyx(BaseType::iEngy,0) += -u*v*(nu(1,0) + lambda(1,0));
    kyx(BaseType::iEngy,1) +=  v*lambda(1,0);
    kyx(BaseType::iEngy,2) +=  u*nu(1,0);

    // d(Gv)/d(Uy)
    kyy(BaseType::ixMom,0) += -u*nu(1,1);
    kyy(BaseType::ixMom,1) +=    nu(1,1);

    kyy(BaseType::iyMom,0) += -v*(2*nu(1,1) + lambda(1,1));
    kyy(BaseType::iyMom,2) +=    (2*nu(1,1) + lambda(1,1));

    kyy(BaseType::iEngy,0) += -u*u*(nu(1,1) - kn(1,1)) - v*v*((2*nu(1,1) + lambda(1,1)) - kn(1,1)) - e0*kn(1,1);
    kyy(BaseType::iEngy,1) +=    u*(nu(1,1) - kn(1,1));
    kyy(BaseType::iEngy,2) +=                              v*((2*nu(1,1) + lambda(1,1)) - kn(1,1));
    kyy(BaseType::iEngy,3) +=                                                                           kn(1,1);
  }
  else if (visc_ == EulerArtViscosity::eBrennerViscosity)
  {
    Tq rho=0, u=0, v=0, t=0;

    qInterpret_.eval( q, rho, u, v, t );

    Real Cv = gas_.Cv();
    Real Cp = gas_.Cp();

    DLA::MatrixSymS<D,Tq> k=0;
    for (int d = 0; d < D; d++)
      k(d,d) = eps(d,d)*Cp/Prandtl_;

    Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

    // defining nu here as mu/rho (and hence lambda/rho as well as k/(Cv*rho)) to remove rho division throughout the K matrix calculation
    DLA::MatrixSymS<D,Tq> nu = eps/rho;
    DLA::MatrixSymS<D,Tq> lambda = -2./3. * eps;
    DLA::MatrixSymS<D,Tq> kn = k/Tq(Cv*rho);

    // d(Fv)/d(Ux)
    kxx(BaseType::ixMom,0) += -u*(2*nu(0,0) + lambda(0,0));
    kxx(BaseType::ixMom,1) +=    (2*nu(0,0) + lambda(0,0));

    kxx(BaseType::iyMom,0) += -v*nu(0,0);
    kxx(BaseType::iyMom,2) +=    nu(0,0);

    kxx(BaseType::iEngy,0) += -u*u*((2*nu(0,0) + lambda(0,0)) - kn(0,0)) - v*v*(nu(0,0) - kn(0,0)) - e0*kn(0,0);
    kxx(BaseType::iEngy,1) +=    u*((2*nu(0,0) + lambda(0,0)) - kn(0,0));
    kxx(BaseType::iEngy,2) +=                                                v*(nu(0,0) - kn(0,0));
    kxx(BaseType::iEngy,3) +=                                                                           kn(0,0);

    // d(Fv)/d(Uy)
    kxy(BaseType::ixMom,0) += -v*lambda(0,1);
    kxy(BaseType::ixMom,2) +=    lambda(0,1);

    kxy(BaseType::iyMom,0) += -u*nu(0,1);
    kxy(BaseType::iyMom,1) +=    nu(0,1);

    kxy(BaseType::iEngy,0) += -u*v*(nu(0,1) + lambda(0,1));
    kxy(BaseType::iEngy,1) +=  v*nu(0,1);
    kxy(BaseType::iEngy,2) +=  u*lambda(0,1);

    // d(Gv)/d(Ux)
    kyx(BaseType::ixMom,0) += -v*nu(1,0);
    kyx(BaseType::ixMom,2) +=    nu(1,0);

    kyx(BaseType::iyMom,0) += -u*lambda(1,0);
    kyx(BaseType::iyMom,1) +=    lambda(1,0);

    kyx(BaseType::iEngy,0) += -u*v*(nu(1,0) + lambda(1,0));
    kyx(BaseType::iEngy,1) +=  v*lambda(1,0);
    kyx(BaseType::iEngy,2) +=  u*nu(1,0);

    // d(Gv)/d(Uy)
    kyy(BaseType::ixMom,0) += -u*nu(1,1);
    kyy(BaseType::ixMom,1) +=    nu(1,1);

    kyy(BaseType::iyMom,0) += -v*(2*nu(1,1) + lambda(1,1));
    kyy(BaseType::iyMom,2) +=    (2*nu(1,1) + lambda(1,1));

    kyy(BaseType::iEngy,0) += -u*u*(nu(1,1) - kn(1,1)) - v*v*((2*nu(1,1) + lambda(1,1)) - kn(1,1)) - e0*kn(1,1);
    kyy(BaseType::iEngy,1) +=    u*(nu(1,1) - kn(1,1));
    kyy(BaseType::iEngy,2) +=                              v*((2*nu(1,1) + lambda(1,1)) - kn(1,1));
    kyy(BaseType::iEngy,3) +=                                                                           kn(1,1);

    // Brenner term
    DLA::MatrixSymS<D,Tq> delta = rhodiff_* nu;

    kxx(BaseType::iCont,0) += delta(0,0);
    kxx(BaseType::ixMom,0) += delta(0,0)*u;
    kxx(BaseType::iyMom,0) += delta(0,0)*v;
    kxx(BaseType::iEngy,0) += delta(0,0)*e0;

    // d(Fv)/d(Uy)
    kxy(BaseType::iCont,0) += delta(0,1);
    kxy(BaseType::ixMom,0) += delta(0,1)*u;
    kxy(BaseType::iyMom,0) += delta(0,1)*v;
    kxy(BaseType::iEngy,0) += delta(0,1)*e0;

    // d(Gv)/d(Ux)
    kyx(BaseType::iCont,0) += delta(1,0);
    kyx(BaseType::ixMom,0) += delta(1,0)*u;
    kyx(BaseType::iyMom,0) += delta(1,0)*v;
    kyx(BaseType::iEngy,0) += delta(1,0)*e0;

    // d(Gv)/d(Uy)
    kyy(BaseType::iCont,0) += delta(1,1);
    kyy(BaseType::ixMom,0) += delta(1,1)*u;
    kyy(BaseType::iyMom,0) += delta(1,1)*v;
    kyy(BaseType::iEngy,0) += delta(1,1)*e0;
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown artificial viscosity model");
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::artificialViscosityFlux(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type Tqg;

  if (visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy)
  {
    MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0;
    MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0;
    MatrixQ<Tq> dudq = 0;

    artificialViscosity( param, x, y, time, q, nu_xx, nu_xy, nu_yx, nu_yy );

    BaseType::jacobianMasterState( x, y, time, q, dudq );
    ArrayQ<Tqg> ux = dudq*qx;
    ArrayQ<Tqg> uy = dudq*qy;

    static_assert( BaseType::iCont == 0 &&
                   BaseType::iEngy > BaseType::iCont &&
                   BaseType::iEngy > BaseType::ixMom &&
                   BaseType::iEngy > BaseType::iyMom, "Below loops assume energy is last");

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
    {
      fx(i) -= nu_xx(i,i)*ux(i) + nu_xy(i,i)*uy(i);
      fy(i) -= nu_yx(i,i)*ux(i) + nu_yy(i,i)*uy(i);
    }

    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      fx(BaseType::iEngy) -= nu_xx(BaseType::iEngy,i)*ux(i) + nu_xy(BaseType::iEngy,i)*uy(i);
      fy(BaseType::iEngy) -= nu_yx(BaseType::iEngy,i)*ux(i) + nu_yy(BaseType::iEngy,i)*uy(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eLaplaceViscosityEnergy)
  {
    MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0;
    MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0;
    MatrixQ<Tq> dudq = 0;

    artificialViscosity( param, x, y, time, q, nu_xx, nu_xy, nu_yx, nu_yy );

    BaseType::jacobianMasterState( x, y, time, q, dudq );
    ArrayQ<Tqg> ux = dudq*qx;
    ArrayQ<Tqg> uy = dudq*qy;

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      fx(i) -= nu_xx(i,i)*ux(i) + nu_xy(i,i)*uy(i);
      fy(i) -= nu_yx(i,i)*ux(i) + nu_yy(i,i)*uy(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eShearViscosity)
  {
    Tq rho=0, u=0, v=0, t=0;
    Tqg rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;

    DLA::MatrixSymS<D,Tf> eps; //Artificial viscosity
    artificialViscosity( param, x, y, time, q, eps );

    Real Cp = gas_.Cp();

    DLA::MatrixSymS<D,Tf> k=0;
    for (int d = 0; d < D; d++)
      k(d,d) = eps(d,d)*Cp/Prandtl_;

    qInterpret_.eval( q, rho, u, v, t );
    qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
    qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

    DLA::MatrixSymS<D,Tf> lambda = -2./3. * eps;

    Tf tauxx = eps(0,0)*(2*ux   ) + lambda(0,0)*(ux + vy);
    Tf tauyx = eps(1,0)*(uy + vx);
    Tf tauyy = eps(1,1)*(2*vy   ) + lambda(1,1)*(ux + vy);

    fx(BaseType::ixMom) -= tauxx;
    fx(BaseType::iyMom) -= tauyx;
    fx(BaseType::iEngy) -= k(0,0)*tx + u*tauxx + v*tauyx;

    fy(BaseType::ixMom) -= tauyx;
    fy(BaseType::iyMom) -= tauyy;
    fy(BaseType::iEngy) -= k(1,1)*ty + u*tauyx + v*tauyy;
  }
  else if (visc_ == EulerArtViscosity::eBrennerViscosity)
  {
    typedef typename promote_Surreal<Tq,Tf>::type T;

    Tq rho=0, u=0, v=0, t=0;
    Tqg rhox=0, rhoy=0, ux=0, uy=0, vx=0, vy=0, tx=0, ty=0;

    DLA::MatrixSymS<D,Tf> eps; //Artificial viscosity
    artificialViscosity( param, x, y, time, q, eps );

    Real Cp = gas_.Cp();

    DLA::MatrixSymS<D,Tf> k=0;
    for (int d = 0; d < D; d++)
      k(d,d) = eps(d,d)*Cp/Prandtl_;

    qInterpret_.eval( q, rho, u, v, t );
    qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
    qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );
    Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v);

    DLA::MatrixSymS<D,Tf> lambda = -2./3. * eps;

    Tf tauxx = eps(0,0)*(2*ux   ) + lambda(0,0)*(ux + vy);
    Tf tauyx = eps(1,0)*(uy + vx);
    Tf tauyy = eps(1,1)*(2*vy   ) + lambda(1,1)*(ux + vy);

    fx(BaseType::ixMom) -= tauxx;
    fx(BaseType::iyMom) -= tauyx;
    fx(BaseType::iEngy) -= k(0,0)*tx + u*tauxx + v*tauyx;

    fy(BaseType::ixMom) -= tauyx;
    fy(BaseType::iyMom) -= tauyy;
    fy(BaseType::iEngy) -= k(1,1)*ty + u*tauyx + v*tauyy;

    // Brenner term
    DLA::MatrixSymS<D,T> delta = rhodiff_/rho*eps;
    fx(BaseType::iCont) -= delta(0,0)      * rhox + delta(1,0)      * rhoy;
    fx(BaseType::ixMom) -= delta(0,0) * u  * rhox + delta(1,0) * u  * rhoy;
    fx(BaseType::iyMom) -= delta(0,0) * v  * rhox + delta(1,0) * v  * rhoy;
    fx(BaseType::iEngy) -= delta(0,0) * e0 * rhox + delta(1,0) * e0 * rhoy;

    fy(BaseType::iCont) -= delta(0,1)      * rhox + delta(1,1)      * rhoy;
    fy(BaseType::ixMom) -= delta(0,1) * u  * rhox + delta(1,1) * u  * rhoy;
    fy(BaseType::iyMom) -= delta(0,1) * v  * rhox + delta(1,1) * v  * rhoy;
    fy(BaseType::iEngy) -= delta(0,1) * e0 * rhox + delta(1,1) * e0 * rhoy;
  }
  else if (visc_ == EulerArtViscosity::eHolst2020)
  {
    typedef typename promote_Surreal<Tq,Tg>::type Tqg;
    MatrixQ<Tqg> nu_xx = 0.0, nu_xy = 0.0;
    MatrixQ<Tqg> nu_yx = 0.0, nu_yy = 0.0;
    MatrixQ<Tq> dudq = 0;

    artificialViscosity( param, x, y, time, q, qx, qy, nu_xx, nu_xy, nu_yx, nu_yy );

    BaseType::jacobianMasterState( x, y, time, q, dudq );
    ArrayQ<Tqg> ux = dudq*qx;
    ArrayQ<Tqg> uy = dudq*qy;

    static_assert( BaseType::iCont == 0 &&
                   BaseType::iEngy > BaseType::iCont &&
                   BaseType::iEngy > BaseType::ixMom &&
                   BaseType::iEngy > BaseType::iyMom, "Below loops assume energy is last");

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
    {
      fx(i) -= nu_xx(i,i)*ux(i) + nu_xy(i,i)*uy(i);
      fy(i) -= nu_yx(i,i)*ux(i) + nu_yy(i,i)*uy(i);
    }

    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      fx(BaseType::iEngy) -= nu_xx(BaseType::iEngy,i)*ux(i) + nu_xy(BaseType::iEngy,i)*uy(i);
      fy(BaseType::iEngy) -= nu_yx(BaseType::iEngy,i)*ux(i) + nu_yy(BaseType::iEngy,i)*uy(i);
    }

    // Holst add diffusion to the SA equation too...
    for (int i = BaseType::iEngy+1; i < N; i++)
    {
      fx(i) -= nu_xx(i,i)*ux(i) + nu_xy(i,i)*uy(i);
      fy(i) -= nu_yx(i,i)*ux(i) + nu_yy(i,i)*uy(i);
    }
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown artificial viscosity model");

}

// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::fluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  artificialViscosityFlux( param, x, y, time, q, qx, qy, fx, fy );
  BaseType::fluxViscous( x, y, time, q, qx, qy, fx, fy );
}

// perturbation to viscous flux from dux, duy
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Tgu, class Tf>
void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::perturbedGradFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
    ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type Tqg;

  if (visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy)
  {
    MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0;
    MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0;
    MatrixQ<Tq> dudq = 0;

    BaseType::jacobianMasterState( x, y, time, q, dudq );
    ArrayQ<Tqg> dux = dudq*dqx;
    ArrayQ<Tqg> duy = dudq*dqy;

    artificialViscosity( param, x, y, time, q, nu_xx, nu_xy, nu_yx, nu_yy );

    static_assert( BaseType::iCont == 0 &&
                   BaseType::iEngy > BaseType::iCont &&
                   BaseType::iEngy > BaseType::ixMom &&
                   BaseType::iEngy > BaseType::iyMom, "Below loops assume energy is last");

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
    {
      df(i) -= nu_xx(i,i)*dux(i) + nu_xy(i,i)*duy(i);
      dg(i) -= nu_yx(i,i)*dux(i) + nu_yy(i,i)*duy(i);
    }

    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      df(BaseType::iEngy) -= nu_xx(BaseType::iEngy,i)*dux(i) + nu_xy(BaseType::iEngy,i)*duy(i);
      dg(BaseType::iEngy) -= nu_yx(BaseType::iEngy,i)*dux(i) + nu_yy(BaseType::iEngy,i)*duy(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eLaplaceViscosityEnergy)
  {
    MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0;
    MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0;
    MatrixQ<Tq> dudq = 0;

    BaseType::jacobianMasterState( x, y, time, q, dudq );
    ArrayQ<Tqg> dux = dudq*dqx;
    ArrayQ<Tqg> duy = dudq*dqy;

    artificialViscosity( param, x, y, time, q, nu_xx, nu_xy, nu_yx, nu_yy );

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      df(i) -= nu_xx(i,i)*dux(i) + nu_xy(i,i)*duy(i);
      dg(i) -= nu_yx(i,i)*dux(i) + nu_yy(i,i)*duy(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eHolst2020)
  {
    MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0;
    MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0;
    MatrixQ<Tq> dudq = 0;

    BaseType::jacobianMasterState( x, y, time, q, dudq );
    ArrayQ<Tqg> dux = dudq*dqx;
    ArrayQ<Tqg> duy = dudq*dqy;

    artificialViscosity( param, x, y, time, q, qx, qy, nu_xx, nu_xy, nu_yx, nu_yy );

    static_assert( BaseType::iCont == 0 &&
                   BaseType::iEngy > BaseType::iCont &&
                   BaseType::iEngy > BaseType::ixMom &&
                   BaseType::iEngy > BaseType::iyMom, "Below loops assume energy is last");

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
    {
      df(i) -= nu_xx(i,i)*dux(i) + nu_xy(i,i)*duy(i);
      dg(i) -= nu_yx(i,i)*dux(i) + nu_yy(i,i)*duy(i);
    }

    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      df(BaseType::iEngy) -= nu_xx(BaseType::iEngy,i)*dux(i) + nu_xy(BaseType::iEngy,i)*duy(i);
      dg(BaseType::iEngy) -= nu_yx(BaseType::iEngy,i)*dux(i) + nu_yy(BaseType::iEngy,i)*duy(i);
    }

    // Holst add diffusion to the SA equation too...
    for (int i = BaseType::iEngy+1; i < N; i++)
    {
      df(i) -= nu_xx(i,i)*dux(i) + nu_xy(i,i)*duy(i);
      dg(i) -= nu_yx(i,i)*dux(i) + nu_yy(i,i)*duy(i);
    }
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("Unknown artificial viscosity model");
  }
  BaseType::perturbedGradFluxViscous(x, y, time, q, qx, qy, dqx, dqy, df, dg);
}
// perturbation to viscous flux from dux, duy
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tk, class Tgu, class Tf>
void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::perturbedGradFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
    const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
    ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
{

  if (visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy)
  {
    static_assert( BaseType::iCont == 0 &&
                   BaseType::iEngy > BaseType::iCont &&
                   BaseType::iEngy > BaseType::ixMom &&
                   BaseType::iEngy > BaseType::iyMom, "Below loops assume energy is last");

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
    {
      df(i) -= Kxx(i,i)*dux(i) + Kxy(i,i)*duy(i);
      dg(i) -= Kyx(i,i)*dux(i) + Kyy(i,i)*duy(i);
    }

    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      df(BaseType::iEngy) -= Kxx(BaseType::iEngy,i)*dux(i) + Kxy(BaseType::iEngy,i)*duy(i);
      dg(BaseType::iEngy) -= Kyx(BaseType::iEngy,i)*dux(i) + Kyy(BaseType::iEngy,i)*duy(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eLaplaceViscosityEnergy)
  {
    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      df(i) -= Kxx(i,i)*dux(i) + Kxy(i,i)*duy(i);
      dg(i) -= Kyx(i,i)*dux(i) + Kyy(i,i)*duy(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eHolst2020)
  {
    static_assert( BaseType::iCont == 0 &&
                   BaseType::iEngy > BaseType::iCont &&
                   BaseType::iEngy > BaseType::ixMom &&
                   BaseType::iEngy > BaseType::iyMom, "Below loops assume energy is last");

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
    {
      df(i) -= Kxx(i,i)*dux(i) + Kxy(i,i)*duy(i);
      dg(i) -= Kyx(i,i)*dux(i) + Kyy(i,i)*duy(i);
    }

    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      df(BaseType::iEngy) -= Kxx(BaseType::iEngy,i)*dux(i) + Kxy(BaseType::iEngy,i)*duy(i);
      dg(BaseType::iEngy) -= Kyx(BaseType::iEngy,i)*dux(i) + Kyy(BaseType::iEngy,i)*duy(i);
    }

    // Holst add diffusion to the SA equation too...
    for (int i = BaseType::iEngy+1; i < N; i++)
    {
      df(i) -= Kxx(i,i)*dux(i) + Kxy(i,i)*duy(i);
      dg(i) -= Kyx(i,i)*dux(i) + Kyy(i,i)*duy(i);
    }
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("Unknown artificial viscosity model");
  }
  BaseType::perturbedGradFluxViscous(x, y, time, Kxx, Kxy, Kyx, Kyy, dux, duy, df, dg);
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::fluxViscous(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  ArrayQ<Tf> fxL = 0, fxR = 0;
  ArrayQ<Tf> fyL = 0, fyR = 0;

  artificialViscosityFlux(paramL, x, y, time, qL, qxL, qyL, fxL, fyL);
  artificialViscosityFlux(paramR, x, y, time, qR, qxR, qyR, fxR, fyR);

  //Compute the average artificial viscosity flux from both sides
  fn += 0.5*(fxL + fxR)*nx + 0.5*(fyL + fyR)*ny;

  //Add on any interface viscous fluxes from the base class
  BaseType::fluxViscous( x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
}


// space-time viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tqg, class Tf>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
fluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qy, const ArrayQ<Tqg>& qt,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
{
  SANS_ASSERT(visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy); //space-time diffusion is Laplacian only

  typedef typename promote_Surreal<Tq,Tqg>::type T;

  MatrixQ<Tf> nu_xx = 0.0, nu_xy = 0.0, nu_xt = 0.0;
  MatrixQ<Tf> nu_yx = 0.0, nu_yy = 0.0, nu_yt = 0.0;
  MatrixQ<Tf> nu_tx = 0.0, nu_ty = 0.0, nu_tt = 0.0;
  MatrixQ<Tq> dudq = 0;

  // Add space-time artificial viscosity
  artificialViscositySpaceTime( x, y, time, q, param,
                                nu_xx, nu_xy, nu_xt,
                                nu_yx, nu_yy, nu_yt,
                                nu_tx, nu_ty, nu_tt );

  BaseType::jacobianMasterState( x, y, time, q, dudq );
  ArrayQ<T> ux = dudq*qx;
  ArrayQ<T> uy = dudq*qy;
  ArrayQ<T> ut = dudq*qt;

  static_assert( BaseType::iCont == 0 &&
                 BaseType::iEngy > BaseType::iCont &&
                 BaseType::iEngy > BaseType::ixMom &&
                 BaseType::iEngy > BaseType::iyMom, "Below loops assume energy is last");

  // Expand out the matmul to reduce computational time
  for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
  {
    f(i) -= nu_xx(i,i)*ux(i) + nu_xy(i,i)*uy(i) + nu_xt(i,i)*ut(i);
    g(i) -= nu_yx(i,i)*ux(i) + nu_yy(i,i)*uy(i) + nu_yt(i,i)*ut(i);
    h(i) -= nu_tx(i,i)*ux(i) + nu_ty(i,i)*uy(i) + nu_tt(i,i)*ut(i);
  }

  for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
  {
    f(BaseType::iEngy) -= nu_xx(BaseType::iEngy,i)*ux(i) + nu_xy(BaseType::iEngy,i)*uy(i) + nu_xt(BaseType::iEngy,i)*ut(i);
    g(BaseType::iEngy) -= nu_yx(BaseType::iEngy,i)*ux(i) + nu_yy(BaseType::iEngy,i)*uy(i) + nu_yt(BaseType::iEngy,i)*ut(i);
    h(BaseType::iEngy) -= nu_tx(BaseType::iEngy,i)*ux(i) + nu_ty(BaseType::iEngy,i)*uy(i) + nu_tt(BaseType::iEngy,i)*ut(i);
  }

  BaseType::fluxViscousSpaceTime( x, y, time, q, qx, qy, qt, f, g, h );
}

// space-time viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tqg, class Tf>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::fluxViscousSpaceTime(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qyL, const ArrayQ<Tqg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qyR, const ArrayQ<Tqg>& qtR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{
  SANS_ASSERT(visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy); //space-time diffusion is Laplacian only

  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;
  ArrayQ<Tf> hL = 0, hR = 0;

  fluxViscousSpaceTime(paramL, x, y, time, qL, qxL, qyL, qtL, fL, gL, hL);
  fluxViscousSpaceTime(paramR, x, y, time, qR, qxR, qyR, qtR, fR, gR, hR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny + 0.5*(hL + hR)*nt;
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::diffusionViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  if (visc_ == EulerArtViscosity::eHolst2020)
  {
    artificialViscosity( param, x, y, time, q, qx, qy,
                         kxx, kxy, kyx, kyy );
  }
  else
  {
    artificialViscosity( param, x, y, time, q,
                         kxx, kxy, kyx, kyy );
  }

  BaseType::diffusionViscous( x, y, time,
                              q, qx, qy,
                              kxx, kxy,
                              kyx, kyy );
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
inline std::vector<std::string>
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
derivedQuantityNames() const
{
  std::vector<std::string> names = {
                                    "epsilon_density_xx",
                                    "epsilon_density_xy",
                                    "epsilon_density_yx",
                                    "epsilon_density_yy",
                                    "epsilon_x-mom_xx",
                                    "epsilon_x-mom_xy",
                                    "epsilon_x-mom_yx",
                                    "epsilon_x-mom_yy",
                                    "epsilon_y-mom_xx",
                                    "epsilon_y-mom_xy",
                                    "epsilon_y-mom_yx",
                                    "epsilon_y-mom_yy",
                                    "epsilon_energy_xx",
                                    "epsilon_energy_xy",
                                    "epsilon_energy_yx",
                                    "epsilon_energy_yy",
                                    "hxx",
                                    "hxy",
                                    "hyx",
                                    "hyy"
                                   };

  // Add a pseudo-sensor for Holst model
  if (visc_ == EulerArtViscosity::eHolst2020)
  {
    names.push_back("Pseudo-Sensor");
  }

  //Append any names from the base class
  std::vector<std::string> basenames = BaseType::derivedQuantityNames();
  names.insert(names.end(), basenames.begin(), basenames.end());

  return names;
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template<class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
derivedQuantity(const int& index,
                const ParamTuple<DLA::MatrixSymS<2,Real>, Real, TupleClass<>>& param,
                const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  int nQtyCurrent = derivedQuantityNames().size() - BaseType::derivedQuantityNames().size();
  if (index >= nQtyCurrent)
  {
    BaseType::derivedQuantity(index - nQtyCurrent, x, y, time, q, qx, qy, J);
    return;
  }

  // Most of the time we need this...
  MatrixQ<Tf> kxx = 0.0;
  MatrixQ<Tf> kxy = 0.0;
  MatrixQ<Tf> kyx = 0.0;
  MatrixQ<Tf> kyy = 0.0;
  if (visc_ == EulerArtViscosity::eHolst2020)
  {
    artificialViscosity( param, x, y, time,
                         q, qx, qy,
                         kxx, kxy, kyx, kyy );
  }
  else
  {
    artificialViscosity( param, x, y, time, q,
                         kxx, kxy, kyx, kyy );
  }

  switch (index)
  {

  case 0:
  {
    J = kxx(0,0);
    break;
  }

  case 1:
  {
    J = kxy(0,0);
    break;
  }

  case 2:
  {
    J = kyx(0,0);
    break;
  }

  case 3:
  {
    J = kyy(0,0);
    break;
  }

  case 4:
  {
    J = kxx(1,1);
    break;
  }

  case 5:
  {
    J = kxy(1,1);
    break;
  }

  case 6:
  {
    J = kyx(1,1);
    break;
  }

  case 7:
  {
    J = kyy(1,1);
    break;
  }

  case 8:
  {
    J = kxx(2,2);
    break;
  }

  case 9:
  {
    J = kxy(2,2);
    break;
  }

  case 10:
  {
    J = kyx(2,2);
    break;
  }

  case 11:
  {
    J = kyy(2,2);
    break;
  }

  case 12:
  {
    J = kxx(3,3);
    break;
  }

  case 13:
  {
    J = kxy(3,3);
    break;
  }

  case 14:
  {
    J = kyx(3,3);
    break;
  }

  case 15:
  {
    J = kyy(3,3);
    break;
  }

  case 16:
  {
    DLA::MatrixSymS<2, Real> H = exp(param.left());
    J = H(0,0);
    break;
  }

  case 17:
  {
    DLA::MatrixSymS<2, Real> H = exp(param.left());
    J = H(1,0);
    break;
  }

  case 18:
  {
    DLA::MatrixSymS<2, Real> H = exp(param.left());
    J = H(0,1);
    break;
  }

  case 19:
  {
    DLA::MatrixSymS<2, Real> H = exp(param.left());
    J = H(1,1);
    break;
  }

  case 20:
  {
    // Add a pseudo-sensor for Holst model
    if (visc_ == EulerArtViscosity::eHolst2020)
    {
      Tf kappa = 3.0;

      DLA::MatrixSymS<2,Real> H = exp(param.left()); //generalized h-tensor
      DLA::MatrixSymS<2,Real> M = H*H;

      Tq rho = 0, u = 0, v = 0, t = 0, p = 0;
      qInterpret_.eval( q, rho, u, v, t );
      p  = gas_.pressure(rho, t);

      Tf rhox = 0, ux = 0, vx = 0, tx = 0;
      Tf rhoy = 0, uy = 0, vy = 0, ty = 0;
      Tf px = 0, py = 0;
      qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
      qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );
      gas_.pressureGradient( rho, t, rhox, tx, px );
      gas_.pressureGradient( rho, t, rhoy, ty, py );
      DLA::VectorS<D, Tf> gradp = {px, py};
      Tf sensor = sqrt(Transpose(gradp)*M*gradp);
      sensor = sensor / (sensor + kappa * p);
      sensor = sensor * tanh(10.0*sensor);
      J = sensor;
    }
    break;
  }

  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEEulermitAVDiffusion2D::derivedQuantity - Invalid index!");
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template<class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
derivedQuantity(const int& index, const DLA::MatrixSymS<2, Real>& param,
                const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  int nQtyCurrent = derivedQuantityNames().size() - BaseType::derivedQuantityNames().size();
  if (index >= nQtyCurrent)
  {
    BaseType::derivedQuantity(index - nQtyCurrent, x, y, time, q, qx, qy, J);
    return;
  }

  // Most of the time we need this...
  MatrixQ<Tf> kxx = 0.0;
  MatrixQ<Tf> kxy = 0.0;
  MatrixQ<Tf> kyx = 0.0;
  MatrixQ<Tf> kyy = 0.0;
  if (visc_ == EulerArtViscosity::eHolst2020)
  {
    artificialViscosity( param, x, y, time,
                         q, qx, qy,
                         kxx, kxy, kyx, kyy );
  }
  else
  {
    artificialViscosity( param, x, y, time, q,
                         kxx, kxy, kyx, kyy );
  }

  switch (index)
  {

  case 0:
  {
    J = kxx(0,0);
    break;
  }

  case 1:
  {
    J = kxy(0,0);
    break;
  }

  case 2:
  {
    J = kyx(0,0);
    break;
  }

  case 3:
  {
    J = kyy(0,0);
    break;
  }

  case 4:
  {
    J = kxx(1,1);
    break;
  }

  case 5:
  {
    J = kxy(1,1);
    break;
  }

  case 6:
  {
    J = kyx(1,1);
    break;
  }

  case 7:
  {
    J = kyy(1,1);
    break;
  }

  case 8:
  {
    J = kxx(2,2);
    break;
  }

  case 9:
  {
    J = kxy(2,2);
    break;
  }

  case 10:
  {
    J = kyx(2,2);
    break;
  }

  case 11:
  {
    J = kyy(2,2);
    break;
  }

  case 12:
  {
    J = kxx(3,3);
    break;
  }

  case 13:
  {
    J = kxy(3,3);
    break;
  }

  case 14:
  {
    J = kyx(3,3);
    break;
  }

  case 15:
  {
    J = kyy(3,3);
    break;
  }

  case 16:
  {
    DLA::MatrixSymS<2, Real> H = exp(param);
    J = H(0,0);
    break;
  }

  case 17:
  {
    DLA::MatrixSymS<2, Real> H = exp(param);
    J = H(1,0);
    break;
  }

  case 18:
  {
    DLA::MatrixSymS<2, Real> H = exp(param);
    J = H(0,1);
    break;
  }

  case 19:
  {
    DLA::MatrixSymS<2, Real> H = exp(param);
    J = H(1,1);
    break;
  }

  case 20:
  {
    // Add a pseudo-sensor for Holst model
    if (visc_ == EulerArtViscosity::eHolst2020)
    {
      Tf kappa = 3.0;

      DLA::MatrixSymS<2,Real> H = exp(param); //generalized h-tensor
      DLA::MatrixSymS<2,Real> M = H*H;

      Tq rho = 0, u = 0, v = 0, t = 0, p = 0;
      qInterpret_.eval( q, rho, u, v, t );
      p  = gas_.pressure(rho, t);

      Tf rhox = 0, ux = 0, vx = 0, tx = 0;
      Tf rhoy = 0, uy = 0, vy = 0, ty = 0;
      Tf px = 0, py = 0;
      qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
      qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );
      gas_.pressureGradient( rho, t, rhox, tx, px );
      gas_.pressureGradient( rho, t, rhoy, ty, py );
      DLA::VectorS<D, Tf> gradp = {px, py};
      Tf sensor = sqrt(Transpose(gradp)*M*gradp);
      sensor = sensor / (sensor + kappa * p);
      sensor = sensor * tanh(10.0*sensor);
      J = sensor;
    }
    break;
  }

  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEEulermitAVDiffusion2D::derivedQuantity - Invalid index!");
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template<class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::
derivedQuantity(const int& index, const DLA::MatrixSymS<3, Real>& param,
                const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  int nQtyCurrent = derivedQuantityNames().size() - BaseType::derivedQuantityNames().size();
  if (index >= nQtyCurrent)
  {
    BaseType::derivedQuantity(index - nQtyCurrent, x, y, time, q, qx, qy, J);
    return;
  }

  // Most of the time we need this...
  MatrixQ<Tf> kxx = 0.0;
  MatrixQ<Tf> kxy = 0.0;
  MatrixQ<Tf> kxt = 0.0;
  MatrixQ<Tf> kyx = 0.0;
  MatrixQ<Tf> kyy = 0.0;
  MatrixQ<Tf> kyt = 0.0;
  MatrixQ<Tf> ktx = 0.0;
  MatrixQ<Tf> kty = 0.0;
  MatrixQ<Tf> ktt = 0.0;

  artificialViscositySpaceTime( x, y, time, q, param,
                                kxx, kxy, kxt,
                                kyx, kyy, kyt,
                                ktx, kxy, ktt );

  switch (index)
  {

  case 0:
  {
    J = kxx(0,0);
    break;
  }

  case 1:
  {
    J = kxy(0,0);
    break;
  }

  case 2:
  {
    J = kyx(0,0);
    break;
  }

  case 3:
  {
    J = kyy(0,0);
    break;
  }

  case 4:
  {
    J = kxx(1,1);
    break;
  }

  case 5:
  {
    J = kxy(1,1);
    break;
  }

  case 6:
  {
    J = kyx(1,1);
    break;
  }

  case 7:
  {
    J = kyy(1,1);
    break;
  }

  case 8:
  {
    J = kxx(2,2);
    break;
  }

  case 9:
  {
    J = kxy(2,2);
    break;
  }

  case 10:
  {
    J = kyx(2,2);
    break;
  }

  case 11:
  {
    J = kyy(2,2);
    break;
  }

  case 12:
  {
    J = kxx(3,3);
    break;
  }

  case 13:
  {
    J = kxy(3,3);
    break;
  }

  case 14:
  {
    J = kyx(3,3);
    break;
  }

  case 15:
  {
    J = kyy(3,3);
    break;
  }

  case 16:
  {
    DLA::MatrixSymS<3, Real> H = exp(param);
    J = H(0,0);
    break;
  }

  case 17:
  {
    DLA::MatrixSymS<3, Real> H = exp(param);
    J = H(1,0);
    break;
  }

  case 18:
  {
    DLA::MatrixSymS<3, Real> H = exp(param);
    J = H(0,1);
    break;
  }

  case 19:
  {
    DLA::MatrixSymS<3, Real> H = exp(param);
    J = H(1,1);
    break;
  }

  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEEulermitAVDiffusion2D::derivedQuantity - Invalid index!");
  }
}

// strong form of viscous flux: d(Fv)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
template <class Tp, class Tq, class Tg, class Th, class Tf>
inline void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::strongFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
//  SANS_DEVELOPER_EXCEPTION("PDEEulermitAVDiffusion2D<NDPDE>::strongFluxViscous is not implemented");
//  MatrixQ<Tf> kxx = 0.0;
//  MatrixQ<Tf> kxy = 0.0;
//  MatrixQ<Tf> kyx = 0.0;
//  MatrixQ<Tf> kyy = 0.0;
//  MatrixQ<Tq> dudq = 0;
//
//  BaseType::jacobianMasterState( x, y, time, q, dudq );
//  // missing d2u /dq2 qx^2 terms..
//  ArrayQ<Tq> uxx = dudq*qxx;
//  ArrayQ<Tq> uxy = dudq*qxy;
//  ArrayQ<Tq> uyy = dudq*qyy;
//
//  artificialViscosity( param, x, y, time, q,
//                       kxx, kxy, kyx, kyy );
//
//  strongPDE -= kxx*uxx + (kxy + kyx)*uxy + kyy*uyy;

  BaseType::strongFluxViscous( x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE );
}

template <template <class> class PDETraitsSize, class PDETraitsModel,
          template< template <class> class, class> class PDEBase>
void
PDEEulermitAVDiffusion2D<PDETraitsSize, PDETraitsModel, PDEBase>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEEulermitAVDiffusion2D2D: pde_ =" << std::endl;
  BaseType::dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEEULER2D_ARTIFICIALVISCOSITY2D_H
