// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Q1DPrimitiveRhoPressure.h"
#include "Q1DPrimitiveSurrogate.h"
#include "Q1DEntropy.h"
#include "Q1DConservative.h"

#include "TraitsEulerArtificialViscosity.h"
#include "BCEulermitAVSensor1D.h"
#include "PDEEuler1D_Source.h"

#include "Fluids1D_Sensor.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux1D.h"
#include "pde/ArtificialViscosity/AVSensor_Source1D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor1D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
// Instantiate BC parameters

typedef AVSensor_AdvectiveFlux1D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux1D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;


// Pressure-primitive variables
typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> PDEBaseRhoPressure;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDEBaseRhoPressure>> SensorSourceRhoPressure;
typedef TraitsModelArtificialViscosity<PDEBaseRhoPressure, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceRhoPressure> TraitsModelAVRhoPressure;


typedef BCEulermitAVSensor1DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelAVRhoPressure > BCVector_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_S )

// Pressure-primitive variables
typedef PDEEuler1D_Source<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> PDEBaseRhoPressure2;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDEBaseRhoPressure2>> SensorSourceRhoPressure2;
typedef TraitsModelArtificialViscosity<PDEBaseRhoPressure2, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceRhoPressure2>
        TraitsModelAVRhoPressure2;

typedef BCEulermitAVSensor1DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelAVRhoPressure2 > BCVector_RhoP_S2;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_S2 )


// Conservative Variables
typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypeConservative, GasModel>> PDEBaseCons;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDEBaseCons>> SensorSourceCons;
typedef TraitsModelArtificialViscosity<PDEBaseCons, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceCons> TraitsModelAVCons;

typedef BCEulermitAVSensor1DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelAVCons > BCVector_Cons_S;
BCPARAMETER_INSTANTIATE( BCVector_Cons_S )


#if 0
// Entropy Variables
typedef BCEulermitAVSensor1DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelEuler<QTypeEntropy, GasModel> > BCVector_Entropy_S;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_S )
#endif

// Surrogate Variables
typedef PDEEulermitAVDiffusion1D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>> PDEBaseSurrogate;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDEBaseSurrogate>> SensorSourceSurrogate;
typedef TraitsModelArtificialViscosity<PDEBaseSurrogate, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceSurrogate> TraitsModelAVSurrogate;

typedef BCEulermitAVSensor1DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelAVSurrogate > BCVector_Surrogate_S;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_S )



// Surrogate Variables
typedef PDEEuler1D_Source<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>> PDESrcBaseSurrogate;
typedef AVSensor_Source1D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD1, PDESrcBaseSurrogate>> SensorSourceSurrogate2;
typedef TraitsModelArtificialViscosity<PDESrcBaseSurrogate, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceSurrogate2> TraitsModelAVSurrogate2;

typedef BCEulermitAVSensor1DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelAVSurrogate2 > BCVector_Surrogate_S2;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_S2 )


} //namespace SANS
