// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBOLTZMANN2D_H
#define BCBOLTZMANN2D_H

// 1-D Euler BC class

//PyDict must be included first
#include <boost/mpl/vector.hpp>
#include <boost/mpl/vector/vector30.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "TraitsBoltzmann2D.h"
#include "PDEBoltzmann2D.h"

#include "BCBoltzmann2D_BounceBackDirichlet.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

/*class BCTypeNone;
class BCTypeTimeOut;
class BCTypeReflect;
class BCTypeInflowSupersonic;
class BCTypeOutflowSubsonic_Pressure;*/

template <class BCType, class PDEBoltzmann2D>
class BCBoltzmann2D;

template <class BCType>
struct BCBoltzmann2DParams;


template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCBoltzmann2DVector = boost::mpl::vector3<
    BCBoltzmann2D<BCTypeNone, PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmann2D<BCTypeDiffuseKinetic, PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmann2D<BCTypeDiffuseKineticDensityOutlet, PDEBoltzmann2D<PDETraitsSize, PDETraitsModel>>
                                                 >;

} //namespace SANS

#endif  // BCBOLTZMANN2D_H
