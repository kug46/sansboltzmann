// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNAVIERSTOKES2D_H
#define BCNAVIERSTOKES2D_H

// 2-D compressible Navier-Stokes BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsNavierStokes.h"
#include "PDENavierStokes2D.h"
#include "PDENavierStokes_Brenner2D.h"
#include "BCEuler2D.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D compressible Navier-Stokes
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeWallNoSlipAdiabatic_mitState;
class BCTypeWallNoSlipIsothermal_mitState;
class BCTypeSymmetry_mitState;

template <class BCType, class PDENavierStokes2D>
class BCNavierStokes2D;

template <class BCType>
struct BCNavierStokes2DParams;


template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCNavierStokes2DVector = boost::mpl::vector13<
    BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState,  PDENavierStokes2D<PDETraitsSize, PDETraitsModel> >,
    BCNavierStokes2D< BCTypeWallNoSlipIsothermal_mitState, PDENavierStokes2D<PDETraitsSize, PDETraitsModel> >,
    BCNavierStokes2D< BCTypeSymmetry_mitState,             PDENavierStokes2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeFunction_mitState,                    PDENavierStokes2D<PDETraitsSize, PDETraitsModel> >,
    BCNone<PhysD2,PDETraitsSize<PhysD2>::N>,
    BCEuler2D< BCTypeOutflowSubsonic_Pressure_mitState,    PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeFullState_mitState,                   PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeInflowSubsonic_PtTta_mitState,        PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeInflowSubsonic_sHqt_mitState,         PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeInflowSubsonic_sHqt_BN,               PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeOutflowSubsonic_Pressure_BN,          PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeFunctionInflow_sHqt_mitState,         PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeFunctionInflow_sHqt_BN,               PDEEuler2D<PDETraitsSize, PDETraitsModel> >
                                                 >;



template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCNavierStokesBrenner2DVector = boost::mpl::vector12<
    BCNone<PhysD2,PDETraitsSize<PhysD2>::N>,
    BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState, PDENavierStokes2D<PDETraitsSize, PDETraitsModel> >,
    BCNavierStokes2D< BCTypeSymmetry_mitState,            PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeOutflowSubsonic_Pressure_mitState,   PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeFullState_mitState,                  PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeInflowSubsonic_PtTta_mitState,       PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeInflowSubsonic_sHqt_mitState,        PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeInflowSubsonic_sHqt_BN,              PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeOutflowSubsonic_Pressure_BN,         PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeFunction_mitState,                   PDENavierStokes_Brenner2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeFunctionInflow_sHqt_mitState,        PDEEuler2D<PDETraitsSize, PDETraitsModel> >,
    BCEuler2D< BCTypeFunctionInflow_sHqt_BN,              PDEEuler2D<PDETraitsSize, PDETraitsModel> >
                                                 >;



//----------------------------------------------------------------------------//
// Conventional PX-style No Slip, Adiabatic BC:
//
// specify: velocity, boundary-normal heat flux
//----------------------------------------------------------------------------//

template<>
struct BCNavierStokes2DParams<BCTypeWallNoSlipAdiabatic_mitState> : noncopyable
{
  static constexpr const char* BCName{"WallNoSlipAdiabatic_mitState"};
  struct Option
  {
    const DictOption WallNoSlipAdiabatic_mitState{BCNavierStokes2DParams::BCName, BCNavierStokes2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCNavierStokes2DParams params;
};


template <class PDE_>
class BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDE_> :
    public BCType< BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeWallNoSlipAdiabatic_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCNavierStokes2DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 2;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCNavierStokes2D( const PDE& pde ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter())  {}
  ~BCNavierStokes2D() {}

  BCNavierStokes2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}

  BCNavierStokes2D( const BCNavierStokes2D& ) = delete;
  BCNavierStokes2D& operator=( const BCNavierStokes2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


// BC state
template <class PDE>
template <class T>
inline void
BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDE>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rhoI = 0, uI = 0, vI = 0, tI = 0;

  // Set boundary state such that pressure matches interior
  qInterpret_.eval( qI, rhoI, uI, vI, tI );
  T pI = gas_.pressure(rhoI, tI);

  qInterpret_.setFromPrimitive( qB, DensityVelocityPressure2D<T>( rhoI, 0.0, 0.0, pI ) );
}


// normal BC flux
template <class PDE>
template <class T>
inline void
BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0;
  pde_.fluxViscous(x, y, time, qB, qIx, qIy, fv, gv);

  Fn += fv*nx + gv*ny;

  // No energy flux at wall
  Fn[pde_.iEngy] = 0;
}


// normal BC flux
template <class PDE>
template <class Tp, class T>
inline void
BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0;
  pde_.fluxViscous(param, x, y, time, qB, qIx, qIy, fv, gv);

  Fn += fv*nx + gv*ny;

  // No energy flux at wall
  Fn[pde_.iEngy] = 0;
}


template <class PDE>
void
BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes2D<BCTypeWallNoSlipAdiabatic_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX-style Slip BC:
//
// specify: normal velocity
//----------------------------------------------------------------------------//


template<>
struct BCNavierStokes2DParams<BCTypeSymmetry_mitState> : noncopyable
{
  static constexpr const char* BCName{"Symmetry_mitState"};
  struct Option
  {
    const DictOption Symmetry_mitState{BCNavierStokes2DParams::BCName, BCNavierStokes2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCNavierStokes2DParams params;
};


template <class PDE_>
class BCNavierStokes2D<BCTypeSymmetry_mitState, PDE_> :
    public BCEuler2D<BCTypeSymmetry_mitState, PDE_>
{
public:
  typedef BCEuler2D<BCTypeSymmetry_mitState, PDE_> BaseType;
  typedef PhysD2 PhysDim;
  typedef BCTypeSymmetry_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCNavierStokes2DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  using BaseType::D;   // physical dimensions
  using BaseType::N;   // total solution variables

  static const int NBC = 2;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCNavierStokes2D( const PDE& pde ) : BaseType(pde)  {}
  ~BCNavierStokes2D() {}

  BCNavierStokes2D(const PDE& pde, const PyDict& d ) : BaseType(pde, d) {}

  BCNavierStokes2D& operator=( const BCNavierStokes2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state
  using BaseType::state;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  using BaseType::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BaseType::pde_;
  using BaseType::gas_;
  using BaseType::qInterpret_;   // solution variable interpreter class
};


// normal BC flux
template <class PDE>
template <class T>
inline void
BCNavierStokes2D<BCTypeSymmetry_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Zero out any possible stabilization
  Fn[pde_.iEngy] = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

//  T rhox, ux, vx, tx;
//  T rhoy, uy, vy, ty;
////  qInterpret_.eval( q, rho, u, v, t );
//  qInterpret_.evalGradient( qI, qIx, rhox, ux, vx, tx );
//  qInterpret_.evalGradient( qI, qIy, rhoy, uy, vy, ty );
//
//  // subtract off the normal gradient
//  T dudn = ux*nx + uy*ny;
//  T dvdn = vx*nx + vy*ny;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0;
  pde_.fluxViscous(x, y, time, qB, qIx, qIy, fv, gv);

  // Compute normal x-momentum and y-momentum equations
  T xMomN = fv[pde_.ixMom]*nx + gv[pde_.ixMom]*ny;
  T yMomN = fv[pde_.iyMom]*nx + gv[pde_.iyMom]*ny;

  // add only momentum viscous flux with zero tau_ns (n-normal, s-tangential coordinates)
  Fn[pde_.ixMom] += (xMomN*nx + yMomN*ny)*nx;
  Fn[pde_.iyMom] += (xMomN*nx + yMomN*ny)*ny;
}


// normal BC flux
template <class PDE>
template <class Tp, class T>
inline void
BCNavierStokes2D<BCTypeSymmetry_mitState, PDE>::
fluxNormal( const Tp& param,
            const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Zero out any possible stabilization
  Fn[pde_.iEngy] = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0;
  pde_.fluxViscous(param, x, y, time, qB, qIx, qIy, fv, gv);

  // Compute normal x-momentum and y-momentum equations
  T xMomN = fv[pde_.ixMom]*nx + gv[pde_.ixMom]*ny;
  T yMomN = fv[pde_.iyMom]*nx + gv[pde_.iyMom]*ny;

  // add only momentum viscous flux with zero tau_ns (n-normal, s-tangential coordinates)
  Fn[pde_.ixMom] += (xMomN*nx + yMomN*ny)*nx;
  Fn[pde_.iyMom] += (xMomN*nx + yMomN*ny)*ny;
}


template <class PDE>
void
BCNavierStokes2D<BCTypeSymmetry_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCNavierStokes2D<BCTypeSymmetry_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes2D<BCTypeSymmetry_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes2D<BCTypeSymmetry_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// Conventional PX-style No Slip, Isothermal BC:
//
// specify: temperature
//----------------------------------------------------------------------------//

template<>
struct BCNavierStokes2DParams<BCTypeWallNoSlipIsothermal_mitState> : noncopyable
{
  const ParameterNumeric<Real> Twall{"Twall", NO_DEFAULT, NO_RANGE, "Wall Temperature"};
  const ParameterBool Upwind{"Upwind", false, "Upwind the Advective Flux"};

  static constexpr const char* BCName{"WallNoSlipIsothermal_mitState"};
  struct Option
  {
    const DictOption WallNoSlipIsothermal_mitState{BCNavierStokes2DParams::BCName, BCNavierStokes2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCNavierStokes2DParams params;
};


template <class PDE_>
class BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDE_> :
    public BCType< BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeWallNoSlipIsothermal_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCNavierStokes2DParams<BCTypeWallNoSlipIsothermal_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCNavierStokes2D( const PDE& pde, const Real& Twall, const bool upwind = false ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    Twall_(Twall),
    upwind_(upwind)
  {
    // Nothing
  }

  ~BCNavierStokes2D() {}

  BCNavierStokes2D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    Twall_(d.get(ParamsType::params.Twall)),
    upwind_(d.get(ParamsType::params.Upwind))
  {
    // Nothing
  }

  BCNavierStokes2D( const BCNavierStokes2D& ) = delete;
  BCNavierStokes2D& operator=( const BCNavierStokes2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, qI, qB);
  }


  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real Twall_;
  const bool upwind_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDE>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rhoI = 0, uI = 0, vI = 0, tI = 0;

  // Set boundary state such that pressure matches interior
  qInterpret_.eval( qI, rhoI, uI, vI, tI );
  T pI = gas_.pressure(rhoI, tI);
  T pB = pI;
  // Temperature is the wall temperature
  T TB = Twall_;
  // Density to match (p,T)
  T rhoB = gas_.density(pB, TB);

  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature2D<T>( rhoB, 0.0, 0.0, TB ) );
}


// normal BC flux
template <class PDE>
template <class T>
inline void
BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Compute the advective flux based on the boundary state


  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);
  Fn += fx*nx + fy*ny;

  if (upwind_)
  {
    MatrixQ<T> dFdu = 0;
    pde_.jacobianFluxAdvectiveAbsoluteValue(x, y, time, qB, nx, ny, dFdu);

    for (int i=0; i<PDE::N; i++)
      dFdu(PDE::iCont, 0) = 0;

    // viscous flux dual consistency term
    MatrixQ<T> uB_qB = 0; // conservation variable jacobians dU(Q)/dQ
    pde_.jacobianMasterState( x, y, time, qB, uB_qB );

    ArrayQ<T> dq = qI - qB;
    ArrayQ<T> du = uB_qB*dq;

    Fn += dFdu*du;

  }

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0;
  pde_.fluxViscous(x, y, time, qB, qIx, qIy, fv, gv);

  Fn += fv*nx + gv*ny;
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  if (upwind_)
  {
    MatrixQ<T> dFdu = 0;
    pde_.jacobianFluxAdvectiveAbsoluteValue(param, x, y, time, qB, nx, ny, dFdu);

    for (int i=0; i<PDE::N; i++)
      dFdu(PDE::iCont, 0) = 0;

    // viscous flux dual consistency term
    MatrixQ<T> uB_qB = 0; // conservation variable jacobians dU(Q)/dQ
    pde_.jacobianMasterState(param, x, y, time, qB, uB_qB );

    ArrayQ<T> dq = qI - qB;
    ArrayQ<T> du = uB_qB*dq;

    Fn += dFdu*du;
  }

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0;
  pde_.fluxViscous(param, x, y, time, qB, qIx, qIy, fv, gv);

  Fn += fv*nx + gv*ny;
}


template <class PDE>
void
BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes2D<BCTypeWallNoSlipIsothermal_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


} //namespace SANS

#endif  // BCNAVIERSTOKES2D_H
