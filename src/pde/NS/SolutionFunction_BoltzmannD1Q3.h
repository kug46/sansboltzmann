// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_BOLTZMANND1Q3_H
#define SOLUTIONFUNCTION_BOLTZMANND1Q3_H

// quasi 1-D Euler PDE: exact and MMS solutions

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // exp

#include "tools/SANSnumerics.h"     // Real

#include "PDEBoltzmannD1Q3.h"
#include "pde/AnalyticFunction/Function1D.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"

//#include "TraitsBoltzmannD1Q3.h"

#include "getBoltzmannD1Q3TraitsModel.h"
//#include "LinearAlgebra/AlgebraicEquationSetBase.h"
//#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class QD1Q3;


//----------------------------------------------------------------------------//
// solution: Linear Combination
// template for combining two solution functions to make another
// TODO: not quite sure how this works at the moment...

//template <class SOLN1, class SOLN2>
//class SolutionFunction_BoltzmannD1Q3_Combination
//{
//public:
//  typedef PhysD1 PhysDim;
//
//  template<class T>
//  using ArrayQ = EulerTraits<PhysD1>::ArrayQ<T>;
//
//  struct ParamsType : noncopyable
//  {
//    const ParameterNumeric<Real> a{"a", 1, NO_RANGE, "Coefficient for Function 1"};
//    const ParameterNumeric<Real> b{"b", 1, NO_RANGE, "Coefficient for Function 2"};
//    const ParameterDict SolutionArgs1{"SolutionArgs1", EMPTY_DICT, SOLN1::ParamsType::checkInputs, "Solution function arguments 1"};
//    const ParameterDict SolutionArgs2{"SolutionArgs2", EMPTY_DICT, SOLN2::ParamsType::checkInputs, "Solution function arguments 2"};
//
//    static void checkInputs(PyDict d);
//
//    static ParamsType params;
//  };
//
//  explicit SolutionFunction_BoltzmannD1Q3_Combination( PyDict d ) :
//      a_(d.get(ParamsType::params.a)),
//      f1_(d.get(ParamsType::params.SolutionArgs1)),
//      b_(d.get(ParamsType::params.b)),
//      f2_(d.get(ParamsType::params.SolutionArgs1)) {}
//  explicit SolutionFunction_BoltzmannD1Q3_Combination( const Real& a, const SOLN1& f1, const Real& b, const SOLN2& f2 ) :
//      a_(a), f1_(f1), b_(b), f2_(f2) {}
//
//  template<class T>
//  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
//  {
//    return a_*f1_(x,y,time) + b_*f2_(x,y,time);
//  }
//
//  void dump( int indentSize=0, std::ostream& out = std::cout ) const
//  {
//    std::string indent(indentSize, ' ');
//    out << indent << "SolutionFunction_BoltzmannD1Q3_Combination:  a_, b_ = " << a_ << ", " << b_ << std::endl;
//  }
//
//private:
//  Real a_;
//  SOLN1 f1_;
//  Real b_;
//  SOLN2 f2_;
//};


//----------------------------------------------------------------------------//
// solution: const

struct SolutionFunction_BoltzmannD1Q3_SinX_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> pdf0{"pdf0", NO_DEFAULT, NO_RANGE, "DistributionFunction0"};
  const ParameterNumeric<Real> pdf1{"pdf1", NO_DEFAULT, NO_RANGE, "DistributionFunction1"};
  const ParameterNumeric<Real> pdf2{"pdf2", NO_DEFAULT, NO_RANGE, "DistributionFunction2"};

  static void checkInputs(PyDict d);

  static SolutionFunction_BoltzmannD1Q3_SinX_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_BoltzmannD1Q3_SinX :
    public Function1DVirtualInterface<SolutionFunction_BoltzmannD1Q3_SinX<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD1>>
{
public:
  typedef PhysD1 PhysDim;

  typedef SolutionFunction_BoltzmannD1Q3_SinX_Params ParamsType;

  typedef typename getBoltzmannD1Q3TraitsModel<PDETraitsModel>::type TraitsModel;

  //typedef PDETraitsModel TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef QD1Q3<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_BoltzmannD1Q3_SinX( const PyDict& d ) :
    qInterpret_(GasModel( d.get(ParamsType::params.gasModel) )),
    pdf0_(d.get(ParamsType::params.pdf0)),
    pdf1_(d.get(ParamsType::params.pdf1)),
    pdf2_(d.get(ParamsType::params.pdf2))
  {
    // Nothing
  }

  explicit SolutionFunction_BoltzmannD1Q3_SinX( const GasModel& gas, const Real& pdf0, const Real& pdf1, const Real& pdf2  ) :
    qInterpret_(gas),
    pdf0_(pdf0),
    pdf1_(pdf1),
    pdf2_(pdf2)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& time ) const
  {
    ArrayQ<T> q = 0;
    PrimitiveDistributionFunctionsD1Q3<T> dvp( pdf0_, pdf1_, pdf2_ );
    //PrimitiveDistributionFunctionsD1Q3<T> dvp( 0., 0., 0.);
    //std::cout<<"x:"<<x<<std::endl;
    //printf("x=%f\n",x);
    //std::cout << "X has a value: " << x << std::endl;
    if ( false )// x>=0 && x<=2*PI )
    {
      PrimitiveDistributionFunctionsD1Q3<T> dvp1( pdf0_*(1+sin(x)), pdf1_*sin(x), pdf2_*sin(x) );
      qInterpret_.setFromPrimitive(q, dvp1);
    }
/*    if ( false && x <= 6.9714e-05 )
    {
      printf("applying Left BC at x= %f\n",x);
      PrimitiveDistributionFunctionsD1Q3<T> dvpLeft( 2.0, pdf1_, pdf2_);
      qInterpret_.setFromPrimitive(q, dvpLeft);
    }
    else if ( false && x >= 0.99993 )
    {
      printf("applying Right BC at x= %f\n",x);
      PrimitiveDistributionFunctionsD1Q3<T> dvpRight( pdf0_, pdf1_, 1.);
      qInterpret_.setFromPrimitive(q, dvpRight);
    }*/
    else
      qInterpret_.setFromPrimitive(q, dvp);

    return q;
  }

private:
  const QInterpret qInterpret_;
  const Real pdf0_;
  const Real pdf1_;
  const Real pdf2_;
};



} // namespace SANS

#endif  // SOLUTIONFUNCTION_BOLTZMANND1Q3_H
