// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER1D_MITFLUX_ST_H
#define BCEULER1D_MITFLUX_ST_H

// 1-D Euler BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsEuler.h"
#include "BCEuler1D_mitState.h"
#include "PDEEulermitAVDiffusion1D.h"
#include "SolutionFunction_Euler1D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeTimeOut;
class BCTypeTimeIC;
class BCTypeTimeIC_Function;
class BCTypeFullStateSpaceTime_mitState;
class BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState;
class BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState;
class BCTypeInflowSubsonic_PtTt_SpaceTime_mitState;

template <class BCType, class PDEEuler1D>
class BCEuler1D;

template <class BCType>
struct BCEuler1DParams;


//----------------------------------------------------------------------------//
// Conventional Full State Imposed (possibly with characteristics)
//
//----------------------------------------------------------------------------//
template<>
struct BCEuler1DParams<BCTypeTimeOut> : noncopyable
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"TimeOut"};
  struct Option
  {
    const DictOption TimeOut{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static BCEuler1DParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeTimeOut, PDE_> :
    public BCType< BCEuler1D<BCTypeTimeOut, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeTimeOut BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 0;                       // total BCs

  typedef typename PDE::QType QType;
  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde ) :
      pde_(pde),
      qInterpret_(pde_.variableInterpreter())
  {
    // Nothing
  }
  ~BCEuler1D() {}

  BCEuler1D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter())
  {
    // Nothing
  }

  BCEuler1D( const BCEuler1D& ) = delete;
  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, qI, qB);
  }

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& qI ) const
  {
    Real uI;
    qInterpret_.evalVelocity( qI, uI );
    // TimeOut requires outflow
    return (uI*nx + nt) > 0;
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeTimeOut, PDE_>::
state( const Real& x, const Real& time,
       const Real& nx,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}
// normal BC flux
template <class PDE_>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeTimeOut, PDE_>::
fluxNormalSpaceTime( const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  ArrayQ<Tf> ft = 0;
  pde_.fluxAdvectiveTime(x, time, qI, ft);

  Fn += ft*nt;
}

// normal BC flux
template <class PDE_>
template <class T, class Tp, class Tf>
inline void
BCEuler1D<BCTypeTimeOut, PDE_>::
fluxNormalSpaceTime( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  ArrayQ<Tf> ft = 0;
  pde_.fluxAdvectiveTime(param, x, time, qI, ft);

  Fn += ft*nt;
}

template <class PDE_>
void
BCEuler1D<BCTypeTimeOut, PDE_>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler1D<BCTypeTimeOut, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeTimeOut, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// SpaceTime IC BC:
//----------------------------------------------------------------------------//
template<class VariableType1DParams>
struct BCEuler1DTimeICParams : noncopyable
{
  BCEuler1DTimeICParams();

  const ParameterOption<typename VariableType1DParams::VariableOptions>& StateVector;

  static constexpr const char* BCName{"TimeIC"};
  struct Option
  {
    const DictOption TimeIC{BCEuler1DTimeICParams::BCName, BCEuler1DTimeICParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DTimeICParams params;
};

template <class PDE_>
class BCEuler1D<BCTypeTimeIC, PDE_> :
    public BCType< BCEuler1D<BCTypeTimeIC, PDE_> >
{
public:
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DTimeICParams<typename PDE_::VariableType1DParams> ParamsType;
  typedef PDE_ PDE;
  typedef PhysD1 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables
  static const int NBC = N;         // total BCs
  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;   // solution/residual arrays
  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>; // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Varibles>
  BCEuler1D( const PDE& pde, const NSVariableType1D<Varibles>& data ) :
      pde_(pde),
      qB_(pde.setDOFFrom(data))
  {
    // Nothing
  }
  BCEuler1D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qB_(pde.setDOFFrom(d))
  {
    // Nothing
  }
  virtual ~BCEuler1D() {}

  BCEuler1D( const BCEuler1D& ) = delete;
  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, time, qB, uB);
    Fn += nt*uB;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(param, x, time, qB, uB);
    Fn += nt*uB;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const
  {
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout )
  {
    std::string indent(indentSize, ' ');
    out << indent <<
        "BCEuler1D<BCTypeTimeIC, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
    out << indent <<
        "BCEuler1D<BCTypeTimeIC, PDE>: qB_ = " << std::endl;
    qB_.dump(indentSize+2, out);
  }

protected:
  const PDE& pde_;
  const ArrayQ<Real> qB_;
};


//----------------------------------------------------------------------------//
// SpaceTime IC BC with function:
//----------------------------------------------------------------------------//
struct BCEuler1DTimeIC_FunctionParams : BCEulerSolution1DParams
{
  static constexpr const char* BCName{"TimeIC_Function"};
  struct Option
  {
    const DictOption TimeIC_Function{BCEuler1DTimeIC_FunctionParams::BCName, BCEuler1DTimeIC_FunctionParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DTimeIC_FunctionParams params;
};



template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler1D<BCTypeTimeIC_Function, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCEuler1D<BCTypeTimeIC_Function, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCEuler1DTimeIC_FunctionParams ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  typedef PhysD1 PhysDim;
  static const int D = PDE::D; // physical dimensions
  static const int N = PDE::N; // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  BCEuler1D(const PDE& pde, const PyDict& d ) :
    uexact_( BCEulerSolution1D<PDETraitsSize, PDETraitsModel>::getSolution(d) ),
    pde_(pde)
  {
    // Nothing
  }

  BCEuler1D(const PDE& pde, const Function_ptr& uexact ) :
    uexact_(uexact),
    pde_(pde)
  {
    // Nothing
  }

  virtual ~BCEuler1D() {}

  BCEuler1D& operator=( const BCEuler1D& );

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state vector
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = (*uexact_)(x, time);
  }

  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(x, time, qB, uB);
    Fn += nt*uB;
  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param,
                            const Real& x, const Real& time,
                            const Real& nx, const Real& nt,
                            const ArrayQ<T>& qI,
                            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                            const ArrayQ<T>& qB,
                            ArrayQ<Tf>& Fn) const
  {
    ArrayQ<T> uB = 0;
    pde_.fluxAdvectiveTime(param, x, time, qB, uB);
    Fn += nt*uB;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const
  {
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout )
  {
    std::string indent(indentSize, ' ');
    out << indent <<
        "BCEuler1D<BCTypeTimeIC_Function, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
  }

protected:
  const Function_ptr uexact_;
  const PDE& pde_;
};


//----------------------------------------------------------------------------//
// Conventional Full State Imposed (possibly with characteristics) in SpaceTime
//
//----------------------------------------------------------------------------//

template<class VariableType1DParams>
struct BCEuler1DFullStateSpaceTimeParams : noncopyable
{
  BCEuler1DFullStateSpaceTimeParams();

  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};
  const ParameterOption<typename VariableType1DParams::VariableOptions>& StateVector;

  static constexpr const char* BCName{"FullStateSpaceTime"};
  struct Option
  {
    const DictOption FullStateSpaceTime{BCEuler1DFullStateSpaceTimeParams::BCName, BCEuler1DFullStateSpaceTimeParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DFullStateSpaceTimeParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeFullStateSpaceTime_mitState, PDE_> :
    public BCType< BCEuler1D<BCTypeFullStateSpaceTime_mitState, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeFullStateSpaceTime_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DFullStateSpaceTimeParams<typename PDE_::VariableType1DParams> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  template <class Varibles>
  BCEuler1D( const PDE& pde, const NSVariableType1D<Varibles>& data, const bool& upwind ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(data)),
    upwind_(upwind)
  {
    qInterpret_.evalVelocity( qB_, uB_ );
  }

  BCEuler1D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(d)),
    upwind_(d.get(ParamsType::params.Characteristic))
  {
    qInterpret_.evalVelocity( qB_, uB_ );
  }


  ~BCEuler1D() {}

  BCEuler1D( const BCEuler1D& ) = delete;
  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB = qB_;
  }

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, nt, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> qB_;
  const bool upwind_;
  Real uB_;
};

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeFullStateSpaceTime_mitState, PDE>::
fluxNormalSpaceTime( const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwindSpaceTime( x, time, qI, qB, nx, nt, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( x, time, qB, fx );

    ArrayQ<T> ft = 0;
    pde_.fluxAdvectiveTime( x, time, qB, ft);

    Fn += fx*nx + ft*nt;
  }

  // No viscous flux
}

// normal BC flux
template <class PDE>
template <class T, class Tp, class Tf>
inline void
BCEuler1D<BCTypeFullStateSpaceTime_mitState, PDE>::
fluxNormalSpaceTime( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwindSpaceTime( param, x, time, qI, qB, nx, nt, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( param, x, time, qB, fx );

    ArrayQ<T> ft = 0;
    pde_.fluxAdvectiveTime( param, x, time, qB, ft);

    Fn += fx*nx + ft*nt;
  }

  // No viscous flux
}

// is the boundary state valid
template <class PDE>
bool
BCEuler1D<BCTypeFullStateSpaceTime_mitState, PDE>::isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& qI) const
{
  if (upwind_)
  {
    // Characteristic can be either inflow or outflow
    return true;
  }
  else
  {
    Real uI;
    qInterpret_.evalVelocity( qI, uI );
    // Full state requires inflow, so of the dot product between the BC an interior must be positive
    return ((uI*uB_) > 0 && (uB_*nx + nt < 0));
  }

}


//----------------------------------------------------------------------------//
// Conventional PX Style supersonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSupersonic_Pressure_SpaceTime_mitState"};
  struct Option
  {
    const DictOption OutflowSupersonic_Pressure_SpaceTime_mitState{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE_> :
    public BCType< BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 2;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde, const Real pSpec ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    pSpec_(pSpec)
  {
    // Nothing
  }
  ~BCEuler1D() {}

  BCEuler1D( const PDE& pde, const PyDict& d ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    pSpec_(d.get(ParamsType::params.pSpec) )
  {
    // Nothing
  }

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  //const ArrayQ<Real> bcdata_;
  const Real pSpec_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE>::
state( const Real& x, const Real& time,
       const Real& nx,  const Real& nt,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rho, u, t, tnew, pnew = 0;

  // set new boundary state

  //compute Riemann Invariant Jm(q,n) - after Fidkowski 2004
  T gamma = gas_.gamma();
  T R = gas_.R();
  qInterpret_.eval( qI, rho, u, t );
  T p = gas_.pressure(rho, t);
  T s = p / pow(rho, gamma);
  T c = gas_.speedofSound(t);
  T Jp = u*nx + 2*c/(gamma-1);

  pnew = pSpec_;
  T rhonew = pow( (pnew/s), (1./gamma));
  T cnew = sqrt(gamma * pnew / rhonew);
  T vnnew = Jp - 2*cnew / (gamma-1);

  T unew = nx*vnnew;

  tnew = pnew / (rhonew*R);

  qB = 0;
  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature1D<T>( rhonew, unew, tnew ) );
}


template <class PDE>
template <class T, class L, class R>
inline void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE>::
state(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const Real& nx,  const Real& nt,
    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, time, nx, nt, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE>::
fluxNormalSpaceTime( const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  ArrayQ<T> f = 0;
  // Compute the advective flux based on the boundary state
  pde_.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, f);

  Fn += f;
  // No viscous flux
}

// normal BC flux
template <class PDE>
template <class T, class Tp, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE>::
fluxNormalSpaceTime( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  ArrayQ<T> f = 0;
  pde_.fluxAdvectiveUpwindSpaceTime(param, x, time, qI, qB, nx, nt, Fn);

  Fn += f;
  // No viscous flux
}


template <class PDE>
void
BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_Pressure_SpaceTime_mitState, PDE>: pSpec_ = " << pSpec_ << std::endl;
}



//----------------------------------------------------------------------------//
// Conventional PX Style supersonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState> : BCEulerSolution1DParams
{

  static constexpr const char* BCName{"OutflowSupersonic_PressureFunction_SpaceTime_mitState"};
  struct Option
  {
    const DictOption OutflowSupersonic_PressureFunction_SpaceTime_mitState{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE_<PDETraitsSize, PDETraitsModel>> :
    public BCType< BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE_<PDETraitsSize, PDETraitsModel>> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState> ParamsType;
  typedef PDE_<PDETraitsSize, PDETraitsModel> PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 2;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde, const Function_ptr& solution ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    solution_(solution)
  {
    // Nothing
  }
  ~BCEuler1D() {}

  BCEuler1D( const PDE& pde, const PyDict& d ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    solution_( BCEulerSolution1D<PDETraitsSize, PDETraitsModel>::getSolution(d) )
  {
    // Nothing
  }

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Function_ptr solution_;
};


// BC state
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T>
inline void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
state( const Real& x, const Real& time,
       const Real& nx,  const Real& nt,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{

  ArrayQ<T> qS = (*solution_)(x,time);
  T rhoS, uS, tS, pS;
  qInterpret_.eval( qS, rhoS, uS, tS );
  pS = gas_.pressure(rhoS, tS);

  T rho, u, t, tnew = 0;

  // set new boundary state

  //compute Riemann Invariant Jm(q,n) - after Fidkowski 2004
  T gamma = gas_.gamma();
  T R = gas_.R();
  qInterpret_.eval( qI, rho, u, t );
  T p = gas_.pressure(rho, t);
  T s = p / pow(rho, gamma);
  T c = gas_.speedofSound(t);
  T Jp = u*nx + 2*c/(gamma-1);

  T rhonew = pow( (pS/s), (1./gamma));
  T cnew = sqrt(gamma * pS / rhonew);
  T vnnew = Jp - 2*cnew / (gamma-1);

  T unew = nx*vnnew;

  tnew = pS / (rhonew*R);

  qB = 0;
  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature1D<T>( rhonew, unew, tnew ) );
}


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class L, class R>
inline void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
state(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const Real& nx,  const Real& nt,
    const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, time, nx, nt, qI, qB);
}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
fluxNormalSpaceTime( const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  ArrayQ<T> f = 0;
  // Compute the advective flux based on the boundary state
  pde_.fluxAdvectiveUpwindSpaceTime(x, time, qI, qB, nx, nt, f);

  Fn += f;
  // No viscous flux
}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tp, class Tf>
inline void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
fluxNormalSpaceTime( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  ArrayQ<T> f = 0;
  pde_.fluxAdvectiveUpwindSpaceTime(param, x, time, qI, qB, nx, nt, Fn);

  Fn += f;
  // No viscous flux
}


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
void
BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE_<PDETraitsSize, PDETraitsModel>>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler1D<BCTypeOutflowSupersonic_PressureFunction_SpaceTime_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX style subsonic inflow BC for SpaceTime:
//
// specify: Stagnation Temperature and stagnation pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler1DParams<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState> : noncopyable
{
  const ParameterNumeric<Real> TtSpec{"TtSpec", NO_DEFAULT, NO_RANGE, "Stagnation Temperature"};
  const ParameterNumeric<Real> PtSpec{"PtSpec", NO_DEFAULT, NO_RANGE, "Stagnation Pressure"};

  static constexpr const char* BCName{"InflowSubsonic_PtTt_SpaceTime_mitState"};
  struct Option
  {
    const DictOption InflowSubsonic_PtTt_SpaceTime_mitState{BCEuler1DParams::BCName, BCEuler1DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler1DParams params;
};


template <class PDE_>
class BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE_> :
    public BCType< BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeInflowSubsonic_PtTt_SpaceTime_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler1DParams<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 2;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler1D( const PDE& pde, const Real& TtSpec, const Real& PtSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      TtSpec_(TtSpec), PtSpec_(PtSpec)  {}
  ~BCEuler1D() {}

  BCEuler1D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    TtSpec_(d.get(ParamsType::params.TtSpec)),
    PtSpec_(d.get(ParamsType::params.PtSpec)) {}

  BCEuler1D& operator=( const BCEuler1D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx, const Real& nt,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormalSpaceTime( const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
                   const Real& nx, const Real& nt,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:

  // Computes the boundary mach number
  template<class T>
  bool MachB(const Real& nx, const ArrayQ<T>& qI, T& M) const;

  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real TtSpec_;
  const Real PtSpec_;

};

template <class PDE>
template <class T>
inline bool
BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>::
MachB( const Real& nx,
       const ArrayQ<T>& qI, T& M ) const
{

  T rho=0, u=0, t=0;

  //preserve outgoing Riemann Invariant Jp(q,n) - after Fidkowski 2004
  qInterpret_.eval( qI, rho, u, t );

  Real R     = gas_.R();
  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  T c = gas_.speedofSound(t);
  T Jp = u*nx + 2*c/gmi;

  // u*n[0] = c*M*( cos(alpha)*n[0] + sin(alpha)*n[1] ) = c*M*ndir

  Real dn = nx;
  // maximum possible J_plus for real M roots
  Real Jpmax = sqrt(2*(2+gmi*dn*dn)*gamma*R*TtSpec_)/gmi;

  if ( fabs(Jp) > Jpmax )
  {
    // Mach from quadratic equation with Jpmax instead of Jp; note only one root
    M = -dn; // added now, may need to delete
    return true;
  }

  T A =    gamma*R*TtSpec_*dn*dn - gmi/2*Jp*Jp;
  T B = 4.*gamma*R*TtSpec_*dn / gmi;
  T C = 4.*gamma*R*TtSpec_/(gmi*gmi) - Jp*Jp;
  T D = B*B - 4*A*C;

  // Check that we have a valid state
  if ( D < 0 ) return false;

  T Mplus  = (-B + sqrt(D))/(2.*A);
  T Mminus = (-B - sqrt(D))/(2.*A);

  // Pick physically relevant solution
  if ((Mplus >= 0.0) && (Mminus <= 0.0))
  {
    M = Mplus;
  }
  else if ((Mplus <= 0.0) && (Mminus >= 0.0))
  {
    M = Mminus;
  }
  else if ((Mplus > 0.0) && (Mminus > 0.0))
  {
    // not clear which is physical...
    // return invalid state
    return false;
#if 0
    // pick smaller for now
    if ( Mminus <= Mplus )
      M = Mminus;
    else
      M = Mplus;
#endif
  }
  else
  {
    // negative Mach means reverse flow and is OK; choose smallest Mach
    if (fabs(Mminus) < fabs(Mplus))
      M = Mminus;
    else
      M = Mplus;
  }

  // the state is valid
  return true;
}


template <class PDE>
template <class T>
inline void
BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>::
state( const Real& x, const Real& time,
       const Real& nx, const Real& nt,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T M = 0;
  MachB( nx, qI, M );

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  // set boundary state
  T tB   = TtSpec_/(1.+0.5*gmi*M*M);
  T pB   = PtSpec_/pow( 1.+0.5*gmi*M*M, gamma/gmi );
  T rhoB = gas_.density(pB, tB);

  T cB = gas_.speedofSound(tB);
  T VB = cB*M;
  T uB = VB;

  qInterpret_.setFromPrimitive( qB, DensityVelocityPressure1D<T>( rhoB, uB, pB ) );

}


template <class PDE>
template <class T, class TL, class TR>
inline void
BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& time,
       const Real& nx, const Real& nt,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, time, nx, nt, qI, qB);

}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>::
fluxNormalSpaceTime( const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  pde_.fluxAdvectiveUpwindSpaceTime( x, time, qI, qB, nx, nt, Fn );
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>::
fluxNormalSpaceTime( const Tp& param, const Real& x, const Real& time,
            const Real& nx, const Real& nt,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIt,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  pde_.fluxAdvectiveUpwindSpaceTime( param, x, time, qI, qB, nx, nt, Fn );
}

// is the boundary state valid
template <class PDE>
bool
BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>::
isValidState( const Real& nx, const Real& nt, const ArrayQ<Real>& q) const
{
  Real M;
  return MachB( nx, q, M );
}


template <class PDE>
void
BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler1D<BCTypeInflowSubsonic_PtTt_SpaceTime_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // BCEULER1D_MITFLUX_ST_H
