// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEEULERQ1D_SOURCE_H
#define PDEEULERQ1D_SOURCE_H

#include "PDEEulermitAVDiffusion1D.h"

namespace SANS
{

#if 1
template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEEuler1D_Source : public PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>
{
  friend class PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>;
public:
  typedef PhysD1 PhysDim;

  typedef PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;                                        // cartesian vectors

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef typename PDETraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                                 // gas model

  using BaseType::ixMom;
  using BaseType::iEngy;

  typedef typename PDEEuler1D<PDETraitsSize, PDETraitsModel>::EulerResidualInterpCategory EulerResidualInterpCategory;
  typedef std::shared_ptr<Function1DBase<Real>> AreaFunction_ptr;

  bool hasSource() const { return true; }

  explicit PDEEuler1D_Source( const int order, const bool hasSpaceTimeDiffusion, const EulerArtViscosity& visc,
                               GasModel& gas0, EulerResidualInterpCategory cat, const AreaFunction_ptr& area = nullptr,
                               const bool userSource = true ) :

    BaseType( order, hasSpaceTimeDiffusion, visc, gas0, cat, area ),
    userSource_(userSource)
  {
    // Nothing
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq,Tg>::type T;

    if (userSource_ == true && time > 10)
    {

      Tq rho=0, u=0, t=0;

      qInterpret_.eval( q, rho, u, t );

      Real x0 = 0.7;

      Real alpha = 20000.0;

      Real Gauss = sqrt(alpha/PI)*exp( -(x-x0)*(x-x0)*alpha );

      Real Q = 0.0;
      Real Ht = 3.5;

      T dE = rho*u*Q*Ht*Gauss;

      // Temperature Source Here
      src(iEngy) -= dE;

    }

    BaseType::source(param, x, time, q, qx, src);


  }

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tgp>& qpx,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;

    source(x, time, qq, qqx, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& s ) const
  {
    source(param, x, time, q, qx, s); //forward to regular source
  }



protected:

  using BaseType::gas_;
  using BaseType::qInterpret_;
  using BaseType::cat_;
  using BaseType::area_;

  bool userSource_;


};
#endif

} //SANS
#endif /* PDEEULERQ1D_SOURCE_H */
