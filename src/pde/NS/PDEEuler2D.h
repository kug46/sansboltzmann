// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEEULER2D_H
#define PDEEULER2D_H

// 2-D compressible Euler

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "pde/ForcingFunctionBase.h"
#include "Topology/Dimension.h"
#include "tools/smoothmath.h"     // Real

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "EulerResidCategory.h"
#include "GasModel.h"
#include "Roe.h"
#include "IsmailRoe.h"
#include "HLLC2D.h"
#include "NSVariable2D.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q2D;


//----------------------------------------------------------------------------//
// PDE class: 2-D Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. Real)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEEuler2D
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef PDETraitsModel TraitsModel;

  typedef typename PDETraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                     // gas model

  typedef Roe2D<QType, PDETraitsSize> RoeFluxType;
  typedef IsmailRoe2D<QType, PDETraitsSize> IsmailRoeFluxType;

  typedef NSVariableType2DParams VariableType2DParams;

  typedef ForcingFunctionBase2D< PDEEuler2D<PDETraitsSize, PDETraitsModel> > ForcingFunctionType;

  explicit PDEEuler2D( const GasModel& gas0,
                       const EulerResidualInterpCategory cat = Euler_ResidInterp_Raw,
                       const RoeEntropyFix entropyFix = eVanLeer,
                       const std::shared_ptr<ForcingFunctionType>& forcing = NULL) :
    gas_(gas0),
    qInterpret_(gas0),
    cat_(cat),
    roe_(gas0, entropyFix),
    ismailRoe_(gas0, entropyFix),
    entropyFix_(entropyFix),
    forcing_(forcing)
  {
    // Nothing
  }

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iyMom = 2;
  static const int iEngy = 3;

  static_assert( iCont == Roe2D<QType, PDETraitsSize>::iCont, "Must match" );
  static_assert( ixMom == Roe2D<QType, PDETraitsSize>::ixMom, "Must match" );
  static_assert( iyMom == Roe2D<QType, PDETraitsSize>::iyMom, "Must match" );
  static_assert( iEngy == Roe2D<QType, PDETraitsSize>::iEngy, "Must match" );

  static_assert( iCont == IsmailRoe2D<QType, PDETraitsSize>::iCont, "Must match" );
  static_assert( ixMom == IsmailRoe2D<QType, PDETraitsSize>::ixMom, "Must match" );
  static_assert( iyMom == IsmailRoe2D<QType, PDETraitsSize>::iyMom, "Must match" );
  static_assert( iEngy == IsmailRoe2D<QType, PDETraitsSize>::iEngy, "Must match" );

  PDEEuler2D( const PDEEuler2D& pde0 ) = delete;
  PDEEuler2D& operator=( const PDEEuler2D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return forcing_ != NULL && (*forcing_).hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // unsteady temporal flux: Ft(Q)
  template<class Tq, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template<class Tq, class Tf>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const;

  // master state: U(Q)
  template<class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& du ) const;

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& uConsx, ArrayQ<Tf>& uConsy  ) const;

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& uConsxx, ArrayQ<Tf>& uConsxy, ArrayQ<Tf>& uConsyy ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class Tp, class T>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    jacobianMasterState(x, y, time, q, dudq);
  }

  // master state: U(Q)
  template<class Tq, class Tf>
  void adjointState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& V ) const;

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void adjointStateGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy  ) const;

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void adjointStateHessian(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vyy ) const;

  // conversion from entropy variables dudv
  template<class Tq>
  void adjointVariableJacobian(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const;

  // conversion to entropy variables dvdu
  template<class Tq>
  void adjointVariableJacobianInverse(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    fluxAdvective(x, y, time, q, f, g);
  }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor = 1 ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwind(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor = 1 ) const
  {
    fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f, scaleFactor);
  }


  template <class T, class Tqp, class Tf>
  void fluxAdvectiveUpwindLinear(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<Tqp>& dq,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tqp, class Tf>
  void fluxAdvectiveUpwindLinear(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<Tqp>& dq,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    fluxAdvectiveUpwindLinear(x, y, time, qL, dq, nx, ny, f);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tu, class Tf>
  void perturbedFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,  const ArrayQ<Tu>& du,
      ArrayQ<Tf>& dF, ArrayQ<Tf>& dG ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const;


  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const
  {
    jacobianFluxAdvectiveAbsoluteValue(x, y, time, q, nx, ny, a);
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}

  // viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    fluxViscous(x, y, time, q, qx, qy, f, g);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const {}

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const {}

  // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const MatrixQ<Tk>& kxx, const MatrixQ<Tk>& kxy, const MatrixQ<Tk>& kyx, const MatrixQ<Tk>& kyy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const {}

  template <class Tp, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
  {
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const {}

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const {}

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;
    ArrayQ<Tgg> qqy = qy + qpy;

    source(x, y, time, qq, qqx, qqy, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    sourceCoarse(x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    sourceFine(x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, time, q, qx, qy, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}


  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSource( const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const {}

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& divSdotN ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Ts>& div_dsdgradu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradientHACK(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Ts>& div_dsdgradu ) const {}

  // linear change in source in response to linear perturbations du, dux, duy
  template <class Tq, class Tg, class Tu, class Tgu, class Ts>
  void perturbedSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tu>& du, const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, ArrayQ<Ts>& dS ) const {}

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const;

  template <class Tp>
  void forcingFunction( const Tp& param, const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const;

  // characteristic speed (needed for timestep)
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template<class Tp, class Tq, class Tc>
  void speedCharacteristic( const Tp& param, Real x, Real y, Real time, const ArrayQ<Tq>& q, Tc& speed ) const
  {
    speedCharacteristic(x, y, time, q, speed);
  }

  template <class Tq, class Tc>
  inline void speedCharacteristicJacobian( Real, Real, Real,
                                           const ArrayQ<Tq>& q, ArrayQ<Tc>& speed_u) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T, class Varibles>
  void setDOFFrom(
      ArrayQ<T>& q, const NSVariableType2D<Varibles>& data ) const;

  // set from variable
  template<class Varibles>
  ArrayQ<Real> setDOFFrom( const NSVariableType2D<Varibles>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the residual-interpreter type
  EulerResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // accessors for gas model and Q interpreter
  const GasModel& gasModel() const { return gas_; }
  const QInterpret& variableInterpreter() const { return qInterpret_; }

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const GasModel& gas_;
  const QInterpret qInterpret_;               // solution variable interpreter class
  EulerResidualInterpCategory cat_;           // Residual interp type
  const RoeFluxType roe_;                       // Roe upwinding
  const IsmailRoeFluxType ismailRoe_;                       // Roe upwinding
  const RoeEntropyFix entropyFix_;
  const std::shared_ptr<ForcingFunctionType> forcing_;
};


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const
{
  Tq rho = 0, u = 0, v = 0, t = 0, e = 0, E = 0;

  qInterpret_.eval( q, rho, u, v, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v);

  uCons(iCont) = rho;
  uCons(ixMom) = rho*u;
  uCons(iyMom) = rho*v;
  uCons(iEngy) = rho*E;
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tqp, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::perturbedMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& du ) const
{
  qInterpret_.conservativePerturbation(q, dq, du);
}




// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::masterStateGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& uConsx, ArrayQ<Tf>& uConsy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq rho = 0, u = 0, v = 0, t = 0, e = 0, E = 0;
  T rhox = 0, ux = 0, vx = 0, tx = 0, ex = 0;
  T rhoy = 0, uy = 0, vy = 0, ty = 0, ey = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient(q, qx, rhox, ux, vx, tx);
  qInterpret_.evalGradient(q, qy, rhoy, uy, vy, ty);

  e = gas_.energy(rho, t);
  Tq erho = 0;
  Tq et = 0;
  gas_.energyJacobian( rho, t, erho, et );

  ex = et*tx;
  ey = et*ty;

  E = e + 0.5*(u*u + v*v);

  T Ex = ex + (u*ux + v*vx);
  T Ey = ey + (u*uy + v*vy);

  uConsx(iCont) = rhox;
  uConsx(ixMom) = rhox*u + rho*ux;
  uConsx(iyMom) = rhox*v + rho*vx;
  uConsx(iEngy) = rhox*E + rho*Ex;

  uConsy(iCont) = rhoy;
  uConsy(ixMom) = rhoy*u + rho*uy;
  uConsy(iyMom) = rhoy*v + rho*vy;
  uConsy(iEngy) = rhoy*E + rho*Ey;
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::masterStateHessian(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& uConsxx, ArrayQ<Tf>& uConsxy, ArrayQ<Tf>& uConsyy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho, u, v, t = 0;
  Tqg rhox =0, rhoy =0, ux =0, uy =0, vx =0, vy=0, tx=0, ty = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  // evaluate second derivatives
  T rhoxx = 0, rhoxy = 0, rhoyy = 0;
  T uxx = 0, uxy = 0, uyy = 0;
  T vxx = 0, vxy = 0, vyy = 0;
  T txx = 0, txy = 0, tyy = 0;

  qInterpret_.evalSecondGradient( q, qx, qy,
                                  qxx, qxy, qyy,
                                  rhoxx, rhoxy, rhoyy,
                                  uxx, uxy, uyy,
                                  vxx, vxy, vyy,
                                  txx, txy, tyy );

  Tq e = gas_.energy(rho, t);

  Tq erho = 0;
  Tq et = 0;
  gas_.energyJacobian( rho, t, erho, et );

  Tqg ex = et*tx;
  Tqg ey = et*ty;
  T exx = et*txx;
  T exy = et*txy;
  T eyy = et*tyy;

  Tq E = e + 0.5*(u*u + v*v);
  T Ex = ex + (u*ux + v*vx);
  T Ey = ey + (u*uy + v*vy);
  T Exx = exx + (u*uxx + ux*ux + v*vxx + vx*vx);
  T Exy = exy + (u*uxy + uy*ux + v*vxy + vx*vy);
  T Eyy = eyy + (u*uyy + uy*uy + v*vyy + vy*vy);

  uConsxx(iCont) = rhoxx;
  uConsxx(ixMom) = rhoxx*u + 2*rhox*ux + rho*uxx;
  uConsxx(iyMom) = rhoxx*v + 2*rhox*vx + rho*vxx;
  uConsxx(iEngy) = rhoxx*E + 2*rhox*Ex + rho*Exx;

  uConsxy(iCont) = rhoxy;
  uConsxy(ixMom) = rhoxy*u + rhox*uy + rhoy*ux + rho*uxy;
  uConsxy(iyMom) = rhoxy*v + rhox*vy + rhoy*vx + rho*vxy;
  uConsxy(iEngy) = rhoxy*E + rhox*Ey + rhoy*Ex + rho*Exy;

  uConsyy(iCont) = rhoyy;
  uConsyy(ixMom) = rhoyy*u + 2*rhoy*uy + rho*uyy;
  uConsyy(iyMom) = rhoyy*v + 2*rhoy*vy + rho*vyy;
  uConsyy(iEngy) = rhoyy*E + 2*rhoy*Ey + rho*Eyy;
}


// unsteady conservative flux Jacobian: dU(Q)/dQ
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  ArrayQ<T> rho_q, u_q, v_q, t_q, E_q;
  T rho = 0, u = 0, v = 0, t = 0, e = 0, E = 0;
  T e_rho, e_t;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalJacobian( q, rho_q, u_q, v_q, t_q );
  e = gas_.energy(rho, t);
  gas_.energyJacobian(rho, t, e_rho, e_t);

  E = e + 0.5*(u*u + v*v);
  E_q = e_rho*rho_q + e_t*t_q + u*u_q + v*v_q;

  dudq(iCont,0) = rho_q[0];
  dudq(iCont,1) = rho_q[1];
  dudq(iCont,2) = rho_q[2];
  dudq(iCont,3) = rho_q[3];

  dudq(ixMom,0) = rho_q[0]*u + rho*u_q[0];
  dudq(ixMom,1) = rho_q[1]*u + rho*u_q[1];
  dudq(ixMom,2) = rho_q[2]*u + rho*u_q[2];
  dudq(ixMom,3) = rho_q[3]*u + rho*u_q[3];

  dudq(iyMom,0) = rho_q[0]*v + rho*v_q[0];
  dudq(iyMom,1) = rho_q[1]*v + rho*v_q[1];
  dudq(iyMom,2) = rho_q[2]*v + rho*v_q[2];
  dudq(iyMom,3) = rho_q[3]*v + rho*v_q[3];

  dudq(iEngy,0) = rho_q[0]*E + rho*E_q[0];
  dudq(iEngy,1) = rho_q[1]*E + rho*E_q[1];
  dudq(iEngy,2) = rho_q[2]*E + rho*E_q[2];
  dudq(iEngy,3) = rho_q[3]*E + rho*E_q[3];
}



// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::adjointState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, ArrayQ<Tf>& V ) const
{
  Tq rho = 0, u = 0, v = 0, t = 0;

  qInterpret_.eval( q, rho, u, v, t );
  Tq p = gas_.pressure(rho, t);
  Real gamma = gas_.gamma();
  Real gm1 = gamma-1.;
  Real R = gas_.R();

  Tq s = log( p/pow(rho,gamma));
  Real s0 = 0;

  V(iCont) = -(s - s0)/gm1 + (gamma)/gm1 - 0.5*(u*u + v*v)/(R*t);
  V(ixMom) = u/(R*t);
  V(iyMom) = v/(R*t);
  V(iEngy) = -1./(R*t);
}



// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::adjointStateGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq rho = 0, u = 0, v = 0, t = 0;
  T rhox = 0, ux = 0, vx = 0, tx = 0;
  T rhoy = 0, uy = 0, vy = 0, ty = 0;

  Real R = gas_.R();
  Real gamma = gas_.gamma();
  Real gm1 = gamma-1.;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient(q, qx, rhox, ux, vx, tx);
  qInterpret_.evalGradient(q, qy, rhoy, uy, vy, ty);


  T px = 0, py =0;
  Tq p = gas_.pressure(rho, t);
  gas_.pressureGradient(rho, t, rhox, tx, px);
  gas_.pressureGradient(rho, t, rhoy, ty, py);

  T sx = px / p - gamma*rhox/rho;
  T sy = py / p - gamma*rhoy/rho;

  Vx(iCont) = -sx/gm1 - (u*ux + v*vx)/(R*t) + 0.5*(u*u + v*v)*tx/(R*t*t);
  Vx(ixMom) = ux/(R*t)-u*tx/(R*t*t);
  Vx(iyMom) = vx/(R*t)-v*tx/(R*t*t);
  Vx(iEngy) = tx/(R*t*t);

  Vy(iCont) = -sy/gm1 - (u*uy + v*vy)/(R*t) + 0.5*(u*u + v*v)*ty/(R*t*t);
  Vy(ixMom) = uy/(R*t)-u*ty/(R*t*t);
  Vy(iyMom) = vy/(R*t)-v*ty/(R*t*t);
  Vy(iEngy) = ty/(R*t*t);
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::adjointStateHessian(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vyy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho, u, v, t = 0;
  Tqg rhox =0, rhoy =0, ux =0, uy =0, vx =0, vy=0, tx=0, ty = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  // evaluate second derivatives
  T rhoxx = 0, rhoxy = 0, rhoyy = 0;
  T uxx = 0, uxy = 0, uyy = 0;
  T vxx = 0, vxy = 0, vyy = 0;
  T txx = 0, txy = 0, tyy = 0;

  qInterpret_.evalSecondGradient( q, qx, qy,
                                  qxx, qxy, qyy,
                                  rhoxx, rhoxy, rhoyy,
                                  uxx, uxy, uyy,
                                  vxx, vxy, vyy,
                                  txx, txy, tyy );

  Real R = gas_.R();
  Real gamma = gas_.gamma();
  Real gm1 = gamma-1.;

  Tq p = gas_.pressure(rho, t);
  Tqg px = 0, py =0;
  gas_.pressureGradient(rho, t, rhox, tx, px);
  gas_.pressureGradient(rho, t, rhoy, ty, py);

  T pxx = R*(rhoxx*t + 2*rhox*tx + rho*txx);
  T pxy = R*(rhoxy*t + rhox*ty + rhoy*tx + rho*txy);
  T pyy = R*(rhoyy*t + 2*rhoy*ty + rho*tyy);

//  T sx = px / p - gamma*rhox/rho;
//  T sy = py / p - gamma*rhoy/rho;

  T sxx = (pxx*p - px*px)/(p*p) - gamma/(rho*rho)*(rhoxx*rho - rhox*rhox );
  T sxy = (pxy*p - px*py)/(p*p) - gamma/(rho*rho)*(rhoxy*rho - rhox*rhoy );
  T syy = (pyy*p - py*py)/(p*p) - gamma/(rho*rho)*(rhoyy*rho - rhoy*rhoy );


//  Vx(iCont) = sx/gm1 - (u*ux + v*vx)/(R*t) + 0.5*(u*u + v*v)/(R*t*t)*tx;  //rhoE/p = 0.5(u^2 + v^2)/(2*R*t)
//  Vx(ixMom) = ux/(R*t)-u/(R*t*t)*tx;
//  Vx(iyMom) = vx/(R*t)-v/(R*t*t)*tx;
//  Vx(iEngy) = 1./(R*t*t)*tx;
//
//  Vy(iCont) = sy/gm1 - (u*uy + v*vy)/(R*t) + 0.5*(u*u + v*v)/(R*t*t)*ty;
//  Vy(ixMom) = uy/(R*t)-u/(R*t*t)*ty;
//  Vy(iyMom) = vy/(R*t)-v/(R*t*t)*ty;
//  Vy(iEngy) = 1./(R*t*t)*ty;

  Vxx(iCont) = -sxx/gm1 - (u*uxx + ux*ux + vx*vx + v*vxx)/(R*t) + 2*(u*ux + v*vx)*tx/(R*t*t)
                 - (u*u + v*v)*tx*tx/(R*t*t*t)+ 0.5*(u*u + v*v)/(R*t*t)*txx;
  Vxx(ixMom) = uxx/(R*t) - 2*ux*tx/(R*t*t) + 2*u*tx*tx/(R*t*t*t) - u*txx/(R*t*t);
  Vxx(iyMom) = vxx/(R*t) - 2*vx*tx/(R*t*t) + 2*v*tx*tx/(R*t*t*t) - v*txx/(R*t*t);
  Vxx(iEngy) = -2*tx*tx/(R*t*t*t) + txx/(R*t*t);

  Vxy(iCont) = -sxy/gm1 - (u*uxy + ux*uy + vx*vy + v*vxy)/(R*t) + (u*ux + v*vx)*ty/(R*t*t) + (u*uy + v*vy)*tx/(R*t*t)
      - (u*u + v*v)/(R*t*t*t)*tx*ty + 0.5*(u*u + v*v)/(R*t*t)*txy;
  Vxy(ixMom) = uxy/(R*t) - ux*ty/(R*t*t) - uy*tx/(R*t*t) + 2*u*tx*ty/(R*t*t*t) - u*txy/(R*t*t);
  Vxy(iyMom) = vxy/(R*t) - vx*ty/(R*t*t) - vy*tx/(R*t*t) + 2*v*tx*ty/(R*t*t*t) - v*txy/(R*t*t);
  Vxy(iEngy) = -2*tx*ty/(R*t*t*t) + txy/(R*t*t);

  Vyy(iCont) = -syy/gm1 - (u*uyy + uy*uy + vy*vy + v*vyy)/(R*t) + 2*(u*uy + v*vy)*ty/(R*t*t)
      - (u*u + v*v)*ty*ty/(R*t*t*t) + 0.5*(u*u + v*v)/(R*t*t)*tyy;
  Vyy(ixMom) = uyy/(R*t) - 2*uy*ty/(R*t*t) + 2*u*ty*ty/(R*t*t*t) - u*tyy/(R*t*t);
  Vyy(iyMom) = vyy/(R*t) - 2*vy*ty/(R*t*t) + 2*v*ty*ty/(R*t*t*t) - v*tyy/(R*t*t);
  Vyy(iEngy) = -2*ty*ty/(R*t*t*t) + tyy/(R*t*t);
}



// entropy variable jacobian du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::adjointVariableJacobian(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const
{
  //entropy variable interpreter for adjoint
  Tq rho = 0, u = 0, v = 0, t = 0;

  Real gamma = gas_.gamma();
//  Real  s0 = 0;

  qInterpret_.eval( q, rho, u, v, t );
  Tq p  = gas_.pressure(rho, t);

  Tq h = gas_.enthalpy(rho, t);
  Tq H = h + 0.5*(u*u + v*v);

  dudv(0,0) = rho;
  dudv(0,1) = rho*u;
  dudv(0,2) = rho*v;
  dudv(0,3) = rho*H - p;

  dudv(1,0) = dudv(0,1);
  dudv(1,1) = p + rho*u*u;
  dudv(1,2) = rho*u*v;
  dudv(1,3) = rho*u*H;

  dudv(2,0) = dudv(0,2);
  dudv(2,1) = dudv(1,2);
  dudv(2,2) = p+rho*v*v;
  dudv(2,3) = rho*v*H;

  dudv(3,0) = dudv(0,3);
  dudv(3,1) = dudv(1,3);
  dudv(3,2) = dudv(2,3);
  dudv(3,3) = rho*H*H - p*p/rho * (gamma)/(gamma-1);

}



// entropy variable jacobian du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::adjointVariableJacobianInverse(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const
{
  //entropy variable interpreter for adjoint
  Tq rho = 0, u = 0, v = 0, t = 0;

  Real gamma = gas_.gamma();
//  Real  s0 = 0;

  qInterpret_.eval( q, rho, u, v, t );
//  e = gas_.energy(rho, t);
//  E = e + 0.5*(u*u + v*v);
  Tq p  = gas_.pressure(rho, t);

//  Tq h = gas_.enthalpy(rho, t);
//  Tq H = h + 0.5*(u*u + v*v);

//  Tq s = log( p / pow(rho,gamma));

  Tq q2 = u*u + v*v;
  Real gm1 = gamma-1.;
  Tq p2 = p*p;

  dvdu(0,0) = (p2*gamma + 0.25*q2*q2*gm1*gm1*rho*rho )/(p2*gm1*rho);
  dvdu(0,1) = -0.5*u*q2*gm1*rho/p2;
  dvdu(0,2) = -0.5*v*q2*gm1*rho/p2;
  dvdu(0,3) = (-p + 0.5*q2*gm1*rho)/p2;

  dvdu(1,0) =  dvdu(0,1);
  dvdu(1,1) = (p + u*u*gm1*rho)/p2;
  dvdu(1,2) = u*v*gm1*rho/p2;
  dvdu(1,3) = -u*gm1*rho/p2;

  dvdu(2,0) = dvdu(0,2);
  dvdu(2,1) = dvdu(1,2);
  dvdu(2,2) = (p + v*v*gm1*rho)/p2;
  dvdu(2,3) = -v*(gm1)*rho/p2;

  dvdu(3,0) = dvdu(0,3);
  dvdu(3,1) = dvdu(1,3);
  dvdu(3,2) = dvdu(2,3);
  dvdu(3,3) = rho*gm1/p2;

}

// strong form of unsteady conservative flux: dU(Q)/dt
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& utCons ) const
{
  T rho, u, v, t, e, E = 0;
  T rhot, ut, vt, tt, et, Et = 0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qt, rhot, ut, vt, tt);

  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v);

  gas_.energyGradient(rho, t, rhot, tt, et);
  Et = et + u*ut + v*vt;

  utCons(iCont) += rhot;
  utCons(ixMom) += rho*ut + u*rhot;
  utCons(iyMom) += rho*vt + v*rhot;
  utCons(iEngy) += rho*Et + E*rhot;
}


// advective flux: F(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  T rho = 0; T u = 0; T v = 0; T t = 0;
  T p, h, H;

  qInterpret_.eval( q, rho, u, v, t );
  p = gas_.pressure(rho, t);
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);

  f(iCont) += rho*u;
  f(ixMom) += rho*u*u + p;
  f(iyMom) += rho*u*v;
  f(iEngy) += rho*u*H;

  g(iCont) += rho*v;
  g(ixMom) += rho*u*v;
  g(iyMom) += rho*v*v + p;
  g(iEngy) += rho*v*H;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor ) const
{
  if ((entropyFix_ == eVanLeer) or (entropyFix_ == eHarten))
  {
    roe_.fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f, scaleFactor);
  }
  else if ((entropyFix_ == eIsmailRoeEntropyNeutral) or (entropyFix_ == eIsmailRoeHartenFix))
  {
    ismailRoe_.fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f, scaleFactor);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("PDEEuler2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind - Unknown entropy fix.");
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tqp, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindLinear(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<Tqp>& dq,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  if ((entropyFix_ == eVanLeer) or (entropyFix_ == eHarten))
  {
    roe_.fluxAdvectiveUpwindLinear(x, y, time, qL, dq, nx, ny, f);
  }
  else if ((entropyFix_ == eIsmailRoeEntropyNeutral) or (entropyFix_ == eIsmailRoeHartenFix))
  {
    ismailRoe_.fluxAdvectiveUpwindLinear(x, y, time, qL, dq, nx, ny, f);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("PDEEuler2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindLinear - Unknown entropy fix.");
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{
//  roe_.fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tq> fnx = 0;
  fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fnx );

  //Upwind temporal flux
  ArrayQ<Tq> ft = 0;
  if (nt >= 0)
    fluxAdvectiveTime( x, y, time, qL, ft );
  else
    fluxAdvectiveTime( x, y, time, qR, ft );

  f += ft*nt + fnx;
}


// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tu, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::perturbedFluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tu>& du,
    ArrayQ<Tf>& dF, ArrayQ<Tf>& dG ) const
{
  Real gamma = gas_.gamma();
  Tq rho, u, v, t, h, H = 0;

  qInterpret_.eval( q, rho, u, v, t );
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);

  dF[iCont] += du[1];

  dF[ixMom] += (-u*u + 0.5*(gamma - 1)*(u*u + v*v))*du[0];
  dF[ixMom] += -(gamma - 3)*u*du[1];
  dF[ixMom] += - (gamma - 1)*v*du[2];
  dF[ixMom] += (gamma - 1)*du[3];

  dF[iyMom] += - u*v*du[0];
  dF[iyMom] += v*du[1];
  dF[iyMom] += u*du[2];

  dF[iEngy] += - u*(H - 0.5*(gamma - 1)*(u*u + v*v))*du[0];
  dF[iEngy] += (H - (gamma - 1)*u*u)*du[1];
  dF[iEngy] += - (gamma - 1)*u*v*du[2];
  dF[iEngy] += gamma*u*du[3];

  dG[iCont] += du[2];

  dG[ixMom] += - u*v*du[0];
  dG[ixMom] += v*du[1];
  dG[ixMom] += u*du[2];

  dG[iyMom] += (-v*v + 0.5*(gamma - 1)*(u*u + v*v))*du[0];
  dG[iyMom] += - (gamma - 1)*u*du[1];
  dG[iyMom] += - (gamma - 3)*v*du[2];
  dG[iyMom] += (gamma - 1)*du[3];

  dG[iEngy] += - v*(H - 0.5*(gamma - 1)*(u*u + v*v))*du[0];
  dG[iyMom] += - (gamma - 1)*u*v*du[1];
  dG[iyMom] += (H - (gamma - 1)*v*v)*du[2];
  dG[iyMom] += gamma*v*du[3];
}


// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  Real gamma = gas_.gamma();
  Tq rho, u, v, t, h, H = 0;

  qInterpret_.eval( q, rho, u, v, t );
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u + v*v);

  dfdu(iCont,1) += 1;

  dfdu(ixMom,0) += - u*u + 0.5*(gamma - 1)*(u*u + v*v);
  dfdu(ixMom,1) += - (gamma - 3)*u;
  dfdu(ixMom,2) += - (gamma - 1)*v;
  dfdu(ixMom,3) += gamma - 1;

  dfdu(iyMom,0) += - u*v;
  dfdu(iyMom,1) += v;
  dfdu(iyMom,2) += u;

  dfdu(iEngy,0) += - u*(H - 0.5*(gamma - 1)*(u*u + v*v));
  dfdu(iEngy,1) += H - (gamma - 1)*u*u;
  dfdu(iEngy,2) += - (gamma - 1)*u*v;
  dfdu(iEngy,3) += gamma*u;

  dgdu(iCont,2) += 1;

  dgdu(ixMom,0) += - u*v;
  dgdu(ixMom,1) += v;
  dgdu(ixMom,2) += u;

  dgdu(iyMom,0) += - v*v + 0.5*(gamma - 1)*(u*u + v*v);
  dgdu(iyMom,1) += - (gamma - 1)*u;
  dgdu(iyMom,2) += - (gamma - 3)*v;
  dgdu(iyMom,3) += gamma - 1;

  dgdu(iEngy,0) += - v*(H - 0.5*(gamma - 1)*(u*u + v*v));
  dgdu(iEngy,1) += - (gamma - 1)*u*v;
  dgdu(iEngy,2) += H - (gamma - 1)*v*v;
  dgdu(iEngy,3) += gamma*v;
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& Nx, const Real& Ny, MatrixQ<Tf>& mtx ) const
{
  if ((entropyFix_ == eVanLeer) or (entropyFix_ == eHarten))
  {
    Real nMag = sqrt(Nx*Nx + Ny*Ny );
    Real eps0 = 1.e-9;
    Real nMag2 = smoothabsP(nMag, eps0);

    Real nx = Nx/nMag2;
    Real ny = Ny/nMag2;


    Tq rho=0, u=0, v=0, t=0;

    qInterpret_.eval( q, rho, u, v, t );
    Tq c = gas_.speedofSound( t );
    Tq h = gas_.enthalpy( rho, t );
    Tq qsq = u*u + v*v;
    Tq H = h + 0.5*qsq;
    Tq qn = nx*u + ny*v;


    Tq eigm = fabs(qn - c);
    Tq eigp = fabs(qn + c);
    Tq eigu = fabs(qn    );

    //HARTEN ENTROPY FIX
    Real eps1 = 1e-2;
    Tq eps    = c*eps1;

    switch (roe_.entropyFix())
    {
    case eVanLeer:
    {
      if (eigm < 0.5*eps)
        eigm = (eps*eps)/eps + 0.25*eps;

      if (eigp < 0.5*eps)
        eigp = (eps*eps)/eps + 0.25*eps;

      if (eigu < 0.5*eps)
        eigu = (eigu*eigu)/eps + 0.25*eps;

      break;
    }
    case eHarten:
    {
      if (eigm < eps)
        eigm = 0.5*(eps + eigm*eigm/eps);

      if (eigp < eps)
        eigp = 0.5*(eps + eigp*eigp/eps);

      if (eigu < eps)
        eigu = 0.5*(eps + eigu*eigu/eps);

      break;
    }
    default:
      SANS_DEVELOPER_EXCEPTION("Unkown entropy fix.");
    }

    eigm *= nMag;
    eigp *= nMag;
    eigu *= nMag;

    // right acoustic eigenvectors

    ArrayQ<Tq> rvecm=0, rvecp=0;
    rvecm[0] = 1;
    rvecm[1] = u - nx*c;
    rvecm[2] = v - ny*c;
    rvecm[3] = H - qn*c;
    rvecp[0] = 1;
    rvecp[1] = u + nx*c;
    rvecp[2] = v + ny*c;
    rvecp[3] = H + qn*c;

    // left acoustic eigenvectors

    ArrayQ<Tq> lvecm=0, lvecp=0;
    lvecm[0] = qsq/(4*h) + qn/(2*c);
    lvecm[1] = -u/(2*h) - nx/(2*c);
    lvecm[2] = -v/(2*h) - ny/(2*c);
    lvecm[3] = 1./(2*h);
    lvecp[0] = qsq/(4*h) - qn/(2*c);
    lvecp[1] = -u/(2*h) + nx/(2*c);
    lvecp[2] = -v/(2*h) + ny/(2*c);
    lvecp[3] = 1./(2*h);

    // absolute value jacobian

    // premultiply delta for performance
    Tq deigm = eigm - eigu;
    Tq deigp = eigp - eigu;

    rvecm *= deigm;
    rvecp *= deigp;

    mtx(0,0) += eigu;
    mtx(1,1) += eigu;
    mtx(2,2) += eigu;
    mtx(3,3) += eigu;
    for (int i = 0; i < 4; i++)
      for (int j = 0; j < 4; j++)
        mtx(i,j) += rvecm[i]*lvecm[j] + rvecp[i]*lvecp[j];

  }
  else if ((entropyFix_ == eIsmailRoeEntropyNeutral) or (entropyFix_ == eIsmailRoeHartenFix))
  {
    SANS_DEVELOPER_EXCEPTION("PDEEuler2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue - IsmailRoe not implemented");
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("PDEEuler2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue - Unknown entropy fix.");
  }

}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  if ((entropyFix_ == eVanLeer) or (entropyFix_ == eHarten))
  {
    roe_.jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, time, q, nx, ny, nt, mtx);
  }
  else if ((entropyFix_ == eIsmailRoeEntropyNeutral) or (entropyFix_ == eIsmailRoeHartenFix))
  {
    ismailRoe_.jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, time, q, nx, ny, nt, mtx);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION("PDEEuler2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime - Unknown entropy fix.");
  }
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;
  Tq rho = 0, u = 0, v = 0, t = 0, h = 0, H = 0;
  T rhox=0, ux=0, vx=0, tx=0, px=0, hx=0, Hx=0;
  T rhoy=0, uy=0, vy=0, ty=0, py=0, hy=0, Hy=0;

  qInterpret_.eval( q, rho, u, v, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx);
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty);

//  p  = gas_.pressure(rho, t);
  gas_.pressureGradient(rho, t, rhox, tx, px);
  gas_.pressureGradient(rho, t, rhoy, ty, py);

  h  = gas_.enthalpy(rho, t);
  gas_.enthalpyGradient(rho, t, rhox, tx, hx);
  gas_.enthalpyGradient(rho, t, rhoy, ty, hy);

  H = h + 0.5*(u*u + v*v);
  Hx = hx + (ux*u + vx*v);
  Hy = hy + (uy*u + vy*v);

  strongPDE(iCont) += rhox*u   +   rho*ux;
  strongPDE(ixMom) += rhox*u*u + 2*rho*ux*u + px;
  strongPDE(iyMom) += rhox*u*v +   rho*ux*v + rho*u*vx;
  strongPDE(iEngy) += rhox*u*H +   rho*ux*H + rho*u*Hx;

  strongPDE(iCont) += rhoy*v   +   rho*vy;
  strongPDE(ixMom) += rhoy*v*u +   rho*vy*u + rho*v*uy;
  strongPDE(iyMom) += rhoy*v*v + 2*rho*vy*v + py;
  strongPDE(iEngy) += rhoy*v*H +   rho*vy*H + rho*v*Hy;
}

// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const
{
  Tq rho = 0, u = 0, v = 0, t = 0, c = 0;

  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);

  speed = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;
}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const
{
  Tq rho = 0, u = 0, v = 0, t = 0, c = 0;

  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);

  speed = sqrt(u*u + v*v) + c;
}


// characteristic speed Jacobian
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::speedCharacteristicJacobian(
    Real, Real, Real, const ArrayQ<Tq>& q, ArrayQ<Tc>& speed_u) const
{
  Tq rho = 0, u = 0, v = 0, t = 0, c_t = 0;
  qInterpret_.eval( q, rho, u, v, t );
  Tq e = gas_.energy(rho, t);
  Tq E = e + 0.5*(u*u + v*v);
  Real Cv = gas_.Cv();

  Tq u_rho  = -u/rho;
  Tq u_rhou =  1/rho;
  Tq v_rho  = -v/rho;
  Tq v_rhov =  1/rho;

  Tq t_rho  = ( -E/rho + ( u*u + v*v )/rho ) / Cv;
  Tq t_rhou = (        - (   u       )/rho ) / Cv;
  Tq t_rhov = (        - (         v )/rho ) / Cv;
  Tq t_rhoE = (  1/rho                     ) / Cv;

  qInterpret_.eval( q, rho, u, v, t );
  
  gas_.speedofSoundJacobian(t, c_t);

//  speed = sqrt(u*u + v*v) + c -- eps to prevent divide by zero issues
  Tq Vmag_u = u / (sqrt(u*u + v*v) + 1e-16);
  Tq Vmag_v = v / (sqrt(u*u + v*v) + 1e-16);

  speed_u(0) += c_t * t_rho  + Vmag_u * u_rho + Vmag_v * v_rho;
  speed_u(1) += c_t * t_rhou + Vmag_u * u_rhou;
  speed_u(2) += c_t * t_rhov + Vmag_v * v_rhov;
  speed_u(3) += c_t * t_rhoE;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::
updateFraction( const Real& x, const Real& y, const Real& time,
                const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  qInterpret_.updateFraction(q, dq, maxChangeFraction, updateFraction);
}


// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEEuler2D<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return qInterpret_.isValidState(q);
}

// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpret_.setFromPrimitive( q, data, name, nn );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Varibles>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const NSVariableType2D<Varibles>& data ) const
{
  qInterpret_.setFromPrimitive( q, data.cast() );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Varibles>
inline typename PDEEuler2D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEEuler2D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const NSVariableType2D<Varibles>& data ) const
{
  ArrayQ<Real> q;
  qInterpret_.setFromPrimitive( q, data.cast() );
  return q;
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEEuler2D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEEuler2D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q;
  DictKeyPair data = d.get(NSVariableType2DParams::params.StateVector);

  if ( data == NSVariableType2DParams::params.StateVector.Conservative )
    qInterpret_.setFromPrimitive( q, Conservative2D<Real>(data) );
  else if ( data == NSVariableType2DParams::params.StateVector.PrimitivePressure )
    qInterpret_.setFromPrimitive( q, DensityVelocityPressure2D<Real>(data) );
  else if ( data == NSVariableType2DParams::params.StateVector.PrimitiveTemperature )
    qInterpret_.setFromPrimitive( q, DensityVelocityTemperature2D<Real>(data) );
  else
    SANS_DEVELOPER_EXCEPTION(" Missing state vector option ");

  return q;
}


// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{

  int nOut = rsdPDEOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N-1);
    rsdPDEOut[0] = rsdPDEIn[iCont];
    rsdPDEOut[1] = sqrt( rsdPDEIn[ixMom]*rsdPDEIn[ixMom] + rsdPDEIn[iyMom]*rsdPDEIn[iyMom] );
    rsdPDEOut[2] = rsdPDEIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdPDEOut[2+i] = rsdPDEIn[iEngy+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
//    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 2D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now

    Real g = gas_.gamma();
    Real pRef = 1; Real rhoRef = 1; Real uRef = sqrt(g);
    Real rhoE = pRef/(g-1) + 0.5*rhoRef*(uRef*uRef);

    ArrayQ<Real> Vref = 0;
    Vref[0] = (g+1)/(g-1) - rhoE/pRef;
    Vref[1] = rhoRef*uRef/pRef;
    Vref[2] = rhoRef*uRef/pRef;
    Vref[3] = -rhoRef/pRef;

    T tmp = 0;
    tmp += Vref[0]*Vref[0]*rsdPDEIn[0]*rsdPDEIn[0];
    tmp += Vref[1]*Vref[1]*rsdPDEIn[1]*rsdPDEIn[1];
    tmp += Vref[2]*Vref[2]*rsdPDEIn[2]*rsdPDEIn[2];
    tmp += Vref[3]*Vref[3]*rsdPDEIn[3]*rsdPDEIn[3];

    rsdPDEOut[0] = sqrt(tmp);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
    {
      rsdAuxOut[i] = rsdAuxIn[i];
    }
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N-1);
    rsdAuxOut[0] = rsdAuxIn[iCont];
    rsdAuxOut[1] = sqrt(rsdAuxIn[ixMom]*rsdAuxIn[ixMom] + rsdAuxIn[iyMom]*rsdAuxIn[iyMom]);
    rsdAuxOut[2] = rsdAuxIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdAuxOut[2+i] = rsdAuxIn[iEngy+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);

    Real g = gas_.gamma();
    Real pRef = 1; Real rhoRef = 1; Real uRef = sqrt(g);
    Real rhoE = pRef/(g-1) + 0.5*rhoRef*(uRef*uRef);

    ArrayQ<Real> Vref = 0;
    Vref[0] = (g+1)/(g-1) - rhoE/pRef;
    Vref[1] = rhoRef*uRef/pRef;
    Vref[2] = rhoRef*uRef/pRef;
    Vref[4] = -rhoRef/pRef;

    T tmp = 0;
    tmp += Vref[0]*Vref[0]*rsdAuxIn[0]*rsdAuxIn[0];
    tmp += Vref[1]*Vref[1]*rsdAuxIn[1]*rsdAuxIn[1];
    tmp += Vref[2]*Vref[2]*rsdAuxIn[2]*rsdAuxIn[2];
    tmp += Vref[3]*Vref[3]*rsdAuxIn[3]*rsdAuxIn[3];

    rsdAuxOut[0] = sqrt(tmp);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N-1);
    rsdBCOut[0] = rsdBCIn[iCont];
    rsdBCOut[1] = sqrt( rsdBCIn[ixMom]*rsdBCIn[ixMom] + rsdBCIn[iyMom]*rsdBCIn[iyMom] );
    rsdBCOut[2] = rsdBCIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdBCOut[2+i] = rsdBCIn[iEngy+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
//    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 2D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now

    Real g = gas_.gamma();
    Real pRef = 1; Real rhoRef = 1; Real uRef = sqrt(g);
    Real rhoE = pRef/(g-1) + 0.5*rhoRef*(uRef*uRef);

    ArrayQ<Real> Vref = 0;
    Vref[0] = (g+1)/(g-1) - rhoE/pRef;
    Vref[1] = rhoRef*uRef/pRef;
    Vref[2] = rhoRef*uRef/pRef;
    Vref[3] = -rhoRef/pRef;

    T tmp = 0;
    tmp += Vref[0]*Vref[0]*rsdBCIn[0]*rsdBCIn[0];
    tmp += Vref[1]*Vref[1]*rsdBCIn[1]*rsdBCIn[1];
    tmp += Vref[2]*Vref[2]*rsdBCIn[2]*rsdBCIn[2];
    tmp += Vref[3]*Vref[3]*rsdBCIn[3]*rsdBCIn[3];

    rsdBCOut[0] = sqrt(tmp);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// return the interp type
template <template <class> class PDETraitsSize, class PDETraitsModel>
EulerResidualInterpCategory
PDEEuler2D<PDETraitsSize, PDETraitsModel>::category() const
{
  return cat_;
}


// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDEEuler2D<PDETraitsSize, PDETraitsModel>::nMonitor() const
{
  int nMon;

  if (cat_==Euler_ResidInterp_Raw) // raw residual
    nMon = N;
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
    nMon = N-1;
  else if (cat_ == Euler_ResidInterp_Entropy)
    nMon = 1;
  else
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );

  return nMon;
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  (*forcing_)(*this, x, y, time, forcing );
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Tp& param, const Real& x, const Real& y, const Real& time, ArrayQ<Real>& frc ) const
{
  forcingFunction(x, y, time, frc);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDEEuler2D<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  return {
          "Density",
          "X-Velocity",
          "Y-Velocity",
          "Pressure",
          "Temperature",
          "Mach",
          "Entropy",
          "Entropy Equation"
         };
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  Tq rho = 0, u = 0, v = 0, t = 0, c = 0, p = 0, s;
  Real gamma = gas_.gamma();
  qInterpret_.eval( q, rho, u, v, t );
  c = gas_.speedofSound(t);
  p  = gas_.pressure(rho, t);
  s = log( p/pow(rho,gamma));

  ArrayQ<Tq> V = 0;
  adjointState(x,y,time,q,V);

  ArrayQ<Tq> F = 0;
  strongFluxAdvective(x, y, time, q, qx, qy, F );
  Tq vF = dot(V,F);

  switch (index)
  {
  case 0:
  {
    J = rho;
    break;
  }
  case 1:
  {
    J = u;
    break;
  }
  case 2:
  {
    J = v;
    break;
  }
  case 3:
  {
    J = p;
    break;
  }
  case 4:
  {
    J = t;
    break;
  }
  case 5:
  {
    J = sqrt(u*u+v*v)/c;
    break;
  }
  case 6:
  {
    J = s;
    break;
  }
  case 7:
  {
    J = vF;
    break;
  }
  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEEuler2D::derivedQuantity - Invalid index!");
  }
}




template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEEuler2D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEEuler2D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDEEuler2D<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDEEuler2D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // PDEEULER2D_H
