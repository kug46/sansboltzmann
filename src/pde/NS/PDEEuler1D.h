// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEEULER1D_H
#define PDEEULER1D_H

// quasi 1-D compressible Euler

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "EulerResidCategory.h"
#include "GasModel.h"
#include "Roe.h"
#include "NSVariable1D.h"

#include "pde/AnalyticFunction/Function1D.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr

namespace SANS
{

// Forward declare
template <class QType, template <class> class PDETraitsSize>
class Q1D;


//----------------------------------------------------------------------------//
// PDE class: quasi 1-D Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEEuler1D
{
public:

  enum EulerResidualInterpCategory
  {
    Euler_ResidInterp_Raw,
    Euler_ResidInterp_Momentum,
    Euler_ResidInterp_Entropy
  };

  typedef PhysD1 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef PDETraitsModel TraitsModel;

  typedef typename PDETraitsModel::QType QType;
  typedef Q1D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                     // gas model

  typedef Roe1D<QType, PDETraitsSize> FluxType;

  typedef NSVariableType1DParams VariableType1DParams;

  typedef std::shared_ptr<Function1DBase<Real>> AreaFunction_ptr;

  explicit PDEEuler1D( const GasModel& gas0,
                       EulerResidualInterpCategory cat,
                       const AreaFunction_ptr& area = nullptr,
                       const RoeEntropyFix entropyFix = eVanLeer ) :
    gas_(gas0),
    qInterpret_(gas0),
    cat_(cat),
    area_(area),
    flux_(gas0, entropyFix)
  {
    // Nothing
  }

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iEngy = 2;

  static_assert( iCont == Roe1D<QType, PDETraitsSize>::iCont, "Must match" );
  static_assert( ixMom == Roe1D<QType, PDETraitsSize>::ixMom, "Must match" );

  // Non-copyable
  PDEEuler1D( const PDEEuler1D& pde0 ) = delete;
  PDEEuler1D& operator=( const PDEEuler1D& pde0 ) = delete;

  ~PDEEuler1D() {}

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return (area_ != nullptr); }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // unsteady temporal flux: Ft(Q)
  template<class Tp, class T, class Tf>
  void fluxAdvectiveTime(
      const Tp& param,
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(param, x, time, q, ftLocal);

    Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
    ft += A*ftLocal;
  }

  // unsteady temporal flux: Ft(Q)
  template<class T, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, time, q, ftLocal);

    Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
    ft += A*ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template<class Tp, class T, class Tu>
  void masterState(
      const Tp& param,
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
  {
    masterState(x, time, q, uCons);
  }

  // master state: U(Q)
  template<class T, class Tu>
  void masterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of master state wrt q: dU/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param,
      const Real& x, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f ) const
  {
    fluxAdvective(x, time, q, f);
  }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx,  ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
   void fluxAdvectiveUpwind(
       const Tp& param,
       const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
       const Real& nx, ArrayQ<Tf>& f ) const
   {
     fluxAdvectiveUpwind(x, time, qL, qR, nx, f);
   }

  template <class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
  {
    fluxAdvectiveUpwindSpaceTime( x, time, qL, qR, nx, nt, f);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T, class Ta>
  void jacobianFluxAdvective(
      const Real& x, const Real& time, const ArrayQ<T>& q,
      MatrixQ<Ta>& ax ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time, const ArrayQ<T>& q, const Real& nx, const Real& nt,
      MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const {}

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const {}


  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const {}


  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      const ArrayQ<Tgu>& dqx,
      ArrayQ<Tf>& df ) const {}

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx ) const {}

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax ) const {}

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      const ArrayQ<T>& qxx,
      ArrayQ<T>& strongPDE ) const {}

  // space-time strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qtx, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    //Not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tgp>& qpx,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;

    source(x, time, qq, qqx, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& src ) const
  {
    source(x, y, time, q, qp, qx, qy, qpx, qpy, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& s ) const
  {
    source(x, time, q, qx, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL,
      const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSourceAbsoluteValue(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class T>
  void jacobianSourceAbsoluteValue( const Tp& param,
      const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& dsdux ) const {}

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue(
      const Real& x, const Real& time, const Real& nx,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Ts>& divSdotN ) const {}

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& time, ArrayQ<T>& forcing ) const {}

  // characteristic speed (needed for timestep)
  template<class T>
  void speedCharacteristic( Real, Real, Real dx, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template<class T>
  void speedCharacteristic( Real, Real, const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from primitive variable
  template<class T, class PrimitiveVarible>
  void setDOFFrom(
      ArrayQ<T>& q, const NSVariableType1D<PrimitiveVarible>& data ) const;

  // set from primitive variable
  template<class PrimitiveVarible>
  ArrayQ<Real> setDOFFrom( const NSVariableType1D<PrimitiveVarible>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // accessors for gas model and Q interpreter
  const GasModel& gasModel() const { return gas_; }
  const QInterpret& variableInterpreter() const { return qInterpret_; }

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the interp type
  EulerResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const;

  // Exact area and area change
  void area( const Real& x, const Real& time,
             Real& A, Real& dAdx ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const GasModel gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  EulerResidualInterpCategory cat_; // Residual interp type
  const AreaFunction_ptr area_;
  const FluxType flux_;                       // Roe upwinding
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tu>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  T rho = 0, u = 0, t = 0, e = 0, E = 0;

  qInterpret_.eval( q, rho, u, t );
  e  = gas_.energy(rho, t);
  E = e + 0.5*(u*u);

  uCons(iCont) = rho;
  uCons(ixMom) = rho*u;
  uCons(iEngy) = rho*E;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  ArrayQ<T> rho_q, u_q, t_q, E_q;
  T rho = 0, u = 0, t = 0, e = 0, E = 0;
  T e_rho, e_t;

  qInterpret_.eval( q, rho, u, t );
  qInterpret_.evalJacobian( q, rho_q, u_q, t_q );
  e = gas_.energy(rho, t);
  gas_.energyJacobian(rho, t, e_rho, e_t);

  E = e + 0.5*(u*u);
  E_q = e_rho*rho_q + e_t*t_q + u*u_q;

  dudq(iCont,iCont) = rho_q[0];
  dudq(iCont,ixMom) = rho_q[1];
  dudq(iCont,iEngy) = rho_q[2];

  dudq(ixMom,iCont) = rho_q[0]*u + rho*u_q[0];
  dudq(ixMom,ixMom) = rho_q[1]*u + rho*u_q[1];
  dudq(ixMom,iEngy) = rho_q[2]*u + rho*u_q[2];

  dudq(iEngy,iCont) = rho_q[0]*E + rho*E_q[0];
  dudq(iEngy,ixMom) = rho_q[1]*E + rho*E_q[1];
  dudq(iEngy,iEngy) = rho_q[2]*E + rho*E_q[2];
}


// strong form of unsteady conservative flux: dU(Q)/dt
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& utCons ) const
{
  T rho, u, t, e, E = 0;
  T rhot, ut, tt, et, Et = 0;

  qInterpret_.eval( q, rho, u, t );
  qInterpret_.evalGradient( q, qt, rhot, ut, tt);

  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u );

  gas_.energyGradient(rho, t, rhot, tt, et);
  Et = et + u*ut;

  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;

  utCons(iCont) += A*rhot;
  utCons(ixMom) += A*(rho*ut + u*rhot);
  utCons(iEngy) += A*(rho*Et + E*rhot);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f ) const
{
  T rho, u, t, p, h, H = 0;
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;

  qInterpret_.eval( q, rho, u, t );
  p  = gas_.pressure(rho, t);
  h  = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u);

  f(iCont) += A *  rho*u;
  f(ixMom) += A * (rho*u*u + p);
  f(iEngy) += A *  rho*u*H;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, ArrayQ<Tf>& fx ) const
{
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
  ArrayQ<Tf> f = 0;
  flux_.fluxAdvectiveUpwind(x, time, qL, qR, nx, f);

  fx += A*f;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<T> fnx = 0;
  fluxAdvectiveUpwind( x, time, qL, qR, nx, fnx );

  //Upwind temporal flux
  ArrayQ<T> ft = 0;
  if (nt >= 0)
    fluxAdvectiveTime( x, time, qL, ft );
  else
    fluxAdvectiveTime( x, time, qR, ft );

  f += ft*nt + fnx;

}

// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Ta>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& time, const ArrayQ<T>& q,
    MatrixQ<Ta>& ax ) const
{
  Real gamma = gas_.gamma();
  T rho, u, t, h, H = 0;
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;

  qInterpret_.eval( q, rho, u, t );
  h = gas_.enthalpy(rho, t);
  H = h + 0.5*(u*u);

  ax(iCont,iCont) = 0;
  ax(iCont,ixMom) = 1;
  ax(iCont,iEngy) = 0;

  ax(ixMom,iCont) = - u*u + 0.5*(gamma - 1)*(u*u);
  ax(ixMom,ixMom) = - (gamma - 3)*u;
  ax(ixMom,iEngy) = gamma - 1;

  ax(iEngy,iCont) = - u*(H - 0.5*(gamma - 1)*(u*u));
  ax(iEngy,ixMom) = H - (gamma - 1)*u*u;
  ax(iEngy,iEngy) = gamma*u;

  ax *= A;
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const
{
#if 0
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
  SANS_ASSERT( false );
#endif
#if 0
  MatrixQ<T> ax = 0, ay = 0;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  T an = nx*ax + ny*ay;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
#endif
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const
{
#if 0
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;
  SANS_ASSERT( false );
#endif
#if 0
  MatrixQ<T> ax = 0, ay = 0;

  jacobianFluxAdvective(x, y, time, q, ax, ay);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  T an = nx*ax + ny*ay + nt*1;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
#endif
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    ArrayQ<T>& strongPDE ) const
{
  T rho, u, t, h, h0 = 0;
  T rhox, ux, tx, px, hx, h0x = 0;
  Real A = (area_ != nullptr) ? (*area_)(x, time) : 1.0;

  qInterpret_.eval( q, rho, u, t );
  qInterpret_.evalGradient( q, qx,
                            rhox,
                            ux,
                            tx );

//  p  = gas_.pressure(rho, t);
  gas_.pressureGradient(rho, t, rhox, tx, px);

  h  = gas_.enthalpy(rho, t);
  gas_.enthalpyGradient(rho, t, rhox, tx, hx);

  h0 = h + 0.5*(u*u);
  h0x = hx + (ux*u);

  strongPDE(iCont) += A*(rhox*u    +   rho*ux);
  strongPDE(ixMom) +=  A*(rhox*u*u  + 2*rho*ux*u  + px);
  strongPDE(iEngy) += A*(rhox*u*h0 +   rho*ux*h0 + rho*u*h0x);

}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::source(
    const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{
  //
  Tq rho = 0, u = 0, t = 0, p = 0;

  if (area_ != nullptr)
  {
    qInterpret_.eval( q, rho, u, t );
    p = gas_.pressure(rho, t);

    Real A, dAdx, dAdt;
    (*area_).gradient(x, time, A, dAdx, dAdt);
    source(ixMom) -= p * dAdx;
  }
}


// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real dx, const ArrayQ<T>& q, T& speed ) const
{
  T rho = 0, u = 0, t = 0, c = 0;

  qInterpret_.eval( q, rho, u, t );
  c = gas_.speedofSound(t);

  speed = fabs(dx*u)/sqrt(dx*dx) + c;
}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, const ArrayQ<T>& q, T& speed ) const
{
  T rho, u, t, c = 0;

  qInterpret_.eval( q, rho, u, t );
  c = gas_.speedofSound(t);

  speed = fabs(u) + c;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::
updateFraction( const Real& x, const Real& time,
                const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  qInterpret_.updateFraction(q, dq, maxChangeFraction, updateFraction);
}


// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEEuler1D<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return qInterpret_.isValidState(q);
}


// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpret_.setFromPrimitive( q, data, name, nn );
}


// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class PrimitiveVarible>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const NSVariableType1D<PrimitiveVarible>& data ) const
{
  qInterpret_.setFromPrimitive( q, data.cast() );
}


// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class PrimitiveVarible>
inline typename PDEEuler1D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEEuler1D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const NSVariableType1D<PrimitiveVarible>& data ) const
{
  ArrayQ<Real> q;
  qInterpret_.setFromPrimitive( q, data.cast() );
  return q;
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEEuler1D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEEuler1D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q;
  DictKeyPair data = d.get(NSVariableType1DParams::params.StateVector);

  if ( data == NSVariableType1DParams::params.StateVector.Conservative )
    qInterpret_.setFromPrimitive( q, Conservative1D<Real>(data) );
  else if ( data == NSVariableType1DParams::params.StateVector.PrimitivePressure )
    qInterpret_.setFromPrimitive( q, DensityVelocityPressure1D<Real>(data) );
  else if ( data == NSVariableType1DParams::params.StateVector.PrimitiveTemperature )
    qInterpret_.setFromPrimitive( q, DensityVelocityTemperature1D<Real>(data) );
  else
    SANS_DEVELOPER_EXCEPTION(" Missing state vector option ");

  return q;
}


// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  int nOut = rsdPDEOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N);
    rsdPDEOut[0] = rsdPDEIn[iCont];
    rsdPDEOut[1] = sqrt( rsdPDEIn[ixMom]*rsdPDEIn[ixMom] );
    rsdPDEOut[2] = rsdPDEIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdPDEOut[2+i] = rsdPDEIn[iEngy+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1 + (N-3));
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 1D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    rsdPDEOut[0] = rsdPDEIn[0] + rsdPDEIn[1] + rsdPDEIn[2];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
    {
      rsdAuxOut[i] = sqrt(rsdAuxIn[i]*rsdAuxIn[i]);
    }
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N);
    rsdAuxOut[0] = rsdAuxIn[iCont];
    rsdAuxOut[1] = sqrt(rsdAuxIn[ixMom]*rsdAuxIn[ixMom]);
    rsdAuxOut[2] = rsdAuxIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdAuxOut[2+i] = rsdAuxIn[iEngy+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1 + (N-3));
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 1D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    rsdAuxOut[0] = rsdAuxIn[0] + rsdAuxIn[1] + rsdAuxIn[2];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_ASSERT(nOut == N);
    rsdBCOut[0] = rsdBCIn[iCont];
    rsdBCOut[1] = sqrt( rsdBCIn[ixMom]*rsdBCIn[ixMom] );
    rsdBCOut[2] = rsdBCIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdBCOut[2+i] = rsdBCIn[iEngy+i];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1 + (N-3));
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in 1D");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    rsdBCOut[0] = rsdBCIn[0] + rsdBCIn[1] + rsdBCIn[2];
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}

// return the interp type
template <template <class> class PDETraitsSize, class PDETraitsModel>
typename PDEEuler1D<PDETraitsSize, PDETraitsModel>::EulerResidualInterpCategory
PDEEuler1D<PDETraitsSize, PDETraitsModel>::category() const
{
  return cat_;
}


// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDEEuler1D<PDETraitsSize, PDETraitsModel>::nMonitor() const
{
  int nMon;

  if (cat_==Euler_ResidInterp_Raw) // raw residual
    nMon = N;
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
    nMon = N;
  else if (cat_ == Euler_ResidInterp_Entropy)
    nMon = 1 + (N - 3);
  else
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );

  return nMon;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDEEuler1D<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  return {
          // Nothing
         };
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Real& x, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const
{
  switch (index)
  {
  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEEuler1D::derivedQuantity - Invalid index!");
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::area( const Real& x, const Real& time, Real& A, Real& dAdx ) const
{
  A = 1.0;
  dAdx = 0.0;

  if (area_ != nullptr)
  {
    Real dAdt;
    (*area_).gradient(x, time, A, dAdx, dAdt);
//    A = (*area_)(x, time);
//    dAdx = (*area_).gradient(x, time);
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEEuler1D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEEuler1D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDEEuler1D<PDETraitsSize, PDETraitsModel>: qInterpret_ = "  << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDEEuler1D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEEULER1D_H
