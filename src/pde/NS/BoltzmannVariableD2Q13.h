// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BOLTZMANNVARIABLED2Q13_H
#define BOLTZMANNVARIABLED2Q13_H

#include "Python/PyDict.h" // python must be included first
#include "Python/Parameter.h"

#include <initializer_list>

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

namespace SANS
{

//===========================================================================//
template<class Derived>
struct BoltzmannVariableTypeD2Q13
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//===========================================================================//
struct PrimitiveDistributionFunctionsD2Q13Params : noncopyable
{
  const ParameterNumeric<Real> pdf0{"pdf0", NO_DEFAULT, NO_RANGE, "DistributionFunction0"};
  const ParameterNumeric<Real> pdf1{"pdf1", NO_DEFAULT, NO_RANGE, "DistributionFunction1"};
  const ParameterNumeric<Real> pdf2{"pdf2", NO_DEFAULT, NO_RANGE, "DistributionFunction2"};
  const ParameterNumeric<Real> pdf3{"pdf3", NO_DEFAULT, NO_RANGE, "DistributionFunction3"};
  const ParameterNumeric<Real> pdf4{"pdf4", NO_DEFAULT, NO_RANGE, "DistributionFunction4"};
  const ParameterNumeric<Real> pdf5{"pdf5", NO_DEFAULT, NO_RANGE, "DistributionFunction5"};
  const ParameterNumeric<Real> pdf6{"pdf6", NO_DEFAULT, NO_RANGE, "DistributionFunction6"};
  const ParameterNumeric<Real> pdf7{"pdf7", NO_DEFAULT, NO_RANGE, "DistributionFunction7"};
  const ParameterNumeric<Real> pdf8{"pdf8", NO_DEFAULT, NO_RANGE, "DistributionFunction8"};
  const ParameterNumeric<Real> pdf9{"pdf9", NO_DEFAULT, NO_RANGE, "DistributionFunction9"};
  const ParameterNumeric<Real> pdf10{"pdf10", NO_DEFAULT, NO_RANGE, "DistributionFunction10"};
  const ParameterNumeric<Real> pdf11{"pdf11", NO_DEFAULT, NO_RANGE, "DistributionFunction11"};
  const ParameterNumeric<Real> pdf12{"pdf12", NO_DEFAULT, NO_RANGE, "DistributionFunction12"};

  static void checkInputs(PyDict d);

  static PrimitiveDistributionFunctionsD2Q13Params params;
};

//---------------------------------------------------------------------------//
template<class T>
struct PrimitiveDistributionFunctionsD2Q13 : public BoltzmannVariableTypeD2Q13<PrimitiveDistributionFunctionsD2Q13<T>>
{
  typedef PrimitiveDistributionFunctionsD2Q13Params ParamsType;

  PrimitiveDistributionFunctionsD2Q13() {}

  explicit PrimitiveDistributionFunctionsD2Q13( const PyDict& d )
    : DistributionFunction0( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf0) ),
      DistributionFunction1( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf1) ),
      DistributionFunction2( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf2) ),
      DistributionFunction3( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf3) ),
      DistributionFunction4( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf4) ),
      DistributionFunction5( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf5) ),
      DistributionFunction6( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf6) ),
      DistributionFunction7( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf7) ),
      DistributionFunction8( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf8) ),
      DistributionFunction9( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf9) ),
      DistributionFunction10( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf10) ),
      DistributionFunction11( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf11) ),
      DistributionFunction12( d.get(PrimitiveDistributionFunctionsD2Q13Params::params.pdf12) ){}

  PrimitiveDistributionFunctionsD2Q13( const T& pdf0, const T& pdf1, const T& pdf2,
                                      const T& pdf3, const T& pdf4, const T& pdf5,
                                      const T& pdf6, const T& pdf7, const T& pdf8,
                                      const T& pdf9, const T& pdf10, const T& pdf11,
                                      const T& pdf12)
    : DistributionFunction0(pdf0), DistributionFunction1(pdf1), DistributionFunction2(pdf2),
      DistributionFunction3(pdf3), DistributionFunction4(pdf4), DistributionFunction5(pdf5),
      DistributionFunction6(pdf6), DistributionFunction7(pdf7), DistributionFunction8(pdf8),
      DistributionFunction9(pdf9), DistributionFunction10(pdf10), DistributionFunction11(pdf11),
      DistributionFunction12(pdf12){}

  PrimitiveDistributionFunctionsD2Q13& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == 13);
    auto q = s.begin();
    DistributionFunction0 = *q; q++;
    DistributionFunction1 = *q; q++;
    DistributionFunction2 = *q; q++;
    DistributionFunction3 = *q; q++;
    DistributionFunction4 = *q; q++;
    DistributionFunction5 = *q; q++;
    DistributionFunction6 = *q; q++;
    DistributionFunction7 = *q; q++;
    DistributionFunction8 = *q; q++;
    DistributionFunction9 = *q; q++;
    DistributionFunction10 = *q; q++;
    DistributionFunction11 = *q; q++;
    DistributionFunction12 = *q;

    return *this;
  }

  T DistributionFunction0;
  T DistributionFunction1;
  T DistributionFunction2;
  T DistributionFunction3;
  T DistributionFunction4;
  T DistributionFunction5;
  T DistributionFunction6;
  T DistributionFunction7;
  T DistributionFunction8;
  T DistributionFunction9;
  T DistributionFunction10;
  T DistributionFunction11;
  T DistributionFunction12;
};

//===========================================================================//
struct BoltzmannVariableTypeD2Q13Params : noncopyable
{
  struct VariableOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Variables{"Variables", NO_DEFAULT, "Name of the variable set" };
    const ParameterString& key = Variables;

    const DictOption PrimitiveDistributionFunctions13{"PrimitiveDistributionFunction13", PrimitiveDistributionFunctionsD2Q13Params::checkInputs};

    const std::vector<DictOption> options{PrimitiveDistributionFunctions13};
  };
  const ParameterOption<VariableOptions> StateVector{"StateVector", NO_DEFAULT, "The state vector of variables"};

  static void checkInputs(PyDict d);

  static BoltzmannVariableTypeD2Q13Params params;
};


}

#endif //BOLTZMANNVARIABLED2Q13_H
