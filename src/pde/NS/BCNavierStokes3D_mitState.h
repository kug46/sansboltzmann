// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNAVIERSTOKES3D_MITSTATE_H
#define BCNAVIERSTOKES3D_MITSTATE_H

// 3-D compressible Navier-Stokes BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"

#include "pde/BCCategory.h"
#include "BCEuler3D.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 3-D compressible Navier-Stokes
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeWallNoSlipAdiabatic_mitState;
class BCTypeWallNoSlipIsothermal_mitState;
class BCTypeSymmetry_mitState;

template <class BCType, class PDENavierStokes3D>
class BCNavierStokes3D;

template <class BCType>
struct BCNavierStokes3DParams;


//----------------------------------------------------------------------------//
// Conventional PX-style No Slip, Adiabatic BC:
//
// specify: velocity, boundary-normal heat flux
//----------------------------------------------------------------------------//

template<>
struct BCNavierStokes3DParams<BCTypeWallNoSlipAdiabatic_mitState> : noncopyable
{
  static constexpr const char* BCName{"WallNoSlipAdiabatic_mitState"};
  struct Option
  {
    const DictOption WallNoSlipAdiabatic_mitState{BCNavierStokes3DParams::BCName, BCNavierStokes3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCNavierStokes3DParams params;
};


template <class PDE_>
class BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDE_> :
    public BCType< BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeWallNoSlipAdiabatic_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCNavierStokes3DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCNavierStokes3D( const PDE& pde ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter())  {}
  ~BCNavierStokes3D() {}

  BCNavierStokes3D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()) {}

  BCNavierStokes3D( const BCNavierStokes3D& ) = delete;
  BCNavierStokes3D& operator=( const BCNavierStokes3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


// BC state
template <class PDE>
template <class T>
inline void
BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDE>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rhoI = 0, uI = 0, vI = 0, wI = 0, tI = 0;

  // Set boundary state such that pressure matches interior
  qInterpret_.eval( qI, rhoI, uI, vI, wI, tI );
  T pI = gas_.pressure(rhoI, tI);

  qInterpret_.setFromPrimitive( qB, DensityVelocityPressure3D<T>( rhoI, 0, 0, 0, pI ) );
}


// normal BC flux
template <class PDE>
template <class T>
inline void
BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0, hv = 0;
  pde_.fluxViscous(x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);

  Fn += fv*nx + gv*ny + hv*nz;

  // No energy flux at wall
  Fn[pde_.iEngy] = 0;
}


// normal BC flux
template <class PDE>
template <class Tp, class T>
inline void
BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDE>::
fluxNormal( const Tp& param,
            const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(param, x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0, hv = 0;
  pde_.fluxViscous(param, x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);

  Fn += fv*nx + gv*ny + hv*nz;

  // No energy flux at wall
  Fn[pde_.iEngy] = 0;
}


template <class PDE>
void
BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes3D<BCTypeWallNoSlipAdiabatic_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX-style No Slip, Isothermal BC:
//
// specify: temperature
//----------------------------------------------------------------------------//

template<>
struct BCNavierStokes3DParams<BCTypeWallNoSlipIsothermal_mitState> : noncopyable
{
  const ParameterNumeric<Real> Twall{"Twall", NO_DEFAULT, NO_RANGE, "Wall Temperature"};

  static constexpr const char* BCName{"WallNoSlipIsothermal_mitState"};
  struct Option
  {
    const DictOption WallNoSlipIsothermal_mitState{BCNavierStokes3DParams::BCName, BCNavierStokes3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCNavierStokes3DParams params;
};


template <class PDE_>
class BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDE_> :
    public BCType< BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDE_> >
{
public:
  typedef PhysD3 PhysDim;
  typedef BCTypeWallNoSlipIsothermal_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCNavierStokes3DParams<BCTypeWallNoSlipIsothermal_mitState> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCNavierStokes3D( const PDE& pde, const Real& Twall ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    Twall_(Twall)
  {
    // Nothing
  }

  ~BCNavierStokes3D() {}

  BCNavierStokes3D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    gas_(pde_.gasModel()),
    qInterpret_(pde_.variableInterpreter()),
    Twall_(d.get(ParamsType::params.Twall))
  {
    // Nothing
  }

  BCNavierStokes3D( const BCNavierStokes3D& ) = delete;
  BCNavierStokes3D& operator=( const BCNavierStokes3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, z, time, nx, ny, nz, qI, qB);
  }


  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn ) const;

  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real Twall_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDE>::
state( const Real& x, const Real& y, const Real& z, const Real& time,
       const Real& nx, const Real& ny, const Real& nz,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  T rhoI = 0, uI = 0, vI = 0, wI = 0, tI = 0;

  // Set boundary state such that pressure matches interior
  qInterpret_.eval( qI, rhoI, uI, vI, wI, tI );
  T pI = gas_.pressure(rhoI, tI);
  T pB = pI;
  // Temperature is the wall temperature
  T TB = Twall_;
  // Density to match (p,T)
  T rhoB = gas_.density(pB, TB);

  qInterpret_.setFromPrimitive( qB, DensityVelocityTemperature3D<T>( rhoB, 0.0, 0.0, 0.0, TB ) );
}


// normal BC flux
template <class PDE>
template <class T>
inline void
BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn ) const
{
  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0, hv = 0;
  pde_.fluxViscous(x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);

  Fn += fv*nx + gv*ny + hv*nz;
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
inline void
BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDE>::
fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(param, x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0, hv = 0;
  pde_.fluxViscous(param, x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);

  Fn += fv*nx + gv*ny + hv*nz;
}


template <class PDE>
void
BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes3D<BCTypeWallNoSlipIsothermal_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX-style symmetry/slip-wall BC:
//
// specify: normal velocity
//----------------------------------------------------------------------------//


template<>
struct BCNavierStokes3DParams<BCTypeSymmetry_mitState> : noncopyable
{
  static constexpr const char* BCName{"Symmetry_mitState"};
  struct Option
  {
    const DictOption Symmetry_mitState{BCNavierStokes3DParams::BCName, BCNavierStokes3DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCNavierStokes3DParams params;
};


template <class PDE_>
class BCNavierStokes3D<BCTypeSymmetry_mitState, PDE_> :
    public BCEuler3D<BCTypeSymmetry_mitState, PDE_>
{
public:
  typedef BCEuler3D<BCTypeSymmetry_mitState, PDE_> BaseType;
  typedef PhysD3 PhysDim;
  typedef BCTypeSymmetry_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCNavierStokes3DParams<BCType> ParamsType;
  typedef PDE_ PDE;

  using BaseType::D;   // physical dimensions
  using BaseType::N;   // total solution variables

  static const int NBC = 1;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCNavierStokes3D( const PDE& pde ) : BaseType(pde)  {}
  ~BCNavierStokes3D() {}

  BCNavierStokes3D(const PDE& pde, const PyDict& d ) : BaseType(pde, d) {}

  BCNavierStokes3D& operator=( const BCNavierStokes3D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return true; }

  // BC state
  using BaseType::state;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp & param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const;

  // is the boundary state valid
  using BaseType::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BaseType::pde_;
  using BaseType::gas_;
  using BaseType::qInterpret_;   // solution variable interpreter class
};


// normal BC flux
template <class PDE>
template <class T>
inline void
BCNavierStokes3D<BCTypeSymmetry_mitState, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Zero out any possible stabilization
  Fn[pde_.iEngy] = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0, hv = 0;
  pde_.fluxViscous(x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);

  // Compute normal x-, y- and z-momentum equations
  T xMomN = fv[pde_.ixMom]*nx + gv[pde_.ixMom]*ny + hv[pde_.ixMom]*nz;
  T yMomN = fv[pde_.iyMom]*nx + gv[pde_.iyMom]*ny + hv[pde_.iyMom]*nz;
  T zMomN = fv[pde_.izMom]*nx + gv[pde_.izMom]*ny + hv[pde_.izMom]*nz;

  // add only momentum viscous flux with zero tau_ns (n-normal, s-tangential coordinates)
  Fn[pde_.ixMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nx;
  Fn[pde_.iyMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*ny;
  Fn[pde_.izMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nz;
}


// normal BC flux
template <class PDE>
template <class Tp, class T>
inline void
BCNavierStokes3D<BCTypeSymmetry_mitState, PDE>::
fluxNormal( const Tp& param,
            const Real& x, const Real& y, const Real& z, const Real& time,
            const Real& nx, const Real& ny, const Real& nz,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
            const ArrayQ<T>& qB,
            ArrayQ<T>& Fn) const
{
  // Zero out any possible stabilization
  Fn[pde_.iEngy] = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0, fz = 0;
  pde_.fluxAdvective(param, x, y, z, time, qB, fx, fy, fz);

  Fn += fx*nx + fy*ny + fz*nz;

  // Compute the viscous flux
  ArrayQ<T> fv = 0, gv = 0, hv = 0;
  pde_.fluxViscous(param, x, y, z, time, qB, qIx, qIy, qIz, fv, gv, hv);

  // Compute normal x-, y- and z-momentum equations
  T xMomN = fv[pde_.ixMom]*nx + gv[pde_.ixMom]*ny + hv[pde_.ixMom]*nz;
  T yMomN = fv[pde_.iyMom]*nx + gv[pde_.iyMom]*ny + hv[pde_.iyMom]*nz;
  T zMomN = fv[pde_.izMom]*nx + gv[pde_.izMom]*ny + hv[pde_.izMom]*nz;

  // add only momentum viscous flux with zero tau_ns (n-normal, s-tangential coordinates)
  Fn[pde_.ixMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nx;
  Fn[pde_.iyMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*ny;
  Fn[pde_.izMom] += (xMomN*nx + yMomN*ny + zMomN*nz)*nz;
}


template <class PDE>
void
BCNavierStokes3D<BCTypeSymmetry_mitState, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCNavierStokes3D<BCTypeSymmetry_mitState, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes3D<BCTypeSymmetry_mitState, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCNavierStokes3D<BCTypeSymmetry_mitState, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // BCNAVIERSTOKES3D_MITSTATE_H
