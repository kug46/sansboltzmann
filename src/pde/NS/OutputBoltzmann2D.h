// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTBOLTZMANN2D_H
#define OUTPUTBOLTZMANN2D_H

#include "PDEBoltzmann2D.h"

#include "pde/OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Provides weights to compute a force from a weighted residual

template <class PDENDConvert>
class OutputBoltzmann2D_Force : public OutputType< OutputBoltzmann2D_Force<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputBoltzmann2D_Force( const PDENDConvert& pde, const Real& wx, const Real& wy ) :
        pde_(pde), wx_(wx), wy_(wy) {}

  void operator()(const Real& x, const Real& y, const Real& time, ArrayQ<Real>& weight  ) const
  {
    weight = 0;
    weight[pde_.ixMom] = wx_;
    weight[pde_.iyMom] = wy_;
  }

private:
  const PDENDConvert& pde_;
  Real wx_, wy_;
};
//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputBoltzmann2D_Density : public OutputType< OutputBoltzmann2D_Density<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputBoltzmann2D_Density( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    GasModel gas = pde_.gasModel();
    QInterpret qInterp = pde_.variableInterpreter();

    T rho = 0;
    T gamma = gas.gamma();

    qInterp.evalDensity( q, rho);
    output = rho;
  }

  template<class Tp, class T>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output  ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

private:
  const PDENDConvert& pde_;
};

//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputBoltzmann2D_Momentum : public OutputType< OutputBoltzmann2D_Momentum<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputBoltzmann2D_Momentum( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output) const
  {
    GasModel gas = pde_.gasModel();
    QInterpret qInterp = pde_.variableInterpreter();

    T rho = 0, U = 0, V = 0;
    T gamma = gas.gamma();

    qInterp.evalDensity( q, rho);
    qInterp.evalVelocity(q, U, V);
    output = rho * sqrt(U*U+V*V);
  }

  template<class Tp, class T>
  void operator()(const  Tp& param,
                  const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

private:
  const PDENDConvert& pde_;
};

}

#endif //OUTPUTBOLTZMANN2D_H
