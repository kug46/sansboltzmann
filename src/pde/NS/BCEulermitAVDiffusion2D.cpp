// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCEuler2D.h"
#include "BCEulermitAVDiffusion2D.h"
#include "Q2DPrimitiveRhoPressure.h"
#include "Q2DPrimitiveSurrogate.h"
#include "Q2DConservative.h"
#include "Q2DEntropy.h"

// TODO: Rework so this is not needed here
#include "TraitsEulerArtificialViscosity.h"
#include "TraitsNavierStokes.h"
#include "TraitsRANSSA.h"
#include "SAVariable2D.h"
#include "BCRANSSA2D_Solution.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
// Instantiate BC parameters

// Pressure-Primitive Variables
typedef BCEulermitAVDiffusion2DVector< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> > BCVector_RhoP;
BCPARAMETER_INSTANTIATE( BCVector_RhoP )

// Conservative Variables
typedef BCEulermitAVDiffusion2DVector< TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel> > BCVector_Cons;
BCPARAMETER_INSTANTIATE( BCVector_Cons )

// Entropy Variables
typedef BCEulermitAVDiffusion2DVector< TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel> > BCVector_Entropy;
BCPARAMETER_INSTANTIATE( BCVector_Entropy )

// Surrogate Variables
typedef BCEulermitAVDiffusion2DVector< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> > BCVector_Surrogate;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate )

} //namespace SANS
