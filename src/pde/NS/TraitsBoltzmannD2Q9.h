// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSBOLTZMANND2Q9_H
#define TRAITSBOLTZMANND2Q9_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

namespace SANS
{

template <class PhysDim_>
class TraitsSizeBoltzmannD2Q9
{
public:
  typedef PhysDim_ PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = D+7;                   // total solution variables

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;           // solution/residual arrays

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>;        // matrices
};


template <class QType_, class GasModel_>
class TraitsModelBoltzmannD2Q9
{
public:
  typedef QType_ QType;                       // solution variables set

  typedef GasModel_ GasModel;                 // gas model
};

}

#endif // TRAITSBOLTZMANND2Q9_H
