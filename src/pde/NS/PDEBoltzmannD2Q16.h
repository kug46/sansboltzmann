// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBOLTZMANND2Q16_H
#define PDEBOLTZMANND2Q16_H

// 2-D compressible Euler

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "pde/ForcingFunctionBase.h"
#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "EulerResidCategory.h"
#include "GasModel.h"
#include "RoeBoltzmannD2Q16.h"
//#include "HLLCD2Q16.h"
#include "BoltzmannVariableD2Q16.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr
#include <math.h>             // isnan

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class QD2Q16;


//----------------------------------------------------------------------------//
// PDE class: 2-D Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. Real)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEBoltzmannD2Q16
{
public:

  enum EulerResidualInterpCategory
  {
    Euler_ResidInterp_Raw,
    Euler_ResidInterp_Momentum,
    Euler_ResidInterp_Entropy
  };

  enum BoltzmannEquationMode
  {
    AdvectionDiffusion,
    Hydrodynamics
  };

  enum CollisionOperatorCategory
  {
    BGK,
    BGK_feq3
  };

  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef PDETraitsModel TraitsModel;

  typedef typename PDETraitsModel::QType QType;
  typedef QD2Q16<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                     // gas model

  typedef RoeBoltzmannD2Q16<QType, PDETraitsSize> FluxType;

  typedef BoltzmannVariableTypeD2Q16Params VariableTypeD2Q16Params;

  //typedef ForcingFunctionBaseD2Q16< PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel> > ForcingFunctionType;

  explicit PDEBoltzmannD2Q16( const GasModel& gas0,
                       EulerResidualInterpCategory cat,
                       CollisionOperatorCategory omega = BGK,
                       const Real tau = 1.,
                       BoltzmannEquationMode eqMode = AdvectionDiffusion,
                       const Real Uadv = 0.,
                       const Real Vadv = 0.,
                       const RoeEntropyFix entropyFix = none) :
    gas_(gas0),
    qInterpret_(gas0),
    cat_(cat),
    omega_(omega),
    tau_(tau),
    eqMode_(eqMode),
    Uadv_(Uadv),
    Vadv_(Vadv),
    flux_(gas0, entropyFix)
  {
    // Nothing
  }

  static const int if0 = 0;
  static const int if1 = 1;
  static const int if2 = 2;
  static const int if3 = 3;
  static const int if4 = 4;
  static const int if5 = 5;
  static const int if6 = 6;
  static const int if7 = 7;
  static const int if8 = 8;
  static const int if9 = 9;
  static const int if10 = 10;
  static const int if11 = 11;
  static const int if12 = 12;
  static const int if13 = 13;
  static const int if14 = 14;
  static const int if15 = 15;

#if 0
  static_assert( if0 == RoeBoltzmannD2Q16<QType, PDETraitsSize>::if0, "Must match" );
  static_assert( if1 == RoeBoltzmannD2Q16<QType, PDETraitsSize>::if1, "Must match" );
  static_assert( if2 == RoeBoltzmannD2Q16<QType, PDETraitsSize>::if2, "Must match" );
  static_assert( if3 == RoeBoltzmannD2Q16<QType, PDETraitsSize>::if3, "Must match" );
  static_assert( if4 == RoeBoltzmannD2Q16<QType, PDETraitsSize>::if4, "Must match" );
  static_assert( if5 == RoeBoltzmannD2Q16<QType, PDETraitsSize>::if5, "Must match" );
  static_assert( if6 == RoeBoltzmannD2Q16<QType, PDETraitsSize>::if6, "Must match" );
  static_assert( if7 == RoeBoltzmannD2Q16<QType, PDETraitsSize>::if7, "Must match" );
  static_assert( if8 == RoeBoltzmannD2Q16<QType, PDETraitsSize>::if8, "Must match" );
#endif

  PDEBoltzmannD2Q16( const PDEBoltzmannD2Q16& pde0 ) = delete;
  PDEBoltzmannD2Q16& operator=( const PDEBoltzmannD2Q16& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // unsteady temporal flux: Ft(Q)
  template<class Tq, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template<class Tq, class Tf>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const;

  // master state: U(Q)
  template<class Tf, class Tq>
  void workingState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tf>& uCons, ArrayQ<Tq>& q ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    fluxAdvective(x, y, time, q, f, g);
  }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwind(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const;


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const {}

  template <class Tq, class Tg, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,
      MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const {}

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, time, q, qx, qy, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const {}

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const;

  // characteristic speed (needed for timestep)
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T, class Varibles>
  void setDOFFrom(
      ArrayQ<T>& q, const BoltzmannVariableTypeD2Q16<Varibles>& data ) const;

  // set from variable
  template<class Varibles>
  ArrayQ<Real> setDOFFrom( const BoltzmannVariableTypeD2Q16<Varibles>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the residual-interpreter type
  EulerResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // accessors for gas model and Q interpreter
  const GasModel& gasModel() const { return gas_; }
  const QInterpret& variableInterpreter() const { return qInterpret_; }

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const GasModel gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  EulerResidualInterpCategory cat_; // Residual interp type
  CollisionOperatorCategory omega_;
  const Real tau_;
  BoltzmannEquationMode eqMode_;
  const Real Uadv_;
  const Real Vadv_;
  const FluxType flux_;
};


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const
{

  for (int i=0; i<16; i++)
    uCons(i) = q(i);

}

// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tf, class Tq>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::workingState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tf>& uCons, ArrayQ<Tq>& q ) const
{
  PrimitiveDistributionFunctionsD2Q16<Tf> data(uCons(if0), uCons(if1), uCons(if2), uCons(if3),
                                               uCons(if4), uCons(if5), uCons(if6), uCons(if7),
                                               uCons(if8), uCons(if9), uCons(if10), uCons(if11),
                                               uCons(if12), uCons(if13), uCons(if14), uCons(if15));

  qInterpret_.setFromPrimitive(q, data);
}


// unsteady conservative flux Jacobian: dU(Q)/dQ
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  ArrayQ<T> pdf0_q, pdf1_q, pdf2_q, pdf3_q, pdf4_q, pdf5_q, pdf6_q, pdf7_q, pdf8_q;
  ArrayQ<T> pdf9_q, pdf10_q, pdf11_q, pdf12_q, pdf13_q, pdf14_q, pdf15_q;

  //qInterpret_.eval( q, pdf0, pdf1, pdf2, pdf3, pdf4, pdf5, pdf6, pdf7, pdf8);
  qInterpret_.evalJacobian( q, pdf0_q, pdf1_q, pdf2_q, pdf3_q,
                               pdf4_q, pdf5_q, pdf6_q, pdf7_q,
                               pdf8_q, pdf9_q, pdf10_q, pdf11_q,
                               pdf12_q, pdf13_q, pdf14_q, pdf15_q);

  for (int i=0; i<16; i++)
  {
    dudq(if0,i) = pdf0_q[i];
    dudq(if1,i) = pdf1_q[i];
    dudq(if2,i) = pdf2_q[i];
    dudq(if3,i) = pdf3_q[i];
    dudq(if4,i) = pdf4_q[i];
    dudq(if5,i) = pdf5_q[i];
    dudq(if6,i) = pdf6_q[i];
    dudq(if7,i) = pdf7_q[i];
    dudq(if8,i) = pdf8_q[i];
    dudq(if9,i) = pdf9_q[i];
    dudq(if10,i) = pdf10_q[i];
    dudq(if11,i) = pdf11_q[i];
    dudq(if12,i) = pdf12_q[i];
    dudq(if13,i) = pdf13_q[i];
    dudq(if14,i) = pdf14_q[i];
    dudq(if15,i) = pdf15_q[i];
  }

}


// strong form of unsteady conservative flux: dU(Q)/dt
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& utCons ) const
{

  for (int i=0; i<16; i++)
    utCons(i) += qt(i);


#if 0
  T pdf0t = 0, pdf1t = 0, pdf2t = 0, pdf3t = 0, pdf4t = 0, pdf5t = 0, pdf6t = 0, pdf7t = 0, pdf8t = 0;
  T pdf9t = 0, pdf10t = 0, pdf11t = 0, pdf12t = 0, pdf13t = 0, pdf14t = 0, pdf15t = 0 ;

  qInterpret_.evalGradient(q, qt, pdf0t, pdf1t, pdf2t, pdf3t, pdf4t, pdf5t, pdf6t, pdf7t, pdf8t,
                           pdf9t, pdf10t, pdf11t, pdf12t, pdf13t, pdf14t, pdf15t);

  utCons(if0) += pdf0t;
  utCons(if1) += pdf1t;
  utCons(if2) += pdf2t;
  utCons(if3) += pdf3t;
  utCons(if4) += pdf4t;
  utCons(if5) += pdf5t;
  utCons(if6) += pdf6t;
  utCons(if7) += pdf7t;
  utCons(if8) += pdf8t;
  utCons(if9) += pdf9t;
  utCons(if10) += pdf10t;
  utCons(if11) += pdf11t;
  utCons(if12) += pdf12t;
  utCons(if13) += pdf13t;
  utCons(if14) += pdf14t;
  utCons(if15) += pdf15t;
#endif

}


// advective flux: F(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  /*
  const int e[16][2] = {   {  1,  1 }, {  -1, 1  }, {  -1,  -1 }, { 1, -1 },
                           {  4,  4 }, {  -4, 4  }, {  -4,  -4 }, { 4, -4 },
                           {  1,  4 }, {  -1, 4  }, {  -1,  -4 }, { 1, -4 },
                           {  4,  1 }, {  -4, 1  }, {  -4,  -1 }, { 4, -1 }   };
  */
  // See eq. 6,7 in  Kim&Pitsch2008 10.1103/PhysRevE.78.016702
  const Real r = std::sqrt(3 + std::sqrt(6));
  const Real s = std::sqrt(3 - std::sqrt(6));
  const Real e[16][2] = {  { r, r }, { -r, r }, { r, -r }, { -r, -r }, // { r, 0 }, { -r, 0 }, { 0, r }, { 0, -r },
                           { s, s }, { -s, s }, { s, -s }, { -s, -s }, // { s, 0 }, { -s, 0 }, { 0, s }, { 0, -s },
                           { r, s }, { -r, s }, { r, -s }, { -r, -s },
                           { s, r }, { -s, r }, { s, -r }, { -s, -r }
                        };


// TODO: Check if this is consistent with what is actually necessary.
  for (int i = 0; i<16; i++)
  {
    f(i) += e[i][0]*q(i);
    g(i) += e[i][1]*q(i);
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  flux_.fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{

  SANS_DEVELOPER_EXCEPTION(" SpaceTime is not implemented in Boltzmann Equation Solver.");
#if 0
//  flux_.fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tq> fnx = 0;
  fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fnx );

  //Upwind temporal flux
  ArrayQ<Tq> ft = 0;
  if (nt >= 0)
    fluxAdvectiveTime( x, y, time, qL, ft );
  else
    fluxAdvectiveTime( x, y, time, qR, ft );

  f += ft*nt + fnx;
#endif
}


// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{/*
 const int e[16][2] = {   {  1,  1 }, {  -1, 1  }, {  -1,  -1 }, { 1, -1 },
                           {  4,  4 }, {  -4, 4  }, {  -4,  -4 }, { 4, -4 },
                           {  1,  4 }, {  -1, 4  }, {  -1,  -4 }, { 1, -4 },
                           {  4,  1 }, {  -4, 1  }, {  -4,  -1 }, { 4, -1 }   };*/
  // See eq. 6,7 in  Kim&Pitsch2008 10.1103/PhysRevE.78.016702
  const Real r = std::sqrt(3 + std::sqrt(6));
  const Real s = std::sqrt(3 - std::sqrt(6));
  const Real e[16][2] = {  { r, r }, { -r, r }, { r, -r }, { -r, -r }, // { r, 0 }, { -r, 0 }, { 0, r }, { 0, -r },
                           { s, s }, { -s, s }, { s, -s }, { -s, -s }, // { s, 0 }, { -s, 0 }, { 0, s }, { 0, -s },
                           { r, s }, { -r, s }, { r, -s }, { -r, -s },
                           { s, r }, { -s, r }, { s, -r }, { -s, -r }
                        };

  dfdu = 0.;
  dgdu = 0.;

  for (int i=0; i<16; i++)
  {
    dfdu(i,i) = e[i][0];
    dgdu(i,i) = e[i][1];
  }
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& Nx, const Real& Ny, MatrixQ<Tf>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION("JacobianFluxAdvectiveAbsVal is needed...");
#if 0
  Real nMag = sqrt(Nx*Nx + Ny*Ny);
  Real nx = Nx/nMag;
  Real ny = Ny/nMag;

  if ( std::isnan(nx) || std::isnan(ny)  ) {}
  else
  {

    Real gamma = gas_.gamma();
    Tq rho, u, v, t, h, H;

    qInterpret_.eval( q, rho, u, v, t );

    Real gm1 = gamma - 1.0;

    Tq qs  = -ny*u + nx*v;
    Tq qn  =  nx*u + ny*v;
    Tq q2  =  u*u + v*v;

    h = gas_.enthalpy(rho, t);
    H = h + 0.5*q2;

    Tq c  = gas_.speedofSound( t ) ;
    Tq c2 = c*c;

    //pre-multiply the eigenvalue by the normal magnitude to save time
    Tq eigumc = fabs(qn - c)*nMag;
    Tq eigupc = fabs(qn + c)*nMag;
    Tq eigu   = fabs(qn    )*nMag;

    mtx(0,0) += (1 - q2*gm1*0.5/c2)*eigu + (2*c*qn + q2*gm1)*eigumc*0.25/c2 + (-2*c*qn + q2*gm1)*eigupc*0.25/c2;
    mtx(1,0) += ny*qs*eigu + u*(1 - 0.5*q2*gm1/c2)*eigu + (-c*nx + u)*(2*c*qn + q2*gm1)*eigumc*0.25/c2
        + (c*nx + u)*(-2*c*qn + q2*gm1)*eigupc*0.25/c2;
    mtx(2,0) += -nx*qs*eigu + v*(1 - 0.5*q2*gm1/c2)*eigu + (-c*ny + v)*(2*c*qn + q2*gm1)*eigumc*0.25/c2
        + (c*ny + v)*(-2*c*qn + q2*gm1)*eigupc*0.25/c2;
    mtx(3,0) += -qs*qs*eigu + 0.5*q2*(1 - 0.5*q2*gm1/c2)*eigu + (H - c*qn)*(2*c*qn + q2*gm1)*eigumc*0.25/c2
        + (H+c*qn)*(-2*c*qn + q2*gm1)*eigupc*0.25/c2;

    mtx(0,1) += u*gm1*eigu/c2 - (c*nx + u*gm1)*eigumc*0.5/c2 + (c*nx - u*gm1)*eigupc*0.5/c2;
    mtx(1,1) += ny*ny*eigu + u*u*gm1*eigu/c2 - (-c*nx + u)*(c*nx + u*gm1)*eigumc*0.5/c2 + (c*nx + u)*(c*nx - u*gm1)*eigupc*0.5/c2;
    mtx(2,1) += -nx*ny*eigu + u*v*gm1*eigu/c2 - (-c*ny + v)*(c*nx + u*gm1)*eigumc*0.5/c2 + (c*ny + v)*(c*nx - u*gm1)*eigupc*0.5/c2;
    mtx(3,1) += -ny*qs*eigu + u*q2*gm1*eigu*0.5/c2 - (H - c*qn)*(c*nx + u*gm1)*eigumc*0.5/c2 + (H + c*qn)*(c*nx - u*gm1)*eigupc*0.5/c2;

    mtx(0,2) += v*gm1*eigu/c2 - (c*ny + v*gm1)*eigumc*0.5/c2 + (c*ny - v*gm1)*eigupc*0.5/c2;
    mtx(1,2) += -nx*ny*eigu + u*v*gm1*eigu/c2 - (-c*nx+u)*(c*ny + v*gm1)*eigumc*0.5/c2 + (c*nx+u)*(c*ny - v*gm1)*eigupc*0.5/c2;
    mtx(2,2) += nx*nx*eigu + v*v*gm1*eigu/c2 - (-c*ny+v)*(c*ny + v*gm1)*eigumc*0.5/c2 + (c*ny+v)*(c*ny - v*gm1)*eigupc*0.5/c2;
    mtx(3,2) += nx*qs*eigu + v*q2*gm1*eigu*0.5/c2 - (H-c*qn)*(c*ny + v*gm1)*eigumc*0.5/c2 + (H+c*qn)*(c*ny - v*gm1)*eigupc*0.5/c2;

    mtx(0,3) += -gm1*eigu/c2 + gm1*eigumc*0.5/c2 + gm1*eigupc*0.5/c2;
    mtx(1,3) += -u*gm1*eigu/c2 + (-c*nx + u)*gm1*eigumc*0.5/c2 + (c*nx+u)*gm1*eigupc*0.5/c2;
    mtx(2,3) += -v*gm1*eigu/c2 + (-c*ny + v)*gm1*eigumc*0.5/c2 + (c*ny+v)*gm1*eigupc*0.5/c2;
    mtx(3,3) += -q2*gm1*eigu*0.5/c2 + (H - c*qn)*gm1*eigumc*0.5/c2 + (H + c*qn)*gm1*eigupc*0.5/c2;
  }
#endif
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION("SpaceTime has not been implemented in Boltzmann Eqation Solver.");
//  flux_.jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, time, q, nx, ny, nt, mtx);
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& strongPDE ) const
{
  /*
  const int e[16][2] = {   {  1,  1 }, {  -1, 1  }, {  -1,  -1 }, { 1, -1 },
                           {  4,  4 }, {  -4, 4  }, {  -4,  -4 }, { 4, -4 },
                           {  1,  4 }, {  -1, 4  }, {  -1,  -4 }, { 1, -4 },
                           {  4,  1 }, {  -4, 1  }, {  -4,  -1 }, { 4, -1 }   };
  */
  // See eq. 6,7 in  Kim&Pitsch2008 10.1103/PhysRevE.78.016702
  const Real r = std::sqrt(3 + std::sqrt(6));
  const Real s = std::sqrt(3 - std::sqrt(6));
  const Real e[16][2] = {  { r, r }, { -r, r }, { r, -r }, { -r, -r }, // { r, 0 }, { -r, 0 }, { 0, r }, { 0, -r },
                           { s, s }, { -s, s }, { s, -s }, { -s, -s }, // { s, 0 }, { -s, 0 }, { 0, s }, { 0, -s },
                           { r, s }, { -r, s }, { r, -s }, { -r, -s },
                           { s, r }, { -s, r }, { s, -r }, { -s, -r }
                        };

  for (int i=0; i<16; i++)
    strongPDE(i) += e[i][0]*qx(i) + e[i][1]*qy(i);

#if 0
  typedef typename promote_Surreal<Tq, Tg>::type T;
  T pdf0x = 0, pdf1x = 0, pdf2x = 0, pdf3x = 0, pdf4x = 0, pdf5x = 0, pdf6x = 0, pdf7x = 0, pdf8x = 0;
  T pdf0y = 0, pdf1y = 0, pdf2y = 0, pdf3y = 0, pdf4y = 0, pdf5y = 0, pdf6y = 0, pdf7y = 0, pdf8y = 0;

  qInterpret_.evalGradient(q, qx, pdf0x, pdf1x, pdf2x, pdf3x, pdf4x, pdf5x, pdf6x, pdf7x, pdf8x);
  qInterpret_.evalGradient(q, qy, pdf0y, pdf1y, pdf2y, pdf3y, pdf4y, pdf5y, pdf6y, pdf7y, pdf8y);

  strongPDE(if0) += e[0][0]*pdf0x + e[0][1]*pdf0y;
  strongPDE(if1) += e[1][0]*pdf1x + e[1][1]*pdf1y;
  strongPDE(if2) += e[2][0]*pdf2x + e[2][1]*pdf2y;
  strongPDE(if3) += e[3][0]*pdf3x + e[3][1]*pdf3y;
  strongPDE(if4) += e[4][0]*pdf4x + e[4][1]*pdf4y;
  strongPDE(if5) += e[5][0]*pdf5x + e[5][1]*pdf5y;
  strongPDE(if6) += e[6][0]*pdf6x + e[6][1]*pdf6y;
  strongPDE(if7) += e[7][0]*pdf7x + e[7][1]*pdf7y;
  strongPDE(if8) += e[8][0]*pdf8x + e[8][1]*pdf8y;
#endif
}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::source(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  Tq rho = 0., U = 0., V = 0.;
  Tq e_dot_u = 0., u_dot_u = 0., feq = 0;        // terms in feq


  /*
  const int e[16][2] = {   {  1,  1 }, {  -1, 1  }, {  -1,  -1 }, { 1, -1 },
                           {  4,  4 }, {  -4, 4  }, {  -4,  -4 }, { 4, -4 },
                           {  1,  4 }, {  -1, 4  }, {  -1,  -4 }, { 1, -4 },
                           {  4,  1 }, {  -4, 1  }, {  -4,  -1 }, { 4, -1 }   };

  const Real Wm = 0.384173012212244;
  const Real Wn = 0.115826987787756;

  const Real weight[16] = {  Wm*Wm, Wm*Wm, Wm*Wm, Wm*Wm,
                             Wn*Wn, Wn*Wn, Wn*Wn, Wn*Wn,
                             Wm*Wn, Wm*Wn, Wm*Wn, Wm*Wn,
                             Wn*Wm, Wn*Wm, Wn*Wm, Wn*Wm };
  */
  const Real csq = 1.;
  // See eq. 6,7 in  Kim&Pitsch2008 10.1103/PhysRevE.78.016702
  const Real r = std::sqrt(3 + std::sqrt(6));
  const Real s = std::sqrt(3 - std::sqrt(6));
  const Real e[16][2] = {  { r, r }, { -r, r }, { r, -r }, { -r, -r }, // { r, 0 }, { -r, 0 }, { 0, r }, { 0, -r },
                           { s, s }, { -s, s }, { s, -s }, { -s, -s }, // { s, 0 }, { -s, 0 }, { 0, s }, { 0, -s },
                           { r, s }, { -r, s }, { r, -s }, { -r, -s },
                           { s, r }, { -s, r }, { s, -r }, { -s, -r }
                        };

  const Real Wa = (5-2*std::sqrt(6))/48.;
  const Real Wb = (5+2*std::sqrt(6))/48.;
  const Real Wc = 1./48.;
  const Real weight[16] = { Wa, Wa, Wa, Wa,
                            Wb, Wb, Wb, Wb,
                            Wc, Wc, Wc, Wc,
                            Wc, Wc, Wc, Wc };

  qInterpret_.evalDensity(q,rho);

  switch (eqMode_)
  {
    case AdvectionDiffusion:
    {
      U = Uadv_;
      V = Vadv_;
      break;
    }
    case Hydrodynamics:
    {
      qInterpret_.evalVelocity(q, U, V);
      break;
    }
    default: SANS_DEVELOPER_EXCEPTION("Unknown Equation mode (only AdvectionDiffusion and Hydrodynamics permitted).");
  }

  u_dot_u = U*U + V*V;

  switch (omega_)
  {
    case BGK:
      for (int i=0; i<16; i++)
      {
        e_dot_u = e[i][0]*U + e[i][1]*V;
        feq = weight[i]*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));
        source(i) -= (1./tau_)*(feq - q(i));
      }
      break;
    case BGK_feq3:
    for (int i=0; i<16; i++)
    {
      e_dot_u = e[i][0]*U + e[i][1]*V;
      feq = weight[i]*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq)
                            + (e_dot_u/6.)*(e_dot_u*e_dot_u - 3*u_dot_u*u_dot_u));
      source(i) -= (1./tau_)*(feq - q(i));
    }
    break;
    default: SANS_DEVELOPER_EXCEPTION("Unknown collision operator.");
  }

}


// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const
{
  Tq t = 0, c = 0;
  Tq u = 0, v = 0;

  if (Uadv_ == 0 && Vadv_ == 0)
    qInterpret_.evalVelocity(q,u,v);
  else
  {
     u = Uadv_;
     v = Vadv_;
  }
  //qInterpret_.evalTemperature( q, t );
  c = 1;//gas_.speedofSound(t);

  speed = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;
}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const
{
  Tq t = 0, c = 0;
  Tq u = 0, v = 0;

  if (Uadv_ == 0 && Vadv_ == 0)
    qInterpret_.evalVelocity(q,u,v);
  else
  {
     u = Uadv_;
     v = Vadv_;
  }
  //qInterpret_.evalTemperature( q, t );
  c = 1;//gas_.speedofSound(t);

  speed = sqrt(u*u + v*v) + c;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::
updateFraction( const Real& x, const Real& y, const Real& time,
                const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  qInterpret_.updateFraction(q, dq, maxChangeFraction, updateFraction);
}


// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return qInterpret_.isValidState(q);
}


// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpret_.setFromPrimitive( q, data, name, nn );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Varibles>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const BoltzmannVariableTypeD2Q16<Varibles>& data ) const
{
  qInterpret_.setFromPrimitive( q, data.cast() );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Varibles>
inline typename PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::setDOFFrom( const BoltzmannVariableTypeD2Q16<Varibles>& data ) const
{
  ArrayQ<Real> q;
  qInterpret_.setFromPrimitive( q, data.cast() );
  return q;
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q;
  DictKeyPair data = d.get(BoltzmannVariableTypeD2Q16Params::params.StateVector);

  if ( data == BoltzmannVariableTypeD2Q16Params::params.StateVector.PrimitiveDistributionFunctions16 )
    qInterpret_.setFromPrimitive( q, PrimitiveDistributionFunctionsD2Q16<Real>(data) );
  else
    SANS_DEVELOPER_EXCEPTION(" Missing state vector option ");

  return q;
}

// --------------------- HERE! TODO TODO TODO
// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{

  int nOut = rsdPDEOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Momentum not implemented in D2Q16");
/*    SANS_ASSERT(nOut == N-1);
    rsdPDEOut[0] = rsdPDEIn[iCont];
    rsdPDEOut[1] = sqrt( rsdPDEIn[ixMom]*rsdPDEIn[ixMom] + rsdPDEIn[iyMom]*rsdPDEIn[iyMom] );
    rsdPDEOut[2] = rsdPDEIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdPDEOut[2+i] = rsdPDEIn[iEngy+i];*/
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
//    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in D2Q16");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    for (int i = 0; i < N; i++)
      rsdPDEOut[0] += rsdPDEIn[i]*rsdPDEIn[i];

    rsdPDEOut[0] = sqrt(rsdPDEOut[0]);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
    {
      rsdAuxOut[i] = rsdAuxIn[i];
    }
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Momentum not implemented in D2Q16");
/*    SANS_ASSERT(nOut == N-1);
    rsdAuxOut[0] = rsdAuxIn[iCont];
    rsdAuxOut[1] = sqrt(rsdAuxIn[ixMom]*rsdAuxIn[ixMom] + rsdAuxIn[iyMom]*rsdAuxIn[iyMom]);
    rsdAuxOut[2] = rsdAuxIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdAuxOut[2+i] = rsdAuxIn[iEngy+i]; */
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
//    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in D2Q16");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    for (int i = 0; i < N; i++)
      rsdAuxOut[0] += rsdAuxIn[i]*rsdAuxIn[i];

    rsdAuxOut[0] = sqrt(rsdAuxOut[0]);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Momentum not implemented in D2Q16");
/*    SANS_ASSERT(nOut == N-1);
    rsdBCOut[0] = rsdBCIn[iCont];
    rsdBCOut[1] = sqrt( rsdBCIn[ixMom]*rsdBCIn[ixMom] + rsdBCIn[iyMom]*rsdBCIn[iyMom] );
    rsdBCOut[2] = rsdBCIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdBCOut[2+i] = rsdBCIn[iEngy+i];*/
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
//    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in D2Q16");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    for (int i = 0; i < N; i++)
      rsdBCOut[0] += rsdBCIn[i]*rsdBCIn[i];

    rsdBCOut[0] = sqrt(rsdBCOut[0]);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}

#if 0
// return the interp type
template <template <class> class PDETraitsSize, class PDETraitsModel>
EulerResidualInterpCategory
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::category() const
{
  return cat_;
}
#endif

// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::nMonitor() const
{
  int nMon;

  if (cat_==Euler_ResidInterp_Raw) // raw residual
    nMon = N;
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
    nMon = N-1;
  else if (cat_ == Euler_ResidInterp_Entropy)
    nMon = 1;
  else
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );

  return nMon;
}

#if 0
// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  (*forcing_)(*this, x, y, time, forcing );
}
#endif

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  return {
          // Nothing
         };
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  switch (index)
  {
  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEBoltzmannD2Q16::derivedQuantity - Invalid index!");
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // PDEBOLTZMANND2Q16_H
