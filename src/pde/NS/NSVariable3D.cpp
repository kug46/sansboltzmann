// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "NSVariable3D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<NSVariableType3DParams::VariableOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<NSVariableType3DParams::VariableOptions> > const&) const;

void DensityVelocityPressure3DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.v));
  allParams.push_back(d.checkInputs(params.w));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
DensityVelocityPressure3DParams DensityVelocityPressure3DParams::params;

void DensityVelocityTemperature3DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.v));
  allParams.push_back(d.checkInputs(params.w));
  allParams.push_back(d.checkInputs(params.t));
  d.checkUnknownInputs(allParams);
}
DensityVelocityTemperature3DParams DensityVelocityTemperature3DParams::params;

void Conservative3DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.rhou));
  allParams.push_back(d.checkInputs(params.rhov));
  allParams.push_back(d.checkInputs(params.rhow));
  allParams.push_back(d.checkInputs(params.rhoE));
  d.checkUnknownInputs(allParams);
}
Conservative3DParams Conservative3DParams::params;


void NSVariableType3DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
NSVariableType3DParams NSVariableType3DParams::params;

}
