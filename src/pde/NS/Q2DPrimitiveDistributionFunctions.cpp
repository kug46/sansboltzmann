// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Q2DPrimitiveDistributionFunctions.h"
#include "TraitsBoltzmann2D.h"

namespace SANS
{

template class Q2D<QTypePrimitiveDistributionFunctions, TraitsSizeBoltzmann2D>;

}
