// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef QD2Q13PRIMITIVEDISTRIBUTIONFUNCTIONS_H
#define QD2Q13PRIMITIVEDISTRIBUTIONFUNCTIONS_H

// solution interpreter class
// Euler/N-S conservation-eqns: primitive variables (rho, u, v, p)

#include <vector>
#include <string>


#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "GasModel.h"
#include "BoltzmannVariableD2Q13.h"
#include "Surreal/PromoteSurreal.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D Euler/N-S
// primitive variables (rho, u, v, p)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (rho, u, v, p)
class QTypePrimitiveDistributionFunctions;


// primary template
template <class QType, template <class> class PDETraitsSize>
class QD2Q13;


template <template <class> class PDETraitsSize>
class QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int if0 = 0;
  static const int if1 = 1;
  static const int if2 = 2;
  static const int if3 = 3;
  static const int if4 = 4;
  static const int if5 = 5;
  static const int if6 = 6;
  static const int if7 = 7;
  static const int if8 = 8;
  static const int if9 = 9;
  static const int if10 = 10;
  static const int if11 = 11;
  static const int if12 = 12;


  // The three components of the state vector that make up the velocity vector
  //static const int ix = if1;
  //static const int iy = if2;

  static std::vector<std::string> stateNames()
  {
    return { "pdf0", "pdf1", "pdf2",
             "pdf3", "pdf4", "pdf5",
             "pdf6", "pdf7", "pdf8",
             "pdf9", "pdf10", "pdf11",
             "pdf12"};
  }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit QD2Q13( const GasModel& gas0 ) : gas_(gas0) {}
  QD2Q13( const QD2Q13& q0 ) : gas_(q0.gas_) {}
  ~QD2Q13() {}

  QD2Q13& operator=( const QD2Q13& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& pdf0, T& pdf1, T& pdf2, T& pdf3, T& pdf4, T& pdf5,
             T& pdf6, T& pdf7, T& pdf8, T& pdf9, T& pdf10, T& pdf11, T& pdf12) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u, T& v ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalDensityJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q,
                     ArrayQ<T>& pdf0_q, ArrayQ<T>& pdf1_q, ArrayQ<T>& pdf2_q,
                     ArrayQ<T>& pdf3_q, ArrayQ<T>& pdf4_q, ArrayQ<T>& pdf5_q,
                     ArrayQ<T>& pdf6_q, ArrayQ<T>& pdf7_q, ArrayQ<T>& pdf8_q,
                     ArrayQ<T>& pdf9_q, ArrayQ<T>& pdf10_q, ArrayQ<T>& pdf11_q,
                     ArrayQ<T>& pdf12_q) const;

  template<class Tq, class Tg>
  void evalDensityGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                            typename promote_Surreal<Tq,Tg>::type& rho_x ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& pdf0x,
    typename promote_Surreal<Tq,Tg>::type& pdf1x,
    typename promote_Surreal<Tq,Tg>::type& pdf2x,
    typename promote_Surreal<Tq,Tg>::type& pdf3x,
    typename promote_Surreal<Tq,Tg>::type& pdf4x,
    typename promote_Surreal<Tq,Tg>::type& pdf5x,
    typename promote_Surreal<Tq,Tg>::type& pdf6x,
    typename promote_Surreal<Tq,Tg>::type& pdf7x,
    typename promote_Surreal<Tq,Tg>::type& pdf8x,
    typename promote_Surreal<Tq,Tg>::type& pdf9x,
    typename promote_Surreal<Tq,Tg>::type& pdf10x,
    typename promote_Surreal<Tq,Tg>::type& pdf11x,
    typename promote_Surreal<Tq,Tg>::type& pdf12x) const;

  template<class Tq, class Tg, class Th>
  void evalSecondGradient(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf0xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf0xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf0yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf1xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf1xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf1yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf2xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf2xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf2yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf3xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf3xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf3yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf4xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf4xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf4yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf5xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf5xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf5yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf6xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf6xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf6yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf7xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf7xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf7yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf8xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf8xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf8yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf9xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf9xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf9yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf10xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf10xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf10yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf11xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf11xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf11yy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf12xx,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf12xy,
      typename promote_Surreal<Tq, Tg, Th>::type& pdf12yy) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const PrimitiveDistributionFunctionsD2Q13<T>& data ) const;

/*  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative2D<T>& data ) const; */

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& pdf0, T& pdf1, T& pdf2, T& pdf3, T& pdf4, T& pdf5,
                 T& pdf6, T& pdf7, T& pdf8, T& pdf9, T& pdf10, T& pdf11, T& pdf12 ) const
{
  pdf0 = q(if0);
  pdf1 = q(if1);
  pdf2 = q(if2);
  pdf3 = q(if3);
  pdf4 = q(if4);
  pdf5 = q(if5);
  pdf6 = q(if6);
  pdf7 = q(if7);
  pdf8 = q(if8);
  pdf8 = q(if9);
  pdf8 = q(if10);
  pdf8 = q(if11);
  pdf8 = q(if12);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  rho = q(if0) + q(if1) + q(if2) + q(if3) + q(if4) + q(if5) + q(if6) + q(if7) + q(if8) +
        q(if9) + q(if10) + q(if11) + q(if12);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u, T& v ) const
{
  const T rho = q(if0) + q(if1) + q(if2) + q(if3) + q(if4) + q(if5) + q(if6) + q(if7) + q(if8) +
                  q(if9) + q(if10) + q(if11) + q(if12);

  // see Leitao Chen & Laura Schaefer '15, DOI: https://doi.org/10.1063/1.4907782
  // c = 1.0 --> csq = 1/2;
  const Real e[13][2] = { { 0, 0 },
                         { 1.0, 0.0 }, { 0.0, 1.0 }, { -1.0, 0.0 }, { 0.0, -1.0 },
                         { 1.0, 1.0 }, { -1.0, 1.0 }, { -1.0, -1.0 }, { 1.0, -1.0 },
                         { 2.0, 0.0 }, { 0.0, 2.0 }, { -2.0, 0.0 }, { 0.0, -2.0 } };
  u = 0.;
  v = 0.;
  for (int i = 0; i < 13; i++)
  {
    u = u + q(i)*e[i][0];
    v = v + q(i)*e[i][1];
  }

  T zero = 0.;
  if ( rho <= zero )
    SANS_DEVELOPER_EXCEPTION("Density going to zero. Not valid");

  u = u/rho;
  v = v/rho;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  // see Leitao Chen & Laura Schaefer '15, DOI: https://doi.org/10.1063/1.4907782
  const Real csq = 1/2.;
  const T& rho = q(if0) + q(if1) + q(if2) + q(if3) + q(if4) + q(if5) + q(if6) + q(if7) + q(if8) +
                  q(if9) + q(if10) + q(if11) + q(if12);
  const T& p   = csq*rho;  // Krueger, (Book) LBM principals and practice eq. 7.15, Springer '17
  // Module necessary for thermal Boltzmann implementation. Not right now.
  t = p/(rho * gas_.R());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalDensityJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const
{
  rho_q = 1;          // rho = sum_i(f_i). drho/df_i is always 1.
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& pdf0_q, ArrayQ<T>& pdf1_q, ArrayQ<T>& pdf2_q,
                        ArrayQ<T>& pdf3_q, ArrayQ<T>& pdf4_q, ArrayQ<T>& pdf5_q,
                        ArrayQ<T>& pdf6_q, ArrayQ<T>& pdf7_q, ArrayQ<T>& pdf8_q,
                        ArrayQ<T>& pdf9_q, ArrayQ<T>& pdf10_q, ArrayQ<T>& pdf11_q,
                        ArrayQ<T>& pdf12_q) const
{
  pdf0_q = 0.; pdf1_q = 0.; pdf2_q = 0.;
  pdf3_q = 0.; pdf4_q = 0.; pdf5_q = 0.;
  pdf6_q = 0.; pdf7_q = 0.; pdf8_q = 0.;
  pdf9_q = 0.; pdf10_q = 0.; pdf11_q = 0.; pdf12_q = 0.;

  pdf0_q[0] = 1.; pdf1_q[1] = 1.; pdf2_q[2] = 1.;
  pdf3_q[3] = 1.; pdf4_q[4] = 1.; pdf5_q[5] = 1.;
  pdf6_q[6] = 1.; pdf7_q[7] = 1.; pdf8_q[8] = 1.;
  pdf9_q[6] = 1.; pdf10_q[7] = 1.; pdf11_q[8] = 1.; pdf12_q[8] = 1.;

}

template <template <class> class PDETraitsSize>
template<class Tq, class Tg>
void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalDensityGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rho_x ) const
{
  rho_x = 0.;
  for (int i = 0; i < 13; i++)
    rho_x = rho_x + qx(i);
}

template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& pdf0x,
    typename promote_Surreal<Tq,Tg>::type& pdf1x,
    typename promote_Surreal<Tq,Tg>::type& pdf2x,
    typename promote_Surreal<Tq,Tg>::type& pdf3x,
    typename promote_Surreal<Tq,Tg>::type& pdf4x,
    typename promote_Surreal<Tq,Tg>::type& pdf5x,
    typename promote_Surreal<Tq,Tg>::type& pdf6x,
    typename promote_Surreal<Tq,Tg>::type& pdf7x,
    typename promote_Surreal<Tq,Tg>::type& pdf8x,
    typename promote_Surreal<Tq,Tg>::type& pdf9x,
    typename promote_Surreal<Tq,Tg>::type& pdf10x,
    typename promote_Surreal<Tq,Tg>::type& pdf11x,
    typename promote_Surreal<Tq,Tg>::type& pdf12x) const
{
  pdf0x = qx(if0);
  pdf1x = qx(if1);
  pdf2x = qx(if2);
  pdf3x = qx(if3);
  pdf4x = qx(if4);
  pdf5x = qx(if5);
  pdf6x = qx(if6);
  pdf7x = qx(if7);
  pdf8x = qx(if8);
  pdf9x = qx(if9);
  pdf10x = qx(if10);
  pdf11x = qx(if11);
  pdf12x = qx(if12);
}


template <template <class> class PDETraitsSize>
template<class Tq, class Tg, class Th>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf0xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf0xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf0yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf1xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf1xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf1yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf2xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf2xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf2yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf3xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf3xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf3yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf4xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf4xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf4yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf5xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf5xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf5yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf6xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf6xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf6yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf7xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf7xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf7yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf8xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf8xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf8yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf9xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf9xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf9yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf10xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf10xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf10yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf11xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf11xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf11yy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf12xx,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf12xy,
    typename promote_Surreal<Tq, Tg, Th>::type& pdf12yy) const
{
  pdf0xx = qxx(if0);
  pdf1xx = qxx(if1);
  pdf2xx = qxx(if2);
  pdf3xx = qxx(if3);
  pdf4xx = qxx(if4);
  pdf5xx = qxx(if5);
  pdf6xx = qxx(if6);
  pdf7xx = qxx(if7);
  pdf8xx = qxx(if8);
  pdf9xx = qxx(if9);
  pdf10xx = qxx(if10);
  pdf11xx = qxx(if11);
  pdf12xx = qxx(if12);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 9);
  SANS_ASSERT((name[0] == "DistributionFunction0") &&
              (name[1] == "DistributionFunction1") &&
              (name[2] == "DistributionFunction2") &&
              (name[3] == "DistributionFunction3") &&
              (name[4] == "DistributionFunction4") &&
              (name[5] == "DistributionFunction5") &&
              (name[6] == "DistributionFunction6") &&
              (name[7] == "DistributionFunction7") &&
              (name[8] == "DistributionFunction8") &&
              (name[9] == "DistributionFunction9") &&
              (name[10] == "DistributionFunction10") &&
              (name[11] == "DistributionFunction11") &&
              (name[12] == "DistributionFunction12") );



  q(if0) = data[0];
  q(if1) = data[1];
  q(if2) = data[2];
  q(if3) = data[3];
  q(if4) = data[4];
  q(if5) = data[5];
  q(if6) = data[6];
  q(if7) = data[7];
  q(if8) = data[8];
  q(if9) = data[9];
  q(if10) = data[10];
  q(if11) = data[11];
  q(if12) = data[12];

}


template <template <class> class PDETraitsSize>
template <class T>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const PrimitiveDistributionFunctionsD2Q13<T>& data ) const
{
  q(if0) = data.DistributionFunction0;
  q(if1) = data.DistributionFunction1;
  q(if2) = data.DistributionFunction2;
  q(if3) = data.DistributionFunction3;
  q(if4) = data.DistributionFunction4;
  q(if5) = data.DistributionFunction5;
  q(if6) = data.DistributionFunction6;
  q(if7) = data.DistributionFunction7;
  q(if8) = data.DistributionFunction8;
  q(if9) = data.DistributionFunction9;
  q(if10) = data.DistributionFunction10;
  q(if11) = data.DistributionFunction11;
  q(if12) = data.DistributionFunction12;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const // TODO: Is this valid?
{
  // see Leitao Chen & Laura Schaefer '15, DOI: https://doi.org/10.1063/1.4907782
  // c = 1.0 --> csq = 1/2;
  const Real csq = 1/2.;
  const Real rho = q(if0) + q(if1) + q(if2) + q(if3) + q(if4) + q(if5) + q(if6) + q(if7) + q(if8) +
                  q(if9) + q(if10) + q(if11) + q(if12);
  const Real p   = csq*rho;  // Krueger, (Book) LBM principals and practice eq. 7.15, Springer '17

  const Real drho = dq(if0) + dq(if1) + dq(if2) + dq(if3) + dq(if4) + dq(if5) + dq(if6) + dq(if7) + dq(if8) +
                  dq(if9) + dq(if10) + dq(if11) + dq(if12);

  const Real dp   = csq*drho;

  Real wr = 1, wp = 1;

  // density check
  Real nrho = rho - drho;

  // Compute wr such that:

  //rho - wr*drho >= (1-maxChangeFraction)*rho
  if (nrho < (1-maxChangeFraction)*rho)
    wr =  maxChangeFraction*rho/drho;

  //rho - wr*drho <= (1+maxChangeFraction)*rho
  if (nrho > (1+maxChangeFraction)*rho)
    wr = -maxChangeFraction*rho/drho;

  // pressure check
  Real np = p - dp;

  // Compute wp such that:

  //p - wp*dp >= (1-maxChangeFraction)*p
  if (np < (1-maxChangeFraction)*p)
    wp =  maxChangeFraction*p/dp;

  //p - wp*dp <= (1+maxChangeFraction)*p
  if (np > (1+maxChangeFraction)*p)
    wp = -maxChangeFraction*p/dp;

  updateFraction = MIN(wr, wp);
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  int flag = 0;
  for (int i=0; i<13; i++)
    if (q(i) == 0.)
      flag = 1;

  if (flag == 1)
    return false;
  else
    return true;
}


template <template <class> class PDETraitsSize>
void
QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "QD2Q13<QTypePrimitiveDistributionFunctions, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // QD2Q13PRIMITIVEDISTRIBUTIONFUNCTIONS_H
