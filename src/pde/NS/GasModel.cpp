// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "GasModel.h"

namespace SANS
{

//---------------------------------------------------------------------------//
void GasModelParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gamma));
  allParams.push_back(d.checkInputs(params.R));
  d.checkUnknownInputs(allParams);
}
GasModelParams GasModelParams::params;


void
GasModel::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "GasModel<T>:" << std::endl;
  out << indent << "  gamma_ = " << gamma_ << std::endl;
  out << indent << "  R_ = " << R_ << std::endl;
}


//---------------------------------------------------------------------------//
void ViscosityModel_SutherlandParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.tSuth));
  allParams.push_back(d.checkInputs(params.tRef));
  allParams.push_back(d.checkInputs(params.muRef));
  d.checkUnknownInputs(allParams);
}
ViscosityModel_SutherlandParams ViscosityModel_SutherlandParams::params;

void ViscosityModel_Sutherland::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ViscosityModel<T>:" << std::endl;
  out << indent << "  tSuth_ = " << tSuth_ << std::endl;
  out << indent << "  tRef_ = " << tRef_ << std::endl;
  out << indent << "  muRef_ = " << muRef_ << std::endl;
}


//---------------------------------------------------------------------------//
void ThermalConductivityModel::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ThermalConductivityModel<T>:" << std::endl;
  out << indent << "  Prandtl_ = " << Prandtl_ << std::endl;
  out << indent << "  Cp_ = " << Cp_ << std::endl;
}


//---------------------------------------------------------------------------//
void ViscosityModel_ConstParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.muRef));
  d.checkUnknownInputs(allParams);
}
ViscosityModel_ConstParams ViscosityModel_ConstParams::params;

void ViscosityModel_Const::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "ViscosityModel<T>:" << std::endl;
  out << indent << "  muRef_ = " << muRef_ << std::endl;
}


}
