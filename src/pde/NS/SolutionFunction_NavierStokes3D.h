// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NAVIERSTOKESSOLUTIONFUNCTION3D_H
#define NAVIERSTOKESSOLUTIONFUNCTION3D_H

// 2-D NavierStokes PDE: exact and MMS solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // pow

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "NSVariable3D.h"
#include "getEulerTraitsModel.h"

#include "pde/AnalyticFunction/Function3D.h"

namespace SANS
{

// forward declare: solution variables interpreter
template<class QType, template<class > class PDETraitsSize>
class Q3D;
//----------------------------------------------------------------------------//
// solution: Tayor Green Initial condition

struct SolutionFunction_NavierStokes3D_TaylorGreen_Params: noncopyable
{
  const ParameterDict gasModel { "Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> rhoRef{"rhoRef", NO_DEFAULT, NO_RANGE, "Reference density"};
  const ParameterNumeric<Real> qRef{"qRef", NO_DEFAULT, NO_RANGE, "Reference dynamic pressure"};
  const ParameterNumeric<Real> pRef{"pRef", NO_DEFAULT, NO_RANGE, "Reference pressure"};
  const ParameterNumeric<Real> lRef{"lRef", NO_DEFAULT, NO_RANGE, "Reference Length"};

static void checkInputs(PyDict d);

static SolutionFunction_NavierStokes3D_TaylorGreen_Params params;
};

template<template<class > class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_NavierStokes3D_TaylorGreen: public Function3DVirtualInterface<
    SolutionFunction_NavierStokes3D_TaylorGreen<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD3>>
{
public:
  typedef PhysD3 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;
  typedef typename TraitsModel::QType QType;

  typedef Q3D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_NavierStokes3D_TaylorGreen_Params ParamsType;

  explicit SolutionFunction_NavierStokes3D_TaylorGreen(const PyDict& d)
      : qinterp_( GasModel( d.get( ParamsType::params.gasModel ) ) ),
        rhoRef_(d.get( ParamsType::params.rhoRef ) ),
        qRef_(d.get( ParamsType::params.qRef ) ),
        pRef_(d.get( ParamsType::params.pRef ) ),
        lRef_(d.get( ParamsType::params.lRef ) )
  {
  }
  explicit SolutionFunction_NavierStokes3D_TaylorGreen(const GasModel& gas, const Real rhoRef, const Real qRef, const Real pRef, const Real lRef)
      : qinterp_( gas ), rhoRef_(rhoRef), qRef_(qRef), pRef_(pRef), lRef_(lRef)
  {
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    // Assuming V0 = 1, L = 1 for a domain -Pi to +Pi
    T rho = rhoRef_;
    T u = qRef_ * sin( x / lRef_) * cos( y / lRef_ ) * cos( z / lRef_ );
    T v = -qRef_ * cos( x / lRef_ ) * sin( y / lRef_ ) * cos( z / lRef_ );
    T w = 0.;
    T p = pRef_ + (rhoRef_ * qRef_ * qRef_ / 16) * (cos( 2 * x / lRef_ ) + cos( 2 * y / lRef_ )) * (cos( 2 * z / lRef_ ) + 2);

    ArrayQ<T> q = 0;
    qinterp_.setFromPrimitive( q, DensityVelocityPressure3D<T>( rho, u, v, w, p ) );

    return q;
  }

private:
  const QInterpret qinterp_;
  const Real rhoRef_;
  const Real qRef_;
  const Real pRef_;
  const Real lRef_;
};


struct SolutionFunction_NavierStokes3D_OjedaMMS_Params: noncopyable
{
  const ParameterDict gasModel { "Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

static void checkInputs(PyDict d);

static SolutionFunction_NavierStokes3D_OjedaMMS_Params params;
};

template<template<class > class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_NavierStokes3D_OjedaMMS: public Function3DVirtualInterface<
SolutionFunction_NavierStokes3D_OjedaMMS<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD3>>
{
public:
  typedef PhysD3 PhysDim;

  typedef typename getEulerTraitsModel<PDETraitsModel>::type TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q3D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_NavierStokes3D_OjedaMMS_Params ParamsType;

  explicit SolutionFunction_NavierStokes3D_OjedaMMS(const PyDict& d)
      : qinterp_( GasModel( d.get( ParamsType::params.gasModel ) ) )
  {
  }
  explicit SolutionFunction_NavierStokes3D_OjedaMMS(const GasModel& gas)
      : qinterp_( gas )
  {
  }

  template<class T>
  ArrayQ<T> operator()(const T& x, const T& y, const T& z, const T& time) const
  {
    T rho, u, v, w, p;

    //POSITIVE BUMP
    rho = 1.0 + 0.1*exp( -(x-0.5)*(x-0.5)*75. -(y-0.5)*(y-0.5)*75.  -(z-0.5)*(z-0.5)*75. );
    u = 0.2 + 0.3*exp( -(x-0.5)*(x-0.5)*75. -(y-0.5)*(y-0.5)*75.  -(z-0.5)*(z-0.5)*75. );
    v =  0.1 + 0.1*exp(-(x-0.5)*(x-0.5)*75. -(y-0.5)*(y-0.5)*75.  -(z-0.5)*(z-0.5)*75. );
    w =  0.15 - 0.05*exp( -(x-0.5)*(x-0.5)*75. -(y-0.5)*(y-0.5)*75.  -(z-0.5)*(z-0.5)*75. );
    p = pow(rho, 1.4);

    ArrayQ<T> q = 0;
    qinterp_.setFromPrimitive(q, DensityVelocityPressure3D<T>( rho, u, v, w, p ) );

    return q;
  }

private:
  const QInterpret qinterp_;
};

} // namespace SANS

#endif  // NAVIERSTOKESSOLUTIONFUNCTION3D_H
