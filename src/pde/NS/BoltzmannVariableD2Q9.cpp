// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BoltzmannVariableD2Q9.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<BoltzmannVariableTypeD2Q9Params::VariableOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<BoltzmannVariableTypeD2Q9Params::VariableOptions> > const&) const;

void PrimitiveDistributionFunctionsD2Q9Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pdf0));
  allParams.push_back(d.checkInputs(params.pdf1));
  allParams.push_back(d.checkInputs(params.pdf2));
  allParams.push_back(d.checkInputs(params.pdf3));
  allParams.push_back(d.checkInputs(params.pdf4));
  allParams.push_back(d.checkInputs(params.pdf5));
  allParams.push_back(d.checkInputs(params.pdf6));
  allParams.push_back(d.checkInputs(params.pdf7));
  allParams.push_back(d.checkInputs(params.pdf8));
  d.checkUnknownInputs(allParams);
}
PrimitiveDistributionFunctionsD2Q9Params PrimitiveDistributionFunctionsD2Q9Params::params;



void BoltzmannVariableTypeD2Q9Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
BoltzmannVariableTypeD2Q9Params BoltzmannVariableTypeD2Q9Params::params;

}
