// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BOLTZMANNVARIABLED1Q3_H
#define BOLTZMANNVARIABLED1Q3_H

#include "Python/PyDict.h" // python must be included first
#include "Python/Parameter.h"

#include <initializer_list>

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

namespace SANS
{

//===========================================================================//
template<class Derived>
struct BoltzmannVariableTypeD1Q3
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//===========================================================================//
struct PrimitiveDistributionFunctionsD1Q3Params : noncopyable
{
  const ParameterNumeric<Real> pdf0{"pdf0", NO_DEFAULT, NO_RANGE, "DistributionFunction0"};
  const ParameterNumeric<Real> pdf1{"pdf1", NO_DEFAULT, NO_RANGE, "DistributionFunction1"};
  const ParameterNumeric<Real> pdf2{"pdf2", NO_DEFAULT, NO_RANGE, "DistributionFunction2"};

  static void checkInputs(PyDict d);

  static PrimitiveDistributionFunctionsD1Q3Params params;
};

//---------------------------------------------------------------------------//
template<class T>
struct PrimitiveDistributionFunctionsD1Q3 : public BoltzmannVariableTypeD1Q3<PrimitiveDistributionFunctionsD1Q3<T>>
{
  typedef PrimitiveDistributionFunctionsD1Q3Params ParamsType;

  PrimitiveDistributionFunctionsD1Q3() {}

  explicit PrimitiveDistributionFunctionsD1Q3( const PyDict& d )
    : DistributionFunction0( d.get(PrimitiveDistributionFunctionsD1Q3Params::params.pdf0) ),
      DistributionFunction1( d.get(PrimitiveDistributionFunctionsD1Q3Params::params.pdf1) ),
      DistributionFunction2( d.get(PrimitiveDistributionFunctionsD1Q3Params::params.pdf2) ) {}

  PrimitiveDistributionFunctionsD1Q3( const T& pdf0, const T& pdf1, const T& pdf2 )
    : DistributionFunction0(pdf0), DistributionFunction1(pdf1), DistributionFunction2(pdf2) {}

  PrimitiveDistributionFunctionsD1Q3& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == 3);
    auto q = s.begin();
    DistributionFunction0 = *q; q++;
    DistributionFunction1 = *q; q++;
    DistributionFunction2 = *q;

    return *this;
  }

  T DistributionFunction0;
  T DistributionFunction1;
  T DistributionFunction2;
};

//===========================================================================//
struct BoltzmannVariableTypeD1Q3Params : noncopyable
{
  struct VariableOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Variables{"Variables", NO_DEFAULT, "Name of the variable set" };
    const ParameterString& key = Variables;

    const DictOption PrimitiveDistributionFunctions3{"Primitive DistributionFunction2", PrimitiveDistributionFunctionsD1Q3Params::checkInputs};

    const std::vector<DictOption> options{PrimitiveDistributionFunctions3};
  };
  const ParameterOption<VariableOptions> StateVector{"StateVector", NO_DEFAULT, "The state vector of variables"};

  static void checkInputs(PyDict d);

  static BoltzmannVariableTypeD1Q3Params params;
};


}

#endif //BOLTZMANNVARIABLED1Q3_H
