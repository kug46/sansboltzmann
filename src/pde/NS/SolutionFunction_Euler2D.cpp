// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/NS/SolutionFunction_Euler2D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_Euler2D_Const_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.v));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Euler2D_Const_Params SolutionFunction_Euler2D_Const_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Euler2D_Wake_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.wakedepth));
  allParams.push_back(d.checkInputs(params.Pt));
  allParams.push_back(d.checkInputs(params.Tt));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Euler2D_Wake_Params SolutionFunction_Euler2D_Wake_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Euler2D_Riemann_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.interface));
  allParams.push_back(d.checkInputs(params.rhoL));
  allParams.push_back(d.checkInputs(params.uL));
  allParams.push_back(d.checkInputs(params.vL));
  allParams.push_back(d.checkInputs(params.pL));
  allParams.push_back(d.checkInputs(params.rhoR));
  allParams.push_back(d.checkInputs(params.uR));
  allParams.push_back(d.checkInputs(params.vR));
  allParams.push_back(d.checkInputs(params.pR));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Euler2D_Riemann_Params SolutionFunction_Euler2D_Riemann_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Euler2D_Riemann_Dumbser_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.interfaceX));
  allParams.push_back(d.checkInputs(params.interfaceY));
  allParams.push_back(d.checkInputs(params.rhoTL));
  allParams.push_back(d.checkInputs(params.uTL));
  allParams.push_back(d.checkInputs(params.vTL));
  allParams.push_back(d.checkInputs(params.pTL));
  allParams.push_back(d.checkInputs(params.rhoTR));
  allParams.push_back(d.checkInputs(params.uTR));
  allParams.push_back(d.checkInputs(params.vTR));
  allParams.push_back(d.checkInputs(params.pTR));
  allParams.push_back(d.checkInputs(params.rhoBL));
  allParams.push_back(d.checkInputs(params.uBL));
  allParams.push_back(d.checkInputs(params.vBL));
  allParams.push_back(d.checkInputs(params.pBL));
  allParams.push_back(d.checkInputs(params.rhoBR));
  allParams.push_back(d.checkInputs(params.uBR));
  allParams.push_back(d.checkInputs(params.vBR));
  allParams.push_back(d.checkInputs(params.pBR));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Euler2D_Riemann_Dumbser_Params SolutionFunction_Euler2D_Riemann_Dumbser_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Euler2D_Vortex_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.xzero));
  allParams.push_back(d.checkInputs(params.yzero));
  allParams.push_back(d.checkInputs(params.beta));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Euler2D_Vortex_Params SolutionFunction_Euler2D_Vortex_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Euler2D_Taylor_Green_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.rhozero));
  allParams.push_back(d.checkInputs(params.pzero));
  allParams.push_back(d.checkInputs(params.knum));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Euler2D_Taylor_Green_Params SolutionFunction_Euler2D_Taylor_Green_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Euler2D_Temperature_Spot_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.xzero));
  allParams.push_back(d.checkInputs(params.yzero));
  allParams.push_back(d.checkInputs(params.vzero));
  allParams.push_back(d.checkInputs(params.theta));
  allParams.push_back(d.checkInputs(params.beta));
  allParams.push_back(d.checkInputs(params.xi));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Euler2D_Temperature_Spot_Params SolutionFunction_Euler2D_Temperature_Spot_Params::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Euler2D_TrigMMS_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  allParams.push_back(d.checkInputs(params.L));

  allParams.push_back(d.checkInputs(params.r0));
  allParams.push_back(d.checkInputs(params.rx));
  allParams.push_back(d.checkInputs(params.ry));
  allParams.push_back(d.checkInputs(params.rxy));
  allParams.push_back(d.checkInputs(params.arx));
  allParams.push_back(d.checkInputs(params.ary));
  allParams.push_back(d.checkInputs(params.arxy));

  allParams.push_back(d.checkInputs(params.u0));
  allParams.push_back(d.checkInputs(params.ux));
  allParams.push_back(d.checkInputs(params.uy));
  allParams.push_back(d.checkInputs(params.uxy));
  allParams.push_back(d.checkInputs(params.aux));
  allParams.push_back(d.checkInputs(params.auy));
  allParams.push_back(d.checkInputs(params.auxy));

  allParams.push_back(d.checkInputs(params.v0));
  allParams.push_back(d.checkInputs(params.vx));
  allParams.push_back(d.checkInputs(params.vy));
  allParams.push_back(d.checkInputs(params.vxy));
  allParams.push_back(d.checkInputs(params.avx));
  allParams.push_back(d.checkInputs(params.avy));
  allParams.push_back(d.checkInputs(params.avxy));

  allParams.push_back(d.checkInputs(params.p0));
  allParams.push_back(d.checkInputs(params.px));
  allParams.push_back(d.checkInputs(params.py));
  allParams.push_back(d.checkInputs(params.pxy));
  allParams.push_back(d.checkInputs(params.apx));
  allParams.push_back(d.checkInputs(params.apy));
  allParams.push_back(d.checkInputs(params.apxy));

  d.checkUnknownInputs(allParams);
}
SolutionFunction_Euler2D_TrigMMS_Params SolutionFunction_Euler2D_TrigMMS_Params::params;

}
