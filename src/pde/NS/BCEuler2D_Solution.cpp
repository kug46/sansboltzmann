// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "TraitsEuler.h"
#include "TraitsEulerArtificialViscosity.h"
#include "TraitsNavierStokes.h"
#include "TraitsRANSSA.h"

#include "BCEuler2D_Solution.h"
#include "BCRANSSA2D_Solution.h"

#include "Q2DPrimitiveRhoPressure.h"
#include "Q2DPrimitiveSurrogate.h"
#include "Q2DConservative.h"
#include "Q2DEntropy.h"

#include "BCEulermitAVSensor2D.h"

#include "Fluids2D_Sensor.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{
// Instantiate the paramter options
PYDICT_PARAMETER_OPTION_INSTANTIATE(BCEulerSolution2DOptions::SolutionOptions)

template <template <class> class PDETraitsSize, class PDETraitsModel>
typename BCEulerSolution2D<PDETraitsSize, PDETraitsModel>::Function_ptr
BCEulerSolution2D<PDETraitsSize, PDETraitsModel>::getSolution(const PyDict& d)
{
  BCEulerSolution2DParams<PDETraitsSize> params;

  DictKeyPair SolutionParam = d.get(params.Function);

  if ( SolutionParam == params.Function.Const )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_Euler2D_Const<PDETraitsSize, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.OjedaMMS )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_NavierStokes2D_OjedaMMS<PDETraitsSize, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.Hartmann )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_NavierStokes2D_Hartmann<PDETraitsSize, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.QuadraticMMS )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_NavierStokes2D_QuadraticMMS<PDETraitsSize, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.Riemann )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_Euler2D_Riemann<PDETraitsSize, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.Wake )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_Euler2D_Wake<PDETraitsSize, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.TrigMMS )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_Euler2D_TrigMMS<PDETraitsSize, PDETraitsModel>( SolutionParam ) ) );


  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized solution function specified");
  return NULL; // suppress compiler warnings
}

template struct BCEulerSolution2D< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> >;
template struct BCEulerSolution2D< TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel> >;
template struct BCEulerSolution2D< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveSurrogate, GasModel> >;
template struct BCEulerSolution2D< TraitsSizeEuler, TraitsModelEuler<QTypeEntropy, GasModel> >;

// Artificial Viscosity
typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;

typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>> PDEBaseSurrogate;
typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD2, PDEBaseSurrogate>> SensorSourceSurrogate;
typedef TraitsModelArtificialViscosity<PDEBaseSurrogate, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceSurrogate> TraitsModelAVSurrogate;
template struct BCEulerSolution2D< TraitsSizeEulerArtificialViscosity, TraitsModelAVSurrogate >;


template struct BCEulerSolution2D< TraitsSizeNavierStokes,
                                   TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeNavierStokes,
                                   TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeNavierStokes,
                                   TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeNavierStokes,
                                   TraitsModelNavierStokes<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;


template struct BCEulerSolution2D< TraitsSizeNavierStokes,
                                   TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel,
                                   ViscosityModel_Sutherland, ThermalConductivityModel> >;

template <class PDETraitsModel>
typename BCEulerSolution2D<TraitsSizeRANSSA, PDETraitsModel>::Function_ptr
BCEulerSolution2D<TraitsSizeRANSSA, PDETraitsModel>::getSolution(const PyDict& d)
{
  BCEulerSolution2DParams<TraitsSizeRANSSA> params;

  DictKeyPair SolutionParam = d.get(params.Function);

  if ( SolutionParam == params.Function.Const )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_RANSSA2D_Const<TraitsSizeRANSSA, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.TrigMMS )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_RANSSA2D_TrigMMS<TraitsSizeRANSSA, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.OjedaMMS )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_RANSSA2D_OjedaMMS<TraitsSizeRANSSA, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.SAWake )
    return Function_ptr( static_cast<Function2DBaseType*>(
                         new SolutionFunction_RANSSA2D_Wake<TraitsSizeRANSSA, PDETraitsModel>( SolutionParam ) ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized solution function specified");
  return NULL; // suppress compiler warnings
}

template struct BCEulerSolution2D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
template struct BCEulerSolution2D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypeEntropy, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;

} //namespace SANS
