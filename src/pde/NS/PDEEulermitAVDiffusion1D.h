// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEEULER1D_ARTIFICIALVISCOSITY1D_H
#define PDEEULER1D_ARTIFICIALVISCOSITY1D_H

// 1-D compressible Euler with artificial viscosity

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/smoothmath.h"
#include "tools/Tuple.h"

#include "Field/Tuple/ParamTuple.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "PDEEuler1D.h"
#include "EulerArtificialViscosityType.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 1D Artificial Viscosity wrapper for Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEEulermitAVDiffusion1D : public PDEEuler1D<PDETraitsSize, PDETraitsModel>
{
public:

  typedef PhysD1 PhysDim;

  typedef PDEEuler1D<PDETraitsSize, PDETraitsModel> BaseType;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables
  static const int Nparam = 1;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  template <class TP>
  using ArrayParam = DLA::VectorS<Nparam,TP>;  // parameter array type

  template <class T>
  using MatrixParam = DLA::MatrixS<N,Nparam,T>;         // e.g. jacobian of PDEs w.r.t. parameters

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs >
  PDEEulermitAVDiffusion1D(const int order, const bool hasSpaceTimeDiffusion, const EulerArtViscosity& visc, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    order_(order), hasSpaceTimeDiffusion_(hasSpaceTimeDiffusion), visc_(visc)
  {
    //Nothing
  }

  ~PDEEulermitAVDiffusion1D() {}

  PDEEulermitAVDiffusion1D& operator=( const PDEEulermitAVDiffusion1D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return BaseType::hasFluxAdvective(); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return BaseType::hasSource(); }
  bool hasSourceTrace() const { return BaseType::hasSourceTrace(); }
  bool hasForcingFunction() const { return BaseType::hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return BaseType::needsSolutionGradientforSource(); }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tp, class Tf>
  void fluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(x, time, q, ft);
  }

  // jacobian of master state wrt q: dU/dQ
  template <class T, class Tp, class Tf>
  void jacobianFluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(x, time, q, J);
  }

  // master state: U(Q)
  template <class T, class Tp, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // derivative of master state wrt q: dU(Q)/dQ
  template<class T, class Tp>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <class T, class Tp>
  void strongFluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const;

  // advective flux: F(Q)
  template <class Tp, class T, class Ts>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Ts>& f ) const
  {
    BaseType::fluxAdvective(x, time, q, f);
  }

  template <class Tp, class T, class Ts>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, ArrayQ<Ts>& f ) const
  {
    BaseType::fluxAdvectiveUpwind(x, time, qL, qR, nx, f);
  }

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
  {
    BaseType::fluxAdvectiveUpwindSpaceTime(x, time, qL, qR, nx, nt, f);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class T, class Ta>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x,  const Real& time, const ArrayQ<T>& q,
      MatrixQ<Ta>& ax ) const
  {
    BaseType::jacobianFluxAdvective( x, time, q, ax );
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const Real& nx,
      MatrixQ<T>& mtx ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValue( x, time, q, nx, mtx );
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class T>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& q,
      const Real& nx, const Real& nt,
      MatrixQ<T>& mtx ) const
  {
    BaseType::jacobianFluxAdvectiveAbsoluteValue( x, time, q, nx, nt, mtx );
  }

  // strong form advective fluxes
  template <class Tp, class T>
  void strongFluxAdvective(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      ArrayQ<T>& strongPDE ) const
  {
    BaseType::strongFluxAdvective( x, time, q, qx, strongPDE );
  }

  // viscous flux: Fv(Q, Qx)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& fn ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tqg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tp, class Tq, class Tqg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tqg, class Th, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tqg, class Th, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Th, class Ts, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class T>
  void jacobianFluxViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax ) const {}

  // strong form viscous fluxes
  template <class T, class L, class R>
  void strongFluxViscous(
      const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      const ArrayQ<T>& qxx,
      ArrayQ<T>& strongPDE ) const;

  // solution-dependent source: S(x, Q, Qx)
  template <class Tp, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, time, q, qx, source );
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const
  {
    BaseType::source( x, time, lifted_quantity, q, qx, source );
  }

  // dual-consistent source
  template <class Tp, class Tq, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    BaseType::sourceTrace(xL, xR, time, qL, qxL, qR, qxR, sourceL, sourceR);
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(xL, xR, time, qL, qR, s);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class T>
  void jacobianSource(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const
  {
    BaseType::jacobianSource(x, time, q, qx, dsdu);
  }

  // right-hand-side forcing function
  template <class Tp, class T>
  void forcingFunction( const Tp& param,
                        const Real& x, const Real& time, ArrayQ<T>& forcing ) const
  {
    BaseType::forcingFunction(x, time, forcing );
  }

  // characteristic speed (needed for timestep)
  template <class Tp, class T>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& time,
      const Real& dx, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, time, dx, q, speed);
  }

  // characteristic speed
  template <class Tp, class T>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, time, q, speed);
  }

  // update fraction needed for physically valid state
  template <class Tp>
  void updateFraction(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(x, time, q, dq, maxChangeFraction, updateFraction);
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  int getOrder() const { return order_; };

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const DLA::MatrixSymS<1, Real>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const DLA::MatrixSymS<2, Real>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const;

protected:

  // Artificial viscosity - scalar h, sensor variable in parameter
  template <class Ts, class Tq, class Tk>
  void artificialViscosity( const ParamTuple<Real, Ts, TupleClass<>>& param,
                            const Real& x, const Real& time, const ArrayQ<Tq>& q,
                            MatrixQ<Tk>& Kxx ) const;

  template <int Dim, class Tq, class Ts, class Te>
  void artificialViscosity( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Ts, TupleClass<>>& param,
                            const Real& x, const Real& time, const ArrayQ<Tq>& q,
                            DLA::MatrixSymS<D,Te>& eps ) const;

  template <class Tq, class Ts, class Te>
  void artificialViscosity( const Real& logH, Ts sensor,
                            const Real& x, const Real& time, const ArrayQ<Tq>& q,
                            MatrixQ<Te>& eps ) const;

  template <int Dim, class Tq, class Te>
  void artificialViscosity( const DLA::MatrixSymS<Dim,Real>& logH,
                            const Real& x, const Real& time, const ArrayQ<Tq>& q,
                            DLA::MatrixSymS<D,Te>& eps ) const;

  // Artificial viscosity - generalized H-tensor
  template <int Dim, class Tq, class Ts, class Te>
  void artificialViscosity( const DLA::MatrixSymS<Dim,Real>& logH, Ts sensor,
                            const Real& x, const Real& time, const ArrayQ<Tq>& q,
                            DLA::MatrixSymS<D,Te>& eps ) const;

  // Artificial viscosity - generalized H-tensor, sensor variable in parameter
  template <int Dim, class Ts, class Tq, class Tk>
  void artificialViscosity( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Ts, TupleClass<>>& param,
                            const Real& x, const Real& time, const ArrayQ<Tq>& q,
                            MatrixQ<Tk>& Kxx ) const;

  // Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
  template <int Dim, class Tq, class Tk>
  void artificialViscosity( const DLA::MatrixSymS<Dim,Real>& logH,
                            const Real& x, const Real& time, const ArrayQ<Tq>& q,
                            MatrixQ<Tk>& Kxx ) const;

  // Artificial viscosity - generalized H-tensor, common independent of sensor storage
  template <int Dim, class Tq, class Ts, class Tk>
  void artificialViscosity( const DLA::MatrixSymS<Dim,Real>& logH, Ts sensor,
                            const Real& x, const Real& time, const ArrayQ<Tq>& q,
                            MatrixQ<Tk>& Kxx ) const;

  // Artificial viscosity flux
  template <class Tp, class Tq, class Tg, class Tf>
  void artificialViscosityFlux( const Tp& param, const Real& x, const Real& time,
                                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                                ArrayQ<Tf>& f ) const;

  template <class T, class Th, class Tg, class Ts>
  void artificialViscositySpaceTime( const Real& x, const Real& time, const ArrayQ<T>& q,
                                     const DLA::MatrixSymS<D+1,Th>& logH, Ts sensor,
                                     MatrixQ<Tg>& Kxx, MatrixQ<Tg>& Kxt, MatrixQ<Tg>& Ktx, MatrixQ<Tg>& Ktt ) const;

  template <class T, class Th, class Ts>
  void artificialViscositySpaceTime( const Real& x, const Real& time, const ArrayQ<T>& q,
                                     const DLA::MatrixSymS<D+1,Th>& logH,
                                     MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxt, MatrixQ<Ts>& Ktx, MatrixQ<Ts>& Ktt ) const;


  const int order_;
  const bool hasSpaceTimeDiffusion_;
  const EulerArtViscosity visc_;
  const Real Prandtl_ = 0.72;

  using BaseType::gas_;
  using BaseType::qInterpret_;
};

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <int Dim, class Tq, class Ts, class Te>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Ts, TupleClass<>>& param,
                     const Real& x, const Real& time, const ArrayQ<Tq>& q,
                     DLA::MatrixSymS<D,Te>& eps ) const
{
  static_assert(N == 3, "Sensor should be in parameter field");

  artificialViscosity(param.left(), param.right(), x, time,
                      q, eps);
}

// Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <int Dim, class Tq, class Te>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity(
    const DLA::MatrixSymS<Dim,Real>& logH,
    const Real& x, const Real& time, const ArrayQ<Tq>& q,
    DLA::MatrixSymS<D,Te>& eps ) const
{
  //Check if the state vector has been augmented with the sensor
  static_assert(N >= 4, "Sensor should be in state vector");

  artificialViscosity(logH, q(N-1), x, time,
                      q, eps);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <int Dim, class Tq, class Ts, class Te>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity( const DLA::MatrixSymS<Dim,Real>& logH, Ts sensor,
                     const Real& x, const Real& time, const ArrayQ<Tq>& q,
                     DLA::MatrixSymS<D,Te>& eps ) const
{
  typedef typename promote_Surreal<Tq,Ts>::type Tqs;

  DLA::MatrixSymS<Dim,Real> H = exp(logH); //generalized h-tensor
  Tq lambda;
  BaseType::speedCharacteristic( x, time, q, lambda );

  Real C = 2.0;

  Ts z = 0;

//  sensor = smoothmax(sensor, z, 40.0);
  sensor = max(sensor, z);

  Tqs factor = C/(Real(max(order_,1))) * lambda * sensor;

  for (int i = 0; i < D; i++)
    for (int j = 0; j <= i; j++)
        eps(i,j) = factor*H(i,j); //Artificial viscosity
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Ts, class Te>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity( const Real& logH, Ts sensor,
                     const Real& x, const Real& time, const ArrayQ<Tq>& q,
                     MatrixQ<Te>& eps ) const
{
  typedef typename promote_Surreal<Tq,Ts>::type Tqs;

  Real H = exp(logH); //generalized h-tensor
  Tq lambda;
  BaseType::speedCharacteristic( x, time, q, lambda );

  Real C = 2.0;

  Ts z = 0;

//  sensor = smoothmax(sensor, z, 40.0);
  sensor = max(sensor, z);

  Tqs factor = C/(Real(max(order_,1))) * lambda * sensor;

  eps = factor*H; //Artificial viscosity
}

// Artificial viscosity - generalized H-tensor, sensor variable in parameter
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <int Dim, class Ts, class Tq, class Tk>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity( const ParamTuple<DLA::MatrixSymS<Dim,Real>, Ts, TupleClass<>>& param,
                     const Real& x, const Real& time, const ArrayQ<Tq>& q,
                     MatrixQ<Tk>& Kxx ) const
{
  static_assert(N == 3, "Sensor should be in parameter field");

  artificialViscosity(param.left(), param.right(), x, time,
                      q,
                      Kxx);
}

// Artificial viscosity - generalized H-tensor, sensor variable in parameter
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Ts, class Tq, class Tk>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity( const ParamTuple<Real, Ts, TupleClass<>>& param,
                     const Real& x, const Real& time, const ArrayQ<Tq>& q,
                     MatrixQ<Tk>& Kxx ) const
{
  static_assert(N == 3, "Sensor should be in parameter field");

  artificialViscosity(param.left(), param.right(), x, time,
                      q,
                      Kxx);
}

// Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <int Dim, class Tq, class Tk>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity(
    const DLA::MatrixSymS<Dim,Real>& logH,
    const Real& x, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tk>& Kxx ) const
{
  //Check if the state vector has been augmented with the sensor
  static_assert(N >= 4, "Sensor should be in state vector");

  artificialViscosity(logH, q(N-1), x, time,
                      q,
                      Kxx);
}

// Artificial viscosity - generalized H-tensor, sensor variable in augmented state vector
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <int Dim, class Tq, class Ts, class Tk>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscosity(
    const DLA::MatrixSymS<Dim,Real>& logH, Ts sensor,
    const Real& x, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tk>& kxx ) const
{
  typedef typename promote_Surreal<Tq,Ts>::type Tqs;

  DLA::MatrixSymS<D,Tqs> eps; //Artificial viscosity
  artificialViscosity( logH, sensor, x, time, q, eps );

  if (visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy)
  {

    // Energy to Enthalpy jacobian
    ArrayQ<Tq> dutdu = 0;

    Real gamma = BaseType::gas_.gamma();
    Tq rho = 0, u = 0, t = 0;
    BaseType::qInterpret_.eval( q, rho, u, t );

    dutdu(BaseType::iCont) = 0.5*(gamma - 1.0)*(u*u);
    dutdu(BaseType::ixMom) = -(gamma - 1.0)*u;
    dutdu(BaseType::iEngy) = gamma;

    static_assert( BaseType::iCont == 0 &&
                   BaseType::iEngy > BaseType::iCont &&
                   BaseType::iEngy > BaseType::ixMom, "Below loops assume energy is last");

    // Don't add diffusion to the sensor equation
    for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
    {
      kxx(i,i) += eps(0,0); // [length^2 / time]
    }

    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      // This changes the gradient to be on Enthalpy rather than Energy
      kxx(BaseType::iEngy,i) += eps(0,0)*dutdu(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eLaplaceViscosityEnergy)
  {
    // Don't add diffusion to the sensor equation
    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      kxx(i,i) += eps(0,0); // [length^2 / time]
    }
  }
  else if (visc_ == EulerArtViscosity::eShearViscosity)
  {
    Tq rho=0, u=0, t=0;

    qInterpret_.eval( q, rho, u, t );

    Real Cv = gas_.Cv();
    Real Cp = gas_.Cp();

    DLA::MatrixSymS<D,Tqs> k=0;
    for (int d = 0; d < D; d++)
      k(d,d) = eps(d,d)*Cp/Prandtl_;

    Tq e0 = gas_.energy(rho, t) + 0.5*(u*u);

    // defining nu here as mu/rho (and hence lambda/rho as well as k/(Cv*rho)) to remove rho division throughout the K matrix calculation
    DLA::MatrixSymS<D,Tqs> nu = eps/rho;
    DLA::MatrixSymS<D,Tqs> lambda = -2./3. * eps;
    DLA::MatrixSymS<D,Tqs> kn = k/Tq(Cv*rho);

    // d(Fv)/d(Ux)
    kxx(BaseType::ixMom,0) += -u*(2*nu(0,0) + lambda(0,0));
    kxx(BaseType::ixMom,1) +=    (2*nu(0,0) + lambda(0,0));

    kxx(BaseType::iEngy,0) += -u*u*((2*nu(0,0) + lambda(0,0)) - kn(0,0)) - e0*kn(0,0);
    kxx(BaseType::iEngy,1) +=    u*((2*nu(0,0) + lambda(0,0)) - kn(0,0));
    kxx(BaseType::iEngy,2) +=                                                 kn(0,0);
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown artificial viscosity model");
}

// Space-time artificial viscosity calculation with generalized h-tensor
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Th, class Ts>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscositySpaceTime(
    const Real& x, const Real& time, const ArrayQ<T>& q,
    const DLA::MatrixSymS<D+1,Th>& logH,
    MatrixQ<Ts>& Kxx, MatrixQ<Ts>& Kxt, MatrixQ<Ts>& Ktx, MatrixQ<Ts>& Ktt ) const
{
  //Check if the state vector has been augmented with the sensor
  static_assert(N >= 4, "Sensor should be in state vector");

  artificialViscositySpaceTime(x, time, q, logH, q(N-1), Kxx, Kxt, Ktx, Ktt );
}


// Space-time artificial viscosity calculation with generalized h-tensor
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Th, class Tg, class Ts>
void inline
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
artificialViscositySpaceTime(
    const Real& x, const Real& time, const ArrayQ<Tq>& q,
    const DLA::MatrixSymS<D+1,Th>& logH, Ts sensor,
    MatrixQ<Tg>& Kxx, MatrixQ<Tg>& Kxt, MatrixQ<Tg>& Ktx, MatrixQ<Tg>& Ktt ) const
{
//  T lambda;
//  Real C = 10.0;
//
//  BaseType::speedCharacteristic( x, time, q, lambda );
//
//  Ts factor = C/(Real(order_)+1) * smoothabs0(sensor, 1.0e-5);

  DLA::MatrixSymS<D+1,Th> H = exp(logH); //generalized h-tensor
  typedef typename promote_Surreal<Tq,Ts>::type Tqs;

  Tq lambda;
  BaseType::speedCharacteristic( x, time, q, lambda );

  Real C = 2.0;

  Ts z = 0;

//  sensor = smoothmax(sensor, z, 40.0);
  sensor = max(sensor, z);

  Tqs factor = C/(Real(max(order_,1))) * sensor;

  for (int i = 0; i < N; i++)
  {
    Kxx(i,i) += factor*lambda*H(0,0); // [length^2 / time]
  }

  if (hasSpaceTimeDiffusion_)
  {
    for (int i = 0; i < N; i++)
    {
      Kxt(i,i) += factor*H(0,1); // [length]
      Ktx(i,i) += factor*H(1,0); // [length]
      Ktt(i,i) += factor*H(1,1); // [time]
    }
  }

}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Tu>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::masterState(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  BaseType::masterState(x, time, q, uCons);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
   BaseType::jacobianMasterState(x, time, q, dudq);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
{
  BaseType::strongFluxAdvectiveTime(x, time, q, qt, dudt);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::artificialViscosityFlux(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& fx ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type Tqg;
  Real A, dAdx;
  ArrayQ<Tf> f = 0;

  // Get area
  BaseType::area(x, time, A, dAdx);

  if (visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy)
  {
    MatrixQ<Tf> nu_xx = 0.0;
    MatrixQ<Tq> dudq = 0;

    artificialViscosity( param, x, time, q, nu_xx );

    BaseType::jacobianMasterState( x, time, q, dudq );
    ArrayQ<Tqg> ux = dudq*qx;

    static_assert( BaseType::iCont == 0 &&
                   BaseType::iEngy > BaseType::iCont &&
                   BaseType::iEngy > BaseType::ixMom, "Below loops assume energy is last");

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
    {
      f(i) -= nu_xx(i,i)*ux(i);
    }

    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      f(BaseType::iEngy) -= nu_xx(BaseType::iEngy,i)*ux(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eLaplaceViscosityEnergy)
  {
    MatrixQ<Tf> nu_xx = 0.0;
    MatrixQ<Tq> dudq = 0;

    artificialViscosity( param, x, time, q, nu_xx );

    BaseType::jacobianMasterState( x, time, q, dudq );
    ArrayQ<Tqg> ux = dudq*qx;

    // Expand out the matmul to reduce computational time
    for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
    {
      f(i) -= nu_xx(i,i)*ux(i);
    }
  }
  else if (visc_ == EulerArtViscosity::eShearViscosity)
  {
    SANS_DEVELOPER_EXCEPTION("PDEEulermitAVDiffusion1D::artificialViscosityFlux not unit tested"
        "with EulerArtViscosity::eShearViscosity");
#if 0
    Tq rho=0, u=0, t=0;
    Tqg rhox=0, rhoy=0, ux=0, tx=0;

    DLA::MatrixSymS<D,Tf> eps; //Artificial viscosity
    artificialViscosity( param, x, time, q, eps );

    Real Cp = gas_.Cp();

    DLA::MatrixSymS<D,Tf> k=0;
    for (int d = 0; d < D; d++)
      k(d,d) = eps(d,d)*Cp/Prandtl_;

    qInterpret_.eval( q, rho, u, t );
    qInterpret_.evalGradient( q, qx, rhox, ux, tx );

    DLA::MatrixSymS<D,Tf> lambda = -2./3. * eps;

    Tf tauxx = eps(0,0)*(2*ux   ) + lambda(0,0)*(ux);

    f(BaseType::ixMom) -= tauxx;
    f(BaseType::iEngy) -= k(0,0)*tx + u*tauxx;
#endif
  }
  else
    SANS_DEVELOPER_EXCEPTION("Unknown artificial viscosity model");

  fx += A*f;
}

// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& fx ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  artificialViscosityFlux( param, x, time, q, qx, fx );
  BaseType::fluxViscous( x, time, q, qx, fx );
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    const Real& nx, ArrayQ<Tf>& fn ) const
{
  SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

  ArrayQ<Tf> fxL = 0, fxR = 0;

  artificialViscosityFlux(paramL, x, time, qL, qxL, fxL);
  artificialViscosityFlux(paramR, x, time, qR, qxR, fxR);

  //Compute the average artificial viscosity flux from both sides
  fn += 0.5*(fxL + fxR)*nx;

  //Add on any interface viscous fluxes from the base class
  BaseType::fluxViscous( x, time, qL, qxL, qR, qxR, nx, fn );
}


// space-time viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tqg, class Tf>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
fluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qt,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  SANS_ASSERT(visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy); //space-time diffusion is Laplacian only

  typedef typename promote_Surreal<Tq,Tqg>::type T;

  MatrixQ<Tf> nu_xx = 0.0, nu_xt = 0.0;
  MatrixQ<Tf> nu_tx = 0.0, nu_tt = 0.0;
  MatrixQ<Tq> dudq = 0;

  // Add space-time artificial viscosity
  artificialViscositySpaceTime( x, time, q, param,
                                nu_xx, nu_xt, nu_tx, nu_tt );

  BaseType::jacobianMasterState( x, time, q, dudq );
  ArrayQ<T> ux = dudq*qx;
  ArrayQ<T> ut = dudq*qt;

  f -= nu_xx*ux + nu_xt*ut;
  g -= nu_tx*ux + nu_tt*ut;

  BaseType::fluxViscousSpaceTime( x, time, q, qx, qt, f, g );
}

// space-time viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tqg, class Tf>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  SANS_ASSERT(visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy); //space-time diffusion is Laplacian only

  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscousSpaceTime(paramL, x, time, qL, qxL, qtL, fL, gL);
  fluxViscousSpaceTime(paramR, x, time, qR, qxR, qtR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*nt;
}

// space-time viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tqg, class Th, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
fluxViscousSpaceTime(
    const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqg>& qx, const ArrayQ<Tqg>& qt,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  typedef typename promote_Surreal<Tq,Tqg>::type T;
  Real A, dAdx;

  // Get area
  BaseType::area(x, time, A, dAdx);

  MatrixQ<Tf> nu_xx = 0.0, nu_xt = 0.0;
  MatrixQ<Tf> nu_tx = 0.0, nu_tt = 0.0;
  MatrixQ<Tq> dudq = 0;

  const Th& Hparam = param.left();
  const Tg& sensor = param.right();

  // Add space-time artificial viscosity
  artificialViscositySpaceTime( x, time, q, Hparam, sensor,
                                nu_xx, nu_xt, nu_tx, nu_tt );

  BaseType::jacobianMasterState( x, time, q, dudq );
  ArrayQ<T> ux = dudq*qx;
  ArrayQ<T> ut = dudq*qt;

  static_assert( BaseType::iCont == 0 &&
                 BaseType::iEngy > BaseType::iCont &&
                 BaseType::iEngy > BaseType::ixMom , "Below loops assume energy is last");

  // Expand out the matmul to reduce computational time
  for (int i = BaseType::iCont; i < BaseType::iEngy; i++)
  {
    f(i) -= (nu_xx(i,i)*ux(i) + nu_xt(i,i)*ut(i))*A;
    g(i) -= (nu_tx(i,i)*ux(i) + nu_tt(i,i)*ut(i))*A;
  }

  for (int i = BaseType::iCont; i <= BaseType::iEngy; i++)
  {
    f(BaseType::iEngy) -= (nu_xx(BaseType::iEngy,i)*ux(i) + nu_xt(BaseType::iEngy,i)*ut(i))*A;
    g(BaseType::iEngy) -= (nu_tx(BaseType::iEngy,i)*ux(i) + nu_tt(BaseType::iEngy,i)*ut(i))*A;
  }

  BaseType::fluxViscousSpaceTime( x, time, q, qx, qt, f, g );
}

// space-time viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tqg, class Th, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const ParamTuple<Th, Tg, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tqg>& qxL, const ArrayQ<Tqg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tqg>& qxR, const ArrayQ<Tqg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  SANS_ASSERT(visc_ == EulerArtViscosity::eLaplaceViscosityEnthalpy); //space-time diffusion is Laplacian only

  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscousSpaceTime(param, x, time, qL, qxL, qtL, fL, gL);
  fluxViscousSpaceTime(param, x, time, qR, qxR, qtR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*nt;
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tk>& kxx ) const
{
  artificialViscosity( param, x, time, q,
                       kxx );

  BaseType::diffusionViscous( x, time,
                              q, qx,
                              kxx );
}

// space-time viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Th, class Ts, class Tq, class Tg, class Tk>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
diffusionViscousSpaceTime(
    const ParamTuple<Th, Ts, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
{
  const Th& Hparam = param.left();
  const Ts& sensor = param.right();

  // Add space-time artificial viscosity
  artificialViscositySpaceTime( x, time, q, Hparam, sensor,
                                kxx, kxt,
                                ktx, ktt );

  BaseType::diffusionViscousSpaceTime( x, time, q, qx, qt, kxx, kxt, ktx, ktt );
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  std::vector<std::string> names = {
                                    "epsilon_density_xx",
                                    "epsilon_x-mom_xx",
                                    "epsilon_energy_xx",
                                    "hxx",
                                   };

  //Append any names from the base class
  std::vector<std::string> basenames = BaseType::derivedQuantityNames();
  names.insert(names.end(), basenames.begin(), basenames.end());

  return names;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const DLA::MatrixSymS<1, Real>& param, const Real& x, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const
{
  int nQtyCurrent = derivedQuantityNames().size() - BaseType::derivedQuantityNames().size();
  if (index >= nQtyCurrent)
  {
    BaseType::derivedQuantity(index - nQtyCurrent, x, time, q, qx, J);
    return;
  }

  // Most of the time we need this...
  MatrixQ<Tf> kxx = 0.0;
  artificialViscosity( param, x, time, q,
                       kxx);

  switch (index)
  {

  case 0:
  {
    J = kxx(0,0);
    break;
  }

  case 1:
  {
    J = kxx(1,1);
    break;
  }

  case 2:
  {
    J = kxx(2,2);
    break;
  }

  case 3:
  {
    DLA::MatrixSymS<1, Real> H = exp(param); //param is either MatrixSymS<D,Th> or MatrixSymS<D+1,Th> of log(H)
    J = H(0,0);
    break;
  }

  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEEulermitAVDiffusion1D::derivedQuantity - Invalid index!");
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const DLA::MatrixSymS<2, Real>& param, const Real& x, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, Tf& J ) const
{
  int nQtyCurrent = derivedQuantityNames().size() - BaseType::derivedQuantityNames().size();
  if (index >= nQtyCurrent)
  {
    BaseType::derivedQuantity(index - nQtyCurrent, x, time, q, qx, J);
    return;
  }

  // Most of the time we need this...
  MatrixQ<Tf> kxx = 0.0;
  MatrixQ<Tf> kxt = 0.0;
  MatrixQ<Tf> ktx = 0.0;
  MatrixQ<Tf> ktt = 0.0;
  artificialViscositySpaceTime( x, time, q, param,
                                kxx, kxt,
                                ktx, ktt );

  switch (index)
  {

  case 0:
  {
    J = kxx(0,0);
    break;
  }

  case 1:
  {
    J = kxx(1,1);
    break;
  }

  case 2:
  {
    J = kxx(2,2);
    break;
  }

  case 3:
  {
    DLA::MatrixSymS<2, Real> H = exp(param); //param is either MatrixSymS<D,Th> or MatrixSymS<D+1,Th> of log(H)
    J = H(0,0);
    break;
  }

  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEEulermitAVDiffusion1D::derivedQuantity - Invalid index!");
  }
}

// strong form of viscous flux: d(Fv)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class L, class R>
inline void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    const ArrayQ<T>& qxx,
    ArrayQ<T>& strongPDE ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEEulermitAVDiffusion1D<NDPDE>::strongFluxViscous is not implemented");

  BaseType::strongFluxViscous( x, time, q, qx, qxx, strongPDE );
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEEulermitAVDiffusion1D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEEulermitAVDiffusion1D1D: pde_ =" << std::endl;
  BaseType::dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEEULER1D_ARTIFICIALVISCOSITY1D_H
