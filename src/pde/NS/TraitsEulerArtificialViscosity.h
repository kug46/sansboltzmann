// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSEULERARTIFICIALVISCOSITY_H
#define TRAITSEULERARTIFICIALVISCOSITY_H

#include "TraitsEuler.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"

namespace SANS
{

template <class PhysDim_>
using TraitsSizeEulerArtificialViscosity = TraitsSizeArtificialViscosity<TraitsSizeEuler,PhysDim_>;

}

#endif // TRAITSEULERARTIFICIALVISCOSITY_H
