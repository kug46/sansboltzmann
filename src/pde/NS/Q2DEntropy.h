// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q2DENTROPY_H
#define Q2DENTROPY_H

// solution interpreter class
// Euler/N-S conservation-eqns: entropy variables

#include <vector>
#include <string>

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "pde/NS/NSVariable2D.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D Euler/N-S
// See: "A new finite element formulation for computational fluid dynamics: I.
//       Symmetric forms of the compressible Euler and Navier-Stokes equations
//       and the second law of thermodynamics" - T.J.R. Hughes, L.P. Franca,
//       M. Mallet
// Specifically equations 48-53
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// entropy variables
class QTypeEntropy;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q2D;


template <template <class> class PDETraitsSize>
class Q2D<QTypeEntropy, PDETraitsSize>
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  // The three components of the state vector that make up the velocity vector
  static const int ix = 1;
  static const int iy = 2;

  // TODO: What are the names??
  static std::vector<std::string> stateNames() { return {"q0", "q1", "q2", "q3"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q2D( const GasModel& gas0 ) : gas_(gas0) {}
  Q2D( const Q2D& q0 ) : gas_(q0.gas_) {}
  ~Q2D() {}

  Q2D& operator=( const Q2D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& v, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u, T& v ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                                         ArrayQ<T>& u_q, ArrayQ<T>& v_q,
                                         ArrayQ<T>& t_q ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type& ux,
    typename promote_Surreal<Tq,Tg>::type& vx,
    typename promote_Surreal<Tq,Tg>::type& tx ) const;

  template<class Tq, class Tg, class Th>
  void evalSecondGradient(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      typename promote_Surreal<Tq, Tg, Th>::type& rhoxx,
      typename promote_Surreal<Tq, Tg, Th>::type& rhoxy,
      typename promote_Surreal<Tq, Tg, Th>::type& rhoyy,
      typename promote_Surreal<Tq, Tg, Th>::type& uxx,
      typename promote_Surreal<Tq, Tg, Th>::type& uxy,
      typename promote_Surreal<Tq, Tg, Th>::type& uyy,
      typename promote_Surreal<Tq, Tg, Th>::type& vxx,
      typename promote_Surreal<Tq, Tg, Th>::type& vxy,
      typename promote_Surreal<Tq, Tg, Th>::type& vyy,
      typename promote_Surreal<Tq, Tg, Th>::type& txx,
      typename promote_Surreal<Tq, Tg, Th>::type& txy,
      typename promote_Surreal<Tq, Tg, Th>::type& tyy ) const;

  template<class Tq, class Tqp, class T>
  void conservativePerturbation(const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<T>& dU) const
  {
    Tq rho = 0, u = 0, v = 0, t = 0;

    Real gamma = gas_.gamma();
  //  Real  s0 = 0;

    eval( q, rho, u, v, t );
    Tq p  = gas_.pressure(rho, t);

    Tq h = gas_.enthalpy(rho, t);
    Tq H = h + 0.5*(u*u + v*v);

    dU[0] =      rho    * dq(0) +      rho*u    * dq(1) +      rho*v    * dq(2) * (rho*H - p) * dq(3);
    dU[1] =      rho*u  * dq(0) + (p + rho*u*u) * dq(1) +      rho*u*v  * dq(2) *    rho*u*H  * dq(3);
    dU[2] =      rho*v  * dq(0) +      rho*u*v  * dq(1) + (p + rho*v*v) * dq(2) *    rho*v*H  * dq(3);
    dU[3] = (rho*H - p) * dq(0) +      rho*u*H  * dq(1) +      rho*v*H  * dq(2) * (rho*H*H - p*p/rho * (gamma)/(gamma-1)) * dq(3);
  }

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure2D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative2D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& v, T& t ) const
{
  Real gamma = gas_.gamma();
  Real gm1 = gamma - 1.0;
  T s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  T rhoe = pow(gm1/pow(-q(3), gamma), 1.0/gm1) * exp(-s/gm1);

  rho = rhoe * -q(3);
  u   = rhoe *  q(1)/rho;
  v   = rhoe *  q(2)/rho;
  t   = rhoe / (rho * gas_.Cv());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  Real gamma = gas_.gamma();
  T s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  T rhoe = pow((gamma-1.0)/pow(-q(3),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));

  rho = rhoe * -q(3);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u, T& v ) const
{
  Real gamma = gas_.gamma();
  T s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  T rhoe = pow((gamma-1.0)/pow(-q(3),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));

  T rho = rhoe * -q(3);
  u     = rhoe *  q(1)/rho;
  v     = rhoe *  q(2)/rho;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  Real gamma = gas_.gamma();
  T s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  T rhoe = pow((gamma-1.0)/pow(-q(3),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));

  T rho = rhoe * -q(3);
  t   = rhoe / (rho * gas_.Cv());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                        ArrayQ<T>& u_q, ArrayQ<T>& v_q,
                        ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; v_q = 0; t_q = 0;

  Real gamma = gas_.gamma();
  Real gm1 = gamma - 1.0;
  T s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  T rhoe = pow(gm1/pow(-q(3), gamma), 1.0/gm1) * exp(-s/gm1);

  // s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  T s_q0 = -1;
  T s_q1 = (q(1))/q(3);
  T s_q2 = (q(2))/q(3);
  T s_q3 = -(q(1)*q(1) + q(2)*q(2))/(q(3)*q(3));

  // rhoe = pow(gm1/pow(-q(3),gamma),1.0/gm1) * exp(-s/gm1);
  T rhoe_q0 = rhoe*(-s_q0/gm1);
  T rhoe_q1 = rhoe*(-s_q1/gm1);
  T rhoe_q2 = rhoe*(-s_q2/gm1);
  //T rhoe_q3 = rhoe*(-s_q3/gm1) - (pow(gm1/pow(-q(3),gamma),1/gm1)*gamma)/(q(3)*gm1) * exp(-s/gm1);
  T rhoe_q3 = rhoe * gamma / gm1 / (-q(3)) - rhoe / gm1 * s_q3;

  T rho = rhoe * -q(3);

  // rho   = rhoe * -q(3);
  rho_q[0] = rhoe_q0 * -q(3);        // drho/dq0
  rho_q[1] = rhoe_q1 * -q(3);        // drho/dq1
  rho_q[2] = rhoe_q2 * -q(3);        // drho/dq2
  rho_q[3] = rhoe_q3 * -q(3) - rhoe; // drho/dq3

  // u   = rhoe *  q(1)/rho;
  u_q[0] = rhoe_q0 * q(1)/rho - rhoe * q(1)*rho_q[0]/(rho*rho);            // du/dq0
  u_q[1] = rhoe_q1 * q(1)/rho - rhoe * q(1)*rho_q[1]/(rho*rho) + rhoe/rho; // du/dq1
  u_q[2] = rhoe_q2 * q(1)/rho - rhoe * q(1)*rho_q[2]/(rho*rho);            // du/dq2
  u_q[3] = rhoe_q3 * q(1)/rho - rhoe * q(1)*rho_q[3]/(rho*rho);            // du/dq3

  // v   = rhoe *  q(2)/rho;
  v_q[0] = rhoe_q0 * q(2)/rho - rhoe * q(2)*rho_q[0]/(rho*rho);            // du/dq0
  v_q[1] = rhoe_q1 * q(2)/rho - rhoe * q(2)*rho_q[1]/(rho*rho);            // du/dq1
  v_q[2] = rhoe_q2 * q(2)/rho - rhoe * q(2)*rho_q[2]/(rho*rho) + rhoe/rho; // du/dq2
  v_q[3] = rhoe_q3 * q(2)/rho - rhoe * q(2)*rho_q[3]/(rho*rho);            // du/dq3

  // t   = rhoe / (rho * gas_.Cv());
  t_q[0] = rhoe_q0 / (rho * gas_.Cv()) - rhoe*rho_q[0] / (rho*rho* gas_.Cv()); // dt/dq0
  t_q[1] = rhoe_q1 / (rho * gas_.Cv()) - rhoe*rho_q[1] / (rho*rho* gas_.Cv()); // dt/dq1
  t_q[2] = rhoe_q2 / (rho * gas_.Cv()) - rhoe*rho_q[2] / (rho*rho* gas_.Cv()); // dt/dq2
  t_q[3] = rhoe_q3 / (rho * gas_.Cv()) - rhoe*rho_q[3] / (rho*rho* gas_.Cv()); // dt/dq3
}


template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type&   ux,
    typename promote_Surreal<Tq,Tg>::type&   vx,
    typename promote_Surreal<Tq,Tg>::type&   tx ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;

  Real gamma = gas_.gamma();
  Real gm1 = gamma - 1.0;
  Tq s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  Tq rhoe = pow(gm1/pow(-q(3),gamma),1.0/gm1) * exp(-s/gm1);

  Tq rho = rhoe * -q(3);

  // s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  Tqg sx = -qx(0) + (q(1)*qx(1) + q(2)*qx(2)) / q(3) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3) * (-qx(3)/q(3));

  // rhoe = pow(gm1/pow(-q(3),gamma),1.0/gm1) * exp(-s/gm1);
  Tqg rhoex = rhoe * gamma / gm1 / (-q(3)) * qx(3) - rhoe / gm1 * sx;


  // rho = rhoe * -q(3);
  rhox = rhoe*-qx(3) + rhoex * -q(3);

  // u = rhoe * q(1)/rho;
  ux   = rhoex *q(1)/rho + rhoe * qx(1)/rho + rhoe * q(1)/rho * (-rhox/rho);
  // v = rhoe * q(2)/rho;
  vx   = rhoex *q(2)/rho + rhoe * qx(2)/rho + rhoe * q(2)/rho * (-rhox/rho);

  // t = rhoe / (rho * gas_.Cv());
  tx = rhoex / (rho * gas_.Cv()) + rhoe / (rho * gas_.Cv()) * (-rhox/rho);
}


template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class Th>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    typename promote_Surreal<Tq, Tg, Th>::type& rhoxx,
    typename promote_Surreal<Tq, Tg, Th>::type& rhoxy,
    typename promote_Surreal<Tq, Tg, Th>::type& rhoyy,
    typename promote_Surreal<Tq, Tg, Th>::type& uxx,
    typename promote_Surreal<Tq, Tg, Th>::type& uxy,
    typename promote_Surreal<Tq, Tg, Th>::type& uyy,
    typename promote_Surreal<Tq, Tg, Th>::type& vxx,
    typename promote_Surreal<Tq, Tg, Th>::type& vxy,
    typename promote_Surreal<Tq, Tg, Th>::type& vyy,
    typename promote_Surreal<Tq, Tg, Th>::type& txx,
    typename promote_Surreal<Tq, Tg, Th>::type& txy,
    typename promote_Surreal<Tq, Tg, Th>::type& tyy ) const
{
//  SANS_DEVELOPER_EXCEPTION("Second derivatives for Q2DEntropy not yet implemented");

  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type Thg;

  Real gamma = gas_.gamma();
  Real gm1 = gamma - 1.0;

  Tq rho, u, v, t;
  rho=0; u=0; v=0; t=0;
  Tqg rhox, ux, vx, tx = 0;
  Tqg rhoy, uy, vy, ty = 0;
//  Th pxx, pxy, pyy;

  eval(q, rho, u, v, t);
//  Tq p = gas_.pressure(rho, t);

  evalGradient(q, qx, rhox, ux, vx, tx);
  evalGradient(q, qy, rhoy, uy, vy, ty);

  Tq s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  Tq rhoe = pow(gm1/pow(-q(3),gamma),1.0/gm1) * exp(-s/gm1);

  Tqg sx = -qx(0) + (q(1)*qx(1) + q(2)*qx(2)) / q(3) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3) * (-qx(3)/q(3));
  Tqg sy = -qy(0) + (q(1)*qy(1) + q(2)*qy(2)) / q(3) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3) * (-qy(3)/q(3));

  Tqg sxx = -qxx(0)
             + (qx(1)*qx(1) + qx(2)*qx(2))/q(3) + (q(1)*qxx(1) + q(2)*qxx(2))/q(3) + (q(1)*qx(1) + q(2)*qx(2))/q(3)*(-qx(3)/q(3))
             + (q(1)*qx(1) + q(2)*qx(2))/q(3) * (-qx(3)/q(3))
             + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3) * (-qxx(3)/q(3))
             + (q(1)*q(1) + q(2)*q(2))/q(3) * (-qx(3)/q(3))* (-qx(3)/q(3));

  Tqg syy = -qyy(0)
             + (qy(1)*qy(1) + qy(2)*qy(2))/q(3) + (q(1)*qyy(1) + q(2)*qyy(2))/q(3) + (q(1)*qy(1) + q(2)*qy(2))/q(3)*(-qy(3) / q(3))
             + (q(1)*qy(1) + q(2)*qy(2))/q(3) * (-qy(3)/q(3))
             + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3) * (-qyy(3)/q(3))
             + (q(1)*q(1) + q(2)*q(2))/q(3) * (-qy(3)/q(3))* (-qy(3)/q(3));

  Tqg sxy = -qxy(0)
             + (qy(1)*qx(1) + qy(2)*qx(2))/q(3) + (q(1)*qxy(1) + q(2)*qxy(2))/q(3) + (q(1)*qx(1) + q(2)*qx(2))/q(3)*(-qy(3) / q(3))
             + (q(1)*qy(1) + q(2)*qy(2))/q(3) * (-qx(3)/q(3))
             + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3) * (-qxy(3)/q(3))
             + (q(1)*q(1) + q(2)*q(2))/q(3) * (-qx(3)/q(3))* (-qy(3)/q(3));

  // rhoe = pow(gm1/pow(-q(3),gamma),1.0/gm1) * exp(-s/gm1);
  Tqg rhoex = rhoe * gamma / gm1 * (-qx(3)/q(3)) - rhoe / gm1 * sx;
  Tqg rhoey = rhoe * gamma / gm1 * (-qy(3)/q(3)) - rhoe / gm1 * sy;

  Thg rhoexx = rhoex * gamma / gm1 * (-qx(3)/q(3))
              + rhoe * gamma/gm1 * (-qxx(3)/q(3))
              + rhoe * gamma / gm1 * (-qx(3)/q(3)) * (-qx(3)/q(3))
              - rhoex*sx /gm1 - rhoe* sxx /gm1 ;

  Thg rhoeyy = rhoey * gamma / gm1 * (-qy(3)/q(3))
                  + rhoe * gamma/gm1 * (-qyy(3)/q(3))
                  + rhoe * gamma / gm1 * (-qy(3)/q(3)) * (-qy(3)/q(3))
                  - rhoey*sy /gm1 - rhoe* syy /gm1 ;

  Thg rhoexy = rhoey * gamma / gm1 * (-qx(3)/q(3))
                  + rhoe * gamma/gm1 * (-qxy(3)/q(3))
                  + rhoe * gamma / gm1 * (-qx(3)/q(3)) * (-qy(3)/q(3))
                  - rhoey*sx /gm1 - rhoe* sxy /gm1 ;

  //rhox = rhoe*-qx(3) + rhoex * -q(3);
  rhoxx = rhoex*-qx(3) - rhoe*qxx(3) + rhoexx * -q(3) + rhoex * -qx(3);
  rhoxy = rhoey*-qx(3) - rhoe*qxy(3) + rhoexy * -q(3) + rhoex * -qy(3);
  rhoyy = rhoey*-qy(3) - rhoe*qyy(3) + rhoeyy * -q(3) + rhoey * -qy(3);

  //ux   = rhoex *q(1)/rho + rhoe * qx(1)/rho + rhoe * q(1)/rho * (-rhox/rho);
  uxx = rhoexx*q(1)/rho + rhoex*qx(1)/rho - rhoex*q(1)/rho/rho*rhox
        + rhoex*qx(1)/rho + rhoe*qxx(1)/rho - rhoe*qx(1)/rho/rho*rhox
        - rhoex*q(1)*rhox/rho/rho - rhoe*qx(1)*rhox/rho/rho - rhoe*q(1)*rhoxx/rho/rho + 2.*rhoe*q(1)*rhox*rhox/rho/rho/rho;

  uyy = rhoeyy*q(1)/rho + rhoey*qy(1)/rho - rhoey*q(1)/rho/rho*rhoy
        + rhoey*qy(1)/rho + rhoe*qyy(1)/rho - rhoe*qy(1)/rho/rho*rhoy
        - rhoey*q(1)*rhoy/rho/rho - rhoe*qy(1)*rhoy/rho/rho - rhoe*q(1)*rhoyy/rho/rho + 2.*rhoe*q(1)*rhoy*rhoy/rho/rho/rho;

  uxy = rhoexy*q(1)/rho + rhoex*qy(1)/rho - rhoex*q(1)/rho/rho*rhoy
      + rhoey*qx(1)/rho + rhoe*qxy(1)/rho - rhoe*qx(1)/rho/rho*rhoy
      - rhoey*q(1)*rhox/rho/rho - rhoe*qy(1)*rhox/rho/rho - rhoe*q(1)*rhoxy/rho/rho + 2.*rhoe*q(1)*rhox*rhoy/rho/rho/rho;

  //vx   = rhoex *q(2)/rho + rhoe * qx(2)/rho + rhoe * q(2)/rho * (-rhox/rho);
  vxx = rhoexx*q(2)/rho + rhoex*qx(2)/rho - rhoex*q(2)/rho/rho*rhox
        + rhoex*qx(2)/rho + rhoe*qxx(2)/rho - rhoe*qx(2)/rho/rho*rhox
        - rhoex*q(2)*rhox/rho/rho - rhoe*qx(2)*rhox/rho/rho - rhoe*q(2)*rhoxx/rho/rho + 2.*rhoe*q(2)*rhox*rhox/rho/rho/rho;

  vyy = rhoeyy*q(2)/rho + rhoey*qy(2)/rho - rhoey*q(2)/rho/rho*rhoy
        + rhoey*qy(2)/rho + rhoe*qyy(2)/rho - rhoe*qy(2)/rho/rho*rhoy
        - rhoey*q(2)*rhoy/rho/rho - rhoe*qy(2)*rhoy/rho/rho - rhoe*q(2)*rhoyy/rho/rho + 2.*rhoe*q(2)*rhoy*rhoy/rho/rho/rho;

  vxy = rhoexy*q(2)/rho + rhoex*qy(2)/rho - rhoex*q(2)/rho/rho*rhoy
      + rhoey*qx(2)/rho + rhoe*qxy(2)/rho - rhoe*qx(2)/rho/rho*rhoy
      - rhoey*q(2)*rhox/rho/rho - rhoe*qy(2)*rhox/rho/rho - rhoe*q(2)*rhoxy/rho/rho + 2.*rhoe*q(2)*rhox*rhoy/rho/rho/rho;

  //tx = rhoex / (rho * gas_.Cv()) + rhoe / (rho * gas_.Cv()) * (-rhox/rho);
  txx = rhoexx/ (rho * gas_.Cv()) - rhoex /rho / (rho* gas_.Cv()) * rhox
        - rhoex  / (rho * gas_.Cv()) * (rhox/rho) -  rhoe  / (rho * gas_.Cv()) * (rhoxx/rho)
        +  2.0*rhoe  / (rho * gas_.Cv()) * (rhox/rho) * (rhox/rho);

  tyy = rhoeyy/ (rho * gas_.Cv()) - rhoey /rho / (rho* gas_.Cv()) * rhoy
        - rhoey  / (rho * gas_.Cv()) * (rhoy/rho) -  rhoe  / (rho * gas_.Cv()) * (rhoyy/rho)
        +  2.0*rhoe  / (rho * gas_.Cv()) * (rhoy/rho) * (rhoy/rho);

  txy = rhoexy/ (rho * gas_.Cv()) - rhoex /rho / (rho* gas_.Cv()) * rhoy
        - rhoey  / (rho * gas_.Cv()) * (rhox/rho) -  rhoe  / (rho * gas_.Cv()) * (rhoxy/rho)
        +  2.0*rhoe  / (rho * gas_.Cv()) * (rhox/rho) * (rhoy/rho);

}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 4);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "VelocityY") &&
              (name[3] == "Temperature"));

  T rho, u, v, t, rhoe, rhoE, gamma, s;

  rho  = data[0];
  u    = data[1];
  v    = data[2];
  t    = data[3];

  rhoe = rho*gas_.energy(rho, t);
  rhoE = rho*(gas_.energy(rho, t) + 0.5*(u*u + v*v));
  gamma = gas_.gamma();
  s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) =  rho*v/rhoe;
  q(3) = -rho/rhoe;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure2D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.VelocityX;
  const T& v = data.VelocityY;
  const T& p = data.Pressure;
  T t = p / (gas_.R()*rho); //TODO: This assumes ideal gas, but we don't have an interface to compute e from rho, p
  T e = gas_.energy(rho, t);

  T rhoe = rho*e;
  T rhoE = rho*(e + 0.5*(u*u + v*v));
  Real gamma = gas_.gamma();
  T s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) =  rho*v/rhoe;
  q(3) = -rho/rhoe;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.VelocityX;
  const T& v = data.VelocityY;
  const T& t = data.Temperature;
  T e = gas_.energy(rho, t);

  T rhoe = rho*e;
  T rhoE = rho*(e + 0.5*(u*u + v*v));
  Real gamma = gas_.gamma();
  T s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) =  rho*v/rhoe;
  q(3) = -rho/rhoe;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative2D<T>& data ) const
{
  const T& rho = data.Density;
  const T& rhou = data.MomentumX;
  const T& rhov = data.MomentumY;
  const T& rhoE = data.TotalEnergy;

  T rhoe = rhoE - 0.5*(rhou*rhou + rhov*rhov)/rho;
  Real gamma = gas_.gamma();
  T s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rhou/rhoe;
  q(2) =  rhov/rhoe;
  q(3) = -rho/rhoe;
}

// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q2D<QTypeEntropy, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
}

// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
Q2D<QTypeEntropy, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  Real rhoe, s, gamma, rho, t;

  if (q(3) >= 0) return false;

  gamma = gas_.gamma();
  s = gamma - q(0) + 0.5*(q(1)*q(1) + q(2)*q(2))/q(3);
  rhoe = pow((gamma-1.0)/pow(-q(3),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));

  rho = rhoe * -q(3);

  if (rho <= 0) return false;
  if (std::isnan(rho)) return false;

  t   = rhoe / (rho * gas_.Cv());

  if (t <= 0) return false;
  if (std::isnan(t)) return false;

  return true;
}


template <template <class> class PDETraitsSize>
void
Q2D<QTypeEntropy, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q2D<QTypeEntropy, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q2D<QTypeEntropy, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // Q2DENTROPY_H
