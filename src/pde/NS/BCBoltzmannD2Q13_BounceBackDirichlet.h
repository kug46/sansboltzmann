// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBOLTZMANND2Q13_BOUNCEBACKDIRICHLET_H
#define BCBOLTZMANND2Q13_BOUNCEBACKDIRICHLET_H

// 1-D Euler BC class

//PyDict must be included first

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsBoltzmannD2Q13.h"
#include "PDEBoltzmannD2Q13.h"
//#include "BCEuler2D_Solution.h"
//#include "pde/NS/SolutionFunction_Euler2D.h"
//#include "pde/NS/SolutionFunction_NavierStokes2D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>
#include <memory> //shared_ptr
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeNone;
class BCTypeDiffuseKinetic;

template <class BCType, class PDEBoltzmannD2Q13>
class BCBoltzmannD2Q13;

template <class BCType>
struct BCBoltzmannD2Q13Params;


//----------------------------------------------------------------------------//
// BCNone
//----------------------------------------------------------------------------//

template<>
struct BCBoltzmannD2Q13Params<BCTypeNone> : noncopyable
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"None"};
  struct Option
  {
    const DictOption None{BCBoltzmannD2Q13Params::BCName, BCBoltzmannD2Q13Params::checkInputs};
  };

  static BCBoltzmannD2Q13Params params;
};


template <class PDE_>
class BCBoltzmannD2Q13<BCTypeNone, PDE_> :
    public BCType< BCBoltzmannD2Q13<BCTypeNone, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeNone BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBoltzmannD2Q13Params<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 0;                       // total BCs

  typedef typename PDE::QType QType;
  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  //typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCBoltzmannD2Q13( const PDE& pde ) :
      pde_(pde),
      qInterpret_(pde_.variableInterpreter())
  {
    // Nothing
  }
  ~BCBoltzmannD2Q13() {}

  BCBoltzmannD2Q13(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter())
  {
    // Nothing
  }

  BCBoltzmannD2Q13( const BCBoltzmannD2Q13& ) = delete;
  BCBoltzmannD2Q13& operator=( const BCBoltzmannD2Q13& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const
  {
    Real rho;
    qInterpret_.evalDensity( qI, rho );
    // None requires outflow
    return (rho) > 0;
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class PDE_>
template <class T>
inline void
BCBoltzmannD2Q13<BCTypeNone, PDE_>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}


// normal BC flux
template <class PDE_>
template <class T, class Tf>
inline void
BCBoltzmannD2Q13<BCTypeNone, PDE_>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on the interior state
  ArrayQ<Tf> f = 0, g = 0;
  pde_.fluxAdvective(x, y, time, qI, f, g);

  Fn += f*nx + g*ny;
}


// normal BC flux
template <class PDE_>
template <class T, class Tp, class Tf>
inline void
BCBoltzmannD2Q13<BCTypeNone, PDE_>::
fluxNormal( const Tp& param,
            const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on the interior state
  ArrayQ<Tf> f = 0, g = 0;
  pde_.fluxAdvective(x, y, time, qI, f, g);

  Fn += f*nx + g*ny;
}

template <class PDE_>
void
BCBoltzmannD2Q13<BCTypeNone, PDE_>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCBoltzmannD2Q13<BCTypeNone, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCBoltzmannD2Q13<BCTypeNone, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// BCDiffuseKinetic
//----------------------------------------------------------------------------//

template<>
struct BCBoltzmannD2Q13Params<BCTypeDiffuseKinetic> : noncopyable
{
  static void checkInputs(PyDict d);

  const ParameterNumeric<Real> Uw{"Uw", 0, NO_RANGE, "Wall velocity-X"};
  const ParameterNumeric<Real> Vw{"Vw", 0, NO_RANGE, "Wall velocity-Y"};
  const ParameterNumeric<Real> Rhow{"Rhow", -1, NO_RANGE, "Wall density"};

  static constexpr const char* BCName{"DiffuseKinetic"};
  struct Option
  {
    const DictOption DiffuseKinetic{BCBoltzmannD2Q13Params::BCName, BCBoltzmannD2Q13Params::checkInputs};
  };

  static BCBoltzmannD2Q13Params params;
};


template <class PDE_>
class BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE_> :
    public BCType< BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeDiffuseKinetic BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBoltzmannD2Q13Params<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;                       // total BCs

  typedef typename PDE::QType QType;
  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  BCBoltzmannD2Q13( const PDE& pde, const Real& Uw, const Real& Vw, const Real& Rhow ):
    pde_(pde), qInterpret_(pde_.variableInterpreter()), Uw_(Uw),
    Vw_(Vw), Rhow_(Rhow) {}

  ~BCBoltzmannD2Q13() {}

  BCBoltzmannD2Q13(const PDE& pde, const PyDict& d) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    Uw_(d.get(ParamsType::params.Uw)),
    Vw_(d.get(ParamsType::params.Vw)),
    Rhow_(d.get(ParamsType::params.Rhow))
  {

  }

  BCBoltzmannD2Q13( const BCBoltzmannD2Q13& ) = delete;
  BCBoltzmannD2Q13& operator=( const BCBoltzmannD2Q13& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // Apply BC Data
  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;
  const Real Uw_;
  const Real Vw_;
  const Real Rhow_;
  const bool upwind_ = true;
};

// BC data
template <class PDE>
template <class T>
void
BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE>::state( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  // see Leitao Chen & Laura Schaefer '15, DOI: https://doi.org/10.1063/1.4907782
  Real csq = 1./2.;
  const Real e[13][2] = { { 0, 0 },
                         { 1.0, 0.0 }, { 0.0, 1.0 }, { -1.0, 0.0 }, { 0.0, -1.0 },
                         { 1.0, 1.0 }, { -1.0, 1.0 }, { -1.0, -1.0 }, { 1.0, -1.0 },
                         { 2.0, 0.0 }, { 0.0, 2.0 }, { -2.0, 0.0 }, { 0.0, -2.0 } };


  // see Leitao Chen & Laura Schaefer '15, DOI: https://doi.org/10.1063/1.4907782
  const Real weight[13] = { 3./8.,
                            1./12., 1./12., 1./12., 1./12.,
                            1./16., 1./16., 1./16., 1./16.,
                            1./96., 1./96., 1./96., 1./96. };

  T rho = 0;
  if (Rhow_ > 0)
    rho = Rhow_;
  else
  {
    for (int i = 0; i<13; i++)
    rho = rho + qI(i);
  }

  Real u_dot_u = Uw_*Uw_ + Vw_*Vw_;

  for (int i=0; i<13; i++)
  {
    if ( (nx*e[i][0] + ny*e[i][1]) < 0)
    {
      Real e_dot_u = e[i][0]*Uw_ + e[i][1]*Vw_;
      //T feq = weight[i]*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));
      //std::cout<<"Direction"<<e[i][0]<<"and"<<e[i][1]<<std::endl<<"Vel"<<Uw_<<","<<Vw_<<std::endl;
      //std::cin >> csq;

      T feq = weight[i]*rho*(1 + (e_dot_u)/csq + 0.5*(e_dot_u/csq)*(e_dot_u/csq) - u_dot_u/(2*csq)
              + (e_dot_u*e_dot_u*e_dot_u)/(2*csq*csq*csq) - 1.5*e_dot_u*u_dot_u/(csq*csq) );

      qB(i) = feq;
    }
    else
      qB(i) = qI(i);
  }
}

// BC data
template <class PDE>
template <class Tp, class T>
void
BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE>::state( const Tp& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
void
BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE>::fluxNormal( const Real& x, const Real& y, const Real& time,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& qI,
                 const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                 const ArrayQ<T>& qB,
                 ArrayQ<Tf>& Fn) const
{
  if ( upwind_ )
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( x, y, time, qI, qB, nx, ny, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<Tf> fx = 0, fy = 0;
    pde_.fluxAdvective(x, y, time, qB, fx, fy);

    Fn += fx*nx + fy*ny;
  }
}

// normal BC flux
template <class PDE>
template <class Tp, class T, class Tf>
void
BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE>::fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                 const Real& nx, const Real& ny,
                 const ArrayQ<T>& qI,
                 const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                 const ArrayQ<T>& qB,
                 ArrayQ<Tf>& Fn) const
{
  if ( upwind_ )
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<Tf> fx = 0, fy = 0;
    pde_.fluxAdvective(param, x, y, time, qB, fx, fy);

    Fn += fx*nx + fy*ny;
  }
}

// is the boundary state valid
template <class PDE>
bool
BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE>::isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const
{
  return true;
}


template <class PDE>
void
BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCBoltzmannD2Q13<BCTypeDiffuseKinetic, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // BCBOLTZMANND2Q13_H
