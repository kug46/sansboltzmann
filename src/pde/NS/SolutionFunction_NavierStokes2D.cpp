// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/NS/SolutionFunction_NavierStokes2D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_NavierStokes2D_OjedaMMS_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  d.checkUnknownInputs(allParams);
}

// cppcheck-suppress passedByValue
void SolutionFunction_NavierStokes2D_Hartmann_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  d.checkUnknownInputs(allParams);
}

// cppcheck-suppress passedByValue
void SolutionFunction_NavierStokes2D_QuadraticMMS_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  d.checkUnknownInputs(allParams);
}

SolutionFunction_NavierStokes2D_OjedaMMS_Params SolutionFunction_NavierStokes2D_OjedaMMS_Params::params;
SolutionFunction_NavierStokes2D_Hartmann_Params SolutionFunction_NavierStokes2D_Hartmann_Params::params;
SolutionFunction_NavierStokes2D_QuadraticMMS_Params SolutionFunction_NavierStokes2D_QuadraticMMS_Params::params;

}
