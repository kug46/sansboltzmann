// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDERANSSA3D_H
#define PDERANSSA3D_H

// 3-D compressible RANS with Spalart-Allmaras PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "Topology/Dimension.h"
#include "pde/ForcingFunctionBase.h"
#include "pde/AnalyticFunction/Function3D.h"

#include "GasModel.h"
#include "SAVariable3D.h"
#include "PDENavierStokes3D.h"

#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include <iostream>
#include <string>
#include <cmath> // sqrt

//#define SANS_SA_QCR

namespace SANS
{

// power<N,T>(x):  power template
// reference: Walter Brown, C++ Meta<Programming> Concepts and Results
// e.g. power<3>(x) = x*x*x;

template <int N, class T>
inline T power( const T& x )
{
  return ((N == 0) ? 1 : ((N == 1) ? x : power<N%2,T>(x) * power<N/2,T>(x*x)));
}


// forward declare: RANS with SA variables interpreter
template<class QEulerType, template <class> class PDETraitsSize>
class QRANSSA3D;

//----------------------------------------------------------------------------//
// PDE class: 3-D compressible RANS with Spalart-Allmaras turbulence model
//
// template parameters:
//   T                        solution DOF data type (e.g. double)
//   QType                    solution variable set (e.g. primitive)
//   ViscosityModel           molecular viscosity model type
//   PDETraitsSize            define PDE size-related features
//     N, D                   PDE size, physical dimensions
//     ArrayQ                 solution/residual arrays
//     MatrixQ                matrices (e.g. flux jacobian)
//   PDETraitsModel           define PDE model-related features
//     QType                  solution variable set (e.g. primitive)
//     QInterpret             solution variable interpreter
//     GasModel               gas model (e.g. ideal gas law)
//     ViscosityModel         molecular viscosity model (e.g. Sutherland)
//     ThermalConductivityModel   thermal conductivity model (e.g. const Prandtl number)
//
// member functions:
//   .hasFluxAdvectiveTime     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//   .masterState        unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDERANSSA3D : public PDENavierStokes3D<PDETraitsSize, PDETraitsModel>
{
public:
  typedef PhysD3 PhysDim;

  typedef PDENavierStokes3D<PDETraitsSize, PDETraitsModel> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;                                        // cartesian vectors

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef typename PDETraitsModel::QType QType;
  typedef QRANSSA3D<QType, PDETraitsSize> QInterpret;                                 // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                                 // gas model

  typedef typename PDETraitsModel::ViscosityModel ViscosityModel;                     // molecular viscosity model

  typedef typename PDETraitsModel::ThermalConductivityModel ThermalConductivityModel; // thermal conductivity model

  typedef SAVariableType3DParams VariableType3DParams;

  // Solution pointer rather than MMS pointer due to distance function
  typedef Function3DBase<ArrayQ<Real>> FunctionBase;

  PDERANSSA3D( const GasModel& gas, const ViscosityModel& visc,
         const ThermalConductivityModel& tcond,
         const EulerResidualInterpCategory cat = Euler_ResidInterp_Raw,
         const RoeEntropyFix entropyFix = eVanLeer,
         const Real ntref = 1.0,
         const std::shared_ptr<FunctionBase>& soln = nullptr) :
      BaseType( gas, visc, tcond, cat, entropyFix, nullptr ),
      soln_(soln),
      ntref_(ntref),
      qInterpret_(gas, ntref)
  {
    setCoeffients();
  }

  static const int iCont = PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::iCont;
  static const int ixMom = PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::ixMom;
  static const int iyMom = PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::iyMom;
  static const int izMom = PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::izMom;
  static const int iEngy = PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::iEngy;
  static const int iSA   = iEngy + 1;

  PDERANSSA3D& operator=( const PDERANSSA3D& ) = delete;

  // flux components
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return true; }
  bool needsSolutionGradientforSource() const { return true; }

  bool hasForcingFunction() const { return soln_ != nullptr; }

  // unsteady conservative flux: U(Q)
  template<class T>
  void fluxAdvectiveTime(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& f ) const
  {
    fluxAdvectiveTime(x, y, z, time, q, f);
  }

  // unsteady conservative flux: U(Q)
  template<class T>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& f ) const
  {
    ArrayQ<T> ftLocal = 0;
    masterState(x, y, z, time, q, ftLocal);
    f += ftLocal;
  }

  // unsteady conservative flux: U(Q)
  template<class T>
  void masterState(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
  {
    masterState(x, y, z, time, q, uCons);
  }

  // unsteady conservative flux: U(Q)
  template<class T>
  void masterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const;

  template<class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Real& x, const Real& y, const Real& z,  const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& dU ) const
  {
    qInterpret_.conservativePerturbation(q, dq, dU);
  }

  template<class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Real& dist, const Real& x, const Real& y, const Real& z,  const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& dU ) const
  {
    perturbedMasterState(x, y, z, time, q, dq, dU);
  }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& Ux, ArrayQ<Tf>& Uy, ArrayQ<Tf>& Uz  ) const
  {
    masterStateGradient(x, y, z, time, q, qx, qy, qz, Ux, Uy, Uz);
  }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& Ux, ArrayQ<Tf>& Uy, ArrayQ<Tf>& Uz  ) const;

  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
      const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& Uxx, ArrayQ<Tf>& Uxy, ArrayQ<Tf>& Uxz,
      ArrayQ<Tf>& Uyy, ArrayQ<Tf>& Uyz, ArrayQ<Tf>& Uzz  ) const
  {
    masterStateHessian(x, y, z, time, q, qx, qy, qz,
                        qxx, qxy, qxz, qyy, qyz, qzz, Uxx, Uxy, Uxz, Uyy, Uyz, Uzz);
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
      const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& Uxx, ArrayQ<Tf>& Uxy, ArrayQ<Tf>& Uxz,
      ArrayQ<Tf>& Uyy, ArrayQ<Tf>& Uyz, ArrayQ<Tf>& Uzz  ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    jacobianMasterState(x, y, z, time, q, dudq);
  }

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux: U(Q)
  template<class Tq, class Tf>
  void adjointState(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& V ) const
  {
    adjointState(x, y, z, time, q, V);
  }

  // unsteady conservative flux: U(Q)
  template<class Tq, class Tf>
  void adjointState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& V ) const;

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void adjointStateGradient(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy, ArrayQ<Tf>& Vz  ) const
  {
    adjointStateGradient(x, y, z, time, q, qx, qy, qz, Vx, Vy, Vz);
  }

  // master state Gradient: Ux(Q)
  template<class Tq, class Tg, class Tf>
  void adjointStateGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy, ArrayQ<Tf>& Vz  ) const;

  template<class Tq, class Tg, class Th, class Tf>
  void adjointStateHessian(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
      const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vxz,
      ArrayQ<Tf>& Vyy, ArrayQ<Tf>& Vyz, ArrayQ<Tf>& Vzz  ) const
  {
    adjointStateHessian(x, y, z, time, q, qx, qy, qz,
                        qxx, qxy, qxz, qyy, qyz, qzz, Vxx, Vxy, Vxz, Vyy, Vyz, Vzz);
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tq, class Tg, class Th, class Tf>
  void adjointStateHessian(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
      const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vxz,
      ArrayQ<Tf>& Vyy, ArrayQ<Tf>& Vyz, ArrayQ<Tf>& Vzz  ) const;

  // conversion from entropy variables dudv
  template<class Tq>
  void adjointVariableJacobian(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const
  {
    adjointVariableJacobian(x, y, z, time, q, dudv);
  }

  template<class Tq>
  void adjointVariableJacobian(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const;

  // conversion to entropy variables dvdu
  template<class Tq>
  void adjointVariableJacobianInverse(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const
  {
    adjointVariableJacobianInverse(x, y, z, time, q, dvdu);
  }

  template<class Tq>
  void adjointVariableJacobianInverse(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const
  {
    strongFluxAdvectiveTime(x, y, z, time, q, qt, strongPDE);
  }

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianFluxAdvectiveTime(x, y, z, time, q, J);
  }

  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
  {
     fluxAdvective(x, y, z, time, q, f, g, h);
  }

  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const;


  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f, const Real& scale = 1 ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f, const Real& scale = 1 ) const
  {
    fluxAdvectiveUpwind(x, y, z, time, qL, qR, nx, ny, nz, f, scale);
  }

  template <class T>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt, ArrayQ<T>& f ) const;

  template <class T>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt, ArrayQ<T>& f ) const
  {
    fluxAdvectiveUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  }

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu, MatrixQ<Tf>& dhdu ) const;

  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu, MatrixQ<Tf>& dhdu ) const
  {
    jacobianFluxAdvective(x, y, z, time, q, dfdu, dgdu, dhdu);
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const Real& Nx, const Real& Ny, const Real& Nz,
      MatrixQ<Tf>& a ) const;

  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<Tf>& a ) const
  {
    jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, nx, ny, nz, a);
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt,
      MatrixQ<Tf>& a ) const;

  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt,
      MatrixQ<Tf>& a ) const
  {
    jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, z, time, q, nx, ny, nz, nt, a);
  }

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& strongPDE ) const;

  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective( const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& strongPDE ) const
  {
    strongFluxAdvective(x, y, z, time, q, qx, qy, qz, strongPDE);
  }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
  {
    fluxViscous(x, y, z, time, q, qx, qy, qz, f, g, h);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& distL, const Real& distR,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f ) const
  {
    fluxViscous(x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, f);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy, const ArrayQ<Tgu>& dqz,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg, ArrayQ<Tf>& dh ) const
  {
    perturbedGradFluxViscous(x, y, z, time, q, qx, qy, qz, dqx, dqy, dqz, df, dg, dh);
  }

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy, const ArrayQ<Tgu>& dqz,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg, ArrayQ<Tf>& dh ) const
  {
    fluxViscous(x, y, z, time, q, dqx, dqy, dqz, df, dg, dh);
  }


  // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const MatrixQ<Tk>& kxx, const MatrixQ<Tk>& kxy, const MatrixQ<Tk>& kxz,
      const MatrixQ<Tk>& kyx, const MatrixQ<Tk>& kyy, const MatrixQ<Tk>& kyz,
      const MatrixQ<Tk>& kzx, const MatrixQ<Tk>& kzy, const MatrixQ<Tk>& kzz,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, const ArrayQ<Tgu>& duz,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg, ArrayQ<Tf>& dh ) const;


  // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const MatrixQ<Tk>& kxx, const MatrixQ<Tk>& kxy, const MatrixQ<Tk>& kxz,
      const MatrixQ<Tk>& kyx, const MatrixQ<Tk>& kyy, const MatrixQ<Tk>& kyz,
      const MatrixQ<Tk>& kzx, const MatrixQ<Tk>& kzy, const MatrixQ<Tk>& kzz,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, const ArrayQ<Tgu>& duz,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg, ArrayQ<Tf>& dh ) const
  {
    perturbedGradFluxViscous(x, y, z, time, kxx, kxy, kxz, kyx, kyy, kyx, kzx, kzy, kzz,
                             dux, duy, duz, df, dg, dh);
  }


  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
  {
    diffusionViscous(x, y, z, time,
                     q, qx, qy, qz,
                     kxx, kxy, kxz,
                     kyx, kyy, kyz,
                     kzx, kzy, kzz);
  }

  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const;

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu, MatrixQ<Tf>& dhdu ) const;

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& strongPDE ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg>
  void source(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const
  {
    SANS_DEVELOPER_EXCEPTION("Calling wrong source term");
  }

  template <class Tq, class Tg, class Ts>
  void source(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const;

  // Forward call to S(Q+QP, QX+QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
    typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

    ArrayQ<Tqq> qq = q + qp;
    ArrayQ<Tgg> qqx = qx + qpx;
    ArrayQ<Tgg> qqy = qy + qpy;
    ArrayQ<Tgg> qqz = qz + qpz;

    source(dist, x, y, z, time, qq, qqx, qqy, qqz, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    source(dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src);
  }

  // Forward call to S(Q,QP, QX,QPX)
  template <class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy, const ArrayQ<Tgp>& qpz,
      ArrayQ<Ts>& src ) const
  {
    source(dist, x, y, z, time, q, qp, qx, qy, qz, qpx, qpy, qpz, src);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& s ) const
  {
    source(dist, x, y, z, time, q, qx, qy, qz, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& distL, const Real& xL, const Real& yL, const Real& zL,
      const Real& distR, const Real& xR, const Real& yR, const Real& zR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& distL, const Real& xL, const Real& yL, const Real& zL,
      const Real& distR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const
  {
    BaseType::sourceLiftedQuantity(xL, yL, zL, xR, yR, zR, time, qL, qR, s);
  }


  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
    MatrixQ<Ts> S = 0;

    jacobianSource(dist, x, y, z, time, q, qx, qy, qz, S);

    Ts tmp = S(iSA,5);
    Real eps0 = 1.e-9;
    Ts tmpabs = smoothabsP(tmp, eps0);

    dsdu += tmp/tmpabs*S;

    // NS source terms (nominally empty function)
    BaseType::jacobianSourceAbsoluteValue( x, y, z, time, q, qx, qy, qz, dsdu );
  }

  // jacobian of source wrt conservation variable gradients: d(S)/d(Ux), d(S)/d(Uy), d(S)/d(Uz)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
  {
    SANS_DEVELOPER_EXCEPTION("Calling wrong jacobianGradientSource");
  }

  // jacobian of source wrt conservation variable gradients: d(S)/d(Ux), d(S)/d(Uy), d(S)/d(Uz)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const;

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const;

  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Ts>& div_dsdgradu ) const;

  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradientHACK(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Ts>& div_dsgradu ) const
  {
    SANS_DEVELOPER_EXCEPTION("3D RANS HACKED JACOBIANGRADIENTSOURCEGRADIENT NOT IMPLEMENTED");
  }

  // linear change in source in response to linear perturbations du, dux, duy, duz
  template <class Tq, class Tg, class Tu, class Tug, class Ts>
  void perturbedSource(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,  const ArrayQ<Tg>& qx,  const ArrayQ<Tg>& qy,  const ArrayQ<Tg>& qz,
      const ArrayQ<Tu>& du, const ArrayQ<Tug>& dux, const ArrayQ<Tug>& duy, const ArrayQ<Tug>& duz,
      ArrayQ<Ts>& dS ) const;

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<T>& forcing ) const;

  // characteristic speed (needed for timestep)
  template<class T>
  void speedCharacteristic( const Real& dist, const Real& x, const Real& y, const Real& z, const Real& t,
                            const Real& dx, const Real& dy, const Real& dz, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, y, z, t, dx, dy, dz, q, speed);
  }

  // characteristic speed
  template<class T>
  void speedCharacteristic( const Real& dist, const Real& x, const Real& y, const Real& z, const Real& t, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(x, y, z, t, q, speed);
  }
  using BaseType::speedCharacteristic;

  // update fraction needed for physically valid state
  void updateFraction( const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
                       const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(x, y, z, time, q, dq, maxChangeFraction, updateFraction);
  }
  using BaseType::updateFraction;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // set from variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  template<class T, class Variables>
  void setDOFFrom( ArrayQ<T>& q, const SAVariableType3D<Variables>& data ) const;

  template<class Variables>
  ArrayQ<Real> setDOFFrom( const SAVariableType3D<Variables>& data ) const;

  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  const QInterpret& variableInterpreter() const { return qInterpret_; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  Real vk() const { return vk_; }

protected:

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSourceRow(
      const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variable gradients: d(S)/d(Ux), d(S)/d(Uy), d(S)/d(Uz)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceRow(
      const Real& dist,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& dsdux, ArrayQ<Ts>& dsduy, ArrayQ<Ts>& dsduz ) const;

  void setCoeffients();                 // default coefficients (initially hard-coded)

  template<class Tq, class Ts> //HELPER FUNCTION CONSTRUCTS dv/du*mat*du/dv
  void jacobianHelperHACK(const Real& x, const Real& y, const Real& z, const Real& time,
                          const ArrayQ<Tq>& q,  const MatrixQ<Ts>& mat, MatrixQ<Ts>& matHACK) const;

  const std::shared_ptr<FunctionBase> soln_;
  const Real ntref_;
  const QInterpret qInterpret_;         // solution variable interpreter class
  using BaseType::gas_;
  using BaseType::visc_;
  using BaseType::tcond_;
  using BaseType::forcing_;
  using BaseType::flux_;
  using BaseType::cat_;

  Real cb1_, cb2_, sigma_, vk_, cw1_, cw2_, cw3_, cv1_, ct1_, ct2_, ct3_, ct4_;
  Real prandtlEddy_;
  Real rlim_;
  Real cv2_, cv3_;
  Real cn1_;
#ifdef SANS_SA_QCR
  Real cr1_;
#endif
};


// SA2012 constants
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::setCoeffients()
{
  cb1_   = 0.1355;
  cb2_   = 0.622;
  sigma_ = 2./3.;
  vk_    = 0.41;
  cw1_   = cb1_/(vk_*vk_) + (1 + cb2_)/sigma_;
  cw2_   = 0.3;
  cw3_   = 2;
  cv1_   = 7.1;
  ct1_   = 1;
  ct2_   = 2;
  ct3_   = 1.2;
  ct4_   = 0.5;
  prandtlEddy_ = 0.9;
  rlim_  = 10;
  cv2_   = 1 - 0.3;
  cv3_   = 1 - 0.1;
  cn1_   = 16;

#ifdef SANS_SA_QCR
  cr1_   = 0.3;
#endif
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
{
  T rho = 0, nt = 0;

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalSA( q, nt );

  uCons(iSA) += rho*nt/ntref_;

  BaseType::masterState( x, y, z, time, q, uCons );
}



// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::masterStateGradient(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& Ux, ArrayQ<Tf>& Uy, ArrayQ<Tf>& Uz ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq nt = 0;
  T ntx = 0, nty = 0, ntz = 0;

  Tq rho = 0;
  qInterpret_.evalDensity( q, rho );

  T rhox=0, ux =0, vx = 0, wx = 0, tx = 0;
  T rhoy=0, uy =0, vy = 0, wy = 0, ty = 0;
  T rhoz=0, uz =0, vz = 0, wz = 0, tz = 0;
  qInterpret_.evalGradient(q, qx, rhox, ux, vx, wx, tx);
  qInterpret_.evalGradient(q, qy, rhoy, uy, vy, wy, ty);
  qInterpret_.evalGradient(q, qz, rhoz, uz, vz, wz, tz);

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  Ux(iSA) = (rhox*nt + rho*ntx)/ntref_;
  Uy(iSA) = (rhoy*nt + rho*nty)/ntref_;
  Uz(iSA) = (rhoz*nt + rho*ntz)/ntref_;

  BaseType::masterStateGradient(x, y, z, time, q, qx, qy, qz, Ux, Uy, Uz);
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::masterStateHessian(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
    const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& Uxx, ArrayQ<Tf>& Uxy, ArrayQ<Tf>& Uxz,
    ArrayQ<Tf>& Uyy, ArrayQ<Tf>& Uyz, ArrayQ<Tf>& Uzz ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq nt=0;
  Tqg ntx=0, nty = 0, ntz = 0;

  Tq rho = 0;
  qInterpret_.evalDensity(q, rho);

  Tqg rhox=0, ux =0, vx = 0, wx = 0, tx = 0;
  Tqg rhoy=0, uy =0, vy = 0, wy = 0, ty = 0;
  Tqg rhoz=0, uz =0, vz = 0, wz = 0, tz = 0;
  qInterpret_.evalGradient(q, qx, rhox, ux, vx, wx, tx);
  qInterpret_.evalGradient(q, qy, rhoy, uy, vy, wy, ty);
  qInterpret_.evalGradient(q, qz, rhoz, uz, vz, wz, tz);

  DLA::MatrixSymS<D,T> rhoH, uH, vH, wH, tH;
  qInterpret_.evalSecondGradient( q, qx, qy, qz,
                                  qxx, qxy, qyy, qxz, qyz, qzz,
                                  rhoH, uH, vH, wH, tH );

  T rhoxx = rhoH(0,0);
  T rhoxy = rhoH(0,1);
  T rhoxz = rhoH(0,2);
  T rhoyy = rhoH(1,1);
  T rhoyz = rhoH(1,2);
  T rhozz = rhoH(2,2);

  // evaluate second derivatives
  T ntxx = 0, ntxy = 0, ntxz = 0, ntyy = 0, ntyz = 0, ntzz = 0;

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  qInterpret_.evalSAHessian( q, qx, qx, qxx, ntxx );
  qInterpret_.evalSAHessian( q, qx, qy, qxy, ntxy );
  qInterpret_.evalSAHessian( q, qx, qz, qxz, ntxz );
  qInterpret_.evalSAHessian( q, qy, qy, qyy, ntyy );
  qInterpret_.evalSAHessian( q, qy, qz, qyz, ntyz );
  qInterpret_.evalSAHessian( q, qz, qz, qzz, ntzz );

  Uxx(iSA) = (rhoxx*nt + 2*rhox*ntx + rho*ntxx)/ntref_;
  Uxy(iSA) = (rhoxy*nt + rhox*nty + rhoy*ntx + rho*ntxy)/ntref_;
  Uxz(iSA) = (rhoxz*nt + rhox*ntz + rhoz*ntx + rho*ntxz)/ntref_;

  Uyy(iSA) = (rhoyy*nt + 2*rhoy*nty + rho*ntyy)/ntref_;
  Uyz(iSA) = (rhoyz*nt + rhoy*ntz + rhoz*nty + rho*ntyz)/ntref_;

  Uzz(iSA) = (rhozz*nt + 2*rhoz*ntz + rho*ntzz)/ntref_;

  BaseType::masterStateHessian(x, y, z, time, q, qx, qy, qz,
                                qxx, qxy, qxz, qyy, qyz, qzz,
                                Uxx, Uxy, Uxz, Uyy, Uyz, Uzz);

}



// unsteady conservative flux Jacobian: dU(Q)/dQ
// NOTE: assumes SA variable independent of NS variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  ArrayQ<T> rho_q, nt_q;
  T rho = 0, nt = 0;

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalSA( q, nt );

  qInterpret_.evalDensityJacobian( q, rho_q );
  qInterpret_.evalSAJacobian( q, nt_q );

  dudq(iCont,5) = 0;
  dudq(ixMom,5) = 0;
  dudq(iyMom,5) = 0;
  dudq(izMom,5) = 0;
  dudq(iEngy,5) = 0;

  dudq(iSA,0) = (rho_q[0]*nt + rho*nt_q[0])/ntref_;
  dudq(iSA,1) = (rho_q[1]*nt + rho*nt_q[1])/ntref_;
  dudq(iSA,2) = (rho_q[2]*nt + rho*nt_q[2])/ntref_;
  dudq(iSA,3) = (rho_q[3]*nt + rho*nt_q[3])/ntref_;
  dudq(iSA,4) = (rho_q[4]*nt + rho*nt_q[4])/ntref_;
  dudq(iSA,5) = (rho_q[5]*nt + rho*nt_q[5])/ntref_;

  BaseType::jacobianMasterState( x, y, z, time, q, dudq );
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::adjointState(
    const Real& x, const Real& y,  const Real& z, const Real& time,
    const ArrayQ<Tq>& q, ArrayQ<Tf>& V ) const
{
  Tq nt = 0;
  qInterpret_.evalSA( q, nt );

  V(iSA) = nt/ntref_;

  BaseType::adjointState( x, y, z, time, q, V );

  V(iCont) -= 0.5*(nt/ntref_)*(nt/ntref_);
}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::adjointStateGradient(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy, ArrayQ<Tf>& Vz ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq nt = 0;
  T ntx = 0, nty = 0, ntz = 0;

  qInterpret_.evalSA( q, nt );

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  Vx(iSA) = ntx/ntref_;
  Vy(iSA) = nty/ntref_;
  Vz(iSA) = ntz/ntref_;

  BaseType::adjointStateGradient(x, y, z, time, q, qx, qy, qz, Vx, Vy, Vz);

  Vx(iCont) -= nt*ntx/(ntref_*ntref_);
  Vy(iCont) -= nt*nty/(ntref_*ntref_);
  Vz(iCont) -= nt*ntz/(ntref_*ntref_);

}


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::adjointStateHessian(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
    const ArrayQ<Th>& qyy, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vxz,
    ArrayQ<Tf>& Vyy, ArrayQ<Tf>& Vyz, ArrayQ<Tf>& Vzz ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq nt=0;
  Tqg ntx=0, nty = 0, ntz = 0;
//
//  qInterpret_.eval( q, rho, u, v, t );
//  qInterpret_.evalGradient( q, qx, rhox, ux, vx, tx );
//  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, ty );

  // evaluate second derivatives
  T ntxx = 0, ntxy = 0, ntxz = 0, ntyy = 0, ntyz = 0, ntzz = 0;

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  qInterpret_.evalSAHessian( q, qx, qx, qxx, ntxx );
  qInterpret_.evalSAHessian( q, qx, qy, qxy, ntxy );
  qInterpret_.evalSAHessian( q, qx, qz, qxz, ntxz );
  qInterpret_.evalSAHessian( q, qy, qy, qyy, ntyy );
  qInterpret_.evalSAHessian( q, qy, qz, qyz, ntyz );
  qInterpret_.evalSAHessian( q, qz, qz, qzz, ntzz );

  Vxx(iSA) = ntxx/ntref_;
  Vxy(iSA) = ntxy/ntref_;
  Vxz(iSA) = ntxz/ntref_;

  Vyy(iSA) = ntyy/ntref_;
  Vyz(iSA) = ntyz/ntref_;

  Vzz(iSA) = ntzz/ntref_;

  BaseType::adjointStateHessian(x, y, z, time, q, qx, qy, qz,
                                qxx, qxy, qxz, qyy, qyz, qzz,
                                Vxx, Vxy, Vxz, Vyy, Vyz, Vzz);
//
  Vxx(iCont) -= (ntx*ntx + nt*ntxx)/(ntref_*ntref_);
  Vxy(iCont) -= (ntx*nty + nt*ntxy)/(ntref_*ntref_);
  Vxz(iCont) -= (ntx*ntz + nt*ntxz)/(ntref_*ntref_);

  Vyy(iCont) -= (nty*nty + nt*ntyy)/(ntref_*ntref_);
  Vyz(iCont) -= (nty*ntz + nt*ntyz)/(ntref_*ntref_);

  Vzz(iCont) -= (ntz*ntz + nt*ntzz)/(ntref_*ntref_);
}




// entropy variable jacobian du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::adjointVariableJacobian(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, MatrixQ<Tq>& dudv ) const
{
  //entropy variable interpreter for adjoint
  Tq rho = 0, u = 0, v = 0, w=0, t = 0, e = 0, E = 0;

//  Real gamma = gas_.gamma();

  qInterpret_.eval( q, rho, u, v, w, t );
  e = gas_.energy(rho, t);
  E = e + 0.5*(u*u + v*v + w*w);

//  Tq p = gas_.pressure(rho, t);

  Tq nt = 0;
  qInterpret_.evalSA( q, nt );

  dudv(iCont,5) = rho*nt/ntref_;
  dudv(ixMom,5) = rho*u*nt/ntref_;
  dudv(iyMom,5) = rho*v*nt/ntref_;
  dudv(izMom,5) = rho*w*nt/ntref_;
  dudv(iEngy,5) = rho*E*nt/ntref_;

  dudv(iSA,0) = dudv(iCont,5);
  dudv(iSA,1) = dudv(ixMom,5);
  dudv(iSA,2) = dudv(iyMom,5);
  dudv(iSA,3) = dudv(izMom,5);
  dudv(iSA,4) = dudv(iEngy,5);
  dudv(iSA,5) = rho*( 1. + (nt*nt)/(ntref_*ntref_) );

  BaseType::adjointVariableJacobian(x, y, z, time, q, dudv);

}

// entropy variable jacobian du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::adjointVariableJacobianInverse(
    const Real& x, const Real& y, const Real& z,  const Real& time,
    const ArrayQ<Tq>& q, MatrixQ<Tq>& dvdu ) const
{
  //entropy variable interpreter for adjoint
  Tq rho = 0, u = 0, v = 0, w=0, t = 0;

  qInterpret_.eval( q, rho, u, v, w, t );

  Tq nt = 0;
  qInterpret_.evalSA( q, nt );

  dvdu(iCont,5) =  -(nt/ntref_)/rho;
  dvdu(iSA,0) = dvdu(iCont,5);
  dvdu(iSA,5) = 1./rho;

  BaseType::adjointVariableJacobianInverse(x, y, z, time,q, dvdu);

  dvdu(iCont, 0) += (nt/ntref_)*(nt/ntref_)/rho;

}



// strong form of unsteady conservative flux: dU(Q)/dt
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& utCons ) const
{
  T rho, nt;
  T rhot, ntt;

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalSA( q, nt );
#if 0
  qInterpret_.evalDensityGradient( q, qt, rhot );
#else
  T ut, vt, wt, tt;
  qInterpret_.evalGradient( q, qt, rhot, ut, vt, wt, tt );
#endif
  qInterpret_.evalSAGradient( q, qt, ntt );

  utCons(iSA) += (rho*ntt + nt*rhot)/ntref_;

  BaseType::strongFluxAdvectiveTime( x, y, z, time, q, qt, utCons );
}


// advective flux: F(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& q,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
{
  T rho = 0, u = 0, v = 0, w = 0, t = 0, nt = 0;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalSA( q, nt );

  f(iSA) += rho*u*nt/ntref_;
  g(iSA) += rho*v*nt/ntref_;
  h(iSA) += rho*w*nt/ntref_;

  BaseType::fluxAdvective( x, y, z, time, q, f, g, h );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f, const Real& scale ) const
{
  Tq C1, eigu;
  flux_.fluxAdvectiveUpwind(x, y, z, time, qL, qR, nx, ny, nz, f, C1, eigu, scale);

  Tq rhoL, uL, vL, wL, tL, ntL;
  Tq rhoR, uR, vR, wR, tR, ntR;

  qInterpret_.eval( qL, rhoL, uL, vL, wL, tL );
  qInterpret_.evalSA( qL, ntL );
  qInterpret_.eval( qR, rhoR, uR, vR, wR, tR );
  qInterpret_.evalSA( qR, ntR );

  // Average convective flux
  f(iSA) += 0.5*(nx*(rhoL*uL*ntL) + ny*(rhoL*vL*ntL) + nz*(rhoL*wL*ntL))/ntref_;
  f(iSA) += 0.5*(nx*(rhoR*uR*ntR) + ny*(rhoR*vR*ntR) + nz*(rhoR*wR*ntR))/ntref_;

  // Roe stabilization
  Tq w1L = sqrt(rhoL);
  Tq w1R = sqrt(rhoR);

  Tq nt = (w1L*ntL + w1R*ntR) / (w1L + w1R);

  Tq delrhont = rhoR*ntR - rhoL*ntL;

  f(iSA) -= 0.5*( nt*C1 + delrhont*eigu )/ntref_*scale;
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& ny, const Real& nz, const Real& nt, ArrayQ<T>& f ) const
{
  SANS_DEVELOPER_EXCEPTION( "Spacetime Roe Scheme Not Yet Implemented" );
#if 0
  flux_.fluxAdvectiveUpwindSpaceTime(x, y, z, time, qL, qR, nx, ny, nz, nt, f);
#endif
}

// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu, MatrixQ<Tf>& dhdu ) const
{
  Tq u=0, v=0, w=0, nt=0;

  qInterpret_.evalVelocity( q, u, v, w );
  qInterpret_.evalSA( q, nt );

//  dfdu(iCont,5) = 0;
//  dfdu(ixMom,5) = 0;
//  dfdu(iyMom,5) = 0;
//  dfdu(izMom,5) = 0;
//  dfdu(iEngy,5) = 0;
//
//  dgdu(iCont,5) = 0;
//  dgdu(ixMom,5) = 0;
//  dgdu(iyMom,5) = 0;
//  dgdu(izMom,5) = 0;
//  dgdu(iEngy,5) = 0;

  dfdu(iSA,0) += -u*nt/ntref_;
  dfdu(iSA,1) +=    nt/ntref_;
//  dfdu(iSA,2) = 0;
//  dfdu(iSA,3) = 0;
//  dfdu(iSA,4) = 0;
  dfdu(iSA,5) =  u;

  dgdu(iSA,0) += -v*nt/ntref_;
//  dgdu(iSA,1) = 0;
  dgdu(iSA,2) +=    nt/ntref_;
//  dgdu(iSA,3) = 0;
//  dgdu(iSA,4) = 0;
  dgdu(iSA,5) =  v;

  dhdu(iSA,0) += -w*nt/ntref_;
//  dhdu(iSA,1) = 0;
//  dhdu(iSA,2) = 0;
  dhdu(iSA,3) =    nt/ntref_;
//  dhdu(iSA,4) = 0;
  dhdu(iSA,5) =  w;

  BaseType::jacobianFluxAdvective( x, y, z, time, q, dfdu, dgdu, dhdu );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
//
// result evaluated using only acoustic eigenvectors and fact that left/right
// eigenvectors are orthogonal (see PDEEuler3D)
//
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& z, const Real& time,
  const ArrayQ<Tq>& q, const Real& Nx, const Real& Ny, const Real& Nz, MatrixQ<Tf>& mtx ) const
{
  Real nMag = sqrt(Nx*Nx + Ny*Ny + Nz*Nz);
  Real nx = Nx/nMag;
  Real ny = Ny/nMag;
  Real nz = Nz/nMag;

  Tq rho, u, v, w, t, c, h, qn, qsq;
  Tq nt;

  qInterpret_.eval( q, rho, u, v, w, t );
  c = gas_.speedofSound( t );
  h = gas_.enthalpy( rho, t );
  qn = nx*u + ny*v + nz*w;
  qsq = u*u + v*v + w*w;

  qInterpret_.evalSA( q, nt );

  Tq eigm = fabs(qn - c)*nMag;
  Tq eigp = fabs(qn + c)*nMag;
  Tq eigu = fabs(qn    )*nMag;

  // right acoustic eigenvectors
  // 'iSA' component is nt itself

  // left acoustic eigenvectors

  ArrayQ<Tq> lvecm, lvecp;

  lvecm[0] = qsq/(4*h) + qn/(2*c);
  lvecm[1] = -u/(2*h) - nx/(2*c);
  lvecm[2] = -v/(2*h) - ny/(2*c);
  lvecm[3] = -w/(2*h) - nz/(2*c);
  lvecm[4] = 1./(2*h);
  lvecm[5] = 0;

  lvecp[0] = qsq/(4*h) - qn/(2*c);
  lvecp[1] = -u/(2*h) + nx/(2*c);
  lvecp[2] = -v/(2*h) + ny/(2*c);
  lvecp[3] = -w/(2*h) + nz/(2*c);
  lvecp[4] = 1./(2*h);
  lvecp[5] = 0;

  // absolute value jacobian

  mtx(iSA,iSA) += eigu;
  for (int j = 0; j < 5; j++)
    mtx(iSA,j) += (eigm - eigu)*(nt/ntref_)*lvecm[j] + (eigp - eigu)*(nt/ntref_)*lvecp[j];

  BaseType::jacobianFluxAdvectiveAbsoluteValue(x, y, z, time, q, Nx, Ny, Nz, mtx);
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& z, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION( "jacobianFluxAdvectiveAbsoluteValueSpaceTime Not Yet Implemented" );
#if 0
  flux_.jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, z, time, q, nx, ny, nz, nt, mtx);
#endif
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type Tqg;

  Tq  rho=0,  u=0,  v=0,  w=0,  t=0,  nt=0;
  Tqg rhox=0, ux=0, vx=0, wx=0, tx=0, ntx=0;
  Tqg rhoy=0, uy=0, vy=0, wy=0, ty=0, nty=0;
  Tqg rhoz=0, uz=0, vz=0, wz=0, tz=0, ntz=0;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  strongPDE(iSA) += (rhox*u*nt + rho*ux*nt + rho*u*ntx
                  + rhoy*v*nt + rho*vy*nt + rho*v*nty
                  + rhoz*w*nt + rho*wz*nt + rho*w*ntz)/ntref_;

  BaseType::strongFluxAdvective( x, y, z, time, q, qx, qy, qz, strongPDE );
}


// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  // SA viscous fluxes

  Tq rho,  u,  v,  w,  t,  nt;
  T  rhox= 0; T ux = 0; T vx = 0; T wx = 0; T tx = 0; T ntx = 0;
  T  rhoy= 0; T uy = 0; T vy = 0; T wy = 0; T ty = 0; T nty = 0;
  T  rhoz= 0; T uz = 0; T vz = 0; T wz = 0; T tz = 0; T ntz = 0;
  Tq mu, k;

  qInterpret_.eval( q, rho, u, v, w, t );

  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  // SA model viscous terms

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  Tq chi  = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);

  if (nt < 0)
  {
    Tq fn  = (cn1_ + chi3) / (cn1_ - chi3);
    f(iSA) -= (mu + rho*nt*fn)*ntx/sigma_/ntref_;
    g(iSA) -= (mu + rho*nt*fn)*nty/sigma_/ntref_;
    h(iSA) -= (mu + rho*nt*fn)*ntz/sigma_/ntref_;
  }
  else
  {
    f(iSA) -= (mu + rho*nt)*ntx/sigma_/ntref_;
    g(iSA) -= (mu + rho*nt)*nty/sigma_/ntref_;
    h(iSA) -= (mu + rho*nt)*ntz/sigma_/ntref_;
  }

  // Reynolds stresses

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Tq fv1 = chi3 / (chi3 + power<3>(cv1_));
    Tq muEddy = rho*nt*fv1;
    Real Cp = this->gas_.Cp();

    // Augment the molecular viscosity and thermal conductivity
    mu += muEddy;
    k  += muEddy*Cp/prandtlEddy_;

#ifdef SANS_SA_QCR
    Tq lambdaEddy = -2./3. * muEddy;

    T tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy + wz);
    T tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy + wz);
    T tauRzz = muEddy*(2*wz   ) + lambdaEddy*(ux + vy + wz);
    T tauRxy = muEddy*(uy + vx);
    T tauRxz = muEddy*(uz + wx);
    T tauRyz = muEddy*(vz + wy);

    T magGradV = sqrt(ux*ux + uy*uy + uz*uz +
                      vx*vx + vy*vy + vz*vz +
                      wx*wx + wy*wy + wz*wz);

    T Wxy = (uy - vx);
    T Wxz = (uz - wx);
    T Wyz = (vz - wy);

    T Oxy = magGradV == 0 ? T(0) : Wxy/magGradV;
    T Oyx = -Oxy;

    T Oxz = magGradV == 0 ? T(0) : Wxz/magGradV;
    T Ozx = -Oxz;

    T Oyz = magGradV == 0 ? T(0) : Wyz/magGradV;
    T Ozy = -Oyz;

    // tauR_ij,QCR = tauR_ij - cr1*(O_ik*tauR_jk + O_jk*tauR_ik)

    // qcrxx = cr1*(Oxx*tauRxx + Oxy*tauRxy + Oxz*tauRxz + Oxx*tauRxx + Oxy*tauRxy + Oxz*tauRxz)
    // qcrxy = cr1*(Oxx*tauRyx + Oxy*tauRyy + Oxz*tauRyz + Oyx*tauRxx + Oyy*tauRxy + Oyz*tauRxz)
    // qcrxz = cr1*(Oxx*tauRzx + Oxy*tauRzy + Oxz*tauRzz + Ozx*tauRxx + Ozy*tauRxy + Ozz*tauRxz)

    // qcryx = cr1*(Oyx*tauRxx + Oyy*tauRxy + Oyz*tauRxz + Oxx*tauRyx + Oxy*tauRyy + Oxz*tauRyz)
    // qcryy = cr1*(Oyx*tauRyx + Oyy*tauRyy + Oyz*tauRyz + Oyx*tauRyx + Oyy*tauRyy + Oyz*tauRyz)
    // qcryz = cr1*(Oyx*tauRzx + Oyy*tauRzy + Oyz*tauRzz + Ozx*tauRyx + Ozy*tauRyy + Ozz*tauRyz)

    // qcrzx = cr1*(Ozx*tauRxx + Ozy*tauRxy + Ozz*tauRxz + Oxx*tauRzx + Oxy*tauRzy + Oxz*tauRzz)
    // qcrzy = cr1*(Ozx*tauRyx + Ozy*tauRyy + Ozz*tauRyz + Oyx*tauRzx + Oyy*tauRzy + Oyz*tauRzz)
    // qcrzz = cr1*(Ozx*tauRzx + Ozy*tauRzy + Ozz*tauRzz + Ozx*tauRzx + Ozy*tauRzy + Ozz*tauRzz)

    T qcrxx = cr1_*2*(Oxy*tauRxy + Oxz*tauRxz);
    T qcrxy = cr1_*  (Oxy*tauRyy + Oxz*tauRyz + Oyx*tauRxx + Oyz*tauRxz);
    T qcrxz = cr1_*  (Oxy*tauRyz + Oxz*tauRzz + Ozx*tauRxx + Ozy*tauRxy);

    T qcryy = cr1_*2*(Oyx*tauRxy + Oyz*tauRyz);
    T qcryz = cr1_*  (Oyx*tauRxz + Oyz*tauRzz + Ozx*tauRxy + Ozy*tauRyy);

    T qcrzz = cr1_*2*(Ozx*tauRxz + Ozy*tauRyz);

    f(ixMom) += qcrxx;
    f(iyMom) += qcrxy;
    f(izMom) += qcrxz;
    f(iEngy) += u*qcrxx + v*qcrxy + w*qcrxz;

    g(ixMom) += qcrxy;
    g(iyMom) += qcryy;
    g(izMom) += qcryz;
    g(iEngy) += u*qcrxy + v*qcryy + w*qcryz;

    h(ixMom) += qcrxz;
    h(iyMom) += qcryz;
    h(izMom) += qcrzz;
    h(iEngy) += u*qcrxz + v*qcryz + w*qcrzz;
#endif
  }

  // Compute the viscous flux
  BaseType::fluxViscous(q, qx, qy, qz,
                        u, v, w, mu, k,
                        ux, vx, wx, tx,
                        uy, vy, wy, ty,
                        uz, vz, wz, tz,
                        f, g, h );
}


// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
    const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;
  ArrayQ<Tf> hL = 0, hR = 0;

  fluxViscous(x, y, z, time, qL, qxL, qyL, qzL, fL, gL, hL);
  fluxViscous(x, y, z, time, qR, qxR, qyR, qzR, fR, gR, hR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny + 0.5*(hL + hR)*nz;
}


// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tk, class Tgu, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::perturbedGradFluxViscous(
    const Real& x, const Real& y,  const Real& z, const Real& time,
    const MatrixQ<Tk>& kxx,  const MatrixQ<Tk>& kxy,  const MatrixQ<Tk>& kxz,
    const MatrixQ<Tk>& kyx,  const MatrixQ<Tk>& kyy, const MatrixQ<Tk>& kyz,
    const MatrixQ<Tk>& kzx,  const MatrixQ<Tk>& kzy, const MatrixQ<Tk>& kzz,
    const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, const ArrayQ<Tgu>& duz,
    ArrayQ<Tf>& dF, ArrayQ<Tf>& dG, ArrayQ<Tf>& dH ) const
{
  //using what we know about the sparsity of K
  dF[iSA] -= kxx(iSA,0)*dux[0];
  dF[iSA] -= kxx(iSA,5)*dux[5];

  dG[iSA] -= kyy(iSA,0)*duy[0];
  dG[iSA] -= kyy(iSA,5)*duy[5];

  dH[iSA] -= kzz(iSA,0)*duz[0];
  dH[iSA] -= kzz(iSA,5)*duz[5];

  BaseType::perturbedGradFluxViscous(x, y, z, time,
                                     kxx, kxy, kxz,
                                     kyx, kyy, kyz,
                                     kzx, kzy, kzz,
                                     dux, duy, duz,
                                     dF, dG, dH);
}


// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
    MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
{
  // SA diffusion matrix

  Tq rho=0, u=0, v=0, w=0, t=0, nt=0;
  Tq mu, nu, k;

  qInterpret_.eval( q, rho, u, v, w, t );

  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  nu = mu/rho;

  qInterpret_.evalSA( q, nt );

  Tq ntr = nt/ntref_;

  Tq chi = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);

  if (nt < 0)
  {
    Tq fn  = (cn1_ + chi3) / (cn1_ - chi3);
    kxx(iSA,0) += -ntr*(nu + nt*fn)/sigma_;
    kxx(iSA,5) +=     (nu + nt*fn)/sigma_;

    kyy(iSA,0) += -ntr*(nu + nt*fn)/sigma_;
    kyy(iSA,5) +=     (nu + nt*fn)/sigma_;

    kzz(iSA,0) += -ntr*(nu + nt*fn)/sigma_;
    kzz(iSA,5) +=     (nu + nt*fn)/sigma_;
  }
  else
  {
    kxx(iSA,0) += -ntr*(nu + nt)/sigma_;
    kxx(iSA,5) +=     (nu + nt)/sigma_;

    kyy(iSA,0) += -ntr*(nu + nt)/sigma_;
    kyy(iSA,5) +=     (nu + nt)/sigma_;

    kzz(iSA,0) += -ntr*(nu + nt)/sigma_;
    kzz(iSA,5) +=     (nu + nt)/sigma_;
  }

  // Reynolds stresses diffusion matrix

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Real Cp = this->gas_.Cp();

    Tq fv1 = chi3 / (chi3 + power<3>(cv1_));
    Tq muEddy = rho*nt*fv1;

    // Augment the molecular viscosity and thermal conductivity
    mu += muEddy;
    k  += muEddy*Cp/prandtlEddy_;

#ifdef SANS_SA_QCR
    typedef typename promote_Surreal<Tq,Tg>::type T;

    Tq lambdaEddy = -2./3. * muEddy;

    T rhox, ux, vx, wx, tx;
    T rhoy, uy, vy, wy, ty;
    T rhoz, uz, vz, wz, tz;
    qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
    qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
    qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

    Tq rhoi = 1/rho;

    // velocity gradient Jacobian wrt conservative variables
    Tq  ux_rhox  = -u*rhoi;
    Tq& ux_rhoux = rhoi;

    Tq& uy_rhoy  = ux_rhox;
    Tq& uy_rhouy = rhoi;

    Tq& uz_rhoz  = ux_rhox;
    Tq& uz_rhouz = rhoi;

    Tq  vx_rhox  = -v*rhoi;
    Tq& vx_rhovx = rhoi;

    Tq& vy_rhoy  = vx_rhox;
    Tq& vy_rhovy = rhoi;

    Tq& vz_rhoz  = vx_rhox;
    Tq& vz_rhovz = rhoi;

    Tq  wx_rhox  = -w*rhoi;
    Tq& wx_rhowx = rhoi;

    Tq& wy_rhoy  = wx_rhox;
    Tq& wy_rhowy = rhoi;

    Tq& wz_rhoz  = wx_rhox;
    Tq& wz_rhowz = rhoi;

    T tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy + wz);
    T tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy + wz);
    T tauRzz = muEddy*(2*wz   ) + lambdaEddy*(ux + vy + wz);
    T tauRxy = muEddy*(uy + vx);
    T tauRxz = muEddy*(uz + wx);
    T tauRyz = muEddy*(vz + wy);

    Tq  tauRxx_ux = muEddy*2 + lambdaEddy;
    Tq& tauRxx_vy = lambdaEddy;
    Tq& tauRxx_wz = lambdaEddy;

    Tq& tauRyy_ux = lambdaEddy;
    Tq  tauRyy_vy = muEddy*2 + lambdaEddy;
    Tq& tauRyy_wz = lambdaEddy;

    Tq& tauRzz_ux = lambdaEddy;
    Tq& tauRzz_vy = lambdaEddy;
    Tq  tauRzz_wz = muEddy*2 + lambdaEddy;

    Tq& tauRxy_uy = muEddy;
    Tq& tauRxy_vx = muEddy;

    Tq& tauRxz_uz = muEddy;
    Tq& tauRxz_wx = muEddy;

    Tq& tauRyz_vz = muEddy;
    Tq& tauRyz_wy = muEddy;

    T magGradV = sqrt(ux*ux + uy*uy + uz*uz +
                      vx*vx + vy*vy + vz*vz +
                      wx*wx + wy*wy + wz*wz);
    T magGradV_ux = (magGradV == 0.0) ? T(0) : ux/magGradV;
    T magGradV_uy = (magGradV == 0.0) ? T(0) : uy/magGradV;
    T magGradV_uz = (magGradV == 0.0) ? T(0) : uz/magGradV;
    T magGradV_vx = (magGradV == 0.0) ? T(0) : vx/magGradV;
    T magGradV_vy = (magGradV == 0.0) ? T(0) : vy/magGradV;
    T magGradV_vz = (magGradV == 0.0) ? T(0) : vz/magGradV;
    T magGradV_wx = (magGradV == 0.0) ? T(0) : wx/magGradV;
    T magGradV_wy = (magGradV == 0.0) ? T(0) : wy/magGradV;
    T magGradV_wz = (magGradV == 0.0) ? T(0) : wz/magGradV;
    T magGradV2   = magGradV*magGradV;

    T    Wxy    = (uy - vx);
    Real Wxy_uy = (1      );
    Real Wxy_vx = (   -  1);

    T    Wxz    = (uz - wx);
    Real Wxz_uz = (1      );
    Real Wxz_wx = (   -  1);

    T    Wyz    = (vz - wy);
    Real Wyz_vz = (1      );
    Real Wyz_wy = (   -  1);

    T Oxy    = (magGradV == 0.0) ? T(0) : Wxy/magGradV;
    T Oxy_ux = (magGradV == 0.0) ? T(0) :                 - Wxy*magGradV_ux/magGradV2;
    T Oxy_uy = (magGradV == 0.0) ? T(0) : Wxy_uy/magGradV - Wxy*magGradV_uy/magGradV2;
    T Oxy_uz = (magGradV == 0.0) ? T(0) :                 - Wxy*magGradV_uz/magGradV2;
    T Oxy_vx = (magGradV == 0.0) ? T(0) : Wxy_vx/magGradV - Wxy*magGradV_vx/magGradV2;
    T Oxy_vy = (magGradV == 0.0) ? T(0) :                 - Wxy*magGradV_vy/magGradV2;
    T Oxy_vz = (magGradV == 0.0) ? T(0) :                 - Wxy*magGradV_vz/magGradV2;
    T Oxy_wx = (magGradV == 0.0) ? T(0) :                 - Wxy*magGradV_wx/magGradV2;
    T Oxy_wy = (magGradV == 0.0) ? T(0) :                 - Wxy*magGradV_wy/magGradV2;
    T Oxy_wz = (magGradV == 0.0) ? T(0) :                 - Wxy*magGradV_wz/magGradV2;

    T Oyx    = -Oxy;
    T Oyx_ux = -Oxy_ux;
    T Oyx_uy = -Oxy_uy;
    T Oyx_uz = -Oxy_uz;
    T Oyx_vx = -Oxy_vx;
    T Oyx_vy = -Oxy_vy;
    T Oyx_vz = -Oxy_vz;
    T Oyx_wx = -Oxy_wx;
    T Oyx_wy = -Oxy_wy;
    T Oyx_wz = -Oxy_wz;

    T Oxz    = (magGradV == 0.0) ? T(0) : Wxz/magGradV;
    T Oxz_ux = (magGradV == 0.0) ? T(0) :                 - Wxz*magGradV_ux/magGradV2;
    T Oxz_uy = (magGradV == 0.0) ? T(0) :                 - Wxz*magGradV_uy/magGradV2;
    T Oxz_uz = (magGradV == 0.0) ? T(0) : Wxz_uz/magGradV - Wxz*magGradV_uz/magGradV2;
    T Oxz_vx = (magGradV == 0.0) ? T(0) :                 - Wxz*magGradV_vx/magGradV2;
    T Oxz_vy = (magGradV == 0.0) ? T(0) :                 - Wxz*magGradV_vy/magGradV2;
    T Oxz_vz = (magGradV == 0.0) ? T(0) :                 - Wxz*magGradV_vz/magGradV2;
    T Oxz_wx = (magGradV == 0.0) ? T(0) : Wxz_wx/magGradV - Wxz*magGradV_wx/magGradV2;
    T Oxz_wy = (magGradV == 0.0) ? T(0) :                 - Wxz*magGradV_wy/magGradV2;
    T Oxz_wz = (magGradV == 0.0) ? T(0) :                 - Wxz*magGradV_wz/magGradV2;

    T Ozx    = -Oxz;
    T Ozx_ux = -Oxz_ux;
    T Ozx_uy = -Oxz_uy;
    T Ozx_uz = -Oxz_uz;
    T Ozx_vx = -Oxz_vx;
    T Ozx_vy = -Oxz_vy;
    T Ozx_vz = -Oxz_vz;
    T Ozx_wx = -Oxz_wx;
    T Ozx_wy = -Oxz_wy;
    T Ozx_wz = -Oxz_wz;

    T Oyz    = (magGradV == 0.0) ? T(0) : Wyz/magGradV;
    T Oyz_ux = (magGradV == 0.0) ? T(0) :                 - Wyz*magGradV_ux/magGradV2;
    T Oyz_uy = (magGradV == 0.0) ? T(0) :                 - Wyz*magGradV_uy/magGradV2;
    T Oyz_uz = (magGradV == 0.0) ? T(0) :                 - Wyz*magGradV_uz/magGradV2;
    T Oyz_vx = (magGradV == 0.0) ? T(0) :                 - Wyz*magGradV_vx/magGradV2;
    T Oyz_vy = (magGradV == 0.0) ? T(0) :                 - Wyz*magGradV_vy/magGradV2;
    T Oyz_vz = (magGradV == 0.0) ? T(0) : Wyz_vz/magGradV - Wyz*magGradV_vz/magGradV2;
    T Oyz_wx = (magGradV == 0.0) ? T(0) :                 - Wyz*magGradV_wx/magGradV2;
    T Oyz_wy = (magGradV == 0.0) ? T(0) : Wyz_wy/magGradV - Wyz*magGradV_wy/magGradV2;
    T Oyz_wz = (magGradV == 0.0) ? T(0) :                 - Wyz*magGradV_wz/magGradV2;

    T Ozy    = -Oyz;
    T Ozy_ux = -Oyz_ux;
    T Ozy_uy = -Oyz_uy;
    T Ozy_uz = -Oyz_uz;
    T Ozy_vx = -Oyz_vx;
    T Ozy_vy = -Oyz_vy;
    T Ozy_vz = -Oyz_vz;
    T Ozy_wx = -Oyz_wx;
    T Ozy_wy = -Oyz_wy;
    T Ozy_wz = -Oyz_wz;


    //qcrxx    = cr1_*2*(Oxy*tauRxy + Oxz*tauRxz);
    T qcrxx_ux = cr1_*2*(Oxy_ux*tauRxy + Oxz_ux*tauRxz);
    T qcrxx_uy = cr1_*2*(Oxy_uy*tauRxy + Oxy*tauRxy_uy + Oxz_uy*tauRxz);
    T qcrxx_uz = cr1_*2*(Oxy_uz*tauRxy + Oxz_uz*tauRxz + Oxz*tauRxz_uz);

    T qcrxx_vx = cr1_*2*(Oxy_vx*tauRxy + Oxy*tauRxy_vx + Oxz_vx*tauRxz);
    T qcrxx_vy = cr1_*2*(Oxy_vy*tauRxy + Oxz_vy*tauRxz);
    T qcrxx_vz = cr1_*2*(Oxy_vz*tauRxy + Oxz_vz*tauRxz);

    T qcrxx_wx = cr1_*2*(Oxy_wx*tauRxy + Oxz_wx*tauRxz + Oxz*tauRxz_wx);
    T qcrxx_wy = cr1_*2*(Oxy_wy*tauRxy + Oxz_wy*tauRxz);
    T qcrxx_wz = cr1_*2*(Oxy_wz*tauRxy + Oxz_wz*tauRxz);

    //qcrxy    = cr1_*(Oxy*tauRyy + Oxz*tauRyz + Oyx*tauRxx + Oyz*tauRxz);
    T qcrxy_ux = cr1_*(Oxy_ux*tauRyy + Oxy*tauRyy_ux + Oxz_ux*tauRyz + Oyx_ux*tauRxx + Oyx*tauRxx_ux + Oyz_ux*tauRxz);
    T qcrxy_uy = cr1_*(Oxy_uy*tauRyy + Oxz_uy*tauRyz + Oyx_uy*tauRxx + Oyz_uy*tauRxz);
    T qcrxy_uz = cr1_*(Oxy_uz*tauRyy + Oxz_uz*tauRyz + Oyx_uz*tauRxx + Oyz_uz*tauRxz + Oyz*tauRxz_uz);

    T qcrxy_vx = cr1_*(Oxy_vx*tauRyy + Oxz_vx*tauRyz + Oyx_vx*tauRxx + Oyz_vx*tauRxz);
    T qcrxy_vy = cr1_*(Oxy_vy*tauRyy + Oxy*tauRyy_vy + Oxz_vy*tauRyz + Oyx_vy*tauRxx + Oyx*tauRxx_vy + Oyz_vy*tauRxz);
    T qcrxy_vz = cr1_*(Oxy_vz*tauRyy + Oxz_vz*tauRyz + Oxz*tauRyz_vz + Oyx_vz*tauRxx + Oyz_vz*tauRxz);

    T qcrxy_wx = cr1_*(Oxy_wx*tauRyy + Oxz_wx*tauRyz + Oyx_wx*tauRxx + Oyz_wx*tauRxz + Oyz*tauRxz_wx);
    T qcrxy_wy = cr1_*(Oxy_wy*tauRyy + Oxz_wy*tauRyz + Oxz*tauRyz_wy + Oyx_wy*tauRxx + Oyz_wy*tauRxz);
    T qcrxy_wz = cr1_*(Oxy_wz*tauRyy + Oxy*tauRyy_wz + Oxz_wz*tauRyz + Oyx_wz*tauRxx + Oyx*tauRxx_wz + Oyz_wz*tauRxz);

    // qcrxz   = cr1_*(Oxy*tauRyz + Oxz*tauRzz + Ozx*tauRxx + Ozy*tauRxy);
    T qcrxz_ux = cr1_*(Oxy_ux*tauRyz + Oxz_ux*tauRzz + Oxz*tauRzz_ux + Ozx_ux*tauRxx + Ozx*tauRxx_ux + Ozy_ux*tauRxy);
    T qcrxz_uy = cr1_*(Oxy_uy*tauRyz + Oxz_uy*tauRzz + Ozx_uy*tauRxx + Ozy_uy*tauRxy + Ozy*tauRxy_uy);
    T qcrxz_uz = cr1_*(Oxy_uz*tauRyz + Oxz_uz*tauRzz + Ozx_uz*tauRxx + Ozy_uz*tauRxy);

    T qcrxz_vx = cr1_*(Oxy_vx*tauRyz + Oxz_vx*tauRzz + Ozx_vx*tauRxx + Ozy_vx*tauRxy + Ozy*tauRxy_vx);
    T qcrxz_vy = cr1_*(Oxy_vy*tauRyz + Oxz_vy*tauRzz + Oxz*tauRzz_vy + Ozx_vy*tauRxx + Ozx*tauRxx_vy + Ozy_vy*tauRxy);
    T qcrxz_vz = cr1_*(Oxy_vz*tauRyz + Oxy*tauRyz_vz + Oxz_vz*tauRzz + Ozx_vz*tauRxx + Ozy_vz*tauRxy);

    T qcrxz_wx = cr1_*(Oxy_wx*tauRyz + Oxz_wx*tauRzz + Ozx_wx*tauRxx + Ozy_wx*tauRxy);
    T qcrxz_wy = cr1_*(Oxy_wy*tauRyz + Oxy*tauRyz_wy + Oxz_wy*tauRzz + Ozx_wy*tauRxx + Ozy_wy*tauRxy);
    T qcrxz_wz = cr1_*(Oxy_wz*tauRyz + Oxz_wz*tauRzz + Oxz*tauRzz_wz + Ozx_wz*tauRxx + Ozx*tauRxx_wz + Ozy_wz*tauRxy);

    //qcryy    = cr1_*2*(Oyx*tauRxy + Oyz*tauRyz);
    T qcryy_ux = cr1_*2*(Oyx_ux*tauRxy + Oyz_ux*tauRyz);
    T qcryy_uy = cr1_*2*(Oyx_uy*tauRxy + Oyx*tauRxy_uy + Oyz_uy*tauRyz);
    T qcryy_uz = cr1_*2*(Oyx_uz*tauRxy + Oyz_uz*tauRyz);

    T qcryy_vx = cr1_*2*(Oyx_vx*tauRxy + Oyx*tauRxy_vx + Oyz_vx*tauRyz);
    T qcryy_vy = cr1_*2*(Oyx_vy*tauRxy + Oyz_vy*tauRyz);
    T qcryy_vz = cr1_*2*(Oyx_vz*tauRxy + Oyz_vz*tauRyz + Oyz*tauRyz_vz);

    T qcryy_wx = cr1_*2*(Oyx_wx*tauRxy + Oyz_wx*tauRyz);
    T qcryy_wy = cr1_*2*(Oyx_wy*tauRxy + Oyz_wy*tauRyz + Oyz*tauRyz_wy);
    T qcryy_wz = cr1_*2*(Oyx_wz*tauRxy + Oyz_wz*tauRyz);

    //qcryz    = cr1_*(Oyx*tauRxz + Oyz*tauRzz + Ozx*tauRxy + Ozy*tauRyy);
    T qcryz_ux = cr1_*(Oyx_ux*tauRxz + Oyz_ux*tauRzz + Oyz*tauRzz_ux + Ozx_ux*tauRxy + Ozy_ux*tauRyy + Ozy*tauRyy_ux);
    T qcryz_uy = cr1_*(Oyx_uy*tauRxz + Oyz_uy*tauRzz +Ozx_uy*tauRxy + Ozx*tauRxy_uy + Ozy_uy*tauRyy);
    T qcryz_uz = cr1_*(Oyx_uz*tauRxz + Oyx*tauRxz_uz + Oyz_uz*tauRzz +Ozx_uz*tauRxy + Ozy_uz*tauRyy);

    T qcryz_vx = cr1_*(Oyx_vx*tauRxz + Oyz_vx*tauRzz + Ozx_vx*tauRxy + Ozx*tauRxy_vx + Ozy_vx*tauRyy);
    T qcryz_vy = cr1_*(Oyx_vy*tauRxz + Oyz_vy*tauRzz + Oyz*tauRzz_vy + Ozx_vy*tauRxy + Ozy_vy*tauRyy + Ozy*tauRyy_vy);
    T qcryz_vz = cr1_*(Oyx_vz*tauRxz + Oyz_vz*tauRzz + Ozx_vz*tauRxy + Ozy_vz*tauRyy);

    T qcryz_wx = cr1_*(Oyx_wx*tauRxz + Oyx*tauRxz_wx + Oyz_wx*tauRzz + Ozx_wx*tauRxy + Ozy_wx*tauRyy);
    T qcryz_wy = cr1_*(Oyx_wy*tauRxz + Oyz_wy*tauRzz + Ozx_wy*tauRxy + Ozy_wy*tauRyy);
    T qcryz_wz = cr1_*(Oyx_wz*tauRxz + Oyz_wz*tauRzz + Oyz*tauRzz_wz + Ozx_wz*tauRxy + Ozy_wz*tauRyy + Ozy*tauRyy_wz);

    //qcrzz    = cr1_*2*(Ozx*tauRxz + Ozy*tauRyz);
    T qcrzz_ux = cr1_*2*(Ozx_ux*tauRxz + Ozy_ux*tauRyz);
    T qcrzz_uy = cr1_*2*(Ozx_uy*tauRxz + Ozy_uy*tauRyz);
    T qcrzz_uz = cr1_*2*(Ozx_uz*tauRxz + Ozx*tauRxz_uz + Ozy_uz*tauRyz);

    T qcrzz_vx = cr1_*2*(Ozx_vx*tauRxz + Ozy_vx*tauRyz);
    T qcrzz_vy = cr1_*2*(Ozx_vy*tauRxz + Ozy_vy*tauRyz);
    T qcrzz_vz = cr1_*2*(Ozx_vz*tauRxz + Ozy_vz*tauRyz + Ozy*tauRyz_vz);

    T qcrzz_wx = cr1_*2*(Ozx_wx*tauRxz + Ozx*tauRxz_wx + Ozy_wx*tauRyz);
    T qcrzz_wy = cr1_*2*(Ozx_wy*tauRxz + Ozy_wy*tauRyz + Ozy*tauRyz_wy);
    T qcrzz_wz = cr1_*2*(Ozx_wz*tauRxz + Ozy_wz*tauRyz);


    // Convert to derivatives w.r.t. master state
    T qcrxx_rhox  = qcrxx_ux*ux_rhox + qcrxx_vx*vx_rhox + qcrxx_wx*wx_rhox;
    T qcrxx_rhoy  = qcrxx_uy*uy_rhoy + qcrxx_vy*vy_rhoy + qcrxx_wy*wy_rhoy;
    T qcrxx_rhoz  = qcrxx_uz*uz_rhoz + qcrxx_vz*vz_rhoz + qcrxx_wz*wz_rhoz;
    T qcrxx_rhoux = qcrxx_ux*ux_rhoux;
    T qcrxx_rhouy = qcrxx_uy*uy_rhouy;
    T qcrxx_rhouz = qcrxx_uz*uz_rhouz;
    T qcrxx_rhovx = qcrxx_vx*vx_rhovx;
    T qcrxx_rhovy = qcrxx_vy*vy_rhovy;
    T qcrxx_rhovz = qcrxx_vz*vz_rhovz;
    T qcrxx_rhowx = qcrxx_wx*wx_rhowx;
    T qcrxx_rhowy = qcrxx_wy*wy_rhowy;
    T qcrxx_rhowz = qcrxx_wz*wz_rhowz;

    T qcrxy_rhox  = qcrxy_ux*ux_rhox + qcrxy_vx*vx_rhox + qcrxy_wx*wx_rhox;
    T qcrxy_rhoy  = qcrxy_uy*uy_rhoy + qcrxy_vy*vy_rhoy + qcrxy_wy*wy_rhoy;
    T qcrxy_rhoz  = qcrxy_uz*uz_rhoz + qcrxy_vz*vz_rhoz + qcrxy_wz*wz_rhoz;
    T qcrxy_rhoux = qcrxy_ux*ux_rhoux;
    T qcrxy_rhouy = qcrxy_uy*uy_rhouy;
    T qcrxy_rhouz = qcrxy_uz*uz_rhouz;
    T qcrxy_rhovx = qcrxy_vx*vx_rhovx;
    T qcrxy_rhovy = qcrxy_vy*vy_rhovy;
    T qcrxy_rhovz = qcrxy_vz*vz_rhovz;
    T qcrxy_rhowx = qcrxy_wx*wx_rhowx;
    T qcrxy_rhowy = qcrxy_wy*wy_rhowy;
    T qcrxy_rhowz = qcrxy_wz*wz_rhowz;

    T qcrxz_rhox  = qcrxz_ux*ux_rhox + qcrxz_vx*vx_rhox + qcrxz_wx*wx_rhox;
    T qcrxz_rhoy  = qcrxz_uy*uy_rhoy + qcrxz_vy*vy_rhoy + qcrxz_wy*wy_rhoy;
    T qcrxz_rhoz  = qcrxz_uz*uz_rhoz + qcrxz_vz*vz_rhoz + qcrxz_wz*wz_rhoz;
    T qcrxz_rhoux = qcrxz_ux*ux_rhoux;
    T qcrxz_rhouy = qcrxz_uy*uy_rhouy;
    T qcrxz_rhouz = qcrxz_uz*uz_rhouz;
    T qcrxz_rhovx = qcrxz_vx*vx_rhovx;
    T qcrxz_rhovy = qcrxz_vy*vy_rhovy;
    T qcrxz_rhovz = qcrxz_vz*vz_rhovz;
    T qcrxz_rhowx = qcrxz_wx*wx_rhowx;
    T qcrxz_rhowy = qcrxz_wy*wy_rhowy;
    T qcrxz_rhowz = qcrxz_wz*wz_rhowz;

    T qcryy_rhox  = qcryy_ux*ux_rhox + qcryy_vx*vx_rhox + qcryy_wx*wx_rhox;
    T qcryy_rhoy  = qcryy_uy*uy_rhoy + qcryy_vy*vy_rhoy + qcryy_wy*wy_rhoy;
    T qcryy_rhoz  = qcryy_uz*uz_rhoz + qcryy_vz*vz_rhoz + qcryy_wz*wz_rhoz;
    T qcryy_rhoux = qcryy_ux*ux_rhoux;
    T qcryy_rhouy = qcryy_uy*uy_rhouy;
    T qcryy_rhouz = qcryy_uz*uz_rhouz;
    T qcryy_rhovx = qcryy_vx*vx_rhovx;
    T qcryy_rhovy = qcryy_vy*vy_rhovy;
    T qcryy_rhovz = qcryy_vz*vz_rhovz;
    T qcryy_rhowx = qcryy_wx*wx_rhowx;
    T qcryy_rhowy = qcryy_wy*wy_rhowy;
    T qcryy_rhowz = qcryy_wz*wz_rhowz;

    T qcryz_rhox  = qcryz_ux*ux_rhox + qcryz_vx*vx_rhox + qcryz_wx*wx_rhox;
    T qcryz_rhoy  = qcryz_uy*uy_rhoy + qcryz_vy*vy_rhoy + qcryz_wy*wy_rhoy;
    T qcryz_rhoz  = qcryz_uz*uz_rhoz + qcryz_vz*vz_rhoz + qcryz_wz*wz_rhoz;
    T qcryz_rhoux = qcryz_ux*ux_rhoux;
    T qcryz_rhouy = qcryz_uy*uy_rhouy;
    T qcryz_rhouz = qcryz_uz*uz_rhouz;
    T qcryz_rhovx = qcryz_vx*vx_rhovx;
    T qcryz_rhovy = qcryz_vy*vy_rhovy;
    T qcryz_rhovz = qcryz_vz*vz_rhovz;
    T qcryz_rhowx = qcryz_wx*wx_rhowx;
    T qcryz_rhowy = qcryz_wy*wy_rhowy;
    T qcryz_rhowz = qcryz_wz*wz_rhowz;

    T qcrzz_rhox  = qcrzz_ux*ux_rhox + qcrzz_vx*vx_rhox + qcrzz_wx*wx_rhox;
    T qcrzz_rhoy  = qcrzz_uy*uy_rhoy + qcrzz_vy*vy_rhoy + qcrzz_wy*wy_rhoy;
    T qcrzz_rhoz  = qcrzz_uz*uz_rhoz + qcrzz_vz*vz_rhoz + qcrzz_wz*wz_rhoz;
    T qcrzz_rhoux = qcrzz_ux*ux_rhoux;
    T qcrzz_rhouy = qcrzz_uy*uy_rhouy;
    T qcrzz_rhouz = qcrzz_uz*uz_rhouz;
    T qcrzz_rhovx = qcrzz_vx*vx_rhovx;
    T qcrzz_rhovy = qcrzz_vy*vy_rhovy;
    T qcrzz_rhovz = qcrzz_vz*vz_rhovz;
    T qcrzz_rhowx = qcrzz_wx*wx_rhowx;
    T qcrzz_rhowy = qcrzz_wy*wy_rhowy;
    T qcrzz_rhowz = qcrzz_wz*wz_rhowz;

    // d(Fv)/d(Ux)
    kxx(ixMom,0) -= qcrxx_rhox ;
    kxx(ixMom,1) -= qcrxx_rhoux;
    kxx(ixMom,2) -= qcrxx_rhovx;
    kxx(ixMom,3) -= qcrxx_rhowx;

    kxx(iyMom,0) -= qcrxy_rhox ;
    kxx(iyMom,1) -= qcrxy_rhoux;
    kxx(iyMom,2) -= qcrxy_rhovx;
    kxx(iyMom,3) -= qcrxy_rhowx;

    kxx(izMom,0) -= qcrxz_rhox ;
    kxx(izMom,1) -= qcrxz_rhoux;
    kxx(izMom,2) -= qcrxz_rhovx;
    kxx(izMom,3) -= qcrxz_rhowx;

    kxx(iEngy,0) -= u*qcrxx_rhox  + v*qcrxy_rhox  + w*qcrxz_rhox;
    kxx(iEngy,1) -= u*qcrxx_rhoux + v*qcrxy_rhoux + w*qcrxz_rhoux;
    kxx(iEngy,2) -= u*qcrxx_rhovx + v*qcrxy_rhovx + w*qcrxz_rhovx;
    kxx(iEngy,3) -= u*qcrxx_rhowx + v*qcrxy_rhowx + w*qcrxz_rhowx;

    // d(Fv)/d(Uy)
    kxy(ixMom,0) -= qcrxx_rhoy ;
    kxy(ixMom,1) -= qcrxx_rhouy;
    kxy(ixMom,2) -= qcrxx_rhovy;
    kxy(ixMom,3) -= qcrxx_rhowy;

    kxy(iyMom,0) -= qcrxy_rhoy ;
    kxy(iyMom,1) -= qcrxy_rhouy;
    kxy(iyMom,2) -= qcrxy_rhovy;
    kxy(iyMom,3) -= qcrxy_rhowy;

    kxy(izMom,0) -= qcrxz_rhoy ;
    kxy(izMom,1) -= qcrxz_rhouy;
    kxy(izMom,2) -= qcrxz_rhovy;
    kxy(izMom,3) -= qcrxz_rhowy;

    kxy(iEngy,0) -= u*qcrxx_rhoy  + v*qcrxy_rhoy  + w*qcrxz_rhoy ;
    kxy(iEngy,1) -= u*qcrxx_rhouy + v*qcrxy_rhouy + w*qcrxz_rhouy;
    kxy(iEngy,2) -= u*qcrxx_rhovy + v*qcrxy_rhovy + w*qcrxz_rhovy;
    kxy(iEngy,3) -= u*qcrxx_rhowy + v*qcrxy_rhowy + w*qcrxz_rhowy;

    // d(Fv)/d(Uz)
    kxz(ixMom,0) -= qcrxx_rhoz ;
    kxz(ixMom,1) -= qcrxx_rhouz;
    kxz(ixMom,2) -= qcrxx_rhovz;
    kxz(ixMom,3) -= qcrxx_rhowz;

    kxz(iyMom,0) -= qcrxy_rhoz ;
    kxz(iyMom,1) -= qcrxy_rhouz;
    kxz(iyMom,2) -= qcrxy_rhovz;
    kxz(iyMom,3) -= qcrxy_rhowz;

    kxz(izMom,0) -= qcrxz_rhoz ;
    kxz(izMom,1) -= qcrxz_rhouz;
    kxz(izMom,2) -= qcrxz_rhovz;
    kxz(izMom,3) -= qcrxz_rhowz;

    kxz(iEngy,0) -= u*qcrxx_rhoz  + v*qcrxy_rhoz  + w*qcrxz_rhoz ;
    kxz(iEngy,1) -= u*qcrxx_rhouz + v*qcrxy_rhouz + w*qcrxz_rhouz;
    kxz(iEngy,2) -= u*qcrxx_rhovz + v*qcrxy_rhovz + w*qcrxz_rhovz;
    kxz(iEngy,3) -= u*qcrxx_rhowz + v*qcrxy_rhowz + w*qcrxz_rhowz;

    // d(Gv)/d(Ux)
    kyx(ixMom,0) -= qcrxy_rhox ;
    kyx(ixMom,1) -= qcrxy_rhoux;
    kyx(ixMom,2) -= qcrxy_rhovx;
    kyx(ixMom,3) -= qcrxy_rhowx;

    kyx(iyMom,0) -= qcryy_rhox ;
    kyx(iyMom,1) -= qcryy_rhoux;
    kyx(iyMom,2) -= qcryy_rhovx;
    kyx(iyMom,3) -= qcryy_rhowx;

    kyx(izMom,0) -= qcryz_rhox ;
    kyx(izMom,1) -= qcryz_rhoux;
    kyx(izMom,2) -= qcryz_rhovx;
    kyx(izMom,3) -= qcryz_rhowx;

    kyx(iEngy,0) -= u*qcrxy_rhox  + v*qcryy_rhox  + w*qcryz_rhox ;
    kyx(iEngy,1) -= u*qcrxy_rhoux + v*qcryy_rhoux + w*qcryz_rhoux;
    kyx(iEngy,2) -= u*qcrxy_rhovx + v*qcryy_rhovx + w*qcryz_rhovx;
    kyx(iEngy,3) -= u*qcrxy_rhowx + v*qcryy_rhowx + w*qcryz_rhowx;

    // d(Gv)/d(Uy)
    kyy(ixMom,0) -= qcrxy_rhoy ;
    kyy(ixMom,1) -= qcrxy_rhouy;
    kyy(ixMom,2) -= qcrxy_rhovy;
    kyy(ixMom,3) -= qcrxy_rhowy;

    kyy(iyMom,0) -= qcryy_rhoy ;
    kyy(iyMom,1) -= qcryy_rhouy;
    kyy(iyMom,2) -= qcryy_rhovy;
    kyy(iyMom,3) -= qcryy_rhowy;

    kyy(izMom,0) -= qcryz_rhoy ;
    kyy(izMom,1) -= qcryz_rhouy;
    kyy(izMom,2) -= qcryz_rhovy;
    kyy(izMom,3) -= qcryz_rhowy;

    kyy(iEngy,0) -= u*qcrxy_rhoy  + v*qcryy_rhoy  + w*qcryz_rhoy ;
    kyy(iEngy,1) -= u*qcrxy_rhouy + v*qcryy_rhouy + w*qcryz_rhouy;
    kyy(iEngy,2) -= u*qcrxy_rhovy + v*qcryy_rhovy + w*qcryz_rhovy;
    kyy(iEngy,3) -= u*qcrxy_rhowy + v*qcryy_rhowy + w*qcryz_rhowy;

    // d(Gv)/d(Uz)
    kyz(ixMom,0) -= qcrxy_rhoz ;
    kyz(ixMom,1) -= qcrxy_rhouz;
    kyz(ixMom,2) -= qcrxy_rhovz;
    kyz(ixMom,3) -= qcrxy_rhowz;

    kyz(iyMom,0) -= qcryy_rhoz ;
    kyz(iyMom,1) -= qcryy_rhouz;
    kyz(iyMom,2) -= qcryy_rhovz;
    kyz(iyMom,3) -= qcryy_rhowz;

    kyz(izMom,0) -= qcryz_rhoz ;
    kyz(izMom,1) -= qcryz_rhouz;
    kyz(izMom,2) -= qcryz_rhovz;
    kyz(izMom,3) -= qcryz_rhowz;

    kyz(iEngy,0) -= u*qcrxy_rhoz  + v*qcryy_rhoz  + w*qcryz_rhoz ;
    kyz(iEngy,1) -= u*qcrxy_rhouz + v*qcryy_rhouz + w*qcryz_rhouz;
    kyz(iEngy,2) -= u*qcrxy_rhovz + v*qcryy_rhovz + w*qcryz_rhovz;
    kyz(iEngy,3) -= u*qcrxy_rhowz + v*qcryy_rhowz + w*qcryz_rhowz;

    // d(Hv)/d(Ux)
    kzx(ixMom,0) -= qcrxz_rhox ;
    kzx(ixMom,1) -= qcrxz_rhoux;
    kzx(ixMom,2) -= qcrxz_rhovx;
    kzx(ixMom,3) -= qcrxz_rhowx;

    kzx(iyMom,0) -= qcryz_rhox ;
    kzx(iyMom,1) -= qcryz_rhoux;
    kzx(iyMom,2) -= qcryz_rhovx;
    kzx(iyMom,3) -= qcryz_rhowx;

    kzx(izMom,0) -= qcrzz_rhox ;
    kzx(izMom,1) -= qcrzz_rhoux;
    kzx(izMom,2) -= qcrzz_rhovx;
    kzx(izMom,3) -= qcrzz_rhowx;

    kzx(iEngy,0) -= u*qcrxz_rhox  + v*qcryz_rhox  + w*qcrzz_rhox ;
    kzx(iEngy,1) -= u*qcrxz_rhoux + v*qcryz_rhoux + w*qcrzz_rhoux;
    kzx(iEngy,2) -= u*qcrxz_rhovx + v*qcryz_rhovx + w*qcrzz_rhovx;
    kzx(iEngy,3) -= u*qcrxz_rhowx + v*qcryz_rhowx + w*qcrzz_rhowx;

    // d(Hv)/d(Uy)
    kzy(ixMom,0) -= qcrxz_rhoy ;
    kzy(ixMom,1) -= qcrxz_rhouy;
    kzy(ixMom,2) -= qcrxz_rhovy;
    kzy(ixMom,3) -= qcrxz_rhowy;

    kzy(iyMom,0) -= qcryz_rhoy ;
    kzy(iyMom,1) -= qcryz_rhouy;
    kzy(iyMom,2) -= qcryz_rhovy;
    kzy(iyMom,3) -= qcryz_rhowy;

    kzy(izMom,0) -= qcrzz_rhoy ;
    kzy(izMom,1) -= qcrzz_rhouy;
    kzy(izMom,2) -= qcrzz_rhovy;
    kzy(izMom,3) -= qcrzz_rhowy;

    kzy(iEngy,0) -= u*qcrxz_rhoy  + v*qcryz_rhoy  + w*qcrzz_rhoy ;
    kzy(iEngy,1) -= u*qcrxz_rhouy + v*qcryz_rhouy + w*qcrzz_rhouy;
    kzy(iEngy,2) -= u*qcrxz_rhovy + v*qcryz_rhovy + w*qcrzz_rhovy;
    kzy(iEngy,3) -= u*qcrxz_rhowy + v*qcryz_rhowy + w*qcrzz_rhowy;

    // d(Hv)/d(Uz)
    kzz(ixMom,0) -= qcrxz_rhoz ;
    kzz(ixMom,1) -= qcrxz_rhouz;
    kzz(ixMom,2) -= qcrxz_rhovz;
    kzz(ixMom,3) -= qcrxz_rhowz;

    kzz(iyMom,0) -= qcryz_rhoz ;
    kzz(iyMom,1) -= qcryz_rhouz;
    kzz(iyMom,2) -= qcryz_rhovz;
    kzz(iyMom,3) -= qcryz_rhowz;

    kzz(izMom,0) -= qcrzz_rhoz ;
    kzz(izMom,1) -= qcrzz_rhouz;
    kzz(izMom,2) -= qcrzz_rhovz;
    kzz(izMom,3) -= qcrzz_rhowz;

    kzz(iEngy,0) -= u*qcrxz_rhoz  + v*qcryz_rhoz  + w*qcrzz_rhoz ;
    kzz(iEngy,1) -= u*qcrxz_rhouz + v*qcryz_rhouz + w*qcrzz_rhouz;
    kzz(iEngy,2) -= u*qcrxz_rhovz + v*qcryz_rhovz + w*qcrzz_rhovz;
    kzz(iEngy,3) -= u*qcrxz_rhowz + v*qcryz_rhowz + w*qcrzz_rhowz;
#endif
  }

  // compute the diffusion matrix
  BaseType::diffusionViscous( q, rho, u, v, w, t, mu, k,
                              kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );
}


// gradient viscous diffusion matrix: div . d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tk>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
    MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;

  // SA diffusion matrix

  Tq rho=0, u=0, v=0, w=0, t=0, nt=0;
  Tqg rhox, ux, vx, wx, tx, ntx;
  Tqg rhoy, uy, vy, wy, ty, nty;
  Tqg rhoz, uz, vz, wz, tz, ntz;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  Tq mu_t = 0;
  visc_.viscosityJacobian(t, mu_t);

  Tq  mu = visc_.viscosity( t );
  Tqg mux = mu_t*tx;
  Tqg muy = mu_t*ty;
  Tqg muz = mu_t*tz;

  Tq k_mu = 0;
  tcond_.conductivityJacobian(mu, k_mu);

  Tq  k  = tcond_.conductivity( mu );
  Tqg kx = k_mu*mux;
  Tqg ky = k_mu*muy;
  Tqg kz = k_mu*muz;

  Tq  nu  = mu/rho;
  Tqg nux = mux/rho - mu*rhox/rho/rho;
  Tqg nuy = muy/rho - mu*rhoy/rho/rho;
  Tqg nuz = muz/rho - mu*rhoz/rho/rho;

  Tq  chi  = rho*nt/mu;
  Tq  chi3 = power<3>(chi);
  Tqg chix = rhox*nt/mu + rho*ntx/mu - rho*nt*mux/(mu*mu);
  Tqg chiy = rhoy*nt/mu + rho*nty/mu - rho*nt*muy/(mu*mu);
  Tqg chiz = rhoz*nt/mu + rho*ntz/mu - rho*nt*muz/(mu*mu);

  if (nt < 0)
  {
    Tq fn  = (cn1_ + chi3) / (cn1_ - chi3);
    Tq fn_chi = 6*cn1_*(chi*chi) / power<2,Tq>(cn1_ - chi3);

    Tqg fnx = fn_chi*chix;
    Tqg fny = fn_chi*chiy;
    Tqg fnz = fn_chi*chiz;

    kxx_x(iSA,0) += (-ntx*(nu + nt*fn) - nt*(nux + ntx*fn + nt*fnx))/sigma_/ntref_;
    kxx_x(iSA,5) +=  (nux + ntx*fn + nt*fnx)/sigma_/ntref_;

    kyy_y(iSA,0) += (-nty*(nu + nt*fn) - nt*(nuy + nty*fn + nt*fny))/sigma_/ntref_;
    kyy_y(iSA,5) +=  (nuy + nty*fn + nt*fny)/sigma_/ntref_;

    kzz_z(iSA,0) += (-ntz*(nu + nt*fn) - nt*(nuz + ntz*fn + nt*fnz))/sigma_/ntref_;
    kzz_z(iSA,5) +=  (nuz + ntz*fn + nt*fnz)/sigma_/ntref_;
  }
  else
  {
    kxx_x(iSA,0) += (-ntx*(nu + nt) - nt*(nux + ntx))/sigma_/ntref_;
    kxx_x(iSA,5) +=  (nux + ntx)/sigma_/ntref_;

    kyy_y(iSA,0) += (-nty*(nu + nt) - nt*(nuy + nty))/sigma_/ntref_;
    kyy_y(iSA,5) +=  (nuy + nty)/sigma_/ntref_;

    kzz_z(iSA,0) += (-ntz*(nu + nt) - nt*(nuz + ntz))/sigma_/ntref_;
    kzz_z(iSA,5) +=  (nuz + ntz)/sigma_/ntref_;
  }

  // Reynolds stresses diffusion matrix

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Real cv13 = power<3>(cv1_);
    Tq fv1 = chi3 / (chi3 + cv13);
    Tq muEddy = rho*nt*fv1;

    Tq fv1_chi = 3*cv13*chi*chi / power<2,Tq>(cv13 + chi3);

    Tqg fv1x = fv1_chi*chix;
    Tqg fv1y = fv1_chi*chiy;
    Tqg fv1z = fv1_chi*chiz;

    Tqg muEddyx = rhox*nt*fv1 + rho*ntx*fv1 + rho*nt*fv1x;
    Tqg muEddyy = rhoy*nt*fv1 + rho*nty*fv1 + rho*nt*fv1y;
    Tqg muEddyz = rhoz*nt*fv1 + rho*ntz*fv1 + rho*nt*fv1z;

    mu  += muEddy;
    mux += muEddyx;
    muy += muEddyy;
    muz += muEddyz;

    Real Cp = this->gas_.Cp();
    Tq k_muEddy = Cp/prandtlEddy_;

    k  += muEddy*Cp/prandtlEddy_;
    kx += k_muEddy*muEddyx;
    ky += k_muEddy*muEddyy;
    kz += k_muEddy*muEddyz;

  }
#ifdef SANS_SA_QCR
    SANS_DEVELOPER_EXCEPTION("diffusionViscousGradient not implemented with QCR");
#endif

  // compute gradient of diffusion matrix
  BaseType::diffusionViscousGradient( rho, u, v, w, t, mu, k,
                                      rhox, ux, vx, wx, tx, mux, kx,
                                      rhoy, uy, vy, wy, ty, muy, ky,
                                      rhoz, uz, vz, wz, tz, muz, kz,
                                      kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                      kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                      kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );
}

// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::
jacobianFluxViscous(
    const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu, MatrixQ<Tf>& dhdu ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  // SA fluxes

  Tq rho=0, u=0, v=0, w=0, t=0, nt=0;
  T  rhox=0, ux=0, vx=0, wx=0, tx=0;
  T  rhoy=0, uy=0, vy=0, wy=0, ty=0;
  T  rhoz=0, uz=0, vz=0, wz=0, tz=0;
  T  ntx=0, nty=0, ntz=0;
  Tq mu, k;
  Tq mu_t = 0;
  Tq k_mu = 0;

  qInterpret_.eval( q, rho, u, v, w, t );

  mu = visc_.viscosity( t );
  visc_.viscosityJacobian(t, mu_t);
  k  = tcond_.conductivity( mu );
  tcond_.conductivityJacobian( mu, k_mu);
  Tq E = gas_.energy(rho, t) + 0.5*(u*u + v*v + w*w);
  Real Cv = gas_.Cv();

  Tq t_rho  = ( -E/rho + ( u*u + v*v + w*w )/rho ) / Cv;
  Tq t_rhou = (        - (   u             )/rho ) / Cv;
  Tq t_rhov = (        - (         v       )/rho ) / Cv;
  Tq t_rhow = (        - (               w )/rho ) / Cv;
  Tq t_rhoE = (  1/rho                           ) / Cv;

  ArrayQ<T> dmu = 0;
  dmu(0) = mu_t*t_rho;
  dmu(1) = mu_t*t_rhou;
  dmu(2) = mu_t*t_rhov;
  dmu(3) = mu_t*t_rhow;
  dmu(4) = mu_t*t_rhoE;

  ArrayQ<T> dk = 0;
  dk(0) = k_mu*dmu(0);
  dk(1) = k_mu*dmu(1);
  dk(2) = k_mu*dmu(2);
  dk(3) = k_mu*dmu(3);
  dk(4) = k_mu*dmu(4);

  // SA model viscous terms

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  qInterpret_.evalSA( q, nt );
  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  Tq chi  = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);

  T chi_rhont = 1./mu;

  T chi_mu = -rho*nt/power<2,T>(mu);
  T chi_rho  = chi_mu*dmu(0);
  T chi_rhou = chi_mu*dmu(1);
  T chi_rhov = chi_mu*dmu(2);
  T chi_rhow = chi_mu*dmu(3);
  T chi_rhoE = chi_mu*dmu(4);

  if (nt < 0)
  {
    T fn  = (cn1_ + chi3) / (cn1_ - chi3);
    T fn_chi = 3*power<2,T>(chi)/(cn1_ - chi3) + 3*power<2,T>(chi)*(cn1_ + chi3)/power<2,T>(cn1_ - chi3);

    T fn_rho  = fn_chi*chi_rho;
    T fn_rhou = fn_chi*chi_rhou;
    T fn_rhov = fn_chi*chi_rhov;
    T fn_rhow = fn_chi*chi_rhow;
    T fn_rhoE = fn_chi*chi_rhoE;
    T fn_rhont = fn_chi*chi_rhont;

//    f(iSA) -= (mu + rho*nt*fn)*ntx/sigma_;
    dfdu(iSA,0) -= ((dmu(0) + rho*nt*fn_rho )*ntx  + (mu + rho*nt*fn)*(nt*rhox - rho*ntx)/power<2>(rho))/sigma_/ntref_;
    dfdu(iSA,1) -= (dmu(1) + rho*nt*fn_rhou)*ntx/sigma_/ntref_;
    dfdu(iSA,2) -= (dmu(2) + rho*nt*fn_rhov)*ntx/sigma_/ntref_;
    dfdu(iSA,3) -= (dmu(3) + rho*nt*fn_rhow)*ntx/sigma_/ntref_;
    dfdu(iSA,4) -= (dmu(4) + rho*nt*fn_rhoE)*ntx/sigma_/ntref_;
    dfdu(iSA,5) -= ((fn + rho*nt*fn_rhont)*ntx - (mu + rho*nt*fn)*rhox/power<2>(rho))/sigma_;

//    g(iSA) -= (mu + rho*nt*fn)*nty/sigma_;
    dgdu(iSA,0) -= ((dmu(0) + rho*nt*fn_rho )*nty + (mu + rho*nt*fn)*(nt*rhoy - rho*nty)/power<2>(rho))/sigma_/ntref_;
    dgdu(iSA,1) -= (dmu(1) + rho*nt*fn_rhou)*nty/sigma_/ntref_;
    dgdu(iSA,2) -= (dmu(2) + rho*nt*fn_rhov)*nty/sigma_/ntref_;
    dgdu(iSA,3) -= (dmu(3) + rho*nt*fn_rhow)*nty/sigma_/ntref_;
    dgdu(iSA,4) -= (dmu(4) + rho*nt*fn_rhoE)*nty/sigma_/ntref_;
    dgdu(iSA,5) -= ((fn + rho*nt*fn_rhont)*nty - (mu + rho*nt*fn)*rhoy/power<2>(rho))/sigma_;

//    h(iSA) -= (mu + rho*nt*fn)*ntz/sigma_;
    dhdu(iSA,0) -= ((dmu(0) + rho*nt*fn_rho )*ntz + (mu + rho*nt*fn)*(nt*rhoz - rho*ntz)/power<2>(rho))/sigma_/ntref_;
    dhdu(iSA,1) -= (dmu(1) + rho*nt*fn_rhou)*ntz/sigma_/ntref_;
    dhdu(iSA,2) -= (dmu(2) + rho*nt*fn_rhov)*ntz/sigma_/ntref_;
    dhdu(iSA,3) -= (dmu(3) + rho*nt*fn_rhow)*ntz/sigma_/ntref_;
    dhdu(iSA,4) -= (dmu(4) + rho*nt*fn_rhoE)*ntz/sigma_/ntref_;
    dhdu(iSA,5) -= ((fn + rho*nt*fn_rhont)*ntz - (mu + rho*nt*fn)*rhoz/power<2>(rho))/sigma_;

  }
  else
  {
//    f(iSA) -= (mu + rho*nt)*ntx/sigma_;
    dfdu(iSA,0) -= (dmu(0)*ntx + (mu + rho*nt)*(nt*rhox - rho*ntx)/power<2>(rho))/sigma_/ntref_;
    dfdu(iSA,1) -= dmu(1)*ntx/sigma_/ntref_;
    dfdu(iSA,2) -= dmu(2)*ntx/sigma_/ntref_;
    dfdu(iSA,3) -= dmu(3)*ntx/sigma_/ntref_;
    dfdu(iSA,4) -= dmu(4)*ntx/sigma_/ntref_;
    dfdu(iSA,5) -= (ntx - (mu + rho*nt)*rhox/power<2>(rho))/sigma_;

//    g(iSA) -= (mu + rho*nt)*nty/sigma_;
    dgdu(iSA,0) -= (dmu(0)*nty + (mu + rho*nt)*(nt*rhoy - rho*nty)/power<2>(rho))/sigma_/ntref_;
    dgdu(iSA,1) -= dmu(1)*nty/sigma_/ntref_;
    dgdu(iSA,2) -= dmu(2)*nty/sigma_/ntref_;
    dgdu(iSA,3) -= dmu(3)*nty/sigma_/ntref_;
    dgdu(iSA,4) -= dmu(4)*nty/sigma_/ntref_;
    dgdu(iSA,5) -= (nty - (mu + rho*nt)*rhoy/power<2>(rho))/sigma_;

//    h(iSA) -= (mu + rho*nt)*ntz/sigma_;
    dhdu(iSA,0) -= (dmu(0)*ntz + (mu + rho*nt)*(nt*rhoz - rho*ntz)/power<2>(rho))/sigma_/ntref_;
    dhdu(iSA,1) -= dmu(1)*ntz/sigma_/ntref_;
    dhdu(iSA,2) -= dmu(2)*ntz/sigma_/ntref_;
    dhdu(iSA,3) -= dmu(3)*ntz/sigma_/ntref_;
    dhdu(iSA,4) -= dmu(4)*ntz/sigma_/ntref_;
    dhdu(iSA,5) -= (ntz - (mu + rho*nt)*rhoz/power<2>(rho))/sigma_;
  }


  // Reynolds stresses

  Real lambda_mu = -2./3.;
  T divV = ux + vy + wz;

  T tauxx_mu = (2*ux   ) + lambda_mu*divV;
  T tauyy_mu = (2*vy   ) + lambda_mu*divV;
  T tauzz_mu = (2*wz   ) + lambda_mu*divV;
  T tauxy_mu = (uy + vx);
  T tauxz_mu = (uz + wx);
  T tauyz_mu = (vz + wy);

  ArrayQ<T> dmuEddy = 0;
  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Tq fv1 = chi3 / (chi3 + power<3>(cv1_));
    Tq muEddy = rho*nt*fv1;
    Real Cp = this->gas_.Cp();

    // Augment the molecular viscosity and thermal conductivity
    mu += muEddy;
    k  += muEddy*Cp/prandtlEddy_;

    T fv1_chi = -3*power<5,T>(chi)/power<2,T>(power<3>(cv1_) + chi3) + 3*power<2,T>(chi)/(power<3>(cv1_) + chi3);

    T fv1_rho  = fv1_chi*chi_rho;
    T fv1_rhou = fv1_chi*chi_rhou;
    T fv1_rhov = fv1_chi*chi_rhov;
    T fv1_rhow = fv1_chi*chi_rhow;
    T fv1_rhoE = fv1_chi*chi_rhoE;
    T fv1_rhont = fv1_chi*chi_rhont;

    dmuEddy(0) += rho*nt*fv1_rho;
    dmuEddy(1) += rho*nt*fv1_rhou;
    dmuEddy(2) += rho*nt*fv1_rhov;
    dmuEddy(3) += rho*nt*fv1_rhow;
    dmuEddy(4) += rho*nt*fv1_rhoE;
    dmuEddy(5) += rho*nt*fv1_rhont + fv1;

    dk += dmuEddy*Cp/prandtlEddy_;
    dmu += dmuEddy;

    // add QCR terms
#ifdef SANS_SA_QCR
    Tq rhoi = 1/rho;

    Tq  u_rho  = -u*rhoi;
    Tq& u_rhou =   rhoi;
    Tq  v_rho  = -v*rhoi;
    Tq& v_rhov =    rhoi;
    Tq  w_rho  = -w*rhoi;
    Tq& w_rhow =    rhoi;

    Tq lambdaEddy = -2./3. * muEddy;

    T tauRxx = muEddy*(2*ux   ) + lambdaEddy*(ux + vy + wz);
    T tauRyy = muEddy*(2*vy   ) + lambdaEddy*(ux + vy + wz);
    T tauRzz = muEddy*(2*wz   ) + lambdaEddy*(ux + vy + wz);
    T tauRxy = muEddy*(uy + vx);
    T tauRxz = muEddy*(uz + wx);
    T tauRyz = muEddy*(vz + wy);

    Tq rho2 = rho*rho;

    T ux_rho  = (rhox*u - rho*ux)/rho2;
    T ux_rhou = -rhox            /rho2;
    T uy_rho  = (rhoy*u - rho*uy)/rho2;
    T uy_rhou = -rhoy            /rho2;
    T uz_rho  = (rhoz*u - rho*uz)/rho2;
    T uz_rhou = -rhoz            /rho2;

    T vx_rho  = (rhox*v - rho*vx)/rho2;
    T vx_rhov = -rhox            /rho2;
    T vy_rho  = (rhoy*v - rho*vy)/rho2;
    T vy_rhov = -rhoy            /rho2;
    T vz_rho  = (rhoz*v - rho*vz)/rho2;
    T vz_rhov = -rhoz            /rho2;

    T wx_rho  = (rhox*w - rho*wx)/rho2;
    T wx_rhow = -rhox            /rho2;
    T wy_rho  = (rhoy*w - rho*wy)/rho2;
    T wy_rhow = -rhoy            /rho2;
    T wz_rho  = (rhoz*w - rho*wz)/rho2;
    T wz_rhow = -rhoz            /rho2;

    T magGradV = sqrt(ux*ux + uy*uy + uz*uz +
                      vx*vx + vy*vy + vz*vz +
                      wx*wx + wy*wy + wz*wz);
    T magGradV_rho  = magGradV == 0.0 ? T(0) : (ux*ux_rho + uy*uy_rho + uz*uz_rho +
                                                vx*vx_rho + vy*vy_rho + vz*vz_rho +
                                                wx*wx_rho + wy*wy_rho + wz*wz_rho)/magGradV;
    T magGradV_rhou = magGradV == 0.0 ? T(0) : (ux*ux_rhou + uy*uy_rhou + uz*uz_rhou)/magGradV;
    T magGradV_rhov = magGradV == 0.0 ? T(0) : (vx*vx_rhov + vy*vy_rhov + vz*vz_rhov)/magGradV;
    T magGradV_rhow = magGradV == 0.0 ? T(0) : (wx*wx_rhow + wy*wy_rhow + wz*wz_rhow)/magGradV;

    T magGradVi      = (magGradV == 0.0) ? T(0) : 1/magGradV;
    T magGradVi2     =  magGradVi*magGradVi;
    T magGradVi_rho  = -magGradV_rho *magGradVi2;
    T magGradVi_rhou = -magGradV_rhou*magGradVi2;
    T magGradVi_rhov = -magGradV_rhov*magGradVi2;
    T magGradVi_rhow = -magGradV_rhow*magGradVi2;

    T Wxy      = (uy      - vx     );
    T Wxy_rho  = (uy_rho  - vx_rho );
    T Wxy_rhou = (uy_rhou          );
    T Wxy_rhov = (        - vx_rhov);

    T Wxz      = (uz      - wx     );
    T Wxz_rho  = (uz_rho  - wx_rho );
    T Wxz_rhou = (uz_rhou          );
    T Wxz_rhow = (        - wx_rhow);

    T Wyz      = (vz      - wy     );
    T Wyz_rho  = (vz_rho  - wy_rho );
    T Wyz_rhov = (vz_rhov          );
    T Wyz_rhow = (        - wy_rhow);

    T Oxy      = magGradV == 0.0 ? T(0) : Wxy*magGradVi;
    T Oxy_rho  = magGradV == 0.0 ? T(0) : Wxy_rho *magGradVi + Wxy*magGradVi_rho ;
    T Oxy_rhou = magGradV == 0.0 ? T(0) : Wxy_rhou*magGradVi + Wxy*magGradVi_rhou;
    T Oxy_rhov = magGradV == 0.0 ? T(0) : Wxy_rhov*magGradVi + Wxy*magGradVi_rhov;
    T Oxy_rhow = magGradV == 0.0 ? T(0) :                      Wxy*magGradVi_rhow;

    T Oyx      = -Oxy;
    T Oyx_rho  = -Oxy_rho;
    T Oyx_rhou = -Oxy_rhou;
    T Oyx_rhov = -Oxy_rhov;
    T Oyx_rhow = -Oxy_rhow;

    T Oxz      = magGradV == 0.0 ? T(0) : Wxz*magGradVi;
    T Oxz_rho  = magGradV == 0.0 ? T(0) : Wxz_rho *magGradVi + Wxz*magGradVi_rho ;
    T Oxz_rhou = magGradV == 0.0 ? T(0) : Wxz_rhou*magGradVi + Wxz*magGradVi_rhou;
    T Oxz_rhov = magGradV == 0.0 ? T(0) :                      Wxz*magGradVi_rhov;
    T Oxz_rhow = magGradV == 0.0 ? T(0) : Wxz_rhow*magGradVi + Wxz*magGradVi_rhow;

    T Ozx      = -Oxz;
    T Ozx_rho  = -Oxz_rho;
    T Ozx_rhou = -Oxz_rhou;
    T Ozx_rhov = -Oxz_rhov;
    T Ozx_rhow = -Oxz_rhow;

    T Oyz      = magGradV == 0.0 ? T(0) : Wyz*magGradVi;
    T Oyz_rho  = magGradV == 0.0 ? T(0) : Wyz_rho *magGradVi + Wyz*magGradVi_rho ;
    T Oyz_rhou = magGradV == 0.0 ? T(0) :                      Wyz*magGradVi_rhou;
    T Oyz_rhov = magGradV == 0.0 ? T(0) : Wyz_rhov*magGradVi + Wyz*magGradVi_rhov;
    T Oyz_rhow = magGradV == 0.0 ? T(0) : Wyz_rhow*magGradVi + Wyz*magGradVi_rhow;

    T Ozy = -Oyz;
    T Ozy_rho  = -Oyz_rho;
    T Ozy_rhou = -Oyz_rhou;
    T Ozy_rhov = -Oyz_rhov;
    T Ozy_rhow = -Oyz_rhow;

    T tauRxx_rho   = tauxx_mu*dmuEddy(0) + muEddy*( 4./3.*ux_rho  - 2./3.*vy_rho  - 2./3.*wz_rho  );
    T tauRxx_rhou  = tauxx_mu*dmuEddy(1) + muEddy*( 4./3.*ux_rhou                                 );
    T tauRxx_rhov  = tauxx_mu*dmuEddy(2) + muEddy*(               - 2./3.*vy_rhov                 );
    T tauRxx_rhow  = tauxx_mu*dmuEddy(3) + muEddy*(                               - 2./3.*wz_rhow );
    T tauRxx_rhoE  = tauxx_mu*dmuEddy(4);
    T tauRxx_rhont = tauxx_mu*dmuEddy(5);

    T tauRyy_rho   = tauyy_mu*dmuEddy(0) + muEddy*(-2./3.*ux_rho  + 4./3.*vy_rho  - 2./3.*wz_rho  );
    T tauRyy_rhou  = tauyy_mu*dmuEddy(1) + muEddy*(-2./3.*ux_rhou                                 );
    T tauRyy_rhov  = tauyy_mu*dmuEddy(2) + muEddy*(               + 4./3.*vy_rhov                 );
    T tauRyy_rhow  = tauyy_mu*dmuEddy(3) + muEddy*(                               - 2./3.*wz_rhow );
    T tauRyy_rhoE  = tauyy_mu*dmuEddy(4);
    T tauRyy_rhont = tauyy_mu*dmuEddy(5);

    T tauRzz_rho   = tauzz_mu*dmuEddy(0) + muEddy*(-2./3.*ux_rho  - 2./3.*vy_rho  + 4./3.*wz_rho  );
    T tauRzz_rhou  = tauzz_mu*dmuEddy(1) + muEddy*(-2./3.*ux_rhou                                 );
    T tauRzz_rhov  = tauzz_mu*dmuEddy(2) + muEddy*(               - 2./3.*vy_rhov                 );
    T tauRzz_rhow  = tauzz_mu*dmuEddy(3) + muEddy*(                               + 4./3.*wz_rhow );
    T tauRzz_rhoE  = tauzz_mu*dmuEddy(4);
    T tauRzz_rhont = tauzz_mu*dmuEddy(5);

    T tauRxy_rho   = tauxy_mu*dmuEddy(0) + muEddy*(uy_rho + vx_rho);
    T tauRxy_rhou  = tauxy_mu*dmuEddy(1) + muEddy*(uy_rhou        );
    T tauRxy_rhov  = tauxy_mu*dmuEddy(2) + muEddy*(vx_rhov        );
    T tauRxy_rhow  = tauxy_mu*dmuEddy(3);
    T tauRxy_rhoE  = tauxy_mu*dmuEddy(4);
    T tauRxy_rhont = tauxy_mu*dmuEddy(5);

    T tauRxz_rho   = tauxz_mu*dmuEddy(0) + muEddy*(uz_rho + wx_rho);
    T tauRxz_rhou  = tauxz_mu*dmuEddy(1) + muEddy*(uz_rhou        );
    T tauRxz_rhov  = tauxz_mu*dmuEddy(2);
    T tauRxz_rhow  = tauxz_mu*dmuEddy(3) + muEddy*(wx_rhow        );
    T tauRxz_rhoE  = tauxz_mu*dmuEddy(4);
    T tauRxz_rhont = tauxz_mu*dmuEddy(5);

    T tauRyz_rho   = tauyz_mu*dmuEddy(0) + muEddy*(vz_rho + wy_rho);
    T tauRyz_rhou  = tauyz_mu*dmuEddy(1);
    T tauRyz_rhov  = tauyz_mu*dmuEddy(2) + muEddy*(vz_rhov        );
    T tauRyz_rhow  = tauyz_mu*dmuEddy(3) + muEddy*(wy_rhow        );
    T tauRyz_rhoE  = tauyz_mu*dmuEddy(4);
    T tauRyz_rhont = tauyz_mu*dmuEddy(5);

    T qcrxx       = cr1_*2*(Oxy*tauRxy + Oxz*tauRxz);
    T qcrxx_rho   = cr1_*2*(Oxy_rho  *tauRxy + Oxy*tauRxy_rho   + Oxz_rho  *tauRxz + Oxz*tauRxz_rho  );
    T qcrxx_rhou  = cr1_*2*(Oxy_rhou *tauRxy + Oxy*tauRxy_rhou  + Oxz_rhou *tauRxz + Oxz*tauRxz_rhou );
    T qcrxx_rhov  = cr1_*2*(Oxy_rhov *tauRxy + Oxy*tauRxy_rhov  + Oxz_rhov *tauRxz + Oxz*tauRxz_rhov );
    T qcrxx_rhow  = cr1_*2*(Oxy_rhow *tauRxy + Oxy*tauRxy_rhow  + Oxz_rhow *tauRxz + Oxz*tauRxz_rhow );
    T qcrxx_rhoE  = cr1_*2*(                   Oxy*tauRxy_rhoE                     + Oxz*tauRxz_rhoE );
    T qcrxx_rhont = cr1_*2*(                   Oxy*tauRxy_rhont                    + Oxz*tauRxz_rhont);

    T qcrxy       = cr1_*(Oxy*tauRyy + Oxz*tauRyz + Oyx*tauRxx + Oyz*tauRxz);
    T qcrxy_rho   = cr1_*(Oxy_rho  *tauRyy + Oxy*tauRyy_rho   + Oxz_rho  *tauRyz + Oxz*tauRyz_rho   + Oyx_rho  *tauRxx + Oyx*tauRxx_rho   + Oyz_rho  *tauRxz + Oyz*tauRxz_rho  );
    T qcrxy_rhou  = cr1_*(Oxy_rhou *tauRyy + Oxy*tauRyy_rhou  + Oxz_rhou *tauRyz + Oxz*tauRyz_rhou  + Oyx_rhou *tauRxx + Oyx*tauRxx_rhou  + Oyz_rhou *tauRxz + Oyz*tauRxz_rhou );
    T qcrxy_rhov  = cr1_*(Oxy_rhov *tauRyy + Oxy*tauRyy_rhov  + Oxz_rhov *tauRyz + Oxz*tauRyz_rhov  + Oyx_rhov *tauRxx + Oyx*tauRxx_rhov  + Oyz_rhov *tauRxz + Oyz*tauRxz_rhov );
    T qcrxy_rhow  = cr1_*(Oxy_rhow *tauRyy + Oxy*tauRyy_rhow  + Oxz_rhow *tauRyz + Oxz*tauRyz_rhow  + Oyx_rhow *tauRxx + Oyx*tauRxx_rhow  + Oyz_rhow *tauRxz + Oyz*tauRxz_rhow );
    T qcrxy_rhoE  = cr1_*(                   Oxy*tauRyy_rhoE                     + Oxz*tauRyz_rhoE                     + Oyx*tauRxx_rhoE                     + Oyz*tauRxz_rhoE );
    T qcrxy_rhont = cr1_*(                   Oxy*tauRyy_rhont                    + Oxz*tauRyz_rhont                    + Oyx*tauRxx_rhont                    + Oyz*tauRxz_rhont);

    T qcrxz       = cr1_*(Oxy*tauRyz + Oxz*tauRzz + Ozx*tauRxx + Ozy*tauRxy);
    T qcrxz_rho   = cr1_*(Oxy_rho  *tauRyz + Oxy*tauRyz_rho   + Oxz_rho  *tauRzz + Oxz*tauRzz_rho   + Ozx_rho  *tauRxx + Ozx*tauRxx_rho   + Ozy_rho  *tauRxy + Ozy*tauRxy_rho  );
    T qcrxz_rhou  = cr1_*(Oxy_rhou *tauRyz + Oxy*tauRyz_rhou  + Oxz_rhou *tauRzz + Oxz*tauRzz_rhou  + Ozx_rhou *tauRxx + Ozx*tauRxx_rhou  + Ozy_rhou *tauRxy + Ozy*tauRxy_rhou );
    T qcrxz_rhov  = cr1_*(Oxy_rhov *tauRyz + Oxy*tauRyz_rhov  + Oxz_rhov *tauRzz + Oxz*tauRzz_rhov  + Ozx_rhov *tauRxx + Ozx*tauRxx_rhov  + Ozy_rhov *tauRxy + Ozy*tauRxy_rhov );
    T qcrxz_rhow  = cr1_*(Oxy_rhow *tauRyz + Oxy*tauRyz_rhow  + Oxz_rhow *tauRzz + Oxz*tauRzz_rhow  + Ozx_rhow *tauRxx + Ozx*tauRxx_rhow  + Ozy_rhow *tauRxy + Ozy*tauRxy_rhow );
    T qcrxz_rhoE  = cr1_*(                   Oxy*tauRyz_rhoE                     + Oxz*tauRzz_rhoE                     + Ozx*tauRxx_rhoE                     + Ozy*tauRxy_rhoE );
    T qcrxz_rhont = cr1_*(                   Oxy*tauRyz_rhont                    + Oxz*tauRzz_rhont                    + Ozx*tauRxx_rhont                    + Ozy*tauRxy_rhont);

    T qcryy       = cr1_*2*(Oyx*tauRxy + Oyz*tauRyz);
    T qcryy_rho   = cr1_*2*(Oyx_rho  *tauRxy + Oyx*tauRxy_rho   + Oyz_rho  *tauRyz + Oyz*tauRyz_rho  );
    T qcryy_rhou  = cr1_*2*(Oyx_rhou *tauRxy + Oyx*tauRxy_rhou  + Oyz_rhou *tauRyz + Oyz*tauRyz_rhou );
    T qcryy_rhov  = cr1_*2*(Oyx_rhov *tauRxy + Oyx*tauRxy_rhov  + Oyz_rhov *tauRyz + Oyz*tauRyz_rhov );
    T qcryy_rhow  = cr1_*2*(Oyx_rhow *tauRxy + Oyx*tauRxy_rhow  + Oyz_rhow *tauRyz + Oyz*tauRyz_rhow );
    T qcryy_rhoE  = cr1_*2*(                   Oyx*tauRxy_rhoE                     + Oyz*tauRyz_rhoE );
    T qcryy_rhont = cr1_*2*(                   Oyx*tauRxy_rhont                    + Oyz*tauRyz_rhont);

    T qcryz       = cr1_*(Oyx*tauRxz + Oyz*tauRzz + Ozx*tauRxy + Ozy*tauRyy);
    T qcryz_rho   = cr1_*(Oyx_rho  *tauRxz + Oyx*tauRxz_rho   + Oyz_rho  *tauRzz + Oyz*tauRzz_rho   + Ozx_rho  *tauRxy + Ozx*tauRxy_rho   + Ozy_rho  *tauRyy + Ozy*tauRyy_rho  );
    T qcryz_rhou  = cr1_*(Oyx_rhou *tauRxz + Oyx*tauRxz_rhou  + Oyz_rhou *tauRzz + Oyz*tauRzz_rhou  + Ozx_rhou *tauRxy + Ozx*tauRxy_rhou  + Ozy_rhou *tauRyy + Ozy*tauRyy_rhou );
    T qcryz_rhov  = cr1_*(Oyx_rhov *tauRxz + Oyx*tauRxz_rhov  + Oyz_rhov *tauRzz + Oyz*tauRzz_rhov  + Ozx_rhov *tauRxy + Ozx*tauRxy_rhov  + Ozy_rhov *tauRyy + Ozy*tauRyy_rhov );
    T qcryz_rhow  = cr1_*(Oyx_rhow *tauRxz + Oyx*tauRxz_rhow  + Oyz_rhow *tauRzz + Oyz*tauRzz_rhow  + Ozx_rhow *tauRxy + Ozx*tauRxy_rhow  + Ozy_rhow *tauRyy + Ozy*tauRyy_rhow );
    T qcryz_rhoE  = cr1_*(                   Oyx*tauRxz_rhoE                     + Oyz*tauRzz_rhoE                     + Ozx*tauRxy_rhoE                     + Ozy*tauRyy_rhoE );
    T qcryz_rhont = cr1_*(                   Oyx*tauRxz_rhont                    + Oyz*tauRzz_rhont                    + Ozx*tauRxy_rhont                    + Ozy*tauRyy_rhont);

    T qcrzz       = cr1_*2*(Ozx*tauRxz + Ozy*tauRyz);
    T qcrzz_rho   = cr1_*2*(Ozx_rho  *tauRxz + Ozx*tauRxz_rho   + Ozy_rho  *tauRyz + Ozy*tauRyz_rho  );
    T qcrzz_rhou  = cr1_*2*(Ozx_rhou *tauRxz + Ozx*tauRxz_rhou  + Ozy_rhou *tauRyz + Ozy*tauRyz_rhou );
    T qcrzz_rhov  = cr1_*2*(Ozx_rhov *tauRxz + Ozx*tauRxz_rhov  + Ozy_rhov *tauRyz + Ozy*tauRyz_rhov );
    T qcrzz_rhow  = cr1_*2*(Ozx_rhow *tauRxz + Ozx*tauRxz_rhow  + Ozy_rhow *tauRyz + Ozy*tauRyz_rhow );
    T qcrzz_rhoE  = cr1_*2*(                   Ozx*tauRxz_rhoE                     + Ozy*tauRyz_rhoE );
    T qcrzz_rhont = cr1_*2*(                   Ozx*tauRxz_rhont                    + Ozy*tauRyz_rhont);

//    f(ixMom) += qcrxx;
//    f(iyMom) += qcrxy;
//    f(izMom) += qcrxz;
//    f(iEngy) += u*qcrxx + v*qcrxy + w*qcrxz;
//
//    g(ixMom) += qcrxy;
//    g(iyMom) += qcryy;
//    g(izMom) += qcryz;
//    g(iEngy) += u*qcrxy + v*qcryy + w*qcryz;
//
//    h(ixMom) += qcrxz;
//    h(iyMom) += qcryz;
//    h(izMom) += qcrzz;
//    h(iEngy) += u*qcrxz + v*qcryz + w*qcrzz;

    dfdu(ixMom,0) += qcrxx_rho  ;
    dfdu(ixMom,1) += qcrxx_rhou ;
    dfdu(ixMom,2) += qcrxx_rhov ;
    dfdu(ixMom,3) += qcrxx_rhow ;
    dfdu(ixMom,4) += qcrxx_rhoE ;
    dfdu(ixMom,5) += qcrxx_rhont;

    dfdu(iyMom,0) += qcrxy_rho  ;
    dfdu(iyMom,1) += qcrxy_rhou ;
    dfdu(iyMom,2) += qcrxy_rhov ;
    dfdu(iyMom,3) += qcrxy_rhow ;
    dfdu(iyMom,4) += qcrxy_rhoE ;
    dfdu(iyMom,5) += qcrxy_rhont;

    dfdu(izMom,0) += qcrxz_rho  ;
    dfdu(izMom,1) += qcrxz_rhou ;
    dfdu(izMom,2) += qcrxz_rhov ;
    dfdu(izMom,3) += qcrxz_rhow ;
    dfdu(izMom,4) += qcrxz_rhoE ;
    dfdu(izMom,5) += qcrxz_rhont;

    dfdu(iEngy,0) += u_rho *qcrxx + u*qcrxx_rho   + v_rho *qcrxy + v*qcrxy_rho   + w_rho *qcrxz + w*qcrxz_rho  ;
    dfdu(iEngy,1) += u_rhou*qcrxx + u*qcrxx_rhou                 + v*qcrxy_rhou                 + w*qcrxz_rhou ;
    dfdu(iEngy,2) +=                u*qcrxx_rhov  + v_rhov*qcrxy + v*qcrxy_rhov                 + w*qcrxz_rhov ;
    dfdu(iEngy,3) +=                u*qcrxx_rhow                 + v*qcrxy_rhow  + w_rhow*qcrxz + w*qcrxz_rhow ;
    dfdu(iEngy,4) +=                u*qcrxx_rhoE                 + v*qcrxy_rhoE                 + w*qcrxz_rhoE ;
    dfdu(iEngy,5) +=                u*qcrxx_rhont                + v*qcrxy_rhont                + w*qcrxz_rhont;

    dgdu(ixMom,0) += qcrxy_rho  ;
    dgdu(ixMom,1) += qcrxy_rhou ;
    dgdu(ixMom,2) += qcrxy_rhov ;
    dgdu(ixMom,3) += qcrxy_rhow ;
    dgdu(ixMom,4) += qcrxy_rhoE ;
    dgdu(ixMom,5) += qcrxy_rhont;

    dgdu(iyMom,0) += qcryy_rho  ;
    dgdu(iyMom,1) += qcryy_rhou ;
    dgdu(iyMom,2) += qcryy_rhov ;
    dgdu(iyMom,3) += qcryy_rhow ;
    dgdu(iyMom,4) += qcryy_rhoE ;
    dgdu(iyMom,5) += qcryy_rhont;

    dgdu(izMom,0) += qcryz_rho  ;
    dgdu(izMom,1) += qcryz_rhou ;
    dgdu(izMom,2) += qcryz_rhov ;
    dgdu(izMom,3) += qcryz_rhow ;
    dgdu(izMom,4) += qcryz_rhoE ;
    dgdu(izMom,5) += qcryz_rhont;

    dgdu(iEngy,0) += u_rho *qcrxy + u*qcrxy_rho   + v_rho *qcryy + v*qcryy_rho   + w_rho *qcryz + w*qcryz_rho  ;
    dgdu(iEngy,1) += u_rhou*qcrxy + u*qcrxy_rhou                 + v*qcryy_rhou                 + w*qcryz_rhou ;
    dgdu(iEngy,2) +=                u*qcrxy_rhov  + v_rhov*qcryy + v*qcryy_rhov                 + w*qcryz_rhov ;
    dgdu(iEngy,3) +=                u*qcrxy_rhow                 + v*qcryy_rhow  + w_rhow*qcryz + w*qcryz_rhow ;
    dgdu(iEngy,4) +=                u*qcrxy_rhoE                 + v*qcryy_rhoE                 + w*qcryz_rhoE ;
    dgdu(iEngy,5) +=                u*qcrxy_rhont                + v*qcryy_rhont                + w*qcryz_rhont;

    dhdu(ixMom,0) += qcrxz_rho  ;
    dhdu(ixMom,1) += qcrxz_rhou ;
    dhdu(ixMom,2) += qcrxz_rhov ;
    dhdu(ixMom,3) += qcrxz_rhow ;
    dhdu(ixMom,4) += qcrxz_rhoE ;
    dhdu(ixMom,5) += qcrxz_rhont;

    dhdu(iyMom,0) += qcryz_rho  ;
    dhdu(iyMom,1) += qcryz_rhou ;
    dhdu(iyMom,2) += qcryz_rhov ;
    dhdu(iyMom,3) += qcryz_rhow ;
    dhdu(iyMom,4) += qcryz_rhoE ;
    dhdu(iyMom,5) += qcryz_rhont;

    dhdu(izMom,0) += qcrzz_rho  ;
    dhdu(izMom,1) += qcrzz_rhou ;
    dhdu(izMom,2) += qcrzz_rhov ;
    dhdu(izMom,3) += qcrzz_rhow ;
    dhdu(izMom,4) += qcrzz_rhoE ;
    dhdu(izMom,5) += qcrzz_rhont;

    dhdu(iEngy,0) += u_rho *qcrxz + u*qcrxz_rho   + v_rho *qcryz + v*qcryz_rho   + w_rho *qcrzz + w*qcrzz_rho  ;
    dhdu(iEngy,1) += u_rhou*qcrxz + u*qcrxz_rhou                 + v*qcryz_rhou                 + w*qcrzz_rhou ;
    dhdu(iEngy,2) +=                u*qcrxz_rhov  + v_rhov*qcryz + v*qcryz_rhov                 + w*qcrzz_rhov ;
    dhdu(iEngy,3) +=                u*qcrxz_rhow                 + v*qcryz_rhow  + w_rhow*qcrzz + w*qcrzz_rhow ;
    dhdu(iEngy,4) +=                u*qcrxz_rhoE                 + v*qcryz_rhoE                 + w*qcrzz_rhoE ;
    dhdu(iEngy,5) +=                u*qcrxz_rhont                + v*qcryz_rhont                + w*qcrzz_rhont;
#endif
  }

  // Compute the jacobian viscous flux for NS

  BaseType::jacobianFluxViscous(mu, k, dmu, dk, q, qx, qy, qz, dfdu, dgdu, dhdu);

  // Finish jacobian

  // symmetric parts because I think it makes the code look cleaner
  T& tauyx_mu = tauxy_mu;
  T& tauzx_mu = tauxz_mu;
  T& tauzy_mu = tauyz_mu;

  Tf tauxx_rhont = tauxx_mu*dmu(5);
  Tf tauxy_rhont = tauxy_mu*dmu(5);
  Tf tauxz_rhont = tauxz_mu*dmu(5);

  Tf tauyx_rhont = tauyx_mu*dmu(5);
  Tf tauyy_rhont = tauyy_mu*dmu(5);
  Tf tauyz_rhont = tauyz_mu*dmu(5);

  Tf tauzx_rhont = tauzx_mu*dmu(5);
  Tf tauzy_rhont = tauzy_mu*dmu(5);
  Tf tauzz_rhont = tauzz_mu*dmu(5);

  //  f(ixMom) -= tauxx;
  //  f(iyMom) -= tauxy;
  //  f(izMom) -= tauxz;
  //  f(iEngy) -= k*tx + u*tauxx + v*tauxy + w*tauxz;
  dfdu(ixMom,5) -= tauxx_rhont*ntref_;
  dfdu(iyMom,5) -= tauxy_rhont*ntref_;
  dfdu(izMom,5) -= tauxz_rhont*ntref_;
  dfdu(iEngy,5) -= (tx*dk(5) + u*tauxx_rhont + v*tauxy_rhont + w*tauxz_rhont)*ntref_;

  //  g(ixMom) -= tauyx;
  //  g(iyMom) -= tauyy;
  //  g(izMom) -= tauyz;
  //  g(iEngy) -= k*ty + u*tauyx + v*tauyy + w*tauyz;
  dgdu(ixMom,5) -= tauyx_rhont*ntref_;
  dgdu(iyMom,5) -= tauyy_rhont*ntref_;
  dgdu(izMom,5) -= tauyz_rhont*ntref_;
  dgdu(iEngy,5) -= (ty*dk(5) + u*tauyx_rhont + v*tauyy_rhont + w*tauyz_rhont)*ntref_;

  //
  //  h(ixMom) -= tauzx;
  //  h(iyMom) -= tauzy;
  //  h(izMom) -= tauzz;
  //  h(iEngy) -= k*tz + u*tauzx + v*tauzy + w*tauzz;
  dhdu(ixMom,5) -= tauzx_rhont*ntref_;
  dhdu(iyMom,5) -= tauzy_rhont*ntref_;
  dhdu(izMom,5) -= tauzz_rhont*ntref_;
  dhdu(iEngy,5) -= (tz*dk(5) + u*tauzx_rhont + v*tauzy_rhont + w*tauzz_rhont)*ntref_;
}


// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq rho=0, t=0, nt=0;
  Tqg rhox = 0, tx = 0, ntx = 0;
  Tqg rhoy = 0, ty = 0, nty = 0;
  Tqg rhoz = 0, tz = 0, ntz = 0;
  T ntxx = 0, ntyy = 0, ntzz = 0;

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalTemperature( q, t );
  qInterpret_.evalSA( q, nt );

#if 0   // density, temperature gradient not yet implemented
  qInterpret_.evalDensityGradient( q, qx, rhox );
  qInterpret_.evalDensityGradient( q, qy, rhoy );
  qInterpret_.evalDensityGradient( q, qz, rhoz );

  qInterpret_.evalTemperatureGradient( q, qx, tx );
  qInterpret_.evalTemperatureGradient( q, qy, ty );
  qInterpret_.evalTemperatureGradient( q, qz, tz );
#else
  Tqg ux, vx, wx;
  Tqg uy, vy, wy;
  Tqg uz, vz, wz;
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );
#endif

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  qInterpret_.evalSAHessian( q, qx, qx, qxx, ntxx );
  qInterpret_.evalSAHessian( q, qy, qy, qyy, ntyy );
  qInterpret_.evalSAHessian( q, qz, qz, qzz, ntzz );

  Tq mu_t = 0;
  visc_.viscosityJacobian( t, mu_t );

  Tq  mu  = visc_.viscosity( t );
  Tqg mux = mu_t*tx;
  Tqg muy = mu_t*ty;
  Tqg muz = mu_t*tz;

  Tq k_mu = 0;
  tcond_.conductivityJacobian( mu, k_mu );

  Tq  k  = tcond_.conductivity( mu );
  Tqg kx = k_mu*mux;
  Tqg ky = k_mu*muy;
  Tqg kz = k_mu*muz;

  Tq chi = rho*nt/mu;
  Tq chi3 = power<3,Tq>(chi);
  Tqg chix = rhox*nt/mu + rho*ntx/mu - rho*nt*mux/(mu*mu);
  Tqg chiy = rhoy*nt/mu + rho*nty/mu - rho*nt*muy/(mu*mu);
  Tqg chiz = rhoz*nt/mu + rho*ntz/mu - rho*nt*muz/(mu*mu);

  if (nt < 0)
  {
    Tq  fn  = (cn1_ + chi3) / (cn1_ - chi3);
    Tq  fn_chi = 6*cn1_*(chi*chi) / power<2,Tq>(cn1_ - chi3);
    Tqg fnx = fn_chi*chix;
    Tqg fny = fn_chi*chiy;
    Tqg fnz = fn_chi*chiz;
    strongPDE(iSA) -= ((mu + rho*nt*fn)*(ntxx + ntyy + ntzz)
                    + (mux + rhox*nt*fn + rho*ntx*fn + rho*nt*fnx)*ntx
                    + (muy + rhoy*nt*fn + rho*nty*fn + rho*nt*fny)*nty
                    + (muz + rhoz*nt*fn + rho*ntz*fn + rho*nt*fnz)*ntz)/sigma_/ntref_;
  }
  else
  {
    strongPDE(iSA) -= ((mu + rho*nt)*(ntxx + ntyy + ntzz)
                    + (mux + rhox*nt + rho*ntx)*ntx
                    + (muy + rhoy*nt + rho*nty)*nty
                    + (muz + rhoz*nt + rho*ntz)*ntz)/sigma_/ntref_;
  }

  // molecular and Reynolds stresses

  if (nt < 0)
  {
    // negative-SA has zero Reynolds stresses
  }
  else
  {
    Real cv13 = power<3>(cv1_);
    Tq fv1 = chi3 / (chi3 + cv13);
    Tq muEddy = rho*nt*fv1;

    Tq fv1_chi = 3*cv13*chi*chi / power<2,Tq>(cv13 + chi3);

    Tqg fv1x = fv1_chi*chix;
    Tqg fv1y = fv1_chi*chiy;
    Tqg fv1z = fv1_chi*chiz;

    Tqg muEddyx = rhox*nt*fv1 + rho*ntx*fv1 + rho*nt*fv1x;
    Tqg muEddyy = rhoy*nt*fv1 + rho*nty*fv1 + rho*nt*fv1y;
    Tqg muEddyz = rhoz*nt*fv1 + rho*ntz*fv1 + rho*nt*fv1z;

    mu  += muEddy;
    mux += muEddyx;
    muy += muEddyy;
    muz += muEddyz;

    Real Cp = this->gas_.Cp();
    Tq k_muEddy = Cp/prandtlEddy_;

    k  += muEddy*Cp/prandtlEddy_;
    kx += k_muEddy*muEddyx;
    ky += k_muEddy*muEddyy;
    kz += k_muEddy*muEddyz;
  }

  BaseType::strongFluxViscous( mu, mux, muy, muz, k, kx, ky, kz, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongPDE );
}


// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::source(
    const Real& dist,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Ts>& source ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  const bool verbose = false;

  Tq rho = 0, t = 0;
  T  rhox, rhoy, rhoz, ux, uy, uz, vx, vy, vz, wx, wy, wz, tx, ty, tz;
  Tq nt;                                // nu_tilde
  T  ntx, nty, ntz;
  Tq mu;                                // molecular viscosity
  Tq chi, chi3;
  T  fv2, ft2;
  T  shr0;                              // shear rate: vorticity
  T  shrmod;                            // shear rate: modified
  T  r, g, fw;                          // wall terms
  T  prod, wall, nonc;                  // production, destruction, non-conservative diffusion terms

  qInterpret_.evalDensity( q, rho );
  qInterpret_.evalTemperature( q, t );
  qInterpret_.evalSA( q, nt );
  mu  = visc_.viscosity( t );
  chi = rho*nt/mu;

  chi3 = power<3,Tq>(chi);

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  // production and near-wall destruction terms

  // shear rate: vorticity
  shr0 = sqrt((uy - vx)*(uy - vx) + (uz - wx)*(uz - wx) + (vz - wy)*(vz - wy));

  // production term (vorticity component only)

  if (nt < 0)
  {
    prod = cb1_*(1 - ct3_)*shr0*nt;
    if (verbose) std::cout << "prodN = " << rho*prod << std::endl;
  }
  else
  {
    prod = cb1_*shr0*nt;
    if (verbose) std::cout << "prod1 = " << rho*prod << std::endl;
  }
  source(iSA) += -rho*prod/ntref_;

  // near-wall destruction term (includes modified production component)

  if (nt < 0)
  {
    wall = -cw1_*power<2,Tq>(nt/dist);
    if (verbose) std::cout << "wallN = " << -rho*wall << std::endl;
  }
  else
  {
    // modified shear rate in viscous sublayer
    //  NOTE: fv2 rewritten for better accuracy
    fv2    = (chi3 + (1 - chi)*power<3>(cv1_)) / ((1 + chi)*chi3 + power<3>(cv1_));
    shrmod = fv2 * nt/(power<2>(vk_*dist));

    if (shrmod < -cv2_*shr0)
    {
      shrmod = shr0 * (cv2_*cv2_*shr0 + cv3_*shrmod)/((cv3_ - 2*cv2_)*shr0 - shrmod);
      if (verbose) std::cout << "source: S~ branch 2" << std::endl;
    }

    if (shr0 + shrmod == 0)
      r = rlim_;
    else
      r = min( rlim_, nt / ((shr0 + shrmod) * power<2>(vk_*dist)) );

    g  = r + cw2_*(power<6,T>(r) - r);
    fw = g * pow((1 + power<6>(cw3_)) / (power<6,T>(g) + power<6>(cw3_)), 1./6.);

    ft2  = ct3_*exp( -ct4_*chi*chi );
    wall = (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*power<2,Tq>(nt/dist)
           + cb1_*ft2*shr0*nt - cb1_*(1 - ft2)*shrmod*nt;

    if (verbose) std::cout << "prod2 = " << -rho*cb1_*ft2*shr0*nt << std::endl;
    if (verbose) std::cout << "prod3 = " <<  rho*cb1_*(1 - ft2)*shrmod*nt << std::endl;
    if (verbose) std::cout << "wall1 = " << -rho*cw1_*fw*power<2,Tq>(nt/dist) << std::endl;
    if (verbose) std::cout << "wall2 = " <<  rho*(cb1_/(vk_*vk_))*ft2*power<2,Tq>(nt/dist) << std::endl;
  }
  source(iSA) += rho*wall/ntref_;

  // non-conservative diffusion terms

  nonc = cb2_*rho*(ntx*ntx + nty*nty + ntz*ntz)/sigma_;

  if (verbose) std::cout << "nonc1 = " << nonc << std::endl;

  if (nt < 0)
  {
    Tq fn = (cn1_ + chi3) / (cn1_ - chi3);
    nonc += - (mu/rho + nt*fn)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;
    if (verbose) std::cout << "nonc2 = " << - (mu/rho + nt*fn)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_ << std::endl;
  }
  else
  {
    nonc += - (mu/rho + nt)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;
    if (verbose) std::cout << "nonc2 = " << - (mu/rho + nt)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_ << std::endl;
  }
  source(iSA) += -nonc/ntref_;

  // NS source terms (nominally empty function)
  BaseType::source( x, y, z, time, q, qx, qy, qz, source );
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianSourceRow(
    const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Ts>& dsdu ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq rho, u, v, w, t;
  Tq t_rho, t_rhou, t_rhov, t_rhow, t_rhoE;
  T  rhox, rhoy, rhoz, ux, uy, uz, vx, vy, vz, wx, wy, wz, tx, ty, tz;
  Tq nt;                                // nu_tilde
  T  ntx, nty, ntz;
  Tq mu, mu_t;                          // molecular viscosity
  Tq mu_rho, mu_rhou, mu_rhov, mu_rhow, mu_rhoE;
  Tq chi;
  Tq chi_mu, chi_rho, chi_rhou, chi_rhov, chi_rhow, chi_rhoE, chi_rhont;
  Tq fv2, ft2;
  T  shr0;                              // shear rate: vorticity
  T  shr0_rho, shr0_rhou, shr0_rhov, shr0_rhow;
  T  shrmod;                            // shear rate: modified
  T  r, g, fw;                          // wall terms
  T  prod, wall, nonc;                  // production, destruction, non-conservative diffusion terms
  T  prod_rho, prod_rhou, prod_rhov, prod_rhow, prod_rhont;
  T  wall_rho, wall_rhou=0, wall_rhov=0, wall_rhow=0, wall_rhoE=0, wall_rhont;
  T  nonc_rho, nonc_rhou=0, nonc_rhov=0, nonc_rhow=0, nonc_rhoE=0, nonc_rhont;

  Real Cv = gas_.Cv();

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalSA( q, nt );

  Tq E = gas_.energy(rho, t) + 0.5*(u*u + v*v + w*w);

  // Temperature from conservative variables
  //t = ( rhoE/rho - 0.5*( rhou*rhou + rhov*rhov )/(rho*rho) ) / Cv
  t_rho  = ( -E/rho + ( u*u + v*v + w*w )/rho ) / Cv;
  t_rhou = (        - (   u             )/rho ) / Cv;
  t_rhov = (        - (         v       )/rho ) / Cv;
  t_rhow = (        - (               w )/rho ) / Cv;
  t_rhoE = (  1/rho                           ) / Cv;

  mu  = visc_.viscosity( t );
  visc_.viscosityJacobian( t, mu_t );

  // viscosity jacobian wrt conservative variables
  mu_rho  = mu_t*t_rho;
  mu_rhou = mu_t*t_rhou;
  mu_rhov = mu_t*t_rhov;
  mu_rhow = mu_t*t_rhow;
  mu_rhoE = mu_t*t_rhoE;

  chi       = rho*nt/mu;

  chi_mu    = -rho*nt/(mu*mu);
  chi_rho   = chi_mu*mu_rho;
  chi_rhou  = chi_mu*mu_rhou;
  chi_rhov  = chi_mu*mu_rhov;
  chi_rhow  = chi_mu*mu_rhow;
  chi_rhoE  = chi_mu*mu_rhoE;
  chi_rhont = 1./mu;

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  // velocity gradient Jacobian wrt conservative variables
  T uy_rho  = -uy/rho + u*rhoy/(rho*rho);
  T uy_rhou =         -   rhoy/(rho*rho);

  T uz_rho  = -uz/rho + u*rhoz/(rho*rho);
  T uz_rhou =         -   rhoz/(rho*rho);

  T vx_rho  = -vx/rho + v*rhox/(rho*rho);
  T vx_rhov =         -   rhox/(rho*rho);

  T vz_rho  = -vz/rho + v*rhoz/(rho*rho);
  T vz_rhov =         -   rhoz/(rho*rho);

  T wx_rho  = -wx/rho + w*rhox/(rho*rho);
  T wx_rhow =         -   rhox/(rho*rho);

  T wy_rho  = -wy/rho + w*rhoy/(rho*rho);
  T wy_rhow =         -   rhoy/(rho*rho);

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  // SA working variable gradient Jacobian wrt conservative variables
  // ntx = rhontx/rho - rhont*rhox/(rho*rho);
  T ntx_rho   = -ntx/rho + nt*rhox/(rho*rho);
  T ntx_rhont =          -    rhox/(rho*rho);

  T nty_rho   = -nty/rho + nt*rhoy/(rho*rho);
  T nty_rhont =          -    rhoy/(rho*rho);

  T ntz_rho   = -ntz/rho + nt*rhoz/(rho*rho);
  T ntz_rhont =          -    rhoz/(rho*rho);

  // production and near-wall destruction terms

  // shear rate: vorticity
  shr0 = sqrt((uy - vx)*(uy - vx) + (uz - wx)*(uz - wx) + (vz - wy)*(vz - wy));

  if (shr0 == 0)
  {
    shr0_rho  = 0;
    shr0_rhou = 0;
    shr0_rhov = 0;
    shr0_rhow = 0;
  }
  else
  {
    shr0_rho  = ((uy - vx)*(uy_rho  - vx_rho ) + (uz - wx)*(uz_rho  - wx_rho ) + (vz - wy)*(vz_rho  - wy_rho )) / shr0;
    shr0_rhou = ((uy - vx)*(uy_rhou          ) + (uz - wx)*(uz_rhou          )                                ) / shr0;
    shr0_rhov = ((uy - vx)*(        - vx_rhov)                                 + (vz - wy)*(vz_rhov          )) / shr0;
    shr0_rhow = (                                (uz - wx)*(        - wx_rhow) + (vz - wy)*(        - wy_rhow)) / shr0;
  }

  // production term (vorticity component only)

  if (nt < 0)
  {
    prod       = cb1_*(1 - ct3_)*shr0*nt;
    prod_rho   = cb1_*(1 - ct3_)*shr0_rho*nt - cb1_*(1 - ct3_)*shr0*nt/rho;
    prod_rhou  = cb1_*(1 - ct3_)*shr0_rhou*nt;
    prod_rhov  = cb1_*(1 - ct3_)*shr0_rhov*nt;
    prod_rhow  = cb1_*(1 - ct3_)*shr0_rhow*nt;
    prod_rhont = cb1_*(1 - ct3_)*shr0/rho;
  }
  else
  {
    prod       = cb1_*shr0*nt;
    prod_rho   = cb1_*shr0_rho*nt - cb1_*shr0*nt/rho;
    prod_rhou  = cb1_*shr0_rhou*nt;
    prod_rhov  = cb1_*shr0_rhov*nt;
    prod_rhow  = cb1_*shr0_rhow*nt;
    prod_rhont = cb1_*shr0/rho;
  }
  //source(iSA) += -rho*prod;
  dsdu[0] += (-prod + -rho*prod_rho)/ntref_;
  dsdu[1] += -rho*prod_rhou/ntref_;
  dsdu[2] += -rho*prod_rhov/ntref_;
  dsdu[3] += -rho*prod_rhow/ntref_;
  dsdu[5] += -rho*prod_rhont;

  // near-wall destruction term (includes modified production component)

  Tq chi2 = power<2>(chi);
  Tq chi3 = power<3>(chi);

  if (dist < 1e-12)
  {
    wall       = -cw1_;
    wall_rho   = 0;
    wall_rhont = 0;
  }
  else if (nt < 0)
  {
    wall       = -cw1_*power<2,Tq>(nt/dist);
    wall_rho   =  cw1_*2*power<2,Tq>(nt/dist)/rho;
    wall_rhont = -cw1_*2*nt/rho/power<2>(dist);
  }
  else
  {
    Real cv13 = power<3>(cv1_);

    // modified shear rate in viscous sublayer
    //  NOTE: fv2 rewritten for better accuracy
    fv2     = (chi3 + (1 - chi)*cv13) / ((1 + chi)*chi3 + cv13);

    Tq fv2_chi = (3*chi2 - cv13) / ((1 + chi)*chi3 + cv13)
               - (chi3 + 3*chi2*(1 + chi))*(chi3 + (1 - chi)*cv13) / power<2,Tq>((1 + chi)*chi3 + cv13);

    Tq fv2_rho   = fv2_chi*chi_rho;
    Tq fv2_rhou  = fv2_chi*chi_rhou;
    Tq fv2_rhov  = fv2_chi*chi_rhov;
    Tq fv2_rhow  = fv2_chi*chi_rhow;
    Tq fv2_rhoE  = fv2_chi*chi_rhoE;
    Tq fv2_rhont = fv2_chi*chi_rhont;

    shrmod = fv2 * nt/(power<2>(vk_*dist));

    Tq tmp = nt/(power<2>(vk_*dist));

    T shrmod_rho   = fv2_rho * tmp - fv2/rho * tmp;
    T shrmod_rhou  = fv2_rhou * tmp;
    T shrmod_rhov  = fv2_rhov * tmp;
    T shrmod_rhow  = fv2_rhow * tmp;
    T shrmod_rhoE  = fv2_rhoE * tmp;
    T shrmod_rhont = fv2_rhont * tmp + fv2/rho/(power<2>(vk_*dist));

    if (shrmod < -cv2_*shr0)
    {
      T denom = ((cv3_ - 2*cv2_)*shr0 - shrmod);
      T tmp = (cv2_*cv2_*shr0 + cv3_*shrmod)/denom;

      T tmp_rho   = (cv2_*cv2_*shr0_rho  + cv3_*shrmod_rho  )/denom - ((cv3_ - 2*cv2_)*shr0_rho  - shrmod_rho  )*tmp/denom;
      T tmp_rhou  = (cv2_*cv2_*shr0_rhou + cv3_*shrmod_rhou )/denom - ((cv3_ - 2*cv2_)*shr0_rhou - shrmod_rhou )*tmp/denom;
      T tmp_rhov  = (cv2_*cv2_*shr0_rhov + cv3_*shrmod_rhov )/denom - ((cv3_ - 2*cv2_)*shr0_rhov - shrmod_rhov )*tmp/denom;
      T tmp_rhow  = (cv2_*cv2_*shr0_rhow + cv3_*shrmod_rhow )/denom - ((cv3_ - 2*cv2_)*shr0_rhow - shrmod_rhow )*tmp/denom;
      T tmp_rhoE  = (                      cv3_*shrmod_rhoE )/denom - (                          - shrmod_rhoE )*tmp/denom;
      T tmp_rhont = (                      cv3_*shrmod_rhont)/denom - (                          - shrmod_rhont)*tmp/denom;

      shrmod       = shr0 * tmp;

      shrmod_rho   = shr0_rho   * tmp + shr0 * tmp_rho;
      shrmod_rhou  = shr0_rhou  * tmp + shr0 * tmp_rhou;
      shrmod_rhov  = shr0_rhov  * tmp + shr0 * tmp_rhov;
      shrmod_rhow  = shr0_rhow  * tmp + shr0 * tmp_rhow;
      shrmod_rhoE  =                    shr0 * tmp_rhoE;
      shrmod_rhont =                    shr0 * tmp_rhont;
    }

    T r_rho   = 0;
    T r_rhou  = 0;
    T r_rhov  = 0;
    T r_rhow  = 0;
    T r_rhoE  = 0;
    T r_rhont = 0;

    if (shr0 + shrmod == 0)
      r = rlim_;
    else
    {
      r = nt / ((shr0 + shrmod) * power<2>(vk_*dist));

      if ( r >= rlim_ )
        r = rlim_;
      else
      {
        T tmp = -nt / (power<2,T>(shr0 + shrmod) * power<2>(vk_*dist));
        r_rho   = (shr0_rho   + shrmod_rho  ) * tmp - nt/rho/((shr0 + shrmod) * power<2>(vk_*dist));
        r_rhou  = (shr0_rhou  + shrmod_rhou ) * tmp;
        r_rhov  = (shr0_rhov  + shrmod_rhov ) * tmp;
        r_rhow  = (shr0_rhow  + shrmod_rhow ) * tmp;
        r_rhoE  = (             shrmod_rhoE ) * tmp;
        r_rhont = (             shrmod_rhont) * tmp + 1./rho/((shr0 + shrmod) * power<2>(vk_*dist));
      }
    }

    g  = r + cw2_*(power<6,T>(r) - r);

    T g_r     = 1 + cw2_*(6*power<5,T>(r) - 1);
    T g_rho   = g_r*r_rho;
    T g_rhou  = g_r*r_rhou;
    T g_rhov  = g_r*r_rhov;
    T g_rhow  = g_r*r_rhow;
    T g_rhoE  = g_r*r_rhoE;
    T g_rhont = g_r*r_rhont;

    Real cw36 = power<6>(cw3_);
    fw = g * pow((1 + cw36) / (power<6>(g) + cw36), 1./6.);

    T fw_g     = cw36/(1+cw36) * pow((1 + cw36) / (power<6>(g) + cw36), 7./6.);
    T fw_rho   = fw_g*g_rho;
    T fw_rhou  = fw_g*g_rhou;
    T fw_rhov  = fw_g*g_rhov;
    T fw_rhow  = fw_g*g_rhow;
    T fw_rhoE  = fw_g*g_rhoE;
    T fw_rhont = fw_g*g_rhont;

    ft2  = ct3_*exp( -ct4_*chi*chi );

    tmp = -ct4_*2*chi*ft2;
    T ft2_rho   = tmp*chi_rho;
    T ft2_rhou  = tmp*chi_rhou;
    T ft2_rhov  = tmp*chi_rhov;
    T ft2_rhow  = tmp*chi_rhow;
    T ft2_rhoE  = tmp*chi_rhoE;
    T ft2_rhont = tmp*chi_rhont;

    wall = (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*power<2,Tq>(nt/dist)
           + cb1_*ft2*shr0*nt
           - cb1_*(1 - ft2)*shrmod*nt;

    wall_rho = (cw1_*fw_rho - (cb1_/(vk_*vk_))*ft2_rho)*power<2,Tq>(nt/dist) - (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*2*power<2,T>(nt/dist)/rho
             + cb1_*ft2_rho*shr0*nt + cb1_*ft2*shr0_rho*nt - cb1_*ft2*shr0*nt/rho
             + cb1_*ft2_rho*shrmod*nt - cb1_*(1 - ft2)*shrmod_rho*nt + cb1_*(1 - ft2)*shrmod*nt/rho;

    wall_rhou = (cw1_*fw_rhou - (cb1_/(vk_*vk_))*ft2_rhou)*power<2,Tq>(nt/dist)
              + cb1_*ft2_rhou*shr0*nt + cb1_*ft2*shr0_rhou*nt
              + cb1_*ft2_rhou*shrmod*nt - cb1_*(1 - ft2)*shrmod_rhou*nt;

    wall_rhov = (cw1_*fw_rhov - (cb1_/(vk_*vk_))*ft2_rhov)*power<2,Tq>(nt/dist)
              + cb1_*ft2_rhov*shr0*nt + cb1_*ft2*shr0_rhov*nt
              + cb1_*ft2_rhov*shrmod*nt - cb1_*(1 - ft2)*shrmod_rhov*nt;

    wall_rhow = (cw1_*fw_rhow - (cb1_/(vk_*vk_))*ft2_rhow)*power<2,Tq>(nt/dist)
              + cb1_*ft2_rhow*shr0*nt + cb1_*ft2*shr0_rhow*nt
              + cb1_*ft2_rhow*shrmod*nt - cb1_*(1 - ft2)*shrmod_rhow*nt;

    wall_rhoE = (cw1_*fw_rhoE - (cb1_/(vk_*vk_))*ft2_rhoE)*power<2,Tq>(nt/dist)
              + cb1_*ft2_rhoE*shr0*nt
              + cb1_*ft2_rhoE*shrmod*nt - cb1_*(1 - ft2)*shrmod_rhoE*nt;

    wall_rhont = (cw1_*fw_rhont - (cb1_/(vk_*vk_))*ft2_rhont)*power<2,Tq>(nt/dist) + (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*2*nt/power<2>(dist)/rho
               + cb1_*ft2_rhont*shr0*nt + cb1_*ft2*shr0/rho
               + cb1_*ft2_rhont*shrmod*nt - cb1_*(1 - ft2)*shrmod_rhont*nt - cb1_*(1 - ft2)*shrmod/rho;
  }
  //source(iSA) += rho*wall;
  dsdu[0] += (wall + rho*wall_rho)/ntref_;
  dsdu[1] += rho*wall_rhou/ntref_;
  dsdu[2] += rho*wall_rhov/ntref_;
  dsdu[3] += rho*wall_rhow/ntref_;
  dsdu[4] += rho*wall_rhoE/ntref_;
  dsdu[5] += rho*wall_rhont;

  // non-conservative diffusion terms

  nonc       = cb2_*rho*(ntx*ntx + nty*nty + ntz*ntz)/sigma_;
  nonc_rho   = cb2_*(ntx*ntx + nty*nty + ntz*ntz)/sigma_ + cb2_*rho*2*(ntx_rho*ntx + nty_rho*nty + ntz_rho*ntz)/sigma_;
  nonc_rhont = cb2_*rho*2*(ntx_rhont*ntx + nty_rhont*nty + ntz_rhont*ntz)/sigma_;

  if (nt < 0)
  {
    Tq fn = (cn1_ + chi3) / (cn1_ - chi3);

    Tq fn_chi = 3*chi2 / (cn1_ - chi3) + 3*chi2*(cn1_ + chi3) / power<2,Tq>(cn1_ - chi3);

    T fn_rho   = fn_chi*chi_rho;
    T fn_rhou  = fn_chi*chi_rhou;
    T fn_rhov  = fn_chi*chi_rhov;
    T fn_rhow  = fn_chi*chi_rhow;
    T fn_rhoE  = fn_chi*chi_rhoE;
    T fn_rhont = fn_chi*chi_rhont;

    T tmp = (rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

    nonc += - (mu/rho + nt*fn)*tmp;

    nonc_rho   += - (mu_rho/rho - mu/(rho*rho) - nt*fn/rho + nt*fn_rho  )*tmp;
    nonc_rhou  += - (mu_rhou/rho                           + nt*fn_rhou )*tmp;
    nonc_rhov  += - (mu_rhov/rho                           + nt*fn_rhov )*tmp;
    nonc_rhow  += - (mu_rhow/rho                           + nt*fn_rhow )*tmp;
    nonc_rhoE  += - (mu_rhoE/rho                           + nt*fn_rhoE )*tmp;
    nonc_rhont += - (                               fn/rho + nt*fn_rhont)*tmp;

    nonc_rho   += - (mu/rho + nt*fn)*(rhox*ntx_rho   + rhoy*nty_rho   + rhoz*ntz_rho  )/sigma_;
    nonc_rhont += - (mu/rho + nt*fn)*(rhox*ntx_rhont + rhoy*nty_rhont + rhoz*ntz_rhont)/sigma_;
  }
  else
  {
    T tmp = (rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

    nonc       += - (mu/rho + nt)*tmp;

    nonc_rho   += - (mu_rho/rho - mu/(rho*rho) - nt/rho)*tmp;
    nonc_rhou  += - (mu_rhou/rho                       )*tmp;
    nonc_rhov  += - (mu_rhov/rho                       )*tmp;
    nonc_rhow  += - (mu_rhow/rho                       )*tmp;
    nonc_rhoE  += - (mu_rhoE/rho                       )*tmp;
    nonc_rhont += - (                             1/rho)*tmp;

    nonc_rho   += - (mu/rho + nt)*(rhox*ntx_rho   + rhoy*nty_rho   + rhoz*ntz_rho  )/sigma_;
    nonc_rhont += - (mu/rho + nt)*(rhox*ntx_rhont + rhoy*nty_rhont + rhoz*ntz_rhont)/sigma_;
  }

  //source(iSA) += -nonc;
  dsdu[0] += -nonc_rho/ntref_;
  dsdu[1] += -nonc_rhou/ntref_;
  dsdu[2] += -nonc_rhov/ntref_;
  dsdu[3] += -nonc_rhow/ntref_;
  dsdu[4] += -nonc_rhoE/ntref_;
  dsdu[5] += -nonc_rhont;
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianSource(
    const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdu ) const
{
  ArrayQ<Ts> dsdu_row = 0;

  jacobianSourceRow(dist, x, y, z, time, q, qx, qy, qz, dsdu_row);

  dsdu(iSA,0) += dsdu_row[0];
  dsdu(iSA,1) += dsdu_row[1];
  dsdu(iSA,2) += dsdu_row[2];
  dsdu(iSA,3) += dsdu_row[3];
  dsdu(iSA,4) += dsdu_row[4];
  dsdu(iSA,5) += dsdu_row[5];

  // NS source terms (nominally empty function)
  BaseType::jacobianSource( x, y, z, time, q, qx, qy, qz, dsdu );
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianHelperHACK(const Real& x, const Real& y, const Real& z, const Real& time,
                        const ArrayQ<Tq>& q, const MatrixQ<Ts>& mat, MatrixQ<Ts>& matHACK) const
{
  Ts s0 = mat(iSA,0);
  Ts s1 = mat(iSA,1);
  Ts s2 = mat(iSA,2);
  Ts s3 = mat(iSA,3);
  Ts s4 = mat(iSA,4);
  Ts s5 = mat(iSA,5);

  Tq rho, u, v, w, t;
  Tq nt;

  Real gamma = gas_.gamma();
  Real gm1 = gamma-1;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalSA( q, nt );
  Tq p = gas_.pressure(rho, t);
  Tq h = gas_.enthalpy(rho, t);
  Tq H = h + 0.5*(u*u + v*v + w*w);

  Tq ntr = nt/ntref_;

  //precomputed  dv/du*mat*du/dv, assuming mat is zero outside of last row
  matHACK(iCont,0) += -ntr*s0 - ntr*u*s1 -ntr*v*s2 - ntr*w*s3 - ntr*(H-p/rho)*s4 - ntr*ntr*s5;

  matHACK(iCont,1) += -ntr*u*s0 - ntr*(p/rho+u*u)*s1 -ntr*u*v*s2 - ntr*u*w*s3 - u*H*ntr*s4 - u*ntr*ntr*s5;
  matHACK(iCont,2) += -ntr*v*s0 - ntr*u*v*s1 -ntr*(p/rho+v*v)*s2 - ntr*v*w*s3 - v*H*ntr*s4 - v*ntr*ntr*s5;
  matHACK(iCont,3) += -ntr*w*s0 - ntr*u*w*s1 - ntr*v*w*s2 - ntr*(p/rho +w*w)*s3 - w*H*ntr*s4 - w*ntr*ntr*s5;

  matHACK(iCont,4) += -ntr*(H -p/rho)*s0 - ntr*u*H*s1 -ntr*v*H*s2 -ntr*w*H*s3
                    - ntr*(H*H - p*p*gamma/gm1/rho/rho)*s4 - ntr*ntr*(H - p/rho)*s5;

  matHACK(iCont,5) += -ntr*ntr*s0 -ntr*ntr*u*s1 -ntr*ntr*v*s2 - ntr*ntr*w*s3 - ntr*ntr*(H - p/rho)*s4 - ntr*(1 + ntr*ntr)*s5;


  matHACK(iSA,0) += s0 + u*s1 + v*s2 + w*s3 + (H-p/rho)*s4 + ntr*s5;
  matHACK(iSA,1) += u*s0 + (u*u + p/rho)*s1 + u*v*s2 + u*w*s3 + u*H*s4 + u*ntr*s5;
  matHACK(iSA,2) += v*s0 + u*v*s1 + (v*v + p/rho)*s2 + v*w*s3 + v*H*s4 + v*ntr*s5;
  matHACK(iSA,3) += w*s0 + u*w*s1 + v*w*s2  + (w*w + p/rho)*s3 + w*H*s4 + w*ntr*s5;
  matHACK(iSA,4) += (H - p/rho)*s0 + u*H*s1 + v*H*s2 + w*H*s3 + (H*H - p*p*gamma/gm1/rho/rho)*s4 + ntr*(H - p/rho)*s5;
  matHACK(iSA,5) += ntr*s0 + ntr*u*s1 + ntr*v*s2 + ntr*w*s3 + ntr*(H-p/rho)*s4 + (1. + ntr*ntr)*s5;

}


// jacobian of source wrt conservation variables, hacked to give entropy variable transformation for GLS/AGLS
// dv/du*d(S)/d(U)*du/dv
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianSourceHACK(
    const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdu ) const
{
  MatrixQ<Ts> tmp = 0;
  jacobianSource(dist, x, y, z, time, q, qx, qy, qz, tmp);

  jacobianHelperHACK(x, y, z, time, q, tmp, dsdu);
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceRow(
    const Real& dist,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Ts>& dsdux, ArrayQ<Ts>& dsduy, ArrayQ<Ts>& dsduz ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq rho=0, u=0, v=0, w=0, t=0;
  T rhox, rhoy, rhoz, ux, uy, uz, vx, vy, vz, wx, wy, wz, tx, ty, tz;
  Tq nt;                                // nu_tilde
  T  ntx, nty, ntz;
  Tq mu;                                // molecular viscosity
  Tq chi, chi3;
  T  ft2;
  T  shr0;                              // shear rate: vorticity
  T  r, g;                              // wall terms

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalSA( q, nt );
  mu  = visc_.viscosity( t );
  chi = rho*nt/mu;

  chi3 = power<3>(chi);

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  // velocity gradient Jacobian wrt conservative variables
  T uy_rhoy  = -u/rho;
  T uy_rhouy =  1/rho;

  T uz_rhoz  = -u/rho;
  T uz_rhouz =  1/rho;

  T vx_rhox  = -v/rho;
  T vx_rhovx =  1/rho;

  T vz_rhoz  = -v/rho;
  T vz_rhovz =  1/rho;

  T wx_rhox  = -w/rho;
  T wx_rhowx =  1/rho;

  T wy_rhoy  = -w/rho;
  T wy_rhowy =  1/rho;

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );

  T rhontx = rho*ntx + rhox*nt;
  T rhonty = rho*nty + rhoy*nt;
  T rhontz = rho*ntz + rhoz*nt;

  // production and near-wall destruction terms

  // shear rate: vorticity
  shr0 = sqrt((uy - vx)*(uy - vx) + (uz - wx)*(uz - wx) + (vz - wy)*(vz - wy));

  T shr0_uy =  (uy - vx)/max(shr0, T(1e-16));
  T shr0_uz =  (uz - wx)/max(shr0, T(1e-16));

  T shr0_vx = -(uy - vx)/max(shr0, T(1e-16));
  T shr0_vz =  (vz - wy)/max(shr0, T(1e-16));

  T shr0_wx = -(uz - wx)/max(shr0, T(1e-16));
  T shr0_wy = -(vz - wy)/max(shr0, T(1e-16));

  T shr0_rhox = shr0_vx*vx_rhox + shr0_wx*wx_rhox;
  T shr0_rhoy = shr0_uy*uy_rhoy + shr0_wy*wy_rhoy;
  T shr0_rhoz = shr0_uz*uz_rhoz + shr0_vz*vz_rhoz;

  T shr0_rhouy = shr0_uy*uy_rhouy;
  T shr0_rhouz = shr0_uz*uz_rhouz;

  T shr0_rhovx = shr0_vx*vx_rhovx;
  T shr0_rhovz = shr0_vz*vz_rhovz;

  T shr0_rhowx = shr0_wx*wx_rhowx;
  T shr0_rhowy = shr0_wy*wy_rhowy;

  // production term (vorticity component only)

  T prod_rhox, prod_rhoy, prod_rhoz;
  T prod_rhouy, prod_rhouz;
  T prod_rhovx, prod_rhovz;
  T prod_rhowx, prod_rhowy;
  if (nt < 0)
  {
  //prod       = cb1_*(1 - ct3_)*shr0*nt;
    prod_rhox  = cb1_*(1 - ct3_)*shr0_rhox*nt;
    prod_rhoy  = cb1_*(1 - ct3_)*shr0_rhoy*nt;
    prod_rhoz  = cb1_*(1 - ct3_)*shr0_rhoz*nt;

    prod_rhouy = cb1_*(1 - ct3_)*shr0_rhouy*nt;
    prod_rhouz = cb1_*(1 - ct3_)*shr0_rhouz*nt;

    prod_rhovx = cb1_*(1 - ct3_)*shr0_rhovx*nt;
    prod_rhovz = cb1_*(1 - ct3_)*shr0_rhovz*nt;

    prod_rhowx = cb1_*(1 - ct3_)*shr0_rhowx*nt;
    prod_rhowy = cb1_*(1 - ct3_)*shr0_rhowy*nt;
  }
  else
  {
  //prod       = cb1_*shr0*nt;
    prod_rhox  = cb1_*shr0_rhox*nt;
    prod_rhoy  = cb1_*shr0_rhoy*nt;
    prod_rhoz  = cb1_*shr0_rhoz*nt;

    prod_rhouy = cb1_*shr0_rhouy*nt;
    prod_rhouz = cb1_*shr0_rhouz*nt;

    prod_rhovx = cb1_*shr0_rhovx*nt;
    prod_rhovz = cb1_*shr0_rhovz*nt;

    prod_rhowx = cb1_*shr0_rhowx*nt;
    prod_rhowy = cb1_*shr0_rhowy*nt;
  }

  dsdux[0] += -rho*prod_rhox/ntref_;
  dsdux[2] += -rho*prod_rhovx/ntref_;
  dsdux[3] += -rho*prod_rhowx/ntref_;

  dsduy[0] += -rho*prod_rhoy/ntref_;
  dsduy[1] += -rho*prod_rhouy/ntref_;
  dsduy[3] += -rho*prod_rhowy/ntref_;

  dsduz[0] += -rho*prod_rhoz/ntref_;
  dsduz[1] += -rho*prod_rhouz/ntref_;
  dsduz[2] += -rho*prod_rhovz/ntref_;

  // near-wall destruction term (includes modified production component)

  if (nt < 0)
  {
    // wall = -cw1_*power<2,Tq>(nt/dist);
    // no dependency on gradient
  }
  else
  {
    // modified shear rate in viscous sublayer
    //  NOTE: fv2 rewritten for better accuracy
    Tq fv2    = (chi3 + (1 - chi)*power<3>(cv1_)) / ((1 + chi)*chi3 + power<3>(cv1_));
    T  shrmod = fv2 * nt/(power<2>(vk_*dist));

    T shrmod_rhox=0, shrmod_rhoy=0, shrmod_rhoz=0;
    T shrmod_rhouy=0, shrmod_rhouz=0;
    T shrmod_rhovx=0, shrmod_rhovz=0;
    T shrmod_rhowx=0, shrmod_rhowy=0;

    if (shrmod < -cv2_*shr0)
    {
      // shrmod = shr0 * (cv2_*cv2_*shr0 + cv3_*shrmod)/((cv3_ - 2*cv2_)*shr0 - shrmod);
      T num   = (cv2_*cv2_*shr0*shr0 + shr0*cv3_*shrmod);
      T denom = (cv3_ - 2*cv2_)*shr0 - shrmod;
      T tmp = num*(cv3_ - 2*cv2_)/power<2>(denom);

      shrmod_rhox = (2*cv2_*cv2_*shr0*shr0_rhox + shr0_rhox*cv3_*shrmod)/denom - tmp*shr0_rhox;
      shrmod_rhoy = (2*cv2_*cv2_*shr0*shr0_rhoy + shr0_rhoy*cv3_*shrmod)/denom - tmp*shr0_rhoy;
      shrmod_rhoz = (2*cv2_*cv2_*shr0*shr0_rhoz + shr0_rhoz*cv3_*shrmod)/denom - tmp*shr0_rhoz;

      shrmod_rhouy = (2*cv2_*cv2_*shr0*shr0_rhouy + shr0_rhouy*cv3_*shrmod)/denom - tmp*shr0_rhouy;
      shrmod_rhouz = (2*cv2_*cv2_*shr0*shr0_rhouz + shr0_rhouz*cv3_*shrmod)/denom - tmp*shr0_rhouz;

      shrmod_rhovx = (2*cv2_*cv2_*shr0*shr0_rhovx + shr0_rhovx*cv3_*shrmod)/denom - tmp*shr0_rhovx;
      shrmod_rhovz = (2*cv2_*cv2_*shr0*shr0_rhovz + shr0_rhovz*cv3_*shrmod)/denom - tmp*shr0_rhovz;

      shrmod_rhowx = (2*cv2_*cv2_*shr0*shr0_rhowx + shr0_rhowx*cv3_*shrmod)/denom - tmp*shr0_rhowx;
      shrmod_rhowy = (2*cv2_*cv2_*shr0*shr0_rhowy + shr0_rhowy*cv3_*shrmod)/denom - tmp*shr0_rhowy;

      shrmod = num/denom;
    }

    T r_rhox=0, r_rhoy=0, r_rhoz=0;
    T r_rhouy=0, r_rhouz=0;
    T r_rhovx=0, r_rhovz=0;
    T r_rhowx=0, r_rhowy=0;

    if (shr0 + shrmod == 0)
      r = rlim_;
    else
    {
      r = nt / ((shr0 + shrmod) * power<2>(vk_*dist));

      if ( r > rlim_ )
        r = rlim_;
      else
      {
        T denom = (power<2,T>(shr0 + shrmod) * power<2>(vk_*dist));

        r_rhox = -nt*(shr0_rhox + shrmod_rhox) / denom;
        r_rhoy = -nt*(shr0_rhoy + shrmod_rhoy) / denom;
        r_rhoz = -nt*(shr0_rhoz + shrmod_rhoz) / denom;

        r_rhouy = -nt*(shr0_rhouy + shrmod_rhouy) / denom;
        r_rhouz = -nt*(shr0_rhouz + shrmod_rhouz) / denom;

        r_rhovx = -nt*(shr0_rhovx + shrmod_rhovx) / denom;
        r_rhovz = -nt*(shr0_rhovz + shrmod_rhovz) / denom;

        r_rhowx = -nt*(shr0_rhowx + shrmod_rhowx) / denom;
        r_rhowy = -nt*(shr0_rhowy + shrmod_rhowy) / denom;
      }
    }

      g      = r + cw2_*(power<6>(r) - r);

    T r5 = power<5>(r);
    T g_rhox = r_rhox + cw2_*(6*r5*r_rhox - r_rhox);
    T g_rhoy = r_rhoy + cw2_*(6*r5*r_rhoy - r_rhoy);
    T g_rhoz = r_rhoz + cw2_*(6*r5*r_rhoz - r_rhoz);

    T g_rhouy = r_rhouy + cw2_*(6*r5*r_rhouy - r_rhouy);
    T g_rhouz = r_rhouz + cw2_*(6*r5*r_rhouz - r_rhouz);

    T g_rhovx = r_rhovx + cw2_*(6*r5*r_rhovx - r_rhovx);
    T g_rhovz = r_rhovz + cw2_*(6*r5*r_rhovz - r_rhovz);

    T g_rhowx = r_rhowx + cw2_*(6*r5*r_rhowx - r_rhowx);
    T g_rhowy = r_rhowy + cw2_*(6*r5*r_rhowy - r_rhowy);

    //fw = g * pow((1 + power<6>(cw3_)) / (power<6>(g) + power<6>(cw3_)), 1./6.);

    Real cw36 = power<6>(cw3_);
    T g6 = power<6>(g);
    T tmp = pow((1 + cw36) / (g6 + cw36), 1./6.);
    T fw_g = tmp - g6 * (1 + cw36) / (power<5>(tmp)*power<2,T>( g6 + cw36 ));

    T fw_rhox = fw_g*g_rhox;
    T fw_rhoy = fw_g*g_rhoy;
    T fw_rhoz = fw_g*g_rhoz;

    T fw_rhouy = fw_g*g_rhouy;
    T fw_rhouz = fw_g*g_rhouz;

    T fw_rhovx = fw_g*g_rhovx;
    T fw_rhovz = fw_g*g_rhovz;

    T fw_rhowx = fw_g*g_rhowx;
    T fw_rhowy = fw_g*g_rhowy;

    ft2  = ct3_*exp( -ct4_*chi*chi );

    //wall = (cw1_*fw - (cb1_/(vk_*vk_))*ft2)*power<2,Tq>(nt/dist) + cb1_*ft2*shr0*nt - cb1_*(1 - ft2)*shrmod*nt;
    T wall_rhox = cw1_*fw_rhox*power<2,Tq>(nt/dist) + cb1_*ft2*shr0_rhox*nt - cb1_*(1 - ft2)*shrmod_rhox*nt;
    T wall_rhoy = cw1_*fw_rhoy*power<2,Tq>(nt/dist) + cb1_*ft2*shr0_rhoy*nt - cb1_*(1 - ft2)*shrmod_rhoy*nt;
    T wall_rhoz = cw1_*fw_rhoz*power<2,Tq>(nt/dist) + cb1_*ft2*shr0_rhoz*nt - cb1_*(1 - ft2)*shrmod_rhoz*nt;

    T wall_rhouy = cw1_*fw_rhouy*power<2,Tq>(nt/dist) + cb1_*ft2*shr0_rhouy*nt - cb1_*(1 - ft2)*shrmod_rhouy*nt;
    T wall_rhouz = cw1_*fw_rhouz*power<2,Tq>(nt/dist) + cb1_*ft2*shr0_rhouz*nt - cb1_*(1 - ft2)*shrmod_rhouz*nt;

    T wall_rhovx = cw1_*fw_rhovx*power<2,Tq>(nt/dist) + cb1_*ft2*shr0_rhovx*nt - cb1_*(1 - ft2)*shrmod_rhovx*nt;
    T wall_rhovz = cw1_*fw_rhovz*power<2,Tq>(nt/dist) + cb1_*ft2*shr0_rhovz*nt - cb1_*(1 - ft2)*shrmod_rhovz*nt;

    T wall_rhowx = cw1_*fw_rhowx*power<2,Tq>(nt/dist) + cb1_*ft2*shr0_rhowx*nt - cb1_*(1 - ft2)*shrmod_rhowx*nt;
    T wall_rhowy = cw1_*fw_rhowy*power<2,Tq>(nt/dist) + cb1_*ft2*shr0_rhowy*nt - cb1_*(1 - ft2)*shrmod_rhowy*nt;

    dsdux[0] += rho*wall_rhox/ntref_;
    dsdux[2] += rho*wall_rhovx/ntref_;
    dsdux[3] += rho*wall_rhowx/ntref_;

    dsduy[0] += rho*wall_rhoy/ntref_;
    dsduy[1] += rho*wall_rhouy/ntref_;
    dsduy[3] += rho*wall_rhowy/ntref_;

    dsduz[0] += rho*wall_rhoz/ntref_;
    dsduz[1] += rho*wall_rhouz/ntref_;
    dsduz[2] += rho*wall_rhovz/ntref_;
  }

  // non-conservative diffusion terms

  //nonc = cb2_*rho*(ntx*ntx + nty*nty + ntz*ntz)/sigma_;
  T nonc_rhox   = cb2_*(-2*nt*(rhontx - nt*rhox)/rho)/sigma_;
  T nonc_rhoy   = cb2_*(-2*nt*(rhonty - nt*rhoy)/rho)/sigma_;
  T nonc_rhoz   = cb2_*(-2*nt*(rhontz - nt*rhoz)/rho)/sigma_;
  T nonc_rhontx = cb2_*(    2*(rhontx - nt*rhox)/rho)/sigma_;
  T nonc_rhonty = cb2_*(    2*(rhonty - nt*rhoy)/rho)/sigma_;
  T nonc_rhontz = cb2_*(    2*(rhontz - nt*rhoz)/rho)/sigma_;

  if (nt < 0)
  {
    Tq fn = (cn1_ + chi3) / (cn1_ - chi3);
    //nonc += - (mu/rho + nt*fn)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

    nonc_rhox += - (mu/rho + nt*fn)*(rhontx - 2*nt*rhox)/rho/sigma_;
    nonc_rhoy += - (mu/rho + nt*fn)*(rhonty - 2*nt*rhoy)/rho/sigma_;
    nonc_rhoz += - (mu/rho + nt*fn)*(rhontz - 2*nt*rhoz)/rho/sigma_;
    nonc_rhontx += - (mu/rho + nt*fn)*(rhox/rho)/sigma_;
    nonc_rhonty += - (mu/rho + nt*fn)*(rhoy/rho)/sigma_;
    nonc_rhontz += - (mu/rho + nt*fn)*(rhoz/rho)/sigma_;
  }
  else
  {
    //nonc += - (mu/rho + nt)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

    nonc_rhox += - (mu/rho + nt)*(rhontx - 2*nt*rhox)/rho/sigma_;
    nonc_rhoy += - (mu/rho + nt)*(rhonty - 2*nt*rhoy)/rho/sigma_;
    nonc_rhoz += - (mu/rho + nt)*(rhontz - 2*nt*rhoz)/rho/sigma_;
    nonc_rhontx += - (mu/rho + nt)*(rhox/rho)/sigma_;
    nonc_rhonty += - (mu/rho + nt)*(rhoy/rho)/sigma_;
    nonc_rhontz += - (mu/rho + nt)*(rhoz/rho)/sigma_;
  }

  dsdux[0] += -nonc_rhox/ntref_;
  dsdux[5] += -nonc_rhontx;

  dsduy[0] += -nonc_rhoy/ntref_;
  dsduy[5] += -nonc_rhonty;

  dsduz[0] += -nonc_rhoz/ntref_;
  dsduz[5] += -nonc_rhontz;
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianGradientSource(
    const Real& dist,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
{
  ArrayQ<Ts> dsdux_row = 0, dsduy_row = 0, dsduz_row = 0;

  jacobianGradientSourceRow( dist, x, y, z, time, q, qx, qy, qz, dsdux_row, dsduy_row, dsduz_row );

  dsdux(iSA, 0) += dsdux_row[0];
  dsdux(iSA, 2) += dsdux_row[2];
  dsdux(iSA, 3) += dsdux_row[3];
  dsdux(iSA, 5) += dsdux_row[5];

  dsduy(iSA, 0) += dsduy_row[0];
  dsduy(iSA, 1) += dsduy_row[1];
  dsduy(iSA, 3) += dsduy_row[3];
  dsduy(iSA, 5) += dsduy_row[5];

  dsduz(iSA, 0) += dsduz_row[0];
  dsduz(iSA, 1) += dsduz_row[1];
  dsduz(iSA, 2) += dsduz_row[2];
  dsduz(iSA, 5) += dsduz_row[5];

  // NS source terms (nominally empty function)
  BaseType::jacobianGradientSource( x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz );
}

// jacobian of source wrt gradients of conservation variables: d(S)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceGradient(
    const Real& dist,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    MatrixQ<Ts>& div_dsdgradu ) const
{
  SANS_DEVELOPER_EXCEPTION("PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceGradient not unit tested");
#if 0
  typedef typename promote_Surreal<Tq,Tg>::type Tpq;

  typedef typename promote_Surreal<Tq, Tg, Th>::type T;
  Tq rho = 0, u = 0, v = 0, w = 0, t = 0;
  Tpq rhox = 0, ux = 0, vx = 0, wx = 0, tx = 0;
  Tpq rhoy = 0, uy = 0, vy = 0, wy = 0, ty = 0;
  Tpq rhoz = 0, uz = 0, vz = 0, wz = 0, tz = 0;
  Tq nt;                                // nu_tilde
  Tpq  ntx, nty, ntz;
  Tq mu;                                // molecular viscosity
  Tq chi, chi3;
  T  ft2;
  T  shr0;                              // shear rate: vorticity
  T  r, g;                              // wall terms

  Real distx = 0, disty = 0, distz = 0; //don't use distance function gradients for now

  Real dist2 = max(dist, 1e-10);

  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  qInterpret_.evalSAGradient( q, qx, ntx );
  qInterpret_.evalSAGradient( q, qy, nty );
  qInterpret_.evalSAGradient( q, qz, ntz );
  // evaluate second derivatives
  T rhoxx, rhoyy, rhozz;
  T      uxy, uyy, uxz, uyz, uzz;
  T vxx, vxy,      vxz, vyz, vzz;
  T wxx, wxy, wyy, wxz, wyz     ;

  DLA::MatrixSymS<D,T> rhoH, uH, vH, wH, tH;
  qInterpret_.evalSecondGradient( q, qx, qy, qz,
                                  qxx, qxy, qyy, qxz, qyz, qzz,
                                  rhoH, uH, vH, wH, tH );
  rhoxx = rhoH(0,0);                  vxx = vH(0,0);  wxx = wH(0,0);
                      uxy = uH(1,0);  vxy = vH(0,1);  wxy = wH(0,1);
  rhoyy = rhoH(1,1);  uyy = uH(1,1);                  wyy = wH(1,1);
                      uxz = uH(0,2);  vxz = vH(0,2);  wxz = wH(0,2);
                      uyz = uH(1,2);  vyz = vH(1,2);  wyz = wH(1,2);
  rhozz = rhoH(2,2);  uzz = uH(2,2);  vzz = vH(2,2);

  T ntxx = 0; T ntyy = 0; T ntzz = 0;
  qInterpret_.evalSAHessian( q, qx, qx, qxx, ntxx );
  qInterpret_.evalSAHessian( q, qy, qy, qyy, ntyy );
  qInterpret_.evalSAHessian( q, qz, qz, qzz, ntzz );


  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalSA( q, nt );
  mu  = visc_.viscosity( t );
  chi = rho*nt/mu;

  Tq mu_t = 0;
  visc_.viscosityJacobian( t, mu_t );
  T mux = mu_t*tx;
  T muy = mu_t*ty;
  T muz = mu_t*tz;

  T chix = rhox*nt/mu + rho*ntx/mu - rho*nt/(mu*mu)*mux;
  T chiy = rhoy*nt/mu + rho*nty/mu - rho*nt/(mu*mu)*muy;
  T chiz = rhoz*nt/mu + rho*ntz/mu - rho*nt/(mu*mu)*muz;

  chi3 = power<3>(chi);
  T chi3x = 3*power<2>(chi)*chix;
  T chi3y = 3*power<2>(chi)*chiy;
  T chi3z = 3*power<2>(chi)*chiz;


  // velocity gradient Jacobian wrt conservative variables
  T uy_rhoy  = -u/rho;
  T uy_rhouy =  1/rho;

  T uz_rhoz  = -u/rho;
  T uz_rhouz =  1/rho;

  T vx_rhox  = -v/rho;
  T vx_rhovx =  1/rho;

  T vz_rhoz  = -v/rho;
  T vz_rhovz =  1/rho;

  T wx_rhox  = -w/rho;
  T wx_rhowx =  1/rho;

  T wy_rhoy  = -w/rho;
  T wy_rhowy =  1/rho;

  T uy_rhoyy = -uy/rho + u*rhoy/(rho*rho);
  T uy_rhouyy = -1/(rho*rho)*rhoy;

  T uz_rhozz = -uz/rho + u*rhoz/(rho*rho);
  T uz_rhouzz = -1/(rho*rho)*rhoz;

  T vx_rhoxx = -vx/rho + v*rhox/(rho*rho);
  T vx_rhovxx = -1/(rho*rho)*rhox;

  T vz_rhozz = -vz/rho + v*rhoz/(rho*rho);
  T vz_rhovzz = -1/(rho*rho)*rhoz;

  T wx_rhoxx = -wx/rho + w*rhox/(rho*rho);
  T wx_rhowxx = -1/(rho*rho)*rhox;

  T wy_rhoyy = -wy/rho + w*rhoy/(rho*rho);
  T wy_rhowyy = -1/(rho*rho)*rhoy;

  // production and near-wall destruction terms

  // shear rate: vorticity
  shr0 = sqrt((uy - vx)*(uy - vx) + (uz - wx)*(uz - wx) + (vz - wy)*(vz - wy));

  T shr0x = (uy - vx)/max(shr0, T(1e-16))*(uxy - vxx) + (uz - wx)/max(shr0, T(1e-16))*(uxz - wxx) + (vz - wy)/max(shr0, T(1e-16))*(vxz - wxy);
  T shr0y = (uy - vx)/max(shr0, T(1e-16))*(uxy - vxy) + (uz - wx)/max(shr0, T(1e-16))*(uxz - wxy) + (vz - wy)/max(shr0, T(1e-16))*(vyz - wyy);
  T shr0z = (uy - vx)/max(shr0, T(1e-16))*(uyz - vxz) + (uz - wx)/max(shr0, T(1e-16))*(uzz - wxz) + (vz - wy)/max(shr0, T(1e-16))*(vzz - wyz);

  T shr0_uy =  (uy - vx)/max(shr0, T(1e-16));
  T shr0_uz =  (uz - wx)/max(shr0, T(1e-16));

  T shr0_vx = -(uy - vx)/max(shr0, T(1e-16));
  T shr0_vz =  (vz - wy)/max(shr0, T(1e-16));

  T shr0_wx = -(uz - wx)/max(shr0, T(1e-16));
  T shr0_wy = -(vz - wy)/max(shr0, T(1e-16));

  T shr0_uyy =  (uyy - vxy)/max(shr0, T(1e-16)) - (uy - vx)*shr0y/max( power<2,T>(shr0), T(1e-16));
  T shr0_uzz =  (uyz - wxz)/max(shr0, T(1e-16)) + (uz - wx)*shr0y/max( power<2,T>(shr0), T(1e-16));

  T shr0_vxx = -(uxy - vxx)/max(shr0, T(1e-16)) - (uy - vx)*shr0x/max( power<2,T>(shr0), T(1e-16));
  T shr0_vzz =  (uzz - wyz)/max(shr0, T(1e-16)) + (uz - wy)*shr0z/max( power<2,T>(shr0), T(1e-16));

  T shr0_wxx = -(uxz - wxx)/max(shr0, T(1e-16)) - (uz - wx)*shr0x/max( power<2,T>(shr0), T(1e-16));
  T shr0_wyy = -(vyz - wxy)/max(shr0, T(1e-16)) - (vz - wy)*shr0y/max( power<2,T>(shr0), T(1e-16));

  T shr0_rhox = shr0_vx*vx_rhox + shr0_wx*wx_rhox;
  T shr0_rhoy = shr0_uy*uy_rhoy + shr0_wy*wy_rhoy;
  T shr0_rhoz = shr0_uz*uz_rhoz + shr0_vz*vz_rhoz;

  T shr0_rhouy = shr0_uy*uy_rhouy;
  T shr0_rhouz = shr0_uz*uz_rhouz;

  T shr0_rhovx = shr0_vx*vx_rhovx;
  T shr0_rhovz = shr0_vz*vz_rhovz;

  T shr0_rhowx = shr0_wx*wx_rhowx;
  T shr0_rhowy = shr0_wy*wy_rhowy;

  T shr0_rhoxx =  shr0_vxx*vx_rhox + shr0_vx*vx_rhoxx + shr0_wxx*wx_rhox + shr0_wx*wx_rhoxx;
  T shr0_rhoyy =  shr0_uyy*uy_rhoy + shr0_uy*uy_rhoyy + shr0_wyy*wy_rhoy + shr0_wy*wy_rhoyy;
  T shr0_rhozz =  shr0_uzz*uz_rhoz + shr0_uz*uz_rhozz + shr0_vzz*vz_rhoz + shr0_vz*vz_rhozz;

  T shr0_rhouyy =  shr0_uyy*uy_rhouy + shr0_uy*uy_rhouyy;
  T shr0_rhouzz =  shr0_uzz*uz_rhouz + shr0_uz*uz_rhouzz;

  T shr0_rhovxx =  shr0_vxx*vx_rhovx + shr0_vx*vx_rhovxx;
  T shr0_rhovzz =  shr0_vzz*vz_rhovz + shr0_vz*vz_rhovzz;

  T shr0_rhowxx =  shr0_wxx*wx_rhowx + shr0_wx*wx_rhowxx;
  T shr0_rhowyy =  shr0_wyy*wy_rhowy + shr0_wy*wy_rhowyy;

  // production term (vorticity component only)

  T prod_rhox, prod_rhoy, prod_rhoz;
  T prod_rhouy, prod_rhouz;
  T prod_rhovx, prod_rhovz;
  T prod_rhowx, prod_rhowy;
  T prod_rhoxx, prod_rhoyy, prod_rhozz;
  T prod_rhouyy, prod_rhouzz;
  T prod_rhovxx, prod_rhovzz;
  T prod_rhowxx, prod_rhowyy;
  if (nt < 0)
  {
    //prod       = cb1_*(1 - ct3_)*shr0*nt;
    prod_rhox  = cb1_*(1 - ct3_)*shr0_rhox*nt;
    prod_rhoy  = cb1_*(1 - ct3_)*shr0_rhoy*nt;
    prod_rhoz  = cb1_*(1 - ct3_)*shr0_rhoz*nt;

    prod_rhouy = cb1_*(1 - ct3_)*shr0_rhouy*nt;
    prod_rhouz = cb1_*(1 - ct3_)*shr0_rhouz*nt;
    prod_rhovx = cb1_*(1 - ct3_)*shr0_rhovx*nt;
    prod_rhovz = cb1_*(1 - ct3_)*shr0_rhovz*nt;
    prod_rhowx = cb1_*(1 - ct3_)*shr0_rhowx*nt;
    prod_rhowy = cb1_*(1 - ct3_)*shr0_rhowy*nt;

    //shr0 doesn't depend on second derivatives
    prod_rhoxx = cb1_*(1 - ct3_)*(shr0_rhoxx*nt + shr0_rhox*ntx);
    prod_rhoyy = cb1_*(1 - ct3_)*(shr0_rhoyy*nt + shr0_rhoy*nty);
    prod_rhozz = cb1_*(1 - ct3_)*(shr0_rhozz*nt + shr0_rhoz*ntz);
    prod_rhouyy = cb1_*(1 - ct3_)*(shr0_rhouyy*nt + shr0_rhouy*nty);
    prod_rhouzz = cb1_*(1 - ct3_)*(shr0_rhouzz*nt + shr0_rhouz*ntz);
    prod_rhovxx = cb1_*(1 - ct3_)*(shr0_rhovxx*nt + shr0_rhovx*ntx);
    prod_rhovzz = cb1_*(1 - ct3_)*(shr0_rhovzz*nt + shr0_rhovz*ntz);
    prod_rhowxx = cb1_*(1 - ct3_)*(shr0_rhowxx*nt + shr0_rhowx*ntx);
    prod_rhowyy = cb1_*(1 - ct3_)*(shr0_rhowyy*nt + shr0_rhowy*nty);

  }
  else
  {
    //prod       = cb1_*shr0*nt;
    prod_rhox  = cb1_*shr0_rhox*nt;
    prod_rhoy  = cb1_*shr0_rhoy*nt;
    prod_rhoz  = cb1_*shr0_rhoz*nt;

    prod_rhouy = cb1_*shr0_rhouy*nt;
    prod_rhouz = cb1_*shr0_rhouz*nt;
    prod_rhovx = cb1_*shr0_rhovx*nt;
    prod_rhovz = cb1_*shr0_rhovz*nt;
    prod_rhowx = cb1_*shr0_rhowx*nt;
    prod_rhowy = cb1_*shr0_rhowy*nt;

    prod_rhoxx = cb1_*(shr0_rhoxx*nt + shr0_rhox*ntx);
    prod_rhoyy = cb1_*(shr0_rhoyy*nt + shr0_rhoy*nty);
    prod_rhozz = cb1_*(shr0_rhozz*nt + shr0_rhoz*ntz);
    prod_rhouyy = cb1_*(shr0_rhouyy*nt + shr0_rhouy*nty);
    prod_rhouzz = cb1_*(shr0_rhouzz*nt + shr0_rhouz*ntz);
    prod_rhovxx = cb1_*(shr0_rhovxx*nt + shr0_rhovx*ntx);
    prod_rhovzz = cb1_*(shr0_rhovzz*nt + shr0_rhovz*ntz);
    prod_rhowxx = cb1_*(shr0_rhowxx*nt + shr0_rhowx*ntx);
    prod_rhowyy = cb1_*(shr0_rhowyy*nt + shr0_rhowy*nty);

  }

//  dsdux(iSA, 0) += -rho*prod_rhox;
  div_dsdgradu(iSA, 0) += -rhox*prod_rhox - rho*prod_rhoxx;
//  dsdux(iSA, 2) += -rho*prod_rhovx;
  div_dsdgradu(iSA, 2) += -rhox*prod_rhovx - rho*prod_rhovxx;
//  dsdux(iSA, 3) += -rho*prod_rhowx;
  div_dsdgradu(iSA, 3) += -rhox*prod_rhowx - rho*prod_rhowxx;

//  dsduy(iSA, 0) += -rho*prod_rhoy;
  div_dsdgradu(iSA, 0) += -rhoy*prod_rhoy - rho*prod_rhoyy;
//  dsduy(iSA, 1) += -rho*prod_rhouy;
  div_dsdgradu(iSA, 1) += -rhoy*prod_rhouy - rho*prod_rhouyy;
//  dsduy(iSA, 3) += -rho*prod_rhowy;
  div_dsdgradu(iSA, 3) += -rhoy*prod_rhowy - rho*prod_rhowyy;

//  dsduz(iSA, 0) += -rho*prod_rhoz;
  div_dsdgradu(iSA, 0) += -rhoz*prod_rhoz - rho*prod_rhozz;
//  dsduz(iSA, 1) += -rho*prod_rhouz;
  div_dsdgradu(iSA, 1) += -rhoz*prod_rhouz - rho*prod_rhouzz;
//  dsduz(iSA, 2) += -rho*prod_rhovz;
  div_dsdgradu(iSA, 2) += -rhoz*prod_rhovz - rho*prod_rhovzz;

  // near-wall destruction term (includes modified production component)

  if (nt < 0)
  {
    // wall = -cw1_*power<2,Tq>(nt/dist);
    // no dependency on gradient
  }
  else
  {
    // modified shear rate in viscous sublayer
    //  NOTE: fv2 rewritten for better accuracy
    Real cv13 = power<3>(cv1_);
    Tq fv2    = (chi3 + (1 - chi)*cv13) / ((1 + chi)*chi3 + cv13);
    T  shrmod = fv2 * nt/(power<2>(vk_*dist2));

    T shrmod_rhox = 0, shrmod_rhoy = 0, shrmod_rhoz = 0;
    T shrmod_rhouy = 0;
    T shrmod_rhouz = 0;
    T shrmod_rhovx = 0;
    T shrmod_rhovz = 0;
    T shrmod_rhowx = 0;
    T shrmod_rhowy = 0;

    T chi4 = power<4>(chi);
    T chi6 = power<6>(chi);
    Real cv16 = power<6>(cv1_);

    T fv2x = - (cv16 + cv13*(2. - 3.*chi)*chi3 + chi6)*chix
             /( cv13 + chi3 + chi4  )/( cv13 + chi3 + chi4  );

    T fv2y = -  (cv16 + cv13*(2. - 3.*chi)*chi3 + chi6)*chiy
             /( cv13 + chi3 + chi4  )/( cv13 + chi3 + chi4  );

    T fv2z = -  (cv16 + cv13*(2. - 3.*chi)*chi3 + chi6)*chiz
             /( cv13 + chi3 + chi4  )/( cv13 + chi3 + chi4  );

    T shrmodx = (fv2x *nt + fv2*ntx)/(power<2>(vk_*dist2)) - 2*shrmod/dist2*distx;
    T shrmody = (fv2y *nt + fv2*nty)/(power<2>(vk_*dist2)) - 2*shrmod/dist2*disty;
    T shrmodz = (fv2z *nt + fv2*ntz)/(power<2>(vk_*dist2)) - 2*shrmod/dist2*distz;

    T shrmod_rhoxx = 0, shrmod_rhoyy = 0, shrmod_rhozz = 0;
    T shrmod_rhouyy = 0, shrmod_rhouzz = 0;
    T shrmod_rhovxx = 0, shrmod_rhovzz = 0;
    T shrmod_rhowxx = 0, shrmod_rhowyy = 0;

    if (shrmod < -cv2_*shr0)
    {
      // shrmod = shr0 * (cv2_*cv2_*shr0 + cv3_*shrmod)/((cv3_ - 2*cv2_)*shr0 - shrmod);
      T num   = (cv2_*cv2_*shr0*shr0 + shr0*cv3_*shrmod);
      T denom = (cv3_ - 2*cv2_)*shr0 - shrmod;
      T tmp = num*(cv3_ - 2*cv2_)/power<2>(denom);

      T numx =  (2.*cv2_*cv2_*shr0*shr0x + cv3_*(shr0x*shrmod + shr0*shrmodx));
      T numy =  (2.*cv2_*cv2_*shr0*shr0y + cv3_*(shr0y*shrmod + shr0*shrmody));
      T numz =  (2.*cv2_*cv2_*shr0*shr0z + cv3_*(shr0z*shrmod + shr0*shrmodz));

      T denomx = (cv3_ - 2*cv2_)*shr0x - shrmodx;
      T denomy = (cv3_ - 2*cv2_)*shr0y - shrmody;
      T denomz = (cv3_ - 2*cv2_)*shr0z - shrmodz;

      T tmpx = numx*(cv3_ - 2*cv2_)/power<2>(denom) - 2*num*(cv3_ - 2*cv2_)*denomx/power<3>(denom);
      T tmpy = numy*(cv3_ - 2*cv2_)/power<2>(denom) - 2*num*(cv3_ - 2*cv2_)*denomy/power<3>(denom);
      T tmpz = numz*(cv3_ - 2*cv2_)/power<2>(denom) - 2*num*(cv3_ - 2*cv2_)*denomz/power<3>(denom);

      shrmod_rhox = (2.*cv2_*cv2_*shr0*shr0_rhox + shr0_rhox*cv3_*shrmod)/denom - tmp*shr0_rhox;
      shrmod_rhoy = (2.*cv2_*cv2_*shr0*shr0_rhoy + shr0_rhoy*cv3_*shrmod)/denom - tmp*shr0_rhoy;
      shrmod_rhoz = (2.*cv2_*cv2_*shr0*shr0_rhoz + shr0_rhoz*cv3_*shrmod)/denom - tmp*shr0_rhoz;

      shrmod_rhouy = (2.*cv2_*cv2_*shr0*shr0_rhouy + shr0_rhouy*cv3_*shrmod)/denom - tmp*shr0_rhouy;
      shrmod_rhouz = (2.*cv2_*cv2_*shr0*shr0_rhouz + shr0_rhouz*cv3_*shrmod)/denom - tmp*shr0_rhouz;

      shrmod_rhovx = (2.*cv2_*cv2_*shr0*shr0_rhovx + shr0_rhovx*cv3_*shrmod)/denom - tmp*shr0_rhovx;
      shrmod_rhovz = (2.*cv2_*cv2_*shr0*shr0_rhovz + shr0_rhovz*cv3_*shrmod)/denom - tmp*shr0_rhovz;

      shrmod_rhowx = (2.*cv2_*cv2_*shr0*shr0_rhowx + shr0_rhowx*cv3_*shrmod)/denom - tmp*shr0_rhowx;
      shrmod_rhowy = (2.*cv2_*cv2_*shr0*shr0_rhowy + shr0_rhowy*cv3_*shrmod)/denom - tmp*shr0_rhowy;

      shrmod_rhoxx = 2.*cv2_*cv2_*(shr0x*shr0_rhox + shr0*shr0_rhoxx) /denom
                      + cv3_*(shr0_rhoxx*shrmod + shr0_rhox*shrmodx)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhox + shr0_rhox*cv3_*shrmod)*denomx/power<2,T>(denom)
                     - tmpx*shr0_rhox - tmp*shr0_rhoxx;

      shrmod_rhoyy = 2.*cv2_*cv2_*(shr0y*shr0_rhoy + shr0*shr0_rhoyy) /denom
                      + cv3_*(shr0_rhoyy*shrmod + shr0_rhoy*shrmody)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhoy + shr0_rhoy*cv3_*shrmod)*denomy/power<2,T>(denom)
                     - tmpy*shr0_rhoy - tmp*shr0_rhoyy;

      shrmod_rhozz = 2.*cv2_*cv2_*(shr0z*shr0_rhoz + shr0*shr0_rhozz) /denom
                      + cv3_*(shr0_rhozz*shrmod + shr0_rhoz*shrmody)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhoz + shr0_rhoz*cv3_*shrmod)*denomz/power<2,T>(denom)
                     - tmpz*shr0_rhoz - tmp*shr0_rhozz;

      shrmod_rhouyy = 2.*cv2_*cv2_*(shr0y*shr0_rhouy + shr0*shr0_rhouyy) /denom
                      + cv3_*(shr0_rhouyy*shrmod + shr0_rhouy*shrmody)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhouy + shr0_rhouy*cv3_*shrmod)*denomy/power<2,T>(denom)
                     - tmpy*shr0_rhouy - tmp*shr0_rhouyy;


      shrmod_rhouzz = 2.*cv2_*cv2_*(shr0z*shr0_rhouz + shr0*shr0_rhouzz) /denom
                      + cv3_*(shr0_rhouzz*shrmod + shr0_rhouz*shrmodz)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhouz + shr0_rhouz*cv3_*shrmod)*denomz/power<2,T>(denom)
                     - tmpz*shr0_rhouz - tmp*shr0_rhouzz;

      shrmod_rhovxx = 2.*cv2_*cv2_*(shr0x*shr0_rhovx + shr0*shr0_rhovxx) /denom
                      + cv3_*(shr0_rhovxx*shrmod + shr0_rhovx*shrmodx)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhovx + shr0_rhovx*cv3_*shrmod)*denomx/power<2,T>(denom)
                     - tmpx*shr0_rhovx - tmp*shr0_rhovxx;

      shrmod_rhovzz = 2.*cv2_*cv2_*(shr0z*shr0_rhovz + shr0*shr0_rhovzz) /denom
                      + cv3_*(shr0_rhovzz*shrmod + shr0_rhovz*shrmodz)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhovz + shr0_rhovz*cv3_*shrmod)*denomz/power<2,T>(denom)
                     - tmpz*shr0_rhovz - tmp*shr0_rhovzz;

      shrmod_rhowxx = 2.*cv2_*cv2_*(shr0x*shr0_rhowx + shr0*shr0_rhowxx) /denom
                      + cv3_*(shr0_rhowxx*shrmod + shr0_rhowx*shrmodx)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhowx + shr0_rhowx*cv3_*shrmod)*denomx/power<2,T>(denom)
                     - tmpx*shr0_rhowx - tmp*shr0_rhowxx;

      shrmod_rhowyy = 2.*cv2_*cv2_*(shr0y*shr0_rhowy + shr0*shr0_rhowyy) /denom
                      + cv3_*(shr0_rhowyy*shrmod + shr0_rhowy*shrmody)/denom
                     -(2*cv2_*cv2_*shr0*shr0_rhowy + shr0_rhowy*cv3_*shrmod)*denomy/power<2,T>(denom)
                     - tmpy*shr0_rhowy - tmp*shr0_rhowyy;

      //new values

      shrmod = num/denom;

      shrmodx = numx/denom - num/(denom*denom)*denomx;
      shrmody = numy/denom - num/(denom*denom)*denomy;
      shrmodz = numz/denom - num/(denom*denom)*denomz;

    }

    T r_rhox=0, r_rhoy=0, r_rhoz=0;
    T r_rhouy=0, r_rhouz=0;
    T r_rhovx=0, r_rhovz=0;
    T r_rhowx=0, r_rhowy=0;

    T rx = 0; T ry = 0; T rz = 0;
    T r_rhoxx = 0; T r_rhoyy = 0; T r_rhozz = 0;
    T r_rhouyy = 0, r_rhouzz = 0;
    T r_rhovxx = 0, r_rhovzz = 0;
    T r_rhowxx = 0, r_rhowyy = 0;

    T shrsum = shr0 + shrmod;
    if (shrsum == 0)
    {
      r = rlim_;
    }
    else
    {
#ifndef SMOOTHR
      r = nt / (shrsum * power<2>(vk_*dist));

      if ( r > rlim_ )
        r = rlim_;
      else
      {
        T denom = power<2,T>(shr0 + shrmod) * power<2>(vk_*dist);
        T denomx = 2*(shr0 + shrmod)*(shr0x + shrmodx)*power<2>(vk_*dist)
                   + 2.*power<2,T>(shr0 + shrmod)*vk_*vk_*dist*distx;
        T denomy = 2*(shr0 + shrmod)*(shr0y + shrmody)*power<2>(vk_*dist)
                   + 2.*power<2,T>(shr0 + shrmod)*vk_*vk_*dist*disty;
        T denomz = 2*(shr0 + shrmod)*(shr0z + shrmodz)*power<2>(vk_*dist)
                   + 2.*power<2,T>(shr0 + shrmod)*vk_*vk_*dist*distz;

        r_rhox = -nt*(shr0_rhox + shrmod_rhox) / denom;
        r_rhoy = -nt*(shr0_rhoy + shrmod_rhoy) / denom;
        r_rhoz = -nt*(shr0_rhoz + shrmod_rhoz) / denom;

        r_rhouy = -nt*(shr0_rhouy + shrmod_rhouy) / denom;
        r_rhouz = -nt*(shr0_rhouz + shrmod_rhouz) / denom;

        r_rhovx = -nt*(shr0_rhovx + shrmod_rhovx) / denom;
        r_rhovz = -nt*(shr0_rhovz + shrmod_rhovz) / denom;

        r_rhowx = -nt*(shr0_rhowx + shrmod_rhowx) / denom;
        r_rhowy = -nt*(shr0_rhowy + shrmod_rhowy) / denom;

        rx = ntx / (shrsum * power<2>(vk_*dist))
               - nt*(shr0x + shrmodx) / (power<2,T>(shrsum) * power<2>(vk_*dist)) - 2./dist*r*distx;

        ry = nty / (shrsum * power<2>(vk_*dist))
               - nt*(shr0y + shrmody) / (power<2,T>(shrsum) * power<2>(vk_*dist)) - 2./dist*r*disty;

        rz = ntz / (shrsum * power<2>(vk_*dist))
               - nt*(shr0z + shrmodz) / (power<2,T>(shrsum) * power<2>(vk_*dist)) - 2./dist*r*distz;

        r_rhoxx = -ntx*(shr0_rhox + shrmod_rhox) / denom - nt*(shr0_rhoxx + shrmod_rhoxx)/denom
                   + nt*(shr0_rhox + shrmod_rhox)*denomx /power<2,T>(denom);
        r_rhoyy = -nty*(shr0_rhoy + shrmod_rhoy) / denom - nt*(shr0_rhoyy + shrmod_rhoyy)/denom
                   + nt*(shr0_rhoy + shrmod_rhoy)*denomy /power<2,T>(denom);
        r_rhozz = -ntz*(shr0_rhoz + shrmod_rhoz) / denom - nt*(shr0_rhozz + shrmod_rhozz)/denom
                   + nt*(shr0_rhoz + shrmod_rhoz)*denomz /power<2,T>(denom);
        r_rhouyy = -nty*(shr0_rhouy + shrmod_rhouy) / denom - nt*(shr0_rhouyy + shrmod_rhouyy)/denom
                   + nt*(shr0_rhouy + shrmod_rhouy)*denomy /power<2,T>(denom);
        r_rhouzz = -ntz*(shr0_rhouz + shrmod_rhouz) / denom - nt*(shr0_rhouzz + shrmod_rhouzz)/denom
                   + nt*(shr0_rhouy + shrmod_rhouy)*denomy /power<2,T>(denom);
        r_rhovxx = -ntx*(shr0_rhovx + shrmod_rhovx) / denom - nt*(shr0_rhovxx + shrmod_rhovxx)/denom
                   + nt*(shr0_rhovx + shrmod_rhovx)*denomx /power<2,T>(denom);
        r_rhovzz = -ntz*(shr0_rhovz + shrmod_rhovz) / denom - nt*(shr0_rhovzz + shrmod_rhovzz)/denom
                   + nt*(shr0_rhovz + shrmod_rhovz)*denomx /power<2,T>(denom);
        r_rhowxx = -ntx*(shr0_rhowx + shrmod_rhowx) / denom - nt*(shr0_rhowxx + shrmod_rhowxx)/denom
                   + nt*(shr0_rhowx + shrmod_rhowx)*denomx /power<2,T>(denom);
        r_rhowyy = -nty*(shr0_rhowy + shrmod_rhowy) / denom - nt*(shr0_rhowyy + shrmod_rhowyy)/denom
                   + nt*(shr0_rhowy + shrmod_rhowy)*denomy /power<2,T>(denom);

      }
#else
      SANS_DEVELOPER_EXCEPTION("PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceGradient using 2D code");
      T rtmp =  nt / ((shr0 + shrmod) * power<2>(vk_*dist2));
      r = pow( 1./(rlim_*rlim_) + 1./(rtmp*rtmp) , -0.5  );

      T r_rtmp = pow( 1./(rlim_*rlim_) + 1./(rtmp*rtmp) , -1.5  )*(1./(rtmp*rtmp*rtmp));

      T denom = power<2,T>(shr0 + shrmod) * power<2>(vk_*dist2);
      T denomx = 2*(shr0 + shrmod)*(shr0x + shrmodx)*power<2>(vk_*dist2)
                 + 2.*power<2,T>(shr0 + shrmod)*vk_*vk_*dist2*distx;
      T denomy = 2*(shr0 + shrmod)*(shr0y + shrmody)*power<2>(vk_*dist2)
                 + 2.*power<2,T>(shr0 + shrmod)*vk_*vk_*dist2*disty;

      r_rhox = -r_rtmp*nt*(shr0_rhox + shrmod_rhox) / denom;
      r_rhoy = -r_rtmp*nt*(shr0_rhoy + shrmod_rhoy) / denom;

      r_rhouy = -r_rtmp*nt*(shr0_rhouy + shrmod_rhouy) / denom;

      r_rhovx = -r_rtmp*nt*(shr0_rhovx + shrmod_rhovx) / denom;

      rx = r_rtmp*(ntx / (shrsum * power<2>(vk_*dist2))
             - nt*(shr0x + shrmodx) / (power<2,T>(shrsum) * power<2>(vk_*dist2)) - 2./dist2*rtmp*distx);

      ry = r_rtmp*(nty / (shrsum * power<2>(vk_*dist2))
             - nt*(shr0y + shrmody) / (power<2,T>(shrsum) * power<2>(vk_*dist2)) - 2./dist2*rtmp*disty);

      r_rhoxx = r_rtmp*(-ntx*(shr0_rhox + shrmod_rhox) / denom - nt*(shr0_rhoxx + shrmod_rhoxx)/denom
                 + nt*(shr0_rhox + shrmod_rhox)*denomx /power<2,T>(denom));
      r_rhoyy = r_rtmp*(-nty*(shr0_rhoy + shrmod_rhoy) / denom - nt*(shr0_rhoyy + shrmod_rhoyy)/denom
                 + nt*(shr0_rhoy + shrmod_rhoy)*denomy /power<2,T>(denom));
      r_rhouyy = r_rtmp*(-nty*(shr0_rhouy + shrmod_rhouy) / denom - nt*(shr0_rhouyy + shrmod_rhouyy)/denom
                 + nt*(shr0_rhouy + shrmod_rhouy)*denomy /power<2,T>(denom));
      r_rhovxx = r_rtmp*(-ntx*(shr0_rhovx + shrmod_rhovx) / denom - nt*(shr0_rhovxx + shrmod_rhovxx)/denom
                 + nt*(shr0_rhovx + shrmod_rhovx)*denomx /power<2,T>(denom));

#endif
    }

    T r5 = power<5>(r);
    T r4 = power<4>(r);

    g      = r + cw2_*(power<6>(r) - r);
    T gx = rx + cw2_*(6.*r5*rx - rx);
    T gy = ry + cw2_*(6.*r5*ry - ry);
    T gz = rz + cw2_*(6.*r5*rz - rz);

    T g_rhox = r_rhox + cw2_*(6.*r5*r_rhox - r_rhox);
    T g_rhoy = r_rhoy + cw2_*(6.*r5*r_rhoy - r_rhoy);
    T g_rhoz = r_rhoz + cw2_*(6.*r5*r_rhoz - r_rhoz);

    T g_rhouy = r_rhouy + cw2_*(6.*r5*r_rhouy - r_rhouy);
    T g_rhouz = r_rhouz + cw2_*(6.*r5*r_rhouz - r_rhouz);
    T g_rhovx = r_rhovx + cw2_*(6.*r5*r_rhovx - r_rhovx);
    T g_rhovz = r_rhovz + cw2_*(6.*r5*r_rhovz - r_rhovz);
    T g_rhowx = r_rhowx + cw2_*(6.*r5*r_rhowx - r_rhowx);
    T g_rhowy = r_rhowy + cw2_*(6.*r5*r_rhowy - r_rhowy);

    T g_rhoxx = r_rhoxx + cw2_*(30.*r4*r_rhox*rx + 6.*r5*r_rhoxx - r_rhoxx);
    T g_rhoyy = r_rhoyy + cw2_*(30.*r4*r_rhoy*ry + 6.*r5*r_rhoyy - r_rhoyy);
    T g_rhozz = r_rhozz + cw2_*(30.*r4*r_rhoz*rz + 6.*r5*r_rhozz - r_rhozz);
    T g_rhouyy = r_rhouyy + cw2_*(30.*r4*r_rhouy*ry + 6.*r5*r_rhouyy - r_rhouyy);
    T g_rhouzz = r_rhouzz + cw2_*(30.*r4*r_rhouz*rz + 6.*r5*r_rhouzz - r_rhouzz);
    T g_rhovxx = r_rhovxx + cw2_*(30.*r4*r_rhovx*rx + 6.*r5*r_rhovxx - r_rhovxx);
    T g_rhovzz = r_rhovzz + cw2_*(30.*r4*r_rhovz*rz + 6.*r5*r_rhovzz - r_rhovzz);
    T g_rhowxx = r_rhowxx + cw2_*(30.*r4*r_rhowx*rx + 6.*r5*r_rhowxx - r_rhowxx);
    T g_rhowyy = r_rhowyy + cw2_*(30.*r4*r_rhowy*ry + 6.*r5*r_rhowyy - r_rhowyy);

    //fw = g * pow((1 + power<6>(cw3_)) / (power<6>(g) + power<6>(cw3_)), 1./6.);

    Real cw36 = power<6>(cw3_);
    T g6 = power<6>(g);
    T g5 = power<5>(g);
    T tmp = pow((1 + cw36) / (g6 + cw36), 1./6.);

    T tmpx = -(1 + cw36)*g5*gx/power<2,T>(g6 + cw36) / power<5>(tmp);
    T tmpy = -(1 + cw36)*g5*gy/power<2,T>(g6 + cw36) / power<5>(tmp);
    T tmpz = -(1 + cw36)*g5*gz/power<2,T>(g6 + cw36) / power<5>(tmp);

    T fw_g = tmp - g6 * (1 + cw36) / (power<5>(tmp)*power<2,T>( g6 + cw36 ));

    T fw_gx = tmpx - 6.*g5 * gx* (1 + cw36) / (power<5>(tmp)*power<2,T>( g6 + cw36 ))
                     +5*g6 * (1 + cw36)*tmpx / (power<6>(tmp)*power<2,T>( g6 + cw36 ))
                     +12*g6*(1+cw36)*g5*gx / (power<5>(tmp)*power<3,T>( g6 + cw36 ));

    T fw_gy = tmpy - 6*g5 * gy* (1 + cw36) / (power<5>(tmp)*power<2,T>( g6 + cw36 ))
                         +5*g6 * (1 + cw36)*tmpy / (power<6>(tmp)*power<2,T>( g6 + cw36 ))
                         +12*g6*(1+cw36)*g5*gy / (power<5>(tmp)*power<3,T>( g6 + cw36 ));

    T fw_gz = tmpz - 6*g5 * gz* (1 + cw36) / (power<5>(tmp)*power<2,T>( g6 + cw36 ))
                         +5*g6 * (1 + cw36)*tmpz / (power<6>(tmp)*power<2,T>( g6 + cw36 ))
                         +12*g6*(1+cw36)*g5*gz / (power<5>(tmp)*power<3,T>( g6 + cw36 ));

    T fw_rhox = fw_g*g_rhox;
    T fw_rhoy = fw_g*g_rhoy;
    T fw_rhoz = fw_g*g_rhoz;

    T fw_rhouy = fw_g*g_rhouy;
    T fw_rhouz = fw_g*g_rhouz;
    T fw_rhovx = fw_g*g_rhovx;
    T fw_rhovz = fw_g*g_rhovz;
    T fw_rhowx = fw_g*g_rhowx;
    T fw_rhowy = fw_g*g_rhowy;

    T fw_rhoxx = fw_gx*g_rhox + fw_g*g_rhoxx;
    T fw_rhoyy = fw_gy*g_rhoy + fw_g*g_rhoyy;
    T fw_rhozz = fw_gz*g_rhoz + fw_g*g_rhozz;

    T fw_rhouyy = fw_gy*g_rhouy + fw_g*g_rhouyy;
    T fw_rhouzz = fw_gz*g_rhouz + fw_g*g_rhouzz;
    T fw_rhovxx = fw_gx*g_rhovx + fw_g*g_rhovxx;
    T fw_rhovzz = fw_gz*g_rhovz + fw_g*g_rhovzz;
    T fw_rhowxx = fw_gx*g_rhowx + fw_g*g_rhowxx;
    T fw_rhowyy = fw_gy*g_rhowy + fw_g*g_rhowyy;

    ft2  = ct3_*exp( -ct4_*chi*chi );
    T ft2x = ct3_*exp( -ct4_*chi*chi )*(-ct4_*2*chi*chix);
    T ft2y = ct3_*exp( -ct4_*chi*chi )*(-ct4_*2*chi*chiy);
    T ft2z = ct3_*exp( -ct4_*chi*chi )*(-ct4_*2*chi*chiz);

    T wall_rhox = cw1_*fw_rhox*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhox*nt - cb1_*(1 - ft2)*shrmod_rhox*nt;
    T wall_rhoy = cw1_*fw_rhoy*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhoy*nt - cb1_*(1 - ft2)*shrmod_rhoy*nt;
    T wall_rhoz = cw1_*fw_rhoz*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhoz*nt - cb1_*(1 - ft2)*shrmod_rhoz*nt;

    T wall_rhouy = cw1_*fw_rhouy*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhouy*nt - cb1_*(1 - ft2)*shrmod_rhouy*nt;
    T wall_rhouz = cw1_*fw_rhouz*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhouz*nt - cb1_*(1 - ft2)*shrmod_rhouz*nt;
    T wall_rhovx = cw1_*fw_rhovx*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhovx*nt - cb1_*(1 - ft2)*shrmod_rhovx*nt;
    T wall_rhovz = cw1_*fw_rhovz*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhovz*nt - cb1_*(1 - ft2)*shrmod_rhovz*nt;
    T wall_rhowx = cw1_*fw_rhowx*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhowx*nt - cb1_*(1 - ft2)*shrmod_rhowx*nt;
    T wall_rhowy = cw1_*fw_rhowy*power<2,Tq>(nt/dist2) + cb1_*ft2*shr0_rhowy*nt - cb1_*(1 - ft2)*shrmod_rhowy*nt;

    T wall_rhoxx = cw1_*fw_rhoxx*power<2,Tq>(nt/dist2)+ cw1_*fw_rhox*2*(nt/dist2)*(ntx/dist2 -nt/dist2/dist2*distx);
    wall_rhoxx += cb1_*ft2x*shr0_rhox*nt + cb1_*ft2*shr0_rhoxx*nt + cb1_*ft2*shr0_rhox*ntx;
    wall_rhoxx += cb1_*ft2x*shrmod_rhox*nt - cb1_*(1 - ft2)*(shrmod_rhoxx*nt + shrmod_rhox*ntx);

    T wall_rhoyy = cw1_*fw_rhoyy*power<2,Tq>(nt/dist2)+ cw1_*fw_rhoy*2*(nt/dist2)*(nty/dist2 - nt/dist2/dist2*disty);
    wall_rhoyy += cb1_*ft2y*shr0_rhoy*nt + cb1_*ft2*shr0_rhoyy*nt + cb1_*ft2*shr0_rhoy*nty;
    wall_rhoyy += cb1_*ft2y*shrmod_rhoy*nt - cb1_*(1 - ft2)*(shrmod_rhoyy*nt + shrmod_rhoy*nty);

    T wall_rhozz = cw1_*fw_rhozz*power<2,Tq>(nt/dist2)+ cw1_*fw_rhoz*2*(nt/dist2)*(ntz/dist2 - nt/dist2/dist2*distz);
    wall_rhozz += cb1_*ft2z*shr0_rhoz*nt + cb1_*ft2*shr0_rhozz*nt + cb1_*ft2*shr0_rhoz*ntz;
    wall_rhozz += cb1_*ft2z*shrmod_rhoz*nt - cb1_*(1 - ft2)*(shrmod_rhozz*nt + shrmod_rhoz*ntz);

    T wall_rhouyy = cw1_*fw_rhouyy*power<2,Tq>(nt/dist2)+ cw1_*fw_rhouy*2*(nt/dist2)*(nty/dist2 - nt/dist2/dist2*disty);
    wall_rhouyy += cb1_*ft2y*shr0_rhouy*nt + cb1_*ft2*shr0_rhouyy*nt + cb1_*ft2*shr0_rhouy*nty;
    wall_rhouyy += cb1_*ft2y*shrmod_rhouy*nt - cb1_*(1 - ft2)*(shrmod_rhouyy*nt  + shrmod_rhouy*nty);
    T wall_rhouzz = cw1_*fw_rhouzz*power<2,Tq>(nt/dist2)+ cw1_*fw_rhouz*2*(nt/dist2)*(ntz/dist2 - nt/dist2/dist2*distz);
    wall_rhouzz += cb1_*ft2z*shr0_rhouz*nt + cb1_*ft2*shr0_rhouzz*nt + cb1_*ft2*shr0_rhouz*ntz;
    wall_rhouzz += cb1_*ft2z*shrmod_rhouz*nt - cb1_*(1 - ft2)*(shrmod_rhouzz*nt  + shrmod_rhouz*ntz);

    T wall_rhovxx = cw1_*fw_rhovxx*power<2,Tq>(nt/dist2)+ cw1_*fw_rhovx*2*(nt/dist2)*(ntx/dist2 -nt/dist2/dist2*distx);
    wall_rhovxx += cb1_*ft2x*shr0_rhovx*nt + cb1_*ft2*shr0_rhovxx*nt + cb1_*ft2*shr0_rhovx*ntx;
    wall_rhovxx += cb1_*ft2x*shrmod_rhovx*nt - cb1_*(1 - ft2)*(shrmod_rhovxx*nt + shrmod_rhovx*ntx);
    T wall_rhovzz = cw1_*fw_rhovzz*power<2,Tq>(nt/dist2)+ cw1_*fw_rhovz*2*(nt/dist2)*(ntz/dist2 -nt/dist2/dist2*distz);
    wall_rhovzz += cb1_*ft2z*shr0_rhovz*nt + cb1_*ft2*shr0_rhovzz*nt + cb1_*ft2*shr0_rhovx*ntz;
    wall_rhovzz += cb1_*ft2z*shrmod_rhovz*nt - cb1_*(1 - ft2)*(shrmod_rhovzz*nt + shrmod_rhovz*ntz);

    T wall_rhowxx = cw1_*fw_rhowxx*power<2,Tq>(nt/dist2)+ cw1_*fw_rhowx*2*(nt/dist2)*(ntx/dist2 -nt/dist2/dist2*distx);
    wall_rhowxx += cb1_*ft2x*shr0_rhowx*nt + cb1_*ft2*shr0_rhowxx*nt + cb1_*ft2*shr0_rhowx*ntx;
    wall_rhowxx += cb1_*ft2x*shrmod_rhowx*nt - cb1_*(1 - ft2)*(shrmod_rhowxx*nt + shrmod_rhowx*ntx);
    T wall_rhowyy = cw1_*fw_rhowyy*power<2,Tq>(nt/dist2)+ cw1_*fw_rhowy*2*(nt/dist2)*(nty/dist2 - nt/dist2/dist2*disty);
    wall_rhowyy += cb1_*ft2y*shr0_rhowy*nt + cb1_*ft2*shr0_rhowyy*nt + cb1_*ft2*shr0_rhowy*nty;
    wall_rhowyy += cb1_*ft2y*shrmod_rhowy*nt - cb1_*(1 - ft2)*(shrmod_rhowyy*nt  + shrmod_rhowy*nty);


//    dsdux(iSA, 0) += rho*wall_rhox;
    div_dsdgradu(iSA, 0) += rhox*wall_rhox + rho*wall_rhoxx;
//    dsdux(iSA, 2) += rho*wall_rhovx;
    div_dsdgradu(iSA, 2) += rhox*wall_rhovx + rho*wall_rhovxx;
//    dsdux(iSA, 3) += rho*wall_rhowx;
    div_dsdgradu(iSA, 3) += rhox*wall_rhowx + rho*wall_rhowxx;

//    dsduy(iSA, 0) += rho*wall_rhoy;
    div_dsdgradu(iSA, 0) += rhoy*wall_rhoy + rho*wall_rhoyy;
//    dsduy(iSA, 1) += rho*wall_rhouy;
    div_dsdgradu(iSA, 1) += rhoy*wall_rhouy + rho*wall_rhouyy;
//    dsduy(iSA, 3) += rho*wall_rhowy;
    div_dsdgradu(iSA, 3) += rhoy*wall_rhowy + rho*wall_rhowyy;

//    dsduz(iSA, 0) += rho*wall_rhoz;
    div_dsdgradu(iSA, 0) += rhoz*wall_rhoz + rho*wall_rhozz;
//    dsduz(iSA, 1) += rho*wall_rhouz;
    div_dsdgradu(iSA, 1) += rhoz*wall_rhouz + rho*wall_rhouzz;
//    dsduz(iSA, 2) += rho*wall_rhovz;
    div_dsdgradu(iSA, 2) += rhoz*wall_rhovz + rho*wall_rhovzz;
  }

  // non-conservative diffusion terms

  //nonc = cb2_*rho*(ntx*ntx + nty*nty + ntz*ntz)/sigma_;
//  T nonc_rhox   = -cb2_*2*nt*ntx/sigma_;
//  T nonc_rhoy   = -cb2_*2*nt*nty/sigma_;
//  T nonc_rhontx = cb2_*2*ntx/sigma_;
//  T nonc_rhonty = cb2_*2*nty/sigma_;

  T nonc_rhoxx = -cb2_*2.*(ntx*ntx + nt*ntxx)/sigma_;
  T nonc_rhoyy = -cb2_*2.*(nty*nty + nt*ntyy)/sigma_;
  T nonc_rhozz = -cb2_*2.*(ntz*ntz + nt*ntzz)/sigma_;
  T nonc_rhontxx = cb2_*2.*ntxx/sigma_;
  T nonc_rhontyy = cb2_*2.*ntyy/sigma_;
  T nonc_rhontzz = cb2_*2.*ntzz/sigma_;

  if (nt < 0)
  {
    Tq fn = (cn1_ + chi3) / (cn1_ - chi3);
    T fnx = (chi3x) /(cn1_ - chi3) + (cn1_+chi3)/(cn1_ - chi3)/(cn1_ - chi3)*(chi3x);
    T fny = (chi3y) /(cn1_ - chi3) + (cn1_+chi3)/(cn1_ - chi3)/(cn1_ - chi3)*(chi3y);
    T fnz = (chi3z) /(cn1_ - chi3) + (cn1_+chi3)/(cn1_ - chi3)/(cn1_ - chi3)*(chi3z);
    //nonc += - (mu/rho + nt*fn)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

//    nonc_rhox += - (mu/rho + nt*fn)*(rhontx - 2*nt*rhox)/rho/sigma_;
//    nonc_rhoy += - (mu/rho + nt*fn)*(rhonty - 2*nt*rhoy)/rho/sigma_;
//    nonc_rhontx += - (mu/rho + nt*fn)*(rhox/rho)/sigma_;
//    nonc_rhonty += - (mu/rho + nt*fn)*(rhoy/rho)/sigma_;

    nonc_rhoxx += - (mux/rho -mu/(rho*rho)*rhox + ntx*fn  + nt*fnx)*(rho*ntx - nt*rhox)/rho/sigma_
                  - (mu/rho + nt*fn)*(rho*ntxx - nt*rhoxx)/rho/sigma_
                  + (mu/rho + nt*fn)*(rho*ntx - nt*rhox)/(rho*rho)*rhox/sigma_;

    nonc_rhoyy += - (muy/rho -mu/(rho*rho)*rhoy + nty*fn + nt*fny)*(rho*nty - nt*rhoy)/rho/sigma_
                  - (mu/rho + nt*fn)*(rho*ntyy - nt*rhoyy)/rho/sigma_
                  + (mu/rho + nt*fn)*(rho*nty - nt*rhoy)/(rho*rho)*rhoy/sigma_;

    nonc_rhozz += - (muz/rho -mu/(rho*rho)*rhoz + ntz*fn + nt*fnz)*(rho*ntz - nt*rhoz)/rho/sigma_
                  - (mu/rho + nt*fn)*(rho*ntzz - nt*rhozz)/rho/sigma_
                  + (mu/rho + nt*fn)*(rho*ntz - nt*rhoz)/(rho*rho)*rhoz/sigma_;

    nonc_rhontxx += - (mux/rho  -mu/(rho*rho)*rhox + ntx*fn + nt*fnx)*(rhox/rho)/sigma_
                    - (mu/rho + nt*fn)*(rhoxx/rho)/sigma_
                    + (mu/rho + nt*fn)*(rhox*rhox/(rho*rho))/sigma_;

    nonc_rhontyy += - (muy/rho -mu/(rho*rho)*rhoy + nty*fn + nt*fny)*(rhoy/rho)/sigma_
                    - (mu/rho + nt*fn)*(rhoyy/rho)/sigma_
                    + (mu/rho + nt*fn)*(rhoy*rhoy/(rho*rho))/sigma_;

    nonc_rhontzz += - (muz/rho -mu/(rho*rho)*rhoz + ntz*fn + nt*fnz)*(rhoz/rho)/sigma_
                    - (mu/rho + nt*fn)*(rhozz/rho)/sigma_
                    + (mu/rho + nt*fn)*(rhoz*rhoz/(rho*rho))/sigma_;

  }
  else
  {
    //nonc += - (mu/rho + nt)*(rhox*ntx + rhoy*nty + rhoz*ntz)/sigma_;

//    nonc_rhox += - (mu/rho + nt)*(rhontx - 2*nt*rhox)/rho/sigma_;
//    nonc_rhoy += - (mu/rho + nt)*(rhonty - 2*nt*rhoy)/rho/sigma_;
//    nonc_rhontx += - (mu/rho + nt)*(rhox/rho)/sigma_;
//    nonc_rhonty += - (mu/rho + nt)*(rhoy/rho)/sigma_;


    nonc_rhoxx += - (mux/rho - mu/(rho*rho)*rhox + ntx)*(rho*ntx - nt*rhox)/rho/sigma_
                  - (mu/rho + nt)*(rho*ntxx - nt*rhoxx)/rho/sigma_
                  + (mu/rho + nt)*(rho*ntx - nt*rhox)/(rho*rho)*rhox/sigma_;

    nonc_rhoyy += - (muy/rho - mu/(rho*rho)*rhoy + nty)*(rho*nty - nt*rhoy)/rho/sigma_
                  - (mu/rho + nt)*(rho*ntyy - nt*rhoyy)/rho/sigma_
                  + (mu/rho + nt)*(rho*nty - nt*rhoy)/(rho*rho)*rhoy/sigma_;

    nonc_rhozz += - (muy/rho - mu/(rho*rho)*rhoz + ntz)*(rho*ntz - nt*rhoz)/rho/sigma_
                  - (mu/rho + nt)*(rho*ntzz - nt*rhozz)/rho/sigma_
                  + (mu/rho + nt)*(rho*ntz - nt*rhoz)/(rho*rho)*rhoz/sigma_;

    nonc_rhontxx += - (mux/rho - mu/(rho*rho)*rhox + ntx)*(rhox/rho)/sigma_
                    - (mu/rho + nt)*(rhoxx/rho - rhox*rhox/(rho*rho))/sigma_ ;
    nonc_rhontyy += - (muy/rho - mu/(rho*rho)*rhoy + nty)*(rhoy/rho)/sigma_
                    - (mu/rho + nt)*(rhoyy/rho - rhoy*rhoy/(rho*rho))/sigma_;
    nonc_rhontzz += - (muy/rho - mu/(rho*rho)*rhoz + ntz)*(rhoz/rho)/sigma_
                    - (mu/rho + nt)*(rhozz/rho - rhoz*rhoz/(rho*rho))/sigma_;

  }

//  dsdux(iSA, 0) += -nonc_rhox;
  div_dsdgradu(iSA, 0) -= nonc_rhoxx;
//  dsdux(iSA, 5) += -nonc_rhontx;
  div_dsdgradu(iSA, 5) -= nonc_rhontxx;

//  dsduy(iSA, 0) += -nonc_rhoy;
  div_dsdgradu(iSA, 0) -= nonc_rhoyy;
//  dsduy(iSA, 5) += -nonc_rhonty;
  div_dsdgradu(iSA, 5) -= nonc_rhontyy;

//  dsduz(iSA, 0) += -nonc_rhoz;
  div_dsdgradu(iSA, 0) -= nonc_rhozz;
//  dsduz(iSA, 5) += -nonc_rhontz;
  div_dsdgradu(iSA, 5) -= nonc_rhontzz;

  // NS source terms (nominally empty function)
  BaseType::jacobianGradientSourceGradient( x, y, z, time,
                                            q, qx, qy, qz,
                                            qxx, qxy, qyy,
                                            qxz, qyz, qzz,
                                            div_dsdgradu );
#endif
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceHACK(
    const Real& dist,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
{
  MatrixQ<Ts> tmpx = 0, tmpy=0, tmpz=0;
  jacobianGradientSource(dist, x, y, z, time, q, qx, qy, qz, tmpx, tmpy, tmpz);

  jacobianHelperHACK(x, y, z, time, q, tmpx, dsdux);
  jacobianHelperHACK(x, y, z, time, q, tmpy, dsduy);
  jacobianHelperHACK(x, y, z, time, q, tmpz, dsduz);
}

// linear change in source in response to linear perturbations du, dux, duy, duz
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tu, class Tug, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::perturbedSource(
    const Real& dist, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q,  const ArrayQ<Tg>& qx,  const ArrayQ<Tg>& qy,  const ArrayQ<Tg>& qz,
    const ArrayQ<Tu>& du, const ArrayQ<Tug>& dux, const ArrayQ<Tug>& duy, const ArrayQ<Tug>& duz,
    ArrayQ<Ts>& dS ) const
{
  ArrayQ<Ts> dsdu_row = 0;
  ArrayQ<Ts> dsdux_row = 0, dsduy_row = 0, dsduz_row = 0;

  jacobianSourceRow(dist, x, y, z, time, q, qx, qy, qz, dsdu_row);
  jacobianGradientSourceRow( dist, x, y, z, time, q, qx, qy, qz, dsdux_row, dsduy_row, dsduz_row );

  dS[iSA] += dot(dsdu_row, du);

  dS[iSA] += dsdux_row[0]*dux[0] + dsdux_row[2]*dux[2] + dsdux_row[3]*dux[3] + dsdux_row[5]*dux[5];
  dS[iSA] += dsduy_row[0]*duy[0] + dsduy_row[1]*duy[1] + dsduy_row[3]*duy[3] + dsduy_row[5]*duy[5];
  dS[iSA] += dsduz_row[0]*duz[0] + dsduz_row[1]*duz[1] + dsduz_row[2]*duz[2] + dsduz_row[5]*duz[5];

  // NS source terms (nominally empty function)
  BaseType::perturbedSource( x, y, z, time, q, qx, qy, qz, du, dux, duy, duz, dS );
}


// dual-consistent source
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::sourceTrace(
    const Real& distL, const Real& xL, const Real& yL, const Real& zL,
    const Real& distR, const Real& xR, const Real& yR, const Real& zR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  SANS_DEVELOPER_EXCEPTION("PDERANSSA3D<PDETraitsSize, PDETraitsModel>::sourceTrace not implemented");
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& dist,
    const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<T>& forcing ) const
{
  SANS_ASSERT( soln_ != nullptr );

  ArrayQ<Real> q = 0;
  ArrayQ<Real> qx = 0, qy = 0, qz = 0, qt = 0;
  ArrayQ<Real> qxx = 0,
               qyx = 0, qyy = 0,
               qzx = 0, qzy = 0, qzz = 0;


  soln_->secondGradient( x, y, z, time,
                         q,
                         qx, qy, qz, qt,
                         qxx,
                         qyx, qyy,
                         qzx, qzy, qzz);

  //strongFluxAdvectiveTime(q, qt, forcing);
  strongFluxAdvective(dist, x, y, z, time, q, qx, qy, qz, forcing);
  strongFluxViscous(dist, x, y, z, time, q, qx, qy, qz, qxx, qyx, qyy, qzx, qzy, qzz, forcing);
  source(dist, x, y, z, time, q, qx, qy, qz, forcing);
}


// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  BaseType::interpResidVariable(rsdPDEIn, rsdPDEOut);

  if (cat_==Euler_ResidInterp_Raw)
  {
    rsdPDEOut[iSA] = rsdPDEIn[iSA];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    rsdPDEOut[iSA-2] = rsdPDEIn[iSA];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {

    Real tmp = rsdPDEOut[0]*rsdPDEOut[0];
    rsdPDEOut[0] = sqrt(tmp + rsdPDEIn[iSA]*rsdPDEIn[iSA] );
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{

  BaseType::interpResidGradient(rsdAuxIn, rsdAuxOut);

  //TEMPORARY SOLUTION FOR AUX VARIABLE
  if (cat_==Euler_ResidInterp_Raw)
  {
    rsdAuxOut[iSA] = rsdAuxIn[iSA];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    rsdAuxOut[iSA-2] = rsdAuxIn[iSA];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    rsdAuxOut[0] = sqrt(rsdAuxOut[0]*rsdAuxOut[0] + rsdAuxIn[iSA]*rsdAuxOut[iSA] );
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  BaseType::interpResidBC(rsdBCIn, rsdBCOut);

  if (cat_==Euler_ResidInterp_Raw)
  {
    rsdBCOut[iSA] = rsdBCIn[iSA];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    rsdBCOut[iSA-2] = rsdBCIn[iSA];
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    Real tmp = rsdBCOut[0]*rsdBCOut[0];
    rsdBCOut[0] = sqrt(tmp + rsdBCIn[iSA]*rsdBCIn[iSA] );
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}



// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpret_.setFromPrimitive( q, data, name, nn );
}

// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class T, class Variables>
inline void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::setDOFFrom( ArrayQ<T>& q, const SAVariableType3D<Variables>& data ) const
{
  qInterpret_.setFromPrimitive( q, data.cast() );
}

// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Variables>
inline typename PDERANSSA3D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const SAVariableType3D<Variables>& data ) const
{
  ArrayQ<Real> q;
  qInterpret_.setFromPrimitive( q, data.cast() );
  return q;
}

// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDERANSSA3D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q;
  DictKeyPair data = d.get(SAVariableType3DParams::params.StateVector);

  if ( data == SAVariableType3DParams::params.StateVector.Conservative )
    qInterpret_.setFromPrimitive( q, SAnt3D<Conservative3D<Real>>(data) );
  else if ( data == SAVariableType3DParams::params.StateVector.PrimitivePressure )
    qInterpret_.setFromPrimitive( q, SAnt3D<DensityVelocityPressure3D<Real>>(data) );
  else if ( data == SAVariableType3DParams::params.StateVector.PrimitiveTemperature )
    qInterpret_.setFromPrimitive( q, SAnt3D<DensityVelocityTemperature3D<Real>>(data) );
  else
    SANS_DEVELOPER_EXCEPTION(" Missing state vector option ");

  return q;
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDERANSSA3D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDERANSSA3D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDERANSSA3D<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  this->qInterpret_.dump(indentSize+2, out);
  out << indent << "PDERANSSA3D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  this->gas_.dump(indentSize+2, out);
  out << indent << "PDERANSSA3D<PDETraitsSize, PDETraitsModel>: visc_ = " << std::endl;
  this->visc_.dump(indentSize+2, out);
  out << indent << "PDERANSSA3D<PDETraitsSize, PDETraitsModel>: tcond_ = " << std::endl;
  this->tcond_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // PDERANSSA3D_H
