// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSBOLTZMANN2D_H
#define TRAITSBOLTZMANN2D_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "UserVariables/BoltzmannNVar.h"
#include "UserVariables/BoltzmannVelocity.h"


namespace SANS
{

template <class PhysDim_>
class TraitsSizeBoltzmann2D
{
public:
  typedef PhysDim_ PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = NVar;                   // total solution variables

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;           // solution/residual arrays

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>;        // matrices
};


template <class QType_, class GasModel_>
class TraitsModelBoltzmann2D
{
public:
  typedef QType_ QType;                       // solution variables set

  typedef GasModel_ GasModel;                 // gas model
};

}

#endif // TRAITSBOLTZMANN2D_H
