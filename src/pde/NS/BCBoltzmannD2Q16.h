// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBOLTZMANND2Q16_H
#define BCBOLTZMANND2Q16_H

// 1-D Euler BC class

//PyDict must be included first
#include <boost/mpl/vector.hpp>
#include <boost/mpl/vector/vector30.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "TraitsBoltzmannD2Q16.h"
#include "PDEBoltzmannD2Q16.h"

#include "BCBoltzmannD2Q16_BounceBackDirichlet.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

/*class BCTypeNone;
class BCTypeTimeOut;
class BCTypeReflect;
class BCTypeInflowSupersonic;
class BCTypeOutflowSubsonic_Pressure;*/

template <class BCType, class PDEBoltzmannD2Q16>
class BCBoltzmannD2Q16;

template <class BCType>
struct BCBoltzmannD2Q16Params;


template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCBoltzmannD2Q16Vector = boost::mpl::vector2<
    BCBoltzmannD2Q16<BCTypeNone, PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>>,
    BCBoltzmannD2Q16<BCTypeDiffuseKinetic, PDEBoltzmannD2Q16<PDETraitsSize, PDETraitsModel>>
                                                 >;

} //namespace SANS

#endif  // BCBOLTZMANND2Q16_H
