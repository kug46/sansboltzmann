// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q3DCONSERVATIVE_H
#define Q3DCONSERVATIVE_H

// solution interpreter class
// Euler/N-S conservation-eqns: conservation variables (rho, rho*u, rho*v, rho*E)

#include <vector>
#include <string>

#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "GasModel.h"
#include "pde/NS/NSVariable3D.h"
#include "Surreal/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D Euler/N-S
// conservative variables (rho, rhou, rhov, rhoE)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//     MatrixQ            solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// conservative variables (rho, rhou, rhov, rhoE)
class QTypeConservative;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q3D;


template <template <class> class PDETraitsSize>
class Q3D<QTypeConservative, PDETraitsSize>
{
public:
  typedef PhysD3 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int irho = 0;
  static const int irhou = 1;
  static const int irhov = 2;
  static const int irhow = 3;
  static const int irhoE = 4;

  // The three components of the state vector that make up the momentum vector
  static const int ix = irhou;
  static const int iy = irhov;
  static const int iz = irhow;

  static std::vector<std::string> stateNames() { return {"rho", "rhou", "rhov", "rhoE"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  explicit Q3D( const GasModel& gas0 ) : gas_(gas0) {}
  Q3D( const Q3D& q0 ) : gas_(q0.gas_) {}
  ~Q3D() {}

  Q3D& operator=( const Q3D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& v, T& w, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u, T& v, T& w ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalDensityJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                                         ArrayQ<T>& u_q, ArrayQ<T>& v_q, ArrayQ<T>& w_q,
                                         ArrayQ<T>& t_q ) const;

  template <class T>
  void conservativeJacobian( const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& duxdqx ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type& ux,
    typename promote_Surreal<Tq,Tg>::type& vx,
    typename promote_Surreal<Tq,Tg>::type& wx,
    typename promote_Surreal<Tq,Tg>::type& tx ) const;

  template<class Tq, class Tg, class Th>
  void evalSecondGradient(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,

      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& rhoH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& uH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& vH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& wH,
      DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& tH ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure3D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature3D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative3D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& v, T& w, T& t ) const
{
  rho = q(irho);
  u   = q(irhou)/rho;
  v   = q(irhov)/rho;
  w   = q(irhow)/rho;
  t   = (q(irhoE)/rho - 0.5*(u*u+v*v+w*w) ) / ( gas_.Cv() );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  rho = q(irho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u, T& v, T& w ) const
{
  u = q(irhou)/q(irho);
  v = q(irhov)/q(irho);
  w = q(irhow)/q(irho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  const T& rho  = q(irho);
  const T& rhou = q(irhou);
  const T& rhov = q(irhov);
  const T& rhow = q(irhow);
  const T& rhoE = q(irhoE);

  t   = ( rhoE/rho - 0.5*( rhou*rhou + rhov*rhov + rhow*rhow )/(rho*rho) ) / ( gas_.Cv() );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::evalDensityJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const
{
  rho_q = 0;
  rho_q[0] = 1;  // drho/drho
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                        ArrayQ<T>& u_q, ArrayQ<T>& v_q, ArrayQ<T>& w_q,
                        ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; v_q = 0; w_q = 0; t_q = 0;

  const T& rho  = q(irho);
  const T& rhou = q(irhou);
  const T& rhov = q(irhov);
  const T& rhow = q(irhow);
  const T& rhoE = q(irhoE);

  rho_q[0] = 1;  // drho/drho

  u_q[0] = -rhou/(rho*rho); // du/drho
  u_q[1] = 1/rho;           // du/drhou

  v_q[0] = -rhov/(rho*rho); // dv/drho
  v_q[2] = 1/rho;           // dv/drhov

  w_q[0] = -rhow/(rho*rho); // dw/drho
  w_q[3] = 1/rho;           // dw/drhow

  t_q[0] = ( -rhoE/(rho*rho) + ( rhou*rhou + rhov*rhov + rhow*rhow )/(rho*rho*rho) ) / ( gas_.Cv() ); // dt/drho
  t_q[1] = ( -( rhou )/(rho*rho) ) / ( gas_.Cv() );                                                   // dt/drhou
  t_q[2] = ( -( rhov )/(rho*rho) ) / ( gas_.Cv() );                                                   // dt/drhov
  t_q[3] = ( -( rhow )/(rho*rho) ) / ( gas_.Cv() );                                                   // dt/drhow
  t_q[4] = ( 1/rho ) / ( gas_.Cv() );                                                                 // dt/drhoE
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::conservativeJacobian(
    const ArrayQ<T>& q, const ArrayQ<T>& qx, MatrixQ<T>& duxdqx ) const
{
  //dUX/dQX
  duxdqx = 0;
  duxdqx(irho ,irho ) = 1;
  duxdqx(irhou,irhou) = 1;
  duxdqx(irhov,irhov) = 1;
  duxdqx(irhow,irhow) = 1;
  duxdqx(irhoE,irhoE) = 1;
}


template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
Q3D<QTypeConservative, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type&   ux,
    typename promote_Surreal<Tq,Tg>::type&   vx,
    typename promote_Surreal<Tq,Tg>::type&   wx,
    typename promote_Surreal<Tq,Tg>::type&   tx ) const
{
  const Tq& rho  = q(irho);
            rhox = qx(irho);

  const Tq& rhou  = q(irhou);
  const Tg& rhoux = qx(irhou);

  const Tq& rhov  = q(irhov);
  const Tg& rhovx = qx(irhov);

  const Tq& rhow  = q(irhow);
  const Tg& rhowx = qx(irhow);

  const Tq& rhoE  = q(irhoE);
  const Tg& rhoEx = qx(irhoE);

  ux   = rhoux/rho - rhou*rhox/(rho*rho);
  vx   = rhovx/rho - rhov*rhox/(rho*rho);
  wx   = rhowx/rho - rhow*rhox/(rho*rho);

  tx = (rhou*rhou + rhov*rhov + rhow*rhow)*rhox/(rho*rho*rho)
            - (rhou*rhoux + rhov*rhovx + rhow*rhowx)/(rho*rho) + rhoEx/rho - rhoE*rhox/(rho*rho);
  tx /= ( gas_.Cv() );
}


template <template <class> class PDETraitsSize>
template<class Tq, class Tg, class Th>
inline void
Q3D<QTypeConservative, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,

    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& rhoH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& uH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& vH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& wH,
    DLA::MatrixSymS<D, typename promote_Surreal<Tq, Tg, Th>::type>& tH ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;

  Tq rho, u, v, w, t;
  Tqg rhox, ux, vx, wx, tx;
  Tqg rhoy, uy, vy, wy, ty;
  Tqg rhoz, uz, vz, wz, tz;

  const Tq                   &rhoux = qx(irhou), &rhouy = qy(irhou), &rhouz = qz(irhou);
  const Tq                   &rhovx = qx(irhov), &rhovy = qy(irhov), &rhovz = qz(irhov);
  const Tq                   &rhowx = qx(irhow), &rhowy = qy(irhow), &rhowz = qz(irhow);
  const Tq &rhoE = q(irhoE), &rhoEx = qx(irhoE), &rhoEy = qy(irhoE), &rhoEz = qz(irhoE);

  rhoH(0,0) = qxx(irho);
  rhoH(1,0) = qxy(irho); rhoH(1,1) = qyy(irho);
  rhoH(2,0) = qxz(irho); rhoH(2,1) = qyz(irho); rhoH(2,2) = qzz(irho);

  const Tq &rhouxx = qxx(irhou);
  const Tq &rhouxy = qxy(irhou), &rhouyy = qyy(irhou);
  const Tq &rhouxz = qxz(irhou), &rhouyz = qyz(irhou), &rhouzz = qzz(irhou);

  const Tq &rhovxx = qxx(irhov);
  const Tq &rhovxy = qxy(irhov), &rhovyy = qyy(irhov);
  const Tq &rhovxz = qxz(irhov), &rhovyz = qyz(irhov), &rhovzz = qzz(irhov);

  const Tq &rhowxx = qxx(irhow);
  const Tq &rhowxy = qxy(irhow), &rhowyy = qyy(irhow);
  const Tq &rhowxz = qxz(irhow), &rhowyz = qyz(irhow), &rhowzz = qzz(irhow);

  const Tq &rhoExx = qxx(irhoE);
  const Tq &rhoExy = qxy(irhoE), &rhoEyy = qyy(irhoE);
  const Tq &rhoExz = qxz(irhoE), &rhoEyz = qyz(irhoE), &rhoEzz = qzz(irhoE);

  eval(q, rho, u, v, w, t);
  evalGradient(q, qx, rhox, ux, vx, wx, tx);
  evalGradient(q, qy, rhoy, uy, vy, wy, ty);
  evalGradient(q, qz, rhoz, uz, vz, wz, tz);

  Tq rho2 = rho*rho;
  Tq rho3 = rho*rho*rho;

  // Diagonal terms
  uH(0,0) = 2.0*u*rhox*rhox/rho2 - 2.0*rhox * rhoux/rho2 - u*rhoH(0,0)/rho + rhouxx/rho;
  uH(1,1) = 2.0*u*rhoy*rhoy/rho2 - 2.0*rhoy * rhouy/rho2 - u*rhoH(1,1)/rho + rhouyy/rho;
  uH(2,2) = 2.0*u*rhoz*rhoz/rho2 - 2.0*rhoz * rhouz/rho2 - u*rhoH(2,2)/rho + rhouzz/rho;

  // Lower triangular
  uH(1,0) = 2.0*u*rhoy*rhox/rho2 - rhouy*rhox/rho2 - rhoy*rhoux/rho2 - u*rhoH(1,0)/rho + rhouxy/rho;
  uH(2,0) = 2.0*u*rhoz*rhox/rho2 - rhouz*rhox/rho2 - rhoz*rhoux/rho2 - u*rhoH(2,0)/rho + rhouxz/rho;
  uH(2,1) = 2.0*u*rhoz*rhoy/rho2 - rhouz*rhoy/rho2 - rhoz*rhouy/rho2 - u*rhoH(2,1)/rho + rhouyz/rho;

  // Diagonal terms
  vH(0,0) = 2.0*v*rhox*rhox/rho2 - 2.0*rhox * rhovx/rho2 - v*rhoH(0,0)/rho + rhovxx/rho;
  vH(1,1) = 2.0*v*rhoy*rhoy/rho2 - 2.0*rhoy * rhovy/rho2 - v*rhoH(1,1)/rho + rhovyy/rho;
  vH(2,2) = 2.0*v*rhoz*rhoz/rho2 - 2.0*rhoz * rhovz/rho2 - v*rhoH(2,2)/rho + rhovzz/rho;

  // Lower triangular
  vH(1,0) = 2.0*v*rhoy*rhox/rho2 - rhovy*rhox/rho2 - rhoy*rhovx/rho2 - v*rhoH(1,0)/rho + rhovxy/rho;
  vH(2,0) = 2.0*v*rhoz*rhox/rho2 - rhovz*rhox/rho2 - rhoz*rhovx/rho2 - v*rhoH(2,0)/rho + rhovxz/rho;
  vH(2,1) = 2.0*v*rhoz*rhoy/rho2 - rhovz*rhoy/rho2 - rhoz*rhovy/rho2 - v*rhoH(2,1)/rho + rhovyz/rho;

  // Diagonal terms
  wH(0,0) = 2.0*w*rhox*rhox/rho2 - 2.0*rhox * rhowx/rho2 - w*rhoH(0,0)/rho + rhowxx/rho;
  wH(1,1) = 2.0*w*rhoy*rhoy/rho2 - 2.0*rhoy * rhowy/rho2 - w*rhoH(1,1)/rho + rhowyy/rho;
  wH(2,2) = 2.0*w*rhoz*rhoz/rho2 - 2.0*rhoz * rhowz/rho2 - w*rhoH(2,2)/rho + rhowzz/rho;

  // Lower triangular
  wH(1,0) = 2.0*w*rhoy*rhox/rho2 - rhowy*rhox/rho2 - rhoy*rhowx/rho2 - w*rhoH(1,0)/rho + rhowxy/rho;
  wH(2,0) = 2.0*w*rhoz*rhox/rho2 - rhowz*rhox/rho2 - rhoz*rhowx/rho2 - w*rhoH(2,0)/rho + rhowxz/rho;
  wH(2,1) = 2.0*w*rhoz*rhoy/rho2 - rhowz*rhoy/rho2 - rhoz*rhowy/rho2 - w*rhoH(2,1)/rho + rhowyz/rho;

  // Diagonal terms
  tH(0,0) = 2.*rhoE*rhox*rhox/rho3 - 2.0*rhox*rhoEx/rho2 - (ux*ux+vx*vx+wx*wx +u*uH(0,0) +v*vH(0,0) +w*wH(0,0)) - rhoE*rhoH(0,0)/rho2 + rhoExx/rho;
  tH(1,1) = 2.*rhoE*rhoy*rhoy/rho3 - 2.0*rhoy*rhoEy/rho2 - (uy*uy+vy*vy+wy*wy +u*uH(1,1) +v*vH(1,1) +w*wH(1,1)) - rhoE*rhoH(1,1)/rho2 + rhoEyy/rho;
  tH(2,2) = 2.*rhoE*rhoz*rhoz/rho3 - 2.0*rhoz*rhoEz/rho2 - (uz*uz+vz*vz+wz*wz +u*uH(2,2) +v*vH(2,2) +w*wH(2,2)) - rhoE*rhoH(2,2)/rho2 + rhoEzz/rho;

  // Lower triangular
  tH(1,0) = 2*rhoE*rhox*rhoy/rho3-(rhoEy*rhox+rhoy*rhoEx)/rho2-(uy*ux+vy*vx+wy*wx+u*uH(1,0)+v*vH(1,0)+w*wH(1,0))-rhoE*rhoH(1,0)/rho2+rhoExy/rho;
  tH(2,0) = 2*rhoE*rhox*rhoz/rho3-(rhoEz*rhox+rhoz*rhoEx)/rho2-(uz*ux+vz*vx+wz*wx+u*uH(2,0)+v*vH(2,0)+w*wH(2,0))-rhoE*rhoH(2,0)/rho2+rhoExz/rho;
  tH(2,1) = 2*rhoE*rhoy*rhoz/rho3-(rhoEz*rhoy+rhoz*rhoEy)/rho2-(uz*uy+vz*vy+wz*wy+u*uH(2,1)+v*vH(2,1)+w*wH(2,1))-rhoE*rhoH(2,1)/rho2+rhoEyz/rho;

  tH /= ( gas_.Cv() );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 5);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "VelocityY") &&
              (name[3] == "VelocityZ") &&
              (name[4] == "Temperature"));

  T rho, u, v, w, t = 0;

  rho = data[0];
  u   = data[1];
  v   = data[2];
  w   = data[3];
  t   = data[4];

  q(irho)  = rho;
  q(irhou) = rho*u;
  q(irhov) = rho*v;
  q(irhow) = rho*w;
  q(irhoE) = rho*( gas_.energy(rho,t) + 0.5*(u*u + v*v + w*w) );
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure3D<T>& data ) const
{
  T rho, u, v, w, t, p, E = 0;

  rho = data.Density;
  u = data.VelocityX;
  v = data.VelocityY;
  w = data.VelocityZ;
  p = data.Pressure;
  t = p / (gas_.R()*rho);
  E = gas_.energy(rho, t) + 0.5*(u*u + v*v + w*w);

  q(irho)  = rho;
  q(irhou) = rho*u;
  q(irhov) = rho*v;
  q(irhow) = rho*w;
  q(irhoE) = rho*E;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature3D<T>& data ) const
{
  T rho, u, v, w, t, E = 0;

  rho = data.Density;
  u = data.VelocityX;
  v = data.VelocityY;
  w = data.VelocityZ;
  t = data.Temperature;
  E = gas_.energy(rho, t) + 0.5*(u*u + v*v + w*w);

  q(irho)  = rho;
  q(irhou) = rho*u;
  q(irhov) = rho*v;
  q(irhow) = rho*w;
  q(irhoE) = rho*E;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q3D<QTypeConservative, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative3D<T>& data ) const
{
  q(irho)  = data.Density;
  q(irhou) = data.MomentumX;
  q(irhov) = data.MomentumY;
  q(irhow) = data.MomentumZ;
  q(irhoE) = data.TotalEnergy;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q3D<QTypeConservative, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not tested...");
#if 0
  Real nrho, nrhou, nrhov, nrhow, nrhoE, nrhoe;

  const Real rho  = q(irho);
  const Real rhou = q(irhou);
  const Real rhov = q(irhov);
  const Real rhow = q(irhow);
  const Real rhoE = q(irhoE);

  const Real drho  = dq(irho);
  const Real drhou = dq(irhou);
  const Real drhov = dq(irhov);
  const Real drhow = dq(irhow);
  const Real drhoE = dq(irhoE);

  Real rhoV2 = (rhou*rhou + rhov*rhov + rhow*rhow);
  Real rhoe = rhoE - 0.5*rhoV2/rho;

  Real wr = 1;

  // density check
  nrho = rho - drho;

  // Compute wr such that:

  //(1-maxChangeFraction)*rho >= rho - wr*drho
  if (nrho < (1-maxChangeFraction)*rho)
    wr =  maxChangeFraction*rho/drho;

  //(1+maxChangeFraction)*rho <= rho - wr*drho
  if (nrho > (1+maxChangeFraction)*rho)
    wr = -maxChangeFraction*rho/drho;

  updateFraction = wr;

  // internal energy check
  nrho  = rho  - wr * drho;
  nrhou = rhou - wr * drhou;
  nrhov = rhov - wr * drhov;
  nrhow = rhow - wr * drhow;
  nrhoE = rhoE - wr * drhoE;

  nrhoe = nrhoE - 0.5*(nrhou*nrhou + nrhov*nrhov + nrhow*nrhow)/nrho;

  // quadratic for internal energy change
  Real w1, w2, w3, w4;
  w1 = w2 = w3 = w4 = 0;
  if ((nrhoe < (1 - maxChangeFraction)*rhoe) || (nrhoe > (1 + maxChangeFraction)*rhoe))
  {
    Real a, b, c;

    // quadratic eqn: nrhoe - (1 + sgn*maxChangeFraction)*rhoe = 0

    // TODO: The equations below are derived for nrho  = rho + wr * drho, which was used in PX
    // However, SANS has the negative step, i.e. nrho  = rho - wr * drho. Not sure which signs need to be flipped...

    a = rho*(2*drho*drhoE - (drhou*drhou + drhov*drhov + drhow*drhow));
    b = rho*(   rho*drhoE - ( rhou*drhou +  rhov*drhov +  rhow*drhow) - maxChangeFraction*drho*rhoE)
        + 0.5*(1 + maxChangeFraction)*drho*rhoV2;
    c = -maxChangeFraction*rho*(2*rho*rhoE - rhoV2);

    if (a != 0)
    {
      if (b*b - a*c >= 0)
      {
        w1 = (-b - sqrt(b*b - a*c))/a;
        w2 = (-b + sqrt(b*b - a*c))/a;
      }
      else
      {
        w1 = -1;
        w2 = -1;
      }
    }
    else if (b != 0)
    {
      w1 = -c/(2*b);
      w2 = w1;
    }
    else
    {
      std::stringstream errmsg;
      errmsg << "Error, quadratic eqn has a & b zero; what to do?" << std::endl;
      errmsg << " U = " <<  rho << " " <<  rhou << " " <<  rhov << " " <<  rhow << " " <<  rhoE << " " << std::endl;
      errmsg << "dU = " << drho << " " << drhou << " " << drhov << " " << drhow << " " << drhoE << " " << std::endl;
      SANS_DEVELOPER_EXCEPTION(errmsg.str());
    }

    a = rho*(2*drho*drhoE - (drhou*drhou + drhov*drhov + drhow*drhow));
    b = rho*(   rho*drhoE - ( rhou*drhou +  rhov*drhov +  rhow*drhow) + maxChangeFraction*drho*rhoE)
        + 0.5*(1 - maxChangeFraction)*drho*rhoV2;
    c = +maxChangeFraction*rho*(2*rho*rhoE - rhoV2);

    if (a != 0)
    {
      if (b*b - a*c >= 0)
      {
        w3 = (-b - sqrt(b*b - a*c))/a;
        w4 = (-b + sqrt(b*b - a*c))/a;
      }
      else
      {
        w3 = -1;
        w4 = -1;
      }
    }
    else if (b != 0)
    {
      w3 = -c/(2*b);
      w4 = w3;
    }
    else
    {
      std::stringstream errmsg;
      errmsg << "Error, quadratic eqn has a & b zero; what to do?" << std::endl;
      errmsg << " U = " <<  rho << " " <<  rhou << " " <<  rhov << " " <<  rhow << " " <<  rhoE << " " << std::endl;
      errmsg << "dU = " << drho << " " << drhou << " " << drhov << " " << drhow << " " << drhoE << " " << std::endl;
      SANS_DEVELOPER_EXCEPTION(errmsg.str());
    }

    if (w1 < wr) updateFraction = w1;
    if (w2 < wr) updateFraction = MAX( updateFraction, w2 );
    if (w3 < wr) updateFraction = MAX( updateFraction, w3 );
    if (w4 < wr) updateFraction = MAX( updateFraction, w4 );

    if (updateFraction < 0) updateFraction = wr;
  }
#endif
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
Q3D<QTypeConservative, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  Real rho = 0, t = 0;

  evalDensity(q, rho);
  if (rho <= 0)
    return false;

  evalTemperature(q, t);
  if (t <= 0)
    return false;

  return true;
}


template <template <class> class PDETraitsSize>
void
Q3D<QTypeConservative, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q3D<QTypeConservative, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q3D<QTypeConservative, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // Q3DCONSERVATIVE_H
