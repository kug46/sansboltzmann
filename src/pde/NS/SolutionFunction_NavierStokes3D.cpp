// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/NS/SolutionFunction_NavierStokes3D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_NavierStokes3D_TaylorGreen_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back( d.checkInputs(params.gasModel));
  allParams.push_back( d.checkInputs (params.rhoRef));
  allParams.push_back( d.checkInputs (params.qRef));
  allParams.push_back( d.checkInputs (params.pRef));
  allParams.push_back( d.checkInputs (params.lRef));
  d.checkUnknownInputs(allParams);
}

SolutionFunction_NavierStokes3D_TaylorGreen_Params SolutionFunction_NavierStokes3D_TaylorGreen_Params::params;


// cppcheck-suppress passedByValue
void SolutionFunction_NavierStokes3D_OjedaMMS_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gasModel));
  d.checkUnknownInputs(allParams);
}

SolutionFunction_NavierStokes3D_OjedaMMS_Params SolutionFunction_NavierStokes3D_OjedaMMS_Params::params;

}
