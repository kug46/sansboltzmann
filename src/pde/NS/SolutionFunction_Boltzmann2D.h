// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_BOLTZMANN2D_H
#define SOLUTIONFUNCTION_BOLTZMANN2D_H

// quasi 1-D Euler PDE: exact and MMS solutions

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // exp

#include "tools/SANSnumerics.h"     // Real

#include "PDEBoltzmann2D.h"
#include "pde/AnalyticFunction/Function2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "TraitsBoltzmann2D.h"

#include "getBoltzmann2DTraitsModel.h"

#include "UserVariables/BoltzmannNVar.h"
#include "UserVariables/BoltzmannVelocity.h"
//#include "LinearAlgebra/AlgebraicEquationSetBase.h"
//#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q2D;

//----------------------------------------------------------------------------//
// solution: Gaussian

struct SolutionFunction_Boltzmann2D_Gaussian_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Amplitude of Gaussian"};
  const ParameterNumeric<Real> sigma_x{"sigma_x", NO_DEFAULT, NO_RANGE, "Spread in x"};
  const ParameterNumeric<Real> sigma_y{"sigma_y", NO_DEFAULT, NO_RANGE, "Spread in y"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Boltzmann2D_Gaussian_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Boltzmann2D_Gaussian :
    public Function2DVirtualInterface<SolutionFunction_Boltzmann2D_Gaussian<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_Boltzmann2D_Gaussian_Params ParamsType;

  typedef typename getBoltzmann2DTraitsModel<PDETraitsModel>::type TraitsModel;

  //typedef PDETraitsModel TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Boltzmann2D_Gaussian( const PyDict& d ) :
    qInterpret_(GasModel( d.get(ParamsType::params.gasModel) )),
    A_(d.get(ParamsType::params.A)),
    sigma_x_(d.get(ParamsType::params.sigma_x)),
    sigma_y_(d.get(ParamsType::params.sigma_y))
  {
    // Nothing
  }

  explicit SolutionFunction_Boltzmann2D_Gaussian( const GasModel& gas, const Real& A, const Real& sigma_x, const Real& sigma_y  ) :
    qInterpret_(gas),
    A_(A),
    sigma_x_(sigma_x),
    sigma_y_(sigma_y)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> q = 0;
    T val = A_*exp(-0.5*((x*x)/(sigma_x_*sigma_x_) + (y*y)/(sigma_y_*sigma_x_)));

    T data[NVar];
    for (int i=0;i<NVar;i++)
      data[i] = ew[i][2]*val;
    qInterpret_.setFromPrimitive(q, data);
    return q;
  }

private:
  const QInterpret qInterpret_;
  const Real A_;
  const Real sigma_x_;
  const Real sigma_y_;
};

//----------------------------------------------------------------------------//
// solution: WeightedDensity

struct SolutionFunction_Boltzmann2D_WeightedDensity_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> Rho{"Rho", NO_DEFAULT, NO_RANGE, "Density"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Boltzmann2D_WeightedDensity_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Boltzmann2D_WeightedDensity :
    public Function2DVirtualInterface<SolutionFunction_Boltzmann2D_WeightedDensity<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_Boltzmann2D_WeightedDensity_Params ParamsType;

  typedef typename getBoltzmann2DTraitsModel<PDETraitsModel>::type TraitsModel;

  //typedef PDETraitsModel TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Boltzmann2D_WeightedDensity( const PyDict& d ) :
    qInterpret_(GasModel( d.get(ParamsType::params.gasModel) )),
    Rho_(d.get(ParamsType::params.Rho))
  {
    // Nothing
  }

  explicit SolutionFunction_Boltzmann2D_WeightedDensity( const GasModel& gas, const Real& Rho  ) :
    qInterpret_(gas),
    Rho_(Rho)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> q = 0;
    T val = Rho_;
#if 0
    PrimitiveDistributionFunctions2D<T> dvp( weight[0]*val, weight[1]*val, weight[2]*val, weight[3]*val,
                                                weight[4]*val, weight[5]*val, weight[6]*val, weight[7]*val,
                                                weight[8]*val, weight[9]*val, weight[10]*val, weight[11]*val,
                                                weight[12]*val, weight[13]*val, weight[14]*val, weight[15]*val
                                               );
#endif
    T data[NVar];
    for (int i=0;i<NVar;i++)
      data[i] = ew[i][2]*val;
    qInterpret_.setFromPrimitive(q, data);//dvp);
    return q;
  }

private:
  const QInterpret qInterpret_;
  const Real Rho_;
};

// -----------------------------------------------------------------------


struct SolutionFunction_Boltzmann2D_WeightedDensityVelocity_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> Rho{"Rho", NO_DEFAULT, NO_RANGE, "Density"};
  const ParameterNumeric<Real> Uw{"Uw", NO_DEFAULT, NO_RANGE, "Uw"};
  const ParameterNumeric<Real> Vw{"Vw", NO_DEFAULT, NO_RANGE, "Vw"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Boltzmann2D_WeightedDensityVelocity_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_Boltzmann2D_WeightedDensityVelocity :
    public Function2DVirtualInterface<SolutionFunction_Boltzmann2D_WeightedDensityVelocity<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_Boltzmann2D_WeightedDensityVelocity_Params ParamsType;

  typedef typename getBoltzmann2DTraitsModel<PDETraitsModel>::type TraitsModel;

  //typedef PDETraitsModel TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef Q2D<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_Boltzmann2D_WeightedDensityVelocity( const PyDict& d ) :
    qInterpret_(GasModel( d.get(ParamsType::params.gasModel) )),
    Rho_(d.get(ParamsType::params.Rho)),
    Uw_(d.get(ParamsType::params.Uw)),
    Vw_(d.get(ParamsType::params.Vw))
  {
    // Nothing
  }

  explicit SolutionFunction_Boltzmann2D_WeightedDensityVelocity( const GasModel& gas, const Real& Rho, const Real& Uw,
                                                                    const Real& Vw) :
    qInterpret_(gas),
    Rho_(Rho),
    Uw_(Uw),
    Vw_(Vw)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> q = 0;
    T u_dot_u = Uw_*Uw_ + Vw_*Vw_;

    Real dummy;
    T data[NVar];
    for (int i=0; i< NVar ;i++)
    {
      T e_dot_u = ew[i][0]*Uw_ + ew[i][1]*Vw_;
      data[i] = ew[i][2]*Rho_*exp(e_dot_u - u_dot_u*u_dot_u/2.);
    }

    qInterpret_.setFromPrimitive(q, data);
    return q;
  }

private:
  const QInterpret qInterpret_;
  const Real Rho_;
  const Real Uw_;
  const Real Vw_;
};



} // namespace SANS

#endif  // SOLUTIONFUNCTION_BOLTZMANN2D_H
