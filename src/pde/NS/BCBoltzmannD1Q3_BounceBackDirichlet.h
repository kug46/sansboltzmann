// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCBOLTZMANND1Q3_BOUNCEBACKDIRICHLET_H
#define BCBOLTZMANND1Q3_BOUNCEBACKDIRICHLET_H

// 1-D Euler BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsBoltzmannD1Q3.h"
#include "PDEBoltzmannD1Q3.h"
//#include "PDEEulermitAVDiffusion1D.h"
//#include "SolutionFunction_Euler1D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeNone;
//class BCTypeFunction_mitState;
class BCTypeBounceBackDirichlet_pdf0;
// pdf1 requires no BC (zero advection velocity)
class BCTypeBounceBackDirichlet_pdf2;
//class BCTypeOutflowSubsonic_Pressure_mitState;
//class BCTypeOutflowSupersonic_Pressure_mitState;
//class BCTypeOutflowSupersonic_PressureFunction_mitState;
//class BCTypeReflect_mitState;
//class BCTypeInflowSubsonic_PtTt_mitState;

template <class BCType, class PDEBoltzmannD1Q3>
class BCBoltzmannD1Q3;

template <class BCType>
struct BCBoltzmannD1Q3Params;


//----------------------------------------------------------------------------//
// Conventional Full State Imposed (possibly with characteristics)
//
//----------------------------------------------------------------------------//

template<>
struct BCBoltzmannD1Q3Params<BCTypeNone> : noncopyable
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"None"};
  struct Option
  {
    const DictOption None{BCBoltzmannD1Q3Params::BCName, BCBoltzmannD1Q3Params::checkInputs};
  };

  static BCBoltzmannD1Q3Params params;
};


template <class PDE_>
class BCBoltzmannD1Q3<BCTypeNone, PDE_> :
    public BCType< BCBoltzmannD1Q3<BCTypeNone, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeNone BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBoltzmannD1Q3Params<BCType> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 0;                       // total BCs

  typedef typename PDE::QType QType;
  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function1DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCBoltzmannD1Q3( const PDE& pde ) :
      pde_(pde),
      qInterpret_(pde_.variableInterpreter())
  {
    // Nothing
  }
  ~BCBoltzmannD1Q3() {}

  BCBoltzmannD1Q3(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter())
  {
    // Nothing
  }

  BCBoltzmannD1Q3( const BCBoltzmannD1Q3& ) = delete;
  BCBoltzmannD1Q3& operator=( const BCBoltzmannD1Q3& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& qI ) const
  {
    Real rho;
    qInterpret_.evalDensity( qI, rho );
    // None requires outflow
    return (rho) > 0;
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class PDE_>
template <class T>
inline void
BCBoltzmannD1Q3<BCTypeNone, PDE_>::
state( const Real& x, const Real& time,
       const Real& nx,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}


// normal BC flux
template <class PDE_>
template <class T, class Tf>
inline void
BCBoltzmannD1Q3<BCTypeNone, PDE_>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on the interior state
  ArrayQ<Tf> fx = 0;
  pde_.fluxAdvective(x, time, qI, fx);

  Fn += fx*nx;
}


// normal BC flux
template <class PDE_>
template <class T, class Tp, class Tf>
inline void
BCBoltzmannD1Q3<BCTypeNone, PDE_>::
fluxNormal( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Compute the advective flux based on the interior state
  ArrayQ<Tf> fx = 0;
  pde_.fluxAdvective(param, x, time, qI, fx);

  Fn += fx*nx;
}

template <class PDE_>
void
BCBoltzmannD1Q3<BCTypeNone, PDE_>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCBoltzmannD1Q3<BCTypeNone, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCBoltzmannD1Q3<BCTypeNone, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// PDF0
//
//----------------------------------------------------------------------------//

template<class VariableTypeD1Q3Params>
struct BCBoltzmannD1Q3BounceBackDirichletPDF0Params : noncopyable
{
  BCBoltzmannD1Q3BounceBackDirichletPDF0Params();

  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};
  const ParameterOption<typename VariableTypeD1Q3Params::VariableOptions>& StateVector;

  static constexpr const char* BCName{"BounceBackDirichletPDF0"};
  struct Option
  {
    const DictOption BounceBackDirichletPDF0{
      BCBoltzmannD1Q3BounceBackDirichletPDF0Params::BCName, BCBoltzmannD1Q3BounceBackDirichletPDF0Params::checkInputs
                                            };
  };

  static void checkInputs(PyDict d);

  static BCBoltzmannD1Q3BounceBackDirichletPDF0Params params;
};


template <class PDE_>
class BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf0, PDE_> :
    public BCType< BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf0, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeBounceBackDirichlet_pdf0 BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBoltzmannD1Q3BounceBackDirichletPDF0Params<typename PDE_::VariableTypeD1Q3Params> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;//N;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices
#if 0
  // cppcheck-suppress noExplicitConstructor
  template <class Variables>
  //BCBoltzmannD1Q3( const PDE& pde, const BoltzmannVariableTypeD1Q3<Variables>& data, const bool& upwind ) :
  BCBoltzmannD1Q3( const PDE& pde, const Variables& data, const bool& upwind ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    //qB_(pde.setDOFFrom(data)),
    qB_(data),
    upwind_(upwind)
  {
    //qInterpret_.evalVelocity( qB_, uB_ );
  }
#endif
#if 1
  // PyDict not set up.................
  BCBoltzmannD1Q3(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(d)),
    upwind_(d.get(ParamsType::params.Characteristic))
  {
    //std::cout<<"PyDict is not set up for BCBoltzmann. See Line 314 BCBoltzmannD1Q3_BounceBackDirichlet.h"<<std::endl;
    //qInterpret_.evalVelocity( qB_, uB_ );

  }
#endif

  ~BCBoltzmannD1Q3() {}

  BCBoltzmannD1Q3( const BCBoltzmannD1Q3& ) = delete;
  BCBoltzmannD1Q3& operator=( const BCBoltzmannD1Q3& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    static const int if0 = 0;
    static const int if1 = 1;
    static const int if2 = 2;
    qB(if1) = qI(if1);
    qB(if2) = qI(if2);
    //qB(if0) = qB_(if0) - qB(if1) - qB(if2);
    qB(if0) = qB_(if0) - qB(if1) - qB(if2);


    //qB(if1) = qB_(if1) - qB(if0) - qB(if2);
    //qB(if2) = qB_(if2) - qB(if0) - qB(if1);
  }

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  //Real qB_;
  const ArrayQ<Real> qB_;
  const bool upwind_;
  //Real uB_;
};

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf0, PDE>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( x, time, qI, qB, nx, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( x, time, qB, fx );
#if 0
    static const int if0 = 0;
    Fn(if0) += fx(if0)*nx;
#endif
    Fn += fx*nx;
  }

  // No viscous flux
}

// normal BC flux
template <class PDE>
template <class T, class Tp, class Tf>
inline void
BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf0, PDE>::
fluxNormal( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( param, x, time, qI, qB, nx, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( param, x, time, qB, fx );
#if 0
    static const int if0 = 0;
    Fn(if0) += fx(if0)*nx;
#endif
    Fn += fx*nx;
  }

  // No viscous flux
}

// is the boundary state valid
template <class PDE>
bool
BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf0, PDE>::isValidState( const Real& nx, const ArrayQ<Real>& qI) const
{
  if (upwind_)
  {
    // Characteristic can be either inflow or outflow
    return true;
  }
  else
  {
   // Real f1 = 0, f2 = 0, f3 =0 ;
    //qInterpret_.eval( qI, f1, f2, f3 );
    // Full state requires inflow, so of the dot product between the BC an interior must be positive
    return true; //(f1 >= 0 && f2 >= 0 && f3 >= 0 );
  }

} // end PDF0


//----------------------------------------------------------------------------//
// PDF2
//
//----------------------------------------------------------------------------//

template<class VariableTypeD1Q3Params>
struct BCBoltzmannD1Q3BounceBackDirichletPDF2Params : noncopyable
{
  BCBoltzmannD1Q3BounceBackDirichletPDF2Params();

  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};
  const ParameterOption<typename VariableTypeD1Q3Params::VariableOptions>& StateVector;

  static constexpr const char* BCName{"BounceBackDirichletPDF2"};
  struct Option
  {
    const DictOption BounceBackDirichletPDF2{
      BCBoltzmannD1Q3BounceBackDirichletPDF2Params::BCName, BCBoltzmannD1Q3BounceBackDirichletPDF2Params::checkInputs
                                            };
  };

  static void checkInputs(PyDict d);

  static BCBoltzmannD1Q3BounceBackDirichletPDF2Params params;
};


template <class PDE_>
class BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf2, PDE_> :
    public BCType< BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf2, PDE_> >
{
public:
  typedef PhysD1 PhysDim;
  typedef BCTypeBounceBackDirichlet_pdf2 BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCBoltzmannD1Q3BounceBackDirichletPDF2Params<typename PDE_::VariableTypeD1Q3Params> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 1;//N;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices
#if 0
  // cppcheck-suppress noExplicitConstructor
  template <class Variables>
  //BCBoltzmannD1Q3( const PDE& pde, const BoltzmannVariableTypeD1Q3<Variables>& data, const bool& upwind ) :
  BCBoltzmannD1Q3( const PDE& pde, const Variables& data, const bool& upwind ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    qB_(data),     //qB_(pde.setDOFFrom(data)),
    upwind_(upwind)
  {
    //qInterpret_.evalVelocity( qB_, uB_ );
  }
#endif
#if 1
  // PyDict not set up.................
  BCBoltzmannD1Q3(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    qInterpret_(pde_.variableInterpreter()),
    qB_(pde.setDOFFrom(d)),
    upwind_(d.get(ParamsType::params.Characteristic))
  {
    //std::cout<<"PyDict is not set up for BCBoltzmann. See Line 535 BCBoltzmannD1Q3_BounceBackDirichlet.h"<<std::endl;
    //uB_ = 0.;
    //qInterpret_.evalVelocity( qB_, uB_ );
  }
#endif


  ~BCBoltzmannD1Q3() {}

  BCBoltzmannD1Q3( const BCBoltzmannD1Q3& ) = delete;
  BCBoltzmannD1Q3& operator=( const BCBoltzmannD1Q3& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    static const int if0 = 0;
    static const int if1 = 1;
    static const int if2 = 2;
    qB(if0) = qI(if0);
    qB(if1) = qI(if1);
    qB(if2) = qB_(if2) - qB(if0) - qB(if1);

  }

  // BC data
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& time,
              const Real& nx,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, time, nx, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class Tp, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& time,
                   const Real& nx,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const ArrayQ<Real> qB_;
  const bool upwind_;
  //Real uB_;
};

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf2, PDE>::
fluxNormal( const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( x, time, qI, qB, nx, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( x, time, qB, fx );
#if 0
    static const int if2 = 0;
    Fn(if2) += fx(if2)*nx;
#endif
    Fn += fx*nx;
  }

  // No viscous flux
}

// normal BC flux
template <class PDE>
template <class T, class Tp, class Tf>
inline void
BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf2, PDE>::
fluxNormal( const Tp& param,
            const Real& x, const Real& time,
            const Real& nx,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  if (upwind_)
  {
    // Compute the advective flux based on interior and boundary state
    pde_.fluxAdvectiveUpwind( param, x, time, qI, qB, nx, Fn );
  }
  else
  {
    // Compute the advective flux based on the boundary state
    ArrayQ<T> fx = 0;
    pde_.fluxAdvective( param, x, time, qB, fx );
#if 0
    static const int if2 = 0;
    Fn(if2) += fx(if2)*nx;
#endif
    Fn += fx*nx;
  }

  // No viscous flux
}

// is the boundary state valid
template <class PDE>
bool
BCBoltzmannD1Q3<BCTypeBounceBackDirichlet_pdf2, PDE>::isValidState( const Real& nx, const ArrayQ<Real>& qI) const
{
  if (upwind_)
  {
    // Characteristic can be either inflow or outflow
    return true;
  }
  else
  {
    //Real f1 = 0, f2 = 0, f3 =0 ;
    //qInterpret_.eval( qI, f1, f2, f3 );
    // Full state requires inflow, so of the dot product between the BC an interior must be positive
    //return (f1 >= 0 && f2 >= 0 && f3 >= 0 );
    return true;
  }

} // end PDF2

} //namespace SANS

#endif  // BCBOLTZMANND1Q3_BOUNCEBACKDIRICHLET_H
