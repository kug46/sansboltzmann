// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BoltzmannVariableD2Q16.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<BoltzmannVariableTypeD2Q16Params::VariableOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<BoltzmannVariableTypeD2Q16Params::VariableOptions> > const&) const;

void PrimitiveDistributionFunctionsD2Q16Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.pdf0));
  allParams.push_back(d.checkInputs(params.pdf1));
  allParams.push_back(d.checkInputs(params.pdf2));
  allParams.push_back(d.checkInputs(params.pdf3));
  allParams.push_back(d.checkInputs(params.pdf4));
  allParams.push_back(d.checkInputs(params.pdf5));
  allParams.push_back(d.checkInputs(params.pdf6));
  allParams.push_back(d.checkInputs(params.pdf7));
  allParams.push_back(d.checkInputs(params.pdf8));
  allParams.push_back(d.checkInputs(params.pdf9));
  allParams.push_back(d.checkInputs(params.pdf10));
  allParams.push_back(d.checkInputs(params.pdf11));
  allParams.push_back(d.checkInputs(params.pdf12));
  allParams.push_back(d.checkInputs(params.pdf13));
  allParams.push_back(d.checkInputs(params.pdf14));
  allParams.push_back(d.checkInputs(params.pdf15));
  d.checkUnknownInputs(allParams);
}
PrimitiveDistributionFunctionsD2Q16Params PrimitiveDistributionFunctionsD2Q16Params::params;



void BoltzmannVariableTypeD2Q16Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
BoltzmannVariableTypeD2Q16Params BoltzmannVariableTypeD2Q16Params::params;

}
