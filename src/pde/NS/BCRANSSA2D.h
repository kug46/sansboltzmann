// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCRANSSA2D_H
#define BCRANSSA2D_H

// 2-D compressible RANS SA BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"


#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsNavierStokes.h"
#include "PDERANSSA2D.h"
#include "BCNavierStokes2D.h"
#include "BCRANSSA2D_Solution.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D compressible RANS SA
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeWall_mitState;
class BCTypeDirichlet_mitState;
class BCTypeNeumann_mitState;
class BCTypeInflowSubsonic_sHqt_BN;
class BCTypeFunctionInflow_sHqt_BN;

template <class BCType, class BCNS2D>
class BCRANSSA2D;

template <class BCType, class BCNS2DParams>
struct BCRANSSA2DParams;


//RANS Slip walls are generally not appropriate. Please do not add a RANS slip wall, use symmetry instead

template <template <class> class PDETraitsSize, class PDETraitsModel>
using BCRANSSA2DVector = boost::mpl::vector11<
  BCRANSSA2D<BCTypeWall_mitState,                BCNavierStokes2D< BCTypeWallNoSlipAdiabatic_mitState,  PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeWall_mitState,                BCNavierStokes2D< BCTypeWallNoSlipIsothermal_mitState, PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeNeumann_mitState,             BCNavierStokes2D< BCTypeSymmetry_mitState,             PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeNeumann_mitState,             BCEuler2D< BCTypeOutflowSubsonic_Pressure_mitState,    PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeNeumann_mitState,             BCEuler2D< BCTypeOutflowSubsonic_Pressure_BN,          PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeFullState_mitState,           BCEuler2D< BCTypeFullState_mitState,                   PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeDirichlet_mitState,           BCEuler2D< BCTypeInflowSubsonic_PtTta_mitState,        PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeFunctionInflow_sHqt_mitState, BCEuler2D< BCTypeFunctionInflow_sHqt_mitState,         PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeFullState_mitState,           BCEuler2D< BCTypeFunction_mitState,                    PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN,       BCEuler2D< BCTypeInflowSubsonic_sHqt_BN,               PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >,
  BCRANSSA2D<BCTypeFunctionInflow_sHqt_BN,       BCEuler2D< BCTypeFunctionInflow_sHqt_BN,               PDERANSSA2D<PDETraitsSize, PDETraitsModel>> >
  >;

//----------------------------------------------------------------------------//
// Conventional PX-style Wall BC
//
// specify: SA working variable as zero
//----------------------------------------------------------------------------//

template <class BCNS2D>
class BCRANSSA2D<BCTypeWall_mitState, BCNS2D> : public BCNS2D
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCNS2D::Category Category;
  typedef typename BCNS2D::ParamsType ParamsType;
  typedef typename BCNS2D::PDE PDE;

  using BCNS2D::D;   // physical dimensions
  using BCNS2D::N;   // total solution variables

  static const int NBC = BCNS2D::NBC+1; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCRANSSA2D( BCArgs&&... args ) :
    BCNS2D(std::forward<BCArgs>(args)...) {}

  BCRANSSA2D( const PDE& pde, const PyDict& d ) :
    BCNS2D(pde, d) {}

  ~BCRANSSA2D() {}

  BCRANSSA2D( const BCRANSSA2D& ) = delete;
  BCRANSSA2D& operator=( const BCRANSSA2D& ) = delete;

  // BC state
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& dist,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS2D::fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);

  }

  // normal BC flux
  template <class T>
  void fluxNormal( const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS2D::fluxNormal(param.left(), x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  }

  // is the boundary state valid
  using BCNS2D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BCNS2D::pde_;
  using BCNS2D::qInterpret_;   // solution variable interpreter class
};


// BC state
template <class BCNS2D>
template <class T>
inline void
BCRANSSA2D<BCTypeWall_mitState, BCNS2D>::
state( const Real& dist,
       const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  BCNS2D::state(x, y, time, nx, ny, qI, qB);

  // TODO: I think this should happen in setFromPrimitive of the qinterpreter
  qB(qInterpret_.iSA) = 0;
}


template <class BCNS2D>
void
BCRANSSA2D<BCTypeWall_mitState, BCNS2D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA2D<BCTypeWall_mitState, BCNS2D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA2D<BCTypeWall_mitState, BCNS2D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// Conventional PX-style Dirichlet BC
//
// specify: SA working variable
//----------------------------------------------------------------------------//

template<class BCNS2DParams>
struct BCRANSSA2DParams<BCTypeDirichlet_mitState, BCNS2DParams> : public BCNS2DParams
{
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "Specified SA working variable"};

  using BCNS2DParams::BCName;
  typedef typename BCNS2DParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.nt));
    BCNS2DParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCRANSSA2DParams params;
};
// Instantiate the singleton
template<class BCNS2DParams>
BCRANSSA2DParams<BCTypeDirichlet_mitState, BCNS2DParams> BCRANSSA2DParams<BCTypeDirichlet_mitState, BCNS2DParams>::params;


template <class BCNS2D>
class BCRANSSA2D<BCTypeDirichlet_mitState, BCNS2D> : public BCNS2D
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCNS2D::Category Category;
  typedef BCRANSSA2DParams<BCTypeDirichlet_mitState, typename BCNS2D::ParamsType> ParamsType;
  typedef typename BCNS2D::PDE PDE;

  using BCNS2D::D;   // physical dimensions
  using BCNS2D::N;   // total solution variables

  static const int NBC = BCNS2D::NBC+1; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >
  BCRANSSA2D(const PDE& pde, const Real nt, const BCArgs&... args) :
    BCNS2D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(nt) {}

  BCRANSSA2D(const PDE& pde, const PyDict& d ) :
    BCNS2D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(d.get(ParamsType::params.nt)) {}

  ~BCRANSSA2D() {}

  BCRANSSA2D( const BCRANSSA2D& ) = delete;
  BCRANSSA2D& operator=( const BCRANSSA2D& ) = delete;

  // BC state
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& dist,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS2D::fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  }

  // is the boundary state valid
  using BCNS2D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real nt_;
};


// BC state
template <class BCNS2D>
template <class T>
inline void
BCRANSSA2D<BCTypeDirichlet_mitState, BCNS2D>::
state( const Real& dist,
       const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  BCNS2D::state(x, y, time, nx, ny, qI, qB);

  // TODO: I think this should happen in setFromPrimitive of the qinterpreter
  qB(qInterpret_.iSA) = nt_;
}


template <class BCNS2D>
void
BCRANSSA2D<BCTypeDirichlet_mitState, BCNS2D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA2D<BCTypeDirichlet_mitState, BCNS2D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA2D<BCTypeDirichlet_mitState, BCNS2D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCRANSSA2D<BCTypeDirichlet_mitState, BCNS2D>: nt_ = " << nt_ << std::endl;
}


//----------------------------------------------------------------------------//
// SA Mit-State Inlet BC w Solution Function
//
// specify: SA working variable
//----------------------------------------------------------------------------//

template<class BCNS2DParams>
struct BCRANSSA2DParams<BCTypeFunctionInflow_sHqt_mitState, BCNS2DParams> : public BCNS2DParams
{
  using BCNS2DParams::BCName;
  typedef typename BCNS2DParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
//    allParams.push_back(d.checkInputs(params.nt));
    BCNS2DParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCRANSSA2DParams params;
};
// Instantiate the singleton
template<class BCNS2DParams>
BCRANSSA2DParams<BCTypeFunctionInflow_sHqt_mitState, BCNS2DParams> BCRANSSA2DParams<BCTypeFunctionInflow_sHqt_mitState, BCNS2DParams>::params;


template <class BCNS2D>
class BCRANSSA2D<BCTypeFunctionInflow_sHqt_mitState, BCNS2D> : public BCNS2D
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCNS2D::Category Category;
  typedef typename BCNS2D::ParamsType ParamsType;
  typedef typename BCNS2D::PDE PDE;

  using BCNS2D::D;   // physical dimensions
  using BCNS2D::N;   // total solution variables

  static const int NBC = BCNS2D::NBC+1; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >

  BCRANSSA2D(const PDE& pde, const BCArgs&... args) :
    BCNS2D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  BCRANSSA2D(const PDE& pde, const PyDict& d ) :
    BCNS2D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  ~BCRANSSA2D() {}

  BCRANSSA2D( const BCRANSSA2D& ) = delete;
  BCRANSSA2D& operator=( const BCRANSSA2D& ) = delete;

  // BC state
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS2D::state(x, y, time, nx, ny, qI, qB);

    ArrayQ<Real> qBspec = (*solution_)(x,y,time);

    // TODO: I think this should happen in setFromPrimitive of the qinterpreter
    qB(qInterpret_.iSA) = qBspec(qInterpret_.iSA);
  }

  template <class T>
  void fluxNormal( const Real& dist,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS2D::fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  }

  // is the boundary state valid
  using BCNS2D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  using BCNS2D::solution_;
};

template <class BCNS2D>
void
BCRANSSA2D<BCTypeFunctionInflow_sHqt_mitState, BCNS2D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA2D<BCTypeSolutionInflow_sHqt_mitState, BCNS2D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA2D<BCTypeSolutionInflow_sHqt_mitState, BCNS2D>: qInterpret_ = " << std::endl;

  qInterpret_.dump(indentSize+2, out);
}



//----------------------------------------------------------------------------//
// Berg-Nordstrom Inlet BC
//
// specify: SA working variable
//----------------------------------------------------------------------------//

template<class BCNS2DParams>
struct BCRANSSA2DParams<BCTypeInflowSubsonic_sHqt_BN, BCNS2DParams> : public BCNS2DParams
{
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "Specified SA working variable"};

  using BCNS2DParams::BCName;
  typedef typename BCNS2DParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.nt));
    BCNS2DParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCRANSSA2DParams params;
};
// Instantiate the singleton
template<class BCNS2DParams>
BCRANSSA2DParams<BCTypeInflowSubsonic_sHqt_BN, BCNS2DParams> BCRANSSA2DParams<BCTypeInflowSubsonic_sHqt_BN, BCNS2DParams>::params;


template <class BCNS2D>
class BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN, BCNS2D> : public BCNS2D
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCNS2D::Category Category;
  typedef BCRANSSA2DParams<BCTypeInflowSubsonic_sHqt_BN, typename BCNS2D::ParamsType> ParamsType;
  typedef typename BCNS2D::PDE PDE;

  using BCNS2D::D;   // physical dimensions
  using BCNS2D::N;   // total solution variables

  static const int NBC = BCNS2D::NBC+1; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >
  BCRANSSA2D(const PDE& pde, const Real nt, const BCArgs&... args) :
    BCNS2D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(nt) {}

  BCRANSSA2D(const PDE& pde, const PyDict& d ) :
    BCNS2D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(d.get(ParamsType::params.nt)) {}

  ~BCRANSSA2D() {}

  BCRANSSA2D( const BCRANSSA2D& ) = delete;
  BCRANSSA2D& operator=( const BCRANSSA2D& ) = delete;

  // BC state
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS2D::state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& dist,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS2D::fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);

    Real gamma = BCNS2D::gas_.gamma();
    Real gmi   = gamma - 1;

    T rho=0, u=0, v=0, t=0;

    //compute BN Flux
    qInterpret_.eval( qI, rho, u, v, t );
    T p = BCNS2D::gas_.pressure(rho,t);

    T H = BCNS2D::gas_.enthalpy(rho, t) + 0.5*(u*u+v*v);
    T c = BCNS2D::gas_.speedofSound(t);
    T s = log( p /pow(rho,gamma) );
    T nt = 0;

    qInterpret_.evalSA( qI, nt );

    // tangential vector
    const Real tx = -ny;
    const Real ty =  nx;

    // normal and tangential velocities
    T vn = u*nx + v*ny;
    T vt = u*tx + v*ty;
    T V2 = u*u+v*v;
    T V = sqrt(V2);

    // Specified tangential velocity component
  //  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

    // Strong BC's that are imposed
    T ds  = s  - BCNS2D::sSpec_;
    T dH  = H  - BCNS2D::HSpec_;
    T dvt = v/sqrt(u*u+v*v) - sin(BCNS2D::aSpec_);
    T dnt = nt - nt_;

    Fn[pde_.iSA] -= -vn*nt*rho/gmi*ds;
    Fn[pde_.iSA] -= vn*gamma*nt*rho/(c*c+V2*gmi)*dH;
    Fn[pde_.iSA] -=  -(vt*V2*V*rho*gamma*nt)/(u*(c*c+V2*gmi))*dvt;
    Fn[pde_.iSA] -=  rho*vn*dnt;

  }

  // is the boundary state valid
  using BCNS2D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real nt_;
};

template <class BCNS2D>
void
BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN, BCNS2D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN, BCNS2D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN, BCNS2D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCRANSSA2D<BCTypeInflowSubsonic_sHqt_BN, BCNS2D>: nt_ = " << nt_ << std::endl;
}


//----------------------------------------------------------------------------//
// Berg-Nordstrom Inlet BC w Solution Function
//
// specify: SA working variable
//----------------------------------------------------------------------------//

template<class BCNS2DParams>
struct BCRANSSA2DParams<BCTypeFunctionInflow_sHqt_BN, BCNS2DParams> : public BCNS2DParams
{
  using BCNS2DParams::BCName;
  typedef typename BCNS2DParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
//    allParams.push_back(d.checkInputs(params.nt));
    BCNS2DParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCRANSSA2DParams params;
};
// Instantiate the singleton
template<class BCNS2DParams>
BCRANSSA2DParams<BCTypeFunctionInflow_sHqt_BN, BCNS2DParams> BCRANSSA2DParams<BCTypeFunctionInflow_sHqt_BN, BCNS2DParams>::params;


template <class BCNS2D>
class BCRANSSA2D<BCTypeFunctionInflow_sHqt_BN, BCNS2D> : public BCNS2D
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCNS2D::Category Category;
  typedef typename BCNS2D::ParamsType ParamsType;
  typedef typename BCNS2D::PDE PDE;

  using BCNS2D::D;   // physical dimensions
  using BCNS2D::N;   // total solution variables

  static const int NBC = BCNS2D::NBC+1; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >

  BCRANSSA2D(const PDE& pde, const BCArgs&... args) :
    BCNS2D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  BCRANSSA2D(const PDE& pde, const PyDict& d ) :
    BCNS2D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  ~BCRANSSA2D() {}

  BCRANSSA2D( const BCRANSSA2D& ) = delete;
  BCRANSSA2D& operator=( const BCRANSSA2D& ) = delete;

  // BC state
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS2D::state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& dist,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS2D::fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);


    Real sSpec, HSpec, vnSpec = 0;
//    BCNS2D::BCQuantities(x,y,time, nx, ny, sSpec, HSpec, vnSpec);

    // TODO: qBspec should be passed into BCQuantities
    ArrayQ<Real> qBspec = (*solution_)(x,y,time);
    Real ntSpec;
    qInterpret_.evalSA( qBspec, ntSpec );


    Real gamma = BCNS2D::gas_.gamma();
    Real gmi   = gamma - 1;

//    ArrayQ<Real> qBspec = 0;
//    qBspec = (*BCNS2D::solution_)(x,y,time);
    Real rhoSpec, uSpec, vSpec, tSpec, pSpec = 0;
    qInterpret_.eval( qBspec, rhoSpec, uSpec, vSpec, tSpec );
    pSpec = BCNS2D::gas_.pressure(rhoSpec, tSpec);
    HSpec = BCNS2D::gas_.enthalpy(rhoSpec, tSpec) + 0.5*(uSpec*uSpec + vSpec*vSpec);
    sSpec = log( pSpec / pow(rhoSpec, gamma) );
    vnSpec = vSpec / sqrt(uSpec*uSpec + vSpec*vSpec);
    qInterpret_.evalSA(qBspec, ntSpec);

    T rho=0, u=0, v=0, t=0;

    //compute BN Flux
    qInterpret_.eval( qI, rho, u, v, t );
    T p = BCNS2D::gas_.pressure(rho,t);

    T H = BCNS2D::gas_.enthalpy(rho, t) + 0.5*(u*u+v*v);
    T c = BCNS2D::gas_.speedofSound(t);
    T s = log( p /pow(rho,gamma) );
    T nt = 0;

    qInterpret_.evalSA( qI, nt );

    // tangential vector
    const Real tx = -ny;
    const Real ty =  nx;

    // normal and tangential velocities
    T vn = u*nx + v*ny;
    T vt = u*tx + v*ty;
    T V2 = u*u+v*v;
    T V = sqrt(V2);

    // Specified tangential velocity component
  //  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

    // Strong BC's that are imposed
    T ds  = s  - sSpec;
    T dH  = H  - HSpec;
    T dvt = v/sqrt(u*u+v*v) - vnSpec;
    T dnt = nt - ntSpec;

    Fn[pde_.iSA] -= -vn*nt*rho/gmi*ds;
    Fn[pde_.iSA] -= vn*gamma*nt*rho/(c*c+V2*gmi)*dH;
    Fn[pde_.iSA] -=  -(vt*V2*V*rho*gamma*nt)/(u*(c*c+V2*gmi))*dvt;
    Fn[pde_.iSA] -=  rho*vn*dnt;

  }

  // is the boundary state valid
  using BCNS2D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  using BCNS2D::solution_;
};

template <class BCNS2D>
void
BCRANSSA2D<BCTypeFunctionInflow_sHqt_BN, BCNS2D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA2D<BCTypeFunctionInflow_sHqt_BN, BCNS2D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA2D<BCTypeFunctionInflow_sHqt_BN, BCNS2D>: qInterpret_ = " << std::endl;

  qInterpret_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX-style Neumann BC
//
//----------------------------------------------------------------------------//

template <class BCNS2D>
class BCRANSSA2D<BCTypeNeumann_mitState, BCNS2D> : public BCNS2D
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCNS2D::Category Category;
  typedef typename BCNS2D::ParamsType ParamsType;
  typedef typename BCNS2D::PDE PDE;

  using BCNS2D::D;   // physical dimensions
  using BCNS2D::N;   // total solution variables

  using BCNS2D::NBC; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >
  BCRANSSA2D(const PDE& pde, const BCArgs&... args) :
    BCNS2D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  BCRANSSA2D(const PDE& pde, const PyDict& d ) :
    BCNS2D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  ~BCRANSSA2D() {}

  BCRANSSA2D( const BCRANSSA2D& ) = delete;
  BCRANSSA2D& operator=( const BCRANSSA2D& ) = delete;

  // BC state
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& dist,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS2D::fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);

    // F[pde_.iSA] += 0; // No flux from SA
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS2D::fluxNormal(param.left(), x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);

    // F[pde_.iSA] += 0; // No flux from SA
  }

  // is the boundary state valid
  using BCNS2D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


// BC state
template <class BCNS2D>
template <class T>
inline void
BCRANSSA2D<BCTypeNeumann_mitState, BCNS2D>::
state( const Real& dist,
       const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  BCNS2D::state(x, y, time, nx, ny, qI, qB);

  qB(qInterpret_.iSA) = qI(qInterpret_.iSA);
}


template <class BCNS2D>
void
BCRANSSA2D<BCTypeNeumann_mitState, BCNS2D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA2D<BCTypeNeumann_mitState, BCNS2D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA2D<BCTypeNeumann_mitState, BCNS2D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX-style Full State
//
//----------------------------------------------------------------------------//

template <class BCNS2D>
class BCRANSSA2D<BCTypeFullState_mitState, BCNS2D> : public BCNS2D
{
public:
  typedef PhysD2 PhysDim;
  typedef typename BCNS2D::Category Category;
  typedef typename BCNS2D::ParamsType ParamsType;
  typedef typename BCNS2D::PDE PDE;

  using BCNS2D::D;   // physical dimensions
  using BCNS2D::N;   // total solution variables

  using BCNS2D::NBC; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >
  BCRANSSA2D(const PDE& pde, const BCArgs&... args) :
    BCNS2D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  BCRANSSA2D(const PDE& pde, const PyDict& d ) :
    BCNS2D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  ~BCRANSSA2D() {}

  BCRANSSA2D( const BCRANSSA2D& ) = delete;
  BCRANSSA2D& operator=( const BCRANSSA2D& ) = delete;

  // BC state
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS2D::state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& dist,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    BCNS2D::fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
                   const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    BCNS2D::fluxNormal(param.left(), x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
  }

  // is the boundary state valid
  using BCNS2D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};


template <class BCNS2D>
void
BCRANSSA2D<BCTypeFullState_mitState, BCNS2D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA2D<BCTypeFullState_mitState, BCNS2D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA2D<BCTypeFullState_mitState, BCNS2D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // BCRANSSA2D_H
