// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTEULER3D_H
#define OUTPUTEULER3D_H

#include "tools/SANSnumerics.h" // Real
#include "Topology/Dimension.h"
#include "pde/OutputCategory.h"

#include "PDERANSSA3D.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/tools/cross.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Provides weights to compute a force from a weighted residual

template <class PDENDConvert>
class OutputEuler3D_Force : public OutputType< OutputEuler3D_Force<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  OutputEuler3D_Force( const PDENDConvert& pde, const Real& wx, const Real& wy, const Real& wz ) :
    pde_(pde), wx_(wx), wy_(wy), wz_(wz) {}

  void operator()(const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = 0;
    weight[pde_.ixMom] = wx_;
    weight[pde_.iyMom] = wy_;
    weight[pde_.izMom] = wz_;
  }

private:
  const PDENDConvert& pde_;
  Real wx_, wy_, wz_;
};


//----------------------------------------------------------------------------//
// Provides weights to compute a moment from a weighted residual

template <class PDENDConvert>
class OutputEuler3D_Moment : public OutputType< OutputEuler3D_Moment<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;
  typedef OutputCategory::WeightedResidual Category;
  typedef DLA::VectorS<PhysDim::D,Real> VectorX;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  OutputEuler3D_Moment( const PDENDConvert& pde,
                        const Real& x0, const Real& y0, const Real& z0,
                        const Real& ax, const Real& ay, const Real& az,
                        const Real& MAC = 1) :
    pde_(pde), x0_(x0), y0_(y0), z0_(z0), Axis_({ax,ay,az}), MAC_(MAC)
  {

  }

  void operator()(const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& weight ) const
  {
    VectorX dX = {(x - x0_), (y - y0_), (z - z0_)};

    VectorX M = cross(dX, Axis_);

    weight = 0;
    weight[pde_.ixMom] = M[0]/MAC_;
    weight[pde_.iyMom] = M[1]/MAC_;
    weight[pde_.izMom] = M[2]/MAC_;
  }

private:
  const PDENDConvert& pde_;
  Real x0_, y0_, z0_;
  VectorX Axis_;
  Real MAC_;
};


template <class PDENDConvert>
class OutputEuler3D_PtFlux : public OutputType< OutputEuler3D_PtFlux<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;

  typedef OutputCategory::Functional Category;
  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler3D_PtFlux( const PDENDConvert& pde, const Real p0Ref = 0) :
    pde_(pde),
    p0Ref_(p0Ref)
  {
    // N
  }

  bool needsSolutionGradient() const { return false; }

public:
  // Area output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("PtFlux output needs nx, ny, nz");
  }

  // Boundary output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    Tq rho=0, u=0, v=0, w=0, t=0;

    pde_.variableInterpreter().eval( q, rho, u, v, w, t );
    Tq p = pde_.gasModel().pressure(rho, t);

    Tq c = pde_.gasModel().speedofSound(t);
    Tq M2 = (u*u + v*v + w*w) / (c*c);
    Tq gamma = pde_.gasModel().gamma();

    // Total pressure (stagnation pressure)
    Tq p0 = p*pow(1+(gamma-1)/2*M2, gamma/(gamma-1));

    Tq vn = u*nx + v*ny + w*nz;

    output = (p0 - p0Ref_)*rho*vn;
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<Tg>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("Pt Flux output needs nx, ny");
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, nx, ny, nz, q, qx, qy, qz, output);
  }

private:
  const PDENDConvert& pde_;
  const Real p0Ref_;   //Reference stagnation pressure for Error
};

template <class PDENDConvert>
class OutputEuler3D_MassFlux : public OutputType< OutputEuler3D_MassFlux<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;

  typedef OutputCategory::Functional Category;
  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler3D_MassFlux( const PDENDConvert& pde, const Real p0Ref = 0) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

public:
  // Area output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("Mass Flux output needs nx, ny");
  }

  // Boundary output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    Tq rho=0, u=0, v=0, w=0, t=0;
    pde_.variableInterpreter().eval( q, rho, u, v, w, t );

    Tq vn = u*nx + v*ny + w*nz;

    output = rho*vn;
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<Tg>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("Mass Flux output needs nx, ny");
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, nx, ny, nz, q, qx, qy, qz, output);
  }

private:
  const PDENDConvert& pde_;
};


template <class PDENDConvert>
class OutputEuler3D_Mach : public OutputType< OutputEuler3D_Mach<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;

  typedef OutputCategory::Functional Category;
  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler3D_Mach( const PDENDConvert& pde, const Real Mref) :
    pde_(pde), Mref_(Mref) {}

  bool needsSolutionGradient() const { return false; }

public:
  // Volume output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    Tq rho=0, u=0, v=0, w=0, t=0;
    pde_.variableInterpreter().eval( q, rho, u, v, w, t );

    Tq c = pde_.gasModel().speedofSound( t );
    Tq V = sqrt( u*u + v*v + w*w );
    Tq M = V/c;
    Tq dM = (M - Mref_);

    output = dM*dM;
  }

  // Boundary output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, q, qx, qy, qz, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, q, qx, qy, qz, output);
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, q, qx, qy, qz, output);
  }

private:
  const PDENDConvert& pde_;
  const Real Mref_; // reference mach number
};


template <class PDENDConvert>
class OutputEuler3D_TotalEnthalpy : public OutputType< OutputEuler3D_TotalEnthalpy<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler3D_TotalEnthalpy( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, w=0, t = 0;

    qInterp.eval( q, rho, u, v, w, t );
    Tq h = gas.enthalpy(rho, t);
    output = h + 0.5*(u*u + v*v + w*w);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, q, qx, qy, qz, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, q, qx, qy, qz, output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& z, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, MatrixJ<To>& dJdu) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();
    Tq rho = 0, u = 0, v = 0, w = 0, t = 0;
    Real gamma = gas.gamma();

    qInterp.eval( q, rho, u, v, w, t );
    Tq p = gas.pressure(rho, t);

    Real gm1 = gamma-1.;

    dJdu[0] = (-2.*p*gamma + rho*(u*u + v*v + w*w)*(2.-3.*gamma + gamma*gamma))/(2.*gm1*rho*rho);
    dJdu[1] = -u*gm1/rho;
    dJdu[2] = -v*gm1/rho;
    dJdu[3] = -w*gm1/rho;
    dJdu[4] = gamma/rho;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, z, time, q, qx, qy, qz, dJdu);
  }

private:
  const PDENDConvert& pde_;
};

template <class PDENDConvert>
class OutputEuler3D_Density : public OutputType< OutputEuler3D_Density<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler3D_Density( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, w=0, t = 0;
    qInterp.eval( q, rho, u, v, w, t );
    output = rho;
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, q, qx, qy, qz, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, q, qx, qy, qz, output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& z, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, MatrixJ<To>& dJdu) const
  {
    dJdu[0] = 1;
    dJdu[1] = 0;
    dJdu[2] = 0;
    dJdu[3] = 0;
    dJdu[4] = 0;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, z, time, q, qx, qy, qz, dJdu);
  }

private:
  const PDENDConvert& pde_;
};



template <class PDENDConvert>
class OutputEuler3D_VelocitySquared : public OutputType< OutputEuler3D_VelocitySquared<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler3D_VelocitySquared( const PDENDConvert& pde ) :
          pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, w=0, t = 0;
    qInterp.eval( q, rho, u, v, w, t );
    output = u*u + v*v + w*w;
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, q, qx, qy, qz, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& output ) const
  {
    operator()(x, y, z, time, q, qx, qy, qz, output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& z, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, MatrixJ<To>& dJdu) const
  {
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, w=0, t = 0;
    qInterp.eval( q, rho, u, v, w, t );

    dJdu[0] = -2*(u*u + v*v + w*w)/rho;
    dJdu[1] = 2*u/rho;
    dJdu[2] = 2*v/rho;
    dJdu[3] = 2*w/rho;
    dJdu[4] = 0;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, z, time, q, qx, qy, qz, dJdu);
  }

private:
  const PDENDConvert& pde_;
};

}

#endif //OUTPUTEULER3D_H
