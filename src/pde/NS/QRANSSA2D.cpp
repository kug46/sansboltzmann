// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "QRANSSA2D.h"
#include "Q2DPrimitiveRhoPressure.h"
#include "Q2DPrimitiveSurrogate.h"
#include "Q2DConservative.h"
#include "Q2DEntropy.h"
#include "TraitsRANSSA.h"

namespace SANS
{
template class QRANSSA2D<QTypePrimitiveRhoPressure, TraitsSizeRANSSA>;
template class QRANSSA2D<QTypePrimitiveSurrogate, TraitsSizeRANSSA>;
template class QRANSSA2D<QTypeConservative, TraitsSizeRANSSA>;
template class QRANSSA2D<QTypeEntropy, TraitsSizeRANSSA>;
}
