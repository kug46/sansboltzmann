// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "NSVariable1D.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

template ParameterOption<NSVariableType1DParams::VariableOptions>::ExtractType
PyDict::get(ParameterType<ParameterOption<NSVariableType1DParams::VariableOptions> > const&) const;

void DensityVelocityPressure1DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.p));
  d.checkUnknownInputs(allParams);
}
DensityVelocityPressure1DParams DensityVelocityPressure1DParams::params;

void DensityVelocityTemperature1DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.u));
  allParams.push_back(d.checkInputs(params.t));
  d.checkUnknownInputs(allParams);
}
DensityVelocityTemperature1DParams DensityVelocityTemperature1DParams::params;

void Conservative1DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.rho));
  allParams.push_back(d.checkInputs(params.rhou));
  allParams.push_back(d.checkInputs(params.rhoE));
  d.checkUnknownInputs(allParams);
}
Conservative1DParams Conservative1DParams::params;


void NSVariableType1DParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.StateVector));
  d.checkUnknownInputs(allParams);
}
NSVariableType1DParams NSVariableType1DParams::params;

}
