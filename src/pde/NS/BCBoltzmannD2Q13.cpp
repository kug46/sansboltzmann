// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCBoltzmannD2Q13.h"
#include "QD2Q13PrimitiveDistributionFunctions.h"

// TODO: Rework so this is not needed here
#include "TraitsBoltzmannD2Q13.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"


namespace SANS
{

// cppcheck-suppress passedByValue
void BCBoltzmannD2Q13Params<BCTypeNone>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCBoltzmannD2Q13Params<BCTypeNone> BCBoltzmannD2Q13Params<BCTypeNone>::params;

// cppcheck-suppress passedByValue
void BCBoltzmannD2Q13Params<BCTypeDiffuseKinetic>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Uw));
  allParams.push_back(d.checkInputs(params.Vw));
  allParams.push_back(d.checkInputs(params.Rhow));
  d.checkUnknownInputs(allParams);
}
BCBoltzmannD2Q13Params<BCTypeDiffuseKinetic> BCBoltzmannD2Q13Params<BCTypeDiffuseKinetic>::params;

//===========================================================================//
// Instantiate BC parameters

// Pressure-Primitive Variables
typedef BCBoltzmannD2Q13Vector< TraitsSizeBoltzmannD2Q13, TraitsModelBoltzmannD2Q13<QTypePrimitiveDistributionFunctions, GasModel> > BCVector_PDFs;
BCPARAMETER_INSTANTIATE( BCVector_PDFs )

} //namespace SANS
