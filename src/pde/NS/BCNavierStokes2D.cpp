// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCNavierStokes2D.h"
#include "pde/NS/Q2DPrimitiveRhoPressure.h"
#include "pde/NS/Q2DPrimitiveSurrogate.h"
#include "pde/NS/Q2DEntropy.h"
#include "pde/NS/Q2DConservative.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
// cppcheck-suppress passedByValue
void BCNavierStokes2DParams<BCTypeWallNoSlipAdiabatic_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCNavierStokes2DParams<BCTypeWallNoSlipAdiabatic_mitState> BCNavierStokes2DParams<BCTypeWallNoSlipAdiabatic_mitState>::params;


// cppcheck-suppress passedByValue
void BCNavierStokes2DParams<BCTypeWallNoSlipIsothermal_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Twall));
  allParams.push_back(d.checkInputs(params.Upwind));
  d.checkUnknownInputs(allParams);
}
BCNavierStokes2DParams<BCTypeWallNoSlipIsothermal_mitState> BCNavierStokes2DParams<BCTypeWallNoSlipIsothermal_mitState>::params;

// cppcheck-suppress passedByValue
void BCNavierStokes2DParams<BCTypeSymmetry_mitState>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCNavierStokes2DParams<BCTypeSymmetry_mitState> BCNavierStokes2DParams<BCTypeSymmetry_mitState>::params;

//===========================================================================//
// Instantiate BC parameters

// Pressure-primitive variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_S )

// Conservative Variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Cons_S;
BCPARAMETER_INSTANTIATE( BCVector_Cons_S )

// Entropy Variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeEntropy, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Entropy_S;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_S )


// Surrogate Variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_Surrogate_S;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_S )


//===========================================================================//
// Repeat for modified form of Sutherland's law

// Pressure-primitive variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel,
                                      ViscosityModel_SutherlandModified, ThermalConductivityModel> > BCVector_RhoP_SM;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_SM )

// Conservative Variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeConservative, GasModel,
                                      ViscosityModel_SutherlandModified, ThermalConductivityModel> > BCVector_Cons_SM;
BCPARAMETER_INSTANTIATE( BCVector_Cons_SM )

// Entropy Variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeEntropy, GasModel,
                                      ViscosityModel_SutherlandModified, ThermalConductivityModel> > BCVector_Entropy_SM;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_SM )


// Surrogate Variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel,
                                      ViscosityModel_SutherlandModified, ThermalConductivityModel> > BCVector_Surrogate_SM;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_SM )

//===========================================================================//
// Repeat for constant viscosity

// Pressure-primitive variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_RhoP_C;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_C )

// Conservative Variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Cons_C;
BCPARAMETER_INSTANTIATE( BCVector_Cons_C )

// Entropy Variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeEntropy, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Entropy_C;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_C )

// Surrogate Variables
typedef BCNavierStokes2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveSurrogate, GasModel, ViscosityModel_Const, ThermalConductivityModel> > BCVector_Surrogate_C;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_C )

//===========================================================================//
// Instantiate BC parameters

// Pressure-primitive variables
typedef BCNavierStokesBrenner2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel,
                                      ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_BrennerRhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_BrennerRhoP_S )

// Conservative Variables
typedef BCNavierStokesBrenner2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeConservative, GasModel,
                                      ViscosityModel_Sutherland, ThermalConductivityModel> > BCVector_BrennerCons_S;
BCPARAMETER_INSTANTIATE( BCVector_BrennerCons_S )


//===========================================================================//
// Repeat for constant viscosity

// Pressure-primitive variables
typedef BCNavierStokesBrenner2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel,
                                      ViscosityModel_Const, ThermalConductivityModel> > BCVector_BrennerRhoP_C;
BCPARAMETER_INSTANTIATE( BCVector_BrennerRhoP_C )

// Conservative Variables
typedef BCNavierStokesBrenner2DVector<
              TraitsSizeNavierStokes,
              TraitsModelNavierStokes<QTypeConservative, GasModel,
                                      ViscosityModel_Const, ThermalConductivityModel> > BCVector_BrennerCons_C;
BCPARAMETER_INSTANTIATE( BCVector_BrennerCons_C )


} //namespace SANS
