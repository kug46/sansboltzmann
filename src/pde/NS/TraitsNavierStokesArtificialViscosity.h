// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef TRAITSNAVIERSTOKESARTIFICIALVISCOSITY_H
#define TRAITSNAVIERSTOKESARTIFICIALVISCOSITY_H

#include "TraitsNavierStokes.h"
#include "pde/ArtificialViscosity/TraitsArtificialViscosity.h"

namespace SANS
{

template <class PhysDim_>
using TraitsSizeNavierStokesArtificialViscosity = TraitsSizeArtificialViscosity<TraitsSizeNavierStokes,PhysDim_>;

}

#endif // TRAITSNAVIERSTOKESARTIFICIALVISCOSITY_H
