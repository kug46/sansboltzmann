// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEBOLTZMANND2Q9_H
#define PDEBOLTZMANND2Q9_H

// 2-D compressible Euler

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "pde/ForcingFunctionBase.h"
#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "EulerResidCategory.h"
#include "GasModel.h"
#include "RoeBoltzmann.h"
//#include "HLLCD2Q9.h"
#include "BoltzmannVariableD2Q9.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr
#include <math.h>             // isnan

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class QD2Q9;


//----------------------------------------------------------------------------//
// PDE class: 2-D Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. Real)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEBoltzmannD2Q9
{
public:

  enum EulerResidualInterpCategory
  {
    Euler_ResidInterp_Raw,
    Euler_ResidInterp_Momentum,
    Euler_ResidInterp_Entropy
  };

  enum BoltzmannEquationMode
  {
    AdvectionDiffusion,
    Hydrodynamics
  };

  enum CollisionOperatorCategory
  {
    BGK
  };

  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef PDETraitsModel TraitsModel;

  typedef typename PDETraitsModel::QType QType;
  typedef QD2Q9<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                     // gas model

  typedef RoeBoltzmannD2Q9<QType, PDETraitsSize> FluxType;

  typedef BoltzmannVariableTypeD2Q9Params VariableTypeD2Q9Params;

  //typedef ForcingFunctionBaseD2Q9< PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel> > ForcingFunctionType;

  explicit PDEBoltzmannD2Q9( const GasModel& gas0,
                       EulerResidualInterpCategory cat,
                       CollisionOperatorCategory omega = BGK,
                       const Real tau = 1.,
                       BoltzmannEquationMode eqMode = AdvectionDiffusion,
                       const Real Uadv = 0.,
                       const Real Vadv = 0.,
                       const RoeEntropyFix entropyFix = none) :
    gas_(gas0),
    qInterpret_(gas0),
    cat_(cat),
    omega_(omega),
    tau_(tau),
    eqMode_(eqMode),
    Uadv_(Uadv),
    Vadv_(Vadv),
    flux_(gas0, entropyFix)
  {
    // Nothing
  }

  static const int if0 = 0;
  static const int if1 = 1;
  static const int if2 = 2;
  static const int if3 = 3;
  static const int if4 = 4;
  static const int if5 = 5;
  static const int if6 = 6;
  static const int if7 = 7;
  static const int if8 = 8;

  static_assert( if0 == RoeBoltzmannD2Q9<QType, PDETraitsSize>::if0, "Must match" );
  static_assert( if1 == RoeBoltzmannD2Q9<QType, PDETraitsSize>::if1, "Must match" );
  static_assert( if2 == RoeBoltzmannD2Q9<QType, PDETraitsSize>::if2, "Must match" );
  static_assert( if3 == RoeBoltzmannD2Q9<QType, PDETraitsSize>::if3, "Must match" );
  static_assert( if4 == RoeBoltzmannD2Q9<QType, PDETraitsSize>::if4, "Must match" );
  static_assert( if5 == RoeBoltzmannD2Q9<QType, PDETraitsSize>::if5, "Must match" );
  static_assert( if6 == RoeBoltzmannD2Q9<QType, PDETraitsSize>::if6, "Must match" );
  static_assert( if7 == RoeBoltzmannD2Q9<QType, PDETraitsSize>::if7, "Must match" );
  static_assert( if8 == RoeBoltzmannD2Q9<QType, PDETraitsSize>::if8, "Must match" );

  PDEBoltzmannD2Q9( const PDEBoltzmannD2Q9& pde0 ) = delete;
  PDEBoltzmannD2Q9& operator=( const PDEBoltzmannD2Q9& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // unsteady temporal flux: Ft(Q)
  template<class Tq, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template<class Tq, class Tf>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const;

  // master state: U(Q)
  template<class Tf, class Tq>
  void workingState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tf>& uCons, ArrayQ<Tq>& q ) const;

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const;


  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    fluxAdvective(x, y, time, q, f, g);
  }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwind(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const;


  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const {}

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const {}

  template <class Tq, class Tg, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,
      MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const {}

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, time, q, qx, qy, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const {}

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const;

  // characteristic speed (needed for timestep)
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variable
  template<class T, class Varibles>
  void setDOFFrom(
      ArrayQ<T>& q, const BoltzmannVariableTypeD2Q9<Varibles>& data ) const;

  // set from variable
  template<class Varibles>
  ArrayQ<Real> setDOFFrom( const BoltzmannVariableTypeD2Q9<Varibles>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // return the residual-interpreter type
  EulerResidualInterpCategory category() const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // accessors for gas model and Q interpreter
  const GasModel& gasModel() const { return gas_; }
  const QInterpret& variableInterpreter() const { return qInterpret_; }

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const GasModel gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  EulerResidualInterpCategory cat_; // Residual interp type
  CollisionOperatorCategory omega_;
  const Real tau_;
  BoltzmannEquationMode eqMode_;
  const Real Uadv_;
  const Real Vadv_;
  const FluxType flux_;
};


// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::masterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const
{
  uCons(if0) = q(if0);
  uCons(if1) = q(if1);
  uCons(if2) = q(if2);
  uCons(if3) = q(if3);
  uCons(if4) = q(if4);
  uCons(if5) = q(if5);
  uCons(if6) = q(if6);
  uCons(if7) = q(if7);
  uCons(if8) = q(if8);
}

// unsteady conservative flux: U(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tf, class Tq>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::workingState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tf>& uCons, ArrayQ<Tq>& q ) const
{
  PrimitiveDistributionFunctionsD2Q9<Tf> data(uCons(if0), uCons(if1), uCons(if2),
                                              uCons(if3), uCons(if4), uCons(if5),
                                              uCons(if6), uCons(if7), uCons(if8));
  qInterpret_.setFromPrimitive(q, data);
}


// unsteady conservative flux Jacobian: dU(Q)/dQ
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::jacobianMasterState(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  ArrayQ<T> pdf0_q, pdf1_q, pdf2_q, pdf3_q, pdf4_q, pdf5_q, pdf6_q, pdf7_q, pdf8_q;
  T pdf0 = 0, pdf1 = 0, pdf2 = 0,
    pdf3 = 0, pdf4 = 0, pdf5 = 0,
    pdf6 = 0, pdf7 = 0, pdf8 = 0;

  qInterpret_.eval( q, pdf0, pdf1, pdf2, pdf3, pdf4, pdf5, pdf6, pdf7, pdf8);
  qInterpret_.evalJacobian( q, pdf0_q, pdf1_q, pdf2_q,
                               pdf3_q, pdf4_q, pdf5_q,
                               pdf6_q, pdf7_q, pdf8_q);

  dudq(if0,if0) = pdf0_q[0];
  dudq(if0,if1) = pdf0_q[1];
  dudq(if0,if2) = pdf0_q[2];
  dudq(if0,if3) = pdf0_q[3];
  dudq(if0,if4) = pdf0_q[4];
  dudq(if0,if5) = pdf0_q[5];
  dudq(if0,if6) = pdf0_q[6];
  dudq(if0,if7) = pdf0_q[7];
  dudq(if0,if8) = pdf0_q[8];

  dudq(if1,if0) = pdf1_q[0];
  dudq(if1,if1) = pdf1_q[1];
  dudq(if1,if2) = pdf1_q[2];
  dudq(if1,if3) = pdf1_q[3];
  dudq(if1,if4) = pdf1_q[4];
  dudq(if1,if5) = pdf1_q[5];
  dudq(if1,if6) = pdf1_q[6];
  dudq(if1,if7) = pdf1_q[7];
  dudq(if1,if8) = pdf1_q[8];

  dudq(if2,if0) = pdf2_q[0];
  dudq(if2,if1) = pdf2_q[1];
  dudq(if2,if2) = pdf2_q[2];
  dudq(if2,if3) = pdf2_q[3];
  dudq(if2,if4) = pdf2_q[4];
  dudq(if2,if5) = pdf2_q[5];
  dudq(if2,if6) = pdf2_q[6];
  dudq(if2,if7) = pdf2_q[7];
  dudq(if2,if8) = pdf2_q[8];

  dudq(if3,if0) = pdf3_q[0];
  dudq(if3,if1) = pdf3_q[1];
  dudq(if3,if2) = pdf3_q[2];
  dudq(if3,if3) = pdf3_q[3];
  dudq(if3,if4) = pdf3_q[4];
  dudq(if3,if5) = pdf3_q[5];
  dudq(if3,if6) = pdf3_q[6];
  dudq(if3,if7) = pdf3_q[7];
  dudq(if3,if8) = pdf3_q[8];

  dudq(if4,if0) = pdf4_q[0];
  dudq(if4,if1) = pdf4_q[1];
  dudq(if4,if2) = pdf4_q[2];
  dudq(if4,if3) = pdf4_q[3];
  dudq(if4,if4) = pdf4_q[4];
  dudq(if4,if5) = pdf4_q[5];
  dudq(if4,if6) = pdf4_q[6];
  dudq(if4,if7) = pdf4_q[7];
  dudq(if4,if8) = pdf4_q[8];

  dudq(if5,if0) = pdf5_q[0];
  dudq(if5,if1) = pdf5_q[1];
  dudq(if5,if2) = pdf5_q[2];
  dudq(if5,if3) = pdf5_q[3];
  dudq(if5,if4) = pdf5_q[4];
  dudq(if5,if5) = pdf5_q[5];
  dudq(if5,if6) = pdf5_q[6];
  dudq(if5,if7) = pdf5_q[7];
  dudq(if5,if8) = pdf5_q[8];

  dudq(if6,if0) = pdf6_q[0];
  dudq(if6,if1) = pdf6_q[1];
  dudq(if6,if2) = pdf6_q[2];
  dudq(if6,if3) = pdf6_q[3];
  dudq(if6,if4) = pdf6_q[4];
  dudq(if6,if5) = pdf6_q[5];
  dudq(if6,if6) = pdf6_q[6];
  dudq(if6,if7) = pdf6_q[7];
  dudq(if6,if8) = pdf6_q[8];

  dudq(if7,if0) = pdf7_q[0];
  dudq(if7,if1) = pdf7_q[1];
  dudq(if7,if2) = pdf7_q[2];
  dudq(if7,if3) = pdf7_q[3];
  dudq(if7,if4) = pdf7_q[4];
  dudq(if7,if5) = pdf7_q[5];
  dudq(if7,if6) = pdf7_q[6];
  dudq(if7,if7) = pdf7_q[7];
  dudq(if7,if8) = pdf7_q[8];

  dudq(if8,if0) = pdf8_q[0];
  dudq(if8,if1) = pdf8_q[1];
  dudq(if8,if2) = pdf8_q[2];
  dudq(if8,if3) = pdf8_q[3];
  dudq(if8,if4) = pdf8_q[4];
  dudq(if8,if5) = pdf8_q[5];
  dudq(if8,if6) = pdf8_q[6];
  dudq(if8,if7) = pdf8_q[7];
  dudq(if8,if8) = pdf8_q[8];

}


// strong form of unsteady conservative flux: dU(Q)/dt
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& utCons ) const
{
  T pdf0t = 0, pdf1t = 0, pdf2t = 0, pdf3t = 0, pdf4t = 0, pdf5t = 0, pdf6t = 0, pdf7t = 0, pdf8t = 0;

  qInterpret_.evalGradient(q, qt, pdf0t, pdf1t, pdf2t, pdf3t, pdf4t, pdf5t, pdf6t, pdf7t, pdf8t);

  utCons(if0) += pdf0t;
  utCons(if1) += pdf1t;
  utCons(if2) += pdf2t;
  utCons(if3) += pdf3t;
  utCons(if4) += pdf4t;
  utCons(if5) += pdf5t;
  utCons(if6) += pdf6t;
  utCons(if7) += pdf7t;
  utCons(if8) += pdf8t;
}


// advective flux: F(Q)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  const int e[9][2] = {   {  0,  0 }, {  1,  0 }, {  0,  1 },
                          { -1,  0 }, {  0, -1 }, {  1,  1 },
                          { -1,  1 }, { -1, -1 }, {  1, -1 }    };

// TODO: Check if this is consistent with what is actually necessary.
  f(if0) += e[0][0]*q(if0);
  f(if1) += e[1][0]*q(if1);
  f(if2) += e[2][0]*q(if2);
  f(if3) += e[3][0]*q(if3);
  f(if4) += e[4][0]*q(if4);
  f(if5) += e[5][0]*q(if5);
  f(if6) += e[6][0]*q(if6);
  f(if7) += e[7][0]*q(if7);
  f(if8) += e[8][0]*q(if8);

  g(if0) += e[0][1]*q(if0);
  g(if1) += e[1][1]*q(if1);
  g(if2) += e[2][1]*q(if2);
  g(if3) += e[3][1]*q(if3);
  g(if4) += e[4][1]*q(if4);
  g(if5) += e[5][1]*q(if5);
  g(if6) += e[6][1]*q(if6);
  g(if7) += e[7][1]*q(if7);
  g(if8) += e[8][1]*q(if8);

}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  flux_.fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f);
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{

  SANS_DEVELOPER_EXCEPTION(" SpaceTime is not implemented in Boltzmann Equation Solver.");
#if 0
//  flux_.fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tq> fnx = 0;
  fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fnx );

  //Upwind temporal flux
  ArrayQ<Tq> ft = 0;
  if (nt >= 0)
    fluxAdvectiveTime( x, y, time, qL, ft );
  else
    fluxAdvectiveTime( x, y, time, qR, ft );

  f += ft*nt + fnx;
#endif
}


// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  const int e[9][2] = {   {  0,  0 }, {  1,  0 }, {  0,  1 },
                          { -1,  0 }, {  0, -1 }, {  1,  1 },
                          { -1,  1 }, { -1, -1 }, {  1, -1 }    };

  dfdu = 0.;
  dgdu = 0.;

  dfdu(if0,if0) = e[0][0];
  dfdu(if1,if1) = e[1][0];
  dfdu(if2,if2) = e[2][0];
  dfdu(if3,if3) = e[3][0];
  dfdu(if4,if4) = e[4][0];
  dfdu(if5,if5) = e[5][0];
  dfdu(if6,if6) = e[6][0];
  dfdu(if7,if7) = e[7][0];
  dfdu(if8,if8) = e[8][0];

  dgdu(if0,if0) = e[0][1];
  dgdu(if1,if1) = e[1][1];
  dgdu(if2,if2) = e[2][1];
  dgdu(if3,if3) = e[3][1];
  dgdu(if4,if4) = e[4][1];
  dgdu(if5,if5) = e[5][1];
  dgdu(if6,if6) = e[6][1];
  dgdu(if7,if7) = e[7][1];
  dgdu(if8,if8) = e[8][1];

}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& Nx, const Real& Ny, MatrixQ<Tf>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION("JacobianFluxAdvectiveAbsVal is needed...");
#if 0
  Real nMag = sqrt(Nx*Nx + Ny*Ny);
  Real nx = Nx/nMag;
  Real ny = Ny/nMag;

  if ( std::isnan(nx) || std::isnan(ny)  ) {}
  else
  {

    Real gamma = gas_.gamma();
    Tq rho, u, v, t, h, H;

    qInterpret_.eval( q, rho, u, v, t );

    Real gm1 = gamma - 1.0;

    Tq qs  = -ny*u + nx*v;
    Tq qn  =  nx*u + ny*v;
    Tq q2  =  u*u + v*v;

    h = gas_.enthalpy(rho, t);
    H = h + 0.5*q2;

    Tq c  = gas_.speedofSound( t ) ;
    Tq c2 = c*c;

    //pre-multiply the eigenvalue by the normal magnitude to save time
    Tq eigumc = fabs(qn - c)*nMag;
    Tq eigupc = fabs(qn + c)*nMag;
    Tq eigu   = fabs(qn    )*nMag;

    mtx(0,0) += (1 - q2*gm1*0.5/c2)*eigu + (2*c*qn + q2*gm1)*eigumc*0.25/c2 + (-2*c*qn + q2*gm1)*eigupc*0.25/c2;
    mtx(1,0) += ny*qs*eigu + u*(1 - 0.5*q2*gm1/c2)*eigu + (-c*nx + u)*(2*c*qn + q2*gm1)*eigumc*0.25/c2
        + (c*nx + u)*(-2*c*qn + q2*gm1)*eigupc*0.25/c2;
    mtx(2,0) += -nx*qs*eigu + v*(1 - 0.5*q2*gm1/c2)*eigu + (-c*ny + v)*(2*c*qn + q2*gm1)*eigumc*0.25/c2
        + (c*ny + v)*(-2*c*qn + q2*gm1)*eigupc*0.25/c2;
    mtx(3,0) += -qs*qs*eigu + 0.5*q2*(1 - 0.5*q2*gm1/c2)*eigu + (H - c*qn)*(2*c*qn + q2*gm1)*eigumc*0.25/c2
        + (H+c*qn)*(-2*c*qn + q2*gm1)*eigupc*0.25/c2;

    mtx(0,1) += u*gm1*eigu/c2 - (c*nx + u*gm1)*eigumc*0.5/c2 + (c*nx - u*gm1)*eigupc*0.5/c2;
    mtx(1,1) += ny*ny*eigu + u*u*gm1*eigu/c2 - (-c*nx + u)*(c*nx + u*gm1)*eigumc*0.5/c2 + (c*nx + u)*(c*nx - u*gm1)*eigupc*0.5/c2;
    mtx(2,1) += -nx*ny*eigu + u*v*gm1*eigu/c2 - (-c*ny + v)*(c*nx + u*gm1)*eigumc*0.5/c2 + (c*ny + v)*(c*nx - u*gm1)*eigupc*0.5/c2;
    mtx(3,1) += -ny*qs*eigu + u*q2*gm1*eigu*0.5/c2 - (H - c*qn)*(c*nx + u*gm1)*eigumc*0.5/c2 + (H + c*qn)*(c*nx - u*gm1)*eigupc*0.5/c2;

    mtx(0,2) += v*gm1*eigu/c2 - (c*ny + v*gm1)*eigumc*0.5/c2 + (c*ny - v*gm1)*eigupc*0.5/c2;
    mtx(1,2) += -nx*ny*eigu + u*v*gm1*eigu/c2 - (-c*nx+u)*(c*ny + v*gm1)*eigumc*0.5/c2 + (c*nx+u)*(c*ny - v*gm1)*eigupc*0.5/c2;
    mtx(2,2) += nx*nx*eigu + v*v*gm1*eigu/c2 - (-c*ny+v)*(c*ny + v*gm1)*eigumc*0.5/c2 + (c*ny+v)*(c*ny - v*gm1)*eigupc*0.5/c2;
    mtx(3,2) += nx*qs*eigu + v*q2*gm1*eigu*0.5/c2 - (H-c*qn)*(c*ny + v*gm1)*eigumc*0.5/c2 + (H+c*qn)*(c*ny - v*gm1)*eigupc*0.5/c2;

    mtx(0,3) += -gm1*eigu/c2 + gm1*eigumc*0.5/c2 + gm1*eigupc*0.5/c2;
    mtx(1,3) += -u*gm1*eigu/c2 + (-c*nx + u)*gm1*eigumc*0.5/c2 + (c*nx+u)*gm1*eigupc*0.5/c2;
    mtx(2,3) += -v*gm1*eigu/c2 + (-c*ny + v)*gm1*eigumc*0.5/c2 + (c*ny+v)*gm1*eigupc*0.5/c2;
    mtx(3,3) += -q2*gm1*eigu*0.5/c2 + (H - c*qn)*gm1*eigumc*0.5/c2 + (H + c*qn)*gm1*eigupc*0.5/c2;
  }
#endif
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tf>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION("SpaceTime has not been implemented in Boltzmann Eqation Solver.");
//  flux_.jacobianFluxAdvectiveAbsoluteValueSpaceTime(x, y, time, q, nx, ny, nt, mtx);
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type T;
  const int e[9][2] = {   {  0,  0 }, {  1,  0 }, {  0,  1 },
                          { -1,  0 }, {  0, -1 }, {  1,  1 },
                          { -1,  1 }, { -1, -1 }, {  1, -1 }    };
  T pdf0x = 0, pdf1x = 0, pdf2x = 0, pdf3x = 0, pdf4x = 0, pdf5x = 0, pdf6x = 0, pdf7x = 0, pdf8x = 0;
  T pdf0y = 0, pdf1y = 0, pdf2y = 0, pdf3y = 0, pdf4y = 0, pdf5y = 0, pdf6y = 0, pdf7y = 0, pdf8y = 0;

  qInterpret_.evalGradient(q, qx, pdf0x, pdf1x, pdf2x, pdf3x, pdf4x, pdf5x, pdf6x, pdf7x, pdf8x);
  qInterpret_.evalGradient(q, qy, pdf0y, pdf1y, pdf2y, pdf3y, pdf4y, pdf5y, pdf6y, pdf7y, pdf8y);

  strongPDE(if0) += e[0][0]*pdf0x + e[0][1]*pdf0y;
  strongPDE(if1) += e[1][0]*pdf1x + e[1][1]*pdf1y;
  strongPDE(if2) += e[2][0]*pdf2x + e[2][1]*pdf2y;
  strongPDE(if3) += e[3][0]*pdf3x + e[3][1]*pdf3y;
  strongPDE(if4) += e[4][0]*pdf4x + e[4][1]*pdf4y;
  strongPDE(if5) += e[5][0]*pdf5x + e[5][1]*pdf5y;
  strongPDE(if6) += e[6][0]*pdf6x + e[6][1]*pdf6y;
  strongPDE(if7) += e[7][0]*pdf7x + e[7][1]*pdf7y;
  strongPDE(if8) += e[8][0]*pdf8x + e[8][1]*pdf8y;

}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Ts>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::source(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  Tq feq0 = 0, feq1 = 0, feq2 = 0, feq3 = 0, feq4 = 0, feq5 = 0, feq6 = 0, feq7 = 0, feq8 = 0;
  Tq rho = 0., U = 0., V = 0.;
  Tq e_dot_u = 0., u_dot_u = 0.;        // terms in feq

  const Real csq = 1./3.;
  const int e[9][2] = {   {  0,  0 }, {  1,  0 }, {  0,  1 },
                          { -1,  0 }, {  0, -1 }, {  1,  1 },
                          { -1,  1 }, { -1, -1 }, {  1, -1 }    };

  rho = q(if0) + q(if1) + q(if2) +
        q(if3) + q(if4) + q(if5) +
        q(if6) + q(if7) + q(if8);

  switch (eqMode_)
  {
    case AdvectionDiffusion:
    {
      U = Uadv_;
      V = Vadv_;
      break;
    }
    case Hydrodynamics:
    {
      qInterpret_.evalVelocity(q, U, V);
      break;
    }
    default: SANS_DEVELOPER_EXCEPTION("Unknown collision operator.");
  }

  const Real w0 = 16./36.;
  const Real w1 =  4./36.;
  const Real w2 =  4./36.;
  const Real w3 =  4./36.;
  const Real w4 =  4./36.;
  const Real w5 =  1./36.;
  const Real w6 =  1./36.;
  const Real w7 =  1./36.;
  const Real w8 =  1./36.;

  u_dot_u = U*U + V*V;
#if 0
  e_dot_u = e[0][0]*U + e[0][1]*V;
  feq0 = w0*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));

  e_dot_u = e[1][0]*U + e[1][1]*V;
  feq1 = w1*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));

  e_dot_u = e[2][0]*U + e[2][1]*V;
  feq2 = w2*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));

  e_dot_u = e[3][0]*U + e[3][1]*V;
  feq3 = w3*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));

  e_dot_u = e[4][0]*U + e[4][1]*V;
  feq4 = w4*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));

  e_dot_u = e[5][0]*U + e[5][1]*V;
  feq5 = w5*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));

  e_dot_u = e[6][0]*U + e[6][1]*V;
  feq6 = w6*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));

  e_dot_u = e[7][0]*U + e[7][1]*V;
  feq7 = w7*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));

  e_dot_u = e[8][0]*U + e[8][1]*V;
  feq8 = w8*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq));
#endif
  e_dot_u = e[0][0]*U + e[0][1]*V;
  feq0 = w0*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) + 
                  e_dot_u*e_dot_u*e_dot_u/(2*csq*csq*csq) - 3*e_dot_u*u_dot_u/(2*csq*csq));

  e_dot_u = e[1][0]*U + e[1][1]*V;
  feq1 = w1*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) + 
                  e_dot_u*e_dot_u*e_dot_u/(2*csq*csq*csq) - 3*e_dot_u*u_dot_u/(2*csq*csq));

  e_dot_u = e[2][0]*U + e[2][1]*V;
  feq2 = w2*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) + 
                  e_dot_u*e_dot_u*e_dot_u/(2*csq*csq*csq) - 3*e_dot_u*u_dot_u/(2*csq*csq));

  e_dot_u = e[3][0]*U + e[3][1]*V;
  feq3 = w3*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) + 
                  e_dot_u*e_dot_u*e_dot_u/(2*csq*csq*csq) - 3*e_dot_u*u_dot_u/(2*csq*csq));

  e_dot_u = e[4][0]*U + e[4][1]*V;
  feq4 = w4*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) + 
                  e_dot_u*e_dot_u*e_dot_u/(2*csq*csq*csq) - 3*e_dot_u*u_dot_u/(2*csq*csq));

  e_dot_u = e[5][0]*U + e[5][1]*V;
  feq5 = w5*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) + 
                  e_dot_u*e_dot_u*e_dot_u/(2*csq*csq*csq) - 3*e_dot_u*u_dot_u/(2*csq*csq));

  e_dot_u = e[6][0]*U + e[6][1]*V;
  feq6 = w6*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) + 
                  e_dot_u*e_dot_u*e_dot_u/(2*csq*csq*csq) - 3*e_dot_u*u_dot_u/(2*csq*csq));

  e_dot_u = e[7][0]*U + e[7][1]*V;
  feq7 = w7*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) + 
                  e_dot_u*e_dot_u*e_dot_u/(2*csq*csq*csq) - 3*e_dot_u*u_dot_u/(2*csq*csq));

  e_dot_u = e[8][0]*U + e[8][1]*V;
  feq8 = w8*rho*( 1 + e_dot_u/csq + (e_dot_u)*(e_dot_u)/(2*csq*csq) - u_dot_u/(2*csq) + 
                  e_dot_u*e_dot_u*e_dot_u/(2*csq*csq*csq) - 3*e_dot_u*u_dot_u/(2*csq*csq));

  switch (omega_)
  {
    case BGK:
#if 1
      source(if0) -= (1./tau_)*(feq0 - q(if0));
      source(if1) -= (1./tau_)*(feq1 - q(if1));
      source(if2) -= (1./tau_)*(feq2 - q(if2));
      source(if3) -= (1./tau_)*(feq3 - q(if3));
      source(if4) -= (1./tau_)*(feq4 - q(if4));
      source(if5) -= (1./tau_)*(feq5 - q(if5));
      source(if6) -= (1./tau_)*(feq6 - q(if6));
      source(if7) -= (1./tau_)*(feq7 - q(if7));
      source(if8) -= (1./tau_)*(feq8 - q(if8));

#endif
      break;
    default: SANS_DEVELOPER_EXCEPTION("Unknown collision operator.");
  }



}


// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const
{
  Tq t = 0, c = 0;
  Tq u = 0, v = 0;

  if (Uadv_ == 0 && Vadv_ == 0)
    qInterpret_.evalVelocity(q,u,v);
  else
  {
     u = Uadv_;
     v = Vadv_;
  }
  qInterpret_.evalTemperature( q, t );
  c = gas_.speedofSound(t);

  speed = fabs(dx*u + dy*v)/sqrt(dx*dx + dy*dy) + c;
}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tc>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const
{
  Tq t = 0, c = 0;
  Tq u = 0, v = 0;

  if (Uadv_ == 0 && Vadv_ == 0)
    qInterpret_.evalVelocity(q,u,v);
  else
  {
     u = Uadv_;
     v = Vadv_;
  }
  qInterpret_.evalTemperature( q, t );
  c = gas_.speedofSound(t);

  speed = sqrt(u*u + v*v) + c;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::
updateFraction( const Real& x, const Real& y, const Real& time,
                const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  qInterpret_.updateFraction(q, dq, maxChangeFraction, updateFraction);
}


// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return qInterpret_.isValidState(q);
}


// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  qInterpret_.setFromPrimitive( q, data, name, nn );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Varibles>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const BoltzmannVariableTypeD2Q9<Varibles>& data ) const
{
  qInterpret_.setFromPrimitive( q, data.cast() );
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Varibles>
inline typename PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::setDOFFrom( const BoltzmannVariableTypeD2Q9<Varibles>& data ) const
{
  ArrayQ<Real> q;
  qInterpret_.setFromPrimitive( q, data.cast() );
  return q;
}


// set from variables
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q;
  DictKeyPair data = d.get(BoltzmannVariableTypeD2Q9Params::params.StateVector);

  if ( data == BoltzmannVariableTypeD2Q9Params::params.StateVector.PrimitiveDistributionFunctions9 )
    qInterpret_.setFromPrimitive( q, PrimitiveDistributionFunctionsD2Q9<Real>(data) );
  else
    SANS_DEVELOPER_EXCEPTION(" Missing state vector option ");

  return q;
}

// --------------------- HERE! TODO TODO TODO
// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{

  int nOut = rsdPDEOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdPDEOut[i] = rsdPDEIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Momentum not implemented in D2Q9");
/*    SANS_ASSERT(nOut == N-1);
    rsdPDEOut[0] = rsdPDEIn[iCont];
    rsdPDEOut[1] = sqrt( rsdPDEIn[ixMom]*rsdPDEIn[ixMom] + rsdPDEIn[iyMom]*rsdPDEIn[iyMom] );
    rsdPDEOut[2] = rsdPDEIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdPDEOut[2+i] = rsdPDEIn[iEngy+i];*/
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in D2Q9");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    for (int i = 0; i < N; i++)
      rsdPDEOut[0] += rsdPDEIn[i]*rsdPDEIn[i];

    rsdPDEOut[0] = sqrt(rsdPDEOut[0]);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
    {
      rsdAuxOut[i] = rsdAuxIn[i];
    }
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Momentum not implemented in D2Q9");
/*    SANS_ASSERT(nOut == N-1);
    rsdAuxOut[0] = rsdAuxIn[iCont];
    rsdAuxOut[1] = sqrt(rsdAuxIn[ixMom]*rsdAuxIn[ixMom] + rsdAuxIn[iyMom]*rsdAuxIn[iyMom]);
    rsdAuxOut[2] = rsdAuxIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdAuxOut[2+i] = rsdAuxIn[iEngy+i]; */
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
//    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in D2Q9");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    for (int i = 0; i < N; i++)
      rsdAuxOut[0] += rsdAuxIn[i]*rsdAuxIn[i];

    rsdAuxOut[0] = sqrt(rsdAuxOut[0]);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  if (cat_==Euler_ResidInterp_Raw) // raw residual
  {
    SANS_ASSERT(nOut == N );
    for (int i = 0; i < N; i++)
      rsdBCOut[i] = rsdBCIn[i];
  }
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
  {
    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Momentum not implemented in D2Q9");
/*    SANS_ASSERT(nOut == N-1);
    rsdBCOut[0] = rsdBCIn[iCont];
    rsdBCOut[1] = sqrt( rsdBCIn[ixMom]*rsdBCIn[ixMom] + rsdBCIn[iyMom]*rsdBCIn[iyMom] );
    rsdBCOut[2] = rsdBCIn[iEngy];
    for (int i = 1; i < N-iEngy; i++)
      rsdBCOut[2+i] = rsdBCIn[iEngy+i];*/
  }
  else if (cat_ == Euler_ResidInterp_Entropy)
  {
    SANS_ASSERT(nOut == 1);
//    SANS_DEVELOPER_EXCEPTION("Euler_ResidInterp_Entropy not implemented in D2Q9");
    //TODO: Implement entropy residual - the following is just a summation of the residuals for now
    for (int i = 0; i < N; i++)
      rsdBCOut[0] += rsdBCIn[i]*rsdBCIn[i];

    rsdBCOut[0] = sqrt(rsdBCOut[0]);
  }
  else
  {
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );
  }
}

#if 0
// return the interp type
template <template <class> class PDETraitsSize, class PDETraitsModel>
EulerResidualInterpCategory
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::category() const
{
  return cat_;
}
#endif

// how many residual equations are we dealing with
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline int
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::nMonitor() const
{
  int nMon;

  if (cat_==Euler_ResidInterp_Raw) // raw residual
    nMon = N;
  else if (cat_==Euler_ResidInterp_Momentum) // combine momentum
    nMon = N-1;
  else if (cat_ == Euler_ResidInterp_Entropy)
    nMon = 1;
  else
    SANS_DEVELOPER_EXCEPTION( "Invalid Residual Interp category" );

  return nMon;
}

#if 0
// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  (*forcing_)(*this, x, y, time, forcing );
}
#endif

template <template <class> class PDETraitsSize, class PDETraitsModel>
inline std::vector<std::string>
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::
derivedQuantityNames() const
{
  return {
          // Nothing
         };
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class Tq, class Tg, class Tf>
inline void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::
derivedQuantity(const int& index, const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  switch (index)
  {
  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEBoltzmannD2Q9::derivedQuantity - Invalid index!");
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDEBoltzmannD2Q9<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // PDEBOLTZMANND2Q9_H
