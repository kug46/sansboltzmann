// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER2D_SOLUTION_H
#define BCEULER2D_SOLUTION_H

#include <memory> // shared_ptr
#include <vector>

#include "Python/PyDict.h"
#include "Python/Parameter.h"


#include "pde/NS/SolutionFunction_Euler2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"
#include "pde/NS/SolutionFunction_RANSSA2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Struct for creating a BC solution function

struct BCEulerSolution2DOptions : noncopyable
{
  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Name{"Name", NO_DEFAULT, "Solution function used on the BC" };
    const ParameterString& key = Name;

    const DictOption Const{"Const", SolutionFunction_Euler2D_Const_Params::checkInputs};
    const DictOption OjedaMMS{"OjedaMMS", SolutionFunction_NavierStokes2D_OjedaMMS_Params::checkInputs};
    const DictOption Hartmann{"Hartmann", SolutionFunction_NavierStokes2D_Hartmann_Params::checkInputs};
    const DictOption QuadraticMMS{"QuadraticMMS", SolutionFunction_NavierStokes2D_QuadraticMMS_Params::checkInputs};
    const DictOption Riemann{"Riemann", SolutionFunction_Euler2D_Riemann_Params::checkInputs};
    const DictOption Wake{"Wake", SolutionFunction_Euler2D_Wake_Params::checkInputs};
    const DictOption TrigMMS{"TrigMMS", SolutionFunction_Euler2D_TrigMMS_Params::checkInputs};
    const DictOption SAWake{"SAWake", SolutionFunction_RANSSA2D_Wake_Params::checkInputs};

    const std::vector<DictOption> options{ Const, OjedaMMS, Hartmann, QuadraticMMS, Riemann, Wake, TrigMMS, SAWake };
  };
  const ParameterOption<SolutionOptions> Function{"Function", NO_DEFAULT, "The BC is a solution function"};

};

template<template <class> class PDETraitsSize>
struct BCEulerSolution2DParams : public BCEulerSolution2DOptions
{
};


// Struct for creating the solution pointer
template <template <class> class PDETraitsSize, class PDETraitsModel>
struct BCEulerSolution2D
{
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  typedef Function2DBase<ArrayQ<Real>> Function2DBaseType;
  typedef std::shared_ptr<Function2DBaseType> Function_ptr;

  static Function_ptr getSolution(const PyDict& d);
};

} // namespace SANS

#endif // BCEULER2D_SOLUTION_H
