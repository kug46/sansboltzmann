// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q2DPRIMITIVERHOPRESSURE_H
#define Q2DPRIMITIVERHOPRESSURE_H

// solution interpreter class
// Euler/N-S conservation-eqns: primitive variables (rho, u, v, p)

#include <vector>
#include <string>

#include "tools/minmax.h"
#include "Topology/Dimension.h"
#include "GasModel.h"
#include "NSVariable2D.h"
#include "Surreal/PromoteSurreal.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 2-D Euler/N-S
// primitive variables (rho, u, v, p)
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (rho, u, v, p)
class QTypePrimitiveRhoPressure;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q2D;


template <template <class> class PDETraitsSize>
class Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  static const int ir = 0;
  static const int iu = 1;
  static const int iv = 2;
  static const int ip = 3;

  // The three components of the state vector that make up the velocity vector
  static const int ix = iu;
  static const int iy = iv;

  static std::vector<std::string> stateNames() { return {"rho", "u", "v", "p"}; }

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q2D( const GasModel& gas0 ) : gas_(gas0) {}
  Q2D( const Q2D& q0 ) : gas_(q0.gas_) {}
  ~Q2D() {}

  Q2D& operator=( const Q2D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& v, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u, T& v ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalDensityJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                                         ArrayQ<T>& u_q, ArrayQ<T>& v_q,
                                         ArrayQ<T>& t_q ) const;

  template<class Tq, class Tg>
  void evalDensityGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                            typename promote_Surreal<Tq,Tg>::type& rho_x ) const;

  template<class Tq, class Tg>
  void evalPressureGradient( const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                             typename promote_Surreal<Tq,Tg>::type& p_x ) const;

  template<class Tq, class Tg>
  void evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type& ux,
    typename promote_Surreal<Tq,Tg>::type& vx,
    typename promote_Surreal<Tq,Tg>::type& tx ) const;

  template<class Tq, class Tg, class Th>
  void evalSecondGradient(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      typename promote_Surreal<Tq, Tg, Th>::type& rhoxx,
      typename promote_Surreal<Tq, Tg, Th>::type& rhoxy,
      typename promote_Surreal<Tq, Tg, Th>::type& rhoyy,
      typename promote_Surreal<Tq, Tg, Th>::type& uxx,
      typename promote_Surreal<Tq, Tg, Th>::type& uxy,
      typename promote_Surreal<Tq, Tg, Th>::type& uyy,
      typename promote_Surreal<Tq, Tg, Th>::type& vxx,
      typename promote_Surreal<Tq, Tg, Th>::type& vxy,
      typename promote_Surreal<Tq, Tg, Th>::type& vyy,
      typename promote_Surreal<Tq, Tg, Th>::type& txx,
      typename promote_Surreal<Tq, Tg, Th>::type& txy,
      typename promote_Surreal<Tq, Tg, Th>::type& tyy ) const;

  template<class Tq, class Tqp, class T>
  void conservativePerturbation(const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<T>& dU) const
  {
    Tq rho = q(ir);
    Tq u   = q(iu);
    Tq v   = q(iv);
//    Tq p   = q(ip);

    Tqp drho = dq(ir);
    Tqp du   = dq(iu);
    Tqp dv   = dq(iv);
    Tqp dp   = dq(ip);

    dU[0] = drho;
    dU[1] = u*drho + rho*du;
    dU[2] = v*drho + rho*dv;
    dU[3] = 0.5*(u*u+v*v)*drho + rho*u*du + rho*v*dv + dp/(gas_.gamma()-1.);
  }

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure2D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative2D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& v, T& t ) const
{
  rho = q(ir);
  u   = q(iu);
  v   = q(iv);
  t   = q(ip)/(rho * gas_.R());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  rho = q(ir);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u, T& v ) const
{
  u = q(iu);
  v = q(iv);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  t = q(ip)/(q(ir) * gas_.R());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalDensityJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q ) const
{
  rho_q = 0;
  rho_q[0] = 1;  // drho/drho
}

template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q,
                        ArrayQ<T>& u_q, ArrayQ<T>& v_q,
                        ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; v_q = 0; t_q = 0;

  const T& rho = q(0);
  const T& p   = q(3);

  rho_q[0] = 1;  // drho/drho

  u_q[1]   = 1;  // du/du

  v_q[2]   = 1;  // dv/dv

  t_q[0]   = -p/(rho*rho * gas_.R()); // dt/drho
  t_q[3]   =  1/(rho * gas_.R());     // dt/dp
}

template <template <class> class PDETraitsSize>
template<class Tq, class Tg>
void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalDensityGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rho_x ) const
{
  rho_x = qx(ir);
}

template <template <class> class PDETraitsSize>
template<class Tq, class Tg>
void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalPressureGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& p_x ) const
{
  p_x = qx(ip);
}

template <template <class> class PDETraitsSize>
template <class Tq, class Tg>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    typename promote_Surreal<Tq,Tg>::type& rhox,
    typename promote_Surreal<Tq,Tg>::type&   ux,
    typename promote_Surreal<Tq,Tg>::type&   vx,
    typename promote_Surreal<Tq,Tg>::type&   tx ) const
{
  const Tq& rho = q(ir);
  const Tq& p   = q(ip);
        Tq  t   = p/(rho * gas_.R());

            rhox = qx(ir);
            ux   = qx(iu);
            vx   = qx(iv);
  const Tg& px   = qx(ip);
            tx   = (t/p)*px - (t/rho)*(rhox);
}


template <template <class> class PDETraitsSize>
template<class Tq, class Tg, class Th>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    typename promote_Surreal<Tq, Tg, Th>::type& rhoxx,
    typename promote_Surreal<Tq, Tg, Th>::type& rhoxy,
    typename promote_Surreal<Tq, Tg, Th>::type& rhoyy,
    typename promote_Surreal<Tq, Tg, Th>::type& uxx,
    typename promote_Surreal<Tq, Tg, Th>::type& uxy,
    typename promote_Surreal<Tq, Tg, Th>::type& uyy,
    typename promote_Surreal<Tq, Tg, Th>::type& vxx,
    typename promote_Surreal<Tq, Tg, Th>::type& vxy,
    typename promote_Surreal<Tq, Tg, Th>::type& vyy,
    typename promote_Surreal<Tq, Tg, Th>::type& txx,
    typename promote_Surreal<Tq, Tg, Th>::type& txy,
    typename promote_Surreal<Tq, Tg, Th>::type& tyy ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;

  Tq rho, u, v, t;
  rho=0; u=0; v=0; t=0;
  Tqg rhox, ux, vx, tx = 0;
  Tqg rhoy, uy, vy, ty = 0;
  Th pxx, pxy, pyy;

//  const Tq& p = q(ip);
//  const Tg &px = qx(ip), &py = qy(ip);

  eval(q, rho, u, v, t);

  evalGradient(q, qx, rhox, ux, vx, tx);
  evalGradient(q, qy, rhoy, uy, vy, ty);

  rhoxx = qxx(ir); uxx = qxx(iu); vxx = qxx(iv); pxx = qxx(ip);
  rhoxy = qxy(ir); uxy = qxy(iu); vxy = qxy(iv); pxy = qxy(ip);
  rhoyy = qyy(ir); uyy = qyy(iu); vyy = qyy(iv); pyy = qyy(ip);

  Real Ri = 1./(gas_.R());

  txx = (Ri*pxx - 2*rhox*tx - rhoxx*t )/rho;
  txy = (Ri*pxy - rhox*ty - rhoy*tx - rhoxy*t)/rho;
  tyy = ( Ri*pyy - 2*rhoy*ty - rhoyy*t )/rho;

}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 4);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "VelocityY") &&
              (name[3] == "Temperature"));

  T rho, u, v, t;

  rho = data[0];
  u   = data[1];
  v   = data[2];
  t   = data[3];
  q(ir) = rho;
  q(iu) = u;
  q(iv) = v;
  q(ip) = gas_.pressure(rho, t);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure2D<T>& data ) const
{
  q(ir) = data.Density;
  q(iu) = data.VelocityX;
  q(iv) = data.VelocityY;
  q(ip) = data.Pressure;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature2D<T>& data ) const
{
  q(ir) = data.Density;
  q(iu) = data.VelocityX;
  q(iv) = data.VelocityY;
  q(ip) = gas_.pressure(data.Density, data.Temperature);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative2D<T>& data ) const
{
  q(ir) = data.Density;
  q(iu) = data.MomentumX/data.Density;
  q(iv) = data.MomentumY/data.Density;

  T e = data.TotalEnergy/data.Density - 0.5*(q(iu)*q(iu) + q(iv)*q(iv));
  T t = gas_.temperature(data.Density, e);

  q(ip) = gas_.pressure(data.Density, t);
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  const Real rho = q(ir);
  const Real p   = q(ip);

  const Real drho = dq(ir);
  const Real dp   = dq(ip);

  Real wr = 1, wp = 1;

  // density check
  Real nrho = rho - drho;

  // Compute wr such that:

  //rho - wr*drho >= (1-maxChangeFraction)*rho
  if (nrho < (1-maxChangeFraction)*rho)
    wr =  maxChangeFraction*rho/drho;

  //rho - wr*drho <= (1+maxChangeFraction)*rho
  if (nrho > (1+maxChangeFraction)*rho)
    wr = -maxChangeFraction*rho/drho;

  // pressure check
  Real np = p - dp;

  // Compute wp such that:

  //p - wp*dp >= (1-maxChangeFraction)*p
  if (np < (1-maxChangeFraction)*p)
    wp =  maxChangeFraction*p/dp;

  //p - wp*dp <= (1+maxChangeFraction)*p
  if (np > (1+maxChangeFraction)*p)
    wp = -maxChangeFraction*p/dp;

  updateFraction = MIN(wr, wp);
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  bool isPositive;
  Real rho, p;

  rho = q(ir);
  p   = q(ip);

#if 0
  printf( "%s: rho = %e  p = %e\n", __func__, rho, p );
#endif

  if ((rho > 0) && (p > 0))
    isPositive = true;
  else
    isPositive = false;

  return isPositive;
}


template <template <class> class PDETraitsSize>
void
Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q2D<QTypePrimitiveRhoPressure, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

} // namespace SANS

#endif  // Q2DPRIMITIVERHOPRESSURE_H
