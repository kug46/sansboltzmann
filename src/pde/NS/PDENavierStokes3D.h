// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDENAVIERSTOKES3D_H
#define PDENAVIERSTOKES3D_H

// 3-D compressible Navier-Stokes PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "tools/smoothmath.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include "pde/ForcingFunctionBase.h"
#include "Topology/Dimension.h"

#include "GasModel.h"
#include "NSVariable3D.h"
#include "PDEEuler3D.h"
//#include "Q3DPrimitiveSurrogate.h"

#include <iostream>
#include <string>
#include <memory> // shared_ptr

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class Q3D;


//----------------------------------------------------------------------------//
// PDE class: 3-D compressible Navier-Stokes
//
// template parameters:
//   T                        solution DOF data type (e.g. Real)
//   PDETraitsSize            define PDE size-related features
//     N, D                   PDE size, physical dimensions
//     ArrayQ                 solution/residual arrays
//     MatrixQ                matrices (e.g. flux jacobian)
//   PDETraitsModel           define PDE model-related features
//     QType                  solution variable set (e.g. primitive)
//     QInterpret             solution variable interpreter
//     GasModel               gas model (e.g. ideal gas law)
//     ViscosityModel         molecular viscosity model (e.g. Sutherland)
//     ThermalConductivityModel   thermal conductivity model (e.g. const Prandtl number)
//
// member functions:
//   .hasFluxAdvectiveTime     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//INHERITS CONSERVATIVE, ADVECTIVE FLUXES FROM EULER3D
//   .masterState        unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDENavierStokes3D : public PDEEuler3D<PDETraitsSize, PDETraitsModel>
{
public:
  typedef PhysD3 PhysDim;

  typedef PDEEuler3D<PDETraitsSize, PDETraitsModel> BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef typename PDETraitsModel::QType QType;
  typedef Q3D<QType, PDETraitsSize> QInterpret;                                       // solution variable interpreter

  typedef typename PDETraitsModel::GasModel GasModel;                                 // gas model

  typedef typename PDETraitsModel::ViscosityModel ViscosityModel;                     // molecular viscosity model

  typedef typename PDETraitsModel::ThermalConductivityModel ThermalConductivityModel; // thermal conductivity model

  typedef ForcingFunctionBase3D< PDENavierStokes3D<PDETraitsSize, PDETraitsModel> > ForcingFunctionType;

  explicit PDENavierStokes3D( const GasModel& gas, const ViscosityModel& visc,
         const ThermalConductivityModel& tcond,
         const EulerResidualInterpCategory cat = Euler_ResidInterp_Raw,
         const RoeEntropyFix entropyFix = eVanLeer,
         const std::shared_ptr<ForcingFunctionType>& forcing = NULL) :
      PDEEuler3D<PDETraitsSize, PDETraitsModel>( gas, cat, entropyFix ),
      visc_(visc),
      tcond_(tcond),
      forcing_(forcing) {}

  using BaseType::iCont;
  using BaseType::ixMom;
  using BaseType::iyMom;
  using BaseType::izMom;
  using BaseType::iEngy;

  PDENavierStokes3D& operator=( const PDENavierStokes3D& ) = delete;

  // flux components
  bool hasFluxViscous() const { return true; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool hasForcingFunction() const { return forcing_ != NULL && (*forcing_).hasForcingFunction(); }

  bool needsSolutionGradientforSource() const { return false; }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const;

protected:
  // internal function for computing the actual viscous flux
  template <class Tq, class Tg, class Tm, class Tf>
  void fluxViscous(
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const Tq& u , const Tq& v , const Tq& w , const Tq& mu, const Tq& k,
      const Tm& ux, const Tm& vx, const Tm& wx, const Tm& tx,
      const Tm& uy, const Tm& vy, const Tm& wy, const Tm& ty,
      const Tm& uz, const Tm& vz, const Tm& wz, const Tm& tz,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const;

public:
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy, const ArrayQ<Tgu>& dqz,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg, ArrayQ<Tf>& dh ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const MatrixQ<Tk>& kxx, const MatrixQ<Tk>& kxy, const MatrixQ<Tk>& kxz,
      const MatrixQ<Tk>& kyx, const MatrixQ<Tk>& kyy, const MatrixQ<Tk>& kyz,
      const MatrixQ<Tk>& kzx, const MatrixQ<Tk>& kzy, const MatrixQ<Tk>& kzz,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, const ArrayQ<Tgu>& duz,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg, ArrayQ<Tf>& dh ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const;

protected:
  template <class Tq, class Tk>
  void diffusionViscous(
      const ArrayQ<Tq>& q,
      const Tq& rho, const Tq& u, const Tq& v, const Tq& w, const Tq& t, const Tq& mu, const Tq& kn,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const;

public:
  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const;

protected:
  template <class Tq,  class Tg, class Tk, class Tqg>
  void diffusionViscousGradient(
      const Tq& rho, const Tq& u, const Tq& v, const Tq& w, const Tq& t, const Tq& mu, const Tq& k,
      const Tg& rhox, const Tg& ux, const Tg& vx, const Tg& wx, const Tg& tx, const Tqg& mux, const Tqg& kx,
      const Tg& rhoy, const Tg& uy, const Tg& vy, const Tg& wy, const Tg& ty, const Tqg& muy, const Tqg& ky,
      const Tg& rhoz, const Tg& uz, const Tg& vz, const Tg& wz, const Tg& tz, const Tqg& muz, const Tqg& kz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const;

public:

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu, MatrixQ<Tf>& dhdu ) const;

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& strongPDE ) const;

protected:
  template <class Tm, class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Tq& mu, const Tq& k,
      const ArrayQ<Tm>& dmu, const ArrayQ<Tm> dk,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu, MatrixQ<Tf>& dhdu ) const;

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Tq& mu,
      const typename promote_Surreal<Tq, Tg>::type& mux,
      const typename promote_Surreal<Tq, Tg>::type& muy,
      const typename promote_Surreal<Tq, Tg>::type& muz,
      const Tq& k,
      const typename promote_Surreal<Tq, Tg>::type& kx,
      const typename promote_Surreal<Tq, Tg>::type& ky,
      const typename promote_Surreal<Tq, Tg>::type& kz,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& strongPDE ) const;

public:

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<T>& forcing ) const;

  const ViscosityModel& viscosityModel() const {return visc_;}
  const ThermalConductivityModel& thermalConductivityModel() const {return tcond_;}

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BaseType::qInterpret_;
  using BaseType::gas_;
  const ViscosityModel& visc_;
  const ThermalConductivityModel& tcond_;
  const std::shared_ptr<ForcingFunctionType> forcing_;
};


// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
{
  typedef typename promote_Surreal<Tq,Tg>::type T;

  Tq rho=0, u=0, v=0, w=0, t=0;
  T rhox=0, ux=0, vx=0, wx=0, tx=0;
  T rhoy=0, uy=0, vy=0, wy=0, ty=0;
  T rhoz=0, uz=0, vz=0, wz=0, tz=0;
  Tq mu, k;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

#if 0
  if (std::is_same<QType, QTypePrimitiveSurrogate>::value)
  {
    const int it = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::it;
    t = smoothabsP(q[it], 0.001);
  }
#endif

  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  // Compute the viscous flux
  fluxViscous(q, qx, qy, qz,
              u, v, w, mu, k,
              ux, vx, wx, tx,
              uy, vy, wy, ty,
              uz, vz, wz, tz,
              f, g, h );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tm, class Tf>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const Tq& u , const Tq& v , const Tq& w , const Tq& mu, const Tq& k,
    const Tm& ux, const Tm& vx, const Tm& wx, const Tm& tx,
    const Tm& uy, const Tm& vy, const Tm& wy, const Tm& ty,
    const Tm& uz, const Tm& vz, const Tm& wz, const Tm& tz,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
{

  Tq lambda = -2./3. * mu;

  Tm tauxx = mu*(2*ux   ) + lambda*(ux + vy + wz);
  Tm tauyy = mu*(2*vy   ) + lambda*(ux + vy + wz);
  Tm tauzz = mu*(2*wz   ) + lambda*(ux + vy + wz);
  Tm tauxy = mu*(uy + vx);
  Tm tauxz = mu*(uz + wx);
  Tm tauyz = mu*(vz + wy);

  f(ixMom) -= tauxx;
  f(iyMom) -= tauxy;
  f(izMom) -= tauxz;
  f(iEngy) -= k*tx + u*tauxx + v*tauxy + w*tauxz;

  g(ixMom) -= tauxy;
  g(iyMom) -= tauyy;
  g(izMom) -= tauyz;
  g(iEngy) -= k*ty + u*tauxy + v*tauyy + w*tauyz;

  h(ixMom) -= tauxz;
  h(iyMom) -= tauyz;
  h(izMom) -= tauzz;
  h(iEngy) -= k*tz + u*tauxz + v*tauyz + w*tauzz;

#if 0
  if (std::is_same<QType, QTypePrimitiveSurrogate>::value)
  {
    const int ir = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::ir;
    //const int iu = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::iu;
    //const int iv = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::iv;
    //const int iw = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::iw;
    const int it = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::it;

    if (q[ir] < 0 || q[it] < 0)
    {
      //f(iCont) -= mu*qx[ir];
      //f(ixMom) -= mu*qx[iu];
      //f(iyMom) -= mu*qx[iv];
      //f(izMom) -= mu*qx[iw];
      //f(iEngy) -=  k*qx[it] + u*mu*qx[iu];

      //g(iCont) -= mu*qy[ir];
      //g(ixMom) -= mu*qy[iu];
      //g(iyMom) -= mu*qy[iv];
      //g(izMom) -= mu*qy[iw];
      //g(iEngy) -=  k*qy[it] + v*mu*qy[iv];

      //h(iCont) -= mu*qz[ir];
      //h(ixMom) -= mu*qz[iu];
      //h(iyMom) -= mu*qz[iv];
      //h(izMom) -= mu*qz[iw];
      //h(iEngy) -=  k*qz[it] + w*mu*qz[iw];
    }
  }
#endif
}


// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
    const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;
  ArrayQ<Tf> hL = 0, hR = 0;

  fluxViscous(x, y, z, time, qL, qxL, qyL, qzL, fL, gL, hL);
  fluxViscous(x, y, z, time, qR, qxR, qyR, qzR, fR, gR, hR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny + 0.5*(hL + hR)*nz;
}


// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tgu, class Tf>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::perturbedGradFluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy, const ArrayQ<Tgu>& dqz,
    ArrayQ<Tf>& dF, ArrayQ<Tf>& dG, ArrayQ<Tf>& dH ) const
{
  // this thing is linear in the gradient so the following works:
  fluxViscous(x, y, z, time, q, dqx, dqy, dqz, dF, dG, dH);
}


// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tk, class Tgu, class Tf>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::perturbedGradFluxViscous(
    const Real& x, const Real& y,  const Real& z, const Real& time,
    const MatrixQ<Tk>& kxx,  const MatrixQ<Tk>& kxy,  const MatrixQ<Tk>& kxz,
    const MatrixQ<Tk>& kyx,  const MatrixQ<Tk>& kyy, const MatrixQ<Tk>& kyz,
    const MatrixQ<Tk>& kzx,  const MatrixQ<Tk>& kzy, const MatrixQ<Tk>& kzz,
    const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy, const ArrayQ<Tgu>& duz,
    ArrayQ<Tf>& dF, ArrayQ<Tf>& dG, ArrayQ<Tf>& dH ) const
{
  //using what we know about the sparsity of K

  // d(Fv)/d(Ux)
  dF[ixMom] -= kxx(ixMom,0)*dux[0];
  dF[ixMom] -= kxx(ixMom,1)*dux[1];

  dF[iyMom] -= kxx(iyMom,0)*dux[0];
  dF[iyMom] -= kxx(iyMom,2)*dux[2];

  dF[izMom] -= kxx(izMom,0)*dux[0];
  dF[izMom] -= kxx(izMom,3)*dux[3];

  dF[iEngy] -= kxx(iEngy,0)*dux[0];
  dF[iEngy] -= kxx(iEngy,1)*dux[1];
  dF[iEngy] -= kxx(iEngy,2)*dux[2];
  dF[iEngy] -= kxx(iEngy,3)*dux[3];
  dF[iEngy] -= kxx(iEngy,4)*dux[4];

  // d(Fv)/d(Uy)
  dF[ixMom] -= kxy(ixMom,0)*duy[0];
  dF[ixMom] -= kxy(ixMom,2)*duy[2];

  dF[iyMom] -= kxy(iyMom,0)*duy[0];
  dF[iyMom] -= kxy(iyMom,1)*duy[1];

  dF[iEngy] -= kxy(iEngy,0)*duy[0];
  dF[iEngy] -= kxy(iEngy,1)*duy[1];
  dF[iEngy] -= kxy(iEngy,2)*duy[2];

  // d(Fv)/d(Uz)
  dF[ixMom] -= kxz(ixMom,0)*duz[0];
  dF[ixMom] -= kxz(ixMom,3)*duz[3];

  dF[izMom] -= kxz(izMom,0)*duz[0];
  dF[izMom] -= kxz(izMom,1)*duz[1];

  dF[iEngy] -= kxz(iEngy,0)*duz[0];
  dF[iEngy] -= kxz(iEngy,1)*duz[1];
  dF[iEngy] -= kxz(iEngy,3)*duz[3];

  // d(Gv)/d(Ux)
  dG[ixMom] -= kyx(ixMom,0)*dux[0];
  dG[ixMom] -= kyx(ixMom,2)*dux[2];

  dG[iyMom] -= kyx(iyMom,0)*dux[0];
  dG[iyMom] -= kyx(iyMom,1)*dux[1];

  dG[iEngy] -= kyx(iEngy,0)*dux[0];
  dG[iEngy] -= kyx(iEngy,1)*dux[1];
  dG[iEngy] -= kyx(iEngy,2)*dux[2];

  // d(Gv)/d(Uy)
  dG[ixMom] -= kyy(ixMom,0)*duy[0];
  dG[ixMom] -= kyy(ixMom,1)*duy[1];

  dG[iyMom] -= kyy(iyMom,0)*duy[0];
  dG[iyMom] -= kyy(iyMom,2)*duy[2];

  dG[izMom] -= kyy(izMom,0)*duy[0];
  dG[izMom] -= kyy(izMom,3)*duy[3];

  dG[iEngy] -= kyy(iEngy,0)*duy[0];
  dG[iEngy] -= kyy(iEngy,1)*duy[1];
  dG[iEngy] -= kyy(iEngy,2)*duy[2];
  dG[iEngy] -= kyy(iEngy,3)*duy[3];
  dG[iEngy] -= kyy(iEngy,4)*duy[4];

  // d(Gv)/d(Uz)
  dG[iyMom] -= kyz(iyMom,0)*duz[0];
  dG[iyMom] -= kyz(iyMom,3)*duz[3];

  dG[izMom] -= kyz(izMom,0)*duz[0];
  dG[izMom] -= kyz(izMom,2)*duz[2];

  dG[iEngy] -= kyz(iEngy,0)*duz[0];
  dG[iEngy] -= kyz(iEngy,2)*duz[2];
  dG[iEngy] -= kyz(iEngy,3)*duz[3];


  // d(Hv)/d(Ux)
  dH[ixMom] -= kzx(ixMom,0)*dux[0];
  dH[ixMom] -= kzx(ixMom,3)*dux[3];

  dH[izMom] -= kzx(izMom,0)*dux[0];
  dH[izMom] -= kzx(izMom,1)*dux[1];

  dH[iEngy] -= kzx(iEngy,0)*dux[0];
  dH[iEngy] -= kzx(iEngy,1)*dux[1];
  dH[iEngy] -= kzx(iEngy,3)*dux[3];

  // d(Hv)/d(Uy)
  dH[iyMom] -= kzy(iyMom,0)*duy[0];
  dH[iyMom] -= kzy(iyMom,3)*duy[3];

  dH[izMom] -= kzy(izMom,0)*duy[0];
  dH[izMom] -= kzy(izMom,2)*duy[2];

  dH[iEngy] -= kzy(iEngy,0)*duy[0];
  dH[iEngy] -= kzy(iEngy,2)*duy[2];
  dH[iEngy] -= kzy(iEngy,3)*duy[3];

  // d(Hv)/d(Uz)
  dH[ixMom] -= kzz(ixMom,0)*duz[0];
  dH[ixMom] -= kzz(ixMom,1)*duz[1];

  dH[iyMom] -= kzz(iyMom,0)*duz[0];
  dH[iyMom] -= kzz(iyMom,2)*duz[2];

  dH[izMom] -= kzz(izMom,0)*duz[0];
  dH[izMom] -= kzz(izMom,3)*duz[3];

  dH[iEngy] -= kzz(iEngy,0)*duz[0];
  dH[iEngy] -= kzz(iEngy,1)*duz[1];
  dH[iEngy] -= kzz(iEngy,2)*duz[2];
  dH[iEngy] -= kzz(iEngy,3)*duz[3];
  dH[iEngy] -= kzz(iEngy,4)*duz[4];
}


// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
    MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
{
  Tq rho=0, u=0, v=0, w=0, t=0;
  Tq mu, k;

  qInterpret_.eval( q, rho, u, v, w, t );
  mu = visc_.viscosity( t );
  k  = tcond_.conductivity( mu );

  diffusionViscous( q, rho, u, v, w, t, mu, k,
                    kxx, kxy, kxz,
                    kyx, kyy, kyz,
                    kzx, kzy, kzz );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tk>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const ArrayQ<Tq>& q,
    const Tq& rho, const Tq& u, const Tq& v, const Tq& w, const Tq& t, const Tq& mu, const Tq& k,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
    MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
{
  Real Cv = gas_.Cv();

  Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v + w*w);

  // defining nu here as mu/rho (and hence lambda/rho as well as k/(Cv*rho)) to remove rho division throughout the K matrix calculation
  Tq nu = mu/rho;
  Tq lambda = -2./3. * nu;
  Tq kn = k/(Cv*rho);

  // d(Fv)/d(Ux)
  kxx(ixMom,0) += -u*(2*nu + lambda);
  kxx(ixMom,1) +=    (2*nu + lambda);

  kxx(iyMom,0) += -v*nu;
  kxx(iyMom,2) +=    nu;

  kxx(izMom,0) += -w*nu;
  kxx(izMom,3) +=    nu;

  kxx(iEngy,0) += -u*u*((2*nu + lambda) - kn) - v*v*(nu - kn) - w*w*(nu - kn) - e0*kn;
  kxx(iEngy,1) +=    u*((2*nu + lambda) - kn);
  kxx(iEngy,2) +=                                 v*(nu - kn);
  kxx(iEngy,3) +=                                                 w*(nu - kn);
  kxx(iEngy,4) +=                                                                  kn;

  // d(Fv)/d(Uy)
  kxy(ixMom,0) += -v*lambda;
  kxy(ixMom,2) +=    lambda;

  kxy(iyMom,0) += -u*nu;
  kxy(iyMom,1) +=    nu;

  kxy(iEngy,0) += -u*v*(nu + lambda);
  kxy(iEngy,1) +=  v*nu;
  kxy(iEngy,2) +=  u*lambda;

  // d(Fv)/d(Uz)
  kxz(ixMom,0) += -w*lambda;
  kxz(ixMom,3) +=    lambda;

  kxz(izMom,0) += -u*nu;
  kxz(izMom,1) +=    nu;

  kxz(iEngy,0) += -u*w*(nu + lambda);
  kxz(iEngy,1) +=  w*nu;
  kxz(iEngy,3) +=  u*lambda;

  // d(Gv)/d(Ux)
  kyx(ixMom,0) += -v*nu;
  kyx(ixMom,2) +=    nu;

  kyx(iyMom,0) += -u*lambda;
  kyx(iyMom,1) +=    lambda;

  kyx(iEngy,0) += -u*v*(nu + lambda);
  kyx(iEngy,1) +=  v*lambda;
  kyx(iEngy,2) +=  u*nu;

  // d(Gv)/d(Uy)
  kyy(ixMom,0) += -u*nu;
  kyy(ixMom,1) +=    nu;

  kyy(iyMom,0) += -v*(2*nu + lambda);
  kyy(iyMom,2) +=    (2*nu + lambda);

  kyy(izMom,0) += -w*nu;
  kyy(izMom,3) +=    nu;

  kyy(iEngy,0) += -u*u*(nu - kn) - v*v*((2*nu + lambda) - kn) - w*w*(nu - kn) - e0*kn;
  kyy(iEngy,1) +=    u*(nu - kn);
  kyy(iEngy,2) +=                    v*((2*nu + lambda) - kn);
  kyy(iEngy,3) +=                                                 w*(nu - kn);
  kyy(iEngy,4) +=                                                                  kn;

  // d(Gv)/d(Uz)
  kyz(iyMom,0) += -w*lambda;
  kyz(iyMom,3) +=    lambda;

  kyz(izMom,0) += -v*nu;
  kyz(izMom,2) +=    nu;

  kyz(iEngy,0) += -v*w*(nu + lambda);
  kyz(iEngy,2) +=  w*nu;
  kyz(iEngy,3) +=  v*lambda;

  // d(Hv)/d(Ux)
  kzx(ixMom,0) += -w*nu;
  kzx(ixMom,3) +=    nu;

  kzx(izMom,0) += -u*lambda;
  kzx(izMom,1) +=    lambda;

  kzx(iEngy,0) += -u*w*(nu + lambda);
  kzx(iEngy,1) +=  w*lambda;
  kzx(iEngy,3) +=  u*nu;

  // d(Hv)/d(Uy)
  kzy(iyMom,0) += -w*nu;
  kzy(iyMom,3) +=    nu;

  kzy(izMom,0) += -v*lambda;
  kzy(izMom,2) +=    lambda;

  kzy(iEngy,0) += -v*w*(nu + lambda);
  kzy(iEngy,2) +=  w*lambda;
  kzy(iEngy,3) +=  v*nu;

  // d(Hv)/d(Uz)
  kzz(ixMom,0) += -u*nu;
  kzz(ixMom,1) +=    nu;

  kzz(iyMom,0) += -v*nu;
  kzz(iyMom,2) +=    nu;

  kzz(izMom,0) += -w*(2*nu + lambda);
  kzz(izMom,3) +=    (2*nu + lambda);

  kzz(iEngy,0) += -u*u*(nu - kn) - v*v*(nu - kn) - w*w*((2*nu + lambda) - kn) - e0*kn;
  kzz(iEngy,1) +=    u*(nu - kn);
  kzz(iEngy,2) +=                    v*(nu - kn);
  kzz(iEngy,3) +=                                    w*((2*nu + lambda) - kn);
  kzz(iEngy,4) +=                                                                  kn;

#if 0
  if (std::is_same<QType, QTypePrimitiveSurrogate>::value)
  {
    const int ir = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::ir;
    //const int iu = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::iu;
    //const int iv = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::iv;
    //const int iw = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::iw;
    const int it = Q3D<QTypePrimitiveSurrogate, PDETraitsSize>::it;

    if (q[ir] < 0 || q[it] < 0)
    {
      //kxx(iCont,ir) += nu;
      //kxx(ixMom,iu) += nu;
      //kxx(iyMom,iv) += nu;
      //kxx(izMom,iw) += nu;
      //kxx(iEngy,it) += kn + u*nu;

      //kyy(iCont,ir) += nu;
      //kyy(ixMom,iu) += nu;
      //kyy(iyMom,iv) += nu;
      //kyy(izMom,iw) += nu;
      //kyy(iEngy,it) += kn + v*nu;

      //kzz(iCont,ir) += nu;
      //kzz(ixMom,iu) += nu;
      //kzz(iyMom,iv) += nu;
      //kzz(izMom,iw) += nu;
      //kzz(iEngy,it) += kn + w*nu;
    }
  }
#endif
}


// gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tk>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
    MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z) const
{
  //SANS_DEVELOPER_EXCEPTION( "Error in PDENavierStokes3D<>::diffusionViscousGradient: not unit tested" );

  typedef typename promote_Surreal<Tq, Tg>::type Tqg;

  Tq rho=0, u=0, v=0, w=0, t=0;
  Tqg rhox, ux, vx, wx, tx;
  Tqg rhoy, uy, vy, wy, ty;
  Tqg rhoz, uz, vz, wz, tz;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  Tq mu_t = 0;
  visc_.viscosityJacobian(t, mu_t);

  Tq  mu = visc_.viscosity( t );
  Tqg mux = mu_t*tx;
  Tqg muy = mu_t*ty;
  Tqg muz = mu_t*tz;

  Tq k_mu = 0;
  tcond_.conductivityJacobian(mu, k_mu);

  Tq  k  = tcond_.conductivity( mu );
  Tqg kx = k_mu*mux;
  Tqg ky = k_mu*muy;
  Tqg kz = k_mu*muz;

  diffusionViscousGradient( rho, u, v, w, t, mu, k,
                            rhox, ux, vx, wx, tx, mux, kx,
                            rhoy, uy, vy, wy, ty, muy, ky,
                            rhoz, uz, vz, wz, tz, muz, kz,
                            kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                            kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                            kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tk, class Tqg>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Tq& rho, const Tq& u, const Tq& v, const Tq& w, const Tq& t, const Tq& mu, const Tq& k,
    const Tg& rhox, const Tg& ux, const Tg& vx, const Tg& wx, const Tg& tx, const Tqg& mux, const Tqg& kx,
    const Tg& rhoy, const Tg& uy, const Tg& vy, const Tg& wy, const Tg& ty, const Tqg& muy, const Tqg& ky,
    const Tg& rhoz, const Tg& uz, const Tg& vz, const Tg& wz, const Tg& tz, const Tqg& muz, const Tqg& kz,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
    MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const
{
  Real Cv = gas_.Cv();

  Tq e0 = gas_.energy(rho, t) + 0.5*(u*u + v*v + w*w);

  Tq e_rho = 0, e_t = 0;
  gas_.energyJacobian(rho, t, e_rho, e_t);
  Tqg e0x = e_rho*rhox + e_t*tx + (u*ux + v*vx + w*wx);
  Tqg e0y = e_rho*rhoy + e_t*ty + (u*uy + v*vy + w*wy);
  Tqg e0z = e_rho*rhoz + e_t*tz + (u*uz + v*vz + w*wz);

  // defining nu here as mu/rho (and hence lambda/rho as well as k/(Cv*rho)) to remove rho division throughout the K matrix calculation
  Tq  nu  = mu/rho;
  Tqg nux = mux/rho - mu*rhox/(rho*rho);
  Tqg nuy = muy/rho - mu*rhoy/(rho*rho);
  Tqg nuz = muz/rho - mu*rhoz/(rho*rho);

  Tq  lambda  = -2./3. * nu;
  Tqg lambdax = -2./3. * nux;
  Tqg lambday = -2./3. * nuy;
  Tqg lambdaz = -2./3. * nuz;

  Tq  kn  = k/(Cv*rho);
  Tqg knx = kx/(Cv*rho) - k*rhox/(Cv*(rho*rho));
  Tqg kny = ky/(Cv*rho) - k*rhoy/(Cv*(rho*rho));
  Tqg knz = kz/(Cv*rho) - k*rhoz/(Cv*(rho*rho));

  // d/dx (d(Fv)/d(Ux))
  kxx_x(ixMom,0) += -ux*(2*nu + lambda) - u*(2*nux + lambdax);
  kxx_x(ixMom,1) +=                         (2*nux + lambdax);

  kxx_x(iyMom,0) += -vx*nu - v*nux;
  kxx_x(iyMom,2) +=            nux;

  kxx_x(izMom,0) += -wx*nu - w*nux;
  kxx_x(izMom,3) +=            nux;

  kxx_x(iEngy,0) += -2*u*ux*((2*nu + lambda) - kn) - u*u*(2*nux + lambdax - knx)
                   - 2*v*vx*(nu - kn)              - v*v*(nux - knx)
                   - 2*w*wx*(nu - kn)              - w*w*(nux - knx)
                   - e0x*kn - e0*knx;
  kxx_x(iEngy,1) +=      ux*((2*nu + lambda) - kn) + u*((2*nux + lambdax) - knx);
  kxx_x(iEngy,2) +=        vx*(nu - kn) +  v*(nux - knx);
  kxx_x(iEngy,3) +=        wx*(nu - kn) +  w*(nux - knx);
  kxx_x(iEngy,4) +=                                                     knx;

  // d/dx (d(Fv)/d(Uy))
  kxy_x(ixMom,0) += -vx*lambda - v*lambdax;
  kxy_x(ixMom,2) +=                lambdax;

  kxy_x(iyMom,0) += -ux*nu - u*nux;
  kxy_x(iyMom,1) +=            nux;

  kxy_x(iEngy,0) += -ux*v*(nu + lambda) - u*vx*(nu + lambda) - u*v*(nux + lambdax);
  kxy_x(iEngy,1) +=  vx*nu + v*nux;
  kxy_x(iEngy,2) +=  ux*lambda + u*lambdax;

  // d/dx (d(Fv)/d(Uz))
  kxz_x(ixMom,0) += -wx*lambda - w*lambdax;
  kxz_x(ixMom,3) +=                lambdax;

  kxz_x(izMom,0) += -ux*nu - u*nux;
  kxz_x(izMom,1) +=            nux;

  kxz_x(iEngy,0) += -ux*w*(nu + lambda) - u*wx*(nu + lambda) - u*w*(nux + lambdax);
  kxz_x(iEngy,1) +=  wx*nu + w*nux;
  kxz_x(iEngy,3) +=  ux*lambda + u*lambdax;

  // d/dx (d(Gv)/d(Ux))
  kyx_x(ixMom,0) += -vx*nu - v*nux;
  kyx_x(ixMom,2) +=            nux;

  kyx_x(iyMom,0) += -ux*lambda -u*lambdax;
  kyx_x(iyMom,1) +=               lambdax;

  kyx_x(iEngy,0) += -ux*v*(nu + lambda) - u*vx*(nu + lambda) - u*v*(nux + lambdax);
  kyx_x(iEngy,1) +=  vx*lambda + v*lambdax;
  kyx_x(iEngy,2) +=  ux*nu + u*nux;

  // d/dx (d(Hv)/d(Ux))
  kzx_x(ixMom,0) += -wx*nu - w*nux;
  kzx_x(ixMom,3) +=            nux;

  kzx_x(izMom,0) += -ux*lambda - u*lambdax;
  kzx_x(izMom,1) +=                lambdax;

  kzx_x(iEngy,0) += -ux*w*(nu + lambda) - u*wx*(nu + lambda) - u*w*(nux + lambdaz);
  kzx_x(iEngy,1) +=  wx*lambda + w*lambdax;
  kzx_x(iEngy,3) +=  ux*nu + u*nux;

  // d/dy (d(Fv)/d(Uy))
  kxy_y(ixMom,0) += -vy*lambda - v*lambday;
  kxy_y(ixMom,2) +=                lambday;

  kxy_y(iyMom,0) += -uy*nu - u*nuy;
  kxy_y(iyMom,1) +=            nuy;

  kxy_y(iEngy,0) += -uy*v*(nu + lambda) - u*vy*(nu + lambda) - u*v*(nuy + lambday);
  kxy_y(iEngy,1) +=  vy*nu + v*nuy;
  kxy_y(iEngy,2) +=  uy*lambda + u*lambday;

  // d/dy (d(Gv)/d(Ux))
  kyx_y(ixMom,0) += -vy*nu - v*nuy;
  kyx_y(ixMom,2) +=            nuy;

  kyx_y(iyMom,0) += -uy*lambda -u*lambday;
  kyx_y(iyMom,1) +=               lambday;

  kyx_y(iEngy,0) += -uy*v*(nu + lambda) - u*vy*(nu + lambda) - u*v*(nuy + lambday);
  kyx_y(iEngy,1) +=  vy*lambda + v*lambday;
  kyx_y(iEngy,2) +=  uy*nu + u*nuy;

  // d/dy (d(Gv)/d(Uy))
  kyy_y(ixMom,0) += -uy*nu - u*nuy;
  kyy_y(ixMom,1) +=            nuy;

  kyy_y(iyMom,0) += -vy*(2*nu + lambda) -v*(2*nuy + lambday);
  kyy_y(iyMom,2) +=                        (2*nuy + lambday);

  kyy_y(izMom,0) += -wy*nu - w*nuy;
  kyy_y(izMom,3) +=            nuy;

  kyy_y(iEngy,0) += -2*u*uy*(nu - kn)              - u*u*(nuy - kny)
                   - 2*v*vy*((2*nu + lambda) - kn) - v*v*(2*nuy + lambday - kny)
                   - 2*w*wy*(nu - kn)              - w*w*(nuy - kny)
                   - e0y*kn - e0*kny;
  kyy_y(iEngy,1) += uy*(nu - kn)              + u*(nuy - kny);
  kyy_y(iEngy,2) += vy*((2*nu + lambda) - kn) + v*((2*nuy + lambday) - kny);
  kyy_y(iEngy,3) += wy*(nu - kn)              +  w*(nuy - kny);
  kyy_y(iEngy,4) +=                                                     kny;

  // d/dy (d(Gv)/d(Uz))
  kyz_y(iyMom,0) += -wy*lambda - w*lambday;
  kyz_y(iyMom,3) +=                lambday;

  kyz_y(izMom,0) += -vy*nu - v*nuy;
  kyz_y(izMom,2) +=            nuy;

  kyz_y(iEngy,0) += -vy*w*(nu + lambda) - v*wy*(nu + lambda) - v*w*(nuy + lambday);
  kyz_y(iEngy,2) +=  wy*nu + w*nuy;
  kyz_y(iEngy,3) +=  vy*lambda + v*lambday;

  // d/dy (d(Hv)/d(Uy))
  kzy_y(iyMom,0) += -wy*nu - w*nuy;
  kzy_y(iyMom,3) +=            nuy;

  kzy_y(izMom,0) += -vy*lambda - v*lambday;
  kzy_y(izMom,2) +=                lambday;

  kzy_y(iEngy,0) += -vy*w*(nu + lambda) - v*wy*(nu + lambda) - v*w*(nuy + lambday);
  kzy_y(iEngy,2) +=  wy*lambda + w*lambday;
  kzy_y(iEngy,3) +=  vy*nu + v*nuy;

  // d/dz (d(Fv)/d(Uz))
  kxz_z(ixMom,0) += -wz*lambda - w*lambdaz;
  kxz_z(ixMom,3) +=                lambdaz;

  kxz_z(izMom,0) += -uz*nu - u*nuz;
  kxz_z(izMom,1) +=            nuz;

  kxz_z(iEngy,0) += -uz*w*(nu + lambda) - u*wz*(nu + lambda) - u*w*(nuz + lambdaz);
  kxz_z(iEngy,1) +=  wz*nu + w*nuz;
  kxz_z(iEngy,3) +=  uz*lambda + u*lambdaz;

  // d/dz (d(Gv)/d(Uz))
  kyz_z(iyMom,0) += -wz*lambda - w*lambdaz;
  kyz_z(iyMom,3) +=                lambdaz;

  kyz_z(izMom,0) += -vz*nu - v*nuz;
  kyz_z(izMom,2) +=            nuz;

  kyz_z(iEngy,0) += -vz*w*(nu + lambda) - v*wz*(nu + lambda) - v*w*(nuz + lambdaz);
  kyz_z(iEngy,2) +=  wz*nu + w*nuz;
  kyz_z(iEngy,3) +=  vz*lambda + v*lambdaz;

  // d/dz (d(Hv)/d(Ux))
  kzx_z(ixMom,0) += -wz*nu - w*nuz;
  kzx_z(ixMom,3) +=            nuz;

  kzx_z(izMom,0) += -uz*lambda - u*lambdaz;
  kzx_z(izMom,1) +=                lambdaz;

  kzx_z(iEngy,0) += -uz*w*(nu + lambda) - u*wz*(nu + lambda) - u*w*(nuz + lambdaz);
  kzx_z(iEngy,1) +=  wz*lambda + w*lambdaz;
  kzx_z(iEngy,3) +=  uz*nu + u*nuz;

  // d/dz (d(Hv)/d(Uy))
  kzy_z(iyMom,0) += -wz*nu - w*nuz;
  kzy_z(iyMom,3) +=            nuz;

  kzy_z(izMom,0) += -vz*lambda - v*lambdaz;
  kzy_z(izMom,2) +=                lambdaz;

  kzy_z(iEngy,0) += -vz*w*(nu + lambda) - v*wz*(nu + lambda) - v*w*(nuz + lambdaz);
  kzy_z(iEngy,2) +=  wz*lambda + w*lambdaz;
  kzy_z(iEngy,3) +=  vz*nu + v*nuz;

  // d/dz (d(Hv)/d(Uz))
  kzz_z(ixMom,0) += -uz*nu - u*nuz;
  kzz_z(ixMom,1) +=            nuz;

  kzz_z(iyMom,0) += -vz*nu - v*nuz;
  kzz_z(iyMom,2) +=            nuz;

  kzz_z(izMom,0) += -wz*(2*nu + lambda) - w*(2*nuz + lambdaz);
  kzz_z(izMom,3) +=                         (2*nuz + lambdaz);

  kzz_z(iEngy,0) += - 2*u*uz*(nu - kn)              - u*u*(nuz - knz)
                    - 2*v*vz*(nu - kn)              - v*v*(nuz - knz)
                    - 2*w*wz*((2*nu + lambda) - kn) - w*w*((2*nuz + lambdaz) - knz)
                    - e0z*kn - e0*knz;
  kzz_z(iEngy,1) +=  uz*(nu - kn) + u*(nuz - knz);
  kzz_z(iEngy,2) +=  vz*(nu - kn) + v*(nuz - knz);
  kzz_z(iEngy,3) +=  wz*((2*nu + lambda) - kn) + w*((2*nuz + lambdaz) - knz);
  kzz_z(iEngy,4) +=  knz;
}


// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tf>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::jacobianFluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu, MatrixQ<Tf>& dhdu ) const
{
  Tq rho=0, u=0, v=0, w=0, t=0;
  Tq mu, k;
  Tq mu_t = 0;
  Tq k_mu = 0;

  qInterpret_.eval( q, rho, u, v, w, t );

  mu = visc_.viscosity( t );
  visc_.viscosityJacobian(t, mu_t);
  k  = tcond_.conductivity( mu );
  tcond_.conductivityJacobian( mu, k_mu);
  Tq E = gas_.energy(rho, t) + 0.5*(u*u + v*v + w*w);
  Real Cv = gas_.Cv();

  Tq t_rho  = ( -E/rho + ( u*u + v*v + w*w )/rho ) / Cv;
  Tq t_rhou = (        - (   u             )/rho ) / Cv;
  Tq t_rhov = (        - (         v       )/rho ) / Cv;
  Tq t_rhow = (        - (               w )/rho ) / Cv;
  Tq t_rhoE = (  1/rho                           ) / Cv;

  ArrayQ<Tq> dmu = 0;
  dmu(0) = mu_t*t_rho;
  dmu(1) = mu_t*t_rhou;
  dmu(2) = mu_t*t_rhov;
  dmu(3) = mu_t*t_rhow;
  dmu(4) = mu_t*t_rhoE;

  ArrayQ<Tq> dk = 0;
  dk(0) = k_mu*dmu(0);
  dk(1) = k_mu*dmu(1);
  dk(2) = k_mu*dmu(2);
  dk(3) = k_mu*dmu(3);
  dk(4) = k_mu*dmu(4);

  jacobianFluxViscous(mu, k, dmu, dk, q, qx, qy, qz, dfdu, dgdu, dhdu);
}

// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tm, class Tq, class Tg, class Tf>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::jacobianFluxViscous(
    const Tq& mu, const Tq& k,
    const ArrayQ<Tm>& dmu, const ArrayQ<Tm> dk,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu, MatrixQ<Tf>& dhdu ) const
{
  Real R = gas_.R();
  Real g = gas_.gamma();
  Real Cv = gas_.Cv();
  Tq rho=0, u=0, v=0, w=0, t=0;

  typedef typename promote_Surreal<Tq,Tg>::type T;
  T  rhox=0, rhoy=0, rhoz=0;
  T    ux=0,   uy=0,   uz=0;
  T    vx=0,   vy=0,   vz=0;
  T    wx=0,   wy=0,   wz=0;
  T    tx=0,   ty=0,   tz=0;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  T divV = (ux + vy + wz);
  Tq V2 = (u*u + v*v + w*w);

  Tq E = gas_.energy(rho, t) + 0.5*V2;

  Real lambda_mu = -2./3.;

  Tq lambda = lambda_mu*mu;

  T tauxx = mu*(2*ux   ) + lambda*divV;
  T tauyy = mu*(2*vy   ) + lambda*divV;
  T tauzz = mu*(2*wz   ) + lambda*divV;
  T tauxy = mu*(uy + vx);
  T tauxz = mu*(uz + wx);
  T tauyz = mu*(vz + wy);

  T tauxx_mu = (2*ux   ) + lambda_mu*divV;
  T tauyy_mu = (2*vy   ) + lambda_mu*divV;
  T tauzz_mu = (2*wz   ) + lambda_mu*divV;
  T tauxy_mu = (uy + vx);
  T tauxz_mu = (uz + wx);
  T tauyz_mu = (vz + wy);

  Tq rho2 = rho*rho;
  Tq rho3 = rho2*rho;

  T ux_rho  = (rhox * u - rho*ux)/rho2;
  T ux_rhou = -rhox              /rho2;
  T uy_rho  = (rhoy * u - rho*uy)/rho2;
  T uy_rhou = -rhoy              /rho2;
  T uz_rho  = (rhoz * u - rho*uz)/rho2;
  T uz_rhou = -rhoz              /rho2;

  T vx_rho  = (rhox * v - rho*vx)/rho2;
  T vx_rhov = -rhox              /rho2;
  T vy_rho  = (rhoy * v - rho*vy)/rho2;
  T vy_rhov = -rhoy              /rho2;
  T vz_rho  = (rhoz * v - rho*vz)/rho2;
  T vz_rhov = -rhoz              /rho2;

  T wx_rho  = (rhox * w - rho*wx)/rho2;
  T wx_rhow = -rhox              /rho2;
  T wy_rho  = (rhoy * w - rho*wy)/rho2;
  T wy_rhow = -rhoy              /rho2;
  T wz_rho  = (rhoz * w - rho*wz)/rho2;
  T wz_rhow = -rhoz              /rho2;

  T rhoEx = (rhox*R*t + rho*R*tx)/(g-1) + 0.5*rhox*V2 + rho*(u*ux + v*vx + w*wx);
  T rhoEy = (rhoy*R*t + rho*R*ty)/(g-1) + 0.5*rhoy*V2 + rho*(u*uy + v*vy + w*wy);
  T rhoEz = (rhoz*R*t + rho*R*tz)/(g-1) + 0.5*rhoz*V2 + rho*(u*uz + v*vz + w*wz);

  T tx_rho  = (2.*u*ux*rho2 + 2.*v*vx*rho2 + 2.*w*wx*rho2 - rho*rhoEx - rho*V2*rhox + 2*rho*E*rhox)/(Cv*rho3);
  T tx_rhou = (u*rhox - rho*ux)/(Cv*rho2);
  T tx_rhov = (v*rhox - rho*vx)/(Cv*rho2);
  T tx_rhow = (w*rhox - rho*wx)/(Cv*rho2);
  T tx_rhoE = -rhox            /(Cv*rho2);

  T ty_rho  = (2.*u*uy*rho2 + 2.*v*vy*rho2 + 2.*w*wy*rho2 - rho*rhoEy - rho*V2*rhoy + 2*rho*E*rhoy)/(Cv*rho3);
  T ty_rhou = (u*rhoy - rho*uy)/(Cv*rho2);
  T ty_rhov = (v*rhoy - rho*vy)/(Cv*rho2);
  T ty_rhow = (w*rhoy - rho*wy)/(Cv*rho2);
  T ty_rhoE = -rhoy            /(Cv*rho2);

  T tz_rho  = (2.*u*uz*rho2 + 2.*v*vz*rho2 + 2.*w*wz*rho2 - rho*rhoEz - rho*V2*rhoz + 2*rho*E*rhoz)/(Cv*rho3);
  T tz_rhou = (u*rhoz - rho*uz)/(Cv*rho2);
  T tz_rhov = (v*rhoz - rho*vz)/(Cv*rho2);
  T tz_rhow = (w*rhoz - rho*wz)/(Cv*rho2);
  T tz_rhoE = -rhoz            /(Cv*rho2);

  Tq u_rho  = -u/rho;
  Tq u_rhou =  1/rho;
  Tq v_rho  = -v/rho;
  Tq v_rhov =  1/rho;
  Tq w_rho  = -w/rho;
  Tq w_rhow =  1/rho;

  T divV_rho = (ux_rho + vy_rho + wz_rho);

  Tf tauxx_rho  = tauxx_mu*dmu(0) + mu*(2*ux_rho  + lambda_mu*divV_rho );
  Tf tauxx_rhou = tauxx_mu*dmu(1) + mu*(2*ux_rhou + lambda_mu*ux_rhou  );
  Tf tauxx_rhov = tauxx_mu*dmu(2) + mu*(            lambda_mu*vy_rhov  );
  Tf tauxx_rhow = tauxx_mu*dmu(3) + mu*(            lambda_mu*wz_rhow  );
  Tf tauxx_rhoE = tauxx_mu*dmu(4);

  Tf tauyy_rho  = tauyy_mu*dmu(0) + mu*(2*vy_rho  + lambda_mu*divV_rho );
  Tf tauyy_rhou = tauyy_mu*dmu(1) + mu*(            lambda_mu*ux_rhou  );
  Tf tauyy_rhov = tauyy_mu*dmu(2) + mu*(2*vy_rhov + lambda_mu*vy_rhov  );
  Tf tauyy_rhow = tauyy_mu*dmu(3) + mu*(            lambda_mu*wz_rhow  );
  Tf tauyy_rhoE = tauyy_mu*dmu(4);

  Tf tauzz_rho  = tauzz_mu*dmu(0) + mu*(2*wz_rho  + lambda_mu*divV_rho );
  Tf tauzz_rhou = tauzz_mu*dmu(1) + mu*(            lambda_mu*ux_rhou  );
  Tf tauzz_rhov = tauzz_mu*dmu(2) + mu*(            lambda_mu*vy_rhov  );
  Tf tauzz_rhow = tauzz_mu*dmu(3) + mu*(2*wz_rhow + lambda_mu*wz_rhow  );
  Tf tauzz_rhoE = tauzz_mu*dmu(4);

  Tf tauxy_rho  = tauxy_mu*dmu(0) + mu*(uy_rho + vx_rho  );
  Tf tauxy_rhou = tauxy_mu*dmu(1) + mu*(uy_rhou          );
  Tf tauxy_rhov = tauxy_mu*dmu(2) + mu*(         vx_rhov );
  Tf tauxy_rhow = tauxy_mu*dmu(3);
  Tf tauxy_rhoE = tauxy_mu*dmu(4);

  Tf tauxz_rho  = tauxz_mu*dmu(0) + mu*(uz_rho + wx_rho  );
  Tf tauxz_rhou = tauxz_mu*dmu(1) + mu*(uz_rhou          );
  Tf tauxz_rhov = tauxz_mu*dmu(2);
  Tf tauxz_rhow = tauxz_mu*dmu(3) + mu*(         wx_rhow );
  Tf tauxz_rhoE = tauxz_mu*dmu(4);

  Tf tauyz_rho  = tauyz_mu*dmu(0) + mu*(vz_rho + wy_rho  );
  Tf tauyz_rhou = tauyz_mu*dmu(1);
  Tf tauyz_rhov = tauyz_mu*dmu(2) + mu*(vz_rhov          );
  Tf tauyz_rhow = tauyz_mu*dmu(3) + mu*(         wy_rhow );
  Tf tauyz_rhoE = tauyz_mu*dmu(4);

  //  f(ixMom) -= tauxx;
  //  f(iyMom) -= tauxy;
  //  f(izMom) -= tauxz;
  //  f(iEngy) -= k*tx + u*tauxx + v*tauxy + w*tauxz;
  //
  //  g(ixMom) -= tauxy;
  //  g(iyMom) -= tauyy;
  //  g(izMom) -= tauyz;
  //  g(iEngy) -= k*ty + u*tauxy + v*tauyy + w*tauyz;
  //
  //  h(ixMom) -= tauxz;
  //  h(iyMom) -= tauyz;
  //  h(izMom) -= tauzz;
  //  h(iEngy) -= k*tz + u*tauxz + v*tauyz + w*tauzz;

  dfdu(ixMom,0) -= tauxx_rho;
  dfdu(ixMom,1) -= tauxx_rhou;
  dfdu(ixMom,2) -= tauxx_rhov;
  dfdu(ixMom,3) -= tauxx_rhow;
  dfdu(ixMom,4) -= tauxx_rhoE;

  dfdu(iyMom,0) -= tauxy_rho;
  dfdu(iyMom,1) -= tauxy_rhou;
  dfdu(iyMom,2) -= tauxy_rhov;
  dfdu(iyMom,3) -= tauxy_rhow;
  dfdu(iyMom,4) -= tauxy_rhoE;

  dfdu(izMom,0) -= tauxz_rho;
  dfdu(izMom,1) -= tauxz_rhou;
  dfdu(izMom,2) -= tauxz_rhov;
  dfdu(izMom,3) -= tauxz_rhow;
  dfdu(izMom,4) -= tauxz_rhoE;

  dfdu(iEngy,0) -= k*tx_rho  + tx*dk(0) + u_rho*tauxx  + u*tauxx_rho  + v_rho*tauxy  + v*tauxy_rho  + w_rho*tauxz  + w*tauxz_rho;
  dfdu(iEngy,1) -= k*tx_rhou + tx*dk(1) + u_rhou*tauxx + u*tauxx_rhou                + v*tauxy_rhou                + w*tauxz_rhou;
  dfdu(iEngy,2) -= k*tx_rhov + tx*dk(2) +                u*tauxx_rhov + v_rhov*tauxy + v*tauxy_rhov                + w*tauxz_rhov;
  dfdu(iEngy,3) -= k*tx_rhow + tx*dk(3) +                u*tauxx_rhow                + v*tauxy_rhow + w_rhow*tauxz + w*tauxz_rhow;
  dfdu(iEngy,4) -= k*tx_rhoE + tx*dk(4) +                u*tauxx_rhoE                + v*tauxy_rhoE                + w*tauxz_rhoE;


  dgdu(ixMom,0) -= tauxy_rho;
  dgdu(ixMom,1) -= tauxy_rhou;
  dgdu(ixMom,2) -= tauxy_rhov;
  dgdu(ixMom,3) -= tauxy_rhow;
  dgdu(ixMom,4) -= tauxy_rhoE;

  dgdu(iyMom,0) -= tauyy_rho;
  dgdu(iyMom,1) -= tauyy_rhou;
  dgdu(iyMom,2) -= tauyy_rhov;
  dgdu(iyMom,3) -= tauyy_rhow;
  dgdu(iyMom,4) -= tauyy_rhoE;

  dgdu(izMom,0) -= tauyz_rho;
  dgdu(izMom,1) -= tauyz_rhou;
  dgdu(izMom,2) -= tauyz_rhov;
  dgdu(izMom,3) -= tauyz_rhow;
  dgdu(izMom,4) -= tauyz_rhoE;

  dgdu(iEngy,0) -= k*ty_rho  + ty*dk(0) + u_rho*tauxy  + u*tauxy_rho  + v_rho*tauyy  + v*tauyy_rho  + w_rho*tauyz  + w*tauyz_rho;
  dgdu(iEngy,1) -= k*ty_rhou + ty*dk(1) + u_rhou*tauxy + u*tauxy_rhou                + v*tauyy_rhou                + w*tauyz_rhou;
  dgdu(iEngy,2) -= k*ty_rhov + ty*dk(2) +                u*tauxy_rhov + v_rhov*tauyy + v*tauyy_rhov                + w*tauyz_rhov;
  dgdu(iEngy,3) -= k*ty_rhow + ty*dk(3) +                u*tauxy_rhow                + v*tauyy_rhow + w_rhow*tauyz + w*tauyz_rhow;
  dgdu(iEngy,4) -= k*ty_rhoE + ty*dk(4) +                u*tauxy_rhoE                + v*tauyy_rhoE                + w*tauyz_rhoE;


  dhdu(ixMom,0) -= tauxz_rho;
  dhdu(ixMom,1) -= tauxz_rhou;
  dhdu(ixMom,2) -= tauxz_rhov;
  dhdu(ixMom,3) -= tauxz_rhow;
  dhdu(ixMom,4) -= tauxz_rhoE;

  dhdu(iyMom,0) -= tauyz_rho;
  dhdu(iyMom,1) -= tauyz_rhou;
  dhdu(iyMom,2) -= tauyz_rhov;
  dhdu(iyMom,3) -= tauyz_rhow;
  dhdu(iyMom,4) -= tauyz_rhoE;

  dhdu(izMom,0) -= tauzz_rho;
  dhdu(izMom,1) -= tauzz_rhou;
  dhdu(izMom,2) -= tauzz_rhov;
  dhdu(izMom,3) -= tauzz_rhow;
  dhdu(izMom,4) -= tauzz_rhoE;

  dhdu(iEngy,0) -= k*tz_rho  + tz*dk(0) + u_rho*tauxz  + u*tauxz_rho  + v_rho*tauyz  + v*tauyz_rho  + w_rho*tauzz  + w*tauzz_rho;
  dhdu(iEngy,1) -= k*tz_rhou + tz*dk(1) + u_rhou*tauxz + u*tauxz_rhou                + v*tauyz_rhou                + w*tauzz_rhou;
  dhdu(iEngy,2) -= k*tz_rhov + tz*dk(2) +                u*tauxz_rhov + v_rhov*tauyz + v*tauyz_rhov                + w*tauzz_rhov;
  dhdu(iEngy,3) -= k*tz_rhow + tz*dk(3) +                u*tauxz_rhow                + v*tauyz_rhow + w_rhow*tauzz + w*tauzz_rhow;
  dhdu(iEngy,4) -= k*tz_rhoE + tz*dk(4) +                u*tauxz_rhoE                + v*tauyz_rhoE                + w*tauzz_rhoE;
}



// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;

  Tq  rho=0,  u=0,  v=0,  w=0,  t=0;
  Tqg rhox=0, ux=0, vx=0, wx=0, tx=0;
  Tqg rhoy=0, uy=0, vy=0, wy=0, ty=0;
  Tqg rhoz=0, uz=0, vz=0, wz=0, tz=0;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

  Tq mu = visc_.viscosity( t );
  Tq mu_t = 0;
  visc_.viscosityJacobian( t, mu_t );
  Tqg mux = mu_t*tx;
  Tqg muy = mu_t*ty;
  Tqg muz = mu_t*tz;

  Tq k = tcond_.conductivity( mu );
  Tq k_mu = 0;
  tcond_.conductivityJacobian( mu, k_mu );
  Tqg kx = k_mu*mux;
  Tqg ky = k_mu*muy;
  Tqg kz = k_mu*muz;

  strongFluxViscous( mu, mux, muy, muz, k, kx, ky, kz, q, qx, qy, qz, qxx, qxy, qyy, qxz, qyz, qzz, strongPDE );
}


// strong form viscous fluxes
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Tq& mu,
    const typename promote_Surreal<Tq, Tg>::type& mux,
    const typename promote_Surreal<Tq, Tg>::type& muy,
    const typename promote_Surreal<Tq, Tg>::type& muz,
    const Tq& k,
    const typename promote_Surreal<Tq, Tg>::type& kx,
    const typename promote_Surreal<Tq, Tg>::type& ky,
    const typename promote_Surreal<Tq, Tg>::type& kz,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& strongPDE ) const
{
  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Tq  rho=0,  u=0,  v=0,  w=0,  t=0;
  Tqg rhox=0, ux=0, vx=0, wx=0, tx=0;
  Tqg rhoy=0, uy=0, vy=0, wy=0, ty=0;
  Tqg rhoz=0, uz=0, vz=0, wz=0, tz=0;

  qInterpret_.eval( q, rho, u, v, w, t );
  qInterpret_.evalGradient( q, qx, rhox, ux, vx, wx, tx );
  qInterpret_.evalGradient( q, qy, rhoy, uy, vy, wy, ty );
  qInterpret_.evalGradient( q, qz, rhoz, uz, vz, wz, tz );

#if 0
  // evaluate second derivatives
  T rhoxx, rhoxy, rhoyy, rhoxz, rhoyz, rhozz;
  T uxx, uxy, uyy, uxz, uyz, uzz;
  T vxx, vxy, vyy, vxz, vyz, vzz;
  T wxx, wxy, wyy, wxz, wyz, wzz;
  T txx, txy, tyy, txz, tyz, tzz;
  qInterpret_.evalSecondGradient( q, qx, qy, qz,
                                  qxx, qxy, qyy, qxz, qyz, qzz,
                                  rhoxx, rhoxy, rhoyy, rhoxz, rhoyz, rhozz,
                                  uxx, uxy, uyy, uxz, uyz, uzz,
                                  vxx, vxy, vyy, vxz, vyz, vzz,
                                  wxx, wxy, wyy, wxz, wyz, wzz,
                                  txx, txy, tyy, txz, tyz, tzz );
#else
  // evaluate second derivatives
  T uxx, uxy, uyy, uxz,      uzz;
  T vxx, vxy, vyy,      vyz, vzz;
  T wxx,      wyy, wxz, wyz, wzz;
  T txx,      tyy,           tzz;

  DLA::MatrixSymS<D,T> rhoH, uH, vH, wH, tH;
  qInterpret_.evalSecondGradient( q, qx, qy, qz,
                                  qxx, qxy, qyy, qxz, qyz, qzz,
                                  rhoH, uH, vH, wH, tH );
  uxx = uH(0,0);  vxx = vH(0,0);  wxx = wH(0,0);  txx = tH(0,0);
  uxy = uH(1,0);  vxy = vH(1,0);
  uyy = uH(1,1);  vyy = vH(1,1);  wyy = wH(1,1);  tyy = tH(1,1);
  uxz = uH(2,0);                  wxz = wH(2,0);
                  vyz = vH(2,1);  wyz = wH(2,1);
  uzz = uH(2,2);  vzz = vH(2,2);  wzz = wH(2,2);  tzz = tH(2,2);

#endif

  Tq  lambda  = -2./3. * mu;
  Tqg lambdax = -2./3. * mux;
  Tqg lambday = -2./3. * muy;
  Tqg lambdaz = -2./3. * muz;

  T tauxx = mu*(2*ux   ) + lambda*(ux + vy + wz);
  T tauyy = mu*(2*vy   ) + lambda*(ux + vy + wz);
  T tauzz = mu*(2*wz   ) + lambda*(ux + vy + wz);
  T tauxy = mu*(uy + vx);
  T tauxz = mu*(uz + wx);
  T tauyz = mu*(vz + wy);

  T tauxx_x = mux*(2*ux   ) + mu*(2*uxx    ) + lambdax*(ux + vy + wz) + lambda*(uxx + vxy + wxz);
  T tauxy_y = muy*(uy + vx) + mu*(uyy + vxy);
  T tauxz_z = muz*(uz + wx) + mu*(uzz + wxz);

  T tauxy_x = mux*(uy + vx) + mu*(uxy + vxx);
  T tauyy_y = muy*(2*vy   ) + mu*(2*vyy    ) + lambday*(ux + vy + wz) + lambda*(uxy + vyy + wyz);
  T tauyz_z = muz*(vz + wy) + mu*(vzz + wyz);

  T tauxz_x = mux*(uz + wx) + mu*(uxz + wxx);
  T tauyz_y = muy*(vz + wy) + mu*(vyz + wyy);
  T tauzz_z = muz*(2*wz   ) + mu*(2*wzz    ) + lambdaz*(ux + vy + wz) + lambda*(uxz + vyz + wzz);

  //      (         tauxx                    )
  //  F = (                   tauxy          )
  //      (                             tauxz)
  //      (k*tx + u*tauxx + v*tauxy + w*tauxz)
  //
  //      (         tauxy                    )
  //  G = (                   tauyy          )
  //      (                             tauyz)
  //      (k*tz + u*tauxy + v*tauyy + w*tauyz)
  //
  //      (         tauxz                    )
  //  H = (                   tauyz          )
  //      (                             tauzz)
  //      (k*tz + u*tauxz + v*tauyz + w*tauzz)

  strongPDE(ixMom) -= tauxx_x + tauxy_y + tauxz_z;
  strongPDE(iyMom) -= tauxy_x + tauyy_y + tauyz_z;
  strongPDE(izMom) -= tauxz_x + tauyz_y + tauzz_z;
  strongPDE(iEngy) -= kx*tx + k*txx + ux*tauxx + u*tauxx_x + vx*tauxy + v*tauxy_x + wx*tauxz + w*tauxz_x;
  strongPDE(iEngy) -= ky*ty + k*tyy + uy*tauxy + u*tauxy_y + vy*tauyy + v*tauyy_y + wy*tauyz + w*tauyz_y;
  strongPDE(iEngy) -= kz*tz + k*tzz + uz*tauxz + u*tauxz_z + vz*tauyz + v*tauyz_z + wz*tauzz + w*tauzz_z;
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<T>& forcing ) const
{
  (*forcing_)(*this, x, y, z, time, forcing );
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDENavierStokes3D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDENavierStokes3D<PDETraitsSize, PDETraitsModel>: N = " << N << std::endl;
  out << indent << "PDENavierStokes3D<PDETraitsSize, PDETraitsModel>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes3D<PDETraitsSize, PDETraitsModel>: gas_ = " << std::endl;
  this->gas_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes3D<PDETraitsSize, PDETraitsModel>: visc_ = " << std::endl;
  this->visc_.dump(indentSize+2, out);
  out << indent << "PDENavierStokes3D<PDETraitsSize, PDETraitsModel>: tcond_ = " << std::endl;
  this->tcond_.dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDENAVIERSTOKES3D_H
