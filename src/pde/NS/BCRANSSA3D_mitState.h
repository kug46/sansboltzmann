// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCRANSSA3D_MITSTATE_H
#define BCRANSSA3D_MITSTATE_H

// 3-D compressible RANS SA BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"

#include "TraitsNavierStokes.h"
#include "PDERANSSA3D.h"
#include "BCNavierStokes3D.h"
#include "BCRANSSA3D_Solution.h"

#include "pde/BCCategory.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 3-D compressible RANS SA
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeWall_mitState;
class BCTypeDirichlet_mitState;
class BCTypeNeumann_mitState;
class BCTypeFullState_mitState;

template <class BCType, class BCNS3D>
class BCRANSSA3D;

template <class BCType, class BCNS3DParams>
struct BCRANSA3DParams;


//----------------------------------------------------------------------------//
// Conventional PX-style Wall BC
//
// specify: SA working variable as zero
//----------------------------------------------------------------------------//

template <class BCNS3D>
class BCRANSSA3D<BCTypeWall_mitState, BCNS3D> : public BCNS3D
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCNS3D::Category Category;
  typedef typename BCNS3D::ParamsType ParamsType;
  typedef typename BCNS3D::PDE PDE;

  using BCNS3D::D;   // physical dimensions
  using BCNS3D::N;   // total solution variables

  static const int NBC = BCNS3D::NBC+1; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs > //cppcheck-suppress noExplicitConstructor
  BCRANSSA3D( BCArgs&&... args ) :
    BCNS3D(std::forward<BCArgs>(args)...) {}

  BCRANSSA3D( const PDE& pde, const PyDict& d ) :
    BCNS3D(pde, d) {}

  ~BCRANSSA3D() {}

  BCRANSSA3D( const BCRANSSA3D& ) = delete;
  BCRANSSA3D& operator=( const BCRANSSA3D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS3D::state(x, y, z, time, nx, ny, nz, qI, qB);
    // TODO: I think this should happen in setFromPrimitive of the qinterpreter
    qB(qInterpret_.iSA) = 0;
  }

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS3D::fluxNormal(param, x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
  }

  // is the boundary state valid
  using BCNS3D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  using BCNS3D::pde_;
  using BCNS3D::qInterpret_;   // solution variable interpreter class
};

template <class BCNS3D>
void
BCRANSSA3D<BCTypeWall_mitState, BCNS3D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA3D<BCTypeWall_mitState, BCNS3D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA3D<BCTypeWall_mitState, BCNS3D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX-style Dirichlet BC
//
// specify: SA working variable
//----------------------------------------------------------------------------//

template<class BCNS3DParams>
struct BCRANSA3DParams<BCTypeDirichlet_mitState, BCNS3DParams> : public BCNS3DParams
{
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "Specified SA working variable"};

  using BCNS3DParams::BCName;
  typedef typename BCNS3DParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.nt));
    BCNS3DParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCRANSA3DParams params;
};
// Instantiate the singleton
template<class BCNS3DParams>
BCRANSA3DParams<BCTypeDirichlet_mitState, BCNS3DParams> BCRANSA3DParams<BCTypeDirichlet_mitState, BCNS3DParams>::params;


template <class BCNS3D>
class BCRANSSA3D<BCTypeDirichlet_mitState, BCNS3D> : public BCNS3D
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCNS3D::Category Category;
  typedef BCRANSA3DParams<BCTypeDirichlet_mitState, typename BCNS3D::ParamsType> ParamsType;
  typedef typename BCNS3D::PDE PDE;

  using BCNS3D::D;   // physical dimensions
  using BCNS3D::N;   // total solution variables

  static const int NBC = BCNS3D::NBC+1; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >
  BCRANSSA3D(const PDE& pde, const Real nt, const BCArgs&... args) :
    BCNS3D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(nt) {}

  BCRANSSA3D(const PDE& pde, const PyDict& d ) :
    BCNS3D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(d.get(ParamsType::params.nt)) {}

  ~BCRANSSA3D() {}

  BCRANSSA3D( const BCRANSSA3D& ) = delete;
  BCRANSSA3D& operator=( const BCRANSSA3D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS3D::state(x, y, z, time, nx, ny, nz, qI, qB);
    // TODO: I think this should happen in setFromPrimitive of the qinterpreter
    qB(qInterpret_.iSA) = nt_;
  }

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS3D::fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
  }

  // is the boundary state valid
  using BCNS3D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real nt_;
};

template <class BCNS3D>
void
BCRANSSA3D<BCTypeDirichlet_mitState, BCNS3D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA3D<BCTypeDirichlet_mitState, BCNS3D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA3D<BCTypeDirichlet_mitState, BCNS3D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCRANSSA3D<BCTypeDirichlet_mitState, BCNS3D>: nt_ = " << nt_ << std::endl;
}


//----------------------------------------------------------------------------//
// Conventional PX-style None BC
//----------------------------------------------------------------------------//

template <class BCNS3D>
class BCRANSSA3D<BCTypeNeumann_mitState, BCNS3D> : public BCNS3D
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCCategory::Flux_mitState Category;
  static_assert( std::is_same<Category, typename BCNS3D::Category>::value, "BC category must match that of base class!");
  typedef typename BCNS3D::ParamsType ParamsType;
  typedef typename BCNS3D::PDE PDE;

  using BCNS3D::D;   // physical dimensions
  using BCNS3D::N;   // total solution variables

  using BCNS3D::NBC; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >
  BCRANSSA3D(const PDE& pde, const BCArgs&... args) :
    BCNS3D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  BCRANSSA3D(const PDE& pde, const PyDict& d ) :
    BCNS3D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  ~BCRANSSA3D() {}

  BCRANSSA3D( const BCRANSSA3D& ) = delete;
  BCRANSSA3D& operator=( const BCRANSSA3D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS3D::state(x, y, z, time, nx, ny, nz, qI, qB);
    qB(qInterpret_.iSA) = qI(qInterpret_.iSA);
  }

  // normal BC flux
  template <class Tp, class T>
  void fluxNormal( const Tp& param,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS3D::fluxNormal(param, x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);
  }

  // is the boundary state valid
  using BCNS3D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};

template <class BCNS3D>
void
BCRANSSA3D<BCTypeNeumann_mitState, BCNS3D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA3D<BCTypeNeumann_mitState, BCNS3D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA3D<BCTypeNeumann_mitState, BCNS3D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Conventional PX-style Full State
//----------------------------------------------------------------------------//

template <class BCNS3D>
class BCRANSSA3D<BCTypeFullState_mitState, BCNS3D> : public BCNS3D
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCNS3D::Category Category;
  typedef typename BCNS3D::ParamsType ParamsType;
  typedef typename BCNS3D::PDE PDE;

  using BCNS3D::D;   // physical dimensions
  using BCNS3D::N;   // total solution variables
  using BCNS3D::NBC; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >
  BCRANSSA3D(const PDE& pde, const BCArgs&... args) :
    BCNS3D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  BCRANSSA3D(const PDE& pde, const PyDict& d ) :
    BCNS3D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()) {}

  ~BCRANSSA3D() {}

  BCRANSSA3D( const BCRANSSA3D& ) = delete;
  BCRANSSA3D& operator=( const BCRANSSA3D& ) = delete;

  // BC state
  template <class Tp, class T>
  void state( const Tp& param,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS3D::state(x, y, z, time, nx, ny, nz, qI, qB);
  }

  // is the boundary state valid
  using BCNS3D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
};

template <class BCNS3D>
void
BCRANSSA3D<BCTypeFullState_mitState, BCNS3D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA3D<BCTypeFullState_mitState, BCNS3D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA3D<BCTypeFullState_mitState, BCNS3D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
}


#if 0   // implementation not complete
//----------------------------------------------------------------------------//
// Berg-Nordstrom Inlet BC
//
// specify: SA working variable
//----------------------------------------------------------------------------//

template<class BCNS3DParams>
struct BCRANSA3DParams<BCTypeInflowSubsonic_sHqt_BN, BCNS3DParams> : public BCNS3DParams
{
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "Specified SA working variable"};

  using BCNS3DParams::BCName;
  typedef typename BCNS3DParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.nt));
    BCNS3DParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCRANSA3DParams params;
};
// Instantiate the singleton
template<class BCNS3DParams>
BCRANSA3DParams<BCTypeInflowSubsonic_sHqt_BN, BCNS3DParams> BCRANSA3DParams<BCTypeInflowSubsonic_sHqt_BN, BCNS3DParams>::params;


template <class BCNS3D>
class BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN, BCNS3D> : public BCNS3D
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCNS3D::Category Category;
  typedef BCRANSA3DParams<BCTypeInflowSubsonic_sHqt_BN, typename BCNS3D::ParamsType> ParamsType;
  typedef typename BCNS3D::PDE PDE;

  using BCNS3D::D;   // physical dimensions
  using BCNS3D::N;   // total solution variables

  static const int NBC = BCNS3D::NBC+1; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >
  BCRANSSA3D(const PDE& pde, const Real nt, const BCArgs&... args) :
    BCNS3D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(nt) {}

  BCRANSSA3D(const PDE& pde, const PyDict& d ) :
    BCNS3D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(d.get(ParamsType::params.nt)) {}

  ~BCRANSSA3D() {}

  BCRANSSA3D( const BCRANSSA3D& ) = delete;
  BCRANSSA3D& operator=( const BCRANSSA3D& ) = delete;

  // BC state
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS3D::state(x, y, z, time, nx, ny, nz, qI, qB);
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& dist,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS3D::fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);

    Real gamma = BCNS3D::gas_.gamma();
    Real gmi   = gamma - 1;

    T rho=0, u=0, v=0, w=0, t=0;
    T nt = 0;

    //compute BN Flux
    qInterpret_.eval( qI, rho, u, v, w, t );
    T p = BCNS3D::gas_.pressure(rho,t);

    T H = BCNS3D::gas_.enthalpy(rho, t) + 0.5*(u*u + v*v + w*w);
    T c = BCNS3D::gas_.speedofSound(t);
    T s = log( p /pow(rho,gamma) );

    qInterpret_.evalSA( qI, nt );

    // tangential vector
    const Real tx = -ny;
    const Real ty =  nx;

    // normal and tangential velocities
    T vn = u*nx + v*ny + w*nz;
    T vt = u*tx + v*ty + w*tz;
    T V2 = u*u + v*v + w*w;
    T V = sqrt(V2);

    // Specified tangential velocity component
  //  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

    // Strong BC's that are imposed
    T ds  = s  - BCNS3D::sSpec_;
    T dH  = H  - BCNS3D::HSpec_;
    T dvt = v/sqrt(u*u + v*v + w*w) - sin(BCNS3D::aSpec_);
    T dnt = nt - nt_;

    Fn[pde_.iSA] -= -vn*nt*rho/gmi*ds;
    Fn[pde_.iSA] -= vn*gamma*nt*rho/(c*c + V2*gmi)*dH;
    Fn[pde_.iSA] -=  -(vt*V2*V*rho*gamma*nt)/(u*(c*c + V2*gmi))*dvt;
    Fn[pde_.iSA] -=  rho*vn*dnt;
  }

  // is the boundary state valid
  using BCNS3D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real nt_;
};

template <class BCNS3D>
void
BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN, BCNS3D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN, BCNS3D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN, BCNS3D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCRANSSA3D<BCTypeInflowSubsonic_sHqt_BN, BCNS3D>: nt_ = " << nt_ << std::endl;
}
#endif  // implementation not complete


#if 0   // implementation not complete
//----------------------------------------------------------------------------//
// Berg-Nordstrom Inlet BC w Solution Function
//
// specify: SA working variable
//----------------------------------------------------------------------------//

template<class BCNS3DParams>
struct BCRANSA3DParams<BCTypeFunctionInflow_sHqt_BN, BCNS3DParams> : public BCNS3DParams
{
  const ParameterNumeric<Real> nt{"nt", NO_DEFAULT, NO_RANGE, "Specified SA working variable"};

  using BCNS3DParams::BCName;
  typedef typename BCNS3DParams::Option Option;

  // cppcheck-suppress passedByValue
  static void checkInputs(PyDict d)
  {
    //TODO: This is not quite right...
    std::vector<const ParameterBase*> allParams;
    allParams.push_back(d.checkInputs(params.nt));
    BCNS3DParams::checkInputs(d);
    //d.checkUnknownInputs(allParams);
  }

  static BCRANSA3DParams params;
};
// Instantiate the singleton
template<class BCNS3DParams>
BCRANSA3DParams<BCTypeFunctionInflow_sHqt_BN, BCNS3DParams> BCRANSA3DParams<BCTypeFunctionInflow_sHqt_BN, BCNS3DParams>::params;


template <class BCNS3D>
class BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN, BCNS3D> : public BCNS3D
{
public:
  typedef PhysD3 PhysDim;
  typedef typename BCNS3D::Category Category;
  typedef BCRANSA3DParams<BCTypeFunctionInflow_sHqt_BN, typename BCNS3D::ParamsType> ParamsType;
  typedef typename BCNS3D::PDE PDE;

  using BCNS3D::D;   // physical dimensions
  using BCNS3D::N;   // total solution variables

  static const int NBC = BCNS3D::NBC+1; // total BCs

  typedef typename PDE::QInterpret QInterpret;         // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  template< class... BCArgs >
  BCRANSSA3D(const PDE& pde, const Real nt, const BCArgs&... args) :
    BCNS3D(pde, args...), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(nt) {}

  BCRANSSA3D(const PDE& pde, const PyDict& d ) :
    BCNS3D(pde, d), pde_(pde), qInterpret_(pde_.variableInterpreter()), nt_(d.get(ParamsType::params.nt)) {}

  ~BCRANSSA3D() {}

  BCRANSSA3D( const BCRANSSA3D& ) = delete;
  BCRANSSA3D& operator=( const BCRANSSA3D& ) = delete;

  // BC state
  template <class T>
  void state( const Real& dist,
              const Real& x, const Real& y, const Real& z, const Real& time,
              const Real& nx, const Real& ny, const Real& nz,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    BCNS3D::state(x, y, z, time, nx, ny, nz, qI, qB);
  }

  // normal BC flux
  template <class T>
  void fluxNormal( const Real& dist,
                   const Real& x, const Real& y, const Real& z, const Real& time,
                   const Real& nx, const Real& ny, const Real& nz,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy, const ArrayQ<T>& qIz,
                   const ArrayQ<T>& qB,
                   ArrayQ<T>& Fn) const
  {
    BCNS3D::fluxNormal(x, y, z, time, nx, ny, nz, qI, qIx, qIy, qIz, qB, Fn);

    Real sSpec, HSpec, vnSpec = 0;
    BCNS3D::BCQuantities(x,y,time, nx, ny, nz, sSpec, HSpec, vnSpec);

    Real gamma = BCNS3D::gas_.gamma();
    Real gmi   = gamma - 1;

    T rho=0, u=0, v=0, w=0, t=0;

    //compute BN Flux
    qInterpret_.eval( qI, rho, u, v, w, t );
    T p = BCNS3D::gas_.pressure(rho,t);

    T H = BCNS3D::gas_.enthalpy(rho, t) + 0.5*(u*u + v*v + w*w);
    T c = BCNS3D::gas_.speedofSound(t);
    T s = log( p /pow(rho,gamma) );
    T nt = 0;

    qInterpret_.evalSA( qI, nt );

    // tangential vector
    const Real tx = -ny;
    const Real ty =  nx;

    // normal and tangential velocities
    T vn = u*nx + v*ny;
    T vt = u*tx + v*ty;
    T V2 = u*u + v*v + w*w;
    T V = sqrt(V2);

    // Specified tangential velocity component
  //  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

    // Strong BC's that are imposed
    T ds  = s  - sSpec;
    T dH  = H  - HSpec;
    T dvt = v/sqrt(u*u + v*v + w*w) - vnSpec;
    T dnt = nt - nt_;

    Fn[pde_.iSA] -= -vn*nt*rho/gmi*ds;
    Fn[pde_.iSA] -= vn*gamma*nt*rho/(c*c+V2*gmi)*dH;
    Fn[pde_.iSA] -=  -(vt*V2*V*rho*gamma*nt)/(u*(c*c+V2*gmi))*dvt;
    Fn[pde_.iSA] -=  rho*vn*dnt;
  }

  // is the boundary state valid
  using BCNS3D::isValidState;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real nt_;
};

template <class BCNS3D>
void
BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN, BCNS3D>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN, BCNS3D>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN, BCNS3D>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCRANSSA3D<BCTypeFunctionInflow_sHqt_BN, BCNS3D>: nt_ = " << nt_ << std::endl;
}
#endif  // implementation not complete

} //namespace SANS

#endif  // BCRANSSA3D_MITSTATE_H
