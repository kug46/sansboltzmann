// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef FLUIDS_SENSOR_FWD_H
#define FLUIDS_SENSOR_FWD_H

#include "Topology/Dimension.h"

namespace SANS
{

// Forward declaration
template<class PhysDim, class PDE>
class Fluids_Sensor;

}

#endif //FLUIDS_SENSOR_FWD_H
