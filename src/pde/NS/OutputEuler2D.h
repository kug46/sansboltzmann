// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTEULER2D_H
#define OUTPUTEULER2D_H

#include "PDERANSSA2D.h"

#include "pde/OutputCategory.h"
#include "pde/AnalyticFunction/Function2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Provides weights to compute a force from a weighted residual

template <class PDENDConvert>
class OutputEuler2D_Force : public OutputType< OutputEuler2D_Force<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputEuler2D_Force( const PDENDConvert& pde, const Real& wx, const Real& wy ) :
    pde_(pde), wx_(wx), wy_(wy) {}

  void operator()(const Real& x, const Real& y, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = 0;
    weight[pde_.ixMom] = wx_;
    weight[pde_.iyMom] = wy_;
  }

private:
  const PDENDConvert& pde_;
  Real wx_, wy_;
};


//----------------------------------------------------------------------------//
// Subfunctions to compute single output from four Euler variables

template <class PDENDConvert>
class OutputEuler2D_Pressure : public OutputType< OutputEuler2D_Pressure<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_Pressure( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0;

    qInterp.eval( q, rho, u, v, t );
    Tq p = gas.pressure(rho, t);
    output = p;
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();
    Tq rho = 0, u = 0, v = 0, t = 0;
    Real gamma = gas.gamma();

    qInterp.eval( q, rho, u, v, t );
//    T p = gas.pressure(rho, t);

    Real gm1 = gamma-1.;

    dJdu[0] = 0.5*(u*u + v*v)*gm1;
    dJdu[1] = -u*gm1;
    dJdu[2] = -v*gm1;
    dJdu[3] = gm1;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, time, q, qx, qy, dJdu);
  }



private:
  const PDENDConvert& pde_;
};


template <class PDENDConvert>
class OutputEuler2D_TotalEnthalpy : public OutputType< OutputEuler2D_TotalEnthalpy<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_TotalEnthalpy( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0;

    qInterp.eval( q, rho, u, v, t );
    Tq h = gas.enthalpy(rho, t);
    output = h + 0.5*(u*u + v*v);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();
    Tq rho = 0, u = 0, v = 0, t = 0;
    Real gamma = gas.gamma();

    qInterp.eval( q, rho, u, v, t );
    Tq p = gas.pressure(rho, t);

    Real gm1 = gamma-1.;

    dJdu[0] = (-2*p*gamma + rho*(u*u + v*v)*(2-3*gamma + gamma*gamma))/(2*gm1*rho*rho);
    dJdu[1] = -u*gm1/rho;
    dJdu[2] = -v*gm1/rho;
    dJdu[3] = gamma/rho;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, time, q, qx, qy, dJdu);
  }

private:
  const PDENDConvert& pde_;
};

template <class PDENDConvert>
class OutputEuler2D_TotalEnthalpyWindowed : public OutputType< OutputEuler2D_TotalEnthalpyWindowed<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_TotalEnthalpyWindowed( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0;

    qInterp.eval( q, rho, u, v, t );
    Tq h = gas.enthalpy(rho, t);
    Real gauss = sqrt(2048./PI)*exp(-2048*( (0.5 - x)*(0.5-x) + (0.5-y)*(0.5-y) ) );
    output = (h + 0.5*(u*u + v*v))*gauss;
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();
    Tq rho = 0, u = 0, v = 0, t = 0;
    Real gamma = gas.gamma();

    qInterp.eval( q, rho, u, v, t );
    Tq p = gas.pressure(rho, t);

    Real gm1 = gamma-1.;

    Real gauss = sqrt(2048./PI)*exp(-2048*( (0.5 - x)*(0.5-x) + (0.5-y)*(0.5-y) ) );

    dJdu[0] = (-2*p*gamma + rho*(u*u + v*v)*(2-3*gamma + gamma*gamma))/(2*gm1*rho*rho)*gauss;
    dJdu[1] = -u*gm1/rho*gauss;
    dJdu[2] = -v*gm1/rho*gauss;
    dJdu[3] = gamma/rho*gauss;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, time, q, qx, qy, dJdu);
  }

private:
  const PDENDConvert& pde_;
};




template <class PDENDConvert>
class OutputEuler2D_Entropy : public OutputType< OutputEuler2D_Entropy<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_Entropy( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0;
    Tq gamma = gas.gamma();

    qInterp.eval( q, rho, u, v, t );
    Tq p = gas.pressure(rho, t);
    Tq s = log(p / pow(rho, gamma));
    output = s;
  }


  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();
    Tq rho = 0, u = 0, v = 0, t = 0;
    Real gamma = gas.gamma();

    qInterp.eval( q, rho, u, v, t );
    Tq p = gas.pressure(rho, t);

    Real gm1 = gamma-1.;

    dJdu[0] = 0.5*(u*u + v*v)*gm1/p;
    dJdu[1] = -u*gm1/p;
    dJdu[2] = -v*gm1/p;
    dJdu[3] = gm1/p;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, time, q, qx, qy, dJdu);
  }


private:
  const PDENDConvert& pde_;
};


template <class PDENDConvert>
class OutputEuler2D_EntropyErrorSquare : public OutputType< OutputEuler2D_EntropyErrorSquare<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDENDConvert PDE;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  OutputEuler2D_EntropyErrorSquare( const PDENDConvert& pde, const Real entropyExact ) :
    pde_(pde), entropyExact_(entropyExact) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0;
    Real gamma = gas.gamma();

    qInterp.eval( q, rho, u, v, t );
    Tq p = gas.pressure(rho, t);
    Tq s = p / pow(rho, gamma);

    Tq ds = s - entropyExact_;

    output = ds*ds;
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();
    Tq rho = 0, u = 0, v = 0, t = 0;
    Real gamma = gas.gamma();

    qInterp.eval( q, rho, u, v, t );
    Tq p = gas.pressure(rho, t);
    Tq s = p / pow(rho, gamma);

    Tq ds = s - entropyExact_;

    dJdu[0] = -pow(rho, -1-gamma)*(2*p*gamma - rho*(gamma-1)*(u*u + v*v))*ds;
    dJdu[1] = -2*u*(gamma-1)*pow(rho, -gamma)*ds;
    dJdu[2] = -2*v*(gamma-1)*pow(rho, -gamma)*ds;
    dJdu[3] = 2*(gamma-1)*pow(rho, -gamma)*ds;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, time, q, qx, qy, dJdu);
  }

private:
  const PDENDConvert& pde_;
  const Real entropyExact_;
};

template <class PDENDConvert>
class OutputEuler2D_Density : public OutputType< OutputEuler2D_Density<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_Density( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    Tq rho = 0;

    pde_.variableInterpreter().evalDensity( q, rho );
    output = rho;
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    dJdu[0] = 1;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, time, q, qx, qy, dJdu);
  }


private:
  const PDENDConvert& pde_;
};


template <class PDENDConvert>
class OutputEuler2D_U : public OutputType< OutputEuler2D_U<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_U( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    Tq rho = 0, u = 0, v = 0, t = 0;

    pde_.variableInterpreter().eval( q, rho, u, v, t );
    output = u;
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    Tq rho = 0, u = 0, v = 0, t = 0;

    pde_.variableInterpreter().eval( q, rho, u, v, t );

    dJdu[0] = -u/rho;
    dJdu[1] = 1/rho;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, time, q, qx, qy, dJdu);
  }

private:
  const PDENDConvert& pde_;
};


template <class PDENDConvert>
class OutputEuler2D_SolutionVariable : public OutputType< OutputEuler2D_SolutionVariable<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_SolutionVariable( const PDENDConvert& pde, const int vnum ) :
    pde_(pde), vnum_(vnum) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    output = q(vnum_);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


private:
  const PDENDConvert& pde_;
  const int vnum_;
};


template <class PDENDConvert>
class OutputEuler2D_ConservativeVariable : public OutputType< OutputEuler2D_ConservativeVariable<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_ConservativeVariable( const PDENDConvert& pde, const int vnum ) :
    pde_(pde), vnum_(vnum) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0, e = 0, E = 0;

    qInterp.eval( q, rho, u, v, t );
    e = gas.energy(rho, t);
    E = e + 0.5*(u*u + v*v);

    if (vnum_==0)
    {
      output = rho;
    }
    else if (vnum_==1)
    {
      output = rho*u;
    }
    else if (vnum_==2)
    {
      output = rho*v;
    }
    else
    {
      output = rho*E;
    }
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


private:
  const PDENDConvert& pde_;
  const int vnum_;
};


template <class PDENDConvert>
class OutputEuler2D_EntropyVariable : public OutputType< OutputEuler2D_EntropyVariable<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_EntropyVariable( const PDENDConvert& pde, const int vnum ) :
    pde_(pde), vnum_(vnum) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0, e = 0, E = 0;

    qInterp.eval( q, rho, u, v, t );
    e = gas.energy(rho, t);
    E = e + 0.5*(u*u + v*v);

    Tq rhoe = rho*e;
    Tq rhoE = rho*E;
    Real gamma = gas.gamma();
    Tq s = log((gamma-1.0)*rhoe / pow(rho, gamma));

    if (vnum_==0)
    {
      output = -rhoE/rhoe + gamma + 1 - s;
    }
    else if (vnum_==1)
    {
      output = rho*u/rhoe;
    }
    else if (vnum_==2)
    {
      output = rho*v/rhoe;
    }
    else
    {
      output = -rho/rhoe;
    }
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


private:
  const PDENDConvert& pde_;
  const int vnum_;
};

template <class PDENDConvert>
class OutputEuler2D_TemperatureProbe : public OutputType< OutputEuler2D_TemperatureProbe<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_TemperatureProbe( const PDENDConvert& pde,
   const std::vector<Real> x, const std::vector<Real> y, const Real width = 1e-1 ) :
    pde_(pde), x_(x.begin(),x.end()), y_(y.begin(),y.end()), width_(width)
    { SANS_ASSERT_MSG( x.size() == y.size(), "x, y coordinates must come in pairs");  }

   explicit OutputEuler2D_TemperatureProbe( const PDENDConvert& pde, const Real x, const Real y, const Real width = 1e-1 ) :
     pde_(pde),x_(1,x), y_(1,y) { }

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    //const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0;
    qInterp.eval( q, rho, u, v, t );

    Real outW = 0;
    //std::cout<< "x_.size() = " << x_.size() << ", x_[0]= " << x_[0] <<std::endl;
    for (std::size_t i = 0; i < x_.size(); i++)
    {
      outW += computeWeight( x, y, x_[i], y_[i], width_ );
    }
    //std::cout << "outW = " << outW << std::endl;
    output = outW * t; // sum of all the weight functions
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


private:
  const PDENDConvert& pde_;
  std::vector<Real> x_, y_;
  Real width_;

  Real computeWeight( const Real x, const Real y, const Real x0, const Real y0, const Real width ) const
  {
    return sqrt(2*PI*width)*exp( (-(x-x0)*(x-x0) - (y-y0)*(y-y0))/(2*width) );
  }
};


template <class PDENDConvert>
class OutputEuler2D_SolutionTotalErrorSquare : public OutputType< OutputEuler2D_SolutionTotalErrorSquare<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_SolutionTotalErrorSquare( const PDENDConvert& pde, const ArrayQ<Real>& qstar ) :
    pde_(pde), qstar_(qstar) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    output = pow(q(0)-qstar_(0), 2.) + pow(q(1)-qstar_(1), 2.) + pow(q(2)-qstar_(2), 2.) + pow(q(3)-qstar_(3), 2.);
  }


  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


private:
  const PDENDConvert& pde_;
  const ArrayQ<Real>& qstar_;
};


template <class PDENDConvert>
class OutputEuler2D_ConservativeTotalErrorSquare : public OutputType< OutputEuler2D_ConservativeTotalErrorSquare<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_ConservativeTotalErrorSquare( const PDENDConvert& pde, const ArrayQ<Real>& ustar ) :
    pde_(pde), ustar_(ustar) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0, e = 0, E = 0;

    qInterp.eval( q, rho, u, v, t );
    e = gas.energy(rho, t);
    E = e + 0.5*(u*u + v*v);

    output = pow(rho-ustar_(0), 2.) + pow(rho*u-ustar_(1), 2.) + pow(rho*v-ustar_(2), 2.) + pow(rho*E-ustar_(3), 2.);
  }


  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


private:
  const PDENDConvert& pde_;
  const ArrayQ<Real>& ustar_;
};


template <class PDENDConvert>
class OutputEuler2D_EntropyTotalErrorSquare : public OutputType< OutputEuler2D_EntropyTotalErrorSquare<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_EntropyTotalErrorSquare( const PDENDConvert& pde, const ArrayQ<Real>& vstar ) :
    pde_(pde), vstar_(vstar) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0, e = 0, E = 0;

    qInterp.eval( q, rho, u, v, t );
    e = gas.energy(rho, t);
    E = e + 0.5*(u*u + v*v);

    Tq rhoe = rho*e;
    Tq rhoE = rho*E;
    Real gamma = gas.gamma();
    Tq s = log((gamma-1.0)*rhoe / pow(rho, gamma));

    Tq v0 = -rhoE/rhoe + gamma + 1 - s;
    Tq v1 = rho*u/rhoe;
    Tq v2 = rho*v/rhoe;
    Tq v3 = -rho/rhoe;

    output = pow(v0-vstar_(0), 2.) + pow(v1*u-vstar_(1), 2.) + pow(v2-vstar_(2), 2.) + pow(v3-vstar_(3), 2.);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

private:
  const PDENDConvert& pde_;
  const ArrayQ<Real>& vstar_;
};


template <class PDENDConvert>
class OutputEuler2D_MomentumErrorSquare : public OutputType< OutputEuler2D_MomentumErrorSquare<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  typedef Function2DBase<ArrayQ<Real>> SolutionBase;

  explicit OutputEuler2D_MomentumErrorSquare( const PDENDConvert& pde, const SolutionBase& soln ) :
    pde_(pde), soln_(soln) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    Tq rho = 0, u = 0, v = 0, t = 0;
    pde_.variableInterpreter().eval( q, rho, u, v, t );

    Tq rhoE = 0, uE =0, vE = 0, tE = 0;
    ArrayQ<Real> qExact = soln_(x, y, time);
    pde_.variableInterpreter().eval( qExact, rhoE, uE, vE, tE );

    output = pow(rho*u - rhoE*uE, 2) + pow(rho*v - rhoE*vE, 2);
  }


  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


private:
  const PDENDConvert& pde_;
  const SolutionBase& soln_;
};


template <class PDENDConvert>
class OutputEuler2D_Momentum : public OutputType< OutputEuler2D_Momentum<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_Momentum( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    Tq rho = 0, u = 0, v = 0, t = 0;

    pde_.variableInterpreter().eval( q, rho, u, v, t );
    output = sqrt(rho*u*rho*u + rho*v*rho*v);
  }


  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


private:
  const PDENDConvert& pde_;
};

template <class PDENDConvert>
class OutputEuler2D_V2 : public OutputType< OutputEuler2D_V2<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_V2( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }


  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    Tq u = 0, v = 0;

    pde_.variableInterpreter().evalVelocity( q, u, v );
    output = (u*u + v*v);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    //const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();
    Tq rho = 0, u = 0, v = 0, t = 0;

    qInterp.eval( q, rho, u, v, t );

    dJdu[0] = -(u*u + v*v)/rho;
    dJdu[1] = 2*u/rho;
    dJdu[2] = 2*v/rho;
    dJdu[3] = 0;

  }


  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, time, q, qx, qy, dJdu);
  }

private:
  const PDENDConvert& pde_;
};


template <class PDENDConvert>
class OutputEuler2D_Theta : public OutputType< OutputEuler2D_Theta<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_Theta( const PDENDConvert& pde, const Real Vinf ) :
    pde_(pde), Vinf_(Vinf) {}

  bool needsSolutionGradient() const { return false; }


  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    Tq u = 0, v = 0;

    pde_.variableInterpreter().evalVelocity( q, u, v );
    Tq V = sqrt(u*u + v*v);
    output = (u/Vinf_)*(1 - u/Vinf_);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }



private:
  const PDENDConvert& pde_;
  const Real Vinf_;
};


template <class PDENDConvert>
class OutputEuler2D_ForceEval : public OutputType< OutputEuler2D_ForceEval<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef OutputCategory::Functional Category;
  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  static const int ixMom = PDENDConvert::ixMom;
  static const int iyMom = PDENDConvert::iyMom;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_ForceEval( const PDENDConvert& pde, const Real& wx, const Real& wy ) :
    pde_(pde), wx_(wx), wy_(wy) {}

  bool needsSolutionGradient() const { return true; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("THIS IS A BOUNDARY OUTPUT");
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    ArrayQ<To> Fn;
    ArrayQ<To> F = 0;
    ArrayQ<To> G = 0;

    pde_.fluxAdvective(x, y, time, q, F, G);
    pde_.fluxViscous(x, y, time, q, qx, qy, F, G);

    Fn = F*nx + G*ny;

    output = Fn(ixMom)*wx_ + Fn(iyMom)*wy_;

  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& dist, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, nx, ny, q, qx, qy, output);
  }

private:
  const PDENDConvert& pde_;
  Real wx_, wy_;
};

// NOTE: this is (h + 1/2 * |U|^2)
template <class PDENDConvert>
class OutputEuler2D_totalEnthalpyErrorSquare : public OutputType< OutputEuler2D_totalEnthalpyErrorSquare<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDENDConvert PDE;

  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  OutputEuler2D_totalEnthalpyErrorSquare( const PDENDConvert& pde, const Real totalEnthalpyExact ) :
    pde_(pde), totalEnthalpyExact_(totalEnthalpyExact) {}

  bool needsSolutionGradient() const { return false; }


  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0;
    qInterp.eval( q, rho, u, v, t );
    Tq H = (gas.enthalpy(rho, t) + 0.5*(u*u + v*v));

    Tq dH = H - totalEnthalpyExact_;

    output = dH*dH;
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class Tq, class Tg, class To>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    const GasModel& gas = pde_.gasModel();
    const QInterpret& qInterp = pde_.variableInterpreter();

    Tq rho = 0, u = 0, v = 0, t = 0;

    qInterp.eval( q, rho, u, v, t );

    Real Cv = gas.Cv();

    Tq e = gas.energy(rho, t);
    Tq E = e + 0.5*(u*u + v*v);

    Tq u_rho  = -u/rho;
    Tq u_rhou =  1/rho;
    Tq v_rho  = -v/rho;
    Tq v_rhov =  1/rho;

    Tq t_rho  = ( -E/rho + ( u*u + v*v )/rho ) / Cv;
    Tq t_rhou = (        - (   u       )/rho ) / Cv;
    Tq t_rhov = (        - (         v )/rho ) / Cv;
    Tq t_rhoE = (  1/rho                     ) / Cv;

    Tq h_rho = 0.0, h_t = 0.0;
    gas.enthalpyJacobian(rho, t, h_rho, h_t);

    Tq H = (gas.enthalpy(rho, t) + 0.5*(u*u + v*v));

      h_rho  = h_rho + h_t * t_rho;
    Tq h_rhou =         h_t * t_rhou;
    Tq h_rhov =         h_t * t_rhov;
    Tq h_rhoE =         h_t * t_rhoE;

    Tq H_rho  = h_rho  + u*u_rho + v*v_rho;
    Tq H_rhou = h_rhou + u*u_rhou;
    Tq H_rhov = h_rhov +           v*v_rhov;
    Tq H_rhoE = h_rhoE;

    Tq dH = H - totalEnthalpyExact_;

    dJdu[0] = 2.0*dH*H_rho;
    dJdu[1] = 2.0*dH*H_rhou;
    dJdu[2] = 2.0*dH*H_rhov;
    dJdu[3] = 2.0*dH*H_rhoE;
  }

  template<class Tp, class Tq, class Tg, class To>
  void outputJacobian(const Tp& param, const Real& x, const Real& y, const Real& time,
                      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, MatrixJ<To>& dJdu) const
  {
    outputJacobian(x, y, time, q, qx, qy, dJdu);
  }

private:
  const PDENDConvert& pde_;
  const Real totalEnthalpyExact_;
};

template <class PDENDConvert>
class OutputEuler2D_TotalPressure : public OutputType< OutputEuler2D_TotalPressure<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef OutputCategory::Functional Category;
  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_TotalPressure( const PDENDConvert& pde, const Real s = 0, const Real x0 = 0, const Real y0 = 0,
                                        const Real p0Ref = 0) :
    pde_(pde),
    // s - standard deviations
    // s2 - variance
    s2_(s*s),
    x0_(x0), y0_(y0), // centroid location
    p0Ref_(p0Ref)
  {
    // N
  }

  bool needsSolutionGradient() const { return true; }

protected:
  template<class Tq, class To>
  void totalPressure(const Real& Gauss,
                     const ArrayQ<Tq>& q, ArrayJ<To>& output ) const
  {
    Tq rho = 0, u = 0, v = 0, t=0;

    pde_.variableInterpreter().eval( q, rho, u, v, t );
    Tq p = pde_.gasModel().pressure(rho, t);

    Tq c = pde_.gasModel().speedofSound(t);
    Tq M2 = (u*u + v*v) / (c*c);
    Tq gamma = pde_.gasModel().gamma();

    // Total pressure (stagnation pressure)
    Tq p0 = p*pow(1+(gamma-1)/2*M2, gamma/(gamma-1));

    output = (p0 - p0Ref_)*Gauss;
  }

public:
  // Area output

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    // Area integral
    Real Gauss = 1;
    if (s2_ > 0)
      Gauss = exp( -0.5*(pow(x-x0_,2) + pow(y-y0_,2))/s2_ )/( 2*s2_*PI);

    totalPressure(Gauss, q, output);
  }

  // Boundary output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    // Line integral
    Real Gauss = 1;
    if (s2_ > 0)
      Gauss = exp( -0.5*(pow(x-x0_,2) + pow(y-y0_,2))/s2_ )/sqrt( 2*s2_*PI);

    totalPressure(Gauss, q, output);
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, nx, ny, q, qx, qy, output);
  }

private:
  const PDENDConvert& pde_;
  const Real s2_;      //variance
  const Real x0_, y0_; //center location
  const Real p0Ref_;   //Reference stagnation pressure for Error
};


template <class PDENDConvert>
class OutputEuler2D_PtFlux : public OutputType< OutputEuler2D_PtFlux<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef OutputCategory::Functional Category;
  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_PtFlux( const PDENDConvert& pde, const Real p0Ref = 0) :
    pde_(pde),
    p0Ref_(p0Ref)
  {
    // N
  }

  bool needsSolutionGradient() const { return false; }

public:
  // Area output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("PtFlux output needs nx, ny");
  }

  // Boundary output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    Tq rho = 0, u = 0, v = 0, t=0;

    pde_.variableInterpreter().eval( q, rho, u, v, t );
    Tq p = pde_.gasModel().pressure(rho, t);

    Tq c = pde_.gasModel().speedofSound(t);
    Tq M2 = (u*u + v*v) / (c*c);
    Tq gamma = pde_.gasModel().gamma();

    // Total pressure (stagnation pressure)
    Tq p0 = p*pow(1+(gamma-1)/2*M2, gamma/(gamma-1));

    Tq vn = u*nx + v*ny;

    output = (p0 - p0Ref_)*rho*vn;
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("PtFlux output needs nx, ny");
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, nx, ny, q, qx, qy, output);
  }

private:
  const PDENDConvert& pde_;
  const Real p0Ref_;   //Reference stagnation pressure for Error
};

template <class PDENDConvert>
class OutputEuler2D_MassFlux : public OutputType< OutputEuler2D_MassFlux<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;

  typedef OutputCategory::Functional Category;
  typedef typename PDENDConvert::QInterpret QInterpret;     // solution variable interpreter

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputEuler2D_MassFlux( const PDENDConvert& pde, const Real p0Ref = 0) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

public:
  // Area output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("Mass Flux output needs nx, ny");
  }

  // Boundary output
  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    Tq rho = 0, u = 0, v = 0, t=0;
    pde_.variableInterpreter().eval( q, rho, u, v, t );

    Tq vn = u*nx + v*ny;

    output = rho*vn;
  }


  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("Mass Flux output needs nx, ny");
  }

  template<class Tp, class Tq, class Tg, class To>
  void operator()(const Tp& param, const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& output ) const
  {
    operator()(x, y, time, nx, ny, q, qx, qy, output);
  }

private:
  const PDENDConvert& pde_;
};



}

#endif //OUTPUTEULER2D_H
