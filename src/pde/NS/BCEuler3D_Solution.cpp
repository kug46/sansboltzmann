// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "TraitsEuler.h"
#include "TraitsEulerArtificialViscosity.h"
#include "TraitsNavierStokes.h"
#include "TraitsRANSSA.h"

#include "BCEuler3D_Solution.h"
#include "BCRANSSA3D_Solution.h"

#include "Q3DPrimitiveRhoPressure.h"
#include "Q3DConservative.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{
// Instantiate the paramter options
PYDICT_PARAMETER_OPTION_INSTANTIATE(BCEulerSolution3DOptions::SolutionOptions)

template <template <class> class PDETraitsSize, class PDETraitsModel>
typename BCEulerSolution3D<PDETraitsSize, PDETraitsModel>::Function_ptr
BCEulerSolution3D<PDETraitsSize, PDETraitsModel>::getSolution(const PyDict& d)
{
  BCEulerSolution3DParams<PDETraitsSize> params;

  DictKeyPair SolutionParam = d.get(params.Function);

  if ( SolutionParam == params.Function.TaylorGreen )
    return Function_ptr( static_cast<Function3DBaseType*>(
                         new SolutionFunction_NavierStokes3D_TaylorGreen<PDETraitsSize, PDETraitsModel>( SolutionParam ) ) );
  else if ( SolutionParam == params.Function.OjedaMMS )
    return Function_ptr( static_cast<Function3DBaseType*>(
                         new SolutionFunction_NavierStokes3D_OjedaMMS<PDETraitsSize, PDETraitsModel>( SolutionParam ) ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized solution function specified");
  return NULL; // suppress compiler warnings
}

template struct BCEulerSolution3D< TraitsSizeEuler, TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel> >;
template struct BCEulerSolution3D< TraitsSizeEuler, TraitsModelEuler<QTypeConservative, GasModel> >;

template struct BCEulerSolution3D< TraitsSizeNavierStokes,
                                   TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution3D< TraitsSizeNavierStokes,
                                   TraitsModelNavierStokes<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;

template struct BCEulerSolution3D< TraitsSizeNavierStokes,
                                   TraitsModelNavierStokes<QTypePrimitiveRhoPressure, GasModel,
                                   ViscosityModel_Sutherland, ThermalConductivityModel> >;

template <class PDETraitsModel>
typename BCEulerSolution3D<TraitsSizeRANSSA, PDETraitsModel>::Function_ptr
BCEulerSolution3D<TraitsSizeRANSSA, PDETraitsModel>::getSolution(const PyDict& d)
{
  BCEulerSolution3DParams<TraitsSizeRANSSA> params;

  DictKeyPair SolutionParam = d.get(params.Function);

  if ( SolutionParam == params.Function.OjedaRANSMMS )
    return Function_ptr( static_cast<Function3DBaseType*>(
                         new SolutionFunction_RANSSA3D_OjedaMMS<TraitsSizeRANSSA, PDETraitsModel>( SolutionParam ) ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION("Unrecognized solution function specified");
  return NULL; // suppress compiler warnings
}

template struct BCEulerSolution3D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution3D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Const, ThermalConductivityModel> >;
template struct BCEulerSolution3D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypePrimitiveRhoPressure, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;
template struct BCEulerSolution3D< TraitsSizeRANSSA,
                                   TraitsModelRANSSA<QTypeConservative, GasModel, ViscosityModel_Sutherland, ThermalConductivityModel> >;

} //namespace SANS
