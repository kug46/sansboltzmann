// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef NSVARIALBE3D_H
#define NSVARIALBE3D_H

#include "Python/PyDict.h" // python must be included first
#include "Python/Parameter.h"

#include <initializer_list>

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

namespace SANS
{

//===========================================================================//
template<class Derived>
struct NSVariableType3D
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//===========================================================================//
struct DensityVelocityPressure3DParams : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Velocity-X"};
  const ParameterNumeric<Real> v{"v", NO_DEFAULT, NO_RANGE, "Velocity-Y"};
  const ParameterNumeric<Real> w{"w", NO_DEFAULT, NO_RANGE, "Velocity-Z"};
  const ParameterNumeric<Real> p{"p", NO_DEFAULT, 0, NO_LIMIT, "Pressure"};

  static void checkInputs(PyDict d);

  static DensityVelocityPressure3DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct DensityVelocityPressure3D : public NSVariableType3D<DensityVelocityPressure3D<T>>
{
  typedef DensityVelocityPressure3DParams ParamsType;

  DensityVelocityPressure3D() {}

  explicit DensityVelocityPressure3D( const PyDict& d )
    : Density( d.get(DensityVelocityPressure3DParams::params.rho) ),
      VelocityX( d.get(DensityVelocityPressure3DParams::params.u) ),
      VelocityY( d.get(DensityVelocityPressure3DParams::params.v) ),
      VelocityZ( d.get(DensityVelocityPressure3DParams::params.w) ),
      Pressure( d.get(DensityVelocityPressure3DParams::params.p) ) {}

  DensityVelocityPressure3D( const T& rho, const T& u, const T& v, const T& w, const T& p )
    : Density(rho), VelocityX(u), VelocityY(v), VelocityZ(w), Pressure(p) {}

  DensityVelocityPressure3D& operator=( const std::initializer_list<Real>& s )
  {
    SANS_ASSERT(s.size() == 5);
    auto q = s.begin();
    Density = *q; q++;
    VelocityX = *q; q++;
    VelocityY = *q; q++;
    VelocityZ = *q; q++;
    Pressure = *q;

    return *this;
  }

  T Density;
  T VelocityX;
  T VelocityY;
  T VelocityZ;
  T Pressure;
};

//===========================================================================//
struct DensityVelocityTemperature3DParams : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> u{"u", NO_DEFAULT, NO_RANGE, "Velocity-X"};
  const ParameterNumeric<Real> v{"v", NO_DEFAULT, NO_RANGE, "Velocity-Y"};
  const ParameterNumeric<Real> w{"w", NO_DEFAULT, NO_RANGE, "Velocity-Z"};
  const ParameterNumeric<Real> t{"t", NO_DEFAULT, 0, NO_LIMIT, "Temperature"};

  static void checkInputs(PyDict d);

  static DensityVelocityTemperature3DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct DensityVelocityTemperature3D : public NSVariableType3D<DensityVelocityTemperature3D<T>>
{
  typedef DensityVelocityTemperature3DParams ParamsType;

  DensityVelocityTemperature3D() {}

  explicit DensityVelocityTemperature3D( const PyDict& d )
    : Density( d.get(DensityVelocityTemperature3DParams::params.rho) ),
      VelocityX( d.get(DensityVelocityTemperature3DParams::params.u) ),
      VelocityY( d.get(DensityVelocityTemperature3DParams::params.v) ),
      VelocityZ( d.get(DensityVelocityTemperature3DParams::params.w) ),
      Temperature( d.get(DensityVelocityTemperature3DParams::params.t) ) {}

  DensityVelocityTemperature3D( const T& rho, const T& u, const T& v, const T& w, const T& t )
    : Density(rho), VelocityX(u), VelocityY(v), VelocityZ(w), Temperature(t) {}

  DensityVelocityTemperature3D& operator=(const std::initializer_list<Real>& s)
  {
    SANS_ASSERT(s.size() == 5);
    auto q = s.begin();
    Density = *q; q++;
    VelocityX = *q; q++;
    VelocityY = *q; q++;
    VelocityZ = *q; q++;
    Temperature = *q;

    return *this;
  }

  T Density;
  T VelocityX;
  T VelocityY;
  T VelocityZ;
  T Temperature;
};

//===========================================================================//
struct Conservative3DParams : noncopyable
{
  const ParameterNumeric<Real> rho{"rho", NO_DEFAULT, 0, NO_LIMIT, "Density"};
  const ParameterNumeric<Real> rhou{"rhou", NO_DEFAULT, NO_RANGE, "Momentum-X"};
  const ParameterNumeric<Real> rhov{"rhov", NO_DEFAULT, NO_RANGE, "Momentum-Y"};
  const ParameterNumeric<Real> rhow{"rhow", NO_DEFAULT, NO_RANGE, "Momentum-Z"};
  const ParameterNumeric<Real> rhoE{"rhoE", NO_DEFAULT, 0, NO_LIMIT, "Total Energy"};

  static void checkInputs(PyDict d);

  static Conservative3DParams params;
};

//---------------------------------------------------------------------------//
template<class T>
struct Conservative3D : public NSVariableType3D<Conservative3D<T>>
{
  typedef Conservative3DParams ParamsType;

  Conservative3D() {}

  explicit Conservative3D( const PyDict& d )
    : Density( d.get(Conservative3DParams::params.rho) ),
      MomentumX( d.get(Conservative3DParams::params.rhou) ),
      MomentumY( d.get(Conservative3DParams::params.rhov) ),
      MomentumZ( d.get(Conservative3DParams::params.rhow) ),
      TotalEnergy( d.get(Conservative3DParams::params.rhoE) ) {}

  Conservative3D( const T& rho, const T& rhou, const T& rhov, const T& rhow, const T& rhoE )
    : Density(rho), MomentumX(rhou), MomentumY(rhov), MomentumZ(rhow), TotalEnergy(rhoE) {}
  Conservative3D& operator=(const std::initializer_list<Real>& s)
  {
    SANS_ASSERT(s.size() == 5);
    auto q = s.begin();
    Density = *q; q++;
    MomentumX = *q; q++;
    MomentumY = *q; q++;
    MomentumZ = *q; q++;
    TotalEnergy = *q;

    return *this;
  }

  T Density;
  T MomentumX;
  T MomentumY;
  T MomentumZ;
  T TotalEnergy;
};


//===========================================================================//
struct NSVariableType3DParams : noncopyable
{
  struct VariableOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString Variables{"Variables", NO_DEFAULT, "Name of the variable set" };
    const ParameterString& key = Variables;

    const DictOption Conservative{"Conservative", Conservative3DParams::checkInputs};
    const DictOption PrimitiveTemperature{"Primitive Temperature", DensityVelocityTemperature3DParams::checkInputs};
    const DictOption PrimitivePressure{"Primitive Pressure", DensityVelocityPressure3DParams::checkInputs};

    const std::vector<DictOption> options{Conservative,PrimitiveTemperature,PrimitivePressure};
  };
  const ParameterOption<VariableOptions> StateVector{"StateVector", NO_DEFAULT, "The state vector of variables"};

  static void checkInputs(PyDict d);

  static NSVariableType3DParams params;
};


}

#endif //NSVARIALBE3D_H
