// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_BOLTZMANND2Q9_H
#define SOLUTIONFUNCTION_BOLTZMANND2Q9_H

// quasi 1-D Euler PDE: exact and MMS solutions

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // exp

#include "tools/SANSnumerics.h"     // Real

#include "PDEBoltzmannD2Q9.h"
#include "pde/AnalyticFunction/Function2D.h"
#include "pde/AnalyticFunction/ScalarFunction2D.h"

#include "TraitsBoltzmannD2Q9.h"

#include "getBoltzmannD2Q9TraitsModel.h"
//#include "LinearAlgebra/AlgebraicEquationSetBase.h"
//#include "LinearAlgebra/AlgebraicEquationSet_Traits.h"

namespace SANS
{

// forward declare: solution variables interpreter
template <class QType, template <class> class PDETraitsSize>
class QD2Q9;

//----------------------------------------------------------------------------//
// solution: Gaussian

struct SolutionFunction_BoltzmannD2Q9_Gaussian_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Amplitude of Gaussian"};
  const ParameterNumeric<Real> sigma_x{"sigma_x", NO_DEFAULT, NO_RANGE, "Spread in x"};
  const ParameterNumeric<Real> sigma_y{"sigma_y", NO_DEFAULT, NO_RANGE, "Spread in y"};

  static void checkInputs(PyDict d);

  static SolutionFunction_BoltzmannD2Q9_Gaussian_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_BoltzmannD2Q9_Gaussian :
    public Function2DVirtualInterface<SolutionFunction_BoltzmannD2Q9_Gaussian<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_BoltzmannD2Q9_Gaussian_Params ParamsType;

  typedef typename getBoltzmannD2Q9TraitsModel<PDETraitsModel>::type TraitsModel;

  //typedef PDETraitsModel TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef QD2Q9<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_BoltzmannD2Q9_Gaussian( const PyDict& d ) :
    qInterpret_(GasModel( d.get(ParamsType::params.gasModel) )),
    A_(d.get(ParamsType::params.A)),
    sigma_x_(d.get(ParamsType::params.sigma_x)),
    sigma_y_(d.get(ParamsType::params.sigma_y))
  {
    // Nothing
  }

  explicit SolutionFunction_BoltzmannD2Q9_Gaussian( const GasModel& gas, const Real& A, const Real& sigma_x, const Real& sigma_y  ) :
    qInterpret_(gas),
    A_(A),
    sigma_x_(sigma_x),
    sigma_y_(sigma_y)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> q = 0;
    T val = A_*exp(-0.5*((x*x)/(sigma_x_*sigma_x_) + (y*y)/(sigma_y_*sigma_x_)));
    const Real w0 = 16./36.;
    const Real w1 =  4./36.;
    const Real w2 =  4./36.;
    const Real w3 =  4./36.;
    const Real w4 =  4./36.;
    const Real w5 =  1./36.;
    const Real w6 =  1./36.;
    const Real w7 =  1./36.;
    const Real w8 =  1./36.;

    PrimitiveDistributionFunctionsD2Q9<T> dvp( w0*val, w1*val, w2*val,
                                               w3*val, w4*val, w5*val,
                                               w6*val, w7*val, w8*val);

    qInterpret_.setFromPrimitive(q, dvp);
    return q;
  }

private:
  const QInterpret qInterpret_;
  const Real A_;
  const Real sigma_x_;
  const Real sigma_y_;
};

//----------------------------------------------------------------------------//
// solution: WeightedDensity

struct SolutionFunction_BoltzmannD2Q9_WeightedDensity_Params : noncopyable
{
  const ParameterDict gasModel{"Gas Model", EMPTY_DICT, GasModelParams::checkInputs, "Gas Model Dictionary"};

  const ParameterNumeric<Real> Rho{"Rho", NO_DEFAULT, NO_RANGE, "Density"};

  static void checkInputs(PyDict d);

  static SolutionFunction_BoltzmannD2Q9_WeightedDensity_Params params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
class SolutionFunction_BoltzmannD2Q9_WeightedDensity :
    public Function2DVirtualInterface<SolutionFunction_BoltzmannD2Q9_WeightedDensity<PDETraitsSize, PDETraitsModel>, PDETraitsSize<PhysD2>>
{
public:
  typedef PhysD2 PhysDim;

  typedef SolutionFunction_BoltzmannD2Q9_WeightedDensity_Params ParamsType;

  typedef typename getBoltzmannD2Q9TraitsModel<PDETraitsModel>::type TraitsModel;

  //typedef PDETraitsModel TraitsModel;

  typedef typename TraitsModel::QType QType;
  typedef QD2Q9<QType, PDETraitsSize> QInterpret;             // solution variable interpreter

  typedef typename TraitsModel::GasModel GasModel;       // gas model

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  explicit SolutionFunction_BoltzmannD2Q9_WeightedDensity( const PyDict& d ) :
    qInterpret_(GasModel( d.get(ParamsType::params.gasModel) )),
    Rho_(d.get(ParamsType::params.Rho))
  {
    // Nothing
  }

  explicit SolutionFunction_BoltzmannD2Q9_WeightedDensity( const GasModel& gas, const Real& Rho  ) :
    qInterpret_(gas),
    Rho_(Rho)
  {
    // Nothing
  }

  template<class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    ArrayQ<T> q = 0;
    T val = Rho_;
    const Real w0 = 16./36.;
    const Real w1 =  4./36.;
    const Real w2 =  4./36.;
    const Real w3 =  4./36.;
    const Real w4 =  4./36.;
    const Real w5 =  1./36.;
    const Real w6 =  1./36.;
    const Real w7 =  1./36.;
    const Real w8 =  1./36.;

    PrimitiveDistributionFunctionsD2Q9<T> dvp( w0*val, w1*val, w2*val,
                                               w3*val, w4*val, w5*val,
                                               w6*val, w7*val, w8*val);

    qInterpret_.setFromPrimitive(q, dvp);
    return q;
  }

private:
  const QInterpret qInterpret_;
  const Real Rho_;
};



} // namespace SANS

#endif  // SOLUTIONFUNCTION_BOLTZMANND2Q9_H
