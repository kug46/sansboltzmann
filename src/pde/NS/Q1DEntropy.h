// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef Q1DENTROPY_H
#define Q1DENTROPY_H

// solution interpreter class
// Euler/N-S conservation-eqns: entropy variables

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "pde/NS/NSVariable1D.h"

#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// solution variable interpreter: 1-D Euler/N-S
// See: "A new finite element formulation for computational fluid dynamics: I.
//       Symmetric forms of the compressible Euler and Navier-Stokes equations
//       and the second law of thermodynamics" - T.J.R. Hughes, L.P. Franca,
//       M. Mallet
// Specifically equations 48-53
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   QType                solution variable set (e.g. primitive)
//   PDETraitsSize        define PDE size-related features
//     N, D               PDE size, physical dimensions
//     ArrayQ             solution/residual arrays
//
// member functions:
//   .eval                extract primitive variables (rho, u, v, t)
//   .evalDensity         extract density (rho)
//   .evalVelocity        extract velocity components (u, v)
//   .evalTemperature     extract temperature (t)
//   .evalGradient        extract primitive variables gradient
//   .isValidState        T/F: determine if state is physically valid (e.g. rho > 0)
//   .setfromPrimitive    set from primitive variable array
//
//   .dump                debug dump of private data
//----------------------------------------------------------------------------//


// primitive variables (rho, u, v, p)
class QTypeEntropy;


// primary template
template <class QType, template <class> class PDETraitsSize>
class Q1D;


template <template <class> class PDETraitsSize>
class Q1D<QTypeEntropy, PDETraitsSize>
{
public:
  typedef PhysD1 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  // TODO: What are the names??
  static std::vector<std::string> stateNames() { return {"q0", "q1", "q2"}; }

  // The components of the state vector that make up the vector
  static const int ix = 1;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution arrays

  explicit Q1D( const GasModel& gas0 ) : gas_(gas0) {}
  Q1D( const Q1D& q0 ) : gas_(q0.gas_) {}
  ~Q1D() {}

  Q1D& operator=( const Q1D& );

  // evaluate primitive variables
  template<class T>
  void eval( const ArrayQ<T>& q, T& rho, T& u, T& t ) const;
  template<class T>
  void evalDensity( const ArrayQ<T>& q, T& rho ) const;
  template<class T>
  void evalVelocity( const ArrayQ<T>& q, T& u ) const;
  template<class T>
  void evalTemperature( const ArrayQ<T>& q, T& t ) const;

  template<class T>
  void evalGradient(
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    T& rhox,
    T&   ux,
    T&   tx ) const;

  template<class T>
  void evalSecondGradient(
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
      T& rhoxx, T& uxx, T& txx ) const;

  template<class T>
  void evalJacobian( const ArrayQ<T>& q, ArrayQ<T>& rho_q, ArrayQ<T>& u_q, ArrayQ<T>& t_q ) const;

  // update fraction needed for physically valid state
  void updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from primitive variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityPressure1D<T>& data ) const;

  // set from primitive variable
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const DensityVelocityTemperature1D<T>& data ) const;

  // set from variables
  template<class T>
  void setFromPrimitive(
      ArrayQ<T>& q, const Conservative1D<T>& data ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const GasModel gas_;
};


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::eval(
    const ArrayQ<T>& q, T& rho, T& u, T& t ) const
{
  T rhoe, s;

  Real gamma = gas_.gamma();
  Real gm1 = gamma - 1.0;
  s = gamma - q(0) + 0.5*(q(1)*q(1))/q(2);
  rhoe = pow(gm1/pow(-q(2), gamma), 1.0/gm1) * exp(-s/gm1);

  rho = rhoe * -q(2);
  u   = rhoe *  q(1)/rho;
  t   = rhoe / (rho * gas_.Cv());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::evalDensity(
    const ArrayQ<T>& q, T& rho ) const
{
  T rhoe, s;
  Real gamma;

  gamma = gas_.gamma();
  s = gamma - q(0) + 0.5*(q(1)*q(1))/q(2);
  rhoe = pow((gamma-1.0)/pow(-q(2),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));

  rho = rhoe * -q(2);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::evalVelocity(
    const ArrayQ<T>& q, T& u ) const
{
  T rhoe, s, rho;
  Real gamma;

  gamma = gas_.gamma();
  s = gamma - q(0) + 0.5*(q(1)*q(1))/q(2);
  rhoe = pow((gamma-1.0)/pow(-q(2),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));

  rho = rhoe * -q(2);
  u   = rhoe *  q(1)/rho;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::evalTemperature(
    const ArrayQ<T>& q, T& t ) const
{
  T rhoe, s, rho;
  Real gamma;

  gamma = gas_.gamma();
  s = gamma - q(0) + 0.5*(q(1)*q(1))/q(2);
  rhoe = pow((gamma-1.0)/pow(-q(2),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));

  rho = rhoe * -q(2);
  t   = rhoe / (rho * gas_.Cv());
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::evalJacobian(
    const ArrayQ<T>& q, ArrayQ<T>& rho_q, ArrayQ<T>& u_q, ArrayQ<T>& t_q ) const
{
  rho_q = 0; u_q = 0; t_q = 0;

  Real gamma = gas_.gamma();
  Real gm1 = gamma - 1.0;
  T s = gamma - q(0) + 0.5*(q(1)*q(1))/q(2);
  T rhoe = pow(gm1/pow(-q(2), gamma), 1.0/gm1) * exp(-s/gm1);

  // s   = gamma - q(0) + 0.5*(q(1)*q(1))/q(2);
  T s_q0 = -1;
  T s_q1 = (q(1))/q(2);
  T s_q2 = -(q(1)*q(1))/(q(2)*q(2));

  // rhoe   = pow(gm1/pow(-q(2), gamma), 1.0/gm1) * exp(-s/gm1);
  T rhoe_q0 = rhoe*(-s_q0/gm1);
  T rhoe_q1 = rhoe*(-s_q1/gm1);
  //T rhoe_q2 = rhoe*(-s_q2/gm1) - (pow(gm1/pow(-q(2),gamma),1/gm1)*gamma)/(q(2)*gm1) * exp(-s/gm1);
  T rhoe_q2 = rhoe * gamma / gm1 / (-q(2)) - rhoe / gm1 * s_q2;

  T rho = rhoe * -q(2);

  // rho   = rhoe * -q(2);
  rho_q[0] = rhoe_q0 * -q(2);        // drho/dq0
  rho_q[1] = rhoe_q1 * -q(2);        // drho/dq1
  rho_q[2] = rhoe_q2 * -q(2) - rhoe; // drho/dq2

  // u   = rhoe *  q(1)/rho;
  u_q[0] = rhoe_q0 * q(1)/rho - rhoe * q(1)*rho_q[0]/(rho*rho);            // du/dq0
  u_q[1] = rhoe_q1 * q(1)/rho - rhoe * q(1)*rho_q[1]/(rho*rho) + rhoe/rho; // du/dq1
  u_q[2] = rhoe_q2 * q(1)/rho - rhoe * q(1)*rho_q[2]/(rho*rho);            // du/dq2

  // t   = rhoe / (rho * gas_.Cv());
  t_q[0] = rhoe_q0 / (rho * gas_.Cv()) - rhoe*rho_q[0] / (rho*rho* gas_.Cv()); // dt/dq0
  t_q[1] = rhoe_q1 / (rho * gas_.Cv()) - rhoe*rho_q[1] / (rho*rho* gas_.Cv()); // dt/dq1
  t_q[2] = rhoe_q2 / (rho * gas_.Cv()) - rhoe*rho_q[2] / (rho*rho* gas_.Cv()); // dt/dq2
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::evalGradient(
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    T& rhox,
    T&   ux,
    T&   tx ) const
{
  T rho;
  T rhoe, s, gamma;
  T rhoex;
  T sx;

  gamma = gas_.gamma();
  s = gamma - q(0) + 0.5*(q(1)*q(1))/q(2);
  rhoe = pow((gamma-1.0)/pow(-q(2),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));

  rho = rhoe * -q(2);

  // s = gamma - q(0) + 0.5*(q(1)*q(1))/q(2);
  sx = -qx(0) + (q(1)*qx(1)) / q(2) + 0.5*(q(1)*q(1))/q(2) * (-qx(2)/q(2));

  // rhoe = pow((gamma-1.0)/pow(-q(2),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));
  rhoex = rhoe * gamma / (gamma-1.0) / (-q(2)) * qx(2) - rhoe / (gamma-1.0) * sx;

  // rho = rhoe * -q(2);
  rhox = rhoe*-qx(2) + rhoex * -q(2);

  // u = rhoe * q(1)/rho;
  ux   = rhoex *q(1)/rho + rhoe * qx(1)/rho + rhoe * q(1)/rho * (-rhox/rho);

  // t = rhoe / (rho * gas_.Cv());
  tx = rhoex / (rho * gas_.Cv()) + rhoe / (rho * gas_.Cv()) * (-rhox/rho);
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::evalSecondGradient(
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qxx,
    T& rhoxx, T& uxx, T& txx ) const
{
  SANS_DEVELOPER_EXCEPTION("Second derivatives for Q1DEntropy not yet implemented");
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn == 3);
  SANS_ASSERT((name[0] == "Density") &&
              (name[1] == "VelocityX") &&
              (name[2] == "Temperature"));

  T rho  = data[0];
  T u    = data[1];
  T t    = data[2];

  T rhoe = rho*gas_.energy(rho, t);
  T rhoE = rho*(gas_.energy(rho, t) + 0.5*(u*u));
  T gamma = gas_.gamma();
  T s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) = -rho/rhoe;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityPressure1D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u = data.VelocityX;
  const T& p = data.Pressure;
  T t = p / (gas_.R()*rho);

  T rhoe = rho*gas_.energy(rho, t);
  T rhoE = rho*(gas_.energy(rho, t) + 0.5*(u*u));
  T gamma = gas_.gamma();
  T s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) = -rho/rhoe;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const DensityVelocityTemperature1D<T>& data ) const
{
  const T& rho = data.Density;
  const T& u   = data.VelocityX;
  const T& t   = data.Temperature;

  T rhoe = rho*gas_.energy(rho, t);
  T rhoE = rho*(gas_.energy(rho, t) + 0.5*(u*u));
  T gamma = gas_.gamma();
  T s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rho*u/rhoe;
  q(2) = -rho/rhoe;
}


template <template <class> class PDETraitsSize>
template <class T>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::setFromPrimitive(
    ArrayQ<T>& q, const Conservative1D<T>& data ) const
{
  const T& rho = data.Density;
  const T& rhou = data.MomentumX;
  const T& rhoE = data.TotalEnergy;

  T rhoe = rhoE - 0.5*(rhou*rhou)/rho;
  Real gamma = gas_.gamma();
  T s = log((gamma-1.0)*rhoe / pow(rho,gamma));

  q(0) = -rhoE/rhoe + gamma + 1 - s;
  q(1) =  rhou/rhoe;
  q(2) = -rho/rhoe;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
Q1D<QTypeEntropy, PDETraitsSize>::
updateFraction( const ArrayQ<Real>& q, const ArrayQ<Real>& dq, const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("Not implemented");
}


// is state physically valid: check for positive density, temperature
template <template <class> class PDETraitsSize>
inline bool
Q1D<QTypeEntropy, PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  Real rhoe, s, gamma, rho, t;

  if (q(2) >= 0) return false;

  gamma = gas_.gamma();
  s = gamma - q(0) + 0.5*(q(1)*q(1))/q(2);
  rhoe = pow((gamma-1.0)/pow(-q(2),gamma),1.0/(gamma-1.0)) * exp(-s/(gamma-1.0));

  rho = rhoe * -q(2);

  if (rho <= 0) return false;

  t   = rhoe / (rho * gas_.Cv());

  if (t <= 0) return false;

  return true;
}


template <template <class> class PDETraitsSize>
void
Q1D<QTypeEntropy, PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Q1D<QTypeEntropy, PDETraitsSize>: N = " << N << std::endl;
  out << indent << "Q1D<QTypeEntropy, PDETraitsSize>: gas = " << std::endl;
  gas_.dump(indentSize+2, out);
}

}

#endif  // Q1DENTROPY_H
