// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER2D_BN_H
#define BCEULER2D_BN_H

// 2-D Euler BC class with Berg-Nordstrom formulations

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsEuler.h"
#include "PDEEuler2D.h"
#include "BCEuler2D_Solution.h"
#include "pde/NS/SolutionFunction_Euler2D.h"
#include "pde/NS/SolutionFunction_NavierStokes2D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>
#include <memory> //shared_ptr

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D Euler
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeInflowSubsonic_sHqt_BN;
class BCTypeInflowSubsonic_PtHa_BN;
class BCTypeOutflowSubsonic_Pressure_BN;
class BCTypeFunctionInflow_sHqt_BN;
class BCTypeFunctionInflow_PtHa_BN;

template <class BCType, class PDEEuler2D>
class BCEuler2D;

template <class BCType>
struct BCEuler2DParams;

//----------------------------------------------------------------------------//
// Berg Nordstrom Style subsonic inflow BC:
//
// specify: entropy, Stagnation enthalpy, vertical velocity
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN> : noncopyable
{
  const ParameterNumeric<Real> sSpec{"sSpec", NO_DEFAULT, NO_RANGE, "Entropy"};
  const ParameterNumeric<Real> HSpec{"HSpec", NO_DEFAULT, 0, NO_LIMIT, "Stagnation Enthalpy"};
  const ParameterNumeric<Real> aSpec{"aSpec", NO_DEFAULT, NO_RANGE, "Angle of attack = arctan(v/u)"};

  static constexpr const char* BCName{"InflowSubsonic_sHqt_BN"};
  struct Option
  {
    const DictOption InflowSubsonic_sHqt_BN{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE_> :
    public BCType< BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeInflowSubsonic_sHqt_BN BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCTypeInflowSubsonic_sHqt_BN> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Real& sSpec, const Real& HSpec, const Real& aSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      sSpec_(sSpec), HSpec_(HSpec), aSpec_(aSpec) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    sSpec_(d.get(ParamsType::params.sSpec)),
    HSpec_(d.get(ParamsType::params.HSpec)),
    aSpec_(d.get(ParamsType::params.aSpec)) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real sSpec_;
  const Real HSpec_;
  const Real aSpec_;
};



template <class PDE>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}


template <class PDE>
template <class T, class TL, class TR>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  T rho=0, u=0, v=0, t=0;

  //compute BN Flux
  qInterpret_.eval( qI, rho, u, v, t );
  T p = gas_.pressure(rho,t);

  T H = gas_.enthalpy(rho, t) + 0.5*(u*u+v*v);
  T c = gas_.speedofSound(t);
  T s = log( p /pow(rho,gamma) );

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // normal and tangential velocities
  T vn = u*nx + v*ny;
  T vt = u*tx + v*ty;
  T V2 = u*u+v*v;
  T V3 = sqrt(V2)*V2;
  T c2 = c*c;

  // Specified tangential velocity component
//  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

  // Strong BC's that are imposed
  T ds  = s  - sSpec_;
  T dH  = H  - HSpec_;
  T dvt = v/sqrt(u*u+v*v) - sin(aSpec_);

  //COMPUTE NORMAL FLUX: DERIVED FROM ENTROPY VARS
//  Fn[pde_.iCont] -= -rho*vn/gmi*ds;
//  Fn[pde_.iCont] -= vn*gamma*rho/(c*c + V2*gmi)*dH;
//  Fn[pde_.iCont] -=  -vt*V2*V*rho*gamma/u/(c*c+V2*gmi)*dvt;
//
//  Fn[pde_.ixMom] -= -rho*(c*c*nx + u*vn*gamma)/(gamma*gmi)*ds;
//  Fn[pde_.ixMom] -= rho*(c*c*nx + u*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
//  Fn[pde_.ixMom] -= -(rho*V2*V*(c*c*v + vn*(v*vn*gmi+u*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;
//
//  Fn[pde_.iyMom] -=  -rho*(c*c*ny+v*vn*gamma*rho)/(gamma*gmi)*ds;
//  Fn[pde_.iyMom] -= rho*(c*c*ny + v*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
//  Fn[pde_.iyMom] -= (rho*V2*V*(c*c*u + vn*(u*vn*gmi-v*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;
//
//  Fn[pde_.iEngy] -= -rho*vn*( 2*c*c + V2*gmi)/(2*gmi*gmi)*ds;
//  Fn[pde_.iEngy] -= (rho*vn*(c*c*(4*gamma-2) + V2*(2-5*gamma+3*gamma*gamma)))/(2*gmi*(c*c + V2*gmi) )*dH;
//  Fn[pde_.iEngy] -=  -(vt*V2*V*rho*gamma*(2*c*c+V2*gmi))/(2*u*gmi*(c*c+V2*gmi))*dvt;

  //NEW VERSION
  Fn[pde_.iCont] -= -rho*vn/gmi*ds;
  Fn[pde_.iCont] -= rho*vn/c2*dH;
  Fn[pde_.iCont] -=  -vt*V3*rho/(c*c*u)*dvt;

  Fn[pde_.ixMom] -= -rho*(c2*nx + u*vn*gamma)/(gamma*gmi)*ds;
  Fn[pde_.ixMom] -= rho*(nx + u*vn/c2)*dH;
  Fn[pde_.ixMom] -= -rho*(c2*v + u*vn*vt)*V3/(c2*u*vn)*dvt;

  Fn[pde_.iyMom] -=  -rho*(c2*ny + v*vn*gamma)/(gamma*gmi)*ds;
  Fn[pde_.iyMom] -= rho*(ny + v*vn/c2)*dH;
  Fn[pde_.iyMom] -= rho*(c2*u - v*vn*vt)*V3/(c2*u*vn)*dvt;

  Fn[pde_.iEngy] -= -rho*vn*H/gmi*ds;
  Fn[pde_.iEngy] -= rho*vn*(0.5*V2/c2 + gamma/gmi)*dH;
  Fn[pde_.iEngy] -=  -(vt*V3*rho*H)/(c2*u)*dvt;

}


// normal BC flux
template <class PDE>
template <class T, class L, class R, class Tf>
inline void
BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>::
fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
}

template <class PDE>
bool
BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>::isValidState(
    const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const
{
  Real rho=0, u=0, v=0, t=0;
  qInterpret_.eval( q, rho, u, v, t );

  // Check that the normal velocity is negative (inflow)
  return (u*nx + v*ny) < 0;
//  return true;
}


template <class PDE>
void
BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}


//----------------------------------------------------------------------------//
// Berg Nordstrom Style subsonic inflow BC:
//
// specify: stagnation pressure, stagnation enthalpy, vertical velocity component
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeInflowSubsonic_PtHa_BN> : noncopyable
{
  const ParameterNumeric<Real> ptSpec{"ptSpec", NO_DEFAULT, NO_RANGE, "Stagnation Pressure"};
  const ParameterNumeric<Real> HSpec{"HSpec", NO_DEFAULT, 0, NO_LIMIT, "Stagnation Enthalpy"};
  const ParameterNumeric<Real> aSpec{"aSpec", NO_DEFAULT, NO_RANGE, "Angle of attack = arctan(v/u)"};

  static constexpr const char* BCName{"BCTypeInflowSubsonic_PtHa_BN"};
  struct Option
  {
    const DictOption InflowSubsonic_PtHa_BN{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeInflowSubsonic_PtHa_BN, PDE_> :
    public BCType< BCEuler2D<BCTypeInflowSubsonic_PtHa_BN, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeInflowSubsonic_PtHa_BN BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCTypeInflowSubsonic_PtHa_BN> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Real& ptSpec, const Real& HSpec, const Real& aSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      ptSpec_(ptSpec), HSpec_(HSpec), aSpec_(aSpec) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    ptSpec_(d.get(ParamsType::params.ptSpec)),
    HSpec_(d.get(ParamsType::params.HSpec)),
    aSpec_(d.get(ParamsType::params.aSpec)) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real ptSpec_;
  const Real HSpec_;
  const Real aSpec_;
};



template <class PDE>
template <class T>
inline void
BCEuler2D<BCTypeInflowSubsonic_PtHa_BN, PDE>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}


template <class PDE>
template <class T, class TL, class TR>
inline void
BCEuler2D<BCTypeInflowSubsonic_PtHa_BN, PDE>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeInflowSubsonic_PtHa_BN, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1.0;

  T rho=0, u=0, v=0, t=0;

  //compute BN Flux
  qInterpret_.eval( qI, rho, u, v, t );
  T p = gas_.pressure(rho,t);

  T H = gas_.enthalpy(rho, t) + 0.5*(u*u+v*v);
  T c = gas_.speedofSound(t);
  T c2 = c*c;
//  T s = log( p /pow(rho,gamma) );

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // normal and tangential velocities
  T vn = u*nx + v*ny;
  T vt = u*tx + v*ty;
  T V2 = u*u+v*v;
  T V = sqrt(V2);

  T M = V/c;
  T M2 = M*M;
  T Gamma = pow(1 + 0.5*gmi*M2,gamma/gmi);
  T pt = p*Gamma;

  // Specified tangential velocity component
//  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

  // Strong BC's that are imposed
  T dpt = pt  - ptSpec_;
  T dH  = H  - HSpec_;
  T dvt = v/sqrt(u*u+v*v) - sin(aSpec_);


  //COMPUTE NORMAL FLUX: DERIVED FROM ENTROPY VARS
  Fn[pde_.iCont] -= vn*Gamma*gamma/c2*dpt;
  Fn[pde_.iCont] -= -vn*M2*gamma*(gamma-1)*rho/(c2 + V2*gmi)/(2 + M2*gmi)*dH;
  Fn[pde_.iCont] -=  -vt*V*M2*rho*gamma/u/(1+M*M*gmi)*dvt;

  Fn[pde_.ixMom] -= Gamma*(nx + u*vn*gamma/c2)*dpt;
  Fn[pde_.ixMom] -= (u*vn - v*vt - u*vn*M2)*gmi*rho/c2/(2 + 3*M2*gmi + M2*M2*gmi*gmi)*dH;
  Fn[pde_.ixMom] -= -(rho*M2*V*(c2*v + vn*(v*vn*gmi+u*vt*gamma)))/(u*vn*(1+M2*gmi))*dvt;

  Fn[pde_.iyMom] -=  Gamma*(ny + v*vn*gamma/c2)*dpt;
  Fn[pde_.iyMom] -= (v*vn + u*vt - v*vn*M2)*gmi*rho/c2/(2 + 3*M2*gmi + M2*M2*gmi*gmi)*dH;
  Fn[pde_.iyMom] -= (rho*M2*V*(c2*u + vn*(u*vn*gmi-v*vt*gamma)))/(u*vn*(1+M2*gmi))*dvt;

  Fn[pde_.iEngy] -= 0.5*vn*(2 + M2*gmi)*Gamma*gamma/gmi*dpt;
  Fn[pde_.iEngy] -= 0.5*vn*(2 + M2*(gamma-2))*rho/(1+ M2*gmi)*dH;
  Fn[pde_.iEngy] -=  -(vt*M2*V*rho*gamma*(2*c*c+V2*gmi))/(2*u*gmi*(1+M2*gmi))*dvt;

}


// normal BC flux
template <class PDE>
template <class T, class L, class R, class Tf>
inline void
BCEuler2D<BCTypeInflowSubsonic_PtHa_BN, PDE>::
fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
}

template <class PDE>
bool
BCEuler2D<BCTypeInflowSubsonic_PtHa_BN, PDE>::isValidState(
    const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const
{
  Real rho=0, u=0, v=0, t=0;
  qInterpret_.eval( q, rho, u, v, t );

  // Check that the normal velocity is negative (inflow)
  return (u*nx + v*ny) < 0;
}


template <class PDE>
void
BCEuler2D<BCTypeInflowSubsonic_PtHa_BN, PDE>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent << "BCEuler2D<BCTypeInflowSubsonic_sHqt_BN, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// Berg-Nordstrom style subsonic outflow BC:
//
// specify: static pressure
//----------------------------------------------------------------------------//

template<>
struct BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN> : noncopyable
{
  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "PressureStatic"};

  static constexpr const char* BCName{"OutflowSubsonic_Pressure_BN"};
  struct Option
  {
    const DictOption OutflowSubsonic_Pressure_BN{BCEuler2DParams::BCName, BCEuler2DParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCEuler2DParams params;
};


template <class PDE_>
class BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE_> :
    public BCType< BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeOutflowSubsonic_Pressure_BN BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEuler2DParams<BCTypeOutflowSubsonic_Pressure_BN> ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Real pSpec ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
      pSpec_(pSpec) {}
  ~BCEuler2D() {}

  BCEuler2D( const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    pSpec_(d.get(ParamsType::params.pSpec) ) {}

  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC state
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const
  {
    Real rho=0, u=0, v=0, t=0;
    qInterpret_.eval( q, rho, u, v, t );

    // Check that the normal velocity is positive (outflow)
//    return (u*nx + v*ny) > 0;
    return true;
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Real pSpec_;
};


// BC state
template <class PDE>
template <class T>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
state( const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  qB = qI;
}


template <class PDE>
template <class T, class TL, class TR>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <class PDE>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  //BERG NORDSTROM BC
  Real gamma = gas_.gamma();

  T rho, u, v, t, p;
  qInterpret_.eval(qI, rho, u, v, t);
  p = gas_.pressure(rho,t);
  T vn = u*nx+v*ny;

  T tmp = ( (p-pSpec_) - rho*vn*vn/gamma*log(p/pSpec_) );

  Fn[pde_.ixMom] -= tmp*nx;
  Fn[pde_.iyMom] -= tmp*ny;
  Fn[pde_.iEngy] -= tmp*vn;
}


// normal BC flux
template <class PDE>
template <class T, class L, class R, class Tf>
inline void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
}


template <class PDE>
void
BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE>::
dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent <<
      "BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE>: qInterpret_ = " << std::endl;
  qInterpret_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE>: gas_ = " << std::endl;
  gas_.dump(indentSize+2, out);
  out << indent <<
      "BCEuler2D<BCTypeOutflowSubsonic_Pressure_BN, PDE>: pSpec_ = " << pSpec_ << std::endl;
}


//----------------------------------------------------------------------------//
// Subsonic Inflow BN with Solution Function
//----------------------------------------------------------------------------//
template<template <class> class PDETraitsSize>
struct BCEulerFunctionInflow_sHqt_BN2DParams : BCEulerSolution2DParams<PDETraitsSize>
{
  typedef BCEulerFunctionInflow_sHqt_BN2DParams This;

  static constexpr const char* BCName{"SolutionInflow_sHqt_BN"};
  struct Option
  {
    const DictOption SolutionInflow_sHqt_BN{This::BCName, This::checkInputs};
  };

  static void checkInputs(PyDict d);

  static This params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeFunctionInflow_sHqt_BN BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEulerFunctionInflow_sHqt_BN2DParams<PDETraitsSize> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Function_ptr& solution ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()), solution_(solution) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    solution_( BCEulerSolution2D<PDETraitsSize, PDETraitsModel>::getSolution(d) ) {}

  BCEuler2D( const BCEuler2D& ) = delete;
  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  bool useUpwind() const { return false; }

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const { qB = qI; }

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  // Computes the specified BC data - s, H, qt
  bool BCQuantities(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    Real& sSpec, Real& HSpec, Real& vNorm) const;

  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Function_ptr solution_;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
inline bool
BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDE_<PDETraitsSize,PDETraitsModel>>::
BCQuantities( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
              Real& sSpec, Real& HSpec, Real& vNorm ) const
{
  Real gamma = gas_.gamma();

  ArrayQ<Real> qBspec = 0;
  qBspec = (*solution_)(x,y,time);
  Real rhoSpec, uSpec, vSpec, tSpec, pSpec = 0;
  qInterpret_.eval( qBspec, rhoSpec, uSpec, vSpec, tSpec );
  pSpec = gas_.pressure(rhoSpec, tSpec);
  HSpec = gas_.enthalpy(rhoSpec, tSpec) + 0.5*(uSpec*uSpec + vSpec*vSpec);
  sSpec = log( pSpec / pow(rhoSpec, gamma) );

  vNorm = vSpec / sqrt(uSpec*uSpec + vSpec*vSpec);

  // the state is valid
  return true;
}

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class TL, class TR>
inline void
BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDE_<PDETraitsSize,PDETraitsModel>>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDE_<PDETraitsSize,PDETraitsModel>>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  T rho=0, u=0, v=0, t=0;

  Real sSpec, HSpec, vNorm;
  BCQuantities(x, y, time, nx, ny, sSpec, HSpec, vNorm);

  //compute BN Flux
  qInterpret_.eval( qI, rho, u, v, t );
  T p = gas_.pressure(rho,t);

  T H = gas_.enthalpy(rho, t) + 0.5*(u*u+v*v);
  T c = gas_.speedofSound(t);
  T s = log( p /pow(rho,gamma) );
  T c2 = c*c;

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // normal and tangential velocities
  T vn = u*nx + v*ny;
  T vt = u*tx + v*ty;
  T V2 = u*u+v*v;
  T V3 = sqrt(V2)*V2;

  // Specified tangential velocity component
//  const Real vtSpec = VxSpec_*tx + VySpec_*ty;

  // Strong BC's that are imposed
  T ds  = s  - sSpec;
  T dH  = H  - HSpec;
  T dvt = v/sqrt(u*u+v*v) - vNorm;

  //COMPUTE NORMAL FLUX: DERIVED FROM ENTROPY VARS
//  Fn[pde_.iCont] -= -rho*vn/gmi*ds;
//  Fn[pde_.iCont] -= vn*gamma*rho/(c*c + V2*gmi)*dH;
//  Fn[pde_.iCont] -=  -vt*V2*V*rho*gamma/u/(c*c+V2*gmi)*dvt;
//
//  Fn[pde_.ixMom] -= -rho*(c*c*nx + u*vn*gamma)/(gamma*gmi)*ds;
//  Fn[pde_.ixMom] -= rho*(c*c*nx + u*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
//  Fn[pde_.ixMom] -= -(rho*V2*V*(c*c*v + vn*(v*vn*gmi+u*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;
//
//  Fn[pde_.iyMom] -=  -rho*(c*c*ny+v*vn*gamma*rho)/(gamma*gmi)*ds;
//  Fn[pde_.iyMom] -= rho*(c*c*ny + v*vn*(2*gamma-1))/(c*c+V2*gmi)*dH;
//  Fn[pde_.iyMom] -= (rho*V2*V*(c*c*u + vn*(u*vn*gmi-v*vt*gamma)))/(u*vn*(c*c+V2*gmi))*dvt;
//
//  Fn[pde_.iEngy] -= -rho*vn*( 2*c*c + V2*gmi)/(2*gmi*gmi)*ds;
//  Fn[pde_.iEngy] -= (rho*vn*(c*c*(4*gamma-2) + V2*(2-5*gamma+3*gamma*gamma)))/(2*gmi*(c*c + V2*gmi) )*dH;
//  Fn[pde_.iEngy] -=  -(vt*V2*V*rho*gamma*(2*c*c+V2*gmi))/(2*u*gmi*(c*c+V2*gmi))*dvt;

  Fn[pde_.iCont] -= -rho*vn/gmi*ds;
  Fn[pde_.iCont] -= rho*vn/c2*dH;
  Fn[pde_.iCont] -=  -vt*V3*rho/(c*c*u)*dvt;

  Fn[pde_.ixMom] -= -rho*(c2*nx + u*vn*gamma)/(gamma*gmi)*ds;
  Fn[pde_.ixMom] -= rho*(nx + u*vn/c2)*dH;
  Fn[pde_.ixMom] -= -rho*(c2*v + u*vn*vt)*V3/(c2*u*vn)*dvt;

  Fn[pde_.iyMom] -=  -rho*(c2*ny + v*vn*gamma)/(gamma*gmi)*ds;
  Fn[pde_.iyMom] -= rho*(ny + v*vn/c2)*dH;
  Fn[pde_.iyMom] -= rho*(c2*u - v*vn*vt)*V3/(c2*u*vn)*dvt;

  Fn[pde_.iEngy] -= -rho*vn*H/gmi*ds;
  Fn[pde_.iEngy] -= rho*vn*(0.5*V2/c2 + gamma/gmi)*dH;
  Fn[pde_.iEngy] -=  -(vt*V3*rho*H)/(c2*u)*dvt;

}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class TL, class TR, class Tf>
inline void
BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDE_<PDETraitsSize,PDETraitsModel>>::
fluxNormal( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
}

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
bool
BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDE_<PDETraitsSize,PDETraitsModel>>::isValidState(
    const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const
{
  Real rho=0, u=0, v=0, t=0;
  qInterpret_.eval( q, rho, u, v, t );

  // Check that the normal velocity is negative (inflow)
  return (u*nx + v*ny) < 0;
//  return true;
}


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
void
BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDE_<PDETraitsSize,PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeFunctionInflow_sHqt_BN, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
}

//----------------------------------------------------------------------------//
// Subsonic Inflow BN with Solution Function
//----------------------------------------------------------------------------//
template<template <class> class PDETraitsSize>
struct BCEulerFunctionInflow_PtHa_BN2DParams : BCEulerSolution2DParams<PDETraitsSize>
{
  typedef BCEulerFunctionInflow_PtHa_BN2DParams This;

  static constexpr const char* BCName{"SolutionInflow_PtHa_BN"};
  struct Option
  {
    const DictOption SolutionInflow_PtHa_BN{This::BCName, This::checkInputs};
  };

  static void checkInputs(PyDict d);

  static This params;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
class BCEuler2D<BCTypeFunctionInflow_PtHa_BN, PDE_<PDETraitsSize,PDETraitsModel>> :
    public BCType< BCEuler2D<BCTypeFunctionInflow_PtHa_BN, PDE_<PDETraitsSize,PDETraitsModel>> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeFunctionInflow_PtHa_BN BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCEulerFunctionInflow_PtHa_BN2DParams<PDETraitsSize> ParamsType;
  typedef PDE_<PDETraitsSize,PDETraitsModel> PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = 3;                         // total BCs

  typedef typename PDE::QInterpret QInterpret;      // solution variable interpreter

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  // cppcheck-suppress noExplicitConstructor
  BCEuler2D( const PDE& pde, const Function_ptr& solution ) :
      pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()), solution_(solution) {}
  ~BCEuler2D() {}

  BCEuler2D(const PDE& pde, const PyDict& d ) :
    pde_(pde), gas_(pde_.gasModel()), qInterpret_(pde_.variableInterpreter()),
    solution_( BCEulerSolution2D<PDETraitsSize, PDETraitsModel>::getSolution(d) ) {}

  BCEuler2D( const BCEuler2D& ) = delete;
  BCEuler2D& operator=( const BCEuler2D& ) = delete;

  bool useUpwind() const { return false; }

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return false; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const { qB = qI; }

  // BC state
  template <class T, class L, class R>
  void state( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const;

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // normal BC flux
  template <class T, class L, class R, class Tf>
  void fluxNormal( const ParamTuple<L, R, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

protected:
  // Computes the specified BC data - s, H, qt
  bool BCQuantities(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                    Real& sSpec, Real& HSpec, Real& vNorm) const;

  const PDE& pde_;
  const GasModel& gas_;
  const QInterpret& qInterpret_;   // solution variable interpreter class
  const Function_ptr solution_;
};


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
inline bool
BCEuler2D<BCTypeFunctionInflow_PtHa_BN, PDE_<PDETraitsSize,PDETraitsModel>>::
BCQuantities( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
              Real& PtSpec, Real& HSpec, Real& vNorm ) const
{
  Real gamma = gas_.gamma();

  ArrayQ<Real> qBspec = 0;
  qBspec = (*solution_)(x,y,time);
  Real rhoSpec, uSpec, vSpec, tSpec, pSpec = 0;
  qInterpret_.eval( qBspec, rhoSpec, uSpec, vSpec, tSpec );
  pSpec = gas_.pressure(rhoSpec, tSpec);
  HSpec = gas_.enthalpy(rhoSpec, tSpec) + 0.5*(uSpec*uSpec + vSpec*vSpec);

  Real c2spec = gamma*pSpec/rhoSpec;

  PtSpec = pSpec*pow( 1 + 0.5*(gamma-1.)*(uSpec*uSpec + vSpec*vSpec)/c2spec, gamma/(gamma-1.) );

  vNorm = vSpec / sqrt(uSpec*uSpec + vSpec*vSpec);

  // the state is valid
  return true;
}

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class TL, class TR>
inline void
BCEuler2D<BCTypeFunctionInflow_PtHa_BN, PDE_<PDETraitsSize,PDETraitsModel>>::
state( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
       const Real& nx, const Real& ny,
       const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
{
  state(x, y, time, nx, ny, qI, qB);
}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class Tf>
inline void
BCEuler2D<BCTypeFunctionInflow_PtHa_BN, PDE_<PDETraitsSize,PDETraitsModel>>::
fluxNormal( const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  // Zero out any possible stabilization
  Fn = 0;

  // Compute the advective flux based on the boundary state
  ArrayQ<T> fx = 0, fy = 0;
  pde_.fluxAdvective(x, y, time, qB, fx, fy);

  Fn += fx*nx + fy*ny;

  Real gamma = gas_.gamma();
  Real gmi   = gamma - 1;

  T rho=0, u=0, v=0, t=0;

  Real PtSpec, HSpec, vNorm;
  BCQuantities(x, y, time, nx, ny, PtSpec, HSpec, vNorm);

  //compute BN Flux
  qInterpret_.eval( qI, rho, u, v, t );
  T p = gas_.pressure(rho,t);

  T H = gas_.enthalpy(rho, t) + 0.5*(u*u+v*v);
  T c = gas_.speedofSound(t);
  T c2 = c*c;
//  T s = log( p /pow(rho,gamma) );

  // tangential vector
  const Real tx = -ny;
  const Real ty =  nx;

  // normal and tangential velocities
  T vn = u*nx + v*ny;
  T vt = u*tx + v*ty;
  T V2 = u*u+v*v;
  T V = sqrt(V2);

  T M = V/c;
  T M2 = M*M;
  T Gamma = pow(1 + 0.5*gmi*M2,gamma/gmi);
  T pt = p*Gamma;

  // Strong BC's that are imposed
  T dpt = pt  - PtSpec;
  T dH  = H  - HSpec;
  T dvt = v/sqrt(u*u+v*v) - vNorm;


  //COMPUTE NORMAL FLUX: DERIVED FROM ENTROPY VARS
  Fn[pde_.iCont] -= vn*Gamma*gamma/c2*dpt;
  Fn[pde_.iCont] -= -vn*M2*gamma*(gamma-1)*rho/(c2 + V2*gmi)/(2 + M2*gmi)*dH;
  Fn[pde_.iCont] -=  -vt*V*M2*rho*gamma/u/(1+M*M*gmi)*dvt;

  Fn[pde_.ixMom] -= Gamma*(nx + u*vn*gamma/c2)*dpt;
  Fn[pde_.ixMom] -= (u*vn - v*vt - u*vn*M2)*gmi*rho/c2/(2 + 3*M2*gmi + M2*M2*gmi*gmi)*dH;
  Fn[pde_.ixMom] -= -(rho*M2*V*(c2*v + vn*(v*vn*gmi+u*vt*gamma)))/(u*vn*(1+M2*gmi))*dvt;

  Fn[pde_.iyMom] -=  Gamma*(ny + v*vn*gamma/c2)*dpt;
  Fn[pde_.iyMom] -= (v*vn + u*vt - v*vn*M2)*gmi*rho/c2/(2 + 3*M2*gmi + M2*M2*gmi*gmi)*dH;
  Fn[pde_.iyMom] -= (rho*M2*V*(c2*u + vn*(u*vn*gmi-v*vt*gamma)))/(u*vn*(1+M2*gmi))*dvt;

  Fn[pde_.iEngy] -= 0.5*vn*(2 + M2*gmi)*Gamma*gamma/gmi*dpt;
  Fn[pde_.iEngy] -= 0.5*vn*(2 + M2*(gamma-2))*rho/(1+ M2*gmi)*dH;
  Fn[pde_.iEngy] -=  -(vt*M2*V*rho*gamma*(2*c*c+V2*gmi))/(2*u*gmi*(1+M2*gmi))*dvt;
}

// normal BC flux
template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
template <class T, class TL, class TR, class Tf>
inline void
BCEuler2D<BCTypeFunctionInflow_PtHa_BN, PDE_<PDETraitsSize,PDETraitsModel>>::
fluxNormal( const ParamTuple<TL, TR, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
            const Real& nx, const Real& ny,
            const ArrayQ<T>& qI,
            const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
            const ArrayQ<T>& qB,
            ArrayQ<Tf>& Fn) const
{
  fluxNormal(x, y, time, nx, ny, qI, qIx, qIy, qB, Fn);
}

template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
bool
BCEuler2D<BCTypeFunctionInflow_PtHa_BN, PDE_<PDETraitsSize,PDETraitsModel>>::isValidState(
    const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const
{
  Real rho=0, u=0, v=0, t=0;
  qInterpret_.eval( q, rho, u, v, t );

  // Check that the normal velocity is negative (inflow)
  return (u*nx + v*ny) < 0;
}


template <template <class> class PDETraitsSize, class PDETraitsModel, template< template<class> class, class> class PDE_>
void
BCEuler2D<BCTypeFunctionInflow_PtHa_BN, PDE_<PDETraitsSize,PDETraitsModel>>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCEuler2D<BCTypeFunctionInflow_PtHa_BN, PDE>: pde_ = " << std::endl;
  pde_.dump(indentSize+2, out);
}



} //namespace SANS

#endif  // BCEULER2D_BN_H
