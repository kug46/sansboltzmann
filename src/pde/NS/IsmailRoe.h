// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ISMAILROE_H
#define ISMAILROE_H

#include "LinearAlgebra/Transpose.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Transpose.h"

#include "Topology/Dimension.h"

#include "GasModel.h"
#include "Roe.h"

#include "tools/minmax.h"

namespace SANS
{

/////////////////////////////////////////////////////////////////////////////////////////
// Affordable, entropy-consistent Euler flux functions II: Entropy production at shocks
//  - Farzad Ismail, Philip L. Roe
/////////////////////////////////////////////////////////////////////////////////////////

// Forward declare
template<class QType, template <class> class PDETraitsSize>
class Q2D;

template <class QType, template <class> class PDETraitsSize>
class IsmailRoe2D
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef Q2D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  explicit IsmailRoe2D(const GasModel& gas0, const RoeEntropyFix entropyFix) :
        gas_(gas0),
        qInterpret_(gas0),
        entropyFix_(entropyFix)
  {
    // Nothing
  }

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iyMom = 2;
  static const int iEngy = 3;

  IsmailRoe2D( const IsmailRoe2D& IsmailRoe0 ) = delete;
  IsmailRoe2D& operator=( const IsmailRoe2D& ) = delete;


  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor = 1 ) const;

  template <class Tq, class Tqp, class Tf>
  void fluxAdvectiveUpwindLinear(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& dq,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    SANS_DEVELOPER_EXCEPTION( "IsmailRoe2D::fluxAdvectiveUpwindLinear Not Yet Implemented" );
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, Tq& C1, Tq& eigu, const Real& scaleFactor ) const;

  template <class Tq, class Tqp, class Tf>
  void fluxAdvectiveUpwindLinear(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& dq,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, Tf& C1, Tq& eigu,
      ArrayQ<Tf>& du ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const
  {
    SANS_DEVELOPER_EXCEPTION( "IsmailRoe2D::fluxAdvectiveUpwindLinear Not Yet Implemented" );
  }

protected:
  // From Appendix B
  template <class T>
  T logMean(const T& aL, const T& aR) const
  {
    const Real eps = 1e-2;

    T xi = aL / aR;
    T f = (xi - 1.0) / (xi + 1.0);
    T u = f*f;

    T F = 0;
    if (u < eps)
    {
      F = 1.0 + u / 3.0 + u*u / 5.0 + u*u*u / 7.0;
    }
    else
    {
      F = log(xi) / 2.0 / f;
    }

    return (aL + aR) / (2.0 * F);
  }

private:
  const GasModel& gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  const RoeEntropyFix entropyFix_;
};


template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
IsmailRoe2D<QType, PDETraitsSize>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor ) const
{
  Tq rhoL=0, uL=0, vL=0, tL=0, pL=0;
  Tq rhoR=0, uR=0, vR=0, tR=0, pR=0;

  qInterpret_.eval( qL, rhoL, uL, vL, tL );
  pL  = gas_.pressure(rhoL, tL);

  qInterpret_.eval( qR, rhoR, uR, vR, tR );
  pR  = gas_.pressure(rhoR, tR);

  Real gamma = gas_.gamma();

  // Eq. 68
  Tq z1L = sqrt(rhoL / pL);
  Tq z2L = sqrt(rhoL / pL) * uL;
  Tq z3L = sqrt(rhoL / pL) * vL;
  Tq z4L = sqrt(rhoL * pL);

  Tq z1R = sqrt(rhoR / pR);
  Tq z2R = sqrt(rhoR / pR) * uR;
  Tq z3R = sqrt(rhoR / pR) * vR;
  Tq z4R = sqrt(rhoR * pR);

  Tq z1bar = 0.5*(z1L + z1R);
  Tq z2bar = 0.5*(z2L + z2R);
  Tq z3bar = 0.5*(z3L + z3R);
  Tq z4bar = 0.5*(z4L + z4R);

  Tq z1ln = logMean(z1L, z1R);
  Tq z4ln = logMean(z4L, z4R);
  Tq uhat = z2bar / z1bar;
  Tq vhat = z3bar / z1bar;

  // Eq. 69
  Tq rhohat = z1bar * z4ln;
  // Eq. 70
  Tq p1hat = z4bar / z1bar;
  // Eq. 71
  Tq p2hat = (gamma + 1.0) / 2.0 / gamma * z4ln / z1ln + (gamma - 1.0) / 2.0 / gamma * z4bar / z1bar;
  // Eq. 72
  Tq chat = sqrt(gamma * p2hat / rhohat);
  // Eq. 73
  Tq Hhat = chat*chat / (gamma - 1.0) + (uhat*uhat + vhat*vhat) / 2.0;

  // Eq 67 - Entropy conservative flux
  f(iCont) += (nx*(rhohat*uhat             ) + ny*(rhohat*vhat             ))*scaleFactor;
  f(ixMom) += (nx*(rhohat*uhat*uhat + p1hat) + ny*(rhohat*vhat*uhat        ))*scaleFactor;
  f(iyMom) += (nx*(rhohat*uhat*vhat        ) + ny*(rhohat*vhat*vhat + p1hat))*scaleFactor;
  f(iEngy) += (nx*(rhohat*uhat*Hhat        ) + ny*(rhohat*vhat*Hhat        ))*scaleFactor;

  if (entropyFix_ == eIsmailRoeHartenFix)
  {
    // Eq. 40
    Tq sL = log(pL) - gamma * log(rhoL);
    ArrayQ<Tq> VL = 0;
    VL(0) = (gamma - sL) / (gamma - 1.0) - 0.5 * rhoL / pL * (uL*uL + vL*vL);
    VL(1) = rhoL * uL / pL;
    VL(2) = rhoL * vL / pL;
    VL(3) = -rhoL / pL;

    Tq sR = log(pR) - gamma * log(rhoR);
    ArrayQ<Tq> VR = 0;
    VR(0) = (gamma - sR) / (gamma - 1.0) - 0.5 * rhoR / pR * (uR*uR + vR*vR);
    VR(1) = rhoR * uR / pR;
    VR(2) = rhoR * vR / pR;
    VR(3) = -rhoR / pR;

    // Eq. 42
    MatrixQ<Tq> Rhat = 0;
    Rhat(0,0) = 1.0;
    Rhat(1,0) = uhat - chat*nx;
    Rhat(2,0) = vhat - chat*ny;
    Rhat(3,0) = Hhat - (uhat*nx + vhat*ny) * chat;

    Rhat(0,1) = nx;
    Rhat(1,1) = uhat*nx;
    Rhat(2,1) = vhat*nx;
    Rhat(3,1) = 0.5*(uhat*uhat + vhat*vhat)*nx;


    Rhat(0,2) = ny;
    Rhat(1,2) = uhat*ny;
    Rhat(2,2) = vhat*ny;
    Rhat(3,2) = 0.5*(uhat*uhat + vhat*vhat)*ny;

    Rhat(0,3) = 1.0;
    Rhat(1,3) = uhat + chat*nx;
    Rhat(2,3) = vhat + chat*ny;
    Rhat(3,3) = Hhat + (uhat*nx + vhat*ny) * chat;

    // Eq. 45
    MatrixQ<Tq> Shat= 0;
    Shat(0,0) = rhohat / 2.0 / gamma;
    Shat(1,1) = (gamma - 1.0) / gamma * rhohat;
    Shat(2,2) = (gamma - 1.0) / gamma * rhohat;
    Shat(3,3) = rhohat / 2.0 / gamma;

  //  // Lets use D_EC2 formulation from Eq. 59
  //  const Real beta = 1.0/6.0;
  //  const Real alpha_max = 2.0;
  //  const Real alpha_min = 1.0/6.0;
  //  const Real dM_max = 0.5;
  //  // Eq. 60
  //  MatrixQ<Tq> Lambdahat = 0;
  //  Lambdahat(0,0) = (1 + beta)*abs((uhat*nx + vhat*ny) - chat);
  //  Lambdahat(1,1) = abs((uhat*nx + vhat*ny));
  //  Lambdahat(2,2) = abs((uhat*nx + vhat*ny));
  //  Lambdahat(3,3) = (1 + beta)*abs((uhat*nx + vhat*ny) + chat);
  //  MatrixQ<Tq> Lambda2 = 0;
  //  Lambda2(0,0) = abs((uhat*nx + vhat*ny) - chat);
  //  Lambda2(3,3) = abs((uhat*nx + vhat*ny) + chat);
  //  // Eq. 61
  //  Tq cL = gas_.speedofSound(tL);
  //  Tq cR = gas_.speedofSound(tR);
  //  Tq ML = ((uL*nx + vL*ny)) / cL;
  //  Tq MR = ((uR*nx + vR*ny)) / cR;
  //  Tq dM = (MR - ML);
  //  Tq sign = 0.0;
  //  if ((dM_max - dM) > 0)
  //    sign = 1.0;
  //  Tq alpha2 = (alpha_max - alpha_min) * max(Tq(0), sign) + alpha_min;
  //
  //  // Eq. 59
  //  MatrixQ<Tq> Dhat = 0;
  //  Dhat = (Lambdahat + alpha2 * Lambda2) * Shat;

    Real eps0 = 1e-4;
    Tq eps    = chat*eps0;
    // Eigenvalues
    Tq eigumc = fabs(uhat*nx + vhat*ny - chat);
    Tq eigupc = fabs(uhat*nx + vhat*ny + chat);
    Tq eigu   = fabs(uhat*nx + vhat*ny       );
    // Entropy fix
    eigumc = sqrt(eigumc*eigumc + eps*eps);
    eigupc = sqrt(eigupc*eigupc + eps*eps);
    eigu   = sqrt(eigu*eigu     + eps*eps);
    MatrixQ<Tq> Lambda = 0;
    Lambda(0,0) = eigumc;
    Lambda(1,1) = eigu;
    Lambda(2,2) = eigu;
    Lambda(3,3) = eigupc;

    MatrixQ<Tq> Dhat = Lambda*Shat;

    // Eq. 41
    MatrixQ<Tq> D = Rhat*Dhat*Transpose(Rhat);
    ArrayQ<Tq> dV = VR - VL;
    ArrayQ<Tq> df = D * dV;
    f -= 0.5*df*scaleFactor;
  }
}



template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tqp, class Tf>
inline void
IsmailRoe2D<QType, PDETraitsSize>::fluxAdvectiveUpwindLinear(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& dq,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f, Tf& C1, Tq& eigu,
    ArrayQ<Tf>& du) const
{
  SANS_DEVELOPER_EXCEPTION( "IsmailRoe2D::fluxAdvectiveUpwindLinear Not Yet Implemented" );
}

template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
IsmailRoe2D<QType, PDETraitsSize>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{
  SANS_DEVELOPER_EXCEPTION( "IsmailRoe2D::fluxAdvectiveUpwindSpaceTime Not Yet Implemented" );
}

}
#endif // ISMAILROE_H
