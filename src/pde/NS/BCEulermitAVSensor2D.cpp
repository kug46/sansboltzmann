// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "Q2DPrimitiveRhoPressure.h"
#include "Q2DPrimitiveSurrogate.h"
#include "Q2DEntropy.h"
#include "Q2DConservative.h"

#include "TraitsEulerArtificialViscosity.h"
#include "TraitsNavierStokesArtificialViscosity.h"
#include "TraitsRANSSAArtificialViscosity.h"
#include "BCEulermitAVSensor2D.h"


#include "Fluids2D_Sensor.h"
#include "pde/ArtificialViscosity/AVSensor_AdvectiveFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_ViscousFlux2D.h"
#include "pde/ArtificialViscosity/AVSensor_Source2D.h"
#include "pde/ArtificialViscosity/PDEmitAVSensor2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
// Instantiate BC parameters

typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeEulerArtificialViscosity> SensorAdvectiveFlux;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeEulerArtificialViscosity> SensorViscousFlux;


// Pressure-primitive variables
typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> PDEBaseRhoPressure;
typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD2, PDEBaseRhoPressure>> SensorSourceRhoPressure;
typedef TraitsModelArtificialViscosity<PDEBaseRhoPressure, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceRhoPressure> TraitsModelAVRhoPressure;

typedef BCEulermitAVSensor2DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelAVRhoPressure > BCVector_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RhoP_S )

// Pressure gradient sensor
typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveRhoPressure, GasModel>> PDEBaseRhoPressure;
typedef AVSensor_Source2D_PressureGrad<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD2, PDEBaseRhoPressure>> SensorGradPSourceRhoPressure;
typedef TraitsModelArtificialViscosity<PDEBaseRhoPressure,
                                       SensorAdvectiveFlux,
                                       SensorViscousFlux,
                                       SensorGradPSourceRhoPressure> TraitsModelGradPAVRhoPressure;

typedef BCEulermitAVSensor2DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelGradPAVRhoPressure > BCVector_GradP_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_GradP_RhoP_S )

// Conservative Variables
typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypeConservative, GasModel>> PDEBaseCons;
typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD2, PDEBaseCons>> SensorSourceCons;
typedef TraitsModelArtificialViscosity<PDEBaseCons, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceCons> TraitsModelAVCons;

typedef BCEulermitAVSensor2DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelAVCons > BCVector_Cons_S;
BCPARAMETER_INSTANTIATE( BCVector_Cons_S )

#if 0
// Entropy Variables
typedef BCEulermitAVSensor2DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelEuler<QTypeEntropy, GasModel> > BCVector_Entropy_S;
BCPARAMETER_INSTANTIATE( BCVector_Entropy_S )
#endif

// Pressure gradient sensor
typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypeEntropy, GasModel>> PDEBaseEntropy;
typedef AVSensor_Source2D_PressureGrad<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD2, PDEBaseEntropy>> SensorGradPSourceEntropy;
typedef TraitsModelArtificialViscosity<PDEBaseEntropy,
                                       SensorAdvectiveFlux,
                                       SensorViscousFlux,
                                       SensorGradPSourceEntropy> TraitsModelGradPAVEntropy;

typedef BCEulermitAVSensor2DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelGradPAVEntropy > BCVector_GradP_Entropy_S;
BCPARAMETER_INSTANTIATE( BCVector_GradP_Entropy_S )

// Surrogate Variables
typedef PDEEulermitAVDiffusion2D<TraitsSizeEulerArtificialViscosity,
                                       TraitsModelEuler<QTypePrimitiveSurrogate, GasModel>> PDEBaseSurrogate;
typedef AVSensor_Source2D_Jump<TraitsSizeEulerArtificialViscosity, Fluids_Sensor<PhysD2, PDEBaseSurrogate>> SensorSourceSurrogate;
typedef TraitsModelArtificialViscosity<PDEBaseSurrogate, SensorAdvectiveFlux, SensorViscousFlux, SensorSourceSurrogate> TraitsModelAVSurrogate;

typedef BCEulermitAVSensor2DVector<
            TraitsSizeEulerArtificialViscosity,
            TraitsModelAVSurrogate > BCVector_Surrogate_S;
BCPARAMETER_INSTANTIATE( BCVector_Surrogate_S )

/// Navier-Stokes --------------------------
// Pressure based primitive variables
// Pressure gradient sensor
// Constant viscosity case
typedef QTypePrimitiveRhoPressure QTypePrim;
typedef ViscosityModel_Const ViscosityModelType_Const;
typedef TraitsModelNavierStokes<QTypePrim, GasModel, ViscosityModelType_Const, ThermalConductivityModel> TraitsModelNavierStokesClass_Prim_Const;
typedef PDEEulermitAVDiffusion2D<TraitsSizeNavierStokesArtificialViscosity,
                                 TraitsModelNavierStokesClass_Prim_Const,
                                 PDENavierStokes2D> PDEBaseClass_Prim_Const;


typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeNavierStokesArtificialViscosity> SensorAdvectiveFluxNS;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeNavierStokesArtificialViscosity> SensorViscousFluxNS;
typedef Fluids_Sensor<PhysD2, PDEBaseClass_Prim_Const> Sensor_Prim_Const;
typedef AVSensor_Source2D_PressureGrad<TraitsSizeNavierStokesArtificialViscosity, Sensor_Prim_Const> SensorSourceNS_Prim_Const;

typedef TraitsModelArtificialViscosity<PDEBaseClass_Prim_Const,
                                       SensorAdvectiveFluxNS,
                                       SensorViscousFluxNS,
                                       SensorSourceNS_Prim_Const> TraitsModelAV_Prim_Const;

typedef BCNavierStokesmitAVDiffusion2DVector<
            TraitsSizeNavierStokesArtificialViscosity, TraitsModelAV_Prim_Const> BCVector_NavierStokes_Primitive_Const;
BCPARAMETER_INSTANTIATE( BCVector_NavierStokes_Primitive_Const )

// Surrogate variables
// Constant viscosity case
typedef QTypePrimitiveSurrogate QType;
typedef ViscosityModel_Const ViscosityModelType_Const;
typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType_Const, ThermalConductivityModel> TraitsModelNavierStokesClass_Const;
typedef PDEEulermitAVDiffusion2D<TraitsSizeNavierStokesArtificialViscosity, TraitsModelNavierStokesClass_Const, PDENavierStokes2D> PDEBaseClass_Const;


typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeNavierStokesArtificialViscosity> SensorAdvectiveFluxNS;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeNavierStokesArtificialViscosity> SensorViscousFluxNS;
typedef Fluids_Sensor<PhysD2, PDEBaseClass_Const> Sensor_Const;
typedef AVSensor_Source2D_Jump<TraitsSizeNavierStokesArtificialViscosity, Sensor_Const> SensorSourceNS_Const;

typedef TraitsModelArtificialViscosity<PDEBaseClass_Const, SensorAdvectiveFluxNS, SensorViscousFluxNS, SensorSourceNS_Const> TraitsModelAV_Const;

typedef BCNavierStokesmitAVDiffusion2DVector<
            TraitsSizeNavierStokesArtificialViscosity, TraitsModelAV_Const> BCVector_NavierStokes_Surrogate_Const;
BCPARAMETER_INSTANTIATE( BCVector_NavierStokes_Surrogate_Const )

// Modified Sutherland's Law case
typedef QTypePrimitiveSurrogate QType;
typedef ViscosityModel_SutherlandModified ViscosityModelType_SuthMod;
typedef TraitsModelNavierStokes<QType, GasModel, ViscosityModelType_SuthMod, ThermalConductivityModel> TraitsModelNavierStokesClass_SuthMod;
typedef PDEEulermitAVDiffusion2D<TraitsSizeNavierStokesArtificialViscosity,
                                 TraitsModelNavierStokesClass_SuthMod, PDENavierStokes2D> PDEBaseClass_SuthMod;


typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeNavierStokesArtificialViscosity> SensorAdvectiveFluxNS;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeNavierStokesArtificialViscosity> SensorViscousFluxNS;
typedef Fluids_Sensor<PhysD2, PDEBaseClass_SuthMod> Sensor_SuthMod;
typedef AVSensor_Source2D_Jump<TraitsSizeNavierStokesArtificialViscosity, Sensor_SuthMod> SensorSourceNS_SuthMod;

typedef TraitsModelArtificialViscosity<PDEBaseClass_SuthMod, SensorAdvectiveFluxNS,
                                       SensorViscousFluxNS, SensorSourceNS_SuthMod> TraitsModelAV_SuthMod;

// Surrogate variables
typedef BCNavierStokesmitAVDiffusion2DVector<
            TraitsSizeNavierStokesArtificialViscosity, TraitsModelAV_SuthMod> BCVector_NavierStokes_Surrogate_SuthMod;
BCPARAMETER_INSTANTIATE( BCVector_NavierStokes_Surrogate_SuthMod )

/// RANS-SA --------------------------
// Pressure gradient sensor
// Constant Viscosity
typedef QTypePrimitiveRhoPressure QTypePrim;
typedef TraitsModelRANSSA<QTypePrim, GasModel, ViscosityModelType_Const, ThermalConductivityModel> TraitsModelRANSSAClass_Const;
typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass_Const, PDERANSSA2D> PDEBaseClassRANSSA_Const;


typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFluxRANSSA;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFluxRANSSA;
typedef Fluids_Sensor<PhysD2, PDEBaseClassRANSSA_Const> SensorRANSSA_Const;
typedef AVSensor_Source2D_PressureGrad<TraitsSizeRANSSAArtificialViscosity, SensorRANSSA_Const> SensorSourceRANSSA_GradP_Const;

typedef TraitsModelArtificialViscosity<PDEBaseClassRANSSA_Const,
                                       SensorAdvectiveFluxRANSSA,
                                       SensorViscousFluxRANSSA,
                                       SensorSourceRANSSA_GradP_Const> TraitsModelAV_GradP_Const;

// Pressure-primitive variables
typedef BCRANSSAmitAVDiffusion2DVector<
            TraitsSizeRANSSAArtificialViscosity, TraitsModelAV_GradP_Const> BCVector_RANSSA_RhoP_GradP_Const;
BCPARAMETER_INSTANTIATE( BCVector_RANSSA_RhoP_GradP_Const )

// jump sensor
// Modified Sutherland's Law case
typedef QTypePrimitiveRhoPressure QTypePrim;
typedef TraitsModelRANSSA<QTypePrim, GasModel, ViscosityModelType_Const, ThermalConductivityModel> TraitsModelRANSSAClass;
typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClass, PDERANSSA2D> PDEBaseClassRANSSA;


typedef AVSensor_AdvectiveFlux2D_Uniform<TraitsSizeRANSSAArtificialViscosity> SensorAdvectiveFluxRANSSA;
typedef AVSensor_ViscousFlux2D_GenHScale<TraitsSizeRANSSAArtificialViscosity> SensorViscousFluxRANSSA;
typedef Fluids_Sensor<PhysD2, PDEBaseClassRANSSA> SensorRANSSA;
typedef AVSensor_Source2D_Jump<TraitsSizeRANSSAArtificialViscosity, SensorRANSSA> SensorSourceRANSSA;

typedef TraitsModelArtificialViscosity<PDEBaseClassRANSSA, SensorAdvectiveFluxRANSSA, SensorViscousFluxRANSSA, SensorSourceRANSSA> TraitsModelAV;

// Pressure-primitive variables
typedef BCRANSSAmitAVDiffusion2DVector<
            TraitsSizeRANSSAArtificialViscosity, TraitsModelAV> BCVector_RANSSA_RhoP_S;
BCPARAMETER_INSTANTIATE( BCVector_RANSSA_RhoP_S )


typedef TraitsModelRANSSA<QType, GasModel, ViscosityModelType_SuthMod, ThermalConductivityModel> TraitsModelRANSSAClassSurr;
typedef PDEEulermitAVDiffusion2D<TraitsSizeRANSSAArtificialViscosity, TraitsModelRANSSAClassSurr, PDERANSSA2D> PDEBaseClassRANSSASurr;

typedef Fluids_Sensor<PhysD2, PDEBaseClassRANSSASurr> SensorRANSSASurr;
typedef AVSensor_Source2D_Jump<TraitsSizeRANSSAArtificialViscosity, SensorRANSSASurr> SensorSourceRANSSASurr;

typedef TraitsModelArtificialViscosity<PDEBaseClassRANSSASurr,
                                       SensorAdvectiveFluxRANSSA,
                                       SensorViscousFluxRANSSA,
                                       SensorSourceRANSSASurr> TraitsModelAVSurr;

// Pressure-primitive variables
typedef BCRANSSAmitAVDiffusion2DVector<
            TraitsSizeRANSSAArtificialViscosity, TraitsModelAVSurr> BCVector_RANSSA_Surr_S;
BCPARAMETER_INSTANTIATE( BCVector_RANSSA_Surr_S )

} //namespace SANS
