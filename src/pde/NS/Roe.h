// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef ROE_H
#define ROE_H

#include "Topology/Dimension.h"
#include "GasModel.h"
#include "tools/minmax.h"

namespace SANS
{

enum RoeEntropyFix
{
  eVanLeer,
  eHarten,
  eIsmailRoeEntropyNeutral,
  eIsmailRoeHartenFix
};

// Forward declare
template<class QType, template <class> class PDETraitsSize>
class Q1D;

template<class QType, template <class> class PDETraitsSize>
class Q2D;

template<class QType, template <class> class PDETraitsSize>
class Q3D;

template <class QType, template <class> class PDETraitsSize>
class Roe1D
{
public:
  typedef PhysD1 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef Q1D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  explicit Roe1D(const GasModel& gas0, const RoeEntropyFix entropyFix) :
        gas_(gas0),
        qInterpret_(gas0),
        entropyFix_(entropyFix)
  {
    // Nothing
  }

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iEngy = 2;

  Roe1D( const Roe1D& roe0 ) = delete;
  Roe1D& operator=( const Roe1D& ) = delete;

  RoeEntropyFix entropyFix() const { return entropyFix_; }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, ArrayQ<Tf>& f ) const
  {
    T C1, eigu;
    fluxAdvectiveUpwind(x, time, qL, qR, nx, f, C1, eigu);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, ArrayQ<Tf>& f, Tq& C1, Tq& eigu ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& nt, MatrixQ<Tf>& mtx ) const;

private:
  const GasModel& gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  const RoeEntropyFix entropyFix_;
};


template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
Roe1D<QType, PDETraitsSize>::fluxAdvectiveUpwind(
    const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, ArrayQ<Tf>& f, Tq& C1, Tq& eigu ) const
{
  Tq rhoL=0, uL=0, tL=0, pL, hL, HL, V2L;
  Tq rhoR=0, uR=0, tR=0, pR, hR, HR, V2R;

  qInterpret_.eval( qL, rhoL, uL, tL );
  pL  = gas_.pressure(rhoL, tL);
  hL  = gas_.enthalpy(rhoL, tL);
  V2L = uL*uL;
  HL  = hL + 0.5*V2L;

  qInterpret_.eval( qR, rhoR, uR, tR );
  pR  = gas_.pressure(rhoR, tR);
  hR  = gas_.enthalpy(rhoR, tR);
  V2R = uR*uR;
  HR  = hR + 0.5*V2R;

  Tq eL, EL;
  Tq eR, ER;

  eL = gas_.energy(rhoL, tL);
  EL = eL + 0.5*V2L;

  eR = gas_.energy(rhoR, tR);
  ER = eR + 0.5*V2R;

  Real gamma = gas_.gamma();

  f(iCont) += 0.5*(nx*(rhoL*uL        ));
  f(ixMom) += 0.5*(nx*(rhoL*uL*uL + pL));
  f(iEngy) += 0.5*(nx*(rhoL*uL*HL     ));

  f(iCont) += 0.5*(nx*(rhoR*uR        ));
  f(ixMom) += 0.5*(nx*(rhoR*uR*uR + pR));
  f(iEngy) += 0.5*(nx*(rhoR*uR*HR     ));

  // TODO: Ideal gas is assumed in the following formulation
  Tq w1L = sqrt(rhoL);
  Tq w1R = sqrt(rhoR);

  // Roe averaged quantities
  Tq u = (w1L*uL + w1R*uR) / (w1L + w1R);
  Tq H = (w1L*HL + w1R*HR) / (w1L + w1R) ;

  // This assumes calorically perfect gas
  Tq c = sqrt( (gamma - 1.0)*(H - 0.5*(u*u)) ) ;

  Tq qn = nx*u;

  // jump conditions and Roe coefficients
  Tq delrho  = rhoR    - rhoL;
  Tq delrhou = rhoR*uR - rhoL*uL;
  Tq delrhoE = rhoR*ER - rhoL*EL;

  Tq eigumc = fabs(qn - c);
  Tq eigupc = fabs(qn + c);
     eigu   = fabs(qn    );

  Real eps0 = 1e-2;
  Tq eps    = c*eps0;

  switch (entropyFix_)
  {
  case eVanLeer:
  {
    // 'entropy fix' of van Leer, Lee and Powell (Sonic Point Capturing)
    //
    // The term
    //
    // deigmin = max(eps, deig);
    //
    // was suggested by Prof Darmofal so that the dissipation does not disappear
    // for eigu == 0
    static const int  FIX = 4; // entropy fix fudge factor

    Tq qnL = nx*uL;
    Tq qnR = nx*uR;
    Tq cL  = gas_.speedofSound(tL);
    Tq cR  = gas_.speedofSound(tR);

    Tq eigL, eigR, deig, deigmin;

    eigL = qnL - cL;
    eigR = qnR - cR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigumc < 0.5*deigmin)
      eigumc = (eigumc*eigumc)/deigmin + 0.25*deigmin;

    eigL = qnL + cL;
    eigR = qnR + cR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigupc < 0.5*deigmin)
      eigupc = (eigupc*eigupc)/deigmin + 0.25*deigmin;

    eigL = qnL;
    eigR = qnR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigu < 0.5*deigmin)
      eigu = (eigu*eigu)/deigmin + 0.25*deigmin;

    break;
  }
  case eHarten:
  {

    /*--------------------------*/
    /*  Harten (PX) Entropy Fix */
    /*--------------------------*/

    if (eigumc < eps)
      eigumc = 0.5*(eps + eigumc*eigumc/eps);
    //eigumc = (eigumc*eigumc + eps*eps) / (2*eps);

    if (eigupc < eps)
      eigupc = 0.5*(eps + eigupc*eigupc/eps);
    //eigupc = (eigupc*eigupc + eps*eps) / (2*eps);

    if (eigu < eps)
      eigu = 0.5*(eps + eigu*eigu/eps);
    //eigu = (eigu*eigu + eps*eps) / (2*eps);

    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown entropy fix.");
  }

  // Roe dissipation flux following Fidkowski
  Tq qq = 0.5*(u*u) ;
  Tq G1 = (gamma-1.0)/(c*c)*(delrho*qq - delrhou*u + delrhoE);
  Tq G2 = (delrhou*nx - delrho*qn);
  Tq s1 = 0.5*(eigupc + eigumc);
  Tq s2 = 0.5*(eigupc - eigumc);
     C1 = G2*s2/c + G1*(s1 - eigu);
  Tq C2 = c*G1*s2 + G2*(s1 - eigu);

  f(iCont) -= 0.5*(           C1 + delrho *eigu );
  f(ixMom) -= 0.5*( nx*C2 + u*C1 + delrhou*eigu );
  f(iEngy) -= 0.5*( qn*C2 + H*C1 + delrhoE*eigu );

#if 0 //Steve's alternate formulation
  T alpha1   = 0.5*(delp   - delqn) * eigumc;
  T alpha2   = 0.5*(delp   + delqn) * eigupc;
  T alpha3   =     (delrho - delp ) * eigu  ;
  T alpha4   =     (delqs         ) * eigu  ;

  /*  Roe dissipation flux  */
  T qq     = 0.5*(u*u + v*v) ;
  flx1_dsp = al1            + al2            + al3                ;
  flxs_dsp = al1*(qs      ) + al2*(qs      ) + al3*(qs) + al4     ;
  flxn_dsp = al1*(qn - c  ) + al2*(qn + c  ) + al3*(qn)           ;
  flx4_dsp = al1*(H - c*qn) + al2*(H + c*qn) + al3*(qq) + al4*(qs);

  f(0) -= 0.5*(ds*flx1_dsp              );
  f(1) -= 0.5*(dx*flxs_dsp + dy*flxn_dsp);
  f(2) -= 0.5*(dy*flxs_dsp - dx*flxn_dsp);
  f(3) -= 0.5*(ds*flx4_dsp              );
#endif
}

template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
Roe1D<QType, PDETraitsSize>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{

  SANS_DEVELOPER_EXCEPTION( "Spacetime Roe Scheme Not Yet Implemented" );
//  T rhoL, uL, tL, pL, hL, HL = 0;
//  T rhoR, uR, tR, pR, hR, HR = 0;
//
//  qInterpret_.eval( qL, rhoL, uL, tL );
//  pL  = gas_.pressure(rhoL, tL);
//  hL  = gas_.enthalpy(rhoL, tL);
//  HL = hL + 0.5*(uL*uL);
//
//  qInterpret_.eval( qR, rhoR, uR, tR );
//  pR  = gas_.pressure(rhoR, tR);
//  hR  = gas_.enthalpy(rhoR, tR);
//  HR = hR + 0.5*(uR*uR);
//
//  f(0) += 0.5*(nx*(rhoL*uL        ));
//  f(1) += 0.5*(nx*(rhoL*uL*uL + pL));
//
//  f(0) += 0.5*(nx*(rhoR*uR        ));
//  f(1) += 0.5*(nx*(rhoR*uR*uR + pR));

}

template <class QType, template <class> class PDETraitsSize>
class Roe2D
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef Q2D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  explicit Roe2D(const GasModel& gas0, const RoeEntropyFix entropyFix) :
        gas_(gas0),
        qInterpret_(gas0),
        entropyFix_(entropyFix)
  {
    // Nothing
  }

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iyMom = 2;
  static const int iEngy = 3;

  Roe2D( const Roe2D& roe0 ) = delete;
  Roe2D& operator=( const Roe2D& ) = delete;

  RoeEntropyFix entropyFix() const { return entropyFix_; }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, const Real& scaleFactor = 1 ) const
  {
    T C1, eigu;
    fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f, C1, eigu, scaleFactor);
  }

  template <class Tq, class Tqp, class Tf>
  void fluxAdvectiveUpwindLinear(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& dq,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    Tf C1 = 0;
    Tq eigu = 0;
    ArrayQ<Tf> du = 0;
    fluxAdvectiveUpwindLinear(x, y, time, qL, dq, nx, ny, f, C1, eigu, du);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, Tq& C1, Tq& eigu, const Real& scaleFactor ) const;

  template <class Tq, class Tqp, class Tf>
  void fluxAdvectiveUpwindLinear(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& dq,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f, Tf& C1, Tq& eigu,
      ArrayQ<Tf>& du ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const
  {
    SANS_DEVELOPER_EXCEPTION( "Spacetime Roe Scheme Not Yet Implemented" );
  }

private:
  const GasModel& gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  const RoeEntropyFix entropyFix_;
};


template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
Roe2D<QType, PDETraitsSize>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f, Tq& C1, Tq& eigu, const Real& scaleFactor ) const
{
  Tq rhoL=0, uL=0, vL=0, tL=0, pL, hL, HL, V2L;
  Tq rhoR=0, uR=0, vR=0, tR=0, pR, hR, HR, V2R;

  qInterpret_.eval( qL, rhoL, uL, vL, tL );
  pL  = gas_.pressure(rhoL, tL);
  hL  = gas_.enthalpy(rhoL, tL);
  V2L = uL*uL + vL*vL;
  HL  = hL + 0.5*V2L;

  qInterpret_.eval( qR, rhoR, uR, vR, tR );
  pR  = gas_.pressure(rhoR, tR);
  hR  = gas_.enthalpy(rhoR, tR);
  V2R = uR*uR + vR*vR;
  HR  = hR + 0.5*V2R;

  Tq eL, EL;
  Tq eR, ER;

  eL = gas_.energy(rhoL, tL);
  EL = eL + 0.5*V2L;

  eR = gas_.energy(rhoR, tR);
  ER = eR + 0.5*V2R;

  Real gamma = gas_.gamma();

  f(iCont) += 0.5*(nx*(rhoL*uL        ) + ny*(rhoL*vL        ));
  f(ixMom) += 0.5*(nx*(rhoL*uL*uL + pL) + ny*(rhoL*vL*uL     ));
  f(iyMom) += 0.5*(nx*(rhoL*uL*vL     ) + ny*(rhoL*vL*vL + pL));
  f(iEngy) += 0.5*(nx*(rhoL*uL*HL     ) + ny*(rhoL*vL*HL     ));

  f(iCont) += 0.5*(nx*(rhoR*uR        ) + ny*(rhoR*vR        ));
  f(ixMom) += 0.5*(nx*(rhoR*uR*uR + pR) + ny*(rhoR*vR*uR     ));
  f(iyMom) += 0.5*(nx*(rhoR*uR*vR     ) + ny*(rhoR*vR*vR + pR));
  f(iEngy) += 0.5*(nx*(rhoR*uR*HR     ) + ny*(rhoR*vR*HR     ));

  // TODO: Ideal gas is assumed in the following formulation
  Tq w1L = sqrt(rhoL);
  Tq w1R = sqrt(rhoR);

  // Roe averaged quantities
  Tq u = (w1L*uL + w1R*uR) / (w1L + w1R);
  Tq v = (w1L*vL + w1R*vR) / (w1L + w1R) ;
  Tq H = (w1L*HL + w1R*HR) / (w1L + w1R) ;

  // This assumes calorically perfect gas
  Tq c = sqrt( (gamma - 1.0)*(H - 0.5*(u*u + v*v)) ) ;

  Tq qn = nx*u + ny*v;

  // jump conditions and Roe coefficients
  Tq delrho  = rhoR    - rhoL;
  Tq delrhou = rhoR*uR - rhoL*uL;
  Tq delrhov = rhoR*vR - rhoL*vL;
  Tq delrhoE = rhoR*ER - rhoL*EL;

  Tq eigumc = fabs(qn - c);
  Tq eigupc = fabs(qn + c);
     eigu   = fabs(qn    );

  Real eps0 = 1e-2;
  Tq eps    = c*eps0;

  switch (entropyFix_)
  {
  case eVanLeer:
  {
    // 'entropy fix' of van Leer, Lee and Powell (Sonic Point Capturing)
    //
    // The term
    //
    // deigmin = max(eps, deig);
    //
    // was suggested by Prof Darmofal so that the dissipation does not disappear
    // for eigu == 0
    static const int  FIX = 4; // entropy fix fudge factor

    Tq qnL = nx*uL + ny*vL;
    Tq qnR = nx*uR + ny*vR;
    Tq cL  = gas_.speedofSound(tL);
    Tq cR  = gas_.speedofSound(tR);

    Tq eigL, eigR, deig, deigmin;

    eigL = qnL - cL;
    eigR = qnR - cR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigumc < 0.5*deigmin)
      eigumc = (eigumc*eigumc)/deigmin + 0.25*deigmin;

    eigL = qnL + cL;
    eigR = qnR + cR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigupc < 0.5*deigmin)
      eigupc = (eigupc*eigupc)/deigmin + 0.25*deigmin;

    eigL = qnL;
    eigR = qnR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigu < 0.5*deigmin)
      eigu = (eigu*eigu)/deigmin + 0.25*deigmin;

    break;
  }
  case eHarten:
  {

    /*--------------------------*/
    /*  Harten (PX) Entropy Fix */
    /*--------------------------*/

    if (eigumc < eps)
      eigumc = 0.5*(eps + eigumc*eigumc/eps);
    //eigumc = (eigumc*eigumc + eps*eps) / (2*eps);

    if (eigupc < eps)
      eigupc = 0.5*(eps + eigupc*eigupc/eps);
    //eigupc = (eigupc*eigupc + eps*eps) / (2*eps);

    if (eigu < eps)
      eigu = 0.5*(eps + eigu*eigu/eps);
    //eigu = (eigu*eigu + eps*eps) / (2*eps);

    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown entropy fix.");
  }

  // Roe dissipation flux following Fidkowski
  Tq qq = 0.5*(u*u + v*v) ;
  Tq G1 = (gamma-1.0)/(c*c)*(delrho*qq - delrhou*u - delrhov*v + delrhoE);
  Tq G2 = (delrhou*nx + delrhov*ny - delrho*qn);
  Tq s1 = 0.5*(eigupc + eigumc);
  Tq s2 = 0.5*(eigupc - eigumc);
     C1 = G2*s2/c + G1*(s1 - eigu);
  Tq C2 = c*G1*s2 + G2*(s1 - eigu);

  f(iCont) -= 0.5*(           C1 + delrho *eigu )*scaleFactor;
  f(ixMom) -= 0.5*( nx*C2 + u*C1 + delrhou*eigu )*scaleFactor;
  f(iyMom) -= 0.5*( ny*C2 + v*C1 + delrhov*eigu )*scaleFactor;
  f(iEngy) -= 0.5*( qn*C2 + H*C1 + delrhoE*eigu )*scaleFactor;

#if 0 //Steve's alternate formulation
  T alpha1   = 0.5*(delp   - delqn) * eigumc;
  T alpha2   = 0.5*(delp   + delqn) * eigupc;
  T alpha3   =     (delrho - delp ) * eigu  ;
  T alpha4   =     (delqs         ) * eigu  ;

  /*  Roe dissipation flux  */
  T qq     = 0.5*(u*u + v*v) ;
  flx1_dsp = al1            + al2            + al3                ;
  flxs_dsp = al1*(qs      ) + al2*(qs      ) + al3*(qs) + al4     ;
  flxn_dsp = al1*(qn - c  ) + al2*(qn + c  ) + al3*(qn)           ;
  flx4_dsp = al1*(H - c*qn) + al2*(H + c*qn) + al3*(qq) + al4*(qs);

  f(0) -= 0.5*(ds*flx1_dsp              );
  f(1) -= 0.5*(dx*flxs_dsp + dy*flxn_dsp);
  f(2) -= 0.5*(dy*flxs_dsp - dx*flxn_dsp);
  f(3) -= 0.5*(ds*flx4_dsp              );
#endif
}



template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tqp, class Tf>
inline void
Roe2D<QType, PDETraitsSize>::fluxAdvectiveUpwindLinear(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tqp>& dq,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f, Tf& C1, Tq& eigu,
    ArrayQ<Tf>& du) const
{
//  typedef typename promote_Surreal<Tq,Tqp>::type T;

  qInterpret_.conservativePerturbation(qL, dq, du);

  Tq rho=0, u=0, v=0, t=0, p, h, H, V2;

  qInterpret_.eval( qL, rho, u, v, t );
  p  = gas_.pressure(rho, t);
  h  = gas_.enthalpy(rho, t);
  V2 = u*u + v*v;
  H  = h + 0.5*V2;

  Real gamma = gas_.gamma();

  f(iCont) += nx*(rho*u        ) + ny*(rho*v        );
  f(ixMom) += nx*(rho*u*u + p) + ny*(rho*v*u     );
  f(iyMom) += nx*(rho*u*v     ) + ny*(rho*v*v + p);
  f(iEngy) += nx*(rho*u*H     ) + ny*(rho*v*H     );

  // This assumes calorically perfect gas
  Tq c = sqrt( (gamma - 1.0)*(H - 0.5*(u*u + v*v)) ) ;

  Tq qn = nx*u + ny*v;

  // jump conditions and Roe coefficients
  Tf delrho  = du[0];
  Tf delrhou = du[1];
  Tf delrhov = du[2];
  Tf delrhoE = du[3];

  Tq eigumc = fabs(qn - c);
  Tq eigupc = fabs(qn + c);
     eigu   = fabs(qn    );

  Real eps0 = 1e-2;
  Tq eps    = c*eps0;

  switch (entropyFix_)
  {
  case eVanLeer:
  {
    // 'entropy fix' of van Leer, Lee and Powell (Sonic Point Capturing)
    //
    // The term
    //
    // deigmin = max(eps, deig);
    //
    // was suggested by Prof Darmofal so that the dissipation does not disappear
    // for eigu == 0
    if (eigumc < 0.5*eps)
      eigumc = (eigumc*eigumc)/eps + 0.25*eps;

    if (eigupc < 0.5*eps)
      eigupc = (eigupc*eigupc)/eps + 0.25*eps;

    if (eigu < 0.5*eps)
      eigu = (eigu*eigu)/eps + 0.25*eps;

    break;
  }
  case eHarten:
  {

    /*--------------------------*/
    /*  Harten (PX) Entropy Fix */
    /*--------------------------*/

    if (eigumc < eps)
      eigumc = 0.5*(eps + eigumc*eigumc/eps);
    //eigumc = (eigumc*eigumc + eps*eps) / (2*eps);

    if (eigupc < eps)
      eigupc = 0.5*(eps + eigupc*eigupc/eps);
    //eigupc = (eigupc*eigupc + eps*eps) / (2*eps);

    if (eigu < eps)
      eigu = 0.5*(eps + eigu*eigu/eps);
    //eigu = (eigu*eigu + eps*eps) / (2*eps);

    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION("Unknown entropy fix.");
  }

  // Roe dissipation flux following Fidkowski
  Tq qq = 0.5*(u*u + v*v) ;
  Tf G1 = (gamma-1.0)/(c*c)*(delrho*qq - delrhou*u - delrhov*v + delrhoE);
  Tf G2 = (delrhou*nx + delrhov*ny - delrho*qn);
  Tq s1 = 0.5*(eigupc + eigumc);
  Tq s2 = 0.5*(eigupc - eigumc);
     C1 = G2*s2/c + G1*(s1 - eigu);
  Tf C2 = c*G1*s2 + G2*(s1 - eigu);

  f(iCont) += (           C1 + delrho *eigu );
  f(ixMom) += ( nx*C2 + u*C1 + delrhou*eigu );
  f(iyMom) += ( ny*C2 + v*C1 + delrhov*eigu );
  f(iEngy) += ( qn*C2 + H*C1 + delrhoE*eigu );
}

template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
Roe2D<QType, PDETraitsSize>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{

  SANS_DEVELOPER_EXCEPTION( "Spacetime Roe Scheme Not Yet Implemented" );
//  T rhoL, uL, vL, tL, pL, hL, HL = 0;
//  T rhoR, uR, vR, tR, pR, hR, HR = 0;
//
//  qInterpret_.eval( qL, rhoL, uL, vL, tL );
//  pL  = gas_.pressure(rhoL, tL);
//  hL  = gas_.enthalpy(rhoL, tL);
//  HL = hL + 0.5*(uL*uL + vL*vL);
//
//  qInterpret_.eval( qR, rhoR, uR, vR, tR );
//  pR  = gas_.pressure(rhoR, tR);
//  hR  = gas_.enthalpy(rhoR, tR);
//  HR = hR + 0.5*(uR*uR + vR*vR);
//
//  f(0) += 0.5*(nx*(rhoL*uL        ) + ny*(rhoL*vL        ));
//  f(1) += 0.5*(nx*(rhoL*uL*uL + pL) + ny*(rhoL*vL*uL     ));
//  f(2) += 0.5*(nx*(rhoL*uL*vL     ) + ny*(rhoL*vL*vL + pL));
//  f(3) += 0.5*(nx*(rhoL*uL*HL     ) + ny*(rhoL*vL*HL     ));
//
//  f(0) += 0.5*(nx*(rhoR*uR        ) + ny*(rhoR*vR        ));
//  f(1) += 0.5*(nx*(rhoR*uR*uR + pR) + ny*(rhoR*vR*uR     ));
//  f(2) += 0.5*(nx*(rhoR*uR*vR     ) + ny*(rhoR*vR*vR + pR));
//  f(3) += 0.5*(nx*(rhoR*uR*HR     ) + ny*(rhoR*vR*HR     ));
}


template <class QType, template <class> class PDETraitsSize>
class Roe3D
{
public:
  typedef PhysD3 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef Q3D<QType, PDETraitsSize> QInterpret;                           // solution variable interpreter

  explicit Roe3D(const GasModel& gas0, const RoeEntropyFix entropyFix) :
        gas_(gas0),
        qInterpret_(gas0),
        entropyFix_(entropyFix)
  {
    // Nothing
  }

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iyMom = 2;
  static const int izMom = 3;
  static const int iEngy = 4;

  Roe3D( const Roe3D& ) = delete;
  Roe3D& operator=( const Roe3D& ) = delete;

  RoeEntropyFix entropyFix() const { return entropyFix_; }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f, const Real& scale ) const
  {
    T C1, eigu;
    fluxAdvectiveUpwind(x, y, z, time, qL, qR, nx, ny, nz, f, C1, eigu, scale);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f, Tq& C1, Tq& eigu, const Real& scale ) const;

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt, ArrayQ<Tf>& f ) const;

  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz, const Real& nt, MatrixQ<Tf>& mtx ) const
  {
    SANS_DEVELOPER_EXCEPTION( "Spacetime Roe Scheme Not Yet Implemented" );
  }

private:
  const GasModel& gas_;
  const QInterpret qInterpret_;   // solution variable interpreter class
  const RoeEntropyFix entropyFix_;
};


template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
Roe3D<QType, PDETraitsSize>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f, Tq& C1, Tq& eigu, const Real& scale ) const
{
  Tq rhoL=0, uL=0, vL=0, wL=0, tL=0, pL, hL, HL, V2L;
  Tq rhoR=0, uR=0, vR=0, wR=0, tR=0, pR, hR, HR, V2R;

  qInterpret_.eval( qL, rhoL, uL, vL, wL, tL );
  pL  = gas_.pressure(rhoL, tL);
  hL  = gas_.enthalpy(rhoL, tL);
  V2L = (uL*uL + vL*vL + wL*wL);
  HL  = hL + 0.5*V2L;

  qInterpret_.eval( qR, rhoR, uR, vR, wR, tR );
  pR  = gas_.pressure(rhoR, tR);
  hR  = gas_.enthalpy(rhoR, tR);
  V2R = (uR*uR + vR*vR + wR*wR);
  HR  = hR + 0.5*V2R;

  Tq eL, EL;
  Tq eR, ER;

  eL = gas_.energy(rhoL, tL);
  EL = eL + 0.5*V2L;

  eR = gas_.energy(rhoR, tR);
  ER = eR + 0.5*V2R;

  Real gamma = gas_.gamma();

  f(iCont) += 0.5*(nx*(rhoL*uL        ) + ny*(rhoL*vL        ) + nz*(rhoL*wL        ));
  f(ixMom) += 0.5*(nx*(rhoL*uL*uL + pL) + ny*(rhoL*vL*uL     ) + nz*(rhoL*wL*uL     ));
  f(iyMom) += 0.5*(nx*(rhoL*uL*vL     ) + ny*(rhoL*vL*vL + pL) + nz*(rhoL*wL*vL     ));
  f(izMom) += 0.5*(nx*(rhoL*uL*wL     ) + ny*(rhoL*vL*wL     ) + nz*(rhoL*wL*wL + pL));
  f(iEngy) += 0.5*(nx*(rhoL*uL*HL     ) + ny*(rhoL*vL*HL     ) + nz*(rhoL*wL*HL     ));

  f(iCont) += 0.5*(nx*(rhoR*uR        ) + ny*(rhoR*vR        ) + nz*(rhoR*wR        ));
  f(ixMom) += 0.5*(nx*(rhoR*uR*uR + pR) + ny*(rhoR*vR*uR     ) + nz*(rhoR*wR*uR     ));
  f(iyMom) += 0.5*(nx*(rhoR*uR*vR     ) + ny*(rhoR*vR*vR + pR) + nz*(rhoR*wR*vR     ));
  f(izMom) += 0.5*(nx*(rhoR*uR*wR     ) + ny*(rhoR*vR*wR     ) + nz*(rhoR*wR*wR + pR));
  f(iEngy) += 0.5*(nx*(rhoR*uR*HR     ) + ny*(rhoR*vR*HR     ) + nz*(rhoR*wR*HR     ));

  // TODO: Ideal gas is assumed in the following formulation
  Tq w1L = sqrt(rhoL);
  Tq w1R = sqrt(rhoR);

  // Roe averaged quantities
  Tq u = (w1L*uL + w1R*uR) / (w1L + w1R);
  Tq v = (w1L*vL + w1R*vR) / (w1L + w1R) ;
  Tq w = (w1L*wL + w1R*wR) / (w1L + w1R) ;
  Tq H = (w1L*HL + w1R*HR) / (w1L + w1R) ;

  // This assumes calorically perfect gas
  Tq c = sqrt( (gamma - 1.0)*(H - 0.5*(u*u + v*v + w*w)) ) ;

  Tq qn = nx*u + ny*v + nz*w;

  // jump conditions and Roe coefficients
  Tq delrho  = rhoR    - rhoL;
  Tq delrhou = rhoR*uR - rhoL*uL;
  Tq delrhov = rhoR*vR - rhoL*vL;
  Tq delrhow = rhoR*wR - rhoL*wL;
  Tq delrhoE = rhoR*ER - rhoL*EL;

  Tq eigumc = fabs(qn - c);
  Tq eigupc = fabs(qn + c);
     eigu   = fabs(qn    );

  Real eps0 = 1e-2;
  Tq eps    = c*eps0;

  switch (entropyFix_)
  {
  case eVanLeer:
  {
    // 'entropy fix' of van Leer, Lee and Powell (Sonic Point Capturing)
    //
    // The term
    //
    // deigmin = max(eps, deig);
    //
    // was suggested by Prof Darmofal so that the dissipation does not disappear
    // for eigu == 0
    static const int  FIX = 4; // entropy fix fudge factor

    Tq qnL = nx*uL + ny*vL + nz*wL;
    Tq qnR = nx*uR + ny*vR + nz*wR;
    Tq cL  = gas_.speedofSound(tL);
    Tq cR  = gas_.speedofSound(tR);

    Tq eigL, eigR, deig, deigmin;

    eigL = qnL - cL;
    eigR = qnR - cR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigumc < 0.5*deigmin)
      eigumc = (eigumc*eigumc)/deigmin + 0.25*deigmin;

    eigL = qnL + cL;
    eigR = qnR + cR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigupc < 0.5*deigmin)
      eigupc = (eigupc*eigupc)/deigmin + 0.25*deigmin;

    eigL = qnL;
    eigR = qnR;
    deig = FIX * fabs( eigR - eigL );
    deigmin = max(eps, deig);
    if (eigu < 0.5*deigmin)
      eigu = (eigu*eigu)/deigmin + 0.25*deigmin;

    break;
  }
  case eHarten:
  {

    /*--------------------------*/
    /*  Harten (PX) Entropy Fix */
    /*--------------------------*/

    if (eigumc < eps)
      eigumc = 0.5*(eps + eigumc*eigumc/eps);
    //eigumc = (eigumc*eigumc + eps*eps) / (2*eps);

    if (eigupc < eps)
      eigupc = 0.5*(eps + eigupc*eigupc/eps);
    //eigupc = (eigupc*eigupc + eps*eps) / (2*eps);

    if (eigu < eps)
      eigu = 0.5*(eps + eigu*eigu/eps);
    //eigu = (eigu*eigu + eps*eps) / (2*eps);

    break;
  }
  default:
    SANS_DEVELOPER_EXCEPTION("Uknown entropy fix.");
  }

  // Roe dissipation flux following Fidkowski
  Tq qq = 0.5*(u*u + v*v + w*w) ;
  Tq G1 = (gamma-1.0)/(c*c)*(delrho*qq - delrhou*u - delrhov*v - delrhow*w + delrhoE);
  Tq G2 = (delrhou*nx + delrhov*ny + delrhow*nz - delrho*qn);
  Tq s1 = 0.5*(eigupc + eigumc);
  Tq s2 = 0.5*(eigupc - eigumc);
     C1 = G2*s2/c + G1*(s1 - eigu);
  Tq C2 = c*G1*s2 + G2*(s1 - eigu);

  f(iCont) -= 0.5*(           C1 + delrho *eigu )*scale;
  f(ixMom) -= 0.5*( nx*C2 + u*C1 + delrhou*eigu )*scale;
  f(iyMom) -= 0.5*( ny*C2 + v*C1 + delrhov*eigu )*scale;
  f(izMom) -= 0.5*( nz*C2 + w*C1 + delrhow*eigu )*scale;
  f(iEngy) -= 0.5*( qn*C2 + H*C1 + delrhoE*eigu )*scale;
}

template <class QType, template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
Roe3D<QType, PDETraitsSize>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nz, const Real& nt, ArrayQ<Tf>& f ) const
{

  SANS_DEVELOPER_EXCEPTION( "Spacetime Roe Scheme Not Yet Implemented" );
//  T rhoL, uL, vL, tL, pL, hL, HL = 0;
//  T rhoR, uR, vR, tR, pR, hR, HR = 0;
//
//  qInterpret_.eval( qL, rhoL, uL, vL, tL );
//  pL  = gas_.pressure(rhoL, tL);
//  hL  = gas_.enthalpy(rhoL, tL);
//  HL = hL + 0.5*(uL*uL + vL*vL);
//
//  qInterpret_.eval( qR, rhoR, uR, vR, tR );
//  pR  = gas_.pressure(rhoR, tR);
//  hR  = gas_.enthalpy(rhoR, tR);
//  HR = hR + 0.5*(uR*uR + vR*vR);
//
//  f(0) += 0.5*(nx*(rhoL*uL        ) + ny*(rhoL*vL        ));
//  f(1) += 0.5*(nx*(rhoL*uL*uL + pL) + ny*(rhoL*vL*uL     ));
//  f(2) += 0.5*(nx*(rhoL*uL*vL     ) + ny*(rhoL*vL*vL + pL));
//  f(3) += 0.5*(nx*(rhoL*uL*HL     ) + ny*(rhoL*vL*HL     ));
//
//  f(0) += 0.5*(nx*(rhoR*uR        ) + ny*(rhoR*vR        ));
//  f(1) += 0.5*(nx*(rhoR*uR*uR + pR) + ny*(rhoR*vR*uR     ));
//  f(2) += 0.5*(nx*(rhoR*uR*vR     ) + ny*(rhoR*vR*vR + pR));
//  f(3) += 0.5*(nx*(rhoR*uR*HR     ) + ny*(rhoR*vR*HR     ));
}


}
#endif // ROE_H
