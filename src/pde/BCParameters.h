// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCPARAMTETERS_H
#define BCPARAMTETERS_H

//Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "BCCategory.h"
#include "tools/add_decorator.h"

#include <map>
#include <string>
#include <memory> // shared_ptr

#include <boost/mpl/inherit_linearly.hpp>
#include <boost/mpl/inherit.hpp>
#include <boost/mpl/deref.hpp>
#include <boost/mpl/vector.hpp>

#include <boost/mpl/lambda.hpp>
#include <boost/mpl/transform.hpp>

namespace SANS
{

// Template meta function to extract the category from a type
template<class BC>
struct getBCOption
{
  typedef typename BC::ParamsType::Option type;
};

template<class BCVector>
struct BCParameters
{
  BCParameters(const BCParameters&) = delete;
  BCParameters& operator=(const BCParameters&) = delete;

  // Create a vector with the Option from each BC type
  // lambda is required here to prevent getBCOption from trying to define the type 'boost::mpl::_1::ParamsType::Option', which does not exist
  typedef typename boost::mpl::transform< BCVector, boost::mpl::lambda< getBCOption<boost::mpl::_1> > >::type OptionsVector;

  // inherit_linearly transforms into : public BCParams0::Options, BCParams1::Options, BCParams2::Options, BCParams3::Options
  // so that BCOptions inherits from all BCParam::Option classes
  struct BCOptions : public boost::mpl::inherit_linearly<OptionsVector,
                                                         boost::mpl::inherit< boost::mpl::_1, boost::mpl::_2 >
                                                        >::type
  {
    typedef DictKeyPair ExtractType;
    const ParameterString BCType = ParameterString("BCType", NO_DEFAULT, "The Boundary Condition type" );
    const ParameterString& key = BCType;

    // The options are provided through the inheritance, do not add any here

    BCOptions();
    const std::vector<DictOption> options; // options is filled in the constructor
  };
  // This is a dummy placeholder to use when creating dictionaries
  const ParameterOption<BCOptions> BC{"BC", NO_DEFAULT, "Dummy parameter used for creating BC dictionaries"};

  static void checkInputs(PyDict d);

  // Allocates each of the BCs requested
  template<template<class,class> class BCNDConvert, class PDE, class... BCArgs>
  static std::map< std::string, std::shared_ptr<BCBase> > createBCs(const PDE& pde, const PyDict& PyBCList, BCArgs&... args);

  // Retrieves the boundary groups that require Lagrange multipliers
  static std::vector<int> getLGBoundaryGroups(const PyDict& PyBCList,
                                              const std::map< std::string, std::vector<int> >& BoundaryGroups );

  static BCParameters params;

protected:
  BCParameters() {} // singleton!
};


namespace BCParameters_detail
{

// Looping class. Get the current BC type check if it matches the BC parameter
template < class iter, class end>
struct BCConstructor
{
  template<class PDE, class... BCArgs>
  static std::shared_ptr<BCBase> create(const PDE& pde, const DictKeyPair& BC, BCArgs&... args )
  {
    typedef typename boost::mpl::deref<iter>::type currentBCType;

    // Check if the BC matches the name of the current BC type
    if ( BC == currentBCType::ParamsType::BCName )
      return std::shared_ptr<BCBase>( static_cast<BCBase*>( new currentBCType( args..., pde, (PyDict&)BC) ) );

    // Iterate recursively to the next option
    return BCConstructor< typename boost::mpl::next<iter>::type, end >::create(pde, BC, args...);
  }
};

// Recursive loop terminates when 'iter' is the same as 'end'
template < class end >
struct BCConstructor< end, end >
{
  template<class PDE, class... BCArgs>
  static std::shared_ptr<BCBase> create(const PDE& pde, const DictKeyPair& BC, BCArgs&... args )
  {
    SANS_DEVELOPER_EXCEPTION("Should not be possible to get here. The BC dictionary must not have been checked with checkInputs.");
    return std::shared_ptr<BCBase>(NULL);
  }
};

} // namespace BCParameters_detail

template<class BCVector>
template<template<class,class> class BCNDConvert, class PDE, class... BCArgs>
std::map< std::string, std::shared_ptr<BCBase> >
BCParameters<BCVector>::createBCs(const PDE& pde, const PyDict& PyBCList, BCArgs&... args)
{
  // Remove SpaceTime BCs if this is only a spatial configuration
  typedef typename BCVectorSanitizeSpaceTime<BCNDConvert, BCVector>::type BCVectorSanitized;

  // Decorate the BCVector with the NDConvert class
  typedef typename boost::mpl::transform< BCVectorSanitized, add_ND_decorator<BCNDConvert, boost::mpl::_1> >::type BCNDVector;

  // Get the start and end iterators from the BCVector
  typedef typename boost::mpl::begin< BCNDVector >::type begin;
  typedef typename boost::mpl::end< BCNDVector >::type   end;

  // The map of boundary conditions
  std::map< std::string, std::shared_ptr<BCBase> > BCs;

  // Extract the keys from the dictionary
  std::vector<std::string> keys = PyBCList.stringKeys();

  for (std::size_t i = 0; i < keys.size(); i++)
  {
    // Create a BC parameter for the given key
    const ParameterOption<BCOptions> BCParam{keys[i], NO_DEFAULT, "Boundary Condition Dictionary"};

    DictKeyPair BC = PyBCList.get(BCParam);

    // Check the parameter
    BCs[keys[i]] = BCParameters_detail::BCConstructor<begin, end>::create(pde, BC, args...);
  }

  return BCs;
}

} //namespace SANS

#endif //BCPARAMTETERS_H
