// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCNONE_H
#define BCNONE_H

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include "BCCategory.h"

namespace SANS
{
//----------------------------------------------------------------------------//
// None BC:
//
struct BCNoneParams : noncopyable
{
  static constexpr const char* BCName{"None"};
  struct Option
  {
    const DictOption None{BCNoneParams::BCName, BCNoneParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCNoneParams params;
};

template<class PhysDim_, int N_>
class BCNone : public BCType< BCNone<PhysDim_,N_> >
{
public:
  typedef typename BCCategory::None Category;
  typedef BCNoneParams ParamsType;

  typedef PhysDim_ PhysDim;
  static const int D = PhysDim::D;
  static const int N = N_; // number of equations

  static const int NBC = 0; // total BCs

  template <class T>
  using ArrayQ = typename DLA::VectorS_or_T<N_,T>::type;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename DLA::MatrixS_or_T<N_,N_,T>::type; // matrices

  template<class PDE>
  BCNone(const PDE& pde, const PyDict& d ) : useFluxViscous_(false){}

  BCNone(const bool useFluxViscous = false) : useFluxViscous_(useFluxViscous){}

  virtual ~BCNone() {}

  BCNone& operator=( const BCNone& ) = delete;

    // is the viscous flux used in the integral
  bool useFluxViscous() const { return useFluxViscous_; }

  // is the boundary state valid
  template < class ArrayQ >
  bool isValidState( const Real& nx, const ArrayQ& q ) const { return true; }
  template < class ArrayQ >
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ& q ) const { return true; }
  template < class ArrayQ >
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ& q ) const { return true; }
  template < class ArrayQ >
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const Real& nt, const ArrayQ& q ) const { return true; }

  void dump( int indentSize, std::ostream& out ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCNone<PhysD" << D << "," << N <<">" << std::endl;
  }
protected:
  const bool useFluxViscous_;
};

}

#endif //BCNONE_H
