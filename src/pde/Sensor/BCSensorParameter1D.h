// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCSensorParameter1D_H
#define BCSensorParameter1D_H

// 1-D Advection-Diffusion BC class

//PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "SensorParameter_Traits.h"
#include "Sensor_ViscousFlux1D.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "pde/BCCategory.h"
#include "pde/BCNone.h"

#include "tools/Tuple.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 1-D Advection-Diffusion
//
// template parameters:
//   PhysDim              Physical dimensions
//   BCType               BC type (e.g. BCTypeLinearRobin)
//----------------------------------------------------------------------------//

template <class PhysDim, class BCType>
class BCSensorParameter;

template <class PhysDim, class BCType>
struct BCSensorParameterParams;

//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeLinearRobin_mitLG;
class BCTypeLinearRobin_sansLG;

// Define the vector of boundary conditions
template<class AdvectiveFlux, class ViscousFlux>
using BCSensorParameter1DVector = boost::mpl::vector3< BCNone<PhysD1,SensorParameterTraits<PhysD1>::N>,
                                                       BCSensorParameter<PhysD1, BCTypeLinearRobin_mitLG>,
                                                       BCSensorParameter<PhysD1, BCTypeLinearRobin_sansLG>
                                                     >;

//----------------------------------------------------------------------------//
// Robin BC:  A u + B (kn un + ks us) = bcdata
//
// coefficients A and B are arbitrary with the exception that they cannot
// simultaneously vanish
template<>
struct BCSensorParameterParams<PhysD1, BCTypeLinearRobin_mitLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_mitLG"};
  struct Option
  {
    const DictOption LinearRobin_mitLG{BCSensorParameterParams::BCName, BCSensorParameterParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCSensorParameterParams params;
};

template<>
struct BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG> : noncopyable
{
  const ParameterNumeric<Real> A{"A", NO_DEFAULT, NO_RANGE, "Value coefficient"};
  const ParameterNumeric<Real> B{"B", NO_DEFAULT, NO_RANGE, "Viscous coefficient"};
  const ParameterNumeric<Real> bcdata{"bcdata", NO_DEFAULT, NO_RANGE, "BC data"};

  static constexpr const char* BCName{"LinearRobin_sansLG"};
  struct Option
  {
    const DictOption LinearRobin_sansLG{BCSensorParameterParams::BCName, BCSensorParameterParams::checkInputs};
  };

  static void checkInputs(PyDict d);
  static BCSensorParameterParams params;
};

template <class PhysDim>
class BCSensorParameter_LinearRobinBase;

template <>
class BCSensorParameter_LinearRobinBase<PhysD1>
{
public:
  typedef PhysD1 PhysDim;
  static const int D = SensorParameterTraits<PhysD1>::D;   // physical dimensions
  static const int N = SensorParameterTraits<PhysD1>::N;   // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::template MatrixQ<T>; // matrices

  BCSensorParameter_LinearRobinBase( const Real& A, const Real& B, const Real& bcdata ) :
    A_(A), B_(B), bcdata_(bcdata) {}

  virtual ~BCSensorParameter_LinearRobinBase() {}

  BCSensorParameter_LinearRobinBase& operator=( const BCSensorParameter_LinearRobinBase& );

  // BC coefficients:  A u + B (kn un + ks us)
  template <class T, class L>
  void coefficients(
      const L& param, const Real& x, const Real& time, const Real& nx,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T, class L>
  void data( const L& param, const Real& x, const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  MatrixQ<Real> A_;
  MatrixQ<Real> B_;
  ArrayQ<Real> bcdata_;
};


template <class T, class L>
inline void
BCSensorParameter_LinearRobinBase<PhysD1>::coefficients(
    const L& param, const Real&, const Real& time, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = B_;
}


template <class T, class L>
inline void
BCSensorParameter_LinearRobinBase<PhysD1>::data(
    const L& param, const Real&, const Real& time, const Real& nx, ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}

template <>
class BCSensorParameter<PhysD1, BCTypeLinearRobin_mitLG> : public BCType< BCSensorParameter<PhysD1, BCTypeLinearRobin_mitLG> >,
                                                              public BCSensorParameter_LinearRobinBase<PhysD1>
{
public:
  typedef BCSensorParameter_LinearRobinBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_mitLG Category;
  typedef BCSensorParameterParams<PhysD1, BCTypeLinearRobin_mitLG> ParamsType;

  template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
  BCSensorParameter(const PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCSensorParameter( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

template <>
class BCSensorParameter<PhysD1, BCTypeLinearRobin_sansLG> : public BCType< BCSensorParameter<PhysD1, BCTypeLinearRobin_sansLG> >,
                                                               public BCSensorParameter_LinearRobinBase<PhysD1>
{
public:
  typedef BCSensorParameter_LinearRobinBase<PhysD1> BaseType;
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG> ParamsType;

  template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
  BCSensorParameter(const PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>& pde, const PyDict& d ) :
    BaseType( d.get(ParamsType::params.A), d.get(ParamsType::params.B), d.get(ParamsType::params.bcdata)) {}

  BCSensorParameter( const Real& A, const Real& B, const Real& bcdata ) : BaseType( A, B, bcdata ) {}
};

} //namespace SANS

#endif  // BCSensorParameter1D_H
