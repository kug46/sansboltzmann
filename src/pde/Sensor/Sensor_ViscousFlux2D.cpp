// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// 2D diffusion matrix

#include <string>
#include <iostream>

#include "Sensor_ViscousFlux2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// diffusion matrix: none
void
Sensor_DiffusionMatrix2D_None::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Sensor_DiffusionMatrix2D_None:" << std::endl;
}

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
void
Sensor_DiffusionMatrix2D_Uniform::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Sensor_DiffusionMatrix2D_Uniform:"
                << "  kxx_ = " << kxx_
                << "  kxy_ = " << kxy_
                << "  kyx_ = " << kyx_
                << "  kyy_ = " << kyy_ << std::endl;
}

//----------------------------------------------------------------------------//
// diffusion matrix: Grid Dependent
void
Sensor_DiffusionMatrix2D_GridDependent::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Sensor_DiffusionMatrix2D_GridDependent:"
                << "  C_ = " << C_ << std::endl;
}

//----------------------------------------------------------------------------//
// diffusion matrix: with generalized h-scale
void
Sensor_DiffusionMatrix2D_GenHScale::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Sensor_DiffusionMatrix2D_GenHScale:"
                << "  C_ = " << C_ << std::endl;
}

} //namespace SANS
