// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCSensorParameter2D.h"

#include "pde/Sensor/Sensor_AdvectiveFlux2D.h"
#include "pde/Sensor/Sensor_ViscousFlux2D.h"
#include "pde/Sensor/Sensor_Source2D.h"
#include "Source2D_JumpSensor.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
//===========================================================================//
//
// Parameter classes
//
// cppcheck-suppress passedByValue
void BCSensorParameterParams<PhysD2, BCTypeLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCSensorParameterParams<PhysD2, BCTypeLinearRobin_mitLG> BCSensorParameterParams<PhysD2, BCTypeLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG> BCSensorParameterParams<PhysD2, BCTypeLinearRobin_sansLG>::params;


//===========================================================================//
// Instantiate the BC parameters
typedef BCSensorParameter2DVector<Sensor_AdvectiveFlux2D_Uniform, Sensor_ViscousFlux2D_GenHScale> BCVector;
BCPARAMETER_INSTANTIATE( BCVector )

} //namespace SANS
