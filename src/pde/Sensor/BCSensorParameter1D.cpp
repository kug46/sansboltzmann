// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCSensorParameter1D.h"

#include "pde/Sensor/Sensor_AdvectiveFlux1D.h"
#include "pde/Sensor/Sensor_ViscousFlux1D.h"
#include "pde/Sensor/Sensor_Source1D.h"
#include "Source1D_JumpSensor.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{
//===========================================================================//
//
// Parameter classes
//
// cppcheck-suppress passedByValue
void BCSensorParameterParams<PhysD1, BCTypeLinearRobin_mitLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCSensorParameterParams<PhysD1, BCTypeLinearRobin_mitLG> BCSensorParameterParams<PhysD1, BCTypeLinearRobin_mitLG>::params;

// cppcheck-suppress passedByValue
void BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.A));
  allParams.push_back(d.checkInputs(params.B));
  allParams.push_back(d.checkInputs(params.bcdata));
  d.checkUnknownInputs(allParams);
}
BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG> BCSensorParameterParams<PhysD1, BCTypeLinearRobin_sansLG>::params;


//===========================================================================//
// Instantiate the BC parameters
typedef BCSensorParameter1DVector<Sensor_AdvectiveFlux1D_None, Sensor_ViscousFlux1D_None> BCVector;
BCPARAMETER_INSTANTIATE( BCVector )

//===========================================================================//

} //namespace SANS
