// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SENSOR_SOURCE2D_H
#define SENSOR_SOURCE2D_H

// 1-D Advection-Diffusion PDE: source term

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/PromoteSurreal.h"
#include "SensorParameter_Traits.h"
#include "SensorParameter_Traits.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: source
//
// member functions:
//   .source                    source term
//   .sourceTrace      dual-consistent source term
//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//
// Source: None
//
// No source term
//----------------------------------------------------------------------------//

class Sensor_Source2D_None
{
public:
  template<class T>
  using ArrayQ = SensorParameterTraits<PhysD2>::ArrayQ<T>;

  template<class T>
  using MatrixQ = SensorParameterTraits<PhysD2>::MatrixQ<T>;

  bool hasSourceTerm() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Tpde, class Th>
  void source( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const
  {
    source += 0;
  }

  template <class T, class Tpde, class Ts, class Th>
  void sourceTrace(const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& paramL, const Real& xL, const Real& yL,
                   const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& paramR, const Real& xR, const Real& yR, const Real& time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL,
                   const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR,
                   ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL += 0;
    sourceR += 0;
  }

  template <class T, class Tpde, class Th>
  void jacobianSource(
      const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const
  {
    dsdu += 0;
  }
//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

};

//----------------------------------------------------------------------------//
// Source: Unform
//
// source = a*q where 'a' is a constant
//----------------------------------------------------------------------------//

class Sensor_Source2D_Uniform
{
public:
  template<class T>
  using ArrayQ = SensorParameterTraits<PhysD2>::ArrayQ<T>;

  template<class T>
  using MatrixQ = SensorParameterTraits<PhysD2>::MatrixQ<T>;

  explicit Sensor_Source2D_Uniform( const Real a) : a_(a) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Tpde, class Th>
  void source( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ< typename promote_Surreal<Tq,Tg>::type >& source ) const
  {
    source += a_*q;
  }

  template <class T, class Tpde, class Ts, class Th>
  void sourceTrace(const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& paramL, const Real& xL, const Real& yL,
                   const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& paramR, const Real& xR, const Real& yR, const Real& time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL,
                   const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR,
                   ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    sourceL += a_*qL;
    sourceR += a_*qR;
  }

  template <class T, class Tpde, class Th>
  void jacobianSource(
      const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const
  {
    dsdu += a_;
  }

protected:
  const Real a_;
};
} //namespace SANS

#endif  // SENSOR_SOURCE2D_H
