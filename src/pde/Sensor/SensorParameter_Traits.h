// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SENSORPARAMETER_TRAITS_H
#define SENSORPARAMETER_TRAITS_H

#include "Topology/Dimension.h"

namespace SANS
{

// Forward declaration
template<class PhysDim, class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
class PDESensorParameter;


template<class PhysDim_>
class SensorParameterTraits
{
public:
  typedef PhysDim_ PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 1;                     // total solution variables

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using MatrixQ = T;   // matrices
};

}

#endif //SENSORPARAMETER_TRAITS_H
