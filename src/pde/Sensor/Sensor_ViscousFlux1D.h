// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SENSOR_DIFFUSIONMATRIX1D_H
#define SENSOR_DIFFUSIONMATRIX1D_H

// 1-D Advection-Diffusion PDE: diffusion matrix

#include "tools/SANSnumerics.h"     // Real
#include "SensorParameter_Traits.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
template<class DiffusionMatrix>
class Sensor_ViscousFlux1D_LinearInGradient : public DiffusionMatrix
{
public:
  using DiffusionMatrix::diffusionViscous;

  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::template MatrixQ<T>;  // matrices

  template< class... Args > // cppcheck-suppress noExplicitConstructor
  Sensor_ViscousFlux1D_LinearInGradient(Args&&... args) : DiffusionMatrix(std::forward<Args>(args)...) {}

  bool fluxViscousLinearInGradient() const { return true; }

  // viscous flux: Fv(Q, QX)
  template <class Th, class Tpde, class Tq, class Tg, class Tf>
  void fluxViscous(
      const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const
  {
    MatrixQ<Tq> kxx = 0.0;

    diffusionViscous(param, x, time, q, qx, kxx );

    f -= kxx*qx;
  }
};

//----------------------------------------------------------------------------//
// diffusion matrix: None
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class Sensor_DiffusionMatrix1D_None
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return false; }

  Sensor_DiffusionMatrix1D_None() {}

  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                         const Real& x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const
  {
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class T, class Tpde, class Th>
  inline void
  strongFluxViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                     const Real& x, const Real time,
                     const ArrayQ<T>& q, const ArrayQ<T>& qx,
                     const ArrayQ<T>& qxx,
                     ArrayQ<T>& strongPDE ) const
  {}

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};

typedef Sensor_ViscousFlux1D_LinearInGradient<Sensor_DiffusionMatrix1D_None> Sensor_ViscousFlux1D_None;

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class Sensor_DiffusionMatrix1D_Uniform
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  explicit Sensor_DiffusionMatrix1D_Uniform( const Real kxx ) :
      kxx_(kxx) {}

  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                         const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const
  {
    kxx += kxx_;
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class T, class Tpde, class Th>
  inline void
  strongFluxViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                     const Real& x, const Real& time,
                     const ArrayQ<T>& q, const ArrayQ<T>& qx,
                     const ArrayQ<T>& qxx,
                     ArrayQ<T>& strongPDE ) const
  {
    strongPDE -= kxx_*qxx;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real kxx_;          // constant diffusion matrix
};

typedef Sensor_ViscousFlux1D_LinearInGradient<Sensor_DiffusionMatrix1D_Uniform> Sensor_ViscousFlux1D_Uniform;

//----------------------------------------------------------------------------//
// diffusion matrix: Grid Dependent
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class Sensor_DiffusionMatrix1D_GridDependent
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  explicit Sensor_DiffusionMatrix1D_GridDependent( const Real C ) :
      C_(C) {}

  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                         const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const
  {
    Th h = get<0>(param);

    kxx += C_*h*h;
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class T, class Tpde, class Th>
  inline void
  strongFluxViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                     const Real& x, const Real& time,
                     const ArrayQ<T>& q, const ArrayQ<T>& qx,
                     const ArrayQ<T>& qxx,
                     ArrayQ<T>& strongPDE ) const
  {
    Th h = get<0>(param);
    strongPDE -= C_*h*h*qxx;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real C_; // constant scaling
};

typedef Sensor_ViscousFlux1D_LinearInGradient<Sensor_DiffusionMatrix1D_GridDependent> Sensor_ViscousFlux1D_GridDependent;

//----------------------------------------------------------------------------//
// diffusion matrix: generalized h-scale dependent
//
// member functions:
//   .()      functor returning diffusion coefficient matrix
//----------------------------------------------------------------------------//

class Sensor_DiffusionMatrix1D_GenHScale
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::template MatrixQ<T>;  // matrices

  static const int D = 1;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  explicit Sensor_DiffusionMatrix1D_GenHScale( const Real C ) : C_(C) {}

  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<DLA::MatrixSymS<D,Th>, ArrayQ<Tpde>, TupleClass<>>& param,
                         const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
                         MatrixQ<Tk>& kxx ) const
  {
    DLA::MatrixSymS<D,Th> H = exp(get<0>(param)); //generalized h-tensor
    DLA::MatrixSymS<D,Th> Hsq = H*H;

    kxx += C_*Hsq(0,0);
  }

  //Overloaded for space-time meshes
  template <class Tq, class Tpde, class Th, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<DLA::MatrixSymS<D+1,Th>, ArrayQ<Tpde>, TupleClass<>>& param,
                         const Real x, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
                         MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt) const
  {
    DLA::MatrixSymS<D+1,Th> H = exp(get<0>(param)); //generalized h-tensor
    DLA::MatrixSymS<D+1,Th> Hsq = H*H;

    kxx += C_*Hsq(0,0); kxt += C_*Hsq(0,1);
    ktx += C_*Hsq(1,0); ktt += C_*Hsq(1,1);
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class T, class Tpde, class Th>
  inline void
  strongFluxViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                     const Real& x, const Real& time,
                     const ArrayQ<T>& q, const ArrayQ<T>& qx,
                     const ArrayQ<T>& qxx,
                     ArrayQ<T>& strongPDE ) const
  {
    SANS_DEVELOPER_EXCEPTION("Sensor_DiffusionMatrix1D_GenHScale::strongFluxViscous - Not implemented yet.");
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real C_; // constant scaling
};

typedef Sensor_ViscousFlux1D_LinearInGradient<Sensor_DiffusionMatrix1D_GenHScale> Sensor_ViscousFlux1D_GenHScale;

} //namespace SANS

#endif  // SENSOR_DIFFUSIONMATRIX1D_H
