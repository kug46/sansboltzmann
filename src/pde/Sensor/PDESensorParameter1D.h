// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDESENSORPARAMETER1D_H
#define PDESENSORPARAMETER1D_H

// 2-D Advection-Diffusion PDE class

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/PromoteSurreal.h"

#include "SensorParameter_Traits.h"

#include "Source1D_JumpSensor.h"
#include "Sensor_AdvectiveFlux1D.h"
#include "Sensor_ViscousFlux1D.h"
#include "Sensor_Source1D.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include "pde/AnalyticFunction/ScalarFunction1D.h"

#include "Topology/ElementTopology.h"
#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/tools/MatrixS_or_T.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 1-D Sensor Parameter PDE
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraits                   this is the PDE that the sensor is being applied to
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
class PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>
{
public:
  typedef PhysD1 PhysDim;
  static const int D = SensorParameterTraits<PhysD1>::D;   // physical dimensions
  static const int N = SensorParameterTraits<PhysD1>::N;   // total solution variables
  static const int Nparam = PDETraits::N;

  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using PDEArrayQ = typename PDETraits::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::template MatrixQ<T>; // matrices

  template <class T>
//  using MatrixParam = T;
  using MatrixParam = typename DLA::MatrixS_or_T<N,Nparam,T>::type;         // e.g. jacobian of PDEs w.r.t. parameters

  typedef PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source> PDEClass;

  PDESensorParameter( const AdvectiveFlux& adv, const ViscousFlux& visc, const Source &src) :
    adv_(adv),
    visc_(visc),
    source_(src)
  {
    // Nothing
  }

  ~PDESensorParameter() {}

  PDESensorParameter& operator=( const PDESensorParameter& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return adv_.hasFluxAdvective(); }
  bool hasFluxViscous() const { return visc_.hasFluxViscous(); }
  bool hasSource() const { return source_.hasSourceTerm(); }
  bool hasSourceTrace() const { return source_.hasSourceTrace(); }
  bool hasSourceLiftedQuantity() const { return false; } //source_.hasSourceLiftedQuantity(); }
  bool hasForcingFunction() const { return false; }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return visc_.fluxViscousLinearInGradient(); }
  bool needsSolutionGradientforSource() const { return source_.needsSolutionGradientforSource(); }

  const AdvectiveFlux& getAdvectiveFlux() const { return adv_; }
  const ViscousFlux& getViscousFlux() const { return visc_; }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tf, class Tpde, class Th>
  void fluxAdvectiveTime(
      const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(param, x, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T, class Tf, class Tpde, class Th>
  void jacobianFluxAdvectiveTime(
      const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
      const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template <class T, class Tu, class Tpde, class Th>
  void masterState(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // master state jacobian wrt q: dU(Q)/dQ
  template<class T, class Tpde, class Th>
  void jacobianMasterState(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <class T, class Tpde, class Th>
  void strongFluxAdvectiveTime(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const;

  // advective flux: F(Q)
  template <class T, class Tpde, class Ts, class Th>
  void fluxAdvective(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Ts>& f ) const;

  template <class T, class Tpde, class Ts, class Th>
  void fluxAdvectiveUpwind(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx,
      ArrayQ<Ts>& f ) const;

  template <class T, class Tpde, class Tf, class Th>
  void fluxAdvectiveUpwindSpaceTime(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)matrix)
  template <class T, class Tpde, class Th>
  void jacobianFluxAdvective(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x,  const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class Tpde, class Th>
  void jacobianFluxAdvectiveAbsoluteValue(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const Real& nx,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class Tpde, class Th>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const Real& nx, const Real& nt,
      MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class T, class Tpde, class Th>
  void strongFluxAdvective(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, QX)
  template <class Th, class Tpde, class Tq, class Tg, class Tf>
  void fluxViscous(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Th, class Tpde, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Th, class Tpde, class Tq, class Tg, class Tf>
  void fluxViscous(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& paramL,
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& paramR,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& f ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Th, class Tpde, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& paramL,
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& paramR,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T, class Tpde, class Th>
  void jacobianFluxViscous(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax ) const {}

  // strong form viscous fluxes
  template <class T, class Tpde, class Th>
  void strongFluxViscous(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      const ArrayQ<T>& qxx,
      ArrayQ<T>& strongPDE ) const;

  // solution-dependent source: S(X, Q, QX)
  template<class Tq, class Tg, class Tpde, class Th, class Ts>
  void source(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& s ) const
  {
    source(param, x, time, q, qx, s); //forward to regular source
  }

  // source on trace
  template <class Tq, class T, class Tg, class Ts, class Th>
  void sourceTrace(const ParamTuple<Th, PDEArrayQ<Tq>, TupleClass<>>& paramL, const Real& xL,
                   const ParamTuple<Th, PDEArrayQ<Tq>, TupleClass<>>& paramR, const Real& xR,
                   const Real& time,
                   const ArrayQ<T>& qL, const ArrayQ<Tg>& qxL,
                   const ArrayQ<T>& qR, const ArrayQ<Tg>& qxR,
                   ArrayQ<Ts>& sourceL,
                   ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, ArrayQ<Ts>& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tpde, class Th>
  void jacobianSource(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const;

  // right-hand-side forcing function
  template <class T, class Tpde, class Th>
  void forcingFunction( const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param,
                        const Real& x, const Real& time, ArrayQ<T>& source ) const {}

  // characteristic speed (needed for timestep)
  template <class T, class Tpde, class Th>
  void speedCharacteristic(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const Real& dx, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T, class Tpde, class Th>
  void speedCharacteristic(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  template <class Tpde, class Th>
  void updateFraction(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const AdvectiveFlux adv_;
  const ViscousFlux visc_;
  const Source source_;
};

template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tu, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::masterState(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  uCons += q;
}

template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::jacobianMasterState(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  dudq = DLA::Identity();
}


template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::strongFluxAdvectiveTime(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
{
  dudt += qt;
}

template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Ts, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::fluxAdvective(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Ts>& f ) const
{
  adv_.flux( param, x, time, q, f );
}

template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Ts, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::fluxAdvectiveUpwind(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx,
    ArrayQ<Ts>& f ) const
{
  adv_.fluxUpwind(param, x, time, qL, qR, nx, f);
}

template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Tf, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::fluxAdvectiveUpwindSpaceTime(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& nt,
    ArrayQ<Tf>& f ) const
{
  adv_.fluxUpwindSpaceTime(param, x, time, qL, qR, nx, nt, f);
}


// advective flux jacobian: d(F)/d(U)
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvective(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x,  const Real& time, const ArrayQ<T>& q,
    MatrixQ<T>& ax ) const
{
  adv_.jacobian( param, x, time, q, ax );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValue(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const
{
  MatrixQ<T> ax = 0;

  jacobianFluxAdvective(param, x, time, q, ax);

  T an = nx*ax;

  mtx += fabs( an );
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const
{
  MatrixQ<T> ax = 0;

  jacobianFluxAdvective(param, x, time, q, ax);

  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  T an = nx*ax + nt*1;

  //mtx += smoothabs0( an, eps );
  mtx += fabs( an );
}


// strong form of advective flux: d(F)/d(X)
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::strongFluxAdvective(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    ArrayQ<T>& strongPDE ) const
{
  adv_.strongFlux( param, x, time, q, qx, strongPDE );
}


// viscous flux
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class Th, class Tpde, class Tq, class Tg, class Tf>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& f ) const
{
  visc_.fluxViscous( param, x, time, q, qx, f );
}

// space-time viscous flux
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class Th, class Tpde, class Tq, class Tg, class Tf>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::fluxViscousSpaceTime(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  MatrixQ<Tq> kxx = 0.0, kxt = 0.0;
  MatrixQ<Tq> ktx = 0.0, ktt = 0.0;

  diffusionViscousSpaceTime(param, x, time, q, qx, qt, kxx, kxt, ktx, ktt);

  f -= kxx*qx + kxt*qt;
  g -= ktx*qx + ktt*qt;
}


// viscous flux: normal flux with left and right states
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class Th, class Tpde, class Tq, class Tg, class Tf>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::fluxViscous(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& paramL,
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& paramR,
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    const Real& nx, ArrayQ<Tf>& f ) const
{
#if 1
  ArrayQ<Tf> fL = 0, fR = 0;

  fluxViscous(paramL, x, time, qL, qxL, fL);
  fluxViscous(paramR, x, time, qR, qxR, fR);

  f += 0.5*(fL + fR)*nx;
#else
  ArrayQ<Tq> q = 0.5*(qL + qR);
  ArrayQ<Tg> qx = 0.5*(qxL + qxR);

  MatrixQ<Tq> kxx = 0.0;

  visc_.diffusionViscous( param, x, time, q, kxx );

  f -= nx*kxx*qx;
#endif
}

// space-time viscous flux: normal flux with left and right states
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class Th, class Tpde, class Tq, class Tg, class Tf>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::fluxViscousSpaceTime(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& paramL,
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& paramR,
    const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscousSpaceTime(paramL, x, time, qL, qxL, qtL, fL, gL);
  fluxViscousSpaceTime(paramR, x, time, qR, qxR, qtR, fR, gR);

  f += 0.5*(fL + fR)*nx + 0.5*(gL + gR)*nt;
}

// viscous diffusion matrix: d(Fv)/d(UX)
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class Th, class Tpde, class Tq, class Tg, class Tk>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::diffusionViscous(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tk>& kxx ) const
{
  visc_.diffusionViscous( param, x, time, q, qx, kxx );
}

// space-time viscous diffusion matrix: d(Fv)/d(UX)
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class Th, class Tpde, class Tq, class Tg, class Tk>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::diffusionViscousSpaceTime(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt ) const
{
  visc_.diffusionViscous( param, x, time, q, qx, qt, kxx, kxt, ktx, ktt );
}

// strong form of viscous flux: d(Fv)/d(X)
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::strongFluxViscous(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    const ArrayQ<T>& qxx,
    ArrayQ<T>& strongPDE ) const
{
  visc_.strongFluxViscous( param, x, time, q, qx, qxx, strongPDE );
}

// solution-dependent source: S(X, Q, QX)
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template<class Tq, class Tg, class Tpde, class Th, class Ts>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::source(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{
  source_.source( param, x, time, q, qx, source );
}

// trace source
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tq, class T, class Tg, class Ts, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::sourceTrace(
    const ParamTuple<Th, PDEArrayQ<Tq>, TupleClass<>>& paramL, const Real& xL,
    const ParamTuple<Th, PDEArrayQ<Tq>, TupleClass<>>& paramR, const Real& xR,
    const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<T>& qR, const ArrayQ<Tg>& qxR,
    ArrayQ<Ts>& sourceL,
    ArrayQ<Ts>& sourceR ) const
{
  source_.sourceTrace(paramL, xL,
                      paramR, xR, time,
                      qL, qxL,
                      qR, qxR,
                      sourceL, sourceR);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::jacobianSource(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    MatrixQ<T>& dsdu ) const
{
  source_.jacobianSource(param, x, time, q, qx, dsdu);
}


// characteristic speed (needed for timestep)
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::speedCharacteristic(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const Real& dx, const ArrayQ<T>& q, T& speed ) const
{
  MatrixQ<T> ax = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(param, x, time, q, ax);

  T uh = dx*ax;

  speed = fabs(uh)/sqrt(dx*dx); // So this 'sqrt(dx*dx)' should be unnecessary
}


// characteristic speed
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T, class Tpde, class Th>
inline void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::speedCharacteristic(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, T& speed ) const
{
  MatrixQ<T> ax = 0;
  //Real eps = 1e-9;

  jacobianFluxAdvective(param, x, time, q, ax);

  speed = fabs(ax);
}

// update fraction needed for physically valid state
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class Tpde, class Th>
void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::updateFraction(
    const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& time,
    const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
    const Real maxChangeFraction, Real& updateFraction ) const
{
  // Limit the update
  ArrayQ<Real> nq = q + dq;

  updateFraction = 1;
  if (nq < (1-maxChangeFraction)*q)
    updateFraction = -maxChangeFraction*q/dq;

  if (nq > (1+maxChangeFraction)*q)
    updateFraction =  maxChangeFraction*q/dq;
}

// is state physically valid
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
inline bool
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::isValidState( const ArrayQ<Real>& q ) const
{
  return true;
}


// set from primitive variable array
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q = data[0];
}

// interpret residuals of the solution variable
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT(rsdPDEOut.m() == 1);
  rsdPDEOut[0] = rsdPDEIn;
}

// interpret residuals of the gradients in the solution variable
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_ASSERT(rsdAuxOut.m() == 1);
  rsdAuxOut[0] = rsdAuxIn;
}

// interpret residuals at the boundary (should forward to BCs)
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
template <class T>
void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT(rsdBCOut.m() == 1);
  rsdBCOut[0] = rsdBCIn;
}

// how many residual equations are we dealing with
template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
inline int
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::nMonitor() const
{
  return 1;
}

template<class PDETraits, class AdvectiveFlux, class ViscousFlux, class Source>
void
PDESensorParameter<PhysD1, PDETraits, AdvectiveFlux, ViscousFlux, Source>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDESensorParameter1D: adv_ =" << std::endl;
  adv_.dump(indentSize+2, out);
  out << indent << "PDESensorParameter1D: visc_ =" << std::endl;
  visc_.dump(indentSize+2, out);
  out << indent << "PDESensorParameter1D: force_ =" << std::endl;
}

} //namespace SANS

#endif  // PDESENSORPARAMETER1D_H
