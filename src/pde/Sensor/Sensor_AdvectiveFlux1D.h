// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SENSOR_ADVECTIVEFLUX1D_H
#define SENSOR_ADVECTIVEFLUX1D_H

// 1-D Advection-Diffusion PDE: advective flux

#include "tools/SANSnumerics.h"     // Real
#include "SensorParameter_Traits.h"
#include "tools/smoothmath.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <cmath>  // fabs
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: advection velocity
//
// member functions:
//   .flux       advective flux: F(Q)
//   .jacobian   advective flux jacboian: d(F)/d(U)
//----------------------------------------------------------------------------//

class Sensor_AdvectiveFlux1D_None
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return false; }

  ~Sensor_AdvectiveFlux1D_None() {}

  // advective flux: F(Q)
  template <class T, class Tpde, class Ts, class Th>
  void flux( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real time,
             const ArrayQ<T>& q, ArrayQ<Ts>& fx ) const
  {}

  // advective flux jacobian: d(F)/d(U)
  template <class T, class Tpde, class Th>
  void jacobian( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real time,
                 const ArrayQ<T>& q, MatrixQ<T>& ax ) const
  {}

  template <class T, class Tpde, class Ts, class Th>
  void fluxUpwind( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx,
                   ArrayQ<Ts>& f ) const
  {}

  template <class T, class Tpde, class Tf, class Th>
  void fluxUpwindSpaceTime( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                            const Real& x, const Real time,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR,
                            const Real& nx, const Real& nt,
                            ArrayQ<Tf>& f ) const
  {
    f += 0.5*(nt)*(qR + qL) - 0.5*fabs(nt)*(qR - qL);
  }

  // strong advective flux: div.F
  template <class T, class Tpde, class Th>
  void strongFlux( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real time,
                   const ArrayQ<T>& q, const ArrayQ<T>& qx,
                   ArrayQ<T>& strongPDE ) const
  {}

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};


//----------------------------------------------------------------------------//
// advection velocity: uniform
//
// member functions:
//   .flux       advective flux: F(Q)
//   .jacobian   advective flux jacboian: d(F)/d(U)
//----------------------------------------------------------------------------//

class Sensor_AdvectiveFlux1D_Uniform
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  explicit Sensor_AdvectiveFlux1D_Uniform( const Real u ) : u_(u) {}
  ~Sensor_AdvectiveFlux1D_Uniform() {}

  template <class T, class Tpde, class Ts, class Th>
  void flux( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real time,
             const ArrayQ<T>& q, ArrayQ<Ts>& fx ) const
  {
    fx += u_*q;
  }

  // advective flux jacobian: d(F)/d(U)
  template<class T, class Tpde, class Tf, class Th>
  void jacobian( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real x, const Real time,
                 const ArrayQ<T>& q, MatrixQ<Tf>& ax ) const
  {
    ax += u_;
  }

  template<class T, class Tpde, class Tf, class Th>
  void fluxUpwind( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real x, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx,
                   ArrayQ<Tf>& f ) const
  {
    f += 0.5*(nx*u_)*(qR + qL) - 0.5*fabs(nx*u_)*(qR - qL);
  }

  template<class T, class Tpde, class Tf, class Th>
  void fluxUpwindSpaceTime( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real x, const Real time,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR,
                            const Real& nx, const Real& nt,
                            ArrayQ<Tf>& f ) const
  {
    f += 0.5*(nt*1 + nx*u_)*(qR + qL) - 0.5*fabs(nt*1 + nx*u_)*(qR - qL);
  }

  // strong advective flux: div.F
  template<class T, class Tpde, class Th>
  void strongFlux( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real x, const Real time,
                   const ArrayQ<T>& q, const ArrayQ<T>& qx,
                   ArrayQ<T>& strongPDE ) const
  {
    strongPDE += u_*qx;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real u_;      // uniform advection velocity
};
} //namespace SANS

#endif  // SENSOR_ADVECTIVEFLUX1D_H
