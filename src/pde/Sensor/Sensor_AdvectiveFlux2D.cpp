// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

//----------------------------------------------------------------------------//
// 2D advection velocity

#include <string>
#include <iostream>

#include "Sensor_AdvectiveFlux2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// advective flux: none

void
Sensor_AdvectiveFlux2D_None::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Sensor_AdvectiveFlux2D_None" << std::endl;
}

//----------------------------------------------------------------------------//
// advection flux: uniform flow

void
Sensor_AdvectiveFlux2D_Uniform::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "Sensor_AdvectiveFlux2D_Uniform:"
            << "  u_ = " << u_
            << "  v_ = " << v_ << std::endl;
}

} //namespace SANS
