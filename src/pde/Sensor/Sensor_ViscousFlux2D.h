// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SENSOR_DIFFUSIONMATRIX2D_H
#define SENSOR_DIFFUSIONMATRIX2D_H

// 1-D Advection-Diffusion PDE: diffusion matrix

#include "tools/SANSnumerics.h"     // Real
#include "SensorParameter_Traits.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"

#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
template<class DiffusionMatrix>
class Sensor_ViscousFlux2D_LinearInGradient : public DiffusionMatrix
{
public:
  using DiffusionMatrix::diffusionViscous;

  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD2>::template MatrixQ<T>;  // matrices

  template< class... Args > // cppcheck-suppress noExplicitConstructor
  Sensor_ViscousFlux2D_LinearInGradient(Args&&... args) : DiffusionMatrix(std::forward<Args>(args)...) {}

  bool fluxViscousLinearInGradient() const { return true; }

  // viscous flux: Fv(Q, QX)
  template <class Th, class Tpde, class Tq, class Tg, class Tf>
  void fluxViscous(
      const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    MatrixQ<Tq> kxx=0, kxy=0,
                kyx=0, kyy=0;

    diffusionViscous( param, x, y, time,
                      q, qx, qy,
                      kxx, kxy,
                      kyx, kyy );

    f -= kxx*qx + kxy*qy;
    g -= kyx*qx + kyy*qy;
  }
};

//----------------------------------------------------------------------------//
// diffusion matrix: None
//----------------------------------------------------------------------------//
class Sensor_DiffusionMatrix2D_None
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  bool hasFluxViscous() const { return false; }

  Sensor_DiffusionMatrix2D_None() {}

  template <class Tpde, class Th, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class T, class Tpde, class Th>
  inline void
  strongFluxViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                     const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
                     const ArrayQ<T>& qxx,
                     const ArrayQ<T>& qyx, const ArrayQ<T>& qyy,
                     ArrayQ<T>& strongPDE ) const
  {}

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;
};

typedef Sensor_ViscousFlux2D_LinearInGradient<Sensor_DiffusionMatrix2D_None> Sensor_ViscousFlux2D_None;

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
//----------------------------------------------------------------------------//
class Sensor_DiffusionMatrix2D_Uniform
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  explicit Sensor_DiffusionMatrix2D_Uniform( const Real kxx, const Real kxy, const Real kyx, const Real kyy ) :
      kxx_(kxx), kxy_(kxy),
      kyx_(kyx), kyy_(kyy) {}

  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                         const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const
  {
    kxx += kxx_; kxy += kxy_;
    kyx += kyx_; kyy += kyy_;
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class T, class Tpde, class Th>
  inline void
  strongFluxViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                     const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
                     const ArrayQ<T>& qxx,
                     const ArrayQ<T>& qyx, const ArrayQ<T>& qyy,
                     ArrayQ<T>& strongPDE ) const
  {
    strongPDE -= kxx_*qxx + (kxy_ + kyx_)*qyx + kyy_*qyy;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real kxx_, kxy_, kyx_, kyy_;          // constant diffusion matrix
};

typedef Sensor_ViscousFlux2D_LinearInGradient<Sensor_DiffusionMatrix2D_Uniform> Sensor_ViscousFlux2D_Uniform;

//----------------------------------------------------------------------------//
// diffusion matrix: Grid Dependent
//----------------------------------------------------------------------------//
class Sensor_DiffusionMatrix2D_GridDependent
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  explicit Sensor_DiffusionMatrix2D_GridDependent( const Real C ) :
      C_(C)
  {
    // Nothing
  }

  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param,
                         const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const
  {
    Th h = get<0>(param);
    MatrixQ<Th> K = C_*h*h;
    kxx += K; kxy += 0;
    kyx += 0; kyy += K;
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class T, class Tpde, class Th>
  inline void
  strongFluxViscous( const ParamTuple<Th, ArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
                     const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
                     const ArrayQ<T>& qxx,
                     const ArrayQ<T>& qyx, const ArrayQ<T>& qyy,
                     ArrayQ<T>& strongPDE ) const
  {
    Th h = get<0>(param);
    MatrixQ<T> K = C_*h*h;

    strongPDE -= K*qxx + K*qyy;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real C_; // constant scaling
};

typedef Sensor_ViscousFlux2D_LinearInGradient<Sensor_DiffusionMatrix2D_GridDependent> Sensor_ViscousFlux2D_GridDependent;

//----------------------------------------------------------------------------//
// Diffusion matrix with generalized h-scale
// Formulation from Masa Yano's PhD thesis - page 191
//----------------------------------------------------------------------------//
class Sensor_DiffusionMatrix2D_GenHScale
{
public:
  template <class T>
  using ArrayQ = SensorParameterTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = SensorParameterTraits<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  explicit Sensor_DiffusionMatrix2D_GenHScale( const Real C ) : C_(C) {}

  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<DLA::MatrixSymS<D,Th>, ArrayQ<Tpde>, TupleClass<>>& param,
                         const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const
  {
    DLA::MatrixSymS<D,Th> H = exp(get<0>(param)); //generalized h-tensor
//    Tpde qprimal = get<1>(param); //solution of primal PDE

    DLA::MatrixSymS<D,Th> Hsq = H*H;

    kxx += C_*Hsq(0,0); kxy += C_*Hsq(0,1);
    kyx += C_*Hsq(1,0); kyy += C_*Hsq(1,1);
  }

  //Overloaded for space-time meshes
  template <class Th, class Tpde, class Tq, class Tg, class Tk>
  void diffusionViscous( const ParamTuple<DLA::MatrixSymS<D+1,Th>, ArrayQ<Tpde>, TupleClass<>>& param,
                         const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
                         MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt) const
  {
    DLA::MatrixSymS<D+1,Th> H = exp(get<0>(param)); //generalized h-tensor
    DLA::MatrixSymS<D+1,Th> Hsq = H*H;

    kxx += C_*Hsq(0,0); kxy += C_*Hsq(0,1); kxt += C_*Hsq(0,2);
    kyx += C_*Hsq(1,0); kyy += C_*Hsq(1,1); kyt += C_*Hsq(1,2);
    ktx += C_*Hsq(2,0); kty += C_*Hsq(2,1); ktt += C_*Hsq(2,2);
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class T, class Tpde, class Th>
  inline void
  strongFluxViscous( const ParamTuple<DLA::MatrixSymS<D,Th>, ArrayQ<Tpde>, TupleClass<>>& param,
                     const Real& x, const Real& y, const Real& time,
                     const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
                     const ArrayQ<T>& qxx,
                     const ArrayQ<T>& qyx, const ArrayQ<T>& qyy,
                     ArrayQ<T>& strongPDE ) const
  {
    SANS_DEVELOPER_EXCEPTION("Sensor_DiffusionMatrix2D_GenHScale::strongFluxViscous - Not implemented yet.");
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real C_; // constant scaling
};

typedef Sensor_ViscousFlux2D_LinearInGradient<Sensor_DiffusionMatrix2D_GenHScale> Sensor_ViscousFlux2D_GenHScale;

} //namespace SANS

#endif  // SENSOR_DIFFUSIONMATRIX2D_H
