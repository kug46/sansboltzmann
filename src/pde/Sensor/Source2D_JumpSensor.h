// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOURCE2D_JUMPSENSOR_H
#define SOURCE2D_JUMPSENSOR_H

// 1-D Advection-Diffusion PDE: source term term the sensor parameter

#include "tools/SANSnumerics.h"     // Real
#include "Surreal/PromoteSurreal.h"
#include "SensorParameter_Traits.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"
#include "tools/smoothmath.h"

#include "Surreal/SurrealS.h"

#include <iostream>
#include <cmath> // fabs

#include "pyrite_fstream.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: source
//
// member functions:
//   .source          element source term
//   .sourceTrace     trace source term
//----------------------------------------------------------------------------//

template <class Sensor>
class Source2D_JumpSensor
{
public:
  template<class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::ArrayQ<T>;

  template<class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::MatrixQ<T>;

  template<class T>
  using PDEArrayQ = typename Sensor::template ArrayQ<T>;

  explicit Source2D_JumpSensor(int PDEorder, const Sensor& sensor, const Real C = 1.0) :
    PDEorder_(Real(PDEorder)),
    sensor_(sensor),
    C_(C)
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return true; }
  bool hasSourceLiftedQuantity() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Tpde, class Th, class Ts>
  void source( const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    source += q;
  }

  template <class Tq, class T, class Tg, class Th, class Ts>
  void sourceTrace(
      const ParamTuple<Th, PDEArrayQ<Tq>, TupleClass<>> paramL, const Real& xL, const Real& yL,
      const ParamTuple<Th, PDEArrayQ<Tq>, TupleClass<>> paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<T>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const
  {
#define BARTER_FORMULATION_2D 1

    Tq gL, gR;
    Tq dg, gbar, jump;

    PDEArrayQ<Tq> paramStateL = get<1>(paramL);
    PDEArrayQ<Tq> paramStateR = get<1>(paramR);

    // trace term
    gL = 0;
    gR = 0;
    sensor_.jumpQuantity( paramStateL, gL );
    sensor_.jumpQuantity( paramStateR, gR );

    dg = gL - gR;
    dg = smoothabs0(dg, 1.0e-5);

    gbar = 0.5*(fabs(gL) + fabs(gR));

    // Check if dg is small relative to gbar
    static const Real delta = 1e-3;
    if ( delta*gbar + 1.0 == 1.0 )
      return; //gbar is nearly zero, just return

//    if ( delta*dg + gbar == gbar )
//      return; //jump is zero => sensor is zero

    jump = dg / gbar;

#if BARTER_FORMULATION_2D
    jump = smoothabs0(jump, 0.03);
    Real psi0 = 2.25 + 3.0*log10(PDEorder_);

    Tq g = log10(jump + 1e-16);

    Tq xi = g + psi0 + 0.5;

    // Calculate the sensor value
//    Tq Sk = smoothActivation_sine(xi);
//    Tq Sk = smoothActivation_cubic(xi);
    Tq Sk = smoothActivation_tanh(xi);

//    std::fstream filedump( "tmp/tracedump.dat", std::fstream::app );
//    filedump << std::setprecision(10) << std::scientific;
//    filedump << xL << ", " << yL << ", " << xi << ", " << Sk << std::endl;
//    filedump.close();

//    sourceL += -C_*(Sk-qL);
//    sourceR += -C_*(Sk-qR);

    sourceL += -C_*Sk;
    sourceR += -C_*Sk;
#else

    Ts g = log10(jump + 1e-16);
    Ts xi = g + 1.0;
    Ts Sk = smoothabs0(xi, 1.0e-5);

//    Ts Sk = smoothActivation_tanh(xi);

    sourceL += -C_*Sk;
    sourceR += -C_*Sk;
#endif
  }

  template <class T, class Tpde, class Th>
  void jacobianSource(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const
  {
    dsdu += 1.0;
  }
//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

protected:
  Real PDEorder_;
  const Sensor& sensor_;
  const Real C_;
};


template <class Sensor>
class Source2D_JumpSensor_BuckleyLeverett
{
public:
  template<class T>
  using ArrayQ = SensorParameterTraits<PhysD1>::ArrayQ<T>;

  template<class T>
  using MatrixQ = SensorParameterTraits<PhysD1>::MatrixQ<T>;

  template<class T>
  using PDEArrayQ = typename Sensor::template ArrayQ<T>;

  explicit Source2D_JumpSensor_BuckleyLeverett(int PDEorder, const Sensor& sensor, const Real C = 1.0) :
    PDEorder_(Real(PDEorder)),
    sensor_(sensor),
    C_(C)
  {
    // Nothing
  }

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return true; }
  bool hasSourceLiftedQuantity() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  template<class Tq, class Tg, class Tpde, class Th, class Ts>
  void source( const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    source += q;
  }

  template <class Tq, class T, class Tg, class Th, class Ts>
  void sourceTrace(
      const ParamTuple<Th, ArrayQ<Tq>, TupleClass<>> paramL, const Real& xL, const Real& yL,
      const ParamTuple<Th, ArrayQ<Tq>, TupleClass<>> paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<T>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL,
      ArrayQ<Ts>& sourceR ) const
  {
    Tq gL, gR, jump;

    PDEArrayQ<Tq> paramStateL = get<1>(paramL);
    PDEArrayQ<Tq> paramStateR = get<1>(paramR);

    // trace term
    gL = 0;
    gR = 0;
    sensor_.jumpQuantity( paramStateL, gL );
    sensor_.jumpQuantity( paramStateR, gR );

    jump = gL - gR;
    jump = smoothabs0(jump, 1.0e-3);

    Real psi0 = 2.25 + 3.0*log10(PDEorder_);

    Tq g = log10(jump + 1e-16);
    Tq xi = g + psi0 + 0.5;

    // Calculate the sensor value
//    Ts Sk = smoothActivation_sine(xi);
//    Ts Sk = smoothActivation_cubic(xi);
    Tq Sk = smoothActivation_tanh(xi);

    sourceL += -C_*Sk;
    sourceR += -C_*Sk;
  }

  template <class T, class Tpde, class Th>
  void jacobianSource(
      const ParamTuple<Th, PDEArrayQ<Tpde>, TupleClass<>>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
      MatrixQ<T>& dsdu ) const
  {
    dsdu += 1.0;
  }
//  void dump( int indentSize=0, std::ostream& out = std::cout ) const = 0;

protected:
  Real PDEorder_;
  const Sensor& sensor_;
  const Real C_;
};

} //namespace SANS

#endif  // SOURCE2D_JUMPSENSOR_H
