// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTCATEGORY_H
#define OUTPUTCATEGORY_H

#include <type_traits>
#include <typeinfo>

#include <boost/mpl/bool.hpp>

#include "tools/SANSException.h"

namespace SANS
{
//---------------------------------------------------------------------------//
//
// Tags used to indicate the Output category
//
//---------------------------------------------------------------------------//
struct OutputCategory
{
  struct WeightedResidual {};
  struct Functional {};
};

template<class Category>
struct CategoryNeedsFieldTrace;

template<> struct CategoryNeedsFieldTrace<OutputCategory::WeightedResidual > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsFieldTrace<OutputCategory::Functional       > : boost::mpl::bool_<false> {};

template<class Category>
struct CategoryNeedsHubTrace;

template<> struct CategoryNeedsHubTrace<OutputCategory::WeightedResidual > : boost::mpl::bool_<false> {};
template<> struct CategoryNeedsHubTrace<OutputCategory::Functional       > : boost::mpl::bool_<false> {};

class OutputBase
{
public:
  OutputBase() {}
  OutputBase(const OutputBase&) = delete;
  OutputBase& operator=(const OutputBase&) = delete;
  virtual ~OutputBase() {}

  virtual const std::type_info& derivedTypeID() const = 0;

  // Safe methods for casting the Output type
  template<class Output>
  Output& cast()
  {
    SANS_ASSERT( typeid(Output) == derivedTypeID() );
    return static_cast<Output&>(*this);
  }

  template<class Output>
  const Output& cast() const
  {
    SANS_ASSERT( typeid(Output) == derivedTypeID() );
    return static_cast<const Output&>(*this);
  }
};


template<class Derived>
struct OutputType : public OutputBase
{
  virtual const std::type_info& derivedTypeID() const override
  {
    static_assert( std::is_base_of<OutputType<Derived>, Derived>::value, "This is usually a copy/paste mistake" );

    return typeid(Derived);
  }

  virtual ~OutputType() {}

  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

} // namespace SANS

#endif //OUTPUTCATEGORY_H
