// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEULER2D_H
#define BCEULER2D_H

// 2-D Stokes BC class
#undef BOOST_MPL_LIMIT_VECTOR_SIZE
#define BOOST_MPL_LIMIT_VECTOR_SIZE 30
#include <boost/mpl/vector.hpp>
#include <boost/mpl/vector/vector30.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "pde/BCCategory.h"
#include "Topology/Dimension.h"
#include "TraitsStokes.h"
#include "PDEStokes2D.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D Stokes
//
// template parameters:
//   BCType               BC type (e.g. BCTypeInflowSupersonic)
//----------------------------------------------------------------------------//

class BCTypeFullState_mitState;

template <class BCType, class PDEStokes2D>
class BCStokes2D;

template <class BCType>
struct BCStokes2DParams;


template <template <class> class PDETraitsSize>
using BCStokes2DVector = boost::mpl::vector1<
    BCStokes2D<BCTypeFullState_mitState, PDEStokes2D<PDETraitsSize>>
                                            >;

//----------------------------------------------------------------------------//
// None BC:
//
// A class for representing input parameters for the BCStokes2D<BCTypeNone, ...>
struct BCStokes2DFullStateParams : noncopyable
{
  BCStokes2DFullStateParams() {}

  const ParameterBool Characteristic{"Characteristic", NO_DEFAULT, "Use characteristics to impose the state"};
  const ParameterBool Viscous{"Viscous", NO_DEFAULT, "Use viscous flux on the boundary"};

  const ParameterNumeric<Real> pSpec{"pSpec", NO_DEFAULT, NO_RANGE, "Pressure"};
  const ParameterNumeric<Real> uSpec{"uSpec", NO_DEFAULT, NO_RANGE, "X-Velocity"};
  const ParameterNumeric<Real> vSpec{"vSpec", NO_DEFAULT, NO_RANGE, "Y-Velocity"};

  static constexpr const char* BCName{"FullState"};
  struct Option
  {
    const DictOption FullState{BCStokes2DFullStateParams::BCName, BCStokes2DFullStateParams::checkInputs};
  };

  static void checkInputs(PyDict d);

  static BCStokes2DFullStateParams params;
};


template <class PDE_>
class BCStokes2D<BCTypeFullState_mitState, PDE_> :
    public BCType< BCStokes2D<BCTypeFullState_mitState, PDE_> >
{
public:
  typedef PhysD2 PhysDim;
  typedef BCTypeFullState_mitState BCType;
  typedef typename BCCategory::Flux_mitState Category;
  typedef BCStokes2DFullStateParams ParamsType;
  typedef PDE_ PDE;

  static const int D = PDE::D;   // physical dimensions
  static const int N = PDE::N;   // total solution variables

  static const int NBC = N;                         // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  // cppcheck-suppress noExplicitConstructor
  BCStokes2D( const PDE& pde, const Real& p, const Real& u, const Real v, const bool& upwind, const bool& viscous  ) :
      pde_(pde),
      p_(p), u_(u), v_(v),
      upwind_(upwind), viscous_(viscous)
  {}

  BCStokes2D(const PDE& pde, const PyDict& d ) :
    pde_(pde),
    p_(d.get(ParamsType::params.pSpec)),
    u_(d.get(ParamsType::params.uSpec)),
    v_(d.get(ParamsType::params.vSpec)),
    upwind_(d.get(ParamsType::params.Characteristic)),
    viscous_(d.get(ParamsType::params.Viscous))
  {}

  ~BCStokes2D() {}

  BCStokes2D( const BCStokes2D& ) = delete;
  BCStokes2D& operator=( const BCStokes2D& ) = delete;

  // Is there a viscous flux on the boundary
  bool hasFluxViscous() const { return viscous_; }

  // BC data
  template <class T>
  void state( const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    qB(0) = p_; qB(1) = u_; qB(2) = v_;
  }

  // BC data
  template <class Tp, class T>
  void state( const Tp& param, const Real& x, const Real& y, const Real& time,
              const Real& nx, const Real& ny,
              const ArrayQ<T>& qI, ArrayQ<T>& qB ) const
  {
    state(x, y, time, nx, ny, qI, qB);
  }

  // normal BC flux
  template <class T, class Tf>
  void fluxNormal( const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if ( upwind_ )
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind( x, y, time, qI, qB, nx, ny, Fn );
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<Tf> fx = 0, fy = 0;
      pde_.fluxAdvective(x, y, time, qB, fx, fy);
      pde_.fluxAdvective(x, y, time, qI, fx, fy);

      Fn += 0.5*fx*nx + 0.5*fy*ny;
      Fn += fx*nx + fy*ny;
    }

    if (viscous_)
    {
      ArrayQ<Tf> fvx = 0, fvy = 0;

      pde_.fluxViscous(x, y, time, qB, qIx, qIy, fvx, fvy);

      Fn += fvx*nx + fvy*ny;

    }

  }

  // normal BC flux
  template <class Tp, class T, class Tf>
  void fluxNormal( const Tp& param, const Real& x, const Real& y, const Real& time,
                   const Real& nx, const Real& ny,
                   const ArrayQ<T>& qI,
                   const ArrayQ<T>& qIx, const ArrayQ<T>& qIy,
                   const ArrayQ<T>& qB,
                   ArrayQ<Tf>& Fn) const
  {
    if ( upwind_ )
    {
      // Compute the advective flux based on interior and boundary state
      pde_.fluxAdvectiveUpwind(param, x, y, time, qI, qB, nx, ny, Fn );
    }
    else
    {
      // Compute the advective flux based on the boundary state
      ArrayQ<Tf> fx = 0, fy = 0;
      pde_.fluxAdvective(param, x, y, time, qB, fx, fy);
      pde_.fluxAdvective(param, x, y, time, qI, fx, fy);

      Fn += 0.5*fx*nx + 0.5*fy*ny;
      Fn += fx*nx + fy*ny;
    }

    if (viscous_)
    {
      ArrayQ<Tf> fvx = 0, fvy = 0;

      pde_.fluxViscous(param, x, y, time, qB, qIx, qIy, fvx, fvy);

      Fn += fvx*nx + fvy*ny;

    }

  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& qI ) const
  {
    if (upwind_)
    {
      // Characteristic can be either inflow or outflow
      return true;
    }
    else
    {
//      Real uI = qI(1);
//      Real vI = qI(2);

      // Full state requires inflow, so the dot product between the normal and velocity should be negative
//      return (uI*nx + vI*ny) < 0;
      return true;
    }
  }

  void dump( int indentSize, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCStokes2D<BCTypeFullState_mitState, PDE>: pde_ = " << std::endl;
    pde_.dump(indentSize+2, out);
  }

protected:
  const PDE& pde_;
  const Real p_;
  const Real u_;
  const Real v_;
  const bool upwind_;
  const bool viscous_;
};


} //namespace SANS

#endif  // BCEULER2D_H
