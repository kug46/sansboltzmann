// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCStokes2D.h"

// TODO: Rework so this is not needed here
#include "TraitsStokes.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

void BCStokes2DFullStateParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Characteristic));
  allParams.push_back(d.checkInputs(params.Viscous));
  allParams.push_back(d.checkInputs(params.pSpec));
  allParams.push_back(d.checkInputs(params.uSpec));
  allParams.push_back(d.checkInputs(params.vSpec));
  d.checkUnknownInputs(allParams);
}

BCStokes2DFullStateParams BCStokes2DFullStateParams::params;

struct BCEuler2DFullStateParams;

typedef BCStokes2DVector<TraitsSizeStokes> BCVectorStokes2D; BCPARAMETER_INSTANTIATE( BCVectorStokes2D )
//template struct BCStokes2DFullStateParams;

} //namespace SANS
