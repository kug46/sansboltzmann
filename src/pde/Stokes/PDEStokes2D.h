// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDESTOKES2D_H
#define PDESTOKES2D_H

// 2-D compressible Euler

#include "Python/PyDict.h" // python must be included first
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "pde/ForcingFunctionBase.h"
#include "Topology/Dimension.h"
#include "tools/smoothmath.h"     // Real

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include <cmath>              // sqrt
#include <iostream>
#include <string>
#include <memory>             // shared_ptr

namespace SANS
{


//----------------------------------------------------------------------------//
// PDE class: 2-D Euler
//
// template parameters:
//   T                           solution DOF data type (e.g. Real)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class PDEStokes2D
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;                              // physical dimensions
  static const int N = PDETraitsSize<PhysDim>::N;               // total solution variables

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  typedef ForcingFunctionBase2D< PDEStokes2D<PDETraitsSize> > ForcingFunctionType;

  explicit PDEStokes2D( const Real& nu,
                       const std::shared_ptr<ForcingFunctionType>& forcing = NULL) :
    nu_(nu),
    forcing_(forcing)
  {
    // Nothing
  }

  static const int iCont = 0;
  static const int ixMom = 1;
  static const int iyMom = 2;

  PDEStokes2D( const PDEStokes2D& pde0 ) = delete;
  PDEStokes2D& operator=( const PDEStokes2D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasSourceLiftedQuantity() const { return false; }
  bool hasForcingFunction() const { return forcing_ != NULL && (*forcing_).hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // unsteady temporal flux: Ft(Q)
  template<class Tq, class Tf>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& ft ) const
  {
    ArrayQ<Tf> ftLocal = 0;
    masterState(x, y, time, q, ftLocal);
    ft += ftLocal;
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& J ) const
  {
    J += DLA::Identity();
  }

  // master state: U(Q)
  template<class Tq, class Tf>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const {uCons = q;}

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template<class T>
  void jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const { dudq = DLA::Identity(); }

  // strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& strongPDE ) const { strongPDE += qt; }

  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvective(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    fluxAdvective(x, y, time, q, f, g);
  }

  template <class T, class Tf>
  void fluxAdvectiveUpwind(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwind(
      const Tp& param,
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
  {
    fluxAdvectiveUpwind(x, y, time, qL, qR, nx, ny, f);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const;


  // strong form advective flux gradient: d(strong form)/dx
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxAdvectiveGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDEx, ArrayQ<Tf>& strongPDEy ) const;

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;

  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x,
      MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y ) const {}

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, q, qx, qy, fx, fy);
  }

  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial viscous flux
    fluxViscous(x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, f);
  }

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial diffusion matrix
    diffusionViscous(x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial jacobianFluxViscous
    jacobianFluxViscous(x, y, time, q, qx, qy, ax, ay);
  }

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& s ) const
  {
    source(x, y, time, q, qx, qy, s); //forward to regular source
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const {}

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const;

  // characteristic speed (needed for timestep)
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const;

  // characteristic speed
  template<class Tq, class Tc>
  void speedCharacteristic( Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const;

  // update fraction needed for physically valid state
  void updateFraction( const Real& x, const Real& y, const Real& time, const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                       const Real maxChangeFraction, Real& updateFraction ) const;

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from variable array
  template<class T>
  void setDOFFrom(
      ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  std::vector<std::string> derivedQuantityNames() const;

  template<class Tq, class Tg, class Tf>
  void derivedQuantity(
      const int& index, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  template <class T>
  void eval ( const ArrayQ<T>& q, T& p, T& u, T& v ) const
  {
    p = q(0); u = q(1); v = q(2);
  }

protected:
  const Real& nu_;
  const std::shared_ptr<ForcingFunctionType> forcing_;
};

// advective flux: F(Q)
template <template <class> class PDETraitsSize>
template <class T, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::fluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  T u, v, p = 0;
  eval(q, p, u, v);

  f(iCont) += u; //CAN PUT IN SOURCE INSTEAD FOR DOUGLASWANG
  f(ixMom) += p;
//  f(iyMom) += 0;

  g(iCont) += v;//CAN PUT IN SOURCE INSTEAD FOR DOUGLASWANG
//  g(ixMom) += 0;
  g(iyMom) += p;
}


template <template <class> class PDETraitsSize>
template <class T, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::fluxAdvectiveUpwind(
    const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{

  T uL, vL, pL = 0;
  eval(qL, pL, uL, vL);

  T uR, vR, pR = 0;
  eval(qR, pR, uR, vR);

  //compute mean quantities
  T u = 0.5*(uL + uR);
  T v = 0.5*(vL + vR);
  T p = 0.5*(pL + pR);

  //mean value flux
  f(iCont) += u*nx + v*ny;
  f(ixMom) += p*nx;
  f(iyMom) += p*ny;

//  Real eps = 1.e-3;
  //plus 0.5* L|Lambda|R dq

  T du = uR - uL;
  T dv = vR - vL;
  T dp = pR - pL;

//  f(iCont) -= 0.5*dp;
//  f(ixMom) -= 0.5*(nx*nx + ny*ny*eps)*du + 0.5*nx*ny*(1-eps)*dv ;
//  f(iyMom) -=  0.5*ny*nx*(1-eps)*du + 0.5*(ny*ny*(1-eps) + eps)*dv ;

  // Lax Friedrichs kind of thing
  f(iCont) -= 0.5*dp;
  f(ixMom) -= 0.5*du;
  f(iyMom) -= 0.5*dv;

}


template <template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::fluxAdvectiveUpwindSpaceTime(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const
{
//  flux_.fluxAdvectiveUpwindSpaceTime(x, y, time, qL, qR, nx, ny, nt, f);
  //Full temporal upwinding

  //Compute upwinded spatial advective flux
  ArrayQ<Tq> fnx = 0;
  fluxAdvectiveUpwind( x, y, time, qL, qR, nx, ny, fnx );

  //Upwind temporal flux
  ArrayQ<Tq> ft = 0;
  if (nt >= 0)
    fluxAdvectiveTime( x, y, time, qL, ft );
  else
    fluxAdvectiveTime( x, y, time, qR, ft );

  f += ft*nt + fnx;
}


// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::jacobianFluxAdvective(
    const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
  dfdu(iCont,1) += 1;
  dfdu(ixMom,0) += 1;

  dgdu(iCont,2) += 1;
  dgdu(iyMom,0) += 1;

}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::jacobianFluxAdvectiveAbsoluteValue(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& Nx, const Real& Ny, MatrixQ<Tf>& mtx ) const
{
  Real nMag = sqrt(Nx*Nx + Ny*Ny);
  Real eps0 = 1.e-11;
  Real nMag2 = smoothabsP(nMag, eps0);

//  Real eps1 = 1.e-3;

//  mtx(0,0) += nMag2;
//  mtx(1,1) += Nx*Nx/nMag2;
//  mtx(1,2) += Nx*Ny/nMag2;
//  mtx(2,1) += Nx*Ny/nMag2;
//  mtx(2,2) += Ny*Ny/nMag2;
  mtx(iCont,iCont) += nMag2;
  mtx(ixMom,ixMom) += nMag2;
  mtx(iyMom,iyMom) += nMag2;
}


// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize>
template <class Tq, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Tf>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION("SPACETIME NOT YET IMPLEMENTED");
}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::strongFluxAdvective(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& strongPDE ) const
{
//  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq u, v, p = 0;
  eval(q, p, u, v);

  Tg ux, vx, px = 0;
  Tg uy, vy, py = 0;
  eval(qx, px, ux, vx);
  eval(qy, py, uy, vy);

  strongPDE(iCont) += ux + vy;
  strongPDE(ixMom) += px;
  strongPDE(iyMom) += py;

}


// strong form of advective flux: div.F
template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::strongFluxAdvectiveGradient(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDEx, ArrayQ<Tf>& strongPDEy  ) const
{
//  typedef typename promote_Surreal<Tq, Tg>::type T;

  Tq u, v, p = 0;
  eval(q, p, u, v);

  Tg uxx, vxx, pxx = 0;
  Tg uxy, vxy, pxy = 0;
  Tg uyy, vyy, pyy = 0;
  eval(qxx, pxx, uxx, vxx);
  eval(qxy, pxy, uxy, vxy);
  eval(qyy, pyy, uyy, vyy);

//  strongPDE(iCont) += ux + vy;
//  strongPDE(ixMom) += px;
//  strongPDE(iyMom) += py;

  strongPDEx(iCont) += uxx + vxy;
  strongPDEx(ixMom) += pxx;
  strongPDEx(iyMom) += pxy;

  strongPDEy(iCont) += uxy + vyy;
  strongPDEy(ixMom) += pxy;
  strongPDEy(iyMom) += pyy;
}

// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize>
template <class Tq, class Tc>
inline void
PDEStokes2D<PDETraitsSize>::speedCharacteristic(
    Real, Real, Real, Real dx, Real dy, const ArrayQ<Tq>& q, Tc& speed ) const
{
  speed = 1.;
}


// characteristic speed
template <template <class> class PDETraitsSize>
template <class Tq, class Tc>
inline void
PDEStokes2D<PDETraitsSize>::speedCharacteristic(
    Real, Real, Real, const ArrayQ<Tq>& q, Tc& speed ) const
{
  speed = 1.;
}


// update fraction needed for physically valid state
template <template <class> class PDETraitsSize>
inline void
PDEStokes2D<PDETraitsSize>::
updateFraction( const Real& x, const Real& y, const Real& time,
                const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
                const Real maxChangeFraction, Real& updateFraction ) const
{
  SANS_DEVELOPER_EXCEPTION("STOKES UPDATE FRACTION NOT IMPLEMENTED");
}


// is state physically valid
template <template <class> class PDETraitsSize>
inline bool
PDEStokes2D<PDETraitsSize>::isValidState( const ArrayQ<Real>& q ) const
{
  return true;
}


// set from primitive variable array
template <template <class> class PDETraitsSize>
template <class T>
inline void
PDEStokes2D<PDETraitsSize>::setDOFFrom(
    ArrayQ<T>& q, const Real data[], const std::string name[], int nn ) const
{
  q(0) = data[0];
  q(1) = data[1];
  q(2) = data[2];
}

// interpret residuals of the solution variable
template <template <class> class PDETraitsSize>
template <class T>
inline void
PDEStokes2D<PDETraitsSize>::interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  int nOut = rsdPDEOut.m();

  SANS_ASSERT(nOut == N );
  for (int i = 0; i < N; i++)
    rsdPDEOut[i] = rsdPDEIn[i];
}


// interpret residuals of the gradients in the solution variable
template <template <class> class PDETraitsSize>
template <class T>
inline void
PDEStokes2D<PDETraitsSize>::interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  //TEMPORARY SOLUTION FOR AUX VARIABLE
  int nOut = rsdAuxOut.m();

  SANS_ASSERT(nOut == N );
  for (int i = 0; i < N; i++)
  {
    rsdAuxOut[i] = rsdAuxIn[i];
  }

}


// interpret residuals at the boundary (should forward to BCs)
template <template <class> class PDETraitsSize>
template <class T>
inline void
PDEStokes2D<PDETraitsSize>::interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  int nOut = rsdBCOut.m();

  SANS_ASSERT(nOut == N );
  for (int i = 0; i < N; i++)
    rsdBCOut[i] = rsdBCIn[i];


}


// how many residual equations are we dealing with
template <template <class> class PDETraitsSize>
inline int
PDEStokes2D<PDETraitsSize>::nMonitor() const
{
  return N;
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize>
inline void
PDEStokes2D<PDETraitsSize>::forcingFunction(
    const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  (*forcing_)(*this, x, y, time, forcing );
}

template <template <class> class PDETraitsSize>
inline std::vector<std::string>
PDEStokes2D<PDETraitsSize>::
derivedQuantityNames() const
{
  return {
          // Nothing
         };
}

template <template <class> class PDETraitsSize>
template<class Tq, class Tg, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::
derivedQuantity(const int& index, const Real& x, const Real& y, const Real& time,
                const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, Tf& J ) const
{
  switch (index)
  {
  default:
    std::cout << "index: " << index << std::endl;
    SANS_DEVELOPER_EXCEPTION("PDEStokes2D::derivedQuantity - Invalid index!");
  }
}



// viscous flux: Fv(Q, QX)
template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{

  Tq u, v, p = 0;
  eval(q, p, u, v);

  Tg ux, vx, px = 0;
  Tg uy, vy, py = 0;
  eval(qx, px, ux, vx);
  eval(qy, py, uy, vy);

//  f(iCont) -= nu_*px; //~BRENNER
  f(ixMom) -= nu_*ux;
  f(iyMom) -= nu_*vx;

//  g(iCont) -= nu_*py; //~BRENNER
  g(ixMom) -= nu_*uy;
  g(iyMom) -= nu_*vy;

}


// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  ArrayQ<Tf> fL = 0, fR = 0;
  ArrayQ<Tf> gL = 0, gR = 0;

  fluxViscous(x, y, time, qL, qxL, qyL, fL, gL);
  fluxViscous(x, y, time, qR, qxR, qyR, fR, gR);

  f -= 0.5*(fL + fR)*nx + 0.5*(gL + gR)*ny;

}

// viscous diffusion matrix: d(Fv)/d(UX)
template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class Tk>
inline void
PDEStokes2D<PDETraitsSize>::diffusionViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
//  kxx(iCont,iCont) += nu_; //~BRENNER
  kxx(ixMom,ixMom) += nu_;
  kxx(iyMom,iyMom) += nu_;

//  kyy(iCont,iCont) += nu_; //~BRENNER
  kyy(ixMom,ixMom) += nu_;
  kyy(iyMom,iyMom) += nu_;
}

// strong form viscous fluxes
template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEStokes2D<PDETraitsSize>::strongFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
//  typedef typename promote_Surreal<Tq, Tg>::type Tqg;
//  typedef typename promote_Surreal<Tq, Tg, Th>::type T;

  Th uxx, vxx, pxx, uyy, vyy, pyy = 0;
  eval(qxx, pxx, uxx, vxx);
  eval(qyy, pyy, uyy, vyy);

//  strongPDE(iCont) -= nu_*(pxx + pyy); //~BRENNER
  strongPDE(ixMom) -= nu_*(uxx + uyy);
  strongPDE(iyMom) -= nu_*(vxx + vyy);

}

template <template <class> class PDETraitsSize>
template <class Tq, class Tg, class Ts>
inline void
PDEStokes2D<PDETraitsSize>::source(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
//  Tg ux = qx(1); Tg vy = qy(2);
//  source(iCont) -= ux + vy;
}



template <template <class> class PDETraitsSize>
void
PDEStokes2D<PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEStokes2D<PDETraitsSize>: N = " << N << std::endl;
  out << indent << "PDEStokes2D<PDETraitsSize>: nu_ = " << nu_ << std::endl;
}

} // namespace SANS

#endif  // PDESTOKES2D_H
