// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTSTOKES2D_H
#define OUTPUTSTOKES2D_H


#include "pde/OutputCategory.h"
#include "pde/AnalyticFunction/Function2D.h"

#include "PDEStokes2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Provides weights to compute a force from a weighted residual

template <class PDENDConvert>
class OutputStokes2D_Force : public OutputType< OutputStokes2D_Force<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef OutputCategory::WeightedResidual Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the transpose Jacobian of this output functional

  explicit OutputStokes2D_Force( const PDENDConvert& pde, const Real& wx, const Real& wy ) :
    pde_(pde), wx_(wx), wy_(wy) {}

  void operator()(const Real& x, const Real& y, const Real& time, ArrayQ<Real>& weight ) const
  {
    weight = 0;
    weight[pde_.ixMom] = wx_;
    weight[pde_.iyMom] = wy_;
  }

private:
  const PDENDConvert& pde_;
  Real wx_, wy_;
};


template <class PDENDConvert>
class OutputStokes2D_TotalPressure : public OutputType< OutputStokes2D_TotalPressure<PDENDConvert> >
{
public:
  typedef PhysD2 PhysDim;
  typedef PDENDConvert PDE;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  template<class T>
  using ArrayJ = T;  // Array of outputs

  template<class T>
  using MatrixJ = ArrayQ<T>; // Matrix required to represent the Jacobian of this output functional

  explicit OutputStokes2D_TotalPressure( const PDENDConvert& pde ) :
    pde_(pde) {}

  bool needsSolutionGradient() const { return false; }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {

    T p = 0, u = 0, v = 0;

    pde_.eval(q, p, u, v);

    output = p + 0.5*(u*u + v*v);
  }

  template<class T>
  void operator()(const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, ArrayJ<T>& output ) const
  {
    operator()(x, y, time, q, qx, qy, output);
  }

  template<class T>
  void outputJacobian(const Real& x, const Real& y, const Real& time,
                      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, MatrixJ<T>& dJdu) const
  {

    T p = 0, u = 0, v = 0;

    pde_.eval(q, p, u, v);

    dJdu[0] = 1;
    dJdu[1] = u;
    dJdu[2] = v;
  }

private:
  const PDENDConvert& pde_;
};




}

#endif //OUTPUTSTOKES2D_H
