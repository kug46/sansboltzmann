// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/Stokes/SolutionFunction_Stokes2D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_Stokes2D_OjedaMMS_Params::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}

SolutionFunction_Stokes2D_OjedaMMS_Params SolutionFunction_Stokes2D_OjedaMMS_Params::params;

}
