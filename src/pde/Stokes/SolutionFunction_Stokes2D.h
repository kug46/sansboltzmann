// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef STOKESSOLUTIONFUNCTION2D_H
#define STOKESSOLUTIONFUNCTION2D_H

// 2-D Stokes PDE: exact and MMS solutions

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <cmath> // pow

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "pde/AnalyticFunction/Function2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// solution: - Ojeda MMS Solution Function
struct SolutionFunction_Stokes2D_OjedaMMS_Params : noncopyable
{
  static void checkInputs(PyDict d);

  static SolutionFunction_Stokes2D_OjedaMMS_Params params;
};


template <template <class> class PDETraitsSize>
class SolutionFunction_Stokes2D_OjedaMMS :
    public Function2DVirtualInterface< SolutionFunction_Stokes2D_OjedaMMS<PDETraitsSize>,
                                       PDETraitsSize<PhysD2> >
{
public:
  typedef PhysD2 PhysDim;

  template<class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;   // solution arrays

  typedef SolutionFunction_Stokes2D_OjedaMMS_Params ParamsType;

  explicit SolutionFunction_Stokes2D_OjedaMMS(PyDict d) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T u, v, p;
//    if (x >= 0.375 && y >= 0.375 && x <= 0.625 && y <= 0.625)
//    {
//      p = 0.0 + 0.1*( pow(sin(4.0*PI*(0.625-x)),5)*pow(sin(4.0*PI*(0.625-y)),5) );
//      u = 0.0 - 0.3*pow(sin(4.0*PI*(0.625-x)),5)*pow(sin(4.0*PI*(0.625-y)),5);
//      v =  0.0 - 0.15*pow(sin(4.0*PI*(0.625-x)),5)*pow(sin(4.0*PI*(0.625-y)),5);
//    }
//    else
//    {
//      u = 0.0;
//      v = 0.0;
//      p = 0.0;
//    }

    p = 0.1*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    u = -0.3*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );
    v = -0.15*exp( -(x-0.5)*(x-0.5)*200 -(y-0.5)*(y-0.5)*200   );

    //EXACT SOLN FOR p+0.5*(u^2 + v^2) is Jtrue = 0.002012582795004;

    ArrayQ<T> q = {p, u, v};

    return q;
  }

};



} // namespace SANS

#endif  // STOKESSOLUTIONFUNCTION2D_H
