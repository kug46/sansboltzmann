// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCCAUCHYRIEMANN2D_H
#define BCCAUCHYRIEMANN2D_H

// 2-D Cauchy-Riemann BC class

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "PDECauchyRiemann2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D Cauchy-Riemann
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   BCType               BC type (e.g. BCTypeLinearRobin)
//----------------------------------------------------------------------------//

class BCTypeNone;
class BCTypeDirichlet;

template <class PDE, class BCType>
class BC;


//----------------------------------------------------------------------------//
// no BC:

template <>
class BC<PDECauchyRiemann2D,BCTypeNone>
{
public:
  static const int D = 2;                     // physical dimensions
  static const int N = 2;                     // total solution variables

  static const int NBC = 0;                   // total BCs

  template <class T>
  using ArrayQ = PDECauchyRiemann2D::ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = PDECauchyRiemann2D::MatrixQ<T>;  // matrices

  BC() {}
  BC( const BC& bc ) {}
  ~BC() {}

  BC& operator=( const BC& ) { return *this; }

  // BC coefficients:  A u (B = 0)
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
};


template <class T>
inline void
BC<PDECauchyRiemann2D,BCTypeNone>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&, MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;
  B = 0;
}


template <class T>
inline void
BC<PDECauchyRiemann2D,BCTypeNone>::data( const Real&, const Real&, const Real&, ArrayQ<T>& bcdata ) const
{
  bcdata = 0;
}


#if 0   // moved to *.cpp
void
BC<PDECauchyRiemann2D,BCTypeNone>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BC<PDECauchyRiemann2D,BCTypeNone>:" << std::endl;
}
#endif


//----------------------------------------------------------------------------//
// Dirichlet BC:  A u = bcdata
//
// A arbitrary but should not vanish; singular OK (?)

template <>
class BC<PDECauchyRiemann2D,BCTypeDirichlet>
{
public:
  static const int D = 2;                     // physical dimensions
  static const int N = 2;                     // total solution variables

  static const int NBC = 2;                   // total BCs

  template <class T>
  using ArrayQ = PDECauchyRiemann2D::ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = PDECauchyRiemann2D::MatrixQ<T>;  // matrices

  BC( const MatrixQ<Real>& A, const ArrayQ<Real>& bcdata ) :
      A_(A), bcdata_(bcdata) {}
  BC( const BC& bc ) {}
  ~BC() {}

  BC& operator=( const BC& );

  // BC coefficients:  A u (B = 0)
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  MatrixQ<Real> A_;
  ArrayQ<Real> bcdata_;
};


inline BC<PDECauchyRiemann2D,BCTypeDirichlet>&
BC<PDECauchyRiemann2D,BCTypeDirichlet>::operator=( const BC& bc )
{
  if (this != &bc)
  {
    A_      = bc.A_;
    bcdata_ = bc.bcdata_;
  }

  return *this;
}


template <class T>
inline void
BC<PDECauchyRiemann2D,BCTypeDirichlet>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&, MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = A_;
  B = 0;
}


template <class T>
inline void
BC<PDECauchyRiemann2D,BCTypeDirichlet>::data( const Real&, const Real&, const Real&, ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}


#if 0   // moved to *.cpp
void
BC<PDECauchyRiemann2D,BCTypeDirichlet>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BC<PDECauchyRiemann2D,BCTypeDirichlet>:" << std::endl;
  out << indent << "  A_ = " << A_ << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}
#endif

} //namespace SANS

#endif  // BCCAUCHYRIEMANN2D_H
