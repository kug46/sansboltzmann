
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( CAUCHYRIEMANN_SRC
  BCCauchyRiemann2D.cpp
  )

ADD_LIBRARY( CauchyRiemannLib STATIC ${CAUCHYRIEMANN_SRC} )

#Create the vera targest for this library
ADD_VERA_CHECKS_RECURSE( CauchyRiemannLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( CauchyRiemannLib *.h )
ADD_CPPCHECK( CauchyRiemannLib ${CAUCHYRIEMANN_SRC} )