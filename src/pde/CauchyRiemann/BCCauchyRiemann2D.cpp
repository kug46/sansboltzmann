// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCCauchyRiemann2D.h"

namespace SANS
{

void
BC<PDECauchyRiemann2D,BCTypeNone>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BC<PDECauchyRiemann2D,BCTypeNone>:" << std::endl;
}

void
BC<PDECauchyRiemann2D,BCTypeDirichlet>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BC<PDECauchyRiemann2D,BCTypeDirichlet>:" << std::endl;
  out << indent << "  A_ = " << A_ << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}

} //namespace SANS
