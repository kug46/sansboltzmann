// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTCELL_FUNCTIONALERRORSQUARED_H
#define OUTPUTCELL_FUNCTIONALERRORSQUARED_H

// Python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"     // Real

#include "OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element cell integrand: solution error squared

template <class OutputFunctional, class SolutionFunction>
class OutputCell_FunctionalErrorSquared :
    public OutputType< OutputCell_FunctionalErrorSquared<OutputFunctional, SolutionFunction> >
{
public:
  typedef typename OutputFunctional::PhysDim PhysDim;
  static const int D = PhysDim::D;

  //Array of solution variables
  template<class T>
  using ArrayQ = typename OutputFunctional::template ArrayQ<T>;

  // Array of outputs
  template<class T>
  using ArrayJ = typename OutputFunctional::template ArrayJ<T>;

  // Matrix required to represent the Jacobian of this output functional
  template<class T>
  using MatrixJ = typename OutputFunctional::template MatrixJ<T>;

  explicit OutputCell_FunctionalErrorSquared( const OutputFunctional& outFcn, const SolutionFunction& solFcn ) : outFcn_(outFcn), solFcn_(solFcn) {}
  //explicit OutputCell_FunctionalErrorSquared( const PyDict& d ) : solFcn_(d) {}

  bool needsSolutionGradient() const { return false; }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, ArrayJ<To>& error ) const
  {
    //Get the exact solution and its gradients
    ArrayQ<Real> qExact, qxExact, qtExact;
    solFcn_.gradient(x, time, qExact, qxExact, qtExact);

    // Compute the exact functional
    ArrayJ<To> outputExact;
    outFcn_(x, time, qExact, qxExact, outputExact);

    // Compute the numerical functional
    ArrayJ<To> output;
    outFcn_(x, time, q, qx, output);

    // Compute the error
    ArrayJ<To> doutput = output - outputExact;

    // Error squared
    error = pow(doutput, 2.0);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, ArrayJ<To>& error ) const
  {
    //Get the exact solution and its gradients
    ArrayQ<Real> qExact, qxExact, qyExact, qtExact;
    solFcn_.gradient(x, y, time, qExact, qxExact, qyExact, qtExact);

    // Compute the exact functional
    ArrayJ<To> outputExact;
    outFcn_(x, y, time, qExact, qxExact, qyExact, outputExact);

    // Compute the numerical functional
    ArrayJ<To> output;
    outFcn_(x, y, time, q, qx, qy, output);

    // Compute the error
    ArrayJ<To> doutput = output - outputExact;

    // Error squared
    error = pow(doutput, 2.0);
  }

  template<class Tq, class Tg, class To>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz, ArrayJ<To>& error ) const
  {
    //Get the exact solution and its gradients
    ArrayQ<Real> qExact, qxExact, qyExact, qzExact, qtExact;
    solFcn_.gradient(x, y, z, time, qExact, qxExact, qyExact, qzExact, qtExact);

    // Compute the exact functional
    ArrayJ<To> outputExact;
    outFcn_(x, y, z, time, qExact, qxExact, qyExact, qzExact, outputExact);

    // Compute the numerical functional
    ArrayJ<To> output;
    outFcn_(x, y, z, time, q, qx, qy, qz, output);

    // Compute the error
    ArrayJ<To> doutput = output - outputExact;

    // Error squared
    error = pow(doutput, 2.0);
  }

protected:
  const OutputFunctional& outFcn_;
  const SolutionFunction& solFcn_;
};

}

#endif //OUTPUTCELL_FUNCTIONALERRORSQUARED_H
