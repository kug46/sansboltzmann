// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PDELorenz.h"
#include "BCLorenz.h"
#include "SourceLorenz.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

// -------------------------------------------------------------------------- //
//
// parameter classes

// cppcheck-suppress passedByValue
void BCLorenzParams<PhysD1, BCTypeDirichlet_mitStateParam>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.q0B));
  allParams.push_back(d.checkInputs(params.q1B));
  allParams.push_back(d.checkInputs(params.q2B));
  d.checkUnknownInputs(allParams);
}
BCLorenzParams<PhysD1, BCTypeDirichlet_mitStateParam> BCLorenzParams<PhysD1, BCTypeDirichlet_mitStateParam>::params;

BCPARAMETER_INSTANTIATE(BCLorenzVector)

} // namespace SANS
