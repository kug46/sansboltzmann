// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDELORENZ_H
#define PDELORENZ_H

// 0D+T as 1D Lorenz system ODE/PDE class

#include <iostream>
#include <string>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "Surreal/PromoteSurreal.h"

#include "Lorenz_Traits.h"
#include "SourceLorenz.h"

namespace SANS
{

// -------------------------------------------------------------------------- //
// PDE class: 1D Lorenz System
//
// in classical form,
// equations:
//    d/dx( u0 )= alpha0 (u1 - u0)
//    d/dx( u1 )= u0 (alpha1 - u2) - u1
//    d/dx( u2 )= u0 u1 - alpha2 u2
//
// converting using flux form, F= u, and a nonlinear source term S(u),
//         u= ( u0, u1, u2 )' and div= d/dx
//    div*u + S(u)= 0
// where
//    S(u)= [ -alpha0 (u1 - u0),
//            -u0 (alpha1 - u2) + u1,
//            -u0 u1 + alpha2 u2 ]
//
// template paramters:
//   T                        solution DOF data type (e.g. double)
//
// member functions:
//   .hasFluxAdvectiveTime       T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. Sw > 0)
//   .setDOFFrom                 set from primitive variable array
// -------------------------------------------------------------------------- //

class PDELorenz
{
public:
  typedef LorenzTraits::PhysDim PhysDim;
  static const int D= LorenzTraits::D; // physical dimensions
  static const int N= LorenzTraits::N; // total solution variables

  template <class T>
  using ArrayQ= LorenzTraits::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using VectorD= DLA::VectorD<T>; // vectors

  template <class T>
  using MatrixQ= LorenzTraits::template MatrixQ<T>; // matrices

  typedef PDELorenz PDEClass;

  // no source term typedefs because it's not templated to hell

  explicit PDELorenz(const Real &alpha0, const Real &alpha1, const Real &alpha2) :
      source_(alpha0, alpha1, alpha2)
  {
    // nothing to do but instantiate
  }

  explicit PDELorenz() {} // source should build with default constructor

  ~PDELorenz() {}

  // flux components
  bool hasFluxAdvectiveTime() const { return false; };
  bool hasFluxAdvective() const { return true; }
  bool hasFluxViscous() const { return false; }
  bool fluxViscousLinearInGradient() const {return true;}
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; } // this may be incorrect?
  bool hasSourceLiftedQuantity() const { return false; } // not sure what this does
  bool hasForcingFunction() const { return false; } // presumably?
  // bool hasForcingFunction() const { return forcing_ != NULL && (*forcing_).hasForcingFunction(); }

  bool needsSolutionGradientforSource() const { return false; }

  // no unsteady temporal flux: Ft(Q)
  template<class Tq, class Tf>
  void fluxAdvectiveTime(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, ArrayQ<Tf> &ft) const
  {} // add zero (no temporal variation (0D+T as 1D))

  // no unsteady tempoaral flux, no jacobian either!!!
  template<class Tq, class Tf>
  void jacobianFluxAdvectiveTime(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, MatrixQ<Tf> &J) const
  {} // add zero (no temporal variation (0D+T as 1D))

  // no strong form of unsteady conservative flux: dU(Q)/dt
  template <class T>
  void strongFluxAdvectiveTime(const Real &x, const Real &time,
      const ArrayQ<T> &q, const ArrayQ<T> &qt, ArrayQ<T> &strongPDE) const
  {} // add zero (no temporal variation (0D+T as 1D))

  // advective flux: F(Q)
  template <class T, class Tf>
  void fluxAdvective(const Real &x, const Real &time,
      const ArrayQ<T> &q, ArrayQ<Tf> &f) const
  {
    // F= u
    f += q;
  }

  template <class Tp, class T, class Tf>
  void fluxAdvective(const Tp &param, const Real &x, const Real &time,
      const ArrayQ<T> &q, ArrayQ<Tf> &f)
  {
    // pass through!
    fluxAdvective(x, time, q, f);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwind(const Real &x, const Real &time, const ArrayQ<Tq> &qL,
      const ArrayQ<Tq> &qR, const Real &nx, ArrayQ<Tf> &fx, Real scale= 1.0) const
  {
    ArrayQ<Tf> f= 0;

    Real ax= 1.0; // temporal advection

    if ((std::abs(nx) - 1.0) >= 1.e-12)
      SANS_DEVELOPER_EXCEPTION("normal is not unit!");

    // full upwinding!
    f= 0.5*(ax*nx)*(qR + qL) - 0.5*scale*fabs(ax*nx)*(qR - qL);

    fx += f;
  }

  template <class Tp, class T, class Tf>
  void fluxAdvectiveUpwind(const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, ArrayQ<Tf>& f,
      Real scale= 1.0) const
  {
    fluxAdvectiveUpwind(x, time, qL, qR, nx, f, scale);
  }

  template <class Tq, class Tf>
  void fluxAdvectiveUpwindSpaceTime(const Real &x, const Real &time,
      const ArrayQ<Tq> &qL, const ArrayQ<Tq> &qR,
      const Real &nx, const Real &nt, ArrayQ<Tf> &f, Real scale= 1.0) const
  {} // add zero (no temporal variation (0D+T as 1D))

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(const Real &x, const Real &time, const ArrayQ<Tq> &q,
      MatrixQ<Tf> &dfdu) const
  {
    dfdu += DLA::Identity();
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const Real &nx, MatrixQ<Tf> &a) const
  {
    SANS_DEVELOPER_EXCEPTION("implement! -cfrontin");
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const Real &nx, const Real &nt, MatrixQ<Tf> &a) const
  {
    SANS_DEVELOPER_EXCEPTION("implement! -cfrontin");
  }

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, ArrayQ<Tf> &strongPDE) const
  {
    // F= u
    strongPDE += q;
  }

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, ArrayQ<Tf> &f) const
  {} // add zero (no viscous flux)

  // spacetime viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Tg> &qt,
      ArrayQ<Tf> &f) const
  {} // add zero (no temporal variation (0D+T as 1D))

  template <class Tq, class Tg, class Tf>
  void fluxViscous(const Real &x, const Real &time,
      const ArrayQ<Tq> &qL, const ArrayQ<Tg> &qxL,
      const ArrayQ<Tq> &qR, const ArrayQ<Tg> &qxR,
      const Real &nx, ArrayQ<Tf> &f) const
  {} // add zero (no viscous flux)

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(const Tp &param, const Real &x, const Real &time,
      const ArrayQ<Tq> &qL, const ArrayQ<Tg> &qxL,
      const ArrayQ<Tq> &qR, const ArrayQ<Tg> &qxR,
      const Real &nx, ArrayQ<Tf> &f) const
  {} // add zero (no viscous flux)

  // perturbation to viscous flux from dqx, dqy
  template <class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx,
      const ArrayQ<Tgu> &dqx, ArrayQ<Tf> &df) const
  {} // add zero (no viscous flux)

  // space-time viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(const Real &x, const Real &time,
      const ArrayQ<Tq> &qL, const ArrayQ<Tg> &qxL, const ArrayQ<Tg> &qtL,
      const ArrayQ<Tq> &qR, const ArrayQ<Tg> &qxR, const ArrayQ<Tg> &qtR,
      const Real &nx, const Real &nt, ArrayQ<Tf> &f) const
  {} // add zero (no viscous flux)

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx,
      MatrixQ<Tk> &kxx) const
  {} // add zero (no viscous flux)

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Tg> &qt,
      MatrixQ<Tk> &kxx, MatrixQ<Tk> &kxt,
      MatrixQ<Tk> &ktx, MatrixQ<Tk> &ktt) const
  {} // add zero (no viscous flux)

  // gradient of diffusion matrix
  template <class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Th> &qxx,
      MatrixQ<Tk> &kxx_x) const
  {} // add zero (no viscous flux)

  // gradient of diffusion matrix
  template <class Tp, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(const Tp &param, const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Th> &qxx,
      MatrixQ<Tk> &kxx_x) const
  {
    // passthrough
    diffusionViscousGradient(x, time, q, qx, qxx, kxx_x); // pass through
  }

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Ta>
  void jacobianFluxViscous(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixQ<Ta> &ax) const
  {} // add zero (no viscous flux)

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Th> &qxx,
      ArrayQ<Tf> &strongPDE) const
  {} // add zero (no viscous flux)

  // space-time strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Tg> &qt,
      const ArrayQ<Th> &qxx, const ArrayQ<Th> &qtx, const ArrayQ<Th> &qtt,
      ArrayQ<Tf> &strongPDE ) const
  {
    // not adding temporal diffusion, so just forward the call to spatial strongFluxViscous
    strongFluxViscous(x, time, q, qx, qxx, strongPDE);
  }

  // solution-dependent source: S(X, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source) const
  {
    // pass to Lorenz system source object
    source_.source(x, time, q, qx, source);
  }

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& s ) const
  {
    // passthrough
    source(x, time, q, qx, s);
  }

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(const Real &xL, const Real &xR, const Real &time,
      const ArrayQ<Tq> &qL, const ArrayQ<Tg> &qxL,
      const ArrayQ<Tq> &qR, const ArrayQ<Tg> &qxR,
      ArrayQ<Ts> &sourceL, ArrayQ<Ts> &sourceR) const
  {
    SANS_DEVELOPER_EXCEPTION("implement! -cfrontin");
  }

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tq, class Ts>
  void sourceLiftedQuantity(const Real &xL, const Real &xR, const Real &time,
      const ArrayQ<Tq> &qL, const ArrayQ<Tq> &qR, Ts &s) const
  {
    SANS_DEVELOPER_EXCEPTION("implement! -cfrontin");
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ta>
  void jacobianSource(const Real &x, const Real &time,
    const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixQ<Ta> &dsdu) const
  {
    // pass to Lorenz system source object
    source_.sourceJacobian(x, time, q, qx, dsdu);
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ta>
  void jacobianSourceHACK(const Real &x, const Real &time,
    const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixQ<Ta> &dsdu) const
  {
    SANS_DEVELOPER_EXCEPTION("should be deprecated AGLS function and not called...");
    // jacobianSource(x, time, q, qx, dsdu); // pass through
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ta>
  void jacobianSourceAbsoluteValue(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixQ<Ta> &dsdu) const
  {
    SANS_DEVELOPER_EXCEPTION("implement! -cfrontin");
  }

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ta>
  void jacobianSourceAbsoluteValue(const Tp& param, const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixQ<Ta> &dsdu) const
  {
    SANS_DEVELOPER_EXCEPTION("implement! -cfrontin");
  }

  // jacobian of source wrt conservation variable gradients: d(S)/d(nabla U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixQ<Ts> &dsdux) const
  {} // add zero (no source dependence on gradient)

  // jacobian of source wrt conservation variable gradients: d(S)/d(nabla U)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceHACK(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixQ<Ts> &dsdux) const
  {
    SANS_DEVELOPER_EXCEPTION("should be deprecated AGLS function and not called...");
    // jacobianGradientSource(x, time, q, qx, dsdux);
  }

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSourceAbsoluteValue(const Real &x, const Real &time,
      const Real &nx, const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx,
      MatrixQ<Ts> &divSdotN) const
  {
    SANS_DEVELOPER_EXCEPTION("implement! -cfrontin");
  } // add zero (no source dependence on gradient)

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradient(const Real& x, const Real& time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Th> &qxx,
      MatrixQ<Ts>& dsdux_x ) const
  {} // add zero (no source dependence on gradient)

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Th, class Ts>
  void jacobianGradientSourceGradientHACK(
      const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Th>& qxx,
      MatrixQ<Ts>& dsdux_x ) const
  {
    SANS_DEVELOPER_EXCEPTION("should be deprecated AGLS function and not called...");
    // jacobianGradientSourceGradient(x, time, q, qx, qxx, dsdux_x);
  }

  // right-hand-side forcing function
  template <class Tf>
  void forcingFunction(const Real &x, const Real &time,
      ArrayQ<Tf> &forcing) const
  {}

  // characteristic speed (needed for timestep)
  template<class Tq, class Ts>
  void speedCharacteristic(Real, Real, Real dx, const ArrayQ<Tq> &q, Ts &speed) const
  {
    SANS_DEVELOPER_EXCEPTION("didn't think this was necessary... implement! -cfrontin");
  }

  // characteristic speed
  template<class Tq, class Ts>
  void speedCharacteristic(Real, Real, const ArrayQ<Tq> &q, Ts &speed) const
  {
    SANS_DEVELOPER_EXCEPTION("didn't think this was necessary... implement! -cfrontin");
  }

  // // update fraction needed for physically valid state
  // void updateFraction(const Real &x, const Real &time,
  //     const ArrayQ<Real> &q, const ArrayQ<Real> &dq,
  //     const Real maxChangeFraction, Real &updateFraction) const;

  // master state
  template <class Tq, class Tf>
  void masterState(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, ArrayQ<Tf> &u) const
  {
    u= q;
  }

  template <class Tp, class Tq, class Tf>
  void masterState(const Tp &param, const Real &x, const Real &time,
      const ArrayQ<Tq> &q, ArrayQ<Tf> &u) const
  {
    masterState(param, x, time, q, u);
  }

  // master state gradient and hessian
  template<class Tq, class Tg, class Tf>
  void masterStateGradient(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, ArrayQ<Tf> &ux) const
  {
    ux= qx;
  }

  template<class Tp, class Tq, class Tg, class Tf>
  void masterStateGradient(const Tp &param, const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, ArrayQ<Tf> &ux) const
  {
    masterStateGradient(x, time, q, qx, ux);
  }

  template<class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Th> &qxx,
      ArrayQ<Tf> &uxx) const
  {
    uxx= qxx;
  }

  template<class Tp, class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(const Tp &param, const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, const ArrayQ<Th> &qxx,
      ArrayQ<Tf> &uxx) const
  {
    masterStateHessian(x, time, q, qx, qxx, uxx);
  }

  // jacobian master state
  template <class Tq, class Tf>
  void jacobianMasterState(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, MatrixQ<Tf> &dudq) const
  {
    dudq= DLA::Identity();
  }

  template <class Tp, class Tq, class Tf>
  void jacobianMasterState(const Tp &param, const Real &x, const Real &time,
      const ArrayQ<Tq> &q, MatrixQ<Tf> &dudq) const
  {
    jacobianMasterState(param, x, time, q, dudq);
  }

  // is state physically valid
  bool isValidState(const ArrayQ<Real> &q) const
  { return true; }

  // set from variable
  ArrayQ<Real> setDOFFrom(const PyDict &d) const
  {
    SANS_DEVELOPER_EXCEPTION("didn't think this was necessary... implement! -cfrontin");
  }

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable(const ArrayQ<T> &rsdPDEIn, VectorD<T> &rsdPDEOut) const
  {
    SANS_ASSERT(rsdPDEOut.m() == N);
    for (int i= 0; i < N; i++)
      rsdPDEOut[i]= rsdPDEIn[i];
    // SANS_DEVELOPER_EXCEPTION("didn't think this was necessary... implement! -cfrontin");
  }

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient(const ArrayQ<T> &rsdAuxIn, VectorD<T> &rsdAuxOut) const
  {
    // rsdAuxOut= rsdAuxIn;
    SANS_DEVELOPER_EXCEPTION("didn't think this was necessary... implement! -cfrontin");
  }

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC(const ArrayQ<T> &rsdBCIn, VectorD<T> &rsdBCOut) const
  {
    SANS_DEVELOPER_EXCEPTION("didn't think this was necessary... implement! -cfrontin");
  }

  // how many residual equations are we dealing with
  int nMonitor() const
  {
    return N;
  }

  // update fraction needed for physically valid state
  void updateFraction(const Real& x, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction) const
  {
    SANS_DEVELOPER_EXCEPTION("implement. -cfrontin");
  }

  // std::vector<std::string> derivedQuantityNames() const;

  void dump(int indentSize, std::ostream &out= std::cout) const;

protected:
  SourceLorenz source_;
}; // PDELorenz

} // namespace SANS

#endif // PDELORENZ_H
