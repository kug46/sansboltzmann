// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOURCELORENZ_H
#define SOURCELORENZ_H

#include <cmath>
#include <iostream>
#include <limits>

// Python must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h" // Real

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include "Lorenz_Traits.h"

namespace SANS
{

struct SourceLorenzParams : noncopyable
{
  const ParameterNumeric<Real> alpha0{"alpha0", 10., NO_RANGE, "sigma from classical Lorenz equations"};
  const ParameterNumeric<Real> alpha1{"alpha1", 28., NO_RANGE, "rho from classical Lorenz equations"};
  const ParameterNumeric<Real> alpha2{"alpha2", 8./3., NO_RANGE, "beta from classical Lorenz equations"};

  static void checkInputs(PyDict d);
  static SourceLorenzParams params;

}; // SourceLorenzParams

// -------------------------------------------------------------------------- //

class SourceLorenz
{
public:
  typedef PhysD1 PhysDim;
  static const int D= LorenzTraits::D; // physical dimensions
  static const int N= LorenzTraits::N; // total solution variables

  template <class T>
  using ArrayQ= LorenzTraits::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using VectorD= DLA::VectorD<T>; // vectors

  template <class T>
  using MatrixQ= LorenzTraits::template MatrixQ<T>; // matrices

  typedef SourceLorenzParams ParamsType;

  explicit SourceLorenz(const Real alpha0= 10., const Real alpha1= 28.,
      const Real alpha2= 8./3.) : alpha0_(alpha0), alpha1_(alpha1),
      alpha2_(alpha2)
  {}

  explicit SourceLorenz(const PyDict &d) :
      alpha0_(d.get(ParamsType::params.alpha0)),
      alpha1_(d.get(ParamsType::params.alpha1)),
      alpha2_(d.get(ParamsType::params.alpha2))
  {}

  template <class Tq, class Tg, class Ts>
  void source(const Real &x, const Real &time,
              const ArrayQ<Tq> &q,
              const ArrayQ<Tg> &qx,
              ArrayQ<Ts> &source) const
  {
    // unpack source variable
    Tq u0= q[0];
    Tq u1= q[1];
    Tq u2= q[2];

    // set in Lorenz equation source (see PDE for details)
    source[0] += -alpha0_*(u1 - u0);
    source[1] += -u0*(alpha1_ - u2) + u1;
    source[2] += -u0*u1 + alpha2_*u2;
  }

  template <class Tq, class Tg, class Ts>
  void sourceJacobian(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixQ<Ts> &dsdu) const
  {
    // unpack
    Tq u0= q(0);
    Tq u1= q(1);
    Tq u2= q(2);

    // derivatives
    dsdu(0, 0) += alpha0_;          dsdu(0, 1) += -alpha0_; dsdu(0, 2) += 0.0;
    dsdu(1, 0) += -(alpha1_ - u2);  dsdu(1, 1) += 1.0;      dsdu(1, 2) += u0;
    dsdu(2, 0) += -u1;              dsdu(2, 1) += -u0;      dsdu(2, 2) += alpha2_;
  }

  ~SourceLorenz() {}

protected:
  Real alpha0_; // Lorenz equation coefficient
  Real alpha1_; // Lorenz equation coefficient
  Real alpha2_; // Lorenz equation coefficient
}; // SourceLorenz

} // namespace SANS

#endif // SOURCELORENZ_H
