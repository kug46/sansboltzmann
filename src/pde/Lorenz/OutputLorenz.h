// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUTLORENZ_H
#define OUTPUTLORENZ_H

// python must be included first
#include "Python/PyDict.h"

#include "tools/SANSnumerics.h"

#include "Lorenz_Traits.h"
#include "PDELorenz.h"

#include "pde/OutputCategory.h"
#include "Topology/Dimension.h"

#include "pde/Lorenz/OutputLorenz.h"
#include "pde/AnalyticFunction/ScalarFunction1D.h"
#include "pde/AnalyticFunction/VectorFunction1D.h"

namespace SANS
{

// -------------------------------------------------------------------------- //
// 1d lorenz output

class OutputLorenz_SolutionErrorSquared : public OutputType<OutputLorenz_SolutionErrorSquared>
{
public:
  typedef PDELorenz PDE;
  typedef LorenzTraits::PhysDim PhysDim;
  static const int D= LorenzTraits::D; // physical dimensions
  static const int N= LorenzTraits::N; // total solution variables

  // array of solution variables
  template <class T>
  using ArrayQ= typename PDE::template ArrayQ<T>;

  // array of solution variables
  template <class T>
  using ArrayJ= T;

  // matrix required to represent the jacobian of this output functional
  template <class T>
  using MatrixJ= ArrayQ<T>;

  explicit OutputLorenz_SolutionErrorSquared(const ScalarFunction1D_ZeroLorenz &sol1Fcn,
      const ScalarFunction1D_ZeroLorenz &sol2Fcn, const ScalarFunction1D_ZeroLorenz &sol3Fcn)
      : sol1Fcn_(sol1Fcn), sol2Fcn_(sol2Fcn), sol3Fcn_(sol3Fcn),
        solFcn_(sol1Fcn, sol2Fcn, sol3Fcn)
  {
  }
  // explicit OutputLorenz_SolutionErrorSquared(const PyDict &d)
  //     : sol1Fcn_(d), sol2Fcn_(d), sol3Fcn_(d) {}

  bool needsSolutionGradient() const { return false; }

  template <class Tq, class Tg, class To>
  void operator()(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, ArrayJ<To> &output) const
  {
    ArrayQ<Real> qExact= solFcn_(x, time);
    ArrayQ<Tq> dq= q - qExact;

    output= 0.0;
    for (int i= 0; i < N; i++)
      output += pow(dq[i], 2.0);
  }

  template <class Tq, class Tg, class To>
  void outputJacobian(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixJ<To> &dJdu) const
  {
    ArrayQ<Real> qExact= solFcn_(x, time);
    ArrayQ<Tq> dq= q - qExact;

    for (int i= 0; i < N; i++)
      dJdu(i, i) += 2*dq;
  }

protected:
  const ScalarFunction1D_ZeroLorenz &sol1Fcn_;
  const ScalarFunction1D_ZeroLorenz &sol2Fcn_;
  const ScalarFunction1D_ZeroLorenz &sol3Fcn_;
  Vector3Function1D<ScalarFunction1D_ZeroLorenz, ScalarFunction1D_ZeroLorenz, ScalarFunction1D_ZeroLorenz> solFcn_;
};

// -------------------------------------------------------------------------- //
class OutputLorenz_SolutionSquared : public OutputType<OutputLorenz_SolutionSquared>
{
public:
  typedef PDELorenz PDE;
  typedef LorenzTraits::PhysDim PhysDim;
  static const int D= LorenzTraits::D; // physical dimensions
  static const int N= LorenzTraits::N; // total solution variables

  // array of solution variables
  template <class T>
  using ArrayQ= typename PDE::template ArrayQ<T>;

  // array of solution variables
  template <class T>
  using ArrayJ= T;

  // matrix required to represent the jacobian of this output functional
  template <class T>
  using MatrixJ= ArrayQ<T>;

  explicit OutputLorenz_SolutionSquared() {}
  explicit OutputLorenz_SolutionSquared(const PyDict &d) {}

  bool needsSolutionGradient() const { return false; }

  template <class Tq, class Tg, class To>
  void operator()(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, ArrayJ<To> &output) const
  {
    // zero output
    output= 0.0;

    for (int i= 0; i < N; i++)
      output += q(i)*q(i);
  }

  template <class Tq, class Tg, class To>
  void outputJacobian(const Real &x, const Real &time,
      const ArrayQ<Tq> &q, const ArrayQ<Tg> &qx, MatrixJ<To> &dJdu) const
  {
    // zero jacobian
    dJdu= 0.0;

    for (int i= 0; i < N; i++)
      dJdu(i)= 2.0*q(i);
  }

};

// // -------------------------------------------------------------------------- //
// // function weighted residual
// template <class WeightFunction>
// class OutputLorenz_FunctionWeightedResidual
//     : public OutputType<OutputLorenz_FunctionWeightedResidual<WeightFunction>>
// {
// public:
//   typedef PhysD1 PhysDim;
//   typedef OutputCategory::WeightedResidual Category;
//
//   static const int D= LorenzTraits::D; // physical dimensions
//   static const int N= LorenzTraits::N; // total solution variables
//
//   template <class T>
//   using ArrayQ= LorenzTraits::template ArrayQ<T>; // solution/residual arrays
//
//   template <class T>
//   using MatrixQ= LorenzTraits::template MatrixQ<T>; // matrices
//
//   explicit OutputLorenz_FunctionWeightedResidual(const WeightFunction &w) : wfcn_(w) {}
//   explicit OutputLorenz_FunctionWeightedResidual(const PyDict &d) : wfcn_(d) {}
//
//   void operator()(const Real &x, const Real &time, ArrayQ<Real> &weight) const
//   {
//     weight= wfcn_(x, time);
//   }
//
// private:
//   const WeightFunction &wfcn_;
// };
//
// // -------------------------------------------------------------------------- //
// // weighted residual
// class OutputLorenz_WeightedResidual
//     : public OutputType<OutputLorenz_WeightedResidual>
// {
// public:
//   typedef PhysD1 PhysDim;
//   typedef OutputCategory::WeightedResidual Category;
//
//   static const int D= LorenzTraits::D; // physical dimensions
//   static const int N= LorenzTraits::N; // total solution variables
//
//   template <class T>
//   using ArrayQ= LorenzTraits::template ArrayQ<T>; // solution/residual arrays
//
//   template <class T>
//   using MatrixQ= LorenzTraits::template MatrixQ<T>; // matrices
//
//   explicit OutputLorenz_WeightedResidual(const Real &w) : w_(w) {}
//
//   void operator()(const Real &x, const Real &time, ArrayQ<Real> &weight) const
//   {
//     weight= w_;
//   }
//
// private:
//   const Real w_;
// };
//
// // -------------------------------------------------------------------------- //
// // function weighted boundary flux
// template <class PDENDConvert, class WeightFunction>
// class OutputLorenz_FunctionWeightedFlux
//     : public OutputType<OutputLorenz_FunctionWeightedFlux<PDENDConvert, WeightFunction>>
// {
// public:
//   typedef PhysD1 PhysDim;
//   typedef PDENDConvert PDEClass;
//   typedef OutputCategory::Functional Category;
//
//   static const int D= LorenzTraits::D; // physical dimensions
//   static const int N= LorenzTraits::N; // total solution variables
//
//   template <class T>
//   using ArrayQ= LorenzTraits::template ArrayQ<T>; // solution/residual arrays
//
//   template <class T>
//   using MatrixQ= LorenzTraits::template MatrixQ<T>; // matrices
//
//   explicit OutputLorenz_FunctionWeightedFlux(const PDEClass &pde,
//       const WeightFunction &w) : pde_(pde), wfcn_(w) {}
//   explicit OutputLorenz_FunctionWeightedFlux(const PDEClass &pde,
//       const PyDict &d) : wfcn_(d) {}
//
//   bool needsSolutionGradient() const {return false;}
//
//   template <class Tq, class Tg, class Tj>
//   void operator()(const Real &x, const Real &time, const ArrayQ<Tq> &q,
//       const ArrayQ<Tg> &qx, const ArrayQ<Tj> &output) const
//   {
//     Real w= wfcn_(x, time);
//
//     ArrayQ<Tq> Fn= 0;
//     ArrayQ<Tq> Fx= 0;
//
//     if (pde_.hasFluxAdvective())
//       pde_.fluxAdvective(x, time, q, Fx);
//
//     Fn= Fx*nx;
//
//     output= w*Fn;
//   }
//
// private:
//   const PDEClass &pde_;
//   const WeightFunction &wfcn_;
// };
//
// // -------------------------------------------------------------------------- //
// // weighted boundary flux
// template <class PDENDConvert>
// class OutputLorenz_WeightedFlux
//     : public OutputType<OutputLorenz_WeightedFlux<PDENDConvert>>
// {
// public:
//   typedef PhysD1 PhysDim;
//   typedef PDENDConvert PDEClass;
//   typedef OutputCategory::Functional Category;
//
//   static const int D= LorenzTraits::D; // physical dimensions
//   static const int N= LorenzTraits::N; // total solution variables
//
//   template <class T>
//   using ArrayQ= LorenzTraits::template ArrayQ<T>; // solution/residual arrays
//
//   template <class T>
//   using MatrixQ= LorenzTraits::template MatrixQ<T>; // matrices
//
//   explicit OutputLorenz_WeightedFlux(const PDEClass &pde, const Real &w)
//       : pde_(pde), w_(w) {}
//
//   // passed but never used
//   bool needsSolutionGradient() const {return false;}
//
//   template <class Tq, class Tg, class Tj>
//   void operator()(const Real &x, const Real &time, const ArrayQ<Tq> &q,
//       const ArrayQ<Tg> &qx, const ArrayQ<Tj> &output) const
//   {
//     ArrayQ<Tq> Fn= 0;
//     ArrayQ<Tq> Fx= 0;
//
//     if (pde_.hasFluxAdvective())
//       pde_.fluxAdvective(x, time, q, Fx);
//
//     Fn= Fx*nx;
//
//     output= w_*Fn;
//   }
//
// private:
//   const PDEClass &pde_;
//   const Real &w_;
// };
//
//
//
//
//
//
//
//
//
//
//
//
//
//

}

#endif // OUTPUTLORENZ_H
