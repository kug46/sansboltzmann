// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SourceLorenz.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SourceLorenzParams::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.alpha0));
  allParams.push_back(d.checkInputs(params.alpha1));
  allParams.push_back(d.checkInputs(params.alpha2));
  d.checkUnknownInputs(allParams);
} // checkInputs
SourceLorenzParams SourceLorenzParams::params;

} // namespace SANS
