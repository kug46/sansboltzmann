// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LORENZ_TRAITS_H
#define LORENZ_TRAITS_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "Topology/Dimension.h"

namespace SANS
{

// forward declaration
class PDELorenz;

class LorenzTraits
{
public:
  typedef PhysD1 PhysDim;
  static const int D= PhysDim::D; // physical dimensions
  static const int N= 3; // total solution variables

  template <class T>
  using ArrayQ= DLA::VectorS<N, T>; // solution/residual arrays

  template <class T>
  using MatrixQ= DLA::MatrixS<N, N, T>; // matrices
};

}

#endif // LORENZ_TRAITS_H
