// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCLORENZ_H
#define BCLORENZ_H

// Lorenz BC class

// PyDict must be included first
#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h" // Real
#include "pde/BCCategory.h"
#include "pde/BCNone.h"
#include "Topology/Dimension.h"
#include "pde/Lorenz/Lorenz_Traits.h"
#include "pde/Lorenz/PDELorenz.h"

#include <iostream>

#include <boost/mpl/vector.hpp>

namespace SANS
{

// -------------------------------------------------------------------------- //
// BC class: Lorenz in time

template <class PhysDim, class BCType>
class BCLorenz;

template <class PhysDim, class BCType>
struct BCLorenzParams;

// -------------------------------------------------------------------------- //
// BC types

class BCTypeDirichlet_mitState;

using BCLorenzVector= boost::mpl::vector2< BCNone<PhysD1, LorenzTraits::N>,
                                           BCLorenz<PhysD1, BCTypeDirichlet_mitState>
                                         >;

// -------------------------------------------------------------------------- //
// Dirichlet BC: set in known boundary flux

class BCTypeDirichlet_mitStateParam;

template <>
struct BCLorenzParams<PhysD1, BCTypeDirichlet_mitStateParam> : noncopyable
{
  const ParameterNumeric<Real> q0B{"q0B", 1.0, NO_RANGE, "boundary condition 0"};
  const ParameterNumeric<Real> q1B{"q1B", 1.0, NO_RANGE, "boundary condition 1"};
  const ParameterNumeric<Real> q2B{"q2B", 1.0, NO_RANGE, "boundary condition 2"};

  static constexpr const char* BCName{"Dirichlet_mitState"};
  struct Option
  {
    const DictOption Dirichlet_mitState{ BCLorenzParams::BCName, BCLorenzParams::checkInputs };
  };

  static void checkInputs(PyDict d);
  static BCLorenzParams params;
};

template <>
class BCLorenz<PhysD1, BCTypeDirichlet_mitState> : public BCType<BCLorenz<PhysD1, BCTypeDirichlet_mitState>>
{
public:
  typedef BCCategory::Flux_mitState Category;
  typedef BCLorenzParams<PhysD1, BCTypeDirichlet_mitStateParam> ParamsType;

  typedef PhysD1 PhysDim;
  static const int D= LorenzTraits::D; // physical dimensions
  static const int N= LorenzTraits::N; // total solution variables

  static const int NBC= 1; // total BCs

  template <class T>
  using ArrayQ= LorenzTraits::template ArrayQ<T>; // solution/residual arrays

  template <class T>
  using MatrixQ= LorenzTraits::template MatrixQ<T>; // matrices

  BCLorenz(const PDELorenz &pde, const PyDict &d) :
      pde_(pde),
      qB_({d.get(ParamsType::params.q0B),
           d.get(ParamsType::params.q1B),
           d.get(ParamsType::params.q2B)})
  {}

  BCLorenz(const PDELorenz &pde, const ArrayQ<Real> &qB) : pde_(pde), qB_(qB)
  {}

  // BCLorenz(const BCLorenz& bc0) : pde_(bc0.pde_), qB_(bc0.qB_) {}

  virtual ~BCLorenz()
  {}

  // BCLorenz &operator= (const BCLorenz &new);

  // BC state vector
  template <class T>
  void state(const Real &x, const Real &time, const Real &nx,
      const ArrayQ<T> &qI, ArrayQ<T> &qB) const
  {
    qB= qB_;
  }

  // normal BC flux
  template <class Tq, class Tg, class Tf>
  void fluxNormal(const Real &x, const Real &time, const Real &nx,
      const ArrayQ<Tq> &qI, const ArrayQ<Tg> &qIx,
      const ArrayQ<Tq> &qB, ArrayQ<Tf> &Fn) const
  {
    // add upwinded advective flux
    pde_.fluxAdvectiveUpwind(x, time, qI, qB, nx, Fn);

    // do nothing else...
  }

  // is the boundary state valid
  bool isValidState(const Real &nx, const ArrayQ<Real> &q) const {return true;}

  // flux things
  bool hasFluxViscous() const {return false;}

  void dump(int indentSize, std::ostream &out= std::cout) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "BCLorenz<PhysD1, BCTypeDirichlet_mitState>: " << std::endl;
    out << indent << "qB_= " << qB_ << std::endl;
  }

private:
  const PDELorenz& pde_;
  ArrayQ<Real> qB_;
};



} // namespace SANS

#endif // BCLORENZ_H
