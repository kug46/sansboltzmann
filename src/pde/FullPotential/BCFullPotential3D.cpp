// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCFullPotential3D.h"

#include "Surreal/SurrealS.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void BCFullPotential3DParams<BCTypeInflow>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.q));
  d.checkUnknownInputs(allParams);
}
BCFullPotential3DParams<BCTypeInflow> BCFullPotential3DParams<BCTypeInflow>::params;

template<>
void
BCFullPotential3D<BCTypeInflow, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCFullPotential3D<BCTypeInflow>:" << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}

// cppcheck-suppress passedByValue
void BCFullPotential3DParams<BCTypeOutflow>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gradqn));
  d.checkUnknownInputs(allParams);
}
BCFullPotential3DParams<BCTypeOutflow> BCFullPotential3DParams<BCTypeOutflow>::params;

template<>
void
BCFullPotential3D<BCTypeOutflow, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCFullPotential3D<BCTypeOutflow>:" << std::endl;
}

// cppcheck-suppress passedByValue
void BCFullPotential3DParams<BCTypeWall>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCFullPotential3DParams<BCTypeWall> BCFullPotential3DParams<BCTypeWall>::params;

template<>
void
BCFullPotential3D<BCTypeWall, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCFullPotential3D<BCTypeWall>:" << std::endl;
  out << indent << "  (u_, v_, w_) = (" << U_[0] << ", " << U_[1] << ", " << U_[2] << ")" << std::endl;
}

#if 0
void
BCFullPotential3D<BCTypeFarfieldVortex, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCFullPotential3D<BCTypeFarfieldVortex>:" << std::endl;
  out << indent << "  (x0_, y0_) = (" << x0_ << ", " << y0_ << ")" << std::endl;
}
#endif

//===========================================================================//
// Instantiate the BC parameters
BCPARAMETER_INSTANTIATE( BCFullPotential3DVector<Real> )
BCPARAMETER_INSTANTIATE( BCFullPotential3DVector<SurrealS<1>> )

} //namespace SANS
