// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCLINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_H
#define BCLINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_H

// 2-D Linearized Incompressible Potential BC class

// NOTE: old style BC classes with abstract base class and dynamic dispatch
// for templated member functions (C++ does not allow for templated virtual functions);
// base class and inheritence will be removed at some point

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "PDELinearizedIncompressiblePotential2D.h"
#include "SolutionFunction2D_LIP.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "pde/BCCategory.h"

#include <iostream>
#include <string>
#include <typeinfo>     // typeid

namespace SANS
{

//----------------------------------------------------------------------------//
// BC base class: 2-D Linearized Incompressible Potential
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   PDE                  PDE type
//   BCType               BC type (e.g. BCTypeNeumann)
//
// NOTE: see end of file for definitions of member functions coefficients/data
//----------------------------------------------------------------------------//

template <class PDE, class BCType>
class BC;

//----------------------------------------------------------------------------//
// Dirichlet BC:  phi = bcdata

class BCTypeDirichlet;

template <>
class BC<PDELinearizedIncompressiblePotential2D, BCTypeDirichlet> :
    public BCBase
{
public:
  typedef BCCategory::LinearScalar_mitLG Category;

  typedef PhysD2 PhysDim;
  static const int D = 2;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = PDELinearizedIncompressiblePotential2D::ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = PDELinearizedIncompressiblePotential2D::MatrixQ<T>;  // matrices

  explicit BC( const Real& bcdata ) : bcdata_(bcdata) {}
  virtual ~BC() {}

  BC& operator=( const BC& );

  virtual const std::type_info& derivedTypeID() const { return typeid(*this); }

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> bcdata_;
};


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeDirichlet>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeDirichlet>::data(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    const SANS::DLA::VectorD<T>&, ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}


//----------------------------------------------------------------------------//
// Neumann BC:  d(phi)/dn = bcdata

class BCTypeNeumann;

template <>
class BC<PDELinearizedIncompressiblePotential2D, BCTypeNeumann> :
    public BCBase
{
public:
  typedef BCCategory::LinearScalar_mitLG Category;

  typedef PhysD2 PhysDim;
  static const int D = 2;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = PDELinearizedIncompressiblePotential2D::ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = PDELinearizedIncompressiblePotential2D::MatrixQ<T>;  // matrices

  explicit BC( const Real& bcdata ) : bcdata_(bcdata) {}
  virtual ~BC() {}

  BC& operator=( const BC& );

  virtual const std::type_info& derivedTypeID() const { return typeid(*this); }

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> bcdata_;
};


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeNeumann>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;
  B = 1;
}


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeNeumann>::data(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    const SANS::DLA::VectorD<T>&, ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}


//----------------------------------------------------------------------------//
// wall BC:  d(phi)/dn = - d(phi_uniform)/dn

class BCTypeWall;

template <>
class BC<PDELinearizedIncompressiblePotential2D, BCTypeWall> :
    public BCBase
{
public:
  typedef BCCategory::LinearScalar_mitLG Category;

  typedef PhysD2 PhysDim;
  static const int D = 2;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = PDELinearizedIncompressiblePotential2D::ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = PDELinearizedIncompressiblePotential2D::MatrixQ<T>;  // matrices

  BC( const Real& u, const Real& v ) : u_(u), v_(v) {}
  virtual ~BC() {}

  BC& operator=( const BC& );

  virtual const std::type_info& derivedTypeID() const { return typeid(*this); }

  // BC coefficients:  A phi + B ( -d(phi)/dn )
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real u_, v_;      // freestream velocity
};


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeWall>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;
  B = 1;
}


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeWall>::data(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    const SANS::DLA::VectorD<T>&, ArrayQ<T>& bcdata ) const
{
  bcdata = -(nx*u_ + ny*v_);
}


//----------------------------------------------------------------------------//
// Farfield BC w/ vortex:  phi = vortex
//
//  phi = circulation * theta / (2*PI)
//
//  where theta = arctan( (y - y0)/(x - x0) ); theta in [0, 2*PI)

class BCTypeFarfieldVortex;

template <>
class BC<PDELinearizedIncompressiblePotential2D, BCTypeFarfieldVortex> :
    public BCBase
{
public:
  typedef BCCategory::LinearScalar_mitLG Category;

  typedef PhysD2 PhysDim;
  static const int D = 2;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = PDELinearizedIncompressiblePotential2D::ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = PDELinearizedIncompressiblePotential2D::MatrixQ<T>;  // matrices

  BC( const Real& x0, const Real& y0 ) : x0_(x0), y0_(y0) {}
  virtual ~BC() {}

  BC& operator=( const BC& );

  virtual const std::type_info& derivedTypeID() const { return typeid(*this); }

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data:  vortex
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real x0_, y0_;      // vortex centroid
};


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeFarfieldVortex>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeFarfieldVortex>::data(
    const Real& x, const Real& y, const Real& time, const Real&, const Real&,
    const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const
{
  T circ = globalVar[0];
  Real th;

  th = atan2( y - y0_, x - x0_ );
  if (th < 0)  th += 2*PI;          // makes th in [0, 2*PI)

  bcdata =  -circ * th / (2*PI);
  //std::cout << x << " " << y << " " << bcdata(0) << std::endl;
}


//----------------------------------------------------------------------------//
// Solution BC:  u = uexact(x,y)
//
// A = 1; B = 0

template<class >
class BCTypeFunction;
class SolutionDummy;

template <>
class BC<PDELinearizedIncompressiblePotential2D, BCTypeFunction<SolutionDummy> > :
    public BCBase
{
public:
  typedef BCCategory::LinearScalar_mitLG Category;

  typedef PhysD2 PhysDim;
  static const int D = 2;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = PDELinearizedIncompressiblePotential2D::ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = PDELinearizedIncompressiblePotential2D::MatrixQ<T>;  // matrices

  explicit BC( const SolutionFunction2D<N>* uexact ) :
      uexact_(uexact) {}
  ~BC() {}

  BC& operator=( const BC& );

  virtual const std::type_info& derivedTypeID() const { return typeid(*this); }

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const SolutionFunction2D<N>* uexact_;
};


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeFunction<SolutionDummy> >::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}


template <class T>
inline void
BC<PDELinearizedIncompressiblePotential2D, BCTypeFunction<SolutionDummy> >::data(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    const SANS::DLA::VectorD<T>&, ArrayQ<T>& bcdata ) const
{
  bcdata = (*uexact_)(x, y, time);
}

} //namespace SANS

#endif  // BCLINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_H
