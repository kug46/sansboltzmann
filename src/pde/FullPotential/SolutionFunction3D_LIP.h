// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION3D_LIP_FIXCIRC_H
#define SOLUTIONFUNCTION3D_LIP_FIXCIRC_H

// 3-D Linearized Incompressible Potential PDE: exact and MMS solutions

#include <cmath>
#include <complex>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: exact/MMS solution functions
//
// member functions:
//   .()      functor returning solution
//----------------------------------------------------------------------------//

template <int N>
class SolutionFunction3D;


template <>
class SolutionFunction3D<1>
{
public:
  typedef Real ArrayQ;

  virtual ~SolutionFunction3D() {}

  virtual ArrayQ operator()( const Real x, const Real y, const Real z, const Real time ) const = 0;

  virtual void operator()( const Real x, const Real y, const Real z, const Real time,
                           ArrayQ& q,
                           ArrayQ& qx, ArrayQ& qy, ArrayQ& qz,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qxz,
                                        ArrayQ& qyy, ArrayQ& qyz,
                                                     ArrayQ& qzz) const = 0;

protected:
  SolutionFunction3D() {}  // abstract base class
};


//----------------------------------------------------------------------------//
//  Trefftz plane potential for elliptically loaded wing
//
//    phi(y,z)
//
//  Notes:
//  (a) wake positioned at -1 < y < 1, z = 0
//  (b) cross-plane potential satisfies Laplace, so streamwise d(phi)/dx = 0

class EllipticPotential : public SolutionFunction3D<1>
{
public:
  typedef Real ArrayQ;
  typedef std::complex<double> Complex;

  explicit EllipticPotential() {}
  ~EllipticPotential() {}

  // complex potential:  F(z) = phi + I streamfunction
  // NOTE: correct only for Re[z] < 0
  // NOTE: analytic behavior discontinuous at Re[z] = 0, so need to nudge negative
  // cppcheck-suppress passedByValue
  Complex F( Complex z ) const
  {
    Complex I(0,1);

    if (z.real() == 0)  z -= 1e-32;
    Complex pot = 0.5*I*(z + sqrt(z*z - 1.0));
    //std::cout << "F(" << z << ") = " << pot << std::endl;

    return pot;
  }

  // complex velocity:  d(F)/d(z) = v - I w
  // NOTE: correct only for Re[z] < 0
  Complex Fz( const Complex& z ) const
  {
    Complex I(0,1);
    Complex vel = 0.5*I*(1. + z/sqrt(z*z - 1.0));

    return vel;
  }

  // potential
  Real operator()( const Real x, const Real y, const Real z, const Real time ) const
  {
    Complex I(0,1);

    return y > 0 ? F(-y + I*z).real() : F(y + I*z).real();
  }

  Real operator()( const DLA::VectorS<3,Real>& X ) const
  {
    return operator()(X[0], X[1], X[2], 0.);
  }

  // velocity
  template<class T>
  void operator()( const DLA::VectorS<3,Real>& X, DLA::VectorS<3,T>& gradq ) const
  {
    Real y = X[1];
    Real z = X[2];

    Complex I(0,1);

    gradq[0] = 0;
    gradq[1] = y > 0 ? -Fz(-y + I*z).real() :  Fz(y + I*z).real();
    gradq[2] = y > 0 ? -Fz(-y + I*z).imag() : -Fz(y + I*z).imag();
  }

  virtual void operator()( const Real x, const Real y, const Real z, const Real time,
                           ArrayQ& q,
                           ArrayQ& qx, ArrayQ& qy, ArrayQ& qz,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qxz,
                                        ArrayQ& qyy, ArrayQ& qyz,
                                                     ArrayQ& qzz) const
  {
    Complex I(0,1);

    q  = y > 0 ?   F(-y + I*z).real() :   F(y + I*z).real();
    qx = 0;
    qy = y > 0 ? -Fz(-y + I*z).real() :  Fz(y + I*z).real();
    qz = y > 0 ? -Fz(-y + I*z).imag() : -Fz(y + I*z).imag();

    //TODO:
    //Not setting 2nd derivatives... hopefully valgrind will notice if they are used
  }
};


#if 0
//----------------------------------------------------------------------------//
// solution: const

template <int N>
class SolutionFunction3D_Const;

template <>
class SolutionFunction3D_Const<1> : public SolutionFunction3D<1>
{
public:
  typedef DLA::VectorS<1,Real> ArrayQ;

  explicit SolutionFunction3D_Const( const Real& a0 ) : a0_(a0) {}
  virtual ~SolutionFunction3D_Const() {}

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const
    { return a0_; }

  virtual void operator()( const Real& x, const Real& y, const Real& time,
                           ArrayQ& q, ArrayQ& qx, ArrayQ& qy,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qyy ) const
    { q = a0_; qx = 0; qy = 0; qxx = 0; qxy = 0; qyy = 0; }

private:
  Real a0_;
};


//----------------------------------------------------------------------------//
// solution: linear

template <int N>
class SolutionFunction3D_Linear;

template <>
class SolutionFunction3D_Linear<1> : public SolutionFunction3D<1>
{
public:
  typedef DLA::VectorS<1,Real> ArrayQ;

  SolutionFunction3D_Linear( const Real& a0, const Real& ax, const Real& ay ) : a0_(a0), ax_(ax), ay_(ay) {}
  virtual ~SolutionFunction3D_Linear() {}

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const
    { return a0_ + ax_*x + ay_*y; }

  virtual void operator()( const Real& x, const Real& y, const Real& time,
                           ArrayQ& q, ArrayQ& qx, ArrayQ& qy,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qyy ) const
    { q = a0_ + ax_*x + ay_*y; qx = ax_; qy = ay_; qxx = 0; qxy = 0; qyy = 0; }

private:
  Real a0_, ax_, ay_;
};


//----------------------------------------------------------------------------//
// solution: sin(2*PI*x)*sin(2*PI*y)

template <int N>
class SolutionFunction3D_SineSine;

template <>
class SolutionFunction3D_SineSine<1> : public SolutionFunction3D<1>
{
public:
  typedef DLA::VectorS<1,Real> ArrayQ;

  SolutionFunction3D_SineSine() {}
  virtual ~SolutionFunction3D_SineSine() {}

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const
    { return sin(2*PI*x)*sin(2*PI*y); }

  virtual void operator()( const Real& x, const Real& y, const Real& time,
                           ArrayQ& q, ArrayQ& qx, ArrayQ& qy,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qyy ) const
  {
    Real snx = sin(2*PI*x), csx = cos(2*PI*x);
    Real sny = sin(2*PI*y), csy = cos(2*PI*y);

    q   = snx*sny;
    qx  = 2*PI*csx*sny;
    qy  = 2*PI*snx*csy;
    qxx = -4*PI*PI*snx*sny;
    qxy =  4*PI*PI*csx*csy;
    qyy = -4*PI*PI*snx*sny;
  }
};
#endif
} //namespace SANS

#endif  // SOLUTIONFUNCTION3D_LIP_FIXCIRC_H
