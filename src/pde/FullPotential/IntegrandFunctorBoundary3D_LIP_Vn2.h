// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDFUNCTORBOUNDARY3D_LIP_VN2_H
#define INTEGRANDFUNCTORBOUNDARY3D_LIP_VN2_H

#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "PDELinearizedIncompressiblePotential3D.h"

#include "Discretization/Integrand_Type.h"
#include "pde/OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element boundary integrand: Force
template <class Tg>
class IntegrandBoundary3D_LIP_Vn2 :
    public IntegrandBoundaryTraceType< IntegrandBoundary3D_LIP_Vn2<Tg> >
{
public:
  typedef PDELinearizedIncompressiblePotential3D<Tg> PDE;
  typedef Galerkin DiscTag;
  typedef typename OutputCategory::Functional Category;

  typedef typename PDE::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;

  // Array of output functionals
  template<class T>
  using ArrayJ = T;

  // Matrix required to represent the Jacobian of this functional
  template<class T>
  using MatrixJ = T;

  IntegrandBoundary3D_LIP_Vn2( const PDE& pde, const std::vector<int>& BoundaryGroups )
    : BoundaryGroups_(BoundaryGroups), pde_(pde)
  {
  }

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL>
  class Functor
  {
  public:
    typedef ElementXField<PhysD3, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysD3, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const PDE& pde,
             const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementXFieldL& xfldElem,
             const ElementQFieldL& qfldElem ) :
               pde_(pde),
               xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
               xfldElem_(xfldElem), qfldElem_(qfldElem) {}

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    typedef typename promote_Surreal<T,Tg>::type Ts;

    // trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, ArrayJ<Ts>& integrand ) const
    {
      VectorX N, X;
      DLA::VectorS<3, ArrayQ<T> > gradq;
      QuadPointCellType sRefCell;

      TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTrace_, sRefTrace, sRefCell );

      xfldElem_.evalGradient( sRefCell, qfldElem_, gradq );

      xfldElem_.eval( sRefCell, X );

      // unit normal: points out of domain
      xfldElemTrace_.unitNormal( sRefTrace, N );

      DLA::VectorS<3,Tg> U = pde_.Up(X);

      // Compute the velocity vector
      DLA::VectorS<3, ArrayQ<Ts> > V = U + gradq;

      ArrayQ<Ts> Vn = dot(V,N);

      // Force integrand
      integrand = Vn*Vn;
      //integrand = (0.5*U2 - 0.5*V2)*N;
    }

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class BCIntegrandBoundaryTrace>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL>
  integrand(const BCIntegrandBoundaryTrace& fcnBC,
            const ElementXField<PhysD3, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysD3, TopoDimCell , TopologyL    >& xfldElem,
            const Element<ArrayQ<T>   , TopoDimCell , TopologyL    >& qfldElem ) const
  {
    return {pde_,
            xfldElemTrace, canonicalTrace,
            xfldElem, qfldElem };
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL>
  integrand(const ElementXField<PhysD3, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysD3, TopoDimCell , TopologyL    >& xfldElem,
            const Element<ArrayQ<T>   , TopoDimCell , TopologyL    >& qfldElem ) const
  {
    return {pde_,
            xfldElemTrace, canonicalTrace,
            xfldElem, qfldElem };
  }

protected:
  const std::vector<int> BoundaryGroups_;
  const PDE& pde_;
};


}

#endif //INTEGRANDFUNCTORBOUNDARY3D_LIP_VN2_H
