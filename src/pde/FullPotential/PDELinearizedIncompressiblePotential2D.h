// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDELINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_H
#define PDELINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_H

// 2-D Linearized Incompressible Potential PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"

#include <iostream>
#include <string>
#include <memory>

namespace SANS
{

template <int N>
class SolutionFunction2D;


//----------------------------------------------------------------------------//
// PDE class: 2-D Linearized Incompressible Potential
//
//   phifull = (a*x + b*y) + phi(x,y)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

class PDELinearizedIncompressiblePotential2D
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 1;                     // total solution variables

  template <class T>
  using ArrayQ = T;           // solution/residual arrays

  template <class T>
  using MatrixQ = T;          // matrices

  template <class T>
  using ArrayD = DLA::VectorD<T>;   // residual norms

  typedef std::shared_ptr< SolutionFunction2D<1> > SolutionFunction2D_ptr;

  PDELinearizedIncompressiblePotential2D( const Real& a, const Real& b ) :
      a_(a), b_(b), sln_(NULL) {}
  PDELinearizedIncompressiblePotential2D( const Real& a, const Real& b, const SolutionFunction2D_ptr& sln ) :
      a_(a), b_(b), sln_(sln) {}
  PDELinearizedIncompressiblePotential2D( const PDELinearizedIncompressiblePotential2D& pde ) :
      a_(pde.a_), b_(pde.b_), sln_(pde.sln_) {}
  ~PDELinearizedIncompressiblePotential2D() {}

  PDELinearizedIncompressiblePotential2D& operator=( const PDELinearizedIncompressiblePotential2D& );

  // flux components
  bool hasFluxAdvectiveTime() const { return false; }
  bool hasFluxAdvective() const { return false; }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasForcingFunction() const { return (sln_ != NULL); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // freestream velocity
  void freestream( Real& a, Real& b ) const { a = a_; b = b_; }

  // unsteady temporal flux: Ft(Q)
  template <class T>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& ft ) const {}

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& J ) const {}

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
  {
    uCons = q;
  }

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template <class T>
  inline void
  jacobianMasterState(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    dudq = DLA::Identity();
  }

  // advective flux: F(Q)
  template<class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}
  template <class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const {}

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const {}

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const {}

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

  // solution-dependent source: S(X, D, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const {}

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // jacobian of solution-dependent source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of solution-dependent source wrt conservation variables: d(S)/d(gradU)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const {}

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const Real& dx, const Real& dy, const ArrayQ<T>& q, Real& speed ) const {}

  // characteristic speed
  template <class Tq, class Tc>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, Tc& speed ) const {}

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return true; }

  // update fraction needed for physically valid state
  template <class T>
  void updateFraction(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& dq,
      Real maxChangeFraction, Real& updateFraction ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, ArrayD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, ArrayD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, ArrayD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real a_, b_;                     // freestream potential: a*x + b*y

  SolutionFunction2D_ptr sln_;     // solution for RHS forcing function
};


inline PDELinearizedIncompressiblePotential2D&
PDELinearizedIncompressiblePotential2D::operator=( const PDELinearizedIncompressiblePotential2D& pde )
{
  a_   = pde.a_;
  b_   = pde.b_;
  sln_ = pde.sln_;

  return *this;
}


// viscous flux
template <class Tq, class Tg, class Tf>
inline void
PDELinearizedIncompressiblePotential2D::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
  f -= qx;
  g -= qy;
}


// viscous flux: normal flux with left and right states
template <class Tq, class Tg, class Tf>
inline void
PDELinearizedIncompressiblePotential2D::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  f -= 0.5*(nx*(qxL + qxR) + ny*(qyL + qyR));
}


// viscous diffusion matrix: d(Fv)/d(UX)
template <class Tq, class Tg, class Tk>
inline void
PDELinearizedIncompressiblePotential2D::diffusionViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
  kxx += 1;
  kxy += 0;
  kyx += 0;
  kyy += 1;
}


// strong form of viscous flux: d(Fv)/d(X)
template <class Tq, class Tg, class Th, class Tf>
inline void
PDELinearizedIncompressiblePotential2D::strongFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
  strongPDE -= qxx + qyy;
}


// right-hand-side forcing function
inline void
PDELinearizedIncompressiblePotential2D::forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  SANS_ASSERT_MSG( sln_ != NULL, "RHS forcing function can only be used with a provided solution" );

#if 0
  ArrayQ<Real> q, qx, qy, qxx, qxy, qyy;
  (*sln_)( x, y, q, qx, qy, qxx, qxy, qyy );

  strongFluxViscous( x, y, q, qx, qy, qxx, qxy, qyy, forcing );
#endif
}


// set from primitive variable array
template <class T>
inline void
PDELinearizedIncompressiblePotential2D::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q = data[0];
}


// interpret residuals of the solution variable
template <class T>
inline void
PDELinearizedIncompressiblePotential2D::interpResidVariable( const ArrayQ<T>& rsdPDEIn, ArrayD<T>& rsdPDEOut ) const
{
  rsdPDEOut[0] = sqrt( rsdPDEIn*rsdPDEIn );
}


// interpret residuals of the gradients in the solution variable
template <class T>
inline void
PDELinearizedIncompressiblePotential2D::interpResidGradient( const ArrayQ<T>& rsdAuxIn, ArrayD<T>& rsdAuxOut ) const
{
  rsdAuxOut[0] = sqrt( rsdAuxIn*rsdAuxIn );
}


// interpret residuals at the boundary (should forward to BCs)
template <class T>
inline void
PDELinearizedIncompressiblePotential2D::interpResidBC( const ArrayQ<T>& rsdBCIn, ArrayD<T>& rsdBCOut ) const
{
  rsdBCOut[0] = sqrt( rsdBCIn*rsdBCIn );
}


// how many residual equations are we dealing with
inline int
PDELinearizedIncompressiblePotential2D::nMonitor() const
{
  return 1;
}

} //namespace SANS

#endif  // PDELINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_H
