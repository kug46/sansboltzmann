// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "SolutionFunction_Potential2D.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void SolutionFunction_Potential2D_Const::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.constant));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Potential2D_Const::ParamsType SolutionFunction_Potential2D_Const::ParamsType::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Potential2D_Linear::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.constant));
  allParams.push_back(d.checkInputs(params.gradx));
  allParams.push_back(d.checkInputs(params.grady));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Potential2D_Linear::ParamsType SolutionFunction_Potential2D_Linear::ParamsType::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Potential2D_SineSine::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Potential2D_SineSine::ParamsType SolutionFunction_Potential2D_SineSine::ParamsType::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Potential2D_CubicSourceBump::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.tau));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Potential2D_CubicSourceBump::ParamsType SolutionFunction_Potential2D_CubicSourceBump::ParamsType::params;

// cppcheck-suppress passedByValue
void SolutionFunction_Potential2D_Joukowski::ParamsType::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.e));
  allParams.push_back(d.checkInputs(params.aoa));
  d.checkUnknownInputs(allParams);
}
SolutionFunction_Potential2D_Joukowski::ParamsType SolutionFunction_Potential2D_Joukowski::ParamsType::params;

}
