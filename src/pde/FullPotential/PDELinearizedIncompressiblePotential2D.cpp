// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PDELinearizedIncompressiblePotential2D.h"

namespace SANS
{

void
PDELinearizedIncompressiblePotential2D::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDELinearizedIncompressiblePotential2D: "
                << "a_ = " << a_ << " b_ = " << b_ << std::endl;
  if (sln_ == NULL)
  {
    out << indent << "PDELinearizedIncompressiblePotential2D: sln_ = NULL" << std::endl;
  }
  else
  {
    out << indent << "PDELinearizedIncompressiblePotential2D: sln_ =" << std::endl;
    //sln_->dump(indentSize+2, out);
  }
}

} //namespace SANS
