// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCPotential2D_SolutionFunction.h"

#define PYDICT_INSTANTIATE
#include "Python/PyDict_impl.h"

namespace SANS
{

//PYDICT_PARAMETER_OPTION_INSTANTIATE(BCPotential2D_SolutionFunction_OptionsList::Function)
template ParameterOption<BCPotential2D_SolutionFunction_Params::SolutionOptions>::ExtractType
PyDict::get(const ParameterType<ParameterOption<BCPotential2D_SolutionFunction_Params::SolutionOptions>>&) const;

#if 0   // should this even be defined?
// cppcheck-suppress passedByValue
void BCPotential2D_SolutionFunction_Params::ParamsType::checkInputs( PyDict d )
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.constant));
  d.checkUnknownInputs(allParams);
}
BCPotential2D_SolutionFunction_Params::ParamsType BCPotential2D_SolutionFunction_Params::ParamsType::params;
#endif

//typename BCPotential2D_SolutionFunction::Function_ptr
BCPotential2D_SolutionFunction::Function_ptr
BCPotential2D_SolutionFunction::getSolution( const PyDict& d )
{
  BCPotential2D_SolutionFunction_Params params;

  DictKeyPair solutionParam = d.get(params.Function);

  if ( solutionParam == params.Function.Const )
    return Function_ptr( static_cast<Function2DBaseType*>( new SolutionFunction_Potential2D_Const( solutionParam ) ) );
  else if ( solutionParam == params.Function.Linear )
    return Function_ptr( static_cast<Function2DBaseType*>( new SolutionFunction_Potential2D_Linear( solutionParam ) ) );
  else if ( solutionParam == params.Function.SineSine )
    return Function_ptr( static_cast<Function2DBaseType*>( new SolutionFunction_Potential2D_SineSine( solutionParam ) ) );
  else if ( solutionParam == params.Function.CubicSourceBump )
    return Function_ptr( static_cast<Function2DBaseType*>( new SolutionFunction_Potential2D_CubicSourceBump( solutionParam ) ) );
  else if ( solutionParam == params.Function.Joukowski )
    return Function_ptr( static_cast<Function2DBaseType*>( new SolutionFunction_Potential2D_Joukowski( solutionParam ) ) );

  // Should never get here if checkInputs was called
  SANS_DEVELOPER_EXCEPTION( "Unrecognized solution function specified" );
  return NULL; // suppress compiler warnings
}

} //namespace SANS
