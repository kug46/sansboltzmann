// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PDELinearizedIncompressiblePotential3D.h"

namespace SANS
{

template <class Tg>
void
PDELinearizedIncompressiblePotential3D<Tg>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDELinearizedIncompressiblePotential3D: "
                << "a_ = " << U_[0] << " b_ = " << U_[1] << " c_ = " << U_[2] << std::endl;
  if (sln_ == NULL)
  {
    out << indent << "PDELinearizedIncompressiblePotential3D: sln_ = NULL" << std::endl;
  }
  else
  {
    out << indent << "PDELinearizedIncompressiblePotential3D: sln_ =" << std::endl;
    //sln_->dump(indentSize+2, out);
  }
}

template class PDELinearizedIncompressiblePotential3D<Real>;

} //namespace SANS
