// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PDEFullPotential3D.h"

namespace SANS
{

template<>
void
PDEFullPotential3D<Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEFullPotential3D: "
                << "a_ = " << U_[0] << " b_ = " << U_[1] << " c_ = " << U_[2]
                << "gamma_ = " << gamma_ << " gm1_ = " << gm1_ << " Minf_ = " << Minf_
                << "Minf2_ = " << Minf2_ << " rhoinf_ = " << rhoinf_ << std::endl;

  if (sln_ == NULL)
  {
    out << indent << "PDEFullPotential3D: sln_ = NULL" << std::endl;
  }
  else
  {
    out << indent << "PDEFullPotential3D: sln_ =" << std::endl;
    //sln_->dump(indentSize+2, out);
  }
}

} //namespace SANS
