// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION2D_LIP_FIXCIRC_H
#define SOLUTIONFUNCTION2D_LIP_FIXCIRC_H

// 2-D Linearized Incompressible Potential PDE: exact and MMS solutions

#include <cmath>
#include <complex>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"


namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: exact/MMS solution functions
//
// member functions:
//   .()      functor returning solution
//----------------------------------------------------------------------------//

template <int N>
class SolutionFunction2D;


template <>
class SolutionFunction2D<1>
{
public:
  typedef Real ArrayQ;

  virtual ~SolutionFunction2D() {}

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const = 0;

  virtual void operator()( const Real& x, const Real& y, const Real& time,
                           ArrayQ& q, ArrayQ& qx, ArrayQ& qy,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qyy ) const = 0;

protected:
  SolutionFunction2D() {}  // abstract base class
};


//----------------------------------------------------------------------------//
// solution: const

template <int N>
class SolutionFunction2D_Const;

template <>
class SolutionFunction2D_Const<1> : public SolutionFunction2D<1>
{
public:
  typedef Real ArrayQ;

  explicit SolutionFunction2D_Const( const Real& a0 ) : a0_(a0) {}
  virtual ~SolutionFunction2D_Const() {}

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const
    { return a0_; }

  virtual void operator()( const Real& x, const Real& y, const Real& time,
                           ArrayQ& q, ArrayQ& qx, ArrayQ& qy,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qyy ) const
    { q = a0_; qx = 0; qy = 0; qxx = 0; qxy = 0; qyy = 0; }

private:
  Real a0_;
};


//----------------------------------------------------------------------------//
// solution: linear

template <int N>
class SolutionFunction2D_Linear;

template <>
class SolutionFunction2D_Linear<1> : public SolutionFunction2D<1>
{
public:
  typedef Real ArrayQ;

  SolutionFunction2D_Linear( const Real& a0, const Real& ax, const Real& ay ) : a0_(a0), ax_(ax), ay_(ay) {}
  virtual ~SolutionFunction2D_Linear() {}

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const
    { return a0_ + ax_*x + ay_*y; }

  virtual void operator()( const Real& x, const Real& y, const Real& time,
                           ArrayQ& q, ArrayQ& qx, ArrayQ& qy,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qyy ) const
    { q = a0_ + ax_*x + ay_*y; qx = ax_; qy = ay_; qxx = 0; qxy = 0; qyy = 0; }

private:
  Real a0_, ax_, ay_;
};


//----------------------------------------------------------------------------//
// solution: sin(2*PI*x)*sin(2*PI*y)

template <int N>
class SolutionFunction2D_SineSine;

template <>
class SolutionFunction2D_SineSine<1> : public SolutionFunction2D<1>
{
public:
  typedef Real ArrayQ;

  SolutionFunction2D_SineSine() {}
  virtual ~SolutionFunction2D_SineSine() {}

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const
    { return sin(2*PI*x)*sin(2*PI*y); }

  virtual void operator()( const Real& x, const Real& y, const Real& time,
                           ArrayQ& q, ArrayQ& qx, ArrayQ& qy,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qyy ) const
  {
    Real snx = sin(2*PI*x), csx = cos(2*PI*x);
    Real sny = sin(2*PI*y), csy = cos(2*PI*y);

    q   = snx*sny;
    qx  = 2*PI*csx*sny;
    qy  = 2*PI*snx*csy;
    qxx = -4*PI*PI*snx*sny;
    qxy =  4*PI*PI*csx*csy;
    qyy = -4*PI*PI*snx*sny;
  }
};


//----------------------------------------------------------------------------//
// solution: cubic-source bump
//
// u(0,0) = 1  u(x,1) = 0  u(1,y) = 0
//
// NOTE: exact solution for 2D advection-diffusion: a ux + b uy - nu (uxx + uyy) = 0

template <int N>
class SolutionFunction2D_CubicSourceBump;

template <>
class SolutionFunction2D_CubicSourceBump<1> : public SolutionFunction2D<1>
{
public:
  typedef Real ArrayQ;

  explicit SolutionFunction2D_CubicSourceBump( const Real& tau ) : tau_(tau) {}
  virtual ~SolutionFunction2D_CubicSourceBump() {}

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const
  {
    Real phi;
    Real src;

    src = tau_*15000 / (1875*PI - 160 - 3750*atan(0.2) - 3243*atan(5.));

    if (y == 0)
    {
      if (x == 0)
        phi = -src/48.;
      else if (x == 1)
        phi =  src/48.;
      else
        phi = -(src/48.)*( 1 + 2*x*(2 - 3*x*(3 - 2*x))
                          - 6*x*x*(x-1)*(x-1)*log(x*x/((x-1)*(x-1))) );
    }
    else
    {
      phi = src*( -(1 - 2*x)*(1 - 6*x*(x-1) + 18*y*y)/48.
                  + 0.5*(1 - 2*x)*y*(x*(x-1) - y*y)*(atan((x-1)/y) - atan(x/y))
                  + 0.125*(x*x*(x-1)*(x-1) - (1 + 6*x*(x-1))*y*y + y*y*y*y)*
                    log((x*x + y*y)/((x-1)*(x-1) + y*y)) );
    }

    return phi;
  }

  virtual void operator()( const Real& x, const Real& y, const Real& time,
                           ArrayQ& q, ArrayQ& qx, ArrayQ& qy,
                           ArrayQ& qxx, ArrayQ& qxy, ArrayQ& qyy ) const {}

private:
  Real tau_;      // bump height (e.g. tau_ = 0.1)
};


//----------------------------------------------------------------------------//
// solution: perturbation potential for Joukowski airfoil at angle-of-attack
//
// chord = 1; LE at x = 0
//
// ref: Katz-Plotkin, 2nd ed (2001), p 128-139

template <int N>
class SolutionFunction2D_Joukowski;

template <>
class SolutionFunction2D_Joukowski<1> : public SolutionFunction2D<1>
{
public:
  typedef Real ArrayQ;

  SolutionFunction2D_Joukowski( const Real& e, const Real& aoa ) : e_(e), aoa_(aoa) {}
  virtual ~SolutionFunction2D_Joukowski() {}

  virtual ArrayQ operator()( const Real& x, const Real& y, const Real& time ) const
  {
   typedef std::complex<Real> Complex;

    Real U = 1;       // freestream speed
    Real c = 1;       // chord
    Real e = e_;      // thickness param (0.1 gives ~12% t/c)

    Real aoar = aoa_*(PI/180.);

    Real C = 4*c/(3 + 2*e + 1./(1 + 2*e));    // Joukowski transform constant
    Real a = 0.25*C*(1 + e);                  // circle radius in zeta-plane
    Real zeta0 = -0.25*e*C;                   // circle center in zeta-plane
    Real Gamma = 4*PI*U*a*sin(aoar);          // circulation

    Real xle = -c*(1 + 2*e*(1 + e))/(2*(1 + e)*(1 + e));    // LE
    //Real xte =  c*(1 + 2*e)/(2*(1 + e)*(1 + e));            // TE

    Real xs = x + xle;          // shifted x giving origin at roughly mid-chord

    Complex I( 0, 1 );
    Complex z  = x  + I*y;
    Complex zs = xs + I*y;      // shifted physical-plane location
    Complex zeta;               // circle-plane location
    Complex logzeta;
    Complex pot;

    if (xs < 0)
      zeta = 0.5*(zs - sqrt(zs*zs - 0.25*C*C));
    else
      zeta = 0.5*(zs + sqrt(zs*zs - 0.25*C*C));

    // imag(log) in [0, 2*PI]
    logzeta = log(zeta - zeta0);
    if (arg(zeta - zeta0) < 0)
      logzeta += 2*PI*I;

    pot  = U*exp(-I*aoar)*(zeta - zeta0) + I*Gamma*logzeta/(2*PI) + U*a*a*exp(I*aoar)/(zeta - zeta0);
    pot -= U*exp(-I*aoar)*z;

    return real(pot);
  }

  virtual void operator()( const Real& x, const Real& y, const Real& time,
                           ArrayQ& phi, ArrayQ& phix, ArrayQ& phiy,
                           ArrayQ& phixx, ArrayQ& phixy, ArrayQ& phiyy ) const
  {
    typedef std::complex<Real> Complex;

    Real U = 1;       // freestream speed
    Real c = 1;       // chord
    Real e = e_;      // thickness param (0.1 gives ~12% t/c)

    Real aoar = aoa_*(PI/180.);

    Real C = 4*c/(3 + 2*e + 1./(1 + 2*e));    // Joukowski transform constant
    Real a = 0.25*C*(1 + e);                  // circle radius in zeta-plane
    Real zeta0 = -0.25*e*C;                   // circle center in zeta-plane
    Real Gamma = 4*PI*U*a*sin(aoar);          // circulation

    Real xle = -c*(1 + 2*e*(1 + e))/(2*(1 + e)*(1 + e));    // LE
    //Real xte =  c*(1 + 2*e)/(2*(1 + e)*(1 + e));            // TE

    Real xs = x + xle;          // shifted x giving origin at roughly mid-chord

    Complex I( 0, 1 );
    Complex z  = x  + I*y;
    Complex zs = xs + I*y;      // shifted physical-plane location
    Complex zeta;               // circle-plane location
    Complex logzeta, logzeta_zeta;
    Complex pot, pot_zeta, pot_zs;
    Complex dzetadzs;

    if (xs < 0)
      zeta = 0.5*(zs - sqrt(zs*zs - 0.25*C*C));
    else
      zeta = 0.5*(zs + sqrt(zs*zs - 0.25*C*C));

    // imag(log) in [0, 2*PI]
    logzeta = log(zeta - zeta0);
    if (arg(zeta - zeta0) < 0)
      logzeta += 2*PI*I;

    logzeta_zeta = 1./(zeta - zeta0);

    pot  = U*exp(-I*aoar)*(zeta - zeta0) + I*Gamma*logzeta/(2*PI) + U*a*a*exp(I*aoar)/(zeta - zeta0);
    pot -= U*exp(-I*aoar)*z;

    dzetadzs = 16.*zeta*zeta/(16.*zeta*zeta - C*C);

    pot_zeta = U*exp(-I*aoar) + I*Gamma*logzeta_zeta/(2*PI) - U*a*a*exp(I*aoar)/((zeta - zeta0)*(zeta - zeta0));
    pot_zs   = pot_zeta*dzetadzs - U*exp(-I*aoar);    // NOTE: d(zs)/d(z) = 1

    phi   =  real(pot);
    phix  =  real(pot_zs);
    phiy  = -imag(pot_zs);
    phixx = 0;
    phixy = 0;
    phiyy = 0;
  }


private:
  Real e_;      // thickness parameter (0.1 gives ~12% t/c)
  Real aoa_;    // angle-of-attack (deg)
};

} //namespace SANS

#endif  // SOLUTIONFUNCTION2D_LIP_FIXCIRC_H
