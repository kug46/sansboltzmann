// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCIncompressiblePotentialTwoField2D_sansLG.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void BCIncompressiblePotentialTwoField2DParams<BCTypeDirichlet>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.q));
  d.checkUnknownInputs(allParams);
}
BCIncompressiblePotentialTwoField2DParams<BCTypeDirichlet> BCIncompressiblePotentialTwoField2DParams<BCTypeDirichlet>::params;

template<>
void
BCIncompressiblePotentialTwoField2D<BCTypeDirichlet, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCIncompressiblePotentialTwoField2D<BCTypeDirichlet>:" << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}


// cppcheck-suppress passedByValue
void BCIncompressiblePotentialTwoField2DParams<BCTypeNeumann>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gradqn));
  d.checkUnknownInputs(allParams);
}
BCIncompressiblePotentialTwoField2DParams<BCTypeNeumann> BCIncompressiblePotentialTwoField2DParams<BCTypeNeumann>::params;

template<>
void
BCIncompressiblePotentialTwoField2D<BCTypeNeumann, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCIncompressiblePotentialTwoField2D<BCTypeNeumann>:" << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}


// cppcheck-suppress passedByValue
void BCIncompressiblePotentialTwoField2DParams<BCTypeWall>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCIncompressiblePotentialTwoField2DParams<BCTypeWall> BCIncompressiblePotentialTwoField2DParams<BCTypeWall>::params;

template<>
void
BCIncompressiblePotentialTwoField2D<BCTypeWall, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCIncompressiblePotentialTwoField2D<BCTypeWall>:" << std::endl;
  out << indent << "  (u_, v_) = (" << u_ << ", " << v_ << ")" << std::endl;
}


// cppcheck-suppress passedByValue
void BCIncompressiblePotentialTwoField2DParams<BCTypeFarfieldVortex>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.x0));
  allParams.push_back(d.checkInputs(params.y0));
  allParams.push_back(d.checkInputs(params.circ));
  d.checkUnknownInputs(allParams);
}
BCIncompressiblePotentialTwoField2DParams<BCTypeFarfieldVortex> BCIncompressiblePotentialTwoField2DParams<BCTypeFarfieldVortex>::params;

#if 0   // moved back to *.h
template<>
void
BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex>:" << std::endl;
  out << indent << "  (x0_, y0_) = (" << x0_ << ", " << y0_ << ")  circ = " << circ_ << std::endl;
}
#endif

// cppcheck-suppress passedByValue
void BCIncompressiblePotentialTwoField2DParams<BCTypeFunction>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCIncompressiblePotentialTwoField2DParams<BCTypeFunction> BCIncompressiblePotentialTwoField2DParams<BCTypeFunction>::params;

template<>
void
BCIncompressiblePotentialTwoField2D<BCTypeFunction, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCIncompressiblePotentialTwoField2D<BCTypeFunction>:" << std::endl;
}


//===========================================================================//
// Instantiate the BC parameters
BCPARAMETER_INSTANTIATE( BCIncompressiblePotentialTwoField2DVector<Real> )
BCPARAMETER_INSTANTIATE( BCIncompressiblePotentialTwoField2DVector<SurrealS<1>> )

} //namespace SANS
