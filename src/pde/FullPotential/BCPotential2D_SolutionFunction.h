// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCPOTENTIAL2D_SOLUTIONFUNCTION_H
#define BCPOTENTIAL2D_SOLUTIONFUNCTION_H

#include <memory> // shared_ptr
#include <vector>

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "Topology/Dimension.h"
#include "SolutionFunction_Potential2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Struct for creating a BC solution function

struct BCPotential2D_SolutionFunction_OptionsList : noncopyable
{
  struct SolutionOptions
  {
    typedef DictKeyPair ExtractType;
    const ParameterString SolutionFunctionName{"SolutionFunctionName", NO_DEFAULT, "Solution function used for the BC" };
    const ParameterString& key = SolutionFunctionName;

    const DictOption Const{"Const", SolutionFunction_Potential2D_Const::ParamsType::checkInputs};
    const DictOption Linear{"Linear", SolutionFunction_Potential2D_Linear::ParamsType::checkInputs};
    const DictOption SineSine{"SineSine", SolutionFunction_Potential2D_SineSine::ParamsType::checkInputs};
    const DictOption CubicSourceBump{"CubicSourceBump", SolutionFunction_Potential2D_CubicSourceBump::ParamsType::checkInputs};
    const DictOption Joukowski{"Joukowski", SolutionFunction_Potential2D_Joukowski::ParamsType::checkInputs};

    const std::vector<DictOption> options{ Const, Linear, SineSine, CubicSourceBump, Joukowski };
  };
  const ParameterOption<SolutionOptions> Function{ "Function", NO_DEFAULT, "The BC data is a solution function" };
};


struct BCPotential2D_SolutionFunction_Params : public BCPotential2D_SolutionFunction_OptionsList
{
  //static void checkInputs(PyDict d);
};


// Struct for creating the solution pointer
struct BCPotential2D_SolutionFunction
{
  typedef PhysD2 PhysDim;

  template <class T>
  using ArrayQ = PotentialFunctionTraits::ArrayQ<T>;    // solution arrays

  typedef Function2DBase<ArrayQ<Real>> Function2DBaseType;
  typedef std::shared_ptr<Function2DBaseType> Function_ptr;

  static Function_ptr getSolution(const PyDict& d);
};

} // namespace SANS

#endif // BCPOTENTIAL2D_SOLUTIONFUNCTION_H
