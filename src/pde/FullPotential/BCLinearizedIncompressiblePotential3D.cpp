// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCLinearizedIncompressiblePotential3D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//---------------------------------------------------------------------------//
// cppcheck-suppress passedByValue
void BCLinearizedIncompressiblePotential3DParams<BCTypeDirichlet>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.q));
  d.checkUnknownInputs(allParams);
}
BCLinearizedIncompressiblePotential3DParams<BCTypeDirichlet> BCLinearizedIncompressiblePotential3DParams<BCTypeDirichlet>::params;

template <class Tg>
void
BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Tg>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Real>:" << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}
template class BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Real>;


//---------------------------------------------------------------------------//
// cppcheck-suppress passedByValue
void BCLinearizedIncompressiblePotential3DParams<BCTypeNeumann>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gradqn));
  d.checkUnknownInputs(allParams);
}
BCLinearizedIncompressiblePotential3DParams<BCTypeNeumann> BCLinearizedIncompressiblePotential3DParams<BCTypeNeumann>::params;

template <class Tg>
void
BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Tg>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Real>:" << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}
template class BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Real>;


//---------------------------------------------------------------------------//
// cppcheck-suppress passedByValue
void BCLinearizedIncompressiblePotential3DParams<BCTypeWall>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCLinearizedIncompressiblePotential3DParams<BCTypeWall> BCLinearizedIncompressiblePotential3DParams<BCTypeWall>::params;

template<class Tg>
void
BCLinearizedIncompressiblePotential3D<BCTypeWall, Tg>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential3D<BCTypeWall, Real>:" << std::endl;
}
template class BCLinearizedIncompressiblePotential3D<BCTypeWall, Real>;
template class BCLinearizedIncompressiblePotential3D<BCTypeWall, SurrealS<1>>;


//---------------------------------------------------------------------------//
// cppcheck-suppress passedByValue
void BCLinearizedIncompressiblePotential3DParams<BCTypeControl>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.hinge));
  allParams.push_back(d.checkInputs(params.deflect));
  d.checkUnknownInputs(allParams);
}
BCLinearizedIncompressiblePotential3DParams<BCTypeControl> BCLinearizedIncompressiblePotential3DParams<BCTypeControl>::params;

template<class Tg>
void
BCLinearizedIncompressiblePotential3D<BCTypeControl, Tg>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential3D<BCTypeControl, Real>:" << std::endl;
  out << indent << "  (hx_, hy_, hz_) = (" << hinge_[0] << ", " << hinge_[1] << ", " << hinge_[2] << ")" << std::endl;
  out << indent << "  theta_ = (" << theta_ << ")" << std::endl;
}
template class BCLinearizedIncompressiblePotential3D<BCTypeControl, Real>;
template class BCLinearizedIncompressiblePotential3D<BCTypeControl, SurrealS<1>>;

#if 0
void
BCLinearizedIncompressiblePotential3D<BCTypeFarfieldVortex>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential3D<BCTypeFarfieldVortex>:" << std::endl;
  out << indent << "  (x0_, y0_) = (" << x0_ << ", " << y0_ << ")" << std::endl;
}
#endif

//===========================================================================//
// Instantiate the BC parameters
BCPARAMETER_INSTANTIATE( BCLinearizedIncompressiblePotential3DVector<Real> )
BCPARAMETER_INSTANTIATE( BCLinearizedIncompressiblePotential3DVector<SurrealS<1>> )
BCPARAMETER_INSTANTIATE( BCLinearizedIncompressiblePotential3DVector<SurrealS<6>> )


} //namespace SANS
