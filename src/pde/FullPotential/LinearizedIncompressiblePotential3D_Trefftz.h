// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef LINEARIZEDINCOMPRESSIBLEPOTENTIAL3D_TREFFTZ_H
#define LINEARIZEDINCOMPRESSIBLEPOTENTIAL3D_TREFFTZ_H

// boundary-trace integral functional

#include <memory>     // std::unique_ptr

#include "Topology/ElementTopology.h"
#include "Field/Field.h"
#include "Field/XFieldVolume.h"
#include "Field/Element/ElementIntegral.h"
#include "Field/Element/ElementXFieldLine.h"
#include "Field/Element/ElementXFieldArea.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "pde/FullPotential/PDELinearizedIncompressiblePotential3D.h"

#include "BasisFunction/Quadrature_Cache.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "LinearAlgebra/DenseLinAlg/tools/cross.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "LinearAlgebra/DenseLinAlg/tools/index.h"

namespace SANS
{

class TrefftzPlaneForce;
class TrefftzInducedDrag;

template<class Force, class Tg>
class IntegrandBoundaryFrame3D_LIP_Trefftz;


//----------------------------------------------------------------------------//
// Trefftz Induced Drag
template<class Tg>
class IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzInducedDrag, Tg>
{
public:
  typedef PDELinearizedIncompressiblePotential3D<Tg> PDE;

  IntegrandBoundaryFrame3D_LIP_Trefftz( const PDE& pde, const std::vector<int>& BoundaryFrameGroups )
    : BoundaryFrameGroups_(BoundaryFrameGroups), pde_(pde)
  {
  }

  std::size_t nBoundaryFrameGroups() const { return BoundaryFrameGroups_.size(); }
  std::size_t boundaryFrameGroup(const int n) const { return BoundaryFrameGroups_[n]; }

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution arrays

  template <class Z>
  using ArrayJ = Z;      // output arrays

  template<class TopologyFrame, class TopologyTrace, class TopologyCellL, class TopologyCellR, class T>
  class Functor
  {
  public:
    typedef ElementXField<PhysD3, TopoD1, TopologyFrame> ElementXFieldFrame;
    typedef ElementXField<PhysD3, TopoD2, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysD3, TopoD3, TopologyCellL> ElementXFieldCellL;
    typedef ElementXField<PhysD3, TopoD3, TopologyCellR> ElementXFieldCellR;

    typedef Element<ArrayQ<T>, TopoD2, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoD3, TopologyCellL> ElementQFieldCellL;
    typedef Element<ArrayQ<T>, TopoD3, TopologyCellR> ElementQFieldCellR;

    typedef typename ElementXFieldFrame::RefCoordType RefCoordFrameType;
    typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
    typedef typename ElementXFieldCellL::RefCoordType RefCoordCellType;

    typedef QuadraturePoint<typename TopologyFrame::TopoDim> QuadPointFrameType;
    //typedef QuadraturePoint<typename TopologyTrace::TopoDim> QuadPointTraceType;
    //typedef QuadratureCellTracePoint<typename TopologyCellL::TopoDim> QuadPointCellLType;

    typedef DLA::VectorS<PhysD3::D,Real> VectorX;

    Functor( const PDE& pde,
             const ElementXFieldTrace& xfldElemTrace,
             const CanonicalTraceToCell& canonicalFrame,
             const CanonicalTraceToCell& canonicalTraceL,
             const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldCellL& xfldElemL,
             const ElementQFieldCellL& qfldElemL,
             const ElementXFieldCellR& xfldElemR,
             const ElementQFieldCellR& qfldElemR
             //const ElementQFieldTrace& lgfldTrace
             ) :
             pde_(pde), xfldElemTrace_(xfldElemTrace),
             canonicalFrame_(canonicalFrame),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR)
             //lgfldTrace_(lgfldTrace)
    {
    }

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    typedef typename promote_Surreal<T,Tg>::type Ts;

    // trace integrand
    void operator()( const QuadPointFrameType& sRefFrame, Ts& integrand ) const
    {
      VectorX X;
      VectorX nL, nR;
      DLA::VectorS<3, ArrayQ<T> > gradPhiL, gradPhiR;
      ArrayQ<T> PhiL, PhiR;
      RefCoordTraceType sRefTrace;
      RefCoordCellType sRefCellL, sRefCellR;

      TraceToCellRefCoord<TopologyFrame, TopoD2, TopologyTrace>::eval( canonicalFrame_, sRefFrame.ref, sRefTrace );
      TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCellL>::eval( canonicalTraceL_, sRefTrace, sRefCellL );
      TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCellR>::eval( canonicalTraceR_, sRefTrace, sRefCellR );

      xfldElemL_.eval( sRefCellL, X );

      xfldElemL_.evalGradient( sRefCellL, qfldElemL_, gradPhiL );
      xfldElemR_.evalGradient( sRefCellR, qfldElemR_, gradPhiR );

      qfldElemL_.eval( sRefCellL, PhiL );
      qfldElemR_.eval( sRefCellR, PhiR );

      // unit normal on the wake
      xfldElemTrace_.unitNormal( sRefTrace, nL );

      DLA::VectorS<3,Tg> U = pde_.Up(X);

      DLA::VectorS<3,Tg> V = U/Tg(sqrt(dot(U,U)));


      // Project the vector into the Trefftz plane which is perpendicular to V
      DLA::VectorS<3,Tg> nL2 = nL - dot(nL, V)*V;
                         nL2 = nL2/Tg(sqrt(dot(nL2,nL2))); // Re-normalize
      DLA::VectorS<3,Tg> nR2 = -nL2;

      // Compute the normal gradient
      ArrayQ<Ts> PhiNL = dot(nL2, gradPhiL);
      ArrayQ<Ts> PhiNR = dot(nR2, gradPhiR);

      // Induced drag integrand
      Ts InducedDrag = 0.5*(PhiL*PhiNL + PhiR*PhiNR);

      //VectorX X;
      //xfldElemTrace_.eval(sRefTrace, X);
      //std::cout << X << ", " << InducedDrag << std::endl;
/*
      DLA::VectorS<3, ArrayQ<T> > gradPhiAvg = 0.5*(gradPhiL + gradPhiR);

      DLA::VectorS<3,T> V = {U_[0] + gradPhiAvg[0][0], U_[1] + gradPhiAvg[1][0], U_[2] + gradPhiAvg[2][0]};

      T u = dot(V_, V);
      T U = dot(U_, V_);

      T ProfileDrag = u*(U - u);

      integrand = ProfileDrag + InducedDrag;
*/
      integrand = InducedDrag;
    }

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalFrame_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldCellL& xfldElemL_;
    const ElementQFieldCellL& qfldElemL_;
    const ElementXFieldCellR& xfldElemR_;
    const ElementQFieldCellR& qfldElemR_;
    //const ElementQFieldTrace& lgfldTrace_;
  };

  template<class TopologyFrame, class TopologyTrace, class TopologyCellL, class TopologyCellR, class T>
  Functor<TopologyFrame, TopologyTrace, TopologyCellL, TopologyCellR, T>
  integrand(const ElementXField<PhysD3, TopoD1, TopologyFrame>& xfldElemFrame,
            const ElementXField<PhysD3, TopoD2, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalFrame,
            const CanonicalTraceToCell& canonicalTraceL,
            const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysD3, TopoD3 , TopologyCellL >& xfldElemL,
            const Element<ArrayQ<T>   , TopoD3 , TopologyCellL >& qfldElemL,
            const ElementXField<PhysD3, TopoD3 , TopologyCellR >& xfldElemR,
            const Element<ArrayQ<T>   , TopoD3 , TopologyCellR >& qfldElemR
            //const Element<ArrayQ<T>   , TopoD2, Triangle>& lgfldTrace
            ) const
  {
    return {pde_,
            xfldElemTrace,
            canonicalFrame, canonicalTraceL, canonicalTraceR,
            xfldElemL, qfldElemL,
            xfldElemR, qfldElemR
            //lgfldTrace
           };
  }

protected:
  const std::vector<int> BoundaryFrameGroups_;
  const PDE& pde_;
};


//----------------------------------------------------------------------------//
// Trefftz in-plane force calculation
template<class Tg>
class IntegrandBoundaryFrame3D_LIP_Trefftz<TrefftzPlaneForce, Tg>
{
public:
  typedef PDELinearizedIncompressiblePotential3D<Tg> PDE;

  IntegrandBoundaryFrame3D_LIP_Trefftz( const PDE& pde, const DLA::VectorS<3,Tg>& axis, const std::vector<int>& BoundaryFrameGroups )
    : pde_(pde), axis_(axis), BoundaryFrameGroups_(BoundaryFrameGroups)
  {
  }

  std::size_t nBoundaryFrameGroups() const { return BoundaryFrameGroups_.size(); }
  std::size_t boundaryFrameGroup(const int n) const { return BoundaryFrameGroups_[n]; }

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;      // solution arrays

  template <class Z>
  using ArrayJ = Z;      // output arrays

  template<class TopologyFrame, class TopologyTrace, class TopologyCellL, class TopologyCellR, class T>
  class Functor
  {
  public:
    typedef ElementXField<PhysD3, TopoD1, TopologyFrame> ElementXFieldFrame;
    typedef ElementXField<PhysD3, TopoD2, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysD3, TopoD3, TopologyCellL> ElementXFieldCellL;
    typedef ElementXField<PhysD3, TopoD3, TopologyCellR> ElementXFieldCellR;

    typedef Element<ArrayQ<T>, TopoD2, TopologyTrace> ElementQFieldTrace;
    typedef Element<ArrayQ<T>, TopoD3, TopologyCellL> ElementQFieldCellL;
    typedef Element<ArrayQ<T>, TopoD3, TopologyCellR> ElementQFieldCellR;

    typedef typename ElementXFieldFrame::RefCoordType RefCoordFrameType;
    typedef typename ElementXFieldTrace::RefCoordType RefCoordTraceType;
    typedef typename ElementXFieldCellL::RefCoordType RefCoordCellType;
    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<typename TopologyFrame::TopoDim> QuadPointFrameType;
    //typedef QuadraturePoint<typename TopologyTrace::TopoDim> QuadPointTraceType;
    //typedef QuadratureCellTracePoint<typename TopologyCellL::TopoDim> QuadPointCellLType;

    Functor( const PDE& pde,
             const DLA::VectorS<3,Tg>& axis,
             const ElementXFieldFrame& xfldElemFrame,
             const CanonicalTraceToCell& canonicalFrame,
             const CanonicalTraceToCell& canonicalTraceL,
             const CanonicalTraceToCell& canonicalTraceR,
             const ElementXFieldCellL& xfldElemL,
             const ElementQFieldCellL& qfldElemL,
             const ElementXFieldCellR& xfldElemR,
             const ElementQFieldCellR& qfldElemR
             //const ElementQFieldTrace& lgfldTrace
             ) :
             pde_(pde), axis_(axis),
             xfldElemFrame_(xfldElemFrame),
             canonicalFrame_(canonicalFrame),
             canonicalTraceL_(canonicalTraceL), canonicalTraceR_(canonicalTraceR),
             xfldElemL_(xfldElemL), qfldElemL_(qfldElemL),
             xfldElemR_(xfldElemR), qfldElemR_(qfldElemR)
             //lgfldTrace_(lgfldTrace)
             {}

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    typedef typename promote_Surreal<T,Tg>::type Ts;

    // trace integrand
    template<class Ti>
    void operator()( const QuadPointFrameType& sRefFrame, Ti& integrand ) const
    {
      VectorX nL, nR, Xs, X;
      DLA::VectorS<3,Tg> Tr;
      ArrayQ<T> PhiL, PhiR;
      RefCoordTraceType sRefTrace;
      RefCoordCellType sRefCellL, sRefCellR;

      TraceToCellRefCoord<TopologyFrame, TopoD2, TopologyTrace>::eval( canonicalFrame_, sRefFrame.ref, sRefTrace );
      TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCellL>::eval( canonicalTraceL_, sRefTrace, sRefCellL );
      TraceToCellRefCoord<TopologyTrace, TopoD3, TopologyCellR>::eval( canonicalTraceR_, sRefTrace, sRefCellR );

      qfldElemL_.eval( sRefCellL, PhiL );
      qfldElemR_.eval( sRefCellR, PhiR );

      xfldElemFrame_.eval( sRefFrame, X );

      // Tangent vector on the wake
      xfldElemFrame_.unitTangent( sRefFrame, Xs );

      DLA::VectorS<3,Tg> U = pde_.Up(X);
      //pde.freestream(U);

      Tg Vinf = sqrt(dot(U,U));
      U /= Vinf;

      // Project the vector into the Trefftz plane which is perpendicular to V
      Tr = axis_ - dot(axis_, U)*U;
      Tg Trmag = sqrt(dot(Tr,Tr));
      if (Trmag == 0.0)
      {
        // axis and U are parallel. Use axis directly
        Tg axmag = sqrt(dot(axis_,axis_));
        Tr = axis_/axmag;
      }
      else
        Tr /= Trmag; // Re-normalize

      // Crossing the force direction with the freestream gives the integration direction
      Tr = cross(Tr,U);

      // In-plane integrand
      Ti InPlaneForce = Vinf*(PhiR - PhiL)*dot(Xs,Tr);

      integrand = InPlaneForce;
    }

  protected:
    const PDE& pde_;
    const DLA::VectorS<3,Tg>& axis_;
    const ElementXFieldFrame& xfldElemFrame_;
    const CanonicalTraceToCell canonicalFrame_;
    const CanonicalTraceToCell canonicalTraceL_;
    const CanonicalTraceToCell canonicalTraceR_;
    const ElementXFieldCellL& xfldElemL_;
    const ElementQFieldCellL& qfldElemL_;
    const ElementXFieldCellR& xfldElemR_;
    const ElementQFieldCellR& qfldElemR_;
    //const ElementQFieldTrace& lgfldTrace_;
  };

  template<class TopologyFrame, class TopologyTrace, class TopologyCellL, class TopologyCellR, class T>
  Functor<TopologyFrame, TopologyTrace, TopologyCellL, TopologyCellR, T>
  integrand(const ElementXField<PhysD3, TopoD1, TopologyFrame>& xfldElemFrame,
            const ElementXField<PhysD3, TopoD2, TopologyTrace>& xfldElemTrace,
            const CanonicalTraceToCell& canonicalFrame,
            const CanonicalTraceToCell& canonicalTraceL,
            const CanonicalTraceToCell& canonicalTraceR,
            const ElementXField<PhysD3, TopoD3 , TopologyCellL>& xfldElemL,
            const Element<ArrayQ<T>   , TopoD3 , TopologyCellL>& qfldElemL,
            const ElementXField<PhysD3, TopoD3 , TopologyCellR>& xfldElemR,
            const Element<ArrayQ<T>   , TopoD3 , TopologyCellR>& qfldElemR
            //const Element<ArrayQ<T>   , TopoD2, Triangle>& lgfldTrace
            ) const
  {
    return Functor<TopologyFrame, TopologyTrace, TopologyCellL, TopologyCellR, T>(pde_, axis_,
                      xfldElemFrame,
                      canonicalFrame, canonicalTraceL, canonicalTraceR,
                      xfldElemL, qfldElemL,
                      xfldElemR, qfldElemR
                      //lgfldTrace
                      );
  }

protected:
  const PDE& pde_;
  const DLA::VectorS<3,Tg> axis_; //Direction of the in-plane force
  const std::vector<int> BoundaryFrameGroups_;
};



//----------------------------------------------------------------------------//
//  Functional Trefftz integral
//

class FunctionalTrefftz
{
protected:
  typedef TopoD3 TopoDim;


  template <class TopologyTrace, class TopologyCellL, class TopologyCellR,
            class XFieldType, class ArrayQ, class Force, class Tg,
            class FunctionalType>
  static void
  Integral(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL>& xfldCellL,
      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL>& qfldCellL,
      const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR>& xfldCellR,
      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR>& qfldCellR,
      //const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<Triangle>& lgfldTrace,
      int quadratureorder,
      FunctionalType& functional )
  {
    typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL> XFieldCellGroupTypeL;
    typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<> ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<> ElementQFieldClassL;

    typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR> XFieldCellGroupTypeR;
    typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<> ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<> ElementQFieldClassR;

    typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename XFieldType::FieldFrameGroupType XFieldFrameGroupType;
    typedef typename XFieldFrameGroupType::template ElementType<> ElementXFieldFrameClass;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

//    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    ElementXFieldFrameClass xfldElemFrame( xfldFrame.basis() );

    // trace element integral
    ElementIntegral<TopoD1, Line, FunctionalType> integral(quadratureorder);

    // loop over elements within group
    int nelem = xfldFrame.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      xfldFrame.getElement(xfldElemFrame, elem);

      int elemTrace = xfldFrame.getElementLeft( elem );
      CanonicalTraceToCell& canonicalFrame  = xfldFrame.getCanonicalTraceLeft( elem );

      xfldTrace.getElement( xfldElemTrace, elemTrace );
//      lgfldTrace.getElement( lgfldElemTrace, elemTrace );

      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elemTrace );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elemTrace );

      int elemL = xfldTrace.getElementLeft( elemTrace );
      int elemR = xfldTrace.getElementRight( elemTrace );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      FunctionalType result = 0;

      integral(
          fcn.integrand(xfldElemFrame, xfldElemTrace, canonicalFrame,
                        canonicalTraceL, canonicalTraceR,
                        xfldElemL, qfldElemL,
                        xfldElemR, qfldElemR
                        //lgfldElemTrace
                        ),
          xfldElemFrame, result );

      functional += result;
    }
  }


  template <class TopologyTrace, class TopologyCellL, class TopologyCellR,
            class XFieldType, class Force, class Tg, class FunctionalType,
            class ArrayQ>
  static void
  CellGroups(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysD3, TopoD3, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      FunctionalType& functional )
  {
    const XField<PhysD3, TopoDim>& xfld = qfld.getXField();

    int groupL = xfldTrace.getGroupLeft();
    int groupR = xfldTrace.getGroupRight();

    // Integrate over the trace group
    Integral<TopologyTrace, TopologyCellL, TopologyCellR, XFieldType, ArrayQ>( fcn, xfldFrame, xfldTrace,
                                  xfld.template getCellGroup<TopologyCellL>(groupL), qfld.template getCellGroup<TopologyCellL>(groupL),
                                  xfld.template getCellGroup<TopologyCellR>(groupR), qfld.template getCellGroup<TopologyCellR>(groupR),
//                      lgfldTrace,
                                  quadratureorder,
                                  functional );
  }

  template <class TopologyCellL,
            class XFieldType, class Force, class Tg, class FunctionalType,
            class ArrayQ>
  static void
  RightTopology_Triangle(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysD3, TopoD3, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      FunctionalType& functional )
  {
    // determine topology for Left cell group
    int groupR = xfldTrace.getGroupRight();

    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      CellGroups<Triangle, TopologyCellL, Tet, XFieldType>( fcn, xfldFrame, xfldTrace, qfld, //lgfldTrace,
                                                            quadratureorder, functional );
    }
    else
    {
      char msg[] = "Error in FunctionalBoundaryTrace_mitLG<TopoD3>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  template <class TopologyCellL,
            class XFieldType, class Force, class Tg, class FunctionalType,
            class ArrayQ>
  static void
  RightTopology_Quad(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysD3, TopoD3, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      FunctionalType& functional )
  {
    // determine topology for Left cell group
    int groupR = xfldTrace.getGroupRight();

    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      CellGroups<Quad, TopologyCellL, Hex, XFieldType>( fcn, xfldFrame, xfldTrace, qfld, //lgfldTrace,
                                                        quadratureorder, functional );
    }
    else
    {
      char msg[] = "Error in FunctionalBoundaryTrace_mitLG<TopoD3>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  template <class XFieldType, class Force, class Tg, class FunctionalType,
            class ArrayQ>
  static void
  LeftTopology_Triangle(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysD3, TopoDim, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      FunctionalType& functional )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      RightTopology_Triangle<Tet, XFieldType>( fcn,
          xfldFrame, xfldTrace, qfld,
//          lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          quadratureorder, functional );
    }
    else
    {
      char msg[] = "Error in FunctionalBoundaryTrace_mitLG<TopoD3>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class XFieldType, class Force, class Tg, class FunctionalType,
            class ArrayQ>
  static void
  LeftTopology_Quad(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysD3, TopoDim, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      FunctionalType& functional )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      RightTopology_Quad<Hex, XFieldType>( fcn,
          xfldFrame, xfldTrace, qfld,
//          lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          quadratureorder, functional );
    }
    else
    {
      char msg[] = "Error in FunctionalBoundaryTrace_mitLG<TopoD3>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class Force, class Tg, class XFieldType, class FunctionalType, class ArrayQ>
  static void
  TraceTopology(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const XFieldType& xfld,
      const Field<PhysD3, TopoDim, ArrayQ>& qfld,
//      const Field<PhysD3, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder,
      FunctionalType& functional )
  {

    // loop over trace element groups

    const int boundaryGroupL = xfldFrame.getGroupLeft();

    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Triangle) )
    {
      LeftTopology_Triangle<XFieldType>( fcn,
          xfldFrame,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupL),
          qfld,
//          lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          quadratureorder, functional );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Quad) )
    {
      LeftTopology_Quad<XFieldType>( fcn,
          xfldFrame,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupL),
          qfld,
//          lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          quadratureorder, functional );
    }
    else
    {
      char msg[] = "Error in FunctionalTrefftz::TraceTopology: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class Force, class Tg, class XFieldType, class FunctionalType,
            class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
//      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder[], int ngroup,
      FunctionalType& functional )
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );
//    SANS_ASSERT( &lgfld.getXField() == &xfld );

    SANS_ASSERT( ngroup == xfld.nBoundaryFrameGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryFrameGroups(); group++)
    {
      const int boundaryFrameGroup = fcn.boundaryFrameGroup(group);

      TraceTopology( fcn,
          xfld.getBoundaryFrameGroup(boundaryFrameGroup),
          xfld,
          qfld,
//          lgfld,
          quadratureorder[boundaryFrameGroup], functional );

    }
  }
};


template<class Surreal>
class JacobianFunctionalTrefftz
{
protected:
  typedef TopoD3 TopoDim;


  template <class TopologyTrace, class TopologyCellL, class TopologyCellR,
            class XFieldType, class ArrayQ, class Force, class Tg,
            template<class > class Vector, class MatrixJ>
  static void
  Integral(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL>& xfldCellL,
      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL>& qfldCellL,
      const typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR>& xfldCellR,
      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR>& qfldCellR,
      //const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<Triangle>& lgfldTrace,
      int quadratureorder,
      Vector<MatrixJ>& jacFunctional )
  {
    typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellL> XFieldCellGroupTypeL;
    typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellL> QFieldCellGroupTypeL;

    typedef typename XFieldCellGroupTypeL::template ElementType<>        ElementXFieldClassL;
    typedef typename QFieldCellGroupTypeL::template ElementType<Surreal> ElementQFieldClassL;

    typedef typename XField<PhysD3, TopoD3>::template FieldCellGroupType<TopologyCellR> XFieldCellGroupTypeR;
    typedef typename Field<PhysD3, TopoD3, ArrayQ>::template FieldCellGroupType<TopologyCellR> QFieldCellGroupTypeR;

    typedef typename XFieldCellGroupTypeR::template ElementType<>        ElementXFieldClassR;
    typedef typename QFieldCellGroupTypeR::template ElementType<Surreal> ElementQFieldClassR;

    typedef typename XField<PhysD3, TopoD3>::template FieldTraceGroupType<TopologyTrace> XFieldTraceGroupType;
    typedef typename XFieldTraceGroupType::template ElementType<> ElementXFieldTraceClass;

    typedef typename XFieldType::FieldFrameGroupType XFieldFrameGroupType;
    typedef typename XFieldFrameGroupType::template ElementType<> ElementXFieldFrameClass;

    typedef typename IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>::template ArrayJ<Surreal> ArrayJSurreal;

    // element field variables
    ElementXFieldClassL xfldElemL( xfldCellL.basis() );
    ElementQFieldClassL qfldElemL( qfldCellL.basis() );

    ElementXFieldClassR xfldElemR( xfldCellR.basis() );
    ElementQFieldClassR qfldElemR( qfldCellR.basis() );

//    ElementQFieldTraceClass lgfldElemTrace( lgfldTrace.basis() );

    ElementXFieldTraceClass xfldElemTrace( xfldTrace.basis() );

    ElementXFieldFrameClass xfldElemFrame( xfldFrame.basis() );

    // trace element integral
    ElementIntegral<TopoD1, Line, ArrayJSurreal> integral(quadratureorder);

    // DOFs per element
    int nDOFL = qfldElemL.nDOF();
    int nDOFR = qfldElemR.nDOF();

    // element functional jacobian vector
    DLA::VectorD<MatrixJ> jacFunctionalElemL(nDOFL);
    DLA::VectorD<MatrixJ> jacFunctionalElemR(nDOFR);

    // total number of entries in ArrayQ
    const int nVar = DLA::VectorSize<ArrayQ>::M;

    // total number of outputs in the functional
    const int nEqn = DLA::VectorSize<ArrayJSurreal>::M;

    // element-to-global DOF mapping
    std::vector<int> mapDOFGlobalL(nDOFL);
    std::vector<int> mapDOFGlobalR(nDOFR);

    // number of simultaneous derivatives per functor call
    const int nDeriv = DLA::index(qfldElemL.DOF(0), 0).size();

    // loop over elements within group
    int nelem = xfldFrame.nElem();
    for (int elem = 0; elem < nelem; elem++)
    {
      xfldFrame.getElement(xfldElemFrame, elem);

      int elemTrace = xfldFrame.getElementLeft( elem );
      CanonicalTraceToCell& canonicalFrame  = xfldFrame.getCanonicalTraceLeft( elem );

      xfldTrace.getElement( xfldElemTrace, elemTrace );
//      lgfldTrace.getElement( lgfldElemTrace, elemTrace );

      CanonicalTraceToCell& canonicalTraceL = xfldTrace.getCanonicalTraceLeft( elemTrace );
      CanonicalTraceToCell& canonicalTraceR = xfldTrace.getCanonicalTraceRight( elemTrace );

      int elemL = xfldTrace.getElementLeft( elemTrace );
      int elemR = xfldTrace.getElementRight( elemTrace );

      // copy global grid/solution DOFs to element
      xfldCellL.getElement( xfldElemL, elemL );
      qfldCellL.getElement( qfldElemL, elemL );

      xfldCellR.getElement( xfldElemR, elemR );
      qfldCellR.getElement( qfldElemR, elemR );

      jacFunctionalElemL = 0;
      jacFunctionalElemR = 0;

      // loop over derivative chunks
      for (int nchunk = 0; nchunk < nVar*(nDOFL + nDOFR); nchunk += nDeriv)
      {

        // associate derivative slots with solution variables

        int slot, slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }
        slotOffset += nDOFL*nVar;

        for (int j = 0; j < nDOFR; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
              DLA::index(qfldElemR.DOF(j), n).deriv(slot - nchunk) = 1;
          }
        }

        // trace integration for canonical element
        ArrayJSurreal functional = 0;
        integral(
            fcn.integrand(xfldElemFrame, xfldElemTrace, canonicalFrame,
                          canonicalTraceL, canonicalTraceR,
                          xfldElemL, qfldElemL,
                          xfldElemR, qfldElemR
                          //lgfldElemTrace
                          ),
            xfldElemFrame, functional );

        // accumulate derivatives into element jacobian
        slotOffset = 0;
        for (int j = 0; j < nDOFL; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemL.DOF(j), n).deriv(slot - nchunk) = 0; // Reset derivative

              // Note that n,m are transposed intentionally
              for (int m = 0; m < nEqn; m++)
                DLA::index(jacFunctionalElemL[j],n,m) += DLA::index(functional, m).deriv(slot - nchunk);
            }
          }
        }
        slotOffset += nDOFL*nVar;

        for (int j = 0; j < nDOFR; j++)
        {
          for (int n = 0; n < nVar; n++)
          {
            slot = slotOffset + nVar*j + n;
            if ((slot >= nchunk) && (slot < nchunk + nDeriv))
            {
              DLA::index(qfldElemR.DOF(j), n).deriv(slot - nchunk) = 0; // Reset derivative

              // Note that n,m are transposed intentionally
              for (int m = 0; m < nEqn; m++)
                DLA::index(jacFunctionalElemR[j],n,m) += DLA::index(functional, m).deriv(slot - nchunk);
            }
          }
        }
      }   // nchunk

      // Get the local-to-global mapping to scatter the jacobian
      qfldCellL.associativity( elemL ).getGlobalMapping( mapDOFGlobalL.data(), nDOFL );
      qfldCellR.associativity( elemR ).getGlobalMapping( mapDOFGlobalR.data(), nDOFR );

      // scatter-add element jacobian to global
      for (int i = 0; i < nDOFL; i++)
        jacFunctional[mapDOFGlobalL[i]] += jacFunctionalElemL[i];

      for (int i = 0; i < nDOFR; i++)
        jacFunctional[mapDOFGlobalR[i]] += jacFunctionalElemR[i];

    } // elem
  }


  template <class TopologyTrace, class TopologyCellL, class TopologyCellR,
            class XFieldType, class Force, class Tg, template<class > class Vector, class MatrixJ,
            class ArrayQ>
  static void
  CellGroups(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<TopologyTrace>& xfldTrace,
      const Field<PhysD3, TopoD3, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      Vector<MatrixJ>& jacFunctional )
  {
    const XField<PhysD3, TopoDim>& xfld = qfld.getXField();

    int groupL = xfldTrace.getGroupLeft();
    int groupR = xfldTrace.getGroupRight();

    // Integrate over the trace group
    Integral<TopologyTrace, TopologyCellL, TopologyCellR, XFieldType, ArrayQ>( fcn, xfldFrame, xfldTrace,
                                  xfld.template getCellGroup<TopologyCellL>(groupL), qfld.template getCellGroup<TopologyCellL>(groupL),
                                  xfld.template getCellGroup<TopologyCellR>(groupR), qfld.template getCellGroup<TopologyCellR>(groupR),
//                      lgfldTrace,
                                  quadratureorder,
                                  jacFunctional );
  }

  template <class TopologyCellL,
            class XFieldType, class Force, class Tg, template<class > class Vector, class MatrixJ,
            class ArrayQ>
  static void
  RightTopology_Triangle(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysD3, TopoD3, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      Vector<MatrixJ>& jacFunctional )
  {
    // determine topology for Left cell group
    int groupR = xfldTrace.getGroupRight();

    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      CellGroups<Triangle, TopologyCellL, Tet, XFieldType>( fcn, xfldFrame, xfldTrace, qfld, //lgfldTrace,
                                                            quadratureorder, jacFunctional );
    }
    else
    {
      char msg[] = "Error in FunctionalBoundaryTrace_mitLG<TopoD3>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  template <class TopologyCellL,
            class XFieldType, class Force, class Tg, template<class > class Vector, class MatrixJ,
            class ArrayQ>
  static void
  RightTopology_Quad(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysD3, TopoD3, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoD3, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      Vector<MatrixJ>& jacFunctional )
  {
    // determine topology for Left cell group
    int groupR = xfldTrace.getGroupRight();

    if ( qfld.getCellGroupBase(groupR).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      CellGroups<Quad, TopologyCellL, Hex, XFieldType>( fcn, xfldFrame, xfldTrace, qfld, //lgfldTrace,
                                                        quadratureorder, jacFunctional );
    }
    else
    {
      char msg[] = "Error in FunctionalBoundaryTrace_mitLG<TopoD3>::RightTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }


  template <class XFieldType, class Force, class Tg, template<class > class Vector, class MatrixJ,
            class ArrayQ>
  static void
  LeftTopology_Triangle(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<Triangle>& xfldTrace,
      const Field<PhysD3, TopoDim, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      Vector<MatrixJ>& jacFunctional )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Tet) )
    {
      // determine topology for R
      RightTopology_Triangle<Tet, XFieldType>( fcn,
          xfldFrame, xfldTrace, qfld,
//          lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          quadratureorder, jacFunctional );
    }
    else
    {
      char msg[] = "Error in FunctionalBoundaryTrace_mitLG<TopoD3>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class XFieldType, class Force, class Tg, template<class > class Vector, class MatrixJ,
            class ArrayQ>
  static void
  LeftTopology_Quad(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const typename XField<PhysD3, TopoDim>::template FieldTraceGroupType<Quad>& xfldTrace,
      const Field<PhysD3, TopoDim, ArrayQ>& qfld,
//      const typename Field<PhysD3, TopoDim, ArrayQ>::template FieldTraceGroupType<TopologyTrace>& lgfldTrace,
      const int quadratureorder,
      Vector<MatrixJ>& jacFunctional )
  {

    // determine topology for Left cell group
    int groupL = xfldTrace.getGroupLeft();

    if ( qfld.getCellGroupBase(groupL).topoTypeID() == typeid(Hex) )
    {
      // determine topology for R
      RightTopology_Quad<Hex, XFieldType>( fcn,
          xfldFrame, xfldTrace, qfld,
//          lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          quadratureorder, jacFunctional );
    }
    else
    {
      char msg[] = "Error in FunctionalBoundaryTrace_mitLG<TopoD3>::LeftTopology: unknown cell topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

  template <class Force, class Tg, class XFieldType, template<class > class Vector, class MatrixJ, class ArrayQ>
  static void
  TraceTopology(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const typename XFieldType::FieldFrameGroupType& xfldFrame,
      const XFieldType& xfld,
      const Field<PhysD3, TopoDim, ArrayQ>& qfld,
//      const Field<PhysD3, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder,
      Vector<MatrixJ>& jacFunctional )
  {

    // loop over trace element groups

    const int boundaryGroupL = xfldFrame.getGroupLeft();

    if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Triangle) )
    {
      LeftTopology_Triangle<XFieldType>( fcn,
          xfldFrame,
          xfld.template getBoundaryTraceGroup<Triangle>(boundaryGroupL),
          qfld,
//          lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          quadratureorder, jacFunctional );
    }
    else if ( xfld.getBoundaryTraceGroupBase(boundaryGroupL).topoTypeID() == typeid(Quad) )
    {
      LeftTopology_Quad<XFieldType>( fcn,
          xfldFrame,
          xfld.template getBoundaryTraceGroup<Quad>(boundaryGroupL),
          qfld,
//          lgfld.template getBoundaryTraceGroupGlobal<Triangle>(boundaryGroupL),
          quadratureorder, jacFunctional );
    }
    else
    {
      char msg[] = "Error in FunctionalTrefftz::TraceTopology: unknown trace topology\n";
      SANS_DEVELOPER_EXCEPTION( msg );
    }
  }

public:
  template <class Force, class Tg, class XFieldType, template<class > class Vector, class MatrixJ,
            class PhysDim, class ArrayQ>
  static void
  integrate(
      const IntegrandBoundaryFrame3D_LIP_Trefftz<Force, Tg>& fcn,
      const XFieldType& xfld,
      const Field<PhysDim, TopoDim, ArrayQ>& qfld,
//      const Field<PhysDim, TopoDim, ArrayQ>& lgfld,
      const int quadratureorder[], int ngroup,
      Vector<MatrixJ>& jacFunctional )
  {
    SANS_ASSERT( &qfld.getXField() == &xfld );
//    SANS_ASSERT( &lgfld.getXField() == &xfld );

    SANS_ASSERT( ngroup == xfld.nBoundaryFrameGroups() );

    // loop over trace element groups
    for (std::size_t group = 0; group < fcn.nBoundaryFrameGroups(); group++)
    {
      const int boundaryFrameGroup = fcn.boundaryFrameGroup(group);

      TraceTopology( fcn,
          xfld.getBoundaryFrameGroup(boundaryFrameGroup),
          xfld,
          qfld,
//          lgfld,
          quadratureorder[boundaryFrameGroup], jacFunctional );

    }
  }
};

}

#endif  // LINEARIZEDINCOMPRESSIBLEPOTENTIAL3D_TREFFTZ_H
