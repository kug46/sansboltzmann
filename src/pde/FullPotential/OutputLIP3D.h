// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef OUTPUT_LIP_3D_H
#define OUTPUT_LIP_3D_H

#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/tools/cross.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"

#include "pde/OutputCategory.h"

#include "Topology/Dimension.h"

namespace SANS
{

//----------------------------------------------------------------------------//
template <class PDENDConvert>
class OutputLIP3D_Force : public OutputType< OutputLIP3D_Force<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  // Array of output functionals
  template<class Z>
  using ArrayJ = Z;

  // Matrix required to represent the Jacobian of this output functional
  template<class Z>
  using MatrixJ = ArrayQ<Z>;

  typedef typename PDENDConvert::VectorX VectorX;

  OutputLIP3D_Force( const PDENDConvert& pde, const Real dx, const Real dy, const Real dz ) :
    pde_(pde), dir_({dx, dy,dz}) {}

  OutputLIP3D_Force( const PDENDConvert& pde, const VectorX& dir ) :
    pde_(pde), dir_(dir) {}

  bool needsSolutionGradient() const { return true; }

public:
  // Area output
  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz, ArrayJ<T>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("LIP force output needs nx, ny, nz");
  }

  // Boundary output
  template<class T, class Ts>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz, ArrayJ<Ts>& output ) const
  {
    DLA::VectorS<PhysDim::D, Ts> force;
    calcForce(nx, ny, nz, q, qx, qy, qz, force);
    output = dot(force, dir_);
  }

  template<class T, class Ts>
  void calcForce(const Real& nx, const Real& ny, const Real& nz,
                 const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz, DLA::VectorS<PhysDim::D, Ts>& force) const
  {
    VectorX N = {nx, ny, nz};
    const auto& U = pde_.freestream();

    // Compute the velocity vector
    //DLA::VectorS<3,T> V = U_ + gradq;
    //DLA::VectorS<3,T> V = {gradq[0], gradq[1], gradq[2]};

    //T U2 = dot(U_,U_);
    //T V2 = dot(V,V); // Local velocity
    T gradq2 = qx*qx + qy*qy + qz*qz;

    // Force integrand
    force = (-0.5*gradq2 - (qx*U[0] + qy*U[1] + qz*U[2]))*N;
    //force = (0.5*U2 - 0.5*V2)*N;
  }

private:
  const PDENDConvert& pde_;
  const VectorX dir_;
};


//----------------------------------------------------------------------------//
template <class PDENDConvert>
class OutputLIP3D_Moment : public OutputType< OutputLIP3D_Force<PDENDConvert> >
{
public:
  typedef PhysD3 PhysDim;
  typedef OutputCategory::Functional Category;

  template<class T>
  using ArrayQ = typename PDENDConvert::template ArrayQ<T>;

  // Array of output functionals
  template<class Z>
  using ArrayJ = Z;

  // Matrix required to represent the Jacobian of this output functional
  template<class Z>
  using MatrixJ = ArrayQ<Z>;

  typedef typename PDENDConvert::VectorX VectorX;

  explicit OutputLIP3D_Moment( const PDENDConvert& pde,
                               const Real& ax, const Real& ay, const Real& az ) :
    outputForce_(pde, ax, ay, az), pde_(pde),
    axis_({ax, ay, az}) {}

  bool needsSolutionGradient() const { return true; }

public:
  // Area output
  template<class T>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz, ArrayJ<T>& output ) const
  {
    SANS_DEVELOPER_EXCEPTION("LIP moment output needs nx, ny, nz");
  }

  // Boundary output
  template<class T, class Ts>
  void operator()(const Real& x, const Real& y, const Real& z, const Real& time,
                  const Real& nx, const Real& ny, const Real& nz,
                  const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz, ArrayJ<Ts>& output ) const
  {
    DLA::VectorS<PhysDim::D, Ts> force, moment;
    const DLA::VectorS<PhysDim::D, Ts>& X0 = pde_.Xref();

    outputForce_.calcForce(nx, ny, nz, q, qx, qy, qz, force);

    DLA::VectorS<PhysDim::D, Ts> dX = {(x - X0[0]), (y - X0[1]), (z - X0[2])};

    // Compute the moment
    moment = cross(dX, force);

    // get the moment about the axis
    output = dot(moment, axis_);
  }

private:
  OutputLIP3D_Force<PDENDConvert> outputForce_;
  const PDENDConvert& pde_;
  const VectorX axis_;
};

}

#endif //OUTPUT_LIP_3D_H
