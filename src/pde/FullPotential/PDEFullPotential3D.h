// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEFULLPOTENTIAL3D_H
#define PDEFULLPOTENTIAL3D_H

// 3-D Full Potential PDE class

#include <iostream>
#include <string>
#include <memory> //shared_ptr
#include <cmath> //sqrt, pow
#include <limits> //numeric_limits

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "Topology/Dimension.h"

namespace SANS
{

template <int N>
class SolutionFunction3D;


//----------------------------------------------------------------------------//
// PDE class: 3-D Full Potential
//
//   phifull = (a*x + b*y + c*z) + phi(x,y,z)
//   0 = rho - rho(phifull)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <class Tg>
class PDEFullPotential3D
{
public:
  typedef PhysD3 PhysDim;                     // physical dimensions

  static const int N = 2;                     // total solution variables

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;           // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>;        // matrices

  typedef std::shared_ptr< SolutionFunction3D<1> > SolutionFunction3D_ptr;

  PDEFullPotential3D( const Real& a, const Real& b, const Real& c,
                      const Real& gamma, const Real& Minf, const Real& rhoinf, const Real& pinf,
                      const Real& critVelFrac, const SolutionFunction3D_ptr& sln = NULL) :
      U_({a,b,c}),
      gamma_(gamma), gm1_(gamma-1), Minf_(Minf), Minf2_(Minf*Minf), rhoinf_(rhoinf), pinf_(pinf),
      Vinf2_(U_[0]*U_[0] + U_[1]*U_[1] + U_[2]*U_[2]), Vcrit_(0),
      rhocrit_(0), pcrit_(0), betacrit_(0),
      sln_(sln)
  {
    SANS_ASSERT( Minf >= 0 );
    SANS_ASSERT( critVelFrac > 0 && critVelFrac < 1);

    if ( Minf == 0 )
      Vcrit_ = std::numeric_limits<Real>::max();
    else
    {
      // Vacum velocity
      Tg Vvac = sqrt(Vinf2_*(1.0 + 2./(gm1_*Minf2_)));

      // Critical velocity for Pade limiting
      Vcrit_ = critVelFrac*Vvac;

      Tg Vcrit2 = Vcrit_*Vcrit_;

      // density/pressure evaluated at the critical velocity
      rhocrit_ = density_sh( Vcrit2 );
      pcrit_   = pressure_sh( Vcrit2 );

      // Beta coefficient in Pade limiter
      betacrit_ = Minf2_*Vcrit_/sqrt(Vinf2_) / ( 1 + (gm1_/2.)*Minf2_*(1. - Vcrit_*Vcrit_/Vinf2_) );
    }
  }

  PDEFullPotential3D( const PDEFullPotential3D& pde ) = delete;
  PDEFullPotential3D& operator=( const PDEFullPotential3D& ) = delete;

  ~PDEFullPotential3D() {}

  // flux components
  bool hasFluxAdvectiveTime() const { return false; }
  bool hasFluxAdvective() const { return false; }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool hasForcingFunction() const { return (sln_ != NULL); }

  bool needsSolutionGradientforSource() const { return true; }

  // freestream velocity
  void freestream( Tg& a, Tg& b, Tg& c ) const { a = U_[0]; b = U_[1];  c = U_[2]; }
  void freestream( DLA::VectorS<3,Tg>& U ) const { U = U_; }
  const DLA::VectorS<3,Tg>& freestream() const { return U_; }
  void setFreestream( DLA::VectorS<3,Tg>& U ) { U_ = U; }

  // unsteady temporal flux: Ft(Q)
  template <class T>
  void fluxAdvectiveTime(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& ft ) const {}

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& J ) const {}

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
  {
    uCons = q;
  }

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template <class T>
  inline void
  jacobianMasterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    dudq = DLA::Identity();
  }

  // advective flux: F(Q)
  template <class T>
  void fluxAdvective(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q,
      ArrayQ< typename promote_Surreal<T,Tg>::type >& F,
      ArrayQ< typename promote_Surreal<T,Tg>::type >& G,
      ArrayQ< typename promote_Surreal<T,Tg>::type >& H ) const {}
  template <class T>
  void fluxAdvective(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<T>& f ) const {}

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q,
      MatrixQ<T>& ax, MatrixQ<T>& ay, MatrixQ<T>& az ) const {}

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& a ) const {}

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q,
      const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
      ArrayQ<T>& strongPDE ) const {}

  // viscous flux: Fv(X, Q, QX)
  template <class T>
  void fluxViscous(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
      ArrayQ< typename promote_Surreal<T,Tg>::type >& F,
      ArrayQ< typename promote_Surreal<T,Tg>::type >& G,
      ArrayQ< typename promote_Surreal<T,Tg>::type >& H ) const;
  template <class T>
  void fluxViscous(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL, const ArrayQ<T>& qzL,
      const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR, const ArrayQ<T>& qzR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<T>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Td, class Tk>
  void diffusionViscous(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Td>& qx, const ArrayQ<Td>& qy, const ArrayQ<Td>& qz,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax, MatrixQ<T>& ay ) const {}

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
      const ArrayQ<T>& qxx,
      const ArrayQ<T>& qxy, const ArrayQ<T>& qyy,
      const ArrayQ<T>& qxz, const ArrayQ<T>& qyz, const ArrayQ<T>& qzz,
      ArrayQ<T>& strongPDE ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class T>
  void source(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
      ArrayQ< typename promote_Surreal<T,Tg>::type >& source ) const;

  // dual-consistent source
  template <class T>
  void sourceTrace(
      const Real& h,
      const Real& xL, const Real& yL, const Real& zL,
      const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL, const ArrayQ<T>& qzL,
      const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR, const ArrayQ<T>& qzR,
      ArrayQ<T>& sourceL, ArrayQ<T>& sourceR ) const {}

  // jacobian of solution-dependent source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
      MatrixQ<T>& dsdu, MatrixQ<T>& dsdux, MatrixQ<T>& dsduy ) const {}

  // right-hand-side forcing function
  void forcingFunction( const Real& h,const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& dx, const Real& dy, const Real& dz,
      const ArrayQ<T>& q, Real& speed ) const {}

  // characteristic speed
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, Real& speed ) const {}

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const;

  // update fraction needed for physically valid state
  template <class T>
  void updateFraction(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& dq,
      Real maxChangeFraction, Real& updateFraction ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

  // density based on the solution
  template<class T>
  typename promote_Surreal<T,Tg>::type
  density( const ArrayQ<T>& q ) const;

  template<class T>
  typename promote_Surreal<T,Tg>::type
  density_grad( const ArrayQ<T>& q, const ArrayQ<T>& qX ) const;

  template<class T>
  typename promote_Surreal<T,Tg>::type
  density_sh( const DLA::VectorS<3,ArrayQ<T>>& gradq ) const { return density_sh(gradq[0],gradq[1],gradq[2]); }

  // density based on the solution gradient
  template<class T>
  typename promote_Surreal<T,Tg>::type
  density_sh( const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const;

  template<class T>
  typename promote_Surreal<T,Tg>::type
  enthalpy_sh( const DLA::VectorS<3,ArrayQ<T>>& gradq ) const { return enthalpy_sh(gradq[0],gradq[1],gradq[2]); }

  // enthalpy based on the solution gradient
  template<class T>
  typename promote_Surreal<T,Tg>::type
  enthalpy_sh( const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const;

  // Mach number based on the solution gradient
  template<class T>
  typename promote_Surreal<T,Tg>::type
  mach(const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const;

  // pressure based on the solution gradient
  template<class T>
  typename promote_Surreal<T,Tg>::type
  pressure( const DLA::VectorS<3,ArrayQ<T>>& gradq ) const { return pressure(gradq[0],gradq[1],gradq[2]); }

  template<class T>
  typename promote_Surreal<T,Tg>::type
  pressure( const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const;

  // pressure given a velocity vector
  template<class T>
  T pressure( const DLA::VectorS<PhysDim::D, T>& V ) const;

  // mach number based density upwinding switch
  template<class T>
  T mu( const T& M ) const;

  Real gamma() const { return gamma_; }
  Real rhoinf() const { return rhoinf_; }
  Real pinf() const { return pinf_; }

private:
  // density based on a velocity
  template<class T>
  T density_sh( const T& V2 ) const;

  // pressure based on a velocity
  template<class T>
  T pressure_sh( const T& V2 ) const;

  // density upwinding function
  template<class T>
  typename promote_Surreal<T,Tg>::type
  rhoUpwind( const Real& h, const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const;

  DLA::VectorS<3, Tg> U_;        // freestream velocity
  const Real gamma_;
  const Real gm1_;
  const Real Minf_;
  const Real Minf2_;
  const Real rhoinf_;
  const Real pinf_;
  const Tg Vinf2_;

  Tg Vcrit_;        // critical velocity for Pade limit on density
  Tg rhocrit_;      // density evaluated at the critical velocity
  Tg pcrit_;        // pressure evaluated at the critical velocity
  Tg betacrit_;     // Pade limiter coefficient

  SolutionFunction3D_ptr sln_;     // solution for RHS forcing function
};

template<class Tg>
template<class T>
inline typename promote_Surreal<T,Tg>::type
PDEFullPotential3D<Tg>::density( const ArrayQ<T>& q ) const
{
  // A limited density calcualtion that is always positive
  //const Tg& c = rhocrit_/2.;
  const T& rho = q[1];

  //if (rho >= c)
    return rho;
  //else
  //  return c / ( 3.0 - 3.0*(rho/c) + (rho/c)*(rho/c) );
}

template<class Tg>
template<class T>
inline typename promote_Surreal<T,Tg>::type
PDEFullPotential3D<Tg>::density_grad( const ArrayQ<T>& q, const ArrayQ<T>& qX ) const
{
  // A limited density calcualtion that is always positive
  //const Tg c = rhocrit_/2.;
  //const T& rho = q[1];
  const T& rhoX = qX[1];

  //if (rho >= c)
    return rhoX;
  //else
  //  return -c * (-3.0*rhoX/c + 2.0*rho*rhoX/(c*c)) / pow( 3.0 - 3.0*(rho/c) + (rho/c)*(rho/c), 2 );
}

template<class Tg>
template<class T>
inline T
PDEFullPotential3D<Tg>::density_sh( const T& V2 ) const
{
  // Isentropic density calculation
  return rhoinf_*pow( 1 + (gm1_/2.)*Minf2_*(1. - V2/Vinf2_), 1./gm1_);
}

template<class Tg>
template<class T>
inline typename promote_Surreal<T,Tg>::type
PDEFullPotential3D<Tg>::density_sh( const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;

  Ts u = qx[0] + U_[0];
  Ts v = qy[0] + U_[1];
  Ts w = qz[0] + U_[2];

  Ts V2 = u*u + v*v + w*w;
  Ts V = sqrt(V2);

  if ( V > Vcrit_ )
  {
    // Pade limited density
    return rhocrit_/( 1 + betacrit_*(V/Vcrit_ - 1));
  }
  else
  {
    // Isentropic density calculation
    return density_sh(V2);
  }
}

template<class Tg>
template<class T>
inline typename promote_Surreal<T,Tg>::type
PDEFullPotential3D<Tg>::enthalpy_sh( const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;

  Ts rho_sh = density_sh(qx,qy,qz);
  Ts p_sh = pressure(qx,qy,qz);

  return gamma_/gm1_*p_sh/rho_sh;
}

template<class Tg>
template<class T>
inline typename promote_Surreal<T,Tg>::type
PDEFullPotential3D<Tg>::mach( const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;

  //const T& rho = q[1];
  //Ts rho = density(q);

  Ts rho_sh = density_sh(qx,qy,qz);
  Ts p_sh = pressure(qx,qy,qz);

  Ts u = qx[0] + U_[0];
  Ts v = qy[0] + U_[1];
  Ts w = qz[0] + U_[2];

  Ts V2 = u*u + v*v + w*w;
  Ts V = sqrt(V2);

  Ts c = sqrt(gamma_*p_sh/rho_sh);

  return V/c;
}

template<class Tg>
template<class T>
inline typename promote_Surreal<T,Tg>::type
PDEFullPotential3D<Tg>::pressure( const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;

  DLA::VectorS<PhysDim::D, Ts> V;

  V[0] = qx[0] + U_[0];
  V[1] = qy[0] + U_[1];
  V[2] = qz[0] + U_[2];

  return pressure(V);
}

template<class Tg>
template<class T>
inline T
PDEFullPotential3D<Tg>::pressure_sh( const T& V2 ) const
{
  // Isentropic pressure calculation
  return pinf_*pow( 1 + (gm1_/2.)*Minf2_*(1. - V2/Vinf2_), gamma_/gm1_);
}

template<class Tg>
template<class T>
inline T
PDEFullPotential3D<Tg>::pressure( const DLA::VectorS<PhysDim::D, T>& Vv ) const
{
  T V2 = Vv[0]*Vv[0] + Vv[1]*Vv[1] + Vv[2]*Vv[2];
  T V = sqrt(V2);

  if ( V > Vcrit_ )
  {
    // Pade limited pressure
    return pcrit_/( 1 + betacrit_*(V/Vcrit_ - 1));
  }
  else
  {
    // Isentropic pressure calculation
    return pressure_sh(V2);
  }
}

template<class Tg>
template<class T>
inline T
PDEFullPotential3D<Tg>::mu( const T& M ) const
{
  if (M > 1)
    return 0.8*sqrt(M*M - 1);
  else
    return 0;
}

template<class Tg>
template<class T>
inline typename promote_Surreal<T,Tg>::type
PDEFullPotential3D<Tg>::rhoUpwind( const Real& h, const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;

  //const T& rho = q[1];

  //const T& rhox = qx[1];
  //const T& rhoy = qy[1];
  //const T& rhoz = qz[1];

  Ts rho = density(q);

  Ts rhox = density_grad(q, qx);
  Ts rhoy = density_grad(q, qy);
  Ts rhoz = density_grad(q, qz);

  Ts u = qx[0] + U_[0];
  Ts v = qy[0] + U_[1];
  Ts w = qz[0] + U_[2];

  Ts V2 = u*u + v*v + w*w;
  Ts V = sqrt(V2);

  Ts M = mach( q, qx, qy, qz );

  return rho - h*mu(M)*(rhox*u + rhoy*v + rhoz*w)/V;
}

// viscous flux
template<class Tg>
template <class T>
inline void
PDEFullPotential3D<Tg>::fluxViscous(
    const Real& h,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
    ArrayQ< typename promote_Surreal<T,Tg>::type >& F,
    ArrayQ< typename promote_Surreal<T,Tg>::type >& G,
    ArrayQ< typename promote_Surreal<T,Tg>::type >& H ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;

  Ts rhowind = rhoUpwind(h, q, qx, qy, qz);

#if 1
  Ts u = qx[0] + U_[0];
  Ts v = qy[0] + U_[1];
  Ts w = qz[0] + U_[2];

  F[0] -= rhowind*u;
  G[0] -= rhowind*v;
  H[0] -= rhowind*w;
#endif

#if 0
  const T& rho = q[1];

  F[0] -= rho*qx[0] + rhowind*U_[0];
  G[0] -= rho*qy[0] + rhowind*U_[1];
  H[0] -= rho*qz[0] + rhowind*U_[2];
#endif
}

// viscous flux: normal flux with left and right states
template<class Tg>
template <class T>
inline void
PDEFullPotential3D<Tg>::fluxViscous(
    const Real& h,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL, const ArrayQ<T>& qzL,
    const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR, const ArrayQ<T>& qzR,
    const Real& nx, const Real& ny, const Real& nz, ArrayQ<T>& f ) const
{
  const T& rhoL = qL[1];
  const T& rhoR = qR[1];

  const T& phixL = qxL[0];
  const T& phiyL = qyL[0];
  const T& phizL = qzL[0];

  const T& phixR = qxR[0];
  const T& phiyR = qyR[0];
  const T& phizR = qzR[0];

  f[0] -= 0.5*(nx*(rhoL*phixL + rhoR*phixR) + ny*(rhoL*phiyL + rhoR*phiyR) + nz*(rhoL*phizL + rhoR*phizR));
}


// viscous diffusion matrix: d(Fv)/d(UX)
template<class Tg>
template <class Tq, class Td, class Tk>
inline void
PDEFullPotential3D<Tg>::diffusionViscous(
    const Real& h,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Td>& qx, const ArrayQ<Td>& qy, const ArrayQ<Td>& qz,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
    MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
{
  SANS_DEVELOPER_EXCEPTION("Can't compute diffusion matrix without solution gradient...");
#if 0
  T rhowind = rhoUpwind(h, q, qx, qy, qz);

  kxx(0,0) += rhowind;
  kyy(0,0) += rhowind;
  kzz(0,0) += rhowind;
#endif
}


// strong form of viscous flux: d(Fv)/d(X)
template<class Tg>
template <class T>
inline void
PDEFullPotential3D<Tg>::strongFluxViscous(
    const Real& h,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q,
    const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
    const ArrayQ<T>& qxx,
    const ArrayQ<T>& qxy, const ArrayQ<T>& qyy,
    const ArrayQ<T>& qxz, const ArrayQ<T>& qyz, const ArrayQ<T>& qzz,
    ArrayQ<T>& strongPDE ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;
#if 0
  const T& rho = q[1];
  const T& rhox = qx[1];
  const T& rhoy = qy[1];
  const T& rhoz = qz[1];
#endif
  Ts rho = density(q);
  Ts rhox = density_grad(q, qx);
  Ts rhoy = density_grad(q, qy);
  Ts rhoz = density_grad(q, qz);

  T u = U_[0] + qx[0];
  T v = U_[1] + qy[0];
  T w = U_[2] + qz[0];

  strongPDE[0] -= rho*(qxx[0] + qyy[0] + qzz[0]) + rhox*u + rhoy*v + rhoz*w;
}


// solution-dependent source: S(X, Q, QX)
template<class Tg>
template <class T>
inline void
PDEFullPotential3D<Tg>::source(
    const Real& h,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
    ArrayQ< typename promote_Surreal<T,Tg>::type >& source ) const
{
  typedef typename promote_Surreal<T,Tg>::type Ts;

  Ts rho = density_sh(qx, qy, qz);

  source[1] += q[1] - rho;
}


// right-hand-side forcing function
template<class Tg>
inline void
PDEFullPotential3D<Tg>::forcingFunction( const Real& h, const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& forcing ) const
{
  SANS_ASSERT_MSG( sln_ != NULL, "RHS forcing function can only be used with a provided solution" );
  SANS_DEVELOPER_EXCEPTION("Forcing function not implemented!");
#if 0
  ArrayQ<Real> q, qx, qy, qxx, qxy, qyy;
  (*sln_)( x, y, q, qx, qy, qxx, qxy, qyy );

  strongFluxViscous( x, y, q, qx, qy, qxx, qxy, qyy, forcing );
#endif
}


// set from primitive variable array
template<class Tg>
template <class T>
inline void
PDEFullPotential3D<Tg>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q[0] = data[0];
  q[1] = data[1];
}

template<class Tg>
template <class T>
inline bool
PDEFullPotential3D<Tg>::isValidState( const ArrayQ<T>& q ) const
{
  // Just check positive density for now
  return density(q) > 0;
}


// interpret residuals of the solution variable
template<class Tg>
template <class T>
inline void
PDEFullPotential3D<Tg>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT(rsdPDEOut.m() == 2);
  rsdPDEOut[0] = rsdPDEIn[0];
  rsdPDEOut[1] = rsdPDEIn[1];
}

// interpret residuals of the gradients in the solution variable
template<class Tg>
template <class T>
inline void
PDEFullPotential3D<Tg>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_DEVELOPER_EXCEPTION( "PDEFullPotential3D<Tg>::interpResidGradient not implemented" );
}

// interpret residuals at the boundary (should forward to BCs)
template<class Tg>
template <class T>
inline void
PDEFullPotential3D<Tg>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT(rsdBCOut.m() == 2);
  rsdBCOut[0] = rsdBCIn[0];
  rsdBCOut[1] = rsdBCIn[1];
}

// how many residual equations are we dealing with
template<class Tg>
inline int
PDEFullPotential3D<Tg>::nMonitor() const
{
  return 2;
}

} //namespace SANS

#endif  // PDEFULLPOTENTIAL3D_H
