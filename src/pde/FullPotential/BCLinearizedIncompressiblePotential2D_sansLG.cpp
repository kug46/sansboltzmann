// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCLinearizedIncompressiblePotential2D_sansLG.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

// cppcheck-suppress passedByValue
void BCLinearizedIncompressiblePotential2DParams<BCTypeDirichlet>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.q));
  d.checkUnknownInputs(allParams);
}
BCLinearizedIncompressiblePotential2DParams<BCTypeDirichlet> BCLinearizedIncompressiblePotential2DParams<BCTypeDirichlet>::params;

template<>
void
BCLinearizedIncompressiblePotential2D<BCTypeDirichlet, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential2D<BCTypeDirichlet>:" << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}


// cppcheck-suppress passedByValue
void BCLinearizedIncompressiblePotential2DParams<BCTypeNeumann>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.gradqn));
  d.checkUnknownInputs(allParams);
}
BCLinearizedIncompressiblePotential2DParams<BCTypeNeumann> BCLinearizedIncompressiblePotential2DParams<BCTypeNeumann>::params;

template<>
void
BCLinearizedIncompressiblePotential2D<BCTypeNeumann, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential2D<BCTypeNeumann>:" << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}


// cppcheck-suppress passedByValue
void BCLinearizedIncompressiblePotential2DParams<BCTypeWall>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  d.checkUnknownInputs(allParams);
}
BCLinearizedIncompressiblePotential2DParams<BCTypeWall> BCLinearizedIncompressiblePotential2DParams<BCTypeWall>::params;

template<>
void
BCLinearizedIncompressiblePotential2D<BCTypeWall, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential2D<BCTypeWall>:" << std::endl;
  out << indent << "  (u_, v_) = (" << u_ << ", " << v_ << ")" << std::endl;
}


// cppcheck-suppress passedByValue
void BCLinearizedIncompressiblePotential2DParams<BCTypeFarfieldVortex>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.x0));
  allParams.push_back(d.checkInputs(params.y0));
  allParams.push_back(d.checkInputs(params.circ));
  d.checkUnknownInputs(allParams);
}
BCLinearizedIncompressiblePotential2DParams<BCTypeFarfieldVortex> BCLinearizedIncompressiblePotential2DParams<BCTypeFarfieldVortex>::params;

template<>
void
BCLinearizedIncompressiblePotential2D<BCTypeFarfieldVortex, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential2D<BCTypeFarfieldVortex>:" << std::endl;
  out << indent << "  (x0_, y0_) = (" << x0_ << ", " << y0_ << ")  circ = " << circ_ << std::endl;
}


// cppcheck-suppress passedByValue
void BCLinearizedIncompressiblePotential2DParams<BCTypeFunction>::checkInputs(PyDict d)
{
  std::vector<const ParameterBase*> allParams;
  allParams.push_back(d.checkInputs(params.Function));
  d.checkUnknownInputs(allParams);
}
BCLinearizedIncompressiblePotential2DParams<BCTypeFunction> BCLinearizedIncompressiblePotential2DParams<BCTypeFunction>::params;

template<>
void
BCLinearizedIncompressiblePotential2D<BCTypeFunction, Real>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential2D<BCTypeFunction>:" << std::endl;
}


//===========================================================================//
// Instantiate the BC parameters
BCPARAMETER_INSTANTIATE( BCLinearizedIncompressiblePotential2DVector<Real> )
BCPARAMETER_INSTANTIATE( BCLinearizedIncompressiblePotential2DVector<SurrealS<1>> )

} //namespace SANS
