// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDELINEARIZEDINCOMPRESSIBLEPOTENTIAL3D_H
#define PDELINEARIZEDINCOMPRESSIBLEPOTENTIAL3D_H

// 3-D Linearized Incompressible Potential PDE class

#include <iostream>
#include <string>
#include <memory>

#include "tools/SANSnumerics.h"     // Real
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/tools/cross.h"
#include "Topology/Dimension.h"

namespace SANS
{

template <int N>
class SolutionFunction3D;


//----------------------------------------------------------------------------//
// PDE class: 3-D Linearized Incompressible Potential
//
//   phifull = (a*x + b*y + c*z) + phi(x,y,z)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous             T/F: PDE has viscous flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: dU(Q)/dQ
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .fluxViscous                viscous fluxes: Fv(Q, QX)
//   .diffusionViscous           viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

class CopyPDE {};

template <class Ta>
class PDELinearizedIncompressiblePotential3D
{
public:
  typedef PhysD3 PhysDim;                     // physical dimensions

 // static const int D = PhysDim::D;            // physical dimensions
  static const int N = 1;                     // total solution variables

  template <class T>
  using ArrayQ = T;         // solution/residual arrays

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using MatrixQ = T;        // matrices

  typedef std::shared_ptr< SolutionFunction3D<1> > SolutionFunction3D_ptr;

  PDELinearizedIncompressiblePotential3D( const Ta& a, const Ta& b, const Ta& c ) :
      U_({a,b,c}), Omega_(0), Xref_(0), sln_(NULL) {}
  PDELinearizedIncompressiblePotential3D( const Ta& a, const Ta& b, const Ta& c, const SolutionFunction3D_ptr& sln ) :
      U_({a,b,c}), Omega_(0), Xref_(0), sln_(sln) {}
  explicit PDELinearizedIncompressiblePotential3D( const DLA::VectorS<3,Ta>& U, const DLA::VectorS<3,Ta>& Omega, const DLA::VectorS<3,Ta>& Xref ) :
      U_(U), Omega_(Omega), Xref_(Xref), sln_(NULL) {}

  PDELinearizedIncompressiblePotential3D( const PDELinearizedIncompressiblePotential3D& pde ) = delete;

  ~PDELinearizedIncompressiblePotential3D() {}

  PDELinearizedIncompressiblePotential3D& operator=( const PDELinearizedIncompressiblePotential3D& );

  // flux components
  bool hasFluxAdvectiveTime() const { return false; }
  bool hasFluxAdvective() const { return false; }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasForcingFunction() const { return (sln_ != NULL); }

  bool needsSolutionGradientforSource() const { return false; }

  // freestream velocity
  void freestream( Ta& a, Ta& b, Ta& c ) const { a = U_[0]; b = U_[1];  c = U_[2]; }
  void freestream( DLA::VectorS<3,Ta>& U ) const { U = U_; }
  const DLA::VectorS<3,Ta>& freestream() const { return U_; }
  void setFreestream( const DLA::VectorS<3,Ta>& U ) { U_ = U; }

  void Omega( DLA::VectorS<3,Ta>& omega ) const { omega = Omega_; }
  const DLA::VectorS<3,Ta>& Omega() const { return Omega_; }
  void setRotationRates( const DLA::VectorS<3,Ta>& Omega ) { Omega_ = Omega; }

  void Xref( DLA::VectorS<3,Ta>& xref ) const { xref = Xref_; }
  const DLA::VectorS<3,Ta>& Xref() const { return Xref_; }
  void setRefLocation( const DLA::VectorS<3,Ta>& Xref ) { Xref_ = Xref; }

  template<class Tx>
  const DLA::VectorS<3, typename promote_Surreal<Ta,Tx>::type> Up( const DLA::VectorS<3,Tx>& X ) const
  {
    return Up(X[0], X[1], X[2]);
  }
  template<class Tx>
  const DLA::VectorS<3, typename promote_Surreal<Ta,Tx>::type> Up( const Tx& x, const Tx& y, const Tx& z ) const
  {
    typedef typename promote_Surreal<Ta,Tx>::type T;
    DLA::VectorS<3, T> rp = {x - Xref_[0], y - Xref_[1], z - Xref_[2]};
    return U_ + cross(Omega_, rp);
  }

  // unsteady temporal flux: Ft(Q)
  template <class T>
  void fluxAdvectiveTime(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& ft ) const {}

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template <class T>
  void jacobianFluxAdvectiveTime(
      const Real& h,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& J ) const {}

  // master state: U(Q)
  template <class T>
  void masterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<T>& uCons ) const
  {
    uCons = q;
  }

  // unsteady conservative flux Jacobian: dU(Q)/dQ
  template <class T>
  inline void
  jacobianMasterState(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
  {
    dudq = DLA::Identity();
  }

  // advective flux: F(Q)
  template <class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const {}
  template <class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f ) const {}

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q,
      MatrixQ<T>& ax, MatrixQ<T>& ay, MatrixQ<T>& az ) const {}

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& a ) const {}

  // strong form advective fluxes
  template <class T>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q,
      const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
      ArrayQ<T>& strongPDE ) const {}

  // viscous flux: Fv(X, Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const;
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class T>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax, MatrixQ<T>& ay ) const {}

  // strong form viscous fluxes
  template <class T>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
      const ArrayQ<T>& qxx,
      const ArrayQ<T>& qxy, const ArrayQ<T>& qyy,
      const ArrayQ<T>& qxz, const ArrayQ<T>& qyz, const ArrayQ<T>& qzz,
      ArrayQ<T>& strongPDE ) const;

  // solution-dependent source: S(X, D, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const {}

  // solution-dependent source: S(Q, QX)
  template <class Tq, class Tg, class Ts>
  void source( const ArrayQ<Tq>& q,
               const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
               ArrayQ<Ts>& source ) const {}

  // dual-consistent source
  template <class T>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& zL,
      const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qxL, const ArrayQ<T>& qyL, const ArrayQ<T>& qzL,
      const ArrayQ<T>& qR, const ArrayQ<T>& qxR, const ArrayQ<T>& qyR, const ArrayQ<T>& qzR,
      ArrayQ<T>& sourceL, ArrayQ<T>& sourceR ) const {}

  // jacobian of solution-dependent source wrt conservation variables: d(S)/d(U)
  template <class T>
  void jacobianSource(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
      MatrixQ<T>& dsdu, MatrixQ<T>& dsdux, MatrixQ<T>& dsduy ) const {}

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& dx, const Real& dy, const Real& dz,
      const ArrayQ<T>& q, Real& speed ) const {}

  // characteristic speed
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, Real& speed ) const {}

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return true; }

  // update fraction needed for physically valid state
  template <class T>
  void updateFraction(
      const Real& x, const Real& y, const Real& z, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& dq,
      Real maxChangeFraction, Real& updateFraction ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  DLA::VectorS<3, Ta> U_;          // freestream velocity (gradient of potential a*x + b*y + c*z)
  DLA::VectorS<3, Ta> Omega_;      // rotation rates
  DLA::VectorS<3, Ta> Xref_;       // reference location (center of gravity)

  SolutionFunction3D_ptr sln_;     // solution for RHS forcing function
};

template <class Ta>
inline PDELinearizedIncompressiblePotential3D<Ta>&
PDELinearizedIncompressiblePotential3D<Ta>::operator=( const PDELinearizedIncompressiblePotential3D& pde )
{
  U_   = pde.U_;
  sln_ = pde.sln_;

  return *this;
}


// viscous flux
template <class Ta>
template <class Tq, class Tg, class Tf>
inline void
PDELinearizedIncompressiblePotential3D<Ta>::fluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
{
  f -= qx;
  g -= qy;
  h -= qz;
}

// viscous flux: normal flux with left and right states
template <class Ta>
template <class Tq, class Tg, class Tf>
inline void
PDELinearizedIncompressiblePotential3D<Ta>::fluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
    const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& f ) const
{
  f -= 0.5*(nx*(qxL + qxR) + ny*(qyL + qyR) + nz*(qzL + qzR));
}


// viscous diffusion matrix: d(Fv)/d(UX)
template <class Ta>
template <class Tq, class Tg, class Tk>
inline void
PDELinearizedIncompressiblePotential3D<Ta>::diffusionViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
    MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
{
  kxx += 1;
  kyy += 1;
  kzz += 1;
}


// strong form of viscous flux: d(Fv)/d(X)
template <class Ta>
template <class T>
inline void
PDELinearizedIncompressiblePotential3D<Ta>::strongFluxViscous(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q,
    const ArrayQ<T>& qx, const ArrayQ<T>& qy, const ArrayQ<T>& qz,
    const ArrayQ<T>& qxx,
    const ArrayQ<T>& qxy, const ArrayQ<T>& qyy,
    const ArrayQ<T>& qxz, const ArrayQ<T>& qyz, const ArrayQ<T>& qzz,
    ArrayQ<T>& strongPDE ) const
{
  strongPDE -= qxx + qyy + qzz;
}


// right-hand-side forcing function
template <class Ta>
inline void
PDELinearizedIncompressiblePotential3D<Ta>::forcingFunction(
    const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<Real>& forcing ) const
{
  SANS_ASSERT_MSG( sln_ != NULL, "RHS forcing function can only be used with a provided solution" );

#if 0
  ArrayQ<Real> q, qx, qy, qxx, qxy, qyy;
  (*sln_)( x, y, q, qx, qy, qxx, qxy, qyy );

  strongFluxViscous( x, y, q, qx, qy, qxx, qxy, qyy, forcing );
#endif
}


// set from primitive variable array
template <class Ta>
template <class T>
inline void
PDELinearizedIncompressiblePotential3D<Ta>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q = data[0];
}

// interpret residuals of the solution variable
template <class Ta>
template <class T>
inline void
PDELinearizedIncompressiblePotential3D<Ta>::interpResidVariable(
    const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  SANS_ASSERT(rsdPDEOut.m() == 1);
  rsdPDEOut[0] = rsdPDEIn;
}

// interpret residuals of the gradients in the solution variable
template <class Ta>
template <class T>
inline void
PDELinearizedIncompressiblePotential3D<Ta>::interpResidGradient(
    const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  SANS_DEVELOPER_EXCEPTION( "PDELinearizedIncompressiblePotential3D<Ta>::interpResidGradient not implemented" );
}

// interpret residuals at the boundary (should forward to BCs)
template <class Ta>
template <class T>
inline void
PDELinearizedIncompressiblePotential3D<Ta>::interpResidBC(
    const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  SANS_ASSERT(rsdBCOut.m() == 1);
  rsdBCOut[0] = rsdBCIn;
}

// how many residual equations are we dealing with
template <class Ta>
inline int
PDELinearizedIncompressiblePotential3D<Ta>::nMonitor() const
{
  return 1;
}

} //namespace SANS

#endif  // PDELINEARIZEDINCOMPRESSIBLEPOTENTIAL3D_H
