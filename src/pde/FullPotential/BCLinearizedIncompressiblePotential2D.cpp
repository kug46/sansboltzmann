// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "BCLinearizedIncompressiblePotential2D.h"

namespace SANS
{

void
BC<PDELinearizedIncompressiblePotential2D, BCTypeDirichlet>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BC<PDELinearizedIncompressiblePotential2D, BCTypeDirichlet>:" << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}


void
BC<PDELinearizedIncompressiblePotential2D, BCTypeNeumann>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BC<PDELinearizedIncompressiblePotential2D, BCTypeNeumann>:" << std::endl;
  out << indent << "  bcdata_ = " << bcdata_ << std::endl;
}


void
BC<PDELinearizedIncompressiblePotential2D, BCTypeWall>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BC<PDELinearizedIncompressiblePotential2D, BCTypeWall>:" << std::endl;
  out << indent << "  (u_, v_) = (" << u_ << ", " << v_ << ")" << std::endl;
}


void
BC<PDELinearizedIncompressiblePotential2D, BCTypeFarfieldVortex>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BC<PDELinearizedIncompressiblePotential2D, BCTypeFarfieldVortex>:" << std::endl;
  out << indent << "  (x0_, y0_) = (" << x0_ << ", " << y0_ << ")" << std::endl;
}


void
BC<PDELinearizedIncompressiblePotential2D, BCTypeFunction<SolutionDummy> >::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BC<PDELinearizedIncompressiblePotential2D, BCTypeFunction<SolutionDummy> >:" << std::endl;
}

} //namespace SANS
