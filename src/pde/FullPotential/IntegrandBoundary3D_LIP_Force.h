// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef INTEGRANDBOUNDARY3D_LIP_FORCE_H
#define INTEGRANDBOUNDARY3D_LIP_FORCE_H

#include "Field/Element/ElementVolume.h"
#include "Field/Element/ElementXFieldVolume.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/VectorSize.h"
#include "BasisFunction/TraceToCellRefCoord.h"

#include "PDELinearizedIncompressiblePotential3D.h"

#include "Discretization/Integrand_Type.h"
#include "pde/OutputCategory.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// Element boundary integrand: Force
template <class Tg>
class IntegrandBoundary3D_LIP_Force :
    public IntegrandBoundaryTraceType< IntegrandBoundary3D_LIP_Force<Tg> >
{
public:
  typedef PDELinearizedIncompressiblePotential3D<Tg> PDE;
  typedef Galerkin DiscTag;
  typedef typename OutputCategory::Functional Category;

  typedef typename PDE::PhysDim PhysDim;

  // Array of the field variables integrated
  template<class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;

  // Array of output functionals
  template<class Z>
  using ArrayJ = DLA::VectorS<3,Z>;

  static const int nEq = DLA::VectorSize<ArrayJ<Real>>::M;
  static const int nVar = DLA::VectorSize<ArrayQ<Real>>::M;

  // Matrix required to represent the transposed Jacobian of this functional
  template<class Z>
  using MatrixJ = DLA::MatrixS<nVar, nEq, Z>;

  IntegrandBoundary3D_LIP_Force( const PDE& pde, const std::vector<int>& BoundaryGroups )
    : BoundaryGroups_(BoundaryGroups), pde_(pde)
  {
  }

  std::size_t nBoundaryGroups() const { return BoundaryGroups_.size(); }
  std::size_t boundaryGroup(const int n) const { return BoundaryGroups_[n]; }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL>
  class Functor
  {
  public:
    typedef ElementXField<PhysD3, TopoDimTrace, TopologyTrace> ElementXFieldTrace;
    typedef ElementXField<PhysD3, TopoDimCell , TopologyL    > ElementXFieldL;

    typedef Element<ArrayQ<T>, TopoDimCell, TopologyL> ElementQFieldL;
    typedef Element<ArrayQ<T>, TopoDimTrace, TopologyTrace> ElementQFieldTrace;

    typedef typename ElementXFieldTrace::VectorX VectorX;

    typedef QuadraturePoint<TopoDimTrace> QuadPointTraceType;
    typedef QuadratureCellTracePoint<TopoDimCell> QuadPointCellType;

    Functor( const PDE& pde,
             const ElementXFieldTrace& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
             const ElementXFieldL& xfldElem,
             const ElementQFieldL& qfldElem ) :
               pde_(pde),
               xfldElemTrace_(xfldElemTrace), canonicalTrace_(canonicalTrace),
               xfldElem_(xfldElem), qfldElem_(qfldElem) {}

    // check whether integrand needs to be evaluated
    //bool needsEvaluation() const { return true; }

    typedef typename promote_Surreal<T,Tg>::type Ts;

    // trace integrand
    void operator()( const QuadPointTraceType& sRefTrace, ArrayJ<Ts>& integrand ) const
    {
      VectorX X;
      VectorX N;
      DLA::VectorS<3, ArrayQ<T> > gradq;
      QuadPointCellType sRefCell;

      TraceToCellRefCoord<TopologyTrace, TopoDimCell, TopologyL>::eval( canonicalTrace_, sRefTrace, sRefCell );

      xfldElem_.eval( sRefCell, X );
      xfldElem_.evalGradient( sRefCell, qfldElem_, gradq );

      // unit normal: points out of domain
      xfldElemTrace_.unitNormal( sRefTrace, N );

      DLA::VectorS<3,Tg> U = pde_.Up(X);

      // Compute the velocity vector
      //DLA::VectorS<3,T> V = U_ + gradq;
      //DLA::VectorS<3,T> V = {gradq[0], gradq[1], gradq[2]};

      //T U2 = dot(U_,U_);
      //T V2 = dot(V,V); // Local velocity
      T gradq2 = dot(gradq,gradq);

      // Force integrand
      integrand = (-0.5*gradq2 - dot(gradq,U))*N;
      //integrand = (0.5*U2 - 0.5*V2)*N;
    }

  protected:
    const PDE& pde_;
    const ElementXFieldTrace& xfldElemTrace_;
    const CanonicalTraceToCell canonicalTrace_;
    const ElementXFieldL& xfldElem_;
    const ElementQFieldL& qfldElem_;
  };

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL, class BCIntegrandBoundaryTrace>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL>
  integrand(const BCIntegrandBoundaryTrace& fcnBC,
            const ElementXField<PhysD3, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysD3, TopoDimCell , TopologyL    >& xfldElem,
            const Element<ArrayQ<T>   , TopoDimCell , TopologyL    >& qfldElem) const
  {
    return {pde_, xfldElemTrace, canonicalTrace,
                  xfldElem, qfldElem};
  }

  template<class T, class TopoDimTrace, class TopologyTrace,
                    class TopoDimCell, class TopologyL>
  Functor<T, TopoDimTrace, TopologyTrace,TopoDimCell, TopologyL>
  integrand(const ElementXField<PhysD3, TopoDimTrace, TopologyTrace>& xfldElemTrace, const CanonicalTraceToCell& canonicalTrace,
            const ElementXField<PhysD3, TopoDimCell , TopologyL    >& xfldElem,
            const Element<ArrayQ<T>   , TopoDimCell , TopologyL    >& qfldElem) const
  {
    return {pde_, xfldElemTrace, canonicalTrace,
                  xfldElem, qfldElem};
  }

protected:
  const std::vector<int> BoundaryGroups_;
  const PDE& pde_;
};


}

#endif //INTEGRANDBOUNDARY3D_LIP_FORCE_H
