// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCINCOMPRESSIBLEPOTENTIAL2D_SANSLG_H
#define BCINCOMPRESSIBLEPOTENTIAL2D_SANSLG_H

// 2-D Incompressible Potential PDE class
// based on weighted-norm functional


#include "Python/PyDict.h" //Python must be included first
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "PDEIncompressiblePotentialTwoField2D.h"
#include "BCPotential2D_SolutionFunction.h"
#include "pde/BCCategory.h"

#include <iostream>
#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// BC base class: 2-D two-field incompressible potential (IPTF)
//
// template parameters:
//   Tg                   solution DOF data type (e.g. double)
//   PDE                  PDE type
//   BCType               BC type (e.g. BCTypeNeumann)
//
// NOTE: see end of file for definitions of member functions coefficients/data
//----------------------------------------------------------------------------//

template <class BCType>
struct BCIncompressiblePotentialTwoField2DParams;

template <class BCType, class Tg>
class BCIncompressiblePotentialTwoField2D;


//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeDirichlet;
class BCTypeNeumann;
class BCTypeWall;
class BCTypeFarfieldVortex;
class BCTypeFunction;

// Define the vector of boundary conditions
template<class Tg>
using BCIncompressiblePotentialTwoField2DVector =
        boost::mpl::vector5< BCIncompressiblePotentialTwoField2D<BCTypeDirichlet, Tg>,
                             BCIncompressiblePotentialTwoField2D<BCTypeNeumann, Tg>,
                             BCIncompressiblePotentialTwoField2D<BCTypeWall, Tg>,
                             BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Tg>,
                             BCIncompressiblePotentialTwoField2D<BCTypeFunction, Tg>
                           >;


//----------------------------------------------------------------------------//
// Dirichlet BC:  phi = bcdata

template <>
struct BCIncompressiblePotentialTwoField2DParams<BCTypeDirichlet> : noncopyable
{
  const ParameterNumeric<Real> q{"q", NO_DEFAULT, NO_RANGE, "Dirichlet BC value"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Dirichlet"};
  struct Option
  {
    const DictOption Dirichlet{BCIncompressiblePotentialTwoField2DParams::BCName, BCIncompressiblePotentialTwoField2DParams::checkInputs};
  };

  static BCIncompressiblePotentialTwoField2DParams params;
};


template <class Tg>
class BCIncompressiblePotentialTwoField2D<BCTypeDirichlet, Tg>
  : public BCType< BCIncompressiblePotentialTwoField2D<BCTypeDirichlet, Tg> >
{
public:
  typedef BCCategory::IPTF_sansLG Category;
  typedef BCIncompressiblePotentialTwoField2DParams<BCTypeDirichlet> ParamsType;

  typedef PDEIncompressiblePotentialTwoField2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  explicit BCIncompressiblePotentialTwoField2D( const Real& bcdata ) : bcdata_(bcdata) {}
  BCIncompressiblePotentialTwoField2D( const PDE& pde, const PyDict& d ) : bcdata_(d.get(ParamsType::params.q)) {}
  virtual ~BCIncompressiblePotentialTwoField2D() {}

  BCIncompressiblePotentialTwoField2D( const BCIncompressiblePotentialTwoField2D& ) = delete;
  BCIncompressiblePotentialTwoField2D& operator=( const BCIncompressiblePotentialTwoField2D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> bcdata_;
};


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeDirichlet, Tg>::coefficients(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A(0,0) = 1;  A(0,1) = 0;
  A(1,0) = 0;  A(1,1) = 0;

  B = 0;
}


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeDirichlet, Tg>::data(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  bcdata(0) = bcdata_(0);
  bcdata(1) = 0;
}


//----------------------------------------------------------------------------//
// Neumann BC:  d(phi)/dn = bcdata

template <>
struct BCIncompressiblePotentialTwoField2DParams<BCTypeNeumann> : noncopyable
{
  const ParameterNumeric<Real> gradqn{"gradqn", NO_DEFAULT, NO_RANGE, "Neumann BC value"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Neumann"};
  struct Option
  {
    const DictOption Neumann{BCIncompressiblePotentialTwoField2DParams::BCName, BCIncompressiblePotentialTwoField2DParams::checkInputs};
  };

  static BCIncompressiblePotentialTwoField2DParams params;
};


template <class Tg>
class BCIncompressiblePotentialTwoField2D<BCTypeNeumann, Tg>
  : public BCType< BCIncompressiblePotentialTwoField2D<BCTypeNeumann, Tg> >
{
public:
  typedef BCCategory::IPTF_sansLG Category;
  typedef BCIncompressiblePotentialTwoField2DParams<BCTypeNeumann> ParamsType;

  typedef PDEIncompressiblePotentialTwoField2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  explicit BCIncompressiblePotentialTwoField2D( const Real& bcdata ) : bcdata_(bcdata) {}
  BCIncompressiblePotentialTwoField2D( const PDE& pde, const PyDict& d ) : bcdata_(d.get(ParamsType::params.gradqn)) {}
  virtual ~BCIncompressiblePotentialTwoField2D() {}

  BCIncompressiblePotentialTwoField2D( const BCIncompressiblePotentialTwoField2D& ) = delete;
  BCIncompressiblePotentialTwoField2D& operator=( const BCIncompressiblePotentialTwoField2D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> bcdata_;
};


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeNeumann, Tg>::coefficients(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;

  B(0,0) = 1;  B(0,1) = 0;
  B(1,0) = 0;  B(1,1) = 0;
}


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeNeumann, Tg>::data(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  bcdata(0) = bcdata_(0);
  bcdata(1) = 0;
}


//----------------------------------------------------------------------------//
// wall BC:  d(phi)/dn = - d(phi_uniform)/dn

template <>
struct BCIncompressiblePotentialTwoField2DParams<BCTypeWall> : noncopyable
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Wall"};
  struct Option
  {
    const DictOption Wall{BCIncompressiblePotentialTwoField2DParams::BCName, BCIncompressiblePotentialTwoField2DParams::checkInputs};
  };

  static BCIncompressiblePotentialTwoField2DParams params;
};


template <class Tg>
class BCIncompressiblePotentialTwoField2D<BCTypeWall, Tg>
  : public BCType< BCIncompressiblePotentialTwoField2D<BCTypeWall, Tg> >
{
public:
  typedef BCCategory::IPTF_sansLG Category;
  typedef BCIncompressiblePotentialTwoField2DParams<BCTypeWall> ParamsType;

  typedef PDEIncompressiblePotentialTwoField2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  explicit BCIncompressiblePotentialTwoField2D( const PDE& pde ) : u_(0), v_(0) { pde.freestream(u_, v_); }
  BCIncompressiblePotentialTwoField2D( const Real& u, const Real& v ) : u_(u), v_(v) {}
  BCIncompressiblePotentialTwoField2D( const PDE& pde, const PyDict& d ) : u_(0), v_(0) { pde.freestream(u_, v_); }
  virtual ~BCIncompressiblePotentialTwoField2D() {}

  BCIncompressiblePotentialTwoField2D( const BCIncompressiblePotentialTwoField2D& ) = delete;
  BCIncompressiblePotentialTwoField2D& operator=( const BCIncompressiblePotentialTwoField2D& ) = delete;

  // BC coefficients:  A phi + B ( -d(phi)/dn )
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real u_, v_;      // freestream velocity
};


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeWall, Tg>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;

  B(0,0) = 1;  B(0,1) = 0;
  B(1,0) = 0;  B(1,1) = 0;
}


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeWall, Tg>::data(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  bcdata(0) = -(nx*u_ + ny*v_);
  bcdata(1) = 0;
}


//----------------------------------------------------------------------------//
// Farfield BC w/ vortex:  phi = vortex
//
//  phi = circulation * theta / (2*PI)
//
//  where theta = arctan( (y - y0)/(x - x0) ); theta in [0, 2*PI)

template <>
struct BCIncompressiblePotentialTwoField2DParams<BCTypeFarfieldVortex> : noncopyable
{
  const ParameterNumeric<Real> x0{"x0", NO_DEFAULT, NO_RANGE, "Vortex centroid (x)"};
  const ParameterNumeric<Real> y0{"y0", NO_DEFAULT, NO_RANGE, "Vortex centroid (y)"};
  const ParameterNumeric<Real> circ{"circ", NO_DEFAULT, NO_RANGE, "Vortex circulation"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"FarfieldVortex"};
  struct Option
  {
    const DictOption FarfieldVortex{BCIncompressiblePotentialTwoField2DParams::BCName, BCIncompressiblePotentialTwoField2DParams::checkInputs};
  };

  static BCIncompressiblePotentialTwoField2DParams params;
};

template <class Tg>
class BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Tg>
  : public BCType< BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Tg> >
{
public:
  typedef BCCategory::IPTF_sansLG Category;
  typedef BCIncompressiblePotentialTwoField2DParams<BCTypeFarfieldVortex> ParamsType;

  typedef PDEIncompressiblePotentialTwoField2D PDE;

  typedef Tg Tglb;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  BCIncompressiblePotentialTwoField2D( const Real& x0, const Real& y0, const Real& circ ) : x0_(x0), y0_(y0), circ_(circ) {}
  BCIncompressiblePotentialTwoField2D( const PDE& pde, const PyDict& d ) :
    x0_(d.get(ParamsType::params.x0)),
    y0_(d.get(ParamsType::params.y0)),
    circ_(d.get(ParamsType::params.circ)) {}
  virtual ~BCIncompressiblePotentialTwoField2D() {}

#if 0
  BCIncompressiblePotentialTwoField2D( const BCIncompressiblePotentialTwoField2D& ) = delete;
  BCIncompressiblePotentialTwoField2D& operator=( const BCIncompressiblePotentialTwoField2D& ) = delete;
#else     // surreal jacobians require copy ctor
  BCIncompressiblePotentialTwoField2D( const BCIncompressiblePotentialTwoField2D& bc ) :
    x0_(bc.x0_), y0_(bc.y0_), circ_(bc.circ_) {}
  BCIncompressiblePotentialTwoField2D& operator=( const BCIncompressiblePotentialTwoField2D& bc )
  {
    x0_   = bc.x0_;
    y0_   = bc.y0_;
    circ_ = bc.circ_;
  }
#endif

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data:  vortex potential
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // unit-strength vortex potential
  Real unitVortexPotential( const Real& x, const Real& y ) const;

  void setCirculation( const Tg& circ )       { circ_ = circ; }
  void getCirculation(       Tg& circ ) const { circ  = circ_; }
  Tg getCirculation() const { return circ_; }

  //      BCIncompressiblePotentialTwoField2D& cast()       { return *this; }
  //const BCIncompressiblePotentialTwoField2D& cast() const { return *this; }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real x0_, y0_;      // vortex centroid
  Tg circ_;           // vortex strength/circulation
};


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Tg>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A(0,0) = 1;  A(0,1) = 0;
  A(1,0) = 0;  A(1,1) = 0;

  B = 0;
}


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Tg>::data(
    const Real& x, const Real& y, const Real& time, const Real&, const Real&,
    ArrayQ<T>& bcdata ) const
{
  Real th;

  th = atan2( y - y0_, x - x0_ );
  if (th < 0)  th += 2*PI;          // makes th in [0, 2*PI)

  bcdata(0) =  -circ_ * th / (2*PI);
  bcdata(1) = 0;
  //std::cout << x << " " << y << " " << bcdata(0) << std::endl;
}


template <class Tg>
Real
BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Tg>::unitVortexPotential(
    const Real& x, const Real& y ) const
{
  Real th;

  th = atan2( y - y0_, x - x0_ );
  if (th < 0)  th += 2*PI;          // makes th in [0, 2*PI)

  Real phiVortex = -th/(2*PI);
  return phiVortex;
}


template<class Tg>
void
BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex, Tg>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCIncompressiblePotentialTwoField2D<BCTypeFarfieldVortex>:" << std::endl;
  out << indent << "  (x0_, y0_) = (" << x0_ << ", " << y0_ << ")  circ = " << circ_ << std::endl;
}


//----------------------------------------------------------------------------//
// Solution BC:  u = uexact(x,y)
//
// A = 1; B = 0

template <>
struct BCIncompressiblePotentialTwoField2DParams<BCTypeFunction> : BCPotential2D_SolutionFunction_Params
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Function"};
  struct Option
  {
    const DictOption Function{BCIncompressiblePotentialTwoField2DParams::BCName, BCIncompressiblePotentialTwoField2DParams::checkInputs};
  };

  static BCIncompressiblePotentialTwoField2DParams params;
};


template <class Tg>
class BCIncompressiblePotentialTwoField2D<BCTypeFunction, Tg>
  : public BCType< BCIncompressiblePotentialTwoField2D<BCTypeFunction, Tg> >
{
public:
  typedef BCCategory::IPTF_sansLG Category;
  typedef BCIncompressiblePotentialTwoField2DParams<BCTypeFunction> ParamsType;

  typedef PDEIncompressiblePotentialTwoField2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;     // solution arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;   // matrices

  typedef std::shared_ptr< Function2DBase<ArrayQ<Real>> > Function_ptr;
  //typedef std::shared_ptr< Function2DBase<Real> > Function_ptr;

  explicit BCIncompressiblePotentialTwoField2D( const Function_ptr& uexact ) :
      uexact_(uexact) {}
  BCIncompressiblePotentialTwoField2D( const PDE& pde, const PyDict& d ) :
    uexact_( {BCPotential2D_SolutionFunction::getSolution(d), 0} ) {}
  virtual ~BCIncompressiblePotentialTwoField2D() {}

  BCIncompressiblePotentialTwoField2D( const BCIncompressiblePotentialTwoField2D& ) = delete;
  BCIncompressiblePotentialTwoField2D& operator=( const BCIncompressiblePotentialTwoField2D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
};


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeFunction, Tg>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A(0,0) = 1;  A(0,1) = 0;
  A(1,0) = 0;  A(1,1) = 0;

  B = 0;
}


template <class Tg>
template <class T>
inline void
BCIncompressiblePotentialTwoField2D<BCTypeFunction, Tg>::data(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
#if 0
  bcdata(0) = (*uexact_)(x, y, time);
  bcdata(1) = 0;
#else
  bcdata = (*uexact_)(x, y, time);
#endif
}


} //namespace SANS

#endif  // BCINCOMPRESSIBLEPOTENTIAL2D_SANSLG_H
