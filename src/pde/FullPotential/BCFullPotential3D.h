// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCFULLPOTENTIAL3D_H
#define BCFULLPOTENTIAL3D_H

// 3-D Full Potential BC class

#include "Python/PyDict.h" //Python must be included first
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "PDEFullPotential3D.h"
#include "SolutionFunction3D_LIP.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "pde/BCCategory.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC base class: 3-D Linearized Incompressible Potential
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   BCType               BC type (e.g. BCTypeOutflow)
//
// NOTE: see end of file for definitions of member functions coefficients/data
//----------------------------------------------------------------------------//

template <class BCType>
struct BCFullPotential3DParams;

template <class BCType, class Tg>
class BCFullPotential3D;

//----------------------------------------------------------------------------//
// Dirichlet BC:  phi = bcdata

class BCTypeInflow;

template <>
struct BCFullPotential3DParams<BCTypeInflow> : noncopyable
{
  const ParameterNumeric<Real> q{"q", NO_DEFAULT, NO_RANGE, "Dirichlet BC value"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Dirichlet"};
  struct Option
  {
    const DictOption Dirichlet{BCFullPotential3DParams::BCName, BCFullPotential3DParams::checkInputs};
  };

  static BCFullPotential3DParams params;
};

template <class Tg>
class BCFullPotential3D<BCTypeInflow, Tg>
  : public BCType< BCFullPotential3D<BCTypeInflow, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG_FP_HACK Category;
  typedef BCFullPotential3DParams<BCTypeInflow> ParamsType;

  typedef PDEFullPotential3D<Tg> PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  explicit BCFullPotential3D( const Real& bcdata ) : bcdata_({bcdata, 0}) {}
  BCFullPotential3D(const PDE& pde, const PyDict& d ) : bcdata_({d.get(ParamsType::params.q), 2}) {}
  virtual ~BCFullPotential3D() {}

  BCFullPotential3D( const BCFullPotential3D& ) = delete;
  BCFullPotential3D& operator=( const BCFullPotential3D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> bcdata_;
};

template <class Tg>
template <class T>
inline void
BCFullPotential3D<BCTypeInflow, Tg>::coefficients(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}

template <class Tg>
template <class T>
inline void
BCFullPotential3D<BCTypeInflow, Tg>::data(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}


//----------------------------------------------------------------------------//
// Neumann BC:  d(q)/dn = bcdata

class BCTypeOutflow;

template <>
struct BCFullPotential3DParams<BCTypeOutflow> : noncopyable
{
  const ParameterNumeric<Real> gradqn{"gradqn", NO_DEFAULT, NO_RANGE, "Neumann BC value"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Neumann"};
  struct Option
  {
    const DictOption Neumann{BCFullPotential3DParams::BCName, BCFullPotential3DParams::checkInputs};
  };

  static BCFullPotential3DParams params;
};

template <class Tg>
class BCFullPotential3D<BCTypeOutflow, Tg>
  : public BCType< BCFullPotential3D<BCTypeOutflow, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG_FP_HACK Category;
  typedef BCFullPotential3DParams<BCTypeOutflow> ParamsType;

  typedef PDEFullPotential3D<Tg> PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  explicit BCFullPotential3D() {}
  BCFullPotential3D(const PDE& pde, const PyDict& d ) {}

  virtual ~BCFullPotential3D() {}

  BCFullPotential3D( const BCFullPotential3D& ) = delete;
  BCFullPotential3D& operator=( const BCFullPotential3D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
};


template <class Tg>
template <class T>
inline void
BCFullPotential3D<BCTypeOutflow, Tg>::coefficients(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;
  B = 1;
}


template <class Tg>
template <class T>
inline void
BCFullPotential3D<BCTypeOutflow, Tg>::data(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<T>& bcdata ) const
{
  bcdata = 0;
}


//----------------------------------------------------------------------------//
// wall BC:  d(q)/dn = 0

class BCTypeWall;

template <>
struct BCFullPotential3DParams<BCTypeWall> : noncopyable
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Wall"};
  struct Option
  {
    const DictOption Wall{BCFullPotential3DParams::BCName, BCFullPotential3DParams::checkInputs};
  };

  static BCFullPotential3DParams params;
};

template <class Tg>
class BCFullPotential3D<BCTypeWall, Tg>
  : public BCType< BCFullPotential3D<BCTypeWall, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG_FP_HACK Category;
  typedef BCFullPotential3DParams<BCTypeWall> ParamsType;

  typedef PDEFullPotential3D<Tg> PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = 2;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  // cppcheck-suppress noExplicitConstructor
  explicit BCFullPotential3D( const PDE& pde ) : U_(pde.freestream()) {}
  BCFullPotential3D(const PDE& pde, const PyDict& d ) : U_(pde.freestream()) {}
  virtual ~BCFullPotential3D() {}

  BCFullPotential3D( const BCFullPotential3D& ) = delete;
  BCFullPotential3D& operator=( const BCFullPotential3D& ) = delete;

  // BC coefficients:  A phi + B ( -d(phi)/dn )
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const DLA::VectorS<3,Tg>& U_;      // freestream velocity
};


template <class Tg>
template <class T>
inline void
BCFullPotential3D<BCTypeWall, Tg>::coefficients(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;
  B = 1;
}


template <class Tg>
template <class T>
inline void
BCFullPotential3D<BCTypeWall, Tg>::data(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<T>& bcdata ) const
{
  bcdata[0] = -(nx*U_[0] + ny*U_[1] + nz*U_[2]);
}

template<class Tg>
using BCFullPotential3DVector =
        boost::mpl::vector3< BCFullPotential3D<BCTypeWall, Tg>,
                             BCFullPotential3D<BCTypeInflow, Tg>,
                             BCFullPotential3D<BCTypeOutflow, Tg>
                           >;


#if 0
//----------------------------------------------------------------------------//
// Farfield BC w/ vortex:  phi = vortex
//
//  phi = circulation * theta / (2*PI)
//
//  where theta = arctan( (y - y0)/(x - x0) ); theta in [0, 2*PI)

class BCTypeFarfieldVortex;

template <>
class BCFullPotential3D<BCTypeFarfieldVortex>
  : public BCType< BCFullPotential3D<BCTypeFarfieldVortex> >
{
public:
  typedef PDEFullPotential3D PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = PDEFullPotential3D::ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = PDEFullPotential3D::MatrixQ<T>;  // matrices

  BC( const Real& x0, const Real& y0 ) : x0_(x0), y0_(y0) {}
  ~BC() {}

  BC& operator=( const BC& );

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data:  vortex
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real x0_, y0_;      // vortex centroid
};


template <class T>
inline void
BCFullPotential3D<BCTypeFarfieldVortex>::coefficients(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}


template <class T>
inline void
BCFullPotential3D<BCTypeFarfieldVortex>::data(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const
{
  T circ = globalVar[0];
  Real th;

  th = atan2( y - y0_, x - x0_ );
  if (th < 0)  th += 2*PI;          // makes th in [0, 2*PI)

  bcdata =  -circ * th / (2*PI);
  //std::cout << x << " " << y << " " << bcdata(0) << std::endl;
}
#endif

//----------------------------------------------------------------------------//
// Solution BC:  u = uexact(x,y)
//
// A = 1; B = 0

template<class >
class BCTypeFunction;

template <class SolutionFunction, class Tg>
class BCFullPotential3D< BCTypeFunction<SolutionFunction>, Tg >
  : public BCType< BCFullPotential3D< BCTypeFunction<SolutionFunction>, Tg > >
{
public:
  typedef BCCategory::LinearScalar_sansLG_FP_HACK Category;

  typedef PDEFullPotential3D<Tg> PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  explicit BCFullPotential3D( const SolutionFunction3D<N>* uexact ) :
      uexact_(uexact) {}
  virtual ~BCFullPotential3D() {}

  BCFullPotential3D( const BCFullPotential3D& ) = delete;
  BCFullPotential3D& operator=( const BCFullPotential3D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const SolutionFunction3D<N>* uexact_;
};


template<class SolutionFunction, class Tg>
template <class T>
inline void
BCFullPotential3D<BCTypeFunction<SolutionFunction>, Tg >::coefficients(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}


template<class SolutionFunction, class Tg>
template <class T>
inline void
BCFullPotential3D<BCTypeFunction<SolutionFunction>, Tg >::data(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<T>& bcdata ) const
{
  bcdata = (*uexact_)(x, y, z, time);
}


template<class SolutionFunction, class Tg>
void
BCFullPotential3D<BCTypeFunction<SolutionFunction>, Tg >::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCFullPotential3D<BCTypeFunction<SolutionFunction> >:" << std::endl;
}

} //namespace SANS

#endif  // BCFULLPOTENTIAL3D_H
