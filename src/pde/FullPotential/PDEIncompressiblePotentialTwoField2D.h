// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEINCOMPRESSIBLEPOTENTIALTWOFIELD2D_H
#define PDEINCOMPRESSIBLEPOTENTIALTWOFIELD2D_H

// 2-D Incompressible Potential PDE class
// based on weighted-norm functional

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"

#include <iostream>
#include <string>
#include <memory>

#undef USE_IPTF_NORM

namespace SANS
{

template <int N>
class SolutionFunction2D;


//----------------------------------------------------------------------------//
// PDE class: 2-D two-field incompressible potential (IPTF)
//
//   phifull = (a*x + b*y) + phi(x,y)
//
// member functions:
//   .hasFluxAdvectiveTime     T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective        T/F: PDE has advective/inviscid flux term
//   .hasFluxViscous          T/F: PDE has viscous flux term
//
//   .fluxConservative        unsteady conservative fluxes: U(Q)
//   .fluxAdvective           advective/inviscid fluxes: F(Q)
//   .fluxViscous             viscous fluxes: Fv(Q, QX)
//   .diffusionViscous        viscous diffusion coefficient: d(Fv)/d(UX)
//   .isValidState            T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom              set from primitive variable array
//----------------------------------------------------------------------------//

class PDEIncompressiblePotentialTwoField2D
{
public:
  typedef PhysD2 PhysDim;

  static const int D = PhysDim::D;            // physical dimensions
  static const int N = 2;                     // total solution variables

  template <class T>
  using ArrayQ = DLA::VectorS<N,T>;           // solution/residual arrays

  template <class T>
  using MatrixQ = DLA::MatrixS<N,N,T>;        // matrices

  template <class T>
  using ArrayD = DLA::VectorD<T>;             // residual norms

  typedef std::shared_ptr< SolutionFunction2D<1> > SolutionFunction2D_ptr;

#ifdef USE_IPTF_NORM
  PDEIncompressiblePotentialTwoField2D(
      const Real& a, const Real& b,
      const Real& x0A, const Real& y0A, const Real& coefA, const Real& maskA, const int& normA,
      const Real& x0D, const Real& y0D, const Real& coefD, const Real& maskD, const int& normD ) :
      a_(a), b_(b),
      x0A_(x0A), y0A_(y0A), coefA_(coefA), maskA_(maskA), normA_(normA),
      x0D_(x0D), y0D_(y0D), coefD_(coefD), maskD_(maskD), normD_(normD),
     sln_(NULL) {}
#else
  PDEIncompressiblePotentialTwoField2D(
      const Real& a, const Real& b,
      const Real& x0A, const Real& y0A, const Real& coefA, const Real& maskA,
      const Real& x0D, const Real& y0D, const Real& coefD, const Real& maskD ) :
      a_(a), b_(b),
      x0A_(x0A), y0A_(y0A), coefA_(coefA), maskA_(maskA),
      x0D_(x0D), y0D_(y0D), coefD_(coefD), maskD_(maskD),
     sln_(NULL) {}
#endif
  #if 0
  PDEIncompressiblePotentialTwoField2D( const Real& a, const Real& b, const SolutionFunction2D_ptr& sln ) :
      a_(a), b_(b), sln_(sln) {}
  #endif
  ~PDEIncompressiblePotentialTwoField2D() {}

  PDEIncompressiblePotentialTwoField2D( const PDEIncompressiblePotentialTwoField2D& ) = delete;
  PDEIncompressiblePotentialTwoField2D& operator=( const PDEIncompressiblePotentialTwoField2D& ) = delete;

  // flux components
  bool hasFluxAdvectiveTime() const { return false; }
  bool hasFluxAdvective() const { return false; }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return false; }
  bool hasSourceTrace() const { return false; }
  bool hasForcingFunction() const { return (sln_ != NULL); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const { return false; }

  // freestream velocity
  void freestream( Real& a, Real& b ) const { a = a_; b = b_; }

  // unsteady conservative flux: U(Q)
  template<class Tq, class Tf>
  void fluxConservative( const ArrayQ<Tq>& q, ArrayQ<Tf>& uCons ) const {}

  // advective flux: F(Q)
  template<class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const {}
  template <class Tq, class Tf>
  void fluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const {}

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tq, class Tf>
  void jacobianFluxAdvective(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, const Real& nx, const Real& ny,
      MatrixQ<Tf>& a ) const {}

  // strong form advective fluxes
  template <class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const {}

  // viscous flux: Fv(Q, QX)
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const;
  template <class Tq, class Tg, class Tf>
  void fluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const;

  // viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tq>& q, MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const {}

  // strong form viscous fluxes
  template <class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

  // solution-dependent source: S(X, D, Q, QX)
  template <class Tq, class Tg, class Ts>
  void source(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const {}

  // dual-consistent source
  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL,
      const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const {}

  // jacobian of solution-dependent source wrt conservation variables: d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const {}

  // jacobian of solution-dependent source wrt conservation variables: d(S)/d(gradU)
  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const {}

  // right-hand-side forcing function
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const Real& dx, const Real& dy, const ArrayQ<T>& q, Real& speed ) const {}

  // characteristic speed
  template <class Tq, class Tc>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q, Tc& speed ) const {}

  // is state physically valid
  template <class T>
  bool isValidState( const ArrayQ<T>& q ) const { return true; }

  // update fraction needed for physically valid state
  template <class T>
  void updateFraction(
      const Real& x, const Real& y, const Real& time, const ArrayQ<T>& q, const ArrayQ<T>& dq,
      Real maxChangeFraction, Real& updateFraction ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, ArrayD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradients in the solution variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, ArrayD<T>& rsdAuxOut ) const;

  // interpret residuals at the boundary (should forward to BCs)
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, ArrayD<T>& rsdBCOut ) const;

  // how many residual equations are we dealing with
  int nMonitor() const;

  // Gaussian weight functions
  Real gaussianA( const Real& x, const Real& y ) const { return maskA_*exp( -coefA_*((x - x0A_)*(x - x0A_) + (y - y0A_)*(y - y0A_)) ); }
  Real gaussianD( const Real& x, const Real& y ) const { return maskD_*exp( -coefD_*((x - x0D_)*(x - x0D_) + (y - y0D_)*(y - y0D_)) ); }
  //Real gaussianA( const Real& x, const Real& y ) const { return 1; }
  //Real gaussianD( const Real& x, const Real& y ) const { return 0; }

#ifdef USE_IPTF_NORM
  // Gaussian-weight norms
  int normA() const { return normA_; }
  int normD() const { return normD_; }
#endif

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real a_, b_;                      // freestream potential: a*x + b*y

  Real x0A_, y0A_;                  // airfoil Gaussian weight-function centroid
  Real coefA_;                      // airfoil Gaussian weight-function exponent coefficient
  Real maskA_;                      // airfoil Gaussian weight-function multiplier/mask
#ifdef USE_IPTF_NORM
  int normA_;                       // airfoil Gaussian weight-function norm (e.g. normA=2 -> L2)
#endif

  Real x0D_, y0D_;                  // domain Gaussian weight-function centroid
  Real coefD_;                      // domain Gaussian weight-function exponent coefficient
  Real maskD_;                      // domain Gaussian weight-function multiplier/mask
#ifdef USE_IPTF_NORM
  int normD_;                       // domain Gaussian weight-function norm
#endif

  SolutionFunction2D_ptr sln_;      // solution for RHS forcing function
};


// viscous flux
template <class Tq, class Tg, class Tf>
inline void
PDEIncompressiblePotentialTwoField2D::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
{
#ifdef USE_IPTF_NORM
  Tg   gradphiSq = qx(0)*qx(0) + qy(0)*qy(0);
  Tg   wghtD = gaussianD( x, y ) * pow( gradphiSq, normD_/2 - 1);
#else
  Real wghtD = gaussianD( x, y );
#endif

  f(0) -= qx(1) + wghtD*qx(0);
  g(0) -= qy(1) + wghtD*qy(0);

  f(1) -= qx(0);
  g(1) -= qy(0);
}


// viscous flux: normal flux with left and right states
template <class Tq, class Tg, class Tf>
inline void
PDEIncompressiblePotentialTwoField2D::fluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& f ) const
{
  Real wghtD = gaussianD( x, y );

#ifdef USE_IPTF_NORM
  Tg   wghtDL = wghtD ;//* pow( qxL(0)*qxL(0) + qyL(0)*qyL(0), normD_/2 - 1);
  Tg   wghtDR = wghtD ;//* pow( qxR(0)*qxR(0) + qyR(0)*qyR(0), normD_/2 - 1);

  f(0) -= 0.5*( nx*((qxL(1) + qxR(1)) + wghtDL*qxL(0) + wghtDR*qxR(0))
              + ny*((qyL(1) + qyR(1)) + wghtDL*qyL(0) + wghtDR*qyR(0)) );
  f(1) -= 0.5*( nx* (qxL(0) + qxR(0))
              + ny* (qyL(0) + qyR(0)) );
#else
  f(0) -= 0.5*( nx*((qxL(1) + qxR(1)) + wghtD*(qxL(0) + qxR(0)))
              + ny*((qyL(1) + qyR(1)) + wghtD*(qyL(0) + qyR(0))) );
  f(1) -= 0.5*( nx* (qxL(0) + qxR(0))
              + ny* (qyL(0) + qyR(0)) );
#endif
}


// viscous diffusion matrix: d(Fv)/d(UX)
template <class Tq, class Tg, class Tk>
inline void
PDEIncompressiblePotentialTwoField2D::diffusionViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
#ifdef USE_IPTF_NORM
  Tg   wghtD = gaussianD( x, y ) ;//* pow( qx(0)*qx(0) + qy(0)*qy(0), normD_/2 - 1);
#else
  Real wghtD = gaussianD( x, y );
#endif

  kxx(0,0) += wghtD;
  kxx(0,1) += 1;
  kxx(1,0) += 1;

  kyy(0,0) += wghtD;
  kyy(0,1) += 1;
  kyy(1,0) += 1;
}


// strong form of viscous flux: d(Fv)/d(X)
template <class Tq, class Tg, class Th, class Tf>
inline void
PDEIncompressiblePotentialTwoField2D::strongFluxViscous(
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
#ifdef USE_IPTF_NORM
  Tg   wghtD = gaussianD( x, y ) ;//* pow( qx(0)*qx(0) + qy(0)*qy(0), normD_/2 - 1);
#else
  Real wghtD = gaussianD( x, y );
#endif

  strongPDE(0) += -(qxx(1) + qyy(1)) - wghtD*(qxx(0) + qyy(0)) - 2*coefD_*((x - x0D_)*qx(0) + (y - y0D_)*qy(0));
  strongPDE(1) += -(qxx(0) + qyy(0));

#ifdef USE_IPTF_NORM
  SANS_DEVELOPER_EXCEPTION( "strong form not completely implemented; missing grad(wghtD) terms" );
#endif
}


// right-hand-side forcing function
inline void
PDEIncompressiblePotentialTwoField2D::forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& forcing ) const
{
  SANS_DEVELOPER_EXCEPTION( "IPTF: RHS forcing function can only be used with a provided solution" );

#if 0
  ArrayQ<Real> q, qx, qy, qxx, qxy, qyy;
  (*sln_)( x, y, q, qx, qy, qxx, qxy, qyy );

  strongFluxViscous( x, y, q, qx, qy, qxx, qxy, qyy, forcing );
#endif
}


// set from primitive variable array
template <class T>
inline void
PDEIncompressiblePotentialTwoField2D::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  SANS_ASSERT(nn >= N);
  q(0) = data[0];
  q(1) = data[1];
}


// interpret residuals of the solution variable
template <class T>
inline void
PDEIncompressiblePotentialTwoField2D::interpResidVariable( const ArrayQ<T>& rsdPDEIn, ArrayD<T>& rsdPDEOut ) const
{
  rsdPDEOut[0] = sqrt( rsdPDEIn(0)*rsdPDEIn(0) );
  rsdPDEOut[1] = sqrt( rsdPDEIn(1)*rsdPDEIn(1) );
}


// interpret residuals of the gradients in the solution variable
template <class T>
inline void
PDEIncompressiblePotentialTwoField2D::interpResidGradient( const ArrayQ<T>& rsdAuxIn, ArrayD<T>& rsdAuxOut ) const
{
  rsdAuxOut[0] = sqrt( rsdAuxIn(0)*rsdAuxIn(0) );
  rsdAuxOut[1] = sqrt( rsdAuxIn(1)*rsdAuxIn(1) );
}


// interpret residuals at the boundary (should forward to BCs)
template <class T>
inline void
PDEIncompressiblePotentialTwoField2D::interpResidBC( const ArrayQ<T>& rsdBCIn, ArrayD<T>& rsdBCOut ) const
{
  rsdBCOut[0] = sqrt( rsdBCIn(0)*rsdBCIn(0) );
}


// how many residual equations are we dealing with
inline int
PDEIncompressiblePotentialTwoField2D::nMonitor() const
{
  return 2;
}

} //namespace SANS

#endif  // PDEINCOMPRESSIBLEPOTENTIALTWOFIELD2D_H
