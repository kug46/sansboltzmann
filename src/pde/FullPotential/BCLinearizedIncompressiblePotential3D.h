// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCLINEARIZEDINCOMPRESSIBLEPOTENTIAL3D_H
#define BCLINEARIZEDINCOMPRESSIBLEPOTENTIAL3D_H

// 3-D Linearized Incompressible Potential BC class

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/tools/dot.h"
#include "LinearAlgebra/DenseLinAlg/tools/cross.h"

#include "PDELinearizedIncompressiblePotential3D.h"

#include "SolutionFunction3D_LIP.h"

#include "pde/BCCategory.h"

#include "Surreal/SurrealS.h"

#include <iostream>
#include <string>

namespace SANS
{

//----------------------------------------------------------------------------//
// BC base class: 3-D Linearized Incompressible Potential
//
// template parameters:
//   T                    solution DOF data type (e.g. double)
//   BCType               BC type (e.g. BCTypeNeumann)
//
// NOTE: see end of file for definitions of member functions coefficients/data
//----------------------------------------------------------------------------//

template <class BCType>
struct BCLinearizedIncompressiblePotential3DParams;

template <class BCType, class Tg>
class BCLinearizedIncompressiblePotential3D;

//----------------------------------------------------------------------------//
// Dirichlet BC:  phi = bcdata

class BCTypeDirichlet;

template <>
struct BCLinearizedIncompressiblePotential3DParams<BCTypeDirichlet> : noncopyable
{
  const ParameterNumeric<Real> q{"q", NO_DEFAULT, NO_RANGE, "Dirichlet BC value"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Dirichlet"};
  struct Option
  {
    const DictOption Dirichlet{BCLinearizedIncompressiblePotential3DParams::BCName, BCLinearizedIncompressiblePotential3DParams::checkInputs};
  };

  static BCLinearizedIncompressiblePotential3DParams params;
};

template <class Tg>
class BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Tg>
  : public BCType< BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCLinearizedIncompressiblePotential3DParams<BCTypeDirichlet> ParamsType;

  typedef PDELinearizedIncompressiblePotential3D<Tg> PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class Z>
  using ArrayQ = typename PDE::template ArrayQ<Z>;    // solution/residual arrays

  template <class Z>
  using MatrixQ = typename PDE::template MatrixQ<Z>;  // matrices

  explicit BCLinearizedIncompressiblePotential3D( const Real& bcdata ) : bcdata_(bcdata) {}
  BCLinearizedIncompressiblePotential3D(const PDE& pde, const PyDict& d ) : bcdata_(d.get(ParamsType::params.q)) {}
  virtual ~BCLinearizedIncompressiblePotential3D() {}

  BCLinearizedIncompressiblePotential3D( const BCLinearizedIncompressiblePotential3D& ) = delete;
  BCLinearizedIncompressiblePotential3D& operator=( const BCLinearizedIncompressiblePotential3D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    A = 1;
    B = 0;
  }

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const
  {
    bcdata = bcdata_;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> bcdata_;
};

//----------------------------------------------------------------------------//
// Neumann BC:  d(phi)/dn = bcdata

class BCTypeNeumann;

template <>
struct BCLinearizedIncompressiblePotential3DParams<BCTypeNeumann> : noncopyable
{
  const ParameterNumeric<Real> gradqn{"gradqn", NO_DEFAULT, NO_RANGE, "Neumann BC value"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Neumann"};
  struct Option
  {
    const DictOption Neumann{BCLinearizedIncompressiblePotential3DParams::BCName, BCLinearizedIncompressiblePotential3DParams::checkInputs};
  };

  static BCLinearizedIncompressiblePotential3DParams params;
};

template <class Tg>
class BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Tg>
  : public BCType< BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCLinearizedIncompressiblePotential3DParams<BCTypeNeumann> ParamsType;

  typedef PDELinearizedIncompressiblePotential3D<Tg> PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  explicit BCLinearizedIncompressiblePotential3D( const Real& bcdata ) : bcdata_(bcdata) {}
  BCLinearizedIncompressiblePotential3D(const PDE& pde, const PyDict& d ) : bcdata_(d.get(ParamsType::params.gradqn)) {}

  virtual ~BCLinearizedIncompressiblePotential3D() {}

  BCLinearizedIncompressiblePotential3D( const BCLinearizedIncompressiblePotential3D& ) = delete;
  BCLinearizedIncompressiblePotential3D& operator=( const BCLinearizedIncompressiblePotential3D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    A = 0;
    B = 1;
  }

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const
  {
    bcdata = bcdata_;
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> bcdata_;
};


//----------------------------------------------------------------------------//
// wall BC:  d(phi)/dn = - d(phi_uniform)/dn

class BCTypeWall;

template <>
struct BCLinearizedIncompressiblePotential3DParams<BCTypeWall> : noncopyable
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Wall"};
  struct Option
  {
    const DictOption Wall{BCLinearizedIncompressiblePotential3DParams::BCName, BCLinearizedIncompressiblePotential3DParams::checkInputs};
  };

  static BCLinearizedIncompressiblePotential3DParams params;
};

template <class Tg>
class BCLinearizedIncompressiblePotential3D<BCTypeWall, Tg>
  : public BCType< BCLinearizedIncompressiblePotential3D<BCTypeWall, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCLinearizedIncompressiblePotential3DParams<BCTypeWall> ParamsType;

  typedef PDELinearizedIncompressiblePotential3D<Tg> PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  // cppcheck-suppress noExplicitConstructor
  BCLinearizedIncompressiblePotential3D( const PDE& pde ) : pde_(pde) {}
  BCLinearizedIncompressiblePotential3D(const PDE& pde, const PyDict& d ) : pde_(pde) {}
  virtual ~BCLinearizedIncompressiblePotential3D() {}

  BCLinearizedIncompressiblePotential3D( const BCLinearizedIncompressiblePotential3D& ) = delete;
  BCLinearizedIncompressiblePotential3D& operator=( const BCLinearizedIncompressiblePotential3D& ) = delete;

  // BC coefficients:  A phi + B ( -d(phi)/dn )
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    A = 0;
    B = 1;
  }

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const
  {
    DLA::VectorS<3,Tg> Up = pde_.Up(x, y, z);

    bcdata = -(nx*Up[0] + ny*Up[1] + nz*Up[2]);
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;
protected:
  const PDE& pde_;
};


//----------------------------------------------------------------------------//
// control surface BC:  d(phi)/dn = - d(phi_uniform)/dn where n is rotated by the control surface deflection

class BCTypeControl;

template <>
struct BCLinearizedIncompressiblePotential3DParams<BCTypeControl> : noncopyable
{
  const ParameterNumericList<Real> hinge  {"hinge"  , EMPTY_LIST, NO_RANGE, "Control Surface Hinge Vector"};
  const ParameterNumeric<Real>     deflect{"deflect",          0, NO_RANGE, "Control Surface deflection in degrees"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"ControlSurface"};
  struct Option
  {
    const DictOption ControlSurface{BCLinearizedIncompressiblePotential3DParams::BCName, BCLinearizedIncompressiblePotential3DParams::checkInputs};
  };

  static BCLinearizedIncompressiblePotential3DParams params;
};

template <class Tg>
class BCLinearizedIncompressiblePotential3D<BCTypeControl, Tg>
  : public BCType< BCLinearizedIncompressiblePotential3D<BCTypeControl, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCLinearizedIncompressiblePotential3DParams<BCTypeControl> ParamsType;

  typedef PDELinearizedIncompressiblePotential3D<Tg> PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  // cppcheck-suppress noExplicitConstructor
  BCLinearizedIncompressiblePotential3D( const PDE& pde, const DLA::VectorS<3,Real> hinge, const Tg theta ) :
    pde_(pde), hinge_(hinge), theta_(theta)
  {
    // normalize the hinge vector and convert to degrees
    hinge_ /= dot(hinge_, hinge_);
    theta_ *= PI/180.;
  }
  BCLinearizedIncompressiblePotential3D( const PDE& pde, const PyDict& d ) :
    pde_(pde),
    hinge_(d.get(ParamsType::params.hinge)),
    theta_(d.get(ParamsType::params.deflect))
  {
    // normalize the hinge vector and convert to degrees
    hinge_ /= dot(hinge_, hinge_);
    theta_ *= PI/180.;
  }

  virtual ~BCLinearizedIncompressiblePotential3D() {}

  BCLinearizedIncompressiblePotential3D( const BCLinearizedIncompressiblePotential3D& ) = delete;
  BCLinearizedIncompressiblePotential3D& operator=( const BCLinearizedIncompressiblePotential3D& ) = delete;

  // BC coefficients:  A phi + B ( -d(phi)/dn )
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    A = 0;
    B = 1;
  }

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const
  {
    DLA::VectorS<3,Real> n = {nx, ny, nz};
    DLA::VectorS<3,Real> hn = dot(n, hinge_)*hinge_; // parallel component
    DLA::VectorS<3,Real> ht = n - hn;                // perpendicular component
    DLA::VectorS<3,Real> w = cross(hinge_, ht);      // perpendicular to both hinge and n

    Real htn = sqrt(dot(ht,ht));

    Tg x1 = cos(theta_);
    Tg x2 = sin(theta_)/sqrt(dot(w,w));

    DLA::VectorS<3,Tg> dn = hn + x1*ht + htn*x2*w;

    DLA::VectorS<3,Tg> Up = pde_.Up(x, y, z);

    bcdata = -(dn[0]*Up[0] + dn[1]*Up[1] + dn[2]*Up[2]);
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const PDE& pde_;
  DLA::VectorS<3,Real> hinge_; // control surface hinge vector
  Tg theta_;                   // control surface angle deflection in radians
};


template<class Tg>
using BCLinearizedIncompressiblePotential3DVector =
        boost::mpl::vector4< BCLinearizedIncompressiblePotential3D<BCTypeWall, Tg>,
                             BCLinearizedIncompressiblePotential3D<BCTypeDirichlet, Tg>,
                             BCLinearizedIncompressiblePotential3D<BCTypeNeumann, Tg>,
                             BCLinearizedIncompressiblePotential3D<BCTypeControl, Tg>
                           >;


#if 0
//----------------------------------------------------------------------------//
// Farfield BC w/ vortex:  phi = vortex
//
//  phi = circulation * theta / (2*PI)
//
//  where theta = arctan( (y - y0)/(x - x0) ); theta in [0, 2*PI)

class BCTypeFarfieldVortex;

template <>
class BCLinearizedIncompressiblePotential3D<BCTypeFarfieldVortex>
  : public BCType< BCLinearizedIncompressiblePotential3D<BCTypeFarfieldVortex> >
{
public:
  typedef PDELinearizedIncompressiblePotential3D PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = PDELinearizedIncompressiblePotential3D::ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = PDELinearizedIncompressiblePotential3D::MatrixQ<T>;  // matrices

  BC( const Real& x0, const Real& y0 ) : x0_(x0), y0_(y0) {}
  ~BC() {}

  BC& operator=( const BC& );

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data:  vortex
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real x0_, y0_;      // vortex centroid
};


template <class T>
inline void
BCLinearizedIncompressiblePotential3D<BCTypeFarfieldVortex>::coefficients(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}


template <class T>
inline void
BCLinearizedIncompressiblePotential3D<BCTypeFarfieldVortex>::data(
    const Real& x, const Real& y, const Real& z, const Real& time,
    const Real& nx, const Real& ny, const Real& nz,
    const SANS::DLA::VectorD<T>& globalVar, ArrayQ<T>& bcdata ) const
{
  T circ = globalVar[0];
  Real th;

  th = atan2( y - y0_, x - x0_ );
  if (th < 0)  th += 2*PI;          // makes th in [0, 2*PI)

  bcdata =  -circ * th / (2*PI);
  //std::cout << x << " " << y << " " << bcdata(0) << std::endl;
}
#endif

//----------------------------------------------------------------------------//
// Solution BC:  u = uexact(x,y)
//
// A = 1; B = 0

template<class >
class BCTypeFunction;

template <class SolutionFunction, class Tg>
class BCLinearizedIncompressiblePotential3D< BCTypeFunction<SolutionFunction>, Tg >
  : public BCType< BCLinearizedIncompressiblePotential3D< BCTypeFunction<SolutionFunction>, Tg > >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;

  typedef PDELinearizedIncompressiblePotential3D<Tg> PDE;

  typedef PhysD3 PhysDim;
  static const int D = 3;                     // physical dimensions
  static const int N = 1;                     // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = typename PDE::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDE::template MatrixQ<T>;  // matrices

  explicit BCLinearizedIncompressiblePotential3D( const SolutionFunction3D<N>* uexact ) :
      uexact_(uexact) {}
  virtual ~BCLinearizedIncompressiblePotential3D() {}

  BCLinearizedIncompressiblePotential3D( const BCLinearizedIncompressiblePotential3D& ) = delete;
  BCLinearizedIncompressiblePotential3D& operator=( const BCLinearizedIncompressiblePotential3D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<T>& A, MatrixQ<T>& B ) const
  {
    A = 1;
    B = 0;
  }

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& nx, const Real& ny, const Real& nz,
      ArrayQ<T>& bcdata ) const
  {
    bcdata = (*uexact_)(x, y, z, time);
  }

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const Real& nz, const ArrayQ<Real>& q ) const { return true; }

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const SolutionFunction3D<N>* uexact_;
};


template <class SolutionFunction, class Tg>
void
BCLinearizedIncompressiblePotential3D<BCTypeFunction<SolutionFunction>, Tg >::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "BCLinearizedIncompressiblePotential3D<BCTypeFunction<SolutionFunction>, Real >:" << std::endl;
}

} //namespace SANS

#endif  // BCLINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_H
