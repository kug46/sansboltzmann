// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SOLUTIONFUNCTION_POTENTIAL2D_H
#define SOLUTIONFUNCTION_POTENTIAL2D_H

// 2-D Linearized Incompressible Potential PDE: exact and MMS solutions

#include <cmath>
#include <complex>

#include "Python/PyDict.h"
#include "Python/Parameter.h"

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"
#include "pde/AnalyticFunction/Function2D.h"


namespace SANS
{

struct PotentialFunctionTraits
{
  template <class T>
  using ArrayQ = T;       // solution arrays

  template <class T>
  using MatrixQ = T;      // solution matrices
};


//----------------------------------------------------------------------------//
// solution: const

#if 0
struct SolutionFunction_Potential2D_Const_Params : noncopyable
{
  const ParameterNumeric<Real> constant{"constant", NO_DEFAULT, NO_RANGE, "Constant value"};

  static void checkInputs(PyDict d);

  static SolutionFunction_Potential2D_Const_Params params;
};
#endif


class SolutionFunction_Potential2D_Const :
    public Function2DVirtualInterface<SolutionFunction_Potential2D_Const, PotentialFunctionTraits>
{
public:
  template <class T>
  using ArrayQ = PotentialFunctionTraits::ArrayQ<T>;    // solution arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> constant{"constant", NO_DEFAULT, NO_RANGE, "Constant value"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit SolutionFunction_Potential2D_Const( const Real& a0 ) : a0_(a0) {}
  explicit SolutionFunction_Potential2D_Const( const PyDict& d ) : a0_(d.get(ParamsType::params.constant)) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
    { return a0_; }

  template <class T>
  void operator()( const T& x, const T& y, const T& time,
                   ArrayQ<T>& q, ArrayQ<T>& qx, ArrayQ<T>& qy,
                   ArrayQ<T>& qxx, ArrayQ<T>& qxy, ArrayQ<T>& qyy ) const
    { q = a0_; qx = 0; qy = 0; qxx = 0; qxy = 0; qyy = 0; }

private:
  Real a0_;
};


//----------------------------------------------------------------------------//
// solution: linear

class SolutionFunction_Potential2D_Linear :
    public Function2DVirtualInterface<SolutionFunction_Potential2D_Linear, PotentialFunctionTraits>
{
public:
  template <class T>
  using ArrayQ = PotentialFunctionTraits::ArrayQ<T>;    // solution arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> constant{"constant", NO_DEFAULT, NO_RANGE, "Constant value"};
    const ParameterNumeric<Real> gradx{"gradx", NO_DEFAULT, NO_RANGE, "Gradient in x"};
    const ParameterNumeric<Real> grady{"grady", NO_DEFAULT, NO_RANGE, "Gradient in y"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  SolutionFunction_Potential2D_Linear( const Real& a0, const Real& ax, const Real& ay ) : a0_(a0), ax_(ax), ay_(ay) {}
  explicit SolutionFunction_Potential2D_Linear( const PyDict& d ) :
    a0_(d.get(ParamsType::params.constant)),
    ax_(d.get(ParamsType::params.gradx)),
    ay_(d.get(ParamsType::params.grady)) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
    { T q = a0_ + ax_*x + ay_*y; return q; }

  template <class T>
  void operator()( const T& x, const T& y, const T& time,
                   ArrayQ<T>& q, ArrayQ<T>& qx, ArrayQ<T>& qy,
                   ArrayQ<T>& qxx, ArrayQ<T>& qxy, ArrayQ<T>& qyy ) const
    { q = a0_ + ax_*x + ay_*y; qx = ax_; qy = ay_; qxx = 0; qxy = 0; qyy = 0; }

private:
  Real a0_, ax_, ay_;
};


//----------------------------------------------------------------------------//
// solution: sin(2*PI*x)*sin(2*PI*y)

class SolutionFunction_Potential2D_SineSine :
    public Function2DVirtualInterface<SolutionFunction_Potential2D_SineSine, PotentialFunctionTraits>
{
public:
  template <class T>
  using ArrayQ = PotentialFunctionTraits::ArrayQ<T>;    // solution arrays

  struct ParamsType : noncopyable
  {
    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit SolutionFunction_Potential2D_SineSine() {}
  explicit SolutionFunction_Potential2D_SineSine( const PyDict& d ) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
    { T q = sin(2*PI*x)*sin(2*PI*y); return q; }

  template <class T>
  void operator()( const T& x, const T& y, const T& time,
                   ArrayQ<T>& q, ArrayQ<T>& qx, ArrayQ<T>& qy,
                   ArrayQ<T>& qxx, ArrayQ<T>& qxy, ArrayQ<T>& qyy ) const
  {
    Real snx = sin(2*PI*x), csx = cos(2*PI*x);
    Real sny = sin(2*PI*y), csy = cos(2*PI*y);

    q   = snx*sny;
    qx  = 2*PI*csx*sny;
    qy  = 2*PI*snx*csy;
    qxx = -4*PI*PI*snx*sny;
    qxy =  4*PI*PI*csx*csy;
    qyy = -4*PI*PI*snx*sny;
  }
};


//----------------------------------------------------------------------------//
// solution: cubic-source bump
//
// u(0,0) = 1  u(x,1) = 0  u(1,y) = 0

class SolutionFunction_Potential2D_CubicSourceBump :
    public Function2DVirtualInterface<SolutionFunction_Potential2D_CubicSourceBump, PotentialFunctionTraits>
{
public:
  template <class T>
  using ArrayQ = PotentialFunctionTraits::ArrayQ<T>;    // solution arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> tau{"tau", NO_DEFAULT, NO_RANGE, "Bump height (%)"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  explicit SolutionFunction_Potential2D_CubicSourceBump( const Real& tau ) : tau_(tau) {}
  explicit SolutionFunction_Potential2D_CubicSourceBump( const PyDict& d ) : tau_(d.get(ParamsType::params.tau)) {}

  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
  {
    T phi;
    T src;

    src = tau_*15000 / (1875*PI - 160 - 3750*atan(0.2) - 3243*atan(5.));

    if (y == 0)
    {
      if (x == 0)
        phi = -src/48.;
      else if (x == 1)
        phi =  src/48.;
      else
        phi = -(src/48.)*( 1 + 2*x*(2 - 3*x*(3 - 2*x))
                          - 6*x*x*(x-1)*(x-1)*log(x*x/((x-1)*(x-1))) );
    }
    else
    {
      phi = src*( -(1 - 2*x)*(1 - 6*x*(x-1) + 18*y*y)/48.
                  + 0.5*(1 - 2*x)*y*(x*(x-1) - y*y)*(atan((x-1)/y) - atan(x/y))
                  + 0.125*(x*x*(x-1)*(x-1) - (1 + 6*x*(x-1))*y*y + y*y*y*y)*
                    log((x*x + y*y)/((x-1)*(x-1) + y*y)) );
    }

    return phi;
  }

  template <class T>
  void operator()( const T& x, const T& y, const T& time,
                   ArrayQ<T>& q, ArrayQ<T>& qx, ArrayQ<T>& qy,
                   ArrayQ<T>& qxx, ArrayQ<T>& qxy, ArrayQ<T>& qyy ) const {}

private:
  Real tau_;      // bump height (e.g. tau_ = 0.1)
};


//----------------------------------------------------------------------------//
// solution: perturbation potential for Joukowski airfoil at angle-of-attack
//
// chord = 1; LE at x = 0
//
// ref: Katz-Plotkin, 2nd ed (2001), p 128-139

class SolutionFunction_Potential2D_Joukowski :
    public Function2DBase< PotentialFunctionTraits::ArrayQ<Real> >
{
public:
  template <class T>
  using ArrayQ = PotentialFunctionTraits::ArrayQ<T>;    // solution arrays

  struct ParamsType : noncopyable
  {
    const ParameterNumeric<Real> e{"e", NO_DEFAULT, NO_RANGE, "thickness parameter (0.1 gives ~12% t/c)"};
    const ParameterNumeric<Real> aoa{"aoa", NO_DEFAULT, NO_RANGE, "angle-of-attack (deg)"};

    static void checkInputs(PyDict d);

    static ParamsType params;
  };

  SolutionFunction_Potential2D_Joukowski( const Real& e, const Real& aoa ) : e_(e), aoa_(aoa) {}
  explicit SolutionFunction_Potential2D_Joukowski( const PyDict& d ) :
    e_(d.get(ParamsType::params.e)),
    aoa_(d.get(ParamsType::params.aoa)) {}

#if 0
  template <class T>
  ArrayQ<T> operator()( const T& x, const T& y, const T& time ) const
#else
  virtual PotentialFunctionTraits::ArrayQ<Real> operator()( const Real& x, const Real& y, const Real& time ) const
#endif
  {
   //typedef std::complex<T> Complex;
   typedef std::complex<Real> Complex;

    Real U = 1;       // freestream speed
    Real c = 1;       // chord
    Real e = e_;      // thickness param (0.1 gives ~12% t/c)

    Real aoar = aoa_*(PI/180.);

    Real C = 4*c/(3 + 2*e + 1./(1 + 2*e));    // Joukowski transform constant
    Real a = 0.25*C*(1 + e);                  // circle radius in zeta-plane
    Real zeta0 = -0.25*e*C;                   // circle center in zeta-plane
    Real Gamma = 4*PI*U*a*sin(aoar);          // circulation

    Real xle = -c*(1 + 2*e*(1 + e))/(2*(1 + e)*(1 + e));    // LE
    //Real xte =  c*(1 + 2*e)/(2*(1 + e)*(1 + e));            // TE

    //T xs = x + xle;             // shifted x giving origin at roughly mid-chord
    Real xs = x + xle;             // shifted x giving origin at roughly mid-chord

    Complex I( 0, 1 );
    Complex z  = x  + I*y;
    Complex zs = xs + I*y;      // shifted physical-plane location
    Complex zeta;               // circle-plane location
    Complex logzeta;
    Complex pot;

    if (xs < 0)
      zeta = 0.5*(zs - sqrt(zs*zs - 0.25*C*C));
    else
      zeta = 0.5*(zs + sqrt(zs*zs - 0.25*C*C));

    // imag(log) in [0, 2*PI]
    logzeta = log(zeta - zeta0);
    if (arg(zeta - zeta0) < 0)
      logzeta += 2*PI*I;

    pot  = U*exp(-I*aoar)*(zeta - zeta0) + I*Gamma*logzeta/(2*PI) + U*a*a*exp(I*aoar)/(zeta - zeta0);
    pot -= U*exp(-I*aoar)*z;
    #if 1   // appears to need a constant shift so that pot->0 for {x,y}->{-inf,0}
    pot -= U*(xle - zeta0);
    #endif

    return real(pot);
  }

  template <class T>
  void operator()( const T& x, const T& y, const T& time,
                   ArrayQ<T>& phi, ArrayQ<T>& phix, ArrayQ<T>& phiy,
                   ArrayQ<T>& phixx, ArrayQ<T>& phixy, ArrayQ<T>& phiyy ) const
  {
    typedef std::complex<T> Complex;

    Real U = 1;       // freestream speed
    Real c = 1;       // chord
    Real e = e_;      // thickness param (0.1 gives ~12% t/c)

    Real aoar = aoa_*(PI/180.);

    Real C = 4*c/(3 + 2*e + 1./(1 + 2*e));    // Joukowski transform constant
    Real a = 0.25*C*(1 + e);                  // circle radius in zeta-plane
    Real zeta0 = -0.25*e*C;                   // circle center in zeta-plane
    Real Gamma = 4*PI*U*a*sin(aoar);          // circulation

    Real xle = -c*(1 + 2*e*(1 + e))/(2*(1 + e)*(1 + e));    // LE
    //Real xte =  c*(1 + 2*e)/(2*(1 + e)*(1 + e));            // TE

    T xs = x + xle;             // shifted x giving origin at roughly mid-chord

    Complex I( 0, 1 );
    Complex z  = x  + I*y;
    Complex zs = xs + I*y;      // shifted physical-plane location
    Complex zeta;               // circle-plane location
    Complex logzeta, logzeta_zeta, logzeta_zeta_zeta;
    Complex pot, pot_zeta, pot_zeta_zeta, pot_zs, pot_zs_zs;
    Complex pot_zsc, pot_zs_zsc, pot_zsc_zsc;         // zsc = conjugate(zs)
    Complex zeta_zs, zeta_zs_zs, tmp;

    if (xs < 0)
      zeta = 0.5*(zs - sqrt(zs*zs - 0.25*C*C));
    else
      zeta = 0.5*(zs + sqrt(zs*zs - 0.25*C*C));

    // imag(log) in [0, 2*PI]
    logzeta = log(zeta - zeta0);
    if (arg(zeta - zeta0) < 0)
      logzeta += 2*PI*I;

    logzeta_zeta = 1./(zeta - zeta0);
    logzeta_zeta_zeta = -logzeta_zeta/(zeta - zeta0);

    pot  = U*exp(-I*aoar)*(zeta - zeta0) + I*Gamma*logzeta/(2*PI) + U*a*a*exp(I*aoar)/(zeta - zeta0);
    pot -= U*exp(-I*aoar)*z;
    #if 1   // appears to need a constant shift so that pot->0 for {x,y}->{-inf,0}
    pot -= U*(xle - zeta0);
    #endif

    zeta_zs = 16.*zeta*zeta/(16.*zeta*zeta - C*C);
    tmp = zeta/(16.*zeta*zeta - C*C);
    zeta_zs_zs = -512*C*C*tmp*tmp*tmp;

    pot_zeta = U*exp(-I*aoar) + I*Gamma*logzeta_zeta/(2*PI) - U*a*a*exp(I*aoar)/((zeta - zeta0)*(zeta - zeta0));
    pot_zs   = pot_zeta*zeta_zs - U*exp(-I*aoar);     // NOTE: d(zs)/d(z) = 1
    pot_zsc  = 0;                                     // zsc = conjugate(zs) ; pot is function of zs alone

    pot_zeta_zeta = I*Gamma*logzeta_zeta_zeta/(2*PI) + 2*U*a*a*exp(I*aoar)/((zeta - zeta0)*(zeta - zeta0)*(zeta - zeta0));
    pot_zs_zs     = pot_zeta_zeta*(zeta_zs*zeta_zs) + pot_zeta*zeta_zs_zs;
    pot_zs_zsc    = 0;
    pot_zsc_zsc   = 0;

    phi   = real(pot);
    phix  = real(    pot_zs + pot_zsc  );
    phiy  = real( I*(pot_zs - pot_zsc) );
    phixx = real( 2.*pot_zs_zsc + (pot_zs_zs + pot_zsc_zsc) );
    phiyy = real( 2.*pot_zs_zsc - (pot_zs_zs + pot_zsc_zsc) );
    phixy = real( I*(pot_zs_zs - pot_zsc_zsc) );
  }

  virtual void gradient( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt ) const
  {
    ArrayQ<Real> qxx, qxy, qyy;
    operator()( x, y, time, q, qx, qy, qxx, qxy, qyy );
    qt = 0;
  }

  virtual void secondGradient( const Real& x, const Real& y, const Real& time, ArrayQ<Real>& q,
                               ArrayQ<Real>& qx, ArrayQ<Real>& qy, ArrayQ<Real>& qt,
                               ArrayQ<Real>& qxx,
                               ArrayQ<Real>& qyx, ArrayQ<Real>& qyy ) const
  {
    operator()( x, y, time, q, qx, qy, qxx, qyx, qyy );
    qt = 0;
  }

  virtual void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    SANS_DEVELOPER_EXCEPTION( "dump function not implemented" );
  }

private:
  Real e_;      // thickness parameter (0.1 gives ~12% t/c)
  Real aoa_;    // angle-of-attack (deg)
};


} //namespace SANS

#endif  // SOLUTIONFUNCTION_POTENTIAL2D_H
