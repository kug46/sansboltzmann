// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCLINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_SANSLG_H
#define BCLINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_SANSLG_H

// 2-D Linearized Incompressible Potential BC class


#include "Python/PyDict.h" //Python must be included first
#include "Python/Parameter.h"

#include <boost/mpl/vector.hpp>

#include "tools/SANSnumerics.h"     // Real
#include "Topology/Dimension.h"
#include "PDELinearizedIncompressiblePotential2D.h"
#include "BCPotential2D_SolutionFunction.h"
#include "pde/BCCategory.h"

#include <iostream>
#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// BC base class: 2-D Linearized Incompressible Potential
//
// template parameters:
//   Tg                   solution DOF data type (e.g. double)
//   PDE                  PDE type
//   BCType               BC type (e.g. BCTypeNeumann)
//
// NOTE: see end of file for definitions of member functions coefficients/data
//----------------------------------------------------------------------------//

template <class BCType>
struct BCLinearizedIncompressiblePotential2DParams;

template <class BCType, class Tg>
class BCLinearizedIncompressiblePotential2D;


//----------------------------------------------------------------------------//
// BC types
//----------------------------------------------------------------------------//

class BCTypeDirichlet;
class BCTypeNeumann;
class BCTypeWall;
class BCTypeFarfieldVortex;
class BCTypeFunction;

// Define the vector of boundary conditions
template<class Tg>
using BCLinearizedIncompressiblePotential2DVector =
        boost::mpl::vector5< BCLinearizedIncompressiblePotential2D<BCTypeDirichlet, Tg>,
                             BCLinearizedIncompressiblePotential2D<BCTypeNeumann, Tg>,
                             BCLinearizedIncompressiblePotential2D<BCTypeWall, Tg>,
                             BCLinearizedIncompressiblePotential2D<BCTypeFarfieldVortex, Tg>,
                             BCLinearizedIncompressiblePotential2D<BCTypeFunction, Tg>
                           >;


//----------------------------------------------------------------------------//
// Dirichlet BC:  phi = bcdata

template <>
struct BCLinearizedIncompressiblePotential2DParams<BCTypeDirichlet> : noncopyable
{
  const ParameterNumeric<Real> q{"q", NO_DEFAULT, NO_RANGE, "Dirichlet BC value"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Dirichlet"};
  struct Option
  {
    const DictOption Dirichlet{BCLinearizedIncompressiblePotential2DParams::BCName, BCLinearizedIncompressiblePotential2DParams::checkInputs};
  };

  static BCLinearizedIncompressiblePotential2DParams params;
};


template <class Tg>
class BCLinearizedIncompressiblePotential2D<BCTypeDirichlet, Tg>
  : public BCType< BCLinearizedIncompressiblePotential2D<BCTypeDirichlet, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCLinearizedIncompressiblePotential2DParams<BCTypeDirichlet> ParamsType;

  typedef PDELinearizedIncompressiblePotential2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using MatrixQ = T;    // matrices

  explicit BCLinearizedIncompressiblePotential2D( const Real& bcdata ) : bcdata_(bcdata) {}
  BCLinearizedIncompressiblePotential2D( const PDE& pde, const PyDict& d ) : bcdata_(d.get(ParamsType::params.q)) {}
  virtual ~BCLinearizedIncompressiblePotential2D() {}

  BCLinearizedIncompressiblePotential2D( const BCLinearizedIncompressiblePotential2D& ) = delete;
  BCLinearizedIncompressiblePotential2D& operator=( const BCLinearizedIncompressiblePotential2D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> bcdata_;
};


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeDirichlet, Tg>::coefficients(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeDirichlet, Tg>::data(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}


//----------------------------------------------------------------------------//
// Neumann BC:  d(phi)/dn = bcdata

template <>
struct BCLinearizedIncompressiblePotential2DParams<BCTypeNeumann> : noncopyable
{
  const ParameterNumeric<Real> gradqn{"gradqn", NO_DEFAULT, NO_RANGE, "Neumann BC value"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Neumann"};
  struct Option
  {
    const DictOption Neumann{BCLinearizedIncompressiblePotential2DParams::BCName, BCLinearizedIncompressiblePotential2DParams::checkInputs};
  };

  static BCLinearizedIncompressiblePotential2DParams params;
};


template <class Tg>
class BCLinearizedIncompressiblePotential2D<BCTypeNeumann, Tg>
  : public BCType< BCLinearizedIncompressiblePotential2D<BCTypeNeumann, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCLinearizedIncompressiblePotential2DParams<BCTypeNeumann> ParamsType;

  typedef PDELinearizedIncompressiblePotential2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using MatrixQ = T;    // matrices

  explicit BCLinearizedIncompressiblePotential2D( const Real& bcdata ) : bcdata_(bcdata) {}
  BCLinearizedIncompressiblePotential2D( const PDE& pde, const PyDict& d ) : bcdata_(d.get(ParamsType::params.gradqn)) {}
  virtual ~BCLinearizedIncompressiblePotential2D() {}

  BCLinearizedIncompressiblePotential2D( const BCLinearizedIncompressiblePotential2D& ) = delete;
  BCLinearizedIncompressiblePotential2D& operator=( const BCLinearizedIncompressiblePotential2D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  ArrayQ<Real> bcdata_;
};


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeNeumann, Tg>::coefficients(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;
  B = 1;
}


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeNeumann, Tg>::data(
    const Real& x, const Real& y, const Real& time,
    const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  bcdata = bcdata_;
}


//----------------------------------------------------------------------------//
// wall BC:  d(phi)/dn = - d(phi_uniform)/dn

template <>
struct BCLinearizedIncompressiblePotential2DParams<BCTypeWall> : noncopyable
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Wall"};
  struct Option
  {
    const DictOption Wall{BCLinearizedIncompressiblePotential2DParams::BCName, BCLinearizedIncompressiblePotential2DParams::checkInputs};
  };

  static BCLinearizedIncompressiblePotential2DParams params;
};


template <class Tg>
class BCLinearizedIncompressiblePotential2D<BCTypeWall, Tg>
  : public BCType< BCLinearizedIncompressiblePotential2D<BCTypeWall, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCLinearizedIncompressiblePotential2DParams<BCTypeWall> ParamsType;

  typedef PDELinearizedIncompressiblePotential2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using MatrixQ = T;    // matrices

  explicit BCLinearizedIncompressiblePotential2D( const PDE& pde ) : u_(0), v_(0) { pde.freestream(u_, v_); }
  BCLinearizedIncompressiblePotential2D( const Real& u, const Real& v ) : u_(u), v_(v) {}
  BCLinearizedIncompressiblePotential2D( const PDE& pde, const PyDict& d ) : u_(0), v_(0) { pde.freestream(u_, v_); }
  virtual ~BCLinearizedIncompressiblePotential2D() {}

  BCLinearizedIncompressiblePotential2D( const BCLinearizedIncompressiblePotential2D& ) = delete;
  BCLinearizedIncompressiblePotential2D& operator=( const BCLinearizedIncompressiblePotential2D& ) = delete;

  // BC coefficients:  A phi + B ( -d(phi)/dn )
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real u_, v_;      // freestream velocity
};


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeWall, Tg>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 0;
  B = 1;
}


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeWall, Tg>::data(
    const Real&, const Real&, const Real&, const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  bcdata = -(nx*u_ + ny*v_);
}


//----------------------------------------------------------------------------//
// Farfield BC w/ vortex:  phi = vortex
//
//  phi = circulation * theta / (2*PI)
//
//  where theta = arctan( (y - y0)/(x - x0) ); theta in [0, 2*PI)

template <>
struct BCLinearizedIncompressiblePotential2DParams<BCTypeFarfieldVortex> : noncopyable
{
  const ParameterNumeric<Real> x0{"x0", NO_DEFAULT, NO_RANGE, "Vortex centroid (x)"};
  const ParameterNumeric<Real> y0{"y0", NO_DEFAULT, NO_RANGE, "Vortex centroid (y)"};
  const ParameterNumeric<Real> circ{"circ", NO_DEFAULT, NO_RANGE, "Vortex circulation"};

  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"FarfieldVortex"};
  struct Option
  {
    const DictOption FarfieldVortex{BCLinearizedIncompressiblePotential2DParams::BCName, BCLinearizedIncompressiblePotential2DParams::checkInputs};
  };

  static BCLinearizedIncompressiblePotential2DParams params;
};

template <class Tg>
class BCLinearizedIncompressiblePotential2D<BCTypeFarfieldVortex, Tg>
  : public BCType< BCLinearizedIncompressiblePotential2D<BCTypeFarfieldVortex, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCLinearizedIncompressiblePotential2DParams<BCTypeFarfieldVortex> ParamsType;

  typedef PDELinearizedIncompressiblePotential2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using MatrixQ = T;    // matrices

  BCLinearizedIncompressiblePotential2D( const Real& x0, const Real& y0, const Real& circ ) : x0_(x0), y0_(y0), circ_(circ) {}
  BCLinearizedIncompressiblePotential2D( const PDE& pde, const PyDict& d ) :
    x0_(d.get(ParamsType::params.x0)),
    y0_(d.get(ParamsType::params.y0)),
    circ_(d.get(ParamsType::params.circ)) {}
  virtual ~BCLinearizedIncompressiblePotential2D() {}

  BCLinearizedIncompressiblePotential2D( const BCLinearizedIncompressiblePotential2D& ) = delete;
  BCLinearizedIncompressiblePotential2D& operator=( const BCLinearizedIncompressiblePotential2D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data:  vortex
  template <class T>
  void data(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  Real x0_, y0_;      // vortex centroid
  Real circ_;         // vortex strength/circulation
};


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeFarfieldVortex, Tg>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeFarfieldVortex, Tg>::data(
    const Real& x, const Real& y, const Real& time, const Real&, const Real&,
    ArrayQ<T>& bcdata ) const
{
  //T circ = globalVar[0];
  Real th;

  th = atan2( y - y0_, x - x0_ );
  if (th < 0)  th += 2*PI;          // makes th in [0, 2*PI)

  bcdata =  -circ_ * th / (2*PI);
  //std::cout << x << " " << y << " " << bcdata(0) << std::endl;
}


//----------------------------------------------------------------------------//
// Solution BC:  u = uexact(x,y)
//
// A = 1; B = 0

template <>
struct BCLinearizedIncompressiblePotential2DParams<BCTypeFunction> : BCPotential2D_SolutionFunction_Params
{
  static void checkInputs(PyDict d);

  static constexpr const char* BCName{"Function"};
  struct Option
  {
    const DictOption Function{BCLinearizedIncompressiblePotential2DParams::BCName, BCLinearizedIncompressiblePotential2DParams::checkInputs};
  };

  static BCLinearizedIncompressiblePotential2DParams params;
};


template <class Tg>
class BCLinearizedIncompressiblePotential2D<BCTypeFunction, Tg>
  : public BCType< BCLinearizedIncompressiblePotential2D<BCTypeFunction, Tg> >
{
public:
  typedef BCCategory::LinearScalar_sansLG Category;
  typedef BCLinearizedIncompressiblePotential2DParams<BCTypeFunction> ParamsType;

  typedef PDELinearizedIncompressiblePotential2D PDE;

  typedef PhysD2 PhysDim;
  static const int D = PhysDim::D;            // physical dimensions
  static const int N = PDE::N;                // total solution variables

  static const int NBC = 1;                   // total BCs

  template <class T>
  using ArrayQ = T;    // solution/residual arrays

  template <class T>
  using MatrixQ = T;    // matrices

  typedef std::shared_ptr<Function2DBase<ArrayQ<Real>>> Function_ptr;

  explicit BCLinearizedIncompressiblePotential2D( const Function_ptr& uexact ) :
      uexact_(uexact) {}
  BCLinearizedIncompressiblePotential2D( const PDE& pde, const PyDict& d ) :
    uexact_( BCPotential2D_SolutionFunction::getSolution(d) ) {}
  virtual ~BCLinearizedIncompressiblePotential2D() {}

  BCLinearizedIncompressiblePotential2D( const BCLinearizedIncompressiblePotential2D& ) = delete;
  BCLinearizedIncompressiblePotential2D& operator=( const BCLinearizedIncompressiblePotential2D& ) = delete;

  // BC coefficients:  A phi + B d(phi)/dn
  template <class T>
  void coefficients(
      const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      MatrixQ<T>& A, MatrixQ<T>& B ) const;

  // BC data
  template <class T>
  void data( const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
      ArrayQ<T>& bcdata ) const;

  // is the boundary state valid
  bool isValidState( const Real& nx, const Real& ny, const ArrayQ<Real>& q ) const { return true; }

  virtual void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const Function_ptr uexact_;
};


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeFunction, Tg>::coefficients(
    const Real&, const Real&, const Real&, const Real&, const Real&,
    MatrixQ<T>& A, MatrixQ<T>& B ) const
{
  A = 1;
  B = 0;
}


template <class Tg>
template <class T>
inline void
BCLinearizedIncompressiblePotential2D<BCTypeFunction, Tg>::data(
    const Real& x, const Real& y, const Real& time, const Real& nx, const Real& ny,
    ArrayQ<T>& bcdata ) const
{
  bcdata = (*uexact_)(x, y, time);
}


} //namespace SANS

#endif  // BCLINEARIZEDINCOMPRESSIBLEPOTENTIAL2D_SANSLG_H
