// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "PDEEikonal_Distance2D.h"

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"

#include "pde/AdvectionDiffusion/AdvectiveFlux2D.h"

#define BCPARAMETERS_INSTANTIATE
#include "pde/BCParameters_impl.h"

namespace SANS
{

//===========================================================================//
// Instantiate the BC parameters

typedef BCAdvectionDiffusion2DVector< AdvectiveFlux2D_None, ViscousFlux2D_Eikonal> BCVector;
BCPARAMETER_INSTANTIATE( BCVector )

} //namespace SANS
