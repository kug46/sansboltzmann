// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#include "pde/AdvectionDiffusion/ViscousFlux2D.h"
#include "pde/AdvectionDiffusion/AdvectiveFlux2D.h"
#include "pde/AdvectionDiffusion/PDEAdvectionDiffusion2D.h"

#ifndef EIKONAL_DISTANCE_H
#define EIKONAL_DISTANCE_H

namespace SANS
{

//----------------------------------------------------------------------------//
// Eikonal with Diffusion PDE
//
//  Diffusion relaxation to the Eikonal distance equaiton:
//    gradq.gradq - q grad^2.q = 1
//
//  where q represents the distance function
//
//  Moving q inside the divergence:
//    q grad^2.q = grad . q gradq - gradq.gradq
//
//  Hence the Eikonal equation is:
//    2 gradq.gradq - grad . q gradq = 1
//
//----------------------------------------------------------------------------//

class DiffusionMatrix2D_Eikonal
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  // cppcheck-suppress noExplicitConstructor
  DiffusionMatrix2D_Eikonal(const Real alpha) : alpha_(alpha) {}
  DiffusionMatrix2D_Eikonal(const DiffusionMatrix2D_Eikonal& diff) : alpha_(diff.alpha_) {}

  template <class Tq, class Tg, class Tk>
  void diffusionViscous( const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
  {
    kxx += alpha_*q; kxy +=        0;
    kyx +=        0; kyy += alpha_*q;
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const Real& x, const Real& y, const Real& time,
                     const ArrayQ<Tq>& q,
                     const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE -= alpha_*(qx*qx + q*qxx + qy*qy + q*qyy);
  }

  // strong form of viscous flux: d(Fv)/d(U)
  template <class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay ) const
  {
    ax -= alpha_*qx;
    ay -= alpha_*qy;
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const
  {
    std::string indent(indentSize, ' ');
    out << indent << "DiffusionMatrix2D_Eikonal:" << std::endl;
  }
protected:
  Real alpha_;
};

typedef ViscousFlux2D_LinearInGradient<DiffusionMatrix2D_Eikonal> ViscousFlux2D_Eikonal;


//----------------------------------------------------------------------------//
// Source: Eikonal
//
// source = gradq.gradq
//----------------------------------------------------------------------------//

class Source2D_Eikonal
{
public:
  template<class T>
  using ArrayQ = AdvectionDiffusionTraits<PhysD2>::ArrayQ<T>;

  template<class T>
  using MatrixQ = AdvectionDiffusionTraits<PhysD2>::MatrixQ<T>;

  // cppcheck-suppress noExplicitConstructor
  Source2D_Eikonal(Real alpha) : alpha_(alpha) {}
  Source2D_Eikonal(const Source2D_Eikonal& src) : alpha_(src.alpha_) {}

  bool hasSourceTerm() const { return true; }
  bool hasSourceTrace() const { return false; }
  bool needsSolutionGradientforSource() const { return true; }

  template<class Tq, class Tg, class Ts>
  void source( const Real& x, const Real& y, const Real& time,
               const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
               ArrayQ<Ts>& source ) const
  {
    source += (1+alpha_)*(qx*qx + qy*qy);
  }

  template <class Tq, class Tg, class Ts>
  void sourceTrace(
      const Real& xL, const Real& yL, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
  {
    SANS_DEVELOPER_EXCEPTION("Should not be used...");
  }

  // d(S)/d(U)
  template <class Tq, class Tg, class Ts>
  void jacobianSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
    // Not a function of U
  }

  template <class Tq, class Tg, class Ts>
  void jacobianGradientSource(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy  ) const
  {
    dsdux += 2*(1+alpha_)*qx;
    dsduy += 2*(1+alpha_)*qy;
  }
protected:
  Real alpha_;
};

// Define the Eikonal PDE
class PDEEikonal_Distance2D : public PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_None, ViscousFlux2D_Eikonal, Source2D_Eikonal>
{
public:
  typedef PDEAdvectionDiffusion<PhysD2, AdvectiveFlux2D_None, ViscousFlux2D_Eikonal, Source2D_Eikonal> BaseType;

  explicit PDEEikonal_Distance2D(const Real alpha = 1) : BaseType(adv_, alpha, alpha) {}

  bool hasForcingFunction() const { return true; }

  // right-hand-side forcing function
  template <class T>
  void forcingFunction( const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const
  {
    forcing += 1;
  }

  // is state physically valid
  bool isValidState( const Real& q ) const { return q >= 0; }

protected:
  AdvectiveFlux2D_None adv_;
};


}

#endif //EIKONAL_DISTANCE_H
