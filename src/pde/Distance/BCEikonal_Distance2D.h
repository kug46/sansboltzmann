// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef BCEIKONAL_DISTANCE2D_H
#define BCEIKONAL_DISTANCE2D_H

#include "pde/AdvectionDiffusion/BCAdvectionDiffusion2D.h"
#include "PDEEikonal_Distance2D.h"

namespace SANS
{

//----------------------------------------------------------------------------//
// BC class: 2-D Eikonal distance
//----------------------------------------------------------------------------//

// Define the vector of boundary conditions
typedef BCAdvectionDiffusion2DVector<AdvectiveFlux2D_None, ViscousFlux2D_Eikonal> BCEional2DVector;

} // namespace SANS

#endif //BCEIKONAL_DISTANCE2D_H
