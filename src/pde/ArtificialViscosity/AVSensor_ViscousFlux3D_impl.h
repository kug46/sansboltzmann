// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(AVSENSOR3D_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "AVSensor_ViscousFlux3D.h"

#include <string>
#include <iostream>


namespace SANS
{

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
template <template <class> class PDETraitsSize>
void
AVSensor_DiffusionMatrix3D_Uniform<PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AVSensor_DiffusionMatrix2D_Uniform:"
      << "  kxx_ = " << kxx_ << "  kxy_ = " << kxy_ << "  kxz_ = " << kxz_
      << "  kyx_ = " << kyx_ << "  kyy_ = " << kyy_ << "  kyz_ = " << kyz_
      << "  kzx_ = " << kzx_ << "  kzy_ = " << kzy_ << "  kzz_ = " << kzz_
      << std::endl;
}

//----------------------------------------------------------------------------//
// diffusion matrix: with generalized h-scale
template <template <class> class PDETraitsSize>
void
AVSensor_DiffusionMatrix3D_GenHScale<PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AVSensor_DiffusionMatrix3D_GenHScale:"
                << "  order_ = " << order_
                << "  C1_ = " << C1_
                << "  C2_ = " << C2_
      << std::endl;
}

} //namespace SANS
