// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEARTIFICIALVISCOSITY1D_H
#define PDEARTIFICIALVISCOSITY1D_H

// PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "AVVariable.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"
#include "tools/smoothmath.h"

#include <iostream>
#include <limits>
#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 1D Artificial Viscosity wrapper
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: U(Q)
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEmitAVSensor1D : public PDETraitsModel::PDEBase
{
public:

  typedef PhysD1 PhysDim;

  typedef typename PDETraitsModel::PDEBase BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  static_assert( D == PDETraitsSize<PhysDim>::D, "Dimension mismatch!");
  static_assert( N == PDETraitsSize<PhysDim>::N, "Solution variable mismatch!");

  static const int Nparam = 1;               // total solution parameters

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  template <class TP>
  using ArrayParam = DLA::VectorS<Nparam,TP>;  // parameter array type

  template <class T>
  using MatrixParam = DLA::MatrixS<N,Nparam,T>;         // e.g. jacobian of PDEs w.r.t. parameters

  typedef typename PDETraitsModel::AdvectiveFlux AdvectiveFlux; //advective flux for sensor PDE
  typedef typename PDETraitsModel::ViscousFlux ViscousFlux; //viscous flux for sensor PDE
  typedef typename PDETraitsModel::Source Source; //source term for sensor PDE

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs >
  PDEmitAVSensor1D(const AdvectiveFlux& adv, const ViscousFlux& visc, const Source& src,
                           const bool isSensorSteady, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    adv_(adv),
    visc_(visc),
    source_(src),
    isSensorSteady_(isSensorSteady),
    C1_(3.0) // Need a better way to sort that
  {
    //Nothing
  }

  ~PDEmitAVSensor1D() {}

  PDEmitAVSensor1D& operator=( const PDEmitAVSensor1D& ) = delete;

  static const int iSens = PDETraitsSize<PhysDim>::iSens;

  Real nMonitor() const { return BaseType::nMonitor(); }

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return ( BaseType::hasFluxAdvective() || adv_.hasFluxAdvective() ); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return ( BaseType::hasSource() || source_.hasSourceTerm() ); }
  bool hasSourceTrace() const { return ( BaseType::hasSourceTrace() || source_.hasSourceTrace() ); }
  bool hasSourceLiftedQuantity() const { return source_.hasSourceLiftedQuantity(); }
  bool hasForcingFunction() const { return BaseType::hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const
  {
    return ( BaseType::needsSolutionGradientforSource() || source_.needsSolutionGradientforSource() );
  }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tp, class Tf>
  void fluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(param, x, time, q, ft);
    if (!isSensorSteady_)
    {
      Tp H = exp(param); //generalized h-tensor
      Tp Hsq = H*H;

      T tau = 1.0;
      typedef DLA::VectorS<Tp::M, Real> VectorX;
      VectorX hPrincipal;
      DLA::EigenValues(H, hPrincipal);

      //Get minimum length scale
      Real hmin = std::numeric_limits<Real>::max();
      for (int d = 0; d < D; d++)
        hmin = min(hmin, hPrincipal[d]);

      T lambda = 0;
      speedCharacteristic( param, x, time, q, lambda );

      // characteristic time of the PDE
      tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
      ft(iSens) += tau*q(iSens);
    }
  }

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class Tp>
  void jacobianFluxAdvectiveTime(
      const DLA::MatrixSymS<D, Tp>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class Tp>
  void jacobianFluxAdvectiveTime(
      const DLA::MatrixSymS<D+1, Tp>& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& dudq ) const;

  // master state: U(Q)
  template <class T, class Tp, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template<class T, class Tp>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    BaseType::jacobianMasterState(param, x, time, q, J);
    J(iSens, iSens) = DLA::Identity();
  }

  // unsteady conservative flux: d(U)/d(t)
  template <class T, class Tp>
  void strongFluxAdvectiveTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const;

  // advective flux: F(Q)
  template <class T, class Tp, class Ts>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Ts>& f ) const;

  template <class T, class Tp, class Ts>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, ArrayQ<Ts>& f ) const;

  template <class T, class Tp, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class T, class Tp>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x,  const Real& time, const ArrayQ<T>& q,
      MatrixQ<T>& ax ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class Tp>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const Real& nx,
      MatrixQ<T>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class T, class Tp>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& q,
      const Real& nx, const Real& nt,
      MatrixQ<T>& a ) const;

  // strong form advective fluxes
  template <class T, class Tp>
  void strongFluxAdvective(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      ArrayQ<T>& strongPDE ) const;

  // viscous flux: Fv(Q, Qx)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Tf>& fx ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR,
      const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      const Real& nx, ArrayQ<Tf>& fn ) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      MatrixQ<Tk>& kxx) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class T>
  void jacobianFluxViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& ax ) const {}

  // strong form viscous fluxes
  template <class Tp, class T>
  void strongFluxViscous(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      const ArrayQ<T>& qxx,
      ArrayQ<T>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& nt,
      ArrayQ<Tf>& f ) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& ktx,MatrixQ<Tk>& ktt ) const;

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& at ) const;

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tp, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
      ArrayQ<Ts>& source ) const;

  // dual-consistent source
  template <class Tq, class Tp, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL,
      const Tp& paramR, const Real& xR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tp>
  void jacobianSource(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class T, class Tp>
  void jacobianSourceAbsoluteValue(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qx,
      MatrixQ<T>& dsdu ) const;

  // right-hand-side forcing function
  template <class T, class Tp>
  void forcingFunction( const Tp& param,
                        const Real& x, const Real& time, ArrayQ<T>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T, class Tp>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& time,
      const Real& dx, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T, class Tp, class Ts>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<T>& q, Ts& speed ) const;

  // characteristic speed
  template <class T, class Ts>
  void speedCharacteristic(
      const Real& x, const Real& time,
      const ArrayQ<T>& q, Ts& speed ) const;

  // update fraction needed for physically valid state
  template <class Tp>
  void updateFraction(
      const Tp& param, const Real& x, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(param, x, time, q, dq, maxChangeFraction, updateFraction);
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from primitive variable
  template<class T, class Variables>
  void setDOFFrom(
      ArrayQ<T>& q, const AVVariableType<Variables>& data ) const;

  // set from primitive variable
  template<class Variables>
  ArrayQ<Real> setDOFFrom( const AVVariableType<Variables>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  // interpret residuals of the gradient variable
  template <class T>
  void interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const;

  // interpret residuals of the bcs
  template <class T>
  void interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const AdvectiveFlux adv_;
  const ViscousFlux visc_;
  const Source source_;
  const bool isSensorSteady_;
  const Real C1_; // time scaling
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Tu>
void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::masterState(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  BaseType::masterState(param, x, time, q, uCons);
  uCons(iSens) = q(iSens);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveTime(
    const DLA::MatrixSymS<D, Tp>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  BaseType::jacobianFluxAdvectiveTime(param, x, time, q, dudq);
  if (!isSensorSteady_)
  {
    DLA::MatrixSymS<D, Tp> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D, Tp> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].value() = q[i];
    }

    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, time, qSurreal, dlambda_dq );

      qSurreal[i].deriv() = 0.0;

      dudq(iSens,i) += q(iSens)*dtau_dlambda*dlambda_dq.deriv();
    }

    dudq(iSens,iSens) += tau;
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveTime(
    const DLA::MatrixSymS<D+1, Tp>& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, MatrixQ<T>& dudq ) const
{
  BaseType::jacobianFluxAdvectiveTime(param, x, time, q, dudq);
  if (!isSensorSteady_)
  {
    DLA::MatrixSymS<D+1, Tp> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D+1, Tp> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<D+1, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D+1; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].value() = q[i];
    }
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, time, qSurreal, dlambda_dq );

      qSurreal[i].deriv() = 0.0;

      dudq(iSens,i) += q(iSens)*dtau_dlambda*dlambda_dq.deriv();
    }

    dudq(iSens,iSens) += tau;
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
{
  BaseType::strongFluxAdvectiveTime(param, x, time, q, qt, dudt);
  if (!isSensorSteady_)
  {
    Tp H = exp(param); //generalized h-tensor
    Tp Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<Tp::M, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = q;
    T dlambda_dt = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, time, qSurreal, dlambda_dq );
      dlambda_dt += dlambda_dq.deriv() * qt(i);

      qSurreal[i].deriv() = 0.0;
    }
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    dudt(iSens) += tau*qt(iSens) + dtau_dlambda*dlambda_dt*q(iSens);
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Ts>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Ts>& fx ) const
{
  BaseType::fluxAdvective(param, x, time, q, fx);
  adv_.flux( param, x, time, q, fx );
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Ts>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx,
    ArrayQ<Ts>& f ) const
{
  BaseType::fluxAdvectiveUpwind(param, x, time, qL, qR, nx, f);
  adv_.fluxUpwind(param, x, time, qL, qR, nx, f);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Tf>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& nt,
    ArrayQ<Tf>& f ) const
{
//  BaseType::fluxAdvectiveUpwindSpaceTime(param, x, time, qL, qR, nx, nt, f);
//  adv_.fluxUpwindSpaceTime(param, x, time, qL, qR, nx, nt, isSensorSteady_, f);

  //Compute upwinded spatial advective flux
  ArrayQ<T> fnx = 0;
  fluxAdvectiveUpwind(param, x, time, qL, qR, nx, fnx );

  //Upwind temporal flux
  ArrayQ<T> ft = 0;
  if (nt >= 0)
    fluxAdvectiveTime( param, x, time, qL, ft );
  else
    fluxAdvectiveTime( param, x, time, qR, ft );

  f += ft*nt + fnx;

}

// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Tp& param, const Real& x, const Real& time, const ArrayQ<T>& q,
    MatrixQ<T>& ax ) const
{
  BaseType::jacobianFluxAdvective(param, x, time, q, ax);
  adv_.jacobian( param, x, time, q, ax );
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Tp& param, const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, MatrixQ<T>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION("jacobianFluxAdvectiveAbsoluteValue not implemented yet.");
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Tp& param, const Real& x, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& nt, MatrixQ<T>& mtx ) const
{
  MatrixQ<T> ax = 0, at = 0;

  jacobianFluxAdvective(param, x, time, q, ax);

  if (!isSensorSteady_)
  {
//    at(iSens, iSens) = 1.0;
    Tp H = exp(param); //generalized h-tensor
    Tp Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<Tp::M, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
    at(iSens,iSens) = tau;
  }
  //TODO: Rather arbitrary smoothing constants here
  //Real eps = 1e-8;

  MatrixQ<T> an = nx*ax + nt*at;

  //mtx += smoothabs( an, eps );
  mtx += fabs( an );
}

// strong form of advective flux: d(F)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    ArrayQ<T>& strongPDE ) const
{
  BaseType::strongFluxAdvective( param, x, time, q, qx, strongPDE );
  adv_.strongFlux( param, x, time, q, qx, strongPDE );
}

// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Tf>& fx ) const
{
  BaseType::fluxViscous( param, x,  time, q, qx, fx );
  visc_.fluxViscous( *this, param, x, time, q, qx, fx );
}

// space-time viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& ft ) const
{
  BaseType::fluxViscousSpaceTime( param, x, time, q, qx, qt, fx, ft );
  visc_.fluxViscousSpaceTime( *this, param, x, time, q, qx, qt, fx, ft );
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    const Real& nx, ArrayQ<Tf>& fn ) const
{
  BaseType::fluxViscous( paramL, paramR, x, time, qL, qxL, qR, qxR, nx, fn );
  visc_.fluxViscous( *this, paramL, paramR, x, time, qL, qxL, qR, qxR, nx, fn );
}

// space-time viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& nt, ArrayQ<Tf>& fn ) const
{
  BaseType::fluxViscousSpaceTime( paramL, paramR, x, time, qL, qxL, qtL, qR, qxR, qtR, nx, nt, fn );
  visc_.fluxViscousSpaceTime( *this, paramL, paramR, x, time, qL, qxL, qtL, qR, qxR, qtR, nx, nt, fn );
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    MatrixQ<Tk>& kxx ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor1D::diffusionViscous is not implemented");

#if 0
  BaseType::diffusionViscous( param, x, y, time,
                              q, qx, qy,
                              kxx, kxy,
                              kyx, kyy );
  visc_.diffusionViscous( param, x, y, time,
                          q, qx, qy,
                          kxx, kxy,
                          kyx, kyy );
#endif
}

// space-time viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::diffusionViscousSpaceTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& ktt) const
{
  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor1D::diffusionViscousSpaceTime is not implemented");
}

// strong form of viscous flux: d(Fv)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class T>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    const ArrayQ<T>& qxx,
    ArrayQ<T>& strongPDE ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor1D::strongFluxViscous is not implemented");

#if 0
  BaseType::strongFluxViscous( param, x, y, time, q, qx, qxx, qxy, qyy, strongPDE );
  visc_.strongFluxViscous( param, x, time, q, qx, qy, qxx, qxy, qyy, strongPDE );
#endif
}

// space-time strong form of viscous flux: d(Fv)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Th, class Tf>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::strongFluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qt,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxt, const ArrayQ<Th>& qtt,
    ArrayQ<Tf>& strongPDE ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor1D::strongFluxViscousSpaceTime is not implemented");
}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::source(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{
  BaseType::source( param, x, time, q, qx, source );
  source_.source( *this, param, x, time, q, qx, source );
}

// solution-dependent source with lifted_quantity: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tlq, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::source(
    const Tp& param, const Real& x, const Real& time,
    const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx,
    ArrayQ<Ts>& source ) const
{
  BaseType::source( param, x, time, lifted_quantity, q, qx, source );
  source_.source( *this, param, x, time, lifted_quantity, q, qx, source );
}

// dual-consistent source
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tp, class Tg, class Ts>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::sourceTrace(
    const Tp& paramL, const Real& xL,
    const Tp& paramR, const Real& xR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  BaseType::sourceTrace(paramL, xL, paramR, xR, time,
                        qL, qxL, qR, qxR,
                        sourceL, sourceR);

  source_.sourceTrace(*this, paramL, xL, paramR, xR, time,
                      qL, qxL, qR, qxR,
                      sourceL, sourceR);
}

// trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Ts>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::sourceLiftedQuantity(
    const Tp& paramL, const Real& xL,
    const Tp& paramR, const Real& xR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    Ts& s ) const
{
  source_.sourceLiftedQuantity(paramL, xL, paramR, xR, time, qL, qR, s);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::jacobianSource(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    MatrixQ<T>& dsdu ) const
{
  BaseType::jacobianSource(param, x, time, q, qx, dsdu);
  source_.jacobianSource(*this, param, x, time, q, qx, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::jacobianSourceAbsoluteValue(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qx,
    MatrixQ<T>& dsdu ) const
{
  BaseType::jacobianSourceAbsoluteValue(param, x, time, q, qx, dsdu);
  source_.jacobianSourceAbsoluteValue(*this, param, x, time, q, qx, dsdu);
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Tp& param, const Real& x, const Real& time, ArrayQ<T>& forcing ) const
{
  BaseType::forcingFunction(param, x, time, forcing );
}


// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    const Tp& param, const Real& x, const Real& time,
    const Real& dx,const ArrayQ<T>& q, T& speed ) const
{
  BaseType::speedCharacteristic(param, x, time, dx, q, speed);
}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Ts>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    const Tp& param, const Real& x, const Real& time,
    const ArrayQ<T>& q, Ts& speed ) const
{
  BaseType::speedCharacteristic(param, x, time, q, speed);
}


// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return BaseType::isValidState(q);
}


// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  BaseType::setDOFFrom(q, data, name, nn);
}

// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Variables>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( ArrayQ<T>& q, const AVVariableType<Variables>& data ) const
{
  BaseType::setDOFFrom(q, data.cast());
  q(iSens) = data.cast().Sensor;
}

// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Variables>
inline typename PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( const AVVariableType<Variables>& data ) const
{
  ArrayQ<Real> q;
  setDOFFrom( q, data );
  return q;
}

// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q = BaseType::setDOFFrom(d);
  //q(iSens) = d.get("Sensor"); TODO
  return q;
}

// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::
interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  BaseType::interpResidVariable(rsdPDEIn, rsdPDEOut);
#if 1
  rsdPDEOut[nMonitor()-1] = rsdPDEIn[BaseType::nMonitor()-1]; // Monitor the AV residual
#else
  rsdPDEOut[nMonitor()-1] = 0; // No residual for artificial viscosity
#endif

}

// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::
interpResidGradient( const ArrayQ<T>& rsdAuxIn, VectorD<T>& rsdAuxOut ) const
{
  BaseType::interpResidGradient(rsdAuxIn, rsdAuxOut);
#if 1
  rsdAuxOut[nMonitor()-1] = rsdAuxIn[BaseType::nMonitor()-1];
#else
  rsdAuxOut[nMonitor()-1] = 0; // No residual for artificial viscosity
#endif

}

// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::
interpResidBC( const ArrayQ<T>& rsdBCIn, VectorD<T>& rsdBCOut ) const
{
  BaseType::interpResidBC(rsdBCIn, rsdBCOut);
#if 1
  rsdBCOut[nMonitor()-1] = rsdBCIn[BaseType::nMonitor()-1];
#else
  rsdBCOut[BaseType::nMonitor()-1] = 0; // No residual for artificial viscosity
#endif
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEmitAVSensor1D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEmitAVSensor1D: adv_ =" << std::endl;
  adv_.dump(indentSize+2, out);
  out << indent << "PDEmitAVSensor1D: visc_ =" << std::endl;
  visc_.dump(indentSize+2, out);
  out << indent << "PDEmitAVSensor1D: source_ =" << std::endl;
  source_.dump(indentSize+2, out);
  out << indent << "PDEmitAVSensor1D: pde_ =" << std::endl;
  BaseType::dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEARTIFICIALVISCOSITY1D_H
