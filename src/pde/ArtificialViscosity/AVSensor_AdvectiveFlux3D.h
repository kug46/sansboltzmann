// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef SENSOR_ADVECTIVEFLUX3D_H
#define SENSOR_ADVECTIVEFLUX3D_H

// 3D advective flux

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include <cmath> // fabs
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// advection velocity: uniform
//
// member functions:
//   .flux         advective flux: F(Q)
//   .jacobian     advective flux jacboian: d(F)/d(U)
//   .strongFlux   advective flux : div.F
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class AVSensor_AdvectiveFlux3D_Uniform
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD3>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  AVSensor_AdvectiveFlux3D_Uniform( const Real u = 0, const Real v = 0, const Real w = 0 ) : u_(u), v_(v), w_(w) {}
  ~AVSensor_AdvectiveFlux3D_Uniform() {}

  static const int iSens = PDETraitsSize<PhysD3>::iSens;

  // advective flux: F(Q)
  template <class T, class Tp, class Tf>
  void flux( const Tp& param, const Real& x, const Real& y, const Real& z, const Real time,
             const ArrayQ<T>& q,
             ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const
  {
    fx(iSens) += u_*q(iSens);
    fy(iSens) += v_*q(iSens);
    fz(iSens) += w_*q(iSens);
  }

  // advective flux jacobian: d(F)/d(U)
  template <class Tp, class Tq, class Ta>
  void jacobian( const Tp& param, const Real& x, const Real& y, const Real& z, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Ta>& ax, MatrixQ<Ta>& ay, MatrixQ<Ta>& az ) const
  {
    ax(iSens,iSens) += u_;
    ay(iSens,iSens) += v_;
    az(iSens,iSens) += w_;
  }

  template <class T, class Tp, class Tf>
  void fluxUpwind( const Tp& param, const Real& x, const Real& y, const Real& z, const Real time,
                   const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nz,
                   ArrayQ<Tf>& f ) const
  {
    f(iSens) += 0.5*(nx*u_ + ny*v_ + nz*w_)*(qR(iSens) + qL(iSens)) - 0.5*fabs(nx*u_ + ny*v_ + nz*w_)*(qR(iSens) - qL(iSens));
  }

  template <class T, class Tp, class Tf>
  void fluxUpwindSpaceTime( const Tp& param, const Real& x, const Real& y, const Real& z, const Real time,
                            const ArrayQ<T>& qL, const ArrayQ<T>& qR,
                            const Real& nx, const Real& ny, const Real& nz, const Real& nt, const bool& isSensorSteady,
                            ArrayQ<Tf>& f ) const
  {
    Real ut = 1.0; //unit speed in temporal direction
    if (isSensorSteady) ut = 0.0;

    f(iSens) += 0.5*(nx*u_ + ny*v_ + nz*w_ + nt*ut)*(qR(iSens) + qL(iSens)) - 0.5*fabs(nx*u_ + ny*v_ + nz*w_ + nt*ut)*(qR(iSens) - qL(iSens));
  }

  // strong advective flux: div.F
  template <class Tp, class Tq, class Tg, class Tf>
  void strongFlux( const Tp& param, const Real& x, const Real& y, const Real& z, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                   ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE(iSens) += u_*qx(iSens) + v_*qy(iSens) + w_*qz(iSens);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real u_, v_, w_;      // uniform advection velocity
};

} //namespace SANS

#endif  // SENSOR_ADVECTIVEFLUX2D_H
