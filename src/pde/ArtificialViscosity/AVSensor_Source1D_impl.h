// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

// No include blocker here. This file should only be included once in a cpp file for explicit instantiation

#if !defined(AVSENSOR1D_INSTANTIATE) && !defined(SANS_HEADERCOMPILE_CHECK)
#error "This file should only be included in a cpp file for explicit instantiations"
#endif

#include "AVSensor_Source1D.h"

#include <string>
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize>
void
AVSensor_Source1D_Uniform<PDETraitsSize>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AVSensor_Source1D_Uniform:" << std::endl;
  out << indent << "  a_ = " << a_ << std::endl;
}

//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize, class Sensor>
void
AVSensor_Source1D_Jump<PDETraitsSize, Sensor>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "AVSensor_Source1D_Jump:" << std::endl;
  out << indent << "  PDEorder_ = " << PDEorder_ << std::endl;
  out << indent << "  C1_ = " << C1_ << std::endl;
  out << indent << "  C3_ = " << C3_ << std::endl;
}

} //namespace SANS
