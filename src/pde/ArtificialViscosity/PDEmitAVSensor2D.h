// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEARTIFICIALVISCOSITY2D_H
#define PDEARTIFICIALVISCOSITY2D_H

// PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "AVVariable.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"
#include "tools/smoothmath.h"

#include <iostream>
#include <limits>
#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 2D Artificial Viscosity wrapper
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: U(Q)
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEmitAVSensor2D : public PDETraitsModel::PDEBase
{
public:

  typedef PhysD2 PhysDim;

  typedef typename PDETraitsModel::PDEBase BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  static_assert( D == PDETraitsSize<PhysDim>::D, "Dimension mismatch!");
  static_assert( N == PDETraitsSize<PhysDim>::N, "Solution variable mismatch!");

  static const int Nparam = 1;               // total solution parameters

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  template <class TP>
  using ArrayParam = DLA::VectorS<Nparam,TP>;  // parameter array type

  template <class T>
  using MatrixParam = DLA::MatrixS<N,Nparam,T>;         // e.g. jacobian of PDEs w.r.t. parameters

  typedef typename PDETraitsModel::AdvectiveFlux AdvectiveFlux; //advective flux for sensor PDE
  typedef typename PDETraitsModel::ViscousFlux ViscousFlux; //viscous flux for sensor PDE
  typedef typename PDETraitsModel::Source Source; //source term for sensor PDE

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs >
  PDEmitAVSensor2D(const AdvectiveFlux& adv, const ViscousFlux& visc, const Source& src,
                           const bool isSensorSteady, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    adv_(adv),
    visc_(visc),
    source_(src),
    isSensorSteady_(isSensorSteady),
    C1_(3.0) // Need a better way to sort that
  {
    //Nothing
  }

  ~PDEmitAVSensor2D() {}

  PDEmitAVSensor2D& operator=( const PDEmitAVSensor2D& ) = delete;

  static const int iSens = PDETraitsSize<PhysDim>::iSens;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return ( BaseType::hasFluxAdvective() || adv_.hasFluxAdvective() ); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return ( BaseType::hasSource() || source_.hasSourceTerm() ); }
  bool hasSourceTrace() const { return ( BaseType::hasSourceTrace() || source_.hasSourceTrace() ); }
  bool hasSourceLiftedQuantity() const { return source_.hasSourceLiftedQuantity(); }
  bool hasForcingFunction() const { return BaseType::hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const
  {
    return ( BaseType::needsSolutionGradientforSource() || source_.needsSolutionGradientforSource() );
  }

  // unsteady temporal flux: Ft(Q)
  template <int Dim, class T, class Tf>
  void fluxAdvectiveTime(
      const DLA::MatrixSymS<Dim,Real>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(param, x, y, time, q, ft);
    if (!isSensorSteady_)
    {
      DLA::MatrixSymS<Dim,Real> H = exp(param); //generalized h-tensor
      DLA::MatrixSymS<Dim,Real> Hsq = H*H;

      T tau = 1.0;
      typedef DLA::VectorS<Dim, Real> VectorX;
      VectorX hPrincipal;
      DLA::EigenValues(H, hPrincipal);

      //Get minimum length scale
      Real hmin = std::numeric_limits<Real>::max();
      for (int d = 0; d < D; d++)
        hmin = min(hmin, hPrincipal[d]);

      T lambda = 0;
      speedCharacteristic( param, x, y, time, q, lambda );

      // characteristic time of the PDE
      tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
      ft(iSens) += tau*q(iSens);
    }
  }

  // unsteady temporal flux: Ft(Q)
  template <int Dim, class T, class Tf>
  void fluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<Dim,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    fluxAdvectiveTime(param.left(), x, y, time, q, ft);
  }

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template<class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const DLA::MatrixSymS<D, Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template<class T, class Tp, class Tf>
  void jacobianFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<D, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template<class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const DLA::MatrixSymS<D+1, Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template<class T, class Tp, class Tf>
  void jacobianFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<D+1, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // master state: U(Q)
  template <class T, class Tp, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class Tp, class Tf>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& J ) const
  {
    BaseType::jacobianMasterState(param, x, y, time, q, J);
    J(iSens,iSens) = DLA::Identity();
  }

  // master state Gradient: Ux(Q)
  template<class Tp, class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& Ux, ArrayQ<Tf>& Uy  ) const
  {
    BaseType::masterStateGradient(param, x, y, time, q, qx, qy, Ux, Uy);
    Ux[iSens] = qx[iSens];
    Uy[iSens] = qy[iSens];
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tp, class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& Uxx, ArrayQ<Tf>& Uxy, ArrayQ<Tf>& Uyy ) const
  {
    BaseType::masterStateHessian(param, x, y, time, q, qx, qy, qxx, qxy, qyy, Uxx, Uxy, Uyy);
    Uxx[iSens] = qxx[iSens];
    Uxy[iSens] = qxy[iSens];
    Uyy[iSens] = qyy[iSens];
  }

  template<class Tp, class Tq, class Tqp, class Tf>
  void perturbedMasterState(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& dq, ArrayQ<Tf>& dU ) const
  {
    SANS_ASSERT(isSensorSteady_);
    BaseType::perturbedMasterState(param, x, y, time, q, dq, dU);
    dU(iSens) = dq(iSens);
  }

  // unsteady conservative flux: d(U)/d(t)
  template <int Dim, class T, class Tf>
  void strongFluxAdvectiveTime(
      const DLA::MatrixSymS<Dim, Real>& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<Tf>& dudt ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <int Dim, class T, class Tp, class Tf>
  void strongFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<Dim, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<Tf>& dudt ) const;

  // advective flux: F(Q)
  template <class T, class Tp, class Ts>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Ts>& f, ArrayQ<Ts>& g ) const;

  template <class T, class Tp, class Ts>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, ArrayQ<Ts>& f, const Real& scaleFactor = 1 ) const;

  template <class T, class Tp, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class Tq, class Ta>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x, const Real& y,  const Real& time, const ArrayQ<Tq>& q,
      MatrixQ<Ta>& ax, MatrixQ<Ta>& ay ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class T, class Ta>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const Real& nx, const Real& ny,
      MatrixQ<Ta>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <int Dim, class Tp, class T, class Ta>
  void jacobianFluxAdvectiveAbsoluteValue(
      const ParamTuple<DLA::MatrixSymS<Dim, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, const Real& nx, const Real& ny,
      MatrixQ<Ta>& a ) const
  {
    jacobianFluxAdvectiveAbsoluteValue(param.right(), x, y, time, q, nx, ny, a);
  }

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Ta>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nt,
      MatrixQ<Ta>& a ) const;

  // strong form advective fluxes
  template <class Tp, class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& strongPDE ) const;

  // viscous flux: Fv(Q, Qx)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const;

  // perturbation to viscous flux from dux, duy
  template <class Tp, class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;


  // perturbation to viscous flux from dux, duy
  template <class Tp, class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tp, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const;

  // strong form viscous fluxes
  template <class Tp, class Tq, class Tg,class Th, class Tf>
  void strongFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      ArrayQ<Tf>& strongPDE ) const;

  // space-time viscous flux: Fv(Q, QX)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt,
      ArrayQ<Tf>& f ) const;

  // space-time viscous diffusion matrix: d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
      MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const;

  // space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class Tq, class Tg, class Tf>
  void jacobianFluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const;

  // space-time strong form viscous fluxes: -d(Fv)/dx - d(Fv)/d(y)
  template <class Tp, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscousSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
      ArrayQ<Tf>& strongPDE ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceCoarse(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source: S(X, Q, QX)
  template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
  void sourceFine(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
      const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Ts>& source ) const;

  // dual-consistent source
  template <class Tq, class Tp, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Ts>& dsdu ) const
  {
//    SANS_DEVELOPER_EXCEPTION("2D AV JACOBIANSOURCEABSOLUTEVALUE NOT IMPLEMENTED");
    BaseType::jacobianSourceAbsoluteValue(param, x, y, time, q, qx, qy, dsdu);
  }

  template <class Tp, class Tq, class Tg, class Ts>
    void jacobianGradientSource(
        const Tp& param, const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const;

  template <class Tp, class Tq, class Tg, class Ts>
    void jacobianGradientSourceHACK(
        const Tp& param, const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const;

  template <class Tp, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradient(
        const Tp& param, const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        MatrixQ<Ts>& div_dsgradu ) const
  {
//    SANS_DEVELOPER_EXCEPTION("2D AV JACOBIANGRADIENTSOURCEGRADIENT NOT IMPLEMENTED");
    BaseType::jacobianGradientSourceGradient(param, x, y, time, q, qx, qy, qxx, qxy, qyy, div_dsgradu);
  }

  template <class Tp, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradientHACK(
        const Tp& param, const Real& x, const Real& y, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        MatrixQ<Ts>& div_dsgradu ) const
  {
    SANS_DEVELOPER_EXCEPTION("2D AV HACKED JACOBIANGRADIENTSOURCEGRADIENT NOT IMPLEMENTED");
  }

  // right-hand-side forcing function
  template <class T, class Tp>
  void forcingFunction( const Tp& param,
                        const Real& x, const Real& y, const Real& time, ArrayQ<T>& source ) const;

  // characteristic speed (needed for timestep)
  template <class T, class Tp>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T, class Tp>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, T& speed ) const;

  // characteristic speed
  template <class T>
  void speedCharacteristic(
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, T& speed ) const;

  // update fraction needed for physically valid state
  template <class Tp>
  void updateFraction(
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(param, x, y, time, q, dq, maxChangeFraction, updateFraction);
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const;

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const;

  // set from primitive variable
  template<class T, class Variables>
  void setDOFFrom(
      ArrayQ<T>& q, const AVVariableType<Variables>& data ) const;

  // set from primitive variable
  template<class Variables>
  ArrayQ<Real> setDOFFrom( const AVVariableType<Variables>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const AdvectiveFlux adv_;
  const ViscousFlux visc_;
  const Source source_;
  const bool isSensorSteady_;
  const Real C1_; // time scaling
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Tu>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::masterState(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  BaseType::masterState(param, x, y, time, q, uCons);
  uCons(iSens) = q(iSens);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class T, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
jacobianFluxAdvectiveTime(
      const DLA::MatrixSymS<D, Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
  BaseType::jacobianFluxAdvectiveTime(param, x, y, time, q, dudq);
  if (!isSensorSteady_)
  {
    const DLA::MatrixSymS<D, Real> H = exp(param); //generalized h-tensor
    const DLA::MatrixSymS<D, Real> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, y, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].value() = q[i];
    }

    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, y, time, qSurreal, dlambda_dq );

      qSurreal[i].deriv() = 0.0;

      dudq(iSens,i) += q(iSens)*dtau_dlambda*dlambda_dq.deriv();
    }

    dudq(iSens,iSens) += tau;
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class T, class Tp, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
jacobianFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<D, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
  BaseType::jacobianFluxAdvectiveTime(param, x, y, time, q, dudq);
  if (!isSensorSteady_)
  {
    DLA::MatrixSymS<D, Real> H = exp(param.left()); //generalized h-tensor
    DLA::MatrixSymS<D, Real> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, y, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].value() = q[i];
    }

    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, y, time, qSurreal, dlambda_dq );

      qSurreal[i].deriv() = 0.0;

      dudq(iSens,i) += q(iSens)*dtau_dlambda*dlambda_dq.deriv();
    }

    dudq(iSens,iSens) += tau;
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class T, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
jacobianFluxAdvectiveTime(
      const DLA::MatrixSymS<D+1, Real>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
  BaseType::jacobianFluxAdvectiveTime(param, x, y, time, q, dudq);
  if (!isSensorSteady_)
  {
    const DLA::MatrixSymS<D+1, Real> H = exp(param); //generalized h-tensor
    const DLA::MatrixSymS<D+1, Real> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<D+1, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D+1; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, y, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].value() = q[i];
    }

    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, y, time, qSurreal, dlambda_dq );

      qSurreal[i].deriv() = 0.0;

      dudq(iSens,i) += q(iSens)*dtau_dlambda*dlambda_dq.deriv();
    }

    dudq(iSens,iSens) += tau;
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class T, class Tp, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
jacobianFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<D+1, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
  BaseType::jacobianFluxAdvectiveTime(param, x, y, time, q, dudq);
  if (!isSensorSteady_)
  {
    DLA::MatrixSymS<D+1, Real> H = exp(param.left()); //generalized h-tensor
    DLA::MatrixSymS<D+1, Real> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<D+1, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D+1; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, y, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = q;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, y, time, qSurreal, dlambda_dq );

      qSurreal[i].deriv() = 0.0;

      dudq(iSens,i) += q(iSens)*dtau_dlambda*dlambda_dq.deriv();
    }

    dudq(iSens,iSens) += tau;
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <int Dim, class T, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const DLA::MatrixSymS<Dim, Real>& param,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<Tf>& dudt ) const
{
  BaseType::strongFluxAdvectiveTime(param, x, y, time, q, qt, dudt);
  if (!isSensorSteady_)
  {
    DLA::MatrixSymS<Dim, Real> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<Dim, Real> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<Dim, Real> VectorXT;
    VectorXT hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < Dim; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, y, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = q;
    T dlambda_dt = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, y, time, qSurreal, dlambda_dq );
      dlambda_dt += dlambda_dq.deriv() * qt(i);

      qSurreal[i].deriv() = 0.0;
    }
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    dudt(iSens) += tau*qt(iSens) + dtau_dlambda*dlambda_dt*q(iSens);
  }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <int Dim, class T, class Tp, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const ParamTuple<DLA::MatrixSymS<Dim, Real>, Tp, TupleClass<> >& param,
    const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<Tf>& dudt ) const
{
  BaseType::strongFluxAdvectiveTime(param, x, y, time, q, qt, dudt);
  if (!isSensorSteady_)
  {
    DLA::MatrixSymS<Dim, Real> H = exp(param.left()); //generalized h-tensor
    DLA::MatrixSymS<Dim, Real> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<Dim, Real> VectorXT;
    VectorXT hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < Dim; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, y, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = q;
    T dlambda_dt = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, y, time, qSurreal, dlambda_dq );
      dlambda_dt += dlambda_dq.deriv() * qt(i);

      qSurreal[i].deriv() = 0.0;
    }
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    dudt(iSens) += tau*qt(iSens) + dtau_dlambda*dlambda_dt*q(iSens);
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Ts>& fx, ArrayQ<Ts>& fy ) const
{
  BaseType::fluxAdvective(param, x, y, time, q, fx, fy);
  adv_.flux( param, x, y, time, q, fx, fy );
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny,
    ArrayQ<Ts>& f, const Real& scaleFactor ) const
{
  BaseType::fluxAdvectiveUpwind(param, x, y, time, qL, qR, nx, ny, f, scaleFactor);
  adv_.fluxUpwind(param, x, y, time, qL, qR, nx, ny, f, scaleFactor);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nt,
    ArrayQ<Tf>& f ) const
{
  BaseType::fluxAdvectiveUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, f);
  adv_.fluxUpwindSpaceTime(param, x, y, time, qL, qR, nx, ny, nt, isSensorSteady_, f);
}

// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Ta>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Tp& param, const Real& x, const Real& y, const Real& time, const ArrayQ<Tq>& q,
    MatrixQ<Ta>& ax, MatrixQ<Ta>& ay ) const
{
  BaseType::jacobianFluxAdvective(param, x, y, time, q, ax, ay);
  adv_.jacobian( param, x, y, time, q, ax, ay );
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class T, class Ta>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Tp& param, const Real& x, const Real& y, const Real& time,
  const ArrayQ<T>& q, const Real& nx, const Real& ny, MatrixQ<Ta>& mtx ) const
{
//  SANS_DEVELOPER_EXCEPTION("jacobianFluxAdvectiveAbsoluteValue not implemented yet.");

  BaseType::jacobianFluxAdvectiveAbsoluteValue(param, x, y, time, q, nx, ny, mtx);
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Ta>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Tp& param, const Real& x, const Real& y, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nt, MatrixQ<Ta>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor2D::jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented");
}

// strong form of advective flux: d(F)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& strongPDE ) const
{
  BaseType::strongFluxAdvective( param, x, y, time, q, qx, qy, strongPDE );
  adv_.strongFlux( param, x, y, time, q, qx, qy, strongPDE );
}

// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy ) const
{
  BaseType::fluxViscous( param, x, y, time, q, qx, qy, fx, fy );
  visc_.fluxViscous( *this, param, x, y, time, q, qx, qy, fx, fy );
}

// space-time viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
{
  BaseType::fluxViscousSpaceTime( param, x, y, time, q, qx, qy, qt, fx, fy, ft );
  visc_.fluxViscousSpaceTime( *this, param, x, y, time, q, qx, qy, qt, fx, fy, ft );
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const
{
  BaseType::fluxViscous( paramL, paramR, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
  visc_.fluxViscous( *this, paramL, paramR, x, y, time, qL, qxL, qyL, qR, qxR, qyR, nx, ny, fn );
}

// perturbation to viscous flux from dux, duy
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tgu, class Tf>
void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::perturbedGradFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
    ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
{
  BaseType::perturbedGradFluxViscous(param, x, y, time, q, qx, qy, dqx, dqy, df, dg);
  visc_.perturbedGradFluxViscous( *this, param, x, y, time, q, qx, qy, dqx, dqy, df, dg );
}
// perturbation to viscous flux from dux, duy
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tk, class Tgu, class Tf>
void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::perturbedGradFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
    const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
    ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
{
  BaseType::perturbedGradFluxViscous(param, x, y, time, Kxx, Kxy, Kyx, Kyy, dux, duy, df, dg);
  visc_.perturbedGradFluxViscous(*this, param, x, y, time, Kxx, Kxy, Kyx, Kyy, dux, duy, df, dg);
}

// space-time viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::fluxViscousSpaceTime(
    const Tp& paramL, const Tp& paramR, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
    const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& fn ) const
{
  BaseType::fluxViscousSpaceTime( paramL, paramR, x, y, time, qL, qxL, qyL, qtL, qR, qxR, qyR, qtR, nx, ny, nt, fn );
  visc_.fluxViscousSpaceTime( *this, paramL, paramR, x, y, time, qL, qxL, qyL, qtL, qR, qxR, qyR, qtR, nx, ny, nt, fn );
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy ) const
{
//  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor2D::diffusionViscous is not implemented");

#if 1
  BaseType::diffusionViscous( param, x, y, time,
                              q, qx, qy,
                              kxx, kxy,
                              kyx, kyy );
  visc_.diffusionViscous( *this, param, x, y, time,
                          q, qx, qy,
                          kxx, kxy,
                          kyx, kyy );
#endif
}


// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Th, class Tk>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::diffusionViscousGradient(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y) const
{
  BaseType::diffusionViscousGradient( param, x, y, time,
                                      q, qx, qy,
                                      qxx, qxy, qyy,
                                      kxx_x, kxy_x, kyx_x,
                                      kxy_y, kyx_y, kyy_y );
  visc_.diffusionViscousGradient( *this, param, x, y, time,
                                  q, qx, qy,
                                  qxx, qxy, qyy,
                                  kxx_x, kxy_x, kyx_x,
                                  kxy_y, kyx_y, kyy_y);
}

// space-time viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tk>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::diffusionViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
    MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt) const
{
  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor2D::diffusionViscousSpaceTime is not implemented");
}

// strong form of viscous flux: d(Fv)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
jacobianFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
{
//  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor2D::strongFluxViscous is not implemented");

  BaseType::jacobianFluxViscous( param, x, y, time, q, qx, qy, dfdu, dgdu );
  visc_.jacobianFluxViscous( *this, param, x, y, time, q, qx, qy, dfdu, dgdu );
}

// strong form of viscous flux: d(Fv)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Th, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    ArrayQ<Tf>& strongPDE ) const
{
//  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor2D::strongFluxViscous is not implemented");

  BaseType::strongFluxViscous( param, x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE );
  visc_.strongFluxViscous( *this, param, x, y, time, q, qx, qy, qxx, qxy, qyy, strongPDE );
}

// space-time jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
jacobianFluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& at ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor2D::jacobianFluxViscousSpaceTime is not implemented");
}

// space-time strong form of viscous flux: d(Fv)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Th, class Tf>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::strongFluxViscousSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxt, const ArrayQ<Th>& qyt, const ArrayQ<Th>& qtt,
    ArrayQ<Tf>& strongPDE ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor2D::strongFluxViscousSpaceTime is not implemented");
}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::source(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  BaseType::source( param, x, y, time, q, qx, qy, source );
  source_.source( *this, param, x, y, time, q, qx, qy, source );
}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::source(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
    ArrayQ<Ts>& src ) const
{
  typedef typename promote_Surreal<Tq, Tqp>::type Tqq;
  typedef typename promote_Surreal<Tg, Tgp>::type Tgg;

  ArrayQ<Tqq> qq = q + qp;
  ArrayQ<Tgg> qqx = qx + qpx;
  ArrayQ<Tgg> qqy = qy + qpy;

  BaseType::source( param, x, y, time, qq, qqx, qqy, src);
  source_.source( *this, param, x, y, time, qq, qqx, qqy, src);
}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::sourceCoarse(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
    ArrayQ<Ts>& src ) const
{
  BaseType::sourceCoarse( param, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  source_.sourceCoarse( *this, param, x, y, time, q, qp, qx, qy, qpx, qpy, src);
}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tqp, class Tg, class Tgp, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::sourceFine(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tqp>& qp,
    const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    const ArrayQ<Tgp>& qpx, const ArrayQ<Tgp>& qpy,
    ArrayQ<Ts>& src ) const
{
  BaseType::sourceFine( param, x, y, time, q, qp, qx, qy, qpx, qpy, src);
  source_.sourceFine( *this, param, x, y, time, q, qp, qx, qy, qpx, qpy, src);
}

// solution-dependent source with lifted_quantity: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tlq, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::source(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    ArrayQ<Ts>& source ) const
{
  BaseType::source( param, x, y, time, lifted_quantity, q, qx, qy, source );
  source_.source( *this, param, x, y, time, lifted_quantity, q, qx, qy, source );
}

// dual-consistent source
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tp, class Tg, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::sourceTrace(
    const Tp& paramL, const Real& xL, const Real& yL,
    const Tp& paramR, const Real& xR, const Real& yR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  BaseType::sourceTrace(paramL, xL, yL, paramR, xR, yR, time,
                        qL, qxL, qyL, qR, qxR, qyR,
                        sourceL, sourceR);

  source_.sourceTrace(*this, paramL, xL, yL, paramR, xR, yR, time,
                      qL, qxL, qyL, qR, qxR, qyR,
                      sourceL, sourceR);
}

// trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::sourceLiftedQuantity(
    const Tp& paramL, const Real& xL, const Real& yL,
    const Tp& paramR, const Real& xR, const Real& yR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    Ts& s ) const
{
  source_.sourceLiftedQuantity(paramL, xL, yL, paramR, xR, yR, time, qL, qR, s);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::jacobianSource(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdu ) const
{
  BaseType::jacobianSource(param, x, y, time, q, qx, qy, dsdu);
  source_.jacobianSource(*this, param, x, y, time, q, qx, qy, dsdu);
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::jacobianSourceHACK(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdu ) const
{
  BaseType::jacobianSourceHACK(param, x, y, time, q, qx, qy, dsdu);
  source_.jacobianSource(*this, param, x, y, time, q, qx, qy, dsdu);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::jacobianGradientSource(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
{
  BaseType::jacobianGradientSource(param, x, y, time, q, qx, qy, dsdux, dsduy);
  source_.jacobianGradientSource(*this, param, x, y, time, q, qx, qy, dsdux, dsduy);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceHACK(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy ) const
{
  BaseType::jacobianGradientSourceHACK(param, x, y, time, q, qx, qy, dsdux, dsduy);
  source_.jacobianGradientSource(*this, param, x, y, time, q, qx, qy, dsdux, dsduy);
}


// right-hand-side forcing function
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::forcingFunction(
    const Tp& param, const Real& x, const Real& y, const Real& time, ArrayQ<T>& forcing ) const
{
  BaseType::forcingFunction(param, x, y, time, forcing );
}


// characteristic speed (needed for timestep)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const Real& dx, const Real& dy, const ArrayQ<T>& q, T& speed ) const
{
  BaseType::speedCharacteristic(param, x, y, time, dx, dy, q, speed);
}


// characteristic speed
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::speedCharacteristic(
    const Tp& param, const Real& x, const Real& y, const Real& time,
    const ArrayQ<T>& q, T& speed ) const
{
  BaseType::speedCharacteristic(param, x, y, time, q, speed);
}


// is state physically valid
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline bool
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::isValidState( const ArrayQ<Real>& q ) const
{
  return BaseType::isValidState(q);
}


// set from primitive variable array
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::setDOFFrom(
    ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
{
  BaseType::setDOFFrom(q, data, name, nn);
}

// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Variables>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( ArrayQ<T>& q, const AVVariableType<Variables>& data ) const
{
  BaseType::setDOFFrom(q, data.cast());
  q(iSens) = data.cast().Sensor;
}

// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Variables>
inline typename PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( const AVVariableType<Variables>& data ) const
{
  ArrayQ<Real> q;
  setDOFFrom( q, data );
  return q;
}

// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q = BaseType::setDOFFrom(d);
  //q(iSens) = d.get("Sensor"); TODO
  return q;
}

// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::
interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  BaseType::interpResidVariable(rsdPDEIn, rsdPDEOut);
  rsdPDEOut[BaseType::nMonitor()-1] = 0; // No residual for artificial viscosity
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEmitAVSensor2D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEmitAVSensor2D: adv_ =" << std::endl;
  adv_.dump(indentSize+2, out);
  out << indent << "PDEmitAVSensor2D: visc_ =" << std::endl;
  visc_.dump(indentSize+2, out);
  out << indent << "PDEmitAVSensor2D: source_ =" << std::endl;
  source_.dump(indentSize+2, out);
  out << indent << "PDEmitAVSensor2D: pde_ =" << std::endl;
  BaseType::dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEARTIFICIALVISCOSITY2D_H
