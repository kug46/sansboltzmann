// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AVSENSOR_VISCOUSFLUX2D_H
#define AVSENSOR_VISCOUSFLUX2D_H

// 2-D Advection-Diffusion PDE: diffusion matrix

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include <iostream>
#include <limits>

namespace SANS
{

//----------------------------------------------------------------------------//
template<class DiffusionMatrix>
class AVSensor_ViscousFlux2D_LinearInGradient : public DiffusionMatrix
{
public:
  using DiffusionMatrix::diffusionViscous;
  using DiffusionMatrix::diffusionViscousSpaceTime;

  template <class T>
  using ArrayQ = typename DiffusionMatrix::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename DiffusionMatrix::template MatrixQ<T>;  // matrices

  static const int iSens = DiffusionMatrix::iSens;

  template< class... Args > // cppcheck-suppress noExplicitConstructor
  AVSensor_ViscousFlux2D_LinearInGradient(Args&&... args) : DiffusionMatrix(std::forward<Args>(args)...) {}

  bool fluxViscousLinearInGradient() const { return true; }

  // viscous flux: Fv(Q, QX)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g ) const
  {
    MatrixQ<Tq> kxx=0, kxy=0,
                kyx=0, kyy=0;

    diffusionViscous( pde, param, x, y, time,
                      q, qx, qy,
                      kxx, kxy,
                      kyx, kyy );

    f(iSens) -= kxx(iSens,iSens)*qx(iSens) + kxy(iSens,iSens)*qy(iSens);
    g(iSens) -= kyx(iSens,iSens)*qx(iSens) + kyy(iSens,iSens)*qy(iSens);
  }

  // space-time viscous flux: Fv(Q, QX)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& ft ) const
  {
    MatrixQ<Tq> kxx = 0, kxy = 0, kxt = 0,
                kyx = 0, kyy = 0, kyt = 0,
                ktx = 0, kty = 0, ktt = 0;

    diffusionViscousSpaceTime( pde, param, x, y, time,
                               q, qx, qy, qt,
                               kxx, kxy, kxt,
                               kyx, kyy, kyt,
                               ktx, kty, ktt );

    fx(iSens) -= kxx(iSens,iSens)*qx(iSens) + kxy(iSens,iSens)*qy(iSens) + kxt(iSens,iSens)*qt(iSens);
    fy(iSens) -= kyx(iSens,iSens)*qx(iSens) + kyy(iSens,iSens)*qy(iSens) + kyt(iSens,iSens)*qt(iSens);
    ft(iSens) -= ktx(iSens,iSens)*qx(iSens) + kty(iSens,iSens)*qy(iSens) + ktt(iSens,iSens)*qt(iSens);
  }

  // viscous flux: normal flux with left and right states
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const PDE& pde, const Tp& paramL, const Tp& paramR,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR,
      const Real& nx, const Real& ny, ArrayQ<Tf>& fn ) const
  {
    ArrayQ<Tf> fxL = 0, fxR = 0;
    ArrayQ<Tf> fyL = 0, fyR = 0;

    fluxViscous(pde, paramL, x, y, time, qL, qxL, qyL, fxL, fyL);
    fluxViscous(pde, paramR, x, y, time, qR, qxR, qyR, fxR, fyR);

    fn(iSens) += 0.5*(fxL(iSens) + fxR(iSens))*nx + 0.5*(fyL(iSens) + fyR(iSens))*ny;
  }

  // space-time viscous flux: normal flux with left and right states
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscousSpaceTime(
      const PDE& pde, const Tp& paramL, const Tp& paramR,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qtL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qtR,
      const Real& nx, const Real& ny, const Real& nt, ArrayQ<Tf>& fn ) const
  {
    ArrayQ<Tf> fxL = 0, fxR = 0;
    ArrayQ<Tf> fyL = 0, fyR = 0;
    ArrayQ<Tf> ftL = 0, ftR = 0;

    fluxViscousSpaceTime(pde, paramL, x, y, time, qL, qxL, qyL, qtL, fxL, fyL, ftL);
    fluxViscousSpaceTime(pde, paramR, x, y, time, qR, qxR, qyR, qtR, fxR, fyR, ftR);

    fn(iSens) += 0.5*(fxL(iSens) + fxR(iSens))*nx + 0.5*(fyL(iSens) + fyR(iSens))*ny + 0.5*(ftL(iSens) + ftR(iSens))*nt;
  }
};

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize>
class AVSensor_DiffusionMatrix2D_Uniform
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  static const int iSens = PDETraitsSize<PhysD2>::iSens;

  explicit AVSensor_DiffusionMatrix2D_Uniform( const Real kxx, const Real kxy, const Real kyx, const Real kyy ) :
      kxx_(kxx), kxy_(kxy),
      kyx_(kyx), kyy_(kyy) {}

  template <class PDE, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde, const Tp& param,
                         const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const
  {
    kxx(iSens, iSens) += kxx_; kxy(iSens, iSens) += kxy_;
    kyx(iSens, iSens) += kyx_; kyy(iSens, iSens) += kyy_;
  }

  template <class PDE, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime( const PDE& pde, const Tp& param,
                                  const Real x, const Real y, const Real time,
                                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
                                  MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
                                  MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
                                  MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt ) const
  {
    diffusionViscous(pde, param, x, y, time, q, qx, qy, kxx, kxy, kyx, kyy);
  }

  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  inline void jacobianFluxViscous(
      const PDE& pde, const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
  {
    // Nothing ... I think...
  }

  template <class PDE, class Tp,  class Tq, class Tg, class Th, class Tk>
  inline void diffusionViscousGradient(
      const PDE& pde, const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y) const
  {
    // Zero
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class T, class Tp>
  inline void
  strongFluxViscous( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
                     const ArrayQ<T>& q, const ArrayQ<T>& qx, const ArrayQ<T>& qy,
                     const ArrayQ<T>& qxx,
                     const ArrayQ<T>& qyx, const ArrayQ<T>& qyy,
                     ArrayQ<T>& strongPDE ) const
  {
    strongPDE(iSens) -= kxx_*qxx(iSens) + (kxy_ + kyx_)*qyx(iSens) + kyy_*qyy(iSens);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real kxx_, kxy_, kyx_, kyy_;          // constant diffusion matrix
};

template <template <class> class PDETraitsSize>
using AVSensor_ViscousFlux2D_Uniform = AVSensor_ViscousFlux2D_LinearInGradient<AVSensor_DiffusionMatrix2D_Uniform<PDETraitsSize>>;

//----------------------------------------------------------------------------//
// diffusion matrix: Grid Dependent
//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize>
class AVSensor_DiffusionMatrix2D_GridDependent
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  static const int iSens = PDETraitsSize<PhysD2>::iSens;

  bool hasFluxViscous() const { return true; }

  explicit AVSensor_DiffusionMatrix2D_GridDependent( const Real C ) :
      C_(C)
  {
    // Nothing
  }

  template <class PDE, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde, const Real& param,
                         const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const
  {
    const Real& h = param;
    Real K = C_*h*h;
    kxx(iSens,iSens) += K; kxy(iSens,iSens) += 0;
    kyx(iSens,iSens) += 0; kyy(iSens,iSens) += K;
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  inline void
  strongFluxViscous( const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                     const ArrayQ<Tg>& qxx,
                     const ArrayQ<Tg>& qyx, const ArrayQ<Tg>& qyy,
                     ArrayQ<Tf>& strongPDE ) const
  {
    const Tp& h = param;
    Tp K = C_*h*h;

    strongPDE(iSens) -= K*qxx(iSens) + K*qyy(iSens);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real C_; // constant scaling
};

template <template <class> class PDETraitsSize>
using AVSensor_ViscousFlux2D_GridDependent = AVSensor_ViscousFlux2D_LinearInGradient<AVSensor_DiffusionMatrix2D_GridDependent<PDETraitsSize>> ;

//----------------------------------------------------------------------------//
// Diffusion matrix with generalized h-scale
// Formulation from Masa Yano's PhD thesis - page 191
//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize>
class AVSensor_DiffusionMatrix2D_GenHScale
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD2>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD2>::template MatrixQ<T>;  // matrices

  static const int D = 2;                     // physical dimensions

  typedef DLA::VectorS<D, Real> VectorX;
  typedef DLA::VectorS<D+1, Real> VectorXT;

  static const int iSens = PDETraitsSize<PhysD2>::iSens;

  bool hasFluxViscous() const { return true; }

  explicit AVSensor_DiffusionMatrix2D_GenHScale( const Real order, const bool hasSpaceTimeDiffusion = false,
                                                 const Real C1 = 3, const Real C2 = 5) :
    order_(order),
    hasSpaceTimeDiffusion_(hasSpaceTimeDiffusion),
    C1_(C1),
    C2_(C2)
  {
    // Nothing
  }

  template <class PDE, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde, const Tp& param,
                         const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const
  {
    SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

    //Tp may be of type DLA::MatrixSymS<D,Real> or DLA::MatrixSymS<D+1,Real>
    Tp H = exp(param); //generalized h-tensor
    Tp Hsq = H*H;

    kxx(iSens,iSens) += C2_*Hsq(0,0);
    kxy(iSens,iSens) += C2_*Hsq(0,1);
    kyx(iSens,iSens) += C2_*Hsq(1,0);
    kyy(iSens,iSens) += C2_*Hsq(1,1);
  }

  template <class PDE, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde,
                         const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
                         const Real x, const Real y, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy) const
  {
    SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

    //Tp may be of type DLA::MatrixSymS<D,Real> or DLA::MatrixSymS<D+1,Real>
    DLA::MatrixSymS<D,Real> H = exp(param.left()); //generalized h-tensor
    DLA::MatrixSymS<D,Real> Hsq = H*H;

    kxx(iSens,iSens) += C2_*Hsq(0,0);
    kxy(iSens,iSens) += C2_*Hsq(0,1);
    kyx(iSens,iSens) += C2_*Hsq(1,0);
    kyy(iSens,iSens) += C2_*Hsq(1,1);
  }

  template <class PDE, class Tq, class Tg, class Tk>
  void diffusionViscousSpaceTime( const PDE& pde, const DLA::MatrixSymS<D+1,Real>& param,
                                  const Real x, const Real y, const Real time,
                                  const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qt,
                                  MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxt,
                                  MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyt,
                                  MatrixQ<Tk>& ktx, MatrixQ<Tk>& kty, MatrixQ<Tk>& ktt) const
  {
    DLA::MatrixSymS<D+1,Real> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D+1,Real> Hsq = H*H;

    kxx(iSens,iSens) += C2_*Hsq(0,0);
    kxy(iSens,iSens) += C2_*Hsq(0,1);

    kyx(iSens,iSens) += C2_*Hsq(1,0);
    kyy(iSens,iSens) += C2_*Hsq(1,1);

    if (hasSpaceTimeDiffusion_)
    {
      kxt(iSens,iSens) += C2_*Hsq(0,2);
      kyt(iSens,iSens) += C2_*Hsq(1,2);

      ktx(iSens,iSens) += C2_*Hsq(2,0);
      kty(iSens,iSens) += C2_*Hsq(2,1);
      ktt(iSens,iSens) += C2_*Hsq(2,2);
    }
  }

  // perturbation to viscous flux from dux, duy
  template <class PDE, class Tp, class Tq, class Tg, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const PDE& pde,
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Tgu>& dqx, const ArrayQ<Tgu>& dqy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
  {
    MatrixQ<Tq> kxx=0, kxy=0,
                kyx=0, kyy=0;

    diffusionViscous( pde, param, x, y, time,
                      q, qx, qy,
                      kxx, kxy,
                      kyx, kyy );

    df(iSens) -= kxx(iSens,iSens)*dqx(iSens) + kxy(iSens,iSens)*dqy(iSens);
    dg(iSens) -= kyx(iSens,iSens)*dqx(iSens) + kyy(iSens,iSens)*dqy(iSens);
  }


  // perturbation to viscous flux from dux, duy
  template <class PDE, class Tp, class Tk, class Tgu, class Tf>
  void perturbedGradFluxViscous(
      const PDE& pde,
      const Tp& param, const Real& x, const Real& y, const Real& time,
      const MatrixQ<Tk>& Kxx, const MatrixQ<Tk>& Kxy, const MatrixQ<Tk>& Kyx, const MatrixQ<Tk>& Kyy,
      const ArrayQ<Tgu>& dux, const ArrayQ<Tgu>& duy,
      ArrayQ<Tf>& df, ArrayQ<Tf>& dg ) const
  {

    df(iSens) -= Kxx(iSens,iSens)*dux(iSens) + Kxy(iSens,iSens)*duy(iSens);
    dg(iSens) -= Kyx(iSens,iSens)*dux(iSens) + Kyy(iSens,iSens)*duy(iSens);
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const PDE& pde, const DLA::MatrixSymS<D,Real>& param,
                     const Real& x, const Real& y, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     ArrayQ<Tf>& strongPDE ) const
  {
    SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

    DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D,Real> Hsq = H*H;

    strongPDE(iSens) -= C2_*( Hsq(0,0)*qxx(iSens) + (Hsq(0,1) + Hsq(1,0))*qyx(iSens) + Hsq(1,1)*qyy(iSens) );
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const PDE& pde,
                     const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
                     const Real& x, const Real& y, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     ArrayQ<Tf>& strongPDE ) const
  {
    SANS_ASSERT(!hasSpaceTimeDiffusion_); //space-time diffusion requires a space-time NDPDE

    DLA::MatrixSymS<D,Real> H = exp(param.left()); //generalized h-tensor
    DLA::MatrixSymS<D,Real> Hsq = H*H;

    strongPDE(iSens) -= C2_*( Hsq(0,0)*qxx(iSens) + (Hsq(0,1) + Hsq(1,0))*qyx(iSens) + Hsq(1,1)*qyy(iSens) );
  }

  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  inline void jacobianFluxViscous(
      const PDE& pde, const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      MatrixQ<Tf>& dfdu, MatrixQ<Tf>& dgdu ) const
  {
    // Nothing here - Fv(sensor) is independent of U(sensor)
  }

  template <class PDE, class Tp,  class Tq, class Tg, class Th, class Tk>
  inline void diffusionViscousGradient(
      const PDE& pde, const Tp& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y) const
  {
    // grad(K) = grad(H) = 0 (since we dont have parameter gradients)
  }

  template <class PDE, class Tq, class Tg, class Th, class Tk>
  inline void diffusionViscousGradient(
      const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Real>, Real, TupleClass<>>& param,
      const Real& x, const Real& y, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kyx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y) const
  {
    // grad(K) = grad(H) = 0 (since we dont have parameter gradients)
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real order_; // solution order
  const bool hasSpaceTimeDiffusion_;
  const Real C1_; // time scaling
  const Real C2_; // diffusion scaling
};

template <template <class> class PDETraitsSize>
using AVSensor_ViscousFlux2D_GenHScale = AVSensor_ViscousFlux2D_LinearInGradient<AVSensor_DiffusionMatrix2D_GenHScale<PDETraitsSize>>;

} //namespace SANS

#endif  // AVSENSOR_VISCOUSFLUX2D_H
