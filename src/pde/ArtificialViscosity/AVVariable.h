// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AVVARIABLE_H
#define AVVARIABLE_H

#include "Python/PyDict.h" // python must be included first
#include "Python/Parameter.h"

#include <initializer_list>

#include "tools/SANSnumerics.h" // Real
#include "tools/SANSException.h"

namespace SANS
{

//===========================================================================//
template<class Derived>
struct AVVariableType
{
  //A convenient method for casting to the derived type
  inline const Derived& cast() const { return static_cast<const Derived&>(*this); }
};

//---------------------------------------------------------------------------//
template<template <class> class BaseVar, class T>
struct AVVariable : public AVVariableType< AVVariable<BaseVar, T> >,
                      public BaseVar<T>
{
  AVVariable() {}

  AVVariable( const BaseVar<T>& var, const T& sensor )
    : BaseVar<T>(var), Sensor(sensor) {}

  AVVariable& operator=( const std::initializer_list<Real>& var ) //example: {{var1, var2}, var_sensor}
  {
    SANS_ASSERT(var.size() == 2);
    auto q = var.begin();

    BaseVar<T>::operator=(*(q+0));
    Sensor = *(q+1);

    return *this;
  }

  T Sensor;
};

}

#endif //AVVARIABLE_H
