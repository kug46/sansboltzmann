
INCLUDE( ${CMAKE_SOURCE_DIR}/CMakeInclude/ForceOutOfSource.cmake ) #This must be the first thing included

SET( ARTIFICIALVISCOSITY_SRC

	#AVSensor_AdvectiveFlux1D.cpp
	#AVSensor_AdvectiveFlux2D.cpp
	#AVSensor_ViscousFlux1D.cpp
	#AVSensor_ViscousFlux2D.cpp
  )

#ADD_LIBRARY( ArtificialViscosityLib STATIC ${ARTIFICIALVISCOSITY_SRC} )

# This should be deleted and replaced with the library if cpp files are added
ADD_CUSTOM_TARGET( ArtificialViscosityLib ) 

#Create the vera targets for this library
ADD_VERA_CHECKS_RECURSE( ArtificialViscosityLib *.h *.cpp )
ADD_HEADER_COMPILE_CHECK_RECURSE( ArtificialViscosityLib *.h )
#ADD_CPPCHECK( ArtificialViscosityLib ${ARTIFICIALVISCOSITY_SRC} )