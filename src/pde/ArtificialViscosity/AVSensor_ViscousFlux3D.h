// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AVSENSOR_VISCOUSFLUX3D_H
#define AVSENSOR_VISCOUSFLUX3D_H

// 3-D Artificial viscosity diffusion matrix

#include "tools/SANSnumerics.h"     // Real

#include "Topology/Dimension.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"

#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/Eigen.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"

#include <iostream>
#include <limits>

namespace SANS
{

//----------------------------------------------------------------------------//
template<class DiffusionMatrix>
class AVSensor_ViscousFlux3D_LinearInGradient : public DiffusionMatrix
{
public:
  using DiffusionMatrix::diffusionViscous;

  template <class T>
  using ArrayQ = typename DiffusionMatrix::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename DiffusionMatrix::template MatrixQ<T>;  // matrices

  static const int iSens = DiffusionMatrix::iSens;

  template< class... Args > // cppcheck-suppress noExplicitConstructor
  AVSensor_ViscousFlux3D_LinearInGradient(Args&&... args) : DiffusionMatrix(std::forward<Args>(args)...) {}

  bool fluxViscousLinearInGradient() const { return true; }

  // viscous flux: Fv(Q, QX)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const PDE& pde, const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& f, ArrayQ<Tf>& g, ArrayQ<Tf>& h ) const
  {
    MatrixQ<Tq> kxx=0, kxy=0, kxz=0,
                kyx=0, kyy=0, kyz=0,
                kzx=0, kzy=0, kzz=0;

    diffusionViscous( pde, param, x, y, z, time,
                      q, qx, qy, qz,
                      kxx, kxy, kxz,
                      kyx, kyy, kyz,
                      kzx, kzy, kzz);

    f(iSens) -= kxx(iSens,iSens)*qx(iSens) + kxy(iSens,iSens)*qy(iSens) + kxz(iSens,iSens)*qz(iSens);
    g(iSens) -= kyx(iSens,iSens)*qx(iSens) + kyy(iSens,iSens)*qy(iSens) + kyz(iSens,iSens)*qz(iSens);
    h(iSens) -= kzx(iSens,iSens)*qx(iSens) + kzy(iSens,iSens)*qy(iSens) + kzz(iSens,iSens)*qz(iSens);
  }

  // viscous flux: normal flux with left and right states
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const PDE& pde, const Tp& paramL, const Tp& paramR,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& fn ) const
  {
    ArrayQ<Tf> fxL = 0, fxR = 0;
    ArrayQ<Tf> fyL = 0, fyR = 0;
    ArrayQ<Tf> fzL = 0, fzR = 0;

    fluxViscous(pde, paramL, x, y, z, time, qL, qxL, qyL, qzL, fxL, fyL, fzL);
    fluxViscous(pde, paramR, x, y, z, time, qR, qxR, qyR, qzR, fxR, fyR, fzR);

    fn(iSens) += 0.5*(fxL(iSens) + fxR(iSens))*nx + 0.5*(fyL(iSens) + fyR(iSens))*ny + 0.5*(fzL(iSens) + fzR(iSens))*nz;
  }
};

//----------------------------------------------------------------------------//
// diffusion matrix: uniform
//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize>
class AVSensor_DiffusionMatrix3D_Uniform
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD3>::template MatrixQ<T>;  // matrices

  static const int D = 3;                     // physical dimensions

  bool hasFluxViscous() const { return true; }

  static const int iSens = PDETraitsSize<PhysD3>::iSens;

  explicit AVSensor_DiffusionMatrix3D_Uniform( const Real kxx, const Real kxy, const Real kxz,
                                               const Real kyx, const Real kyy, const Real kyz,
                                               const Real kzx, const Real kzy, const Real kzz )
  : kxx_( kxx ), kxy_( kxy ), kxz_( kxz ),
    kyx_( kyx ), kyy_( kyy ), kyz_( kyz ),
    kzx_( kzx ), kzy_( kzy ), kzz_( kzz ) {}

  template <class PDE, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde, const Tp& param,
                         const Real x, const Real y, const Real z, const Real& time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
                         MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
  {
    kxx(iSens, iSens) += kxx_; kxy(iSens, iSens) += kxy_; kxz(iSens, iSens) += kxz_;
    kyx(iSens, iSens) += kyx_; kyy(iSens, iSens) += kyy_; kyz(iSens, iSens) += kyz_;
    kzx(iSens, iSens) += kzx_; kzy(iSens, iSens) += kzy_; kzz(iSens, iSens) += kzz_;
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class Tp, class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const PDE& pde,
                     const Tp& param,
                     const Real& x, const Real& y, const Real z, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     const ArrayQ<Th>& qzx, const ArrayQ<Th>& qzy, const ArrayQ<Th>& qzz,
                     ArrayQ<Tf>& strongPDE ) const
  {
    // for sensor q = u;
    strongPDE(iSens) -=  kxx_*qxx(iSens) + kxy_*qyx(iSens) + kxz_*qzx(iSens)
                       + kyx_*qyx(iSens) + kyy_*qyy(iSens) + kyz_*qzy(iSens)
                       + kzx_*qzx(iSens) + kzy_*qzy(iSens) + kzz_*qzz(iSens);
  }

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  inline void
  jacobianFluxViscous(
      const PDE& pde,
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
  {
    // Nothing here
  }

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class PDE, class Tp, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const PDE& pde,
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z) const
  {
    // grad(K) = 0 (since K is constant)
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real kxx_, kxy_, kxz_,
             kyx_, kyy_, kyz_,
             kzx_, kzy_, kzz_;          // constant diffusion matrix
};

template <template <class> class PDETraitsSize>
using AVSensor_ViscousFlux3D_Uniform = AVSensor_ViscousFlux3D_LinearInGradient<AVSensor_DiffusionMatrix3D_Uniform<PDETraitsSize>>;

//----------------------------------------------------------------------------//
// Diffusion matrix with generalized h-scale
// Formulation from Masa Yano's PhD thesis - page 191
//----------------------------------------------------------------------------//
template <template <class> class PDETraitsSize>
class AVSensor_DiffusionMatrix3D_GenHScale
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD3>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD3>::template MatrixQ<T>;  // matrices

  static const int D = 3;                     // physical dimensions

  typedef DLA::VectorS<D, Real> VectorX;

  static const int iSens = PDETraitsSize<PhysD3>::iSens;

  bool hasFluxViscous() const { return true; }

  explicit AVSensor_DiffusionMatrix3D_GenHScale( const Real order, const Real C1 = 3, const Real C2 = 5) :
    order_(order),
    C1_(C1),
    C2_(C2)
  {
    // Nothing
  }

  template <class PDE, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde, const DLA::MatrixSymS<D,Real>& param,
                         const Real x, const Real y, const Real z, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
                         MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
  {
    DLA::MatrixSymS<D,Real> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D,Real> Hsq = H*H;

    Tq eta = C2_;

    kxx(iSens,iSens) += eta*Hsq(0,0);  kxy(iSens,iSens) += eta*Hsq(0,1);  kxz(iSens,iSens) += eta*Hsq(0,2);
    kyx(iSens,iSens) += eta*Hsq(1,0);  kyy(iSens,iSens) += eta*Hsq(1,1);  kyz(iSens,iSens) += eta*Hsq(1,2);
    kzx(iSens,iSens) += eta*Hsq(2,0);  kzy(iSens,iSens) += eta*Hsq(2,1);  kzz(iSens,iSens) += eta*Hsq(2,2);
  }

  template <class PDE, class Th, class Tp, class Tq, class Tg, class Tk>
  void diffusionViscous( const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Th>, Tp, TupleClass<>>& param,
                         const Real x, const Real y, const Real z, const Real time,
                         const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                         MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
                         MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
                         MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
  {
    diffusionViscous(pde, param.left(), x, y, z, time,
                     q, qx, qy, qz,
                     kxx, kxy, kxz,
                     kyx, kyy, kyz,
                     kzx, kzy, kzz);
  }

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class PDE, class Tp, class Tq, class Tg, class Tf>
  inline void
  jacobianFluxViscous(
      const PDE& pde,
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
  {
    // Nothing here - Fv(sensor) is independent of U(sensor)
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class Tp, class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const PDE& pde, const DLA::MatrixSymS<D,Tp>& param,
                     const Real& x, const Real& y, const Real z, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     const ArrayQ<Th>& qzx, const ArrayQ<Th>& qzy, const ArrayQ<Th>& qzz,
                     ArrayQ<Tf>& strongPDE ) const
  {
//    SANS_DEVELOPER_EXCEPTION("Not tested.");
    DLA::MatrixSymS<D,Tp> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D,Tp> Hsq = H*H;
    strongPDE -= C2_*(              Hsq(0,0)  * qxx
                      + (Hsq(0,1) + Hsq(1,0)) * qyx
                      +             Hsq(1,1)  * qyy
                      + (Hsq(0,1) + Hsq(2,0)) * qzx
                      + (Hsq(1,2) + Hsq(2,1)) * qzy
                      +             Hsq(2,2)  * qzz );
  }

  // strong form of viscous flux: d(Fv)/d(X)
  template <class PDE, class Tp, class Tq, class Tg, class Th, class Tf>
  inline void
  strongFluxViscous( const PDE& pde,
                     const ParamTuple<DLA::MatrixSymS<D,Tp>, Real, TupleClass<>>& param,
                     const Real& x, const Real& y, const Real z, const Real& time,
                     const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
                     const ArrayQ<Th>& qxx,
                     const ArrayQ<Th>& qyx, const ArrayQ<Th>& qyy,
                     const ArrayQ<Th>& qzx, const ArrayQ<Th>& qzy, const ArrayQ<Th>& qzz,
                     ArrayQ<Tf>& strongPDE ) const
  {
//    SANS_DEVELOPER_EXCEPTION("Not tested.");
    DLA::MatrixSymS<D,Tp> H = exp(param.left()); //generalized h-tensor
    DLA::MatrixSymS<D,Tp> Hsq = H*H;
    strongPDE -= C2_*(              Hsq(0,0)  * qxx
                      + (Hsq(0,1) + Hsq(1,0)) * qyx
                      +             Hsq(1,1)  * qyy
                      + (Hsq(0,1) + Hsq(2,0)) * qzx
                      + (Hsq(1,2) + Hsq(2,1)) * qzy
                      +             Hsq(2,2)  * qzz );
  }

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class PDE, class Tp, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
    const PDE& pde,
    const Tp& param, const Real x, const Real y, const Real z, const Real time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
    MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z) const
  {
    // grad(K) = grad(H) = 0 (since we dont have parameter gradients)
  }

  template <class PDE, class Thp, class Tp, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
    const PDE& pde, const ParamTuple<DLA::MatrixSymS<D,Thp>, Tp, TupleClass<>>& param,
    const Real x, const Real y, const Real z, const Real time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
    MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z) const
  {
    diffusionViscousGradient(pde, param.left(), x, y, z, time,
                             q, qx, qy, qz,
                             qxx,
                             qxy, qyy,
                             qxz, qyz, qzz,
                             kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                             kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                             kxz_z, kyz_z, kzx_z, kzy_z, kzz_z );
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real order_; // solution order
  const Real C1_; // time scaling
  const Real C2_; // diffusion scaling
};

template <template <class> class PDETraitsSize>
using AVSensor_ViscousFlux3D_GenHScale = AVSensor_ViscousFlux3D_LinearInGradient<AVSensor_DiffusionMatrix3D_GenHScale<PDETraitsSize>>;

} //namespace SANS

#endif  // AVSENSOR_VISCOUSFLUX3D_H
