// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef PDEARTIFICIALVISCOSITY3D_H
#define PDEARTIFICIALVISCOSITY3D_H

// PDE class

#include "tools/SANSnumerics.h"     // Real
#include "tools/SANSException.h"

#include "Topology/Dimension.h"

#include "LinearAlgebra/DenseLinAlg/tools/PromoteSurreal.h"
#include "LinearAlgebra/DenseLinAlg/DynamicSize/VectorD.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/VectorS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixSymS.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Exp.h"
#include "LinearAlgebra/DenseLinAlg/StaticSize/MatrixS_Det.h"

#include "AVVariable.h"

#include "Field/Tuple/ParamTuple.h"
#include "tools/Tuple.h"
#include "tools/smoothmath.h"

#include <iostream>
#include <limits>
#include <string>


namespace SANS
{

//----------------------------------------------------------------------------//
// PDE class: 3D Artificial Viscosity wrapper
//
// template parameters:
//   T                           solution DOF data type (e.g. double)
//   PDETraitsSize               define PDE size-related features
//     N, D                      PDE size, physical dimensions
//     ArrayQ                    solution/residual arrays
//     MatrixQ                   matrices (e.g. flux jacobian)
//   PDETraitsModel              define PDE model-related features
//     QType                     solution variable set (e.g. primitive)
//     QInterpret                solution variable interpreter
//     GasModel                  gas model (e.g. ideal gas law)
//
// member functions:
//   .hasFluxAdvectiveTime        T/F: PDE has unsteady conservative flux term
//   .hasFluxAdvective           T/F: PDE has advective/inviscid flux term
//
//   .fluxAdvectiveTime          temporal flux: Ft(Q)
//   .jacobianFluxAdvectiveTime  jacobian of temporal flux: d(Ft)/dU
//   .masterState                master state: U(Q)
//   .jacobianMasterState        jacobian of master state wrt q: U(Q)
//   .fluxAdvective              advective/inviscid fluxes: F(Q)
//   .isValidState               T/F: determine if state is physically valid (e.g. rho > 0)
//   .setDOFFrom                 set from primitive variable array
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize, class PDETraitsModel>
class PDEmitAVSensor3D : public PDETraitsModel::PDEBase
{
public:

  typedef PhysD3 PhysDim;

  typedef typename PDETraitsModel::PDEBase BaseType;

  using BaseType::D;               // physical dimensions
  using BaseType::N;               // total solution variables

  static_assert( D == PDETraitsSize<PhysDim>::D, "Dimension mismatch!");
  static_assert( N == PDETraitsSize<PhysDim>::N, "Solution variable mismatch!");

  static const int Nparam = 1;               // total solution parameters

  template <class T>
  using VectorD = DLA::VectorD<T>;

  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysDim>::template ArrayQ<T>;     // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysDim>::template MatrixQ<T>;   // matrices

  template <class TP>
  using ArrayParam = DLA::VectorS<Nparam,TP>;  // parameter array type

  template <class T>
  using MatrixParam = DLA::MatrixS<N,Nparam,T>;         // e.g. jacobian of PDEs w.r.t. parameters

  typedef typename PDETraitsModel::AdvectiveFlux AdvectiveFlux; //advective flux for sensor PDE
  typedef typename PDETraitsModel::ViscousFlux ViscousFlux; //viscous flux for sensor PDE
  typedef typename PDETraitsModel::Source Source; //source term for sensor PDE

  // Constructor forwards arguments to PDE class using varargs
  template< class... PDEArgs >
  PDEmitAVSensor3D(const AdvectiveFlux& adv, const ViscousFlux& visc, const Source& src,
                           const bool isSensorSteady, PDEArgs&&... args) :
    BaseType(std::forward<PDEArgs>(args)...),
    adv_(adv), visc_(visc),
    source_(src),
    isSensorSteady_(isSensorSteady),
    C1_(3.0) // Need a better way to sort that
  {
    //Nothing
  }

  ~PDEmitAVSensor3D() {}

  PDEmitAVSensor3D& operator=( const PDEmitAVSensor3D& ) = delete;

  static const int iSens = PDETraitsSize<PhysDim>::iSens;

  // flux components
  bool hasFluxAdvectiveTime() const { return true; }
  bool hasFluxAdvective() const { return ( BaseType::hasFluxAdvective() || adv_.hasFluxAdvective() ); }
  bool hasFluxViscous() const { return true; }
  bool hasSource() const { return ( BaseType::hasSource() || source_.hasSourceTerm() ); }
  bool hasSourceTrace() const { return ( BaseType::hasSourceTrace() || source_.hasSourceTrace() ); }
  bool hasSourceLiftedQuantity() const { return source_.hasSourceLiftedQuantity(); }
  bool hasForcingFunction() const { return BaseType::hasForcingFunction(); }

  // viscous flux is of the form K(q).gradu
  bool fluxViscousLinearInGradient() const { return true; }
  bool needsSolutionGradientforSource() const
  {
    return ( BaseType::needsSolutionGradientforSource() || source_.needsSolutionGradientforSource() );
  }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tp, class Tf>
  void fluxAdvectiveTime(
      const SANS::DLA::MatrixSymS<D, Tp>& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(param, x, y, z, time, q, ft);
    if (!isSensorSteady_)
    {
      DLA::MatrixSymS<D, Tp> H = exp(param); //generalized h-tensor
      DLA::MatrixSymS<D, Tp> Hsq = H*H;

      T tau = 1.0;
      typedef DLA::VectorS<D, Real> VectorX;
      VectorX hPrincipal;
      DLA::EigenValues(H, hPrincipal);

      //Get minimum length scale
      Real hmin = std::numeric_limits<Real>::max();
      for (int d = 0; d < D; d++)
        hmin = min(hmin, hPrincipal[d]);

      T lambda = 0;
      speedCharacteristic( param, x, y, z, time, q, lambda );

      // characteristic time of the PDE
      tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);

      ft(iSens) += tau*q(iSens);
    }
  }

  // unsteady temporal flux: Ft(Q)
  template <class T, class Tp, class R, class Tf>
  void fluxAdvectiveTime(
      const ParamTuple<SANS::DLA::MatrixSymS<D, Tp>, R, SANS::TupleClass<0> >& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tf>& ft ) const
  {
    BaseType::fluxAdvectiveTime(param, x, y, z, time, q, ft);
    if (!isSensorSteady_)
    {
      DLA::MatrixSymS<D, Tp> H = exp(param.left()); //generalized h-tensor
      DLA::MatrixSymS<D, Tp> Hsq = H*H;

      T tau = 1.0;
      typedef DLA::VectorS<D, Real> VectorX;
      VectorX hPrincipal;
      DLA::EigenValues(H, hPrincipal);

      //Get minimum length scale
      Real hmin = std::numeric_limits<Real>::max();
      for (int d = 0; d < D; d++)
        hmin = min(hmin, hPrincipal[d]);

      T lambda = 0;
      speedCharacteristic( param, x, y, z, time, q, lambda );

      // characteristic time of the PDE
      tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);

      ft(iSens) += tau*q(iSens);
    }
  }

  // master state: U(Q)
  template <class T, class Tp, class Tu>
  void masterState(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const;

  // jacobian of unsteady temporal flux: d(Ft)/dU
  template<class T, class Tp>
  void jacobianMasterState(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<T>& J ) const
  {
    BaseType::jacobianMasterState(param, x, y, z, time, q, J);
    J(iSens,iSens) = DLA::Identity();
  }

  // master state Gradient: Ux(Q)
  template<class Tp, class Tq, class Tg, class Tf>
  void masterStateGradient(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& Vx, ArrayQ<Tf>& Vy, ArrayQ<Tf>& Vz  ) const
  {
    BaseType::masterStateGradient(param, x, y, z, time, q, qx, qy, qz, Vx, Vy, Vz);
    Vx[iSens] = qx[iSens];
    Vy[iSens] = qy[iSens];
    Vz[iSens] = qz[iSens];
  }

  // master state Hessian: Uxx(Q), Uxy(Q), Uyy(Q)
  template<class Tp, class Tq, class Tg, class Th, class Tf>
  void masterStateHessian(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qxz,
      const ArrayQ<Th>& qyy,const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& Vxx, ArrayQ<Tf>& Vxy, ArrayQ<Tf>& Vxz,
      ArrayQ<Tf>& Vyy, ArrayQ<Tf>& Vyz, ArrayQ<Tf>& Vzz ) const
  {
    BaseType::masterStateHessian(param, x, y, z, time, q, qx, qy, qz,
                                 qxx, qxy, qxz, qyy, qyz, qzz,
                                 Vxx, Vxy, Vxz, Vyy, Vyz, Vzz );

    Vxx[iSens] = qxx[iSens];
    Vxy[iSens] = qxy[iSens];
    Vxz[iSens] = qxz[iSens];
    Vyy[iSens] = qyy[iSens];
    Vyz[iSens] = qyz[iSens];
    Vzz[iSens] = qzz[iSens];
  }

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class Tf>
  void jacobianFluxAdvectiveTime(
      const DLA::MatrixSymS<D, Real>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // jacobian of master state wrt q: dU(Q)/dQ
  template<class T, class Tp, class Tf>
  void jacobianFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<D, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <class T>
  void strongFluxAdvectiveTime(
      const DLA::MatrixSymS<D, Real>& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const;

  // unsteady conservative flux: d(U)/d(t)
  template <class T, class Tp>
  void strongFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<D, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const;

  // advective flux: F(Q)
  template <class T, class Tp, class Ts>
  void fluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q,
      ArrayQ<Ts>& f, ArrayQ<Ts>& g, ArrayQ<Ts>& h ) const;

  template <class T, class Tp, class Ts>
  void fluxAdvectiveUpwind(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Ts>& f ) const;

  template <class T, class Tp, class Tf>
  void fluxAdvectiveUpwindSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& qL, const ArrayQ<T>& qR,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt, ArrayQ<Tf>& f ) const;

  // jacobian of advective flux wrt conservation variables: d(F)/d(U) (advective velocity matrix)
  template <class Tp, class Tq, class Ta>
  void jacobianFluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      MatrixQ<Ta>& ax, MatrixQ<Ta>& ay, MatrixQ<Ta>& az ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz,
      MatrixQ<Tf>& a ) const;

  // absolute value jacobian of advective flux wrt conservation variables: |n . d(F)/d(U)|
  template <class Tp, class Tq, class Tf>
  void jacobianFluxAdvectiveAbsoluteValueSpaceTime(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q,
      const Real& nx, const Real& ny, const Real& nz, const Real& nt,
      MatrixQ<Tf>& a ) const;

  // strong form advective fluxes
  template <class Tp, class Tq, class Tg, class Tf>
  void strongFluxAdvective(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& strongPDE ) const;

  // viscous flux: Fv(Q, Qx)
  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Tf>& fx, ArrayQ<Tf>& fy, ArrayQ<Tf>& fz ) const;

  template <class Tp, class Tq, class Tg, class Tf>
  void fluxViscous(
      const Tp& paramL, const Tp& paramR,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& fn ) const;

  // viscous diffusion matrix: d(Fv)/d(Ux)
  template <class Tq, class Tp, class Tg, class Tk>
  void diffusionViscous(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
      MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
      MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz) const;

  // gradient of viscous diffusion matrix: div . d(Fv)/d(UX)
  template <class Tp, class Tq, class Tg, class Th, class Tk>
  void diffusionViscousGradient(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
      MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
      MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z) const;

  // jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
  template <class Tp, class Tq, class Tg, class Tf>
  void jacobianFluxViscous(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const;

  // strong form viscous fluxes
  template <class Tp, class Tq, class Tg, class Th, class Tf>
  void strongFluxViscous(
      const Tp& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      const ArrayQ<Th>& qxx,
      const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
      const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
      ArrayQ<Tf>& strongPDE ) const;

  // solution-dependent source: S(x, Q, Qx)
  template <class Tq, class Tg, class Tp, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const;

  // solution-dependent source with lifted quantity: S(X, LQ, Q, QX)
  template <class Tp, class Tlq, class Tq, class Tg, class Ts>
  void source(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      ArrayQ<Ts>& source ) const;

  // dual-consistent source
  template <class Tq, class Tp, class Tg, class Ts>
  void sourceTrace(
      const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& zR,
      const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
      const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
      ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const;

  // trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
  template <class Tp, class Tq, class Ts>
  void sourceLiftedQuantity(
      const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
      const Tp& paramR, const Real& xR, const Real& yR, const Real& zR, const Real& time,
      const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, Ts& s ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSource(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceHACK(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const;

  // jacobian of source wrt conservation variables: d(S)/d(U)
  template <class Tp, class Tq, class Tg, class Ts>
  void jacobianSourceAbsoluteValue(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
      MatrixQ<Ts>& dsdu ) const
  {
//    SANS_DEVELOPER_EXCEPTION("3D AV JACOBIANSOURCEABSOLUTEVALUE NOT IMPLEMENTED");
    BaseType::jacobianSourceAbsoluteValue(param, x, y, z, time, q, qx, qy, qz, dsdu);
  }

  template <class Tp, class Tq, class Tg, class Ts>
    void jacobianGradientSource(
        const Tp& param,
        const Real& x, const Real& y, const Real& z, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const;

  template <class Tp, class Tq, class Tg, class Ts>
    void jacobianGradientSourceHACK(
        const Tp& param,
        const Real& x, const Real& y, const Real& z, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
        MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const;

  template <class Tp, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradient(
        const Tp& param,
        const Real& x, const Real& y, const Real& z, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
        MatrixQ<Ts>& div_dgradsdu ) const
  {
//    SANS_DEVELOPER_EXCEPTION("3D AV JACOBIANGRADIENTSOURCEGRADIENT NOT IMPLEMENTED");
    BaseType::jacobianGradientSourceGradient(param, x, y, z, time,
                                             q, qx, qy, qz,
                                             qxx, qxy, qyy,
                                             qxz, qyz, qzz, div_dgradsdu);
  }

  template <class Tp, class Tq, class Tg, class Th, class Ts>
    void jacobianGradientSourceGradientHACK(
        const Tp& param,
        const Real& x, const Real& y, const Real& z, const Real& time,
        const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
        const ArrayQ<Th>& qxx, const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
        const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
        MatrixQ<Ts>& div_dgradsdu ) const
  {
    SANS_DEVELOPER_EXCEPTION("3D AV HACKED JACOBIANGRADIENTSOURCEGRADIENT NOT IMPLEMENTED");
  }

  // right-hand-side forcing function
  template <class T, class Tp>
  void forcingFunction( const Tp& param,
                        const Real& x, const Real& y, const Real& z, const Real& time, ArrayQ<T>& forcing ) const
  {
    BaseType::forcingFunction(param, x, y, z, time, forcing );
  }

  // characteristic speed (needed for timestep)
  template <class T, class Tp>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const Real& dx, const Real& dy, const Real& dz, const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(param, x, y, z, time, dx, dy, dz, q, speed);
  }

  // characteristic speed
  template <class T, class Tp>
  void speedCharacteristic(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, T& speed ) const
  {
    BaseType::speedCharacteristic(param, x, y, z, time, q, speed);
  }

  // update fraction needed for physically valid state
  template <class Tp>
  void updateFraction(
      const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<Real>& q, const ArrayQ<Real>& dq,
      const Real maxChangeFraction, Real& updateFraction ) const
  {
    BaseType::updateFraction(param, x, y, z, time, q, dq, maxChangeFraction, updateFraction);
  }

  // is state physically valid
  bool isValidState( const ArrayQ<Real>& q ) const
  {
    return BaseType::isValidState(q);
  }

  // set from primitive variable array
  template <class T>
  void setDOFFrom(
      ArrayQ<T>& q, const T data[], const std::string name[], int nn ) const
  {
    BaseType::setDOFFrom(q, data, name, nn);
  }

  // set from primitive variable
  template<class T, class Variables>
  void setDOFFrom(
      ArrayQ<T>& q, const AVVariableType<Variables>& data ) const;

  // set from primitive variable
  template<class Variables>
  ArrayQ<Real> setDOFFrom( const AVVariableType<Variables>& data ) const;

  // set from variable
  ArrayQ<Real> setDOFFrom( const PyDict& d ) const;

  // interpret residuals of the solution variable
  template <class T>
  void interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const;

  void dump( int indentSize, std::ostream& out = std::cout ) const;

private:
  const AdvectiveFlux adv_;
  const ViscousFlux visc_;
  const Source source_;
  const bool isSensorSteady_;
  const Real C1_; // time scaling
};


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Tu>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::masterState(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, ArrayQ<Tu>& uCons ) const
{
  BaseType::masterState(param, x, y, z, time, q, uCons);
  uCons(iSens) = q(iSens);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tf>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::
jacobianFluxAdvectiveTime(
    const DLA::MatrixSymS<D, Real>& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
   BaseType::jacobianFluxAdvectiveTime(param, x, y, z, time, q, dudq);
   if (!isSensorSteady_)
   {
     DLA::MatrixSymS<D, Real> H = exp(param); //generalized h-tensor
     DLA::MatrixSymS<D, Real> Hsq = H*H;

     T tau = 1.0;
     typedef DLA::VectorS<D, Real> VectorX;
     VectorX hPrincipal;
     DLA::EigenValues(H, hPrincipal);

     //Get minimum length scale
     Real hmin = std::numeric_limits<Real>::max();
     for (int d = 0; d < D; d++)
       hmin = min(hmin, hPrincipal[d]);

     T lambda = 0;
     speedCharacteristic( param, x, y, z, time, q, lambda );

     // characteristic time of the PDE
     tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
     T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

     ArrayQ<SurrealS<N,T>> qSurreal = 0;
     for (int i = 0; i < N; i++)
     {
       qSurreal[i].value() = q[i];
     }

     for (int i = 0; i < N; i++)
     {
       qSurreal[i].deriv() = 1.0;

       SurrealS<N,T> dlambda_dq = 0;
       speedCharacteristic( param, x, y, z, time, qSurreal, dlambda_dq );

       qSurreal[i].deriv() = 0.0;

       dudq(iSens,i) += q(iSens)*dtau_dlambda*dlambda_dq.deriv();
     }

     dudq(iSens,iSens) += tau;
   }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template<class T, class Tp, class Tf>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::
jacobianFluxAdvectiveTime(
      const ParamTuple<DLA::MatrixSymS<D, Real>, Tp, TupleClass<> >& param,
      const Real& x, const Real& y, const Real& z, const Real& time,
      const ArrayQ<T>& q, MatrixQ<Tf>& dudq ) const
{
   BaseType::jacobianFluxAdvectiveTime(param, x, y, z, time, q, dudq);
   if (!isSensorSteady_)
   {
     DLA::MatrixSymS<D, Real> H = exp(param.left()); //generalized h-tensor
     DLA::MatrixSymS<D, Real> Hsq = H*H;

     T tau = 1.0;
     typedef DLA::VectorS<D, Real> VectorX;
     VectorX hPrincipal;
     DLA::EigenValues(H, hPrincipal);

     //Get minimum length scale
     Real hmin = std::numeric_limits<Real>::max();
     for (int d = 0; d < D; d++)
       hmin = min(hmin, hPrincipal[d]);

     T lambda = 0;
     speedCharacteristic( param, x, y, z, time, q, lambda );

     // characteristic time of the PDE
     tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);
     T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

     ArrayQ<SurrealS<N,T>> qSurreal = 0;
     for (int i = 0; i < N; i++)
     {
       qSurreal[i].value() = q[i];
     }

     for (int i = 0; i < N; i++)
     {
       qSurreal[i].deriv() = 1.0;

       SurrealS<N,T> dlambda_dq = 0;
       speedCharacteristic( param, x, y, z, time, qSurreal, dlambda_dq );

       qSurreal[i].deriv() = 0.0;

       dudq(iSens,i) += q(iSens)*dtau_dlambda*dlambda_dq.deriv();
     }

     dudq(iSens,iSens) += tau;
   }
}


template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const DLA::MatrixSymS<D, Real>& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
{
  BaseType::strongFluxAdvectiveTime(param, x, y, z, time, q, qt, dudt);
  if (!isSensorSteady_)
  {
    DLA::MatrixSymS<D, Real> H = exp(param); //generalized h-tensor
    DLA::MatrixSymS<D, Real> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, y, z, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = q;
    T dlambda_dt = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, y, z, time, qSurreal, dlambda_dq );
      dlambda_dt += dlambda_dq.deriv() * qt(i);

      qSurreal[i].deriv() = 0.0;
    }
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    dudt(iSens) += tau*qt(iSens) + dtau_dlambda*dlambda_dt*q(iSens);
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::strongFluxAdvectiveTime(
    const ParamTuple<DLA::MatrixSymS<D, Real>, Tp, TupleClass<> >& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q, const ArrayQ<T>& qt, ArrayQ<T>& dudt ) const
{
  BaseType::strongFluxAdvectiveTime(param, x, y, z, time, q, qt, dudt);
  if (!isSensorSteady_)
  {
    DLA::MatrixSymS<D, Real> H = exp(param.left()); //generalized h-tensor
    DLA::MatrixSymS<D, Real> Hsq = H*H;

    T tau = 1.0;
    typedef DLA::VectorS<D, Real> VectorX;
    VectorX hPrincipal;
    DLA::EigenValues(H, hPrincipal);

    //Get minimum length scale
    Real hmin = std::numeric_limits<Real>::max();
    for (int d = 0; d < D; d++)
      hmin = min(hmin, hPrincipal[d]);

    T lambda = 0;
    speedCharacteristic( param, x, y, z, time, q, lambda );

    // characteristic time of the PDE
    tau = hmin/(C1_*max(BaseType::getOrder(),1)*lambda);

    ArrayQ<SurrealS<N,T>> qSurreal = q;
    T dlambda_dt = 0;
    for (int i = 0; i < N; i++)
    {
      qSurreal[i].deriv() = 1.0;

      SurrealS<N,T> dlambda_dq = 0;
      speedCharacteristic( param, x, y, z, time, qSurreal, dlambda_dq );
      dlambda_dt += dlambda_dq.deriv() * qt(i);

      qSurreal[i].deriv() = 0.0;
    }
    T dtau_dlambda =  - hmin/(C1_*max(BaseType::getOrder(),1)*lambda*lambda);

    dudt(iSens) += tau*qt(iSens) + dtau_dlambda*dlambda_dt*q(iSens);
  }
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::fluxAdvective(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& q,
    ArrayQ<Ts>& fx, ArrayQ<Ts>& fy, ArrayQ<Ts>& fz ) const
{
  BaseType::fluxAdvective(param, x, y, z, time, q, fx, fy, fz);
  adv_.flux( param, x, y, z, time, q, fx, fy, fz );
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwind(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nz,
    ArrayQ<Ts>& f ) const
{
  BaseType::fluxAdvectiveUpwind(param, x, y, z, time, qL, qR, nx, ny, nz, f);
  adv_.fluxUpwind(param, x, y, z, time, qL, qR, nx, ny, nz, f);
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Tp, class Tf>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::fluxAdvectiveUpwindSpaceTime(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<T>& qL, const ArrayQ<T>& qR, const Real& nx, const Real& ny, const Real& nz, const Real& nt,
    ArrayQ<Tf>& f ) const
{
  BaseType::fluxAdvectiveUpwindSpaceTime(param, x, y, z, time, qL, qR, nx, ny, nz, nt, f);
  adv_.fluxUpwindSpaceTime(param, x, y, z, time, qL, qR, nx, ny, nz, nt, isSensorSteady_, f);
}

// advective flux jacobian: d(F)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Ta>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvective(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q,
    MatrixQ<Ta>& ax, MatrixQ<Ta>& ay, MatrixQ<Ta>& az ) const
{
  BaseType::jacobianFluxAdvective(param, x, y, z, time, q, ax, ay, az);
  adv_.jacobian( param, x, y, z, time, q, ax, ay, az );
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tf>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValue(
  const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz, MatrixQ<Tf>& mtx ) const
{
  BaseType::jacobianFluxAdvectiveAbsoluteValue(param, x, y, z, time,
                                               q, nx, ny, nz,
                                               mtx);
//  adv_.jacobian( param, x, y, z, time, q, ax, ay, az );
//  SANS_DEVELOPER_EXCEPTION("jacobianFluxAdvectiveAbsoluteValue not implemented yet.");
}

// absolute value of advective flux jacobian: |n . d(F)/d(U)|
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tf>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::jacobianFluxAdvectiveAbsoluteValueSpaceTime(
  const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
  const ArrayQ<Tq>& q, const Real& nx, const Real& ny, const Real& nz, const Real& nt,
  MatrixQ<Tf>& mtx ) const
{
  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor3D::jacobianFluxAdvectiveAbsoluteValueSpaceTime is not implemented");
}

// strong form of advective flux: d(F)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::strongFluxAdvective(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Tf>& strongPDE ) const
{
  BaseType::strongFluxAdvective( param, x, y, z, time, q, qx, qy, qz, strongPDE );
  adv_.strongFlux( param, x, y, z, time, q, qx, qy, qz, strongPDE );
}

// viscous flux
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Ts>& fx, ArrayQ<Ts>& fy, ArrayQ<Ts>& fz ) const
{
  BaseType::fluxViscous( param, x, y, z, time, q, qx, qy, qz, fx, fy, fz );
  visc_.fluxViscous( *this, param, x, y, z, time, q, qx, qy, qz, fx, fy, fz );
}

// viscous flux: normal flux with left and right states
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::fluxViscous(
    const Tp& paramL, const Tp& paramR,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
    const Real& nx, const Real& ny, const Real& nz, ArrayQ<Tf>& fn ) const
{
  BaseType::fluxViscous( paramL, paramR, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
  visc_.fluxViscous( *this, paramL, paramR, x, y, z, time, qL, qxL, qyL, qzL, qR, qxR, qyR, qzR, nx, ny, nz, fn );
}

// viscous diffusion matrix: d(Fv)/d(Ux)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tp, class Tg, class Tk>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::diffusionViscous(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tk>& kxx, MatrixQ<Tk>& kxy, MatrixQ<Tk>& kxz,
    MatrixQ<Tk>& kyx, MatrixQ<Tk>& kyy, MatrixQ<Tk>& kyz,
    MatrixQ<Tk>& kzx, MatrixQ<Tk>& kzy, MatrixQ<Tk>& kzz ) const
{
//  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor3D::diffusionViscous is not implemented");

#if 1
  BaseType::diffusionViscous( param, x, y, z, time,
                              q, qx, qy, qz,
                              kxx, kxy, kxz,
                              kyx, kyy, kyz,
                              kzx, kzy, kzz );
  visc_.diffusionViscous( *this, param, x, y, z, time,
                          q, qx, qy, qz,
                          kxx, kxy, kxz,
                          kyx, kyy, kyz,
                          kzx, kzy, kzz );
#endif
}


// gradient viscous diffusion matrix: div . d(Fv)/d(UX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Th, class Tk>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::
diffusionViscousGradient(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    MatrixQ<Tk>& kxx_x, MatrixQ<Tk>& kxy_x, MatrixQ<Tk>& kxz_x, MatrixQ<Tk>& kyx_x, MatrixQ<Tk>& kzx_x,
    MatrixQ<Tk>& kxy_y, MatrixQ<Tk>& kyx_y, MatrixQ<Tk>& kyy_y, MatrixQ<Tk>& kyz_y, MatrixQ<Tk>& kzy_y,
    MatrixQ<Tk>& kxz_z, MatrixQ<Tk>& kyz_z, MatrixQ<Tk>& kzx_z, MatrixQ<Tk>& kzy_z, MatrixQ<Tk>& kzz_z ) const
{
  BaseType::diffusionViscousGradient( param, x, y, z, time,
                                      q, qx, qy, qz,
                                      qxx,
                                      qxy, qyy,
                                      qxz, qyz, qzz,
                                      kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                      kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                      kxz_z, kyz_z, kzx_z, kzy_z, kzz_z  );
  visc_.diffusionViscousGradient( *this, param, x, y, z, time,
                                  q, qx, qy, qz,
                                  qxx,
                                  qxy, qyy,
                                  qxz, qyz, qzz,
                                  kxx_x, kxy_x, kxz_x, kyx_x, kzx_x,
                                  kxy_y, kyx_y, kyy_y, kyz_y, kzy_y,
                                  kxz_z, kyz_z, kzx_z, kzy_z, kzz_z  );
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::jacobianGradientSource(
    const Tp& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
{
  BaseType::jacobianGradientSource(param, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
//  source_.jacobianGradientSource(*this, param, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::jacobianGradientSourceHACK(
    const Tp& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdux, MatrixQ<Ts>& dsduy, MatrixQ<Ts>& dsduz ) const
{
  BaseType::jacobianGradientSourceHACK(param, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
//  source_.jacobianGradientSource(*this, param, x, y, z, time, q, qx, qy, qz, dsdux, dsduy, dsduz);
}

// strong form of viscous flux: d(Fv)/d(x)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Th, class Tf>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::strongFluxViscous(
    const Tp& param,
    const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    const ArrayQ<Th>& qxx,
    const ArrayQ<Th>& qxy, const ArrayQ<Th>& qyy,
    const ArrayQ<Th>& qxz, const ArrayQ<Th>& qyz, const ArrayQ<Th>& qzz,
    ArrayQ<Tf>& strongPDE ) const
{
//  SANS_DEVELOPER_EXCEPTION("PDEmitAVSensor3D::strongFluxViscous is not implemented");

  BaseType::strongFluxViscous( param,
                               x, y, z, time,
                               q, qx, qy, qz,
                               qxx, qxy, qyy, qxz, qyz, qzz,
                               strongPDE );
  visc_.strongFluxViscous( *this, param,
                           x, y, z, time,
                           q, qx, qy, qz,
                           qxx, qxy, qyy, qxz, qyz, qzz,
                           strongPDE );
}

// jacobian of viscous flux wrt conservation variables: d(Fv)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Tf>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::
jacobianFluxViscous(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Tf>& ax, MatrixQ<Tf>& ay, MatrixQ<Tf>& az ) const
{
  BaseType::jacobianFluxViscous( param,
                               x, y, z, time,
                               q, qx, qy, qz,
                               ax, ay, az );
  visc_.jacobianFluxViscous( *this, param,
                           x, y, z, time,
                           q, qx, qy, qz,
                           ax, ay, az );
}

// solution-dependent source: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tg, class Tp, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::source(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Ts>& source ) const
{
  BaseType::source( param, x, y, z, time, q, qx, qy, qz, source );
  source_.source( *this, param, x, y, z, time, q, qx, qy, qz, source );
}

// solution-dependent source with lifted_quantity: S(X, Q, QX)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tlq, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::source(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const Tlq& lifted_quantity, const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    ArrayQ<Ts>& source ) const
{
  BaseType::source( param, x, y, z, time, lifted_quantity, q, qx, qy, qz, source );
  source_.source( *this, param, x, y, z, time, lifted_quantity, q, qx, qy, qz, source );
}

// dual-consistent source
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tq, class Tp, class Tg, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::sourceTrace(
    const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
    const Tp& paramR, const Real& xR, const Real& yR, const Real& zR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tg>& qxL, const ArrayQ<Tg>& qyL, const ArrayQ<Tg>& qzL,
    const ArrayQ<Tq>& qR, const ArrayQ<Tg>& qxR, const ArrayQ<Tg>& qyR, const ArrayQ<Tg>& qzR,
    ArrayQ<Ts>& sourceL, ArrayQ<Ts>& sourceR ) const
{
  BaseType::sourceTrace(paramL, xL, yL, zL, paramR, xR, yR, zR, time,
                        qL, qxL, qyL, qzL, qR, qxR, qyR, qzR,
                        sourceL, sourceR);

  source_.sourceTrace(*this, paramL, xL, yL, zL, paramR, xR, yR, zR, time,
                      qL, qxL, qyL, qzL, qR, qxR, qyR, qzR,
                      sourceL, sourceR);
}

// trace quantity to be lifted in to volume for source terms (i.e. shock sensors)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::sourceLiftedQuantity(
    const Tp& paramL, const Real& xL, const Real& yL, const Real& zL,
    const Tp& paramR, const Real& xR, const Real& yR, const Real& zR,
    const Real& time,
    const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
    Ts& s ) const
{
  source_.sourceLiftedQuantity(paramL, xL, yL, zL, paramR, xR, yR, zR, time, qL, qR, s);
}

// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::jacobianSource(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdu ) const
{
  BaseType::jacobianSource(param, x, y, z, time, q, qx, qy, qz, dsdu);
  source_.jacobianSource(*this, param, x, y, z, time, q, qx, qy, qz, dsdu);
}


// jacobian of source wrt conservation variables: d(S)/d(U)
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Tp, class Tq, class Tg, class Ts>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::jacobianSourceHACK(
    const Tp& param, const Real& x, const Real& y, const Real& z, const Real& time,
    const ArrayQ<Tq>& q, const ArrayQ<Tg>& qx, const ArrayQ<Tg>& qy, const ArrayQ<Tg>& qz,
    MatrixQ<Ts>& dsdu ) const
{
  BaseType::jacobianSourceHACK(param, x, y, z, time, q, qx, qy, qz, dsdu);
  source_.jacobianSource(*this, param, x, y, z, time, q, qx, qy, qz, dsdu);
}

// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T, class Variables>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( ArrayQ<T>& q, const AVVariableType<Variables>& data ) const
{
  BaseType::setDOFFrom(q, data.cast());
  q(iSens) = data.cast().Sensor;
}

// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class Variables>
inline typename PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( const AVVariableType<Variables>& data ) const
{
  ArrayQ<Real> q;
  setDOFFrom( q, data );
  return q;
}

// set from primitive variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
inline typename PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::template ArrayQ<Real>
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::
setDOFFrom( const PyDict& d ) const
{
  ArrayQ<Real> q = BaseType::setDOFFrom(d);
  //q(iSens) = d.get("Sensor"); TODO
  return q;
}

// interpret residuals of the solution variable
template <template <class> class PDETraitsSize, class PDETraitsModel>
template <class T>
inline void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::
interpResidVariable( const ArrayQ<T>& rsdPDEIn, VectorD<T>& rsdPDEOut ) const
{
  BaseType::interpResidVariable(rsdPDEIn, rsdPDEOut);
  rsdPDEOut[BaseType::nMonitor()-1] = 0; // No residual for artificial viscosity
}

template <template <class> class PDETraitsSize, class PDETraitsModel>
void
PDEmitAVSensor3D<PDETraitsSize, PDETraitsModel>::dump( int indentSize, std::ostream& out ) const
{
  std::string indent(indentSize, ' ');
  out << indent << "PDEmitAVSensor3D: adv_ =" << std::endl;
  adv_.dump(indentSize+2, out);
  out << indent << "PDEmitAVSensor3D: visc_ =" << std::endl;
  visc_.dump(indentSize+2, out);
  out << indent << "PDEmitAVSensor3D: source_ =" << std::endl;
  source_.dump(indentSize+2, out);
  out << indent << "PDEmitAVSensor3D: pde_ =" << std::endl;
  BaseType::dump(indentSize+2, out);
}

} //namespace SANS

#endif  // PDEARTIFICIALVISCOSITY3D_H
