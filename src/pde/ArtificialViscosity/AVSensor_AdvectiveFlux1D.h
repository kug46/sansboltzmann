// Solution Adaptive Numerical Simulator (SANS)
// Copyright 2013-2019, Massachusetts Institute of Technology
// Licensed under The GNU Lesser General Public License, version 2.1
// See http://www.opensource.org/licenses/lgpl-2.1.php

#ifndef AVSENSOR_ADVECTIVEFLUX1D_H
#define AVSENSOR_ADVECTIVEFLUX1D_H

// 1-D Advection-Diffusion PDE: advective flux

#include "tools/SANSnumerics.h"     // Real
#include "tools/smoothmath.h"

#include "Topology/Dimension.h"

#include <cmath>  // fabs
#include <iostream>

namespace SANS
{

//----------------------------------------------------------------------------//
// advection velocity: uniform
//
// member functions:
//   .flux       advective flux: F(Q)
//   .jacobian   advective flux jacboian: d(F)/d(U)
//----------------------------------------------------------------------------//

template <template <class> class PDETraitsSize>
class AVSensor_AdvectiveFlux1D_Uniform
{
public:
  template <class T>
  using ArrayQ = typename PDETraitsSize<PhysD1>::template ArrayQ<T>;    // solution/residual arrays

  template <class T>
  using MatrixQ = typename PDETraitsSize<PhysD1>::template MatrixQ<T>;  // matrices

  bool hasFluxAdvective() const { return true; }

  explicit AVSensor_AdvectiveFlux1D_Uniform( const Real u ) : u_(u) {}
  ~AVSensor_AdvectiveFlux1D_Uniform() {}

  static const int iSens = PDETraitsSize<PhysD1>::iSens;

  template <class Tp, class Tq, class Tf>
  void flux( const Tp& param, const Real& x, const Real time,
             const ArrayQ<Tq>& q, ArrayQ<Tf>& fx ) const
  {
    fx(iSens) += u_*q(iSens);
  }

  // advective flux jacobian: d(F)/d(U)
  template<class Tp, class Tq, class Tf>
  void jacobian( const Tp& param, const Real x, const Real time,
                 const ArrayQ<Tq>& q, MatrixQ<Tf>& ax ) const
  {
    ax(iSens,iSens) += u_;
  }

  template<class Tp, class Tq, class Tf>
  void fluxUpwind( const Tp& param, const Real x, const Real time,
                   const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR, const Real& nx,
                   ArrayQ<Tf>& f ) const
  {
    f(iSens) += 0.5*(nx*u_)*(qR(iSens) + qL(iSens)) - 0.5*fabs(nx*u_)*(qR(iSens) - qL(iSens));
  }

  template<class Tp, class Tq, class Tf>
  void fluxUpwindSpaceTime( const Tp& param, const Real x, const Real time,
                            const ArrayQ<Tq>& qL, const ArrayQ<Tq>& qR,
                            const Real& nx, const Real& nt, const bool& isSensorSteady,
                            ArrayQ<Tf>& f ) const
  {
    Real v = 1.0; //unit speed in temporal direction
    if (isSensorSteady) v = 0.0;

    f(iSens) += 0.5*(nx*u_ + nt*v)*(qR(iSens) + qL(iSens)) - 0.5*fabs(nx*u_ + nt*v)*(qR(iSens) - qL(iSens));
  }

  // strong advective flux: div.F
  template<class Tp, class Tq, class Tf>
  void strongFlux( const Tp& param, const Real x, const Real time,
                   const ArrayQ<Tq>& q, const ArrayQ<Tq>& qx,
                   ArrayQ<Tf>& strongPDE ) const
  {
    strongPDE(iSens) += u_*qx(iSens);
  }

  void dump( int indentSize=0, std::ostream& out = std::cout ) const;

private:
  const Real u_;      // uniform advection velocity
};
} //namespace SANS

#endif  // AVSENSOR_ADVECTIVEFLUX1D_H
